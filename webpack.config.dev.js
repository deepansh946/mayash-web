/** @format */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://localhost:5001/',
      'webpack/hot/only-dev-server',
      './src/client/index.js',
    ],
    vendor: ['react', 'react-dom', 'react-router', 'react-router-dom'],
  },
  output: {
    path: path.resolve(__dirname, 'public', 'build'),
    publicPath: '/public/build/',
    pathinfo: true,
    filename: '[name].js',
    chunkFilename: '[name].bundle.js',
  },
  context: path.resolve(__dirname),
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    host: 'localhost',
    port: 5001,
    proxy: {
      '*': 'http://localhost:5000',
    },
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, 'public', 'build'),
    publicPath: '/public/build/',
  },
  module: {
    rules: [
      {
        test: /(\.js)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: ['env', 'stage-0', 'react'],
            plugins: ['transform-runtime'],
            cacheDirectory: true,
          },
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),

    // enable HMR globally
    // new webpack.HotModuleReplacementPlugin(),

    // prints more readable module names in the browser console on HMR updates
    new webpack.NamedModulesPlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      minChunks: Infinity,
      name: 'vendor',
      filename: 'vendor.js',
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'meta',
      chunks: ['vendor'],
      filename: 'meta.js',
    }),

    new HtmlWebpackPlugin({
      template: './templates/index.hbs',
      inject: 'body',
    }),
  ],
};
