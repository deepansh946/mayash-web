webpackJsonp([37],{

/***/ "./node_modules/dom-helpers/activeElement.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/activeElement.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = activeElement;

var _ownerDocument = __webpack_require__(/*! ./ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function activeElement() {
  var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _ownerDocument2.default)();

  try {
    return doc.activeElement;
  } catch (e) {/* ie throws if no active element */}
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/ownerDocument.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/ownerDocument.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ownerDocument;
function ownerDocument(node) {
  return node && node.ownerDocument || document;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/query/isWindow.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/query/isWindow.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getWindow;
function getWindow(node) {
  return node === node.window ? node : node.nodeType === 9 ? node.defaultView || node.parentWindow : false;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/util/scrollbarSize.js":
/*!********************************************************!*\
  !*** ./node_modules/dom-helpers/util/scrollbarSize.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (recalc) {
  if (!size && size !== 0 || recalc) {
    if (_inDOM2.default) {
      var scrollDiv = document.createElement('div');

      scrollDiv.style.position = 'absolute';
      scrollDiv.style.top = '-9999px';
      scrollDiv.style.width = '50px';
      scrollDiv.style.height = '50px';
      scrollDiv.style.overflow = 'scroll';

      document.body.appendChild(scrollDiv);
      size = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
    }
  }

  return size;
};

var _inDOM = __webpack_require__(/*! ./inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = void 0;

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/material-ui-icons/Comment.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui-icons/Comment.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var Comment = function Comment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Comment = (0, _pure2.default)(Comment);
Comment.muiName = 'SvgIcon';

exports.default = Comment;

/***/ }),

/***/ "./node_modules/material-ui-icons/Delete.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui-icons/Delete.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z' });

var Delete = function Delete(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Delete = (0, _pure2.default)(Delete);
Delete.muiName = 'SvgIcon';

exports.default = Delete;

/***/ }),

/***/ "./node_modules/material-ui-icons/Favorite.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/Favorite.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z' });

var Favorite = function Favorite(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Favorite = (0, _pure2.default)(Favorite);
Favorite.muiName = 'SvgIcon';

exports.default = Favorite;

/***/ }),

/***/ "./node_modules/material-ui-icons/MoreVert.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/MoreVert.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });

var MoreVert = function MoreVert(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

MoreVert = (0, _pure2.default)(MoreVert);
MoreVert.muiName = 'SvgIcon';

exports.default = MoreVert;

/***/ }),

/***/ "./node_modules/material-ui-icons/RemoveRedEye.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/RemoveRedEye.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z' });

var RemoveRedEye = function RemoveRedEye(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

RemoveRedEye = (0, _pure2.default)(RemoveRedEye);
RemoveRedEye.muiName = 'SvgIcon';

exports.default = RemoveRedEye;

/***/ }),

/***/ "./node_modules/material-ui-icons/Share.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Share.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z' });

var Share = function Share(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Share = (0, _pure2.default)(Share);
Share.muiName = 'SvgIcon';

exports.default = Share;

/***/ }),

/***/ "./node_modules/material-ui-icons/TurnedIn.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/TurnedIn.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z' });

var TurnedIn = function TurnedIn(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

TurnedIn = (0, _pure2.default)(TurnedIn);
TurnedIn.muiName = 'SvgIcon';

exports.default = TurnedIn;

/***/ }),

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Badge/Badge.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/Badge.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var RADIUS = 12;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'inline-flex'
    },
    badge: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      top: -RADIUS,
      right: -RADIUS,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeight,
      fontSize: theme.typography.pxToRem(RADIUS),
      width: RADIUS * 2,
      height: RADIUS * 2,
      borderRadius: '50%',
      backgroundColor: theme.palette.color,
      color: theme.palette.textColor,
      zIndex: 1 // Render the badge on top of potential ripples.
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content rendered within the badge.
   */
  badgeContent: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * The badge will be added relative to this node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'primary', 'accent']).isRequired
};

var Badge = function (_React$Component) {
  (0, _inherits3.default)(Badge, _React$Component);

  function Badge() {
    (0, _classCallCheck3.default)(this, Badge);
    return (0, _possibleConstructorReturn3.default)(this, (Badge.__proto__ || (0, _getPrototypeOf2.default)(Badge)).apply(this, arguments));
  }

  (0, _createClass3.default)(Badge, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          badgeContent = _props.badgeContent,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          children = _props.children,
          other = (0, _objectWithoutProperties3.default)(_props, ['badgeContent', 'classes', 'className', 'color', 'children']);

      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var badgeClassName = (0, _classnames2.default)(classes.badge, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'));

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        children,
        _react2.default.createElement(
          'span',
          { className: badgeClassName },
          badgeContent
        )
      );
    }
  }]);
  return Badge;
}(_react2.default.Component);

Badge.defaultProps = {
  color: 'default'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBadge' })(Badge);

/***/ }),

/***/ "./node_modules/material-ui/Badge/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Badge = __webpack_require__(/*! ./Badge */ "./node_modules/material-ui/Badge/Badge.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Badge).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Dialog/Dialog.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Dialog/Dialog.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Modal = __webpack_require__(/*! ../Modal */ "./node_modules/material-ui/Modal/index.js");

var _Modal2 = _interopRequireDefault(_Modal);

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Modal

var babelPluginFlowReactPropTypes_proptype_ComponentType = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    paper: {
      display: 'flex',
      margin: theme.spacing.unit * 4,
      flexDirection: 'column',
      flex: '0 1 auto',
      position: 'relative',
      maxHeight: '90vh',
      overflowY: 'auto', // Fix IE11 issue, to remove at some point.
      '&:focus': {
        outline: 'none'
      }
    },
    paperWidthXs: {
      maxWidth: Math.max(theme.breakpoints.values.xs, 360)
    },
    paperWidthSm: {
      maxWidth: theme.breakpoints.values.sm
    },
    paperWidthMd: {
      maxWidth: theme.breakpoints.values.md
    },
    fullWidth: {
      width: '100%'
    },
    fullScreen: {
      margin: 0,
      width: '100%',
      maxWidth: '100%',
      height: '100%',
      maxHeight: '100%',
      borderRadius: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Dialog children, usually the included sub-components.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, it will be full-screen
   */
  fullScreen: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  transitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * Determine the max width of the dialog.
   * The dialog width grows with the size of the screen, this property is useful
   * on the desktop where you might need some coherent different width size across your
   * application.
   */
  maxWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['xs', 'sm', 'md']).isRequired,

  /**
   * If specified, stretches dialog to max width.
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Callback fired when the backdrop is clicked.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the dialog enters.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is released and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the dialog exits.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Dialog is open.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Transition component.
   */
  transition: typeof babelPluginFlowReactPropTypes_proptype_ComponentType === 'function' ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired : babelPluginFlowReactPropTypes_proptype_ComponentType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ComponentType).isRequired
};

/**
 * Dialogs are overlaid modal paper based components with a backdrop.
 */
var Dialog = function (_React$Component) {
  (0, _inherits3.default)(Dialog, _React$Component);

  function Dialog() {
    (0, _classCallCheck3.default)(this, Dialog);
    return (0, _possibleConstructorReturn3.default)(this, (Dialog.__proto__ || (0, _getPrototypeOf2.default)(Dialog)).apply(this, arguments));
  }

  (0, _createClass3.default)(Dialog, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          fullScreen = _props.fullScreen,
          ignoreBackdropClick = _props.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props.ignoreEscapeKeyUp,
          transitionDuration = _props.transitionDuration,
          maxWidth = _props.maxWidth,
          fullWidth = _props.fullWidth,
          open = _props.open,
          onBackdropClick = _props.onBackdropClick,
          onEscapeKeyUp = _props.onEscapeKeyUp,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onEntered = _props.onEntered,
          onExit = _props.onExit,
          onExiting = _props.onExiting,
          onExited = _props.onExited,
          onRequestClose = _props.onRequestClose,
          TransitionProp = _props.transition,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'fullScreen', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'transitionDuration', 'maxWidth', 'fullWidth', 'open', 'onBackdropClick', 'onEscapeKeyUp', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'onRequestClose', 'transition']);


      return _react2.default.createElement(
        _Modal2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, className),
          BackdropTransitionDuration: transitionDuration,
          ignoreBackdropClick: ignoreBackdropClick,
          ignoreEscapeKeyUp: ignoreEscapeKeyUp,
          onBackdropClick: onBackdropClick,
          onEscapeKeyUp: onEscapeKeyUp,
          onRequestClose: onRequestClose,
          show: open
        }, other),
        _react2.default.createElement(
          TransitionProp,
          {
            appear: true,
            'in': open,
            timeout: transitionDuration,
            onEnter: onEnter,
            onEntering: onEntering,
            onEntered: onEntered,
            onExit: onExit,
            onExiting: onExiting,
            onExited: onExited
          },
          _react2.default.createElement(
            _Paper2.default,
            {
              elevation: 24,
              className: (0, _classnames2.default)(classes.paper, classes['paperWidth' + (0, _helpers.capitalizeFirstLetter)(maxWidth)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.fullScreen, fullScreen), (0, _defineProperty3.default)(_classNames, classes.fullWidth, fullWidth), _classNames))
            },
            children
          )
        )
      );
    }
  }]);
  return Dialog;
}(_react2.default.Component);

Dialog.defaultProps = {
  fullScreen: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  transitionDuration: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  },
  maxWidth: 'sm',
  fullWidth: false,
  open: false,
  transition: _Fade2.default
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialog' })(Dialog);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogActions.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogActions.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

__webpack_require__(/*! ../Button */ "./node_modules/material-ui/Button/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// So we don't have any override priority issue.

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      margin: theme.spacing.unit + 'px ' + theme.spacing.unit / 2 + 'px',
      flex: '0 0 auto'
    },
    action: {
      margin: '0 ' + theme.spacing.unit / 2 + 'px'
    },
    button: {
      minWidth: 64
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogActions(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.createElement(
    'div',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    _react2.default.Children.map(children, function (child) {
      if (!_react2.default.isValidElement(child)) {
        return null;
      }

      return _react2.default.createElement(
        'div',
        { className: classes.action },
        _react2.default.cloneElement(child, {
          className: (0, _classnames2.default)(classes.button, child.props.className)
        })
      );
    })
  );
}

DialogActions.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * @ignore
   */
  theme: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogActions' })(DialogActions);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogContent.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogContent.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  var spacing = theme.spacing.unit * 3;
  return {
    root: {
      flex: '1 1 auto',
      overflowY: 'auto',
      padding: '0 ' + spacing + 'px ' + spacing + 'px ' + spacing + 'px',
      '&:first-child': {
        paddingTop: spacing
      }
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogContent(props) {
  var classes = props.classes,
      children = props.children,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['classes', 'children', 'className']);


  return _react2.default.createElement(
    'div',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    children
  );
}

DialogContent.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogContent' })(DialogContent);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogContentText.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogContentText.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends3.default)({}, theme.typography.subheading, {
      color: theme.palette.text.secondary,
      margin: 0
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogContentText(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.createElement(
    'p',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    children
  );
}

DialogContentText.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * @ignore
   */
  theme: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogContentText' })(DialogContentText);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogTitle.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogTitle.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! ../Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      margin: 0,
      padding: theme.spacing.unit * 3 + 'px ' + theme.spacing.unit * 3 + 'px       20px ' + theme.spacing.unit * 3 + 'px',
      flex: '0 0 auto'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the children won't be wrapped by a typography component.
   * For instance, this can be useful to render an h4 instead of the default h2.
   */
  disableTypography: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var DialogTitle = function (_React$Component) {
  (0, _inherits3.default)(DialogTitle, _React$Component);

  function DialogTitle() {
    (0, _classCallCheck3.default)(this, DialogTitle);
    return (0, _possibleConstructorReturn3.default)(this, (DialogTitle.__proto__ || (0, _getPrototypeOf2.default)(DialogTitle)).apply(this, arguments));
  }

  (0, _createClass3.default)(DialogTitle, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          disableTypography = _props.disableTypography,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'disableTypography']);


      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
        disableTypography ? children : _react2.default.createElement(
          _Typography2.default,
          { type: 'title' },
          children
        )
      );
    }
  }]);
  return DialogTitle;
}(_react2.default.Component);

DialogTitle.defaultProps = {
  disableTypography: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogTitle' })(DialogTitle);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Dialog/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Dialog = __webpack_require__(/*! ./Dialog */ "./node_modules/material-ui/Dialog/Dialog.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Dialog).default;
  }
});

var _DialogActions = __webpack_require__(/*! ./DialogActions */ "./node_modules/material-ui/Dialog/DialogActions.js");

Object.defineProperty(exports, 'DialogActions', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogActions).default;
  }
});

var _DialogTitle = __webpack_require__(/*! ./DialogTitle */ "./node_modules/material-ui/Dialog/DialogTitle.js");

Object.defineProperty(exports, 'DialogTitle', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogTitle).default;
  }
});

var _DialogContent = __webpack_require__(/*! ./DialogContent */ "./node_modules/material-ui/Dialog/DialogContent.js");

Object.defineProperty(exports, 'DialogContent', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogContent).default;
  }
});

var _DialogContentText = __webpack_require__(/*! ./DialogContentText */ "./node_modules/material-ui/Dialog/DialogContentText.js");

Object.defineProperty(exports, 'DialogContentText', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogContentText).default;
  }
});

var _withMobileDialog = __webpack_require__(/*! ./withMobileDialog */ "./node_modules/material-ui/Dialog/withMobileDialog.js");

Object.defineProperty(exports, 'withMobileDialog', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_withMobileDialog).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Dialog/withMobileDialog.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui/Dialog/withMobileDialog.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _wrapDisplayName = __webpack_require__(/*! recompose/wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _withWidth = __webpack_require__(/*! ../utils/withWidth */ "./node_modules/material-ui/utils/withWidth.js");

var _withWidth2 = _interopRequireDefault(_withWidth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_HigherOrderComponent = __webpack_require__(/*! react-flow-types */ "./node_modules/react-flow-types/index.js").babelPluginFlowReactPropTypes_proptype_HigherOrderComponent || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Breakpoint = __webpack_require__(/*! ../styles/createBreakpoints */ "./node_modules/material-ui/styles/createBreakpoints.js").babelPluginFlowReactPropTypes_proptype_Breakpoint || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_InjectedProps = {
  /**
   * If isWidthDown(options.breakpoint), return true.
   */
  fullScreen: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};


/**
 * Dialog will responsively be full screen *at or below* the given breakpoint
 * (defaults to 'sm' for mobile devices).
 * Notice that this Higher-order Component is incompatible with server side rendering.
 */
var withMobileDialog = function withMobileDialog() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { breakpoint: 'sm' };
  return function (Component) {
    var breakpoint = options.breakpoint;


    function WithMobileDialog(props) {
      return _react2.default.createElement(Component, (0, _extends3.default)({ fullScreen: (0, _withWidth.isWidthDown)(breakpoint, props.width) }, props));
    }

    WithMobileDialog.propTypes =  true ? {
      width: typeof babelPluginFlowReactPropTypes_proptype_Breakpoint === 'function' ? babelPluginFlowReactPropTypes_proptype_Breakpoint.isRequired ? babelPluginFlowReactPropTypes_proptype_Breakpoint.isRequired : babelPluginFlowReactPropTypes_proptype_Breakpoint : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Breakpoint).isRequired
    } : {};
    if (true) {
      WithMobileDialog.displayName = (0, _wrapDisplayName2.default)(Component, 'withMobileDialog');
    }

    return (0, _withWidth2.default)()(WithMobileDialog);
  };
};

exports.default = withMobileDialog;

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/Backdrop.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui/Modal/Backdrop.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      zIndex: -1,
      width: '100%',
      height: '100%',
      position: 'fixed',
      top: 0,
      left: 0,
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      backgroundColor: theme.palette.common.lightBlack,
      transition: theme.transitions.create('opacity'),
      willChange: 'opacity',
      opacity: 0
    },
    invisible: {
      backgroundColor: theme.palette.common.transparent
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Can be used, for instance, to render a letter inside the avatar.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the backdrop is invisible.
   */
  invisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * @ignore - internal component.
 */
var Backdrop = function (_React$Component) {
  (0, _inherits3.default)(Backdrop, _React$Component);

  function Backdrop() {
    (0, _classCallCheck3.default)(this, Backdrop);
    return (0, _possibleConstructorReturn3.default)(this, (Backdrop.__proto__ || (0, _getPrototypeOf2.default)(Backdrop)).apply(this, arguments));
  }

  (0, _createClass3.default)(Backdrop, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          invisible = _props.invisible,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'invisible']);


      var backdropClass = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.invisible, invisible), className);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: backdropClass, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Backdrop;
}(_react2.default.Component);

Backdrop.defaultProps = {
  invisible: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBackdrop' })(Backdrop);

/***/ }),

/***/ "./node_modules/material-ui/Modal/Modal.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/Modal.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _addEventListener = __webpack_require__(/*! ../utils/addEventListener */ "./node_modules/material-ui/utils/addEventListener.js");

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _modalManager = __webpack_require__(/*! ./modalManager */ "./node_modules/material-ui/Modal/modalManager.js");

var _modalManager2 = _interopRequireDefault(_modalManager);

var _Backdrop = __webpack_require__(/*! ./Backdrop */ "./node_modules/material-ui/Modal/Backdrop.js");

var _Backdrop2 = _interopRequireDefault(_Backdrop);

var _Portal = __webpack_require__(/*! ../internal/Portal */ "./node_modules/material-ui/internal/Portal.js");

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// Modals don't open on the server so this won't break concurrency.
// Could also put this on context.
var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var modalManager = (0, _modalManager2.default)();

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      position: 'fixed',
      zIndex: theme.zIndex.dialog,
      top: 0,
      left: 0
    },
    hidden: {
      visibility: 'hidden'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the backdrop element.
   */
  BackdropClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Pass a component class to use as the backdrop.
   */
  BackdropComponent: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, the backdrop is invisible.
   */
  BackdropInvisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the backdrop transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  BackdropTransitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Always keep the children in the DOM.
   * This property can be useful in SEO situation or
   * when you want to maximize the responsiveness of the Modal.
   */
  keepMounted: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the backdrop is disabled.
   */
  disableBackdrop: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  modalManager: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * Callback fires when the backdrop is clicked on.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is pressed and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Modal is visible.
   */
  show: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * The modal component provides a solid foundation for creating dialogs,
 * popovers, or whatever else.
 * The component renders its `children` node in front of a backdrop component.
 *
 * The `Modal` offers a few helpful features over using just a `Portal` component and some styles:
 * - Manages dialog stacking when one-at-a-time just isn't enough.
 * - Creates a backdrop, for disabling interaction below the modal.
 * - It properly manages focus; moving to the modal content,
 *   and keeping it there until the modal is closed.
 * - It disables scrolling of the page content while open.
 * - Adds the appropriate ARIA roles are automatically.
 *
 * This component shares many concepts with [react-overlays](https://react-bootstrap.github.io/react-overlays/#modals).
 */
var Modal = function (_React$Component) {
  (0, _inherits3.default)(Modal, _React$Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call.apply(_ref, [this].concat(args))), _this), _initialiseProps.call(_this), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Modal, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (!this.props.show) {
        this.setState({ exited: true });
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.mounted = true;
      if (this.props.show) {
        this.handleShow();
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.show && this.state.exited) {
        this.setState({ exited: false });
      }
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps) {
      if (!this.props.show && nextProps.show) {
        this.checkForFocus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.show && this.props.show) {
        this.handleShow();
      }
      // We are waiting for the onExited callback to call handleHide.
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.props.show || !this.state.exited) {
        this.handleHide();
      }
      this.mounted = false;
    }
  }, {
    key: 'checkForFocus',
    value: function checkForFocus() {
      if (_inDOM2.default) {
        this.lastFocus = (0, _activeElement2.default)();
      }
    }
  }, {
    key: 'restoreLastFocus',
    value: function restoreLastFocus() {
      if (this.lastFocus && this.lastFocus.focus) {
        this.lastFocus.focus();
        this.lastFocus = undefined;
      }
    }
  }, {
    key: 'handleShow',
    value: function handleShow() {
      var doc = (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this));
      this.props.modalManager.add(this);
      this.onDocumentKeyUpListener = (0, _addEventListener2.default)(doc, 'keyup', this.handleDocumentKeyUp);
      this.onFocusListener = (0, _addEventListener2.default)(doc, 'focus', this.handleFocusListener, true);
      this.focus();
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this)));
      var modalContent = this.modal && this.modal.lastChild;
      var focusInModal = currentFocus && (0, _contains2.default)(modalContent, currentFocus);

      if (modalContent && !focusInModal) {
        if (!modalContent.hasAttribute('tabIndex')) {
          modalContent.setAttribute('tabIndex', -1);
           true ? (0, _warning2.default)(false, 'Material-UI: the modal content node does not accept focus. ' + 'For the benefit of assistive technologies, ' + 'the tabIndex of the node is being set to "-1".') : void 0;
        }

        modalContent.focus();
      }
    }
  }, {
    key: 'handleHide',
    value: function handleHide() {
      this.props.modalManager.remove(this);
      if (this.onDocumentKeyUpListener) this.onDocumentKeyUpListener.remove();
      if (this.onFocusListener) this.onFocusListener.remove();
      this.restoreLastFocus();
    }
  }, {
    key: 'renderBackdrop',
    value: function renderBackdrop() {
      var other = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var _props = this.props,
          BackdropComponent = _props.BackdropComponent,
          BackdropClassName = _props.BackdropClassName,
          BackdropTransitionDuration = _props.BackdropTransitionDuration,
          BackdropInvisible = _props.BackdropInvisible,
          show = _props.show;


      return _react2.default.createElement(
        _Fade2.default,
        (0, _extends3.default)({ appear: true, 'in': show, timeout: BackdropTransitionDuration }, other),
        _react2.default.createElement(BackdropComponent, {
          invisible: BackdropInvisible,
          className: BackdropClassName,
          onClick: this.handleBackdropClick
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          disableBackdrop = _props2.disableBackdrop,
          BackdropComponent = _props2.BackdropComponent,
          BackdropClassName = _props2.BackdropClassName,
          BackdropTransitionDuration = _props2.BackdropTransitionDuration,
          BackdropInvisible = _props2.BackdropInvisible,
          ignoreBackdropClick = _props2.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props2.ignoreEscapeKeyUp,
          children = _props2.children,
          classes = _props2.classes,
          className = _props2.className,
          keepMounted = _props2.keepMounted,
          modalManagerProp = _props2.modalManager,
          onBackdropClick = _props2.onBackdropClick,
          onEscapeKeyUp = _props2.onEscapeKeyUp,
          onRequestClose = _props2.onRequestClose,
          onEnter = _props2.onEnter,
          onEntering = _props2.onEntering,
          onEntered = _props2.onEntered,
          onExit = _props2.onExit,
          onExiting = _props2.onExiting,
          onExited = _props2.onExited,
          show = _props2.show,
          other = (0, _objectWithoutProperties3.default)(_props2, ['disableBackdrop', 'BackdropComponent', 'BackdropClassName', 'BackdropTransitionDuration', 'BackdropInvisible', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'children', 'classes', 'className', 'keepMounted', 'modalManager', 'onBackdropClick', 'onEscapeKeyUp', 'onRequestClose', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'show']);


      if (!keepMounted && !show && this.state.exited) {
        return null;
      }

      var transitionCallbacks = {
        onEnter: onEnter,
        onEntering: onEntering,
        onEntered: onEntered,
        onExit: onExit,
        onExiting: onExiting,
        onExited: this.handleTransitionExited
      };

      var modalChild = _react2.default.Children.only(children);
      var _modalChild$props = modalChild.props,
          role = _modalChild$props.role,
          tabIndex = _modalChild$props.tabIndex;

      var childProps = {};

      if (role === undefined) {
        childProps.role = role === undefined ? 'document' : role;
      }

      if (tabIndex === undefined) {
        childProps.tabIndex = tabIndex == null ? -1 : tabIndex;
      }

      var backdropProps = void 0;

      // It's a Transition like component
      if (modalChild.props.hasOwnProperty('in')) {
        (0, _keys2.default)(transitionCallbacks).forEach(function (key) {
          childProps[key] = (0, _helpers.createChainedFunction)(transitionCallbacks[key], modalChild.props[key]);
        });
      } else {
        backdropProps = transitionCallbacks;
      }

      if ((0, _keys2.default)(childProps).length) {
        modalChild = _react2.default.cloneElement(modalChild, childProps);
      }

      return _react2.default.createElement(
        _Portal2.default,
        {
          open: true,
          ref: function ref(node) {
            _this2.mountNode = node ? node.getLayer() : null;
          }
        },
        _react2.default.createElement(
          'div',
          (0, _extends3.default)({
            className: (0, _classnames2.default)(classes.root, className, (0, _defineProperty3.default)({}, classes.hidden, this.state.exited))
          }, other, {
            ref: function ref(node) {
              _this2.modal = node;
            }
          }),
          !disableBackdrop && (!keepMounted || show || !this.state.exited) && this.renderBackdrop(backdropProps),
          modalChild
        )
      );
    }
  }]);
  return Modal;
}(_react2.default.Component);

Modal.defaultProps = {
  BackdropComponent: _Backdrop2.default,
  BackdropTransitionDuration: 300,
  BackdropInvisible: false,
  keepMounted: false,
  disableBackdrop: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  modalManager: modalManager
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.state = {
    exited: false
  };
  this.onDocumentKeyUpListener = null;
  this.onFocusListener = null;
  this.mounted = false;
  this.lastFocus = undefined;
  this.modal = null;
  this.mountNode = null;

  this.handleFocusListener = function () {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(_this3)));
    var modalContent = _this3.modal && _this3.modal.lastChild;

    if (modalContent && modalContent !== currentFocus && !(0, _contains2.default)(modalContent, currentFocus)) {
      modalContent.focus();
    }
  };

  this.handleDocumentKeyUp = function (event) {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    if ((0, _keycode2.default)(event) !== 'esc') {
      return;
    }

    var _props3 = _this3.props,
        onEscapeKeyUp = _props3.onEscapeKeyUp,
        onRequestClose = _props3.onRequestClose,
        ignoreEscapeKeyUp = _props3.ignoreEscapeKeyUp;


    if (onEscapeKeyUp) {
      onEscapeKeyUp(event);
    }

    if (onRequestClose && !ignoreEscapeKeyUp) {
      onRequestClose(event);
    }
  };

  this.handleBackdropClick = function (event) {
    if (event.target !== event.currentTarget) {
      return;
    }

    var _props4 = _this3.props,
        onBackdropClick = _props4.onBackdropClick,
        onRequestClose = _props4.onRequestClose,
        ignoreBackdropClick = _props4.ignoreBackdropClick;


    if (onBackdropClick) {
      onBackdropClick(event);
    }

    if (onRequestClose && !ignoreBackdropClick) {
      onRequestClose(event);
    }
  };

  this.handleTransitionExited = function () {
    if (_this3.props.onExited) {
      var _props5;

      (_props5 = _this3.props).onExited.apply(_props5, arguments);
    }

    _this3.setState({ exited: true });
    _this3.handleHide();
  };
};

exports.default = (0, _withStyles2.default)(styles, { flip: false, name: 'MuiModal' })(Modal);

/***/ }),

/***/ "./node_modules/material-ui/Modal/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Modal = __webpack_require__(/*! ./Modal */ "./node_modules/material-ui/Modal/Modal.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Modal).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/modalManager.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Modal/modalManager.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _isWindow = __webpack_require__(/*! dom-helpers/query/isWindow */ "./node_modules/dom-helpers/query/isWindow.js");

var _isWindow2 = _interopRequireDefault(_isWindow);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _manageAriaHidden = __webpack_require__(/*! ../utils/manageAriaHidden */ "./node_modules/material-ui/utils/manageAriaHidden.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Taken from https://github.com/react-bootstrap/react-overlays/blob/master/src/ModalManager.js

function getPaddingRight(node) {
  return parseInt(node.style.paddingRight || 0, 10);
}

// Do we have a scroll bar?
function bodyIsOverflowing(node) {
  var doc = (0, _ownerDocument2.default)(node);
  var win = (0, _isWindow2.default)(doc);

  // Takes in account potential non zero margin on the body.
  var style = window.getComputedStyle(doc.body);
  var marginLeft = parseInt(style.getPropertyValue('margin-left'), 10);
  var marginRight = parseInt(style.getPropertyValue('margin-right'), 10);

  return marginLeft + doc.body.clientWidth + marginRight < win.innerWidth;
}

function getContainer() {
  var container = _inDOM2.default ? window.document.body : {};
   true ? (0, _warning2.default)(container !== null, '\nMaterial-UI: you are most likely evaluating the code before the\nbrowser has a chance to reach the <body>.\nPlease move the import at the end of the <body>.\n  ') : void 0;
  return container;
}
/**
 * State management helper for modals/layers.
 * Simplified, but inspired by react-overlay's ModalManager class
 *
 * @internal Used by the Modal to ensure proper focus management.
 */
function createModalManager() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$hideSiblingNodes = _ref.hideSiblingNodes,
      hideSiblingNodes = _ref$hideSiblingNodes === undefined ? true : _ref$hideSiblingNodes;

  var modals = [];

  var prevOverflow = void 0;
  var prevPaddings = [];

  function add(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx !== -1) {
      return modalIdx;
    }

    modalIdx = modals.length;
    modals.push(modal);

    if (hideSiblingNodes) {
      (0, _manageAriaHidden.hideSiblings)(container, modal.mountNode);
    }

    if (modals.length === 1) {
      // Save our current overflow so we can revert
      // back to it when all modals are closed!
      prevOverflow = container.style.overflow;

      if (bodyIsOverflowing(container)) {
        prevPaddings = [getPaddingRight(container)];
        var scrollbarSize = (0, _scrollbarSize2.default)();
        container.style.paddingRight = prevPaddings[0] + scrollbarSize + 'px';

        var fixedNodes = document.querySelectorAll('.mui-fixed');
        for (var i = 0; i < fixedNodes.length; i += 1) {
          var paddingRight = getPaddingRight(fixedNodes[i]);
          prevPaddings.push(paddingRight);
          fixedNodes[i].style.paddingRight = paddingRight + scrollbarSize + 'px';
        }
      }

      container.style.overflow = 'hidden';
    }

    return modalIdx;
  }

  function remove(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx === -1) {
      return modalIdx;
    }

    modals.splice(modalIdx, 1);

    if (modals.length === 0) {
      container.style.overflow = prevOverflow;
      container.style.paddingRight = prevPaddings[0];

      var fixedNodes = document.querySelectorAll('.mui-fixed');
      for (var i = 0; i < fixedNodes.length; i += 1) {
        fixedNodes[i].style.paddingRight = prevPaddings[i + 1] + 'px';
      }

      prevOverflow = undefined;
      prevPaddings = [];
      if (hideSiblingNodes) {
        (0, _manageAriaHidden.showSiblings)(container, modal.mountNode);
      }
    } else if (hideSiblingNodes) {
      // otherwise make sure the next top modal is visible to a SR
      (0, _manageAriaHidden.ariaHidden)(false, modals[modals.length - 1].mountNode);
    }

    return modalIdx;
  }

  function isTopModal(modal) {
    return !!modals.length && modals[modals.length - 1] === modal;
  }

  var modalManager = { add: add, remove: remove, isTopModal: isTopModal };

  return modalManager;
}

exports.default = createModalManager;

/***/ }),

/***/ "./node_modules/material-ui/internal/Portal.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/internal/Portal.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content to portal in order to escape the parent DOM node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * If `true` the children will be mounted into the DOM.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

/**
 * @ignore - internal component.
 */
var Portal = function (_React$Component) {
  (0, _inherits3.default)(Portal, _React$Component);

  function Portal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Portal.__proto__ || (0, _getPrototypeOf2.default)(Portal)).call.apply(_ref, [this].concat(args))), _this), _this.layer = null, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Portal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unrenderLayer();
    }
  }, {
    key: 'getLayer',
    value: function getLayer() {
      if (!this.layer) {
        this.layer = document.createElement('div');
        this.layer.setAttribute('data-mui-portal', 'true');
        if (document.body && this.layer) {
          document.body.appendChild(this.layer);
        }
      }

      return this.layer;
    }
  }, {
    key: 'unrenderLayer',
    value: function unrenderLayer() {
      if (!this.layer) {
        return;
      }

      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        _reactDom2.default.unmountComponentAtNode(this.layer);
      }

      if (document.body) {
        document.body.removeChild(this.layer);
      }
      this.layer = null;
    }
  }, {
    key: 'renderLayer',
    value: function renderLayer() {
      var _props = this.props,
          children = _props.children,
          open = _props.open;


      if (open) {
        // By calling this method in componentDidMount() and
        // componentDidUpdate(), you're effectively creating a "wormhole" that
        // funnels React's hierarchical updates through to a DOM node on an
        // entirely different part of the page.
        var layerElement = _react2.default.Children.only(children);
        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, layerElement, this.getLayer());
      } else {
        this.unrenderLayer();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          children = _props2.children,
          open = _props2.open;

      // Support react@15.x, will be removed at some point

      if (!_reactDom2.default.createPortal) {
        return null;
      }

      // Can't be rendered server-side.
      if (_inDOM2.default) {
        if (open) {
          var layer = this.getLayer();
          // $FlowFixMe layer is non-null
          return _reactDom2.default.createPortal(children, layer);
        }

        this.unrenderLayer();
      }

      return null;
    }
  }]);
  return Portal;
}(_react2.default.Component);

Portal.defaultProps = {
  open: false
};
Portal.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
} : {};
exports.default = Portal;

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Fade.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Fade.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  timeout: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired
};


var reflow = function reflow(node) {
  return node.scrollTop;
};

/**
 * The Fade transition is used by the Modal component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */

var Fade = function (_React$Component) {
  (0, _inherits3.default)(Fade, _React$Component);

  function Fade() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Fade);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Fade.__proto__ || (0, _getPrototypeOf2.default)(Fade)).call.apply(_ref, [this].concat(args))), _this), _this.handleEnter = function (node) {
      node.style.opacity = '0';
      reflow(node);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      node.style.opacity = '1';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      node.style.opacity = '0';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Fade, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          styleProp = _props.style,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'style', 'theme']);


      var style = (0, _extends3.default)({}, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          style: style,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit
        }, other),
        children
      );
    }
  }]);
  return Fade;
}(_react2.default.Component);

Fade.defaultProps = {
  appear: true,
  timeout: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  }
};
exports.default = (0, _withTheme2.default)()(Fade);

/***/ }),

/***/ "./node_modules/material-ui/utils/manageAriaHidden.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui/utils/manageAriaHidden.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ariaHidden = ariaHidden;
exports.hideSiblings = hideSiblings;
exports.showSiblings = showSiblings;
//  weak

var BLACKLIST = ['template', 'script', 'style'];

var isHidable = function isHidable(_ref) {
  var nodeType = _ref.nodeType,
      tagName = _ref.tagName;
  return nodeType === 1 && BLACKLIST.indexOf(tagName.toLowerCase()) === -1;
};

var siblings = function siblings(container, mount, cb) {
  mount = [].concat(mount); // eslint-disable-line no-param-reassign
  [].forEach.call(container.children, function (node) {
    if (mount.indexOf(node) === -1 && isHidable(node)) {
      cb(node);
    }
  });
};

function ariaHidden(show, node) {
  if (!node) {
    return;
  }
  if (show) {
    node.setAttribute('aria-hidden', 'true');
  } else {
    node.removeAttribute('aria-hidden');
  }
}

function hideSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(true, node);
  });
}

function showSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(false, node);
  });
}

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/react-router/Route.js":
/*!********************************************!*\
  !*** ./node_modules/react-router/Route.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _matchPath = __webpack_require__(/*! ./matchPath */ "./node_modules/react-router/matchPath.js");

var _matchPath2 = _interopRequireDefault(_matchPath);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isEmptyChildren = function isEmptyChildren(children) {
  return _react2.default.Children.count(children) === 0;
};

/**
 * The public API for matching a single path and rendering.
 */

var Route = function (_React$Component) {
  _inherits(Route, _React$Component);

  function Route() {
    var _temp, _this, _ret;

    _classCallCheck(this, Route);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
      match: _this.computeMatch(_this.props, _this.context.router)
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Route.prototype.getChildContext = function getChildContext() {
    return {
      router: _extends({}, this.context.router, {
        route: {
          location: this.props.location || this.context.router.route.location,
          match: this.state.match
        }
      })
    };
  };

  Route.prototype.computeMatch = function computeMatch(_ref, router) {
    var computedMatch = _ref.computedMatch,
        location = _ref.location,
        path = _ref.path,
        strict = _ref.strict,
        exact = _ref.exact,
        sensitive = _ref.sensitive;

    if (computedMatch) return computedMatch; // <Switch> already computed the match for us

    (0, _invariant2.default)(router, 'You should not use <Route> or withRouter() outside a <Router>');

    var route = router.route;

    var pathname = (location || route.location).pathname;

    return path ? (0, _matchPath2.default)(pathname, { path: path, strict: strict, exact: exact, sensitive: sensitive }) : route.match;
  };

  Route.prototype.componentWillMount = function componentWillMount() {
    (0, _warning2.default)(!(this.props.component && this.props.render), 'You should not use <Route component> and <Route render> in the same route; <Route render> will be ignored');

    (0, _warning2.default)(!(this.props.component && this.props.children && !isEmptyChildren(this.props.children)), 'You should not use <Route component> and <Route children> in the same route; <Route children> will be ignored');

    (0, _warning2.default)(!(this.props.render && this.props.children && !isEmptyChildren(this.props.children)), 'You should not use <Route render> and <Route children> in the same route; <Route children> will be ignored');
  };

  Route.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps, nextContext) {
    (0, _warning2.default)(!(nextProps.location && !this.props.location), '<Route> elements should not change from uncontrolled to controlled (or vice versa). You initially used no "location" prop and then provided one on a subsequent render.');

    (0, _warning2.default)(!(!nextProps.location && this.props.location), '<Route> elements should not change from controlled to uncontrolled (or vice versa). You provided a "location" prop initially but omitted it on a subsequent render.');

    this.setState({
      match: this.computeMatch(nextProps, nextContext.router)
    });
  };

  Route.prototype.render = function render() {
    var match = this.state.match;
    var _props = this.props,
        children = _props.children,
        component = _props.component,
        render = _props.render;
    var _context$router = this.context.router,
        history = _context$router.history,
        route = _context$router.route,
        staticContext = _context$router.staticContext;

    var location = this.props.location || route.location;
    var props = { match: match, location: location, history: history, staticContext: staticContext };

    return component ? // component prop gets first priority, only called if there's a match
    match ? _react2.default.createElement(component, props) : null : render ? // render prop is next, only called if there's a match
    match ? render(props) : null : children ? // children come last, always called
    typeof children === 'function' ? children(props) : !isEmptyChildren(children) ? _react2.default.Children.only(children) : null : null;
  };

  return Route;
}(_react2.default.Component);

Route.propTypes = {
  computedMatch: _propTypes2.default.object, // private, from <Switch>
  path: _propTypes2.default.string,
  exact: _propTypes2.default.bool,
  strict: _propTypes2.default.bool,
  sensitive: _propTypes2.default.bool,
  component: _propTypes2.default.func,
  render: _propTypes2.default.func,
  children: _propTypes2.default.oneOfType([_propTypes2.default.func, _propTypes2.default.node]),
  location: _propTypes2.default.object
};
Route.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.object.isRequired,
    route: _propTypes2.default.object.isRequired,
    staticContext: _propTypes2.default.object
  })
};
Route.childContextTypes = {
  router: _propTypes2.default.object.isRequired
};
exports.default = Route;

/***/ }),

/***/ "./node_modules/react-router/matchPath.js":
/*!************************************************!*\
  !*** ./node_modules/react-router/matchPath.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _pathToRegexp = __webpack_require__(/*! path-to-regexp */ "./node_modules/path-to-regexp/index.js");

var _pathToRegexp2 = _interopRequireDefault(_pathToRegexp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var patternCache = {};
var cacheLimit = 10000;
var cacheCount = 0;

var compilePath = function compilePath(pattern, options) {
  var cacheKey = '' + options.end + options.strict + options.sensitive;
  var cache = patternCache[cacheKey] || (patternCache[cacheKey] = {});

  if (cache[pattern]) return cache[pattern];

  var keys = [];
  var re = (0, _pathToRegexp2.default)(pattern, keys, options);
  var compiledPattern = { re: re, keys: keys };

  if (cacheCount < cacheLimit) {
    cache[pattern] = compiledPattern;
    cacheCount++;
  }

  return compiledPattern;
};

/**
 * Public API for matching a URL pathname to a path pattern.
 */
var matchPath = function matchPath(pathname) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (typeof options === 'string') options = { path: options };

  var _options = options,
      _options$path = _options.path,
      path = _options$path === undefined ? '/' : _options$path,
      _options$exact = _options.exact,
      exact = _options$exact === undefined ? false : _options$exact,
      _options$strict = _options.strict,
      strict = _options$strict === undefined ? false : _options$strict,
      _options$sensitive = _options.sensitive,
      sensitive = _options$sensitive === undefined ? false : _options$sensitive;

  var _compilePath = compilePath(path, { end: exact, strict: strict, sensitive: sensitive }),
      re = _compilePath.re,
      keys = _compilePath.keys;

  var match = re.exec(pathname);

  if (!match) return null;

  var url = match[0],
      values = match.slice(1);

  var isExact = pathname === url;

  if (exact && !isExact) return null;

  return {
    path: path, // the path pattern used to match
    url: path === '/' && url === '' ? '/' : url, // the matched portion of the URL
    isExact: isExact, // whether or not we matched exactly
    params: keys.reduce(function (memo, key, index) {
      memo[key.name] = values[index];
      return memo;
    }, {})
  };
};

exports.default = matchPath;

/***/ }),

/***/ "./node_modules/react-router/withRouter.js":
/*!*************************************************!*\
  !*** ./node_modules/react-router/withRouter.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _hoistNonReactStatics = __webpack_require__(/*! hoist-non-react-statics */ "./node_modules/hoist-non-react-statics/index.js");

var _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics);

var _Route = __webpack_require__(/*! ./Route */ "./node_modules/react-router/Route.js");

var _Route2 = _interopRequireDefault(_Route);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

/**
 * A public higher-order component to access the imperative API
 */
var withRouter = function withRouter(Component) {
  var C = function C(props) {
    var wrappedComponentRef = props.wrappedComponentRef,
        remainingProps = _objectWithoutProperties(props, ['wrappedComponentRef']);

    return _react2.default.createElement(_Route2.default, { render: function render(routeComponentProps) {
        return _react2.default.createElement(Component, _extends({}, remainingProps, routeComponentProps, { ref: wrappedComponentRef }));
      } });
  };

  C.displayName = 'withRouter(' + (Component.displayName || Component.name) + ')';
  C.WrappedComponent = Component;
  C.propTypes = {
    wrappedComponentRef: _propTypes2.default.func
  };

  return (0, _hoistNonReactStatics2.default)(C, Component);
};

exports.default = withRouter;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/elements/getById.js":
/*!************************************************!*\
  !*** ./src/client/actions/elements/getById.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

var _getById = __webpack_require__(/*! ../../api/elements/getById */ "./src/client/api/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @return {Object}
 */
/** @format */

var getElementStart = function getElementStart(payload) {
  return { type: _elements.ELEMENT_GET_START, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType
 * @param {string} payload.circleType - only if elementType is 'circle'
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @return {Object}
 */
var getElementSuccess = function getElementSuccess(payload) {
  return { type: _elements.ELEMENT_GET_SUCCESS, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
var getElementError = function getElementError(payload) {
  return { type: _elements.ELEMENT_GET_ERROR, payload: payload };
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
var get = function get(_ref) {
  var token = _ref.token,
      id = _ref.id;
  return function (dispatch) {
    dispatch(getElementStart({ id: id }));

    (0, _getById2.default)({ token: token, id: id }).then(function (_ref2) {
      var statusCode = _ref2.statusCode,
          error = _ref2.error,
          payload = _ref2.payload;

      if (statusCode >= 300) {
        dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));

        return;
      }

      dispatch(getElementSuccess((0, _extends3.default)({ id: id, statusCode: statusCode }, payload)));
    }).catch(function (_ref3) {
      var statusCode = _ref3.statusCode,
          error = _ref3.error;

      dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));
    });
  };
};

exports.default = get;

/***/ }),

/***/ "./src/client/actions/posts/delete.js":
/*!********************************************!*\
  !*** ./src/client/actions/posts/delete.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../constants/posts */ "./src/client/constants/posts.js");

/**
 *
 * @function deletePost
 * @param {Object} payload
 * @param {number} payload.postId
 * @returns {Object}
 */
var deletePost = function deletePost(payload) {
  return {
    type: _posts.POST_DELETE,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = deletePost;

/***/ }),

/***/ "./src/client/api/elements/getById.js":
/*!********************************************!*\
  !*** ./src/client/api/elements/getById.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @function getById
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
/** @format */

var getById = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        id = _ref2.id;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getById;

/***/ }),

/***/ "./src/client/api/posts/users/delete.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/delete.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * delete post of user
 * @async
 * @function deletePost
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file delete post of user.
 */

var deletePost = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token,
        postId = _ref2.postId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts/' + postId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deletePost(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deletePost;

/***/ }),

/***/ "./src/client/containers/Post/index.js":
/*!*********************************************!*\
  !*** ./src/client/containers/Post/index.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withRouter = __webpack_require__(/*! react-router/withRouter */ "./node_modules/react-router/withRouter.js");

var _withRouter2 = _interopRequireDefault(_withRouter);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _classnames2 = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames3 = _interopRequireDefault(_classnames2);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Dialog = __webpack_require__(/*! material-ui/Dialog */ "./node_modules/material-ui/Dialog/index.js");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Badge = __webpack_require__(/*! material-ui/Badge */ "./node_modules/material-ui/Badge/index.js");

var _Badge2 = _interopRequireDefault(_Badge);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _TurnedIn = __webpack_require__(/*! material-ui-icons/TurnedIn */ "./node_modules/material-ui-icons/TurnedIn.js");

var _TurnedIn2 = _interopRequireDefault(_TurnedIn);

var _Comment = __webpack_require__(/*! material-ui-icons/Comment */ "./node_modules/material-ui-icons/Comment.js");

var _Comment2 = _interopRequireDefault(_Comment);

var _Delete = __webpack_require__(/*! material-ui-icons/Delete */ "./node_modules/material-ui-icons/Delete.js");

var _Delete2 = _interopRequireDefault(_Delete);

var _RemoveRedEye = __webpack_require__(/*! material-ui-icons/RemoveRedEye */ "./node_modules/material-ui-icons/RemoveRedEye.js");

var _RemoveRedEye2 = _interopRequireDefault(_RemoveRedEye);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _delete = __webpack_require__(/*! ../../api/posts/users/delete */ "./src/client/api/posts/users/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _delete3 = __webpack_require__(/*! ../../actions/posts/delete */ "./src/client/actions/posts/delete.js");

var _delete4 = _interopRequireDefault(_delete3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      padding: '1%'
    },
    card: {
      borderRadius: '8px'
    },
    avatar: {
      borderRadius: '50%'
    },
    title: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    subheader: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    media: {
      height: 194
    },
    favoriteIconActive: {
      color: '#F50057'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    badge: {
      margin: '0 ' + theme.spacing.unit * 2 + 'px',
      color: 'blue'
    },
    margin: {
      border: '0.5px solid rgba(209,209,209,1)',
      marginBottom: '-10px'
    },
    content: {
      textAlign: 'justify'
    }
  };
};

// All actions are defined here.
/**
 * Post container component
 * It will display one post with some of actions (eg. Bookmarks, Share, Delete)
 *
 * @format
 */

var Post = function (_Component) {
  (0, _inherits3.default)(Post, _Component);

  function Post(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, Post);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Post.__proto__ || (0, _getPrototypeOf2.default)(Post)).call(this, props));

    _this.onClickFavorite = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var favorite;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              favorite = _this.state.favorite;


              _this.setState({ favorite: !favorite });

            case 2:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this2);
    }));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onClickPost = function () {
      return _this.props.history.push('/posts/' + _this.props.postId);
    };

    _this.deleteDialogOpen = function () {
      _this.setState({ deleteDialog: true });
    };

    _this.deleteDialogClose = function () {
      _this.setState({ deleteDialog: false });
    };

    _this.deletePostClick = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
      var _this$props$elements$, token, id, postId, _ref3, statusCode, error;

      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _this$props$elements$ = _this.props.elements.user, token = _this$props$elements$.token, id = _this$props$elements$.id;
              postId = _this.props.postId;
              _context2.next = 4;
              return (0, _delete2.default)({
                token: token,
                userId: id,
                postId: postId
              });

            case 4:
              _ref3 = _context2.sent;
              statusCode = _ref3.statusCode;
              error = _ref3.error;

              if (!(statusCode !== 200)) {
                _context2.next = 10;
                break;
              }

              // show some error message.
              console.log(error);
              return _context2.abrupt('return');

            case 10:

              _this.props.actionPostDelete({ postId: postId });
              _this.setState({ deleteDialog: false });

            case 12:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }));

    _this.state = {
      hover: false,
      favorite: false,
      deleteDialog: false
    };
    return _this;
  }

  // componentWillMount is executed before rendering


  (0, _createClass3.default)(Post, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _props = this.props,
          elements = _props.elements,
          posts = _props.posts,
          postId = _props.postId;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = posts[postId].authorId;


      var author = elements[authorId];

      // if author is not found
      if (userId !== authorId && !author) {
        this.props.getElementById({ id: authorId, token: token });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          posts = _props2.posts,
          elements = _props2.elements,
          postId = _props2.postId;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id;


      var post = posts[postId];

      var favorite = this.state.favorite;


      if (typeof post === 'undefined') {
        return _react2.default.createElement('div', null);
      }

      var cover = post.cover,
          title = post.title,
          description = post.description,
          authorId = post.authorId;

      var author = authorId === userId ? user : elements[authorId];

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + author.username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: author.name,
                  src: '/public/photos/mayash-logo-transparent.png'
                })
              )
            ),
            action: _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Coming soon', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { disabled: true },
                _react2.default.createElement(_MoreVert2.default, null)
              )
            ),
            title: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.title },
              author.name
            ),
            subheader: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.subheader },
              '@',
              author.username
            )
          }),
          _react2.default.createElement(_Card.CardMedia, {
            className: classes.media,
            image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg',
            title: title
          }),
          _react2.default.createElement(
            _Card.CardContent,
            { onClick: this.onClickPost },
            _react2.default.createElement(
              _Typography2.default,
              { type: 'title' },
              title
            ),
            typeof description === 'string' ? _react2.default.createElement(
              _Typography2.default,
              { type: 'body1', gutterBottom: true, className: classes.content },
              description
            ) : null
          ),
          _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement(
              _IconButton2.default,
              { onClick: this.onClickFavorite },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_Favorite2.default, {
                  className: (0, _classnames3.default)((0, _defineProperty3.default)({}, '' + classes.favoriteIconActive, favorite))
                })
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Comment' },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_Comment2.default, null)
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Book Marks' },
              _react2.default.createElement(_TurnedIn2.default, null)
            ),
            isSignedIn && userId === authorId ? _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Delete', onClick: this.deleteDialogOpen },
                _react2.default.createElement(_Delete2.default, null)
              ),
              _react2.default.createElement(
                _Dialog2.default,
                {
                  open: this.state.deleteDialog,
                  onClose: this.deleteDialogClose,
                  'aria-labelledby': 'post-' + postId + '-delete-dialog'
                },
                _react2.default.createElement(
                  _Dialog.DialogTitle,
                  { id: 'post-' + postId + '-delete-dialog' },
                  'Are you sure?'
                ),
                _react2.default.createElement(
                  _Dialog.DialogActions,
                  null,
                  _react2.default.createElement(
                    _Button2.default,
                    { onClick: this.deleteDialogClose, color: 'primary' },
                    'Cancel'
                  ),
                  _react2.default.createElement(
                    _Button2.default,
                    {
                      onClick: this.deletePostClick,
                      color: 'primary',
                      autoFocus: true
                    },
                    'Delete'
                  )
                )
              )
            ) : null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share' },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_RemoveRedEye2.default, null)
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share', disabled: true },
              _react2.default.createElement(_Share2.default, null)
            )
          )
        )
      );
    }
  }]);
  return Post;
}(_react.Component);

Post.propTypes = {
  history: _propTypes2.default.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object.isRequired,

  // post: PropTypes.shape({
  //   authorId: PropTypes.number.isRequired,
  //   postId: PropTypes.number.isRequired,
  //   title: PropTypes.string.isRequired,
  //   description: PropTypes.string,
  // }).isRequired,

  postId: _propTypes2.default.number.isRequired,

  getElementById: _propTypes2.default.func.isRequired,
  actionPostDelete: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements,
      posts = _ref4.posts;
  return { elements: elements, posts: posts };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    getElementById: _getById2.default,
    actionPostDelete: _delete4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)((0, _withRouter2.default)(Post)));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1JlbW92ZVJlZEV5ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL0JhZGdlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0FjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nQ29udGVudFRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dUaXRsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvd2l0aE1vYmlsZURpYWxvZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9CYWNrZHJvcC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvTW9kYWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1BvcnRhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvRmFkZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvUm91dGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9tYXRjaFBhdGguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci93aXRoUm91dGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2VsZW1lbnRzL2dldEJ5SWQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL3Bvc3RzL2RlbGV0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRCeUlkLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3Bvc3RzL3VzZXJzL2RlbGV0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdC9pbmRleC5qcyJdLCJuYW1lcyI6WyJnZXRFbGVtZW50U3RhcnQiLCJwYXlsb2FkIiwidHlwZSIsImdldEVsZW1lbnRTdWNjZXNzIiwiZ2V0RWxlbWVudEVycm9yIiwiZ2V0IiwidG9rZW4iLCJpZCIsImRpc3BhdGNoIiwidGhlbiIsInN0YXR1c0NvZGUiLCJlcnJvciIsImNhdGNoIiwiZGVsZXRlUG9zdCIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsImpzb24iLCJjb25zb2xlIiwiZ2V0QnlJZCIsInVzZXJJZCIsInBvc3RJZCIsInN0eWxlcyIsInRoZW1lIiwicm9vdCIsInBhZGRpbmciLCJjYXJkIiwiYm9yZGVyUmFkaXVzIiwiYXZhdGFyIiwidGl0bGUiLCJjdXJzb3IiLCJ0ZXh0RGVjb3JhdGlvbiIsInN1YmhlYWRlciIsIm1lZGlhIiwiaGVpZ2h0IiwiZmF2b3JpdGVJY29uQWN0aXZlIiwiY29sb3IiLCJmbGV4R3JvdyIsImZsZXgiLCJiYWRnZSIsIm1hcmdpbiIsInNwYWNpbmciLCJ1bml0IiwiYm9yZGVyIiwibWFyZ2luQm90dG9tIiwiY29udGVudCIsInRleHRBbGlnbiIsIlBvc3QiLCJwcm9wcyIsIm9uQ2xpY2tGYXZvcml0ZSIsImZhdm9yaXRlIiwic3RhdGUiLCJzZXRTdGF0ZSIsIm9uTW91c2VFbnRlciIsImhvdmVyIiwib25Nb3VzZUxlYXZlIiwib25DbGlja1Bvc3QiLCJoaXN0b3J5IiwicHVzaCIsImRlbGV0ZURpYWxvZ09wZW4iLCJkZWxldGVEaWFsb2ciLCJkZWxldGVEaWFsb2dDbG9zZSIsImRlbGV0ZVBvc3RDbGljayIsImVsZW1lbnRzIiwidXNlciIsImxvZyIsImFjdGlvblBvc3REZWxldGUiLCJwb3N0cyIsImF1dGhvcklkIiwiYXV0aG9yIiwiZ2V0RWxlbWVudEJ5SWQiLCJjbGFzc2VzIiwiaXNTaWduZWRJbiIsInBvc3QiLCJjb3ZlciIsImRlc2NyaXB0aW9uIiwidXNlcm5hbWUiLCJuYW1lIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm51bWJlciIsImZ1bmMiLCJtYXBTdGF0ZVRvUHJvcHMiLCJtYXBEaXNwYXRjaFRvUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRyxZQUFZO0FBQ2Y7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNUQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNUQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUEsb0M7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxrSUFBa0k7O0FBRXBMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMEI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxxRkFBcUY7O0FBRXZJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxzTEFBc0w7O0FBRXhPO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx5SkFBeUo7O0FBRTNNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx5TkFBeU47O0FBRTNRO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCwrV0FBK1c7O0FBRWphO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsd0I7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxtRUFBbUU7O0FBRXJIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGO0FBQzlGOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlFQUFpRSxnQ0FBZ0M7QUFDakcsU0FBUztBQUNUO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELG9CQUFvQixVOzs7Ozs7Ozs7Ozs7O0FDL016RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STs7QUFFNUk7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esb0dBQW9HOztBQUVwRztBQUNBO0FBQ0EsZ0NBQWdDLHVCQUF1QjtBQUN2RDtBQUNBO0FBQ0E7QUFDQSxXQUFXLDRCQUE0QjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELG1CQUFtQixTOzs7Ozs7Ozs7Ozs7O0FDbkt4RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwSkFBMEo7QUFDMUosYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUNoVXpFOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLDRCQUE0QixnRUFBZ0U7QUFDNUY7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFNBQVMsNEJBQTRCO0FBQ3JDO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QscURBQXFELDJCQUEyQixpQjs7Ozs7Ozs7Ozs7OztBQ2hIaEY7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsNEJBQTRCLGdFQUFnRTtBQUM1RjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELHFEQUFxRCwyQkFBMkIsaUI7Ozs7Ozs7Ozs7Ozs7QUN2RmhGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLDRCQUE0QixnRUFBZ0U7QUFDNUY7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxxREFBcUQsK0JBQStCLHFCOzs7Ozs7Ozs7Ozs7O0FDdkZwRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxnQ0FBZ0MsZ0VBQWdFO0FBQ2hHO0FBQ0E7QUFDQSxXQUFXLGdCQUFnQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELHlCQUF5QixlOzs7Ozs7Ozs7Ozs7O0FDNUg5RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDNUQ3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxRkFBcUY7QUFDckY7QUFDQTs7O0FBR0E7QUFDQSw4RUFBOEUsbUVBQW1FO0FBQ2pKOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2hFQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixtUEFBNEk7QUFDNUk7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSw4RUFBOEU7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFdBQVcsMkJBQTJCO0FBQ3RDO0FBQ0E7QUFDQSxhQUFhLDBCQUEwQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmOztBQUVBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCx3QkFBd0IsYzs7Ozs7Ozs7Ozs7OztBQ3JPN0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLGtHQUFrRzs7QUFFbEc7QUFDQTtBQUNBLGdDQUFnQyxrREFBa0Q7QUFDbEY7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsc0JBQXNCLFk7Ozs7Ozs7Ozs7Ozs7QUN4STNFOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsZUFBZTtBQUN0QztBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixnQkFBZ0I7QUFDdkM7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxnQ0FBZ0MsZ0VBQWdFO0FBQ2hHO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsMEdBQTBHO0FBQzFHLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFCQUFxQixlQUFlO0FBQ3BDO0FBQ0E7QUFDQTs7QUFFQSxxREFBcUQsZ0NBQWdDLFM7Ozs7Ozs7Ozs7Ozs7QUNqbEJyRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtRkFBbUY7QUFDbkY7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx1QkFBdUIsdUJBQXVCO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHNCQUFzQjs7QUFFdEI7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ3RKQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELHlCOzs7Ozs7Ozs7Ozs7O0FDckxBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7O0FDaEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSwyQ0FBMkM7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EOzs7Ozs7Ozs7Ozs7O0FDcE5BOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQzs7Ozs7Ozs7Ozs7OztBQ2hEQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsOENBQThDLGlCQUFpQixxQkFBcUIsb0NBQW9DLDZEQUE2RCxvQkFBb0IsRUFBRSxlQUFlOztBQUUxTixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnRkFBZ0Y7O0FBRWhGOztBQUVBLGdGQUFnRixlQUFlOztBQUUvRix5REFBeUQsVUFBVSx1REFBdUQ7QUFDMUg7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLHVCOzs7Ozs7Ozs7Ozs7O0FDN0dBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNENBQTRDOztBQUU1Qzs7QUFFQTs7QUFFQTs7QUFFQSxzREFBc0QsaUVBQWlFO0FBQ3ZIOztBQUVBO0FBQ0Esb0pBQW9KOztBQUVwSixpTUFBaU07O0FBRWpNLDJMQUEyTDtBQUMzTDs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxpQkFBaUI7O0FBRWpCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0I7Ozs7Ozs7Ozs7Ozs7QUN2SkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0Esb0VBQW9FOztBQUVwRTs7QUFFQTtBQUNBO0FBQ0EseUJBQXlCOztBQUV6QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsOENBQThDOztBQUU5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsd0NBQXdDLG1EQUFtRDtBQUMzRjtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLLElBQUk7QUFDVDtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDNUVBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Riw4Q0FBOEMsaUJBQWlCLHFCQUFxQixvQ0FBb0MsNkRBQTZELG9CQUFvQixFQUFFLGVBQWU7O0FBRTFOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDJEQUEyRDtBQUMzRCxtRUFBbUUsd0NBQXdDLDJCQUEyQjtBQUN0SSxPQUFPLEVBQUU7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsNkI7Ozs7Ozs7Ozs7Ozs7QUNoREE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7OztBQ3JCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDZkE7O0FBRUE7O0FBRUEsb0dBQW9HLG1CQUFtQixFQUFFLG1CQUFtQiw4SEFBOEg7O0FBRTFRO0FBQ0E7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsOEQ7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDWkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLHlDOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ3pEQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFVBQVUscUJBQXFCO0FBQzVEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHlDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWkE7O0FBTUE7Ozs7OztBQUVBOzs7Ozs7QUFWQTs7QUFnQkEsSUFBTUEsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFDQyxPQUFEO0FBQUEsU0FBYyxFQUFFQyxpQ0FBRixFQUEyQkQsZ0JBQTNCLEVBQWQ7QUFBQSxDQUF4Qjs7QUFFQTs7Ozs7Ozs7Ozs7OztBQWFBLElBQU1FLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQUNGLE9BQUQ7QUFBQSxTQUFjLEVBQUVDLG1DQUFGLEVBQTZCRCxnQkFBN0IsRUFBZDtBQUFBLENBQTFCOztBQUVBOzs7Ozs7OztBQVFBLElBQU1HLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ0gsT0FBRDtBQUFBLFNBQWMsRUFBRUMsaUNBQUYsRUFBMkJELGdCQUEzQixFQUFkO0FBQUEsQ0FBeEI7O0FBRUE7Ozs7Ozs7QUFPQSxJQUFNSSxNQUFNLFNBQU5BLEdBQU07QUFBQSxNQUFHQyxLQUFILFFBQUdBLEtBQUg7QUFBQSxNQUFVQyxFQUFWLFFBQVVBLEVBQVY7QUFBQSxTQUFtQixVQUFDQyxRQUFELEVBQWM7QUFDM0NBLGFBQVNSLGdCQUFnQixFQUFFTyxNQUFGLEVBQWhCLENBQVQ7O0FBRUEsMkJBQVcsRUFBRUQsWUFBRixFQUFTQyxNQUFULEVBQVgsRUFDR0UsSUFESCxDQUNRLGlCQUFvQztBQUFBLFVBQWpDQyxVQUFpQyxTQUFqQ0EsVUFBaUM7QUFBQSxVQUFyQkMsS0FBcUIsU0FBckJBLEtBQXFCO0FBQUEsVUFBZFYsT0FBYyxTQUFkQSxPQUFjOztBQUN4QyxVQUFJUyxjQUFjLEdBQWxCLEVBQXVCO0FBQ3JCRixpQkFBU0osZ0JBQWdCLEVBQUVHLE1BQUYsRUFBTUcsc0JBQU4sRUFBa0JDLFlBQWxCLEVBQWhCLENBQVQ7O0FBRUE7QUFDRDs7QUFFREgsZUFBU0wsMkNBQW9CSSxNQUFwQixFQUF3Qkcsc0JBQXhCLElBQXVDVCxPQUF2QyxFQUFUO0FBQ0QsS0FUSCxFQVVHVyxLQVZILENBVVMsaUJBQTJCO0FBQUEsVUFBeEJGLFVBQXdCLFNBQXhCQSxVQUF3QjtBQUFBLFVBQVpDLEtBQVksU0FBWkEsS0FBWTs7QUFDaENILGVBQVNKLGdCQUFnQixFQUFFRyxNQUFGLEVBQU1HLHNCQUFOLEVBQWtCQyxZQUFsQixFQUFoQixDQUFUO0FBQ0QsS0FaSDtBQWFELEdBaEJXO0FBQUEsQ0FBWjs7a0JBa0JlTixHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0RmOztBQVlBOzs7Ozs7O0FBT0EsSUFBTVEsYUFBYSxTQUFiQSxVQUFhLENBQUNaLE9BQUQ7QUFBQSxTQUErQjtBQUNoREMsNEJBRGdEO0FBRWhERDtBQUZnRCxHQUEvQjtBQUFBLENBQW5CLEMsQ0F4QkE7Ozs7O2tCQTZCZVksVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hCZjs7Ozs7OztBQUxBOzs7c0ZBWUE7QUFBQSxRQUF5QlAsS0FBekIsU0FBeUJBLEtBQXpCO0FBQUEsUUFBZ0NDLEVBQWhDLFNBQWdDQSxFQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVTyxlQUZWLHFDQUV3Q1AsRUFGeEM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTU8sR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVaO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVYSxlQUpWO0FBYVlDLGtCQWJaLEdBYW1DRCxHQWJuQyxDQWFZQyxNQWJaLEVBYW9CQyxVQWJwQixHQWFtQ0YsR0FibkMsQ0Fhb0JFLFVBYnBCOztBQUFBLGtCQWVRRCxVQUFVLEdBZmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdCYTtBQUNMViwwQkFBWVUsTUFEUDtBQUVMVCxxQkFBT1U7QUFGRixhQWhCYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0J1QkYsSUFBSUcsSUFBSixFQXRCdkI7O0FBQUE7QUFzQlVBLGdCQXRCVjtBQUFBLHdFQXdCZ0JBLElBeEJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMEJJQyxvQkFBUVosS0FBUjs7QUExQkosNkNBNEJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWEsTzs7Ozs7QUFWZjs7OztBQUNBOzs7O2tCQTRDZUEsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZDZjs7Ozs7Ozs7Ozs7O0FBUkE7Ozs7OztzRkFvQkE7QUFBQSxRQUE0QkMsTUFBNUIsU0FBNEJBLE1BQTVCO0FBQUEsUUFBb0NuQixLQUFwQyxTQUFvQ0EsS0FBcEM7QUFBQSxRQUEyQ29CLE1BQTNDLFNBQTJDQSxNQUEzQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVWixlQUZWLGtDQUVxQ1csTUFGckMsZUFFcURDLE1BRnJEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1aLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsUUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlWjtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVWEsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTFYsMEJBQVlVLE1BRFA7QUFFTFQscUJBQU9VO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlHLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFaLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVFLFU7Ozs7O0FBZmY7Ozs7QUFDQTs7OztrQkFpRGVBLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hEZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQU1BOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUdBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTWMsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pDLGVBQVM7QUFETCxLQURtQjtBQUl6QkMsVUFBTTtBQUNKQyxvQkFBYztBQURWLEtBSm1CO0FBT3pCQyxZQUFRO0FBQ05ELG9CQUFjO0FBRFIsS0FQaUI7QUFVekJFLFdBQU87QUFDTDtBQUNBQyxjQUFRLFNBRkg7QUFHTEMsc0JBQWdCO0FBSFgsS0FWa0I7QUFlekJDLGVBQVc7QUFDVDtBQUNBRixjQUFRLFNBRkM7QUFHVEMsc0JBQWdCO0FBSFAsS0FmYztBQW9CekJFLFdBQU87QUFDTEMsY0FBUTtBQURILEtBcEJrQjtBQXVCekJDLHdCQUFvQjtBQUNsQkMsYUFBTztBQURXLEtBdkJLO0FBMEJ6QkMsY0FBVTtBQUNSQyxZQUFNO0FBREUsS0ExQmU7QUE2QnpCQyxXQUFPO0FBQ0xDLHFCQUFhakIsTUFBTWtCLE9BQU4sQ0FBY0MsSUFBZCxHQUFxQixDQUFsQyxPQURLO0FBRUxOLGFBQU87QUFGRixLQTdCa0I7QUFpQ3pCSSxZQUFRO0FBQ05HLGNBQVEsaUNBREY7QUFFTkMsb0JBQWM7QUFGUixLQWpDaUI7QUFxQ3pCQyxhQUFTO0FBQ1BDLGlCQUFXO0FBREo7QUFyQ2dCLEdBQVo7QUFBQSxDQUFmOztBQU5BO0FBekNBOzs7Ozs7O0lBeUZNQyxJOzs7QUFDSixnQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBOztBQUFBLGtJQUNYQSxLQURXOztBQUFBLFVBdUJuQkMsZUF2Qm1CLDRFQXVCRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDUkMsc0JBRFEsR0FDSyxNQUFLQyxLQURWLENBQ1JELFFBRFE7OztBQUdoQixvQkFBS0UsUUFBTCxDQUFjLEVBQUVGLFVBQVUsQ0FBQ0EsUUFBYixFQUFkOztBQUhnQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQXZCQzs7QUFBQSxVQTZCbkJHLFlBN0JtQixHQTZCSjtBQUFBLGFBQU0sTUFBS0QsUUFBTCxDQUFjLEVBQUVFLE9BQU8sSUFBVCxFQUFkLENBQU47QUFBQSxLQTdCSTs7QUFBQSxVQThCbkJDLFlBOUJtQixHQThCSjtBQUFBLGFBQU0sTUFBS0gsUUFBTCxDQUFjLEVBQUVFLE9BQU8sS0FBVCxFQUFkLENBQU47QUFBQSxLQTlCSTs7QUFBQSxVQWdDbkJFLFdBaENtQixHQWdDTDtBQUFBLGFBQU0sTUFBS1IsS0FBTCxDQUFXUyxPQUFYLENBQW1CQyxJQUFuQixhQUFrQyxNQUFLVixLQUFMLENBQVczQixNQUE3QyxDQUFOO0FBQUEsS0FoQ0s7O0FBQUEsVUFrQ25Cc0MsZ0JBbENtQixHQWtDQSxZQUFNO0FBQ3ZCLFlBQUtQLFFBQUwsQ0FBYyxFQUFFUSxjQUFjLElBQWhCLEVBQWQ7QUFDRCxLQXBDa0I7O0FBQUEsVUFzQ25CQyxpQkF0Q21CLEdBc0NDLFlBQU07QUFDeEIsWUFBS1QsUUFBTCxDQUFjLEVBQUVRLGNBQWMsS0FBaEIsRUFBZDtBQUNELEtBeENrQjs7QUFBQSxVQTBDbkJFLGVBMUNtQiw0RUEwQ0Q7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQUNNLE1BQUtkLEtBQUwsQ0FBV2UsUUFBWCxDQUFvQkMsSUFEMUIsRUFDUi9ELEtBRFEseUJBQ1JBLEtBRFEsRUFDREMsRUFEQyx5QkFDREEsRUFEQztBQUVSbUIsb0JBRlEsR0FFRyxNQUFLMkIsS0FGUixDQUVSM0IsTUFGUTtBQUFBO0FBQUEscUJBSW9CLHNCQUFjO0FBQ2hEcEIsNEJBRGdEO0FBRWhEbUIsd0JBQVFsQixFQUZ3QztBQUdoRG1CO0FBSGdELGVBQWQsQ0FKcEI7O0FBQUE7QUFBQTtBQUlSaEIsd0JBSlEsU0FJUkEsVUFKUTtBQUlJQyxtQkFKSixTQUlJQSxLQUpKOztBQUFBLG9CQVVaRCxlQUFlLEdBVkg7QUFBQTtBQUFBO0FBQUE7O0FBV2Q7QUFDQWEsc0JBQVErQyxHQUFSLENBQVkzRCxLQUFaO0FBWmM7O0FBQUE7O0FBZ0JoQixvQkFBSzBDLEtBQUwsQ0FBV2tCLGdCQUFYLENBQTRCLEVBQUU3QyxjQUFGLEVBQTVCO0FBQ0Esb0JBQUsrQixRQUFMLENBQWMsRUFBRVEsY0FBYyxLQUFoQixFQUFkOztBQWpCZ0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0ExQ0M7O0FBRWpCLFVBQUtULEtBQUwsR0FBYTtBQUNYRyxhQUFPLEtBREk7QUFFWEosZ0JBQVUsS0FGQztBQUdYVSxvQkFBYztBQUhILEtBQWI7QUFGaUI7QUFPbEI7O0FBRUQ7Ozs7O3lDQUNxQjtBQUFBLG1CQUNpQixLQUFLWixLQUR0QjtBQUFBLFVBQ1hlLFFBRFcsVUFDWEEsUUFEVztBQUFBLFVBQ0RJLEtBREMsVUFDREEsS0FEQztBQUFBLFVBQ005QyxNQUROLFVBQ01BLE1BRE47QUFBQSwyQkFFVzBDLFNBQVNDLElBRnBCO0FBQUEsVUFFWC9ELEtBRlcsa0JBRVhBLEtBRlc7QUFBQSxVQUVBbUIsTUFGQSxrQkFFSmxCLEVBRkk7QUFBQSxVQUdYa0UsUUFIVyxHQUdFRCxNQUFNOUMsTUFBTixDQUhGLENBR1grQyxRQUhXOzs7QUFLbkIsVUFBTUMsU0FBU04sU0FBU0ssUUFBVCxDQUFmOztBQUVBO0FBQ0EsVUFBSWhELFdBQVdnRCxRQUFYLElBQXVCLENBQUNDLE1BQTVCLEVBQW9DO0FBQ2xDLGFBQUtyQixLQUFMLENBQVdzQixjQUFYLENBQTBCLEVBQUVwRSxJQUFJa0UsUUFBTixFQUFnQm5FLFlBQWhCLEVBQTFCO0FBQ0Q7QUFDRjs7OzZCQXlDUTtBQUFBLG9CQUNzQyxLQUFLK0MsS0FEM0M7QUFBQSxVQUNDdUIsT0FERCxXQUNDQSxPQUREO0FBQUEsVUFDVUosS0FEVixXQUNVQSxLQURWO0FBQUEsVUFDaUJKLFFBRGpCLFdBQ2lCQSxRQURqQjtBQUFBLFVBQzJCMUMsTUFEM0IsV0FDMkJBLE1BRDNCO0FBQUEsVUFFQzJDLElBRkQsR0FFVUQsUUFGVixDQUVDQyxJQUZEO0FBQUEsVUFHQ1EsVUFIRCxHQUc0QlIsSUFINUIsQ0FHQ1EsVUFIRDtBQUFBLFVBR2lCcEQsTUFIakIsR0FHNEI0QyxJQUg1QixDQUdhOUQsRUFIYjs7O0FBS1AsVUFBTXVFLE9BQU9OLE1BQU05QyxNQUFOLENBQWI7O0FBTE8sVUFPQzZCLFFBUEQsR0FPYyxLQUFLQyxLQVBuQixDQU9DRCxRQVBEOzs7QUFTUCxVQUFJLE9BQU91QixJQUFQLEtBQWdCLFdBQXBCLEVBQWlDO0FBQy9CLGVBQU8sMENBQVA7QUFDRDs7QUFYTSxVQWFDQyxLQWJELEdBYXlDRCxJQWJ6QyxDQWFDQyxLQWJEO0FBQUEsVUFhUTdDLEtBYlIsR0FheUM0QyxJQWJ6QyxDQWFRNUMsS0FiUjtBQUFBLFVBYWU4QyxXQWJmLEdBYXlDRixJQWJ6QyxDQWFlRSxXQWJmO0FBQUEsVUFhNEJQLFFBYjVCLEdBYXlDSyxJQWJ6QyxDQWE0QkwsUUFiNUI7O0FBY1AsVUFBTUMsU0FBU0QsYUFBYWhELE1BQWIsR0FBc0I0QyxJQUF0QixHQUE2QkQsU0FBU0ssUUFBVCxDQUE1Qzs7QUFFQSxhQUNFO0FBQUE7QUFBQTtBQUNFLHFCQUFXRyxRQUFRL0MsSUFEckI7QUFFRSx3QkFBYyxLQUFLNkIsWUFGckI7QUFHRSx3QkFBYyxLQUFLRTtBQUhyQjtBQUtFO0FBQUE7QUFBQSxZQUFNLFFBQVEsS0FBS0osS0FBTCxDQUFXRyxLQUF6QixFQUFnQyxXQUFXaUIsUUFBUTdDLElBQW5EO0FBQ0U7QUFDRSxvQkFDRTtBQUFBO0FBQUEsZ0JBQU8sV0FBVzZDLFFBQVEzQyxNQUExQixFQUFrQyxXQUFXLENBQTdDO0FBQ0U7QUFBQTtBQUFBLGtCQUFNLFdBQVN5QyxPQUFPTyxRQUF0QjtBQUNFO0FBQ0UsdUJBQUtQLE9BQU9RLElBRGQ7QUFFRSx1QkFBSztBQUZQO0FBREY7QUFERixhQUZKO0FBV0Usb0JBQ0U7QUFBQTtBQUFBLGdCQUFTLE9BQU0sYUFBZixFQUE2QixXQUFVLFFBQXZDO0FBQ0U7QUFBQTtBQUFBLGtCQUFZLGNBQVo7QUFDRTtBQURGO0FBREYsYUFaSjtBQWtCRSxtQkFDRTtBQUFBO0FBQUEsZ0JBQU0sV0FBU1IsT0FBT08sUUFBdEIsRUFBa0MsV0FBV0wsUUFBUTFDLEtBQXJEO0FBQ0d3QyxxQkFBT1E7QUFEVixhQW5CSjtBQXVCRSx1QkFDRTtBQUFBO0FBQUEsZ0JBQU0sV0FBU1IsT0FBT08sUUFBdEIsRUFBa0MsV0FBV0wsUUFBUXZDLFNBQXJEO0FBQUE7QUFDSXFDLHFCQUFPTztBQURYO0FBeEJKLFlBREY7QUE4QkU7QUFDRSx1QkFBV0wsUUFBUXRDLEtBRHJCO0FBRUUsbUJBQ0V5QyxTQUNBLDJFQUpKO0FBTUUsbUJBQU83QztBQU5ULFlBOUJGO0FBc0NFO0FBQUE7QUFBQSxjQUFhLFNBQVMsS0FBSzJCLFdBQTNCO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLE1BQUssT0FBakI7QUFBMEIzQjtBQUExQixhQURGO0FBRUcsbUJBQU84QyxXQUFQLEtBQXVCLFFBQXZCLEdBQ0M7QUFBQTtBQUFBLGdCQUFZLE1BQUssT0FBakIsRUFBeUIsa0JBQXpCLEVBQXNDLFdBQVdKLFFBQVExQixPQUF6RDtBQUNHOEI7QUFESCxhQURELEdBSUc7QUFOTixXQXRDRjtBQStDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsZ0JBQVksU0FBUyxLQUFLMUIsZUFBMUI7QUFDRTtBQUFBO0FBQUEsa0JBQU8sY0FBYyxDQUFyQjtBQUNFO0FBQ0UsNkJBQVcsaUVBQ0xzQixRQUFRcEMsa0JBREgsRUFDMEJlLFFBRDFCO0FBRGI7QUFERjtBQURGLGFBREY7QUFVRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxTQUF2QjtBQUNFO0FBQUE7QUFBQSxrQkFBTyxjQUFjLENBQXJCO0FBQ0U7QUFERjtBQURGLGFBVkY7QUFlRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxZQUF2QjtBQUNFO0FBREYsYUFmRjtBQWtCR3NCLDBCQUFjcEQsV0FBV2dELFFBQXpCLEdBQ0M7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLGtCQUFZLGNBQVcsUUFBdkIsRUFBZ0MsU0FBUyxLQUFLVCxnQkFBOUM7QUFDRTtBQURGLGVBREY7QUFJRTtBQUFBO0FBQUE7QUFDRSx3QkFBTSxLQUFLUixLQUFMLENBQVdTLFlBRG5CO0FBRUUsMkJBQVMsS0FBS0MsaUJBRmhCO0FBR0UsK0NBQXlCeEMsTUFBekI7QUFIRjtBQUtFO0FBQUE7QUFBQSxvQkFBYSxjQUFZQSxNQUFaLG1CQUFiO0FBQUE7QUFBQSxpQkFMRjtBQVFFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxzQkFBUSxTQUFTLEtBQUt3QyxpQkFBdEIsRUFBeUMsT0FBTSxTQUEvQztBQUFBO0FBQUEsbUJBREY7QUFJRTtBQUFBO0FBQUE7QUFDRSwrQkFBUyxLQUFLQyxlQURoQjtBQUVFLDZCQUFNLFNBRlI7QUFHRTtBQUhGO0FBQUE7QUFBQTtBQUpGO0FBUkY7QUFKRixhQURELEdBMkJHLElBN0NOO0FBOENFLG1EQUFLLFdBQVdTLFFBQVFsQyxRQUF4QixHQTlDRjtBQStDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QjtBQUNFO0FBQUE7QUFBQSxrQkFBTyxjQUFjLENBQXJCO0FBQ0U7QUFERjtBQURGLGFBL0NGO0FBb0RFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE9BQXZCLEVBQStCLGNBQS9CO0FBQ0U7QUFERjtBQXBERjtBQS9DRjtBQUxGLE9BREY7QUFnSEQ7Ozs7O0FBR0hVLEtBQUsrQixTQUFMLEdBQWlCO0FBQ2ZyQixXQUFTLG9CQUFVc0IsTUFBVixDQUFpQkMsVUFEWDs7QUFHZlQsV0FBUyxvQkFBVVEsTUFBVixDQUFpQkMsVUFIWDs7QUFLZmpCLFlBQVUsb0JBQVVnQixNQUFWLENBQWlCQyxVQUxaO0FBTWZiLFNBQU8sb0JBQVVZLE1BQVYsQ0FBaUJDLFVBTlQ7O0FBUWY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBM0QsVUFBUSxvQkFBVTRELE1BQVYsQ0FBaUJELFVBZlY7O0FBaUJmVixrQkFBZ0Isb0JBQVVZLElBQVYsQ0FBZUYsVUFqQmhCO0FBa0JmZCxvQkFBa0Isb0JBQVVnQixJQUFWLENBQWVGO0FBbEJsQixDQUFqQjs7QUFxQkEsSUFBTUcsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdwQixRQUFILFNBQUdBLFFBQUg7QUFBQSxNQUFhSSxLQUFiLFNBQWFBLEtBQWI7QUFBQSxTQUEwQixFQUFFSixrQkFBRixFQUFZSSxZQUFaLEVBQTFCO0FBQUEsQ0FBeEI7O0FBRUEsSUFBTWlCLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUNqRixRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRW1FLHFDQURGO0FBRUVKO0FBRkYsR0FERixFQUtFL0QsUUFMRixDQUR5QjtBQUFBLENBQTNCOztrQkFTZSx5QkFBUWdGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXOUQsTUFBWCxFQUFtQiwwQkFBV3lCLElBQVgsQ0FBbkIsQ0FEYSxDIiwiZmlsZSI6IjM3LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGFjdGl2ZUVsZW1lbnQ7XG5cbnZhciBfb3duZXJEb2N1bWVudCA9IHJlcXVpcmUoJy4vb3duZXJEb2N1bWVudCcpO1xuXG52YXIgX293bmVyRG9jdW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3duZXJEb2N1bWVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIGFjdGl2ZUVsZW1lbnQoKSB7XG4gIHZhciBkb2MgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6ICgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoKTtcblxuICB0cnkge1xuICAgIHJldHVybiBkb2MuYWN0aXZlRWxlbWVudDtcbiAgfSBjYXRjaCAoZSkgey8qIGllIHRocm93cyBpZiBubyBhY3RpdmUgZWxlbWVudCAqL31cbn1cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1snZGVmYXVsdCddO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSBvd25lckRvY3VtZW50O1xuZnVuY3Rpb24gb3duZXJEb2N1bWVudChub2RlKSB7XG4gIHJldHVybiBub2RlICYmIG5vZGUub3duZXJEb2N1bWVudCB8fCBkb2N1bWVudDtcbn1cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1tcImRlZmF1bHRcIl07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGdldFdpbmRvdztcbmZ1bmN0aW9uIGdldFdpbmRvdyhub2RlKSB7XG4gIHJldHVybiBub2RlID09PSBub2RlLndpbmRvdyA/IG5vZGUgOiBub2RlLm5vZGVUeXBlID09PSA5ID8gbm9kZS5kZWZhdWx0VmlldyB8fCBub2RlLnBhcmVudFdpbmRvdyA6IGZhbHNlO1xufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzW1wiZGVmYXVsdFwiXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9xdWVyeS9pc1dpbmRvdy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAocmVjYWxjKSB7XG4gIGlmICghc2l6ZSAmJiBzaXplICE9PSAwIHx8IHJlY2FsYykge1xuICAgIGlmIChfaW5ET00yLmRlZmF1bHQpIHtcbiAgICAgIHZhciBzY3JvbGxEaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblxuICAgICAgc2Nyb2xsRGl2LnN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJztcbiAgICAgIHNjcm9sbERpdi5zdHlsZS50b3AgPSAnLTk5OTlweCc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUud2lkdGggPSAnNTBweCc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUuaGVpZ2h0ID0gJzUwcHgnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLm92ZXJmbG93ID0gJ3Njcm9sbCc7XG5cbiAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc2Nyb2xsRGl2KTtcbiAgICAgIHNpemUgPSBzY3JvbGxEaXYub2Zmc2V0V2lkdGggLSBzY3JvbGxEaXYuY2xpZW50V2lkdGg7XG4gICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHNjcm9sbERpdik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHNpemU7XG59O1xuXG52YXIgX2luRE9NID0gcmVxdWlyZSgnLi9pbkRPTScpO1xuXG52YXIgX2luRE9NMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luRE9NKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNpemUgPSB2b2lkIDA7XG5cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1snZGVmYXVsdCddO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvdXRpbC9zY3JvbGxiYXJTaXplLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTIxLjk5IDRjMC0xLjEtLjg5LTItMS45OS0ySDRjLTEuMSAwLTIgLjktMiAydjEyYzAgMS4xLjkgMiAyIDJoMTRsNCA0LS4wMS0xOHpNMTggMTRINnYtMmgxMnYyem0wLTNINlY5aDEydjJ6bTAtM0g2VjZoMTJ2MnonIH0pO1xuXG52YXIgQ29tbWVudCA9IGZ1bmN0aW9uIENvbW1lbnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkNvbW1lbnQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKENvbW1lbnQpO1xuQ29tbWVudC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDb21tZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0NvbW1lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0NvbW1lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYgMTljMCAxLjEuOSAyIDIgMmg4YzEuMSAwIDItLjkgMi0yVjdINnYxMnpNMTkgNGgtMy41bC0xLTFoLTVsLTEgMUg1djJoMTRWNHonIH0pO1xuXG52YXIgRGVsZXRlID0gZnVuY3Rpb24gRGVsZXRlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5EZWxldGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKERlbGV0ZSk7XG5EZWxldGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRGVsZXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0RlbGV0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjYgMjcgMjggMzIgMzUgMzcgMzggNDAgNDEgNDIgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiAyMS4zNWwtMS40NS0xLjMyQzUuNCAxNS4zNiAyIDEyLjI4IDIgOC41IDIgNS40MiA0LjQyIDMgNy41IDNjMS43NCAwIDMuNDEuODEgNC41IDIuMDlDMTMuMDkgMy44MSAxNC43NiAzIDE2LjUgMyAxOS41OCAzIDIyIDUuNDIgMjIgOC41YzAgMy43OC0zLjQgNi44Ni04LjU1IDExLjU0TDEyIDIxLjM1eicgfSk7XG5cbnZhciBGYXZvcml0ZSA9IGZ1bmN0aW9uIEZhdm9yaXRlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5GYXZvcml0ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRmF2b3JpdGUpO1xuRmF2b3JpdGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRmF2b3JpdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zhdm9yaXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDAgNDEgNDIgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiA4YzEuMSAwIDItLjkgMi0ycy0uOS0yLTItMi0yIC45LTIgMiAuOSAyIDIgMnptMCAyYy0xLjEgMC0yIC45LTIgMnMuOSAyIDIgMiAyLS45IDItMi0uOS0yLTItMnptMCA2Yy0xLjEgMC0yIC45LTIgMnMuOSAyIDIgMiAyLS45IDItMi0uOS0yLTItMnonIH0pO1xuXG52YXIgTW9yZVZlcnQgPSBmdW5jdGlvbiBNb3JlVmVydChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuTW9yZVZlcnQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKE1vcmVWZXJ0KTtcbk1vcmVWZXJ0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IE1vcmVWZXJ0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMjcgMjggMzEgMzQgMzUgMzcgNDAgNDEgNDIgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiA0LjVDNyA0LjUgMi43MyA3LjYxIDEgMTJjMS43MyA0LjM5IDYgNy41IDExIDcuNXM5LjI3LTMuMTEgMTEtNy41Yy0xLjczLTQuMzktNi03LjUtMTEtNy41ek0xMiAxN2MtMi43NiAwLTUtMi4yNC01LTVzMi4yNC01IDUtNSA1IDIuMjQgNSA1LTIuMjQgNS01IDV6bTAtOGMtMS42NiAwLTMgMS4zNC0zIDNzMS4zNCAzIDMgMyAzLTEuMzQgMy0zLTEuMzQtMy0zLTN6JyB9KTtcblxudmFyIFJlbW92ZVJlZEV5ZSA9IGZ1bmN0aW9uIFJlbW92ZVJlZEV5ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuUmVtb3ZlUmVkRXllID0gKDAsIF9wdXJlMi5kZWZhdWx0KShSZW1vdmVSZWRFeWUpO1xuUmVtb3ZlUmVkRXllLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFJlbW92ZVJlZEV5ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9SZW1vdmVSZWRFeWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1JlbW92ZVJlZEV5ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM1IDM3IDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTggMTYuMDhjLS43NiAwLTEuNDQuMy0xLjk2Ljc3TDguOTEgMTIuN2MuMDUtLjIzLjA5LS40Ni4wOS0uN3MtLjA0LS40Ny0uMDktLjdsNy4wNS00LjExYy41NC41IDEuMjUuODEgMi4wNC44MSAxLjY2IDAgMy0xLjM0IDMtM3MtMS4zNC0zLTMtMy0zIDEuMzQtMyAzYzAgLjI0LjA0LjQ3LjA5LjdMOC4wNCA5LjgxQzcuNSA5LjMxIDYuNzkgOSA2IDljLTEuNjYgMC0zIDEuMzQtMyAzczEuMzQgMyAzIDNjLjc5IDAgMS41LS4zMSAyLjA0LS44MWw3LjEyIDQuMTZjLS4wNS4yMS0uMDguNDMtLjA4LjY1IDAgMS42MSAxLjMxIDIuOTIgMi45MiAyLjkyIDEuNjEgMCAyLjkyLTEuMzEgMi45Mi0yLjkycy0xLjMxLTIuOTItMi45Mi0yLjkyeicgfSk7XG5cbnZhciBTaGFyZSA9IGZ1bmN0aW9uIFNoYXJlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5TaGFyZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoU2hhcmUpO1xuU2hhcmUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gU2hhcmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NoYXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDAgNDEgNDIgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNyAzSDdjLTEuMSAwLTEuOTkuOS0xLjk5IDJMNSAyMWw3LTMgNyAzVjVjMC0xLjEtLjktMi0yLTJ6JyB9KTtcblxudmFyIFR1cm5lZEluID0gZnVuY3Rpb24gVHVybmVkSW4ocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblR1cm5lZEluID0gKDAsIF9wdXJlMi5kZWZhdWx0KShUdXJuZWRJbik7XG5UdXJuZWRJbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBUdXJuZWRJbjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UdXJuZWRJbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVHVybmVkSW4uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MCA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfY29sb3JNYW5pcHVsYXRvciA9IHJlcXVpcmUoJy4uL3N0eWxlcy9jb2xvck1hbmlwdWxhdG9yJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB3aWR0aDogNDAsXG4gICAgICBoZWlnaHQ6IDQwLFxuICAgICAgZm9udEZhbWlseTogdGhlbWUudHlwb2dyYXBoeS5mb250RmFtaWx5LFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyMCksXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckRlZmF1bHQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogKDAsIF9jb2xvck1hbmlwdWxhdG9yLmVtcGhhc2l6ZSkodGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsIDAuMjYpXG4gICAgfSxcbiAgICBpbWc6IHtcbiAgICAgIG1heFdpZHRoOiAnMTAwJScsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnYXV0bycsXG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZCBpbiBjb21iaW5hdGlvbiB3aXRoIGBzcmNgIG9yIGBzcmNTZXRgIHRvXG4gICAqIHByb3ZpZGUgYW4gYWx0IGF0dHJpYnV0ZSBmb3IgdGhlIHJlbmRlcmVkIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBhbHQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWQgdG8gcmVuZGVyIGljb24gb3IgdGV4dCBlbGVtZW50cyBpbnNpZGUgdGhlIEF2YXRhci5cbiAgICogYHNyY2AgYW5kIGBhbHRgIHByb3BzIHdpbGwgbm90IGJlIHVzZWQgYW5kIG5vIGBpbWdgIHdpbGxcbiAgICogYmUgcmVuZGVyZWQgYnkgZGVmYXVsdC5cbiAgICpcbiAgICogVGhpcyBjYW4gYmUgYW4gZWxlbWVudCwgb3IganVzdCBhIHN0cmluZy5cbiAgICovXG4gIGNoaWxkcmVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBUaGUgY2xhc3NOYW1lIG9mIHRoZSBjaGlsZCBlbGVtZW50LlxuICAgKiBVc2VkIGJ5IENoaXAgYW5kIExpc3RJdGVtSWNvbiB0byBzdHlsZSB0aGUgQXZhdGFyIGljb24uXG4gICAqL1xuICBjaGlsZHJlbkNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvcGVydGllcyBhcHBsaWVkIHRvIHRoZSBgaW1nYCBlbGVtZW50IHdoZW4gdGhlIGNvbXBvbmVudFxuICAgKiBpcyB1c2VkIHRvIGRpc3BsYXkgYW4gaW1hZ2UuXG4gICAqL1xuICBpbWdQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGBzaXplc2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNpemVzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyYzogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGBzcmNTZXRgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzcmNTZXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cbnZhciBBdmF0YXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShBdmF0YXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEF2YXRhcigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBBdmF0YXIpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChBdmF0YXIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEF2YXRhcikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQXZhdGFyLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFsdCA9IF9wcm9wcy5hbHQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcCA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjaGlsZHJlbkNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2hpbGRyZW5DbGFzc05hbWUsXG4gICAgICAgICAgQ29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgaW1nUHJvcHMgPSBfcHJvcHMuaW1nUHJvcHMsXG4gICAgICAgICAgc2l6ZXMgPSBfcHJvcHMuc2l6ZXMsXG4gICAgICAgICAgc3JjID0gX3Byb3BzLnNyYyxcbiAgICAgICAgICBzcmNTZXQgPSBfcHJvcHMuc3JjU2V0LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2FsdCcsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjaGlsZHJlbicsICdjaGlsZHJlbkNsYXNzTmFtZScsICdjb21wb25lbnQnLCAnaW1nUHJvcHMnLCAnc2l6ZXMnLCAnc3JjJywgJ3NyY1NldCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmNvbG9yRGVmYXVsdCwgY2hpbGRyZW5Qcm9wICYmICFzcmMgJiYgIXNyY1NldCksIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIGNoaWxkcmVuID0gbnVsbDtcblxuICAgICAgaWYgKGNoaWxkcmVuUHJvcCkge1xuICAgICAgICBpZiAoY2hpbGRyZW5DbGFzc05hbWVQcm9wICYmIHR5cGVvZiBjaGlsZHJlblByb3AgIT09ICdzdHJpbmcnICYmIF9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChjaGlsZHJlblByb3ApKSB7XG4gICAgICAgICAgdmFyIF9jaGlsZHJlbkNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2hpbGRyZW5DbGFzc05hbWVQcm9wLCBjaGlsZHJlblByb3AucHJvcHMuY2xhc3NOYW1lKTtcbiAgICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGRyZW5Qcm9wLCB7IGNsYXNzTmFtZTogX2NoaWxkcmVuQ2xhc3NOYW1lIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNoaWxkcmVuID0gY2hpbGRyZW5Qcm9wO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHNyYyB8fCBzcmNTZXQpIHtcbiAgICAgICAgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnaW1nJywgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgYWx0OiBhbHQsXG4gICAgICAgICAgc3JjOiBzcmMsXG4gICAgICAgICAgc3JjU2V0OiBzcmNTZXQsXG4gICAgICAgICAgc2l6ZXM6IHNpemVzLFxuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3Nlcy5pbWdcbiAgICAgICAgfSwgaW1nUHJvcHMpKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEF2YXRhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkF2YXRhci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbXBvbmVudDogJ2Rpdidcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQXZhdGFyJyB9KShBdmF0YXIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgNiAyNCAyNiAyNyAyOCAzMSAzMyAzNCAzNSAzNiAzNyAzOSA0MCA0MyA0OSA1NCA1NyA1OCA1OSA2MSA2MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9BdmF0YXIgPSByZXF1aXJlKCcuL0F2YXRhcicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BdmF0YXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA0IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuXG52YXIgUkFESVVTID0gMTI7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgZGlzcGxheTogJ2lubGluZS1mbGV4J1xuICAgIH0sXG4gICAgYmFkZ2U6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGZsZXhEaXJlY3Rpb246ICdyb3cnLFxuICAgICAgZmxleFdyYXA6ICd3cmFwJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGFsaWduQ29udGVudDogJ2NlbnRlcicsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgdG9wOiAtUkFESVVTLFxuICAgICAgcmlnaHQ6IC1SQURJVVMsXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250V2VpZ2h0OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRXZWlnaHQsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKFJBRElVUyksXG4gICAgICB3aWR0aDogUkFESVVTICogMixcbiAgICAgIGhlaWdodDogUkFESVVTICogMixcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuY29sb3IsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0Q29sb3IsXG4gICAgICB6SW5kZXg6IDEgLy8gUmVuZGVyIHRoZSBiYWRnZSBvbiB0b3Agb2YgcG90ZW50aWFsIHJpcHBsZXMuXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0sXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDApXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgcmVuZGVyZWQgd2l0aGluIHRoZSBiYWRnZS5cbiAgICovXG4gIGJhZGdlQ29udGVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBiYWRnZSB3aWxsIGJlIGFkZGVkIHJlbGF0aXZlIHRvIHRoaXMgbm9kZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdwcmltYXJ5JywgJ2FjY2VudCddKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgQmFkZ2UgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShCYWRnZSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQmFkZ2UoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQmFkZ2UpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChCYWRnZS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQmFkZ2UpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEJhZGdlLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJhZGdlQ29udGVudCA9IF9wcm9wcy5iYWRnZUNvbnRlbnQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2JhZGdlQ29udGVudCcsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICdjaGlsZHJlbiddKTtcblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWVQcm9wKTtcbiAgICAgIHZhciBiYWRnZUNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5iYWRnZSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnZGVmYXVsdCcpKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW4sXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogYmFkZ2VDbGFzc05hbWUgfSxcbiAgICAgICAgICBiYWRnZUNvbnRlbnRcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEJhZGdlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQmFkZ2UuZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ2RlZmF1bHQnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUJhZGdlJyB9KShCYWRnZSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvQmFkZ2UuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL0JhZGdlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfQmFkZ2UgPSByZXF1aXJlKCcuL0JhZGdlJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0JhZGdlKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9Nb2RhbCA9IHJlcXVpcmUoJy4uL01vZGFsJyk7XG5cbnZhciBfTW9kYWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTW9kYWwpO1xuXG52YXIgX0ZhZGUgPSByZXF1aXJlKCcuLi90cmFuc2l0aW9ucy9GYWRlJyk7XG5cbnZhciBfRmFkZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9GYWRlKTtcblxudmFyIF90cmFuc2l0aW9ucyA9IHJlcXVpcmUoJy4uL3N0eWxlcy90cmFuc2l0aW9ucycpO1xuXG52YXIgX1BhcGVyID0gcmVxdWlyZSgnLi4vUGFwZXInKTtcblxudmFyIF9QYXBlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9QYXBlcik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IE1vZGFsXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlID0gcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmM7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInXG4gICAgfSxcbiAgICBwYXBlcjoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgbWFyZ2luOiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA0LFxuICAgICAgZmxleERpcmVjdGlvbjogJ2NvbHVtbicsXG4gICAgICBmbGV4OiAnMCAxIGF1dG8nLFxuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBtYXhIZWlnaHQ6ICc5MHZoJyxcbiAgICAgIG92ZXJmbG93WTogJ2F1dG8nLCAvLyBGaXggSUUxMSBpc3N1ZSwgdG8gcmVtb3ZlIGF0IHNvbWUgcG9pbnQuXG4gICAgICAnJjpmb2N1cyc6IHtcbiAgICAgICAgb3V0bGluZTogJ25vbmUnXG4gICAgICB9XG4gICAgfSxcbiAgICBwYXBlcldpZHRoWHM6IHtcbiAgICAgIG1heFdpZHRoOiBNYXRoLm1heCh0aGVtZS5icmVha3BvaW50cy52YWx1ZXMueHMsIDM2MClcbiAgICB9LFxuICAgIHBhcGVyV2lkdGhTbToge1xuICAgICAgbWF4V2lkdGg6IHRoZW1lLmJyZWFrcG9pbnRzLnZhbHVlcy5zbVxuICAgIH0sXG4gICAgcGFwZXJXaWR0aE1kOiB7XG4gICAgICBtYXhXaWR0aDogdGhlbWUuYnJlYWtwb2ludHMudmFsdWVzLm1kXG4gICAgfSxcbiAgICBmdWxsV2lkdGg6IHtcbiAgICAgIHdpZHRoOiAnMTAwJSdcbiAgICB9LFxuICAgIGZ1bGxTY3JlZW46IHtcbiAgICAgIG1hcmdpbjogMCxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBtYXhXaWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnMTAwJScsXG4gICAgICBtYXhIZWlnaHQ6ICcxMDAlJyxcbiAgICAgIGJvcmRlclJhZGl1czogMFxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIERpYWxvZyBjaGlsZHJlbiwgdXN1YWxseSB0aGUgaW5jbHVkZWQgc3ViLWNvbXBvbmVudHMuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgaXQgd2lsbCBiZSBmdWxsLXNjcmVlblxuICAgKi9cbiAgZnVsbFNjcmVlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBjbGlja2luZyB0aGUgYmFja2Ryb3Agd2lsbCBub3QgZmlyZSB0aGUgYG9uUmVxdWVzdENsb3NlYCBjYWxsYmFjay5cbiAgICovXG4gIGlnbm9yZUJhY2tkcm9wQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgaGl0dGluZyBlc2NhcGUgd2lsbCBub3QgZmlyZSB0aGUgYG9uUmVxdWVzdENsb3NlYCBjYWxsYmFjay5cbiAgICovXG4gIGlnbm9yZUVzY2FwZUtleVVwOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgZHVyYXRpb24gZm9yIHRoZSB0cmFuc2l0aW9uLCBpbiBtaWxsaXNlY29uZHMuXG4gICAqIFlvdSBtYXkgc3BlY2lmeSBhIHNpbmdsZSB0aW1lb3V0IGZvciBhbGwgdHJhbnNpdGlvbnMsIG9yIGluZGl2aWR1YWxseSB3aXRoIGFuIG9iamVjdC5cbiAgICovXG4gIHRyYW5zaXRpb25EdXJhdGlvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmUgdGhlIG1heCB3aWR0aCBvZiB0aGUgZGlhbG9nLlxuICAgKiBUaGUgZGlhbG9nIHdpZHRoIGdyb3dzIHdpdGggdGhlIHNpemUgb2YgdGhlIHNjcmVlbiwgdGhpcyBwcm9wZXJ0eSBpcyB1c2VmdWxcbiAgICogb24gdGhlIGRlc2t0b3Agd2hlcmUgeW91IG1pZ2h0IG5lZWQgc29tZSBjb2hlcmVudCBkaWZmZXJlbnQgd2lkdGggc2l6ZSBhY3Jvc3MgeW91clxuICAgKiBhcHBsaWNhdGlvbi5cbiAgICovXG4gIG1heFdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWyd4cycsICdzbScsICdtZCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBzcGVjaWZpZWQsIHN0cmV0Y2hlcyBkaWFsb2cgdG8gbWF4IHdpZHRoLlxuICAgKi9cbiAgZnVsbFdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBiYWNrZHJvcCBpcyBjbGlja2VkLlxuICAgKi9cbiAgb25CYWNrZHJvcENsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBkaWFsb2cgZW50ZXJzLlxuICAgKi9cbiAgb25FbnRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBkaWFsb2cgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGRpYWxvZyBoYXMgZW50ZXJlZC5cbiAgICovXG4gIG9uRW50ZXJlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlcyB3aGVuIHRoZSBlc2NhcGUga2V5IGlzIHJlbGVhc2VkIGFuZCB0aGUgbW9kYWwgaXMgaW4gZm9jdXMuXG4gICAqL1xuICBvbkVzY2FwZUtleVVwOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBkaWFsb2cgZXhpdHMuXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgZGlhbG9nIGlzIGV4aXRpbmcuXG4gICAqL1xuICBvbkV4aXRpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgZGlhbG9nIGhhcyBleGl0ZWQuXG4gICAqL1xuICBvbkV4aXRlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBjb21wb25lbnQgcmVxdWVzdHMgdG8gYmUgY2xvc2VkLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2tcbiAgICovXG4gIG9uUmVxdWVzdENsb3NlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgRGlhbG9nIGlzIG9wZW4uXG4gICAqL1xuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUcmFuc2l0aW9uIGNvbXBvbmVudC5cbiAgICovXG4gIHRyYW5zaXRpb246IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlKS5pc1JlcXVpcmVkXG59O1xuXG4vKipcbiAqIERpYWxvZ3MgYXJlIG92ZXJsYWlkIG1vZGFsIHBhcGVyIGJhc2VkIGNvbXBvbmVudHMgd2l0aCBhIGJhY2tkcm9wLlxuICovXG52YXIgRGlhbG9nID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoRGlhbG9nLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBEaWFsb2coKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgRGlhbG9nKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoRGlhbG9nLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShEaWFsb2cpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKERpYWxvZywgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGZ1bGxTY3JlZW4gPSBfcHJvcHMuZnVsbFNjcmVlbixcbiAgICAgICAgICBpZ25vcmVCYWNrZHJvcENsaWNrID0gX3Byb3BzLmlnbm9yZUJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgaWdub3JlRXNjYXBlS2V5VXAgPSBfcHJvcHMuaWdub3JlRXNjYXBlS2V5VXAsXG4gICAgICAgICAgdHJhbnNpdGlvbkR1cmF0aW9uID0gX3Byb3BzLnRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgICBtYXhXaWR0aCA9IF9wcm9wcy5tYXhXaWR0aCxcbiAgICAgICAgICBmdWxsV2lkdGggPSBfcHJvcHMuZnVsbFdpZHRoLFxuICAgICAgICAgIG9wZW4gPSBfcHJvcHMub3BlbixcbiAgICAgICAgICBvbkJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMub25CYWNrZHJvcENsaWNrLFxuICAgICAgICAgIG9uRXNjYXBlS2V5VXAgPSBfcHJvcHMub25Fc2NhcGVLZXlVcCxcbiAgICAgICAgICBvbkVudGVyID0gX3Byb3BzLm9uRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZyA9IF9wcm9wcy5vbkVudGVyaW5nLFxuICAgICAgICAgIG9uRW50ZXJlZCA9IF9wcm9wcy5vbkVudGVyZWQsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzLm9uRXhpdCxcbiAgICAgICAgICBvbkV4aXRpbmcgPSBfcHJvcHMub25FeGl0aW5nLFxuICAgICAgICAgIG9uRXhpdGVkID0gX3Byb3BzLm9uRXhpdGVkLFxuICAgICAgICAgIG9uUmVxdWVzdENsb3NlID0gX3Byb3BzLm9uUmVxdWVzdENsb3NlLFxuICAgICAgICAgIFRyYW5zaXRpb25Qcm9wID0gX3Byb3BzLnRyYW5zaXRpb24sXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnZnVsbFNjcmVlbicsICdpZ25vcmVCYWNrZHJvcENsaWNrJywgJ2lnbm9yZUVzY2FwZUtleVVwJywgJ3RyYW5zaXRpb25EdXJhdGlvbicsICdtYXhXaWR0aCcsICdmdWxsV2lkdGgnLCAnb3BlbicsICdvbkJhY2tkcm9wQ2xpY2snLCAnb25Fc2NhcGVLZXlVcCcsICdvbkVudGVyJywgJ29uRW50ZXJpbmcnLCAnb25FbnRlcmVkJywgJ29uRXhpdCcsICdvbkV4aXRpbmcnLCAnb25FeGl0ZWQnLCAnb25SZXF1ZXN0Q2xvc2UnLCAndHJhbnNpdGlvbiddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9Nb2RhbDIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSxcbiAgICAgICAgICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbjogdHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgIGlnbm9yZUJhY2tkcm9wQ2xpY2s6IGlnbm9yZUJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgaWdub3JlRXNjYXBlS2V5VXA6IGlnbm9yZUVzY2FwZUtleVVwLFxuICAgICAgICAgIG9uQmFja2Ryb3BDbGljazogb25CYWNrZHJvcENsaWNrLFxuICAgICAgICAgIG9uRXNjYXBlS2V5VXA6IG9uRXNjYXBlS2V5VXAsXG4gICAgICAgICAgb25SZXF1ZXN0Q2xvc2U6IG9uUmVxdWVzdENsb3NlLFxuICAgICAgICAgIHNob3c6IG9wZW5cbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBUcmFuc2l0aW9uUHJvcCxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBhcHBlYXI6IHRydWUsXG4gICAgICAgICAgICAnaW4nOiBvcGVuLFxuICAgICAgICAgICAgdGltZW91dDogdHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgICAgb25FbnRlcjogb25FbnRlcixcbiAgICAgICAgICAgIG9uRW50ZXJpbmc6IG9uRW50ZXJpbmcsXG4gICAgICAgICAgICBvbkVudGVyZWQ6IG9uRW50ZXJlZCxcbiAgICAgICAgICAgIG9uRXhpdDogb25FeGl0LFxuICAgICAgICAgICAgb25FeGl0aW5nOiBvbkV4aXRpbmcsXG4gICAgICAgICAgICBvbkV4aXRlZDogb25FeGl0ZWRcbiAgICAgICAgICB9LFxuICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgX1BhcGVyMi5kZWZhdWx0LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBlbGV2YXRpb246IDI0LFxuICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5wYXBlciwgY2xhc3Nlc1sncGFwZXJXaWR0aCcgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShtYXhXaWR0aCldLCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZnVsbFNjcmVlbiwgZnVsbFNjcmVlbiksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmZ1bGxXaWR0aCwgZnVsbFdpZHRoKSwgX2NsYXNzTmFtZXMpKVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKVxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gRGlhbG9nO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuRGlhbG9nLmRlZmF1bHRQcm9wcyA9IHtcbiAgZnVsbFNjcmVlbjogZmFsc2UsXG4gIGlnbm9yZUJhY2tkcm9wQ2xpY2s6IGZhbHNlLFxuICBpZ25vcmVFc2NhcGVLZXlVcDogZmFsc2UsXG4gIHRyYW5zaXRpb25EdXJhdGlvbjoge1xuICAgIGVudGVyOiBfdHJhbnNpdGlvbnMuZHVyYXRpb24uZW50ZXJpbmdTY3JlZW4sXG4gICAgZXhpdDogX3RyYW5zaXRpb25zLmR1cmF0aW9uLmxlYXZpbmdTY3JlZW5cbiAgfSxcbiAgbWF4V2lkdGg6ICdzbScsXG4gIGZ1bGxXaWR0aDogZmFsc2UsXG4gIG9wZW46IGZhbHNlLFxuICB0cmFuc2l0aW9uOiBfRmFkZTIuZGVmYXVsdFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlEaWFsb2cnIH0pKERpYWxvZyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNSAyNyAyOCAzNSAzNyA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfcmVmO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnJlcXVpcmUoJy4uL0J1dHRvbicpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG4vLyBTbyB3ZSBkb24ndCBoYXZlIGFueSBvdmVycmlkZSBwcmlvcml0eSBpc3N1ZS5cblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdmbGV4LWVuZCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIG1hcmdpbjogdGhlbWUuc3BhY2luZy51bml0ICsgJ3B4ICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgLyAyICsgJ3B4JyxcbiAgICAgIGZsZXg6ICcwIDAgYXV0bydcbiAgICB9LFxuICAgIGFjdGlvbjoge1xuICAgICAgbWFyZ2luOiAnMCAnICsgdGhlbWUuc3BhY2luZy51bml0IC8gMiArICdweCdcbiAgICB9LFxuICAgIGJ1dHRvbjoge1xuICAgICAgbWluV2lkdGg6IDY0XG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxuXG5mdW5jdGlvbiBEaWFsb2dBY3Rpb25zKHByb3BzKSB7XG4gIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWUgPSBwcm9wcy5jbGFzc05hbWUsXG4gICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKHByb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJ10pO1xuXG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICdkaXYnLFxuICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWUpIH0sIG90aGVyKSxcbiAgICBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ubWFwKGNoaWxkcmVuLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgIGlmICghX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkKSkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuYWN0aW9uIH0sXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5idXR0b24sIGNoaWxkLnByb3BzLmNsYXNzTmFtZSlcbiAgICAgICAgfSlcbiAgICAgICk7XG4gICAgfSlcbiAgKTtcbn1cblxuRGlhbG9nQWN0aW9ucy5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoX3JlZiA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICB0aGVtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKVxufSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzZXMnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0KSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzTmFtZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcpLCBfcmVmKSA6IHt9O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aURpYWxvZ0FjdGlvbnMnIH0pKERpYWxvZ0FjdGlvbnMpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dBY3Rpb25zLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nQWN0aW9ucy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNSAyNyAyOCAzNSAzNyA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfcmVmO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICB2YXIgc3BhY2luZyA9IHRoZW1lLnNwYWNpbmcudW5pdCAqIDM7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZmxleDogJzEgMSBhdXRvJyxcbiAgICAgIG92ZXJmbG93WTogJ2F1dG8nLFxuICAgICAgcGFkZGluZzogJzAgJyArIHNwYWNpbmcgKyAncHggJyArIHNwYWNpbmcgKyAncHggJyArIHNwYWNpbmcgKyAncHgnLFxuICAgICAgJyY6Zmlyc3QtY2hpbGQnOiB7XG4gICAgICAgIHBhZGRpbmdUb3A6IHNwYWNpbmdcbiAgICAgIH1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIERpYWxvZ0NvbnRlbnQocHJvcHMpIHtcbiAgdmFyIGNsYXNzZXMgPSBwcm9wcy5jbGFzc2VzLFxuICAgICAgY2hpbGRyZW4gPSBwcm9wcy5jaGlsZHJlbixcbiAgICAgIGNsYXNzTmFtZSA9IHByb3BzLmNsYXNzTmFtZSxcbiAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkocHJvcHMsIFsnY2xhc3NlcycsICdjaGlsZHJlbicsICdjbGFzc05hbWUnXSk7XG5cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgJ2RpdicsXG4gICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSkgfSwgb3RoZXIpLFxuICAgIGNoaWxkcmVuXG4gICk7XG59XG5cbkRpYWxvZ0NvbnRlbnQucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKF9yZWYgPSB7XG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKVxufSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzZXMnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0KSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzTmFtZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcpLCBfcmVmKSA6IHt9O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aURpYWxvZ0NvbnRlbnQnIH0pKERpYWxvZ0NvbnRlbnQpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nQ29udGVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNSAyNyAyOCAzNSAzNyA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfcmVmO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6ICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe30sIHRoZW1lLnR5cG9ncmFwaHkuc3ViaGVhZGluZywge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5zZWNvbmRhcnksXG4gICAgICBtYXJnaW46IDBcbiAgICB9KVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxuXG5mdW5jdGlvbiBEaWFsb2dDb250ZW50VGV4dChwcm9wcykge1xuICB2YXIgY2hpbGRyZW4gPSBwcm9wcy5jaGlsZHJlbixcbiAgICAgIGNsYXNzZXMgPSBwcm9wcy5jbGFzc2VzLFxuICAgICAgY2xhc3NOYW1lID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShwcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZSddKTtcblxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAncCcsXG4gICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSkgfSwgb3RoZXIpLFxuICAgIGNoaWxkcmVuXG4gICk7XG59XG5cbkRpYWxvZ0NvbnRlbnRUZXh0LnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IChfcmVmID0ge1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHRoZW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpRGlhbG9nQ29udGVudFRleHQnIH0pKERpYWxvZ0NvbnRlbnRUZXh0KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nQ29udGVudFRleHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50VGV4dC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNSAyNyAyOCAzNSAzNyA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX1R5cG9ncmFwaHkgPSByZXF1aXJlKCcuLi9UeXBvZ3JhcGh5Jyk7XG5cbnZhciBfVHlwb2dyYXBoeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UeXBvZ3JhcGh5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgbWFyZ2luOiAwLFxuICAgICAgcGFkZGluZzogdGhlbWUuc3BhY2luZy51bml0ICogMyArICdweCAnICsgdGhlbWUuc3BhY2luZy51bml0ICogMyArICdweCAgICAgICAyMHB4ICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzICsgJ3B4JyxcbiAgICAgIGZsZXg6ICcwIDAgYXV0bydcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjaGlsZHJlbiB3b24ndCBiZSB3cmFwcGVkIGJ5IGEgdHlwb2dyYXBoeSBjb21wb25lbnQuXG4gICAqIEZvciBpbnN0YW5jZSwgdGhpcyBjYW4gYmUgdXNlZnVsIHRvIHJlbmRlciBhbiBoNCBpbnN0ZWFkIG9mIHRoZSBkZWZhdWx0IGgyLlxuICAgKi9cbiAgZGlzYWJsZVR5cG9ncmFwaHk6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWRcbn07XG5cbnZhciBEaWFsb2dUaXRsZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKERpYWxvZ1RpdGxlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBEaWFsb2dUaXRsZSgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBEaWFsb2dUaXRsZSk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKERpYWxvZ1RpdGxlLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShEaWFsb2dUaXRsZSkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoRGlhbG9nVGl0bGUsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZGlzYWJsZVR5cG9ncmFwaHkgPSBfcHJvcHMuZGlzYWJsZVR5cG9ncmFwaHksXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnZGlzYWJsZVR5cG9ncmFwaHknXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSkgfSwgb3RoZXIpLFxuICAgICAgICBkaXNhYmxlVHlwb2dyYXBoeSA/IGNoaWxkcmVuIDogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgX1R5cG9ncmFwaHkyLmRlZmF1bHQsXG4gICAgICAgICAgeyB0eXBlOiAndGl0bGUnIH0sXG4gICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIERpYWxvZ1RpdGxlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuRGlhbG9nVGl0bGUuZGVmYXVsdFByb3BzID0ge1xuICBkaXNhYmxlVHlwb2dyYXBoeTogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpRGlhbG9nVGl0bGUnIH0pKERpYWxvZ1RpdGxlKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nVGl0bGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dUaXRsZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNSAyNyAyOCAzNSAzNyA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9EaWFsb2cgPSByZXF1aXJlKCcuL0RpYWxvZycpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9EaWFsb2cpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX0RpYWxvZ0FjdGlvbnMgPSByZXF1aXJlKCcuL0RpYWxvZ0FjdGlvbnMnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdEaWFsb2dBY3Rpb25zJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRGlhbG9nQWN0aW9ucykuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfRGlhbG9nVGl0bGUgPSByZXF1aXJlKCcuL0RpYWxvZ1RpdGxlJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnRGlhbG9nVGl0bGUnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9EaWFsb2dUaXRsZSkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfRGlhbG9nQ29udGVudCA9IHJlcXVpcmUoJy4vRGlhbG9nQ29udGVudCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0RpYWxvZ0NvbnRlbnQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9EaWFsb2dDb250ZW50KS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9EaWFsb2dDb250ZW50VGV4dCA9IHJlcXVpcmUoJy4vRGlhbG9nQ29udGVudFRleHQnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdEaWFsb2dDb250ZW50VGV4dCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZ0NvbnRlbnRUZXh0KS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF93aXRoTW9iaWxlRGlhbG9nID0gcmVxdWlyZSgnLi93aXRoTW9iaWxlRGlhbG9nJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnd2l0aE1vYmlsZURpYWxvZycsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhNb2JpbGVEaWFsb2cpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxudmFyIF93aXRoV2lkdGggPSByZXF1aXJlKCcuLi91dGlscy93aXRoV2lkdGgnKTtcblxudmFyIF93aXRoV2lkdGgyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFdpZHRoKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0hpZ2hlck9yZGVyQ29tcG9uZW50ID0gcmVxdWlyZSgncmVhY3QtZmxvdy10eXBlcycpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0hpZ2hlck9yZGVyQ29tcG9uZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50ID0gcmVxdWlyZSgnLi4vc3R5bGVzL2NyZWF0ZUJyZWFrcG9pbnRzJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQnJlYWtwb2ludCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSW5qZWN0ZWRQcm9wcyA9IHtcbiAgLyoqXG4gICAqIElmIGlzV2lkdGhEb3duKG9wdGlvbnMuYnJlYWtwb2ludCksIHJldHVybiB0cnVlLlxuICAgKi9cbiAgZnVsbFNjcmVlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2xcbn07XG5cblxuLyoqXG4gKiBEaWFsb2cgd2lsbCByZXNwb25zaXZlbHkgYmUgZnVsbCBzY3JlZW4gKmF0IG9yIGJlbG93KiB0aGUgZ2l2ZW4gYnJlYWtwb2ludFxuICogKGRlZmF1bHRzIHRvICdzbScgZm9yIG1vYmlsZSBkZXZpY2VzKS5cbiAqIE5vdGljZSB0aGF0IHRoaXMgSGlnaGVyLW9yZGVyIENvbXBvbmVudCBpcyBpbmNvbXBhdGlibGUgd2l0aCBzZXJ2ZXIgc2lkZSByZW5kZXJpbmcuXG4gKi9cbnZhciB3aXRoTW9iaWxlRGlhbG9nID0gZnVuY3Rpb24gd2l0aE1vYmlsZURpYWxvZygpIHtcbiAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHsgYnJlYWtwb2ludDogJ3NtJyB9O1xuICByZXR1cm4gZnVuY3Rpb24gKENvbXBvbmVudCkge1xuICAgIHZhciBicmVha3BvaW50ID0gb3B0aW9ucy5icmVha3BvaW50O1xuXG5cbiAgICBmdW5jdGlvbiBXaXRoTW9iaWxlRGlhbG9nKHByb3BzKSB7XG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgZnVsbFNjcmVlbjogKDAsIF93aXRoV2lkdGguaXNXaWR0aERvd24pKGJyZWFrcG9pbnQsIHByb3BzLndpZHRoKSB9LCBwcm9wcykpO1xuICAgIH1cblxuICAgIFdpdGhNb2JpbGVEaWFsb2cucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8ge1xuICAgICAgd2lkdGg6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQnJlYWtwb2ludC5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQnJlYWtwb2ludC5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQnJlYWtwb2ludCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50KS5pc1JlcXVpcmVkXG4gICAgfSA6IHt9O1xuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICBXaXRoTW9iaWxlRGlhbG9nLmRpc3BsYXlOYW1lID0gKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKENvbXBvbmVudCwgJ3dpdGhNb2JpbGVEaWFsb2cnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gKDAsIF93aXRoV2lkdGgyLmRlZmF1bHQpKCkoV2l0aE1vYmlsZURpYWxvZyk7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3aXRoTW9iaWxlRGlhbG9nO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy93aXRoTW9iaWxlRGlhbG9nLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvd2l0aE1vYmlsZURpYWxvZy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNSAyNyAyOCAzNSAzNyA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQnV0dG9uQmFzZSA9IHJlcXVpcmUoJy4uL0J1dHRvbkJhc2UnKTtcblxudmFyIF9CdXR0b25CYXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0J1dHRvbkJhc2UpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4uL0ljb24nKTtcblxudmFyIF9JY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pO1xuXG5yZXF1aXJlKCcuLi9TdmdJY29uJyk7XG5cbnZhciBfcmVhY3RIZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvcmVhY3RIZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7IC8vICB3ZWFrXG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IEJ1dHRvbkJhc2VcblxuLy8gRW5zdXJlIENTUyBzcGVjaWZpY2l0eVxuXG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gICAgICBmbGV4OiAnMCAwIGF1dG8nLFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyNCksXG4gICAgICB3aWR0aDogdGhlbWUuc3BhY2luZy51bml0ICogNixcbiAgICAgIGhlaWdodDogdGhlbWUuc3BhY2luZy51bml0ICogNixcbiAgICAgIHBhZGRpbmc6IDAsXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZSxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnYmFja2dyb3VuZC1jb2xvcicsIHtcbiAgICAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXN0XG4gICAgICB9KVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH0sXG4gICAgY29sb3JJbmhlcml0OiB7XG4gICAgICBjb2xvcjogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBkaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBsYWJlbDoge1xuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdpbmhlcml0JyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnaW5oZXJpdCdcbiAgICB9LFxuICAgIGljb246IHtcbiAgICAgIHdpZHRoOiAnMWVtJyxcbiAgICAgIGhlaWdodDogJzFlbSdcbiAgICB9LFxuICAgIGtleWJvYXJkRm9jdXNlZDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlclxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIG5hdGl2ZSBidXR0b24gY29tcG9uZW50LlxuICAgKi9cbiAgYnV0dG9uUmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogVGhlIGljb24gZWxlbWVudC5cbiAgICogSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhbiBpY29uIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydkZWZhdWx0JywgJ2luaGVyaXQnLCAncHJpbWFyeScsICdjb250cmFzdCcsICdhY2NlbnQnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYnV0dG9uIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgcmlwcGxlIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlUmlwcGxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSByb290IGNvbXBvbmVudC5cbiAgICovXG4gIHJvb3RSZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jXG59O1xuXG4vKipcbiAqIFJlZmVyIHRvIHRoZSBbSWNvbnNdKC9zdHlsZS9pY29ucykgc2VjdGlvbiBvZiB0aGUgZG9jdW1lbnRhdGlvblxuICogcmVnYXJkaW5nIHRoZSBhdmFpbGFibGUgaWNvbiBvcHRpb25zLlxuICovXG52YXIgSWNvbkJ1dHRvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEljb25CdXR0b24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEljb25CdXR0b24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgSWNvbkJ1dHRvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEljb25CdXR0b24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEljb25CdXR0b24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEljb25CdXR0b24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBidXR0b25SZWYgPSBfcHJvcHMuYnV0dG9uUmVmLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIGRpc2FibGVkID0gX3Byb3BzLmRpc2FibGVkLFxuICAgICAgICAgIHJvb3RSZWYgPSBfcHJvcHMucm9vdFJlZixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydidXR0b25SZWYnLCAnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAnZGlzYWJsZWQnLCAncm9vdFJlZiddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9CdXR0b25CYXNlMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnZGVmYXVsdCcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5kaXNhYmxlZCwgZGlzYWJsZWQpLCBfY2xhc3NOYW1lcyksIGNsYXNzTmFtZSksXG4gICAgICAgICAgY2VudGVyUmlwcGxlOiB0cnVlLFxuICAgICAgICAgIGtleWJvYXJkRm9jdXNlZENsYXNzTmFtZTogY2xhc3Nlcy5rZXlib2FyZEZvY3VzZWQsXG4gICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVkXG4gICAgICAgIH0sIG90aGVyLCB7XG4gICAgICAgICAgcm9vdFJlZjogYnV0dG9uUmVmLFxuICAgICAgICAgIHJlZjogcm9vdFJlZlxuICAgICAgICB9KSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmxhYmVsIH0sXG4gICAgICAgICAgdHlwZW9mIGNoaWxkcmVuID09PSAnc3RyaW5nJyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgX0ljb24yLmRlZmF1bHQsXG4gICAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5pY29uIH0sXG4gICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICkgOiBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ubWFwKGNoaWxkcmVuLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICAgIGlmICgoMCwgX3JlYWN0SGVscGVycy5pc011aUVsZW1lbnQpKGNoaWxkLCBbJ0ljb24nLCAnU3ZnSWNvbiddKSkge1xuICAgICAgICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmljb24sIGNoaWxkLnByb3BzLmNsYXNzTmFtZSlcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBjaGlsZDtcbiAgICAgICAgICB9KVxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gSWNvbkJ1dHRvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkljb25CdXR0b24uZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ2RlZmF1bHQnLFxuICBkaXNhYmxlZDogZmFsc2UsXG4gIGRpc2FibGVSaXBwbGU6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUljb25CdXR0b24nIH0pKEljb25CdXR0b24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAzIDYgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzcgMzggMzkgNDAgNDQgNDUgNDYgNDkgNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfSWNvbkJ1dHRvbiA9IHJlcXVpcmUoJy4vSWNvbkJ1dHRvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uQnV0dG9uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAzIDYgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzcgMzggMzkgNDAgNDQgNDUgNDYgNDkgNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB6SW5kZXg6IC0xLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJzEwMCUnLFxuICAgICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAwLFxuICAgICAgLy8gUmVtb3ZlIGdyZXkgaGlnaGxpZ2h0XG4gICAgICBXZWJraXRUYXBIaWdobGlnaHRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb21tb24udHJhbnNwYXJlbnQsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuY29tbW9uLmxpZ2h0QmxhY2ssXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknKSxcbiAgICAgIHdpbGxDaGFuZ2U6ICdvcGFjaXR5JyxcbiAgICAgIG9wYWNpdHk6IDBcbiAgICB9LFxuICAgIGludmlzaWJsZToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLmNvbW1vbi50cmFuc3BhcmVudFxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIENhbiBiZSB1c2VkLCBmb3IgaW5zdGFuY2UsIHRvIHJlbmRlciBhIGxldHRlciBpbnNpZGUgdGhlIGF2YXRhci5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYmFja2Ryb3AgaXMgaW52aXNpYmxlLlxuICAgKi9cbiAgaW52aXNpYmxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkXG59O1xuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBCYWNrZHJvcCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEJhY2tkcm9wLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBCYWNrZHJvcCgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBCYWNrZHJvcCk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEJhY2tkcm9wLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShCYWNrZHJvcCkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQmFja2Ryb3AsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgaW52aXNpYmxlID0gX3Byb3BzLmludmlzaWJsZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdpbnZpc2libGUnXSk7XG5cblxuICAgICAgdmFyIGJhY2tkcm9wQ2xhc3MgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuaW52aXNpYmxlLCBpbnZpc2libGUpLCBjbGFzc05hbWUpO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBiYWNrZHJvcENsYXNzLCAnYXJpYS1oaWRkZW4nOiAndHJ1ZScgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEJhY2tkcm9wO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQmFja2Ryb3AuZGVmYXVsdFByb3BzID0ge1xuICBpbnZpc2libGU6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUJhY2tkcm9wJyB9KShCYWNrZHJvcCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvQmFja2Ryb3AuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL0JhY2tkcm9wLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfa2V5cyA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3Qva2V5cycpO1xuXG52YXIgX2tleXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfa2V5cyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9yZWFjdERvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdERvbSk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dhcm5pbmcgPSByZXF1aXJlKCd3YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxudmFyIF9rZXljb2RlID0gcmVxdWlyZSgna2V5Y29kZScpO1xuXG52YXIgX2tleWNvZGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfa2V5Y29kZSk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG52YXIgX2NvbnRhaW5zID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvcXVlcnkvY29udGFpbnMnKTtcblxudmFyIF9jb250YWluczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb250YWlucyk7XG5cbnZhciBfYWN0aXZlRWxlbWVudCA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQnKTtcblxudmFyIF9hY3RpdmVFbGVtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2FjdGl2ZUVsZW1lbnQpO1xuXG52YXIgX293bmVyRG9jdW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxudmFyIF9hZGRFdmVudExpc3RlbmVyID0gcmVxdWlyZSgnLi4vdXRpbHMvYWRkRXZlbnRMaXN0ZW5lcicpO1xuXG52YXIgX2FkZEV2ZW50TGlzdGVuZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfYWRkRXZlbnRMaXN0ZW5lcik7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9GYWRlID0gcmVxdWlyZSgnLi4vdHJhbnNpdGlvbnMvRmFkZScpO1xuXG52YXIgX0ZhZGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRmFkZSk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9tb2RhbE1hbmFnZXIgPSByZXF1aXJlKCcuL21vZGFsTWFuYWdlcicpO1xuXG52YXIgX21vZGFsTWFuYWdlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9tb2RhbE1hbmFnZXIpO1xuXG52YXIgX0JhY2tkcm9wID0gcmVxdWlyZSgnLi9CYWNrZHJvcCcpO1xuXG52YXIgX0JhY2tkcm9wMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0JhY2tkcm9wKTtcblxudmFyIF9Qb3J0YWwgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC9Qb3J0YWwnKTtcblxudmFyIF9Qb3J0YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUG9ydGFsKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG4vLyBNb2RhbHMgZG9uJ3Qgb3BlbiBvbiB0aGUgc2VydmVyIHNvIHRoaXMgd29uJ3QgYnJlYWsgY29uY3VycmVuY3kuXG4vLyBDb3VsZCBhbHNvIHB1dCB0aGlzIG9uIGNvbnRleHQuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgbW9kYWxNYW5hZ2VyID0gKDAsIF9tb2RhbE1hbmFnZXIyLmRlZmF1bHQpKCk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgICAgekluZGV4OiB0aGVtZS56SW5kZXguZGlhbG9nLFxuICAgICAgdG9wOiAwLFxuICAgICAgbGVmdDogMFxuICAgIH0sXG4gICAgaGlkZGVuOiB7XG4gICAgICB2aXNpYmlsaXR5OiAnaGlkZGVuJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBDU1MgY2xhc3MgbmFtZSBvZiB0aGUgYmFja2Ryb3AgZWxlbWVudC5cbiAgICovXG4gIEJhY2tkcm9wQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBQYXNzIGEgY29tcG9uZW50IGNsYXNzIHRvIHVzZSBhcyB0aGUgYmFja2Ryb3AuXG4gICAqL1xuICBCYWNrZHJvcENvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYmFja2Ryb3AgaXMgaW52aXNpYmxlLlxuICAgKi9cbiAgQmFja2Ryb3BJbnZpc2libGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIGJhY2tkcm9wIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb246IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbikuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQSBzaW5nbGUgY2hpbGQgY29udGVudCBlbGVtZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBBbHdheXMga2VlcCB0aGUgY2hpbGRyZW4gaW4gdGhlIERPTS5cbiAgICogVGhpcyBwcm9wZXJ0eSBjYW4gYmUgdXNlZnVsIGluIFNFTyBzaXR1YXRpb24gb3JcbiAgICogd2hlbiB5b3Ugd2FudCB0byBtYXhpbWl6ZSB0aGUgcmVzcG9uc2l2ZW5lc3Mgb2YgdGhlIE1vZGFsLlxuICAgKi9cbiAga2VlcE1vdW50ZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJhY2tkcm9wIGlzIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZUJhY2tkcm9wOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGNsaWNraW5nIHRoZSBiYWNrZHJvcCB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlQmFja2Ryb3BDbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBoaXR0aW5nIGVzY2FwZSB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG1vZGFsTWFuYWdlcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlcyB3aGVuIHRoZSBiYWNrZHJvcCBpcyBjbGlja2VkIG9uLlxuICAgKi9cbiAgb25CYWNrZHJvcENsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBtb2RhbCBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGhhcyBlbnRlcmVkLlxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVzIHdoZW4gdGhlIGVzY2FwZSBrZXkgaXMgcHJlc3NlZCBhbmQgdGhlIG1vZGFsIGlzIGluIGZvY3VzLlxuICAgKi9cbiAgb25Fc2NhcGVLZXlVcDogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIGJlZm9yZSB0aGUgbW9kYWwgaXMgZXhpdGluZy5cbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBtb2RhbCBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0aW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGhhcyBleGl0ZWQuXG4gICAqL1xuICBvbkV4aXRlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBjb21wb25lbnQgcmVxdWVzdHMgdG8gYmUgY2xvc2VkLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2tcbiAgICovXG4gIG9uUmVxdWVzdENsb3NlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgTW9kYWwgaXMgdmlzaWJsZS5cbiAgICovXG4gIHNob3c6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogVGhlIG1vZGFsIGNvbXBvbmVudCBwcm92aWRlcyBhIHNvbGlkIGZvdW5kYXRpb24gZm9yIGNyZWF0aW5nIGRpYWxvZ3MsXG4gKiBwb3BvdmVycywgb3Igd2hhdGV2ZXIgZWxzZS5cbiAqIFRoZSBjb21wb25lbnQgcmVuZGVycyBpdHMgYGNoaWxkcmVuYCBub2RlIGluIGZyb250IG9mIGEgYmFja2Ryb3AgY29tcG9uZW50LlxuICpcbiAqIFRoZSBgTW9kYWxgIG9mZmVycyBhIGZldyBoZWxwZnVsIGZlYXR1cmVzIG92ZXIgdXNpbmcganVzdCBhIGBQb3J0YWxgIGNvbXBvbmVudCBhbmQgc29tZSBzdHlsZXM6XG4gKiAtIE1hbmFnZXMgZGlhbG9nIHN0YWNraW5nIHdoZW4gb25lLWF0LWEtdGltZSBqdXN0IGlzbid0IGVub3VnaC5cbiAqIC0gQ3JlYXRlcyBhIGJhY2tkcm9wLCBmb3IgZGlzYWJsaW5nIGludGVyYWN0aW9uIGJlbG93IHRoZSBtb2RhbC5cbiAqIC0gSXQgcHJvcGVybHkgbWFuYWdlcyBmb2N1czsgbW92aW5nIHRvIHRoZSBtb2RhbCBjb250ZW50LFxuICogICBhbmQga2VlcGluZyBpdCB0aGVyZSB1bnRpbCB0aGUgbW9kYWwgaXMgY2xvc2VkLlxuICogLSBJdCBkaXNhYmxlcyBzY3JvbGxpbmcgb2YgdGhlIHBhZ2UgY29udGVudCB3aGlsZSBvcGVuLlxuICogLSBBZGRzIHRoZSBhcHByb3ByaWF0ZSBBUklBIHJvbGVzIGFyZSBhdXRvbWF0aWNhbGx5LlxuICpcbiAqIFRoaXMgY29tcG9uZW50IHNoYXJlcyBtYW55IGNvbmNlcHRzIHdpdGggW3JlYWN0LW92ZXJsYXlzXShodHRwczovL3JlYWN0LWJvb3RzdHJhcC5naXRodWIuaW8vcmVhY3Qtb3ZlcmxheXMvI21vZGFscykuXG4gKi9cbnZhciBNb2RhbCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKE1vZGFsLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBNb2RhbCgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBNb2RhbCk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBNb2RhbC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoTW9kYWwpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfaW5pdGlhbGlzZVByb3BzLmNhbGwoX3RoaXMpLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoTW9kYWwsIFt7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgICAgaWYgKCF0aGlzLnByb3BzLnNob3cpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGV4aXRlZDogdHJ1ZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdGhpcy5tb3VudGVkID0gdHJ1ZTtcbiAgICAgIGlmICh0aGlzLnByb3BzLnNob3cpIHtcbiAgICAgICAgdGhpcy5oYW5kbGVTaG93KCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzKSB7XG4gICAgICBpZiAobmV4dFByb3BzLnNob3cgJiYgdGhpcy5zdGF0ZS5leGl0ZWQpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGV4aXRlZDogZmFsc2UgfSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgICBpZiAoIXRoaXMucHJvcHMuc2hvdyAmJiBuZXh0UHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLmNoZWNrRm9yRm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICBpZiAoIXByZXZQcm9wcy5zaG93ICYmIHRoaXMucHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLmhhbmRsZVNob3coKTtcbiAgICAgIH1cbiAgICAgIC8vIFdlIGFyZSB3YWl0aW5nIGZvciB0aGUgb25FeGl0ZWQgY2FsbGJhY2sgdG8gY2FsbCBoYW5kbGVIaWRlLlxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICBpZiAodGhpcy5wcm9wcy5zaG93IHx8ICF0aGlzLnN0YXRlLmV4aXRlZCkge1xuICAgICAgICB0aGlzLmhhbmRsZUhpZGUoKTtcbiAgICAgIH1cbiAgICAgIHRoaXMubW91bnRlZCA9IGZhbHNlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NoZWNrRm9yRm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjaGVja0ZvckZvY3VzKCkge1xuICAgICAgaWYgKF9pbkRPTTIuZGVmYXVsdCkge1xuICAgICAgICB0aGlzLmxhc3RGb2N1cyA9ICgwLCBfYWN0aXZlRWxlbWVudDIuZGVmYXVsdCkoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZXN0b3JlTGFzdEZvY3VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVzdG9yZUxhc3RGb2N1cygpIHtcbiAgICAgIGlmICh0aGlzLmxhc3RGb2N1cyAmJiB0aGlzLmxhc3RGb2N1cy5mb2N1cykge1xuICAgICAgICB0aGlzLmxhc3RGb2N1cy5mb2N1cygpO1xuICAgICAgICB0aGlzLmxhc3RGb2N1cyA9IHVuZGVmaW5lZDtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdoYW5kbGVTaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaGFuZGxlU2hvdygpIHtcbiAgICAgIHZhciBkb2MgPSAoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKF9yZWFjdERvbTIuZGVmYXVsdC5maW5kRE9NTm9kZSh0aGlzKSk7XG4gICAgICB0aGlzLnByb3BzLm1vZGFsTWFuYWdlci5hZGQodGhpcyk7XG4gICAgICB0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyID0gKDAsIF9hZGRFdmVudExpc3RlbmVyMi5kZWZhdWx0KShkb2MsICdrZXl1cCcsIHRoaXMuaGFuZGxlRG9jdW1lbnRLZXlVcCk7XG4gICAgICB0aGlzLm9uRm9jdXNMaXN0ZW5lciA9ICgwLCBfYWRkRXZlbnRMaXN0ZW5lcjIuZGVmYXVsdCkoZG9jLCAnZm9jdXMnLCB0aGlzLmhhbmRsZUZvY3VzTGlzdGVuZXIsIHRydWUpO1xuICAgICAgdGhpcy5mb2N1cygpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2ZvY3VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZm9jdXMoKSB7XG4gICAgICB2YXIgY3VycmVudEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKF9yZWFjdERvbTIuZGVmYXVsdC5maW5kRE9NTm9kZSh0aGlzKSkpO1xuICAgICAgdmFyIG1vZGFsQ29udGVudCA9IHRoaXMubW9kYWwgJiYgdGhpcy5tb2RhbC5sYXN0Q2hpbGQ7XG4gICAgICB2YXIgZm9jdXNJbk1vZGFsID0gY3VycmVudEZvY3VzICYmICgwLCBfY29udGFpbnMyLmRlZmF1bHQpKG1vZGFsQ29udGVudCwgY3VycmVudEZvY3VzKTtcblxuICAgICAgaWYgKG1vZGFsQ29udGVudCAmJiAhZm9jdXNJbk1vZGFsKSB7XG4gICAgICAgIGlmICghbW9kYWxDb250ZW50Lmhhc0F0dHJpYnV0ZSgndGFiSW5kZXgnKSkge1xuICAgICAgICAgIG1vZGFsQ29udGVudC5zZXRBdHRyaWJ1dGUoJ3RhYkluZGV4JywgLTEpO1xuICAgICAgICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkoZmFsc2UsICdNYXRlcmlhbC1VSTogdGhlIG1vZGFsIGNvbnRlbnQgbm9kZSBkb2VzIG5vdCBhY2NlcHQgZm9jdXMuICcgKyAnRm9yIHRoZSBiZW5lZml0IG9mIGFzc2lzdGl2ZSB0ZWNobm9sb2dpZXMsICcgKyAndGhlIHRhYkluZGV4IG9mIHRoZSBub2RlIGlzIGJlaW5nIHNldCB0byBcIi0xXCIuJykgOiB2b2lkIDA7XG4gICAgICAgIH1cblxuICAgICAgICBtb2RhbENvbnRlbnQuZm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdoYW5kbGVIaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaGFuZGxlSGlkZSgpIHtcbiAgICAgIHRoaXMucHJvcHMubW9kYWxNYW5hZ2VyLnJlbW92ZSh0aGlzKTtcbiAgICAgIGlmICh0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyKSB0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyLnJlbW92ZSgpO1xuICAgICAgaWYgKHRoaXMub25Gb2N1c0xpc3RlbmVyKSB0aGlzLm9uRm9jdXNMaXN0ZW5lci5yZW1vdmUoKTtcbiAgICAgIHRoaXMucmVzdG9yZUxhc3RGb2N1cygpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlckJhY2tkcm9wJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyQmFja2Ryb3AoKSB7XG4gICAgICB2YXIgb3RoZXIgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgQmFja2Ryb3BDb21wb25lbnQgPSBfcHJvcHMuQmFja2Ryb3BDb21wb25lbnQsXG4gICAgICAgICAgQmFja2Ryb3BDbGFzc05hbWUgPSBfcHJvcHMuQmFja2Ryb3BDbGFzc05hbWUsXG4gICAgICAgICAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gPSBfcHJvcHMuQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgQmFja2Ryb3BJbnZpc2libGUgPSBfcHJvcHMuQmFja2Ryb3BJbnZpc2libGUsXG4gICAgICAgICAgc2hvdyA9IF9wcm9wcy5zaG93O1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0ZhZGUyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBhcHBlYXI6IHRydWUsICdpbic6IHNob3csIHRpbWVvdXQ6IEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uIH0sIG90aGVyKSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQmFja2Ryb3BDb21wb25lbnQsIHtcbiAgICAgICAgICBpbnZpc2libGU6IEJhY2tkcm9wSW52aXNpYmxlLFxuICAgICAgICAgIGNsYXNzTmFtZTogQmFja2Ryb3BDbGFzc05hbWUsXG4gICAgICAgICAgb25DbGljazogdGhpcy5oYW5kbGVCYWNrZHJvcENsaWNrXG4gICAgICAgIH0pXG4gICAgICApO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgZGlzYWJsZUJhY2tkcm9wID0gX3Byb3BzMi5kaXNhYmxlQmFja2Ryb3AsXG4gICAgICAgICAgQmFja2Ryb3BDb21wb25lbnQgPSBfcHJvcHMyLkJhY2tkcm9wQ29tcG9uZW50LFxuICAgICAgICAgIEJhY2tkcm9wQ2xhc3NOYW1lID0gX3Byb3BzMi5CYWNrZHJvcENsYXNzTmFtZSxcbiAgICAgICAgICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiA9IF9wcm9wczIuQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgQmFja2Ryb3BJbnZpc2libGUgPSBfcHJvcHMyLkJhY2tkcm9wSW52aXNpYmxlLFxuICAgICAgICAgIGlnbm9yZUJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMyLmlnbm9yZUJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgaWdub3JlRXNjYXBlS2V5VXAgPSBfcHJvcHMyLmlnbm9yZUVzY2FwZUtleVVwLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzMi5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzMi5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wczIuY2xhc3NOYW1lLFxuICAgICAgICAgIGtlZXBNb3VudGVkID0gX3Byb3BzMi5rZWVwTW91bnRlZCxcbiAgICAgICAgICBtb2RhbE1hbmFnZXJQcm9wID0gX3Byb3BzMi5tb2RhbE1hbmFnZXIsXG4gICAgICAgICAgb25CYWNrZHJvcENsaWNrID0gX3Byb3BzMi5vbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgb25Fc2NhcGVLZXlVcCA9IF9wcm9wczIub25Fc2NhcGVLZXlVcCxcbiAgICAgICAgICBvblJlcXVlc3RDbG9zZSA9IF9wcm9wczIub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wczIub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzMi5vbkVudGVyaW5nLFxuICAgICAgICAgIG9uRW50ZXJlZCA9IF9wcm9wczIub25FbnRlcmVkLFxuICAgICAgICAgIG9uRXhpdCA9IF9wcm9wczIub25FeGl0LFxuICAgICAgICAgIG9uRXhpdGluZyA9IF9wcm9wczIub25FeGl0aW5nLFxuICAgICAgICAgIG9uRXhpdGVkID0gX3Byb3BzMi5vbkV4aXRlZCxcbiAgICAgICAgICBzaG93ID0gX3Byb3BzMi5zaG93LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzMiwgWydkaXNhYmxlQmFja2Ryb3AnLCAnQmFja2Ryb3BDb21wb25lbnQnLCAnQmFja2Ryb3BDbGFzc05hbWUnLCAnQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24nLCAnQmFja2Ryb3BJbnZpc2libGUnLCAnaWdub3JlQmFja2Ryb3BDbGljaycsICdpZ25vcmVFc2NhcGVLZXlVcCcsICdjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdrZWVwTW91bnRlZCcsICdtb2RhbE1hbmFnZXInLCAnb25CYWNrZHJvcENsaWNrJywgJ29uRXNjYXBlS2V5VXAnLCAnb25SZXF1ZXN0Q2xvc2UnLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRW50ZXJlZCcsICdvbkV4aXQnLCAnb25FeGl0aW5nJywgJ29uRXhpdGVkJywgJ3Nob3cnXSk7XG5cblxuICAgICAgaWYgKCFrZWVwTW91bnRlZCAmJiAhc2hvdyAmJiB0aGlzLnN0YXRlLmV4aXRlZCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgdmFyIHRyYW5zaXRpb25DYWxsYmFja3MgPSB7XG4gICAgICAgIG9uRW50ZXI6IG9uRW50ZXIsXG4gICAgICAgIG9uRW50ZXJpbmc6IG9uRW50ZXJpbmcsXG4gICAgICAgIG9uRW50ZXJlZDogb25FbnRlcmVkLFxuICAgICAgICBvbkV4aXQ6IG9uRXhpdCxcbiAgICAgICAgb25FeGl0aW5nOiBvbkV4aXRpbmcsXG4gICAgICAgIG9uRXhpdGVkOiB0aGlzLmhhbmRsZVRyYW5zaXRpb25FeGl0ZWRcbiAgICAgIH07XG5cbiAgICAgIHZhciBtb2RhbENoaWxkID0gX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm9ubHkoY2hpbGRyZW4pO1xuICAgICAgdmFyIF9tb2RhbENoaWxkJHByb3BzID0gbW9kYWxDaGlsZC5wcm9wcyxcbiAgICAgICAgICByb2xlID0gX21vZGFsQ2hpbGQkcHJvcHMucm9sZSxcbiAgICAgICAgICB0YWJJbmRleCA9IF9tb2RhbENoaWxkJHByb3BzLnRhYkluZGV4O1xuXG4gICAgICB2YXIgY2hpbGRQcm9wcyA9IHt9O1xuXG4gICAgICBpZiAocm9sZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGNoaWxkUHJvcHMucm9sZSA9IHJvbGUgPT09IHVuZGVmaW5lZCA/ICdkb2N1bWVudCcgOiByb2xlO1xuICAgICAgfVxuXG4gICAgICBpZiAodGFiSW5kZXggPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjaGlsZFByb3BzLnRhYkluZGV4ID0gdGFiSW5kZXggPT0gbnVsbCA/IC0xIDogdGFiSW5kZXg7XG4gICAgICB9XG5cbiAgICAgIHZhciBiYWNrZHJvcFByb3BzID0gdm9pZCAwO1xuXG4gICAgICAvLyBJdCdzIGEgVHJhbnNpdGlvbiBsaWtlIGNvbXBvbmVudFxuICAgICAgaWYgKG1vZGFsQ2hpbGQucHJvcHMuaGFzT3duUHJvcGVydHkoJ2luJykpIHtcbiAgICAgICAgKDAsIF9rZXlzMi5kZWZhdWx0KSh0cmFuc2l0aW9uQ2FsbGJhY2tzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICBjaGlsZFByb3BzW2tleV0gPSAoMCwgX2hlbHBlcnMuY3JlYXRlQ2hhaW5lZEZ1bmN0aW9uKSh0cmFuc2l0aW9uQ2FsbGJhY2tzW2tleV0sIG1vZGFsQ2hpbGQucHJvcHNba2V5XSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYmFja2Ryb3BQcm9wcyA9IHRyYW5zaXRpb25DYWxsYmFja3M7XG4gICAgICB9XG5cbiAgICAgIGlmICgoMCwgX2tleXMyLmRlZmF1bHQpKGNoaWxkUHJvcHMpLmxlbmd0aCkge1xuICAgICAgICBtb2RhbENoaWxkID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChtb2RhbENoaWxkLCBjaGlsZFByb3BzKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfUG9ydGFsMi5kZWZhdWx0LFxuICAgICAgICB7XG4gICAgICAgICAgb3BlbjogdHJ1ZSxcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICBfdGhpczIubW91bnROb2RlID0gbm9kZSA/IG5vZGUuZ2V0TGF5ZXIoKSA6IG51bGw7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuaGlkZGVuLCB0aGlzLnN0YXRlLmV4aXRlZCkpXG4gICAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgX3RoaXMyLm1vZGFsID0gbm9kZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KSxcbiAgICAgICAgICAhZGlzYWJsZUJhY2tkcm9wICYmICgha2VlcE1vdW50ZWQgfHwgc2hvdyB8fCAhdGhpcy5zdGF0ZS5leGl0ZWQpICYmIHRoaXMucmVuZGVyQmFja2Ryb3AoYmFja2Ryb3BQcm9wcyksXG4gICAgICAgICAgbW9kYWxDaGlsZFxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTW9kYWw7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Nb2RhbC5kZWZhdWx0UHJvcHMgPSB7XG4gIEJhY2tkcm9wQ29tcG9uZW50OiBfQmFja2Ryb3AyLmRlZmF1bHQsXG4gIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uOiAzMDAsXG4gIEJhY2tkcm9wSW52aXNpYmxlOiBmYWxzZSxcbiAga2VlcE1vdW50ZWQ6IGZhbHNlLFxuICBkaXNhYmxlQmFja2Ryb3A6IGZhbHNlLFxuICBpZ25vcmVCYWNrZHJvcENsaWNrOiBmYWxzZSxcbiAgaWdub3JlRXNjYXBlS2V5VXA6IGZhbHNlLFxuICBtb2RhbE1hbmFnZXI6IG1vZGFsTWFuYWdlclxufTtcblxudmFyIF9pbml0aWFsaXNlUHJvcHMgPSBmdW5jdGlvbiBfaW5pdGlhbGlzZVByb3BzKCkge1xuICB2YXIgX3RoaXMzID0gdGhpcztcblxuICB0aGlzLnN0YXRlID0ge1xuICAgIGV4aXRlZDogZmFsc2VcbiAgfTtcbiAgdGhpcy5vbkRvY3VtZW50S2V5VXBMaXN0ZW5lciA9IG51bGw7XG4gIHRoaXMub25Gb2N1c0xpc3RlbmVyID0gbnVsbDtcbiAgdGhpcy5tb3VudGVkID0gZmFsc2U7XG4gIHRoaXMubGFzdEZvY3VzID0gdW5kZWZpbmVkO1xuICB0aGlzLm1vZGFsID0gbnVsbDtcbiAgdGhpcy5tb3VudE5vZGUgPSBudWxsO1xuXG4gIHRoaXMuaGFuZGxlRm9jdXNMaXN0ZW5lciA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIV90aGlzMy5tb3VudGVkIHx8ICFfdGhpczMucHJvcHMubW9kYWxNYW5hZ2VyLmlzVG9wTW9kYWwoX3RoaXMzKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBjdXJyZW50Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKF90aGlzMykpKTtcbiAgICB2YXIgbW9kYWxDb250ZW50ID0gX3RoaXMzLm1vZGFsICYmIF90aGlzMy5tb2RhbC5sYXN0Q2hpbGQ7XG5cbiAgICBpZiAobW9kYWxDb250ZW50ICYmIG1vZGFsQ29udGVudCAhPT0gY3VycmVudEZvY3VzICYmICEoMCwgX2NvbnRhaW5zMi5kZWZhdWx0KShtb2RhbENvbnRlbnQsIGN1cnJlbnRGb2N1cykpIHtcbiAgICAgIG1vZGFsQ29udGVudC5mb2N1cygpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLmhhbmRsZURvY3VtZW50S2V5VXAgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBpZiAoIV90aGlzMy5tb3VudGVkIHx8ICFfdGhpczMucHJvcHMubW9kYWxNYW5hZ2VyLmlzVG9wTW9kYWwoX3RoaXMzKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICgoMCwgX2tleWNvZGUyLmRlZmF1bHQpKGV2ZW50KSAhPT0gJ2VzYycpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgX3Byb3BzMyA9IF90aGlzMy5wcm9wcyxcbiAgICAgICAgb25Fc2NhcGVLZXlVcCA9IF9wcm9wczMub25Fc2NhcGVLZXlVcCxcbiAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHMzLm9uUmVxdWVzdENsb3NlLFxuICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcCA9IF9wcm9wczMuaWdub3JlRXNjYXBlS2V5VXA7XG5cblxuICAgIGlmIChvbkVzY2FwZUtleVVwKSB7XG4gICAgICBvbkVzY2FwZUtleVVwKGV2ZW50KTtcbiAgICB9XG5cbiAgICBpZiAob25SZXF1ZXN0Q2xvc2UgJiYgIWlnbm9yZUVzY2FwZUtleVVwKSB7XG4gICAgICBvblJlcXVlc3RDbG9zZShldmVudCk7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMuaGFuZGxlQmFja2Ryb3BDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIGlmIChldmVudC50YXJnZXQgIT09IGV2ZW50LmN1cnJlbnRUYXJnZXQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgX3Byb3BzNCA9IF90aGlzMy5wcm9wcyxcbiAgICAgICAgb25CYWNrZHJvcENsaWNrID0gX3Byb3BzNC5vbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgIG9uUmVxdWVzdENsb3NlID0gX3Byb3BzNC5vblJlcXVlc3RDbG9zZSxcbiAgICAgICAgaWdub3JlQmFja2Ryb3BDbGljayA9IF9wcm9wczQuaWdub3JlQmFja2Ryb3BDbGljaztcblxuXG4gICAgaWYgKG9uQmFja2Ryb3BDbGljaykge1xuICAgICAgb25CYWNrZHJvcENsaWNrKGV2ZW50KTtcbiAgICB9XG5cbiAgICBpZiAob25SZXF1ZXN0Q2xvc2UgJiYgIWlnbm9yZUJhY2tkcm9wQ2xpY2spIHtcbiAgICAgIG9uUmVxdWVzdENsb3NlKGV2ZW50KTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5oYW5kbGVUcmFuc2l0aW9uRXhpdGVkID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChfdGhpczMucHJvcHMub25FeGl0ZWQpIHtcbiAgICAgIHZhciBfcHJvcHM1O1xuXG4gICAgICAoX3Byb3BzNSA9IF90aGlzMy5wcm9wcykub25FeGl0ZWQuYXBwbHkoX3Byb3BzNSwgYXJndW1lbnRzKTtcbiAgICB9XG5cbiAgICBfdGhpczMuc2V0U3RhdGUoeyBleGl0ZWQ6IHRydWUgfSk7XG4gICAgX3RoaXMzLmhhbmRsZUhpZGUoKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IGZsaXA6IGZhbHNlLCBuYW1lOiAnTXVpTW9kYWwnIH0pKE1vZGFsKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9Nb2RhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvTW9kYWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX01vZGFsID0gcmVxdWlyZSgnLi9Nb2RhbCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Nb2RhbCkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfaXNXaW5kb3cgPSByZXF1aXJlKCdkb20taGVscGVycy9xdWVyeS9pc1dpbmRvdycpO1xuXG52YXIgX2lzV2luZG93MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzV2luZG93KTtcblxudmFyIF9vd25lckRvY3VtZW50ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudCcpO1xuXG52YXIgX293bmVyRG9jdW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3duZXJEb2N1bWVudCk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG52YXIgX3Njcm9sbGJhclNpemUgPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL3Njcm9sbGJhclNpemUnKTtcblxudmFyIF9zY3JvbGxiYXJTaXplMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Njcm9sbGJhclNpemUpO1xuXG52YXIgX21hbmFnZUFyaWFIaWRkZW4gPSByZXF1aXJlKCcuLi91dGlscy9tYW5hZ2VBcmlhSGlkZGVuJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8vIFRha2VuIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWJvb3RzdHJhcC9yZWFjdC1vdmVybGF5cy9ibG9iL21hc3Rlci9zcmMvTW9kYWxNYW5hZ2VyLmpzXG5cbmZ1bmN0aW9uIGdldFBhZGRpbmdSaWdodChub2RlKSB7XG4gIHJldHVybiBwYXJzZUludChub2RlLnN0eWxlLnBhZGRpbmdSaWdodCB8fCAwLCAxMCk7XG59XG5cbi8vIERvIHdlIGhhdmUgYSBzY3JvbGwgYmFyP1xuZnVuY3Rpb24gYm9keUlzT3ZlcmZsb3dpbmcobm9kZSkge1xuICB2YXIgZG9jID0gKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShub2RlKTtcbiAgdmFyIHdpbiA9ICgwLCBfaXNXaW5kb3cyLmRlZmF1bHQpKGRvYyk7XG5cbiAgLy8gVGFrZXMgaW4gYWNjb3VudCBwb3RlbnRpYWwgbm9uIHplcm8gbWFyZ2luIG9uIHRoZSBib2R5LlxuICB2YXIgc3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShkb2MuYm9keSk7XG4gIHZhciBtYXJnaW5MZWZ0ID0gcGFyc2VJbnQoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLWxlZnQnKSwgMTApO1xuICB2YXIgbWFyZ2luUmlnaHQgPSBwYXJzZUludChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdtYXJnaW4tcmlnaHQnKSwgMTApO1xuXG4gIHJldHVybiBtYXJnaW5MZWZ0ICsgZG9jLmJvZHkuY2xpZW50V2lkdGggKyBtYXJnaW5SaWdodCA8IHdpbi5pbm5lcldpZHRoO1xufVxuXG5mdW5jdGlvbiBnZXRDb250YWluZXIoKSB7XG4gIHZhciBjb250YWluZXIgPSBfaW5ET00yLmRlZmF1bHQgPyB3aW5kb3cuZG9jdW1lbnQuYm9keSA6IHt9O1xuICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKGNvbnRhaW5lciAhPT0gbnVsbCwgJ1xcbk1hdGVyaWFsLVVJOiB5b3UgYXJlIG1vc3QgbGlrZWx5IGV2YWx1YXRpbmcgdGhlIGNvZGUgYmVmb3JlIHRoZVxcbmJyb3dzZXIgaGFzIGEgY2hhbmNlIHRvIHJlYWNoIHRoZSA8Ym9keT4uXFxuUGxlYXNlIG1vdmUgdGhlIGltcG9ydCBhdCB0aGUgZW5kIG9mIHRoZSA8Ym9keT4uXFxuICAnKSA6IHZvaWQgMDtcbiAgcmV0dXJuIGNvbnRhaW5lcjtcbn1cbi8qKlxuICogU3RhdGUgbWFuYWdlbWVudCBoZWxwZXIgZm9yIG1vZGFscy9sYXllcnMuXG4gKiBTaW1wbGlmaWVkLCBidXQgaW5zcGlyZWQgYnkgcmVhY3Qtb3ZlcmxheSdzIE1vZGFsTWFuYWdlciBjbGFzc1xuICpcbiAqIEBpbnRlcm5hbCBVc2VkIGJ5IHRoZSBNb2RhbCB0byBlbnN1cmUgcHJvcGVyIGZvY3VzIG1hbmFnZW1lbnQuXG4gKi9cbmZ1bmN0aW9uIGNyZWF0ZU1vZGFsTWFuYWdlcigpIHtcbiAgdmFyIF9yZWYgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9LFxuICAgICAgX3JlZiRoaWRlU2libGluZ05vZGVzID0gX3JlZi5oaWRlU2libGluZ05vZGVzLFxuICAgICAgaGlkZVNpYmxpbmdOb2RlcyA9IF9yZWYkaGlkZVNpYmxpbmdOb2RlcyA9PT0gdW5kZWZpbmVkID8gdHJ1ZSA6IF9yZWYkaGlkZVNpYmxpbmdOb2RlcztcblxuICB2YXIgbW9kYWxzID0gW107XG5cbiAgdmFyIHByZXZPdmVyZmxvdyA9IHZvaWQgMDtcbiAgdmFyIHByZXZQYWRkaW5ncyA9IFtdO1xuXG4gIGZ1bmN0aW9uIGFkZChtb2RhbCkge1xuICAgIHZhciBjb250YWluZXIgPSBnZXRDb250YWluZXIoKTtcbiAgICB2YXIgbW9kYWxJZHggPSBtb2RhbHMuaW5kZXhPZihtb2RhbCk7XG5cbiAgICBpZiAobW9kYWxJZHggIT09IC0xKSB7XG4gICAgICByZXR1cm4gbW9kYWxJZHg7XG4gICAgfVxuXG4gICAgbW9kYWxJZHggPSBtb2RhbHMubGVuZ3RoO1xuICAgIG1vZGFscy5wdXNoKG1vZGFsKTtcblxuICAgIGlmIChoaWRlU2libGluZ05vZGVzKSB7XG4gICAgICAoMCwgX21hbmFnZUFyaWFIaWRkZW4uaGlkZVNpYmxpbmdzKShjb250YWluZXIsIG1vZGFsLm1vdW50Tm9kZSk7XG4gICAgfVxuXG4gICAgaWYgKG1vZGFscy5sZW5ndGggPT09IDEpIHtcbiAgICAgIC8vIFNhdmUgb3VyIGN1cnJlbnQgb3ZlcmZsb3cgc28gd2UgY2FuIHJldmVydFxuICAgICAgLy8gYmFjayB0byBpdCB3aGVuIGFsbCBtb2RhbHMgYXJlIGNsb3NlZCFcbiAgICAgIHByZXZPdmVyZmxvdyA9IGNvbnRhaW5lci5zdHlsZS5vdmVyZmxvdztcblxuICAgICAgaWYgKGJvZHlJc092ZXJmbG93aW5nKGNvbnRhaW5lcikpIHtcbiAgICAgICAgcHJldlBhZGRpbmdzID0gW2dldFBhZGRpbmdSaWdodChjb250YWluZXIpXTtcbiAgICAgICAgdmFyIHNjcm9sbGJhclNpemUgPSAoMCwgX3Njcm9sbGJhclNpemUyLmRlZmF1bHQpKCk7XG4gICAgICAgIGNvbnRhaW5lci5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwcmV2UGFkZGluZ3NbMF0gKyBzY3JvbGxiYXJTaXplICsgJ3B4JztcblxuICAgICAgICB2YXIgZml4ZWROb2RlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5tdWktZml4ZWQnKTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaXhlZE5vZGVzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgICAgdmFyIHBhZGRpbmdSaWdodCA9IGdldFBhZGRpbmdSaWdodChmaXhlZE5vZGVzW2ldKTtcbiAgICAgICAgICBwcmV2UGFkZGluZ3MucHVzaChwYWRkaW5nUmlnaHQpO1xuICAgICAgICAgIGZpeGVkTm9kZXNbaV0uc3R5bGUucGFkZGluZ1JpZ2h0ID0gcGFkZGluZ1JpZ2h0ICsgc2Nyb2xsYmFyU2l6ZSArICdweCc7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgY29udGFpbmVyLnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1vZGFsSWR4O1xuICB9XG5cbiAgZnVuY3Rpb24gcmVtb3ZlKG1vZGFsKSB7XG4gICAgdmFyIGNvbnRhaW5lciA9IGdldENvbnRhaW5lcigpO1xuICAgIHZhciBtb2RhbElkeCA9IG1vZGFscy5pbmRleE9mKG1vZGFsKTtcblxuICAgIGlmIChtb2RhbElkeCA9PT0gLTEpIHtcbiAgICAgIHJldHVybiBtb2RhbElkeDtcbiAgICB9XG5cbiAgICBtb2RhbHMuc3BsaWNlKG1vZGFsSWR4LCAxKTtcblxuICAgIGlmIChtb2RhbHMubGVuZ3RoID09PSAwKSB7XG4gICAgICBjb250YWluZXIuc3R5bGUub3ZlcmZsb3cgPSBwcmV2T3ZlcmZsb3c7XG4gICAgICBjb250YWluZXIuc3R5bGUucGFkZGluZ1JpZ2h0ID0gcHJldlBhZGRpbmdzWzBdO1xuXG4gICAgICB2YXIgZml4ZWROb2RlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5tdWktZml4ZWQnKTtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZml4ZWROb2Rlcy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICBmaXhlZE5vZGVzW2ldLnN0eWxlLnBhZGRpbmdSaWdodCA9IHByZXZQYWRkaW5nc1tpICsgMV0gKyAncHgnO1xuICAgICAgfVxuXG4gICAgICBwcmV2T3ZlcmZsb3cgPSB1bmRlZmluZWQ7XG4gICAgICBwcmV2UGFkZGluZ3MgPSBbXTtcbiAgICAgIGlmIChoaWRlU2libGluZ05vZGVzKSB7XG4gICAgICAgICgwLCBfbWFuYWdlQXJpYUhpZGRlbi5zaG93U2libGluZ3MpKGNvbnRhaW5lciwgbW9kYWwubW91bnROb2RlKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKGhpZGVTaWJsaW5nTm9kZXMpIHtcbiAgICAgIC8vIG90aGVyd2lzZSBtYWtlIHN1cmUgdGhlIG5leHQgdG9wIG1vZGFsIGlzIHZpc2libGUgdG8gYSBTUlxuICAgICAgKDAsIF9tYW5hZ2VBcmlhSGlkZGVuLmFyaWFIaWRkZW4pKGZhbHNlLCBtb2RhbHNbbW9kYWxzLmxlbmd0aCAtIDFdLm1vdW50Tm9kZSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1vZGFsSWR4O1xuICB9XG5cbiAgZnVuY3Rpb24gaXNUb3BNb2RhbChtb2RhbCkge1xuICAgIHJldHVybiAhIW1vZGFscy5sZW5ndGggJiYgbW9kYWxzW21vZGFscy5sZW5ndGggLSAxXSA9PT0gbW9kYWw7XG4gIH1cblxuICB2YXIgbW9kYWxNYW5hZ2VyID0geyBhZGQ6IGFkZCwgcmVtb3ZlOiByZW1vdmUsIGlzVG9wTW9kYWw6IGlzVG9wTW9kYWwgfTtcblxuICByZXR1cm4gbW9kYWxNYW5hZ2VyO1xufVxuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVNb2RhbE1hbmFnZXI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvbW9kYWxNYW5hZ2VyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3JlYWN0RG9tID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG5cbnZhciBfcmVhY3REb20yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3REb20pO1xuXG52YXIgX2luRE9NID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvdXRpbC9pbkRPTScpO1xuXG52YXIgX2luRE9NMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luRE9NKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgdG8gcG9ydGFsIGluIG9yZGVyIHRvIGVzY2FwZSB0aGUgcGFyZW50IERPTSBub2RlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogSWYgYHRydWVgIHRoZSBjaGlsZHJlbiB3aWxsIGJlIG1vdW50ZWQgaW50byB0aGUgRE9NLlxuICAgKi9cbiAgb3BlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2xcbn07XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIFBvcnRhbCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFBvcnRhbCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gUG9ydGFsKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFBvcnRhbCk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBQb3J0YWwuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFBvcnRhbCkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmxheWVyID0gbnVsbCwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFBvcnRhbCwgW3tcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgLy8gU3VwcG9ydCByZWFjdEAxNS54LCB3aWxsIGJlIHJlbW92ZWQgYXQgc29tZSBwb2ludFxuICAgICAgaWYgKCFfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKSB7XG4gICAgICAgIHRoaXMucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUoKSB7XG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJMYXllcigpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdnZXRMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldExheWVyKCkge1xuICAgICAgaWYgKCF0aGlzLmxheWVyKSB7XG4gICAgICAgIHRoaXMubGF5ZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgdGhpcy5sYXllci5zZXRBdHRyaWJ1dGUoJ2RhdGEtbXVpLXBvcnRhbCcsICd0cnVlJyk7XG4gICAgICAgIGlmIChkb2N1bWVudC5ib2R5ICYmIHRoaXMubGF5ZXIpIHtcbiAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRoaXMubGF5ZXIpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLmxheWVyO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3VucmVuZGVyTGF5ZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1bnJlbmRlckxheWVyKCkge1xuICAgICAgaWYgKCF0aGlzLmxheWVyKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgLy8gU3VwcG9ydCByZWFjdEAxNS54LCB3aWxsIGJlIHJlbW92ZWQgYXQgc29tZSBwb2ludFxuICAgICAgaWYgKCFfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKSB7XG4gICAgICAgIF9yZWFjdERvbTIuZGVmYXVsdC51bm1vdW50Q29tcG9uZW50QXROb2RlKHRoaXMubGF5ZXIpO1xuICAgICAgfVxuXG4gICAgICBpZiAoZG9jdW1lbnQuYm9keSkge1xuICAgICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHRoaXMubGF5ZXIpO1xuICAgICAgfVxuICAgICAgdGhpcy5sYXllciA9IG51bGw7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyTGF5ZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXJMYXllcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIG9wZW4gPSBfcHJvcHMub3BlbjtcblxuXG4gICAgICBpZiAob3Blbikge1xuICAgICAgICAvLyBCeSBjYWxsaW5nIHRoaXMgbWV0aG9kIGluIGNvbXBvbmVudERpZE1vdW50KCkgYW5kXG4gICAgICAgIC8vIGNvbXBvbmVudERpZFVwZGF0ZSgpLCB5b3UncmUgZWZmZWN0aXZlbHkgY3JlYXRpbmcgYSBcIndvcm1ob2xlXCIgdGhhdFxuICAgICAgICAvLyBmdW5uZWxzIFJlYWN0J3MgaGllcmFyY2hpY2FsIHVwZGF0ZXMgdGhyb3VnaCB0byBhIERPTSBub2RlIG9uIGFuXG4gICAgICAgIC8vIGVudGlyZWx5IGRpZmZlcmVudCBwYXJ0IG9mIHRoZSBwYWdlLlxuICAgICAgICB2YXIgbGF5ZXJFbGVtZW50ID0gX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm9ubHkoY2hpbGRyZW4pO1xuICAgICAgICBfcmVhY3REb20yLmRlZmF1bHQudW5zdGFibGVfcmVuZGVyU3VidHJlZUludG9Db250YWluZXIodGhpcywgbGF5ZXJFbGVtZW50LCB0aGlzLmdldExheWVyKCkpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy51bnJlbmRlckxheWVyKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzMi5jaGlsZHJlbixcbiAgICAgICAgICBvcGVuID0gX3Byb3BzMi5vcGVuO1xuXG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG5cbiAgICAgIGlmICghX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgLy8gQ2FuJ3QgYmUgcmVuZGVyZWQgc2VydmVyLXNpZGUuXG4gICAgICBpZiAoX2luRE9NMi5kZWZhdWx0KSB7XG4gICAgICAgIGlmIChvcGVuKSB7XG4gICAgICAgICAgdmFyIGxheWVyID0gdGhpcy5nZXRMYXllcigpO1xuICAgICAgICAgIC8vICRGbG93Rml4TWUgbGF5ZXIgaXMgbm9uLW51bGxcbiAgICAgICAgICByZXR1cm4gX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbChjaGlsZHJlbiwgbGF5ZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy51bnJlbmRlckxheWVyKCk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gUG9ydGFsO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuUG9ydGFsLmRlZmF1bHRQcm9wcyA9IHtcbiAgb3BlbjogZmFsc2Vcbn07XG5Qb3J0YWwucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8ge1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufSA6IHt9O1xuZXhwb3J0cy5kZWZhdWx0ID0gUG9ydGFsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1BvcnRhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvUG9ydGFsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIlwidXNlIHN0cmljdFwiO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID0gcmVxdWlyZShcInByb3AtdHlwZXNcIikub25lT2ZUeXBlKFtyZXF1aXJlKFwicHJvcC10eXBlc1wiKS5udW1iZXIsIHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnNoYXBlKHtcbiAgZW50ZXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlci5pc1JlcXVpcmVkLFxuICBleGl0OiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5udW1iZXIuaXNSZXF1aXJlZFxufSldKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLmZ1bmM7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcyA9IHtcbiAgYXBwZWFyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGFwcGVhckFjdGl2ZTogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBlbnRlcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBlbnRlckFjdGl2ZTogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBleGl0OiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGV4aXRBY3RpdmU6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZ1xufTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC90cmFuc2l0aW9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC90cmFuc2l0aW9uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAxMSAyNyAyOCAzNCAzNSAzNiAzNyAzOCA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9UcmFuc2l0aW9uID0gcmVxdWlyZSgncmVhY3QtdHJhbnNpdGlvbi1ncm91cC9UcmFuc2l0aW9uJyk7XG5cbnZhciBfVHJhbnNpdGlvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UcmFuc2l0aW9uKTtcblxudmFyIF90cmFuc2l0aW9ucyA9IHJlcXVpcmUoJy4uL3N0eWxlcy90cmFuc2l0aW9ucycpO1xuXG52YXIgX3dpdGhUaGVtZSA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoVGhlbWUnKTtcblxudmFyIF93aXRoVGhlbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFRoZW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgVHJhbnNpdGlvblxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBhcHBlYXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEEgc2luZ2xlIGNoaWxkIGNvbnRlbnQgZWxlbWVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgY29tcG9uZW50IHdpbGwgdHJhbnNpdGlvbiBpbi5cbiAgICovXG4gIGluOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FeGl0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHN0eWxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgZHVyYXRpb24gZm9yIHRoZSB0cmFuc2l0aW9uLCBpbiBtaWxsaXNlY29uZHMuXG4gICAqIFlvdSBtYXkgc3BlY2lmeSBhIHNpbmdsZSB0aW1lb3V0IGZvciBhbGwgdHJhbnNpdGlvbnMsIG9yIGluZGl2aWR1YWxseSB3aXRoIGFuIG9iamVjdC5cbiAgICovXG4gIHRpbWVvdXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbikuaXNSZXF1aXJlZFxufTtcblxuXG52YXIgcmVmbG93ID0gZnVuY3Rpb24gcmVmbG93KG5vZGUpIHtcbiAgcmV0dXJuIG5vZGUuc2Nyb2xsVG9wO1xufTtcblxuLyoqXG4gKiBUaGUgRmFkZSB0cmFuc2l0aW9uIGlzIHVzZWQgYnkgdGhlIE1vZGFsIGNvbXBvbmVudC5cbiAqIEl0J3MgdXNpbmcgW3JlYWN0LXRyYW5zaXRpb24tZ3JvdXBdKGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdGpzL3JlYWN0LXRyYW5zaXRpb24tZ3JvdXApIGludGVybmFsbHkuXG4gKi9cblxudmFyIEZhZGUgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShGYWRlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBGYWRlKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEZhZGUpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gRmFkZS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoRmFkZSkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmhhbmRsZUVudGVyID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcwJztcbiAgICAgIHJlZmxvdyhub2RlKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXIpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcihub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFbnRlcmluZyA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzLnRoZW1lLFxuICAgICAgICAgIHRpbWVvdXQgPSBfdGhpcyRwcm9wcy50aW1lb3V0O1xuXG4gICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb24gPSB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0eXBlb2YgdGltZW91dCA9PT0gJ251bWJlcicgPyB0aW1lb3V0IDogdGltZW91dC5lbnRlclxuICAgICAgfSk7XG4gICAgICAvLyAkRmxvd0ZpeE1lIC0gaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL2Zsb3cvcHVsbC81MTYxXG4gICAgICBub2RlLnN0eWxlLndlYmtpdFRyYW5zaXRpb24gPSB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0eXBlb2YgdGltZW91dCA9PT0gJ251bWJlcicgPyB0aW1lb3V0IDogdGltZW91dC5lbnRlclxuICAgICAgfSk7XG4gICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMSc7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyaW5nKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJpbmcobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlRXhpdCA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMyID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wczIudGhlbWUsXG4gICAgICAgICAgdGltZW91dCA9IF90aGlzJHByb3BzMi50aW1lb3V0O1xuXG4gICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb24gPSB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0eXBlb2YgdGltZW91dCA9PT0gJ251bWJlcicgPyB0aW1lb3V0IDogdGltZW91dC5leGl0XG4gICAgICB9KTtcbiAgICAgIC8vICRGbG93Rml4TWUgLSBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svZmxvdy9wdWxsLzUxNjFcbiAgICAgIG5vZGUuc3R5bGUud2Via2l0VHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmV4aXRcbiAgICAgIH0pO1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzAnO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FeGl0KSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRXhpdChub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoRmFkZSwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhcHBlYXIgPSBfcHJvcHMuYXBwZWFyLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzLm9uRXhpdCxcbiAgICAgICAgICBzdHlsZVByb3AgPSBfcHJvcHMuc3R5bGUsXG4gICAgICAgICAgdGhlbWUgPSBfcHJvcHMudGhlbWUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYXBwZWFyJywgJ2NoaWxkcmVuJywgJ29uRW50ZXInLCAnb25FbnRlcmluZycsICdvbkV4aXQnLCAnc3R5bGUnLCAndGhlbWUnXSk7XG5cblxuICAgICAgdmFyIHN0eWxlID0gKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgc3R5bGVQcm9wKTtcblxuICAgICAgLy8gRm9yIHNlcnZlciBzaWRlIHJlbmRlcmluZy5cbiAgICAgIGlmICghdGhpcy5wcm9wcy5pbiB8fCBhcHBlYXIpIHtcbiAgICAgICAgc3R5bGUub3BhY2l0eSA9ICcwJztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfVHJhbnNpdGlvbjIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgYXBwZWFyOiBhcHBlYXIsXG4gICAgICAgICAgc3R5bGU6IHN0eWxlLFxuICAgICAgICAgIG9uRW50ZXI6IHRoaXMuaGFuZGxlRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZzogdGhpcy5oYW5kbGVFbnRlcmluZyxcbiAgICAgICAgICBvbkV4aXQ6IHRoaXMuaGFuZGxlRXhpdFxuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gRmFkZTtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkZhZGUuZGVmYXVsdFByb3BzID0ge1xuICBhcHBlYXI6IHRydWUsXG4gIHRpbWVvdXQ6IHtcbiAgICBlbnRlcjogX3RyYW5zaXRpb25zLmR1cmF0aW9uLmVudGVyaW5nU2NyZWVuLFxuICAgIGV4aXQ6IF90cmFuc2l0aW9ucy5kdXJhdGlvbi5sZWF2aW5nU2NyZWVuXG4gIH1cbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhUaGVtZTIuZGVmYXVsdCkoKShGYWRlKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9GYWRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9GYWRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuYXJpYUhpZGRlbiA9IGFyaWFIaWRkZW47XG5leHBvcnRzLmhpZGVTaWJsaW5ncyA9IGhpZGVTaWJsaW5ncztcbmV4cG9ydHMuc2hvd1NpYmxpbmdzID0gc2hvd1NpYmxpbmdzO1xuLy8gIHdlYWtcblxudmFyIEJMQUNLTElTVCA9IFsndGVtcGxhdGUnLCAnc2NyaXB0JywgJ3N0eWxlJ107XG5cbnZhciBpc0hpZGFibGUgPSBmdW5jdGlvbiBpc0hpZGFibGUoX3JlZikge1xuICB2YXIgbm9kZVR5cGUgPSBfcmVmLm5vZGVUeXBlLFxuICAgICAgdGFnTmFtZSA9IF9yZWYudGFnTmFtZTtcbiAgcmV0dXJuIG5vZGVUeXBlID09PSAxICYmIEJMQUNLTElTVC5pbmRleE9mKHRhZ05hbWUudG9Mb3dlckNhc2UoKSkgPT09IC0xO1xufTtcblxudmFyIHNpYmxpbmdzID0gZnVuY3Rpb24gc2libGluZ3MoY29udGFpbmVyLCBtb3VudCwgY2IpIHtcbiAgbW91bnQgPSBbXS5jb25jYXQobW91bnQpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gIFtdLmZvckVhY2guY2FsbChjb250YWluZXIuY2hpbGRyZW4sIGZ1bmN0aW9uIChub2RlKSB7XG4gICAgaWYgKG1vdW50LmluZGV4T2Yobm9kZSkgPT09IC0xICYmIGlzSGlkYWJsZShub2RlKSkge1xuICAgICAgY2Iobm9kZSk7XG4gICAgfVxuICB9KTtcbn07XG5cbmZ1bmN0aW9uIGFyaWFIaWRkZW4oc2hvdywgbm9kZSkge1xuICBpZiAoIW5vZGUpIHtcbiAgICByZXR1cm47XG4gIH1cbiAgaWYgKHNob3cpIHtcbiAgICBub2RlLnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLCAndHJ1ZScpO1xuICB9IGVsc2Uge1xuICAgIG5vZGUucmVtb3ZlQXR0cmlidXRlKCdhcmlhLWhpZGRlbicpO1xuICB9XG59XG5cbmZ1bmN0aW9uIGhpZGVTaWJsaW5ncyhjb250YWluZXIsIG1vdW50Tm9kZSkge1xuICBzaWJsaW5ncyhjb250YWluZXIsIG1vdW50Tm9kZSwgZnVuY3Rpb24gKG5vZGUpIHtcbiAgICByZXR1cm4gYXJpYUhpZGRlbih0cnVlLCBub2RlKTtcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHNob3dTaWJsaW5ncyhjb250YWluZXIsIG1vdW50Tm9kZSkge1xuICBzaWJsaW5ncyhjb250YWluZXIsIG1vdW50Tm9kZSwgZnVuY3Rpb24gKG5vZGUpIHtcbiAgICByZXR1cm4gYXJpYUhpZGRlbihmYWxzZSwgbm9kZSk7XG4gIH0pO1xufVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3V0aWxzL21hbmFnZUFyaWFIaWRkZW4uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3V0aWxzL21hbmFnZUFyaWFIaWRkZW4uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2ludmFyaWFudCA9IHJlcXVpcmUoJ2ludmFyaWFudCcpO1xuXG52YXIgX2ludmFyaWFudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbnZhcmlhbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGlzTW9kaWZpZWRFdmVudCA9IGZ1bmN0aW9uIGlzTW9kaWZpZWRFdmVudChldmVudCkge1xuICByZXR1cm4gISEoZXZlbnQubWV0YUtleSB8fCBldmVudC5hbHRLZXkgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5zaGlmdEtleSk7XG59O1xuXG4vKipcbiAqIFRoZSBwdWJsaWMgQVBJIGZvciByZW5kZXJpbmcgYSBoaXN0b3J5LWF3YXJlIDxhPi5cbiAqL1xuXG52YXIgTGluayA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhMaW5rLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaW5rKCkge1xuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTGluayk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbC5hcHBseShfUmVhY3QkQ29tcG9uZW50LCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlQ2xpY2sgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkNsaWNrKSBfdGhpcy5wcm9wcy5vbkNsaWNrKGV2ZW50KTtcblxuICAgICAgaWYgKCFldmVudC5kZWZhdWx0UHJldmVudGVkICYmIC8vIG9uQ2xpY2sgcHJldmVudGVkIGRlZmF1bHRcbiAgICAgIGV2ZW50LmJ1dHRvbiA9PT0gMCAmJiAvLyBpZ25vcmUgcmlnaHQgY2xpY2tzXG4gICAgICAhX3RoaXMucHJvcHMudGFyZ2V0ICYmIC8vIGxldCBicm93c2VyIGhhbmRsZSBcInRhcmdldD1fYmxhbmtcIiBldGMuXG4gICAgICAhaXNNb2RpZmllZEV2ZW50KGV2ZW50KSAvLyBpZ25vcmUgY2xpY2tzIHdpdGggbW9kaWZpZXIga2V5c1xuICAgICAgKSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgIHZhciBoaXN0b3J5ID0gX3RoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeTtcbiAgICAgICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICAgICAgcmVwbGFjZSA9IF90aGlzJHByb3BzLnJlcGxhY2UsXG4gICAgICAgICAgICAgIHRvID0gX3RoaXMkcHJvcHMudG87XG5cblxuICAgICAgICAgIGlmIChyZXBsYWNlKSB7XG4gICAgICAgICAgICBoaXN0b3J5LnJlcGxhY2UodG8pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBoaXN0b3J5LnB1c2godG8pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgTGluay5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICByZXBsYWNlID0gX3Byb3BzLnJlcGxhY2UsXG4gICAgICAgIHRvID0gX3Byb3BzLnRvLFxuICAgICAgICBpbm5lclJlZiA9IF9wcm9wcy5pbm5lclJlZixcbiAgICAgICAgcHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzLCBbJ3JlcGxhY2UnLCAndG8nLCAnaW5uZXJSZWYnXSk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW51c2VkLXZhcnNcblxuICAgICgwLCBfaW52YXJpYW50Mi5kZWZhdWx0KSh0aGlzLmNvbnRleHQucm91dGVyLCAnWW91IHNob3VsZCBub3QgdXNlIDxMaW5rPiBvdXRzaWRlIGEgPFJvdXRlcj4nKTtcblxuICAgIHZhciBocmVmID0gdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5LmNyZWF0ZUhyZWYodHlwZW9mIHRvID09PSAnc3RyaW5nJyA/IHsgcGF0aG5hbWU6IHRvIH0gOiB0byk7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2EnLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgb25DbGljazogdGhpcy5oYW5kbGVDbGljaywgaHJlZjogaHJlZiwgcmVmOiBpbm5lclJlZiB9KSk7XG4gIH07XG5cbiAgcmV0dXJuIExpbms7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5MaW5rLnByb3BUeXBlcyA9IHtcbiAgb25DbGljazogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLFxuICB0YXJnZXQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIHRvOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XSkuaXNSZXF1aXJlZCxcbiAgaW5uZXJSZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jXSlcbn07XG5MaW5rLmRlZmF1bHRQcm9wcyA9IHtcbiAgcmVwbGFjZTogZmFsc2Vcbn07XG5MaW5rLmNvbnRleHRUeXBlcyA9IHtcbiAgcm91dGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICBoaXN0b3J5OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICAgIHB1c2g6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICBjcmVhdGVIcmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZFxuICAgIH0pLmlzUmVxdWlyZWRcbiAgfSkuaXNSZXF1aXJlZFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9IExpbms7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNiAyNyAyOCAzMCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA1NyA1OCA1OSA2MSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfaW52YXJpYW50ID0gcmVxdWlyZSgnaW52YXJpYW50Jyk7XG5cbnZhciBfaW52YXJpYW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ludmFyaWFudCk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfbWF0Y2hQYXRoID0gcmVxdWlyZSgnLi9tYXRjaFBhdGgnKTtcblxudmFyIF9tYXRjaFBhdGgyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfbWF0Y2hQYXRoKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgaXNFbXB0eUNoaWxkcmVuID0gZnVuY3Rpb24gaXNFbXB0eUNoaWxkcmVuKGNoaWxkcmVuKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4uY291bnQoY2hpbGRyZW4pID09PSAwO1xufTtcblxuLyoqXG4gKiBUaGUgcHVibGljIEFQSSBmb3IgbWF0Y2hpbmcgYSBzaW5nbGUgcGF0aCBhbmQgcmVuZGVyaW5nLlxuICovXG5cbnZhciBSb3V0ZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhSb3V0ZSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gUm91dGUoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBSb3V0ZSk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbC5hcHBseShfUmVhY3QkQ29tcG9uZW50LCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBtYXRjaDogX3RoaXMuY29tcHV0ZU1hdGNoKF90aGlzLnByb3BzLCBfdGhpcy5jb250ZXh0LnJvdXRlcilcbiAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gIFJvdXRlLnByb3RvdHlwZS5nZXRDaGlsZENvbnRleHQgPSBmdW5jdGlvbiBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJvdXRlcjogX2V4dGVuZHMoe30sIHRoaXMuY29udGV4dC5yb3V0ZXIsIHtcbiAgICAgICAgcm91dGU6IHtcbiAgICAgICAgICBsb2NhdGlvbjogdGhpcy5wcm9wcy5sb2NhdGlvbiB8fCB0aGlzLmNvbnRleHQucm91dGVyLnJvdXRlLmxvY2F0aW9uLFxuICAgICAgICAgIG1hdGNoOiB0aGlzLnN0YXRlLm1hdGNoXG4gICAgICAgIH1cbiAgICAgIH0pXG4gICAgfTtcbiAgfTtcblxuICBSb3V0ZS5wcm90b3R5cGUuY29tcHV0ZU1hdGNoID0gZnVuY3Rpb24gY29tcHV0ZU1hdGNoKF9yZWYsIHJvdXRlcikge1xuICAgIHZhciBjb21wdXRlZE1hdGNoID0gX3JlZi5jb21wdXRlZE1hdGNoLFxuICAgICAgICBsb2NhdGlvbiA9IF9yZWYubG9jYXRpb24sXG4gICAgICAgIHBhdGggPSBfcmVmLnBhdGgsXG4gICAgICAgIHN0cmljdCA9IF9yZWYuc3RyaWN0LFxuICAgICAgICBleGFjdCA9IF9yZWYuZXhhY3QsXG4gICAgICAgIHNlbnNpdGl2ZSA9IF9yZWYuc2Vuc2l0aXZlO1xuXG4gICAgaWYgKGNvbXB1dGVkTWF0Y2gpIHJldHVybiBjb21wdXRlZE1hdGNoOyAvLyA8U3dpdGNoPiBhbHJlYWR5IGNvbXB1dGVkIHRoZSBtYXRjaCBmb3IgdXNcblxuICAgICgwLCBfaW52YXJpYW50Mi5kZWZhdWx0KShyb3V0ZXIsICdZb3Ugc2hvdWxkIG5vdCB1c2UgPFJvdXRlPiBvciB3aXRoUm91dGVyKCkgb3V0c2lkZSBhIDxSb3V0ZXI+Jyk7XG5cbiAgICB2YXIgcm91dGUgPSByb3V0ZXIucm91dGU7XG5cbiAgICB2YXIgcGF0aG5hbWUgPSAobG9jYXRpb24gfHwgcm91dGUubG9jYXRpb24pLnBhdGhuYW1lO1xuXG4gICAgcmV0dXJuIHBhdGggPyAoMCwgX21hdGNoUGF0aDIuZGVmYXVsdCkocGF0aG5hbWUsIHsgcGF0aDogcGF0aCwgc3RyaWN0OiBzdHJpY3QsIGV4YWN0OiBleGFjdCwgc2Vuc2l0aXZlOiBzZW5zaXRpdmUgfSkgOiByb3V0ZS5tYXRjaDtcbiAgfTtcblxuICBSb3V0ZS5wcm90b3R5cGUuY29tcG9uZW50V2lsbE1vdW50ID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgICgwLCBfd2FybmluZzIuZGVmYXVsdCkoISh0aGlzLnByb3BzLmNvbXBvbmVudCAmJiB0aGlzLnByb3BzLnJlbmRlciksICdZb3Ugc2hvdWxkIG5vdCB1c2UgPFJvdXRlIGNvbXBvbmVudD4gYW5kIDxSb3V0ZSByZW5kZXI+IGluIHRoZSBzYW1lIHJvdXRlOyA8Um91dGUgcmVuZGVyPiB3aWxsIGJlIGlnbm9yZWQnKTtcblxuICAgICgwLCBfd2FybmluZzIuZGVmYXVsdCkoISh0aGlzLnByb3BzLmNvbXBvbmVudCAmJiB0aGlzLnByb3BzLmNoaWxkcmVuICYmICFpc0VtcHR5Q2hpbGRyZW4odGhpcy5wcm9wcy5jaGlsZHJlbikpLCAnWW91IHNob3VsZCBub3QgdXNlIDxSb3V0ZSBjb21wb25lbnQ+IGFuZCA8Um91dGUgY2hpbGRyZW4+IGluIHRoZSBzYW1lIHJvdXRlOyA8Um91dGUgY2hpbGRyZW4+IHdpbGwgYmUgaWdub3JlZCcpO1xuXG4gICAgKDAsIF93YXJuaW5nMi5kZWZhdWx0KSghKHRoaXMucHJvcHMucmVuZGVyICYmIHRoaXMucHJvcHMuY2hpbGRyZW4gJiYgIWlzRW1wdHlDaGlsZHJlbih0aGlzLnByb3BzLmNoaWxkcmVuKSksICdZb3Ugc2hvdWxkIG5vdCB1c2UgPFJvdXRlIHJlbmRlcj4gYW5kIDxSb3V0ZSBjaGlsZHJlbj4gaW4gdGhlIHNhbWUgcm91dGU7IDxSb3V0ZSBjaGlsZHJlbj4gd2lsbCBiZSBpZ25vcmVkJyk7XG4gIH07XG5cbiAgUm91dGUucHJvdG90eXBlLmNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcywgbmV4dENvbnRleHQpIHtcbiAgICAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKCEobmV4dFByb3BzLmxvY2F0aW9uICYmICF0aGlzLnByb3BzLmxvY2F0aW9uKSwgJzxSb3V0ZT4gZWxlbWVudHMgc2hvdWxkIG5vdCBjaGFuZ2UgZnJvbSB1bmNvbnRyb2xsZWQgdG8gY29udHJvbGxlZCAob3IgdmljZSB2ZXJzYSkuIFlvdSBpbml0aWFsbHkgdXNlZCBubyBcImxvY2F0aW9uXCIgcHJvcCBhbmQgdGhlbiBwcm92aWRlZCBvbmUgb24gYSBzdWJzZXF1ZW50IHJlbmRlci4nKTtcblxuICAgICgwLCBfd2FybmluZzIuZGVmYXVsdCkoISghbmV4dFByb3BzLmxvY2F0aW9uICYmIHRoaXMucHJvcHMubG9jYXRpb24pLCAnPFJvdXRlPiBlbGVtZW50cyBzaG91bGQgbm90IGNoYW5nZSBmcm9tIGNvbnRyb2xsZWQgdG8gdW5jb250cm9sbGVkIChvciB2aWNlIHZlcnNhKS4gWW91IHByb3ZpZGVkIGEgXCJsb2NhdGlvblwiIHByb3AgaW5pdGlhbGx5IGJ1dCBvbWl0dGVkIGl0IG9uIGEgc3Vic2VxdWVudCByZW5kZXIuJyk7XG5cbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIG1hdGNoOiB0aGlzLmNvbXB1dGVNYXRjaChuZXh0UHJvcHMsIG5leHRDb250ZXh0LnJvdXRlcilcbiAgICB9KTtcbiAgfTtcblxuICBSb3V0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBtYXRjaCA9IHRoaXMuc3RhdGUubWF0Y2g7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICBjb21wb25lbnQgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICByZW5kZXIgPSBfcHJvcHMucmVuZGVyO1xuICAgIHZhciBfY29udGV4dCRyb3V0ZXIgPSB0aGlzLmNvbnRleHQucm91dGVyLFxuICAgICAgICBoaXN0b3J5ID0gX2NvbnRleHQkcm91dGVyLmhpc3RvcnksXG4gICAgICAgIHJvdXRlID0gX2NvbnRleHQkcm91dGVyLnJvdXRlLFxuICAgICAgICBzdGF0aWNDb250ZXh0ID0gX2NvbnRleHQkcm91dGVyLnN0YXRpY0NvbnRleHQ7XG5cbiAgICB2YXIgbG9jYXRpb24gPSB0aGlzLnByb3BzLmxvY2F0aW9uIHx8IHJvdXRlLmxvY2F0aW9uO1xuICAgIHZhciBwcm9wcyA9IHsgbWF0Y2g6IG1hdGNoLCBsb2NhdGlvbjogbG9jYXRpb24sIGhpc3Rvcnk6IGhpc3RvcnksIHN0YXRpY0NvbnRleHQ6IHN0YXRpY0NvbnRleHQgfTtcblxuICAgIHJldHVybiBjb21wb25lbnQgPyAvLyBjb21wb25lbnQgcHJvcCBnZXRzIGZpcnN0IHByaW9yaXR5LCBvbmx5IGNhbGxlZCBpZiB0aGVyZSdzIGEgbWF0Y2hcbiAgICBtYXRjaCA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KGNvbXBvbmVudCwgcHJvcHMpIDogbnVsbCA6IHJlbmRlciA/IC8vIHJlbmRlciBwcm9wIGlzIG5leHQsIG9ubHkgY2FsbGVkIGlmIHRoZXJlJ3MgYSBtYXRjaFxuICAgIG1hdGNoID8gcmVuZGVyKHByb3BzKSA6IG51bGwgOiBjaGlsZHJlbiA/IC8vIGNoaWxkcmVuIGNvbWUgbGFzdCwgYWx3YXlzIGNhbGxlZFxuICAgIHR5cGVvZiBjaGlsZHJlbiA9PT0gJ2Z1bmN0aW9uJyA/IGNoaWxkcmVuKHByb3BzKSA6ICFpc0VtcHR5Q2hpbGRyZW4oY2hpbGRyZW4pID8gX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm9ubHkoY2hpbGRyZW4pIDogbnVsbCA6IG51bGw7XG4gIH07XG5cbiAgcmV0dXJuIFJvdXRlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuUm91dGUucHJvcFR5cGVzID0ge1xuICBjb21wdXRlZE1hdGNoOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdCwgLy8gcHJpdmF0ZSwgZnJvbSA8U3dpdGNoPlxuICBwYXRoOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcbiAgZXhhY3Q6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgc3RyaWN0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIHNlbnNpdGl2ZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICBjb21wb25lbnQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcbiAgcmVuZGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIGNoaWxkcmVuOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLCBfcHJvcFR5cGVzMi5kZWZhdWx0Lm5vZGVdKSxcbiAgbG9jYXRpb246IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XG59O1xuUm91dGUuY29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgIGhpc3Rvcnk6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgcm91dGU6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgc3RhdGljQ29udGV4dDogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3RcbiAgfSlcbn07XG5Sb3V0ZS5jaGlsZENvbnRleHRUeXBlcyA9IHtcbiAgcm91dGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdC5pc1JlcXVpcmVkXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gUm91dGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL1JvdXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvUm91dGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNSAzNyA0MCA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9wYXRoVG9SZWdleHAgPSByZXF1aXJlKCdwYXRoLXRvLXJlZ2V4cCcpO1xuXG52YXIgX3BhdGhUb1JlZ2V4cDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wYXRoVG9SZWdleHApO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcGF0dGVybkNhY2hlID0ge307XG52YXIgY2FjaGVMaW1pdCA9IDEwMDAwO1xudmFyIGNhY2hlQ291bnQgPSAwO1xuXG52YXIgY29tcGlsZVBhdGggPSBmdW5jdGlvbiBjb21waWxlUGF0aChwYXR0ZXJuLCBvcHRpb25zKSB7XG4gIHZhciBjYWNoZUtleSA9ICcnICsgb3B0aW9ucy5lbmQgKyBvcHRpb25zLnN0cmljdCArIG9wdGlvbnMuc2Vuc2l0aXZlO1xuICB2YXIgY2FjaGUgPSBwYXR0ZXJuQ2FjaGVbY2FjaGVLZXldIHx8IChwYXR0ZXJuQ2FjaGVbY2FjaGVLZXldID0ge30pO1xuXG4gIGlmIChjYWNoZVtwYXR0ZXJuXSkgcmV0dXJuIGNhY2hlW3BhdHRlcm5dO1xuXG4gIHZhciBrZXlzID0gW107XG4gIHZhciByZSA9ICgwLCBfcGF0aFRvUmVnZXhwMi5kZWZhdWx0KShwYXR0ZXJuLCBrZXlzLCBvcHRpb25zKTtcbiAgdmFyIGNvbXBpbGVkUGF0dGVybiA9IHsgcmU6IHJlLCBrZXlzOiBrZXlzIH07XG5cbiAgaWYgKGNhY2hlQ291bnQgPCBjYWNoZUxpbWl0KSB7XG4gICAgY2FjaGVbcGF0dGVybl0gPSBjb21waWxlZFBhdHRlcm47XG4gICAgY2FjaGVDb3VudCsrO1xuICB9XG5cbiAgcmV0dXJuIGNvbXBpbGVkUGF0dGVybjtcbn07XG5cbi8qKlxuICogUHVibGljIEFQSSBmb3IgbWF0Y2hpbmcgYSBVUkwgcGF0aG5hbWUgdG8gYSBwYXRoIHBhdHRlcm4uXG4gKi9cbnZhciBtYXRjaFBhdGggPSBmdW5jdGlvbiBtYXRjaFBhdGgocGF0aG5hbWUpIHtcbiAgdmFyIG9wdGlvbnMgPSBhcmd1bWVudHMubGVuZ3RoID4gMSAmJiBhcmd1bWVudHNbMV0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1sxXSA6IHt9O1xuXG4gIGlmICh0eXBlb2Ygb3B0aW9ucyA9PT0gJ3N0cmluZycpIG9wdGlvbnMgPSB7IHBhdGg6IG9wdGlvbnMgfTtcblxuICB2YXIgX29wdGlvbnMgPSBvcHRpb25zLFxuICAgICAgX29wdGlvbnMkcGF0aCA9IF9vcHRpb25zLnBhdGgsXG4gICAgICBwYXRoID0gX29wdGlvbnMkcGF0aCA9PT0gdW5kZWZpbmVkID8gJy8nIDogX29wdGlvbnMkcGF0aCxcbiAgICAgIF9vcHRpb25zJGV4YWN0ID0gX29wdGlvbnMuZXhhY3QsXG4gICAgICBleGFjdCA9IF9vcHRpb25zJGV4YWN0ID09PSB1bmRlZmluZWQgPyBmYWxzZSA6IF9vcHRpb25zJGV4YWN0LFxuICAgICAgX29wdGlvbnMkc3RyaWN0ID0gX29wdGlvbnMuc3RyaWN0LFxuICAgICAgc3RyaWN0ID0gX29wdGlvbnMkc3RyaWN0ID09PSB1bmRlZmluZWQgPyBmYWxzZSA6IF9vcHRpb25zJHN0cmljdCxcbiAgICAgIF9vcHRpb25zJHNlbnNpdGl2ZSA9IF9vcHRpb25zLnNlbnNpdGl2ZSxcbiAgICAgIHNlbnNpdGl2ZSA9IF9vcHRpb25zJHNlbnNpdGl2ZSA9PT0gdW5kZWZpbmVkID8gZmFsc2UgOiBfb3B0aW9ucyRzZW5zaXRpdmU7XG5cbiAgdmFyIF9jb21waWxlUGF0aCA9IGNvbXBpbGVQYXRoKHBhdGgsIHsgZW5kOiBleGFjdCwgc3RyaWN0OiBzdHJpY3QsIHNlbnNpdGl2ZTogc2Vuc2l0aXZlIH0pLFxuICAgICAgcmUgPSBfY29tcGlsZVBhdGgucmUsXG4gICAgICBrZXlzID0gX2NvbXBpbGVQYXRoLmtleXM7XG5cbiAgdmFyIG1hdGNoID0gcmUuZXhlYyhwYXRobmFtZSk7XG5cbiAgaWYgKCFtYXRjaCkgcmV0dXJuIG51bGw7XG5cbiAgdmFyIHVybCA9IG1hdGNoWzBdLFxuICAgICAgdmFsdWVzID0gbWF0Y2guc2xpY2UoMSk7XG5cbiAgdmFyIGlzRXhhY3QgPSBwYXRobmFtZSA9PT0gdXJsO1xuXG4gIGlmIChleGFjdCAmJiAhaXNFeGFjdCkgcmV0dXJuIG51bGw7XG5cbiAgcmV0dXJuIHtcbiAgICBwYXRoOiBwYXRoLCAvLyB0aGUgcGF0aCBwYXR0ZXJuIHVzZWQgdG8gbWF0Y2hcbiAgICB1cmw6IHBhdGggPT09ICcvJyAmJiB1cmwgPT09ICcnID8gJy8nIDogdXJsLCAvLyB0aGUgbWF0Y2hlZCBwb3J0aW9uIG9mIHRoZSBVUkxcbiAgICBpc0V4YWN0OiBpc0V4YWN0LCAvLyB3aGV0aGVyIG9yIG5vdCB3ZSBtYXRjaGVkIGV4YWN0bHlcbiAgICBwYXJhbXM6IGtleXMucmVkdWNlKGZ1bmN0aW9uIChtZW1vLCBrZXksIGluZGV4KSB7XG4gICAgICBtZW1vW2tleS5uYW1lXSA9IHZhbHVlc1tpbmRleF07XG4gICAgICByZXR1cm4gbWVtbztcbiAgICB9LCB7fSlcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IG1hdGNoUGF0aDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvbWF0Y2hQYXRoLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvbWF0Y2hQYXRoLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzUgMzcgNDAgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfaG9pc3ROb25SZWFjdFN0YXRpY3MgPSByZXF1aXJlKCdob2lzdC1ub24tcmVhY3Qtc3RhdGljcycpO1xuXG52YXIgX2hvaXN0Tm9uUmVhY3RTdGF0aWNzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2hvaXN0Tm9uUmVhY3RTdGF0aWNzKTtcblxudmFyIF9Sb3V0ZSA9IHJlcXVpcmUoJy4vUm91dGUnKTtcblxudmFyIF9Sb3V0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Sb3V0ZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuLyoqXG4gKiBBIHB1YmxpYyBoaWdoZXItb3JkZXIgY29tcG9uZW50IHRvIGFjY2VzcyB0aGUgaW1wZXJhdGl2ZSBBUElcbiAqL1xudmFyIHdpdGhSb3V0ZXIgPSBmdW5jdGlvbiB3aXRoUm91dGVyKENvbXBvbmVudCkge1xuICB2YXIgQyA9IGZ1bmN0aW9uIEMocHJvcHMpIHtcbiAgICB2YXIgd3JhcHBlZENvbXBvbmVudFJlZiA9IHByb3BzLndyYXBwZWRDb21wb25lbnRSZWYsXG4gICAgICAgIHJlbWFpbmluZ1Byb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKHByb3BzLCBbJ3dyYXBwZWRDb21wb25lbnRSZWYnXSk7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX1JvdXRlMi5kZWZhdWx0LCB7IHJlbmRlcjogZnVuY3Rpb24gcmVuZGVyKHJvdXRlQ29tcG9uZW50UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgX2V4dGVuZHMoe30sIHJlbWFpbmluZ1Byb3BzLCByb3V0ZUNvbXBvbmVudFByb3BzLCB7IHJlZjogd3JhcHBlZENvbXBvbmVudFJlZiB9KSk7XG4gICAgICB9IH0pO1xuICB9O1xuXG4gIEMuZGlzcGxheU5hbWUgPSAnd2l0aFJvdXRlcignICsgKENvbXBvbmVudC5kaXNwbGF5TmFtZSB8fCBDb21wb25lbnQubmFtZSkgKyAnKSc7XG4gIEMuV3JhcHBlZENvbXBvbmVudCA9IENvbXBvbmVudDtcbiAgQy5wcm9wVHlwZXMgPSB7XG4gICAgd3JhcHBlZENvbXBvbmVudFJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jXG4gIH07XG5cbiAgcmV0dXJuICgwLCBfaG9pc3ROb25SZWFjdFN0YXRpY3MyLmRlZmF1bHQpKEMsIENvbXBvbmVudCk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3aXRoUm91dGVyO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci93aXRoUm91dGVyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvd2l0aFJvdXRlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM1IDM3IDQwIDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSByZXF1aXJlKCcuL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwnKTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSByZXF1aXJlKCcuL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQnKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRmFjdG9yeSA9IGZ1bmN0aW9uIGNyZWF0ZUZhY3RvcnkodHlwZSkge1xuICB2YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQgPSAoMCwgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyLmRlZmF1bHQpKHR5cGUpO1xuICByZXR1cm4gZnVuY3Rpb24gKHAsIGMpIHtcbiAgICByZXR1cm4gKDAsIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMi5kZWZhdWx0KShmYWxzZSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHAsIGMpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRmFjdG9yeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgZ2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBnZXREaXNwbGF5TmFtZShDb21wb25lbnQpIHtcbiAgaWYgKHR5cGVvZiBDb21wb25lbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfVxuXG4gIGlmICghQ29tcG9uZW50KSB7XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuXG4gIHJldHVybiBDb21wb25lbnQuZGlzcGxheU5hbWUgfHwgQ29tcG9uZW50Lm5hbWUgfHwgJ0NvbXBvbmVudCc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBnZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XG5cbnZhciBpc0NsYXNzQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNDbGFzc0NvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4oQ29tcG9uZW50ICYmIENvbXBvbmVudC5wcm90b3R5cGUgJiYgX3R5cGVvZihDb21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQpID09PSAnb2JqZWN0Jyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc0NsYXNzQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQgPSByZXF1aXJlKCcuL2lzQ2xhc3NDb21wb25lbnQnKTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzQ2xhc3NDb21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IGZ1bmN0aW9uIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKHR5cGVvZiBDb21wb25lbnQgPT09ICdmdW5jdGlvbicgJiYgISgwLCBfaXNDbGFzc0NvbXBvbmVudDIuZGVmYXVsdCkoQ29tcG9uZW50KSAmJiAhQ29tcG9uZW50LmRlZmF1bHRQcm9wcyAmJiAhQ29tcG9uZW50LmNvbnRleHRUeXBlcyAmJiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdwcm9kdWN0aW9uJyB8fCAhQ29tcG9uZW50LnByb3BUeXBlcykpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaG91bGRVcGRhdGUgPSByZXF1aXJlKCcuL3Nob3VsZFVwZGF0ZScpO1xuXG52YXIgX3Nob3VsZFVwZGF0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaG91bGRVcGRhdGUpO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJy4vc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwdXJlID0gZnVuY3Rpb24gcHVyZShCYXNlQ29tcG9uZW50KSB7XG4gIHZhciBob2MgPSAoMCwgX3Nob3VsZFVwZGF0ZTIuZGVmYXVsdCkoZnVuY3Rpb24gKHByb3BzLCBuZXh0UHJvcHMpIHtcbiAgICByZXR1cm4gISgwLCBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0KShwcm9wcywgbmV4dFByb3BzKTtcbiAgfSk7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdwdXJlJykpKGhvYyhCYXNlQ29tcG9uZW50KSk7XG4gIH1cblxuICByZXR1cm4gaG9jKEJhc2VDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gcHVyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zZXRTdGF0aWMgPSByZXF1aXJlKCcuL3NldFN0YXRpYycpO1xuXG52YXIgX3NldFN0YXRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRTdGF0aWMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBzZXREaXNwbGF5TmFtZShkaXNwbGF5TmFtZSkge1xuICByZXR1cm4gKDAsIF9zZXRTdGF0aWMyLmRlZmF1bHQpKCdkaXNwbGF5TmFtZScsIGRpc3BsYXlOYW1lKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBzZXRTdGF0aWMgPSBmdW5jdGlvbiBzZXRTdGF0aWMoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIEJhc2VDb21wb25lbnRba2V5XSA9IHZhbHVlO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICByZXR1cm4gQmFzZUNvbXBvbmVudDtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldFN0YXRpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnZmJqcy9saWIvc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9zaGFsbG93RXF1YWwyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkgPSByZXF1aXJlKCcuL2NyZWF0ZUVhZ2VyRmFjdG9yeScpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckZhY3RvcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBzaG91bGRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRVcGRhdGUodGVzdCkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICB2YXIgZmFjdG9yeSA9ICgwLCBfY3JlYXRlRWFnZXJGYWN0b3J5Mi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KTtcblxuICAgIHZhciBTaG91bGRVcGRhdGUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgICAgX2luaGVyaXRzKFNob3VsZFVwZGF0ZSwgX0NvbXBvbmVudCk7XG5cbiAgICAgIGZ1bmN0aW9uIFNob3VsZFVwZGF0ZSgpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNob3VsZFVwZGF0ZSk7XG5cbiAgICAgICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgICB9XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgICByZXR1cm4gdGVzdCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiBmYWN0b3J5KHRoaXMucHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgICB9KF9yZWFjdC5Db21wb25lbnQpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3Nob3VsZFVwZGF0ZScpKShTaG91bGRVcGRhdGUpO1xuICAgIH1cbiAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2hvdWxkVXBkYXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IGZ1bmN0aW9uIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwoaGFzS2V5LCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcHJvcHMsIGNoaWxkcmVuKSB7XG4gIGlmICghaGFzS2V5ICYmIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50KSB7XG4gICAgaWYgKGNoaWxkcmVuKSB7XG4gICAgICByZXR1cm4gdHlwZShfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgY2hpbGRyZW46IGNoaWxkcmVuIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHR5cGUocHJvcHMpO1xuICB9XG5cbiAgdmFyIENvbXBvbmVudCA9IHR5cGU7XG5cbiAgaWYgKGNoaWxkcmVuKSB7XG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgQ29tcG9uZW50LFxuICAgICAgcHJvcHMsXG4gICAgICBjaGlsZHJlblxuICAgICk7XG4gIH1cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBwcm9wcyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVFYWdlckVsZW1lbnRVdGlsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2dldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9nZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX2dldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHdyYXBEaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHdyYXBEaXNwbGF5TmFtZShCYXNlQ29tcG9uZW50LCBob2NOYW1lKSB7XG4gIHJldHVybiBob2NOYW1lICsgJygnICsgKDAsIF9nZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCkgKyAnKSc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3cmFwRGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IHtcbiAgRUxFTUVOVF9HRVRfU1RBUlQsXG4gIEVMRU1FTlRfR0VUX0VSUk9SLFxuICBFTEVNRU5UX0dFVF9TVUNDRVNTLFxufSBmcm9tICcuLi8uLi9jb25zdGFudHMvZWxlbWVudHMnO1xuXG5pbXBvcnQgYXBpR2V0QnlJZCBmcm9tICcuLi8uLi9hcGkvZWxlbWVudHMvZ2V0QnlJZCc7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5jb25zdCBnZXRFbGVtZW50U3RhcnQgPSAocGF5bG9hZCkgPT4gKHsgdHlwZTogRUxFTUVOVF9HRVRfU1RBUlQsIHBheWxvYWQgfSk7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5zdGF0dXNDb2RlXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudXNlcm5hbWVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmVsZW1lbnRUeXBlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jaXJjbGVUeXBlIC0gb25seSBpZiBlbGVtZW50VHlwZSBpcyAnY2lyY2xlJ1xuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmFtZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5hdmF0YXIgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY292ZXIgLVxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5jb25zdCBnZXRFbGVtZW50U3VjY2VzcyA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UX0dFVF9TVUNDRVNTLCBwYXlsb2FkIH0pO1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnN0YXR1c0NvZGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmVycm9yXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEVsZW1lbnRFcnJvciA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UX0dFVF9FUlJPUiwgcGF5bG9hZCB9KTtcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uIGdldFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICovXG5jb25zdCBnZXQgPSAoeyB0b2tlbiwgaWQgfSkgPT4gKGRpc3BhdGNoKSA9PiB7XG4gIGRpc3BhdGNoKGdldEVsZW1lbnRTdGFydCh7IGlkIH0pKTtcblxuICBhcGlHZXRCeUlkKHsgdG9rZW4sIGlkIH0pXG4gICAgLnRoZW4oKHsgc3RhdHVzQ29kZSwgZXJyb3IsIHBheWxvYWQgfSkgPT4ge1xuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIGRpc3BhdGNoKGdldEVsZW1lbnRFcnJvcih7IGlkLCBzdGF0dXNDb2RlLCBlcnJvciB9KSk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBkaXNwYXRjaChnZXRFbGVtZW50U3VjY2Vzcyh7IGlkLCBzdGF0dXNDb2RlLCAuLi5wYXlsb2FkIH0pKTtcbiAgICB9KVxuICAgIC5jYXRjaCgoeyBzdGF0dXNDb2RlLCBlcnJvciB9KSA9PiB7XG4gICAgICBkaXNwYXRjaChnZXRFbGVtZW50RXJyb3IoeyBpZCwgc3RhdHVzQ29kZSwgZXJyb3IgfSkpO1xuICAgIH0pO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgZ2V0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2VsZW1lbnRzL2dldEJ5SWQuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgUE9TVF9ERUxFVEUgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvcG9zdHMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHBvc3RJZDogbnVtYmVyLFxuICBhdXRob3JJZDogbnVtYmVyLFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICpcbiAqIEBmdW5jdGlvbiBkZWxldGVQb3N0XG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucG9zdElkXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICovXG5jb25zdCBkZWxldGVQb3N0ID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHtcbiAgdHlwZTogUE9TVF9ERUxFVEUsXG4gIHBheWxvYWQsXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgZGVsZXRlUG9zdDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9wb3N0cy9kZWxldGUuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vY29uZmlnJztcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uIGdldEJ5SWRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QnlJZCh7IHRva2VuLCBpZCB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL2VsZW1lbnRzLyR7aWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRCeUlkO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvZWxlbWVudHMvZ2V0QnlJZC5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZpbGUgZGVsZXRlIHBvc3Qgb2YgdXNlci5cbiAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcblxuLyoqXG4gKiBkZWxldGUgcG9zdCBvZiB1c2VyXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBkZWxldGVQb3N0XG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucG9zdElkIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBkZWxldGVQb3N0KHsgdXNlcklkLCB0b2tlbiwgcG9zdElkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L3Bvc3RzLyR7cG9zdElkfWA7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0RFTEVURScsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZGVsZXRlUG9zdDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3Bvc3RzL3VzZXJzL2RlbGV0ZS5qcyIsIi8qKlxuICogUG9zdCBjb250YWluZXIgY29tcG9uZW50XG4gKiBJdCB3aWxsIGRpc3BsYXkgb25lIHBvc3Qgd2l0aCBzb21lIG9mIGFjdGlvbnMgKGVnLiBCb29rbWFya3MsIFNoYXJlLCBEZWxldGUpXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHdpdGhSb3V0ZXIgZnJvbSAncmVhY3Qtcm91dGVyL3dpdGhSb3V0ZXInO1xuaW1wb3J0IExpbmsgZnJvbSAncmVhY3Qtcm91dGVyLWRvbS9MaW5rJztcblxuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmltcG9ydCBDYXJkLCB7XG4gIENhcmRIZWFkZXIsXG4gIENhcmRNZWRpYSxcbiAgQ2FyZENvbnRlbnQsXG4gIENhcmRBY3Rpb25zLFxufSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCBEaWFsb2csIHsgRGlhbG9nQWN0aW9ucywgRGlhbG9nVGl0bGUgfSBmcm9tICdtYXRlcmlhbC11aS9EaWFsb2cnO1xuaW1wb3J0IEF2YXRhciBmcm9tICdtYXRlcmlhbC11aS9BdmF0YXInO1xuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnbWF0ZXJpYWwtdWkvVHlwb2dyYXBoeSc7XG5pbXBvcnQgQmFkZ2UgZnJvbSAnbWF0ZXJpYWwtdWkvQmFkZ2UnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgUGFwZXIgZnJvbSAnbWF0ZXJpYWwtdWkvUGFwZXInO1xuaW1wb3J0IFRvb2x0aXAgZnJvbSAnbWF0ZXJpYWwtdWkvVG9vbHRpcCc7XG5cbmltcG9ydCBNb3JlVmVydEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQnO1xuaW1wb3J0IEZhdm9yaXRlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZSc7XG5pbXBvcnQgVHVybmVkSW5JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluJztcbmltcG9ydCBDb21tZW50SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Db21tZW50JztcbmltcG9ydCBEZWxldGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0RlbGV0ZSc7XG5pbXBvcnQgUmVtb3ZlUmVkRXllSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9SZW1vdmVSZWRFeWUnO1xuaW1wb3J0IFNoYXJlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9TaGFyZSc7XG5cbi8vIEFsbCBhY3Rpb25zIGFyZSBkZWZpbmVkIGhlcmUuXG5pbXBvcnQgZ2V0RWxlbWVudEJ5SWQgZnJvbSAnLi4vLi4vYWN0aW9ucy9lbGVtZW50cy9nZXRCeUlkJztcblxuaW1wb3J0IGFwaVBvc3REZWxldGUgZnJvbSAnLi4vLi4vYXBpL3Bvc3RzL3VzZXJzL2RlbGV0ZSc7XG5pbXBvcnQgYWN0aW9uUG9zdERlbGV0ZSBmcm9tICcuLi8uLi9hY3Rpb25zL3Bvc3RzL2RlbGV0ZSc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICB0aXRsZToge1xuICAgIC8vIGNvbG9yOiAnIzM2NTg5OScsXG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgc3ViaGVhZGVyOiB7XG4gICAgLy8gY29sb3I6ICcjMzY1ODk5JyxcbiAgICBjdXJzb3I6ICdwb2ludGVyJyxcbiAgICB0ZXh0RGVjb3JhdGlvbjogJ25vbmUnLFxuICB9LFxuICBtZWRpYToge1xuICAgIGhlaWdodDogMTk0LFxuICB9LFxuICBmYXZvcml0ZUljb25BY3RpdmU6IHtcbiAgICBjb2xvcjogJyNGNTAwNTcnLFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG4gIGJhZGdlOiB7XG4gICAgbWFyZ2luOiBgMCAke3RoZW1lLnNwYWNpbmcudW5pdCAqIDJ9cHhgLFxuICAgIGNvbG9yOiAnYmx1ZScsXG4gIH0sXG4gIG1hcmdpbjoge1xuICAgIGJvcmRlcjogJzAuNXB4IHNvbGlkIHJnYmEoMjA5LDIwOSwyMDksMSknLFxuICAgIG1hcmdpbkJvdHRvbTogJy0xMHB4JyxcbiAgfSxcbiAgY29udGVudDoge1xuICAgIHRleHRBbGlnbjogJ2p1c3RpZnknLFxuICB9LFxufSk7XG5cbmNsYXNzIFBvc3QgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgaG92ZXI6IGZhbHNlLFxuICAgICAgZmF2b3JpdGU6IGZhbHNlLFxuICAgICAgZGVsZXRlRGlhbG9nOiBmYWxzZSxcbiAgICB9O1xuICB9XG5cbiAgLy8gY29tcG9uZW50V2lsbE1vdW50IGlzIGV4ZWN1dGVkIGJlZm9yZSByZW5kZXJpbmdcbiAgY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgIGNvbnN0IHsgZWxlbWVudHMsIHBvc3RzLCBwb3N0SWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyB0b2tlbiwgaWQ6IHVzZXJJZCB9ID0gZWxlbWVudHMudXNlcjtcbiAgICBjb25zdCB7IGF1dGhvcklkIH0gPSBwb3N0c1twb3N0SWRdO1xuXG4gICAgY29uc3QgYXV0aG9yID0gZWxlbWVudHNbYXV0aG9ySWRdO1xuXG4gICAgLy8gaWYgYXV0aG9yIGlzIG5vdCBmb3VuZFxuICAgIGlmICh1c2VySWQgIT09IGF1dGhvcklkICYmICFhdXRob3IpIHtcbiAgICAgIHRoaXMucHJvcHMuZ2V0RWxlbWVudEJ5SWQoeyBpZDogYXV0aG9ySWQsIHRva2VuIH0pO1xuICAgIH1cbiAgfVxuXG4gIG9uQ2xpY2tGYXZvcml0ZSA9IGFzeW5jICgpID0+IHtcbiAgICBjb25zdCB7IGZhdm9yaXRlIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7IGZhdm9yaXRlOiAhZmF2b3JpdGUgfSk7XG4gIH07XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuXG4gIG9uQ2xpY2tQb3N0ID0gKCkgPT4gdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goYC9wb3N0cy8ke3RoaXMucHJvcHMucG9zdElkfWApO1xuXG4gIGRlbGV0ZURpYWxvZ09wZW4gPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGRlbGV0ZURpYWxvZzogdHJ1ZSB9KTtcbiAgfTtcblxuICBkZWxldGVEaWFsb2dDbG9zZSA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgZGVsZXRlRGlhbG9nOiBmYWxzZSB9KTtcbiAgfTtcblxuICBkZWxldGVQb3N0Q2xpY2sgPSBhc3luYyAoKSA9PiB7XG4gICAgY29uc3QgeyB0b2tlbiwgaWQgfSA9IHRoaXMucHJvcHMuZWxlbWVudHMudXNlcjtcbiAgICBjb25zdCB7IHBvc3RJZCB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IgfSA9IGF3YWl0IGFwaVBvc3REZWxldGUoe1xuICAgICAgdG9rZW4sXG4gICAgICB1c2VySWQ6IGlkLFxuICAgICAgcG9zdElkLFxuICAgIH0pO1xuXG4gICAgaWYgKHN0YXR1c0NvZGUgIT09IDIwMCkge1xuICAgICAgLy8gc2hvdyBzb21lIGVycm9yIG1lc3NhZ2UuXG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25Qb3N0RGVsZXRlKHsgcG9zdElkIH0pO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IGZhbHNlIH0pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHBvc3RzLCBlbGVtZW50cywgcG9zdElkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgdXNlciB9ID0gZWxlbWVudHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluLCBpZDogdXNlcklkIH0gPSB1c2VyO1xuXG4gICAgY29uc3QgcG9zdCA9IHBvc3RzW3Bvc3RJZF07XG5cbiAgICBjb25zdCB7IGZhdm9yaXRlIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgaWYgKHR5cGVvZiBwb3N0ID09PSAndW5kZWZpbmVkJykge1xuICAgICAgcmV0dXJuIDxkaXYgLz47XG4gICAgfVxuXG4gICAgY29uc3QgeyBjb3ZlciwgdGl0bGUsIGRlc2NyaXB0aW9uLCBhdXRob3JJZCB9ID0gcG9zdDtcbiAgICBjb25zdCBhdXRob3IgPSBhdXRob3JJZCA9PT0gdXNlcklkID8gdXNlciA6IGVsZW1lbnRzW2F1dGhvcklkXTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgPlxuICAgICAgICA8Q2FyZCByYWlzZWQ9e3RoaXMuc3RhdGUuaG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgPFBhcGVyIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9IGVsZXZhdGlvbj17MX0+XG4gICAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7YXV0aG9yLnVzZXJuYW1lfWB9PlxuICAgICAgICAgICAgICAgICAgPEF2YXRhclxuICAgICAgICAgICAgICAgICAgICBhbHQ9e2F1dGhvci5uYW1lfVxuICAgICAgICAgICAgICAgICAgICBzcmM9eycvcHVibGljL3Bob3Rvcy9tYXlhc2gtbG9nby10cmFuc3BhcmVudC5wbmcnfVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIDwvUGFwZXI+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBhY3Rpb249e1xuICAgICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvbWluZyBzb29uXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgPEljb25CdXR0b24gZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgICA8TW9yZVZlcnRJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfSBjbGFzc05hbWU9e2NsYXNzZXMudGl0bGV9PlxuICAgICAgICAgICAgICAgIHthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc3ViaGVhZGVyPXtcbiAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7YXV0aG9yLnVzZXJuYW1lfWB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5zdWJoZWFkZXJ9PlxuICAgICAgICAgICAgICAgIEB7YXV0aG9yLnVzZXJuYW1lfVxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Q2FyZE1lZGlhXG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMubWVkaWF9XG4gICAgICAgICAgICBpbWFnZT17XG4gICAgICAgICAgICAgIGNvdmVyIHx8XG4gICAgICAgICAgICAgICdodHRwOi8vZ2ludmEuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzA4L2dpbnZhXzIwMTYtMDgtMDJfMDItMzAtNTQuanBnJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGl0bGU9e3RpdGxlfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPENhcmRDb250ZW50IG9uQ2xpY2s9e3RoaXMub25DbGlja1Bvc3R9PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cInRpdGxlXCI+e3RpdGxlfTwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIHt0eXBlb2YgZGVzY3JpcHRpb24gPT09ICdzdHJpbmcnID8gKFxuICAgICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwiYm9keTFcIiBndXR0ZXJCb3R0b20gY2xhc3NOYW1lPXtjbGFzc2VzLmNvbnRlbnR9PlxuICAgICAgICAgICAgICAgIHtkZXNjcmlwdGlvbn1cbiAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMubWFyZ2lufSAvPiAqL31cbiAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICA8SWNvbkJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tGYXZvcml0ZX0+XG4gICAgICAgICAgICAgIDxCYWRnZSBiYWRnZUNvbnRlbnQ9ezB9PlxuICAgICAgICAgICAgICAgIDxGYXZvcml0ZUljb25cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NuYW1lcyh7XG4gICAgICAgICAgICAgICAgICAgIFtgJHtjbGFzc2VzLmZhdm9yaXRlSWNvbkFjdGl2ZX1gXTogZmF2b3JpdGUsXG4gICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L0JhZGdlPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkNvbW1lbnRcIj5cbiAgICAgICAgICAgICAgPEJhZGdlIGJhZGdlQ29udGVudD17MH0+XG4gICAgICAgICAgICAgICAgPENvbW1lbnRJY29uIC8+XG4gICAgICAgICAgICAgIDwvQmFkZ2U+XG4gICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQm9vayBNYXJrc1wiPlxuICAgICAgICAgICAgICA8VHVybmVkSW5JY29uIC8+XG4gICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICB7aXNTaWduZWRJbiAmJiB1c2VySWQgPT09IGF1dGhvcklkID8gKFxuICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJEZWxldGVcIiBvbkNsaWNrPXt0aGlzLmRlbGV0ZURpYWxvZ09wZW59PlxuICAgICAgICAgICAgICAgICAgPERlbGV0ZUljb24gLz5cbiAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgICAgPERpYWxvZ1xuICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5kZWxldGVEaWFsb2d9XG4gICAgICAgICAgICAgICAgICBvbkNsb3NlPXt0aGlzLmRlbGV0ZURpYWxvZ0Nsb3NlfVxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PXtgcG9zdC0ke3Bvc3RJZH0tZGVsZXRlLWRpYWxvZ2B9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPERpYWxvZ1RpdGxlIGlkPXtgcG9zdC0ke3Bvc3RJZH0tZGVsZXRlLWRpYWxvZ2B9PlxuICAgICAgICAgICAgICAgICAgICBBcmUgeW91IHN1cmU/XG4gICAgICAgICAgICAgICAgICA8L0RpYWxvZ1RpdGxlPlxuICAgICAgICAgICAgICAgICAgPERpYWxvZ0FjdGlvbnM+XG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b24gb25DbGljaz17dGhpcy5kZWxldGVEaWFsb2dDbG9zZX0gY29sb3I9XCJwcmltYXJ5XCI+XG4gICAgICAgICAgICAgICAgICAgICAgQ2FuY2VsXG4gICAgICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5kZWxldGVQb3N0Q2xpY2t9XG4gICAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgICAgICAgICBhdXRvRm9jdXNcbiAgICAgICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgICAgIERlbGV0ZVxuICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgICAgICAgIDwvRGlhbG9nQWN0aW9ucz5cbiAgICAgICAgICAgICAgICA8L0RpYWxvZz5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlNoYXJlXCI+XG4gICAgICAgICAgICAgIDxCYWRnZSBiYWRnZUNvbnRlbnQ9ezB9PlxuICAgICAgICAgICAgICAgIDxSZW1vdmVSZWRFeWVJY29uIC8+XG4gICAgICAgICAgICAgIDwvQmFkZ2U+XG4gICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiU2hhcmVcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgPFNoYXJlSWNvbiAvPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgIDwvQ2FyZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuUG9zdC5wcm9wVHlwZXMgPSB7XG4gIGhpc3Rvcnk6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgcG9zdHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAvLyBwb3N0OiBQcm9wVHlwZXMuc2hhcGUoe1xuICAvLyAgIGF1dGhvcklkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gIC8vICAgcG9zdElkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gIC8vICAgdGl0bGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgLy8gICBkZXNjcmlwdGlvbjogUHJvcFR5cGVzLnN0cmluZyxcbiAgLy8gfSkuaXNSZXF1aXJlZCxcblxuICBwb3N0SWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcblxuICBnZXRFbGVtZW50QnlJZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgYWN0aW9uUG9zdERlbGV0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzLCBwb3N0cyB9KSA9PiAoeyBlbGVtZW50cywgcG9zdHMgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGdldEVsZW1lbnRCeUlkLFxuICAgICAgYWN0aW9uUG9zdERlbGV0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKHdpdGhSb3V0ZXIoUG9zdCkpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0L2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==