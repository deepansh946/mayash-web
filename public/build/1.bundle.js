webpackJsonp([1],{

/***/ "./node_modules/dom-helpers/activeElement.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/activeElement.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = activeElement;

var _ownerDocument = __webpack_require__(/*! ./ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function activeElement() {
  var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _ownerDocument2.default)();

  try {
    return doc.activeElement;
  } catch (e) {/* ie throws if no active element */}
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/class/addClass.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/class/addClass.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = addClass;

var _hasClass = __webpack_require__(/*! ./hasClass */ "./node_modules/dom-helpers/class/hasClass.js");

var _hasClass2 = _interopRequireDefault(_hasClass);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function addClass(element, className) {
  if (element.classList) element.classList.add(className);else if (!(0, _hasClass2.default)(element, className)) if (typeof element.className === 'string') element.className = element.className + ' ' + className;else element.setAttribute('class', (element.className && element.className.baseVal || '') + ' ' + className);
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/class/hasClass.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/class/hasClass.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = hasClass;
function hasClass(element, className) {
  if (element.classList) return !!className && element.classList.contains(className);else return (" " + (element.className.baseVal || element.className) + " ").indexOf(" " + className + " ") !== -1;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/class/removeClass.js":
/*!*******************************************************!*\
  !*** ./node_modules/dom-helpers/class/removeClass.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function replaceClassName(origClass, classToRemove) {
  return origClass.replace(new RegExp('(^|\\s)' + classToRemove + '(?:\\s|$)', 'g'), '$1').replace(/\s+/g, ' ').replace(/^\s*|\s*$/g, '');
}

module.exports = function removeClass(element, className) {
  if (element.classList) element.classList.remove(className);else if (typeof element.className === 'string') element.className = replaceClassName(element.className, className);else element.setAttribute('class', replaceClassName(element.className && element.className.baseVal || '', className));
};

/***/ }),

/***/ "./node_modules/dom-helpers/ownerDocument.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/ownerDocument.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ownerDocument;
function ownerDocument(node) {
  return node && node.ownerDocument || document;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/query/isWindow.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/query/isWindow.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getWindow;
function getWindow(node) {
  return node === node.window ? node : node.nodeType === 9 ? node.defaultView || node.parentWindow : false;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/util/scrollbarSize.js":
/*!********************************************************!*\
  !*** ./node_modules/dom-helpers/util/scrollbarSize.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (recalc) {
  if (!size && size !== 0 || recalc) {
    if (_inDOM2.default) {
      var scrollDiv = document.createElement('div');

      scrollDiv.style.position = 'absolute';
      scrollDiv.style.top = '-9999px';
      scrollDiv.style.width = '50px';
      scrollDiv.style.height = '50px';
      scrollDiv.style.overflow = 'scroll';

      document.body.appendChild(scrollDiv);
      size = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
    }
  }

  return size;
};

var _inDOM = __webpack_require__(/*! ./inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = void 0;

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/material-ui-icons/Menu.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Menu.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z' });

var Menu = function Menu(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Menu = (0, _pure2.default)(Menu);
Menu.muiName = 'SvgIcon';

exports.default = Menu;

/***/ }),

/***/ "./node_modules/material-ui-icons/MoreVert.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/MoreVert.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });

var MoreVert = function MoreVert(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

MoreVert = (0, _pure2.default)(MoreVert);
MoreVert.muiName = 'SvgIcon';

exports.default = MoreVert;

/***/ }),

/***/ "./node_modules/material-ui-icons/Search.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui-icons/Search.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z' });

var Search = function Search(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Search = (0, _pure2.default)(Search);
Search.muiName = 'SvgIcon';

exports.default = Search;

/***/ }),

/***/ "./node_modules/material-ui/AppBar/AppBar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/AppBar/AppBar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Paper

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      boxSizing: 'border-box', // Prevent padding issue with the Modal and fixed positioned AppBar.
      zIndex: theme.zIndex.appBar,
      flexShrink: 0
    },
    positionFixed: {
      position: 'fixed',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionAbsolute: {
      position: 'absolute',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionStatic: {
      position: 'static',
      flexShrink: 0
    },
    colorDefault: {
      backgroundColor: theme.palette.background.appBar,
      color: theme.palette.getContrastText(theme.palette.background.appBar)
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']);

var babelPluginFlowReactPropTypes_proptype_Position = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']).isRequired,

  /**
   * The positioning type.
   */
  position: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']).isRequired
};

var AppBar = function (_React$Component) {
  (0, _inherits3.default)(AppBar, _React$Component);

  function AppBar() {
    (0, _classCallCheck3.default)(this, AppBar);
    return (0, _possibleConstructorReturn3.default)(this, (AppBar.__proto__ || (0, _getPrototypeOf2.default)(AppBar)).apply(this, arguments));
  }

  (0, _createClass3.default)(AppBar, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          position = _props.position,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'position']);


      var className = (0, _classnames2.default)(classes.root, classes['position' + (0, _helpers.capitalizeFirstLetter)(position)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), (0, _defineProperty3.default)(_classNames, 'mui-fixed', position === 'fixed'), _classNames), classNameProp);

      return _react2.default.createElement(
        _Paper2.default,
        (0, _extends3.default)({ square: true, component: 'header', elevation: 4, className: className }, other),
        children
      );
    }
  }]);
  return AppBar;
}(_react2.default.Component);

AppBar.defaultProps = {
  color: 'primary',
  position: 'fixed'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAppBar' })(AppBar);

/***/ }),

/***/ "./node_modules/material-ui/AppBar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/AppBar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppBar = __webpack_require__(/*! ./AppBar */ "./node_modules/material-ui/AppBar/AppBar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_AppBar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Icon/Icon.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Icon/Icon.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      userSelect: 'none'
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The name of the icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired
};

var Icon = function (_React$Component) {
  (0, _inherits3.default)(Icon, _React$Component);

  function Icon() {
    (0, _classCallCheck3.default)(this, Icon);
    return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
  }

  (0, _createClass3.default)(Icon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color']);


      var className = (0, _classnames2.default)('material-icons', classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'span',
        (0, _extends3.default)({ className: className, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Icon;
}(_react2.default.Component);

Icon.defaultProps = {
  color: 'inherit'
};
Icon.muiName = 'Icon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIcon' })(Icon);

/***/ }),

/***/ "./node_modules/material-ui/Icon/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Icon/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/material-ui/Icon/Icon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Icon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/List/List.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/List/List.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      flex: '1 1 auto',
      listStyle: 'none',
      margin: 0,
      padding: 0,
      position: 'relative'
    },
    padding: {
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit
    },
    dense: {
      paddingTop: theme.spacing.unit / 2,
      paddingBottom: theme.spacing.unit / 2
    },
    subheader: {
      paddingTop: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, compact vertical padding designed for keyboard and mouse input will be used for
   * the list and list items. The property is available to descendant components as the
   * `dense` context.
   */
  dense: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, vertical padding will be removed from the list.
   */
  disablePadding: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The content of the component, normally `ListItem`.
   */
  subheader: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
};

var List = function (_React$Component) {
  (0, _inherits3.default)(List, _React$Component);

  function List() {
    (0, _classCallCheck3.default)(this, List);
    return (0, _possibleConstructorReturn3.default)(this, (List.__proto__ || (0, _getPrototypeOf2.default)(List)).apply(this, arguments));
  }

  (0, _createClass3.default)(List, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        dense: this.props.dense
      };
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          ComponentProp = _props.component,
          disablePadding = _props.disablePadding,
          children = _props.children,
          dense = _props.dense,
          subheader = _props.subheader,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'component', 'disablePadding', 'children', 'dense', 'subheader', 'rootRef']);

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.dense, dense && !disablePadding), (0, _defineProperty3.default)(_classNames, classes.padding, !disablePadding), (0, _defineProperty3.default)(_classNames, classes.subheader, subheader), _classNames), classNameProp);

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other, { ref: rootRef }),
        subheader,
        children
      );
    }
  }]);
  return List;
}(_react2.default.Component);

List.defaultProps = {
  component: 'ul',
  dense: false,
  disablePadding: false
};


List.childContextTypes = {
  dense: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiList' })(List);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItem.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/List/ListItem.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      position: 'relative',
      textDecoration: 'none'
    },
    container: {
      position: 'relative'
    },
    keyboardFocused: {
      background: theme.palette.text.divider
    },
    default: {
      paddingTop: 12,
      paddingBottom: 12
    },
    dense: {
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit
    },
    disabled: {
      opacity: 0.5
    },
    divider: {
      borderBottom: '1px solid ' + theme.palette.text.lightDivider
    },
    gutters: {
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    button: {
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      }),
      '&:hover': {
        textDecoration: 'none',
        backgroundColor: theme.palette.text.divider,
        // Reset on mouse devices
        '@media (hover: none)': {
          backgroundColor: 'transparent'
        },
        '&$disabled': {
          backgroundColor: 'transparent'
        }
      }
    },
    secondaryAction: {
      // Add some space to avoid collision as `ListItemSecondaryAction`
      // is absolutely positionned.
      paddingRight: theme.spacing.unit * 4
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * If `true`, the ListItem will be a button.
   */
  button: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, compact vertical padding designed for keyboard and mouse input will be used.
   */
  dense: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the left and right padding is removed.
   */
  disableGutters: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, a 1px light border is added to the bottom of the list item.
   */
  divider: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var ListItem = function (_React$Component) {
  (0, _inherits3.default)(ListItem, _React$Component);

  function ListItem() {
    (0, _classCallCheck3.default)(this, ListItem);
    return (0, _possibleConstructorReturn3.default)(this, (ListItem.__proto__ || (0, _getPrototypeOf2.default)(ListItem)).apply(this, arguments));
  }

  (0, _createClass3.default)(ListItem, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        dense: this.props.dense || this.context.dense || false
      };
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          button = _props.button,
          childrenProp = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          componentProp = _props.component,
          dense = _props.dense,
          disabled = _props.disabled,
          divider = _props.divider,
          disableGutters = _props.disableGutters,
          other = (0, _objectWithoutProperties3.default)(_props, ['button', 'children', 'classes', 'className', 'component', 'dense', 'disabled', 'divider', 'disableGutters']);

      var isDense = dense || this.context.dense || false;
      var children = _react2.default.Children.toArray(childrenProp);

      var hasAvatar = children.some(function (value) {
        return (0, _reactHelpers.isMuiElement)(value, ['ListItemAvatar']);
      });
      var hasSecondaryAction = children.length && (0, _reactHelpers.isMuiElement)(children[children.length - 1], ['ListItemSecondaryAction']);

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.gutters, !disableGutters), (0, _defineProperty3.default)(_classNames, classes.divider, divider), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), (0, _defineProperty3.default)(_classNames, classes.button, button), (0, _defineProperty3.default)(_classNames, classes.secondaryAction, hasSecondaryAction), (0, _defineProperty3.default)(_classNames, isDense || hasAvatar ? classes.dense : classes.default, true), _classNames), classNameProp);

      var listItemProps = (0, _extends3.default)({ className: className, disabled: disabled }, other);
      var ComponentMain = componentProp;

      if (button) {
        ComponentMain = _ButtonBase2.default;
        listItemProps.component = componentProp || 'li';
        listItemProps.keyboardFocusedClassName = classes.keyboardFocused;
      }

      if (hasSecondaryAction) {
        return _react2.default.createElement(
          'div',
          { className: classes.container },
          _react2.default.createElement(
            ComponentMain,
            listItemProps,
            children
          ),
          children.pop()
        );
      }

      return _react2.default.createElement(
        ComponentMain,
        listItemProps,
        children
      );
    }
  }]);
  return ListItem;
}(_react2.default.Component);

ListItem.defaultProps = {
  button: false,
  component: 'li',
  dense: false,
  disabled: false,
  disableGutters: false,
  divider: false
};


ListItem.contextTypes = {
  dense: _propTypes2.default.bool
};

ListItem.childContextTypes = {
  dense: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItem' })(ListItem);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemAvatar.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemAvatar.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      width: 36,
      height: 36,
      fontSize: theme.typography.pxToRem(18),
      marginRight: 4
    },
    icon: {
      width: 20,
      height: 20,
      fontSize: theme.typography.pxToRem(20)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component, normally `Avatar`.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


/**
 * It's a simple wrapper to apply the `dense` mode styles to `Avatar`.
 */
function ListItemAvatar(props, context) {
  if (context.dense === undefined) {
     true ? (0, _warning2.default)(false, 'Material-UI: <ListItemAvatar> is a simple wrapper to apply the dense styles\n      to <Avatar>. You do not need it unless you are controlling the <List> dense property.') : void 0;
    return props.children;
  }

  var children = props.children,
      classes = props.classes,
      classNameProp = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.cloneElement(children, (0, _extends3.default)({
    className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.root, context.dense), classNameProp, children.props.className),
    childrenClassName: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.icon, context.dense), children.props.childrenClassName)
  }, other));
}

ListItemAvatar.contextTypes = {
  dense: _propTypes2.default.bool
};

ListItemAvatar.muiName = 'ListItemAvatar';

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemAvatar' })(ListItemAvatar);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemIcon.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemIcon.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      height: 24,
      marginRight: theme.spacing.unit * 2,
      width: 24,
      color: theme.palette.action.active,
      flexShrink: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component, normally `Icon`, `SvgIcon`,
   * or a `material-ui-icons` SVG icon component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


/**
 * A simple wrapper to apply `List` styles to an `Icon` or `SvgIcon`.
 */
function ListItemIcon(props) {
  var children = props.children,
      classes = props.classes,
      classNameProp = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.cloneElement(children, (0, _extends3.default)({
    className: (0, _classnames2.default)(classes.root, classNameProp, children.props.className)
  }, other));
}

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemIcon' })(ListItemIcon);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemSecondaryAction.js":
/*!******************************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemSecondaryAction.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _ref; //  weak

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      right: 4,
      top: '50%',
      marginTop: -theme.spacing.unit * 3
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component, normally an `IconButton` or selection control.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function ListItemSecondaryAction(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className;


  return _react2.default.createElement(
    'div',
    { className: (0, _classnames2.default)(classes.root, className) },
    children
  );
}

ListItemSecondaryAction.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
ListItemSecondaryAction.muiName = 'ListItemSecondaryAction';

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemSecondaryAction' })(ListItemSecondaryAction);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemText.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemText.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! ../Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      flex: '1 1 auto',
      padding: '0 16px',
      '&:first-child': {
        paddingLeft: 0
      }
    },
    inset: {
      '&:first-child': {
        paddingLeft: theme.spacing.unit * 7
      }
    },
    dense: {
      fontSize: theme.typography.pxToRem(13)
    },
    text: {}, // Present to allow external customization
    textDense: {
      fontSize: 'inherit'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the children won't be wrapped by a typography component.
   * For instance, that can be useful to can render an h4 instead of a
   */
  disableTypography: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the children will be indented.
   * This should be used if there is no left avatar or left icon.
   */
  inset: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,
  primary: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,
  secondary: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired
};

var ListItemText = function (_React$Component) {
  (0, _inherits3.default)(ListItemText, _React$Component);

  function ListItemText() {
    (0, _classCallCheck3.default)(this, ListItemText);
    return (0, _possibleConstructorReturn3.default)(this, (ListItemText.__proto__ || (0, _getPrototypeOf2.default)(ListItemText)).apply(this, arguments));
  }

  (0, _createClass3.default)(ListItemText, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          disableTypography = _props.disableTypography,
          primary = _props.primary,
          secondary = _props.secondary,
          inset = _props.inset,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'disableTypography', 'primary', 'secondary', 'inset']);
      var dense = this.context.dense;

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.dense, dense), (0, _defineProperty3.default)(_classNames, classes.inset, inset), _classNames), classNameProp);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        primary && (disableTypography ? primary : _react2.default.createElement(
          _Typography2.default,
          {
            type: 'subheading',
            className: (0, _classnames2.default)(classes.text, (0, _defineProperty3.default)({}, classes.textDense, dense))
          },
          primary
        )),
        secondary && (disableTypography ? secondary : _react2.default.createElement(
          _Typography2.default,
          {
            color: 'secondary',
            type: 'body1',
            className: (0, _classnames2.default)(classes.text, (0, _defineProperty3.default)({}, classes.textDense, dense))
          },
          secondary
        ))
      );
    }
  }]);
  return ListItemText;
}(_react2.default.Component);

ListItemText.defaultProps = {
  disableTypography: false,
  primary: false,
  secondary: false,
  inset: false
};
ListItemText.contextTypes = {
  dense: _propTypes2.default.bool
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemText' })(ListItemText);

/***/ }),

/***/ "./node_modules/material-ui/List/ListSubheader.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/List/ListSubheader.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      boxSizing: 'border-box',
      lineHeight: '48px',
      listStyle: 'none',
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      color: theme.palette.text.secondary,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeightMedium,
      fontSize: theme.typography.pxToRem(theme.typography.fontSize)
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    inset: {
      paddingLeft: theme.spacing.unit * 9
    },
    sticky: {
      position: 'sticky',
      top: 0,
      zIndex: 1,
      backgroundColor: 'inherit'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   * The default value is a `button`.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'primary', 'inherit']),

  /**
   * If `true`, the List Subheader will not stick to the top during scroll.
   */
  disableSticky: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the List Subheader will be indented.
   */
  inset: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var ListSubheader = function (_React$Component) {
  (0, _inherits3.default)(ListSubheader, _React$Component);

  function ListSubheader() {
    (0, _classCallCheck3.default)(this, ListSubheader);
    return (0, _possibleConstructorReturn3.default)(this, (ListSubheader.__proto__ || (0, _getPrototypeOf2.default)(ListSubheader)).apply(this, arguments));
  }

  (0, _createClass3.default)(ListSubheader, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          ComponentProp = _props.component,
          color = _props.color,
          disableSticky = _props.disableSticky,
          inset = _props.inset,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'component', 'color', 'disableSticky', 'inset']);

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.inset, inset), (0, _defineProperty3.default)(_classNames, classes.sticky, !disableSticky), _classNames), classNameProp);

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return ListSubheader;
}(_react2.default.Component);

ListSubheader.defaultProps = {
  component: 'li',
  color: 'default',
  disableSticky: false,
  inset: false
};
ListSubheader.muiName = 'ListSubheader';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListSubheader' })(ListSubheader);

/***/ }),

/***/ "./node_modules/material-ui/List/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/List/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _List = __webpack_require__(/*! ./List */ "./node_modules/material-ui/List/List.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_List).default;
  }
});

var _ListItem = __webpack_require__(/*! ./ListItem */ "./node_modules/material-ui/List/ListItem.js");

Object.defineProperty(exports, 'ListItem', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItem).default;
  }
});

var _ListItemAvatar = __webpack_require__(/*! ./ListItemAvatar */ "./node_modules/material-ui/List/ListItemAvatar.js");

Object.defineProperty(exports, 'ListItemAvatar', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemAvatar).default;
  }
});

var _ListItemText = __webpack_require__(/*! ./ListItemText */ "./node_modules/material-ui/List/ListItemText.js");

Object.defineProperty(exports, 'ListItemText', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemText).default;
  }
});

var _ListItemIcon = __webpack_require__(/*! ./ListItemIcon */ "./node_modules/material-ui/List/ListItemIcon.js");

Object.defineProperty(exports, 'ListItemIcon', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemIcon).default;
  }
});

var _ListItemSecondaryAction = __webpack_require__(/*! ./ListItemSecondaryAction */ "./node_modules/material-ui/List/ListItemSecondaryAction.js");

Object.defineProperty(exports, 'ListItemSecondaryAction', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemSecondaryAction).default;
  }
});

var _ListSubheader = __webpack_require__(/*! ./ListSubheader */ "./node_modules/material-ui/List/ListSubheader.js");

Object.defineProperty(exports, 'ListSubheader', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListSubheader).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Menu/Menu.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Menu/Menu.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Popover = __webpack_require__(/*! ../Popover */ "./node_modules/material-ui/Popover/index.js");

var _Popover2 = _interopRequireDefault(_Popover);

var _MenuList = __webpack_require__(/*! ./MenuList */ "./node_modules/material-ui/Menu/MenuList.js");

var _MenuList2 = _interopRequireDefault(_MenuList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Popover

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The DOM element used to set the position of the menu.
   */
  anchorEl: typeof HTMLElement === 'function' ? __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").instanceOf(HTMLElement) : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any,
  // match Popover
  /**
   * Menu contents, normally `MenuItem`s.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Properties applied to the `MenuList` element.
   */
  MenuListProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Callback fired before the Menu enters.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired before the Menu exits.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the menu is visible.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  PaperProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * `classes` property applied to the `Popover` element.
   */
  PopoverClasses: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The length of the transition in `ms`, or 'auto'
   */
  transitionDuration: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]).isRequired
};


var rtlOrigin = {
  vertical: 'top',
  horizontal: 'right'
};

var ltrOrigin = {
  vertical: 'top',
  horizontal: 'left'
};

var styles = exports.styles = {
  paper: {
    // specZ: The maximum height of a simple menu should be one or more rows less than the view
    // height. This ensures a tappable area outside of the simple menu with which to dismiss
    // the menu.
    maxHeight: 'calc(100vh - 96px)',
    // Add iOS momentum scrolling.
    WebkitOverflowScrolling: 'touch'
  }
};

var Menu = function (_React$Component) {
  (0, _inherits3.default)(Menu, _React$Component);

  function Menu() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Menu);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Menu.__proto__ || (0, _getPrototypeOf2.default)(Menu)).call.apply(_ref, [this].concat(args))), _this), _this.getContentAnchorEl = function () {
      if (!_this.menuList || !_this.menuList.selectedItem) {
        // $FlowFixMe
        return (0, _reactDom.findDOMNode)(_this.menuList).firstChild;
      }

      return (0, _reactDom.findDOMNode)(_this.menuList.selectedItem);
    }, _this.menuList = undefined, _this.focus = function () {
      if (_this.menuList && _this.menuList.selectedItem) {
        // $FlowFixMe
        (0, _reactDom.findDOMNode)(_this.menuList.selectedItem).focus();
        return;
      }

      var menuList = (0, _reactDom.findDOMNode)(_this.menuList);
      if (menuList && menuList.firstChild) {
        // $FlowFixMe
        menuList.firstChild.focus();
      }
    }, _this.handleEnter = function (element) {
      var theme = _this.props.theme;


      var menuList = (0, _reactDom.findDOMNode)(_this.menuList);

      // Focus so the scroll computation of the Popover works as expected.
      _this.focus();

      // Let's ignore that piece of logic if users are already overriding the width
      // of the menu.
      // $FlowFixMe
      if (menuList && element.clientHeight < menuList.clientHeight && !menuList.style.width) {
        var size = (0, _scrollbarSize2.default)() + 'px';
        // $FlowFixMe
        menuList.style[theme.direction === 'rtl' ? 'paddingLeft' : 'paddingRight'] = size;
        // $FlowFixMe
        menuList.style.width = 'calc(100% + ' + size + ')';
      }

      if (_this.props.onEnter) {
        _this.props.onEnter(element);
      }
    }, _this.handleListKeyDown = function (event, key) {
      if (key === 'tab') {
        event.preventDefault();

        if (_this.props.onRequestClose) {
          _this.props.onRequestClose(event);
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Menu, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.open) {
        this.focus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.open && this.props.open) {
        // Needs to refocus as when a menu is rendered into another Modal,
        // the first modal might change the focus to prevent any leak.
        this.focus();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          MenuListProps = _props.MenuListProps,
          onEnter = _props.onEnter,
          _props$PaperProps = _props.PaperProps,
          PaperProps = _props$PaperProps === undefined ? {} : _props$PaperProps,
          PopoverClasses = _props.PopoverClasses,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'MenuListProps', 'onEnter', 'PaperProps', 'PopoverClasses', 'theme']);


      var themeDirection = theme && theme.direction;
      return _react2.default.createElement(
        _Popover2.default,
        (0, _extends3.default)({
          getContentAnchorEl: this.getContentAnchorEl,
          classes: PopoverClasses,
          onEnter: this.handleEnter,
          anchorOrigin: themeDirection === 'rtl' ? rtlOrigin : ltrOrigin,
          transformOrigin: themeDirection === 'rtl' ? rtlOrigin : ltrOrigin,
          PaperProps: (0, _extends3.default)({}, PaperProps, {
            classes: (0, _extends3.default)({}, PaperProps.classes, {
              root: classes.paper
            })
          })
        }, other),
        _react2.default.createElement(
          _MenuList2.default,
          (0, _extends3.default)({
            role: 'menu',
            onKeyDown: this.handleListKeyDown
          }, MenuListProps, {
            ref: function ref(node) {
              _this2.menuList = node;
            }
          }),
          children
        )
      );
    }
  }]);
  return Menu;
}(_react2.default.Component);

Menu.defaultProps = {
  open: false,
  transitionDuration: 'auto'
};
exports.default = (0, _withStyles2.default)(styles, { withTheme: true, name: 'MuiMenu' })(Menu);

/***/ }),

/***/ "./node_modules/material-ui/Menu/MenuItem.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Menu/MenuItem.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ListItem = __webpack_require__(/*! ../List/ListItem */ "./node_modules/material-ui/List/ListItem.js");

var _ListItem2 = _interopRequireDefault(_ListItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent ListItem

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends3.default)({}, theme.typography.subheading, {
      height: 24,
      boxSizing: 'content-box',
      background: 'none',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      '&:focus': {
        background: theme.palette.text.divider
      },
      '&:hover': {
        backgroundColor: theme.palette.text.divider
      }
    }),
    selected: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Menu item contents.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType),

  /**
   * @ignore
   */
  role: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired,

  /**
   * Use to apply selected styling.
   */
  selected: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var MenuItem = function (_React$Component) {
  (0, _inherits3.default)(MenuItem, _React$Component);

  function MenuItem() {
    (0, _classCallCheck3.default)(this, MenuItem);
    return (0, _possibleConstructorReturn3.default)(this, (MenuItem.__proto__ || (0, _getPrototypeOf2.default)(MenuItem)).apply(this, arguments));
  }

  (0, _createClass3.default)(MenuItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          component = _props.component,
          selected = _props.selected,
          role = _props.role,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'component', 'selected', 'role']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.selected, selected), classNameProp);

      return _react2.default.createElement(_ListItem2.default, (0, _extends3.default)({
        button: true,
        role: role,
        tabIndex: -1,
        className: className,
        component: component
      }, other));
    }
  }]);
  return MenuItem;
}(_react2.default.Component);

MenuItem.defaultProps = {
  role: 'menuitem',
  selected: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiMenuItem' })(MenuItem);

/***/ }),

/***/ "./node_modules/material-ui/Menu/MenuList.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Menu/MenuList.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _List = __webpack_require__(/*! ../List */ "./node_modules/material-ui/List/index.js");

var _List2 = _interopRequireDefault(_List);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent List

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * MenuList contents, normally `MenuItem`s.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   */
  onBlur: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  onKeyDown: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

var MenuList = function (_React$Component) {
  (0, _inherits3.default)(MenuList, _React$Component);

  function MenuList() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, MenuList);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = MenuList.__proto__ || (0, _getPrototypeOf2.default)(MenuList)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      currentTabIndex: undefined
    }, _this.list = undefined, _this.selectedItem = undefined, _this.blurTimer = undefined, _this.handleBlur = function (event) {
      _this.blurTimer = setTimeout(function () {
        if (_this.list) {
          var list = (0, _reactDom.findDOMNode)(_this.list);
          var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(list));
          if (!(0, _contains2.default)(list, currentFocus)) {
            _this.resetTabIndex();
          }
        }
      }, 30);

      if (_this.props.onBlur) {
        _this.props.onBlur(event);
      }
    }, _this.handleKeyDown = function (event) {
      var list = (0, _reactDom.findDOMNode)(_this.list);
      var key = (0, _keycode2.default)(event);
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(list));

      if ((key === 'up' || key === 'down') && (!currentFocus || currentFocus && !(0, _contains2.default)(list, currentFocus))) {
        if (_this.selectedItem) {
          // $FlowFixMe
          (0, _reactDom.findDOMNode)(_this.selectedItem).focus();
        } else {
          // $FlowFixMe
          list.firstChild.focus();
        }
      } else if (key === 'down') {
        event.preventDefault();
        if (currentFocus.nextElementSibling) {
          currentFocus.nextElementSibling.focus();
        }
      } else if (key === 'up') {
        event.preventDefault();
        if (currentFocus.previousElementSibling) {
          currentFocus.previousElementSibling.focus();
        }
      }

      if (_this.props.onKeyDown) {
        _this.props.onKeyDown(event, key);
      }
    }, _this.handleItemFocus = function (event) {
      var list = (0, _reactDom.findDOMNode)(_this.list);
      if (list) {
        // $FlowFixMe
        for (var i = 0; i < list.children.length; i += 1) {
          // $FlowFixMe
          if (list.children[i] === event.currentTarget) {
            _this.setTabIndex(i);
            break;
          }
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(MenuList, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.resetTabIndex();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      clearTimeout(this.blurTimer);
    }
  }, {
    key: 'setTabIndex',
    value: function setTabIndex(index) {
      this.setState({ currentTabIndex: index });
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentTabIndex = this.state.currentTabIndex;

      var list = (0, _reactDom.findDOMNode)(this.list);
      if (!list || !list.children || !list.firstChild) {
        return;
      }

      if (currentTabIndex && currentTabIndex >= 0) {
        // $FlowFixMe
        list.children[currentTabIndex].focus();
      } else {
        // $FlowFixMe
        list.firstChild.focus();
      }
    }
  }, {
    key: 'resetTabIndex',
    value: function resetTabIndex() {
      var list = (0, _reactDom.findDOMNode)(this.list);
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(list));
      // $FlowFixMe
      var items = [].concat((0, _toConsumableArray3.default)(list.children));
      var currentFocusIndex = items.indexOf(currentFocus);

      if (currentFocusIndex !== -1) {
        return this.setTabIndex(currentFocusIndex);
      }

      if (this.selectedItem) {
        return this.setTabIndex(items.indexOf((0, _reactDom.findDOMNode)(this.selectedItem)));
      }

      return this.setTabIndex(0);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          className = _props.className,
          onBlur = _props.onBlur,
          onKeyDown = _props.onKeyDown,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'className', 'onBlur', 'onKeyDown']);


      return _react2.default.createElement(
        _List2.default,
        (0, _extends3.default)({
          role: 'menu',
          rootRef: function rootRef(node) {
            _this2.list = node;
          },
          className: className,
          onKeyDown: this.handleKeyDown,
          onBlur: this.handleBlur
        }, other),
        _react2.default.Children.map(children, function (child, index) {
          if (!_react2.default.isValidElement(child)) {
            return null;
          }

          return _react2.default.cloneElement(child, {
            tabIndex: index === _this2.state.currentTabIndex ? 0 : -1,
            ref: child.props.selected ? function (node) {
              _this2.selectedItem = node;
            } : undefined,
            onFocus: _this2.handleItemFocus
          });
        })
      );
    }
  }]);
  return MenuList;
}(_react2.default.Component);

MenuList.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  onBlur: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,
  onKeyDown: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
} : {};
exports.default = MenuList;

/***/ }),

/***/ "./node_modules/material-ui/Menu/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Menu/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Menu = __webpack_require__(/*! ./Menu */ "./node_modules/material-ui/Menu/Menu.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Menu).default;
  }
});

var _MenuList = __webpack_require__(/*! ./MenuList */ "./node_modules/material-ui/Menu/MenuList.js");

Object.defineProperty(exports, 'MenuList', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_MenuList).default;
  }
});

var _MenuItem = __webpack_require__(/*! ./MenuItem */ "./node_modules/material-ui/Menu/MenuItem.js");

Object.defineProperty(exports, 'MenuItem', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_MenuItem).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/Backdrop.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui/Modal/Backdrop.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      zIndex: -1,
      width: '100%',
      height: '100%',
      position: 'fixed',
      top: 0,
      left: 0,
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      backgroundColor: theme.palette.common.lightBlack,
      transition: theme.transitions.create('opacity'),
      willChange: 'opacity',
      opacity: 0
    },
    invisible: {
      backgroundColor: theme.palette.common.transparent
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Can be used, for instance, to render a letter inside the avatar.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the backdrop is invisible.
   */
  invisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * @ignore - internal component.
 */
var Backdrop = function (_React$Component) {
  (0, _inherits3.default)(Backdrop, _React$Component);

  function Backdrop() {
    (0, _classCallCheck3.default)(this, Backdrop);
    return (0, _possibleConstructorReturn3.default)(this, (Backdrop.__proto__ || (0, _getPrototypeOf2.default)(Backdrop)).apply(this, arguments));
  }

  (0, _createClass3.default)(Backdrop, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          invisible = _props.invisible,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'invisible']);


      var backdropClass = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.invisible, invisible), className);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: backdropClass, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Backdrop;
}(_react2.default.Component);

Backdrop.defaultProps = {
  invisible: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBackdrop' })(Backdrop);

/***/ }),

/***/ "./node_modules/material-ui/Modal/Modal.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/Modal.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _addEventListener = __webpack_require__(/*! ../utils/addEventListener */ "./node_modules/material-ui/utils/addEventListener.js");

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _modalManager = __webpack_require__(/*! ./modalManager */ "./node_modules/material-ui/Modal/modalManager.js");

var _modalManager2 = _interopRequireDefault(_modalManager);

var _Backdrop = __webpack_require__(/*! ./Backdrop */ "./node_modules/material-ui/Modal/Backdrop.js");

var _Backdrop2 = _interopRequireDefault(_Backdrop);

var _Portal = __webpack_require__(/*! ../internal/Portal */ "./node_modules/material-ui/internal/Portal.js");

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// Modals don't open on the server so this won't break concurrency.
// Could also put this on context.
var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var modalManager = (0, _modalManager2.default)();

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      position: 'fixed',
      zIndex: theme.zIndex.dialog,
      top: 0,
      left: 0
    },
    hidden: {
      visibility: 'hidden'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the backdrop element.
   */
  BackdropClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Pass a component class to use as the backdrop.
   */
  BackdropComponent: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, the backdrop is invisible.
   */
  BackdropInvisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the backdrop transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  BackdropTransitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Always keep the children in the DOM.
   * This property can be useful in SEO situation or
   * when you want to maximize the responsiveness of the Modal.
   */
  keepMounted: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the backdrop is disabled.
   */
  disableBackdrop: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  modalManager: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * Callback fires when the backdrop is clicked on.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is pressed and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Modal is visible.
   */
  show: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * The modal component provides a solid foundation for creating dialogs,
 * popovers, or whatever else.
 * The component renders its `children` node in front of a backdrop component.
 *
 * The `Modal` offers a few helpful features over using just a `Portal` component and some styles:
 * - Manages dialog stacking when one-at-a-time just isn't enough.
 * - Creates a backdrop, for disabling interaction below the modal.
 * - It properly manages focus; moving to the modal content,
 *   and keeping it there until the modal is closed.
 * - It disables scrolling of the page content while open.
 * - Adds the appropriate ARIA roles are automatically.
 *
 * This component shares many concepts with [react-overlays](https://react-bootstrap.github.io/react-overlays/#modals).
 */
var Modal = function (_React$Component) {
  (0, _inherits3.default)(Modal, _React$Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call.apply(_ref, [this].concat(args))), _this), _initialiseProps.call(_this), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Modal, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (!this.props.show) {
        this.setState({ exited: true });
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.mounted = true;
      if (this.props.show) {
        this.handleShow();
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.show && this.state.exited) {
        this.setState({ exited: false });
      }
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps) {
      if (!this.props.show && nextProps.show) {
        this.checkForFocus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.show && this.props.show) {
        this.handleShow();
      }
      // We are waiting for the onExited callback to call handleHide.
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.props.show || !this.state.exited) {
        this.handleHide();
      }
      this.mounted = false;
    }
  }, {
    key: 'checkForFocus',
    value: function checkForFocus() {
      if (_inDOM2.default) {
        this.lastFocus = (0, _activeElement2.default)();
      }
    }
  }, {
    key: 'restoreLastFocus',
    value: function restoreLastFocus() {
      if (this.lastFocus && this.lastFocus.focus) {
        this.lastFocus.focus();
        this.lastFocus = undefined;
      }
    }
  }, {
    key: 'handleShow',
    value: function handleShow() {
      var doc = (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this));
      this.props.modalManager.add(this);
      this.onDocumentKeyUpListener = (0, _addEventListener2.default)(doc, 'keyup', this.handleDocumentKeyUp);
      this.onFocusListener = (0, _addEventListener2.default)(doc, 'focus', this.handleFocusListener, true);
      this.focus();
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this)));
      var modalContent = this.modal && this.modal.lastChild;
      var focusInModal = currentFocus && (0, _contains2.default)(modalContent, currentFocus);

      if (modalContent && !focusInModal) {
        if (!modalContent.hasAttribute('tabIndex')) {
          modalContent.setAttribute('tabIndex', -1);
           true ? (0, _warning2.default)(false, 'Material-UI: the modal content node does not accept focus. ' + 'For the benefit of assistive technologies, ' + 'the tabIndex of the node is being set to "-1".') : void 0;
        }

        modalContent.focus();
      }
    }
  }, {
    key: 'handleHide',
    value: function handleHide() {
      this.props.modalManager.remove(this);
      if (this.onDocumentKeyUpListener) this.onDocumentKeyUpListener.remove();
      if (this.onFocusListener) this.onFocusListener.remove();
      this.restoreLastFocus();
    }
  }, {
    key: 'renderBackdrop',
    value: function renderBackdrop() {
      var other = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var _props = this.props,
          BackdropComponent = _props.BackdropComponent,
          BackdropClassName = _props.BackdropClassName,
          BackdropTransitionDuration = _props.BackdropTransitionDuration,
          BackdropInvisible = _props.BackdropInvisible,
          show = _props.show;


      return _react2.default.createElement(
        _Fade2.default,
        (0, _extends3.default)({ appear: true, 'in': show, timeout: BackdropTransitionDuration }, other),
        _react2.default.createElement(BackdropComponent, {
          invisible: BackdropInvisible,
          className: BackdropClassName,
          onClick: this.handleBackdropClick
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          disableBackdrop = _props2.disableBackdrop,
          BackdropComponent = _props2.BackdropComponent,
          BackdropClassName = _props2.BackdropClassName,
          BackdropTransitionDuration = _props2.BackdropTransitionDuration,
          BackdropInvisible = _props2.BackdropInvisible,
          ignoreBackdropClick = _props2.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props2.ignoreEscapeKeyUp,
          children = _props2.children,
          classes = _props2.classes,
          className = _props2.className,
          keepMounted = _props2.keepMounted,
          modalManagerProp = _props2.modalManager,
          onBackdropClick = _props2.onBackdropClick,
          onEscapeKeyUp = _props2.onEscapeKeyUp,
          onRequestClose = _props2.onRequestClose,
          onEnter = _props2.onEnter,
          onEntering = _props2.onEntering,
          onEntered = _props2.onEntered,
          onExit = _props2.onExit,
          onExiting = _props2.onExiting,
          onExited = _props2.onExited,
          show = _props2.show,
          other = (0, _objectWithoutProperties3.default)(_props2, ['disableBackdrop', 'BackdropComponent', 'BackdropClassName', 'BackdropTransitionDuration', 'BackdropInvisible', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'children', 'classes', 'className', 'keepMounted', 'modalManager', 'onBackdropClick', 'onEscapeKeyUp', 'onRequestClose', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'show']);


      if (!keepMounted && !show && this.state.exited) {
        return null;
      }

      var transitionCallbacks = {
        onEnter: onEnter,
        onEntering: onEntering,
        onEntered: onEntered,
        onExit: onExit,
        onExiting: onExiting,
        onExited: this.handleTransitionExited
      };

      var modalChild = _react2.default.Children.only(children);
      var _modalChild$props = modalChild.props,
          role = _modalChild$props.role,
          tabIndex = _modalChild$props.tabIndex;

      var childProps = {};

      if (role === undefined) {
        childProps.role = role === undefined ? 'document' : role;
      }

      if (tabIndex === undefined) {
        childProps.tabIndex = tabIndex == null ? -1 : tabIndex;
      }

      var backdropProps = void 0;

      // It's a Transition like component
      if (modalChild.props.hasOwnProperty('in')) {
        (0, _keys2.default)(transitionCallbacks).forEach(function (key) {
          childProps[key] = (0, _helpers.createChainedFunction)(transitionCallbacks[key], modalChild.props[key]);
        });
      } else {
        backdropProps = transitionCallbacks;
      }

      if ((0, _keys2.default)(childProps).length) {
        modalChild = _react2.default.cloneElement(modalChild, childProps);
      }

      return _react2.default.createElement(
        _Portal2.default,
        {
          open: true,
          ref: function ref(node) {
            _this2.mountNode = node ? node.getLayer() : null;
          }
        },
        _react2.default.createElement(
          'div',
          (0, _extends3.default)({
            className: (0, _classnames2.default)(classes.root, className, (0, _defineProperty3.default)({}, classes.hidden, this.state.exited))
          }, other, {
            ref: function ref(node) {
              _this2.modal = node;
            }
          }),
          !disableBackdrop && (!keepMounted || show || !this.state.exited) && this.renderBackdrop(backdropProps),
          modalChild
        )
      );
    }
  }]);
  return Modal;
}(_react2.default.Component);

Modal.defaultProps = {
  BackdropComponent: _Backdrop2.default,
  BackdropTransitionDuration: 300,
  BackdropInvisible: false,
  keepMounted: false,
  disableBackdrop: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  modalManager: modalManager
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.state = {
    exited: false
  };
  this.onDocumentKeyUpListener = null;
  this.onFocusListener = null;
  this.mounted = false;
  this.lastFocus = undefined;
  this.modal = null;
  this.mountNode = null;

  this.handleFocusListener = function () {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(_this3)));
    var modalContent = _this3.modal && _this3.modal.lastChild;

    if (modalContent && modalContent !== currentFocus && !(0, _contains2.default)(modalContent, currentFocus)) {
      modalContent.focus();
    }
  };

  this.handleDocumentKeyUp = function (event) {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    if ((0, _keycode2.default)(event) !== 'esc') {
      return;
    }

    var _props3 = _this3.props,
        onEscapeKeyUp = _props3.onEscapeKeyUp,
        onRequestClose = _props3.onRequestClose,
        ignoreEscapeKeyUp = _props3.ignoreEscapeKeyUp;


    if (onEscapeKeyUp) {
      onEscapeKeyUp(event);
    }

    if (onRequestClose && !ignoreEscapeKeyUp) {
      onRequestClose(event);
    }
  };

  this.handleBackdropClick = function (event) {
    if (event.target !== event.currentTarget) {
      return;
    }

    var _props4 = _this3.props,
        onBackdropClick = _props4.onBackdropClick,
        onRequestClose = _props4.onRequestClose,
        ignoreBackdropClick = _props4.ignoreBackdropClick;


    if (onBackdropClick) {
      onBackdropClick(event);
    }

    if (onRequestClose && !ignoreBackdropClick) {
      onRequestClose(event);
    }
  };

  this.handleTransitionExited = function () {
    if (_this3.props.onExited) {
      var _props5;

      (_props5 = _this3.props).onExited.apply(_props5, arguments);
    }

    _this3.setState({ exited: true });
    _this3.handleHide();
  };
};

exports.default = (0, _withStyles2.default)(styles, { flip: false, name: 'MuiModal' })(Modal);

/***/ }),

/***/ "./node_modules/material-ui/Modal/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Modal = __webpack_require__(/*! ./Modal */ "./node_modules/material-ui/Modal/Modal.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Modal).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/modalManager.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Modal/modalManager.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _isWindow = __webpack_require__(/*! dom-helpers/query/isWindow */ "./node_modules/dom-helpers/query/isWindow.js");

var _isWindow2 = _interopRequireDefault(_isWindow);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _manageAriaHidden = __webpack_require__(/*! ../utils/manageAriaHidden */ "./node_modules/material-ui/utils/manageAriaHidden.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Taken from https://github.com/react-bootstrap/react-overlays/blob/master/src/ModalManager.js

function getPaddingRight(node) {
  return parseInt(node.style.paddingRight || 0, 10);
}

// Do we have a scroll bar?
function bodyIsOverflowing(node) {
  var doc = (0, _ownerDocument2.default)(node);
  var win = (0, _isWindow2.default)(doc);

  // Takes in account potential non zero margin on the body.
  var style = window.getComputedStyle(doc.body);
  var marginLeft = parseInt(style.getPropertyValue('margin-left'), 10);
  var marginRight = parseInt(style.getPropertyValue('margin-right'), 10);

  return marginLeft + doc.body.clientWidth + marginRight < win.innerWidth;
}

function getContainer() {
  var container = _inDOM2.default ? window.document.body : {};
   true ? (0, _warning2.default)(container !== null, '\nMaterial-UI: you are most likely evaluating the code before the\nbrowser has a chance to reach the <body>.\nPlease move the import at the end of the <body>.\n  ') : void 0;
  return container;
}
/**
 * State management helper for modals/layers.
 * Simplified, but inspired by react-overlay's ModalManager class
 *
 * @internal Used by the Modal to ensure proper focus management.
 */
function createModalManager() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$hideSiblingNodes = _ref.hideSiblingNodes,
      hideSiblingNodes = _ref$hideSiblingNodes === undefined ? true : _ref$hideSiblingNodes;

  var modals = [];

  var prevOverflow = void 0;
  var prevPaddings = [];

  function add(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx !== -1) {
      return modalIdx;
    }

    modalIdx = modals.length;
    modals.push(modal);

    if (hideSiblingNodes) {
      (0, _manageAriaHidden.hideSiblings)(container, modal.mountNode);
    }

    if (modals.length === 1) {
      // Save our current overflow so we can revert
      // back to it when all modals are closed!
      prevOverflow = container.style.overflow;

      if (bodyIsOverflowing(container)) {
        prevPaddings = [getPaddingRight(container)];
        var scrollbarSize = (0, _scrollbarSize2.default)();
        container.style.paddingRight = prevPaddings[0] + scrollbarSize + 'px';

        var fixedNodes = document.querySelectorAll('.mui-fixed');
        for (var i = 0; i < fixedNodes.length; i += 1) {
          var paddingRight = getPaddingRight(fixedNodes[i]);
          prevPaddings.push(paddingRight);
          fixedNodes[i].style.paddingRight = paddingRight + scrollbarSize + 'px';
        }
      }

      container.style.overflow = 'hidden';
    }

    return modalIdx;
  }

  function remove(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx === -1) {
      return modalIdx;
    }

    modals.splice(modalIdx, 1);

    if (modals.length === 0) {
      container.style.overflow = prevOverflow;
      container.style.paddingRight = prevPaddings[0];

      var fixedNodes = document.querySelectorAll('.mui-fixed');
      for (var i = 0; i < fixedNodes.length; i += 1) {
        fixedNodes[i].style.paddingRight = prevPaddings[i + 1] + 'px';
      }

      prevOverflow = undefined;
      prevPaddings = [];
      if (hideSiblingNodes) {
        (0, _manageAriaHidden.showSiblings)(container, modal.mountNode);
      }
    } else if (hideSiblingNodes) {
      // otherwise make sure the next top modal is visible to a SR
      (0, _manageAriaHidden.ariaHidden)(false, modals[modals.length - 1].mountNode);
    }

    return modalIdx;
  }

  function isTopModal(modal) {
    return !!modals.length && modals[modals.length - 1] === modal;
  }

  var modalManager = { add: add, remove: remove, isTopModal: isTopModal };

  return modalManager;
}

exports.default = createModalManager;

/***/ }),

/***/ "./node_modules/material-ui/Popover/Popover.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/Popover/Popover.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _debounce = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");

var _debounce2 = _interopRequireDefault(_debounce);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Modal = __webpack_require__(/*! ../Modal */ "./node_modules/material-ui/Modal/index.js");

var _Modal2 = _interopRequireDefault(_Modal);

var _Grow = __webpack_require__(/*! ../transitions/Grow */ "./node_modules/material-ui/transitions/Grow.js");

var _Grow2 = _interopRequireDefault(_Grow);

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Modal

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionClasses || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

function getOffsetTop(rect, vertical) {
  var offset = 0;

  if (typeof vertical === 'number') {
    offset = vertical;
  } else if (vertical === 'center') {
    offset = rect.height / 2;
  } else if (vertical === 'bottom') {
    offset = rect.height;
  }

  return offset;
}

function getOffsetLeft(rect, horizontal) {
  var offset = 0;

  if (typeof horizontal === 'number') {
    offset = horizontal;
  } else if (horizontal === 'center') {
    offset = rect.width / 2;
  } else if (horizontal === 'right') {
    offset = rect.width;
  }

  return offset;
}

function getTransformOriginValue(transformOrigin) {
  return [transformOrigin.horizontal, transformOrigin.vertical].map(function (n) {
    return typeof n === 'number' ? n + 'px' : n;
  }).join(' ');
}

// Sum the scrollTop between two elements.
function getScrollParent(parent, child) {
  var element = child;
  var scrollTop = 0;

  while (element && element !== parent) {
    element = element.parentNode;
    scrollTop += element.scrollTop;
  }
  return scrollTop;
}

var styles = exports.styles = {
  paper: {
    position: 'absolute',
    overflowY: 'auto',
    overflowX: 'hidden',
    // So we see the popover when it's empty.
    // It's most likely on issue on userland.
    minWidth: 16,
    minHeight: 16,
    maxWidth: 'calc(100vw - 32px)',
    maxHeight: 'calc(100vh - 32px)',
    '&:focus': {
      outline: 'none'
    }
  }
};

var babelPluginFlowReactPropTypes_proptype_Origin = {
  horizontal: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['right']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired,
  vertical: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['top']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['bottom']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired
};
var babelPluginFlowReactPropTypes_proptype_Position = {
  top: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
};
var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * This is the DOM element that may be used
   * to set the position of the popover.
   */
  anchorEl: typeof HTMLElement === 'function' ? __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").instanceOf(HTMLElement) : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any,

  /**
   * This is the position that may be used
   * to set the position of the popover.
   * The coordinates are relative to
   * the application's client area.
   */
  anchorPosition: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    top: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
    left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
  }),

  /*
   * This determines which anchor prop to refer to to set
   * the position of the popover.
   */
  anchorReference: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['anchorEl', 'anchorPosition']),

  /**
   * This is the point on the anchor where the popover's
   * `anchorEl` will attach to. This is not used when the
   * anchorReference is 'anchorPosition'.
   *
   * Options:
   * vertical: [top, center, bottom];
   * horizontal: [left, center, right].
   */
  anchorOrigin: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    horizontal: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['right']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired,
    vertical: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['top']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['bottom']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired
  }).isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The elevation of the popover.
   */
  elevation: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,

  /**
   * This function is called in order to retrieve the content anchor element.
   * It's the opposite of the `anchorEl` property.
   * The content anchor element should be an element inside the popover.
   * It's used to correctly scroll and set the position of the popover.
   * The positioning strategy tries to make the content anchor element just above the
   * anchor element.
   */
  getContentAnchorEl: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Specifies how close to the edge of the window the popover can appear.
   */
  marginThreshold: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,

  /**
   * Callback fired before the component is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired before the component is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback.
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the popover is visible.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Properties applied to the `Paper` element.
   */
  PaperProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  role: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * This is the point on the popover which
   * will attach to the anchor's origin.
   *
   * Options:
   * vertical: [top, center, bottom, x(px)];
   * horizontal: [left, center, right, x(px)].
   */
  transformOrigin: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    horizontal: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['right']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired,
    vertical: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['top']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['bottom']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired
  }).isRequired,

  /**
   * The animation classNames applied to the component as it enters or exits.
   * This property is a direct binding to [`CSSTransition.classNames`](https://reactcommunity.org/react-transition-group/#CSSTransition-prop-classNames).
   */
  transitionClasses: typeof babelPluginFlowReactPropTypes_proptype_TransitionClasses === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionClasses : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionClasses),

  /**
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  transitionDuration: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])])
};

var Popover = function (_React$Component) {
  (0, _inherits3.default)(Popover, _React$Component);

  function Popover() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Popover);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Popover.__proto__ || (0, _getPrototypeOf2.default)(Popover)).call.apply(_ref, [this].concat(args))), _this), _this.componentWillUnmount = function () {
      _this.handleResize.cancel();
    }, _this.setPositioningStyles = function (element) {
      if (element && element.style) {
        var positioning = _this.getPositioningStyle(element);

        element.style.top = positioning.top;
        element.style.left = positioning.left;
        element.style.transformOrigin = positioning.transformOrigin;
      }
    }, _this.getPositioningStyle = function (element) {
      var marginThreshold = _this.props.marginThreshold;

      // Check if the parent has requested anchoring on an inner content node

      var contentAnchorOffset = _this.getContentAnchorOffset(element);
      // Get the offset of of the anchoring element
      var anchorOffset = _this.getAnchorOffset(contentAnchorOffset);

      var elemRect = {
        width: element.clientWidth,
        height: element.clientHeight
      };
      // Get the transform origin point on the element itself
      var transformOrigin = _this.getTransformOrigin(elemRect, contentAnchorOffset);

      // Calculate element positioning
      var top = anchorOffset.top - transformOrigin.vertical;
      var left = anchorOffset.left - transformOrigin.horizontal;
      var bottom = top + elemRect.height;
      var right = left + elemRect.width;

      // Window thresholds taking required margin into account
      var heightThreshold = window.innerHeight - marginThreshold;
      var widthThreshold = window.innerWidth - marginThreshold;

      // Check if the vertical axis needs shifting
      if (top < marginThreshold) {
        var diff = top - marginThreshold;
        top -= diff;
        transformOrigin.vertical += diff;
      } else if (bottom > heightThreshold) {
        var _diff = bottom - heightThreshold;
        top -= _diff;
        transformOrigin.vertical += _diff;
      }

       true ? (0, _warning2.default)(elemRect.height < heightThreshold || !elemRect.height || !heightThreshold, ['Material-UI: the popover component is too tall.', 'Some part of it can not be seen on the screen (' + (elemRect.height - heightThreshold) + 'px).', 'Please consider adding a `max-height` to improve the user-experience.'].join('\n')) : void 0;

      // Check if the horizontal axis needs shifting
      if (left < marginThreshold) {
        var _diff2 = left - marginThreshold;
        left -= _diff2;
        transformOrigin.horizontal += _diff2;
      } else if (right > widthThreshold) {
        var _diff3 = right - widthThreshold;
        left -= _diff3;
        transformOrigin.horizontal += _diff3;
      }

      return {
        top: top + 'px',
        left: left + 'px',
        transformOrigin: getTransformOriginValue(transformOrigin)
      };
    }, _this.transitionEl = undefined, _this.handleGetOffsetTop = getOffsetTop, _this.handleGetOffsetLeft = getOffsetLeft, _this.handleEnter = function (element) {
      if (_this.props.onEnter) {
        _this.props.onEnter(element);
      }

      _this.setPositioningStyles(element);
    }, _this.handleResize = (0, _debounce2.default)(function () {
      var element = _reactDom2.default.findDOMNode(_this.transitionEl);
      _this.setPositioningStyles(element);
    }, 166), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Popover, [{
    key: 'getAnchorOffset',


    // Returns the top/left offset of the position
    // to attach to on the anchor element (or body if none is provided)
    value: function getAnchorOffset(contentAnchorOffset) {
      // $FlowFixMe
      var _props = this.props,
          anchorEl = _props.anchorEl,
          anchorOrigin = _props.anchorOrigin,
          anchorReference = _props.anchorReference,
          anchorPosition = _props.anchorPosition;


      if (anchorReference === 'anchorPosition') {
        return anchorPosition;
      }

      var anchorElement = anchorEl || document.body;
      var anchorRect = anchorElement.getBoundingClientRect();
      var anchorVertical = contentAnchorOffset === 0 ? anchorOrigin.vertical : 'center';

      return {
        top: anchorRect.top + this.handleGetOffsetTop(anchorRect, anchorVertical),
        left: anchorRect.left + this.handleGetOffsetLeft(anchorRect, anchorOrigin.horizontal)
      };
    }

    // Returns the vertical offset of inner content to anchor the transform on if provided

  }, {
    key: 'getContentAnchorOffset',
    value: function getContentAnchorOffset(element) {
      var _props2 = this.props,
          getContentAnchorEl = _props2.getContentAnchorEl,
          anchorReference = _props2.anchorReference;

      var contentAnchorOffset = 0;

      if (getContentAnchorEl && anchorReference === 'anchorEl') {
        var contentAnchorEl = getContentAnchorEl(element);

        if (contentAnchorEl && (0, _contains2.default)(element, contentAnchorEl)) {
          var scrollTop = getScrollParent(element, contentAnchorEl);
          contentAnchorOffset = contentAnchorEl.offsetTop + contentAnchorEl.clientHeight / 2 - scrollTop || 0;
        }

        // != the default value
         true ? (0, _warning2.default)(this.props.anchorOrigin.vertical === 'top', ['Material-UI: you can not change the default `anchorOrigin.vertical` value when also ', 'providing the `getContentAnchorEl` property to the popover component.', 'Only use one of the two properties', 'Set `getContentAnchorEl` to null or left `anchorOrigin.vertical` unchanged'].join()) : void 0;
      }

      return contentAnchorOffset;
    }

    // Return the base transform origin using the element
    // and taking the content anchor offset into account if in use

  }, {
    key: 'getTransformOrigin',
    value: function getTransformOrigin(elemRect) {
      var contentAnchorOffset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var transformOrigin = this.props.transformOrigin;

      return {
        vertical: this.handleGetOffsetTop(elemRect, transformOrigin.vertical) + contentAnchorOffset,
        horizontal: this.handleGetOffsetLeft(elemRect, transformOrigin.horizontal)
      };
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props3 = this.props,
          anchorEl = _props3.anchorEl,
          anchorReference = _props3.anchorReference,
          anchorPosition = _props3.anchorPosition,
          anchorOrigin = _props3.anchorOrigin,
          children = _props3.children,
          classes = _props3.classes,
          elevation = _props3.elevation,
          getContentAnchorEl = _props3.getContentAnchorEl,
          marginThreshold = _props3.marginThreshold,
          onEnter = _props3.onEnter,
          onEntering = _props3.onEntering,
          onEntered = _props3.onEntered,
          onExit = _props3.onExit,
          onExiting = _props3.onExiting,
          onExited = _props3.onExited,
          open = _props3.open,
          PaperProps = _props3.PaperProps,
          role = _props3.role,
          transformOrigin = _props3.transformOrigin,
          transitionClasses = _props3.transitionClasses,
          transitionDuration = _props3.transitionDuration,
          other = (0, _objectWithoutProperties3.default)(_props3, ['anchorEl', 'anchorReference', 'anchorPosition', 'anchorOrigin', 'children', 'classes', 'elevation', 'getContentAnchorEl', 'marginThreshold', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'open', 'PaperProps', 'role', 'transformOrigin', 'transitionClasses', 'transitionDuration']);


      return _react2.default.createElement(
        _Modal2.default,
        (0, _extends3.default)({ show: open, BackdropInvisible: true }, other),
        _react2.default.createElement(
          _Grow2.default,
          {
            appear: true,
            'in': open,
            onEnter: this.handleEnter,
            onEntering: onEntering,
            onEntered: onEntered,
            onExit: onExit,
            onExiting: onExiting,
            onExited: onExited,
            role: role,
            transitionClasses: transitionClasses,
            timeout: transitionDuration,
            rootRef: function rootRef(node) {
              _this2.transitionEl = node;
            }
          },
          _react2.default.createElement(
            _Paper2.default,
            (0, _extends3.default)({
              className: classes.paper,
              elevation: elevation
            }, PaperProps),
            _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }),
            children
          )
        )
      );
    }
  }]);
  return Popover;
}(_react2.default.Component);

Popover.defaultProps = {
  anchorReference: 'anchorEl',
  anchorOrigin: {
    vertical: 'top',
    horizontal: 'left'
  },
  transformOrigin: {
    vertical: 'top',
    horizontal: 'left'
  },
  transitionDuration: 'auto',
  elevation: 8,
  marginThreshold: 16
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiPopover' })(Popover);

/***/ }),

/***/ "./node_modules/material-ui/Popover/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Popover/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Popover = __webpack_require__(/*! ./Popover */ "./node_modules/material-ui/Popover/Popover.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Popover).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Toolbar/Toolbar.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/Toolbar/Toolbar.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends3.default)({
      position: 'relative',
      display: 'flex',
      alignItems: 'center'
    }, theme.mixins.toolbar),
    gutters: theme.mixins.gutters({})
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Toolbar children, usually a mixture of `IconButton`, `Button` and `Typography`.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, disables gutter padding.
   */
  disableGutters: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var Toolbar = function (_React$Component) {
  (0, _inherits3.default)(Toolbar, _React$Component);

  function Toolbar() {
    (0, _classCallCheck3.default)(this, Toolbar);
    return (0, _possibleConstructorReturn3.default)(this, (Toolbar.__proto__ || (0, _getPrototypeOf2.default)(Toolbar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Toolbar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          disableGutters = _props.disableGutters,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'disableGutters']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.gutters, !disableGutters), classNameProp);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Toolbar;
}(_react2.default.Component);

Toolbar.defaultProps = {
  disableGutters: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiToolbar' })(Toolbar);

/***/ }),

/***/ "./node_modules/material-ui/Toolbar/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Toolbar/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Toolbar = __webpack_require__(/*! ./Toolbar */ "./node_modules/material-ui/Toolbar/Toolbar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Toolbar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/internal/Portal.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/internal/Portal.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content to portal in order to escape the parent DOM node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * If `true` the children will be mounted into the DOM.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

/**
 * @ignore - internal component.
 */
var Portal = function (_React$Component) {
  (0, _inherits3.default)(Portal, _React$Component);

  function Portal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Portal.__proto__ || (0, _getPrototypeOf2.default)(Portal)).call.apply(_ref, [this].concat(args))), _this), _this.layer = null, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Portal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unrenderLayer();
    }
  }, {
    key: 'getLayer',
    value: function getLayer() {
      if (!this.layer) {
        this.layer = document.createElement('div');
        this.layer.setAttribute('data-mui-portal', 'true');
        if (document.body && this.layer) {
          document.body.appendChild(this.layer);
        }
      }

      return this.layer;
    }
  }, {
    key: 'unrenderLayer',
    value: function unrenderLayer() {
      if (!this.layer) {
        return;
      }

      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        _reactDom2.default.unmountComponentAtNode(this.layer);
      }

      if (document.body) {
        document.body.removeChild(this.layer);
      }
      this.layer = null;
    }
  }, {
    key: 'renderLayer',
    value: function renderLayer() {
      var _props = this.props,
          children = _props.children,
          open = _props.open;


      if (open) {
        // By calling this method in componentDidMount() and
        // componentDidUpdate(), you're effectively creating a "wormhole" that
        // funnels React's hierarchical updates through to a DOM node on an
        // entirely different part of the page.
        var layerElement = _react2.default.Children.only(children);
        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, layerElement, this.getLayer());
      } else {
        this.unrenderLayer();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          children = _props2.children,
          open = _props2.open;

      // Support react@15.x, will be removed at some point

      if (!_reactDom2.default.createPortal) {
        return null;
      }

      // Can't be rendered server-side.
      if (_inDOM2.default) {
        if (open) {
          var layer = this.getLayer();
          // $FlowFixMe layer is non-null
          return _reactDom2.default.createPortal(children, layer);
        }

        this.unrenderLayer();
      }

      return null;
    }
  }]);
  return Portal;
}(_react2.default.Component);

Portal.defaultProps = {
  open: false
};
Portal.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
} : {};
exports.default = Portal;

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Fade.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Fade.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  timeout: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired
};


var reflow = function reflow(node) {
  return node.scrollTop;
};

/**
 * The Fade transition is used by the Modal component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */

var Fade = function (_React$Component) {
  (0, _inherits3.default)(Fade, _React$Component);

  function Fade() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Fade);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Fade.__proto__ || (0, _getPrototypeOf2.default)(Fade)).call.apply(_ref, [this].concat(args))), _this), _this.handleEnter = function (node) {
      node.style.opacity = '0';
      reflow(node);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      node.style.opacity = '1';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      node.style.opacity = '0';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Fade, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          styleProp = _props.style,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'style', 'theme']);


      var style = (0, _extends3.default)({}, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          style: style,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit
        }, other),
        children
      );
    }
  }]);
  return Fade;
}(_react2.default.Component);

Fade.defaultProps = {
  appear: true,
  timeout: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  }
};
exports.default = (0, _withTheme2.default)()(Fade);

/***/ }),

/***/ "./node_modules/material-ui/transitions/Grow.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Grow.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

exports.getScale = getScale;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _CSSTransition = __webpack_require__(/*! react-transition-group/CSSTransition */ "./node_modules/react-transition-group/CSSTransition.js");

var _CSSTransition2 = _interopRequireDefault(_CSSTransition);

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent CSSTransition

// Only exported for tests.
var babelPluginFlowReactPropTypes_proptype_TransitionClasses = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionClasses || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

function getScale(value) {
  return 'scale(' + value + ', ' + Math.pow(value, 2) + ')';
}

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, show the component; triggers the enter or exit animation.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The animation classNames applied to the component as it enters or exits.
   * This property is a direct binding to [`CSSTransition.classNames`](https://reactcommunity.org/react-transition-group/#CSSTransition-prop-classNames).
   */
  transitionClasses: typeof babelPluginFlowReactPropTypes_proptype_TransitionClasses === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionClasses.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionClasses.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionClasses : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionClasses).isRequired,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   *
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  timeout: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]).isRequired
};

/**
 * The Grow transition is used by the Popover component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */
var Grow = function (_React$Component) {
  (0, _inherits3.default)(Grow, _React$Component);

  function Grow() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Grow);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Grow.__proto__ || (0, _getPrototypeOf2.default)(Grow)).call.apply(_ref, [this].concat(args))), _this), _this.autoTimeout = undefined, _this.handleEnter = function (node) {
      node.style.opacity = '0';
      node.style.transform = getScale(0.75);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      var duration = 0;

      if (timeout === 'auto') {
        duration = theme.transitions.getAutoHeightDuration(node.clientHeight);
        _this.autoTimeout = duration;
      } else if (typeof timeout === 'number') {
        duration = timeout;
      } else if (timeout && typeof timeout.enter === 'number') {
        duration = timeout.enter;
      } else {
        // The propType will warn in this case.
      }

      node.style.transition = [theme.transitions.create('opacity', {
        duration: duration
      }), theme.transitions.create('transform', {
        duration: duration * 0.666
      })].join(',');

      node.style.opacity = '1';
      node.style.transform = getScale(1);

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      var duration = 0;

      if (timeout === 'auto') {
        duration = theme.transitions.getAutoHeightDuration(node.clientHeight);
        _this.autoTimeout = duration;
      } else if (typeof timeout === 'number') {
        duration = timeout;
      } else if (timeout && typeof timeout.exit === 'number') {
        duration = timeout.exit;
      } else {
        // The propType will warn in this case.
      }

      node.style.transition = [theme.transitions.create('opacity', {
        duration: duration
      }), theme.transitions.create('transform', {
        duration: duration * 0.666,
        delay: duration * 0.333
      })].join(',');

      node.style.opacity = '0';
      node.style.transform = getScale(0.75);

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _this.addEndListener = function (node, next) {
      if (_this.props.timeout === 'auto') {
        setTimeout(next, _this.autoTimeout || 0);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Grow, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          rootRef = _props.rootRef,
          styleProp = _props.style,
          transitionClasses = _props.transitionClasses,
          timeout = _props.timeout,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'rootRef', 'style', 'transitionClasses', 'timeout', 'theme']);


      var style = (0, _extends3.default)({}, children.props.style, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _CSSTransition2.default,
        (0, _extends3.default)({
          classNames: transitionClasses,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit,
          addEndListener: this.addEndListener,
          appear: appear,
          style: style,
          timeout: timeout === 'auto' ? null : timeout
        }, other, {
          ref: rootRef
        }),
        children
      );
    }
  }]);
  return Grow;
}(_react2.default.Component);

Grow.defaultProps = {
  appear: true,
  timeout: 'auto',
  transitionClasses: {}
};
exports.default = (0, _withTheme2.default)()(Grow);

/***/ }),

/***/ "./node_modules/material-ui/utils/manageAriaHidden.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui/utils/manageAriaHidden.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ariaHidden = ariaHidden;
exports.hideSiblings = hideSiblings;
exports.showSiblings = showSiblings;
//  weak

var BLACKLIST = ['template', 'script', 'style'];

var isHidable = function isHidable(_ref) {
  var nodeType = _ref.nodeType,
      tagName = _ref.tagName;
  return nodeType === 1 && BLACKLIST.indexOf(tagName.toLowerCase()) === -1;
};

var siblings = function siblings(container, mount, cb) {
  mount = [].concat(mount); // eslint-disable-line no-param-reassign
  [].forEach.call(container.children, function (node) {
    if (mount.indexOf(node) === -1 && isHidable(node)) {
      cb(node);
    }
  });
};

function ariaHidden(show, node) {
  if (!node) {
    return;
  }
  if (show) {
    node.setAttribute('aria-hidden', 'true');
  } else {
    node.removeAttribute('aria-hidden');
  }
}

function hideSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(true, node);
  });
}

function showSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(false, node);
  });
}

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/react-transition-group/CSSTransition.js":
/*!**************************************************************!*\
  !*** ./node_modules/react-transition-group/CSSTransition.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var PropTypes = _interopRequireWildcard(_propTypes);

var _addClass = __webpack_require__(/*! dom-helpers/class/addClass */ "./node_modules/dom-helpers/class/addClass.js");

var _addClass2 = _interopRequireDefault(_addClass);

var _removeClass = __webpack_require__(/*! dom-helpers/class/removeClass */ "./node_modules/dom-helpers/class/removeClass.js");

var _removeClass2 = _interopRequireDefault(_removeClass);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! ./Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _PropTypes = __webpack_require__(/*! ./utils/PropTypes */ "./node_modules/react-transition-group/utils/PropTypes.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addClass = function addClass(node, classes) {
  return classes && classes.split(' ').forEach(function (c) {
    return (0, _addClass2.default)(node, c);
  });
};
var removeClass = function removeClass(node, classes) {
  return classes && classes.split(' ').forEach(function (c) {
    return (0, _removeClass2.default)(node, c);
  });
};

var propTypes = _extends({}, _Transition2.default.propTypes, {

  /**
   * The animation classNames applied to the component as it enters or exits.
   * A single name can be provided and it will be suffixed for each stage: e.g.
   *
   * `classNames="fade"` applies `fade-enter`, `fade-enter-active`,
   * `fade-exit`, `fade-exit-active`, `fade-appear`, and `fade-appear-active`.
   * Each individual classNames can also be specified independently like:
   *
   * ```js
   * classNames={{
   *  appear: 'my-appear',
   *  appearActive: 'my-active-appear',
   *  enter: 'my-enter',
   *  enterActive: 'my-active-enter',
   *  exit: 'my-exit',
   *  exitActive: 'my-active-exit',
   * }}
   * ```
   *
   * @type {string | {
   *  appear?: string,
   *  appearActive?: string,
   *  enter?: string,
   *  enterActive?: string,
   *  exit?: string,
   *  exitActive?: string,
   * }}
   */
  classNames: _PropTypes.classNamesShape,

  /**
   * A `<Transition>` callback fired immediately after the 'enter' or 'appear' class is
   * applied.
   *
   * @type Function(node: HtmlElement, isAppearing: bool)
   */
  onEnter: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'enter-active' or
   * 'appear-active' class is applied.
   *
   * @type Function(node: HtmlElement, isAppearing: bool)
   */
  onEntering: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'enter' or
   * 'appear' classes are **removed** from the DOM node.
   *
   * @type Function(node: HtmlElement, isAppearing: bool)
   */
  onEntered: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'exit' class is
   * applied.
   *
   * @type Function(node: HtmlElement)
   */
  onExit: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'exit-active' is applied.
   *
   * @type Function(node: HtmlElement
   */
  onExiting: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'exit' classes
   * are **removed** from the DOM node.
   *
   * @type Function(node: HtmlElement)
   */
  onExited: PropTypes.func
});

/**
 * A `Transition` component using CSS transitions and animations.
 * It's inspired by the excellent [ng-animate](http://www.nganimate.org/) library.
 *
 * `CSSTransition` applies a pair of class names during the `appear`, `enter`,
 * and `exit` stages of the transition. The first class is applied and then a
 * second "active" class in order to activate the css animation.
 *
 * When the `in` prop is toggled to `true` the Component will get
 * the `example-enter` CSS class and the `example-enter-active` CSS class
 * added in the next tick. This is a convention based on the `classNames` prop.
 *
 * ```js
 * import CSSTransition from 'react-transition-group/CSSTransition';
 *
 * const Fade = ({ children, ...props }) => (
 *  <CSSTransition
 *    {...props}
 *    timeout={500}
 *    classNames="fade"
 *  >
 *   {children}
 *  </CSSTransition>
 * );
 *
 * class FadeInAndOut extends React.Component {
 *   constructor(...args) {
 *     super(...args);
 *     this.state= { show: false }
 *
 *     setInterval(() => {
 *       this.setState({ show: !this.state.show })
 *     }, 5000)
 *   }
 *   render() {
 *     return (
 *       <Fade in={this.state.show}>
 *         <div>Hello world</div>
 *       </Fade>
 *     )
 *   }
 * }
 * ```
 *
 * And the coorresponding CSS for the `<Fade>` component:
 *
 * ```css
 * .fade-enter {
 *   opacity: 0.01;
 * }
 *
 * .fade-enter.fade-enter-active {
 *   opacity: 1;
 *   transition: opacity 500ms ease-in;
 * }
 *
 * .fade-exit {
 *   opacity: 1;
 * }
 *
 * .fade-exit.fade-exit-active {
 *   opacity: 0.01;
 *   transition: opacity 300ms ease-in;
 * }
 * ```
 */

var CSSTransition = function (_React$Component) {
  _inherits(CSSTransition, _React$Component);

  function CSSTransition() {
    var _temp, _this, _ret;

    _classCallCheck(this, CSSTransition);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.onEnter = function (node, appearing) {
      var _this$getClassNames = _this.getClassNames(appearing ? 'appear' : 'enter'),
          className = _this$getClassNames.className;

      _this.removeClasses(node, 'exit');
      addClass(node, className);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.onEntering = function (node, appearing) {
      var _this$getClassNames2 = _this.getClassNames(appearing ? 'appear' : 'enter'),
          activeClassName = _this$getClassNames2.activeClassName;

      _this.reflowAndAddClass(node, activeClassName);

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.onEntered = function (node, appearing) {
      _this.removeClasses(node, appearing ? 'appear' : 'enter');

      if (_this.props.onEntered) {
        _this.props.onEntered(node);
      }
    }, _this.onExit = function (node) {
      var _this$getClassNames3 = _this.getClassNames('exit'),
          className = _this$getClassNames3.className;

      _this.removeClasses(node, 'appear');
      _this.removeClasses(node, 'enter');
      addClass(node, className);

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _this.onExiting = function (node) {
      var _this$getClassNames4 = _this.getClassNames('exit'),
          activeClassName = _this$getClassNames4.activeClassName;

      _this.reflowAndAddClass(node, activeClassName);

      if (_this.props.onExiting) {
        _this.props.onExiting(node);
      }
    }, _this.onExited = function (node) {
      _this.removeClasses(node, 'exit');

      if (_this.props.onExited) {
        _this.props.onExited(node);
      }
    }, _this.getClassNames = function (type) {
      var classNames = _this.props.classNames;


      var className = typeof classNames !== 'string' ? classNames[type] : classNames + '-' + type;

      var activeClassName = typeof classNames !== 'string' ? classNames[type + 'Active'] : className + '-active';

      return { className: className, activeClassName: activeClassName };
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  CSSTransition.prototype.removeClasses = function removeClasses(node, type) {
    var _getClassNames = this.getClassNames(type),
        className = _getClassNames.className,
        activeClassName = _getClassNames.activeClassName;

    className && removeClass(node, className);
    activeClassName && removeClass(node, activeClassName);
  };

  CSSTransition.prototype.reflowAndAddClass = function reflowAndAddClass(node, className) {
    // This is for to force a repaint,
    // which is necessary in order to transition styles when adding a class name.
    /* eslint-disable no-unused-expressions */
    node.scrollTop;
    /* eslint-enable no-unused-expressions */
    addClass(node, className);
  };

  CSSTransition.prototype.render = function render() {
    var props = _extends({}, this.props);

    delete props.classNames;

    return _react2.default.createElement(_Transition2.default, _extends({}, props, {
      onEnter: this.onEnter,
      onEntered: this.onEntered,
      onEntering: this.onEntering,
      onExit: this.onExit,
      onExiting: this.onExiting,
      onExited: this.onExited
    }));
  };

  return CSSTransition;
}(_react2.default.Component);

CSSTransition.propTypes =  true ? propTypes : {};

exports.default = CSSTransition;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/users/signOut.js":
/*!*********************************************!*\
  !*** ./src/client/actions/users/signOut.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _users = __webpack_require__(/*! ../../constants/users */ "./src/client/constants/users.js");

exports.default = function () {
  return { type: _users.SIGN_OUT };
}; /**
    * @format
    * 
    */

/***/ }),

/***/ "./src/client/containers/AppHeader/Search.js":
/*!***************************************************!*\
  !*** ./src/client/containers/AppHeader/Search.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! material-ui/styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

var _Search = __webpack_require__(/*! material-ui-icons/Search */ "./node_modules/material-ui-icons/Search.js");

var _Search2 = _interopRequireDefault(_Search);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  var _root;

  return {
    root: (_root = {
      fontFamily: theme.typography.fontFamily,
      position: 'relative',
      borderRadius: '6px',
      background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.15)
    }, (0, _defineProperty3.default)(_root, theme.breakpoints.up('lg'), {
      background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.15),
      '&:hover': {
        background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.25)
      },
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 190,
        '&:focus': {
          width: 290
        }
      }
    }), (0, _defineProperty3.default)(_root, theme.breakpoints.down('lg'), {
      background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.15),
      '&:hover': {
        background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.25)
      },
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 190,
        '&:focus': {
          width: 290
        }
      }
    }), (0, _defineProperty3.default)(_root, theme.breakpoints.down('md'), {
      background: 'none',
      borderRadius: '6px',
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 0,
        borderRadius: '6px',
        '&:focus': {
          width: 130,
          borderRadius: '6px',
          background: (0, _colorManipulator.fade)(theme.palette.common.white, 0.25)
        }
      }
    }), _root),
    search: {
      width: theme.spacing.unit * 10,
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    input: {
      font: 'inherit',
      padding: theme.spacing.unit + 'px ' + theme.spacing.unit + 'px ' + theme.spacing.unit + 'px ' + theme.spacing.unit * 9 + 'px',
      border: 0,
      display: 'block',
      verticalAlign: 'middle',
      whiteSpace: 'normal',
      background: 'none',
      margin: 0, // Reset for Safari
      color: 'inherit',
      width: '100%',
      '&:focus': {
        outline: 0
      }
    }
  };
}; /** @format */

var SearchBox = function (_Component) {
  (0, _inherits3.default)(SearchBox, _Component);

  function SearchBox(props) {
    (0, _classCallCheck3.default)(this, SearchBox);

    var _this = (0, _possibleConstructorReturn3.default)(this, (SearchBox.__proto__ || (0, _getPrototypeOf2.default)(SearchBox)).call(this, props));

    _this.state = {
      value: ''
    };
    return _this;
  }

  (0, _createClass3.default)(SearchBox, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var classes = this.props.classes;
      var value = this.state.value;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          'div',
          { className: classes.search },
          _react2.default.createElement(_Search2.default, null)
        ),
        _react2.default.createElement('input', {
          value: value,
          onChange: function onChange(e) {
            return _this2.setState({ value: e.target.value });
          },
          className: classes.input
        })
      );
    }
  }]);
  return SearchBox;
}(_react.Component);

SearchBox.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(SearchBox);

/***/ }),

/***/ "./src/client/containers/AppHeader/index.js":
/*!**************************************************!*\
  !*** ./src/client/containers/AppHeader/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactRouter = __webpack_require__(/*! react-router */ "./node_modules/react-router/es/index.js");

var _classnames2 = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames3 = _interopRequireDefault(_classnames2);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _AppBar = __webpack_require__(/*! material-ui/AppBar */ "./node_modules/material-ui/AppBar/index.js");

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Toolbar = __webpack_require__(/*! material-ui/Toolbar */ "./node_modules/material-ui/Toolbar/index.js");

var _Toolbar2 = _interopRequireDefault(_Toolbar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Menu = __webpack_require__(/*! material-ui/Menu */ "./node_modules/material-ui/Menu/index.js");

var _Menu2 = _interopRequireDefault(_Menu);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Menu3 = __webpack_require__(/*! material-ui-icons/Menu */ "./node_modules/material-ui-icons/Menu.js");

var _Menu4 = _interopRequireDefault(_Menu3);

var _Search = __webpack_require__(/*! ./Search */ "./src/client/containers/AppHeader/Search.js");

var _Search2 = _interopRequireDefault(_Search);

var _signOut = __webpack_require__(/*! ../../actions/users/signOut */ "./src/client/actions/users/signOut.js");

var _signOut2 = _interopRequireDefault(_signOut);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      width: '100%',
      // backgroundColor: theme.palette.primary[700],
      color: theme.palette.getContrastText(theme.palette.primary[700])
    },
    rootHome: {
      boxShadow: 'none',
      background: 'transparent'
    },
    menuButton: {
      color: theme.palette.getContrastText(theme.palette.primary[700])
    },
    title: {
      flex: '0 1 auto'
    },
    titleLink: {
      color: theme.palette.getContrastText(theme.palette.primary[700]),
      textDecoration: 'none'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    moreVertButton: {
      color: theme.palette.getContrastText(theme.palette.primary[700])
    }
  };
}; /**
    * This is a Header Component
    * It will be displayed on Head
    *
    * @format
    */

var AppHeader = function (_Component) {
  (0, _inherits3.default)(AppHeader, _Component);

  function AppHeader(props) {
    (0, _classCallCheck3.default)(this, AppHeader);

    var _this = (0, _possibleConstructorReturn3.default)(this, (AppHeader.__proto__ || (0, _getPrototypeOf2.default)(AppHeader)).call(this, props));

    _this.onSignOut = function () {
      _this.props.actionSignOut();
      _this.props.history.push('/');
    };

    _this.state = {
      anchorEl: null,
      open: false
    };

    _this.onClick = _this.onClick.bind(_this);
    _this.onRequestClose = _this.onRequestClose.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(AppHeader, [{
    key: 'onClick',
    value: function onClick(e) {
      this.setState({
        open: true,
        anchorEl: e.currentTarget
      });
    }
  }, {
    key: 'onRequestClose',
    value: function onRequestClose() {
      this.setState({ open: false });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements,
          openDrawer = _props.openDrawer;
      var isSignedIn = elements.user.isSignedIn;
      var _state = this.state,
          open = _state.open,
          anchorEl = _state.anchorEl;
      var pathname = this.props.location.pathname;


      return _react2.default.createElement(
        _AppBar2.default,
        {
          className: (0, _classnames3.default)(classes.root, (0, _defineProperty3.default)({}, '' + classes.rootHome, !isSignedIn && pathname === '/'))
        },
        _react2.default.createElement(
          _Toolbar2.default,
          null,
          _react2.default.createElement(
            _IconButton2.default,
            {
              'aria-label': 'Menu',
              onClick: openDrawer,
              className: (0, _classnames3.default)(classes.menuButton)
            },
            _react2.default.createElement(_Menu4.default, null)
          ),
          _react2.default.createElement(
            _Typography2.default,
            {
              className: classes.title,
              type: 'title',
              color: 'inherit',
              noWrap: true
            },
            !(!isSignedIn && pathname === '/') ? _react2.default.createElement(
              _Link2.default,
              { to: '/', className: classes.titleLink },
              'Mayash'
            ) : null
          ),
          _react2.default.createElement('div', { className: classes.flexGrow }),
          _react2.default.createElement(
            _IconButton2.default,
            {
              'aria-label': 'More',
              'aria-owns': open ? 'long-menu' : null,
              'aria-haspopup': 'true',
              onClick: this.onClick,
              className: (0, _classnames3.default)(classes.moreVertButton)
            },
            _react2.default.createElement(_MoreVert2.default, null)
          ),
          _react2.default.createElement(
            _Menu2.default,
            {
              id: 'long-menu',
              anchorEl: anchorEl,
              open: open,
              onRequestClose: this.onRequestClose,
              PaperProps: {
                style: {
                  maxHeight: 250,
                  width: 200
                }
              }
            },
            isSignedIn ? _react2.default.createElement(
              _Menu.MenuItem,
              { onClick: this.onSignOut },
              'Sign Out'
            ) : _react2.default.createElement(
              _Menu.MenuItem,
              {
                button: true,
                component: _Link2.default,
                to: '/sign-in',
                onClick: this.onRequestClose
              },
              'Sign In'
            )
          )
        )
      );
    }
  }]);
  return AppHeader;
}(_react.Component);

AppHeader.propTypes = {
  // match: PropTypes.object.isRequired,
  location: _propTypes2.default.shape({
    pathname: _propTypes2.default.string.isRequired
  }).isRequired,
  history: _propTypes2.default.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  openDrawer: _propTypes2.default.func.isRequired,
  actionSignOut: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref) {
  var elements = _ref.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionSignOut: _signOut2.default
  }, dispatch);
};

exports.default = (0, _reactRouter.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(AppHeader)));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvY2xhc3MvYWRkQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2NsYXNzL2hhc0NsYXNzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9yZW1vdmVDbGFzcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTWVudS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NlYXJjaC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL0FwcEJhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtQXZhdGFyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtSWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbVNlY29uZGFyeUFjdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbVRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdFN1YmhlYWRlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9NZW51L01lbnVJdGVtLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9NZW51L01lbnVMaXN0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9NZW51L2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9CYWNrZHJvcC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvTW9kYWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1BvcG92ZXIvUG9wb3Zlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvUG9wb3Zlci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Ub29sYmFyL1Rvb2xiYXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1Rvb2xiYXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1BvcnRhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvRmFkZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvR3Jvdy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwL0NTU1RyYW5zaXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvdXNlcnMvc2lnbk91dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQXBwSGVhZGVyL1NlYXJjaC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQXBwSGVhZGVyL2luZGV4LmpzIl0sIm5hbWVzIjpbInR5cGUiLCJzdHlsZXMiLCJ0aGVtZSIsInJvb3QiLCJmb250RmFtaWx5IiwidHlwb2dyYXBoeSIsInBvc2l0aW9uIiwiYm9yZGVyUmFkaXVzIiwiYmFja2dyb3VuZCIsInBhbGV0dGUiLCJjb21tb24iLCJ3aGl0ZSIsImJyZWFrcG9pbnRzIiwidXAiLCJ0cmFuc2l0aW9uIiwidHJhbnNpdGlvbnMiLCJjcmVhdGUiLCJ3aWR0aCIsImRvd24iLCJzZWFyY2giLCJzcGFjaW5nIiwidW5pdCIsImhlaWdodCIsInBvaW50ZXJFdmVudHMiLCJkaXNwbGF5IiwiYWxpZ25JdGVtcyIsImp1c3RpZnlDb250ZW50IiwiaW5wdXQiLCJmb250IiwicGFkZGluZyIsImJvcmRlciIsInZlcnRpY2FsQWxpZ24iLCJ3aGl0ZVNwYWNlIiwibWFyZ2luIiwiY29sb3IiLCJvdXRsaW5lIiwiU2VhcmNoQm94IiwicHJvcHMiLCJzdGF0ZSIsInZhbHVlIiwiY2xhc3NlcyIsImUiLCJzZXRTdGF0ZSIsInRhcmdldCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJnZXRDb250cmFzdFRleHQiLCJwcmltYXJ5Iiwicm9vdEhvbWUiLCJib3hTaGFkb3ciLCJtZW51QnV0dG9uIiwidGl0bGUiLCJmbGV4IiwidGl0bGVMaW5rIiwidGV4dERlY29yYXRpb24iLCJmbGV4R3JvdyIsIm1vcmVWZXJ0QnV0dG9uIiwiQXBwSGVhZGVyIiwib25TaWduT3V0IiwiYWN0aW9uU2lnbk91dCIsImhpc3RvcnkiLCJwdXNoIiwiYW5jaG9yRWwiLCJvcGVuIiwib25DbGljayIsImJpbmQiLCJvblJlcXVlc3RDbG9zZSIsImN1cnJlbnRUYXJnZXQiLCJlbGVtZW50cyIsIm9wZW5EcmF3ZXIiLCJpc1NpZ25lZEluIiwidXNlciIsInBhdGhuYW1lIiwibG9jYXRpb24iLCJzdHlsZSIsIm1heEhlaWdodCIsInNoYXBlIiwic3RyaW5nIiwiZnVuYyIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUcsWUFBWTtBQUNmO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNwQkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQSwwREFBMEQsMEpBQTBKO0FBQ3BOO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNoQkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0EscUZBQXFGO0FBQ3JGO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNUQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw2REFBNkQsbUhBQW1IO0FBQ2hMLEU7Ozs7Ozs7Ozs7Ozs7QUNSQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNUQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0M7Ozs7Ozs7Ozs7Ozs7QUNUQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUEsb0M7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxxREFBcUQ7O0FBRXZHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx5SkFBeUo7O0FBRTNNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxrUEFBa1A7O0FBRXBTO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0Esb0pBQW9KOztBQUVwSjtBQUNBO0FBQ0EsZ0NBQWdDLHdFQUF3RTtBQUN4RztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELG9CQUFvQixVOzs7Ozs7Ozs7Ozs7O0FDNUt6RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsZ0hBQWdIOztBQUVoSDtBQUNBO0FBQ0EsZ0NBQWdDLDhDQUE4QztBQUM5RTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELGtCQUFrQixROzs7Ozs7Ozs7Ozs7O0FDOUl2RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STtBQUM1STs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RTtBQUM5RTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBLGFBQWEsMEJBQTBCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHdCQUF3QixjOzs7Ozs7Ozs7Ozs7O0FDck83RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrRUFBK0U7O0FBRS9FO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCLFVBQVUsZUFBZTtBQUNoRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBLHFEQUFxRCxrQkFBa0IsUTs7Ozs7Ozs7Ozs7OztBQ3hMdkU7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUEsK0VBQStFOztBQUUvRSxrREFBa0QsMkNBQTJDO0FBQzdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVywrQkFBK0I7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEscURBQXFELHNCQUFzQixZOzs7Ozs7Ozs7Ozs7O0FDeFEzRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0EseUVBQXlFO0FBQ3pFLGlGQUFpRjtBQUNqRixHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBLHFEQUFxRCw0QkFBNEIsa0I7Ozs7Ozs7Ozs7Ozs7QUN4R2pGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUEscURBQXFELDBCQUEwQixnQjs7Ozs7Ozs7Ozs7OztBQzdFL0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQSxTQUFTOztBQUVUOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsS0FBSyxnRUFBZ0U7QUFDckU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQSxxREFBcUQscUNBQXFDLDJCOzs7Ozs7Ozs7Ozs7O0FDN0UxRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixtUEFBNEk7O0FBRTVJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTCxZQUFZO0FBQ1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrRUFBK0U7O0FBRS9FO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0ZBQStGO0FBQy9GLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtGQUErRjtBQUMvRixXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCwwQkFBMEIsZ0I7Ozs7Ozs7Ozs7Ozs7QUM3Sy9FOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlRQUEwSjs7QUFFMUo7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrRUFBK0U7O0FBRS9FO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsMkJBQTJCLGlCOzs7Ozs7Ozs7Ozs7O0FDM0toRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDckU3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7OztBQUdBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkRBQTJEO0FBQzNEO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0NBQStDO0FBQy9DLDhDQUE4QztBQUM5QztBQUNBLGFBQWE7QUFDYixXQUFXO0FBQ1gsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxtQ0FBbUMsUTs7Ozs7Ozs7Ozs7OztBQ2pUeEY7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGOztBQUU5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHNCQUFzQixZOzs7Ozs7Ozs7Ozs7O0FDM0ozRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsMEJBQTBCO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLHFCQUFxQix5QkFBeUI7QUFDOUM7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCwyQjs7Ozs7Ozs7Ozs7OztBQ3pRQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDakM3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLGtHQUFrRzs7QUFFbEc7QUFDQTtBQUNBLGdDQUFnQyxrREFBa0Q7QUFDbEY7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsc0JBQXNCLFk7Ozs7Ozs7Ozs7Ozs7QUN4STNFOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsZUFBZTtBQUN0QztBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixnQkFBZ0I7QUFDdkM7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxnQ0FBZ0MsZ0VBQWdFO0FBQ2hHO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsMEdBQTBHO0FBQzFHLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFCQUFxQixlQUFlO0FBQ3BDO0FBQ0E7QUFDQTs7QUFFQSxxREFBcUQsZ0NBQWdDLFM7Ozs7Ozs7Ozs7Ozs7QUNqbEJyRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtRkFBbUY7QUFDbkY7QUFDQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx1QkFBdUIsdUJBQXVCO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EscUJBQXFCLHVCQUF1QjtBQUM1QztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLHNCQUFzQjs7QUFFdEI7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ3RKQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLGdDQUFnQyxzQ0FBc0M7QUFDdEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiLHlFQUF5RSxnREFBZ0Q7QUFDekg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQscUJBQXFCLFc7Ozs7Ozs7Ozs7Ozs7QUNoaUIxRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7O0FBRTlGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQscUJBQXFCLFc7Ozs7Ozs7Ozs7Ozs7QUNsTDFFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsbVBBQTRJOztBQUU1STtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsb0NBQW9DO0FBQ3BDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7O0FBRTlGO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELHFCQUFxQixXOzs7Ozs7Ozs7Ozs7O0FDMUgxRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QseUI7Ozs7Ozs7Ozs7Ozs7QUNyTEE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7Ozs7QUNoQkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDJDQUEyQzs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUQ7Ozs7Ozs7Ozs7Ozs7QUNwTkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTzs7QUFFUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSwyQ0FBMkM7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUQ7Ozs7Ozs7Ozs7Ozs7QUM1UkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDOzs7Ozs7Ozs7Ozs7O0FDaERBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Riw4Q0FBOEMsaUJBQWlCLHFCQUFxQixvQ0FBb0MsNkRBQTZELG9CQUFvQixFQUFFLGVBQWU7O0FBRTFOLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdGQUFnRjs7QUFFaEY7O0FBRUEsZ0ZBQWdGLGVBQWU7O0FBRS9GLHlEQUF5RCxVQUFVLHVEQUF1RDtBQUMxSDs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsdUI7Ozs7Ozs7Ozs7Ozs7QUM3R0E7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLHVDQUF1Qyw2QkFBNkIsWUFBWSxFQUFFLE9BQU8saUJBQWlCLG1CQUFtQix1QkFBdUIsNEVBQTRFLEVBQUUsRUFBRSxzQkFBc0IsZUFBZSxFQUFFOztBQUUzUSxpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQSwyQkFBMkI7O0FBRTNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQixxQkFBcUI7QUFDdkM7QUFDQSxPQUFPO0FBQ1AsZUFBZTtBQUNmO0FBQ0E7QUFDQSxNQUFNO0FBQ047QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CO0FBQ3BCO0FBQ0E7QUFDQSx3QkFBd0IseUJBQXlCO0FBQ2pELFFBQVE7QUFDUjtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsZ0JBQWdCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOzs7QUFHQTs7QUFFQTs7QUFFQSxjQUFjO0FBQ2QsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMkJBQTJCOztBQUUzQjs7QUFFQSwwRUFBMEU7QUFDMUU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBLG9DOzs7Ozs7Ozs7Ozs7O0FDdFRBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7Ozs7QUNyQkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2ZBOztBQUVBOztBQUVBLG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUTtBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLDhEOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ1pBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix5Qzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUN6REE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixVQUFVLHFCQUFxQjtBQUM1RDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVEE7O2tCQU1lO0FBQUEsU0FBZSxFQUFFQSxxQkFBRixFQUFmO0FBQUEsQyxFQVhmOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7O0FBRUE7Ozs7OztBQUVBLElBQU1DLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxLQUFEO0FBQUE7O0FBQUEsU0FBWTtBQUN6QkM7QUFDRUMsa0JBQVlGLE1BQU1HLFVBQU4sQ0FBaUJELFVBRC9CO0FBRUVFLGdCQUFVLFVBRlo7QUFHRUMsb0JBQWMsS0FIaEI7QUFJRUMsa0JBQVksNEJBQUtOLE1BQU1PLE9BQU4sQ0FBY0MsTUFBZCxDQUFxQkMsS0FBMUIsRUFBaUMsSUFBakM7QUFKZCw0Q0FLR1QsTUFBTVUsV0FBTixDQUFrQkMsRUFBbEIsQ0FBcUIsSUFBckIsQ0FMSCxFQUtnQztBQUM1Qkwsa0JBQVksNEJBQUtOLE1BQU1PLE9BQU4sQ0FBY0MsTUFBZCxDQUFxQkMsS0FBMUIsRUFBaUMsSUFBakMsQ0FEZ0I7QUFFNUIsaUJBQVc7QUFDVEgsb0JBQVksNEJBQUtOLE1BQU1PLE9BQU4sQ0FBY0MsTUFBZCxDQUFxQkMsS0FBMUIsRUFBaUMsSUFBakM7QUFESCxPQUZpQjtBQUs1QixrQkFBWTtBQUNWRyxvQkFBWVosTUFBTWEsV0FBTixDQUFrQkMsTUFBbEIsQ0FBeUIsT0FBekIsQ0FERjtBQUVWQyxlQUFPLEdBRkc7QUFHVixtQkFBVztBQUNUQSxpQkFBTztBQURFO0FBSEQ7QUFMZ0IsS0FMaEMsd0NBa0JHZixNQUFNVSxXQUFOLENBQWtCTSxJQUFsQixDQUF1QixJQUF2QixDQWxCSCxFQWtCa0M7QUFDOUJWLGtCQUFZLDRCQUFLTixNQUFNTyxPQUFOLENBQWNDLE1BQWQsQ0FBcUJDLEtBQTFCLEVBQWlDLElBQWpDLENBRGtCO0FBRTlCLGlCQUFXO0FBQ1RILG9CQUFZLDRCQUFLTixNQUFNTyxPQUFOLENBQWNDLE1BQWQsQ0FBcUJDLEtBQTFCLEVBQWlDLElBQWpDO0FBREgsT0FGbUI7QUFLOUIsa0JBQVk7QUFDVkcsb0JBQVlaLE1BQU1hLFdBQU4sQ0FBa0JDLE1BQWxCLENBQXlCLE9BQXpCLENBREY7QUFFVkMsZUFBTyxHQUZHO0FBR1YsbUJBQVc7QUFDVEEsaUJBQU87QUFERTtBQUhEO0FBTGtCLEtBbEJsQyx3Q0ErQkdmLE1BQU1VLFdBQU4sQ0FBa0JNLElBQWxCLENBQXVCLElBQXZCLENBL0JILEVBK0JrQztBQUM5QlYsa0JBQVksTUFEa0I7QUFFOUJELG9CQUFjLEtBRmdCO0FBRzlCLGtCQUFZO0FBQ1ZPLG9CQUFZWixNQUFNYSxXQUFOLENBQWtCQyxNQUFsQixDQUF5QixPQUF6QixDQURGO0FBRVZDLGVBQU8sQ0FGRztBQUdWVixzQkFBYyxLQUhKO0FBSVYsbUJBQVc7QUFDVFUsaUJBQU8sR0FERTtBQUVUVix3QkFBYyxLQUZMO0FBR1RDLHNCQUFZLDRCQUFLTixNQUFNTyxPQUFOLENBQWNDLE1BQWQsQ0FBcUJDLEtBQTFCLEVBQWlDLElBQWpDO0FBSEg7QUFKRDtBQUhrQixLQS9CbEMsU0FEeUI7QUErQ3pCUSxZQUFRO0FBQ05GLGFBQU9mLE1BQU1rQixPQUFOLENBQWNDLElBQWQsR0FBcUIsRUFEdEI7QUFFTkMsY0FBUSxNQUZGO0FBR05oQixnQkFBVSxVQUhKO0FBSU5pQixxQkFBZSxNQUpUO0FBS05DLGVBQVMsTUFMSDtBQU1OQyxrQkFBWSxRQU5OO0FBT05DLHNCQUFnQjtBQVBWLEtBL0NpQjtBQXdEekJDLFdBQU87QUFDTEMsWUFBTSxTQUREO0FBRUxDLGVBQVkzQixNQUFNa0IsT0FBTixDQUFjQyxJQUExQixXQUFvQ25CLE1BQU1rQixPQUFOLENBQWNDLElBQWxELFdBQ0VuQixNQUFNa0IsT0FBTixDQUFjQyxJQURoQixXQUVNbkIsTUFBTWtCLE9BQU4sQ0FBY0MsSUFBZCxHQUFxQixDQUYzQixPQUZLO0FBS0xTLGNBQVEsQ0FMSDtBQU1MTixlQUFTLE9BTko7QUFPTE8scUJBQWUsUUFQVjtBQVFMQyxrQkFBWSxRQVJQO0FBU0x4QixrQkFBWSxNQVRQO0FBVUx5QixjQUFRLENBVkgsRUFVTTtBQUNYQyxhQUFPLFNBWEY7QUFZTGpCLGFBQU8sTUFaRjtBQWFMLGlCQUFXO0FBQ1RrQixpQkFBUztBQURBO0FBYk47QUF4RGtCLEdBQVo7QUFBQSxDQUFmLEMsQ0FWQTs7SUFxRk1DLFM7OztBQUNKLHFCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsNElBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYTtBQUNYQyxhQUFPO0FBREksS0FBYjtBQUZpQjtBQUtsQjs7Ozs2QkFFUTtBQUFBOztBQUFBLFVBQ0NDLE9BREQsR0FDYSxLQUFLSCxLQURsQixDQUNDRyxPQUREO0FBQUEsVUFFQ0QsS0FGRCxHQUVXLEtBQUtELEtBRmhCLENBRUNDLEtBRkQ7OztBQUlQLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBV0MsUUFBUXJDLElBQXhCO0FBQ0U7QUFBQTtBQUFBLFlBQUssV0FBV3FDLFFBQVFyQixNQUF4QjtBQUNFO0FBREYsU0FERjtBQUlFO0FBQ0UsaUJBQU9vQixLQURUO0FBRUUsb0JBQVUsa0JBQUNFLENBQUQ7QUFBQSxtQkFBTyxPQUFLQyxRQUFMLENBQWMsRUFBRUgsT0FBT0UsRUFBRUUsTUFBRixDQUFTSixLQUFsQixFQUFkLENBQVA7QUFBQSxXQUZaO0FBR0UscUJBQVdDLFFBQVFiO0FBSHJCO0FBSkYsT0FERjtBQVlEOzs7OztBQUdIUyxVQUFVUSxTQUFWLEdBQXNCO0FBQ3BCSixXQUFTLG9CQUFVSyxNQUFWLENBQWlCQztBQUROLENBQXRCOztrQkFJZSwwQkFBVTdDLE1BQVYsRUFBa0JtQyxTQUFsQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0dmOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOztBQUNBOztBQUNBOztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1uQyxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRDtBQUFBLFNBQVk7QUFDekJDLFVBQU07QUFDSmMsYUFBTyxNQURIO0FBRUo7QUFDQWlCLGFBQU9oQyxNQUFNTyxPQUFOLENBQWNzQyxlQUFkLENBQThCN0MsTUFBTU8sT0FBTixDQUFjdUMsT0FBZCxDQUFzQixHQUF0QixDQUE5QjtBQUhILEtBRG1CO0FBTXpCQyxjQUFVO0FBQ1JDLGlCQUFXLE1BREg7QUFFUjFDLGtCQUFZO0FBRkosS0FOZTtBQVV6QjJDLGdCQUFZO0FBQ1ZqQixhQUFPaEMsTUFBTU8sT0FBTixDQUFjc0MsZUFBZCxDQUE4QjdDLE1BQU1PLE9BQU4sQ0FBY3VDLE9BQWQsQ0FBc0IsR0FBdEIsQ0FBOUI7QUFERyxLQVZhO0FBYXpCSSxXQUFPO0FBQ0xDLFlBQU07QUFERCxLQWJrQjtBQWdCekJDLGVBQVc7QUFDVHBCLGFBQU9oQyxNQUFNTyxPQUFOLENBQWNzQyxlQUFkLENBQThCN0MsTUFBTU8sT0FBTixDQUFjdUMsT0FBZCxDQUFzQixHQUF0QixDQUE5QixDQURFO0FBRVRPLHNCQUFnQjtBQUZQLEtBaEJjO0FBb0J6QkMsY0FBVTtBQUNSSCxZQUFNO0FBREUsS0FwQmU7QUF1QnpCSSxvQkFBZ0I7QUFDZHZCLGFBQU9oQyxNQUFNTyxPQUFOLENBQWNzQyxlQUFkLENBQThCN0MsTUFBTU8sT0FBTixDQUFjdUMsT0FBZCxDQUFzQixHQUF0QixDQUE5QjtBQURPO0FBdkJTLEdBQVo7QUFBQSxDQUFmLEMsQ0EvQkE7Ozs7Ozs7SUEyRE1VLFM7OztBQUNKLHFCQUFZckIsS0FBWixFQUFtQjtBQUFBOztBQUFBLDRJQUNYQSxLQURXOztBQUFBLFVBc0JuQnNCLFNBdEJtQixHQXNCUCxZQUFNO0FBQ2hCLFlBQUt0QixLQUFMLENBQVd1QixhQUFYO0FBQ0EsWUFBS3ZCLEtBQUwsQ0FBV3dCLE9BQVgsQ0FBbUJDLElBQW5CLENBQXdCLEdBQXhCO0FBQ0QsS0F6QmtCOztBQUVqQixVQUFLeEIsS0FBTCxHQUFhO0FBQ1h5QixnQkFBVSxJQURDO0FBRVhDLFlBQU07QUFGSyxLQUFiOztBQUtBLFVBQUtDLE9BQUwsR0FBZSxNQUFLQSxPQUFMLENBQWFDLElBQWIsT0FBZjtBQUNBLFVBQUtDLGNBQUwsR0FBc0IsTUFBS0EsY0FBTCxDQUFvQkQsSUFBcEIsT0FBdEI7QUFSaUI7QUFTbEI7Ozs7NEJBRU96QixDLEVBQUc7QUFDVCxXQUFLQyxRQUFMLENBQWM7QUFDWnNCLGNBQU0sSUFETTtBQUVaRCxrQkFBVXRCLEVBQUUyQjtBQUZBLE9BQWQ7QUFJRDs7O3FDQUVnQjtBQUNmLFdBQUsxQixRQUFMLENBQWMsRUFBRXNCLE1BQU0sS0FBUixFQUFkO0FBQ0Q7Ozs2QkFPUTtBQUFBLG1CQUNtQyxLQUFLM0IsS0FEeEM7QUFBQSxVQUNDRyxPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVNkIsUUFEVixVQUNVQSxRQURWO0FBQUEsVUFDb0JDLFVBRHBCLFVBQ29CQSxVQURwQjtBQUFBLFVBRUNDLFVBRkQsR0FFZ0JGLFNBQVNHLElBRnpCLENBRUNELFVBRkQ7QUFBQSxtQkFHb0IsS0FBS2pDLEtBSHpCO0FBQUEsVUFHQzBCLElBSEQsVUFHQ0EsSUFIRDtBQUFBLFVBR09ELFFBSFAsVUFHT0EsUUFIUDtBQUFBLFVBSUNVLFFBSkQsR0FJYyxLQUFLcEMsS0FBTCxDQUFXcUMsUUFKekIsQ0FJQ0QsUUFKRDs7O0FBTVAsYUFDRTtBQUFBO0FBQUE7QUFDRSxxQkFBVywwQkFBV2pDLFFBQVFyQyxJQUFuQix5Q0FDTHFDLFFBQVFTLFFBREgsRUFDZ0IsQ0FBQ3NCLFVBQUQsSUFBZUUsYUFBYSxHQUQ1QztBQURiO0FBS0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsNEJBQVcsTUFEYjtBQUVFLHVCQUFTSCxVQUZYO0FBR0UseUJBQVcsMEJBQVc5QixRQUFRVyxVQUFuQjtBQUhiO0FBS0U7QUFMRixXQURGO0FBUUU7QUFBQTtBQUFBO0FBQ0UseUJBQVdYLFFBQVFZLEtBRHJCO0FBRUUsb0JBQUssT0FGUDtBQUdFLHFCQUFNLFNBSFI7QUFJRTtBQUpGO0FBTUcsY0FBRSxDQUFDbUIsVUFBRCxJQUFlRSxhQUFhLEdBQTlCLElBQ0M7QUFBQTtBQUFBLGdCQUFNLElBQUksR0FBVixFQUFlLFdBQVdqQyxRQUFRYyxTQUFsQztBQUFBO0FBQUEsYUFERCxHQUlHO0FBVk4sV0FSRjtBQW9CRSxpREFBSyxXQUFXZCxRQUFRZ0IsUUFBeEIsR0FwQkY7QUFzQkU7QUFBQTtBQUFBO0FBQ0UsNEJBQVcsTUFEYjtBQUVFLDJCQUFXUSxPQUFPLFdBQVAsR0FBcUIsSUFGbEM7QUFHRSwrQkFBYyxNQUhoQjtBQUlFLHVCQUFTLEtBQUtDLE9BSmhCO0FBS0UseUJBQVcsMEJBQVd6QixRQUFRaUIsY0FBbkI7QUFMYjtBQU9FO0FBUEYsV0F0QkY7QUErQkU7QUFBQTtBQUFBO0FBQ0Usa0JBQUcsV0FETDtBQUVFLHdCQUFVTSxRQUZaO0FBR0Usb0JBQU1DLElBSFI7QUFJRSw4QkFBZ0IsS0FBS0csY0FKdkI7QUFLRSwwQkFBWTtBQUNWUSx1QkFBTztBQUNMQyw2QkFBVyxHQUROO0FBRUwzRCx5QkFBTztBQUZGO0FBREc7QUFMZDtBQVlHc0QseUJBQ0M7QUFBQTtBQUFBLGdCQUFVLFNBQVMsS0FBS1osU0FBeEI7QUFBQTtBQUFBLGFBREQsR0FHQztBQUFBO0FBQUE7QUFDRSw0QkFERjtBQUVFLHlDQUZGO0FBR0Usb0JBQUksVUFITjtBQUlFLHlCQUFTLEtBQUtRO0FBSmhCO0FBQUE7QUFBQTtBQWZKO0FBL0JGO0FBTEYsT0FERjtBQWlFRDs7Ozs7QUFHSFQsVUFBVWQsU0FBVixHQUFzQjtBQUNwQjtBQUNBOEIsWUFBVSxvQkFBVUcsS0FBVixDQUFnQjtBQUN4QkosY0FBVSxvQkFBVUssTUFBVixDQUFpQmhDO0FBREgsR0FBaEIsRUFFUEEsVUFKaUI7QUFLcEJlLFdBQVMsb0JBQVVoQixNQUFWLENBQWlCQyxVQUxOOztBQU9wQk4sV0FBUyxvQkFBVUssTUFBVixDQUFpQkMsVUFQTjs7QUFTcEJ1QixZQUFVLG9CQUFVeEIsTUFBVixDQUFpQkMsVUFUUDs7QUFXcEJ3QixjQUFZLG9CQUFVUyxJQUFWLENBQWVqQyxVQVhQO0FBWXBCYyxpQkFBZSxvQkFBVW1CLElBQVYsQ0FBZWpDO0FBWlYsQ0FBdEI7O0FBZUEsSUFBTWtDLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHWCxRQUFILFFBQUdBLFFBQUg7QUFBQSxTQUFtQixFQUFFQSxrQkFBRixFQUFuQjtBQUFBLENBQXhCOztBQUVBLElBQU1ZLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFdEI7QUFERixHQURGLEVBSUVzQixRQUpGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVFlLDZCQUNiLHlCQUFRRixlQUFSLEVBQXlCQyxrQkFBekIsRUFBNkMsMEJBQVdoRixNQUFYLEVBQW1CeUQsU0FBbkIsQ0FBN0MsQ0FEYSxDIiwiZmlsZSI6IjEuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gYWN0aXZlRWxlbWVudDtcblxudmFyIF9vd25lckRvY3VtZW50ID0gcmVxdWlyZSgnLi9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gYWN0aXZlRWxlbWVudCgpIHtcbiAgdmFyIGRvYyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KSgpO1xuXG4gIHRyeSB7XG4gICAgcmV0dXJuIGRvYy5hY3RpdmVFbGVtZW50O1xuICB9IGNhdGNoIChlKSB7LyogaWUgdGhyb3dzIGlmIG5vIGFjdGl2ZSBlbGVtZW50ICovfVxufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSBhZGRDbGFzcztcblxudmFyIF9oYXNDbGFzcyA9IHJlcXVpcmUoJy4vaGFzQ2xhc3MnKTtcblxudmFyIF9oYXNDbGFzczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9oYXNDbGFzcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIGFkZENsYXNzKGVsZW1lbnQsIGNsYXNzTmFtZSkge1xuICBpZiAoZWxlbWVudC5jbGFzc0xpc3QpIGVsZW1lbnQuY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO2Vsc2UgaWYgKCEoMCwgX2hhc0NsYXNzMi5kZWZhdWx0KShlbGVtZW50LCBjbGFzc05hbWUpKSBpZiAodHlwZW9mIGVsZW1lbnQuY2xhc3NOYW1lID09PSAnc3RyaW5nJykgZWxlbWVudC5jbGFzc05hbWUgPSBlbGVtZW50LmNsYXNzTmFtZSArICcgJyArIGNsYXNzTmFtZTtlbHNlIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdjbGFzcycsIChlbGVtZW50LmNsYXNzTmFtZSAmJiBlbGVtZW50LmNsYXNzTmFtZS5iYXNlVmFsIHx8ICcnKSArICcgJyArIGNsYXNzTmFtZSk7XG59XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9hZGRDbGFzcy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvY2xhc3MvYWRkQ2xhc3MuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIlwidXNlIHN0cmljdFwiO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gaGFzQ2xhc3M7XG5mdW5jdGlvbiBoYXNDbGFzcyhlbGVtZW50LCBjbGFzc05hbWUpIHtcbiAgaWYgKGVsZW1lbnQuY2xhc3NMaXN0KSByZXR1cm4gISFjbGFzc05hbWUgJiYgZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoY2xhc3NOYW1lKTtlbHNlIHJldHVybiAoXCIgXCIgKyAoZWxlbWVudC5jbGFzc05hbWUuYmFzZVZhbCB8fCBlbGVtZW50LmNsYXNzTmFtZSkgKyBcIiBcIikuaW5kZXhPZihcIiBcIiArIGNsYXNzTmFtZSArIFwiIFwiKSAhPT0gLTE7XG59XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbXCJkZWZhdWx0XCJdO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2NsYXNzL2hhc0NsYXNzLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9oYXNDbGFzcy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5mdW5jdGlvbiByZXBsYWNlQ2xhc3NOYW1lKG9yaWdDbGFzcywgY2xhc3NUb1JlbW92ZSkge1xuICByZXR1cm4gb3JpZ0NsYXNzLnJlcGxhY2UobmV3IFJlZ0V4cCgnKF58XFxcXHMpJyArIGNsYXNzVG9SZW1vdmUgKyAnKD86XFxcXHN8JCknLCAnZycpLCAnJDEnKS5yZXBsYWNlKC9cXHMrL2csICcgJykucmVwbGFjZSgvXlxccyp8XFxzKiQvZywgJycpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIHJlbW92ZUNsYXNzKGVsZW1lbnQsIGNsYXNzTmFtZSkge1xuICBpZiAoZWxlbWVudC5jbGFzc0xpc3QpIGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShjbGFzc05hbWUpO2Vsc2UgaWYgKHR5cGVvZiBlbGVtZW50LmNsYXNzTmFtZSA9PT0gJ3N0cmluZycpIGVsZW1lbnQuY2xhc3NOYW1lID0gcmVwbGFjZUNsYXNzTmFtZShlbGVtZW50LmNsYXNzTmFtZSwgY2xhc3NOYW1lKTtlbHNlIGVsZW1lbnQuc2V0QXR0cmlidXRlKCdjbGFzcycsIHJlcGxhY2VDbGFzc05hbWUoZWxlbWVudC5jbGFzc05hbWUgJiYgZWxlbWVudC5jbGFzc05hbWUuYmFzZVZhbCB8fCAnJywgY2xhc3NOYW1lKSk7XG59O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2NsYXNzL3JlbW92ZUNsYXNzLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9yZW1vdmVDbGFzcy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSBvd25lckRvY3VtZW50O1xuZnVuY3Rpb24gb3duZXJEb2N1bWVudChub2RlKSB7XG4gIHJldHVybiBub2RlICYmIG5vZGUub3duZXJEb2N1bWVudCB8fCBkb2N1bWVudDtcbn1cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1tcImRlZmF1bHRcIl07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGdldFdpbmRvdztcbmZ1bmN0aW9uIGdldFdpbmRvdyhub2RlKSB7XG4gIHJldHVybiBub2RlID09PSBub2RlLndpbmRvdyA/IG5vZGUgOiBub2RlLm5vZGVUeXBlID09PSA5ID8gbm9kZS5kZWZhdWx0VmlldyB8fCBub2RlLnBhcmVudFdpbmRvdyA6IGZhbHNlO1xufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzW1wiZGVmYXVsdFwiXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9xdWVyeS9pc1dpbmRvdy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBmdW5jdGlvbiAocmVjYWxjKSB7XG4gIGlmICghc2l6ZSAmJiBzaXplICE9PSAwIHx8IHJlY2FsYykge1xuICAgIGlmIChfaW5ET00yLmRlZmF1bHQpIHtcbiAgICAgIHZhciBzY3JvbGxEaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcblxuICAgICAgc2Nyb2xsRGl2LnN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJztcbiAgICAgIHNjcm9sbERpdi5zdHlsZS50b3AgPSAnLTk5OTlweCc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUud2lkdGggPSAnNTBweCc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUuaGVpZ2h0ID0gJzUwcHgnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLm92ZXJmbG93ID0gJ3Njcm9sbCc7XG5cbiAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoc2Nyb2xsRGl2KTtcbiAgICAgIHNpemUgPSBzY3JvbGxEaXYub2Zmc2V0V2lkdGggLSBzY3JvbGxEaXYuY2xpZW50V2lkdGg7XG4gICAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKHNjcm9sbERpdik7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHNpemU7XG59O1xuXG52YXIgX2luRE9NID0gcmVxdWlyZSgnLi9pbkRPTScpO1xuXG52YXIgX2luRE9NMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luRE9NKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNpemUgPSB2b2lkIDA7XG5cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1snZGVmYXVsdCddO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvdXRpbC9zY3JvbGxiYXJTaXplLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMgMThoMTh2LTJIM3Yyem0wLTVoMTh2LTJIM3Yyem0wLTd2MmgxOFY2SDN6JyB9KTtcblxudmFyIE1lbnUgPSBmdW5jdGlvbiBNZW51KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5NZW51ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShNZW51KTtcbk1lbnUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gTWVudTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9NZW51LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9NZW51LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDhjMS4xIDAgMi0uOSAyLTJzLS45LTItMi0yLTIgLjktMiAyIC45IDIgMiAyem0wIDJjLTEuMSAwLTIgLjktMiAycy45IDIgMiAyIDItLjkgMi0yLS45LTItMi0yem0wIDZjLTEuMSAwLTIgLjktMiAycy45IDIgMiAyIDItLjkgMi0yLS45LTItMi0yeicgfSk7XG5cbnZhciBNb3JlVmVydCA9IGZ1bmN0aW9uIE1vcmVWZXJ0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Nb3JlVmVydCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoTW9yZVZlcnQpO1xuTW9yZVZlcnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gTW9yZVZlcnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyNyAyOCAzMSAzNCAzNSAzNyA0MCA0MSA0MiA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE1LjUgMTRoLS43OWwtLjI4LS4yN0MxNS40MSAxMi41OSAxNiAxMS4xMSAxNiA5LjUgMTYgNS45MSAxMy4wOSAzIDkuNSAzUzMgNS45MSAzIDkuNSA1LjkxIDE2IDkuNSAxNmMxLjYxIDAgMy4wOS0uNTkgNC4yMy0xLjU3bC4yNy4yOHYuNzlsNSA0Ljk5TDIwLjQ5IDE5bC00Ljk5LTV6bS02IDBDNy4wMSAxNCA1IDExLjk5IDUgOS41UzcuMDEgNSA5LjUgNSAxNCA3LjAxIDE0IDkuNSAxMS45OSAxNCA5LjUgMTR6JyB9KTtcblxudmFyIFNlYXJjaCA9IGZ1bmN0aW9uIFNlYXJjaChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuU2VhcmNoID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTZWFyY2gpO1xuU2VhcmNoLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFNlYXJjaDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TZWFyY2guanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NlYXJjaC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfUGFwZXIgPSByZXF1aXJlKCcuLi9QYXBlcicpO1xuXG52YXIgX1BhcGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BhcGVyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgUGFwZXJcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgZmxleERpcmVjdGlvbjogJ2NvbHVtbicsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgYm94U2l6aW5nOiAnYm9yZGVyLWJveCcsIC8vIFByZXZlbnQgcGFkZGluZyBpc3N1ZSB3aXRoIHRoZSBNb2RhbCBhbmQgZml4ZWQgcG9zaXRpb25lZCBBcHBCYXIuXG4gICAgICB6SW5kZXg6IHRoZW1lLnpJbmRleC5hcHBCYXIsXG4gICAgICBmbGV4U2hyaW5rOiAwXG4gICAgfSxcbiAgICBwb3NpdGlvbkZpeGVkOiB7XG4gICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6ICdhdXRvJyxcbiAgICAgIHJpZ2h0OiAwXG4gICAgfSxcbiAgICBwb3NpdGlvbkFic29sdXRlOiB7XG4gICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6ICdhdXRvJyxcbiAgICAgIHJpZ2h0OiAwXG4gICAgfSxcbiAgICBwb3NpdGlvblN0YXRpYzoge1xuICAgICAgcG9zaXRpb246ICdzdGF0aWMnLFxuICAgICAgZmxleFNocmluazogMFxuICAgIH0sXG4gICAgY29sb3JEZWZhdWx0OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5hcHBCYXIsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmFwcEJhcilcbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDAsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMClcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ3ByaW1hcnknLCAnYWNjZW50JywgJ2RlZmF1bHQnXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qb3NpdGlvbiA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3N0YXRpYycsICdmaXhlZCcsICdhYnNvbHV0ZSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAncHJpbWFyeScsICdhY2NlbnQnLCAnZGVmYXVsdCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgcG9zaXRpb25pbmcgdHlwZS5cbiAgICovXG4gIHBvc2l0aW9uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydzdGF0aWMnLCAnZml4ZWQnLCAnYWJzb2x1dGUnXSkuaXNSZXF1aXJlZFxufTtcblxudmFyIEFwcEJhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEFwcEJhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQXBwQmFyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEFwcEJhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEFwcEJhci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQXBwQmFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShBcHBCYXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgcG9zaXRpb24gPSBfcHJvcHMucG9zaXRpb24sXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAncG9zaXRpb24nXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc2VzWydwb3NpdGlvbicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShwb3NpdGlvbildLCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgJ211aS1maXhlZCcsIHBvc2l0aW9uID09PSAnZml4ZWQnKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfUGFwZXIyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBzcXVhcmU6IHRydWUsIGNvbXBvbmVudDogJ2hlYWRlcicsIGVsZXZhdGlvbjogNCwgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEFwcEJhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkFwcEJhci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAncHJpbWFyeScsXG4gIHBvc2l0aW9uOiAnZml4ZWQnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUFwcEJhcicgfSkoQXBwQmFyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvQXBwQmFyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvQXBwQmFyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0FwcEJhciA9IHJlcXVpcmUoJy4vQXBwQmFyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0FwcEJhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHVzZXJTZWxlY3Q6ICdub25lJ1xuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckFjdGlvbjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZVxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JEaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBjb2xvckVycm9yOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5lcnJvcls1MDBdXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db2xvciA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgbmFtZSBvZiB0aGUgaWNvbiBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSkuaXNSZXF1aXJlZFxufTtcblxudmFyIEljb24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShJY29uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJY29uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEljb24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChJY29uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShJY29uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShJY29uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvciddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KSgnbWF0ZXJpYWwtaWNvbnMnLCBjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2luaGVyaXQnKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ3NwYW4nLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUsICdhcmlhLWhpZGRlbic6ICd0cnVlJyB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gSWNvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkljb24uZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ2luaGVyaXQnXG59O1xuSWNvbi5tdWlOYW1lID0gJ0ljb24nO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUljb24nIH0pKEljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vSWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9JY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNiA3IDggOSAzNCAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9JY29uID0gcmVxdWlyZSgnLi9JY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA2IDcgOCA5IDM0IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9CdXR0b25CYXNlID0gcmVxdWlyZSgnLi4vQnV0dG9uQmFzZScpO1xuXG52YXIgX0J1dHRvbkJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQnV0dG9uQmFzZSk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9JY29uID0gcmVxdWlyZSgnLi4vSWNvbicpO1xuXG52YXIgX0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbik7XG5cbnJlcXVpcmUoJy4uL1N2Z0ljb24nKTtcblxudmFyIF9yZWFjdEhlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9yZWFjdEhlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTsgLy8gIHdlYWtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgQnV0dG9uQmFzZVxuXG4vLyBFbnN1cmUgQ1NTIHNwZWNpZmljaXR5XG5cblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgICAgIGZsZXg6ICcwIDAgYXV0bycsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDI0KSxcbiAgICAgIHdpZHRoOiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA2LFxuICAgICAgaGVpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA2LFxuICAgICAgcGFkZGluZzogMCxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdiYWNrZ3JvdW5kLWNvbG9yJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlc3RcbiAgICAgIH0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfSxcbiAgICBjb2xvckluaGVyaXQ6IHtcbiAgICAgIGNvbG9yOiAnaW5oZXJpdCdcbiAgICB9LFxuICAgIGRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGxhYmVsOiB7XG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgYWxpZ25JdGVtczogJ2luaGVyaXQnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgaWNvbjoge1xuICAgICAgd2lkdGg6ICcxZW0nLFxuICAgICAgaGVpZ2h0OiAnMWVtJ1xuICAgIH0sXG4gICAga2V5Ym9hcmRGb2N1c2VkOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgbmF0aXZlIGJ1dHRvbiBjb21wb25lbnQuXG4gICAqL1xuICBidXR0b25SZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiBlbGVtZW50LlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGFuIGljb24gZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2RlZmF1bHQnLCAnaW5oZXJpdCcsICdwcmltYXJ5JywgJ2NvbnRyYXN0JywgJ2FjY2VudCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBidXR0b24gd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSByaXBwbGUgd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVSaXBwbGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIHJvb3QgY29tcG9uZW50LlxuICAgKi9cbiAgcm9vdFJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmNcbn07XG5cbi8qKlxuICogUmVmZXIgdG8gdGhlIFtJY29uc10oL3N0eWxlL2ljb25zKSBzZWN0aW9uIG9mIHRoZSBkb2N1bWVudGF0aW9uXG4gKiByZWdhcmRpbmcgdGhlIGF2YWlsYWJsZSBpY29uIG9wdGlvbnMuXG4gKi9cbnZhciBJY29uQnV0dG9uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoSWNvbkJ1dHRvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gSWNvbkJ1dHRvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBJY29uQnV0dG9uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoSWNvbkJ1dHRvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoSWNvbkJ1dHRvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoSWNvbkJ1dHRvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJ1dHRvblJlZiA9IF9wcm9wcy5idXR0b25SZWYsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgICAgcm9vdFJlZiA9IF9wcm9wcy5yb290UmVmLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2J1dHRvblJlZicsICdjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICdkaXNhYmxlZCcsICdyb290UmVmJ10pO1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0J1dHRvbkJhc2UyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdkZWZhdWx0JyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmRpc2FibGVkLCBkaXNhYmxlZCksIF9jbGFzc05hbWVzKSwgY2xhc3NOYW1lKSxcbiAgICAgICAgICBjZW50ZXJSaXBwbGU6IHRydWUsXG4gICAgICAgICAga2V5Ym9hcmRGb2N1c2VkQ2xhc3NOYW1lOiBjbGFzc2VzLmtleWJvYXJkRm9jdXNlZCxcbiAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZWRcbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICByb290UmVmOiBidXR0b25SZWYsXG4gICAgICAgICAgcmVmOiByb290UmVmXG4gICAgICAgIH0pLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnc3BhbicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMubGFiZWwgfSxcbiAgICAgICAgICB0eXBlb2YgY2hpbGRyZW4gPT09ICdzdHJpbmcnID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBfSWNvbjIuZGVmYXVsdCxcbiAgICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmljb24gfSxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKSA6IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICAgICAgaWYgKCgwLCBfcmVhY3RIZWxwZXJzLmlzTXVpRWxlbWVudCkoY2hpbGQsIFsnSWNvbicsICdTdmdJY29uJ10pKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMuaWNvbiwgY2hpbGQucHJvcHMuY2xhc3NOYW1lKVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgICAgIH0pXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBJY29uQnV0dG9uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuSWNvbkJ1dHRvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnZGVmYXVsdCcsXG4gIGRpc2FibGVkOiBmYWxzZSxcbiAgZGlzYWJsZVJpcHBsZTogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpSWNvbkJ1dHRvbicgfSkoSWNvbkJ1dHRvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDMgNiAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNyAzOCAzOSA0MCA0NCA0NSA0NiA0OSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9JY29uQnV0dG9uID0gcmVxdWlyZSgnLi9JY29uQnV0dG9uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb25CdXR0b24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDMgNiAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNyAzOCAzOSA0MCA0NCA0NSA0NiA0OSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gICAgICBsaXN0U3R5bGU6ICdub25lJyxcbiAgICAgIG1hcmdpbjogMCxcbiAgICAgIHBhZGRpbmc6IDAsXG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJ1xuICAgIH0sXG4gICAgcGFkZGluZzoge1xuICAgICAgcGFkZGluZ1RvcDogdGhlbWUuc3BhY2luZy51bml0LFxuICAgICAgcGFkZGluZ0JvdHRvbTogdGhlbWUuc3BhY2luZy51bml0XG4gICAgfSxcbiAgICBkZW5zZToge1xuICAgICAgcGFkZGluZ1RvcDogdGhlbWUuc3BhY2luZy51bml0IC8gMixcbiAgICAgIHBhZGRpbmdCb3R0b206IHRoZW1lLnNwYWNpbmcudW5pdCAvIDJcbiAgICB9LFxuICAgIHN1YmhlYWRlcjoge1xuICAgICAgcGFkZGluZ1RvcDogMFxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGNvbXBhY3QgdmVydGljYWwgcGFkZGluZyBkZXNpZ25lZCBmb3Iga2V5Ym9hcmQgYW5kIG1vdXNlIGlucHV0IHdpbGwgYmUgdXNlZCBmb3JcbiAgICogdGhlIGxpc3QgYW5kIGxpc3QgaXRlbXMuIFRoZSBwcm9wZXJ0eSBpcyBhdmFpbGFibGUgdG8gZGVzY2VuZGFudCBjb21wb25lbnRzIGFzIHRoZVxuICAgKiBgZGVuc2VgIGNvbnRleHQuXG4gICAqL1xuICBkZW5zZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB2ZXJ0aWNhbCBwYWRkaW5nIHdpbGwgYmUgcmVtb3ZlZCBmcm9tIHRoZSBsaXN0LlxuICAgKi9cbiAgZGlzYWJsZVBhZGRpbmc6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIHJvb3QgY29tcG9uZW50LlxuICAgKi9cbiAgcm9vdFJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQsIG5vcm1hbGx5IGBMaXN0SXRlbWAuXG4gICAqL1xuICBzdWJoZWFkZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKVxufTtcblxudmFyIExpc3QgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShMaXN0LCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaXN0KCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIExpc3QpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChMaXN0Ll9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShMaXN0KSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShMaXN0LCBbe1xuICAgIGtleTogJ2dldENoaWxkQ29udGV4dCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENoaWxkQ29udGV4dCgpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGRlbnNlOiB0aGlzLnByb3BzLmRlbnNlXG4gICAgICB9O1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIENvbXBvbmVudFByb3AgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICAgIGRpc2FibGVQYWRkaW5nID0gX3Byb3BzLmRpc2FibGVQYWRkaW5nLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGRlbnNlID0gX3Byb3BzLmRlbnNlLFxuICAgICAgICAgIHN1YmhlYWRlciA9IF9wcm9wcy5zdWJoZWFkZXIsXG4gICAgICAgICAgcm9vdFJlZiA9IF9wcm9wcy5yb290UmVmLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdkaXNhYmxlUGFkZGluZycsICdjaGlsZHJlbicsICdkZW5zZScsICdzdWJoZWFkZXInLCAncm9vdFJlZiddKTtcblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGVuc2UsIGRlbnNlICYmICFkaXNhYmxlUGFkZGluZyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLnBhZGRpbmcsICFkaXNhYmxlUGFkZGluZyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLnN1YmhlYWRlciwgc3ViaGVhZGVyKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIsIHsgcmVmOiByb290UmVmIH0pLFxuICAgICAgICBzdWJoZWFkZXIsXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTGlzdDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpc3QuZGVmYXVsdFByb3BzID0ge1xuICBjb21wb25lbnQ6ICd1bCcsXG4gIGRlbnNlOiBmYWxzZSxcbiAgZGlzYWJsZVBhZGRpbmc6IGZhbHNlXG59O1xuXG5cbkxpc3QuY2hpbGRDb250ZXh0VHlwZXMgPSB7XG4gIGRlbnNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2xcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlMaXN0JyB9KShMaXN0KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3QuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQnV0dG9uQmFzZSA9IHJlcXVpcmUoJy4uL0J1dHRvbkJhc2UnKTtcblxudmFyIF9CdXR0b25CYXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0J1dHRvbkJhc2UpO1xuXG52YXIgX3JlYWN0SGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL3JlYWN0SGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnZmxleC1zdGFydCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgdGV4dERlY29yYXRpb246ICdub25lJ1xuICAgIH0sXG4gICAgY29udGFpbmVyOiB7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJ1xuICAgIH0sXG4gICAga2V5Ym9hcmRGb2N1c2VkOiB7XG4gICAgICBiYWNrZ3JvdW5kOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlclxuICAgIH0sXG4gICAgZGVmYXVsdDoge1xuICAgICAgcGFkZGluZ1RvcDogMTIsXG4gICAgICBwYWRkaW5nQm90dG9tOiAxMlxuICAgIH0sXG4gICAgZGVuc2U6IHtcbiAgICAgIHBhZGRpbmdUb3A6IHRoZW1lLnNwYWNpbmcudW5pdCxcbiAgICAgIHBhZGRpbmdCb3R0b206IHRoZW1lLnNwYWNpbmcudW5pdFxuICAgIH0sXG4gICAgZGlzYWJsZWQ6IHtcbiAgICAgIG9wYWNpdHk6IDAuNVxuICAgIH0sXG4gICAgZGl2aWRlcjoge1xuICAgICAgYm9yZGVyQm90dG9tOiAnMXB4IHNvbGlkICcgKyB0aGVtZS5wYWxldHRlLnRleHQubGlnaHREaXZpZGVyXG4gICAgfSxcbiAgICBndXR0ZXJzOiB7XG4gICAgICBwYWRkaW5nTGVmdDogdGhlbWUuc3BhY2luZy51bml0ICogMixcbiAgICAgIHBhZGRpbmdSaWdodDogdGhlbWUuc3BhY2luZy51bml0ICogMlxuICAgIH0sXG4gICAgYnV0dG9uOiB7XG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2JhY2tncm91bmQtY29sb3InLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdFxuICAgICAgfSksXG4gICAgICAnJjpob3Zlcic6IHtcbiAgICAgICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlcixcbiAgICAgICAgLy8gUmVzZXQgb24gbW91c2UgZGV2aWNlc1xuICAgICAgICAnQG1lZGlhIChob3Zlcjogbm9uZSknOiB7XG4gICAgICAgICAgYmFja2dyb3VuZENvbG9yOiAndHJhbnNwYXJlbnQnXG4gICAgICAgIH0sXG4gICAgICAgICcmJGRpc2FibGVkJzoge1xuICAgICAgICAgIGJhY2tncm91bmRDb2xvcjogJ3RyYW5zcGFyZW50J1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcbiAgICBzZWNvbmRhcnlBY3Rpb246IHtcbiAgICAgIC8vIEFkZCBzb21lIHNwYWNlIHRvIGF2b2lkIGNvbGxpc2lvbiBhcyBgTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb25gXG4gICAgICAvLyBpcyBhYnNvbHV0ZWx5IHBvc2l0aW9ubmVkLlxuICAgICAgcGFkZGluZ1JpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA0XG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgTGlzdEl0ZW0gd2lsbCBiZSBhIGJ1dHRvbi5cbiAgICovXG4gIGJ1dHRvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIGZvciB0aGUgcm9vdCBub2RlLlxuICAgKiBFaXRoZXIgYSBzdHJpbmcgdG8gdXNlIGEgRE9NIGVsZW1lbnQgb3IgYSBjb21wb25lbnQuXG4gICAqL1xuICBjb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgY29tcGFjdCB2ZXJ0aWNhbCBwYWRkaW5nIGRlc2lnbmVkIGZvciBrZXlib2FyZCBhbmQgbW91c2UgaW5wdXQgd2lsbCBiZSB1c2VkLlxuICAgKi9cbiAgZGVuc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBsZWZ0IGFuZCByaWdodCBwYWRkaW5nIGlzIHJlbW92ZWQuXG4gICAqL1xuICBkaXNhYmxlR3V0dGVyczogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBhIDFweCBsaWdodCBib3JkZXIgaXMgYWRkZWQgdG8gdGhlIGJvdHRvbSBvZiB0aGUgbGlzdCBpdGVtLlxuICAgKi9cbiAgZGl2aWRlcjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxudmFyIExpc3RJdGVtID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTGlzdEl0ZW0sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpc3RJdGVtKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIExpc3RJdGVtKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoTGlzdEl0ZW0uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKExpc3RJdGVtKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShMaXN0SXRlbSwgW3tcbiAgICBrZXk6ICdnZXRDaGlsZENvbnRleHQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRDaGlsZENvbnRleHQoKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBkZW5zZTogdGhpcy5wcm9wcy5kZW5zZSB8fCB0aGlzLmNvbnRleHQuZGVuc2UgfHwgZmFsc2VcbiAgICAgIH07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBidXR0b24gPSBfcHJvcHMuYnV0dG9uLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcCA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgZGVuc2UgPSBfcHJvcHMuZGVuc2UsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgICAgZGl2aWRlciA9IF9wcm9wcy5kaXZpZGVyLFxuICAgICAgICAgIGRpc2FibGVHdXR0ZXJzID0gX3Byb3BzLmRpc2FibGVHdXR0ZXJzLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2J1dHRvbicsICdjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb21wb25lbnQnLCAnZGVuc2UnLCAnZGlzYWJsZWQnLCAnZGl2aWRlcicsICdkaXNhYmxlR3V0dGVycyddKTtcblxuICAgICAgdmFyIGlzRGVuc2UgPSBkZW5zZSB8fCB0aGlzLmNvbnRleHQuZGVuc2UgfHwgZmFsc2U7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4udG9BcnJheShjaGlsZHJlblByb3ApO1xuXG4gICAgICB2YXIgaGFzQXZhdGFyID0gY2hpbGRyZW4uc29tZShmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgcmV0dXJuICgwLCBfcmVhY3RIZWxwZXJzLmlzTXVpRWxlbWVudCkodmFsdWUsIFsnTGlzdEl0ZW1BdmF0YXInXSk7XG4gICAgICB9KTtcbiAgICAgIHZhciBoYXNTZWNvbmRhcnlBY3Rpb24gPSBjaGlsZHJlbi5sZW5ndGggJiYgKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KShjaGlsZHJlbltjaGlsZHJlbi5sZW5ndGggLSAxXSwgWydMaXN0SXRlbVNlY29uZGFyeUFjdGlvbiddKTtcblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZ3V0dGVycywgIWRpc2FibGVHdXR0ZXJzKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGl2aWRlciwgZGl2aWRlciksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmRpc2FibGVkLCBkaXNhYmxlZCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmJ1dHRvbiwgYnV0dG9uKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuc2Vjb25kYXJ5QWN0aW9uLCBoYXNTZWNvbmRhcnlBY3Rpb24pLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgaXNEZW5zZSB8fCBoYXNBdmF0YXIgPyBjbGFzc2VzLmRlbnNlIDogY2xhc3Nlcy5kZWZhdWx0LCB0cnVlKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgdmFyIGxpc3RJdGVtUHJvcHMgPSAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUsIGRpc2FibGVkOiBkaXNhYmxlZCB9LCBvdGhlcik7XG4gICAgICB2YXIgQ29tcG9uZW50TWFpbiA9IGNvbXBvbmVudFByb3A7XG5cbiAgICAgIGlmIChidXR0b24pIHtcbiAgICAgICAgQ29tcG9uZW50TWFpbiA9IF9CdXR0b25CYXNlMi5kZWZhdWx0O1xuICAgICAgICBsaXN0SXRlbVByb3BzLmNvbXBvbmVudCA9IGNvbXBvbmVudFByb3AgfHwgJ2xpJztcbiAgICAgICAgbGlzdEl0ZW1Qcm9wcy5rZXlib2FyZEZvY3VzZWRDbGFzc05hbWUgPSBjbGFzc2VzLmtleWJvYXJkRm9jdXNlZDtcbiAgICAgIH1cblxuICAgICAgaWYgKGhhc1NlY29uZGFyeUFjdGlvbikge1xuICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuY29udGFpbmVyIH0sXG4gICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBDb21wb25lbnRNYWluLFxuICAgICAgICAgICAgbGlzdEl0ZW1Qcm9wcyxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKSxcbiAgICAgICAgICBjaGlsZHJlbi5wb3AoKVxuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIENvbXBvbmVudE1haW4sXG4gICAgICAgIGxpc3RJdGVtUHJvcHMsXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTGlzdEl0ZW07XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5MaXN0SXRlbS5kZWZhdWx0UHJvcHMgPSB7XG4gIGJ1dHRvbjogZmFsc2UsXG4gIGNvbXBvbmVudDogJ2xpJyxcbiAgZGVuc2U6IGZhbHNlLFxuICBkaXNhYmxlZDogZmFsc2UsXG4gIGRpc2FibGVHdXR0ZXJzOiBmYWxzZSxcbiAgZGl2aWRlcjogZmFsc2Vcbn07XG5cblxuTGlzdEl0ZW0uY29udGV4dFR5cGVzID0ge1xuICBkZW5zZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sXG59O1xuXG5MaXN0SXRlbS5jaGlsZENvbnRleHRUeXBlcyA9IHtcbiAgZGVuc2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbFxufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUxpc3RJdGVtJyB9KShMaXN0SXRlbSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHdpZHRoOiAzNixcbiAgICAgIGhlaWdodDogMzYsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDE4KSxcbiAgICAgIG1hcmdpblJpZ2h0OiA0XG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICB3aWR0aDogMjAsXG4gICAgICBoZWlnaHQ6IDIwLFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyMClcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LCBub3JtYWxseSBgQXZhdGFyYC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbi8qKlxuICogSXQncyBhIHNpbXBsZSB3cmFwcGVyIHRvIGFwcGx5IHRoZSBgZGVuc2VgIG1vZGUgc3R5bGVzIHRvIGBBdmF0YXJgLlxuICovXG5mdW5jdGlvbiBMaXN0SXRlbUF2YXRhcihwcm9wcywgY29udGV4dCkge1xuICBpZiAoY29udGV4dC5kZW5zZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KShmYWxzZSwgJ01hdGVyaWFsLVVJOiA8TGlzdEl0ZW1BdmF0YXI+IGlzIGEgc2ltcGxlIHdyYXBwZXIgdG8gYXBwbHkgdGhlIGRlbnNlIHN0eWxlc1xcbiAgICAgIHRvIDxBdmF0YXI+LiBZb3UgZG8gbm90IG5lZWQgaXQgdW5sZXNzIHlvdSBhcmUgY29udHJvbGxpbmcgdGhlIDxMaXN0PiBkZW5zZSBwcm9wZXJ0eS4nKSA6IHZvaWQgMDtcbiAgICByZXR1cm4gcHJvcHMuY2hpbGRyZW47XG4gIH1cblxuICB2YXIgY2hpbGRyZW4gPSBwcm9wcy5jaGlsZHJlbixcbiAgICAgIGNsYXNzZXMgPSBwcm9wcy5jbGFzc2VzLFxuICAgICAgY2xhc3NOYW1lUHJvcCA9IHByb3BzLmNsYXNzTmFtZSxcbiAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkocHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnXSk7XG5cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZHJlbiwgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLnJvb3QsIGNvbnRleHQuZGVuc2UpLCBjbGFzc05hbWVQcm9wLCBjaGlsZHJlbi5wcm9wcy5jbGFzc05hbWUpLFxuICAgIGNoaWxkcmVuQ2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmljb24sIGNvbnRleHQuZGVuc2UpLCBjaGlsZHJlbi5wcm9wcy5jaGlsZHJlbkNsYXNzTmFtZSlcbiAgfSwgb3RoZXIpKTtcbn1cblxuTGlzdEl0ZW1BdmF0YXIuY29udGV4dFR5cGVzID0ge1xuICBkZW5zZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sXG59O1xuXG5MaXN0SXRlbUF2YXRhci5tdWlOYW1lID0gJ0xpc3RJdGVtQXZhdGFyJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUxpc3RJdGVtQXZhdGFyJyB9KShMaXN0SXRlbUF2YXRhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbUF2YXRhci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbUF2YXRhci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgaGVpZ2h0OiAyNCxcbiAgICAgIG1hcmdpblJpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAyLFxuICAgICAgd2lkdGg6IDI0LFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZSxcbiAgICAgIGZsZXhTaHJpbms6IDBcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LCBub3JtYWxseSBgSWNvbmAsIGBTdmdJY29uYCxcbiAgICogb3IgYSBgbWF0ZXJpYWwtdWktaWNvbnNgIFNWRyBpY29uIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbi8qKlxuICogQSBzaW1wbGUgd3JhcHBlciB0byBhcHBseSBgTGlzdGAgc3R5bGVzIHRvIGFuIGBJY29uYCBvciBgU3ZnSWNvbmAuXG4gKi9cbmZ1bmN0aW9uIExpc3RJdGVtSWNvbihwcm9wcykge1xuICB2YXIgY2hpbGRyZW4gPSBwcm9wcy5jaGlsZHJlbixcbiAgICAgIGNsYXNzZXMgPSBwcm9wcy5jbGFzc2VzLFxuICAgICAgY2xhc3NOYW1lUHJvcCA9IHByb3BzLmNsYXNzTmFtZSxcbiAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkocHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnXSk7XG5cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZHJlbiwgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lUHJvcCwgY2hpbGRyZW4ucHJvcHMuY2xhc3NOYW1lKVxuICB9LCBvdGhlcikpO1xufVxuXG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpTGlzdEl0ZW1JY29uJyB9KShMaXN0SXRlbUljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1JY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtSWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9yZWY7IC8vICB3ZWFrXG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICByaWdodDogNCxcbiAgICAgIHRvcDogJzUwJScsXG4gICAgICBtYXJnaW5Ub3A6IC10aGVtZS5zcGFjaW5nLnVuaXQgKiAzXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudCwgbm9ybWFsbHkgYW4gYEljb25CdXR0b25gIG9yIHNlbGVjdGlvbiBjb250cm9sLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIExpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uKHByb3BzKSB7XG4gIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWUgPSBwcm9wcy5jbGFzc05hbWU7XG5cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgJ2RpdicsXG4gICAgeyBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWUpIH0sXG4gICAgY2hpbGRyZW5cbiAgKTtcbn1cblxuTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24ucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKF9yZWYgPSB7XG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKVxufSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzZXMnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0KSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzTmFtZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcpLCBfcmVmKSA6IHt9O1xuTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24ubXVpTmFtZSA9ICdMaXN0SXRlbVNlY29uZGFyeUFjdGlvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlMaXN0SXRlbVNlY29uZGFyeUFjdGlvbicgfSkoTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCAzNiA0MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX1R5cG9ncmFwaHkgPSByZXF1aXJlKCcuLi9UeXBvZ3JhcGh5Jyk7XG5cbnZhciBfVHlwb2dyYXBoeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UeXBvZ3JhcGh5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTsgLy8gIHdlYWtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZmxleDogJzEgMSBhdXRvJyxcbiAgICAgIHBhZGRpbmc6ICcwIDE2cHgnLFxuICAgICAgJyY6Zmlyc3QtY2hpbGQnOiB7XG4gICAgICAgIHBhZGRpbmdMZWZ0OiAwXG4gICAgICB9XG4gICAgfSxcbiAgICBpbnNldDoge1xuICAgICAgJyY6Zmlyc3QtY2hpbGQnOiB7XG4gICAgICAgIHBhZGRpbmdMZWZ0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA3XG4gICAgICB9XG4gICAgfSxcbiAgICBkZW5zZToge1xuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgxMylcbiAgICB9LFxuICAgIHRleHQ6IHt9LCAvLyBQcmVzZW50IHRvIGFsbG93IGV4dGVybmFsIGN1c3RvbWl6YXRpb25cbiAgICB0ZXh0RGVuc2U6IHtcbiAgICAgIGZvbnRTaXplOiAnaW5oZXJpdCdcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNoaWxkcmVuIHdvbid0IGJlIHdyYXBwZWQgYnkgYSB0eXBvZ3JhcGh5IGNvbXBvbmVudC5cbiAgICogRm9yIGluc3RhbmNlLCB0aGF0IGNhbiBiZSB1c2VmdWwgdG8gY2FuIHJlbmRlciBhbiBoNCBpbnN0ZWFkIG9mIGFcbiAgICovXG4gIGRpc2FibGVUeXBvZ3JhcGh5OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjaGlsZHJlbiB3aWxsIGJlIGluZGVudGVkLlxuICAgKiBUaGlzIHNob3VsZCBiZSB1c2VkIGlmIHRoZXJlIGlzIG5vIGxlZnQgYXZhdGFyIG9yIGxlZnQgaWNvbi5cbiAgICovXG4gIGluc2V0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuICBwcmltYXJ5OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcbiAgc2Vjb25kYXJ5OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZFxufTtcblxudmFyIExpc3RJdGVtVGV4dCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKExpc3RJdGVtVGV4dCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGlzdEl0ZW1UZXh0KCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIExpc3RJdGVtVGV4dCk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKExpc3RJdGVtVGV4dC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoTGlzdEl0ZW1UZXh0KSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShMaXN0SXRlbVRleHQsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZGlzYWJsZVR5cG9ncmFwaHkgPSBfcHJvcHMuZGlzYWJsZVR5cG9ncmFwaHksXG4gICAgICAgICAgcHJpbWFyeSA9IF9wcm9wcy5wcmltYXJ5LFxuICAgICAgICAgIHNlY29uZGFyeSA9IF9wcm9wcy5zZWNvbmRhcnksXG4gICAgICAgICAgaW5zZXQgPSBfcHJvcHMuaW5zZXQsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnZGlzYWJsZVR5cG9ncmFwaHknLCAncHJpbWFyeScsICdzZWNvbmRhcnknLCAnaW5zZXQnXSk7XG4gICAgICB2YXIgZGVuc2UgPSB0aGlzLmNvbnRleHQuZGVuc2U7XG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmRlbnNlLCBkZW5zZSksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmluc2V0LCBpbnNldCksIF9jbGFzc05hbWVzKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIHByaW1hcnkgJiYgKGRpc2FibGVUeXBvZ3JhcGh5ID8gcHJpbWFyeSA6IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIF9UeXBvZ3JhcGh5Mi5kZWZhdWx0LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIHR5cGU6ICdzdWJoZWFkaW5nJyxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnRleHQsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLnRleHREZW5zZSwgZGVuc2UpKVxuICAgICAgICAgIH0sXG4gICAgICAgICAgcHJpbWFyeVxuICAgICAgICApKSxcbiAgICAgICAgc2Vjb25kYXJ5ICYmIChkaXNhYmxlVHlwb2dyYXBoeSA/IHNlY29uZGFyeSA6IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIF9UeXBvZ3JhcGh5Mi5kZWZhdWx0LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGNvbG9yOiAnc2Vjb25kYXJ5JyxcbiAgICAgICAgICAgIHR5cGU6ICdib2R5MScsXG4gICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy50ZXh0LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy50ZXh0RGVuc2UsIGRlbnNlKSlcbiAgICAgICAgICB9LFxuICAgICAgICAgIHNlY29uZGFyeVxuICAgICAgICApKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIExpc3RJdGVtVGV4dDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpc3RJdGVtVGV4dC5kZWZhdWx0UHJvcHMgPSB7XG4gIGRpc2FibGVUeXBvZ3JhcGh5OiBmYWxzZSxcbiAgcHJpbWFyeTogZmFsc2UsXG4gIHNlY29uZGFyeTogZmFsc2UsXG4gIGluc2V0OiBmYWxzZVxufTtcbkxpc3RJdGVtVGV4dC5jb250ZXh0VHlwZXMgPSB7XG4gIGRlbnNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2xcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpTGlzdEl0ZW1UZXh0JyB9KShMaXN0SXRlbVRleHQpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1UZXh0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtVGV4dC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7IC8vICB3ZWFrXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGJveFNpemluZzogJ2JvcmRlci1ib3gnLFxuICAgICAgbGluZUhlaWdodDogJzQ4cHgnLFxuICAgICAgbGlzdFN0eWxlOiAnbm9uZScsXG4gICAgICBwYWRkaW5nTGVmdDogdGhlbWUuc3BhY2luZy51bml0ICogMixcbiAgICAgIHBhZGRpbmdSaWdodDogdGhlbWUuc3BhY2luZy51bml0ICogMixcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuc2Vjb25kYXJ5LFxuICAgICAgZm9udEZhbWlseTogdGhlbWUudHlwb2dyYXBoeS5mb250RmFtaWx5LFxuICAgICAgZm9udFdlaWdodDogdGhlbWUudHlwb2dyYXBoeS5mb250V2VpZ2h0TWVkaXVtLFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSh0aGVtZS50eXBvZ3JhcGh5LmZvbnRTaXplKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIGNvbG9ySW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgaW5zZXQ6IHtcbiAgICAgIHBhZGRpbmdMZWZ0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA5XG4gICAgfSxcbiAgICBzdGlja3k6IHtcbiAgICAgIHBvc2l0aW9uOiAnc3RpY2t5JyxcbiAgICAgIHRvcDogMCxcbiAgICAgIHpJbmRleDogMSxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJ2luaGVyaXQnXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIGZvciB0aGUgcm9vdCBub2RlLlxuICAgKiBFaXRoZXIgYSBzdHJpbmcgdG8gdXNlIGEgRE9NIGVsZW1lbnQgb3IgYSBjb21wb25lbnQuXG4gICAqIFRoZSBkZWZhdWx0IHZhbHVlIGlzIGEgYGJ1dHRvbmAuXG4gICAqL1xuICBjb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydkZWZhdWx0JywgJ3ByaW1hcnknLCAnaW5oZXJpdCddKSxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgTGlzdCBTdWJoZWFkZXIgd2lsbCBub3Qgc3RpY2sgdG8gdGhlIHRvcCBkdXJpbmcgc2Nyb2xsLlxuICAgKi9cbiAgZGlzYWJsZVN0aWNreTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIExpc3QgU3ViaGVhZGVyIHdpbGwgYmUgaW5kZW50ZWQuXG4gICAqL1xuICBpbnNldDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2xcbn07XG5cbnZhciBMaXN0U3ViaGVhZGVyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTGlzdFN1YmhlYWRlciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGlzdFN1YmhlYWRlcigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBMaXN0U3ViaGVhZGVyKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoTGlzdFN1YmhlYWRlci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoTGlzdFN1YmhlYWRlcikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoTGlzdFN1YmhlYWRlciwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBDb21wb25lbnRQcm9wID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBkaXNhYmxlU3RpY2t5ID0gX3Byb3BzLmRpc2FibGVTdGlja3ksXG4gICAgICAgICAgaW5zZXQgPSBfcHJvcHMuaW5zZXQsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29tcG9uZW50JywgJ2NvbG9yJywgJ2Rpc2FibGVTdGlja3knLCAnaW5zZXQnXSk7XG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2RlZmF1bHQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuaW5zZXQsIGluc2V0KSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuc3RpY2t5LCAhZGlzYWJsZVN0aWNreSksIF9jbGFzc05hbWVzKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQ29tcG9uZW50UHJvcCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBMaXN0U3ViaGVhZGVyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGlzdFN1YmhlYWRlci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbXBvbmVudDogJ2xpJyxcbiAgY29sb3I6ICdkZWZhdWx0JyxcbiAgZGlzYWJsZVN0aWNreTogZmFsc2UsXG4gIGluc2V0OiBmYWxzZVxufTtcbkxpc3RTdWJoZWFkZXIubXVpTmFtZSA9ICdMaXN0U3ViaGVhZGVyJztcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlMaXN0U3ViaGVhZGVyJyB9KShMaXN0U3ViaGVhZGVyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RTdWJoZWFkZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdFN1YmhlYWRlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0xpc3QgPSByZXF1aXJlKCcuL0xpc3QnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTGlzdCkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfTGlzdEl0ZW0gPSByZXF1aXJlKCcuL0xpc3RJdGVtJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnTGlzdEl0ZW0nLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaXN0SXRlbSkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfTGlzdEl0ZW1BdmF0YXIgPSByZXF1aXJlKCcuL0xpc3RJdGVtQXZhdGFyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnTGlzdEl0ZW1BdmF0YXInLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaXN0SXRlbUF2YXRhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfTGlzdEl0ZW1UZXh0ID0gcmVxdWlyZSgnLi9MaXN0SXRlbVRleHQnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdMaXN0SXRlbVRleHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaXN0SXRlbVRleHQpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX0xpc3RJdGVtSWNvbiA9IHJlcXVpcmUoJy4vTGlzdEl0ZW1JY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnTGlzdEl0ZW1JY29uJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTGlzdEl0ZW1JY29uKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9MaXN0SXRlbVNlY29uZGFyeUFjdGlvbiA9IHJlcXVpcmUoJy4vTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdMaXN0SXRlbVNlY29uZGFyeUFjdGlvbicsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9MaXN0U3ViaGVhZGVyID0gcmVxdWlyZSgnLi9MaXN0U3ViaGVhZGVyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnTGlzdFN1YmhlYWRlcicsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3RTdWJoZWFkZXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCAzNiA0MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3JlYWN0RG9tID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG5cbnZhciBfc2Nyb2xsYmFyU2l6ZSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZScpO1xuXG52YXIgX3Njcm9sbGJhclNpemUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2Nyb2xsYmFyU2l6ZSk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9Qb3BvdmVyID0gcmVxdWlyZSgnLi4vUG9wb3ZlcicpO1xuXG52YXIgX1BvcG92ZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUG9wb3Zlcik7XG5cbnZhciBfTWVudUxpc3QgPSByZXF1aXJlKCcuL01lbnVMaXN0Jyk7XG5cbnZhciBfTWVudUxpc3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTWVudUxpc3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBQb3BvdmVyXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBET00gZWxlbWVudCB1c2VkIHRvIHNldCB0aGUgcG9zaXRpb24gb2YgdGhlIG1lbnUuXG4gICAqL1xuICBhbmNob3JFbDogdHlwZW9mIEhUTUxFbGVtZW50ID09PSAnZnVuY3Rpb24nID8gcmVxdWlyZSgncHJvcC10eXBlcycpLmluc3RhbmNlT2YoSFRNTEVsZW1lbnQpIDogcmVxdWlyZSgncHJvcC10eXBlcycpLmFueSxcbiAgLy8gbWF0Y2ggUG9wb3ZlclxuICAvKipcbiAgICogTWVudSBjb250ZW50cywgbm9ybWFsbHkgYE1lbnVJdGVtYHMuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBNZW51TGlzdGAgZWxlbWVudC5cbiAgICovXG4gIE1lbnVMaXN0UHJvcHM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIGJlZm9yZSB0aGUgTWVudSBlbnRlcnMuXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIE1lbnUgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIE1lbnUgaGFzIGVudGVyZWQuXG4gICAqL1xuICBvbkVudGVyZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBNZW51IGV4aXRzLlxuICAgKi9cbiAgb25FeGl0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIE1lbnUgaXMgZXhpdGluZy5cbiAgICovXG4gIG9uRXhpdGluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBNZW51IGhhcyBleGl0ZWQuXG4gICAqL1xuICBvbkV4aXRlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBjb21wb25lbnQgcmVxdWVzdHMgdG8gYmUgY2xvc2VkLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2tcbiAgICovXG4gIG9uUmVxdWVzdENsb3NlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgbWVudSBpcyB2aXNpYmxlLlxuICAgKi9cbiAgb3BlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgUGFwZXJQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogYGNsYXNzZXNgIHByb3BlcnR5IGFwcGxpZWQgdG8gdGhlIGBQb3BvdmVyYCBlbGVtZW50LlxuICAgKi9cbiAgUG9wb3ZlckNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBsZW5ndGggb2YgdGhlIHRyYW5zaXRpb24gaW4gYG1zYCwgb3IgJ2F1dG8nXG4gICAqL1xuICB0cmFuc2l0aW9uRHVyYXRpb246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgZW50ZXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gICAgZXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxuICB9KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pLmlzUmVxdWlyZWRcbn07XG5cblxudmFyIHJ0bE9yaWdpbiA9IHtcbiAgdmVydGljYWw6ICd0b3AnLFxuICBob3Jpem9udGFsOiAncmlnaHQnXG59O1xuXG52YXIgbHRyT3JpZ2luID0ge1xuICB2ZXJ0aWNhbDogJ3RvcCcsXG4gIGhvcml6b250YWw6ICdsZWZ0J1xufTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0ge1xuICBwYXBlcjoge1xuICAgIC8vIHNwZWNaOiBUaGUgbWF4aW11bSBoZWlnaHQgb2YgYSBzaW1wbGUgbWVudSBzaG91bGQgYmUgb25lIG9yIG1vcmUgcm93cyBsZXNzIHRoYW4gdGhlIHZpZXdcbiAgICAvLyBoZWlnaHQuIFRoaXMgZW5zdXJlcyBhIHRhcHBhYmxlIGFyZWEgb3V0c2lkZSBvZiB0aGUgc2ltcGxlIG1lbnUgd2l0aCB3aGljaCB0byBkaXNtaXNzXG4gICAgLy8gdGhlIG1lbnUuXG4gICAgbWF4SGVpZ2h0OiAnY2FsYygxMDB2aCAtIDk2cHgpJyxcbiAgICAvLyBBZGQgaU9TIG1vbWVudHVtIHNjcm9sbGluZy5cbiAgICBXZWJraXRPdmVyZmxvd1Njcm9sbGluZzogJ3RvdWNoJ1xuICB9XG59O1xuXG52YXIgTWVudSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKE1lbnUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIE1lbnUoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgTWVudSk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBNZW51Ll9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShNZW51KSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuZ2V0Q29udGVudEFuY2hvckVsID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKCFfdGhpcy5tZW51TGlzdCB8fCAhX3RoaXMubWVudUxpc3Quc2VsZWN0ZWRJdGVtKSB7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgcmV0dXJuICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKF90aGlzLm1lbnVMaXN0KS5maXJzdENoaWxkO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkoX3RoaXMubWVudUxpc3Quc2VsZWN0ZWRJdGVtKTtcbiAgICB9LCBfdGhpcy5tZW51TGlzdCA9IHVuZGVmaW5lZCwgX3RoaXMuZm9jdXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMubWVudUxpc3QgJiYgX3RoaXMubWVudUxpc3Quc2VsZWN0ZWRJdGVtKSB7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkoX3RoaXMubWVudUxpc3Quc2VsZWN0ZWRJdGVtKS5mb2N1cygpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHZhciBtZW51TGlzdCA9ICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKF90aGlzLm1lbnVMaXN0KTtcbiAgICAgIGlmIChtZW51TGlzdCAmJiBtZW51TGlzdC5maXJzdENoaWxkKSB7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgbWVudUxpc3QuZmlyc3RDaGlsZC5mb2N1cygpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUVudGVyID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgIHZhciB0aGVtZSA9IF90aGlzLnByb3BzLnRoZW1lO1xuXG5cbiAgICAgIHZhciBtZW51TGlzdCA9ICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKF90aGlzLm1lbnVMaXN0KTtcblxuICAgICAgLy8gRm9jdXMgc28gdGhlIHNjcm9sbCBjb21wdXRhdGlvbiBvZiB0aGUgUG9wb3ZlciB3b3JrcyBhcyBleHBlY3RlZC5cbiAgICAgIF90aGlzLmZvY3VzKCk7XG5cbiAgICAgIC8vIExldCdzIGlnbm9yZSB0aGF0IHBpZWNlIG9mIGxvZ2ljIGlmIHVzZXJzIGFyZSBhbHJlYWR5IG92ZXJyaWRpbmcgdGhlIHdpZHRoXG4gICAgICAvLyBvZiB0aGUgbWVudS5cbiAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgIGlmIChtZW51TGlzdCAmJiBlbGVtZW50LmNsaWVudEhlaWdodCA8IG1lbnVMaXN0LmNsaWVudEhlaWdodCAmJiAhbWVudUxpc3Quc3R5bGUud2lkdGgpIHtcbiAgICAgICAgdmFyIHNpemUgPSAoMCwgX3Njcm9sbGJhclNpemUyLmRlZmF1bHQpKCkgKyAncHgnO1xuICAgICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICAgIG1lbnVMaXN0LnN0eWxlW3RoZW1lLmRpcmVjdGlvbiA9PT0gJ3J0bCcgPyAncGFkZGluZ0xlZnQnIDogJ3BhZGRpbmdSaWdodCddID0gc2l6ZTtcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICBtZW51TGlzdC5zdHlsZS53aWR0aCA9ICdjYWxjKDEwMCUgKyAnICsgc2l6ZSArICcpJztcbiAgICAgIH1cblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXIpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcihlbGVtZW50KTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVMaXN0S2V5RG93biA9IGZ1bmN0aW9uIChldmVudCwga2V5KSB7XG4gICAgICBpZiAoa2V5ID09PSAndGFiJykge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgIGlmIChfdGhpcy5wcm9wcy5vblJlcXVlc3RDbG9zZSkge1xuICAgICAgICAgIF90aGlzLnByb3BzLm9uUmVxdWVzdENsb3NlKGV2ZW50KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShNZW51LCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICBpZiAodGhpcy5wcm9wcy5vcGVuKSB7XG4gICAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICBpZiAoIXByZXZQcm9wcy5vcGVuICYmIHRoaXMucHJvcHMub3Blbikge1xuICAgICAgICAvLyBOZWVkcyB0byByZWZvY3VzIGFzIHdoZW4gYSBtZW51IGlzIHJlbmRlcmVkIGludG8gYW5vdGhlciBNb2RhbCxcbiAgICAgICAgLy8gdGhlIGZpcnN0IG1vZGFsIG1pZ2h0IGNoYW5nZSB0aGUgZm9jdXMgdG8gcHJldmVudCBhbnkgbGVhay5cbiAgICAgICAgdGhpcy5mb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgTWVudUxpc3RQcm9wcyA9IF9wcm9wcy5NZW51TGlzdFByb3BzLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMub25FbnRlcixcbiAgICAgICAgICBfcHJvcHMkUGFwZXJQcm9wcyA9IF9wcm9wcy5QYXBlclByb3BzLFxuICAgICAgICAgIFBhcGVyUHJvcHMgPSBfcHJvcHMkUGFwZXJQcm9wcyA9PT0gdW5kZWZpbmVkID8ge30gOiBfcHJvcHMkUGFwZXJQcm9wcyxcbiAgICAgICAgICBQb3BvdmVyQ2xhc3NlcyA9IF9wcm9wcy5Qb3BvdmVyQ2xhc3NlcyxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ01lbnVMaXN0UHJvcHMnLCAnb25FbnRlcicsICdQYXBlclByb3BzJywgJ1BvcG92ZXJDbGFzc2VzJywgJ3RoZW1lJ10pO1xuXG5cbiAgICAgIHZhciB0aGVtZURpcmVjdGlvbiA9IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbjtcbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX1BvcG92ZXIyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGdldENvbnRlbnRBbmNob3JFbDogdGhpcy5nZXRDb250ZW50QW5jaG9yRWwsXG4gICAgICAgICAgY2xhc3NlczogUG9wb3ZlckNsYXNzZXMsXG4gICAgICAgICAgb25FbnRlcjogdGhpcy5oYW5kbGVFbnRlcixcbiAgICAgICAgICBhbmNob3JPcmlnaW46IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyA/IHJ0bE9yaWdpbiA6IGx0ck9yaWdpbixcbiAgICAgICAgICB0cmFuc2Zvcm1PcmlnaW46IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyA/IHJ0bE9yaWdpbiA6IGx0ck9yaWdpbixcbiAgICAgICAgICBQYXBlclByb3BzOiAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBQYXBlclByb3BzLCB7XG4gICAgICAgICAgICBjbGFzc2VzOiAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBQYXBlclByb3BzLmNsYXNzZXMsIHtcbiAgICAgICAgICAgICAgcm9vdDogY2xhc3Nlcy5wYXBlclxuICAgICAgICAgICAgfSlcbiAgICAgICAgICB9KVxuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIF9NZW51TGlzdDIuZGVmYXVsdCxcbiAgICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICAgIHJvbGU6ICdtZW51JyxcbiAgICAgICAgICAgIG9uS2V5RG93bjogdGhpcy5oYW5kbGVMaXN0S2V5RG93blxuICAgICAgICAgIH0sIE1lbnVMaXN0UHJvcHMsIHtcbiAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgX3RoaXMyLm1lbnVMaXN0ID0gbm9kZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KSxcbiAgICAgICAgICBjaGlsZHJlblxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTWVudTtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbk1lbnUuZGVmYXVsdFByb3BzID0ge1xuICBvcGVuOiBmYWxzZSxcbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiAnYXV0bydcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyB3aXRoVGhlbWU6IHRydWUsIG5hbWU6ICdNdWlNZW51JyB9KShNZW51KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9NZW51L01lbnUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9MaXN0SXRlbSA9IHJlcXVpcmUoJy4uL0xpc3QvTGlzdEl0ZW0nKTtcblxudmFyIF9MaXN0SXRlbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaXN0SXRlbSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IExpc3RJdGVtXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDogKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgdGhlbWUudHlwb2dyYXBoeS5zdWJoZWFkaW5nLCB7XG4gICAgICBoZWlnaHQ6IDI0LFxuICAgICAgYm94U2l6aW5nOiAnY29udGVudC1ib3gnLFxuICAgICAgYmFja2dyb3VuZDogJ25vbmUnLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgdGV4dE92ZXJmbG93OiAnZWxsaXBzaXMnLFxuICAgICAgd2hpdGVTcGFjZTogJ25vd3JhcCcsXG4gICAgICAnJjpmb2N1cyc6IHtcbiAgICAgICAgYmFja2dyb3VuZDogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICAgIH0sXG4gICAgICAnJjpob3Zlcic6IHtcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlclxuICAgICAgfVxuICAgIH0pLFxuICAgIHNlbGVjdGVkOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogTWVudSBpdGVtIGNvbnRlbnRzLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICByb2xlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZSB0byBhcHBseSBzZWxlY3RlZCBzdHlsaW5nLlxuICAgKi9cbiAgc2VsZWN0ZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWRcbn07XG5cbnZhciBNZW51SXRlbSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKE1lbnVJdGVtLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBNZW51SXRlbSgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBNZW51SXRlbSk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKE1lbnVJdGVtLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShNZW51SXRlbSkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoTWVudUl0ZW0sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbXBvbmVudCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgc2VsZWN0ZWQgPSBfcHJvcHMuc2VsZWN0ZWQsXG4gICAgICAgICAgcm9sZSA9IF9wcm9wcy5yb2xlLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdzZWxlY3RlZCcsICdyb2xlJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuc2VsZWN0ZWQsIHNlbGVjdGVkKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfTGlzdEl0ZW0yLmRlZmF1bHQsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICBidXR0b246IHRydWUsXG4gICAgICAgIHJvbGU6IHJvbGUsXG4gICAgICAgIHRhYkluZGV4OiAtMSxcbiAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgIGNvbXBvbmVudDogY29tcG9uZW50XG4gICAgICB9LCBvdGhlcikpO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTWVudUl0ZW07XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5NZW51SXRlbS5kZWZhdWx0UHJvcHMgPSB7XG4gIHJvbGU6ICdtZW51aXRlbScsXG4gIHNlbGVjdGVkOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlNZW51SXRlbScgfSkoTWVudUl0ZW0pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudUl0ZW0uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudUl0ZW0uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMzQiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfdG9Db25zdW1hYmxlQXJyYXkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3RvQ29uc3VtYWJsZUFycmF5Jyk7XG5cbnZhciBfdG9Db25zdW1hYmxlQXJyYXkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfdG9Db25zdW1hYmxlQXJyYXkyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9yZWFjdERvbSA9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG52YXIgX2tleWNvZGUgPSByZXF1aXJlKCdrZXljb2RlJyk7XG5cbnZhciBfa2V5Y29kZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9rZXljb2RlKTtcblxudmFyIF9jb250YWlucyA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3F1ZXJ5L2NvbnRhaW5zJyk7XG5cbnZhciBfY29udGFpbnMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY29udGFpbnMpO1xuXG52YXIgX2FjdGl2ZUVsZW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9hY3RpdmVFbGVtZW50Jyk7XG5cbnZhciBfYWN0aXZlRWxlbWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9hY3RpdmVFbGVtZW50KTtcblxudmFyIF9vd25lckRvY3VtZW50ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudCcpO1xuXG52YXIgX293bmVyRG9jdW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3duZXJEb2N1bWVudCk7XG5cbnZhciBfTGlzdCA9IHJlcXVpcmUoJy4uL0xpc3QnKTtcblxudmFyIF9MaXN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBMaXN0XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIE1lbnVMaXN0IGNvbnRlbnRzLCBub3JtYWxseSBgTWVudUl0ZW1gcy5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25CbHVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25LZXlEb3duOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufTtcblxudmFyIE1lbnVMaXN0ID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTWVudUxpc3QsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIE1lbnVMaXN0KCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIE1lbnVMaXN0KTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IE1lbnVMaXN0Ll9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShNZW51TGlzdCkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0ge1xuICAgICAgY3VycmVudFRhYkluZGV4OiB1bmRlZmluZWRcbiAgICB9LCBfdGhpcy5saXN0ID0gdW5kZWZpbmVkLCBfdGhpcy5zZWxlY3RlZEl0ZW0gPSB1bmRlZmluZWQsIF90aGlzLmJsdXJUaW1lciA9IHVuZGVmaW5lZCwgX3RoaXMuaGFuZGxlQmx1ciA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgX3RoaXMuYmx1clRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChfdGhpcy5saXN0KSB7XG4gICAgICAgICAgdmFyIGxpc3QgPSAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKShfdGhpcy5saXN0KTtcbiAgICAgICAgICB2YXIgY3VycmVudEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKGxpc3QpKTtcbiAgICAgICAgICBpZiAoISgwLCBfY29udGFpbnMyLmRlZmF1bHQpKGxpc3QsIGN1cnJlbnRGb2N1cykpIHtcbiAgICAgICAgICAgIF90aGlzLnJlc2V0VGFiSW5kZXgoKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0sIDMwKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQmx1cikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkJsdXIoZXZlbnQpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUtleURvd24gPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciBsaXN0ID0gKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkoX3RoaXMubGlzdCk7XG4gICAgICB2YXIga2V5ID0gKDAsIF9rZXljb2RlMi5kZWZhdWx0KShldmVudCk7XG4gICAgICB2YXIgY3VycmVudEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKGxpc3QpKTtcblxuICAgICAgaWYgKChrZXkgPT09ICd1cCcgfHwga2V5ID09PSAnZG93bicpICYmICghY3VycmVudEZvY3VzIHx8IGN1cnJlbnRGb2N1cyAmJiAhKDAsIF9jb250YWluczIuZGVmYXVsdCkobGlzdCwgY3VycmVudEZvY3VzKSkpIHtcbiAgICAgICAgaWYgKF90aGlzLnNlbGVjdGVkSXRlbSkge1xuICAgICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgICAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKShfdGhpcy5zZWxlY3RlZEl0ZW0pLmZvY3VzKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICAgIGxpc3QuZmlyc3RDaGlsZC5mb2N1cygpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKGtleSA9PT0gJ2Rvd24nKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGlmIChjdXJyZW50Rm9jdXMubmV4dEVsZW1lbnRTaWJsaW5nKSB7XG4gICAgICAgICAgY3VycmVudEZvY3VzLm5leHRFbGVtZW50U2libGluZy5mb2N1cygpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKGtleSA9PT0gJ3VwJykge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICBpZiAoY3VycmVudEZvY3VzLnByZXZpb3VzRWxlbWVudFNpYmxpbmcpIHtcbiAgICAgICAgICBjdXJyZW50Rm9jdXMucHJldmlvdXNFbGVtZW50U2libGluZy5mb2N1cygpO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbktleURvd24pIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25LZXlEb3duKGV2ZW50LCBrZXkpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUl0ZW1Gb2N1cyA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIGxpc3QgPSAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKShfdGhpcy5saXN0KTtcbiAgICAgIGlmIChsaXN0KSB7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBsaXN0LmNoaWxkcmVuLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICAgIGlmIChsaXN0LmNoaWxkcmVuW2ldID09PSBldmVudC5jdXJyZW50VGFyZ2V0KSB7XG4gICAgICAgICAgICBfdGhpcy5zZXRUYWJJbmRleChpKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShNZW51TGlzdCwgW3tcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdGhpcy5yZXNldFRhYkluZGV4KCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aGlzLmJsdXJUaW1lcik7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnc2V0VGFiSW5kZXgnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBzZXRUYWJJbmRleChpbmRleCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGN1cnJlbnRUYWJJbmRleDogaW5kZXggfSk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICAgIHZhciBjdXJyZW50VGFiSW5kZXggPSB0aGlzLnN0YXRlLmN1cnJlbnRUYWJJbmRleDtcblxuICAgICAgdmFyIGxpc3QgPSAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKSh0aGlzLmxpc3QpO1xuICAgICAgaWYgKCFsaXN0IHx8ICFsaXN0LmNoaWxkcmVuIHx8ICFsaXN0LmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAoY3VycmVudFRhYkluZGV4ICYmIGN1cnJlbnRUYWJJbmRleCA+PSAwKSB7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgbGlzdC5jaGlsZHJlbltjdXJyZW50VGFiSW5kZXhdLmZvY3VzKCk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICAgIGxpc3QuZmlyc3RDaGlsZC5mb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3Jlc2V0VGFiSW5kZXgnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXNldFRhYkluZGV4KCkge1xuICAgICAgdmFyIGxpc3QgPSAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKSh0aGlzLmxpc3QpO1xuICAgICAgdmFyIGN1cnJlbnRGb2N1cyA9ICgwLCBfYWN0aXZlRWxlbWVudDIuZGVmYXVsdCkoKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShsaXN0KSk7XG4gICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICB2YXIgaXRlbXMgPSBbXS5jb25jYXQoKDAsIF90b0NvbnN1bWFibGVBcnJheTMuZGVmYXVsdCkobGlzdC5jaGlsZHJlbikpO1xuICAgICAgdmFyIGN1cnJlbnRGb2N1c0luZGV4ID0gaXRlbXMuaW5kZXhPZihjdXJyZW50Rm9jdXMpO1xuXG4gICAgICBpZiAoY3VycmVudEZvY3VzSW5kZXggIT09IC0xKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNldFRhYkluZGV4KGN1cnJlbnRGb2N1c0luZGV4KTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRJdGVtKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNldFRhYkluZGV4KGl0ZW1zLmluZGV4T2YoKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkodGhpcy5zZWxlY3RlZEl0ZW0pKSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB0aGlzLnNldFRhYkluZGV4KDApO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIG9uQmx1ciA9IF9wcm9wcy5vbkJsdXIsXG4gICAgICAgICAgb25LZXlEb3duID0gX3Byb3BzLm9uS2V5RG93bixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc05hbWUnLCAnb25CbHVyJywgJ29uS2V5RG93biddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9MaXN0Mi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICByb2xlOiAnbWVudScsXG4gICAgICAgICAgcm9vdFJlZjogZnVuY3Rpb24gcm9vdFJlZihub2RlKSB7XG4gICAgICAgICAgICBfdGhpczIubGlzdCA9IG5vZGU7XG4gICAgICAgICAgfSxcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBvbktleURvd246IHRoaXMuaGFuZGxlS2V5RG93bixcbiAgICAgICAgICBvbkJsdXI6IHRoaXMuaGFuZGxlQmx1clxuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCwgaW5kZXgpIHtcbiAgICAgICAgICBpZiAoIV9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChjaGlsZCkpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgICAgICB0YWJJbmRleDogaW5kZXggPT09IF90aGlzMi5zdGF0ZS5jdXJyZW50VGFiSW5kZXggPyAwIDogLTEsXG4gICAgICAgICAgICByZWY6IGNoaWxkLnByb3BzLnNlbGVjdGVkID8gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgICAgICAgICAgX3RoaXMyLnNlbGVjdGVkSXRlbSA9IG5vZGU7XG4gICAgICAgICAgICB9IDogdW5kZWZpbmVkLFxuICAgICAgICAgICAgb25Gb2N1czogX3RoaXMyLmhhbmRsZUl0ZW1Gb2N1c1xuICAgICAgICAgIH0pO1xuICAgICAgICB9KVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIE1lbnVMaXN0O1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTWVudUxpc3QucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8ge1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG4gIG9uQmx1cjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG4gIG9uS2V5RG93bjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmNcbn0gOiB7fTtcbmV4cG9ydHMuZGVmYXVsdCA9IE1lbnVMaXN0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudUxpc3QuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudUxpc3QuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9NZW51ID0gcmVxdWlyZSgnLi9NZW51Jyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX01lbnUpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX01lbnVMaXN0ID0gcmVxdWlyZSgnLi9NZW51TGlzdCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ01lbnVMaXN0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTWVudUxpc3QpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX01lbnVJdGVtID0gcmVxdWlyZSgnLi9NZW51SXRlbScpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ01lbnVJdGVtJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTWVudUl0ZW0pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMzQiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB6SW5kZXg6IC0xLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJzEwMCUnLFxuICAgICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAwLFxuICAgICAgLy8gUmVtb3ZlIGdyZXkgaGlnaGxpZ2h0XG4gICAgICBXZWJraXRUYXBIaWdobGlnaHRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb21tb24udHJhbnNwYXJlbnQsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuY29tbW9uLmxpZ2h0QmxhY2ssXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknKSxcbiAgICAgIHdpbGxDaGFuZ2U6ICdvcGFjaXR5JyxcbiAgICAgIG9wYWNpdHk6IDBcbiAgICB9LFxuICAgIGludmlzaWJsZToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLmNvbW1vbi50cmFuc3BhcmVudFxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIENhbiBiZSB1c2VkLCBmb3IgaW5zdGFuY2UsIHRvIHJlbmRlciBhIGxldHRlciBpbnNpZGUgdGhlIGF2YXRhci5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYmFja2Ryb3AgaXMgaW52aXNpYmxlLlxuICAgKi9cbiAgaW52aXNpYmxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkXG59O1xuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBCYWNrZHJvcCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEJhY2tkcm9wLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBCYWNrZHJvcCgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBCYWNrZHJvcCk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEJhY2tkcm9wLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShCYWNrZHJvcCkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQmFja2Ryb3AsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgaW52aXNpYmxlID0gX3Byb3BzLmludmlzaWJsZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdpbnZpc2libGUnXSk7XG5cblxuICAgICAgdmFyIGJhY2tkcm9wQ2xhc3MgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuaW52aXNpYmxlLCBpbnZpc2libGUpLCBjbGFzc05hbWUpO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBiYWNrZHJvcENsYXNzLCAnYXJpYS1oaWRkZW4nOiAndHJ1ZScgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEJhY2tkcm9wO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQmFja2Ryb3AuZGVmYXVsdFByb3BzID0ge1xuICBpbnZpc2libGU6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUJhY2tkcm9wJyB9KShCYWNrZHJvcCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvQmFja2Ryb3AuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL0JhY2tkcm9wLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfa2V5cyA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3Qva2V5cycpO1xuXG52YXIgX2tleXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfa2V5cyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9yZWFjdERvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdERvbSk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dhcm5pbmcgPSByZXF1aXJlKCd3YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxudmFyIF9rZXljb2RlID0gcmVxdWlyZSgna2V5Y29kZScpO1xuXG52YXIgX2tleWNvZGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfa2V5Y29kZSk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG52YXIgX2NvbnRhaW5zID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvcXVlcnkvY29udGFpbnMnKTtcblxudmFyIF9jb250YWluczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb250YWlucyk7XG5cbnZhciBfYWN0aXZlRWxlbWVudCA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQnKTtcblxudmFyIF9hY3RpdmVFbGVtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2FjdGl2ZUVsZW1lbnQpO1xuXG52YXIgX293bmVyRG9jdW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxudmFyIF9hZGRFdmVudExpc3RlbmVyID0gcmVxdWlyZSgnLi4vdXRpbHMvYWRkRXZlbnRMaXN0ZW5lcicpO1xuXG52YXIgX2FkZEV2ZW50TGlzdGVuZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfYWRkRXZlbnRMaXN0ZW5lcik7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9GYWRlID0gcmVxdWlyZSgnLi4vdHJhbnNpdGlvbnMvRmFkZScpO1xuXG52YXIgX0ZhZGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRmFkZSk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9tb2RhbE1hbmFnZXIgPSByZXF1aXJlKCcuL21vZGFsTWFuYWdlcicpO1xuXG52YXIgX21vZGFsTWFuYWdlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9tb2RhbE1hbmFnZXIpO1xuXG52YXIgX0JhY2tkcm9wID0gcmVxdWlyZSgnLi9CYWNrZHJvcCcpO1xuXG52YXIgX0JhY2tkcm9wMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0JhY2tkcm9wKTtcblxudmFyIF9Qb3J0YWwgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC9Qb3J0YWwnKTtcblxudmFyIF9Qb3J0YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUG9ydGFsKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG4vLyBNb2RhbHMgZG9uJ3Qgb3BlbiBvbiB0aGUgc2VydmVyIHNvIHRoaXMgd29uJ3QgYnJlYWsgY29uY3VycmVuY3kuXG4vLyBDb3VsZCBhbHNvIHB1dCB0aGlzIG9uIGNvbnRleHQuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgbW9kYWxNYW5hZ2VyID0gKDAsIF9tb2RhbE1hbmFnZXIyLmRlZmF1bHQpKCk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgICAgekluZGV4OiB0aGVtZS56SW5kZXguZGlhbG9nLFxuICAgICAgdG9wOiAwLFxuICAgICAgbGVmdDogMFxuICAgIH0sXG4gICAgaGlkZGVuOiB7XG4gICAgICB2aXNpYmlsaXR5OiAnaGlkZGVuJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBDU1MgY2xhc3MgbmFtZSBvZiB0aGUgYmFja2Ryb3AgZWxlbWVudC5cbiAgICovXG4gIEJhY2tkcm9wQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBQYXNzIGEgY29tcG9uZW50IGNsYXNzIHRvIHVzZSBhcyB0aGUgYmFja2Ryb3AuXG4gICAqL1xuICBCYWNrZHJvcENvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYmFja2Ryb3AgaXMgaW52aXNpYmxlLlxuICAgKi9cbiAgQmFja2Ryb3BJbnZpc2libGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIGJhY2tkcm9wIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb246IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbikuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQSBzaW5nbGUgY2hpbGQgY29udGVudCBlbGVtZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBBbHdheXMga2VlcCB0aGUgY2hpbGRyZW4gaW4gdGhlIERPTS5cbiAgICogVGhpcyBwcm9wZXJ0eSBjYW4gYmUgdXNlZnVsIGluIFNFTyBzaXR1YXRpb24gb3JcbiAgICogd2hlbiB5b3Ugd2FudCB0byBtYXhpbWl6ZSB0aGUgcmVzcG9uc2l2ZW5lc3Mgb2YgdGhlIE1vZGFsLlxuICAgKi9cbiAga2VlcE1vdW50ZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJhY2tkcm9wIGlzIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZUJhY2tkcm9wOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGNsaWNraW5nIHRoZSBiYWNrZHJvcCB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlQmFja2Ryb3BDbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBoaXR0aW5nIGVzY2FwZSB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG1vZGFsTWFuYWdlcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlcyB3aGVuIHRoZSBiYWNrZHJvcCBpcyBjbGlja2VkIG9uLlxuICAgKi9cbiAgb25CYWNrZHJvcENsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBtb2RhbCBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGhhcyBlbnRlcmVkLlxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVzIHdoZW4gdGhlIGVzY2FwZSBrZXkgaXMgcHJlc3NlZCBhbmQgdGhlIG1vZGFsIGlzIGluIGZvY3VzLlxuICAgKi9cbiAgb25Fc2NhcGVLZXlVcDogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIGJlZm9yZSB0aGUgbW9kYWwgaXMgZXhpdGluZy5cbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBtb2RhbCBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0aW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGhhcyBleGl0ZWQuXG4gICAqL1xuICBvbkV4aXRlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBjb21wb25lbnQgcmVxdWVzdHMgdG8gYmUgY2xvc2VkLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2tcbiAgICovXG4gIG9uUmVxdWVzdENsb3NlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgTW9kYWwgaXMgdmlzaWJsZS5cbiAgICovXG4gIHNob3c6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogVGhlIG1vZGFsIGNvbXBvbmVudCBwcm92aWRlcyBhIHNvbGlkIGZvdW5kYXRpb24gZm9yIGNyZWF0aW5nIGRpYWxvZ3MsXG4gKiBwb3BvdmVycywgb3Igd2hhdGV2ZXIgZWxzZS5cbiAqIFRoZSBjb21wb25lbnQgcmVuZGVycyBpdHMgYGNoaWxkcmVuYCBub2RlIGluIGZyb250IG9mIGEgYmFja2Ryb3AgY29tcG9uZW50LlxuICpcbiAqIFRoZSBgTW9kYWxgIG9mZmVycyBhIGZldyBoZWxwZnVsIGZlYXR1cmVzIG92ZXIgdXNpbmcganVzdCBhIGBQb3J0YWxgIGNvbXBvbmVudCBhbmQgc29tZSBzdHlsZXM6XG4gKiAtIE1hbmFnZXMgZGlhbG9nIHN0YWNraW5nIHdoZW4gb25lLWF0LWEtdGltZSBqdXN0IGlzbid0IGVub3VnaC5cbiAqIC0gQ3JlYXRlcyBhIGJhY2tkcm9wLCBmb3IgZGlzYWJsaW5nIGludGVyYWN0aW9uIGJlbG93IHRoZSBtb2RhbC5cbiAqIC0gSXQgcHJvcGVybHkgbWFuYWdlcyBmb2N1czsgbW92aW5nIHRvIHRoZSBtb2RhbCBjb250ZW50LFxuICogICBhbmQga2VlcGluZyBpdCB0aGVyZSB1bnRpbCB0aGUgbW9kYWwgaXMgY2xvc2VkLlxuICogLSBJdCBkaXNhYmxlcyBzY3JvbGxpbmcgb2YgdGhlIHBhZ2UgY29udGVudCB3aGlsZSBvcGVuLlxuICogLSBBZGRzIHRoZSBhcHByb3ByaWF0ZSBBUklBIHJvbGVzIGFyZSBhdXRvbWF0aWNhbGx5LlxuICpcbiAqIFRoaXMgY29tcG9uZW50IHNoYXJlcyBtYW55IGNvbmNlcHRzIHdpdGggW3JlYWN0LW92ZXJsYXlzXShodHRwczovL3JlYWN0LWJvb3RzdHJhcC5naXRodWIuaW8vcmVhY3Qtb3ZlcmxheXMvI21vZGFscykuXG4gKi9cbnZhciBNb2RhbCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKE1vZGFsLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBNb2RhbCgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBNb2RhbCk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBNb2RhbC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoTW9kYWwpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfaW5pdGlhbGlzZVByb3BzLmNhbGwoX3RoaXMpLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoTW9kYWwsIFt7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgICAgaWYgKCF0aGlzLnByb3BzLnNob3cpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGV4aXRlZDogdHJ1ZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdGhpcy5tb3VudGVkID0gdHJ1ZTtcbiAgICAgIGlmICh0aGlzLnByb3BzLnNob3cpIHtcbiAgICAgICAgdGhpcy5oYW5kbGVTaG93KCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzKSB7XG4gICAgICBpZiAobmV4dFByb3BzLnNob3cgJiYgdGhpcy5zdGF0ZS5leGl0ZWQpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGV4aXRlZDogZmFsc2UgfSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgICBpZiAoIXRoaXMucHJvcHMuc2hvdyAmJiBuZXh0UHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLmNoZWNrRm9yRm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzKSB7XG4gICAgICBpZiAoIXByZXZQcm9wcy5zaG93ICYmIHRoaXMucHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLmhhbmRsZVNob3coKTtcbiAgICAgIH1cbiAgICAgIC8vIFdlIGFyZSB3YWl0aW5nIGZvciB0aGUgb25FeGl0ZWQgY2FsbGJhY2sgdG8gY2FsbCBoYW5kbGVIaWRlLlxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICBpZiAodGhpcy5wcm9wcy5zaG93IHx8ICF0aGlzLnN0YXRlLmV4aXRlZCkge1xuICAgICAgICB0aGlzLmhhbmRsZUhpZGUoKTtcbiAgICAgIH1cbiAgICAgIHRoaXMubW91bnRlZCA9IGZhbHNlO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NoZWNrRm9yRm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjaGVja0ZvckZvY3VzKCkge1xuICAgICAgaWYgKF9pbkRPTTIuZGVmYXVsdCkge1xuICAgICAgICB0aGlzLmxhc3RGb2N1cyA9ICgwLCBfYWN0aXZlRWxlbWVudDIuZGVmYXVsdCkoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZXN0b3JlTGFzdEZvY3VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVzdG9yZUxhc3RGb2N1cygpIHtcbiAgICAgIGlmICh0aGlzLmxhc3RGb2N1cyAmJiB0aGlzLmxhc3RGb2N1cy5mb2N1cykge1xuICAgICAgICB0aGlzLmxhc3RGb2N1cy5mb2N1cygpO1xuICAgICAgICB0aGlzLmxhc3RGb2N1cyA9IHVuZGVmaW5lZDtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdoYW5kbGVTaG93JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaGFuZGxlU2hvdygpIHtcbiAgICAgIHZhciBkb2MgPSAoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKF9yZWFjdERvbTIuZGVmYXVsdC5maW5kRE9NTm9kZSh0aGlzKSk7XG4gICAgICB0aGlzLnByb3BzLm1vZGFsTWFuYWdlci5hZGQodGhpcyk7XG4gICAgICB0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyID0gKDAsIF9hZGRFdmVudExpc3RlbmVyMi5kZWZhdWx0KShkb2MsICdrZXl1cCcsIHRoaXMuaGFuZGxlRG9jdW1lbnRLZXlVcCk7XG4gICAgICB0aGlzLm9uRm9jdXNMaXN0ZW5lciA9ICgwLCBfYWRkRXZlbnRMaXN0ZW5lcjIuZGVmYXVsdCkoZG9jLCAnZm9jdXMnLCB0aGlzLmhhbmRsZUZvY3VzTGlzdGVuZXIsIHRydWUpO1xuICAgICAgdGhpcy5mb2N1cygpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2ZvY3VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZm9jdXMoKSB7XG4gICAgICB2YXIgY3VycmVudEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKF9yZWFjdERvbTIuZGVmYXVsdC5maW5kRE9NTm9kZSh0aGlzKSkpO1xuICAgICAgdmFyIG1vZGFsQ29udGVudCA9IHRoaXMubW9kYWwgJiYgdGhpcy5tb2RhbC5sYXN0Q2hpbGQ7XG4gICAgICB2YXIgZm9jdXNJbk1vZGFsID0gY3VycmVudEZvY3VzICYmICgwLCBfY29udGFpbnMyLmRlZmF1bHQpKG1vZGFsQ29udGVudCwgY3VycmVudEZvY3VzKTtcblxuICAgICAgaWYgKG1vZGFsQ29udGVudCAmJiAhZm9jdXNJbk1vZGFsKSB7XG4gICAgICAgIGlmICghbW9kYWxDb250ZW50Lmhhc0F0dHJpYnV0ZSgndGFiSW5kZXgnKSkge1xuICAgICAgICAgIG1vZGFsQ29udGVudC5zZXRBdHRyaWJ1dGUoJ3RhYkluZGV4JywgLTEpO1xuICAgICAgICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkoZmFsc2UsICdNYXRlcmlhbC1VSTogdGhlIG1vZGFsIGNvbnRlbnQgbm9kZSBkb2VzIG5vdCBhY2NlcHQgZm9jdXMuICcgKyAnRm9yIHRoZSBiZW5lZml0IG9mIGFzc2lzdGl2ZSB0ZWNobm9sb2dpZXMsICcgKyAndGhlIHRhYkluZGV4IG9mIHRoZSBub2RlIGlzIGJlaW5nIHNldCB0byBcIi0xXCIuJykgOiB2b2lkIDA7XG4gICAgICAgIH1cblxuICAgICAgICBtb2RhbENvbnRlbnQuZm9jdXMoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdoYW5kbGVIaWRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gaGFuZGxlSGlkZSgpIHtcbiAgICAgIHRoaXMucHJvcHMubW9kYWxNYW5hZ2VyLnJlbW92ZSh0aGlzKTtcbiAgICAgIGlmICh0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyKSB0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyLnJlbW92ZSgpO1xuICAgICAgaWYgKHRoaXMub25Gb2N1c0xpc3RlbmVyKSB0aGlzLm9uRm9jdXNMaXN0ZW5lci5yZW1vdmUoKTtcbiAgICAgIHRoaXMucmVzdG9yZUxhc3RGb2N1cygpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlckJhY2tkcm9wJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyQmFja2Ryb3AoKSB7XG4gICAgICB2YXIgb3RoZXIgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9O1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgQmFja2Ryb3BDb21wb25lbnQgPSBfcHJvcHMuQmFja2Ryb3BDb21wb25lbnQsXG4gICAgICAgICAgQmFja2Ryb3BDbGFzc05hbWUgPSBfcHJvcHMuQmFja2Ryb3BDbGFzc05hbWUsXG4gICAgICAgICAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gPSBfcHJvcHMuQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgQmFja2Ryb3BJbnZpc2libGUgPSBfcHJvcHMuQmFja2Ryb3BJbnZpc2libGUsXG4gICAgICAgICAgc2hvdyA9IF9wcm9wcy5zaG93O1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0ZhZGUyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBhcHBlYXI6IHRydWUsICdpbic6IHNob3csIHRpbWVvdXQ6IEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uIH0sIG90aGVyKSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQmFja2Ryb3BDb21wb25lbnQsIHtcbiAgICAgICAgICBpbnZpc2libGU6IEJhY2tkcm9wSW52aXNpYmxlLFxuICAgICAgICAgIGNsYXNzTmFtZTogQmFja2Ryb3BDbGFzc05hbWUsXG4gICAgICAgICAgb25DbGljazogdGhpcy5oYW5kbGVCYWNrZHJvcENsaWNrXG4gICAgICAgIH0pXG4gICAgICApO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgZGlzYWJsZUJhY2tkcm9wID0gX3Byb3BzMi5kaXNhYmxlQmFja2Ryb3AsXG4gICAgICAgICAgQmFja2Ryb3BDb21wb25lbnQgPSBfcHJvcHMyLkJhY2tkcm9wQ29tcG9uZW50LFxuICAgICAgICAgIEJhY2tkcm9wQ2xhc3NOYW1lID0gX3Byb3BzMi5CYWNrZHJvcENsYXNzTmFtZSxcbiAgICAgICAgICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiA9IF9wcm9wczIuQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgQmFja2Ryb3BJbnZpc2libGUgPSBfcHJvcHMyLkJhY2tkcm9wSW52aXNpYmxlLFxuICAgICAgICAgIGlnbm9yZUJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMyLmlnbm9yZUJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgaWdub3JlRXNjYXBlS2V5VXAgPSBfcHJvcHMyLmlnbm9yZUVzY2FwZUtleVVwLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzMi5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzMi5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wczIuY2xhc3NOYW1lLFxuICAgICAgICAgIGtlZXBNb3VudGVkID0gX3Byb3BzMi5rZWVwTW91bnRlZCxcbiAgICAgICAgICBtb2RhbE1hbmFnZXJQcm9wID0gX3Byb3BzMi5tb2RhbE1hbmFnZXIsXG4gICAgICAgICAgb25CYWNrZHJvcENsaWNrID0gX3Byb3BzMi5vbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgb25Fc2NhcGVLZXlVcCA9IF9wcm9wczIub25Fc2NhcGVLZXlVcCxcbiAgICAgICAgICBvblJlcXVlc3RDbG9zZSA9IF9wcm9wczIub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wczIub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzMi5vbkVudGVyaW5nLFxuICAgICAgICAgIG9uRW50ZXJlZCA9IF9wcm9wczIub25FbnRlcmVkLFxuICAgICAgICAgIG9uRXhpdCA9IF9wcm9wczIub25FeGl0LFxuICAgICAgICAgIG9uRXhpdGluZyA9IF9wcm9wczIub25FeGl0aW5nLFxuICAgICAgICAgIG9uRXhpdGVkID0gX3Byb3BzMi5vbkV4aXRlZCxcbiAgICAgICAgICBzaG93ID0gX3Byb3BzMi5zaG93LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzMiwgWydkaXNhYmxlQmFja2Ryb3AnLCAnQmFja2Ryb3BDb21wb25lbnQnLCAnQmFja2Ryb3BDbGFzc05hbWUnLCAnQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24nLCAnQmFja2Ryb3BJbnZpc2libGUnLCAnaWdub3JlQmFja2Ryb3BDbGljaycsICdpZ25vcmVFc2NhcGVLZXlVcCcsICdjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdrZWVwTW91bnRlZCcsICdtb2RhbE1hbmFnZXInLCAnb25CYWNrZHJvcENsaWNrJywgJ29uRXNjYXBlS2V5VXAnLCAnb25SZXF1ZXN0Q2xvc2UnLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRW50ZXJlZCcsICdvbkV4aXQnLCAnb25FeGl0aW5nJywgJ29uRXhpdGVkJywgJ3Nob3cnXSk7XG5cblxuICAgICAgaWYgKCFrZWVwTW91bnRlZCAmJiAhc2hvdyAmJiB0aGlzLnN0YXRlLmV4aXRlZCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgIH1cblxuICAgICAgdmFyIHRyYW5zaXRpb25DYWxsYmFja3MgPSB7XG4gICAgICAgIG9uRW50ZXI6IG9uRW50ZXIsXG4gICAgICAgIG9uRW50ZXJpbmc6IG9uRW50ZXJpbmcsXG4gICAgICAgIG9uRW50ZXJlZDogb25FbnRlcmVkLFxuICAgICAgICBvbkV4aXQ6IG9uRXhpdCxcbiAgICAgICAgb25FeGl0aW5nOiBvbkV4aXRpbmcsXG4gICAgICAgIG9uRXhpdGVkOiB0aGlzLmhhbmRsZVRyYW5zaXRpb25FeGl0ZWRcbiAgICAgIH07XG5cbiAgICAgIHZhciBtb2RhbENoaWxkID0gX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm9ubHkoY2hpbGRyZW4pO1xuICAgICAgdmFyIF9tb2RhbENoaWxkJHByb3BzID0gbW9kYWxDaGlsZC5wcm9wcyxcbiAgICAgICAgICByb2xlID0gX21vZGFsQ2hpbGQkcHJvcHMucm9sZSxcbiAgICAgICAgICB0YWJJbmRleCA9IF9tb2RhbENoaWxkJHByb3BzLnRhYkluZGV4O1xuXG4gICAgICB2YXIgY2hpbGRQcm9wcyA9IHt9O1xuXG4gICAgICBpZiAocm9sZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGNoaWxkUHJvcHMucm9sZSA9IHJvbGUgPT09IHVuZGVmaW5lZCA/ICdkb2N1bWVudCcgOiByb2xlO1xuICAgICAgfVxuXG4gICAgICBpZiAodGFiSW5kZXggPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjaGlsZFByb3BzLnRhYkluZGV4ID0gdGFiSW5kZXggPT0gbnVsbCA/IC0xIDogdGFiSW5kZXg7XG4gICAgICB9XG5cbiAgICAgIHZhciBiYWNrZHJvcFByb3BzID0gdm9pZCAwO1xuXG4gICAgICAvLyBJdCdzIGEgVHJhbnNpdGlvbiBsaWtlIGNvbXBvbmVudFxuICAgICAgaWYgKG1vZGFsQ2hpbGQucHJvcHMuaGFzT3duUHJvcGVydHkoJ2luJykpIHtcbiAgICAgICAgKDAsIF9rZXlzMi5kZWZhdWx0KSh0cmFuc2l0aW9uQ2FsbGJhY2tzKS5mb3JFYWNoKGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICBjaGlsZFByb3BzW2tleV0gPSAoMCwgX2hlbHBlcnMuY3JlYXRlQ2hhaW5lZEZ1bmN0aW9uKSh0cmFuc2l0aW9uQ2FsbGJhY2tzW2tleV0sIG1vZGFsQ2hpbGQucHJvcHNba2V5XSk7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYmFja2Ryb3BQcm9wcyA9IHRyYW5zaXRpb25DYWxsYmFja3M7XG4gICAgICB9XG5cbiAgICAgIGlmICgoMCwgX2tleXMyLmRlZmF1bHQpKGNoaWxkUHJvcHMpLmxlbmd0aCkge1xuICAgICAgICBtb2RhbENoaWxkID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChtb2RhbENoaWxkLCBjaGlsZFByb3BzKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfUG9ydGFsMi5kZWZhdWx0LFxuICAgICAgICB7XG4gICAgICAgICAgb3BlbjogdHJ1ZSxcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICBfdGhpczIubW91bnROb2RlID0gbm9kZSA/IG5vZGUuZ2V0TGF5ZXIoKSA6IG51bGw7XG4gICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuaGlkZGVuLCB0aGlzLnN0YXRlLmV4aXRlZCkpXG4gICAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgX3RoaXMyLm1vZGFsID0gbm9kZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9KSxcbiAgICAgICAgICAhZGlzYWJsZUJhY2tkcm9wICYmICgha2VlcE1vdW50ZWQgfHwgc2hvdyB8fCAhdGhpcy5zdGF0ZS5leGl0ZWQpICYmIHRoaXMucmVuZGVyQmFja2Ryb3AoYmFja2Ryb3BQcm9wcyksXG4gICAgICAgICAgbW9kYWxDaGlsZFxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTW9kYWw7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Nb2RhbC5kZWZhdWx0UHJvcHMgPSB7XG4gIEJhY2tkcm9wQ29tcG9uZW50OiBfQmFja2Ryb3AyLmRlZmF1bHQsXG4gIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uOiAzMDAsXG4gIEJhY2tkcm9wSW52aXNpYmxlOiBmYWxzZSxcbiAga2VlcE1vdW50ZWQ6IGZhbHNlLFxuICBkaXNhYmxlQmFja2Ryb3A6IGZhbHNlLFxuICBpZ25vcmVCYWNrZHJvcENsaWNrOiBmYWxzZSxcbiAgaWdub3JlRXNjYXBlS2V5VXA6IGZhbHNlLFxuICBtb2RhbE1hbmFnZXI6IG1vZGFsTWFuYWdlclxufTtcblxudmFyIF9pbml0aWFsaXNlUHJvcHMgPSBmdW5jdGlvbiBfaW5pdGlhbGlzZVByb3BzKCkge1xuICB2YXIgX3RoaXMzID0gdGhpcztcblxuICB0aGlzLnN0YXRlID0ge1xuICAgIGV4aXRlZDogZmFsc2VcbiAgfTtcbiAgdGhpcy5vbkRvY3VtZW50S2V5VXBMaXN0ZW5lciA9IG51bGw7XG4gIHRoaXMub25Gb2N1c0xpc3RlbmVyID0gbnVsbDtcbiAgdGhpcy5tb3VudGVkID0gZmFsc2U7XG4gIHRoaXMubGFzdEZvY3VzID0gdW5kZWZpbmVkO1xuICB0aGlzLm1vZGFsID0gbnVsbDtcbiAgdGhpcy5tb3VudE5vZGUgPSBudWxsO1xuXG4gIHRoaXMuaGFuZGxlRm9jdXNMaXN0ZW5lciA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoIV90aGlzMy5tb3VudGVkIHx8ICFfdGhpczMucHJvcHMubW9kYWxNYW5hZ2VyLmlzVG9wTW9kYWwoX3RoaXMzKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBjdXJyZW50Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKF90aGlzMykpKTtcbiAgICB2YXIgbW9kYWxDb250ZW50ID0gX3RoaXMzLm1vZGFsICYmIF90aGlzMy5tb2RhbC5sYXN0Q2hpbGQ7XG5cbiAgICBpZiAobW9kYWxDb250ZW50ICYmIG1vZGFsQ29udGVudCAhPT0gY3VycmVudEZvY3VzICYmICEoMCwgX2NvbnRhaW5zMi5kZWZhdWx0KShtb2RhbENvbnRlbnQsIGN1cnJlbnRGb2N1cykpIHtcbiAgICAgIG1vZGFsQ29udGVudC5mb2N1cygpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLmhhbmRsZURvY3VtZW50S2V5VXAgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBpZiAoIV90aGlzMy5tb3VudGVkIHx8ICFfdGhpczMucHJvcHMubW9kYWxNYW5hZ2VyLmlzVG9wTW9kYWwoX3RoaXMzKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmICgoMCwgX2tleWNvZGUyLmRlZmF1bHQpKGV2ZW50KSAhPT0gJ2VzYycpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgX3Byb3BzMyA9IF90aGlzMy5wcm9wcyxcbiAgICAgICAgb25Fc2NhcGVLZXlVcCA9IF9wcm9wczMub25Fc2NhcGVLZXlVcCxcbiAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHMzLm9uUmVxdWVzdENsb3NlLFxuICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcCA9IF9wcm9wczMuaWdub3JlRXNjYXBlS2V5VXA7XG5cblxuICAgIGlmIChvbkVzY2FwZUtleVVwKSB7XG4gICAgICBvbkVzY2FwZUtleVVwKGV2ZW50KTtcbiAgICB9XG5cbiAgICBpZiAob25SZXF1ZXN0Q2xvc2UgJiYgIWlnbm9yZUVzY2FwZUtleVVwKSB7XG4gICAgICBvblJlcXVlc3RDbG9zZShldmVudCk7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMuaGFuZGxlQmFja2Ryb3BDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIGlmIChldmVudC50YXJnZXQgIT09IGV2ZW50LmN1cnJlbnRUYXJnZXQpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgX3Byb3BzNCA9IF90aGlzMy5wcm9wcyxcbiAgICAgICAgb25CYWNrZHJvcENsaWNrID0gX3Byb3BzNC5vbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgIG9uUmVxdWVzdENsb3NlID0gX3Byb3BzNC5vblJlcXVlc3RDbG9zZSxcbiAgICAgICAgaWdub3JlQmFja2Ryb3BDbGljayA9IF9wcm9wczQuaWdub3JlQmFja2Ryb3BDbGljaztcblxuXG4gICAgaWYgKG9uQmFja2Ryb3BDbGljaykge1xuICAgICAgb25CYWNrZHJvcENsaWNrKGV2ZW50KTtcbiAgICB9XG5cbiAgICBpZiAob25SZXF1ZXN0Q2xvc2UgJiYgIWlnbm9yZUJhY2tkcm9wQ2xpY2spIHtcbiAgICAgIG9uUmVxdWVzdENsb3NlKGV2ZW50KTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5oYW5kbGVUcmFuc2l0aW9uRXhpdGVkID0gZnVuY3Rpb24gKCkge1xuICAgIGlmIChfdGhpczMucHJvcHMub25FeGl0ZWQpIHtcbiAgICAgIHZhciBfcHJvcHM1O1xuXG4gICAgICAoX3Byb3BzNSA9IF90aGlzMy5wcm9wcykub25FeGl0ZWQuYXBwbHkoX3Byb3BzNSwgYXJndW1lbnRzKTtcbiAgICB9XG5cbiAgICBfdGhpczMuc2V0U3RhdGUoeyBleGl0ZWQ6IHRydWUgfSk7XG4gICAgX3RoaXMzLmhhbmRsZUhpZGUoKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IGZsaXA6IGZhbHNlLCBuYW1lOiAnTXVpTW9kYWwnIH0pKE1vZGFsKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9Nb2RhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvTW9kYWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX01vZGFsID0gcmVxdWlyZSgnLi9Nb2RhbCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Nb2RhbCkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfaXNXaW5kb3cgPSByZXF1aXJlKCdkb20taGVscGVycy9xdWVyeS9pc1dpbmRvdycpO1xuXG52YXIgX2lzV2luZG93MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzV2luZG93KTtcblxudmFyIF9vd25lckRvY3VtZW50ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudCcpO1xuXG52YXIgX293bmVyRG9jdW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3duZXJEb2N1bWVudCk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG52YXIgX3Njcm9sbGJhclNpemUgPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL3Njcm9sbGJhclNpemUnKTtcblxudmFyIF9zY3JvbGxiYXJTaXplMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Njcm9sbGJhclNpemUpO1xuXG52YXIgX21hbmFnZUFyaWFIaWRkZW4gPSByZXF1aXJlKCcuLi91dGlscy9tYW5hZ2VBcmlhSGlkZGVuJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8vIFRha2VuIGZyb20gaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWJvb3RzdHJhcC9yZWFjdC1vdmVybGF5cy9ibG9iL21hc3Rlci9zcmMvTW9kYWxNYW5hZ2VyLmpzXG5cbmZ1bmN0aW9uIGdldFBhZGRpbmdSaWdodChub2RlKSB7XG4gIHJldHVybiBwYXJzZUludChub2RlLnN0eWxlLnBhZGRpbmdSaWdodCB8fCAwLCAxMCk7XG59XG5cbi8vIERvIHdlIGhhdmUgYSBzY3JvbGwgYmFyP1xuZnVuY3Rpb24gYm9keUlzT3ZlcmZsb3dpbmcobm9kZSkge1xuICB2YXIgZG9jID0gKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShub2RlKTtcbiAgdmFyIHdpbiA9ICgwLCBfaXNXaW5kb3cyLmRlZmF1bHQpKGRvYyk7XG5cbiAgLy8gVGFrZXMgaW4gYWNjb3VudCBwb3RlbnRpYWwgbm9uIHplcm8gbWFyZ2luIG9uIHRoZSBib2R5LlxuICB2YXIgc3R5bGUgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShkb2MuYm9keSk7XG4gIHZhciBtYXJnaW5MZWZ0ID0gcGFyc2VJbnQoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLWxlZnQnKSwgMTApO1xuICB2YXIgbWFyZ2luUmlnaHQgPSBwYXJzZUludChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdtYXJnaW4tcmlnaHQnKSwgMTApO1xuXG4gIHJldHVybiBtYXJnaW5MZWZ0ICsgZG9jLmJvZHkuY2xpZW50V2lkdGggKyBtYXJnaW5SaWdodCA8IHdpbi5pbm5lcldpZHRoO1xufVxuXG5mdW5jdGlvbiBnZXRDb250YWluZXIoKSB7XG4gIHZhciBjb250YWluZXIgPSBfaW5ET00yLmRlZmF1bHQgPyB3aW5kb3cuZG9jdW1lbnQuYm9keSA6IHt9O1xuICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKGNvbnRhaW5lciAhPT0gbnVsbCwgJ1xcbk1hdGVyaWFsLVVJOiB5b3UgYXJlIG1vc3QgbGlrZWx5IGV2YWx1YXRpbmcgdGhlIGNvZGUgYmVmb3JlIHRoZVxcbmJyb3dzZXIgaGFzIGEgY2hhbmNlIHRvIHJlYWNoIHRoZSA8Ym9keT4uXFxuUGxlYXNlIG1vdmUgdGhlIGltcG9ydCBhdCB0aGUgZW5kIG9mIHRoZSA8Ym9keT4uXFxuICAnKSA6IHZvaWQgMDtcbiAgcmV0dXJuIGNvbnRhaW5lcjtcbn1cbi8qKlxuICogU3RhdGUgbWFuYWdlbWVudCBoZWxwZXIgZm9yIG1vZGFscy9sYXllcnMuXG4gKiBTaW1wbGlmaWVkLCBidXQgaW5zcGlyZWQgYnkgcmVhY3Qtb3ZlcmxheSdzIE1vZGFsTWFuYWdlciBjbGFzc1xuICpcbiAqIEBpbnRlcm5hbCBVc2VkIGJ5IHRoZSBNb2RhbCB0byBlbnN1cmUgcHJvcGVyIGZvY3VzIG1hbmFnZW1lbnQuXG4gKi9cbmZ1bmN0aW9uIGNyZWF0ZU1vZGFsTWFuYWdlcigpIHtcbiAgdmFyIF9yZWYgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6IHt9LFxuICAgICAgX3JlZiRoaWRlU2libGluZ05vZGVzID0gX3JlZi5oaWRlU2libGluZ05vZGVzLFxuICAgICAgaGlkZVNpYmxpbmdOb2RlcyA9IF9yZWYkaGlkZVNpYmxpbmdOb2RlcyA9PT0gdW5kZWZpbmVkID8gdHJ1ZSA6IF9yZWYkaGlkZVNpYmxpbmdOb2RlcztcblxuICB2YXIgbW9kYWxzID0gW107XG5cbiAgdmFyIHByZXZPdmVyZmxvdyA9IHZvaWQgMDtcbiAgdmFyIHByZXZQYWRkaW5ncyA9IFtdO1xuXG4gIGZ1bmN0aW9uIGFkZChtb2RhbCkge1xuICAgIHZhciBjb250YWluZXIgPSBnZXRDb250YWluZXIoKTtcbiAgICB2YXIgbW9kYWxJZHggPSBtb2RhbHMuaW5kZXhPZihtb2RhbCk7XG5cbiAgICBpZiAobW9kYWxJZHggIT09IC0xKSB7XG4gICAgICByZXR1cm4gbW9kYWxJZHg7XG4gICAgfVxuXG4gICAgbW9kYWxJZHggPSBtb2RhbHMubGVuZ3RoO1xuICAgIG1vZGFscy5wdXNoKG1vZGFsKTtcblxuICAgIGlmIChoaWRlU2libGluZ05vZGVzKSB7XG4gICAgICAoMCwgX21hbmFnZUFyaWFIaWRkZW4uaGlkZVNpYmxpbmdzKShjb250YWluZXIsIG1vZGFsLm1vdW50Tm9kZSk7XG4gICAgfVxuXG4gICAgaWYgKG1vZGFscy5sZW5ndGggPT09IDEpIHtcbiAgICAgIC8vIFNhdmUgb3VyIGN1cnJlbnQgb3ZlcmZsb3cgc28gd2UgY2FuIHJldmVydFxuICAgICAgLy8gYmFjayB0byBpdCB3aGVuIGFsbCBtb2RhbHMgYXJlIGNsb3NlZCFcbiAgICAgIHByZXZPdmVyZmxvdyA9IGNvbnRhaW5lci5zdHlsZS5vdmVyZmxvdztcblxuICAgICAgaWYgKGJvZHlJc092ZXJmbG93aW5nKGNvbnRhaW5lcikpIHtcbiAgICAgICAgcHJldlBhZGRpbmdzID0gW2dldFBhZGRpbmdSaWdodChjb250YWluZXIpXTtcbiAgICAgICAgdmFyIHNjcm9sbGJhclNpemUgPSAoMCwgX3Njcm9sbGJhclNpemUyLmRlZmF1bHQpKCk7XG4gICAgICAgIGNvbnRhaW5lci5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwcmV2UGFkZGluZ3NbMF0gKyBzY3JvbGxiYXJTaXplICsgJ3B4JztcblxuICAgICAgICB2YXIgZml4ZWROb2RlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5tdWktZml4ZWQnKTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaXhlZE5vZGVzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgICAgdmFyIHBhZGRpbmdSaWdodCA9IGdldFBhZGRpbmdSaWdodChmaXhlZE5vZGVzW2ldKTtcbiAgICAgICAgICBwcmV2UGFkZGluZ3MucHVzaChwYWRkaW5nUmlnaHQpO1xuICAgICAgICAgIGZpeGVkTm9kZXNbaV0uc3R5bGUucGFkZGluZ1JpZ2h0ID0gcGFkZGluZ1JpZ2h0ICsgc2Nyb2xsYmFyU2l6ZSArICdweCc7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgY29udGFpbmVyLnN0eWxlLm92ZXJmbG93ID0gJ2hpZGRlbic7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1vZGFsSWR4O1xuICB9XG5cbiAgZnVuY3Rpb24gcmVtb3ZlKG1vZGFsKSB7XG4gICAgdmFyIGNvbnRhaW5lciA9IGdldENvbnRhaW5lcigpO1xuICAgIHZhciBtb2RhbElkeCA9IG1vZGFscy5pbmRleE9mKG1vZGFsKTtcblxuICAgIGlmIChtb2RhbElkeCA9PT0gLTEpIHtcbiAgICAgIHJldHVybiBtb2RhbElkeDtcbiAgICB9XG5cbiAgICBtb2RhbHMuc3BsaWNlKG1vZGFsSWR4LCAxKTtcblxuICAgIGlmIChtb2RhbHMubGVuZ3RoID09PSAwKSB7XG4gICAgICBjb250YWluZXIuc3R5bGUub3ZlcmZsb3cgPSBwcmV2T3ZlcmZsb3c7XG4gICAgICBjb250YWluZXIuc3R5bGUucGFkZGluZ1JpZ2h0ID0gcHJldlBhZGRpbmdzWzBdO1xuXG4gICAgICB2YXIgZml4ZWROb2RlcyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5tdWktZml4ZWQnKTtcbiAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZml4ZWROb2Rlcy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICBmaXhlZE5vZGVzW2ldLnN0eWxlLnBhZGRpbmdSaWdodCA9IHByZXZQYWRkaW5nc1tpICsgMV0gKyAncHgnO1xuICAgICAgfVxuXG4gICAgICBwcmV2T3ZlcmZsb3cgPSB1bmRlZmluZWQ7XG4gICAgICBwcmV2UGFkZGluZ3MgPSBbXTtcbiAgICAgIGlmIChoaWRlU2libGluZ05vZGVzKSB7XG4gICAgICAgICgwLCBfbWFuYWdlQXJpYUhpZGRlbi5zaG93U2libGluZ3MpKGNvbnRhaW5lciwgbW9kYWwubW91bnROb2RlKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKGhpZGVTaWJsaW5nTm9kZXMpIHtcbiAgICAgIC8vIG90aGVyd2lzZSBtYWtlIHN1cmUgdGhlIG5leHQgdG9wIG1vZGFsIGlzIHZpc2libGUgdG8gYSBTUlxuICAgICAgKDAsIF9tYW5hZ2VBcmlhSGlkZGVuLmFyaWFIaWRkZW4pKGZhbHNlLCBtb2RhbHNbbW9kYWxzLmxlbmd0aCAtIDFdLm1vdW50Tm9kZSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIG1vZGFsSWR4O1xuICB9XG5cbiAgZnVuY3Rpb24gaXNUb3BNb2RhbChtb2RhbCkge1xuICAgIHJldHVybiAhIW1vZGFscy5sZW5ndGggJiYgbW9kYWxzW21vZGFscy5sZW5ndGggLSAxXSA9PT0gbW9kYWw7XG4gIH1cblxuICB2YXIgbW9kYWxNYW5hZ2VyID0geyBhZGQ6IGFkZCwgcmVtb3ZlOiByZW1vdmUsIGlzVG9wTW9kYWw6IGlzVG9wTW9kYWwgfTtcblxuICByZXR1cm4gbW9kYWxNYW5hZ2VyO1xufVxuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVNb2RhbE1hbmFnZXI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvbW9kYWxNYW5hZ2VyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9yZWFjdERvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdERvbSk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX2NvbnRhaW5zID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvcXVlcnkvY29udGFpbnMnKTtcblxudmFyIF9jb250YWluczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb250YWlucyk7XG5cbnZhciBfZGVib3VuY2UgPSByZXF1aXJlKCdsb2Rhc2gvZGVib3VuY2UnKTtcblxudmFyIF9kZWJvdW5jZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWJvdW5jZSk7XG5cbnZhciBfcmVhY3RFdmVudExpc3RlbmVyID0gcmVxdWlyZSgncmVhY3QtZXZlbnQtbGlzdGVuZXInKTtcblxudmFyIF9yZWFjdEV2ZW50TGlzdGVuZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3RFdmVudExpc3RlbmVyKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX01vZGFsID0gcmVxdWlyZSgnLi4vTW9kYWwnKTtcblxudmFyIF9Nb2RhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Nb2RhbCk7XG5cbnZhciBfR3JvdyA9IHJlcXVpcmUoJy4uL3RyYW5zaXRpb25zL0dyb3cnKTtcblxudmFyIF9Hcm93MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0dyb3cpO1xuXG52YXIgX1BhcGVyID0gcmVxdWlyZSgnLi4vUGFwZXInKTtcblxudmFyIF9QYXBlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9QYXBlcik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IE1vZGFsXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcyA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcyB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG5mdW5jdGlvbiBnZXRPZmZzZXRUb3AocmVjdCwgdmVydGljYWwpIHtcbiAgdmFyIG9mZnNldCA9IDA7XG5cbiAgaWYgKHR5cGVvZiB2ZXJ0aWNhbCA9PT0gJ251bWJlcicpIHtcbiAgICBvZmZzZXQgPSB2ZXJ0aWNhbDtcbiAgfSBlbHNlIGlmICh2ZXJ0aWNhbCA9PT0gJ2NlbnRlcicpIHtcbiAgICBvZmZzZXQgPSByZWN0LmhlaWdodCAvIDI7XG4gIH0gZWxzZSBpZiAodmVydGljYWwgPT09ICdib3R0b20nKSB7XG4gICAgb2Zmc2V0ID0gcmVjdC5oZWlnaHQ7XG4gIH1cblxuICByZXR1cm4gb2Zmc2V0O1xufVxuXG5mdW5jdGlvbiBnZXRPZmZzZXRMZWZ0KHJlY3QsIGhvcml6b250YWwpIHtcbiAgdmFyIG9mZnNldCA9IDA7XG5cbiAgaWYgKHR5cGVvZiBob3Jpem9udGFsID09PSAnbnVtYmVyJykge1xuICAgIG9mZnNldCA9IGhvcml6b250YWw7XG4gIH0gZWxzZSBpZiAoaG9yaXpvbnRhbCA9PT0gJ2NlbnRlcicpIHtcbiAgICBvZmZzZXQgPSByZWN0LndpZHRoIC8gMjtcbiAgfSBlbHNlIGlmIChob3Jpem9udGFsID09PSAncmlnaHQnKSB7XG4gICAgb2Zmc2V0ID0gcmVjdC53aWR0aDtcbiAgfVxuXG4gIHJldHVybiBvZmZzZXQ7XG59XG5cbmZ1bmN0aW9uIGdldFRyYW5zZm9ybU9yaWdpblZhbHVlKHRyYW5zZm9ybU9yaWdpbikge1xuICByZXR1cm4gW3RyYW5zZm9ybU9yaWdpbi5ob3Jpem9udGFsLCB0cmFuc2Zvcm1PcmlnaW4udmVydGljYWxdLm1hcChmdW5jdGlvbiAobikge1xuICAgIHJldHVybiB0eXBlb2YgbiA9PT0gJ251bWJlcicgPyBuICsgJ3B4JyA6IG47XG4gIH0pLmpvaW4oJyAnKTtcbn1cblxuLy8gU3VtIHRoZSBzY3JvbGxUb3AgYmV0d2VlbiB0d28gZWxlbWVudHMuXG5mdW5jdGlvbiBnZXRTY3JvbGxQYXJlbnQocGFyZW50LCBjaGlsZCkge1xuICB2YXIgZWxlbWVudCA9IGNoaWxkO1xuICB2YXIgc2Nyb2xsVG9wID0gMDtcblxuICB3aGlsZSAoZWxlbWVudCAmJiBlbGVtZW50ICE9PSBwYXJlbnQpIHtcbiAgICBlbGVtZW50ID0gZWxlbWVudC5wYXJlbnROb2RlO1xuICAgIHNjcm9sbFRvcCArPSBlbGVtZW50LnNjcm9sbFRvcDtcbiAgfVxuICByZXR1cm4gc2Nyb2xsVG9wO1xufVxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSB7XG4gIHBhcGVyOiB7XG4gICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgb3ZlcmZsb3dZOiAnYXV0bycsXG4gICAgb3ZlcmZsb3dYOiAnaGlkZGVuJyxcbiAgICAvLyBTbyB3ZSBzZWUgdGhlIHBvcG92ZXIgd2hlbiBpdCdzIGVtcHR5LlxuICAgIC8vIEl0J3MgbW9zdCBsaWtlbHkgb24gaXNzdWUgb24gdXNlcmxhbmQuXG4gICAgbWluV2lkdGg6IDE2LFxuICAgIG1pbkhlaWdodDogMTYsXG4gICAgbWF4V2lkdGg6ICdjYWxjKDEwMHZ3IC0gMzJweCknLFxuICAgIG1heEhlaWdodDogJ2NhbGMoMTAwdmggLSAzMnB4KScsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZSdcbiAgICB9XG4gIH1cbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9PcmlnaW4gPSB7XG4gIGhvcml6b250YWw6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2xlZnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2NlbnRlciddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncmlnaHQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICB2ZXJ0aWNhbDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsndG9wJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydjZW50ZXInXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2JvdHRvbSddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcl0pLmlzUmVxdWlyZWRcbn07XG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUG9zaXRpb24gPSB7XG4gIHRvcDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkLFxuICBsZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWRcbn07XG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGlzIGlzIHRoZSBET00gZWxlbWVudCB0aGF0IG1heSBiZSB1c2VkXG4gICAqIHRvIHNldCB0aGUgcG9zaXRpb24gb2YgdGhlIHBvcG92ZXIuXG4gICAqL1xuICBhbmNob3JFbDogdHlwZW9mIEhUTUxFbGVtZW50ID09PSAnZnVuY3Rpb24nID8gcmVxdWlyZSgncHJvcC10eXBlcycpLmluc3RhbmNlT2YoSFRNTEVsZW1lbnQpIDogcmVxdWlyZSgncHJvcC10eXBlcycpLmFueSxcblxuICAvKipcbiAgICogVGhpcyBpcyB0aGUgcG9zaXRpb24gdGhhdCBtYXkgYmUgdXNlZFxuICAgKiB0byBzZXQgdGhlIHBvc2l0aW9uIG9mIHRoZSBwb3BvdmVyLlxuICAgKiBUaGUgY29vcmRpbmF0ZXMgYXJlIHJlbGF0aXZlIHRvXG4gICAqIHRoZSBhcHBsaWNhdGlvbidzIGNsaWVudCBhcmVhLlxuICAgKi9cbiAgYW5jaG9yUG9zaXRpb246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgdG9wOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG4gICAgbGVmdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkXG4gIH0pLFxuXG4gIC8qXG4gICAqIFRoaXMgZGV0ZXJtaW5lcyB3aGljaCBhbmNob3IgcHJvcCB0byByZWZlciB0byB0byBzZXRcbiAgICogdGhlIHBvc2l0aW9uIG9mIHRoZSBwb3BvdmVyLlxuICAgKi9cbiAgYW5jaG9yUmVmZXJlbmNlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhbmNob3JFbCcsICdhbmNob3JQb3NpdGlvbiddKSxcblxuICAvKipcbiAgICogVGhpcyBpcyB0aGUgcG9pbnQgb24gdGhlIGFuY2hvciB3aGVyZSB0aGUgcG9wb3ZlcidzXG4gICAqIGBhbmNob3JFbGAgd2lsbCBhdHRhY2ggdG8uIFRoaXMgaXMgbm90IHVzZWQgd2hlbiB0aGVcbiAgICogYW5jaG9yUmVmZXJlbmNlIGlzICdhbmNob3JQb3NpdGlvbicuXG4gICAqXG4gICAqIE9wdGlvbnM6XG4gICAqIHZlcnRpY2FsOiBbdG9wLCBjZW50ZXIsIGJvdHRvbV07XG4gICAqIGhvcml6b250YWw6IFtsZWZ0LCBjZW50ZXIsIHJpZ2h0XS5cbiAgICovXG4gIGFuY2hvck9yaWdpbjogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKHtcbiAgICBob3Jpem9udGFsOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydsZWZ0J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydjZW50ZXInXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3JpZ2h0J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXSkuaXNSZXF1aXJlZCxcbiAgICB2ZXJ0aWNhbDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsndG9wJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydjZW50ZXInXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2JvdHRvbSddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcl0pLmlzUmVxdWlyZWRcbiAgfSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGVsZXZhdGlvbiBvZiB0aGUgcG9wb3Zlci5cbiAgICovXG4gIGVsZXZhdGlvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiBpcyBjYWxsZWQgaW4gb3JkZXIgdG8gcmV0cmlldmUgdGhlIGNvbnRlbnQgYW5jaG9yIGVsZW1lbnQuXG4gICAqIEl0J3MgdGhlIG9wcG9zaXRlIG9mIHRoZSBgYW5jaG9yRWxgIHByb3BlcnR5LlxuICAgKiBUaGUgY29udGVudCBhbmNob3IgZWxlbWVudCBzaG91bGQgYmUgYW4gZWxlbWVudCBpbnNpZGUgdGhlIHBvcG92ZXIuXG4gICAqIEl0J3MgdXNlZCB0byBjb3JyZWN0bHkgc2Nyb2xsIGFuZCBzZXQgdGhlIHBvc2l0aW9uIG9mIHRoZSBwb3BvdmVyLlxuICAgKiBUaGUgcG9zaXRpb25pbmcgc3RyYXRlZ3kgdHJpZXMgdG8gbWFrZSB0aGUgY29udGVudCBhbmNob3IgZWxlbWVudCBqdXN0IGFib3ZlIHRoZVxuICAgKiBhbmNob3IgZWxlbWVudC5cbiAgICovXG4gIGdldENvbnRlbnRBbmNob3JFbDogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFNwZWNpZmllcyBob3cgY2xvc2UgdG8gdGhlIGVkZ2Ugb2YgdGhlIHdpbmRvdyB0aGUgcG9wb3ZlciBjYW4gYXBwZWFyLlxuICAgKi9cbiAgbWFyZ2luVGhyZXNob2xkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIGJlZm9yZSB0aGUgY29tcG9uZW50IGlzIGVudGVyaW5nLlxuICAgKi9cbiAgb25FbnRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBjb21wb25lbnQgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCBoYXMgZW50ZXJlZC5cbiAgICovXG4gIG9uRW50ZXJlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIGNvbXBvbmVudCBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0aW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCBoYXMgZXhpdGVkLlxuICAgKi9cbiAgb25FeGl0ZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgY29tcG9uZW50IHJlcXVlc3RzIHRvIGJlIGNsb3NlZC5cbiAgICpcbiAgICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IFRoZSBldmVudCBzb3VyY2Ugb2YgdGhlIGNhbGxiYWNrLlxuICAgKi9cbiAgb25SZXF1ZXN0Q2xvc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBwb3BvdmVyIGlzIHZpc2libGUuXG4gICAqL1xuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBQYXBlcmAgZWxlbWVudC5cbiAgICovXG4gIFBhcGVyUHJvcHM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHJvbGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoaXMgaXMgdGhlIHBvaW50IG9uIHRoZSBwb3BvdmVyIHdoaWNoXG4gICAqIHdpbGwgYXR0YWNoIHRvIHRoZSBhbmNob3IncyBvcmlnaW4uXG4gICAqXG4gICAqIE9wdGlvbnM6XG4gICAqIHZlcnRpY2FsOiBbdG9wLCBjZW50ZXIsIGJvdHRvbSwgeChweCldO1xuICAgKiBob3Jpem9udGFsOiBbbGVmdCwgY2VudGVyLCByaWdodCwgeChweCldLlxuICAgKi9cbiAgdHJhbnNmb3JtT3JpZ2luOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoe1xuICAgIGhvcml6b250YWw6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2xlZnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2NlbnRlciddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncmlnaHQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICAgIHZlcnRpY2FsOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWyd0b3AnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2NlbnRlciddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYm90dG9tJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgYW5pbWF0aW9uIGNsYXNzTmFtZXMgYXBwbGllZCB0byB0aGUgY29tcG9uZW50IGFzIGl0IGVudGVycyBvciBleGl0cy5cbiAgICogVGhpcyBwcm9wZXJ0eSBpcyBhIGRpcmVjdCBiaW5kaW5nIHRvIFtgQ1NTVHJhbnNpdGlvbi5jbGFzc05hbWVzYF0oaHR0cHM6Ly9yZWFjdGNvbW11bml0eS5vcmcvcmVhY3QtdHJhbnNpdGlvbi1ncm91cC8jQ1NTVHJhbnNpdGlvbi1wcm9wLWNsYXNzTmFtZXMpLlxuICAgKi9cbiAgdHJhbnNpdGlvbkNsYXNzZXM6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcyA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzKSxcblxuICAvKipcbiAgICogU2V0IHRvICdhdXRvJyB0byBhdXRvbWF0aWNhbGx5IGNhbGN1bGF0ZSB0cmFuc2l0aW9uIHRpbWUgYmFzZWQgb24gaGVpZ2h0LlxuICAgKi9cbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoe1xuICAgIGVudGVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICAgIGV4aXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJcbiAgfSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2F1dG8nXSldKVxufTtcblxudmFyIFBvcG92ZXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShQb3BvdmVyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBQb3BvdmVyKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFBvcG92ZXIpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gUG9wb3Zlci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoUG9wb3ZlcikpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmNvbXBvbmVudFdpbGxVbm1vdW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMuaGFuZGxlUmVzaXplLmNhbmNlbCgpO1xuICAgIH0sIF90aGlzLnNldFBvc2l0aW9uaW5nU3R5bGVzID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgIGlmIChlbGVtZW50ICYmIGVsZW1lbnQuc3R5bGUpIHtcbiAgICAgICAgdmFyIHBvc2l0aW9uaW5nID0gX3RoaXMuZ2V0UG9zaXRpb25pbmdTdHlsZShlbGVtZW50KTtcblxuICAgICAgICBlbGVtZW50LnN0eWxlLnRvcCA9IHBvc2l0aW9uaW5nLnRvcDtcbiAgICAgICAgZWxlbWVudC5zdHlsZS5sZWZ0ID0gcG9zaXRpb25pbmcubGVmdDtcbiAgICAgICAgZWxlbWVudC5zdHlsZS50cmFuc2Zvcm1PcmlnaW4gPSBwb3NpdGlvbmluZy50cmFuc2Zvcm1PcmlnaW47XG4gICAgICB9XG4gICAgfSwgX3RoaXMuZ2V0UG9zaXRpb25pbmdTdHlsZSA9IGZ1bmN0aW9uIChlbGVtZW50KSB7XG4gICAgICB2YXIgbWFyZ2luVGhyZXNob2xkID0gX3RoaXMucHJvcHMubWFyZ2luVGhyZXNob2xkO1xuXG4gICAgICAvLyBDaGVjayBpZiB0aGUgcGFyZW50IGhhcyByZXF1ZXN0ZWQgYW5jaG9yaW5nIG9uIGFuIGlubmVyIGNvbnRlbnQgbm9kZVxuXG4gICAgICB2YXIgY29udGVudEFuY2hvck9mZnNldCA9IF90aGlzLmdldENvbnRlbnRBbmNob3JPZmZzZXQoZWxlbWVudCk7XG4gICAgICAvLyBHZXQgdGhlIG9mZnNldCBvZiBvZiB0aGUgYW5jaG9yaW5nIGVsZW1lbnRcbiAgICAgIHZhciBhbmNob3JPZmZzZXQgPSBfdGhpcy5nZXRBbmNob3JPZmZzZXQoY29udGVudEFuY2hvck9mZnNldCk7XG5cbiAgICAgIHZhciBlbGVtUmVjdCA9IHtcbiAgICAgICAgd2lkdGg6IGVsZW1lbnQuY2xpZW50V2lkdGgsXG4gICAgICAgIGhlaWdodDogZWxlbWVudC5jbGllbnRIZWlnaHRcbiAgICAgIH07XG4gICAgICAvLyBHZXQgdGhlIHRyYW5zZm9ybSBvcmlnaW4gcG9pbnQgb24gdGhlIGVsZW1lbnQgaXRzZWxmXG4gICAgICB2YXIgdHJhbnNmb3JtT3JpZ2luID0gX3RoaXMuZ2V0VHJhbnNmb3JtT3JpZ2luKGVsZW1SZWN0LCBjb250ZW50QW5jaG9yT2Zmc2V0KTtcblxuICAgICAgLy8gQ2FsY3VsYXRlIGVsZW1lbnQgcG9zaXRpb25pbmdcbiAgICAgIHZhciB0b3AgPSBhbmNob3JPZmZzZXQudG9wIC0gdHJhbnNmb3JtT3JpZ2luLnZlcnRpY2FsO1xuICAgICAgdmFyIGxlZnQgPSBhbmNob3JPZmZzZXQubGVmdCAtIHRyYW5zZm9ybU9yaWdpbi5ob3Jpem9udGFsO1xuICAgICAgdmFyIGJvdHRvbSA9IHRvcCArIGVsZW1SZWN0LmhlaWdodDtcbiAgICAgIHZhciByaWdodCA9IGxlZnQgKyBlbGVtUmVjdC53aWR0aDtcblxuICAgICAgLy8gV2luZG93IHRocmVzaG9sZHMgdGFraW5nIHJlcXVpcmVkIG1hcmdpbiBpbnRvIGFjY291bnRcbiAgICAgIHZhciBoZWlnaHRUaHJlc2hvbGQgPSB3aW5kb3cuaW5uZXJIZWlnaHQgLSBtYXJnaW5UaHJlc2hvbGQ7XG4gICAgICB2YXIgd2lkdGhUaHJlc2hvbGQgPSB3aW5kb3cuaW5uZXJXaWR0aCAtIG1hcmdpblRocmVzaG9sZDtcblxuICAgICAgLy8gQ2hlY2sgaWYgdGhlIHZlcnRpY2FsIGF4aXMgbmVlZHMgc2hpZnRpbmdcbiAgICAgIGlmICh0b3AgPCBtYXJnaW5UaHJlc2hvbGQpIHtcbiAgICAgICAgdmFyIGRpZmYgPSB0b3AgLSBtYXJnaW5UaHJlc2hvbGQ7XG4gICAgICAgIHRvcCAtPSBkaWZmO1xuICAgICAgICB0cmFuc2Zvcm1PcmlnaW4udmVydGljYWwgKz0gZGlmZjtcbiAgICAgIH0gZWxzZSBpZiAoYm90dG9tID4gaGVpZ2h0VGhyZXNob2xkKSB7XG4gICAgICAgIHZhciBfZGlmZiA9IGJvdHRvbSAtIGhlaWdodFRocmVzaG9sZDtcbiAgICAgICAgdG9wIC09IF9kaWZmO1xuICAgICAgICB0cmFuc2Zvcm1PcmlnaW4udmVydGljYWwgKz0gX2RpZmY7XG4gICAgICB9XG5cbiAgICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkoZWxlbVJlY3QuaGVpZ2h0IDwgaGVpZ2h0VGhyZXNob2xkIHx8ICFlbGVtUmVjdC5oZWlnaHQgfHwgIWhlaWdodFRocmVzaG9sZCwgWydNYXRlcmlhbC1VSTogdGhlIHBvcG92ZXIgY29tcG9uZW50IGlzIHRvbyB0YWxsLicsICdTb21lIHBhcnQgb2YgaXQgY2FuIG5vdCBiZSBzZWVuIG9uIHRoZSBzY3JlZW4gKCcgKyAoZWxlbVJlY3QuaGVpZ2h0IC0gaGVpZ2h0VGhyZXNob2xkKSArICdweCkuJywgJ1BsZWFzZSBjb25zaWRlciBhZGRpbmcgYSBgbWF4LWhlaWdodGAgdG8gaW1wcm92ZSB0aGUgdXNlci1leHBlcmllbmNlLiddLmpvaW4oJ1xcbicpKSA6IHZvaWQgMDtcblxuICAgICAgLy8gQ2hlY2sgaWYgdGhlIGhvcml6b250YWwgYXhpcyBuZWVkcyBzaGlmdGluZ1xuICAgICAgaWYgKGxlZnQgPCBtYXJnaW5UaHJlc2hvbGQpIHtcbiAgICAgICAgdmFyIF9kaWZmMiA9IGxlZnQgLSBtYXJnaW5UaHJlc2hvbGQ7XG4gICAgICAgIGxlZnQgLT0gX2RpZmYyO1xuICAgICAgICB0cmFuc2Zvcm1PcmlnaW4uaG9yaXpvbnRhbCArPSBfZGlmZjI7XG4gICAgICB9IGVsc2UgaWYgKHJpZ2h0ID4gd2lkdGhUaHJlc2hvbGQpIHtcbiAgICAgICAgdmFyIF9kaWZmMyA9IHJpZ2h0IC0gd2lkdGhUaHJlc2hvbGQ7XG4gICAgICAgIGxlZnQgLT0gX2RpZmYzO1xuICAgICAgICB0cmFuc2Zvcm1PcmlnaW4uaG9yaXpvbnRhbCArPSBfZGlmZjM7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRvcDogdG9wICsgJ3B4JyxcbiAgICAgICAgbGVmdDogbGVmdCArICdweCcsXG4gICAgICAgIHRyYW5zZm9ybU9yaWdpbjogZ2V0VHJhbnNmb3JtT3JpZ2luVmFsdWUodHJhbnNmb3JtT3JpZ2luKVxuICAgICAgfTtcbiAgICB9LCBfdGhpcy50cmFuc2l0aW9uRWwgPSB1bmRlZmluZWQsIF90aGlzLmhhbmRsZUdldE9mZnNldFRvcCA9IGdldE9mZnNldFRvcCwgX3RoaXMuaGFuZGxlR2V0T2Zmc2V0TGVmdCA9IGdldE9mZnNldExlZnQsIF90aGlzLmhhbmRsZUVudGVyID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXIoZWxlbWVudCk7XG4gICAgICB9XG5cbiAgICAgIF90aGlzLnNldFBvc2l0aW9uaW5nU3R5bGVzKGVsZW1lbnQpO1xuICAgIH0sIF90aGlzLmhhbmRsZVJlc2l6ZSA9ICgwLCBfZGVib3VuY2UyLmRlZmF1bHQpKGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBlbGVtZW50ID0gX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKF90aGlzLnRyYW5zaXRpb25FbCk7XG4gICAgICBfdGhpcy5zZXRQb3NpdGlvbmluZ1N0eWxlcyhlbGVtZW50KTtcbiAgICB9LCAxNjYpLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoUG9wb3ZlciwgW3tcbiAgICBrZXk6ICdnZXRBbmNob3JPZmZzZXQnLFxuXG5cbiAgICAvLyBSZXR1cm5zIHRoZSB0b3AvbGVmdCBvZmZzZXQgb2YgdGhlIHBvc2l0aW9uXG4gICAgLy8gdG8gYXR0YWNoIHRvIG9uIHRoZSBhbmNob3IgZWxlbWVudCAob3IgYm9keSBpZiBub25lIGlzIHByb3ZpZGVkKVxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRBbmNob3JPZmZzZXQoY29udGVudEFuY2hvck9mZnNldCkge1xuICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYW5jaG9yRWwgPSBfcHJvcHMuYW5jaG9yRWwsXG4gICAgICAgICAgYW5jaG9yT3JpZ2luID0gX3Byb3BzLmFuY2hvck9yaWdpbixcbiAgICAgICAgICBhbmNob3JSZWZlcmVuY2UgPSBfcHJvcHMuYW5jaG9yUmVmZXJlbmNlLFxuICAgICAgICAgIGFuY2hvclBvc2l0aW9uID0gX3Byb3BzLmFuY2hvclBvc2l0aW9uO1xuXG5cbiAgICAgIGlmIChhbmNob3JSZWZlcmVuY2UgPT09ICdhbmNob3JQb3NpdGlvbicpIHtcbiAgICAgICAgcmV0dXJuIGFuY2hvclBvc2l0aW9uO1xuICAgICAgfVxuXG4gICAgICB2YXIgYW5jaG9yRWxlbWVudCA9IGFuY2hvckVsIHx8IGRvY3VtZW50LmJvZHk7XG4gICAgICB2YXIgYW5jaG9yUmVjdCA9IGFuY2hvckVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICB2YXIgYW5jaG9yVmVydGljYWwgPSBjb250ZW50QW5jaG9yT2Zmc2V0ID09PSAwID8gYW5jaG9yT3JpZ2luLnZlcnRpY2FsIDogJ2NlbnRlcic7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIHRvcDogYW5jaG9yUmVjdC50b3AgKyB0aGlzLmhhbmRsZUdldE9mZnNldFRvcChhbmNob3JSZWN0LCBhbmNob3JWZXJ0aWNhbCksXG4gICAgICAgIGxlZnQ6IGFuY2hvclJlY3QubGVmdCArIHRoaXMuaGFuZGxlR2V0T2Zmc2V0TGVmdChhbmNob3JSZWN0LCBhbmNob3JPcmlnaW4uaG9yaXpvbnRhbClcbiAgICAgIH07XG4gICAgfVxuXG4gICAgLy8gUmV0dXJucyB0aGUgdmVydGljYWwgb2Zmc2V0IG9mIGlubmVyIGNvbnRlbnQgdG8gYW5jaG9yIHRoZSB0cmFuc2Zvcm0gb24gaWYgcHJvdmlkZWRcblxuICB9LCB7XG4gICAga2V5OiAnZ2V0Q29udGVudEFuY2hvck9mZnNldCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENvbnRlbnRBbmNob3JPZmZzZXQoZWxlbWVudCkge1xuICAgICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGdldENvbnRlbnRBbmNob3JFbCA9IF9wcm9wczIuZ2V0Q29udGVudEFuY2hvckVsLFxuICAgICAgICAgIGFuY2hvclJlZmVyZW5jZSA9IF9wcm9wczIuYW5jaG9yUmVmZXJlbmNlO1xuXG4gICAgICB2YXIgY29udGVudEFuY2hvck9mZnNldCA9IDA7XG5cbiAgICAgIGlmIChnZXRDb250ZW50QW5jaG9yRWwgJiYgYW5jaG9yUmVmZXJlbmNlID09PSAnYW5jaG9yRWwnKSB7XG4gICAgICAgIHZhciBjb250ZW50QW5jaG9yRWwgPSBnZXRDb250ZW50QW5jaG9yRWwoZWxlbWVudCk7XG5cbiAgICAgICAgaWYgKGNvbnRlbnRBbmNob3JFbCAmJiAoMCwgX2NvbnRhaW5zMi5kZWZhdWx0KShlbGVtZW50LCBjb250ZW50QW5jaG9yRWwpKSB7XG4gICAgICAgICAgdmFyIHNjcm9sbFRvcCA9IGdldFNjcm9sbFBhcmVudChlbGVtZW50LCBjb250ZW50QW5jaG9yRWwpO1xuICAgICAgICAgIGNvbnRlbnRBbmNob3JPZmZzZXQgPSBjb250ZW50QW5jaG9yRWwub2Zmc2V0VG9wICsgY29udGVudEFuY2hvckVsLmNsaWVudEhlaWdodCAvIDIgLSBzY3JvbGxUb3AgfHwgMDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vICE9IHRoZSBkZWZhdWx0IHZhbHVlXG4gICAgICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkodGhpcy5wcm9wcy5hbmNob3JPcmlnaW4udmVydGljYWwgPT09ICd0b3AnLCBbJ01hdGVyaWFsLVVJOiB5b3UgY2FuIG5vdCBjaGFuZ2UgdGhlIGRlZmF1bHQgYGFuY2hvck9yaWdpbi52ZXJ0aWNhbGAgdmFsdWUgd2hlbiBhbHNvICcsICdwcm92aWRpbmcgdGhlIGBnZXRDb250ZW50QW5jaG9yRWxgIHByb3BlcnR5IHRvIHRoZSBwb3BvdmVyIGNvbXBvbmVudC4nLCAnT25seSB1c2Ugb25lIG9mIHRoZSB0d28gcHJvcGVydGllcycsICdTZXQgYGdldENvbnRlbnRBbmNob3JFbGAgdG8gbnVsbCBvciBsZWZ0IGBhbmNob3JPcmlnaW4udmVydGljYWxgIHVuY2hhbmdlZCddLmpvaW4oKSkgOiB2b2lkIDA7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBjb250ZW50QW5jaG9yT2Zmc2V0O1xuICAgIH1cblxuICAgIC8vIFJldHVybiB0aGUgYmFzZSB0cmFuc2Zvcm0gb3JpZ2luIHVzaW5nIHRoZSBlbGVtZW50XG4gICAgLy8gYW5kIHRha2luZyB0aGUgY29udGVudCBhbmNob3Igb2Zmc2V0IGludG8gYWNjb3VudCBpZiBpbiB1c2VcblxuICB9LCB7XG4gICAga2V5OiAnZ2V0VHJhbnNmb3JtT3JpZ2luJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0VHJhbnNmb3JtT3JpZ2luKGVsZW1SZWN0KSB7XG4gICAgICB2YXIgY29udGVudEFuY2hvck9mZnNldCA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDogMDtcbiAgICAgIHZhciB0cmFuc2Zvcm1PcmlnaW4gPSB0aGlzLnByb3BzLnRyYW5zZm9ybU9yaWdpbjtcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdmVydGljYWw6IHRoaXMuaGFuZGxlR2V0T2Zmc2V0VG9wKGVsZW1SZWN0LCB0cmFuc2Zvcm1PcmlnaW4udmVydGljYWwpICsgY29udGVudEFuY2hvck9mZnNldCxcbiAgICAgICAgaG9yaXpvbnRhbDogdGhpcy5oYW5kbGVHZXRPZmZzZXRMZWZ0KGVsZW1SZWN0LCB0cmFuc2Zvcm1PcmlnaW4uaG9yaXpvbnRhbClcbiAgICAgIH07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhbmNob3JFbCA9IF9wcm9wczMuYW5jaG9yRWwsXG4gICAgICAgICAgYW5jaG9yUmVmZXJlbmNlID0gX3Byb3BzMy5hbmNob3JSZWZlcmVuY2UsXG4gICAgICAgICAgYW5jaG9yUG9zaXRpb24gPSBfcHJvcHMzLmFuY2hvclBvc2l0aW9uLFxuICAgICAgICAgIGFuY2hvck9yaWdpbiA9IF9wcm9wczMuYW5jaG9yT3JpZ2luLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzMy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzMy5jbGFzc2VzLFxuICAgICAgICAgIGVsZXZhdGlvbiA9IF9wcm9wczMuZWxldmF0aW9uLFxuICAgICAgICAgIGdldENvbnRlbnRBbmNob3JFbCA9IF9wcm9wczMuZ2V0Q29udGVudEFuY2hvckVsLFxuICAgICAgICAgIG1hcmdpblRocmVzaG9sZCA9IF9wcm9wczMubWFyZ2luVGhyZXNob2xkLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMzLm9uRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZyA9IF9wcm9wczMub25FbnRlcmluZyxcbiAgICAgICAgICBvbkVudGVyZWQgPSBfcHJvcHMzLm9uRW50ZXJlZCxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMzLm9uRXhpdCxcbiAgICAgICAgICBvbkV4aXRpbmcgPSBfcHJvcHMzLm9uRXhpdGluZyxcbiAgICAgICAgICBvbkV4aXRlZCA9IF9wcm9wczMub25FeGl0ZWQsXG4gICAgICAgICAgb3BlbiA9IF9wcm9wczMub3BlbixcbiAgICAgICAgICBQYXBlclByb3BzID0gX3Byb3BzMy5QYXBlclByb3BzLFxuICAgICAgICAgIHJvbGUgPSBfcHJvcHMzLnJvbGUsXG4gICAgICAgICAgdHJhbnNmb3JtT3JpZ2luID0gX3Byb3BzMy50cmFuc2Zvcm1PcmlnaW4sXG4gICAgICAgICAgdHJhbnNpdGlvbkNsYXNzZXMgPSBfcHJvcHMzLnRyYW5zaXRpb25DbGFzc2VzLFxuICAgICAgICAgIHRyYW5zaXRpb25EdXJhdGlvbiA9IF9wcm9wczMudHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzMywgWydhbmNob3JFbCcsICdhbmNob3JSZWZlcmVuY2UnLCAnYW5jaG9yUG9zaXRpb24nLCAnYW5jaG9yT3JpZ2luJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnZWxldmF0aW9uJywgJ2dldENvbnRlbnRBbmNob3JFbCcsICdtYXJnaW5UaHJlc2hvbGQnLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRW50ZXJlZCcsICdvbkV4aXQnLCAnb25FeGl0aW5nJywgJ29uRXhpdGVkJywgJ29wZW4nLCAnUGFwZXJQcm9wcycsICdyb2xlJywgJ3RyYW5zZm9ybU9yaWdpbicsICd0cmFuc2l0aW9uQ2xhc3NlcycsICd0cmFuc2l0aW9uRHVyYXRpb24nXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfTW9kYWwyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBzaG93OiBvcGVuLCBCYWNrZHJvcEludmlzaWJsZTogdHJ1ZSB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIF9Hcm93Mi5kZWZhdWx0LFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFwcGVhcjogdHJ1ZSxcbiAgICAgICAgICAgICdpbic6IG9wZW4sXG4gICAgICAgICAgICBvbkVudGVyOiB0aGlzLmhhbmRsZUVudGVyLFxuICAgICAgICAgICAgb25FbnRlcmluZzogb25FbnRlcmluZyxcbiAgICAgICAgICAgIG9uRW50ZXJlZDogb25FbnRlcmVkLFxuICAgICAgICAgICAgb25FeGl0OiBvbkV4aXQsXG4gICAgICAgICAgICBvbkV4aXRpbmc6IG9uRXhpdGluZyxcbiAgICAgICAgICAgIG9uRXhpdGVkOiBvbkV4aXRlZCxcbiAgICAgICAgICAgIHJvbGU6IHJvbGUsXG4gICAgICAgICAgICB0cmFuc2l0aW9uQ2xhc3NlczogdHJhbnNpdGlvbkNsYXNzZXMsXG4gICAgICAgICAgICB0aW1lb3V0OiB0cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgICByb290UmVmOiBmdW5jdGlvbiByb290UmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgX3RoaXMyLnRyYW5zaXRpb25FbCA9IG5vZGU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSxcbiAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIF9QYXBlcjIuZGVmYXVsdCxcbiAgICAgICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzZXMucGFwZXIsXG4gICAgICAgICAgICAgIGVsZXZhdGlvbjogZWxldmF0aW9uXG4gICAgICAgICAgICB9LCBQYXBlclByb3BzKSxcbiAgICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9yZWFjdEV2ZW50TGlzdGVuZXIyLmRlZmF1bHQsIHsgdGFyZ2V0OiAnd2luZG93Jywgb25SZXNpemU6IHRoaXMuaGFuZGxlUmVzaXplIH0pLFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBQb3BvdmVyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuUG9wb3Zlci5kZWZhdWx0UHJvcHMgPSB7XG4gIGFuY2hvclJlZmVyZW5jZTogJ2FuY2hvckVsJyxcbiAgYW5jaG9yT3JpZ2luOiB7XG4gICAgdmVydGljYWw6ICd0b3AnLFxuICAgIGhvcml6b250YWw6ICdsZWZ0J1xuICB9LFxuICB0cmFuc2Zvcm1PcmlnaW46IHtcbiAgICB2ZXJ0aWNhbDogJ3RvcCcsXG4gICAgaG9yaXpvbnRhbDogJ2xlZnQnXG4gIH0sXG4gIHRyYW5zaXRpb25EdXJhdGlvbjogJ2F1dG8nLFxuICBlbGV2YXRpb246IDgsXG4gIG1hcmdpblRocmVzaG9sZDogMTZcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpUG9wb3ZlcicgfSkoUG9wb3Zlcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvUG9wb3Zlci9Qb3BvdmVyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Qb3BvdmVyL1BvcG92ZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9Qb3BvdmVyID0gcmVxdWlyZSgnLi9Qb3BvdmVyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BvcG92ZXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1BvcG92ZXIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1BvcG92ZXIvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2lubGluZS1ibG9jaycsXG4gICAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcbiAgICAgIGhlaWdodDogMjQsXG4gICAgICB3aWR0aDogMjQsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZScsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdmaWxsJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlclxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRWxlbWVudHMgcGFzc2VkIGludG8gdGhlIFNWRyBJY29uLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm92aWRlcyBhIGh1bWFuLXJlYWRhYmxlIHRpdGxlIGZvciB0aGUgZWxlbWVudCB0aGF0IGNvbnRhaW5zIGl0LlxuICAgKiBodHRwczovL3d3dy53My5vcmcvVFIvU1ZHLWFjY2Vzcy8jRXF1aXZhbGVudFxuICAgKi9cbiAgdGl0bGVBY2Nlc3M6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsbG93cyB5b3UgdG8gcmVkZWZpbmUgd2hhdCB0aGUgY29vcmRpbmF0ZXMgd2l0aG91dCB1bml0cyBtZWFuIGluc2lkZSBhbiBzdmcgZWxlbWVudC5cbiAgICogRm9yIGV4YW1wbGUsIGlmIHRoZSBTVkcgZWxlbWVudCBpcyA1MDAgKHdpZHRoKSBieSAyMDAgKGhlaWdodCksXG4gICAqIGFuZCB5b3UgcGFzcyB2aWV3Qm94PVwiMCAwIDUwIDIwXCIsXG4gICAqIHRoaXMgbWVhbnMgdGhhdCB0aGUgY29vcmRpbmF0ZXMgaW5zaWRlIHRoZSBzdmcgd2lsbCBnbyBmcm9tIHRoZSB0b3AgbGVmdCBjb3JuZXIgKDAsMClcbiAgICogdG8gYm90dG9tIHJpZ2h0ICg1MCwyMCkgYW5kIGVhY2ggdW5pdCB3aWxsIGJlIHdvcnRoIDEwcHguXG4gICAqL1xuICB2aWV3Qm94OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWRcbn07XG5cbnZhciBTdmdJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoU3ZnSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3ZnSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBTdmdJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoU3ZnSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU3ZnSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU3ZnSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgdGl0bGVBY2Nlc3MgPSBfcHJvcHMudGl0bGVBY2Nlc3MsXG4gICAgICAgICAgdmlld0JveCA9IF9wcm9wcy52aWV3Qm94LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ3RpdGxlQWNjZXNzJywgJ3ZpZXdCb3gnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzdmcnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBmb2N1c2FibGU6ICdmYWxzZScsXG4gICAgICAgICAgdmlld0JveDogdmlld0JveCxcbiAgICAgICAgICAnYXJpYS1oaWRkZW4nOiB0aXRsZUFjY2VzcyA/ICdmYWxzZScgOiAndHJ1ZSdcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICB0aXRsZUFjY2VzcyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICd0aXRsZScsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICB0aXRsZUFjY2Vzc1xuICAgICAgICApIDogbnVsbCxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBTdmdJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuU3ZnSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpZXdCb3g6ICcwIDAgMjQgMjQnLFxuICBjb2xvcjogJ2luaGVyaXQnXG59O1xuU3ZnSWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVN2Z0ljb24nIH0pKFN2Z0ljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi9TdmdJY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTsgLy8gIHdlYWtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDogKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInXG4gICAgfSwgdGhlbWUubWl4aW5zLnRvb2xiYXIpLFxuICAgIGd1dHRlcnM6IHRoZW1lLm1peGlucy5ndXR0ZXJzKHt9KVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVG9vbGJhciBjaGlsZHJlbiwgdXN1YWxseSBhIG1peHR1cmUgb2YgYEljb25CdXR0b25gLCBgQnV0dG9uYCBhbmQgYFR5cG9ncmFwaHlgLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGRpc2FibGVzIGd1dHRlciBwYWRkaW5nLlxuICAgKi9cbiAgZGlzYWJsZUd1dHRlcnM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59O1xuXG52YXIgVG9vbGJhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFRvb2xiYXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFRvb2xiYXIoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgVG9vbGJhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKFRvb2xiYXIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFRvb2xiYXIpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFRvb2xiYXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGRpc2FibGVHdXR0ZXJzID0gX3Byb3BzLmRpc2FibGVHdXR0ZXJzLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2Rpc2FibGVHdXR0ZXJzJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuZ3V0dGVycywgIWRpc2FibGVHdXR0ZXJzKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gVG9vbGJhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblRvb2xiYXIuZGVmYXVsdFByb3BzID0ge1xuICBkaXNhYmxlR3V0dGVyczogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpVG9vbGJhcicgfSkoVG9vbGJhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVG9vbGJhci9Ub29sYmFyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Ub29sYmFyL1Rvb2xiYXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1Rvb2xiYXIgPSByZXF1aXJlKCcuL1Rvb2xiYXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVG9vbGJhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVG9vbGJhci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVG9vbGJhci9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9yZWFjdERvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdERvbSk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCB0byBwb3J0YWwgaW4gb3JkZXIgdG8gZXNjYXBlIHRoZSBwYXJlbnQgRE9NIG5vZGUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAgdGhlIGNoaWxkcmVuIHdpbGwgYmUgbW91bnRlZCBpbnRvIHRoZSBET00uXG4gICAqL1xuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgUG9ydGFsID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoUG9ydGFsLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBQb3J0YWwoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgUG9ydGFsKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IFBvcnRhbC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoUG9ydGFsKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMubGF5ZXIgPSBudWxsLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoUG9ydGFsLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJMYXllcigpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcbiAgICAgIGlmICghX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbCkge1xuICAgICAgICB0aGlzLnJlbmRlckxheWVyKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHRoaXMudW5yZW5kZXJMYXllcigpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldExheWVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0TGF5ZXIoKSB7XG4gICAgICBpZiAoIXRoaXMubGF5ZXIpIHtcbiAgICAgICAgdGhpcy5sYXllciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0aGlzLmxheWVyLnNldEF0dHJpYnV0ZSgnZGF0YS1tdWktcG9ydGFsJywgJ3RydWUnKTtcbiAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkgJiYgdGhpcy5sYXllcikge1xuICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5sYXllcik7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMubGF5ZXI7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndW5yZW5kZXJMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVucmVuZGVyTGF5ZXIoKSB7XG4gICAgICBpZiAoIXRoaXMubGF5ZXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgX3JlYWN0RG9tMi5kZWZhdWx0LnVubW91bnRDb21wb25lbnRBdE5vZGUodGhpcy5sYXllcik7XG4gICAgICB9XG5cbiAgICAgIGlmIChkb2N1bWVudC5ib2R5KSB7XG4gICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGhpcy5sYXllcik7XG4gICAgICB9XG4gICAgICB0aGlzLmxheWVyID0gbnVsbDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXJMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckxheWVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb3BlbiA9IF9wcm9wcy5vcGVuO1xuXG5cbiAgICAgIGlmIChvcGVuKSB7XG4gICAgICAgIC8vIEJ5IGNhbGxpbmcgdGhpcyBtZXRob2QgaW4gY29tcG9uZW50RGlkTW91bnQoKSBhbmRcbiAgICAgICAgLy8gY29tcG9uZW50RGlkVXBkYXRlKCksIHlvdSdyZSBlZmZlY3RpdmVseSBjcmVhdGluZyBhIFwid29ybWhvbGVcIiB0aGF0XG4gICAgICAgIC8vIGZ1bm5lbHMgUmVhY3QncyBoaWVyYXJjaGljYWwgdXBkYXRlcyB0aHJvdWdoIHRvIGEgRE9NIG5vZGUgb24gYW5cbiAgICAgICAgLy8gZW50aXJlbHkgZGlmZmVyZW50IHBhcnQgb2YgdGhlIHBhZ2UuXG4gICAgICAgIHZhciBsYXllckVsZW1lbnQgPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ub25seShjaGlsZHJlbik7XG4gICAgICAgIF9yZWFjdERvbTIuZGVmYXVsdC51bnN0YWJsZV9yZW5kZXJTdWJ0cmVlSW50b0NvbnRhaW5lcih0aGlzLCBsYXllckVsZW1lbnQsIHRoaXMuZ2V0TGF5ZXIoKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMyLmNoaWxkcmVuLFxuICAgICAgICAgIG9wZW4gPSBfcHJvcHMyLm9wZW47XG5cbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcblxuICAgICAgaWYgKCFfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICAvLyBDYW4ndCBiZSByZW5kZXJlZCBzZXJ2ZXItc2lkZS5cbiAgICAgIGlmIChfaW5ET00yLmRlZmF1bHQpIHtcbiAgICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgICB2YXIgbGF5ZXIgPSB0aGlzLmdldExheWVyKCk7XG4gICAgICAgICAgLy8gJEZsb3dGaXhNZSBsYXllciBpcyBub24tbnVsbFxuICAgICAgICAgIHJldHVybiBfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKGNoaWxkcmVuLCBsYXllcik7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBQb3J0YWw7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Qb3J0YWwuZGVmYXVsdFByb3BzID0ge1xuICBvcGVuOiBmYWxzZVxufTtcblBvcnRhbC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyB7XG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59IDoge307XG5leHBvcnRzLmRlZmF1bHQgPSBQb3J0YWw7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvUG9ydGFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC9Qb3J0YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5vbmVPZlR5cGUoW3JlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlciwgcmVxdWlyZShcInByb3AtdHlwZXNcIikuc2hhcGUoe1xuICBlbnRlcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLmlzUmVxdWlyZWQsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlci5pc1JlcXVpcmVkXG59KV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZShcInByb3AtdHlwZXNcIikuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0ge1xuICBhcHBlYXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgYXBwZWFyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZXhpdEFjdGl2ZTogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nXG59O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDExIDI3IDI4IDM0IDM1IDM2IDM3IDM4IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX1RyYW5zaXRpb24gPSByZXF1aXJlKCdyZWFjdC10cmFuc2l0aW9uLWdyb3VwL1RyYW5zaXRpb24nKTtcblxudmFyIF9UcmFuc2l0aW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RyYW5zaXRpb24pO1xuXG52YXIgX3RyYW5zaXRpb25zID0gcmVxdWlyZSgnLi4vc3R5bGVzL3RyYW5zaXRpb25zJyk7XG5cbnZhciBfd2l0aFRoZW1lID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhUaGVtZScpO1xuXG52YXIgX3dpdGhUaGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoVGhlbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBUcmFuc2l0aW9uXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGFwcGVhcjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQSBzaW5nbGUgY2hpbGQgY29udGVudCBlbGVtZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjb21wb25lbnQgd2lsbCB0cmFuc2l0aW9uIGluLlxuICAgKi9cbiAgaW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcmluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgdGltZW91dDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uKS5pc1JlcXVpcmVkXG59O1xuXG5cbnZhciByZWZsb3cgPSBmdW5jdGlvbiByZWZsb3cobm9kZSkge1xuICByZXR1cm4gbm9kZS5zY3JvbGxUb3A7XG59O1xuXG4vKipcbiAqIFRoZSBGYWRlIHRyYW5zaXRpb24gaXMgdXNlZCBieSB0aGUgTW9kYWwgY29tcG9uZW50LlxuICogSXQncyB1c2luZyBbcmVhY3QtdHJhbnNpdGlvbi1ncm91cF0oaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0anMvcmVhY3QtdHJhbnNpdGlvbi1ncm91cCkgaW50ZXJuYWxseS5cbiAqL1xuXG52YXIgRmFkZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEZhZGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEZhZGUoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgRmFkZSk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBGYWRlLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShGYWRlKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgcmVmbG93KG5vZGUpO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUVudGVyaW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMudGhlbWUsXG4gICAgICAgICAgdGltZW91dCA9IF90aGlzJHByb3BzLnRpbWVvdXQ7XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmVudGVyXG4gICAgICB9KTtcbiAgICAgIC8vICRGbG93Rml4TWUgLSBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svZmxvdy9wdWxsLzUxNjFcbiAgICAgIG5vZGUuc3R5bGUud2Via2l0VHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmVudGVyXG4gICAgICB9KTtcbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcxJztcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcmluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMyLnRpbWVvdXQ7XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmV4aXRcbiAgICAgIH0pO1xuICAgICAgLy8gJEZsb3dGaXhNZSAtIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9mbG93L3B1bGwvNTE2MVxuICAgICAgbm9kZS5zdHlsZS53ZWJraXRUcmFuc2l0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5Jywge1xuICAgICAgICBkdXJhdGlvbjogdHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInID8gdGltZW91dCA6IHRpbWVvdXQuZXhpdFxuICAgICAgfSk7XG4gICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMCc7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXQpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0KG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShGYWRlLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFwcGVhciA9IF9wcm9wcy5hcHBlYXIsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wcy5vbkVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmcgPSBfcHJvcHMub25FbnRlcmluZyxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMub25FeGl0LFxuICAgICAgICAgIHN0eWxlUHJvcCA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhcHBlYXInLCAnY2hpbGRyZW4nLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRXhpdCcsICdzdHlsZScsICd0aGVtZSddKTtcblxuXG4gICAgICB2YXIgc3R5bGUgPSAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBzdHlsZVByb3ApO1xuXG4gICAgICAvLyBGb3Igc2VydmVyIHNpZGUgcmVuZGVyaW5nLlxuICAgICAgaWYgKCF0aGlzLnByb3BzLmluIHx8IGFwcGVhcikge1xuICAgICAgICBzdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9UcmFuc2l0aW9uMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhcHBlYXI6IGFwcGVhcixcbiAgICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgICAgb25FbnRlcjogdGhpcy5oYW5kbGVFbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nOiB0aGlzLmhhbmRsZUVudGVyaW5nLFxuICAgICAgICAgIG9uRXhpdDogdGhpcy5oYW5kbGVFeGl0XG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBGYWRlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuRmFkZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGFwcGVhcjogdHJ1ZSxcbiAgdGltZW91dDoge1xuICAgIGVudGVyOiBfdHJhbnNpdGlvbnMuZHVyYXRpb24uZW50ZXJpbmdTY3JlZW4sXG4gICAgZXhpdDogX3RyYW5zaXRpb25zLmR1cmF0aW9uLmxlYXZpbmdTY3JlZW5cbiAgfVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFRoZW1lMi5kZWZhdWx0KSgpKEZhZGUpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0ZhZGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0ZhZGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxuZXhwb3J0cy5nZXRTY2FsZSA9IGdldFNjYWxlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfQ1NTVHJhbnNpdGlvbiA9IHJlcXVpcmUoJ3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvQ1NTVHJhbnNpdGlvbicpO1xuXG52YXIgX0NTU1RyYW5zaXRpb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQ1NTVHJhbnNpdGlvbik7XG5cbnZhciBfd2l0aFRoZW1lID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhUaGVtZScpO1xuXG52YXIgX3dpdGhUaGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoVGhlbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBDU1NUcmFuc2l0aW9uXG5cbi8vIE9ubHkgZXhwb3J0ZWQgZm9yIHRlc3RzLlxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbmZ1bmN0aW9uIGdldFNjYWxlKHZhbHVlKSB7XG4gIHJldHVybiAnc2NhbGUoJyArIHZhbHVlICsgJywgJyArIE1hdGgucG93KHZhbHVlLCAyKSArICcpJztcbn1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gIGVudGVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICBleGl0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG59KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBhcHBlYXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEEgc2luZ2xlIGNoaWxkIGNvbnRlbnQgZWxlbWVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBzaG93IHRoZSBjb21wb25lbnQ7IHRyaWdnZXJzIHRoZSBlbnRlciBvciBleGl0IGFuaW1hdGlvbi5cbiAgICovXG4gIGluOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkV4aXRpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FeGl0ZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBhbmltYXRpb24gY2xhc3NOYW1lcyBhcHBsaWVkIHRvIHRoZSBjb21wb25lbnQgYXMgaXQgZW50ZXJzIG9yIGV4aXRzLlxuICAgKiBUaGlzIHByb3BlcnR5IGlzIGEgZGlyZWN0IGJpbmRpbmcgdG8gW2BDU1NUcmFuc2l0aW9uLmNsYXNzTmFtZXNgXShodHRwczovL3JlYWN0Y29tbXVuaXR5Lm9yZy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwLyNDU1NUcmFuc2l0aW9uLXByb3AtY2xhc3NOYW1lcykuXG4gICAqL1xuICB0cmFuc2l0aW9uQ2xhc3NlczogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNsYXNzZXMuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcyA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcykuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGR1cmF0aW9uIGZvciB0aGUgdHJhbnNpdGlvbiwgaW4gbWlsbGlzZWNvbmRzLlxuICAgKiBZb3UgbWF5IHNwZWNpZnkgYSBzaW5nbGUgdGltZW91dCBmb3IgYWxsIHRyYW5zaXRpb25zLCBvciBpbmRpdmlkdWFsbHkgd2l0aCBhbiBvYmplY3QuXG4gICAqXG4gICAqIFNldCB0byAnYXV0bycgdG8gYXV0b21hdGljYWxseSBjYWxjdWxhdGUgdHJhbnNpdGlvbiB0aW1lIGJhc2VkIG9uIGhlaWdodC5cbiAgICovXG4gIHRpbWVvdXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgZW50ZXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gICAgZXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxuICB9KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogVGhlIEdyb3cgdHJhbnNpdGlvbiBpcyB1c2VkIGJ5IHRoZSBQb3BvdmVyIGNvbXBvbmVudC5cbiAqIEl0J3MgdXNpbmcgW3JlYWN0LXRyYW5zaXRpb24tZ3JvdXBdKGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdGpzL3JlYWN0LXRyYW5zaXRpb24tZ3JvdXApIGludGVybmFsbHkuXG4gKi9cbnZhciBHcm93ID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoR3JvdywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gR3JvdygpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBHcm93KTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IEdyb3cuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEdyb3cpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5hdXRvVGltZW91dCA9IHVuZGVmaW5lZCwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgbm9kZS5zdHlsZS50cmFuc2Zvcm0gPSBnZXRTY2FsZSgwLjc1KTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXIpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcihub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFbnRlcmluZyA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzLnRoZW1lLFxuICAgICAgICAgIHRpbWVvdXQgPSBfdGhpcyRwcm9wcy50aW1lb3V0O1xuXG4gICAgICB2YXIgZHVyYXRpb24gPSAwO1xuXG4gICAgICBpZiAodGltZW91dCA9PT0gJ2F1dG8nKSB7XG4gICAgICAgIGR1cmF0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuZ2V0QXV0b0hlaWdodER1cmF0aW9uKG5vZGUuY2xpZW50SGVpZ2h0KTtcbiAgICAgICAgX3RoaXMuYXV0b1RpbWVvdXQgPSBkdXJhdGlvbjtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIGR1cmF0aW9uID0gdGltZW91dDtcbiAgICAgIH0gZWxzZSBpZiAodGltZW91dCAmJiB0eXBlb2YgdGltZW91dC5lbnRlciA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgZHVyYXRpb24gPSB0aW1lb3V0LmVudGVyO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gVGhlIHByb3BUeXBlIHdpbGwgd2FybiBpbiB0aGlzIGNhc2UuXG4gICAgICB9XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IFt0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiBkdXJhdGlvblxuICAgICAgfSksIHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgndHJhbnNmb3JtJywge1xuICAgICAgICBkdXJhdGlvbjogZHVyYXRpb24gKiAwLjY2NlxuICAgICAgfSldLmpvaW4oJywnKTtcblxuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzEnO1xuICAgICAgbm9kZS5zdHlsZS50cmFuc2Zvcm0gPSBnZXRTY2FsZSgxKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcmluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMyLnRpbWVvdXQ7XG5cbiAgICAgIHZhciBkdXJhdGlvbiA9IDA7XG5cbiAgICAgIGlmICh0aW1lb3V0ID09PSAnYXV0bycpIHtcbiAgICAgICAgZHVyYXRpb24gPSB0aGVtZS50cmFuc2l0aW9ucy5nZXRBdXRvSGVpZ2h0RHVyYXRpb24obm9kZS5jbGllbnRIZWlnaHQpO1xuICAgICAgICBfdGhpcy5hdXRvVGltZW91dCA9IGR1cmF0aW9uO1xuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgdGltZW91dCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgZHVyYXRpb24gPSB0aW1lb3V0O1xuICAgICAgfSBlbHNlIGlmICh0aW1lb3V0ICYmIHR5cGVvZiB0aW1lb3V0LmV4aXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIGR1cmF0aW9uID0gdGltZW91dC5leGl0O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gVGhlIHByb3BUeXBlIHdpbGwgd2FybiBpbiB0aGlzIGNhc2UuXG4gICAgICB9XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IFt0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiBkdXJhdGlvblxuICAgICAgfSksIHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgndHJhbnNmb3JtJywge1xuICAgICAgICBkdXJhdGlvbjogZHVyYXRpb24gKiAwLjY2NixcbiAgICAgICAgZGVsYXk6IGR1cmF0aW9uICogMC4zMzNcbiAgICAgIH0pXS5qb2luKCcsJyk7XG5cbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcwJztcbiAgICAgIG5vZGUuc3R5bGUudHJhbnNmb3JtID0gZ2V0U2NhbGUoMC43NSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXQpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0KG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmFkZEVuZExpc3RlbmVyID0gZnVuY3Rpb24gKG5vZGUsIG5leHQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy50aW1lb3V0ID09PSAnYXV0bycpIHtcbiAgICAgICAgc2V0VGltZW91dChuZXh0LCBfdGhpcy5hdXRvVGltZW91dCB8fCAwKTtcbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoR3JvdywgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhcHBlYXIgPSBfcHJvcHMuYXBwZWFyLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzLm9uRXhpdCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgc3R5bGVQcm9wID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICAgIHRyYW5zaXRpb25DbGFzc2VzID0gX3Byb3BzLnRyYW5zaXRpb25DbGFzc2VzLFxuICAgICAgICAgIHRpbWVvdXQgPSBfcHJvcHMudGltZW91dCxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhcHBlYXInLCAnY2hpbGRyZW4nLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRXhpdCcsICdyb290UmVmJywgJ3N0eWxlJywgJ3RyYW5zaXRpb25DbGFzc2VzJywgJ3RpbWVvdXQnLCAndGhlbWUnXSk7XG5cblxuICAgICAgdmFyIHN0eWxlID0gKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgY2hpbGRyZW4ucHJvcHMuc3R5bGUsIHN0eWxlUHJvcCk7XG5cbiAgICAgIC8vIEZvciBzZXJ2ZXIgc2lkZSByZW5kZXJpbmcuXG4gICAgICBpZiAoIXRoaXMucHJvcHMuaW4gfHwgYXBwZWFyKSB7XG4gICAgICAgIHN0eWxlLm9wYWNpdHkgPSAnMCc7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0NTU1RyYW5zaXRpb24yLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZXM6IHRyYW5zaXRpb25DbGFzc2VzLFxuICAgICAgICAgIG9uRW50ZXI6IHRoaXMuaGFuZGxlRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZzogdGhpcy5oYW5kbGVFbnRlcmluZyxcbiAgICAgICAgICBvbkV4aXQ6IHRoaXMuaGFuZGxlRXhpdCxcbiAgICAgICAgICBhZGRFbmRMaXN0ZW5lcjogdGhpcy5hZGRFbmRMaXN0ZW5lcixcbiAgICAgICAgICBhcHBlYXI6IGFwcGVhcixcbiAgICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgICAgdGltZW91dDogdGltZW91dCA9PT0gJ2F1dG8nID8gbnVsbCA6IHRpbWVvdXRcbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gR3Jvdztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkdyb3cuZGVmYXVsdFByb3BzID0ge1xuICBhcHBlYXI6IHRydWUsXG4gIHRpbWVvdXQ6ICdhdXRvJyxcbiAgdHJhbnNpdGlvbkNsYXNzZXM6IHt9XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoVGhlbWUyLmRlZmF1bHQpKCkoR3Jvdyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvR3Jvdy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvR3Jvdy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5hcmlhSGlkZGVuID0gYXJpYUhpZGRlbjtcbmV4cG9ydHMuaGlkZVNpYmxpbmdzID0gaGlkZVNpYmxpbmdzO1xuZXhwb3J0cy5zaG93U2libGluZ3MgPSBzaG93U2libGluZ3M7XG4vLyAgd2Vha1xuXG52YXIgQkxBQ0tMSVNUID0gWyd0ZW1wbGF0ZScsICdzY3JpcHQnLCAnc3R5bGUnXTtcblxudmFyIGlzSGlkYWJsZSA9IGZ1bmN0aW9uIGlzSGlkYWJsZShfcmVmKSB7XG4gIHZhciBub2RlVHlwZSA9IF9yZWYubm9kZVR5cGUsXG4gICAgICB0YWdOYW1lID0gX3JlZi50YWdOYW1lO1xuICByZXR1cm4gbm9kZVR5cGUgPT09IDEgJiYgQkxBQ0tMSVNULmluZGV4T2YodGFnTmFtZS50b0xvd2VyQ2FzZSgpKSA9PT0gLTE7XG59O1xuXG52YXIgc2libGluZ3MgPSBmdW5jdGlvbiBzaWJsaW5ncyhjb250YWluZXIsIG1vdW50LCBjYikge1xuICBtb3VudCA9IFtdLmNvbmNhdChtb3VudCk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgW10uZm9yRWFjaC5jYWxsKGNvbnRhaW5lci5jaGlsZHJlbiwgZnVuY3Rpb24gKG5vZGUpIHtcbiAgICBpZiAobW91bnQuaW5kZXhPZihub2RlKSA9PT0gLTEgJiYgaXNIaWRhYmxlKG5vZGUpKSB7XG4gICAgICBjYihub2RlKTtcbiAgICB9XG4gIH0pO1xufTtcblxuZnVuY3Rpb24gYXJpYUhpZGRlbihzaG93LCBub2RlKSB7XG4gIGlmICghbm9kZSkge1xuICAgIHJldHVybjtcbiAgfVxuICBpZiAoc2hvdykge1xuICAgIG5vZGUuc2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG4gIH0gZWxzZSB7XG4gICAgbm9kZS5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJyk7XG4gIH1cbn1cblxuZnVuY3Rpb24gaGlkZVNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlKSB7XG4gIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBhcmlhSGlkZGVuKHRydWUsIG5vZGUpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gc2hvd1NpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlKSB7XG4gIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBhcmlhSGlkZGVuKGZhbHNlLCBub2RlKTtcbiAgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfaW52YXJpYW50ID0gcmVxdWlyZSgnaW52YXJpYW50Jyk7XG5cbnZhciBfaW52YXJpYW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ludmFyaWFudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgaXNNb2RpZmllZEV2ZW50ID0gZnVuY3Rpb24gaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB7XG4gIHJldHVybiAhIShldmVudC5tZXRhS2V5IHx8IGV2ZW50LmFsdEtleSB8fCBldmVudC5jdHJsS2V5IHx8IGV2ZW50LnNoaWZ0S2V5KTtcbn07XG5cbi8qKlxuICogVGhlIHB1YmxpYyBBUEkgZm9yIHJlbmRlcmluZyBhIGhpc3RvcnktYXdhcmUgPGE+LlxuICovXG5cbnZhciBMaW5rID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKExpbmssIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpbmsoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMaW5rKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5oYW5kbGVDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2xpY2spIF90aGlzLnByb3BzLm9uQ2xpY2soZXZlbnQpO1xuXG4gICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgJiYgLy8gb25DbGljayBwcmV2ZW50ZWQgZGVmYXVsdFxuICAgICAgZXZlbnQuYnV0dG9uID09PSAwICYmIC8vIGlnbm9yZSByaWdodCBjbGlja3NcbiAgICAgICFfdGhpcy5wcm9wcy50YXJnZXQgJiYgLy8gbGV0IGJyb3dzZXIgaGFuZGxlIFwidGFyZ2V0PV9ibGFua1wiIGV0Yy5cbiAgICAgICFpc01vZGlmaWVkRXZlbnQoZXZlbnQpIC8vIGlnbm9yZSBjbGlja3Mgd2l0aCBtb2RpZmllciBrZXlzXG4gICAgICApIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgdmFyIGhpc3RvcnkgPSBfdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5O1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgICByZXBsYWNlID0gX3RoaXMkcHJvcHMucmVwbGFjZSxcbiAgICAgICAgICAgICAgdG8gPSBfdGhpcyRwcm9wcy50bztcblxuXG4gICAgICAgICAgaWYgKHJlcGxhY2UpIHtcbiAgICAgICAgICAgIGhpc3RvcnkucmVwbGFjZSh0byk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGhpc3RvcnkucHVzaCh0byk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBMaW5rLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHJlcGxhY2UgPSBfcHJvcHMucmVwbGFjZSxcbiAgICAgICAgdG8gPSBfcHJvcHMudG8sXG4gICAgICAgIGlubmVyUmVmID0gX3Byb3BzLmlubmVyUmVmLFxuICAgICAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsncmVwbGFjZScsICd0bycsICdpbm5lclJlZiddKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgKDAsIF9pbnZhcmlhbnQyLmRlZmF1bHQpKHRoaXMuY29udGV4dC5yb3V0ZXIsICdZb3Ugc2hvdWxkIG5vdCB1c2UgPExpbms+IG91dHNpZGUgYSA8Um91dGVyPicpO1xuXG4gICAgdmFyIGhyZWYgPSB0aGlzLmNvbnRleHQucm91dGVyLmhpc3RvcnkuY3JlYXRlSHJlZih0eXBlb2YgdG8gPT09ICdzdHJpbmcnID8geyBwYXRobmFtZTogdG8gfSA6IHRvKTtcblxuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnYScsIF9leHRlbmRzKHt9LCBwcm9wcywgeyBvbkNsaWNrOiB0aGlzLmhhbmRsZUNsaWNrLCBocmVmOiBocmVmLCByZWY6IGlubmVyUmVmIH0pKTtcbiAgfTtcblxuICByZXR1cm4gTGluaztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpbmsucHJvcFR5cGVzID0ge1xuICBvbkNsaWNrOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIHRhcmdldDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG4gIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgdG86IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3RdKS5pc1JlcXVpcmVkLFxuICBpbm5lclJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNdKVxufTtcbkxpbmsuZGVmYXVsdFByb3BzID0ge1xuICByZXBsYWNlOiBmYWxzZVxufTtcbkxpbmsuY29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgIGhpc3Rvcnk6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgICAgcHVzaDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIGNyZWF0ZUhyZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkXG4gICAgfSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gTGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI2IDI3IDI4IDMwIDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDU3IDU4IDU5IDYxIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIFByb3BUeXBlcyA9IF9pbnRlcm9wUmVxdWlyZVdpbGRjYXJkKF9wcm9wVHlwZXMpO1xuXG52YXIgX2FkZENsYXNzID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvY2xhc3MvYWRkQ2xhc3MnKTtcblxudmFyIF9hZGRDbGFzczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9hZGRDbGFzcyk7XG5cbnZhciBfcmVtb3ZlQ2xhc3MgPSByZXF1aXJlKCdkb20taGVscGVycy9jbGFzcy9yZW1vdmVDbGFzcycpO1xuXG52YXIgX3JlbW92ZUNsYXNzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlbW92ZUNsYXNzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX1RyYW5zaXRpb24gPSByZXF1aXJlKCcuL1RyYW5zaXRpb24nKTtcblxudmFyIF9UcmFuc2l0aW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RyYW5zaXRpb24pO1xuXG52YXIgX1Byb3BUeXBlcyA9IHJlcXVpcmUoJy4vdXRpbHMvUHJvcFR5cGVzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZVdpbGRjYXJkKG9iaikgeyBpZiAob2JqICYmIG9iai5fX2VzTW9kdWxlKSB7IHJldHVybiBvYmo7IH0gZWxzZSB7IHZhciBuZXdPYmogPSB7fTsgaWYgKG9iaiAhPSBudWxsKSB7IGZvciAodmFyIGtleSBpbiBvYmopIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGtleSkpIG5ld09ialtrZXldID0gb2JqW2tleV07IH0gfSBuZXdPYmouZGVmYXVsdCA9IG9iajsgcmV0dXJuIG5ld09iajsgfSB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGFkZENsYXNzID0gZnVuY3Rpb24gYWRkQ2xhc3Mobm9kZSwgY2xhc3Nlcykge1xuICByZXR1cm4gY2xhc3NlcyAmJiBjbGFzc2VzLnNwbGl0KCcgJykuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiAoMCwgX2FkZENsYXNzMi5kZWZhdWx0KShub2RlLCBjKTtcbiAgfSk7XG59O1xudmFyIHJlbW92ZUNsYXNzID0gZnVuY3Rpb24gcmVtb3ZlQ2xhc3Mobm9kZSwgY2xhc3Nlcykge1xuICByZXR1cm4gY2xhc3NlcyAmJiBjbGFzc2VzLnNwbGl0KCcgJykuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiAoMCwgX3JlbW92ZUNsYXNzMi5kZWZhdWx0KShub2RlLCBjKTtcbiAgfSk7XG59O1xuXG52YXIgcHJvcFR5cGVzID0gX2V4dGVuZHMoe30sIF9UcmFuc2l0aW9uMi5kZWZhdWx0LnByb3BUeXBlcywge1xuXG4gIC8qKlxuICAgKiBUaGUgYW5pbWF0aW9uIGNsYXNzTmFtZXMgYXBwbGllZCB0byB0aGUgY29tcG9uZW50IGFzIGl0IGVudGVycyBvciBleGl0cy5cbiAgICogQSBzaW5nbGUgbmFtZSBjYW4gYmUgcHJvdmlkZWQgYW5kIGl0IHdpbGwgYmUgc3VmZml4ZWQgZm9yIGVhY2ggc3RhZ2U6IGUuZy5cbiAgICpcbiAgICogYGNsYXNzTmFtZXM9XCJmYWRlXCJgIGFwcGxpZXMgYGZhZGUtZW50ZXJgLCBgZmFkZS1lbnRlci1hY3RpdmVgLFxuICAgKiBgZmFkZS1leGl0YCwgYGZhZGUtZXhpdC1hY3RpdmVgLCBgZmFkZS1hcHBlYXJgLCBhbmQgYGZhZGUtYXBwZWFyLWFjdGl2ZWAuXG4gICAqIEVhY2ggaW5kaXZpZHVhbCBjbGFzc05hbWVzIGNhbiBhbHNvIGJlIHNwZWNpZmllZCBpbmRlcGVuZGVudGx5IGxpa2U6XG4gICAqXG4gICAqIGBgYGpzXG4gICAqIGNsYXNzTmFtZXM9e3tcbiAgICogIGFwcGVhcjogJ215LWFwcGVhcicsXG4gICAqICBhcHBlYXJBY3RpdmU6ICdteS1hY3RpdmUtYXBwZWFyJyxcbiAgICogIGVudGVyOiAnbXktZW50ZXInLFxuICAgKiAgZW50ZXJBY3RpdmU6ICdteS1hY3RpdmUtZW50ZXInLFxuICAgKiAgZXhpdDogJ215LWV4aXQnLFxuICAgKiAgZXhpdEFjdGl2ZTogJ215LWFjdGl2ZS1leGl0JyxcbiAgICogfX1cbiAgICogYGBgXG4gICAqXG4gICAqIEB0eXBlIHtzdHJpbmcgfCB7XG4gICAqICBhcHBlYXI/OiBzdHJpbmcsXG4gICAqICBhcHBlYXJBY3RpdmU/OiBzdHJpbmcsXG4gICAqICBlbnRlcj86IHN0cmluZyxcbiAgICogIGVudGVyQWN0aXZlPzogc3RyaW5nLFxuICAgKiAgZXhpdD86IHN0cmluZyxcbiAgICogIGV4aXRBY3RpdmU/OiBzdHJpbmcsXG4gICAqIH19XG4gICAqL1xuICBjbGFzc05hbWVzOiBfUHJvcFR5cGVzLmNsYXNzTmFtZXNTaGFwZSxcblxuICAvKipcbiAgICogQSBgPFRyYW5zaXRpb24+YCBjYWxsYmFjayBmaXJlZCBpbW1lZGlhdGVseSBhZnRlciB0aGUgJ2VudGVyJyBvciAnYXBwZWFyJyBjbGFzcyBpc1xuICAgKiBhcHBsaWVkLlxuICAgKlxuICAgKiBAdHlwZSBGdW5jdGlvbihub2RlOiBIdG1sRWxlbWVudCwgaXNBcHBlYXJpbmc6IGJvb2wpXG4gICAqL1xuICBvbkVudGVyOiBQcm9wVHlwZXMuZnVuYyxcblxuICAvKipcbiAgICogQSBgPFRyYW5zaXRpb24+YCBjYWxsYmFjayBmaXJlZCBpbW1lZGlhdGVseSBhZnRlciB0aGUgJ2VudGVyLWFjdGl2ZScgb3JcbiAgICogJ2FwcGVhci1hY3RpdmUnIGNsYXNzIGlzIGFwcGxpZWQuXG4gICAqXG4gICAqIEB0eXBlIEZ1bmN0aW9uKG5vZGU6IEh0bWxFbGVtZW50LCBpc0FwcGVhcmluZzogYm9vbClcbiAgICovXG4gIG9uRW50ZXJpbmc6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZW50ZXInIG9yXG4gICAqICdhcHBlYXInIGNsYXNzZXMgYXJlICoqcmVtb3ZlZCoqIGZyb20gdGhlIERPTSBub2RlLlxuICAgKlxuICAgKiBAdHlwZSBGdW5jdGlvbihub2RlOiBIdG1sRWxlbWVudCwgaXNBcHBlYXJpbmc6IGJvb2wpXG4gICAqL1xuICBvbkVudGVyZWQ6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZXhpdCcgY2xhc3MgaXNcbiAgICogYXBwbGllZC5cbiAgICpcbiAgICogQHR5cGUgRnVuY3Rpb24obm9kZTogSHRtbEVsZW1lbnQpXG4gICAqL1xuICBvbkV4aXQ6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZXhpdC1hY3RpdmUnIGlzIGFwcGxpZWQuXG4gICAqXG4gICAqIEB0eXBlIEZ1bmN0aW9uKG5vZGU6IEh0bWxFbGVtZW50XG4gICAqL1xuICBvbkV4aXRpbmc6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZXhpdCcgY2xhc3Nlc1xuICAgKiBhcmUgKipyZW1vdmVkKiogZnJvbSB0aGUgRE9NIG5vZGUuXG4gICAqXG4gICAqIEB0eXBlIEZ1bmN0aW9uKG5vZGU6IEh0bWxFbGVtZW50KVxuICAgKi9cbiAgb25FeGl0ZWQ6IFByb3BUeXBlcy5mdW5jXG59KTtcblxuLyoqXG4gKiBBIGBUcmFuc2l0aW9uYCBjb21wb25lbnQgdXNpbmcgQ1NTIHRyYW5zaXRpb25zIGFuZCBhbmltYXRpb25zLlxuICogSXQncyBpbnNwaXJlZCBieSB0aGUgZXhjZWxsZW50IFtuZy1hbmltYXRlXShodHRwOi8vd3d3Lm5nYW5pbWF0ZS5vcmcvKSBsaWJyYXJ5LlxuICpcbiAqIGBDU1NUcmFuc2l0aW9uYCBhcHBsaWVzIGEgcGFpciBvZiBjbGFzcyBuYW1lcyBkdXJpbmcgdGhlIGBhcHBlYXJgLCBgZW50ZXJgLFxuICogYW5kIGBleGl0YCBzdGFnZXMgb2YgdGhlIHRyYW5zaXRpb24uIFRoZSBmaXJzdCBjbGFzcyBpcyBhcHBsaWVkIGFuZCB0aGVuIGFcbiAqIHNlY29uZCBcImFjdGl2ZVwiIGNsYXNzIGluIG9yZGVyIHRvIGFjdGl2YXRlIHRoZSBjc3MgYW5pbWF0aW9uLlxuICpcbiAqIFdoZW4gdGhlIGBpbmAgcHJvcCBpcyB0b2dnbGVkIHRvIGB0cnVlYCB0aGUgQ29tcG9uZW50IHdpbGwgZ2V0XG4gKiB0aGUgYGV4YW1wbGUtZW50ZXJgIENTUyBjbGFzcyBhbmQgdGhlIGBleGFtcGxlLWVudGVyLWFjdGl2ZWAgQ1NTIGNsYXNzXG4gKiBhZGRlZCBpbiB0aGUgbmV4dCB0aWNrLiBUaGlzIGlzIGEgY29udmVudGlvbiBiYXNlZCBvbiB0aGUgYGNsYXNzTmFtZXNgIHByb3AuXG4gKlxuICogYGBganNcbiAqIGltcG9ydCBDU1NUcmFuc2l0aW9uIGZyb20gJ3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvQ1NTVHJhbnNpdGlvbic7XG4gKlxuICogY29uc3QgRmFkZSA9ICh7IGNoaWxkcmVuLCAuLi5wcm9wcyB9KSA9PiAoXG4gKiAgPENTU1RyYW5zaXRpb25cbiAqICAgIHsuLi5wcm9wc31cbiAqICAgIHRpbWVvdXQ9ezUwMH1cbiAqICAgIGNsYXNzTmFtZXM9XCJmYWRlXCJcbiAqICA+XG4gKiAgIHtjaGlsZHJlbn1cbiAqICA8L0NTU1RyYW5zaXRpb24+XG4gKiApO1xuICpcbiAqIGNsYXNzIEZhZGVJbkFuZE91dCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gKiAgIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcbiAqICAgICBzdXBlciguLi5hcmdzKTtcbiAqICAgICB0aGlzLnN0YXRlPSB7IHNob3c6IGZhbHNlIH1cbiAqXG4gKiAgICAgc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICogICAgICAgdGhpcy5zZXRTdGF0ZSh7IHNob3c6ICF0aGlzLnN0YXRlLnNob3cgfSlcbiAqICAgICB9LCA1MDAwKVxuICogICB9XG4gKiAgIHJlbmRlcigpIHtcbiAqICAgICByZXR1cm4gKFxuICogICAgICAgPEZhZGUgaW49e3RoaXMuc3RhdGUuc2hvd30+XG4gKiAgICAgICAgIDxkaXY+SGVsbG8gd29ybGQ8L2Rpdj5cbiAqICAgICAgIDwvRmFkZT5cbiAqICAgICApXG4gKiAgIH1cbiAqIH1cbiAqIGBgYFxuICpcbiAqIEFuZCB0aGUgY29vcnJlc3BvbmRpbmcgQ1NTIGZvciB0aGUgYDxGYWRlPmAgY29tcG9uZW50OlxuICpcbiAqIGBgYGNzc1xuICogLmZhZGUtZW50ZXIge1xuICogICBvcGFjaXR5OiAwLjAxO1xuICogfVxuICpcbiAqIC5mYWRlLWVudGVyLmZhZGUtZW50ZXItYWN0aXZlIHtcbiAqICAgb3BhY2l0eTogMTtcbiAqICAgdHJhbnNpdGlvbjogb3BhY2l0eSA1MDBtcyBlYXNlLWluO1xuICogfVxuICpcbiAqIC5mYWRlLWV4aXQge1xuICogICBvcGFjaXR5OiAxO1xuICogfVxuICpcbiAqIC5mYWRlLWV4aXQuZmFkZS1leGl0LWFjdGl2ZSB7XG4gKiAgIG9wYWNpdHk6IDAuMDE7XG4gKiAgIHRyYW5zaXRpb246IG9wYWNpdHkgMzAwbXMgZWFzZS1pbjtcbiAqIH1cbiAqIGBgYFxuICovXG5cbnZhciBDU1NUcmFuc2l0aW9uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKENTU1RyYW5zaXRpb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIENTU1RyYW5zaXRpb24oKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDU1NUcmFuc2l0aW9uKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5vbkVudGVyID0gZnVuY3Rpb24gKG5vZGUsIGFwcGVhcmluZykge1xuICAgICAgdmFyIF90aGlzJGdldENsYXNzTmFtZXMgPSBfdGhpcy5nZXRDbGFzc05hbWVzKGFwcGVhcmluZyA/ICdhcHBlYXInIDogJ2VudGVyJyksXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3RoaXMkZ2V0Q2xhc3NOYW1lcy5jbGFzc05hbWU7XG5cbiAgICAgIF90aGlzLnJlbW92ZUNsYXNzZXMobm9kZSwgJ2V4aXQnKTtcbiAgICAgIGFkZENsYXNzKG5vZGUsIGNsYXNzTmFtZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXIobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMub25FbnRlcmluZyA9IGZ1bmN0aW9uIChub2RlLCBhcHBlYXJpbmcpIHtcbiAgICAgIHZhciBfdGhpcyRnZXRDbGFzc05hbWVzMiA9IF90aGlzLmdldENsYXNzTmFtZXMoYXBwZWFyaW5nID8gJ2FwcGVhcicgOiAnZW50ZXInKSxcbiAgICAgICAgICBhY3RpdmVDbGFzc05hbWUgPSBfdGhpcyRnZXRDbGFzc05hbWVzMi5hY3RpdmVDbGFzc05hbWU7XG5cbiAgICAgIF90aGlzLnJlZmxvd0FuZEFkZENsYXNzKG5vZGUsIGFjdGl2ZUNsYXNzTmFtZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyaW5nKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJpbmcobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMub25FbnRlcmVkID0gZnVuY3Rpb24gKG5vZGUsIGFwcGVhcmluZykge1xuICAgICAgX3RoaXMucmVtb3ZlQ2xhc3Nlcyhub2RlLCBhcHBlYXJpbmcgPyAnYXBwZWFyJyA6ICdlbnRlcicpO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcmVkKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJlZChub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5vbkV4aXQgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgdmFyIF90aGlzJGdldENsYXNzTmFtZXMzID0gX3RoaXMuZ2V0Q2xhc3NOYW1lcygnZXhpdCcpLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF90aGlzJGdldENsYXNzTmFtZXMzLmNsYXNzTmFtZTtcblxuICAgICAgX3RoaXMucmVtb3ZlQ2xhc3Nlcyhub2RlLCAnYXBwZWFyJyk7XG4gICAgICBfdGhpcy5yZW1vdmVDbGFzc2VzKG5vZGUsICdlbnRlcicpO1xuICAgICAgYWRkQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRXhpdCkge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkV4aXQobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMub25FeGl0aW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRnZXRDbGFzc05hbWVzNCA9IF90aGlzLmdldENsYXNzTmFtZXMoJ2V4aXQnKSxcbiAgICAgICAgICBhY3RpdmVDbGFzc05hbWUgPSBfdGhpcyRnZXRDbGFzc05hbWVzNC5hY3RpdmVDbGFzc05hbWU7XG5cbiAgICAgIF90aGlzLnJlZmxvd0FuZEFkZENsYXNzKG5vZGUsIGFjdGl2ZUNsYXNzTmFtZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXRpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0aW5nKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLm9uRXhpdGVkID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIF90aGlzLnJlbW92ZUNsYXNzZXMobm9kZSwgJ2V4aXQnKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRXhpdGVkKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRXhpdGVkKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmdldENsYXNzTmFtZXMgPSBmdW5jdGlvbiAodHlwZSkge1xuICAgICAgdmFyIGNsYXNzTmFtZXMgPSBfdGhpcy5wcm9wcy5jbGFzc05hbWVzO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSB0eXBlb2YgY2xhc3NOYW1lcyAhPT0gJ3N0cmluZycgPyBjbGFzc05hbWVzW3R5cGVdIDogY2xhc3NOYW1lcyArICctJyArIHR5cGU7XG5cbiAgICAgIHZhciBhY3RpdmVDbGFzc05hbWUgPSB0eXBlb2YgY2xhc3NOYW1lcyAhPT0gJ3N0cmluZycgPyBjbGFzc05hbWVzW3R5cGUgKyAnQWN0aXZlJ10gOiBjbGFzc05hbWUgKyAnLWFjdGl2ZSc7XG5cbiAgICAgIHJldHVybiB7IGNsYXNzTmFtZTogY2xhc3NOYW1lLCBhY3RpdmVDbGFzc05hbWU6IGFjdGl2ZUNsYXNzTmFtZSB9O1xuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgQ1NTVHJhbnNpdGlvbi5wcm90b3R5cGUucmVtb3ZlQ2xhc3NlcyA9IGZ1bmN0aW9uIHJlbW92ZUNsYXNzZXMobm9kZSwgdHlwZSkge1xuICAgIHZhciBfZ2V0Q2xhc3NOYW1lcyA9IHRoaXMuZ2V0Q2xhc3NOYW1lcyh0eXBlKSxcbiAgICAgICAgY2xhc3NOYW1lID0gX2dldENsYXNzTmFtZXMuY2xhc3NOYW1lLFxuICAgICAgICBhY3RpdmVDbGFzc05hbWUgPSBfZ2V0Q2xhc3NOYW1lcy5hY3RpdmVDbGFzc05hbWU7XG5cbiAgICBjbGFzc05hbWUgJiYgcmVtb3ZlQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKTtcbiAgICBhY3RpdmVDbGFzc05hbWUgJiYgcmVtb3ZlQ2xhc3Mobm9kZSwgYWN0aXZlQ2xhc3NOYW1lKTtcbiAgfTtcblxuICBDU1NUcmFuc2l0aW9uLnByb3RvdHlwZS5yZWZsb3dBbmRBZGRDbGFzcyA9IGZ1bmN0aW9uIHJlZmxvd0FuZEFkZENsYXNzKG5vZGUsIGNsYXNzTmFtZSkge1xuICAgIC8vIFRoaXMgaXMgZm9yIHRvIGZvcmNlIGEgcmVwYWludCxcbiAgICAvLyB3aGljaCBpcyBuZWNlc3NhcnkgaW4gb3JkZXIgdG8gdHJhbnNpdGlvbiBzdHlsZXMgd2hlbiBhZGRpbmcgYSBjbGFzcyBuYW1lLlxuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXVudXNlZC1leHByZXNzaW9ucyAqL1xuICAgIG5vZGUuc2Nyb2xsVG9wO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tdW51c2VkLWV4cHJlc3Npb25zICovXG4gICAgYWRkQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKTtcbiAgfTtcblxuICBDU1NUcmFuc2l0aW9uLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIHByb3BzID0gX2V4dGVuZHMoe30sIHRoaXMucHJvcHMpO1xuXG4gICAgZGVsZXRlIHByb3BzLmNsYXNzTmFtZXM7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX1RyYW5zaXRpb24yLmRlZmF1bHQsIF9leHRlbmRzKHt9LCBwcm9wcywge1xuICAgICAgb25FbnRlcjogdGhpcy5vbkVudGVyLFxuICAgICAgb25FbnRlcmVkOiB0aGlzLm9uRW50ZXJlZCxcbiAgICAgIG9uRW50ZXJpbmc6IHRoaXMub25FbnRlcmluZyxcbiAgICAgIG9uRXhpdDogdGhpcy5vbkV4aXQsXG4gICAgICBvbkV4aXRpbmc6IHRoaXMub25FeGl0aW5nLFxuICAgICAgb25FeGl0ZWQ6IHRoaXMub25FeGl0ZWRcbiAgICB9KSk7XG4gIH07XG5cbiAgcmV0dXJuIENTU1RyYW5zaXRpb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5DU1NUcmFuc2l0aW9uLnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IHByb3BUeXBlcyA6IHt9O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDU1NUcmFuc2l0aW9uO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3QtdHJhbnNpdGlvbi1ncm91cC9DU1NUcmFuc2l0aW9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwL0NTU1RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gcmVxdWlyZSgnLi91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsJyk7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUZhY3RvcnkgPSBmdW5jdGlvbiBjcmVhdGVGYWN0b3J5KHR5cGUpIHtcbiAgdmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50ID0gKDAsIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Mi5kZWZhdWx0KSh0eXBlKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwLCBjKSB7XG4gICAgcmV0dXJuICgwLCBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIuZGVmYXVsdCkoZmFsc2UsIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwLCBjKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUZhY3Rvcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGdldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gZ2V0RGlzcGxheU5hbWUoQ29tcG9uZW50KSB7XG4gIGlmICh0eXBlb2YgQ29tcG9uZW50ID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBDb21wb25lbnQ7XG4gIH1cblxuICBpZiAoIUNvbXBvbmVudCkge1xuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgaXNDbGFzc0NvbXBvbmVudCA9IGZ1bmN0aW9uIGlzQ2xhc3NDb21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKENvbXBvbmVudCAmJiBDb21wb25lbnQucHJvdG90eXBlICYmIF90eXBlb2YoQ29tcG9uZW50LnByb3RvdHlwZS5pc1JlYWN0Q29tcG9uZW50KSA9PT0gJ29iamVjdCcpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNDbGFzc0NvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc0NsYXNzQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc0NsYXNzQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSBmdW5jdGlvbiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbih0eXBlb2YgQ29tcG9uZW50ID09PSAnZnVuY3Rpb24nICYmICEoMCwgX2lzQ2xhc3NDb21wb25lbnQyLmRlZmF1bHQpKENvbXBvbmVudCkgJiYgIUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgJiYgIUNvbXBvbmVudC5jb250ZXh0VHlwZXMgJiYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicgfHwgIUNvbXBvbmVudC5wcm9wVHlwZXMpKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5ID0gcmVxdWlyZSgnLi9jcmVhdGVFYWdlckZhY3RvcnknKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJGYWN0b3J5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSBmdW5jdGlvbiBjcmVhdGVFYWdlckVsZW1lbnRVdGlsKGhhc0tleSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHByb3BzLCBjaGlsZHJlbikge1xuICBpZiAoIWhhc0tleSAmJiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCkge1xuICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgcmV0dXJuIHR5cGUoX2V4dGVuZHMoe30sIHByb3BzLCB7IGNoaWxkcmVuOiBjaGlsZHJlbiB9KSk7XG4gICAgfVxuICAgIHJldHVybiB0eXBlKHByb3BzKTtcbiAgfVxuXG4gIHZhciBDb21wb25lbnQgPSB0eXBlO1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgIENvbXBvbmVudCxcbiAgICAgIHByb3BzLFxuICAgICAgY2hpbGRyZW5cbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgcHJvcHMpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRWFnZXJFbGVtZW50VXRpbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9nZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vZ2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9nZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciB3cmFwRGlzcGxheU5hbWUgPSBmdW5jdGlvbiB3cmFwRGlzcGxheU5hbWUoQmFzZUNvbXBvbmVudCwgaG9jTmFtZSkge1xuICByZXR1cm4gaG9jTmFtZSArICcoJyArICgwLCBfZ2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpICsgJyknO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd3JhcERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBTSUdOX09VVCB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy91c2Vycyc7XG5cbnR5cGUgQWN0aW9uID0ge1xuICB0eXBlOiBzdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCAoKTogQWN0aW9uID0+ICh7IHR5cGU6IFNJR05fT1VUIH0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL3VzZXJzL3NpZ25PdXQuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHdpdGhTdHlsZSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgeyBmYWRlIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL2NvbG9yTWFuaXB1bGF0b3InO1xuXG5pbXBvcnQgU2VhcmNoSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9TZWFyY2gnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgYm9yZGVyUmFkaXVzOiAnNnB4JyxcbiAgICBiYWNrZ3JvdW5kOiBmYWRlKHRoZW1lLnBhbGV0dGUuY29tbW9uLndoaXRlLCAwLjE1KSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMudXAoJ2xnJyldOiB7XG4gICAgICBiYWNrZ3JvdW5kOiBmYWRlKHRoZW1lLnBhbGV0dGUuY29tbW9uLndoaXRlLCAwLjE1KSxcbiAgICAgICcmOmhvdmVyJzoge1xuICAgICAgICBiYWNrZ3JvdW5kOiBmYWRlKHRoZW1lLnBhbGV0dGUuY29tbW9uLndoaXRlLCAwLjI1KSxcbiAgICAgIH0sXG4gICAgICAnJiAkaW5wdXQnOiB7XG4gICAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnd2lkdGgnKSxcbiAgICAgICAgd2lkdGg6IDE5MCxcbiAgICAgICAgJyY6Zm9jdXMnOiB7XG4gICAgICAgICAgd2lkdGg6IDI5MCxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbGcnKV06IHtcbiAgICAgIGJhY2tncm91bmQ6IGZhZGUodGhlbWUucGFsZXR0ZS5jb21tb24ud2hpdGUsIDAuMTUpLFxuICAgICAgJyY6aG92ZXInOiB7XG4gICAgICAgIGJhY2tncm91bmQ6IGZhZGUodGhlbWUucGFsZXR0ZS5jb21tb24ud2hpdGUsIDAuMjUpLFxuICAgICAgfSxcbiAgICAgICcmICRpbnB1dCc6IHtcbiAgICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCd3aWR0aCcpLFxuICAgICAgICB3aWR0aDogMTkwLFxuICAgICAgICAnJjpmb2N1cyc6IHtcbiAgICAgICAgICB3aWR0aDogMjkwLFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge1xuICAgICAgYmFja2dyb3VuZDogJ25vbmUnLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNnB4JyxcbiAgICAgICcmICRpbnB1dCc6IHtcbiAgICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCd3aWR0aCcpLFxuICAgICAgICB3aWR0aDogMCxcbiAgICAgICAgYm9yZGVyUmFkaXVzOiAnNnB4JyxcbiAgICAgICAgJyY6Zm9jdXMnOiB7XG4gICAgICAgICAgd2lkdGg6IDEzMCxcbiAgICAgICAgICBib3JkZXJSYWRpdXM6ICc2cHgnLFxuICAgICAgICAgIGJhY2tncm91bmQ6IGZhZGUodGhlbWUucGFsZXR0ZS5jb21tb24ud2hpdGUsIDAuMjUpLFxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9LFxuICB9LFxuICBzZWFyY2g6IHtcbiAgICB3aWR0aDogdGhlbWUuc3BhY2luZy51bml0ICogMTAsXG4gICAgaGVpZ2h0OiAnMTAwJScsXG4gICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgcG9pbnRlckV2ZW50czogJ25vbmUnLFxuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcicsXG4gIH0sXG4gIGlucHV0OiB7XG4gICAgZm9udDogJ2luaGVyaXQnLFxuICAgIHBhZGRpbmc6IGAke3RoZW1lLnNwYWNpbmcudW5pdH1weCAke3RoZW1lLnNwYWNpbmcudW5pdH1weCAke1xuICAgICAgdGhlbWUuc3BhY2luZy51bml0XG4gICAgfXB4ICR7dGhlbWUuc3BhY2luZy51bml0ICogOX1weGAsXG4gICAgYm9yZGVyOiAwLFxuICAgIGRpc3BsYXk6ICdibG9jaycsXG4gICAgdmVydGljYWxBbGlnbjogJ21pZGRsZScsXG4gICAgd2hpdGVTcGFjZTogJ25vcm1hbCcsXG4gICAgYmFja2dyb3VuZDogJ25vbmUnLFxuICAgIG1hcmdpbjogMCwgLy8gUmVzZXQgZm9yIFNhZmFyaVxuICAgIGNvbG9yOiAnaW5oZXJpdCcsXG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICAnJjpmb2N1cyc6IHtcbiAgICAgIG91dGxpbmU6IDAsXG4gICAgfSxcbiAgfSxcbn0pO1xuXG5jbGFzcyBTZWFyY2hCb3ggZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgdmFsdWU6ICcnLFxuICAgIH07XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgdmFsdWUgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnNlYXJjaH0+XG4gICAgICAgICAgPFNlYXJjaEljb24gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxpbnB1dFxuICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHRoaXMuc2V0U3RhdGUoeyB2YWx1ZTogZS50YXJnZXQudmFsdWUgfSl9XG4gICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgICAvPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5TZWFyY2hCb3gucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGUoc3R5bGVzKShTZWFyY2hCb3gpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0FwcEhlYWRlci9TZWFyY2guanMiLCIvKipcbiAqIFRoaXMgaXMgYSBIZWFkZXIgQ29tcG9uZW50XG4gKiBJdCB3aWxsIGJlIGRpc3BsYXllZCBvbiBIZWFkXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCB7IHdpdGhSb3V0ZXIgfSBmcm9tICdyZWFjdC1yb3V0ZXInO1xuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuaW1wb3J0IEFwcEJhciBmcm9tICdtYXRlcmlhbC11aS9BcHBCYXInO1xuaW1wb3J0IFRvb2xiYXIgZnJvbSAnbWF0ZXJpYWwtdWkvVG9vbGJhcic7XG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdtYXRlcmlhbC11aS9UeXBvZ3JhcGh5JztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IE1lbnUsIHsgTWVudUl0ZW0gfSBmcm9tICdtYXRlcmlhbC11aS9NZW51JztcblxuaW1wb3J0IE1vcmVWZXJ0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydCc7XG5pbXBvcnQgTWVudUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvTWVudSc7XG5cbmltcG9ydCBTZWFyY2ggZnJvbSAnLi9TZWFyY2gnO1xuXG5pbXBvcnQgYWN0aW9uU2lnbk91dCBmcm9tICcuLi8uLi9hY3Rpb25zL3VzZXJzL3NpZ25PdXQnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIC8vIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzcwMF0sXG4gICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs3MDBdKSxcbiAgfSxcbiAgcm9vdEhvbWU6IHtcbiAgICBib3hTaGFkb3c6ICdub25lJyxcbiAgICBiYWNrZ3JvdW5kOiAndHJhbnNwYXJlbnQnLFxuICB9LFxuICBtZW51QnV0dG9uOiB7XG4gICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs3MDBdKSxcbiAgfSxcbiAgdGl0bGU6IHtcbiAgICBmbGV4OiAnMCAxIGF1dG8nLFxuICB9LFxuICB0aXRsZUxpbms6IHtcbiAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzcwMF0pLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbiAgbW9yZVZlcnRCdXR0b246IHtcbiAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzcwMF0pLFxuICB9LFxufSk7XG5cbmNsYXNzIEFwcEhlYWRlciBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBhbmNob3JFbDogbnVsbCxcbiAgICAgIG9wZW46IGZhbHNlLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2xpY2sgPSB0aGlzLm9uQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uUmVxdWVzdENsb3NlID0gdGhpcy5vblJlcXVlc3RDbG9zZS5iaW5kKHRoaXMpO1xuICB9XG5cbiAgb25DbGljayhlKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBvcGVuOiB0cnVlLFxuICAgICAgYW5jaG9yRWw6IGUuY3VycmVudFRhcmdldCxcbiAgICB9KTtcbiAgfVxuXG4gIG9uUmVxdWVzdENsb3NlKCkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBvcGVuOiBmYWxzZSB9KTtcbiAgfVxuXG4gIG9uU2lnbk91dCA9ICgpID0+IHtcbiAgICB0aGlzLnByb3BzLmFjdGlvblNpZ25PdXQoKTtcbiAgICB0aGlzLnByb3BzLmhpc3RvcnkucHVzaCgnLycpO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzLCBvcGVuRHJhd2VyIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaXNTaWduZWRJbiB9ID0gZWxlbWVudHMudXNlcjtcbiAgICBjb25zdCB7IG9wZW4sIGFuY2hvckVsIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgcGF0aG5hbWUgfSA9IHRoaXMucHJvcHMubG9jYXRpb247XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEFwcEJhclxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzbmFtZXMoY2xhc3Nlcy5yb290LCB7XG4gICAgICAgICAgW2Ake2NsYXNzZXMucm9vdEhvbWV9YF06ICFpc1NpZ25lZEluICYmIHBhdGhuYW1lID09PSAnLycsXG4gICAgICAgIH0pfVxuICAgICAgPlxuICAgICAgICA8VG9vbGJhcj5cbiAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgYXJpYS1sYWJlbD1cIk1lbnVcIlxuICAgICAgICAgICAgb25DbGljaz17b3BlbkRyYXdlcn1cbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NuYW1lcyhjbGFzc2VzLm1lbnVCdXR0b24pfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxNZW51SWNvbiAvPlxuICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICA8VHlwb2dyYXBoeVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlfVxuICAgICAgICAgICAgdHlwZT1cInRpdGxlXCJcbiAgICAgICAgICAgIGNvbG9yPVwiaW5oZXJpdFwiXG4gICAgICAgICAgICBub1dyYXBcbiAgICAgICAgICA+XG4gICAgICAgICAgICB7ISghaXNTaWduZWRJbiAmJiBwYXRobmFtZSA9PT0gJy8nKSA/IChcbiAgICAgICAgICAgICAgPExpbmsgdG89eycvJ30gY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlTGlua30+XG4gICAgICAgICAgICAgICAgTWF5YXNoXG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICB7Lyoge2lzU2lnbmVkSW4gJiYgPFNlYXJjaCAvPn0gKi99XG4gICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJNb3JlXCJcbiAgICAgICAgICAgIGFyaWEtb3ducz17b3BlbiA/ICdsb25nLW1lbnUnIDogbnVsbH1cbiAgICAgICAgICAgIGFyaWEtaGFzcG9wdXA9XCJ0cnVlXCJcbiAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25DbGlja31cbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NuYW1lcyhjbGFzc2VzLm1vcmVWZXJ0QnV0dG9uKX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8TW9yZVZlcnRJY29uIC8+XG4gICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgIDxNZW51XG4gICAgICAgICAgICBpZD1cImxvbmctbWVudVwiXG4gICAgICAgICAgICBhbmNob3JFbD17YW5jaG9yRWx9XG4gICAgICAgICAgICBvcGVuPXtvcGVufVxuICAgICAgICAgICAgb25SZXF1ZXN0Q2xvc2U9e3RoaXMub25SZXF1ZXN0Q2xvc2V9XG4gICAgICAgICAgICBQYXBlclByb3BzPXt7XG4gICAgICAgICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgICAgICAgbWF4SGVpZ2h0OiAyNTAsXG4gICAgICAgICAgICAgICAgd2lkdGg6IDIwMCxcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgPlxuICAgICAgICAgICAge2lzU2lnbmVkSW4gPyAoXG4gICAgICAgICAgICAgIDxNZW51SXRlbSBvbkNsaWNrPXt0aGlzLm9uU2lnbk91dH0+U2lnbiBPdXQ8L01lbnVJdGVtPlxuICAgICAgICAgICAgKSA6IChcbiAgICAgICAgICAgICAgPE1lbnVJdGVtXG4gICAgICAgICAgICAgICAgYnV0dG9uXG4gICAgICAgICAgICAgICAgY29tcG9uZW50PXtMaW5rfVxuICAgICAgICAgICAgICAgIHRvPXsnL3NpZ24taW4nfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25SZXF1ZXN0Q2xvc2V9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICBTaWduIEluXG4gICAgICAgICAgICAgIDwvTWVudUl0ZW0+XG4gICAgICAgICAgICApfVxuICAgICAgICAgIDwvTWVudT5cbiAgICAgICAgPC9Ub29sYmFyPlxuICAgICAgPC9BcHBCYXI+XG4gICAgKTtcbiAgfVxufVxuXG5BcHBIZWFkZXIucHJvcFR5cGVzID0ge1xuICAvLyBtYXRjaDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBsb2NhdGlvbjogUHJvcFR5cGVzLnNoYXBlKHtcbiAgICBwYXRobmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB9KS5pc1JlcXVpcmVkLFxuICBoaXN0b3J5OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgb3BlbkRyYXdlcjogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgYWN0aW9uU2lnbk91dDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IGVsZW1lbnRzIH0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25TaWduT3V0LFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhSb3V0ZXIoXG4gIGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKHdpdGhTdHlsZXMoc3R5bGVzKShBcHBIZWFkZXIpKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQXBwSGVhZGVyL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==