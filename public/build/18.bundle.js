webpackJsonp([18],{

/***/ "./node_modules/babel-runtime/core-js/object/values.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/values.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/values */ "./node_modules/core-js/library/fn/object/values.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/values.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/values.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../modules/es7.object.values */ "./node_modules/core-js/library/modules/es7.object.values.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.values;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-to-array.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-to-array.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var isEnum = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) if (isEnum.call(O, key = keys[i++])) {
      result.push(isEntries ? [key, O[key]] : O[key]);
    } return result;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.object.values.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.object.values.js ***!
  \*******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var $values = __webpack_require__(/*! ./_object-to-array */ "./node_modules/core-js/library/modules/_object-to-array.js")(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ }),

/***/ "./src/client/containers/ElementPage/index.js":
/*!****************************************************!*\
  !*** ./src/client/containers/ElementPage/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AsyncUser = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(39).then(__webpack_require__.bind(null, /*! ./User */ "./src/client/containers/ElementPage/User.js"));
  },
  modules: ['./User'],
  loading: _Loading2.default
}); /** @format */

var AsyncCircleEdu = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(53).then(__webpack_require__.bind(null, /*! ./CircleEdu */ "./src/client/containers/ElementPage/CircleEdu.js"));
  },
  modules: ['./CircleEdu'],
  loading: _Loading2.default
});

var AsyncCircleOrg = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(51).then(__webpack_require__.bind(null, /*! ./CircleOrg */ "./src/client/containers/ElementPage/CircleOrg.js"));
  },
  modules: ['./CircleOrg'],
  loading: _Loading2.default
});

var AsyncCircleField = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(52).then(__webpack_require__.bind(null, /*! ./CircleField */ "./src/client/containers/ElementPage/CircleField.js"));
  },
  modules: ['./CircleField'],
  loading: _Loading2.default
});

var AsyncCircleLocation = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(50).then(__webpack_require__.bind(null, /*! ./CircleLocation */ "./src/client/containers/ElementPage/CircleLocation.js"));
  },
  modules: ['./CircleLocation'],
  loading: _Loading2.default
});

var AsyncErrorPage = (0, _reactLoadable2.default)({
  loader: function loader() {
    return new Promise(function(resolve) { resolve(); }).then(__webpack_require__.bind(null, /*! ../../components/ErrorPage */ "./src/client/components/ErrorPage.js"));
  },
  modules: ['../../components/ErrorPage'],
  loading: _Loading2.default
});

var ElementPage = function ElementPage(props) {
  var match = props.match,
      elements = props.elements;
  var username = match.params.username;


  var element = (0, _values2.default)(elements).find(function (e) {
    return typeof e.username !== 'undefined' && e.username === username;
  });

  if (typeof element === 'undefined') {
    return _react2.default.createElement(AsyncErrorPage, element);
  }

  var statusCode = element.statusCode,
      elementType = element.elementType,
      circleType = element.circleType;


  if (typeof statusCode !== 'number' || statusCode >= 300) {
    return _react2.default.createElement(AsyncErrorPage, element);
  }

  if (elementType === 'user') {
    return _react2.default.createElement(AsyncUser, (0, _extends3.default)({ element: element }, props));
  }

  if (elementType === 'circle' && circleType === 'edu') {
    return _react2.default.createElement(AsyncCircleEdu, (0, _extends3.default)({ element: element }, props));
  }

  if (elementType === 'circle' && circleType === 'org') {
    return _react2.default.createElement(AsyncCircleOrg, (0, _extends3.default)({ element: element }, props));
  }

  if (elementType === 'circle' && circleType === 'field') {
    return _react2.default.createElement(AsyncCircleField, (0, _extends3.default)({ element: element }, props));
  }

  if (elementType === 'circle' && circleType === 'location') {
    return _react2.default.createElement(AsyncCircleLocation, (0, _extends3.default)({ element: element }, props));
  }

  // all posible errors are handled above, still something goes odd, this error
  // page will be rendered.
  return _react2.default.createElement(AsyncErrorPage, element);
};

ElementPage.propTypes = {
  match: _ReactPropTypes2.default.shape({
    params: _ReactPropTypes2.default.shape({
      username: _ReactPropTypes2.default.string.isRequired
    }).isRequired
  }).isRequired,

  elements: _ReactPropTypes2.default.object
};

var mapStateToProps = function mapStateToProps(_ref) {
  var elements = _ref.elements;
  return { elements: elements };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)(ElementPage);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC92YWx1ZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczcub2JqZWN0LnZhbHVlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRWxlbWVudFBhZ2UvaW5kZXguanMiXSwibmFtZXMiOlsiQXN5bmNVc2VyIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBc3luY0NpcmNsZUVkdSIsIkFzeW5jQ2lyY2xlT3JnIiwiQXN5bmNDaXJjbGVGaWVsZCIsIkFzeW5jQ2lyY2xlTG9jYXRpb24iLCJBc3luY0Vycm9yUGFnZSIsIkVsZW1lbnRQYWdlIiwicHJvcHMiLCJtYXRjaCIsImVsZW1lbnRzIiwidXNlcm5hbWUiLCJwYXJhbXMiLCJlbGVtZW50IiwiZmluZCIsImUiLCJzdGF0dXNDb2RlIiwiZWxlbWVudFR5cGUiLCJjaXJjbGVUeXBlIiwicHJvcFR5cGVzIiwic2hhcGUiLCJzdHJpbmciLCJpc1JlcXVpcmVkIiwib2JqZWN0IiwibWFwU3RhdGVUb1Byb3BzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsa0JBQWtCLGtKOzs7Ozs7Ozs7Ozs7QUNBbEI7QUFDQTs7Ozs7Ozs7Ozs7OztBQ0RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNmQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTkQ7Ozs7QUFDQTs7OztBQUVBOztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1BLFlBQVksNkJBQVM7QUFDekJDLFVBQVE7QUFBQSxXQUFNLHlJQUFOO0FBQUEsR0FEaUI7QUFFekJDLFdBQVMsQ0FBQyxRQUFELENBRmdCO0FBR3pCQztBQUh5QixDQUFULENBQWxCLEMsQ0FWQTs7QUFnQkEsSUFBTUMsaUJBQWlCLDZCQUFTO0FBQzlCSCxVQUFRO0FBQUEsV0FBTSxtSkFBTjtBQUFBLEdBRHNCO0FBRTlCQyxXQUFTLENBQUMsYUFBRCxDQUZxQjtBQUc5QkM7QUFIOEIsQ0FBVCxDQUF2Qjs7QUFNQSxJQUFNRSxpQkFBaUIsNkJBQVM7QUFDOUJKLFVBQVE7QUFBQSxXQUFNLG1KQUFOO0FBQUEsR0FEc0I7QUFFOUJDLFdBQVMsQ0FBQyxhQUFELENBRnFCO0FBRzlCQztBQUg4QixDQUFULENBQXZCOztBQU1BLElBQU1HLG1CQUFtQiw2QkFBUztBQUNoQ0wsVUFBUTtBQUFBLFdBQU0sdUpBQU47QUFBQSxHQUR3QjtBQUVoQ0MsV0FBUyxDQUFDLGVBQUQsQ0FGdUI7QUFHaENDO0FBSGdDLENBQVQsQ0FBekI7O0FBTUEsSUFBTUksc0JBQXNCLDZCQUFTO0FBQ25DTixVQUFRO0FBQUEsV0FBTSw2SkFBTjtBQUFBLEdBRDJCO0FBRW5DQyxXQUFTLENBQUMsa0JBQUQsQ0FGMEI7QUFHbkNDO0FBSG1DLENBQVQsQ0FBNUI7O0FBTUEsSUFBTUssaUJBQWlCLDZCQUFTO0FBQzlCUCxVQUFRO0FBQUEsV0FBTSw0SkFBTjtBQUFBLEdBRHNCO0FBRTlCQyxXQUFTLENBQUMsNEJBQUQsQ0FGcUI7QUFHOUJDO0FBSDhCLENBQVQsQ0FBdkI7O0FBTUEsSUFBTU0sY0FBYyxTQUFkQSxXQUFjLENBQUNDLEtBQUQsRUFBVztBQUFBLE1BQ3JCQyxLQURxQixHQUNERCxLQURDLENBQ3JCQyxLQURxQjtBQUFBLE1BQ2RDLFFBRGMsR0FDREYsS0FEQyxDQUNkRSxRQURjO0FBQUEsTUFFckJDLFFBRnFCLEdBRVJGLE1BQU1HLE1BRkUsQ0FFckJELFFBRnFCOzs7QUFJN0IsTUFBTUUsVUFBVSxzQkFBY0gsUUFBZCxFQUF3QkksSUFBeEIsQ0FDZCxVQUFDQyxDQUFEO0FBQUEsV0FBTyxPQUFPQSxFQUFFSixRQUFULEtBQXNCLFdBQXRCLElBQXFDSSxFQUFFSixRQUFGLEtBQWVBLFFBQTNEO0FBQUEsR0FEYyxDQUFoQjs7QUFJQSxNQUFJLE9BQU9FLE9BQVAsS0FBbUIsV0FBdkIsRUFBb0M7QUFDbEMsV0FBTyw4QkFBQyxjQUFELEVBQW9CQSxPQUFwQixDQUFQO0FBQ0Q7O0FBVjRCLE1BWXJCRyxVQVpxQixHQVltQkgsT0FabkIsQ0FZckJHLFVBWnFCO0FBQUEsTUFZVEMsV0FaUyxHQVltQkosT0FabkIsQ0FZVEksV0FaUztBQUFBLE1BWUlDLFVBWkosR0FZbUJMLE9BWm5CLENBWUlLLFVBWko7OztBQWM3QixNQUFJLE9BQU9GLFVBQVAsS0FBc0IsUUFBdEIsSUFBa0NBLGNBQWMsR0FBcEQsRUFBeUQ7QUFDdkQsV0FBTyw4QkFBQyxjQUFELEVBQW9CSCxPQUFwQixDQUFQO0FBQ0Q7O0FBRUQsTUFBSUksZ0JBQWdCLE1BQXBCLEVBQTRCO0FBQzFCLFdBQU8sOEJBQUMsU0FBRCwyQkFBVyxTQUFTSixPQUFwQixJQUFpQ0wsS0FBakMsRUFBUDtBQUNEOztBQUVELE1BQUlTLGdCQUFnQixRQUFoQixJQUE0QkMsZUFBZSxLQUEvQyxFQUFzRDtBQUNwRCxXQUFPLDhCQUFDLGNBQUQsMkJBQWdCLFNBQVNMLE9BQXpCLElBQXNDTCxLQUF0QyxFQUFQO0FBQ0Q7O0FBRUQsTUFBSVMsZ0JBQWdCLFFBQWhCLElBQTRCQyxlQUFlLEtBQS9DLEVBQXNEO0FBQ3BELFdBQU8sOEJBQUMsY0FBRCwyQkFBZ0IsU0FBU0wsT0FBekIsSUFBc0NMLEtBQXRDLEVBQVA7QUFDRDs7QUFFRCxNQUFJUyxnQkFBZ0IsUUFBaEIsSUFBNEJDLGVBQWUsT0FBL0MsRUFBd0Q7QUFDdEQsV0FBTyw4QkFBQyxnQkFBRCwyQkFBa0IsU0FBU0wsT0FBM0IsSUFBd0NMLEtBQXhDLEVBQVA7QUFDRDs7QUFFRCxNQUFJUyxnQkFBZ0IsUUFBaEIsSUFBNEJDLGVBQWUsVUFBL0MsRUFBMkQ7QUFDekQsV0FBTyw4QkFBQyxtQkFBRCwyQkFBcUIsU0FBU0wsT0FBOUIsSUFBMkNMLEtBQTNDLEVBQVA7QUFDRDs7QUFFRDtBQUNBO0FBQ0EsU0FBTyw4QkFBQyxjQUFELEVBQW9CSyxPQUFwQixDQUFQO0FBQ0QsQ0F6Q0Q7O0FBMkNBTixZQUFZWSxTQUFaLEdBQXdCO0FBQ3RCVixTQUFPLHlCQUFVVyxLQUFWLENBQWdCO0FBQ3JCUixZQUFRLHlCQUFVUSxLQUFWLENBQWdCO0FBQ3RCVCxnQkFBVSx5QkFBVVUsTUFBVixDQUFpQkM7QUFETCxLQUFoQixFQUVMQTtBQUhrQixHQUFoQixFQUlKQSxVQUxtQjs7QUFPdEJaLFlBQVUseUJBQVVhO0FBUEUsQ0FBeEI7O0FBVUEsSUFBTUMsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdkLFFBQUgsUUFBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7O2tCQUVlLHlCQUFRYyxlQUFSLEVBQXlCakIsV0FBekIsQyIsImZpbGUiOiIxOC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L3ZhbHVlc1wiKSwgX19lc01vZHVsZTogdHJ1ZSB9O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvdmFsdWVzLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L3ZhbHVlcy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMTggNjAgNjUiLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3QudmFsdWVzO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC92YWx1ZXMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsInZhciBnZXRLZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMnKTtcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgaXNFbnVtID0gcmVxdWlyZSgnLi9fb2JqZWN0LXBpZScpLmY7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpc0VudHJpZXMpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChpdCkge1xuICAgIHZhciBPID0gdG9JT2JqZWN0KGl0KTtcbiAgICB2YXIga2V5cyA9IGdldEtleXMoTyk7XG4gICAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIHZhciBpID0gMDtcbiAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgdmFyIGtleTtcbiAgICB3aGlsZSAobGVuZ3RoID4gaSkgaWYgKGlzRW51bS5jYWxsKE8sIGtleSA9IGtleXNbaSsrXSkpIHtcbiAgICAgIHJlc3VsdC5wdXNoKGlzRW50cmllcyA/IFtrZXksIE9ba2V5XV0gOiBPW2tleV0pO1xuICAgIH0gcmV0dXJuIHJlc3VsdDtcbiAgfTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS90YzM5L3Byb3Bvc2FsLW9iamVjdC12YWx1ZXMtZW50cmllc1xudmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbnZhciAkdmFsdWVzID0gcmVxdWlyZSgnLi9fb2JqZWN0LXRvLWFycmF5JykoZmFsc2UpO1xuXG4kZXhwb3J0KCRleHBvcnQuUywgJ09iamVjdCcsIHtcbiAgdmFsdWVzOiBmdW5jdGlvbiB2YWx1ZXMoaXQpIHtcbiAgICByZXR1cm4gJHZhbHVlcyhpdCk7XG4gIH1cbn0pO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM3Lm9iamVjdC52YWx1ZXMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5cbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgTG9hZGFibGUgZnJvbSAncmVhY3QtbG9hZGFibGUnO1xuXG5pbXBvcnQgTG9hZGluZyBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xvYWRpbmcnO1xuXG5jb25zdCBBc3luY1VzZXIgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuL1VzZXInKSxcbiAgbW9kdWxlczogWycuL1VzZXInXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0NpcmNsZUVkdSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vQ2lyY2xlRWR1JyksXG4gIG1vZHVsZXM6IFsnLi9DaXJjbGVFZHUnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0NpcmNsZU9yZyA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vQ2lyY2xlT3JnJyksXG4gIG1vZHVsZXM6IFsnLi9DaXJjbGVPcmcnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0NpcmNsZUZpZWxkID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9DaXJjbGVGaWVsZCcpLFxuICBtb2R1bGVzOiBbJy4vQ2lyY2xlRmllbGQnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0NpcmNsZUxvY2F0aW9uID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9DaXJjbGVMb2NhdGlvbicpLFxuICBtb2R1bGVzOiBbJy4vQ2lyY2xlTG9jYXRpb24nXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0Vycm9yUGFnZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4uLy4uL2NvbXBvbmVudHMvRXJyb3JQYWdlJyksXG4gIG1vZHVsZXM6IFsnLi4vLi4vY29tcG9uZW50cy9FcnJvclBhZ2UnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBFbGVtZW50UGFnZSA9IChwcm9wcykgPT4ge1xuICBjb25zdCB7IG1hdGNoLCBlbGVtZW50cyB9ID0gcHJvcHM7XG4gIGNvbnN0IHsgdXNlcm5hbWUgfSA9IG1hdGNoLnBhcmFtcztcblxuICBjb25zdCBlbGVtZW50ID0gT2JqZWN0LnZhbHVlcyhlbGVtZW50cykuZmluZChcbiAgICAoZSkgPT4gdHlwZW9mIGUudXNlcm5hbWUgIT09ICd1bmRlZmluZWQnICYmIGUudXNlcm5hbWUgPT09IHVzZXJuYW1lLFxuICApO1xuXG4gIGlmICh0eXBlb2YgZWxlbWVudCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICByZXR1cm4gPEFzeW5jRXJyb3JQYWdlIHsuLi5lbGVtZW50fSAvPjtcbiAgfVxuXG4gIGNvbnN0IHsgc3RhdHVzQ29kZSwgZWxlbWVudFR5cGUsIGNpcmNsZVR5cGUgfSA9IGVsZW1lbnQ7XG5cbiAgaWYgKHR5cGVvZiBzdGF0dXNDb2RlICE9PSAnbnVtYmVyJyB8fCBzdGF0dXNDb2RlID49IDMwMCkge1xuICAgIHJldHVybiA8QXN5bmNFcnJvclBhZ2Ugey4uLmVsZW1lbnR9IC8+O1xuICB9XG5cbiAgaWYgKGVsZW1lbnRUeXBlID09PSAndXNlcicpIHtcbiAgICByZXR1cm4gPEFzeW5jVXNlciBlbGVtZW50PXtlbGVtZW50fSB7Li4ucHJvcHN9IC8+O1xuICB9XG5cbiAgaWYgKGVsZW1lbnRUeXBlID09PSAnY2lyY2xlJyAmJiBjaXJjbGVUeXBlID09PSAnZWR1Jykge1xuICAgIHJldHVybiA8QXN5bmNDaXJjbGVFZHUgZWxlbWVudD17ZWxlbWVudH0gey4uLnByb3BzfSAvPjtcbiAgfVxuXG4gIGlmIChlbGVtZW50VHlwZSA9PT0gJ2NpcmNsZScgJiYgY2lyY2xlVHlwZSA9PT0gJ29yZycpIHtcbiAgICByZXR1cm4gPEFzeW5jQ2lyY2xlT3JnIGVsZW1lbnQ9e2VsZW1lbnR9IHsuLi5wcm9wc30gLz47XG4gIH1cblxuICBpZiAoZWxlbWVudFR5cGUgPT09ICdjaXJjbGUnICYmIGNpcmNsZVR5cGUgPT09ICdmaWVsZCcpIHtcbiAgICByZXR1cm4gPEFzeW5jQ2lyY2xlRmllbGQgZWxlbWVudD17ZWxlbWVudH0gey4uLnByb3BzfSAvPjtcbiAgfVxuXG4gIGlmIChlbGVtZW50VHlwZSA9PT0gJ2NpcmNsZScgJiYgY2lyY2xlVHlwZSA9PT0gJ2xvY2F0aW9uJykge1xuICAgIHJldHVybiA8QXN5bmNDaXJjbGVMb2NhdGlvbiBlbGVtZW50PXtlbGVtZW50fSB7Li4ucHJvcHN9IC8+O1xuICB9XG5cbiAgLy8gYWxsIHBvc2libGUgZXJyb3JzIGFyZSBoYW5kbGVkIGFib3ZlLCBzdGlsbCBzb21ldGhpbmcgZ29lcyBvZGQsIHRoaXMgZXJyb3JcbiAgLy8gcGFnZSB3aWxsIGJlIHJlbmRlcmVkLlxuICByZXR1cm4gPEFzeW5jRXJyb3JQYWdlIHsuLi5lbGVtZW50fSAvPjtcbn07XG5cbkVsZW1lbnRQYWdlLnByb3BUeXBlcyA9IHtcbiAgbWF0Y2g6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgcGFyYW1zOiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgdXNlcm5hbWU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICB9KS5pc1JlcXVpcmVkLFxuICB9KS5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzKShFbGVtZW50UGFnZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRWxlbWVudFBhZ2UvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9