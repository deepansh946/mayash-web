webpackJsonp([20],{

/***/ "./src/client/containers/PostPage/index.js":
/*!*************************************************!*\
  !*** ./src/client/containers/PostPage/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AsyncArticle = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(34).then(__webpack_require__.bind(null, /*! ./Article */ "./src/client/containers/PostPage/Article.js"));
  },
  modules: ['./Article'],
  loading: _Loading2.default
}); /**
     * For displaying Article with Title, Description & content
     *
     * @format
     */

var AsyncErrorPage = (0, _reactLoadable2.default)({
  loader: function loader() {
    return new Promise(function(resolve) { resolve(); }).then(__webpack_require__.bind(null, /*! ../../components/ErrorPage */ "./src/client/components/ErrorPage.js"));
  },
  modules: ['../../components/ErrorPage'],
  loading: _Loading2.default
});

var PostPage = function (_Component) {
  (0, _inherits3.default)(PostPage, _Component);

  function PostPage() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, PostPage);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = PostPage.__proto__ || (0, _getPrototypeOf2.default)(PostPage)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(PostPage, [{
    key: 'render',


    // shouldComponentUpdate(nextProps, nextState) {
    //   return this.state !== nextState && this.props !== nextProps;
    // }

    value: function render() {
      var _props = this.props,
          match = _props.match,
          posts = _props.posts;
      var postId = match.params.postId;

      var post = posts[postId];
      var statusCode = post.statusCode;

      // if post is not found or any other error occurs.

      if (!post || typeof statusCode !== 'number' || statusCode >= 300) {
        return _react2.default.createElement(AsyncErrorPage, post);
      }

      return _react2.default.createElement(AsyncArticle, (0, _extends3.default)({ post: post }, this.props));
    }
  }]);
  return PostPage;
}(_react.Component);

PostPage.propTypes = {
  match: _propTypes2.default.shape({
    params: _propTypes2.default.shape({
      postId: _propTypes2.default.string.isRequired // This should be number.
    }).isRequired
  }).isRequired,

  posts: _propTypes2.default.object.isRequired
};

var mapStateToProps = function mapStateToProps(_ref2) {
  var posts = _ref2.posts;
  return { posts: posts };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)(PostPage);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdFBhZ2UvaW5kZXguanMiXSwibmFtZXMiOlsiQXN5bmNBcnRpY2xlIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBc3luY0Vycm9yUGFnZSIsIlBvc3RQYWdlIiwic3RhdGUiLCJwcm9wcyIsIm1hdGNoIiwicG9zdHMiLCJwb3N0SWQiLCJwYXJhbXMiLCJwb3N0Iiwic3RhdHVzQ29kZSIsInByb3BUeXBlcyIsInNoYXBlIiwic3RyaW5nIiwiaXNSZXF1aXJlZCIsIm9iamVjdCIsIm1hcFN0YXRlVG9Qcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFNQTs7OztBQUNBOzs7O0FBRUE7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsZUFBZSw2QkFBUztBQUM1QkMsVUFBUTtBQUFBLFdBQU0sNElBQU47QUFBQSxHQURvQjtBQUU1QkMsV0FBUyxDQUFDLFdBQUQsQ0FGbUI7QUFHNUJDO0FBSDRCLENBQVQsQ0FBckIsQyxDQWRBOzs7Ozs7QUFvQkEsSUFBTUMsaUJBQWlCLDZCQUFTO0FBQzlCSCxVQUFRO0FBQUEsV0FBTSw0SkFBTjtBQUFBLEdBRHNCO0FBRTlCQyxXQUFTLENBQUMsNEJBQUQsQ0FGcUI7QUFHOUJDO0FBSDhCLENBQVQsQ0FBdkI7O0lBTU1FLFE7Ozs7Ozs7Ozs7Ozs7O2dOQUNKQyxLLEdBQVEsRTs7Ozs7OztBQUVSO0FBQ0E7QUFDQTs7NkJBRVM7QUFBQSxtQkFDa0IsS0FBS0MsS0FEdkI7QUFBQSxVQUNDQyxLQURELFVBQ0NBLEtBREQ7QUFBQSxVQUNRQyxLQURSLFVBQ1FBLEtBRFI7QUFBQSxVQUVDQyxNQUZELEdBRVlGLE1BQU1HLE1BRmxCLENBRUNELE1BRkQ7O0FBR1AsVUFBTUUsT0FBT0gsTUFBTUMsTUFBTixDQUFiO0FBSE8sVUFJQ0csVUFKRCxHQUlnQkQsSUFKaEIsQ0FJQ0MsVUFKRDs7QUFNUDs7QUFDQSxVQUFJLENBQUNELElBQUQsSUFBUyxPQUFPQyxVQUFQLEtBQXNCLFFBQS9CLElBQTJDQSxjQUFjLEdBQTdELEVBQWtFO0FBQ2hFLGVBQU8sOEJBQUMsY0FBRCxFQUFvQkQsSUFBcEIsQ0FBUDtBQUNEOztBQUVELGFBQU8sOEJBQUMsWUFBRCwyQkFBYyxNQUFNQSxJQUFwQixJQUE4QixLQUFLTCxLQUFuQyxFQUFQO0FBQ0Q7Ozs7O0FBR0hGLFNBQVNTLFNBQVQsR0FBcUI7QUFDbkJOLFNBQU8sb0JBQVVPLEtBQVYsQ0FBZ0I7QUFDckJKLFlBQVEsb0JBQVVJLEtBQVYsQ0FBZ0I7QUFDdEJMLGNBQVEsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBREgsQ0FDZTtBQURmLEtBQWhCLEVBRUxBO0FBSGtCLEdBQWhCLEVBSUpBLFVBTGdCOztBQU9uQlIsU0FBTyxvQkFBVVMsTUFBVixDQUFpQkQ7QUFQTCxDQUFyQjs7QUFVQSxJQUFNRSxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR1YsS0FBSCxTQUFHQSxLQUFIO0FBQUEsU0FBZ0IsRUFBRUEsWUFBRixFQUFoQjtBQUFBLENBQXhCOztrQkFFZSx5QkFBUVUsZUFBUixFQUF5QmQsUUFBekIsQyIsImZpbGUiOiIyMC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEZvciBkaXNwbGF5aW5nIEFydGljbGUgd2l0aCBUaXRsZSwgRGVzY3JpcHRpb24gJiBjb250ZW50XG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IExvYWRhYmxlIGZyb20gJ3JlYWN0LWxvYWRhYmxlJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuY29uc3QgQXN5bmNBcnRpY2xlID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9BcnRpY2xlJyksXG4gIG1vZHVsZXM6IFsnLi9BcnRpY2xlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNFcnJvclBhZ2UgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuLi8uLi9jb21wb25lbnRzL0Vycm9yUGFnZScpLFxuICBtb2R1bGVzOiBbJy4uLy4uL2NvbXBvbmVudHMvRXJyb3JQYWdlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY2xhc3MgUG9zdFBhZ2UgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0ZSA9IHt9O1xuXG4gIC8vIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMsIG5leHRTdGF0ZSkge1xuICAvLyAgIHJldHVybiB0aGlzLnN0YXRlICE9PSBuZXh0U3RhdGUgJiYgdGhpcy5wcm9wcyAhPT0gbmV4dFByb3BzO1xuICAvLyB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgbWF0Y2gsIHBvc3RzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgcG9zdElkIH0gPSBtYXRjaC5wYXJhbXM7XG4gICAgY29uc3QgcG9zdCA9IHBvc3RzW3Bvc3RJZF07XG4gICAgY29uc3QgeyBzdGF0dXNDb2RlIH0gPSBwb3N0O1xuXG4gICAgLy8gaWYgcG9zdCBpcyBub3QgZm91bmQgb3IgYW55IG90aGVyIGVycm9yIG9jY3Vycy5cbiAgICBpZiAoIXBvc3QgfHwgdHlwZW9mIHN0YXR1c0NvZGUgIT09ICdudW1iZXInIHx8IHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICByZXR1cm4gPEFzeW5jRXJyb3JQYWdlIHsuLi5wb3N0fSAvPjtcbiAgICB9XG5cbiAgICByZXR1cm4gPEFzeW5jQXJ0aWNsZSBwb3N0PXtwb3N0fSB7Li4udGhpcy5wcm9wc30gLz47XG4gIH1cbn1cblxuUG9zdFBhZ2UucHJvcFR5cGVzID0ge1xuICBtYXRjaDogUHJvcFR5cGVzLnNoYXBlKHtcbiAgICBwYXJhbXM6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgICBwb3N0SWQ6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCwgLy8gVGhpcyBzaG91bGQgYmUgbnVtYmVyLlxuICAgIH0pLmlzUmVxdWlyZWQsXG4gIH0pLmlzUmVxdWlyZWQsXG5cbiAgcG9zdHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IHBvc3RzIH0pID0+ICh7IHBvc3RzIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcykoUG9zdFBhZ2UpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Bvc3RQYWdlL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==