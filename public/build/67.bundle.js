webpackJsonp([67],{

/***/ "./src/client/api/users/update.js":
/*!****************************************!*\
  !*** ./src/client/api/users/update.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.token -
 * @param {string} payload.name -
 * @param {Object} payload.avatar -
 * @param {Object} payload.cover -
 * @param {Object} payload.resume -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var id = _ref2.id,
        token = _ref2.token,
        name = _ref2.name,
        avatar = _ref2.avatar,
        cover = _ref2.cover,
        resume = _ref2.resume;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ name: name, avatar: avatar, cover: cover, resume: resume })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/Form/index.js":
/*!*********************************************!*\
  !*** ./src/client/containers/Form/index.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This is the form Component
 * Generaly it will be used creating posts and modules
 *
 * @format
 */

var styles = {
  root: {
    flex: '1 0 100%'
  },
  card: {
    borderRadius: '8px'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

var Form = function (_Component) {
  (0, _inherits3.default)(Form, _Component);

  function Form(props) {
    (0, _classCallCheck3.default)(this, Form);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Form.__proto__ || (0, _getPrototypeOf2.default)(Form)).call(this, props));

    _this.state = {
      value: props.value,
      active: false,
      focus: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onClick = _this.onClick.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Form, [{
    key: 'onChange',
    value: function onChange(e) {
      this.setState({ value: e.target.value });
    }
  }, {
    key: 'onClick',
    value: function onClick() {
      this.props.onSubmit(this.props.keyName, this.state.value);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _state = this.state,
          value = _state.value,
          focus = _state.focus;
      var _props = this.props,
          classes = _props.classes,
          title = _props.title,
          placeholder = _props.placeholder,
          disabled = _props.disabled;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: function onMouseEnter() {
            return _this2.setState({ hover: true });
          },
          onMouseLeave: function onMouseLeave() {
            return _this2.setState({ hover: false });
          }
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(
            _Card.CardContent,
            {
              onFocus: function onFocus() {
                return _this2.setState({ focus: true });
              },
              onBlur: function onBlur() {
                return _this2.setState({
                  focus: !(_this2.props.value === value)
                });
              }
            },
            title + ': ',
            _react2.default.createElement(_Input2.default, {
              value: value,
              onChange: this.onChange,
              placeholder: placeholder,
              disabled: disabled
            })
          ),
          focus && !(disabled === true) ? _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onClick },
              'Submit'
            )
          ) : null
        )
      );
    }
  }]);
  return Form;
}(_react.Component);

/**
 * This will validate all the props comming to the component
 */


Form.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  keyName: _propTypes2.default.string,
  title: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  onSubmit: _propTypes2.default.func,
  placeholder: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

// Default props for the component
Form.defaultProp = {
  disabled: false,
  placeholder: 'Input'
};

exports.default = (0, _styles.withStyles)(styles)(Form);

/***/ }),

/***/ "./src/client/containers/ProfilePage/Profile.js":
/*!******************************************************!*\
  !*** ./src/client/containers/ProfilePage/Profile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Form = __webpack_require__(/*! ../Form */ "./src/client/containers/Form/index.js");

var _Form2 = _interopRequireDefault(_Form);

var _update = __webpack_require__(/*! ../../api/users/update */ "./src/client/api/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {},
  gridItem: {
    paddingBottom: '1%'
  },
  form: {
    paddingTop: '1%'
  }
}; /** @format */

var Profile = function (_Component) {
  (0, _inherits3.default)(Profile, _Component);

  function Profile(props) {
    (0, _classCallCheck3.default)(this, Profile);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Profile.__proto__ || (0, _getPrototypeOf2.default)(Profile)).call(this, props));

    var _props$user = props.user,
        id = _props$user.id,
        username = _props$user.username,
        name = _props$user.name,
        description = _props$user.description;


    _this.state = { id: id, username: username, name: name, description: description, message: '' };

    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Profile, [{
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      var _nextProps$user = nextProps.user,
          name = _nextProps$user.name,
          username = _nextProps$user.username,
          description = _nextProps$user.description;

      this.setState({ name: name, username: username, description: description });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(key, value) {
        var _props$user2, id, token, _ref2, statusCode, message, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props$user2 = this.props.user, id = _props$user2.id, token = _props$user2.token;
                _context.next = 4;
                return (0, _update2.default)({
                  id: id,
                  token: token,
                  name: value
                });

              case 4:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                message = _ref2.message;
                error = _ref2.error;

                if (!(statusCode >= 300)) {
                  _context.next = 11;
                  break;
                }

                this.setState({
                  message: 'Your request failed due to some technical problem'
                });
                return _context.abrupt('return');

              case 11:

                this.setState({ message: message });

                this.props.userUpdate({ id: id, name: value });
                _context.next = 18;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context['catch'](0);

                console.log(_context.t0);

              case 18:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 15]]);
      }));

      function onSubmit(_x, _x2) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var _state = this.state,
          id = _state.id,
          name = _state.name,
          message = _state.message;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 6,
            xl: 6,
            className: classes.gridItem
          },
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, { title: 'UserId', value: id, disabled: true })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Name',
              keyName: (0, _keys2.default)({ name: name })[0],
              value: name,
              placeholder: 'Name',
              onSubmit: this.onSubmit
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Bio',
              placeholder: 'Bio Description',
              value: '',
              disabled: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Linkedin',
              placeholder: 'Linkedin URL',
              value: '',
              disabled: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Facebook',
              placeholder: 'Facebook URL',
              value: '',
              disabled: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Google Plus',
              placeholder: 'Google Plus URL',
              value: '',
              disabled: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Instagram',
              placeholder: 'Instagram URL',
              value: '',
              disabled: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Twitter',
              placeholder: 'Twitter URL',
              value: '',
              disabled: true
            })
          ),
          _react2.default.createElement(
            'div',
            { className: classes.form },
            _react2.default.createElement(_Form2.default, {
              title: 'Skype',
              placeholder: 'Skype Username',
              value: '',
              disabled: true
            })
          )
        )
      );
    }
  }]);
  return Profile;
}(_react.Component);

Profile.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    token: _propTypes2.default.string.isRequired,
    username: _propTypes2.default.string,
    name: _propTypes2.default.string
  }).isRequired,
  userUpdate: _propTypes2.default.func.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Profile);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS91c2Vycy91cGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Gb3JtL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Qcm9maWxlUGFnZS9Qcm9maWxlLmpzIl0sIm5hbWVzIjpbImlkIiwidG9rZW4iLCJuYW1lIiwiYXZhdGFyIiwiY292ZXIiLCJyZXN1bWUiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsImJvZHkiLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJ1cGRhdGUiLCJzdHlsZXMiLCJyb290IiwiZGlzcGxheSIsImlucHV0IiwiZmxleEdyb3ciLCJib3JkZXIiLCJib3JkZXJSYWRpdXMiLCJwYWRkaW5nIiwib3V0bGluZSIsImJvcmRlckNvbG9yIiwiYm94U2hhZG93IiwiSW5wdXQiLCJjbGFzc2VzIiwib25DaGFuZ2UiLCJ2YWx1ZSIsInBsYWNlaG9sZGVyIiwiZGlzYWJsZWQiLCJ0eXBlIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJvbmVPZlR5cGUiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiZmxleCIsImNhcmQiLCJGb3JtIiwicHJvcHMiLCJzdGF0ZSIsImFjdGl2ZSIsImZvY3VzIiwiYmluZCIsIm9uQ2xpY2siLCJlIiwic2V0U3RhdGUiLCJ0YXJnZXQiLCJvblN1Ym1pdCIsImtleU5hbWUiLCJ0aXRsZSIsImhvdmVyIiwiZGVmYXVsdFByb3AiLCJncmlkSXRlbSIsInBhZGRpbmdCb3R0b20iLCJmb3JtIiwicGFkZGluZ1RvcCIsIlByb2ZpbGUiLCJ1c2VyIiwidXNlcm5hbWUiLCJkZXNjcmlwdGlvbiIsIm1lc3NhZ2UiLCJuZXh0UHJvcHMiLCJrZXkiLCJ1c2VyVXBkYXRlIiwibG9nIiwic2hhcGUiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtBOzs7Ozs7Ozs7Ozs7Ozs7QUFMQTs7O3NGQW9CQTtBQUFBLFFBQXdCQSxFQUF4QixTQUF3QkEsRUFBeEI7QUFBQSxRQUE0QkMsS0FBNUIsU0FBNEJBLEtBQTVCO0FBQUEsUUFBbUNDLElBQW5DLFNBQW1DQSxJQUFuQztBQUFBLFFBQXlDQyxNQUF6QyxTQUF5Q0EsTUFBekM7QUFBQSxRQUFpREMsS0FBakQsU0FBaURBLEtBQWpEO0FBQUEsUUFBd0RDLE1BQXhELFNBQXdEQSxNQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ04sRUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTU0sR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVUO0FBSFIsZUFGa0I7QUFPM0JVLG9CQUFNLHlCQUFlLEVBQUVULFVBQUYsRUFBUUMsY0FBUixFQUFnQkMsWUFBaEIsRUFBdUJDLGNBQXZCLEVBQWY7QUFQcUIsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBY1lDLGtCQWRaLEdBY21DRCxHQWRuQyxDQWNZQyxNQWRaLEVBY29CQyxVQWRwQixHQWNtQ0YsR0FkbkMsQ0Fjb0JFLFVBZHBCOztBQUFBLGtCQWdCUUQsVUFBVSxHQWhCbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBaUJhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBakJiOztBQUFBO0FBQUE7QUFBQSxtQkF1QnVCRixJQUFJSyxJQUFKLEVBdkJ2Qjs7QUFBQTtBQXVCVUEsZ0JBdkJWO0FBQUEsd0VBeUJnQkEsSUF6QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEyQklDLG9CQUFRRixLQUFSOztBQTNCSiw2Q0E2Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBN0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlRyxNOzs7OztBQWxCZjs7OztBQUNBOzs7O2tCQXFEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pEZjs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1DLFNBQVM7QUFDYkMsUUFBTTtBQUNKQyxhQUFTO0FBREwsR0FETztBQUliQyxTQUFPO0FBQ0xDLGNBQVUsR0FETDtBQUVMQyxZQUFRLG1CQUZIO0FBR0xDLGtCQUFjLEtBSFQ7QUFJTEMsYUFBUyxLQUpKO0FBS0wsZUFBVztBQUNUQyxlQUFTLE1BREE7QUFFVEMsbUJBQWEsU0FGSjtBQUdUQyxpQkFBVztBQUhGO0FBTE47QUFKTSxDQUFmLEMsQ0FaQTs7Ozs7OztBQTZCQSxJQUFNQyxRQUFRLFNBQVJBLEtBQVE7QUFBQSxNQUFHQyxPQUFILFFBQUdBLE9BQUg7QUFBQSxNQUFZQyxRQUFaLFFBQVlBLFFBQVo7QUFBQSxNQUFzQkMsS0FBdEIsUUFBc0JBLEtBQXRCO0FBQUEsTUFBNkJDLFdBQTdCLFFBQTZCQSxXQUE3QjtBQUFBLE1BQTBDQyxRQUExQyxRQUEwQ0EsUUFBMUM7QUFBQSxNQUFvREMsSUFBcEQsUUFBb0RBLElBQXBEO0FBQUEsU0FDWjtBQUFBO0FBQUEsTUFBSyxXQUFXTCxRQUFRWCxJQUF4QjtBQUNFO0FBQ0UsWUFBTWdCLFFBQVEsTUFEaEI7QUFFRSxtQkFBYUYsZUFBZSxPQUY5QjtBQUdFLGFBQU9ELEtBSFQ7QUFJRSxnQkFBVUQsUUFKWjtBQUtFLGlCQUFXRCxRQUFRVCxLQUxyQjtBQU1FLGdCQUFVLENBQUMsQ0FBQ2E7QUFOZDtBQURGLEdBRFk7QUFBQSxDQUFkOztBQWFBTCxNQUFNTyxTQUFOLEdBQWtCO0FBQ2hCTixXQUFTLG9CQUFVTyxNQUFWLENBQWlCQyxVQURWO0FBRWhCUCxZQUFVLG9CQUFVUSxJQUFWLENBQWVELFVBRlQ7QUFHaEJOLFNBQU8sb0JBQVVRLFNBQVYsQ0FBb0IsQ0FBQyxvQkFBVUMsTUFBWCxFQUFtQixvQkFBVUMsTUFBN0IsQ0FBcEIsRUFBMERKLFVBSGpEO0FBSWhCTCxlQUFhLG9CQUFVUSxNQUpQO0FBS2hCTixRQUFNLG9CQUFVTSxNQUxBO0FBTWhCUCxZQUFVLG9CQUFVUztBQU5KLENBQWxCOztrQkFTZSwwQkFBV3pCLE1BQVgsRUFBbUJXLEtBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVDZjs7OztBQUNBOzs7O0FBRUE7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFkQTs7Ozs7OztBQWdCQSxJQUFNWCxTQUFTO0FBQ2JDLFFBQU07QUFDSnlCLFVBQU07QUFERixHQURPO0FBSWJDLFFBQU07QUFDSnJCLGtCQUFjO0FBRFYsR0FKTztBQU9iRixZQUFVO0FBQ1JzQixVQUFNO0FBREU7QUFQRyxDQUFmOztJQVlNRSxJOzs7QUFDSixnQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGtJQUNYQSxLQURXOztBQUVqQixVQUFLQyxLQUFMLEdBQWE7QUFDWGhCLGFBQU9lLE1BQU1mLEtBREY7QUFFWGlCLGNBQVEsS0FGRztBQUdYQyxhQUFPO0FBSEksS0FBYjs7QUFNQSxVQUFLbkIsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWNvQixJQUFkLE9BQWhCO0FBQ0EsVUFBS0MsT0FBTCxHQUFlLE1BQUtBLE9BQUwsQ0FBYUQsSUFBYixPQUFmO0FBVGlCO0FBVWxCOzs7OzZCQUVRRSxDLEVBQUc7QUFDVixXQUFLQyxRQUFMLENBQWMsRUFBRXRCLE9BQU9xQixFQUFFRSxNQUFGLENBQVN2QixLQUFsQixFQUFkO0FBQ0Q7Ozs4QkFFUztBQUNSLFdBQUtlLEtBQUwsQ0FBV1MsUUFBWCxDQUFvQixLQUFLVCxLQUFMLENBQVdVLE9BQS9CLEVBQXdDLEtBQUtULEtBQUwsQ0FBV2hCLEtBQW5EO0FBQ0Q7Ozs2QkFFUTtBQUFBOztBQUFBLG1CQUNrQixLQUFLZ0IsS0FEdkI7QUFBQSxVQUNDaEIsS0FERCxVQUNDQSxLQUREO0FBQUEsVUFDUWtCLEtBRFIsVUFDUUEsS0FEUjtBQUFBLG1CQUUyQyxLQUFLSCxLQUZoRDtBQUFBLFVBRUNqQixPQUZELFVBRUNBLE9BRkQ7QUFBQSxVQUVVNEIsS0FGVixVQUVVQSxLQUZWO0FBQUEsVUFFaUJ6QixXQUZqQixVQUVpQkEsV0FGakI7QUFBQSxVQUU4QkMsUUFGOUIsVUFFOEJBLFFBRjlCOzs7QUFJUCxhQUNFO0FBQUE7QUFBQTtBQUNFLHFCQUFXSixRQUFRWCxJQURyQjtBQUVFLHdCQUFjO0FBQUEsbUJBQU0sT0FBS21DLFFBQUwsQ0FBYyxFQUFFSyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsV0FGaEI7QUFHRSx3QkFBYztBQUFBLG1CQUFNLE9BQUtMLFFBQUwsQ0FBYyxFQUFFSyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUE7QUFIaEI7QUFLRTtBQUFBO0FBQUEsWUFBTSxRQUFRLEtBQUtYLEtBQUwsQ0FBV1csS0FBekIsRUFBZ0MsV0FBVzdCLFFBQVFlLElBQW5EO0FBR0U7QUFBQTtBQUFBO0FBQ0UsdUJBQVM7QUFBQSx1QkFBTSxPQUFLUyxRQUFMLENBQWMsRUFBRUosT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLGVBRFg7QUFFRSxzQkFBUTtBQUFBLHVCQUNOLE9BQUtJLFFBQUwsQ0FBYztBQUNaSix5QkFBTyxFQUFFLE9BQUtILEtBQUwsQ0FBV2YsS0FBWCxLQUFxQkEsS0FBdkI7QUFESyxpQkFBZCxDQURNO0FBQUE7QUFGVjtBQVFNMEIsaUJBUk47QUFTRTtBQUNFLHFCQUFPMUIsS0FEVDtBQUVFLHdCQUFVLEtBQUtELFFBRmpCO0FBR0UsMkJBQWFFLFdBSGY7QUFJRSx3QkFBVUM7QUFKWjtBQVRGLFdBSEY7QUFtQkdnQixtQkFBUyxFQUFFaEIsYUFBYSxJQUFmLENBQVQsR0FDQztBQUFBO0FBQUE7QUFDRSxtREFBSyxXQUFXSixRQUFRUixRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLGdCQUFRLFlBQVIsRUFBZSxPQUFNLFFBQXJCLEVBQThCLFNBQVMsS0FBSzhCLE9BQTVDO0FBQUE7QUFBQTtBQUZGLFdBREQsR0FPRztBQTFCTjtBQUxGLE9BREY7QUFvQ0Q7Ozs7O0FBR0g7Ozs7O0FBR0FOLEtBQUtWLFNBQUwsR0FBaUI7QUFDZk4sV0FBUyxvQkFBVU8sTUFBVixDQUFpQkMsVUFEWDtBQUVmbUIsV0FBUyxvQkFBVWhCLE1BRko7QUFHZmlCLFNBQU8sb0JBQVVqQixNQUFWLENBQWlCSCxVQUhUO0FBSWZOLFNBQU8sb0JBQVVRLFNBQVYsQ0FBb0IsQ0FBQyxvQkFBVUMsTUFBWCxFQUFtQixvQkFBVUMsTUFBN0IsQ0FBcEIsRUFBMERKLFVBSmxEO0FBS2ZrQixZQUFVLG9CQUFVakIsSUFMTDtBQU1mTixlQUFhLG9CQUFVUSxNQU5SO0FBT2ZQLFlBQVUsb0JBQVVTO0FBUEwsQ0FBakI7O0FBVUE7QUFDQUcsS0FBS2MsV0FBTCxHQUFtQjtBQUNqQjFCLFlBQVUsS0FETztBQUVqQkQsZUFBYTtBQUZJLENBQW5COztrQkFLZSx3QkFBV2YsTUFBWCxFQUFtQjRCLElBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdHZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTTVCLFNBQVM7QUFDYkMsUUFBTSxFQURPO0FBRWIwQyxZQUFVO0FBQ1JDLG1CQUFlO0FBRFAsR0FGRztBQUtiQyxRQUFNO0FBQ0pDLGdCQUFZO0FBRFI7QUFMTyxDQUFmLEMsQ0FiQTs7SUF1Qk1DLE87OztBQUNKLG1CQUFZbEIsS0FBWixFQUFtQjtBQUFBOztBQUFBLHdJQUNYQSxLQURXOztBQUFBLHNCQUUyQkEsTUFBTW1CLElBRmpDO0FBQUEsUUFFVHBFLEVBRlMsZUFFVEEsRUFGUztBQUFBLFFBRUxxRSxRQUZLLGVBRUxBLFFBRks7QUFBQSxRQUVLbkUsSUFGTCxlQUVLQSxJQUZMO0FBQUEsUUFFV29FLFdBRlgsZUFFV0EsV0FGWDs7O0FBSWpCLFVBQUtwQixLQUFMLEdBQWEsRUFBRWxELE1BQUYsRUFBTXFFLGtCQUFOLEVBQWdCbkUsVUFBaEIsRUFBc0JvRSx3QkFBdEIsRUFBbUNDLFNBQVMsRUFBNUMsRUFBYjs7QUFFQSxVQUFLYixRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0wsSUFBZCxPQUFoQjtBQU5pQjtBQU9sQjs7Ozs4Q0FFeUJtQixTLEVBQVc7QUFBQSw0QkFDS0EsVUFBVUosSUFEZjtBQUFBLFVBQzNCbEUsSUFEMkIsbUJBQzNCQSxJQUQyQjtBQUFBLFVBQ3JCbUUsUUFEcUIsbUJBQ3JCQSxRQURxQjtBQUFBLFVBQ1hDLFdBRFcsbUJBQ1hBLFdBRFc7O0FBRW5DLFdBQUtkLFFBQUwsQ0FBYyxFQUFFdEQsVUFBRixFQUFRbUUsa0JBQVIsRUFBa0JDLHdCQUFsQixFQUFkO0FBQ0Q7Ozs7MkdBRWNHLEcsRUFBS3ZDLEs7Ozs7Ozs7OytCQUVNLEtBQUtlLEtBQUwsQ0FBV21CLEksRUFBekJwRSxFLGdCQUFBQSxFLEVBQUlDLEssZ0JBQUFBLEs7O3VCQUNpQyxzQkFBYztBQUN6REQsd0JBRHlEO0FBRXpEQyw4QkFGeUQ7QUFHekRDLHdCQUFNZ0M7QUFIbUQsaUJBQWQsQzs7OztBQUFyQ25CLDBCLFNBQUFBLFU7QUFBWXdELHVCLFNBQUFBLE87QUFBU3ZELHFCLFNBQUFBLEs7O3NCQU16QkQsY0FBYyxHOzs7OztBQUNoQixxQkFBS3lDLFFBQUwsQ0FBYztBQUNaZSwyQkFBUztBQURHLGlCQUFkOzs7OztBQU1GLHFCQUFLZixRQUFMLENBQWMsRUFBRWUsZ0JBQUYsRUFBZDs7QUFFQSxxQkFBS3RCLEtBQUwsQ0FBV3lCLFVBQVgsQ0FBc0IsRUFBRTFFLE1BQUYsRUFBTUUsTUFBTWdDLEtBQVosRUFBdEI7Ozs7Ozs7O0FBRUFoQix3QkFBUXlELEdBQVI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFJSztBQUFBLFVBQ0MzQyxPQURELEdBQ2EsS0FBS2lCLEtBRGxCLENBQ0NqQixPQUREO0FBQUEsbUJBRXVCLEtBQUtrQixLQUY1QjtBQUFBLFVBRUNsRCxFQUZELFVBRUNBLEVBRkQ7QUFBQSxVQUVLRSxJQUZMLFVBRUtBLElBRkw7QUFBQSxVQUVXcUUsT0FGWCxVQUVXQSxPQUZYOzs7QUFJUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDLEVBQTZDLFdBQVd2QyxRQUFRWCxJQUFoRTtBQUNFO0FBQUE7QUFBQTtBQUNFLHNCQURGO0FBRUUsZ0JBQUksRUFGTjtBQUdFLGdCQUFJLEVBSE47QUFJRSxnQkFBSSxDQUpOO0FBS0UsZ0JBQUksQ0FMTjtBQU1FLGdCQUFJLENBTk47QUFPRSx1QkFBV1csUUFBUStCO0FBUHJCO0FBU0U7QUFBQTtBQUFBLGNBQUssV0FBVy9CLFFBQVFpQyxJQUF4QjtBQUNFLDREQUFNLE9BQU8sUUFBYixFQUF1QixPQUFPakUsRUFBOUIsRUFBa0MsY0FBbEM7QUFERixXQVRGO0FBWUU7QUFBQTtBQUFBLGNBQUssV0FBV2dDLFFBQVFpQyxJQUF4QjtBQUNFO0FBQ0UscUJBQU8sTUFEVDtBQUVFLHVCQUFTLG9CQUFZLEVBQUUvRCxVQUFGLEVBQVosRUFBc0IsQ0FBdEIsQ0FGWDtBQUdFLHFCQUFPQSxJQUhUO0FBSUUsMkJBQWEsTUFKZjtBQUtFLHdCQUFVLEtBQUt3RDtBQUxqQjtBQURGLFdBWkY7QUFxQkU7QUFBQTtBQUFBLGNBQUssV0FBVzFCLFFBQVFpQyxJQUF4QjtBQUNFO0FBQ0UscUJBQU8sS0FEVDtBQUVFLDJCQUFhLGlCQUZmO0FBR0UscUJBQU8sRUFIVDtBQUlFO0FBSkY7QUFERixXQXJCRjtBQTZCRTtBQUFBO0FBQUEsY0FBSyxXQUFXakMsUUFBUWlDLElBQXhCO0FBQ0U7QUFDRSxxQkFBTyxVQURUO0FBRUUsMkJBQWEsY0FGZjtBQUdFLHFCQUFPLEVBSFQ7QUFJRTtBQUpGO0FBREYsV0E3QkY7QUFxQ0U7QUFBQTtBQUFBLGNBQUssV0FBV2pDLFFBQVFpQyxJQUF4QjtBQUNFO0FBQ0UscUJBQU8sVUFEVDtBQUVFLDJCQUFhLGNBRmY7QUFHRSxxQkFBTyxFQUhUO0FBSUU7QUFKRjtBQURGLFdBckNGO0FBNkNFO0FBQUE7QUFBQSxjQUFLLFdBQVdqQyxRQUFRaUMsSUFBeEI7QUFDRTtBQUNFLHFCQUFPLGFBRFQ7QUFFRSwyQkFBYSxpQkFGZjtBQUdFLHFCQUFPLEVBSFQ7QUFJRTtBQUpGO0FBREYsV0E3Q0Y7QUFxREU7QUFBQTtBQUFBLGNBQUssV0FBV2pDLFFBQVFpQyxJQUF4QjtBQUNFO0FBQ0UscUJBQU8sV0FEVDtBQUVFLDJCQUFhLGVBRmY7QUFHRSxxQkFBTyxFQUhUO0FBSUU7QUFKRjtBQURGLFdBckRGO0FBNkRFO0FBQUE7QUFBQSxjQUFLLFdBQVdqQyxRQUFRaUMsSUFBeEI7QUFDRTtBQUNFLHFCQUFPLFNBRFQ7QUFFRSwyQkFBYSxhQUZmO0FBR0UscUJBQU8sRUFIVDtBQUlFO0FBSkY7QUFERixXQTdERjtBQXFFRTtBQUFBO0FBQUEsY0FBSyxXQUFXakMsUUFBUWlDLElBQXhCO0FBQ0U7QUFDRSxxQkFBTyxPQURUO0FBRUUsMkJBQWEsZ0JBRmY7QUFHRSxxQkFBTyxFQUhUO0FBSUU7QUFKRjtBQURGO0FBckVGO0FBREYsT0FERjtBQWtGRDs7Ozs7QUFHSEUsUUFBUTdCLFNBQVIsR0FBb0I7QUFDbEJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFI7QUFFbEI0QixRQUFNLG9CQUFVUSxLQUFWLENBQWdCO0FBQ3BCNUUsUUFBSSxvQkFBVTRDLE1BQVYsQ0FBaUJKLFVBREQ7QUFFcEJ2QyxXQUFPLG9CQUFVMEMsTUFBVixDQUFpQkgsVUFGSjtBQUdwQjZCLGNBQVUsb0JBQVUxQixNQUhBO0FBSXBCekMsVUFBTSxvQkFBVXlDO0FBSkksR0FBaEIsRUFLSEgsVUFQZTtBQVFsQmtDLGNBQVksb0JBQVVqQyxJQUFWLENBQWVEO0FBUlQsQ0FBcEI7O2tCQVdlLDBCQUFXcEIsTUFBWCxFQUFtQitDLE9BQW5CLEMiLCJmaWxlIjoiNjcuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmFtZSAtXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5hdmF0YXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuY292ZXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQucmVzdW1lIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiB1cGRhdGUoeyBpZCwgdG9rZW4sIG5hbWUsIGF2YXRhciwgY292ZXIsIHJlc3VtZSB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7aWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUFVUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgbmFtZSwgYXZhdGFyLCBjb3ZlciwgcmVzdW1lIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3VzZXJzL3VwZGF0ZS5qcyIsIi8qKlxuICogVGhpcyBpbnB1dCBjb21wb25lbnQgaXMgbWFpbmx5IGRldmVsb3BlZCBmb3IgY3JlYXRlIHBvc3QsIGNyZWF0ZVxuICogY291cnNlIGNvbXBvbmVudC5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZGlzcGxheTogJ2ZsZXgnLFxuICB9LFxuICBpbnB1dDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gICAgYm9yZGVyOiAnMnB4IHNvbGlkICNkYWRhZGEnLFxuICAgIGJvcmRlclJhZGl1czogJzdweCcsXG4gICAgcGFkZGluZzogJzhweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbn07XG5cbmNvbnN0IElucHV0ID0gKHsgY2xhc3Nlcywgb25DaGFuZ2UsIHZhbHVlLCBwbGFjZWhvbGRlciwgZGlzYWJsZWQsIHR5cGUgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICA8aW5wdXRcbiAgICAgIHR5cGU9e3R5cGUgfHwgJ3RleHQnfVxuICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyIHx8ICdpbnB1dCd9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMuaW5wdXR9XG4gICAgICBkaXNhYmxlZD17ISFkaXNhYmxlZH1cbiAgICAvPlxuICA8L2Rpdj5cbik7XG5cbklucHV0LnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgdmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKElucHV0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsIi8qKlxuICogVGhpcyBpcyB0aGUgZm9ybSBDb21wb25lbnRcbiAqIEdlbmVyYWx5IGl0IHdpbGwgYmUgdXNlZCBjcmVhdGluZyBwb3N0cyBhbmQgbW9kdWxlc1xuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHsgd2l0aFN0eWxlcyB9IGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcyc7XG5pbXBvcnQgQ2FyZCwgeyBDYXJkQ29udGVudCwgQ2FyZEFjdGlvbnMgfSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcblxuaW1wb3J0IElucHV0IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvSW5wdXQnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBmbGV4OiAnMSAwIDEwMCUnLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuY2xhc3MgRm9ybSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICB2YWx1ZTogcHJvcHMudmFsdWUsXG4gICAgICBhY3RpdmU6IGZhbHNlLFxuICAgICAgZm9jdXM6IGZhbHNlLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25DbGljayA9IHRoaXMub25DbGljay5iaW5kKHRoaXMpO1xuICB9XG5cbiAgb25DaGFuZ2UoZSkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyB2YWx1ZTogZS50YXJnZXQudmFsdWUgfSk7XG4gIH1cblxuICBvbkNsaWNrKCkge1xuICAgIHRoaXMucHJvcHMub25TdWJtaXQodGhpcy5wcm9wcy5rZXlOYW1lLCB0aGlzLnN0YXRlLnZhbHVlKTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IHZhbHVlLCBmb2N1cyB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHRpdGxlLCBwbGFjZWhvbGRlciwgZGlzYWJsZWQgfSA9IHRoaXMucHJvcHM7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH1cbiAgICAgICAgb25Nb3VzZUVudGVyPXsoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IHRydWUgfSl9XG4gICAgICAgIG9uTW91c2VMZWF2ZT17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KX1cbiAgICAgID5cbiAgICAgICAgPENhcmQgcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgey8qIG9uQmx1ciBpcyBvcHBvc2l0IHRvIG9uRm9jdXMsIEdlbmVyYWx5IGl0IGlzIHVzZWQgd2hlbiB0aGUgXG4gICAgICAgICAgdXNlciBsZWF2ZXMgYSBmb3JtIGZpZWxkICovfVxuICAgICAgICAgIDxDYXJkQ29udGVudFxuICAgICAgICAgICAgb25Gb2N1cz17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGZvY3VzOiB0cnVlIH0pfVxuICAgICAgICAgICAgb25CbHVyPXsoKSA9PlxuICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICBmb2N1czogISh0aGlzLnByb3BzLnZhbHVlID09PSB2YWx1ZSksXG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9XG4gICAgICAgICAgPlxuICAgICAgICAgICAge2Ake3RpdGxlfTogYH1cbiAgICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICAgIGRpc2FibGVkPXtkaXNhYmxlZH1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICB7Zm9jdXMgJiYgIShkaXNhYmxlZCA9PT0gdHJ1ZSkgPyAoXG4gICAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgICA8QnV0dG9uIHJhaXNlZCBjb2xvcj1cImFjY2VudFwiIG9uQ2xpY2s9e3RoaXMub25DbGlja30+XG4gICAgICAgICAgICAgICAgU3VibWl0XG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG4vKipcbiAqIFRoaXMgd2lsbCB2YWxpZGF0ZSBhbGwgdGhlIHByb3BzIGNvbW1pbmcgdG8gdGhlIGNvbXBvbmVudFxuICovXG5Gb3JtLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBrZXlOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm51bWJlcl0pLmlzUmVxdWlyZWQsXG4gIG9uU3VibWl0OiBQcm9wVHlwZXMuZnVuYyxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbi8vIERlZmF1bHQgcHJvcHMgZm9yIHRoZSBjb21wb25lbnRcbkZvcm0uZGVmYXVsdFByb3AgPSB7XG4gIGRpc2FibGVkOiBmYWxzZSxcbiAgcGxhY2Vob2xkZXI6ICdJbnB1dCcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoRm9ybSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRm9ybS9pbmRleC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcblxuaW1wb3J0IEZvcm0gZnJvbSAnLi4vRm9ybSc7XG5cbmltcG9ydCBhcGlVc2VyVXBkYXRlIGZyb20gJy4uLy4uL2FwaS91c2Vycy91cGRhdGUnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHt9LFxuICBncmlkSXRlbToge1xuICAgIHBhZGRpbmdCb3R0b206ICcxJScsXG4gIH0sXG4gIGZvcm06IHtcbiAgICBwYWRkaW5nVG9wOiAnMSUnLFxuICB9LFxufTtcblxuY2xhc3MgUHJvZmlsZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIGNvbnN0IHsgaWQsIHVzZXJuYW1lLCBuYW1lLCBkZXNjcmlwdGlvbiB9ID0gcHJvcHMudXNlcjtcblxuICAgIHRoaXMuc3RhdGUgPSB7IGlkLCB1c2VybmFtZSwgbmFtZSwgZGVzY3JpcHRpb24sIG1lc3NhZ2U6ICcnIH07XG5cbiAgICB0aGlzLm9uU3VibWl0ID0gdGhpcy5vblN1Ym1pdC5iaW5kKHRoaXMpO1xuICB9XG5cbiAgY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcbiAgICBjb25zdCB7IG5hbWUsIHVzZXJuYW1lLCBkZXNjcmlwdGlvbiB9ID0gbmV4dFByb3BzLnVzZXI7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IG5hbWUsIHVzZXJuYW1lLCBkZXNjcmlwdGlvbiB9KTtcbiAgfVxuXG4gIGFzeW5jIG9uU3VibWl0KGtleSwgdmFsdWUpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBpZCwgdG9rZW4gfSA9IHRoaXMucHJvcHMudXNlcjtcbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgbWVzc2FnZSwgZXJyb3IgfSA9IGF3YWl0IGFwaVVzZXJVcGRhdGUoe1xuICAgICAgICBpZCxcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIG5hbWU6IHZhbHVlLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICBtZXNzYWdlOiAnWW91ciByZXF1ZXN0IGZhaWxlZCBkdWUgdG8gc29tZSB0ZWNobmljYWwgcHJvYmxlbScsXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlIH0pO1xuXG4gICAgICB0aGlzLnByb3BzLnVzZXJVcGRhdGUoeyBpZCwgbmFtZTogdmFsdWUgfSk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgfVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcyB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGlkLCBuYW1lLCBtZXNzYWdlIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZFxuICAgICAgICAgIGl0ZW1cbiAgICAgICAgICB4cz17MTJ9XG4gICAgICAgICAgc209ezEwfVxuICAgICAgICAgIG1kPXs4fVxuICAgICAgICAgIGxnPXs2fVxuICAgICAgICAgIHhsPXs2fVxuICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZvcm19PlxuICAgICAgICAgICAgPEZvcm0gdGl0bGU9eydVc2VySWQnfSB2YWx1ZT17aWR9IGRpc2FibGVkIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZm9ybX0+XG4gICAgICAgICAgICA8Rm9ybVxuICAgICAgICAgICAgICB0aXRsZT17J05hbWUnfVxuICAgICAgICAgICAgICBrZXlOYW1lPXtPYmplY3Qua2V5cyh7IG5hbWUgfSlbMF19XG4gICAgICAgICAgICAgIHZhbHVlPXtuYW1lfVxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17J05hbWUnfVxuICAgICAgICAgICAgICBvblN1Ym1pdD17dGhpcy5vblN1Ym1pdH1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZm9ybX0+XG4gICAgICAgICAgICA8Rm9ybVxuICAgICAgICAgICAgICB0aXRsZT17J0Jpbyd9XG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXsnQmlvIERlc2NyaXB0aW9uJ31cbiAgICAgICAgICAgICAgdmFsdWU9eycnfVxuICAgICAgICAgICAgICBkaXNhYmxlZFxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mb3JtfT5cbiAgICAgICAgICAgIDxGb3JtXG4gICAgICAgICAgICAgIHRpdGxlPXsnTGlua2VkaW4nfVxuICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17J0xpbmtlZGluIFVSTCd9XG4gICAgICAgICAgICAgIHZhbHVlPXsnJ31cbiAgICAgICAgICAgICAgZGlzYWJsZWRcbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZm9ybX0+XG4gICAgICAgICAgICA8Rm9ybVxuICAgICAgICAgICAgICB0aXRsZT17J0ZhY2Vib29rJ31cbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eydGYWNlYm9vayBVUkwnfVxuICAgICAgICAgICAgICB2YWx1ZT17Jyd9XG4gICAgICAgICAgICAgIGRpc2FibGVkXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZvcm19PlxuICAgICAgICAgICAgPEZvcm1cbiAgICAgICAgICAgICAgdGl0bGU9eydHb29nbGUgUGx1cyd9XG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXsnR29vZ2xlIFBsdXMgVVJMJ31cbiAgICAgICAgICAgICAgdmFsdWU9eycnfVxuICAgICAgICAgICAgICBkaXNhYmxlZFxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mb3JtfT5cbiAgICAgICAgICAgIDxGb3JtXG4gICAgICAgICAgICAgIHRpdGxlPXsnSW5zdGFncmFtJ31cbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eydJbnN0YWdyYW0gVVJMJ31cbiAgICAgICAgICAgICAgdmFsdWU9eycnfVxuICAgICAgICAgICAgICBkaXNhYmxlZFxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mb3JtfT5cbiAgICAgICAgICAgIDxGb3JtXG4gICAgICAgICAgICAgIHRpdGxlPXsnVHdpdHRlcid9XG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXsnVHdpdHRlciBVUkwnfVxuICAgICAgICAgICAgICB2YWx1ZT17Jyd9XG4gICAgICAgICAgICAgIGRpc2FibGVkXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZvcm19PlxuICAgICAgICAgICAgPEZvcm1cbiAgICAgICAgICAgICAgdGl0bGU9eydTa3lwZSd9XG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXsnU2t5cGUgVXNlcm5hbWUnfVxuICAgICAgICAgICAgICB2YWx1ZT17Jyd9XG4gICAgICAgICAgICAgIGRpc2FibGVkXG4gICAgICAgICAgICAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L0dyaWQ+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5Qcm9maWxlLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB1c2VyOiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIGlkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gICAgdG9rZW46IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICB1c2VybmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB9KS5pc1JlcXVpcmVkLFxuICB1c2VyVXBkYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFByb2ZpbGUpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Byb2ZpbGVQYWdlL1Byb2ZpbGUuanMiXSwic291cmNlUm9vdCI6IiJ9