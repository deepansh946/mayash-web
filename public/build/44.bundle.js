webpackJsonp([44],{

/***/ "./node_modules/material-ui-icons/AttachFile.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/AttachFile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z' });

var AttachFile = function AttachFile(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

AttachFile = (0, _pure2.default)(AttachFile);
AttachFile.muiName = 'SvgIcon';

exports.default = AttachFile;

/***/ }),

/***/ "./node_modules/material-ui-icons/Code.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Code.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z' });

var Code = function Code(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Code = (0, _pure2.default)(Code);
Code.muiName = 'SvgIcon';

exports.default = Code;

/***/ }),

/***/ "./node_modules/material-ui-icons/Edit.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Edit.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z' });

var Edit = function Edit(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Edit = (0, _pure2.default)(Edit);
Edit.muiName = 'SvgIcon';

exports.default = Edit;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignCenter.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignCenter.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M7 15v2h10v-2H7zm-4 6h18v-2H3v2zm0-8h18v-2H3v2zm4-6v2h10V7H7zM3 3v2h18V3H3z' });

var FormatAlignCenter = function FormatAlignCenter(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignCenter = (0, _pure2.default)(FormatAlignCenter);
FormatAlignCenter.muiName = 'SvgIcon';

exports.default = FormatAlignCenter;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignJustify.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignJustify.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18V7H3v2zm0-6v2h18V3H3z' });

var FormatAlignJustify = function FormatAlignJustify(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignJustify = (0, _pure2.default)(FormatAlignJustify);
FormatAlignJustify.muiName = 'SvgIcon';

exports.default = FormatAlignJustify;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignLeft.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignLeft.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15 15H3v2h12v-2zm0-8H3v2h12V7zM3 13h18v-2H3v2zm0 8h18v-2H3v2zM3 3v2h18V3H3z' });

var FormatAlignLeft = function FormatAlignLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignLeft = (0, _pure2.default)(FormatAlignLeft);
FormatAlignLeft.muiName = 'SvgIcon';

exports.default = FormatAlignLeft;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignRight.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignRight.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm6-4h12v-2H9v2zm-6-4h18v-2H3v2zm6-4h12V7H9v2zM3 3v2h18V3H3z' });

var FormatAlignRight = function FormatAlignRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignRight = (0, _pure2.default)(FormatAlignRight);
FormatAlignRight.muiName = 'SvgIcon';

exports.default = FormatAlignRight;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatBold.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatBold.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.6 10.79c.97-.67 1.65-1.77 1.65-2.79 0-2.26-1.75-4-4-4H7v14h7.04c2.09 0 3.71-1.7 3.71-3.79 0-1.52-.86-2.82-2.15-3.42zM10 6.5h3c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-3v-3zm3.5 9H10v-3h3.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5z' });

var FormatBold = function FormatBold(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatBold = (0, _pure2.default)(FormatBold);
FormatBold.muiName = 'SvgIcon';

exports.default = FormatBold;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatItalic.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatItalic.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4z' });

var FormatItalic = function FormatItalic(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatItalic = (0, _pure2.default)(FormatItalic);
FormatItalic.muiName = 'SvgIcon';

exports.default = FormatItalic;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListBulleted.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListBulleted.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M4 10.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5-.67-1.5-1.5-1.5zm0-6c-.83 0-1.5.67-1.5 1.5S3.17 7.5 4 7.5 5.5 6.83 5.5 6 4.83 4.5 4 4.5zm0 12c-.83 0-1.5.68-1.5 1.5s.68 1.5 1.5 1.5 1.5-.68 1.5-1.5-.67-1.5-1.5-1.5zM7 19h14v-2H7v2zm0-6h14v-2H7v2zm0-8v2h14V5H7z' });

var FormatListBulleted = function FormatListBulleted(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListBulleted = (0, _pure2.default)(FormatListBulleted);
FormatListBulleted.muiName = 'SvgIcon';

exports.default = FormatListBulleted;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListNumbered.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListNumbered.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z' });

var FormatListNumbered = function FormatListNumbered(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListNumbered = (0, _pure2.default)(FormatListNumbered);
FormatListNumbered.muiName = 'SvgIcon';

exports.default = FormatListNumbered;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatQuote.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatQuote.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' });

var FormatQuote = function FormatQuote(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatQuote = (0, _pure2.default)(FormatQuote);
FormatQuote.muiName = 'SvgIcon';

exports.default = FormatQuote;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatUnderlined.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatUnderlined.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z' });

var FormatUnderlined = function FormatUnderlined(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatUnderlined = (0, _pure2.default)(FormatUnderlined);
FormatUnderlined.muiName = 'SvgIcon';

exports.default = FormatUnderlined;

/***/ }),

/***/ "./node_modules/material-ui-icons/Functions.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Functions.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 4H6v2l6.5 6L6 18v2h12v-3h-7l5-5-5-5h7z' });

var Functions = function Functions(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Functions = (0, _pure2.default)(Functions);
Functions.muiName = 'SvgIcon';

exports.default = Functions;

/***/ }),

/***/ "./node_modules/material-ui-icons/Highlight.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Highlight.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 14l3 3v5h6v-5l3-3V9H6zm5-12h2v3h-2zM3.5 5.875L4.914 4.46l2.12 2.122L5.62 7.997zm13.46.71l2.123-2.12 1.414 1.414L18.375 8z' });

var Highlight = function Highlight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Highlight = (0, _pure2.default)(Highlight);
Highlight.muiName = 'SvgIcon';

exports.default = Highlight;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertComment.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertComment.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var InsertComment = function InsertComment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertComment = (0, _pure2.default)(InsertComment);
InsertComment.muiName = 'SvgIcon';

exports.default = InsertComment;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertEmoticon.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertEmoticon.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });

var InsertEmoticon = function InsertEmoticon(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertEmoticon = (0, _pure2.default)(InsertEmoticon);
InsertEmoticon.muiName = 'SvgIcon';

exports.default = InsertEmoticon;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertLink.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertLink.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z' });

var InsertLink = function InsertLink(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertLink = (0, _pure2.default)(InsertLink);
InsertLink.muiName = 'SvgIcon';

exports.default = InsertLink;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui-icons/Save.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Save.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z' });

var Save = function Save(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Save = (0, _pure2.default)(Save);
Save.muiName = 'SvgIcon';

exports.default = Save;

/***/ }),

/***/ "./node_modules/material-ui-icons/Title.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Title.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M5 4v3h5.5v12h3V7H19V4z' });

var Title = function Title(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Title = (0, _pure2.default)(Title);
Title.muiName = 'SvgIcon';

exports.default = Title;

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/courses/update.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/update.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus -
 * @param {Object[]} payload.courseModules -
 * @param {Object} payload.courseModules[] -
 * @param {number} payload.courseModules[].moduleId -
 *
 * @return {Object} -
 */
var update = function update(payload) {
  return { type: _courses.COURSE_UPDATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = update;

/***/ }),

/***/ "./src/client/api/courses/users/update.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/update.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Update Course
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {string} payload.cover -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus -
 * @param {Object[]} payload.courseModules -
 * @param {number} payload.courseModules[].moduleId -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * 
 */

var updateCourse = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        courseId = _ref2.courseId,
        token = _ref2.token,
        cover = _ref2.cover,
        title = _ref2.title,
        description = _ref2.description,
        syllabus = _ref2.syllabus,
        courseModules = _ref2.courseModules;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({
                cover: cover,
                title: title,
                description: description,
                syllabus: syllabus,
                courseModules: courseModules
              })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function updateCourse(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = updateCourse;

/***/ }),

/***/ "./src/client/containers/CoursePage/Syllabus.js":
/*!******************************************************!*\
  !*** ./src/client/containers/CoursePage/Syllabus.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Edit = __webpack_require__(/*! material-ui-icons/Edit */ "./node_modules/material-ui-icons/Edit.js");

var _Edit2 = _interopRequireDefault(_Edit);

var _Save = __webpack_require__(/*! material-ui-icons/Save */ "./node_modules/material-ui-icons/Save.js");

var _Save2 = _interopRequireDefault(_Save);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _mayashEditor = __webpack_require__(/*! ../../../lib/mayash-editor */ "./src/lib/mayash-editor/index.js");

var _mayashEditor2 = _interopRequireDefault(_mayashEditor);

var _update = __webpack_require__(/*! ../../api/courses/users/update */ "./src/client/api/courses/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _update3 = __webpack_require__(/*! ../../actions/courses/update */ "./src/client/actions/courses/update.js");

var _update4 = _interopRequireDefault(_update3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  var _editButton;

  return {
    root: {
      padding: '1%'
    },
    card: {
      borderRadius: '8px'
    },
    title: {
      textAlign: 'center'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    editButton: (_editButton = {
      position: 'fixed'
    }, (0, _defineProperty3.default)(_editButton, theme.breakpoints.up('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('md'), {
      bottom: '30px',
      right: '30px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('sm'), {
      display: 'none',
      bottom: '25px',
      right: '25px'
    }), _editButton)
  };
}; /** @format */

var Syllabus = function (_Component) {
  (0, _inherits3.default)(Syllabus, _Component);

  function Syllabus(props) {
    (0, _classCallCheck3.default)(this, Syllabus);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Syllabus.__proto__ || (0, _getPrototypeOf2.default)(Syllabus)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    var syllabus = props.course.syllabus;


    _this.state = {
      edit: false,

      syllabus: (0, _mayashEditor.createEditorState)(syllabus)
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onEdit = _this.onEdit.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Syllabus, [{
    key: 'onChange',
    value: function onChange(syllabus) {
      this.setState({ syllabus: syllabus });
    }
  }, {
    key: 'onEdit',
    value: function onEdit() {
      var edit = this.state.edit;

      this.setState({ edit: !edit });
    }
  }, {
    key: 'onSave',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, elements, course, _elements$user, userId, token, courseId, edit, syllabus, _ref2, statusCode, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, elements = _props.elements, course = _props.course;
                _elements$user = elements.user, userId = _elements$user.id, token = _elements$user.token;
                courseId = course.courseId;
                edit = this.state.edit;
                syllabus = (0, _draftJs.convertToRaw)(this.state.syllabus.getCurrentContent());
                _context.next = 8;
                return (0, _update2.default)({
                  userId: userId,
                  courseId: courseId,
                  token: token,
                  syllabus: syllabus
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;

                if (!(statusCode !== 200)) {
                  _context.next = 14;
                  break;
                }

                // handle error;
                console.error(error);
                return _context.abrupt('return');

              case 14:

                this.props.actionCourseUpdate({ courseId: courseId, syllabus: syllabus });
                this.setState({ edit: !edit });
                _context.next = 21;
                break;

              case 18:
                _context.prev = 18;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 18]]);
      }));

      function onSave() {
        return _ref.apply(this, arguments);
      }

      return onSave;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          elements = _props2.elements,
          course = _props2.course;
      var _elements$user2 = elements.user,
          isSignedIn = _elements$user2.isSignedIn,
          userId = _elements$user2.id;
      var authorId = course.authorId;
      var _state = this.state,
          edit = _state.edit,
          syllabus = _state.syllabus;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 12, md: 10, lg: 8, xl: 8 },
          _react2.default.createElement(
            'div',
            {
              onMouseEnter: this.onMouseEnter,
              onMouseLeave: this.onMouseLeave
            },
            _react2.default.createElement(
              _Card2.default,
              { raised: this.state.hover, className: classes.card },
              _react2.default.createElement(_Card.CardHeader, { title: 'Syllabus', className: classes.title }),
              _react2.default.createElement(
                _Card.CardContent,
                null,
                _react2.default.createElement(_mayashEditor2.default, {
                  editorState: syllabus,
                  onChange: this.onChange,
                  readOnly: !edit,
                  placeholder: 'Syllabus Content'
                })
              )
            )
          ),
          isSignedIn && authorId === userId && _react2.default.createElement(
            _Button2.default,
            {
              fab: true,
              color: 'accent',
              'aria-label': 'Edit',
              className: classes.editButton,
              onClick: edit ? this.onSave : this.onEdit
            },
            edit ? _react2.default.createElement(_Save2.default, null) : _react2.default.createElement(_Edit2.default, null)
          )
        )
      );
    }
  }]);
  return Syllabus;
}(_react.Component);

Syllabus.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired,

  actionCourseUpdate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionCourseUpdate: _update4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(Syllabus));

/***/ }),

/***/ "./src/lib/mayash-editor/Editor.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-editor/Editor.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! material-ui-icons/Title */ "./node_modules/material-ui-icons/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _FormatBold = __webpack_require__(/*! material-ui-icons/FormatBold */ "./node_modules/material-ui-icons/FormatBold.js");

var _FormatBold2 = _interopRequireDefault(_FormatBold);

var _FormatItalic = __webpack_require__(/*! material-ui-icons/FormatItalic */ "./node_modules/material-ui-icons/FormatItalic.js");

var _FormatItalic2 = _interopRequireDefault(_FormatItalic);

var _FormatUnderlined = __webpack_require__(/*! material-ui-icons/FormatUnderlined */ "./node_modules/material-ui-icons/FormatUnderlined.js");

var _FormatUnderlined2 = _interopRequireDefault(_FormatUnderlined);

var _FormatQuote = __webpack_require__(/*! material-ui-icons/FormatQuote */ "./node_modules/material-ui-icons/FormatQuote.js");

var _FormatQuote2 = _interopRequireDefault(_FormatQuote);

var _Code = __webpack_require__(/*! material-ui-icons/Code */ "./node_modules/material-ui-icons/Code.js");

var _Code2 = _interopRequireDefault(_Code);

var _FormatListNumbered = __webpack_require__(/*! material-ui-icons/FormatListNumbered */ "./node_modules/material-ui-icons/FormatListNumbered.js");

var _FormatListNumbered2 = _interopRequireDefault(_FormatListNumbered);

var _FormatListBulleted = __webpack_require__(/*! material-ui-icons/FormatListBulleted */ "./node_modules/material-ui-icons/FormatListBulleted.js");

var _FormatListBulleted2 = _interopRequireDefault(_FormatListBulleted);

var _FormatAlignCenter = __webpack_require__(/*! material-ui-icons/FormatAlignCenter */ "./node_modules/material-ui-icons/FormatAlignCenter.js");

var _FormatAlignCenter2 = _interopRequireDefault(_FormatAlignCenter);

var _FormatAlignLeft = __webpack_require__(/*! material-ui-icons/FormatAlignLeft */ "./node_modules/material-ui-icons/FormatAlignLeft.js");

var _FormatAlignLeft2 = _interopRequireDefault(_FormatAlignLeft);

var _FormatAlignRight = __webpack_require__(/*! material-ui-icons/FormatAlignRight */ "./node_modules/material-ui-icons/FormatAlignRight.js");

var _FormatAlignRight2 = _interopRequireDefault(_FormatAlignRight);

var _FormatAlignJustify = __webpack_require__(/*! material-ui-icons/FormatAlignJustify */ "./node_modules/material-ui-icons/FormatAlignJustify.js");

var _FormatAlignJustify2 = _interopRequireDefault(_FormatAlignJustify);

var _AttachFile = __webpack_require__(/*! material-ui-icons/AttachFile */ "./node_modules/material-ui-icons/AttachFile.js");

var _AttachFile2 = _interopRequireDefault(_AttachFile);

var _InsertLink = __webpack_require__(/*! material-ui-icons/InsertLink */ "./node_modules/material-ui-icons/InsertLink.js");

var _InsertLink2 = _interopRequireDefault(_InsertLink);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _InsertEmoticon = __webpack_require__(/*! material-ui-icons/InsertEmoticon */ "./node_modules/material-ui-icons/InsertEmoticon.js");

var _InsertEmoticon2 = _interopRequireDefault(_InsertEmoticon);

var _InsertComment = __webpack_require__(/*! material-ui-icons/InsertComment */ "./node_modules/material-ui-icons/InsertComment.js");

var _InsertComment2 = _interopRequireDefault(_InsertComment);

var _Highlight = __webpack_require__(/*! material-ui-icons/Highlight */ "./node_modules/material-ui-icons/Highlight.js");

var _Highlight2 = _interopRequireDefault(_Highlight);

var _Functions = __webpack_require__(/*! material-ui-icons/Functions */ "./node_modules/material-ui-icons/Functions.js");

var _Functions2 = _interopRequireDefault(_Functions);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Atomic = __webpack_require__(/*! ./components/Atomic */ "./src/lib/mayash-editor/components/Atomic.js");

var _Atomic2 = _interopRequireDefault(_Atomic);

var _EditorStyles = __webpack_require__(/*! ./EditorStyles */ "./src/lib/mayash-editor/EditorStyles.js");

var _EditorStyles2 = _interopRequireDefault(_EditorStyles);

var _constants = __webpack_require__(/*! ./constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * MayashEditor
 */


// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

var MayashEditor = function (_Component) {
  (0, _inherits3.default)(MayashEditor, _Component);

  /**
   * Mayash Editor's proptypes are defined here.
   */
  function MayashEditor() {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, MayashEditor);

    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    var _this = (0, _possibleConstructorReturn3.default)(this, (MayashEditor.__proto__ || (0, _getPrototypeOf2.default)(MayashEditor)).call(this));

    _this.onChangeInsertPhoto = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _ref2, statusCode, error, payload, src, editorState, contentState, contentStateWithEntity, entityKey, middleEditorState, newEditorState;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // e.preventDefault();
                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 21;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _context.next = 6;
                return _this.props.apiPhotoUpload({
                  formData: formData
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle Error
                console.error(statusCode, error);
                return _context.abrupt('return');

              case 13:
                src = payload.photoUrl;
                editorState = _this.props.editorState;
                contentState = editorState.getCurrentContent();
                contentStateWithEntity = contentState.createEntity(_constants.Blocks.PHOTO, 'IMMUTABLE', { src: src });
                entityKey = contentStateWithEntity.getLastCreatedEntityKey();
                middleEditorState = _draftJs.EditorState.set(editorState, {
                  currentContent: contentStateWithEntity
                });
                newEditorState = _draftJs.AtomicBlockUtils.insertAtomicBlock(middleEditorState, entityKey, ' ');


                _this.onChange(newEditorState);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.onChange = function (editorState) {
      _this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    _this.focus = function () {
      return _this.editorNode.focus();
    };

    _this.onTab = _this.onTab.bind(_this);
    _this.onTitleClick = _this.onTitleClick.bind(_this);
    _this.onCodeClick = _this.onCodeClick.bind(_this);
    _this.onQuoteClick = _this.onQuoteClick.bind(_this);
    _this.onListBulletedClick = _this.onListBulletedClick.bind(_this);
    _this.onListNumberedClick = _this.onListNumberedClick.bind(_this);
    _this.onBoldClick = _this.onBoldClick.bind(_this);
    _this.onItalicClick = _this.onItalicClick.bind(_this);
    _this.onUnderLineClick = _this.onUnderLineClick.bind(_this);
    _this.onHighlightClick = _this.onHighlightClick.bind(_this);
    _this.handleKeyCommand = _this.handleKeyCommand.bind(_this);
    _this.onClickInsertPhoto = _this.onClickInsertPhoto.bind(_this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
    return _this;
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */


  (0, _createClass3.default)(MayashEditor, [{
    key: 'onTab',
    value: function onTab(e) {
      e.preventDefault();
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.onTab(e, editorState, 4);

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Title Button.
     * @function onTitleClick
     */

  }, {
    key: 'onTitleClick',
    value: function onTitleClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'header-one');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Code Button.
     * @function onCodeClick
     */

  }, {
    key: 'onCodeClick',
    value: function onCodeClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'code-block');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Quote Button.
     * @function onQuoteClick
     */

  }, {
    key: 'onQuoteClick',
    value: function onQuoteClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'blockquote');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with unordered list items
     * @function onListBulletedClick
     */

  }, {
    key: 'onListBulletedClick',
    value: function onListBulletedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'unordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with ordered list item.
     * @function onListNumberedClick
     */

  }, {
    key: 'onListNumberedClick',
    value: function onListNumberedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'ordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will give selected content Bold styling.
     * @function onBoldClick
     */

  }, {
    key: 'onBoldClick',
    value: function onBoldClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'BOLD');

      this.onChange(newEditorState);
    }

    /**
     * This function will give italic styling to selected content.
     * @function onItalicClick
     */

  }, {
    key: 'onItalicClick',
    value: function onItalicClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'ITALIC');

      this.onChange(newEditorState);
    }

    /**
     * This function will give underline styling to selected content.
     * @function onUnderLineClick
     */

  }, {
    key: 'onUnderLineClick',
    value: function onUnderLineClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');

      this.onChange(newEditorState);
    }

    /**
     * This function will highlight selected content.
     * @function onHighlightClick
     */

  }, {
    key: 'onHighlightClick',
    value: function onHighlightClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'CODE');

      this.onChange(newEditorState);
    }

    /**
     *
     */

  }, {
    key: 'onClickInsertPhoto',
    value: function onClickInsertPhoto() {
      this.photo.value = null;
      this.photo.click();
    }

    /**
     *
     */

  }, {
    key: 'handleKeyCommand',


    /**
     * This function will give custom handle commands to Editor.
     * @function handleKeyCommand
     * @param {string} command
     * @return {string}
     */
    value: function handleKeyCommand(command) {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.handleKeyCommand(editorState, command);

      if (newEditorState) {
        this.onChange(newEditorState);
        return _constants.HANDLED;
      }

      return _constants.NOT_HANDLED;
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockRendererFn',
    value: function blockRendererFn(block) {
      switch (block.getType()) {
        case _constants.Blocks.ATOMIC:
          return {
            component: _Atomic2.default,
            editable: false
          };

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockStyleFn',
    value: function blockStyleFn(block) {
      switch (block.getType()) {
        case 'blockquote':
          return 'RichEditor-blockquote';

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          readOnly = _props.readOnly,
          onChange = _props.onChange,
          editorState = _props.editorState,
          placeholder = _props.placeholder;

      // Custom overrides for "code" style.

      var styleMap = {
        CODE: {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
          fontSize: 16,
          padding: 2
        }
      };

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        !readOnly ? _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-controls': '' }) },
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Title', id: 'title', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Title', onClick: this.onTitleClick },
              _react2.default.createElement(_Title2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Bold', id: 'bold', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Bold', onClick: this.onBoldClick },
              _react2.default.createElement(_FormatBold2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Italic', id: 'italic', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Italic', onClick: this.onItalicClick },
              _react2.default.createElement(_FormatItalic2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Underline', id: 'underline', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Underline',
                onClick: this.onUnderLineClick
              },
              _react2.default.createElement(_FormatUnderlined2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Code', id: 'code', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Code', onClick: this.onCodeClick },
              _react2.default.createElement(_Code2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Quote', id: 'quote', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Quote', onClick: this.onQuoteClick },
              _react2.default.createElement(_FormatQuote2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Unordered List',
              id: 'unorderd-list',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Unordered List',
                onClick: this.onListBulletedClick
              },
              _react2.default.createElement(_FormatListBulleted2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Ordered List', id: 'ordered-list', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Ordered List',
                onClick: this.onListNumberedClick
              },
              _react2.default.createElement(_FormatListNumbered2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Left', id: 'align-left', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Left', disabled: true },
              _react2.default.createElement(_FormatAlignLeft2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Center', id: 'align-center', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Center', disabled: true },
              _react2.default.createElement(_FormatAlignCenter2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignRight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignJustify2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Attach File', id: 'attach-file', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Attach File', disabled: true },
              _react2.default.createElement(_AttachFile2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Link', id: 'insert-link', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Link', disabled: true },
              _react2.default.createElement(_InsertLink2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Photo', id: 'insert-photo', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Insert Photo',
                disabled: typeof this.props.apiPhotoUpload !== 'function',
                onClick: this.onClickInsertPhoto
              },
              _react2.default.createElement(_InsertPhoto2.default, null),
              _react2.default.createElement('input', {
                type: 'file',
                accept: 'image/jpeg|png|gif',
                onChange: this.onChangeInsertPhoto,
                ref: function ref(photo) {
                  _this3.photo = photo;
                },
                style: { display: 'none' }
              })
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Emoticon',
              id: 'insertE-emoticon',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Emoticon', disabled: true },
              _react2.default.createElement(_InsertEmoticon2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Comment',
              id: 'insert-comment',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Comment', disabled: true },
              _react2.default.createElement(_InsertComment2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Highlight Text',
              id: 'highlight-text',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Highlight Text',
                onClick: this.onHighlightClick
              },
              _react2.default.createElement(_Highlight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Add Functions',
              id: 'add-functions',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Add Functions', disabled: true },
              _react2.default.createElement(_Functions2.default, null)
            )
          )
        ) : null,
        _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-area': '' }) },
          _react2.default.createElement(_draftJs.Editor
          /* Basics */

          , { editorState: editorState,
            onChange: onChange
            /* Presentation */

            , placeholder: placeholder
            // textAlignment="center"

            // textDirectionality="LTR"

            , blockRendererFn: this.blockRendererFn,
            blockStyleFn: this.blockStyleFn,
            customStyleMap: styleMap
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            , readOnly: readOnly,
            spellCheck: true
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            , handleKeyCommand: this.handleKeyCommand
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            , onTab: this.onTab
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            , ref: function ref(node) {
              _this3.editorNode = node;
            }
          })
        )
      );
    }
  }]);
  return MayashEditor;
}(_react.Component);

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

// import Icon from 'material-ui/Icon';
/** @format */

MayashEditor.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  editorState: _ReactPropTypes2.default.object.isRequired,
  onChange: _ReactPropTypes2.default.func.isRequired,

  placeholder: _ReactPropTypes2.default.string,

  readOnly: _ReactPropTypes2.default.bool.isRequired,

  /**
   * This api function should be applied for enabling photo adding
   * feature in Mayash Editor.
   */
  apiPhotoUpload: _ReactPropTypes2.default.func
};
MayashEditor.defaultProps = {
  readOnly: true,
  placeholder: 'Write Here...'
};
exports.default = (0, _withStyles2.default)(_EditorStyles2.default)(MayashEditor);

/***/ }),

/***/ "./src/lib/mayash-editor/EditorState.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/EditorState.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = undefined;

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Decorators = __webpack_require__(/*! ./components/Decorators */ "./src/lib/mayash-editor/components/Decorators.js");

/** @format */

var defaultDecorators = new _draftJs.CompositeDecorator([{
  strategy: _Decorators.handleStrategy,
  component: _Decorators.HandleSpan
}, {
  strategy: _Decorators.hashtagStrategy,
  component: _Decorators.HashtagSpan
}]);

var createEditorState = exports.createEditorState = function createEditorState() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var decorators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDecorators;

  if (content === null) {
    return _draftJs.EditorState.createEmpty(decorators);
  }
  return _draftJs.EditorState.createWithContent((0, _draftJs.convertFromRaw)(content), decorators);
};

exports.default = createEditorState;

/***/ }),

/***/ "./src/lib/mayash-editor/EditorStyles.js":
/*!***********************************************!*\
  !*** ./src/lib/mayash-editor/EditorStyles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the CSS-in-JS styles of Editor component.
 *
 * @format
 */

exports.default = {
  '@global': {
    '.RichEditor-root': {
      background: '#fff',
      border: '1px solid #ddd',
      fontFamily: "'Georgia', serif",
      fontSize: '14px',
      padding: '15px'
    },
    '.RichEditor-editor': {
      borderTop: '1px solid #ddd',
      cursor: 'text',
      fontSize: '16px',
      marginTop: '10px'
    },
    '.public-DraftEditorPlaceholder-root': {
      margin: '0 -15px -15px',
      padding: '15px'
    },
    '.public-DraftEditor-content': {
      margin: '0 -15px -15px',
      padding: '15px'
      // minHeight: '100px',
    },
    '.RichEditor-blockquote': {
      backgroundColor: '5px solid #eee',
      borderLeft: '5px solid #eee',
      color: '#666',
      fontFamily: "'Hoefler Text', 'Georgia', serif",
      fontStyle: 'italic',
      margin: '16px 0',
      padding: '10px 20px'
    },
    '.public-DraftStyleDefault-pre': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: "'Inconsolata', 'Menlo', 'Consolas', monospace",
      fontSize: '16px',
      padding: '20px'
    }
  },
  root: {
    // padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/***/ }),

/***/ "./src/lib/mayash-editor/components/Atomic.js":
/*!****************************************************!*\
  !*** ./src/lib/mayash-editor/components/Atomic.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = __webpack_require__(/*! ../constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial'
  }
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
/**
 *
 * @format
 */

var Atomic = function (_Component) {
  (0, _inherits3.default)(Atomic, _Component);

  function Atomic(props) {
    (0, _classCallCheck3.default)(this, Atomic);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Atomic.__proto__ || (0, _getPrototypeOf2.default)(Atomic)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Atomic, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          contentState = _props.contentState,
          block = _props.block;


      var entity = contentState.getEntity(block.getEntityAt(0));

      var _entity$getData = entity.getData(),
          src = _entity$getData.src;

      var type = entity.getType();

      if (type === _constants.Blocks.PHOTO) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('img', { alt: 'alt', src: src, style: styles.photo })
        );
      }

      return _react2.default.createElement('div', null);
    }
  }]);
  return Atomic;
}(_react.Component);

Atomic.propTypes = {
  contentState: _propTypes2.default.object.isRequired,
  block: _propTypes2.default.any.isRequired
};
exports.default = Atomic;

/***/ }),

/***/ "./src/lib/mayash-editor/components/Decorators.js":
/*!********************************************************!*\
  !*** ./src/lib/mayash-editor/components/Decorators.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashtagSpan = exports.HandleSpan = undefined;
exports.handleStrategy = handleStrategy;
exports.hashtagStrategy = hashtagStrategy;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _style = __webpack_require__(/*! ../style */ "./src/lib/mayash-editor/style/index.js");

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
// eslint is disabled here for now.
var HANDLE_REGEX = /\@[\w]+/g;
var HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function findWithRegex(regex, contentBlock, callback) {
  var text = contentBlock.getText();
  var matchArr = void 0,
      start = void 0;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

var HandleSpan = exports.HandleSpan = function HandleSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.handle },
    props.children
  );
};

var HashtagSpan = exports.HashtagSpan = function HashtagSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.hashtag },
    props.children
  );
};

exports.default = {
  handleStrategy: handleStrategy,
  HandleSpan: HandleSpan,

  hashtagStrategy: hashtagStrategy,
  HashtagSpan: HashtagSpan
};

/***/ }),

/***/ "./src/lib/mayash-editor/constants.js":
/*!********************************************!*\
  !*** ./src/lib/mayash-editor/constants.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
var Blocks = exports.Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video'
};

/**
 * @constant Inline
 */
var Inline = exports.Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT'
};

/**
 * @constant Entity
 */
var Entity = exports.Entity = {
  LINK: 'LINK'
};

/**
 * @constant HYPERLINK
 */
var HYPERLINK = exports.HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
var HANDLED = exports.HANDLED = 'handled';
var NOT_HANDLED = exports.NOT_HANDLED = 'not_handled';

exports.default = {
  Blocks: Blocks,
  Inline: Inline,
  Entity: Entity
};

/***/ }),

/***/ "./src/lib/mayash-editor/index.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-editor/index.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = exports.MayashEditor = undefined;

var _Editor = __webpack_require__(/*! ./Editor */ "./src/lib/mayash-editor/Editor.js");

Object.defineProperty(exports, 'MayashEditor', {
  enumerable: true,
  get: function get() {
    return _Editor.MayashEditor;
  }
});

var _EditorState = __webpack_require__(/*! ./EditorState */ "./src/lib/mayash-editor/EditorState.js");

Object.defineProperty(exports, 'createEditorState', {
  enumerable: true,
  get: function get() {
    return _EditorState.createEditorState;
  }
});

var _Editor2 = _interopRequireDefault(_Editor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Editor2.default;

/***/ }),

/***/ "./src/lib/mayash-editor/style/index.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/style/index.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @format */

var style = exports.style = {
  root: {
    padding: 20,
    width: 600
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10
  },
  button: {
    marginTop: 10,
    textAlign: 'center'
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override'
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)'
  }
};

exports.default = style;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnblJpZ2h0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2F2ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9TeWxsYWJ1cy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3R5bGVzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0F0b21pYy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9EZWNvcmF0b3JzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9zdHlsZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJ1cGRhdGUiLCJwYXlsb2FkIiwidHlwZSIsInVzZXJJZCIsImNvdXJzZUlkIiwidG9rZW4iLCJjb3ZlciIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJzeWxsYWJ1cyIsImNvdXJzZU1vZHVsZXMiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsImJvZHkiLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJ1cGRhdGVDb3Vyc2UiLCJzdHlsZXMiLCJ0aGVtZSIsInJvb3QiLCJwYWRkaW5nIiwiY2FyZCIsImJvcmRlclJhZGl1cyIsInRleHRBbGlnbiIsImZsZXhHcm93IiwiZmxleCIsImVkaXRCdXR0b24iLCJwb3NpdGlvbiIsImJyZWFrcG9pbnRzIiwidXAiLCJib3R0b20iLCJyaWdodCIsImRvd24iLCJkaXNwbGF5IiwiU3lsbGFidXMiLCJwcm9wcyIsIm9uTW91c2VFbnRlciIsInNldFN0YXRlIiwiaG92ZXIiLCJvbk1vdXNlTGVhdmUiLCJjb3Vyc2UiLCJzdGF0ZSIsImVkaXQiLCJvbkNoYW5nZSIsImJpbmQiLCJvbkVkaXQiLCJvblNhdmUiLCJlbGVtZW50cyIsInVzZXIiLCJpZCIsImdldEN1cnJlbnRDb250ZW50IiwiYWN0aW9uQ291cnNlVXBkYXRlIiwiY2xhc3NlcyIsImlzU2lnbmVkSW4iLCJhdXRob3JJZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJNYXlhc2hFZGl0b3IiLCJvbkNoYW5nZUluc2VydFBob3RvIiwiZSIsImZpbGUiLCJ0YXJnZXQiLCJmaWxlcyIsImluZGV4T2YiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiYXBpUGhvdG9VcGxvYWQiLCJzcmMiLCJwaG90b1VybCIsImVkaXRvclN0YXRlIiwiY29udGVudFN0YXRlIiwiY29udGVudFN0YXRlV2l0aEVudGl0eSIsImNyZWF0ZUVudGl0eSIsIlBIT1RPIiwiZW50aXR5S2V5IiwiZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkiLCJtaWRkbGVFZGl0b3JTdGF0ZSIsInNldCIsImN1cnJlbnRDb250ZW50IiwibmV3RWRpdG9yU3RhdGUiLCJpbnNlcnRBdG9taWNCbG9jayIsImZvY3VzIiwiZWRpdG9yTm9kZSIsIm9uVGFiIiwib25UaXRsZUNsaWNrIiwib25Db2RlQ2xpY2siLCJvblF1b3RlQ2xpY2siLCJvbkxpc3RCdWxsZXRlZENsaWNrIiwib25MaXN0TnVtYmVyZWRDbGljayIsIm9uQm9sZENsaWNrIiwib25JdGFsaWNDbGljayIsIm9uVW5kZXJMaW5lQ2xpY2siLCJvbkhpZ2hsaWdodENsaWNrIiwiaGFuZGxlS2V5Q29tbWFuZCIsIm9uQ2xpY2tJbnNlcnRQaG90byIsInByZXZlbnREZWZhdWx0IiwidG9nZ2xlQmxvY2tUeXBlIiwidG9nZ2xlSW5saW5lU3R5bGUiLCJwaG90byIsInZhbHVlIiwiY2xpY2siLCJjb21tYW5kIiwiYmxvY2siLCJnZXRUeXBlIiwiQVRPTUlDIiwiY29tcG9uZW50IiwiZWRpdGFibGUiLCJyZWFkT25seSIsInBsYWNlaG9sZGVyIiwic3R5bGVNYXAiLCJDT0RFIiwiYmFja2dyb3VuZENvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTaXplIiwiYmxvY2tSZW5kZXJlckZuIiwiYmxvY2tTdHlsZUZuIiwibm9kZSIsInN0cmluZyIsImJvb2wiLCJkZWZhdWx0UHJvcHMiLCJkZWZhdWx0RGVjb3JhdG9ycyIsInN0cmF0ZWd5IiwiY3JlYXRlRWRpdG9yU3RhdGUiLCJjb250ZW50IiwiZGVjb3JhdG9ycyIsImNyZWF0ZUVtcHR5IiwiY3JlYXRlV2l0aENvbnRlbnQiLCJiYWNrZ3JvdW5kIiwiYm9yZGVyIiwiYm9yZGVyVG9wIiwiY3Vyc29yIiwibWFyZ2luVG9wIiwibWFyZ2luIiwiYm9yZGVyTGVmdCIsImNvbG9yIiwiZm9udFN0eWxlIiwid2lkdGgiLCJ3aGl0ZVNwYWNlIiwiQXRvbWljIiwiZW50aXR5IiwiZ2V0RW50aXR5IiwiZ2V0RW50aXR5QXQiLCJnZXREYXRhIiwiYW55IiwiaGFuZGxlU3RyYXRlZ3kiLCJoYXNodGFnU3RyYXRlZ3kiLCJIQU5ETEVfUkVHRVgiLCJIQVNIVEFHX1JFR0VYIiwiZmluZFdpdGhSZWdleCIsInJlZ2V4IiwiY29udGVudEJsb2NrIiwiY2FsbGJhY2siLCJ0ZXh0IiwiZ2V0VGV4dCIsIm1hdGNoQXJyIiwic3RhcnQiLCJleGVjIiwiaW5kZXgiLCJsZW5ndGgiLCJIYW5kbGVTcGFuIiwiaGFuZGxlIiwiY2hpbGRyZW4iLCJIYXNodGFnU3BhbiIsImhhc2h0YWciLCJCbG9ja3MiLCJVTlNUWUxFRCIsIlBBUkFHUkFQSCIsIkgxIiwiSDIiLCJIMyIsIkg0IiwiSDUiLCJINiIsIk9MIiwiVUwiLCJCTE9DS1FVT1RFIiwiVklERU8iLCJJbmxpbmUiLCJCT0xEIiwiSVRBTElDIiwiU1RSSUtFVEhST1VHSCIsIlVOREVSTElORSIsIkhJR0hMSUdIVCIsIkVudGl0eSIsIkxJTksiLCJIWVBFUkxJTksiLCJIQU5ETEVEIiwiTk9UX0hBTkRMRUQiLCJzdHlsZSIsImVkaXRvciIsIm1pbkhlaWdodCIsImJ1dHRvbiIsImRpcmVjdGlvbiIsInVuaWNvZGVCaWRpIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb1FBQW9ROztBQUV0VDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0dBQWtHOztBQUVwSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsNkpBQTZKOztBQUUvTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsbUZBQW1GOztBQUVySTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG9DOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0ZBQWtGOztBQUVwSTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb0ZBQW9GOztBQUV0STtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsbUZBQW1GOztBQUVySTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsNk9BQTZPOztBQUUvUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsd0RBQXdEOztBQUUxRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsdVJBQXVSOztBQUV6VTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0pBQWtKOztBQUVwTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsZ0RBQWdEOztBQUVsRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDhCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsMkhBQTJIOztBQUU3SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0RBQWtEOztBQUVwRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb0lBQW9JOztBQUV0TDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsc0hBQXNIOztBQUV4SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QseVdBQXlXOztBQUUzWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsMk5BQTJOOztBQUU3UTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsZ0lBQWdJOztBQUVsTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDhCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsc0pBQXNKOztBQUV4TTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsK0JBQStCOztBQUVqRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHdCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STtBQUM1STs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RTtBQUM5RTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBLGFBQWEsMEJBQTBCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHdCQUF3QixjOzs7Ozs7Ozs7Ozs7O0FDck83RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7Ozs7QUNyQkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2ZBOztBQUVBOztBQUVBLG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUTtBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLDhEOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ1pBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix5Qzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUN6REE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixVQUFVLHFCQUFxQjtBQUM1RDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVEE7O0FBbUJBOzs7Ozs7Ozs7Ozs7OztBQWNBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLENBQWYsQyxDQXRDQTs7Ozs7a0JBd0NlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25CZjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBckJBOzs7Ozs7c0ZBdUNBO0FBQUEsUUFDRUcsTUFERixTQUNFQSxNQURGO0FBQUEsUUFFRUMsUUFGRixTQUVFQSxRQUZGO0FBQUEsUUFHRUMsS0FIRixTQUdFQSxLQUhGO0FBQUEsUUFJRUMsS0FKRixTQUlFQSxLQUpGO0FBQUEsUUFLRUMsS0FMRixTQUtFQSxLQUxGO0FBQUEsUUFNRUMsV0FORixTQU1FQSxXQU5GO0FBQUEsUUFPRUMsUUFQRixTQU9FQSxRQVBGO0FBQUEsUUFRRUMsYUFSRixTQVFFQSxhQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV1VDLGVBWFYsa0NBV3FDUixNQVhyQyxpQkFXdURDLFFBWHZEO0FBQUE7QUFBQSxtQkFhc0IsK0JBQU1PLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlVjtBQUhSLGVBRmtCO0FBTzNCVyxvQkFBTSx5QkFBZTtBQUNuQlYsNEJBRG1CO0FBRW5CQyw0QkFGbUI7QUFHbkJDLHdDQUhtQjtBQUluQkMsa0NBSm1CO0FBS25CQztBQUxtQixlQUFmO0FBUHFCLGFBQVgsQ0FidEI7O0FBQUE7QUFhVU8sZUFiVjtBQTZCWUMsa0JBN0JaLEdBNkJtQ0QsR0E3Qm5DLENBNkJZQyxNQTdCWixFQTZCb0JDLFVBN0JwQixHQTZCbUNGLEdBN0JuQyxDQTZCb0JFLFVBN0JwQjs7QUFBQSxrQkErQlFELFVBQVUsR0EvQmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdDYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWhDYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0N1QkYsSUFBSUssSUFBSixFQXRDdkI7O0FBQUE7QUFzQ1VBLGdCQXRDVjtBQUFBLHdFQXdDZ0JBLElBeENoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMENJQyxvQkFBUUYsS0FBUjs7QUExQ0osNkNBNENXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVDWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZUcsWTs7Ozs7QUFsQ2Y7Ozs7QUFDQTs7OztrQkFvRmVBLFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGZjs7OztBQUNBOzs7O0FBRUE7O0FBQ0E7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQUVBLElBQU1DLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxLQUFEO0FBQUE7O0FBQUEsU0FBWTtBQUN6QkMsVUFBTTtBQUNKQyxlQUFTO0FBREwsS0FEbUI7QUFJekJDLFVBQU07QUFDSkMsb0JBQWM7QUFEVixLQUptQjtBQU96QnZCLFdBQU87QUFDTHdCLGlCQUFXO0FBRE4sS0FQa0I7QUFVekJDLGNBQVU7QUFDUkMsWUFBTTtBQURFLEtBVmU7QUFhekJDO0FBQ0VDLGdCQUFVO0FBRFosa0RBRUdULE1BQU1VLFdBQU4sQ0FBa0JDLEVBQWxCLENBQXFCLElBQXJCLENBRkgsRUFFZ0M7QUFDNUJDLGNBQVEsTUFEb0I7QUFFNUJDLGFBQU87QUFGcUIsS0FGaEMsOENBTUdiLE1BQU1VLFdBQU4sQ0FBa0JJLElBQWxCLENBQXVCLElBQXZCLENBTkgsRUFNa0M7QUFDOUJGLGNBQVEsTUFEc0I7QUFFOUJDLGFBQU87QUFGdUIsS0FObEMsOENBVUdiLE1BQU1VLFdBQU4sQ0FBa0JJLElBQWxCLENBQXVCLElBQXZCLENBVkgsRUFVa0M7QUFDOUJGLGNBQVEsTUFEc0I7QUFFOUJDLGFBQU87QUFGdUIsS0FWbEMsOENBY0diLE1BQU1VLFdBQU4sQ0FBa0JJLElBQWxCLENBQXVCLElBQXZCLENBZEgsRUFja0M7QUFDOUJDLGVBQVMsTUFEcUI7QUFFOUJILGNBQVEsTUFGc0I7QUFHOUJDLGFBQU87QUFIdUIsS0FkbEM7QUFieUIsR0FBWjtBQUFBLENBQWYsQyxDQXhCQTs7SUEyRE1HLFE7OztBQUNKLG9CQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsMElBQ1hBLEtBRFc7O0FBQUEsVUFvRG5CQyxZQXBEbUIsR0FvREo7QUFBQSxhQUFNLE1BQUtDLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0FwREk7O0FBQUEsVUFxRG5CQyxZQXJEbUIsR0FxREo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0FyREk7O0FBQUEsUUFFVHJDLFFBRlMsR0FFSWtDLE1BQU1LLE1BRlYsQ0FFVHZDLFFBRlM7OztBQUlqQixVQUFLd0MsS0FBTCxHQUFhO0FBQ1hDLFlBQU0sS0FESzs7QUFHWHpDLGdCQUFVLHFDQUFrQkEsUUFBbEI7QUFIQyxLQUFiOztBQU1BLFVBQUswQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0MsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlELElBQVosT0FBZDtBQUNBLFVBQUtFLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlGLElBQVosT0FBZDtBQVppQjtBQWFsQjs7Ozs2QkFFUTNDLFEsRUFBVTtBQUNqQixXQUFLb0MsUUFBTCxDQUFjLEVBQUVwQyxrQkFBRixFQUFkO0FBQ0Q7Ozs2QkFDUTtBQUFBLFVBQ0N5QyxJQURELEdBQ1UsS0FBS0QsS0FEZixDQUNDQyxJQUREOztBQUVQLFdBQUtMLFFBQUwsQ0FBYyxFQUFFSyxNQUFNLENBQUNBLElBQVQsRUFBZDtBQUNEOzs7Ozs7Ozs7Ozs7eUJBR2dDLEtBQUtQLEssRUFBMUJZLFEsVUFBQUEsUSxFQUFVUCxNLFVBQUFBLE07aUNBQ1lPLFNBQVNDLEksRUFBM0JyRCxNLGtCQUFKc0QsRSxFQUFZcEQsSyxrQkFBQUEsSztBQUNaRCx3QixHQUFhNEMsTSxDQUFiNUMsUTtBQUVBOEMsb0IsR0FBUyxLQUFLRCxLLENBQWRDLEk7QUFFRnpDLHdCLEdBQVcsMkJBQWEsS0FBS3dDLEtBQUwsQ0FBV3hDLFFBQVgsQ0FBb0JpRCxpQkFBcEIsRUFBYixDOzt1QkFFbUIsc0JBQWdCO0FBQ2xEdkQsZ0NBRGtEO0FBRWxEQyxvQ0FGa0Q7QUFHbERDLDhCQUhrRDtBQUlsREk7QUFKa0QsaUJBQWhCLEM7Ozs7QUFBNUJXLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSzs7c0JBT2hCRCxlQUFlLEc7Ozs7O0FBQ2pCO0FBQ0FHLHdCQUFRRixLQUFSLENBQWNBLEtBQWQ7Ozs7O0FBSUYscUJBQUtzQixLQUFMLENBQVdnQixrQkFBWCxDQUE4QixFQUFFdkQsa0JBQUYsRUFBWUssa0JBQVosRUFBOUI7QUFDQSxxQkFBS29DLFFBQUwsQ0FBYyxFQUFFSyxNQUFNLENBQUNBLElBQVQsRUFBZDs7Ozs7Ozs7QUFFQTNCLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBT0s7QUFBQSxvQkFDK0IsS0FBS3NCLEtBRHBDO0FBQUEsVUFDQ2lCLE9BREQsV0FDQ0EsT0FERDtBQUFBLFVBQ1VMLFFBRFYsV0FDVUEsUUFEVjtBQUFBLFVBQ29CUCxNQURwQixXQUNvQkEsTUFEcEI7QUFBQSw0QkFFNEJPLFNBQVNDLElBRnJDO0FBQUEsVUFFQ0ssVUFGRCxtQkFFQ0EsVUFGRDtBQUFBLFVBRWlCMUQsTUFGakIsbUJBRWFzRCxFQUZiO0FBQUEsVUFHQ0ssUUFIRCxHQUdjZCxNQUhkLENBR0NjLFFBSEQ7QUFBQSxtQkFLb0IsS0FBS2IsS0FMekI7QUFBQSxVQUtDQyxJQUxELFVBS0NBLElBTEQ7QUFBQSxVQUtPekMsUUFMUCxVQUtPQSxRQUxQOzs7QUFPUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDLEVBQTZDLFdBQVdtRCxRQUFRakMsSUFBaEU7QUFDRTtBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLElBQUksRUFBdkIsRUFBMkIsSUFBSSxFQUEvQixFQUFtQyxJQUFJLENBQXZDLEVBQTBDLElBQUksQ0FBOUM7QUFDRTtBQUFBO0FBQUE7QUFDRSw0QkFBYyxLQUFLaUIsWUFEckI7QUFFRSw0QkFBYyxLQUFLRztBQUZyQjtBQUlFO0FBQUE7QUFBQSxnQkFBTSxRQUFRLEtBQUtFLEtBQUwsQ0FBV0gsS0FBekIsRUFBZ0MsV0FBV2MsUUFBUS9CLElBQW5EO0FBQ0UsZ0VBQVksT0FBTyxVQUFuQixFQUErQixXQUFXK0IsUUFBUXJELEtBQWxELEdBREY7QUFFRTtBQUFBO0FBQUE7QUFDRTtBQUNFLCtCQUFhRSxRQURmO0FBRUUsNEJBQVUsS0FBSzBDLFFBRmpCO0FBR0UsNEJBQVUsQ0FBQ0QsSUFIYjtBQUlFLCtCQUFhO0FBSmY7QUFERjtBQUZGO0FBSkYsV0FERjtBQWlCR1csd0JBQ0NDLGFBQWEzRCxNQURkLElBRUc7QUFBQTtBQUFBO0FBQ0UsdUJBREY7QUFFRSxxQkFBTSxRQUZSO0FBR0UsNEJBQVcsTUFIYjtBQUlFLHlCQUFXeUQsUUFBUTFCLFVBSnJCO0FBS0UsdUJBQVNnQixPQUFPLEtBQUtJLE1BQVosR0FBcUIsS0FBS0Q7QUFMckM7QUFPR0gsbUJBQU8sbURBQVAsR0FBc0I7QUFQekI7QUFuQk47QUFERixPQURGO0FBa0NEOzs7OztBQUdIUixTQUFTcUIsU0FBVCxHQUFxQjtBQUNuQkgsV0FBUyxvQkFBVUksTUFBVixDQUFpQkMsVUFEUDs7QUFHbkJWLFlBQVUsb0JBQVVTLE1BQVYsQ0FBaUJDLFVBSFI7QUFJbkJqQixVQUFRLG9CQUFVZ0IsTUFBVixDQUFpQkMsVUFKTjs7QUFNbkJOLHNCQUFvQixvQkFBVU8sSUFBVixDQUFlRDtBQU5oQixDQUFyQjs7QUFTQSxJQUFNRSxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR1osUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBbUIsRUFBRUEsa0JBQUYsRUFBbkI7QUFBQSxDQUF4Qjs7QUFFQSxJQUFNYSxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRVY7QUFERixHQURGLEVBSUVVLFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVFGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXM0MsTUFBWCxFQUFtQmlCLFFBQW5CLENBRGEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoTGY7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFHQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQVVBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQVFBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFrQkE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7O0FBaENBO0FBQ0E7QUFDQTs7QUFFQTs7QUE3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7SUFxRE00QixZOzs7QUFDSjs7O0FBeUJBLDBCQUFjO0FBQUE7O0FBQUE7O0FBRVo7O0FBRUE7Ozs7OztBQUpZOztBQUFBLFVBNkxkQyxtQkE3TGM7QUFBQSwwRkE2TFEsaUJBQU9DLENBQVA7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNwQjtBQUNNQyxvQkFGYyxHQUVQRCxFQUFFRSxNQUFGLENBQVNDLEtBQVQsQ0FBZSxDQUFmLENBRk87O0FBQUEsc0JBSWhCRixLQUFLdkUsSUFBTCxDQUFVMEUsT0FBVixDQUFrQixRQUFsQixNQUFnQyxDQUpoQjtBQUFBO0FBQUE7QUFBQTs7QUFLWkMsd0JBTFksR0FLRCxJQUFJQyxRQUFKLEVBTEM7O0FBTWxCRCx5QkFBU0UsTUFBVCxDQUFnQixPQUFoQixFQUF5Qk4sSUFBekI7O0FBTmtCO0FBQUEsdUJBUTJCLE1BQUs5QixLQUFMLENBQVdxQyxjQUFYLENBQTBCO0FBQ3JFSDtBQURxRSxpQkFBMUIsQ0FSM0I7O0FBQUE7QUFBQTtBQVFWekQsMEJBUlUsU0FRVkEsVUFSVTtBQVFFQyxxQkFSRixTQVFFQSxLQVJGO0FBUVNwQix1QkFSVCxTQVFTQSxPQVJUOztBQUFBLHNCQVlkbUIsY0FBYyxHQVpBO0FBQUE7QUFBQTtBQUFBOztBQWFoQjtBQUNBRyx3QkFBUUYsS0FBUixDQUFjRCxVQUFkLEVBQTBCQyxLQUExQjtBQWRnQjs7QUFBQTtBQWtCQTRELG1CQWxCQSxHQWtCUWhGLE9BbEJSLENBa0JWaUYsUUFsQlU7QUFvQlZDLDJCQXBCVSxHQW9CTSxNQUFLeEMsS0FwQlgsQ0FvQlZ3QyxXQXBCVTtBQXNCWkMsNEJBdEJZLEdBc0JHRCxZQUFZekIsaUJBQVosRUF0Qkg7QUF1QloyQixzQ0F2QlksR0F1QmFELGFBQWFFLFlBQWIsQ0FDN0Isa0JBQU9DLEtBRHNCLEVBRTdCLFdBRjZCLEVBRzdCLEVBQUVOLFFBQUYsRUFINkIsQ0F2QmI7QUE2QlpPLHlCQTdCWSxHQTZCQUgsdUJBQXVCSSx1QkFBdkIsRUE3QkE7QUErQlpDLGlDQS9CWSxHQStCUSxxQkFBWUMsR0FBWixDQUFnQlIsV0FBaEIsRUFBNkI7QUFDckRTLGtDQUFnQlA7QUFEcUMsaUJBQTdCLENBL0JSO0FBbUNaUSw4QkFuQ1ksR0FtQ0ssMEJBQWlCQyxpQkFBakIsQ0FDckJKLGlCQURxQixFQUVyQkYsU0FGcUIsRUFHckIsR0FIcUIsQ0FuQ0w7OztBQXlDbEIsc0JBQUtyQyxRQUFMLENBQWMwQyxjQUFkOztBQXpDa0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0E3TFI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBVVosVUFBSzFDLFFBQUwsR0FBZ0IsVUFBQ2dDLFdBQUQsRUFBaUI7QUFDL0IsWUFBS3hDLEtBQUwsQ0FBV1EsUUFBWCxDQUFvQmdDLFdBQXBCO0FBQ0QsS0FGRDs7QUFJQTs7OztBQUlBLFVBQUtZLEtBQUwsR0FBYTtBQUFBLGFBQU0sTUFBS0MsVUFBTCxDQUFnQkQsS0FBaEIsRUFBTjtBQUFBLEtBQWI7O0FBRUEsVUFBS0UsS0FBTCxHQUFhLE1BQUtBLEtBQUwsQ0FBVzdDLElBQVgsT0FBYjtBQUNBLFVBQUs4QyxZQUFMLEdBQW9CLE1BQUtBLFlBQUwsQ0FBa0I5QyxJQUFsQixPQUFwQjtBQUNBLFVBQUsrQyxXQUFMLEdBQW1CLE1BQUtBLFdBQUwsQ0FBaUIvQyxJQUFqQixPQUFuQjtBQUNBLFVBQUtnRCxZQUFMLEdBQW9CLE1BQUtBLFlBQUwsQ0FBa0JoRCxJQUFsQixPQUFwQjtBQUNBLFVBQUtpRCxtQkFBTCxHQUEyQixNQUFLQSxtQkFBTCxDQUF5QmpELElBQXpCLE9BQTNCO0FBQ0EsVUFBS2tELG1CQUFMLEdBQTJCLE1BQUtBLG1CQUFMLENBQXlCbEQsSUFBekIsT0FBM0I7QUFDQSxVQUFLbUQsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCbkQsSUFBakIsT0FBbkI7QUFDQSxVQUFLb0QsYUFBTCxHQUFxQixNQUFLQSxhQUFMLENBQW1CcEQsSUFBbkIsT0FBckI7QUFDQSxVQUFLcUQsZ0JBQUwsR0FBd0IsTUFBS0EsZ0JBQUwsQ0FBc0JyRCxJQUF0QixPQUF4QjtBQUNBLFVBQUtzRCxnQkFBTCxHQUF3QixNQUFLQSxnQkFBTCxDQUFzQnRELElBQXRCLE9BQXhCO0FBQ0EsVUFBS3VELGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCdkQsSUFBdEIsT0FBeEI7QUFDQSxVQUFLd0Qsa0JBQUwsR0FBMEIsTUFBS0Esa0JBQUwsQ0FBd0J4RCxJQUF4QixPQUExQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVDWTtBQTZDYjs7QUFFRDs7Ozs7Ozs7OzBCQUtNb0IsQyxFQUFHO0FBQ1BBLFFBQUVxQyxjQUFGO0FBRE8sVUFFQzFCLFdBRkQsR0FFaUIsS0FBS3hDLEtBRnRCLENBRUN3QyxXQUZEOzs7QUFJUCxVQUFNVSxpQkFBaUIsbUJBQVVJLEtBQVYsQ0FBZ0J6QixDQUFoQixFQUFtQlcsV0FBbkIsRUFBZ0MsQ0FBaEMsQ0FBdkI7O0FBRUEsV0FBS2hDLFFBQUwsQ0FBYzBDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzttQ0FJZTtBQUFBLFVBQ0xWLFdBREssR0FDVyxLQUFLeEMsS0FEaEIsQ0FDTHdDLFdBREs7OztBQUdiLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FBMEIzQixXQUExQixFQUF1QyxZQUF2QyxDQUF2Qjs7QUFFQSxXQUFLaEMsUUFBTCxDQUFjMEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O2tDQUljO0FBQUEsVUFDSlYsV0FESSxHQUNZLEtBQUt4QyxLQURqQixDQUNKd0MsV0FESTs7O0FBR1osVUFBTVUsaUJBQWlCLG1CQUFVaUIsZUFBVixDQUEwQjNCLFdBQTFCLEVBQXVDLFlBQXZDLENBQXZCOztBQUVBLFdBQUtoQyxRQUFMLENBQWMwQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7bUNBSWU7QUFBQSxVQUNMVixXQURLLEdBQ1csS0FBS3hDLEtBRGhCLENBQ0x3QyxXQURLOzs7QUFHYixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQTBCM0IsV0FBMUIsRUFBdUMsWUFBdkMsQ0FBdkI7O0FBRUEsV0FBS2hDLFFBQUwsQ0FBYzBDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzswQ0FJc0I7QUFBQSxVQUNaVixXQURZLEdBQ0ksS0FBS3hDLEtBRFQsQ0FDWndDLFdBRFk7OztBQUdwQixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQ3JCM0IsV0FEcUIsRUFFckIscUJBRnFCLENBQXZCOztBQUtBLFdBQUtoQyxRQUFMLENBQWMwQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7MENBSXNCO0FBQUEsVUFDWlYsV0FEWSxHQUNJLEtBQUt4QyxLQURULENBQ1p3QyxXQURZOzs7QUFHcEIsVUFBTVUsaUJBQWlCLG1CQUFVaUIsZUFBVixDQUNyQjNCLFdBRHFCLEVBRXJCLG1CQUZxQixDQUF2Qjs7QUFLQSxXQUFLaEMsUUFBTCxDQUFjMEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O2tDQUljO0FBQUEsVUFDSlYsV0FESSxHQUNZLEtBQUt4QyxLQURqQixDQUNKd0MsV0FESTs7O0FBR1osVUFBTVUsaUJBQWlCLG1CQUFVa0IsaUJBQVYsQ0FBNEI1QixXQUE1QixFQUF5QyxNQUF6QyxDQUF2Qjs7QUFFQSxXQUFLaEMsUUFBTCxDQUFjMEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O29DQUlnQjtBQUFBLFVBQ05WLFdBRE0sR0FDVSxLQUFLeEMsS0FEZixDQUNOd0MsV0FETTs7O0FBR2QsVUFBTVUsaUJBQWlCLG1CQUFVa0IsaUJBQVYsQ0FBNEI1QixXQUE1QixFQUF5QyxRQUF6QyxDQUF2Qjs7QUFFQSxXQUFLaEMsUUFBTCxDQUFjMEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O3VDQUltQjtBQUFBLFVBQ1RWLFdBRFMsR0FDTyxLQUFLeEMsS0FEWixDQUNUd0MsV0FEUzs7O0FBR2pCLFVBQU1VLGlCQUFpQixtQkFBVWtCLGlCQUFWLENBQ3JCNUIsV0FEcUIsRUFFckIsV0FGcUIsQ0FBdkI7O0FBS0EsV0FBS2hDLFFBQUwsQ0FBYzBDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7Ozt1Q0FJbUI7QUFBQSxVQUNUVixXQURTLEdBQ08sS0FBS3hDLEtBRFosQ0FDVHdDLFdBRFM7OztBQUdqQixVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLE1BQXpDLENBQXZCOztBQUVBLFdBQUtoQyxRQUFMLENBQWMwQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozt5Q0FHcUI7QUFDbkIsV0FBS21CLEtBQUwsQ0FBV0MsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUtELEtBQUwsQ0FBV0UsS0FBWDtBQUNEOztBQUVEOzs7Ozs7OztBQWdEQTs7Ozs7O3FDQU1pQkMsTyxFQUFTO0FBQUEsVUFDaEJoQyxXQURnQixHQUNBLEtBQUt4QyxLQURMLENBQ2hCd0MsV0FEZ0I7OztBQUd4QixVQUFNVSxpQkFBaUIsbUJBQVVjLGdCQUFWLENBQTJCeEIsV0FBM0IsRUFBd0NnQyxPQUF4QyxDQUF2Qjs7QUFFQSxVQUFJdEIsY0FBSixFQUFvQjtBQUNsQixhQUFLMUMsUUFBTCxDQUFjMEMsY0FBZDtBQUNBO0FBQ0Q7O0FBRUQ7QUFDRDs7QUFFRDs7OztvQ0FDZ0J1QixLLEVBQU87QUFDckIsY0FBUUEsTUFBTUMsT0FBTixFQUFSO0FBQ0UsYUFBSyxrQkFBT0MsTUFBWjtBQUNFLGlCQUFPO0FBQ0xDLHVDQURLO0FBRUxDLHNCQUFVO0FBRkwsV0FBUDs7QUFLRjtBQUNFLGlCQUFPLElBQVA7QUFSSjtBQVVEO0FBQ0Q7O0FBRUE7Ozs7aUNBQ2FKLEssRUFBTztBQUNsQixjQUFRQSxNQUFNQyxPQUFOLEVBQVI7QUFDRSxhQUFLLFlBQUw7QUFDRSxpQkFBTyx1QkFBUDs7QUFFRjtBQUNFLGlCQUFPLElBQVA7QUFMSjtBQU9EO0FBQ0Q7Ozs7NkJBRVM7QUFBQTs7QUFBQSxtQkFPSCxLQUFLMUUsS0FQRjtBQUFBLFVBRUxpQixPQUZLLFVBRUxBLE9BRks7QUFBQSxVQUdMNkQsUUFISyxVQUdMQSxRQUhLO0FBQUEsVUFJTHRFLFFBSkssVUFJTEEsUUFKSztBQUFBLFVBS0xnQyxXQUxLLFVBS0xBLFdBTEs7QUFBQSxVQU1MdUMsV0FOSyxVQU1MQSxXQU5LOztBQVNQOztBQUNBLFVBQU1DLFdBQVc7QUFDZkMsY0FBTTtBQUNKQywyQkFBaUIscUJBRGI7QUFFSkMsc0JBQVksK0NBRlI7QUFHSkMsb0JBQVUsRUFITjtBQUlKbkcsbUJBQVM7QUFKTDtBQURTLE9BQWpCOztBQVNBLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBV2dDLFFBQVFqQyxJQUF4QjtBQUNHLFNBQUM4RixRQUFELEdBQ0M7QUFBQTtBQUFBLFlBQUssV0FBVywwQkFBVyxFQUFFLG1CQUFtQixFQUFyQixFQUFYLENBQWhCO0FBQ0U7QUFBQTtBQUFBLGNBQVMsT0FBTSxPQUFmLEVBQXVCLElBQUcsT0FBMUIsRUFBa0MsV0FBVSxRQUE1QztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE9BQXZCLEVBQStCLFNBQVMsS0FBS3ZCLFlBQTdDO0FBQ0U7QUFERjtBQURGLFdBREY7QUFNRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE1BQWYsRUFBc0IsSUFBRyxNQUF6QixFQUFnQyxXQUFVLFFBQTFDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsTUFBdkIsRUFBOEIsU0FBUyxLQUFLSyxXQUE1QztBQUNFO0FBREY7QUFERixXQU5GO0FBV0U7QUFBQTtBQUFBLGNBQVMsT0FBTSxRQUFmLEVBQXdCLElBQUcsUUFBM0IsRUFBb0MsV0FBVSxRQUE5QztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLFFBQXZCLEVBQWdDLFNBQVMsS0FBS0MsYUFBOUM7QUFDRTtBQURGO0FBREYsV0FYRjtBQWdCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLFdBQWYsRUFBMkIsSUFBRyxXQUE5QixFQUEwQyxXQUFVLFFBQXBEO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsV0FEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQURGLFdBaEJGO0FBd0JFO0FBQUE7QUFBQSxjQUFTLE9BQU0sTUFBZixFQUFzQixJQUFHLE1BQXpCLEVBQWdDLFdBQVUsUUFBMUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxNQUF2QixFQUE4QixTQUFTLEtBQUtOLFdBQTVDO0FBQ0U7QUFERjtBQURGLFdBeEJGO0FBNkJFO0FBQUE7QUFBQSxjQUFTLE9BQU0sT0FBZixFQUF1QixJQUFHLE9BQTFCLEVBQWtDLFdBQVUsUUFBNUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QixFQUErQixTQUFTLEtBQUtDLFlBQTdDO0FBQ0U7QUFERjtBQURGLFdBN0JGO0FBa0NFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGdCQURSO0FBRUUsa0JBQUcsZUFGTDtBQUdFLHlCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxnQkFEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQUxGLFdBbENGO0FBOENFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxjQURiO0FBRUUseUJBQVMsS0FBS0M7QUFGaEI7QUFJRTtBQUpGO0FBREYsV0E5Q0Y7QUFzREU7QUFBQTtBQUFBLGNBQVMsT0FBTSxZQUFmLEVBQTRCLElBQUcsWUFBL0IsRUFBNEMsV0FBVSxRQUF0RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLFlBQXZCLEVBQW9DLGNBQXBDO0FBQ0U7QUFERjtBQURGLFdBdERGO0FBMkRFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxjQUF2QixFQUFzQyxjQUF0QztBQUNFO0FBREY7QUFERixXQTNERjtBQWdFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0FoRUY7QUFxRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBckVGO0FBMEVFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQTFFRjtBQStFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0EvRUY7QUFvRkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxjQUFmLEVBQThCLElBQUcsY0FBakMsRUFBZ0QsV0FBVSxRQUExRDtBQUNFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGNBRGI7QUFFRSwwQkFBVSxPQUFPLEtBQUszRCxLQUFMLENBQVdxQyxjQUFsQixLQUFxQyxVQUZqRDtBQUdFLHlCQUFTLEtBQUs0QjtBQUhoQjtBQUtFLHdFQUxGO0FBTUU7QUFDRSxzQkFBSyxNQURQO0FBRUUsd0JBQU8sb0JBRlQ7QUFHRSwwQkFBVSxLQUFLckMsbUJBSGpCO0FBSUUscUJBQUssYUFBQ3lDLEtBQUQsRUFBVztBQUNkLHlCQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDRCxpQkFOSDtBQU9FLHVCQUFPLEVBQUV2RSxTQUFTLE1BQVg7QUFQVDtBQU5GO0FBREYsV0FwRkY7QUFzR0U7QUFBQTtBQUFBO0FBQ0UscUJBQU0saUJBRFI7QUFFRSxrQkFBRyxrQkFGTDtBQUdFLHlCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxpQkFBdkIsRUFBeUMsY0FBekM7QUFDRTtBQURGO0FBTEYsV0F0R0Y7QUErR0U7QUFBQTtBQUFBO0FBQ0UscUJBQU0sZ0JBRFI7QUFFRSxrQkFBRyxnQkFGTDtBQUdFLHlCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxnQkFBdkIsRUFBd0MsY0FBeEM7QUFDRTtBQURGO0FBTEYsV0EvR0Y7QUF3SEU7QUFBQTtBQUFBO0FBQ0UscUJBQU0sZ0JBRFI7QUFFRSxrQkFBRyxnQkFGTDtBQUdFLHlCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxnQkFEYjtBQUVFLHlCQUFTLEtBQUtpRTtBQUZoQjtBQUlFO0FBSkY7QUFMRixXQXhIRjtBQW9JRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxlQURSO0FBRUUsa0JBQUcsZUFGTDtBQUdFLHlCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxlQUF2QixFQUF1QyxjQUF2QztBQUNFO0FBREY7QUFMRjtBQXBJRixTQURELEdBK0lHLElBaEpOO0FBaUpFO0FBQUE7QUFBQSxZQUFLLFdBQVcsMEJBQVcsRUFBRSxlQUFlLEVBQWpCLEVBQVgsQ0FBaEI7QUFDRTtBQUNFOztBQURGLGNBR0UsYUFBYXZCLFdBSGY7QUFJRSxzQkFBVWhDO0FBQ1Y7O0FBTEYsY0FPRSxhQUFhdUU7QUFDYjs7QUFFQTs7QUFWRixjQVlFLGlCQUFpQixLQUFLTSxlQVp4QjtBQWFFLDBCQUFjLEtBQUtDLFlBYnJCO0FBY0UsNEJBQWdCTjtBQUNoQjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUF2QkYsY0F5QkUsVUFBVUYsUUF6Qlo7QUEwQkU7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFuQ0YsY0FxQ0Usa0JBQWtCLEtBQUtkO0FBQ3ZCOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQWxERixjQW9ERSxPQUFPLEtBQUtWO0FBQ1o7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBM0VGLGNBNkVFLEtBQUssYUFBQ2lDLElBQUQsRUFBVTtBQUNiLHFCQUFLbEMsVUFBTCxHQUFrQmtDLElBQWxCO0FBQ0Q7QUEvRUg7QUFERjtBQWpKRixPQURGO0FBdU9EOzs7OztBQTVqQkg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUEzREE7QUFSQTs7QUE2RU01RCxZLENBSUdQLFMsR0FBWTtBQUNqQkgsV0FBUyx5QkFBVUksTUFBVixDQUFpQkMsVUFEVDs7QUFHakJrQixlQUFhLHlCQUFVbkIsTUFBVixDQUFpQkMsVUFIYjtBQUlqQmQsWUFBVSx5QkFBVWUsSUFBVixDQUFlRCxVQUpSOztBQU1qQnlELGVBQWEseUJBQVVTLE1BTk47O0FBUWpCVixZQUFVLHlCQUFVVyxJQUFWLENBQWVuRSxVQVJSOztBQVVqQjs7OztBQUlBZSxrQkFBZ0IseUJBQVVkO0FBZFQsQztBQUpmSSxZLENBcUJHK0QsWSxHQUFlO0FBQ3BCWixZQUFVLElBRFU7QUFFcEJDLGVBQWE7QUFGTyxDO2tCQTBoQlQsa0RBQW1CcEQsWUFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxbkJmOztBQUVBOztBQUpBOztBQVdBLElBQU1nRSxvQkFBb0IsZ0NBQXVCLENBQy9DO0FBQ0VDLHNDQURGO0FBRUVoQjtBQUZGLENBRCtDLEVBSy9DO0FBQ0VnQix1Q0FERjtBQUVFaEI7QUFGRixDQUwrQyxDQUF2QixDQUExQjs7QUFXTyxJQUFNaUIsZ0RBQW9CLFNBQXBCQSxpQkFBb0IsR0FHNUI7QUFBQSxNQUZIQyxPQUVHLHVFQUZPLElBRVA7QUFBQSxNQURIQyxVQUNHLHVFQURVSixpQkFDVjs7QUFDSCxNQUFJRyxZQUFZLElBQWhCLEVBQXNCO0FBQ3BCLFdBQU8scUJBQVlFLFdBQVosQ0FBd0JELFVBQXhCLENBQVA7QUFDRDtBQUNELFNBQU8scUJBQVlFLGlCQUFaLENBQThCLDZCQUFlSCxPQUFmLENBQTlCLEVBQXVEQyxVQUF2RCxDQUFQO0FBQ0QsQ0FSTTs7a0JBVVFGLGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQ2Y7Ozs7OztrQkFNZTtBQUNiLGFBQVc7QUFDVCx3QkFBb0I7QUFDbEJLLGtCQUFZLE1BRE07QUFFbEJDLGNBQVEsZ0JBRlU7QUFHbEJoQixrQkFBWSxrQkFITTtBQUlsQkMsZ0JBQVUsTUFKUTtBQUtsQm5HLGVBQVM7QUFMUyxLQURYO0FBUVQsMEJBQXNCO0FBQ3BCbUgsaUJBQVcsZ0JBRFM7QUFFcEJDLGNBQVEsTUFGWTtBQUdwQmpCLGdCQUFVLE1BSFU7QUFJcEJrQixpQkFBVztBQUpTLEtBUmI7QUFjVCwyQ0FBdUM7QUFDckNDLGNBQVEsZUFENkI7QUFFckN0SCxlQUFTO0FBRjRCLEtBZDlCO0FBa0JULG1DQUErQjtBQUM3QnNILGNBQVEsZUFEcUI7QUFFN0J0SCxlQUFTO0FBQ1Q7QUFINkIsS0FsQnRCO0FBdUJULDhCQUEwQjtBQUN4QmlHLHVCQUFpQixnQkFETztBQUV4QnNCLGtCQUFZLGdCQUZZO0FBR3hCQyxhQUFPLE1BSGlCO0FBSXhCdEIsa0JBQVksa0NBSlk7QUFLeEJ1QixpQkFBVyxRQUxhO0FBTXhCSCxjQUFRLFFBTmdCO0FBT3hCdEgsZUFBUztBQVBlLEtBdkJqQjtBQWdDVCxxQ0FBaUM7QUFDL0JpRyx1QkFBaUIscUJBRGM7QUFFL0JDLGtCQUFZLCtDQUZtQjtBQUcvQkMsZ0JBQVUsTUFIcUI7QUFJL0JuRyxlQUFTO0FBSnNCO0FBaEN4QixHQURFO0FBd0NiRCxRQUFNO0FBQ0o7QUFESSxHQXhDTztBQTJDYkssWUFBVTtBQUNSQyxVQUFNO0FBREU7QUEzQ0csQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUEsSUFBTVIsU0FBUztBQUNidUYsU0FBTztBQUNMc0MsV0FBTyxNQURGO0FBRUw7QUFDQTtBQUNBQyxnQkFBWTtBQUpQO0FBRE0sQ0FBZjs7QUFTQTs7Ozs7Ozs7QUFuQkE7Ozs7O0lBMkJNQyxNOzs7QUFNSixrQkFBWTdHLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxzSUFDWEEsS0FEVzs7QUFFakIsVUFBS00sS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQSxtQkFDeUIsS0FBS04sS0FEOUI7QUFBQSxVQUNDeUMsWUFERCxVQUNDQSxZQUREO0FBQUEsVUFDZWdDLEtBRGYsVUFDZUEsS0FEZjs7O0FBR1AsVUFBTXFDLFNBQVNyRSxhQUFhc0UsU0FBYixDQUF1QnRDLE1BQU11QyxXQUFOLENBQWtCLENBQWxCLENBQXZCLENBQWY7O0FBSE8sNEJBSVNGLE9BQU9HLE9BQVAsRUFKVDtBQUFBLFVBSUMzRSxHQUpELG1CQUlDQSxHQUpEOztBQUtQLFVBQU0vRSxPQUFPdUosT0FBT3BDLE9BQVAsRUFBYjs7QUFFQSxVQUFJbkgsU0FBUyxrQkFBT3FGLEtBQXBCLEVBQTJCO0FBQ3pCLGVBQ0U7QUFBQTtBQUFBO0FBQ0UsaURBQUssS0FBSyxLQUFWLEVBQWlCLEtBQUtOLEdBQXRCLEVBQTJCLE9BQU94RCxPQUFPdUYsS0FBekM7QUFERixTQURGO0FBS0Q7O0FBRUQsYUFBTywwQ0FBUDtBQUNEOzs7OztBQTNCR3dDLE0sQ0FDR3pGLFMsR0FBWTtBQUNqQnFCLGdCQUFjLG9CQUFVcEIsTUFBVixDQUFpQkMsVUFEZDtBQUVqQm1ELFNBQU8sb0JBQVV5QyxHQUFWLENBQWM1RjtBQUZKLEM7a0JBNkJOdUYsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQ3ZDQ00sYyxHQUFBQSxjO1FBSUFDLGUsR0FBQUEsZTs7QUFwQmhCOzs7O0FBQ0E7Ozs7OztBQUhBO0FBQ0E7QUFJQSxJQUFNQyxlQUFlLFVBQXJCO0FBQ0EsSUFBTUMsZ0JBQWdCLHVCQUF0Qjs7QUFFQSxTQUFTQyxhQUFULENBQXVCQyxLQUF2QixFQUE4QkMsWUFBOUIsRUFBNENDLFFBQTVDLEVBQXNEO0FBQ3BELE1BQU1DLE9BQU9GLGFBQWFHLE9BQWIsRUFBYjtBQUNBLE1BQUlDLGlCQUFKO0FBQUEsTUFDRUMsY0FERjtBQUVBLFNBQU8sQ0FBQ0QsV0FBV0wsTUFBTU8sSUFBTixDQUFXSixJQUFYLENBQVosTUFBa0MsSUFBekMsRUFBK0M7QUFDN0NHLFlBQVFELFNBQVNHLEtBQWpCO0FBQ0FOLGFBQVNJLEtBQVQsRUFBZ0JBLFFBQVFELFNBQVMsQ0FBVCxFQUFZSSxNQUFwQztBQUNEO0FBQ0Y7O0FBRU0sU0FBU2QsY0FBVCxDQUF3Qk0sWUFBeEIsRUFBc0NDLFFBQXRDLEVBQWdEakYsWUFBaEQsRUFBOEQ7QUFDbkU4RSxnQkFBY0YsWUFBZCxFQUE0QkksWUFBNUIsRUFBMENDLFFBQTFDO0FBQ0Q7O0FBRU0sU0FBU04sZUFBVCxDQUF5QkssWUFBekIsRUFBdUNDLFFBQXZDLEVBQWlEakYsWUFBakQsRUFBK0Q7QUFDcEU4RSxnQkFBY0QsYUFBZCxFQUE2QkcsWUFBN0IsRUFBMkNDLFFBQTNDO0FBQ0Q7O0FBRU0sSUFBTVEsa0NBQWEsU0FBYkEsVUFBYTtBQUFBLFNBQVM7QUFBQTtBQUFBLE1BQU0sT0FBTyxnQkFBTUMsTUFBbkI7QUFBNEJuSSxVQUFNb0k7QUFBbEMsR0FBVDtBQUFBLENBQW5COztBQUVBLElBQU1DLG9DQUFjLFNBQWRBLFdBQWM7QUFBQSxTQUFTO0FBQUE7QUFBQSxNQUFNLE9BQU8sZ0JBQU1DLE9BQW5CO0FBQTZCdEksVUFBTW9JO0FBQW5DLEdBQVQ7QUFBQSxDQUFwQjs7a0JBR1E7QUFDYmpCLGdDQURhO0FBRWJlLHdCQUZhOztBQUliZCxrQ0FKYTtBQUtiaUI7QUFMYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQmY7Ozs7Ozs7QUFPQTs7O0FBR08sSUFBTUUsMEJBQVM7QUFDcEJDLFlBQVUsVUFEVTtBQUVwQkMsYUFBVyxVQUZTOztBQUlwQkMsTUFBSSxZQUpnQjtBQUtwQkMsTUFBSSxZQUxnQjtBQU1wQkMsTUFBSSxjQU5nQjtBQU9wQkMsTUFBSSxhQVBnQjtBQVFwQkMsTUFBSSxhQVJnQjtBQVNwQkMsTUFBSSxZQVRnQjs7QUFXcEJDLE1BQUksbUJBWGdCO0FBWXBCQyxNQUFJLHFCQVpnQjs7QUFjcEJoRSxRQUFNLFlBZGM7O0FBZ0JwQmlFLGNBQVksWUFoQlE7O0FBa0JwQnZFLFVBQVEsUUFsQlk7QUFtQnBCL0IsU0FBTyxjQW5CYTtBQW9CcEJ1RyxTQUFPO0FBcEJhLENBQWY7O0FBdUJQOzs7QUFHTyxJQUFNQywwQkFBUztBQUNwQkMsUUFBTSxNQURjO0FBRXBCcEUsUUFBTSxNQUZjO0FBR3BCcUUsVUFBUSxRQUhZO0FBSXBCQyxpQkFBZSxlQUpLO0FBS3BCQyxhQUFXLFdBTFM7QUFNcEJDLGFBQVc7QUFOUyxDQUFmOztBQVNQOzs7QUFHTyxJQUFNQywwQkFBUztBQUNwQkMsUUFBTTtBQURjLENBQWY7O0FBSVA7OztBQUdPLElBQU1DLGdDQUFZLFdBQWxCOztBQUVQOzs7QUFHTyxJQUFNQyw0QkFBVSxTQUFoQjtBQUNBLElBQU1DLG9DQUFjLGFBQXBCOztrQkFFUTtBQUNidkIsZ0JBRGE7QUFFYmEsZ0JBRmE7QUFHYk07QUFIYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdEZjs7Ozs7bUJBRVMvSCxZOzs7Ozs7Ozs7d0JBRUFrRSxpQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOVDs7QUFFTyxJQUFNa0Usd0JBQVE7QUFDbkIvSyxRQUFNO0FBQ0pDLGFBQVMsRUFETDtBQUVKMEgsV0FBTztBQUZILEdBRGE7QUFLbkJxRCxVQUFRO0FBQ043RCxZQUFRLGdCQURGO0FBRU5FLFlBQVEsTUFGRjtBQUdOakIsY0FBVSxFQUhKO0FBSU42RSxlQUFXLEVBSkw7QUFLTmhMLGFBQVM7QUFMSCxHQUxXO0FBWW5CaUwsVUFBUTtBQUNONUQsZUFBVyxFQURMO0FBRU5sSCxlQUFXO0FBRkwsR0FaVztBQWdCbkIrSSxVQUFRO0FBQ04xQixXQUFPLHlCQUREO0FBRU4wRCxlQUFXLEtBRkw7QUFHTkMsaUJBQWE7QUFIUCxHQWhCVztBQXFCbkI5QixXQUFTO0FBQ1A3QixXQUFPO0FBREE7QUFyQlUsQ0FBZDs7a0JBMEJRc0QsSyIsImZpbGUiOiI0NC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNi41IDZ2MTEuNWMwIDIuMjEtMS43OSA0LTQgNHMtNC0xLjc5LTQtNFY1YzAtMS4zOCAxLjEyLTIuNSAyLjUtMi41czIuNSAxLjEyIDIuNSAyLjV2MTAuNWMwIC41NS0uNDUgMS0xIDFzLTEtLjQ1LTEtMVY2SDEwdjkuNWMwIDEuMzggMS4xMiAyLjUgMi41IDIuNXMyLjUtMS4xMiAyLjUtMi41VjVjMC0yLjIxLTEuNzktNC00LTRTNyAyLjc5IDcgNXYxMi41YzAgMy4wNCAyLjQ2IDUuNSA1LjUgNS41czUuNS0yLjQ2IDUuNS01LjVWNmgtMS41eicgfSk7XG5cbnZhciBBdHRhY2hGaWxlID0gZnVuY3Rpb24gQXR0YWNoRmlsZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQXR0YWNoRmlsZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQXR0YWNoRmlsZSk7XG5BdHRhY2hGaWxlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEF0dGFjaEZpbGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ005LjQgMTYuNkw0LjggMTJsNC42LTQuNkw4IDZsLTYgNiA2IDYgMS40LTEuNHptNS4yIDBsNC42LTQuNi00LjYtNC42TDE2IDZsNiA2LTYgNi0xLjQtMS40eicgfSk7XG5cbnZhciBDb2RlID0gZnVuY3Rpb24gQ29kZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQ29kZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQ29kZSk7XG5Db2RlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvZGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zIDE3LjI1VjIxaDMuNzVMMTcuODEgOS45NGwtMy43NS0zLjc1TDMgMTcuMjV6TTIwLjcxIDcuMDRjLjM5LS4zOS4zOS0xLjAyIDAtMS40MWwtMi4zNC0yLjM0Yy0uMzktLjM5LTEuMDItLjM5LTEuNDEgMGwtMS44MyAxLjgzIDMuNzUgMy43NSAxLjgzLTEuODN6JyB9KTtcblxudmFyIEVkaXQgPSBmdW5jdGlvbiBFZGl0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5FZGl0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShFZGl0KTtcbkVkaXQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRWRpdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FZGl0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FZGl0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjYgMzEgMzQgMzggNDQgNDUgNDcgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ003IDE1djJoMTB2LTJIN3ptLTQgNmgxOHYtMkgzdjJ6bTAtOGgxOHYtMkgzdjJ6bTQtNnYyaDEwVjdIN3pNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkNlbnRlciA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduQ2VudGVyKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRBbGlnbkNlbnRlciA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25DZW50ZXIpO1xuRm9ybWF0QWxpZ25DZW50ZXIubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0QWxpZ25DZW50ZXI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMgMjFoMTh2LTJIM3Yyem0wLTRoMTh2LTJIM3Yyem0wLTRoMTh2LTJIM3Yyem0wLTRoMThWN0gzdjJ6bTAtNnYyaDE4VjNIM3onIH0pO1xuXG52YXIgRm9ybWF0QWxpZ25KdXN0aWZ5ID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25KdXN0aWZ5KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRBbGlnbkp1c3RpZnkgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEFsaWduSnVzdGlmeSk7XG5Gb3JtYXRBbGlnbkp1c3RpZnkubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0QWxpZ25KdXN0aWZ5O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE1IDE1SDN2MmgxMnYtMnptMC04SDN2MmgxMlY3ek0zIDEzaDE4di0ySDN2MnptMCA4aDE4di0ySDN2MnpNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkxlZnQgPSBmdW5jdGlvbiBGb3JtYXRBbGlnbkxlZnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduTGVmdCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25MZWZ0KTtcbkZvcm1hdEFsaWduTGVmdC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkxlZnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAyMWgxOHYtMkgzdjJ6bTYtNGgxMnYtMkg5djJ6bS02LTRoMTh2LTJIM3Yyem02LTRoMTJWN0g5djJ6TTMgM3YyaDE4VjNIM3onIH0pO1xuXG52YXIgRm9ybWF0QWxpZ25SaWdodCA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduUmlnaHQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduUmlnaHQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEFsaWduUmlnaHQpO1xuRm9ybWF0QWxpZ25SaWdodC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnblJpZ2h0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTUuNiAxMC43OWMuOTctLjY3IDEuNjUtMS43NyAxLjY1LTIuNzkgMC0yLjI2LTEuNzUtNC00LTRIN3YxNGg3LjA0YzIuMDkgMCAzLjcxLTEuNyAzLjcxLTMuNzkgMC0xLjUyLS44Ni0yLjgyLTIuMTUtMy40MnpNMTAgNi41aDNjLjgzIDAgMS41LjY3IDEuNSAxLjVzLS42NyAxLjUtMS41IDEuNWgtM3YtM3ptMy41IDlIMTB2LTNoMy41Yy44MyAwIDEuNS42NyAxLjUgMS41cy0uNjcgMS41LTEuNSAxLjV6JyB9KTtcblxudmFyIEZvcm1hdEJvbGQgPSBmdW5jdGlvbiBGb3JtYXRCb2xkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRCb2xkID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRCb2xkKTtcbkZvcm1hdEJvbGQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0Qm9sZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEwIDR2M2gyLjIxbC0zLjQyIDhINnYzaDh2LTNoLTIuMjFsMy40Mi04SDE4VjR6JyB9KTtcblxudmFyIEZvcm1hdEl0YWxpYyA9IGZ1bmN0aW9uIEZvcm1hdEl0YWxpYyhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0SXRhbGljID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRJdGFsaWMpO1xuRm9ybWF0SXRhbGljLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEl0YWxpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ000IDEwLjVjLS44MyAwLTEuNS42Ny0xLjUgMS41cy42NyAxLjUgMS41IDEuNSAxLjUtLjY3IDEuNS0xLjUtLjY3LTEuNS0xLjUtMS41em0wLTZjLS44MyAwLTEuNS42Ny0xLjUgMS41UzMuMTcgNy41IDQgNy41IDUuNSA2LjgzIDUuNSA2IDQuODMgNC41IDQgNC41em0wIDEyYy0uODMgMC0xLjUuNjgtMS41IDEuNXMuNjggMS41IDEuNSAxLjUgMS41LS42OCAxLjUtMS41LS42Ny0xLjUtMS41LTEuNXpNNyAxOWgxNHYtMkg3djJ6bTAtNmgxNHYtMkg3djJ6bTAtOHYyaDE0VjVIN3onIH0pO1xuXG52YXIgRm9ybWF0TGlzdEJ1bGxldGVkID0gZnVuY3Rpb24gRm9ybWF0TGlzdEJ1bGxldGVkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRMaXN0QnVsbGV0ZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdExpc3RCdWxsZXRlZCk7XG5Gb3JtYXRMaXN0QnVsbGV0ZWQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0TGlzdEJ1bGxldGVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTIgMTdoMnYuNUgzdjFoMXYuNUgydjFoM3YtNEgydjF6bTEtOWgxVjRIMnYxaDF2M3ptLTEgM2gxLjhMMiAxMy4xdi45aDN2LTFIMy4yTDUgMTAuOVYxMEgydjF6bTUtNnYyaDE0VjVIN3ptMCAxNGgxNHYtMkg3djJ6bTAtNmgxNHYtMkg3djJ6JyB9KTtcblxudmFyIEZvcm1hdExpc3ROdW1iZXJlZCA9IGZ1bmN0aW9uIEZvcm1hdExpc3ROdW1iZXJlZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0TGlzdE51bWJlcmVkID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRMaXN0TnVtYmVyZWQpO1xuRm9ybWF0TGlzdE51bWJlcmVkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdExpc3ROdW1iZXJlZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ002IDE3aDNsMi00VjdINXY2aDN6bTggMGgzbDItNFY3aC02djZoM3onIH0pO1xuXG52YXIgRm9ybWF0UXVvdGUgPSBmdW5jdGlvbiBGb3JtYXRRdW90ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0UXVvdGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdFF1b3RlKTtcbkZvcm1hdFF1b3RlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdFF1b3RlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiAxN2MzLjMxIDAgNi0yLjY5IDYtNlYzaC0yLjV2OGMwIDEuOTMtMS41NyAzLjUtMy41IDMuNVM4LjUgMTIuOTMgOC41IDExVjNINnY4YzAgMy4zMSAyLjY5IDYgNiA2em0tNyAydjJoMTR2LTJINXonIH0pO1xuXG52YXIgRm9ybWF0VW5kZXJsaW5lZCA9IGZ1bmN0aW9uIEZvcm1hdFVuZGVybGluZWQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdFVuZGVybGluZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdFVuZGVybGluZWQpO1xuRm9ybWF0VW5kZXJsaW5lZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRVbmRlcmxpbmVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTggNEg2djJsNi41IDZMNiAxOHYyaDEydi0zaC03bDUtNS01LTVoN3onIH0pO1xuXG52YXIgRnVuY3Rpb25zID0gZnVuY3Rpb24gRnVuY3Rpb25zKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5GdW5jdGlvbnMgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZ1bmN0aW9ucyk7XG5GdW5jdGlvbnMubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRnVuY3Rpb25zO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYgMTRsMyAzdjVoNnYtNWwzLTNWOUg2em01LTEyaDJ2M2gtMnpNMy41IDUuODc1TDQuOTE0IDQuNDZsMi4xMiAyLjEyMkw1LjYyIDcuOTk3em0xMy40Ni43MWwyLjEyMy0yLjEyIDEuNDE0IDEuNDE0TDE4LjM3NSA4eicgfSk7XG5cbnZhciBIaWdobGlnaHQgPSBmdW5jdGlvbiBIaWdobGlnaHQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkhpZ2hsaWdodCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSGlnaGxpZ2h0KTtcbkhpZ2hsaWdodC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBIaWdobGlnaHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjAgMkg0Yy0xLjEgMC0yIC45LTIgMnYxMmMwIDEuMS45IDIgMiAyaDE0bDQgNFY0YzAtMS4xLS45LTItMi0yem0tMiAxMkg2di0yaDEydjJ6bTAtM0g2VjloMTJ2MnptMC0zSDZWNmgxMnYyeicgfSk7XG5cbnZhciBJbnNlcnRDb21tZW50ID0gZnVuY3Rpb24gSW5zZXJ0Q29tbWVudChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0Q29tbWVudCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSW5zZXJ0Q29tbWVudCk7XG5JbnNlcnRDb21tZW50Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydENvbW1lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0Q29tbWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0Q29tbWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMS45OSAyQzYuNDcgMiAyIDYuNDggMiAxMnM0LjQ3IDEwIDkuOTkgMTBDMTcuNTIgMjIgMjIgMTcuNTIgMjIgMTJTMTcuNTIgMiAxMS45OSAyek0xMiAyMGMtNC40MiAwLTgtMy41OC04LThzMy41OC04IDgtOCA4IDMuNTggOCA4LTMuNTggOC04IDh6bTMuNS05Yy44MyAwIDEuNS0uNjcgMS41LTEuNVMxNi4zMyA4IDE1LjUgOCAxNCA4LjY3IDE0IDkuNXMuNjcgMS41IDEuNSAxLjV6bS03IDBjLjgzIDAgMS41LS42NyAxLjUtMS41UzkuMzMgOCA4LjUgOCA3IDguNjcgNyA5LjUgNy42NyAxMSA4LjUgMTF6bTMuNSA2LjVjMi4zMyAwIDQuMzEtMS40NiA1LjExLTMuNUg2Ljg5Yy44IDIuMDQgMi43OCAzLjUgNS4xMSAzLjV6JyB9KTtcblxudmFyIEluc2VydEVtb3RpY29uID0gZnVuY3Rpb24gSW5zZXJ0RW1vdGljb24ocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydEVtb3RpY29uID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRFbW90aWNvbik7XG5JbnNlcnRFbW90aWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRFbW90aWNvbjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRFbW90aWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMy45IDEyYzAtMS43MSAxLjM5LTMuMSAzLjEtMy4xaDRWN0g3Yy0yLjc2IDAtNSAyLjI0LTUgNXMyLjI0IDUgNSA1aDR2LTEuOUg3Yy0xLjcxIDAtMy4xLTEuMzktMy4xLTMuMXpNOCAxM2g4di0ySDh2MnptOS02aC00djEuOWg0YzEuNzEgMCAzLjEgMS4zOSAzLjEgMy4xcy0xLjM5IDMuMS0zLjEgMy4xaC00VjE3aDRjMi43NiAwIDUtMi4yNCA1LTVzLTIuMjQtNS01LTV6JyB9KTtcblxudmFyIEluc2VydExpbmsgPSBmdW5jdGlvbiBJbnNlcnRMaW5rKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRMaW5rID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRMaW5rKTtcbkluc2VydExpbmsubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0TGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTIxIDE5VjVjMC0xLjEtLjktMi0yLTJINWMtMS4xIDAtMiAuOS0yIDJ2MTRjMCAxLjEuOSAyIDIgMmgxNGMxLjEgMCAyLS45IDItMnpNOC41IDEzLjVsMi41IDMuMDFMMTQuNSAxMmw0LjUgNkg1bDMuNS00LjV6JyB9KTtcblxudmFyIEluc2VydFBob3RvID0gZnVuY3Rpb24gSW5zZXJ0UGhvdG8ocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydFBob3RvID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRQaG90byk7XG5JbnNlcnRQaG90by5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRQaG90bztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDI0IDMxIDM0IDM4IDM5IDQ0IDQ1IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTcgM0g1Yy0xLjExIDAtMiAuOS0yIDJ2MTRjMCAxLjEuODkgMiAyIDJoMTRjMS4xIDAgMi0uOSAyLTJWN2wtNC00em0tNSAxNmMtMS42NiAwLTMtMS4zNC0zLTNzMS4zNC0zIDMtMyAzIDEuMzQgMyAzLTEuMzQgMy0zIDN6bTMtMTBINVY1aDEwdjR6JyB9KTtcblxudmFyIFNhdmUgPSBmdW5jdGlvbiBTYXZlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5TYXZlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTYXZlKTtcblNhdmUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gU2F2ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjYgMzEgMzQgMzggNDQgNDUgNDYgNDcgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ001IDR2M2g1LjV2MTJoM1Y3SDE5VjR6JyB9KTtcblxudmFyIFRpdGxlID0gZnVuY3Rpb24gVGl0bGUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblRpdGxlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShUaXRsZSk7XG5UaXRsZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBUaXRsZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9CdXR0b25CYXNlID0gcmVxdWlyZSgnLi4vQnV0dG9uQmFzZScpO1xuXG52YXIgX0J1dHRvbkJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQnV0dG9uQmFzZSk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9JY29uID0gcmVxdWlyZSgnLi4vSWNvbicpO1xuXG52YXIgX0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbik7XG5cbnJlcXVpcmUoJy4uL1N2Z0ljb24nKTtcblxudmFyIF9yZWFjdEhlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9yZWFjdEhlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTsgLy8gIHdlYWtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgQnV0dG9uQmFzZVxuXG4vLyBFbnN1cmUgQ1NTIHNwZWNpZmljaXR5XG5cblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgICAgIGZsZXg6ICcwIDAgYXV0bycsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDI0KSxcbiAgICAgIHdpZHRoOiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA2LFxuICAgICAgaGVpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA2LFxuICAgICAgcGFkZGluZzogMCxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdiYWNrZ3JvdW5kLWNvbG9yJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlc3RcbiAgICAgIH0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfSxcbiAgICBjb2xvckluaGVyaXQ6IHtcbiAgICAgIGNvbG9yOiAnaW5oZXJpdCdcbiAgICB9LFxuICAgIGRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGxhYmVsOiB7XG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgYWxpZ25JdGVtczogJ2luaGVyaXQnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgaWNvbjoge1xuICAgICAgd2lkdGg6ICcxZW0nLFxuICAgICAgaGVpZ2h0OiAnMWVtJ1xuICAgIH0sXG4gICAga2V5Ym9hcmRGb2N1c2VkOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgbmF0aXZlIGJ1dHRvbiBjb21wb25lbnQuXG4gICAqL1xuICBidXR0b25SZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiBlbGVtZW50LlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGFuIGljb24gZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2RlZmF1bHQnLCAnaW5oZXJpdCcsICdwcmltYXJ5JywgJ2NvbnRyYXN0JywgJ2FjY2VudCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBidXR0b24gd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSByaXBwbGUgd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVSaXBwbGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIHJvb3QgY29tcG9uZW50LlxuICAgKi9cbiAgcm9vdFJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmNcbn07XG5cbi8qKlxuICogUmVmZXIgdG8gdGhlIFtJY29uc10oL3N0eWxlL2ljb25zKSBzZWN0aW9uIG9mIHRoZSBkb2N1bWVudGF0aW9uXG4gKiByZWdhcmRpbmcgdGhlIGF2YWlsYWJsZSBpY29uIG9wdGlvbnMuXG4gKi9cbnZhciBJY29uQnV0dG9uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoSWNvbkJ1dHRvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gSWNvbkJ1dHRvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBJY29uQnV0dG9uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoSWNvbkJ1dHRvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoSWNvbkJ1dHRvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoSWNvbkJ1dHRvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJ1dHRvblJlZiA9IF9wcm9wcy5idXR0b25SZWYsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgICAgcm9vdFJlZiA9IF9wcm9wcy5yb290UmVmLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2J1dHRvblJlZicsICdjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICdkaXNhYmxlZCcsICdyb290UmVmJ10pO1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0J1dHRvbkJhc2UyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdkZWZhdWx0JyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmRpc2FibGVkLCBkaXNhYmxlZCksIF9jbGFzc05hbWVzKSwgY2xhc3NOYW1lKSxcbiAgICAgICAgICBjZW50ZXJSaXBwbGU6IHRydWUsXG4gICAgICAgICAga2V5Ym9hcmRGb2N1c2VkQ2xhc3NOYW1lOiBjbGFzc2VzLmtleWJvYXJkRm9jdXNlZCxcbiAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZWRcbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICByb290UmVmOiBidXR0b25SZWYsXG4gICAgICAgICAgcmVmOiByb290UmVmXG4gICAgICAgIH0pLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnc3BhbicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMubGFiZWwgfSxcbiAgICAgICAgICB0eXBlb2YgY2hpbGRyZW4gPT09ICdzdHJpbmcnID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBfSWNvbjIuZGVmYXVsdCxcbiAgICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmljb24gfSxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKSA6IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICAgICAgaWYgKCgwLCBfcmVhY3RIZWxwZXJzLmlzTXVpRWxlbWVudCkoY2hpbGQsIFsnSWNvbicsICdTdmdJY29uJ10pKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMuaWNvbiwgY2hpbGQucHJvcHMuY2xhc3NOYW1lKVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgICAgIH0pXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBJY29uQnV0dG9uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuSWNvbkJ1dHRvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnZGVmYXVsdCcsXG4gIGRpc2FibGVkOiBmYWxzZSxcbiAgZGlzYWJsZVJpcHBsZTogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpSWNvbkJ1dHRvbicgfSkoSWNvbkJ1dHRvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDMgNiAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNyAzOCAzOSA0MCA0NCA0NSA0NiA0OSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9JY29uQnV0dG9uID0gcmVxdWlyZSgnLi9JY29uQnV0dG9uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb25CdXR0b24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDMgNiAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNyAzOCAzOSA0MCA0NCA0NSA0NiA0OSA1NSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gcmVxdWlyZSgnLi91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsJyk7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUZhY3RvcnkgPSBmdW5jdGlvbiBjcmVhdGVGYWN0b3J5KHR5cGUpIHtcbiAgdmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50ID0gKDAsIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Mi5kZWZhdWx0KSh0eXBlKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwLCBjKSB7XG4gICAgcmV0dXJuICgwLCBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIuZGVmYXVsdCkoZmFsc2UsIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwLCBjKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUZhY3Rvcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGdldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gZ2V0RGlzcGxheU5hbWUoQ29tcG9uZW50KSB7XG4gIGlmICh0eXBlb2YgQ29tcG9uZW50ID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBDb21wb25lbnQ7XG4gIH1cblxuICBpZiAoIUNvbXBvbmVudCkge1xuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgaXNDbGFzc0NvbXBvbmVudCA9IGZ1bmN0aW9uIGlzQ2xhc3NDb21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKENvbXBvbmVudCAmJiBDb21wb25lbnQucHJvdG90eXBlICYmIF90eXBlb2YoQ29tcG9uZW50LnByb3RvdHlwZS5pc1JlYWN0Q29tcG9uZW50KSA9PT0gJ29iamVjdCcpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNDbGFzc0NvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc0NsYXNzQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc0NsYXNzQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSBmdW5jdGlvbiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbih0eXBlb2YgQ29tcG9uZW50ID09PSAnZnVuY3Rpb24nICYmICEoMCwgX2lzQ2xhc3NDb21wb25lbnQyLmRlZmF1bHQpKENvbXBvbmVudCkgJiYgIUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgJiYgIUNvbXBvbmVudC5jb250ZXh0VHlwZXMgJiYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicgfHwgIUNvbXBvbmVudC5wcm9wVHlwZXMpKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5ID0gcmVxdWlyZSgnLi9jcmVhdGVFYWdlckZhY3RvcnknKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJGYWN0b3J5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSBmdW5jdGlvbiBjcmVhdGVFYWdlckVsZW1lbnRVdGlsKGhhc0tleSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHByb3BzLCBjaGlsZHJlbikge1xuICBpZiAoIWhhc0tleSAmJiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCkge1xuICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgcmV0dXJuIHR5cGUoX2V4dGVuZHMoe30sIHByb3BzLCB7IGNoaWxkcmVuOiBjaGlsZHJlbiB9KSk7XG4gICAgfVxuICAgIHJldHVybiB0eXBlKHByb3BzKTtcbiAgfVxuXG4gIHZhciBDb21wb25lbnQgPSB0eXBlO1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgIENvbXBvbmVudCxcbiAgICAgIHByb3BzLFxuICAgICAgY2hpbGRyZW5cbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgcHJvcHMpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRWFnZXJFbGVtZW50VXRpbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9nZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vZ2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9nZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciB3cmFwRGlzcGxheU5hbWUgPSBmdW5jdGlvbiB3cmFwRGlzcGxheU5hbWUoQmFzZUNvbXBvbmVudCwgaG9jTmFtZSkge1xuICByZXR1cm4gaG9jTmFtZSArICcoJyArICgwLCBfZ2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpICsgJyknO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd3JhcERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBDT1VSU0VfVVBEQVRFIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL2NvdXJzZXMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIGNvdmVyPzogc3RyaW5nLFxuICB0aXRsZT86IHN0cmluZyxcbiAgZGVzY3JpcHRpb24/OiBzdHJpbmcsXG4gIHN5bGxhYnVzPzogbWl4ZWQsXG4gIGNvdXJzZU1vZHVsZXM/OiBBcnJheTx7XG4gICAgbW9kdWxlSWQ6IG51bWJlcixcbiAgfT4sXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYXV0aG9ySWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZGVzY3JpcHRpb24gLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuc3lsbGFidXMgLVxuICogQHBhcmFtIHtPYmplY3RbXX0gcGF5bG9hZC5jb3Vyc2VNb2R1bGVzIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmNvdXJzZU1vZHVsZXNbXSAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VNb2R1bGVzW10ubW9kdWxlSWQgLVxuICpcbiAqIEByZXR1cm4ge09iamVjdH0gLVxuICovXG5jb25zdCB1cGRhdGUgPSAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoeyB0eXBlOiBDT1VSU0VfVVBEQVRFLCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCB1cGRhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy91cGRhdGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgdG9rZW46IHN0cmluZyxcbiAgdXNlcklkOiBudW1iZXIsXG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIGNvdmVyPzogc3RyaW5nLFxuICB0aXRsZT86IHN0cmluZyxcbiAgZGVzY3JpcHRpb24/OiBzdHJpbmcsXG4gIHN5bGxhYnVzPzogYW55LFxuICBjb3Vyc2VNb2R1bGVzPzogQXJyYXk8e1xuICAgIG1vZHVsZUlkOiBudW1iZXIsXG4gIH0+LFxufTtcblxuLyoqXG4gKiBVcGRhdGUgQ291cnNlXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiB1cGRhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNvdmVyIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRpdGxlIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmRlc2NyaXB0aW9uIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLnN5bGxhYnVzIC1cbiAqIEBwYXJhbSB7T2JqZWN0W119IHBheWxvYWQuY291cnNlTW9kdWxlcyAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VNb2R1bGVzW10ubW9kdWxlSWQgLVxuICogQHJldHVybiB7UHJvbWlzZX0gLVxuICpcbiAqIEBleGFtcGxlXG4gKi9cbmFzeW5jIGZ1bmN0aW9uIHVwZGF0ZUNvdXJzZSh7XG4gIHVzZXJJZCxcbiAgY291cnNlSWQsXG4gIHRva2VuLFxuICBjb3ZlcixcbiAgdGl0bGUsXG4gIGRlc2NyaXB0aW9uLFxuICBzeWxsYWJ1cyxcbiAgY291cnNlTW9kdWxlcyxcbn06IFBheWxvYWQpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L2NvdXJzZXMvJHtjb3Vyc2VJZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdQVVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICBjb3ZlcixcbiAgICAgICAgdGl0bGUsXG4gICAgICAgIGRlc2NyaXB0aW9uLFxuICAgICAgICBzeWxsYWJ1cyxcbiAgICAgICAgY291cnNlTW9kdWxlcyxcbiAgICAgIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZUNvdXJzZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvdXBkYXRlLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciwgQ2FyZENvbnRlbnQsIENhcmRBY3Rpb25zIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgSWNvbkJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9JY29uQnV0dG9uJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcblxuaW1wb3J0IEVkaXRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0VkaXQnO1xuaW1wb3J0IFNhdmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NhdmUnO1xuXG5pbXBvcnQgeyBjb252ZXJ0VG9SYXcgfSBmcm9tICdkcmFmdC1qcyc7XG5cbmltcG9ydCBNYXlhc2hFZGl0b3IsIHsgY3JlYXRlRWRpdG9yU3RhdGUgfSBmcm9tICcuLi8uLi8uLi9saWIvbWF5YXNoLWVkaXRvcic7XG5cbmltcG9ydCBhcGlDb3Vyc2VVcGRhdGUgZnJvbSAnLi4vLi4vYXBpL2NvdXJzZXMvdXNlcnMvdXBkYXRlJztcbmltcG9ydCBhY3Rpb25Db3Vyc2VVcGRhdGUgZnJvbSAnLi4vLi4vYWN0aW9ucy9jb3Vyc2VzL3VwZGF0ZSc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICB0aXRsZToge1xuICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbiAgZWRpdEJ1dHRvbjoge1xuICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHtcbiAgICAgIGJvdHRvbTogJzQwcHgnLFxuICAgICAgcmlnaHQ6ICc0MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge1xuICAgICAgYm90dG9tOiAnNDBweCcsXG4gICAgICByaWdodDogJzQwcHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBib3R0b206ICczMHB4JyxcbiAgICAgIHJpZ2h0OiAnMzBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIGRpc3BsYXk6ICdub25lJyxcbiAgICAgIGJvdHRvbTogJzI1cHgnLFxuICAgICAgcmlnaHQ6ICcyNXB4JyxcbiAgICB9LFxuICB9LFxufSk7XG5cbmNsYXNzIFN5bGxhYnVzIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgY29uc3QgeyBzeWxsYWJ1cyB9ID0gcHJvcHMuY291cnNlO1xuXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGVkaXQ6IGZhbHNlLFxuXG4gICAgICBzeWxsYWJ1czogY3JlYXRlRWRpdG9yU3RhdGUoc3lsbGFidXMpLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25FZGl0ID0gdGhpcy5vbkVkaXQuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uU2F2ZSA9IHRoaXMub25TYXZlLmJpbmQodGhpcyk7XG4gIH1cblxuICBvbkNoYW5nZShzeWxsYWJ1cykge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBzeWxsYWJ1cyB9KTtcbiAgfVxuICBvbkVkaXQoKSB7XG4gICAgY29uc3QgeyBlZGl0IH0gPSB0aGlzLnN0YXRlO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBlZGl0OiAhZWRpdCB9KTtcbiAgfVxuICBhc3luYyBvblNhdmUoKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgZWxlbWVudHMsIGNvdXJzZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgICBjb25zdCB7IGNvdXJzZUlkIH0gPSBjb3Vyc2U7XG5cbiAgICAgIGNvbnN0IHsgZWRpdCB9ID0gdGhpcy5zdGF0ZTtcblxuICAgICAgY29uc3Qgc3lsbGFidXMgPSBjb252ZXJ0VG9SYXcodGhpcy5zdGF0ZS5zeWxsYWJ1cy5nZXRDdXJyZW50Q29udGVudCgpKTtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciB9ID0gYXdhaXQgYXBpQ291cnNlVXBkYXRlKHtcbiAgICAgICAgdXNlcklkLFxuICAgICAgICBjb3Vyc2VJZCxcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIHN5bGxhYnVzLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlICE9PSAyMDApIHtcbiAgICAgICAgLy8gaGFuZGxlIGVycm9yO1xuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnByb3BzLmFjdGlvbkNvdXJzZVVwZGF0ZSh7IGNvdXJzZUlkLCBzeWxsYWJ1cyB9KTtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBlZGl0OiAhZWRpdCB9KTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGUpO1xuICAgIH1cbiAgfVxuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBlbGVtZW50cywgY291cnNlIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaXNTaWduZWRJbiwgaWQ6IHVzZXJJZCB9ID0gZWxlbWVudHMudXNlcjtcbiAgICBjb25zdCB7IGF1dGhvcklkIH0gPSBjb3Vyc2U7XG5cbiAgICBjb25zdCB7IGVkaXQsIHN5bGxhYnVzIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0gc209ezEyfSBtZD17MTB9IGxnPXs4fSB4bD17OH0+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLm9uTW91c2VFbnRlcn1cbiAgICAgICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbk1vdXNlTGVhdmV9XG4gICAgICAgICAgPlxuICAgICAgICAgICAgPENhcmQgcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPXsnU3lsbGFidXMnfSBjbGFzc05hbWU9e2NsYXNzZXMudGl0bGV9IC8+XG4gICAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgICA8TWF5YXNoRWRpdG9yXG4gICAgICAgICAgICAgICAgICBlZGl0b3JTdGF0ZT17c3lsbGFidXN9XG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgICAgICAgIHJlYWRPbmx5PXshZWRpdH1cbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXsnU3lsbGFidXMgQ29udGVudCd9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICB7aXNTaWduZWRJbiAmJlxuICAgICAgICAgICAgYXV0aG9ySWQgPT09IHVzZXJJZCAmJiAoXG4gICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICBmYWJcbiAgICAgICAgICAgICAgICBjb2xvcj1cImFjY2VudFwiXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkVkaXRcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5lZGl0QnV0dG9ufVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2VkaXQgPyB0aGlzLm9uU2F2ZSA6IHRoaXMub25FZGl0fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2VkaXQgPyA8U2F2ZUljb24gLz4gOiA8RWRpdEljb24gLz59XG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgPC9HcmlkPlxuICAgICAgPC9HcmlkPlxuICAgICk7XG4gIH1cbn1cblxuU3lsbGFidXMucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY291cnNlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgYWN0aW9uQ291cnNlVXBkYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbkNvdXJzZVVwZGF0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKFN5bGxhYnVzKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9TeWxsYWJ1cy5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG4vLyBpbXBvcnQgSWNvbiBmcm9tICdtYXRlcmlhbC11aS9JY29uJztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IFRvb2x0aXAgZnJvbSAnbWF0ZXJpYWwtdWkvVG9vbHRpcCc7XG5cbmltcG9ydCBUaXRsZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUnO1xuaW1wb3J0IEZvcm1hdEJvbGRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQnO1xuaW1wb3J0IEZvcm1hdEl0YWxpYyBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMnO1xuaW1wb3J0IEZvcm1hdFVuZGVybGluZWRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQnO1xuaW1wb3J0IEZvcm1hdFF1b3RlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZSc7XG4vLyBpbXBvcnQgRm9ybWF0Q2xlYXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENsZWFyJztcbi8vIGltcG9ydCBGb3JtYXRDb2xvckZpbGxJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENvbG9yRmlsbCc7XG4vLyBpbXBvcnQgRm9ybWF0Q29sb3JSZXNldEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q29sb3JSZXNldCc7XG4vLyBpbXBvcnQgRm9ybWF0Q29sb3JUZXh0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRDb2xvclRleHQnO1xuLy8gaW1wb3J0IEZvcm1hdEluZGVudERlY3JlYXNlSWNvblxuLy8gICBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJbmRlbnREZWNyZWFzZSc7XG4vLyBpbXBvcnQgRm9ybWF0SW5kZW50SW5jcmVhc2VJY29uXG4vLyAgIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEluZGVudEluY3JlYXNlJztcblxuaW1wb3J0IENvZGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0NvZGUnO1xuXG5pbXBvcnQgRm9ybWF0TGlzdE51bWJlcmVkSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQnO1xuaW1wb3J0IEZvcm1hdExpc3RCdWxsZXRlZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkJztcblxuaW1wb3J0IEZvcm1hdEFsaWduQ2VudGVySWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlcic7XG5pbXBvcnQgRm9ybWF0QWxpZ25MZWZ0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQnO1xuaW1wb3J0IEZvcm1hdEFsaWduUmlnaHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQnO1xuaW1wb3J0IEZvcm1hdEFsaWduSnVzdGlmeUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5JztcblxuaW1wb3J0IEF0dGFjaEZpbGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0F0dGFjaEZpbGUnO1xuaW1wb3J0IEluc2VydExpbmtJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsnO1xuaW1wb3J0IEluc2VydFBob3RvSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90byc7XG5pbXBvcnQgSW5zZXJ0RW1vdGljb25JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uJztcbmltcG9ydCBJbnNlcnRDb21tZW50SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50JztcblxuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25Ub3BJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25Ub3AnO1xuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25Cb3R0b21JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25Cb3R0b20nO1xuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25DZW50ZXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25DZW50ZXInO1xuXG4vLyBpbXBvcnQgV3JhcFRleHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1dyYXBUZXh0JztcblxuaW1wb3J0IEhpZ2hsaWdodEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0JztcbmltcG9ydCBGdW5jdGlvbnNJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucyc7XG5cbmltcG9ydCB7XG4gIEVkaXRvcixcbiAgUmljaFV0aWxzLFxuICAvLyBFbnRpdHksXG4gIC8vIENvbXBvc2l0ZURlY29yYXRvcixcbiAgQXRvbWljQmxvY2tVdGlscyxcbiAgRWRpdG9yU3RhdGUsXG4gIC8vICAgY29udmVydFRvUmF3LFxufSBmcm9tICdkcmFmdC1qcyc7XG5cbi8vIGltcG9ydCB7XG4vLyAgIGhhc2h0YWdTdHJhdGVneSxcbi8vICAgSGFzaHRhZ1NwYW4sXG5cbi8vICAgaGFuZGxlU3RyYXRlZ3ksXG4vLyAgIEhhbmRsZVNwYW4sXG4vLyB9IGZyb20gJy4vY29tcG9uZW50cy9EZWNvcmF0b3JzJztcblxuaW1wb3J0IEF0b21pYyBmcm9tICcuL2NvbXBvbmVudHMvQXRvbWljJztcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi9FZGl0b3JTdHlsZXMnO1xuXG5pbXBvcnQgeyBCbG9ja3MsIEhBTkRMRUQsIE5PVF9IQU5ETEVEIH0gZnJvbSAnLi9jb25zdGFudHMnO1xuXG4vKipcbiAqIE1heWFzaEVkaXRvclxuICovXG5jbGFzcyBNYXlhc2hFZGl0b3IgZXh0ZW5kcyBDb21wb25lbnQge1xuICAvKipcbiAgICogTWF5YXNoIEVkaXRvcidzIHByb3B0eXBlcyBhcmUgZGVmaW5lZCBoZXJlLlxuICAgKi9cbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgICBlZGl0b3JTdGF0ZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuXG4gICAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG5cbiAgICByZWFkT25seTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcblxuICAgIC8qKlxuICAgICAqIFRoaXMgYXBpIGZ1bmN0aW9uIHNob3VsZCBiZSBhcHBsaWVkIGZvciBlbmFibGluZyBwaG90byBhZGRpbmdcbiAgICAgKiBmZWF0dXJlIGluIE1heWFzaCBFZGl0b3IuXG4gICAgICovXG4gICAgYXBpUGhvdG9VcGxvYWQ6IFByb3BUeXBlcy5mdW5jLFxuICB9O1xuXG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgcmVhZE9ubHk6IHRydWUsXG4gICAgcGxhY2Vob2xkZXI6ICdXcml0ZSBIZXJlLi4uJyxcbiAgfTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIC8vIHRoaXMuc3RhdGUgPSB7fTtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBiZSBzdXBwbGllZCB0byBEcmFmdC5qcyBFZGl0b3IgdG8gaGFuZGxlXG4gICAgICogb25DaGFuZ2UgZXZlbnQuXG4gICAgICogQGZ1bmN0aW9uIG9uQ2hhbmdlXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGVkaXRvclN0YXRlXG4gICAgICovXG4gICAgdGhpcy5vbkNoYW5nZSA9IChlZGl0b3JTdGF0ZSkgPT4ge1xuICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZShlZGl0b3JTdGF0ZSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZm9jdXMgZXZlbnQgaW4gRHJhZnQuanMgRWRpdG9yLlxuICAgICAqIEBmdW5jdGlvbiBmb2N1c1xuICAgICAqL1xuICAgIHRoaXMuZm9jdXMgPSAoKSA9PiB0aGlzLmVkaXRvck5vZGUuZm9jdXMoKTtcblxuICAgIHRoaXMub25UYWIgPSB0aGlzLm9uVGFiLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblRpdGxlQ2xpY2sgPSB0aGlzLm9uVGl0bGVDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25Db2RlQ2xpY2sgPSB0aGlzLm9uQ29kZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblF1b3RlQ2xpY2sgPSB0aGlzLm9uUXVvdGVDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25MaXN0QnVsbGV0ZWRDbGljayA9IHRoaXMub25MaXN0QnVsbGV0ZWRDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25MaXN0TnVtYmVyZWRDbGljayA9IHRoaXMub25MaXN0TnVtYmVyZWRDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25Cb2xkQ2xpY2sgPSB0aGlzLm9uQm9sZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkl0YWxpY0NsaWNrID0gdGhpcy5vbkl0YWxpY0NsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblVuZGVyTGluZUNsaWNrID0gdGhpcy5vblVuZGVyTGluZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkhpZ2hsaWdodENsaWNrID0gdGhpcy5vbkhpZ2hsaWdodENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5oYW5kbGVLZXlDb21tYW5kID0gdGhpcy5oYW5kbGVLZXlDb21tYW5kLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNsaWNrSW5zZXJ0UGhvdG8gPSB0aGlzLm9uQ2xpY2tJbnNlcnRQaG90by5iaW5kKHRoaXMpO1xuXG4gICAgLy8gdGhpcy5ibG9ja1JlbmRlcmVyRm4gPSB0aGlzLmJsb2NrUmVuZGVyZXJGbi5iaW5kKHRoaXMpO1xuXG4gICAgLy8gY29uc3QgY29tcG9zaXRlRGVjb3JhdG9yID0gbmV3IENvbXBvc2l0ZURlY29yYXRvcihbXG4gICAgLy8gICB7XG4gICAgLy8gICAgIHN0cmF0ZWd5OiBoYW5kbGVTdHJhdGVneSxcbiAgICAvLyAgICAgY29tcG9uZW50OiBIYW5kbGVTcGFuLFxuICAgIC8vICAgfSxcbiAgICAvLyAgIHtcbiAgICAvLyAgICAgc3RyYXRlZ3k6IGhhc2h0YWdTdHJhdGVneSxcbiAgICAvLyAgICAgY29tcG9uZW50OiBIYXNodGFnU3BhbixcbiAgICAvLyAgIH0sXG4gICAgLy8gXSk7XG4gIH1cblxuICAvKipcbiAgICogb25UYWIoKSB3aWxsIGhhbmRsZSBUYWIgYnV0dG9uIHByZXNzIGV2ZW50IGluIEVkaXRvci5cbiAgICogQGZ1bmN0aW9uIG9uVGFiXG4gICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICovXG4gIG9uVGFiKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLm9uVGFiKGUsIGVkaXRvclN0YXRlLCA0KTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gVGl0bGUgQnV0dG9uLlxuICAgKiBAZnVuY3Rpb24gb25UaXRsZUNsaWNrXG4gICAqL1xuICBvblRpdGxlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2hlYWRlci1vbmUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gQ29kZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvbkNvZGVDbGlja1xuICAgKi9cbiAgb25Db2RlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2NvZGUtYmxvY2snKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gUXVvdGUgQnV0dG9uLlxuICAgKiBAZnVuY3Rpb24gb25RdW90ZUNsaWNrXG4gICAqL1xuICBvblF1b3RlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2Jsb2NrcXVvdGUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCB1cGRhdGUgYmxvY2sgd2l0aCB1bm9yZGVyZWQgbGlzdCBpdGVtc1xuICAgKiBAZnVuY3Rpb24gb25MaXN0QnVsbGV0ZWRDbGlja1xuICAgKi9cbiAgb25MaXN0QnVsbGV0ZWRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICAndW5vcmRlcmVkLWxpc3QtaXRlbScsXG4gICAgKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCB1cGRhdGUgYmxvY2sgd2l0aCBvcmRlcmVkIGxpc3QgaXRlbS5cbiAgICogQGZ1bmN0aW9uIG9uTGlzdE51bWJlcmVkQ2xpY2tcbiAgICovXG4gIG9uTGlzdE51bWJlcmVkQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShcbiAgICAgIGVkaXRvclN0YXRlLFxuICAgICAgJ29yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICApO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGdpdmUgc2VsZWN0ZWQgY29udGVudCBCb2xkIHN0eWxpbmcuXG4gICAqIEBmdW5jdGlvbiBvbkJvbGRDbGlja1xuICAgKi9cbiAgb25Cb2xkQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnQk9MRCcpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGdpdmUgaXRhbGljIHN0eWxpbmcgdG8gc2VsZWN0ZWQgY29udGVudC5cbiAgICogQGZ1bmN0aW9uIG9uSXRhbGljQ2xpY2tcbiAgICovXG4gIG9uSXRhbGljQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnSVRBTElDJyk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSB1bmRlcmxpbmUgc3R5bGluZyB0byBzZWxlY3RlZCBjb250ZW50LlxuICAgKiBAZnVuY3Rpb24gb25VbmRlckxpbmVDbGlja1xuICAgKi9cbiAgb25VbmRlckxpbmVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgICdVTkRFUkxJTkUnLFxuICAgICk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgaGlnaGxpZ2h0IHNlbGVjdGVkIGNvbnRlbnQuXG4gICAqIEBmdW5jdGlvbiBvbkhpZ2hsaWdodENsaWNrXG4gICAqL1xuICBvbkhpZ2hsaWdodENsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVJbmxpbmVTdHlsZShlZGl0b3JTdGF0ZSwgJ0NPREUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqL1xuICBvbkNsaWNrSW5zZXJ0UGhvdG8oKSB7XG4gICAgdGhpcy5waG90by52YWx1ZSA9IG51bGw7XG4gICAgdGhpcy5waG90by5jbGljaygpO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqL1xuICBvbkNoYW5nZUluc2VydFBob3RvID0gYXN5bmMgKGUpID0+IHtcbiAgICAvLyBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xuXG4gICAgaWYgKGZpbGUudHlwZS5pbmRleE9mKCdpbWFnZS8nKSA9PT0gMCkge1xuICAgICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICAgIGZvcm1EYXRhLmFwcGVuZCgncGhvdG8nLCBmaWxlKTtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgdGhpcy5wcm9wcy5hcGlQaG90b1VwbG9hZCh7XG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgRXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY29uc3QgeyBwaG90b1VybDogc3JjIH0gPSBwYXlsb2FkO1xuXG4gICAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCBjb250ZW50U3RhdGUgPSBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgICAgY29uc3QgY29udGVudFN0YXRlV2l0aEVudGl0eSA9IGNvbnRlbnRTdGF0ZS5jcmVhdGVFbnRpdHkoXG4gICAgICAgIEJsb2Nrcy5QSE9UTyxcbiAgICAgICAgJ0lNTVVUQUJMRScsXG4gICAgICAgIHsgc3JjIH0sXG4gICAgICApO1xuXG4gICAgICBjb25zdCBlbnRpdHlLZXkgPSBjb250ZW50U3RhdGVXaXRoRW50aXR5LmdldExhc3RDcmVhdGVkRW50aXR5S2V5KCk7XG5cbiAgICAgIGNvbnN0IG1pZGRsZUVkaXRvclN0YXRlID0gRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7XG4gICAgICAgIGN1cnJlbnRDb250ZW50OiBjb250ZW50U3RhdGVXaXRoRW50aXR5LFxuICAgICAgfSk7XG5cbiAgICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gQXRvbWljQmxvY2tVdGlscy5pbnNlcnRBdG9taWNCbG9jayhcbiAgICAgICAgbWlkZGxlRWRpdG9yU3RhdGUsXG4gICAgICAgIGVudGl0eUtleSxcbiAgICAgICAgJyAnLFxuICAgICAgKTtcblxuICAgICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBjdXN0b20gaGFuZGxlIGNvbW1hbmRzIHRvIEVkaXRvci5cbiAgICogQGZ1bmN0aW9uIGhhbmRsZUtleUNvbW1hbmRcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvbW1hbmRcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgaGFuZGxlS2V5Q29tbWFuZChjb21tYW5kKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLmhhbmRsZUtleUNvbW1hbmQoZWRpdG9yU3RhdGUsIGNvbW1hbmQpO1xuXG4gICAgaWYgKG5ld0VkaXRvclN0YXRlKSB7XG4gICAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICAgIHJldHVybiBIQU5ETEVEO1xuICAgIH1cblxuICAgIHJldHVybiBOT1RfSEFORExFRDtcbiAgfVxuXG4gIC8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbiAgYmxvY2tSZW5kZXJlckZuKGJsb2NrKSB7XG4gICAgc3dpdGNoIChibG9jay5nZXRUeXBlKCkpIHtcbiAgICAgIGNhc2UgQmxvY2tzLkFUT01JQzpcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBjb21wb25lbnQ6IEF0b21pYyxcbiAgICAgICAgICBlZGl0YWJsZTogZmFsc2UsXG4gICAgICAgIH07XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cblxuICAvKiBlc2xpbnQtZGlzYWJsZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzICovXG4gIGJsb2NrU3R5bGVGbihibG9jaykge1xuICAgIHN3aXRjaCAoYmxvY2suZ2V0VHlwZSgpKSB7XG4gICAgICBjYXNlICdibG9ja3F1b3RlJzpcbiAgICAgICAgcmV0dXJuICdSaWNoRWRpdG9yLWJsb2NrcXVvdGUnO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cbiAgLyogZXNsaW50LWVuYWJsZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzICovXG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGNsYXNzZXMsXG4gICAgICByZWFkT25seSxcbiAgICAgIG9uQ2hhbmdlLFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICBwbGFjZWhvbGRlcixcbiAgICB9ID0gdGhpcy5wcm9wcztcblxuICAgIC8vIEN1c3RvbSBvdmVycmlkZXMgZm9yIFwiY29kZVwiIHN0eWxlLlxuICAgIGNvbnN0IHN0eWxlTWFwID0ge1xuICAgICAgQ09ERToge1xuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsIDAsIDAsIDAuMDUpJyxcbiAgICAgICAgZm9udEZhbWlseTogJ1wiSW5jb25zb2xhdGFcIiwgXCJNZW5sb1wiLCBcIkNvbnNvbGFzXCIsIG1vbm9zcGFjZScsXG4gICAgICAgIGZvbnRTaXplOiAxNixcbiAgICAgICAgcGFkZGluZzogMixcbiAgICAgIH0sXG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgeyFyZWFkT25seSA/IChcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyh7ICdlZGl0b3ItY29udHJvbHMnOiAnJyB9KX0+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIlRpdGxlXCIgaWQ9XCJ0aXRsZVwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiVGl0bGVcIiBvbkNsaWNrPXt0aGlzLm9uVGl0bGVDbGlja30+XG4gICAgICAgICAgICAgICAgPFRpdGxlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkJvbGRcIiBpZD1cImJvbGRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvbGRcIiBvbkNsaWNrPXt0aGlzLm9uQm9sZENsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0Qm9sZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJJdGFsaWNcIiBpZD1cIml0YWxpY1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSXRhbGljXCIgb25DbGljaz17dGhpcy5vbkl0YWxpY0NsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0SXRhbGljIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiVW5kZXJsaW5lXCIgaWQ9XCJ1bmRlcmxpbmVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiVW5kZXJsaW5lXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uVW5kZXJMaW5lQ2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0VW5kZXJsaW5lZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJDb2RlXCIgaWQ9XCJjb2RlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJDb2RlXCIgb25DbGljaz17dGhpcy5vbkNvZGVDbGlja30+XG4gICAgICAgICAgICAgICAgPENvZGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiUXVvdGVcIiBpZD1cInF1b3RlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJRdW90ZVwiIG9uQ2xpY2s9e3RoaXMub25RdW90ZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0UXVvdGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgIHRpdGxlPVwiVW5vcmRlcmVkIExpc3RcIlxuICAgICAgICAgICAgICBpZD1cInVub3JkZXJkLWxpc3RcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVbm9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZvcm1hdExpc3RCdWxsZXRlZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJPcmRlcmVkIExpc3RcIiBpZD1cIm9yZGVyZWQtbGlzdFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJPcmRlcmVkIExpc3RcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25MaXN0TnVtYmVyZWRDbGlja31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRMaXN0TnVtYmVyZWRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gTGVmdFwiIGlkPVwiYWxpZ24tbGVmdFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gTGVmdFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkxlZnRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gQ2VudGVyXCIgaWQ9XCJhbGlnbi1jZW50ZXJcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIENlbnRlclwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkNlbnRlckljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBSaWdodFwiIGlkPVwiYWxpZ24tcmlnaHRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIFJpZ2h0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduUmlnaHRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gUmlnaHRcIiBpZD1cImFsaWduLXJpZ2h0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBbGlnbiBSaWdodFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkp1c3RpZnlJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQXR0YWNoIEZpbGVcIiBpZD1cImF0dGFjaC1maWxlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBdHRhY2ggRmlsZVwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxBdHRhY2hGaWxlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkluc2VydCBMaW5rXCIgaWQ9XCJpbnNlcnQtbGlua1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IExpbmtcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8SW5zZXJ0TGlua0ljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJJbnNlcnQgUGhvdG9cIiBpZD1cImluc2VydC1waG90b1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJJbnNlcnQgUGhvdG9cIlxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0eXBlb2YgdGhpcy5wcm9wcy5hcGlQaG90b1VwbG9hZCAhPT0gJ2Z1bmN0aW9uJ31cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tJbnNlcnRQaG90b31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRQaG90b0ljb24gLz5cbiAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcbiAgICAgICAgICAgICAgICAgIGFjY2VwdD1cImltYWdlL2pwZWd8cG5nfGdpZlwiXG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZUluc2VydFBob3RvfVxuICAgICAgICAgICAgICAgICAgcmVmPXsocGhvdG8pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5waG90byA9IHBob3RvO1xuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6ICdub25lJyB9fVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgIHRpdGxlPVwiSW5zZXJ0IEVtb3RpY29uXCJcbiAgICAgICAgICAgICAgaWQ9XCJpbnNlcnRFLWVtb3RpY29uXCJcbiAgICAgICAgICAgICAgcGxhY2VtZW50PVwiYm90dG9tXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkluc2VydCBFbW90aWNvblwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRFbW90aWNvbkljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJJbnNlcnQgQ29tbWVudFwiXG4gICAgICAgICAgICAgIGlkPVwiaW5zZXJ0LWNvbW1lbnRcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IENvbW1lbnRcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8SW5zZXJ0Q29tbWVudEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJIaWdobGlnaHQgVGV4dFwiXG4gICAgICAgICAgICAgIGlkPVwiaGlnaGxpZ2h0LXRleHRcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJIaWdobGlnaHQgVGV4dFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkhpZ2hsaWdodENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEhpZ2hsaWdodEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJBZGQgRnVuY3Rpb25zXCJcbiAgICAgICAgICAgICAgaWQ9XCJhZGQtZnVuY3Rpb25zXCJcbiAgICAgICAgICAgICAgcGxhY2VtZW50PVwiYm90dG9tXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFkZCBGdW5jdGlvbnNcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8RnVuY3Rpb25zSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkgOiBudWxsfVxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyh7ICdlZGl0b3ItYXJlYSc6ICcnIH0pfT5cbiAgICAgICAgICA8RWRpdG9yXG4gICAgICAgICAgICAvKiBCYXNpY3MgKi9cblxuICAgICAgICAgICAgZWRpdG9yU3RhdGU9e2VkaXRvclN0YXRlfVxuICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgLyogUHJlc2VudGF0aW9uICovXG5cbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgIC8vIHRleHRBbGlnbm1lbnQ9XCJjZW50ZXJcIlxuXG4gICAgICAgICAgICAvLyB0ZXh0RGlyZWN0aW9uYWxpdHk9XCJMVFJcIlxuXG4gICAgICAgICAgICBibG9ja1JlbmRlcmVyRm49e3RoaXMuYmxvY2tSZW5kZXJlckZufVxuICAgICAgICAgICAgYmxvY2tTdHlsZUZuPXt0aGlzLmJsb2NrU3R5bGVGbn1cbiAgICAgICAgICAgIGN1c3RvbVN0eWxlTWFwPXtzdHlsZU1hcH1cbiAgICAgICAgICAgIC8vIGN1c3RvbVN0eWxlRm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBCZWhhdmlvciAqL1xuXG4gICAgICAgICAgICAvLyBhdXRvQ2FwaXRhbGl6ZT1cInNlbnRlbmNlc1wiXG5cbiAgICAgICAgICAgIC8vIGF1dG9Db21wbGV0ZT1cIm9mZlwiXG5cbiAgICAgICAgICAgIC8vIGF1dG9Db3JyZWN0PVwib2ZmXCJcblxuICAgICAgICAgICAgcmVhZE9ubHk9e3JlYWRPbmx5fVxuICAgICAgICAgICAgc3BlbGxDaGVja1xuICAgICAgICAgICAgLy8gc3RyaXBQYXN0ZWRTdHlsZXM9e2ZhbHNlfVxuXG4gICAgICAgICAgICAvKiBET00gYW5kIEFjY2Vzc2liaWxpdHkgKi9cblxuICAgICAgICAgICAgLy8gZWRpdG9yS2V5XG5cbiAgICAgICAgICAgIC8qIENhbmNlbGFibGUgSGFuZGxlcnMgKi9cblxuICAgICAgICAgICAgLy8gaGFuZGxlUmV0dXJuPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgaGFuZGxlS2V5Q29tbWFuZD17dGhpcy5oYW5kbGVLZXlDb21tYW5kfVxuICAgICAgICAgICAgLy8gaGFuZGxlQmVmb3JlSW5wdXQ9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVQYXN0ZWRUZXh0PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gaGFuZGxlUGFzdGVkRmlsZXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVEcm9wcGVkRmlsZXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVEcm9wPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogS2V5IEhhbmRsZXJzICovXG5cbiAgICAgICAgICAgIC8vIG9uRXNjYXBlPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgb25UYWI9e3RoaXMub25UYWJ9XG4gICAgICAgICAgICAvLyBvblVwQXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBvblJpZ2h0QXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBvbkRvd25BcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uTGVmdEFycm93PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8ga2V5QmluZGluZ0ZuPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogTW91c2UgRXZlbnQgKi9cblxuICAgICAgICAgICAgLy8gb25Gb2N1cz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uQmx1cj17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8qIE1ldGhvZHMgKi9cblxuICAgICAgICAgICAgLy8gZm9jdXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBibHVyPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogRm9yIFJlZmVyZW5jZSAqL1xuXG4gICAgICAgICAgICByZWY9eyhub2RlKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZWRpdG9yTm9kZSA9IG5vZGU7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTWF5YXNoRWRpdG9yKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3IuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgeyBFZGl0b3JTdGF0ZSwgY29udmVydEZyb21SYXcsIENvbXBvc2l0ZURlY29yYXRvciB9IGZyb20gJ2RyYWZ0LWpzJztcblxuaW1wb3J0IHtcbiAgaGFzaHRhZ1N0cmF0ZWd5LFxuICBIYXNodGFnU3BhbixcbiAgaGFuZGxlU3RyYXRlZ3ksXG4gIEhhbmRsZVNwYW4sXG59IGZyb20gJy4vY29tcG9uZW50cy9EZWNvcmF0b3JzJztcblxuY29uc3QgZGVmYXVsdERlY29yYXRvcnMgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKFtcbiAge1xuICAgIHN0cmF0ZWd5OiBoYW5kbGVTdHJhdGVneSxcbiAgICBjb21wb25lbnQ6IEhhbmRsZVNwYW4sXG4gIH0sXG4gIHtcbiAgICBzdHJhdGVneTogaGFzaHRhZ1N0cmF0ZWd5LFxuICAgIGNvbXBvbmVudDogSGFzaHRhZ1NwYW4sXG4gIH0sXG5dKTtcblxuZXhwb3J0IGNvbnN0IGNyZWF0ZUVkaXRvclN0YXRlID0gKFxuICBjb250ZW50ID0gbnVsbCxcbiAgZGVjb3JhdG9ycyA9IGRlZmF1bHREZWNvcmF0b3JzLFxuKSA9PiB7XG4gIGlmIChjb250ZW50ID09PSBudWxsKSB7XG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLmNyZWF0ZUVtcHR5KGRlY29yYXRvcnMpO1xuICB9XG4gIHJldHVybiBFZGl0b3JTdGF0ZS5jcmVhdGVXaXRoQ29udGVudChjb252ZXJ0RnJvbVJhdyhjb250ZW50KSwgZGVjb3JhdG9ycyk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVFZGl0b3JTdGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdGF0ZS5qcyIsIi8qKlxuICogVGhpcyBmaWxlIGNvbnRhaW5zIGFsbCB0aGUgQ1NTLWluLUpTIHN0eWxlcyBvZiBFZGl0b3IgY29tcG9uZW50LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gICdAZ2xvYmFsJzoge1xuICAgICcuUmljaEVkaXRvci1yb290Jzoge1xuICAgICAgYmFja2dyb3VuZDogJyNmZmYnLFxuICAgICAgYm9yZGVyOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgICAgZm9udEZhbWlseTogXCInR2VvcmdpYScsIHNlcmlmXCIsXG4gICAgICBmb250U2l6ZTogJzE0cHgnLFxuICAgICAgcGFkZGluZzogJzE1cHgnLFxuICAgIH0sXG4gICAgJy5SaWNoRWRpdG9yLWVkaXRvcic6IHtcbiAgICAgIGJvcmRlclRvcDogJzFweCBzb2xpZCAjZGRkJyxcbiAgICAgIGN1cnNvcjogJ3RleHQnLFxuICAgICAgZm9udFNpemU6ICcxNnB4JyxcbiAgICAgIG1hcmdpblRvcDogJzEwcHgnLFxuICAgIH0sXG4gICAgJy5wdWJsaWMtRHJhZnRFZGl0b3JQbGFjZWhvbGRlci1yb290Jzoge1xuICAgICAgbWFyZ2luOiAnMCAtMTVweCAtMTVweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgfSxcbiAgICAnLnB1YmxpYy1EcmFmdEVkaXRvci1jb250ZW50Jzoge1xuICAgICAgbWFyZ2luOiAnMCAtMTVweCAtMTVweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgICAvLyBtaW5IZWlnaHQ6ICcxMDBweCcsXG4gICAgfSxcbiAgICAnLlJpY2hFZGl0b3ItYmxvY2txdW90ZSc6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJzVweCBzb2xpZCAjZWVlJyxcbiAgICAgIGJvcmRlckxlZnQ6ICc1cHggc29saWQgI2VlZScsXG4gICAgICBjb2xvcjogJyM2NjYnLFxuICAgICAgZm9udEZhbWlseTogXCInSG9lZmxlciBUZXh0JywgJ0dlb3JnaWEnLCBzZXJpZlwiLFxuICAgICAgZm9udFN0eWxlOiAnaXRhbGljJyxcbiAgICAgIG1hcmdpbjogJzE2cHggMCcsXG4gICAgICBwYWRkaW5nOiAnMTBweCAyMHB4JyxcbiAgICB9LFxuICAgICcucHVibGljLURyYWZ0U3R5bGVEZWZhdWx0LXByZSc6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwgMCwgMCwgMC4wNSknLFxuICAgICAgZm9udEZhbWlseTogXCInSW5jb25zb2xhdGEnLCAnTWVubG8nLCAnQ29uc29sYXMnLCBtb25vc3BhY2VcIixcbiAgICAgIGZvbnRTaXplOiAnMTZweCcsXG4gICAgICBwYWRkaW5nOiAnMjBweCcsXG4gICAgfSxcbiAgfSxcbiAgcm9vdDoge1xuICAgIC8vIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3R5bGVzLmpzIiwiLyoqXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBCbG9ja3MgfSBmcm9tICcuLi9jb25zdGFudHMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHBob3RvOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICAvLyBGaXggYW4gaXNzdWUgd2l0aCBGaXJlZm94IHJlbmRlcmluZyB2aWRlbyBjb250cm9sc1xuICAgIC8vIHdpdGggJ3ByZS13cmFwJyB3aGl0ZS1zcGFjZVxuICAgIHdoaXRlU3BhY2U6ICdpbml0aWFsJyxcbiAgfSxcbn07XG5cbi8qKlxuICpcbiAqIEBjbGFzcyBBdG9taWMgLSB0aGlzIFJlYWN0IGNvbXBvbmVudCB3aWxsIGJlIHVzZWQgdG8gcmVuZGVyIEF0b21pY1xuICogY29tcG9uZW50cyBvZiBEcmFmdC5qc1xuICpcbiAqIEB0b2RvIC0gY29uZmlndXJlIHRoaXMgZm9yIGF1ZGlvLlxuICogQHRvZG8gLSBjb25maWd1cmUgdGhpcyBmb3IgdmlkZW8uXG4gKi9cbmNsYXNzIEF0b21pYyBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY29udGVudFN0YXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgYmxvY2s6IFByb3BUeXBlcy5hbnkuaXNSZXF1aXJlZCxcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjb250ZW50U3RhdGUsIGJsb2NrIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgZW50aXR5ID0gY29udGVudFN0YXRlLmdldEVudGl0eShibG9jay5nZXRFbnRpdHlBdCgwKSk7XG4gICAgY29uc3QgeyBzcmMgfSA9IGVudGl0eS5nZXREYXRhKCk7XG4gICAgY29uc3QgdHlwZSA9IGVudGl0eS5nZXRUeXBlKCk7XG5cbiAgICBpZiAodHlwZSA9PT0gQmxvY2tzLlBIT1RPKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIDxpbWcgYWx0PXsnYWx0J30gc3JjPXtzcmN9IHN0eWxlPXtzdHlsZXMucGhvdG99IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gPGRpdiAvPjtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBBdG9taWM7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9BdG9taWMuanMiLCIvKiBlc2xpbnQtZGlzYWJsZSAqL1xuLy8gZXNsaW50IGlzIGRpc2FibGVkIGhlcmUgZm9yIG5vdy5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgc3R5bGUgZnJvbSAnLi4vc3R5bGUnO1xuXG5jb25zdCBIQU5ETEVfUkVHRVggPSAvXFxAW1xcd10rL2c7XG5jb25zdCBIQVNIVEFHX1JFR0VYID0gL1xcI1tcXHdcXHUwNTkwLVxcdTA1ZmZdKy9nO1xuXG5mdW5jdGlvbiBmaW5kV2l0aFJlZ2V4KHJlZ2V4LCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKSB7XG4gIGNvbnN0IHRleHQgPSBjb250ZW50QmxvY2suZ2V0VGV4dCgpO1xuICBsZXQgbWF0Y2hBcnIsXG4gICAgc3RhcnQ7XG4gIHdoaWxlICgobWF0Y2hBcnIgPSByZWdleC5leGVjKHRleHQpKSAhPT0gbnVsbCkge1xuICAgIHN0YXJ0ID0gbWF0Y2hBcnIuaW5kZXg7XG4gICAgY2FsbGJhY2soc3RhcnQsIHN0YXJ0ICsgbWF0Y2hBcnJbMF0ubGVuZ3RoKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFuZGxlU3RyYXRlZ3koY29udGVudEJsb2NrLCBjYWxsYmFjaywgY29udGVudFN0YXRlKSB7XG4gIGZpbmRXaXRoUmVnZXgoSEFORExFX1JFR0VYLCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGhhc2h0YWdTdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrLCBjb250ZW50U3RhdGUpIHtcbiAgZmluZFdpdGhSZWdleChIQVNIVEFHX1JFR0VYLCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKTtcbn1cblxuZXhwb3J0IGNvbnN0IEhhbmRsZVNwYW4gPSBwcm9wcyA9PiA8c3BhbiBzdHlsZT17c3R5bGUuaGFuZGxlfT57cHJvcHMuY2hpbGRyZW59PC9zcGFuPjtcblxuZXhwb3J0IGNvbnN0IEhhc2h0YWdTcGFuID0gcHJvcHMgPT4gPHNwYW4gc3R5bGU9e3N0eWxlLmhhc2h0YWd9Pntwcm9wcy5jaGlsZHJlbn08L3NwYW4+O1xuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaGFuZGxlU3RyYXRlZ3ksXG4gIEhhbmRsZVNwYW4sXG5cbiAgaGFzaHRhZ1N0cmF0ZWd5LFxuICBIYXNodGFnU3Bhbixcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9EZWNvcmF0b3JzLmpzIiwiLyoqXG4gKiBTb21lIG9mIHRoZSBjb25zdGFudHMgd2hpY2ggYXJlIHVzZWQgdGhyb3VnaG91dCB0aGlzIHByb2plY3QgaW5zdGVhZCBvZlxuICogZGlyZWN0bHkgdXNpbmcgc3RyaW5nLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG4vKipcbiAqIEBjb25zdGFudCBCbG9ja3NcbiAqL1xuZXhwb3J0IGNvbnN0IEJsb2NrcyA9IHtcbiAgVU5TVFlMRUQ6ICd1bnN0eWxlZCcsXG4gIFBBUkFHUkFQSDogJ3Vuc3R5bGVkJyxcblxuICBIMTogJ2hlYWRlci1vbmUnLFxuICBIMjogJ2hlYWRlci10d28nLFxuICBIMzogJ2hlYWRlci10aHJlZScsXG4gIEg0OiAnaGVhZGVyLWZvdXInLFxuICBINTogJ2hlYWRlci1maXZlJyxcbiAgSDY6ICdoZWFkZXItc2l4JyxcblxuICBPTDogJ29yZGVyZWQtbGlzdC1pdGVtJyxcbiAgVUw6ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcblxuICBDT0RFOiAnY29kZS1ibG9jaycsXG5cbiAgQkxPQ0tRVU9URTogJ2Jsb2NrcXVvdGUnLFxuXG4gIEFUT01JQzogJ2F0b21pYycsXG4gIFBIT1RPOiAnYXRvbWljOnBob3RvJyxcbiAgVklERU86ICdhdG9taWM6dmlkZW8nLFxufTtcblxuLyoqXG4gKiBAY29uc3RhbnQgSW5saW5lXG4gKi9cbmV4cG9ydCBjb25zdCBJbmxpbmUgPSB7XG4gIEJPTEQ6ICdCT0xEJyxcbiAgQ09ERTogJ0NPREUnLFxuICBJVEFMSUM6ICdJVEFMSUMnLFxuICBTVFJJS0VUSFJPVUdIOiAnU1RSSUtFVEhST1VHSCcsXG4gIFVOREVSTElORTogJ1VOREVSTElORScsXG4gIEhJR0hMSUdIVDogJ0hJR0hMSUdIVCcsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBFbnRpdHlcbiAqL1xuZXhwb3J0IGNvbnN0IEVudGl0eSA9IHtcbiAgTElOSzogJ0xJTksnLFxufTtcblxuLyoqXG4gKiBAY29uc3RhbnQgSFlQRVJMSU5LXG4gKi9cbmV4cG9ydCBjb25zdCBIWVBFUkxJTksgPSAnaHlwZXJsaW5rJztcblxuLyoqXG4gKiBDb25zdGFudHMgdG8gaGFuZGxlIGtleSBjb21tYW5kc1xuICovXG5leHBvcnQgY29uc3QgSEFORExFRCA9ICdoYW5kbGVkJztcbmV4cG9ydCBjb25zdCBOT1RfSEFORExFRCA9ICdub3RfaGFuZGxlZCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgQmxvY2tzLFxuICBJbmxpbmUsXG4gIEVudGl0eSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29uc3RhbnRzLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IEVkaXRvciBmcm9tICcuL0VkaXRvcic7XG5cbmV4cG9ydCB7IE1heWFzaEVkaXRvciB9IGZyb20gJy4vRWRpdG9yJztcblxuZXhwb3J0IHsgY3JlYXRlRWRpdG9yU3RhdGUgfSBmcm9tICcuL0VkaXRvclN0YXRlJztcblxuZXhwb3J0IGRlZmF1bHQgRWRpdG9yO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuZXhwb3J0IGNvbnN0IHN0eWxlID0ge1xuICByb290OiB7XG4gICAgcGFkZGluZzogMjAsXG4gICAgd2lkdGg6IDYwMCxcbiAgfSxcbiAgZWRpdG9yOiB7XG4gICAgYm9yZGVyOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgIGN1cnNvcjogJ3RleHQnLFxuICAgIGZvbnRTaXplOiAxNixcbiAgICBtaW5IZWlnaHQ6IDQwLFxuICAgIHBhZGRpbmc6IDEwLFxuICB9LFxuICBidXR0b246IHtcbiAgICBtYXJnaW5Ub3A6IDEwLFxuICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gIH0sXG4gIGhhbmRsZToge1xuICAgIGNvbG9yOiAncmdiYSg5OCwgMTc3LCAyNTQsIDEuMCknLFxuICAgIGRpcmVjdGlvbjogJ2x0cicsXG4gICAgdW5pY29kZUJpZGk6ICdiaWRpLW92ZXJyaWRlJyxcbiAgfSxcbiAgaGFzaHRhZzoge1xuICAgIGNvbG9yOiAncmdiYSg5NSwgMTg0LCAxMzgsIDEuMCknLFxuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3Ivc3R5bGUvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9