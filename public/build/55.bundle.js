webpackJsonp([55],{

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Switch/Switch.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Switch/Switch.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref; //  weak

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _SwitchBase = __webpack_require__(/*! ../internal/SwitchBase */ "./node_modules/material-ui/internal/SwitchBase.js");

var _SwitchBase2 = _interopRequireDefault(_SwitchBase);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-flex',
      width: 62,
      position: 'relative',
      flexShrink: 0
    },
    bar: {
      borderRadius: 7,
      display: 'block',
      position: 'absolute',
      width: 34,
      height: 14,
      top: '50%',
      marginTop: -7,
      left: '50%',
      marginLeft: -17,
      transition: theme.transitions.create(['opacity', 'background-color'], {
        duration: theme.transitions.duration.shortest
      }),
      backgroundColor: theme.palette.type === 'light' ? '#000' : '#fff',
      opacity: theme.palette.type === 'light' ? 0.38 : 0.3
    },
    icon: {
      boxShadow: theme.shadows[1],
      backgroundColor: 'currentColor',
      width: 20,
      height: 20,
      borderRadius: '50%'
    },
    // For SwitchBase
    default: {
      zIndex: 1,
      color: theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[400],
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })
    },
    checked: {
      color: theme.palette.primary[500],
      transform: 'translateX(14px)',
      '& + $bar': {
        backgroundColor: theme.palette.primary[500],
        opacity: 0.5
      }
    },
    disabled: {
      color: theme.palette.type === 'light' ? theme.palette.grey[400] : theme.palette.grey[800],
      '& + $bar': {
        backgroundColor: theme.palette.type === 'light' ? '#000' : '#fff',
        opacity: theme.palette.type === 'light' ? 0.12 : 0.1
      }
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * If `true`, the component is checked.
   */
  checked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * The icon to display when the component is checked.
   * If a string is provided, it will be used as a font ligature.
   */
  checkedIcon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   */
  defaultChecked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the switch will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the ripple effect will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * The icon to display when the component is unchecked.
   * If a string is provided, it will be used as a font ligature.
   */
  icon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Properties applied to the `input` element.
   */
  inputProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Use that property to pass a ref callback to the native input component.
   */
  inputRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /*
   * @ignore
   */
  name: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Callback fired when the state is changed.
   *
   * @param {object} event The event source of the callback
   * @param {boolean} checked The `checked` value of the switch
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  tabIndex: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * The value of the component.
   */
  value: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function Switch(props) {
  var classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['classes', 'className']);

  var icon = _react2.default.createElement('span', { className: classes.icon });

  return _react2.default.createElement(
    'span',
    { className: (0, _classnames2.default)(classes.root, className) },
    _react2.default.createElement(_SwitchBase2.default, (0, _extends3.default)({
      icon: icon,
      classes: {
        default: classes.default,
        checked: classes.checked,
        disabled: classes.disabled
      },
      checkedIcon: icon
    }, other)),
    _react2.default.createElement('span', { className: classes.bar })
  );
}

Switch.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,
  checked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),
  checkedIcon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), (0, _defineProperty3.default)(_ref, 'defaultChecked', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool), (0, _defineProperty3.default)(_ref, 'disabled', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool), (0, _defineProperty3.default)(_ref, 'disableRipple', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool), (0, _defineProperty3.default)(_ref, 'icon', typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)), (0, _defineProperty3.default)(_ref, 'inputProps', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'inputRef', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func), (0, _defineProperty3.default)(_ref, 'name', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), (0, _defineProperty3.default)(_ref, 'onChange', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func), (0, _defineProperty3.default)(_ref, 'tabIndex', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string])), (0, _defineProperty3.default)(_ref, 'value', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSwitch' })(Switch);

/***/ }),

/***/ "./node_modules/material-ui/Switch/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Switch/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Switch = __webpack_require__(/*! ./Switch */ "./node_modules/material-ui/Switch/Switch.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Switch).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/internal/SwitchBase.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/SwitchBase.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! ../IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _CheckBoxOutlineBlank = __webpack_require__(/*! ../svg-icons/CheckBoxOutlineBlank */ "./node_modules/material-ui/svg-icons/CheckBoxOutlineBlank.js");

var _CheckBoxOutlineBlank2 = _interopRequireDefault(_CheckBoxOutlineBlank);

var _CheckBox = __webpack_require__(/*! ../svg-icons/CheckBox */ "./node_modules/material-ui/svg-icons/CheckBox.js");

var _CheckBox2 = _interopRequireDefault(_CheckBox);

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = {
  root: {
    display: 'inline-flex',
    alignItems: 'center',
    transition: 'none'
  },
  input: {
    cursor: 'inherit',
    position: 'absolute',
    opacity: 0,
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
    margin: 0,
    padding: 0
  },
  default: {},
  checked: {},
  disabled: {}
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * If `true`, the component is checked.
   */
  checked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * The icon to display when the component is checked.
   * If a string is provided, it will be used as a font ligature.
   */
  checkedIcon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * @ignore
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   */
  defaultChecked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the switch will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the ripple effect will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The icon to display when the component is unchecked.
   * If a string is provided, it will be used as a font ligature.
   */
  icon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * If `true`, the component appears indeterminate.
   */
  indeterminate: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * The icon to display when the component is indeterminate.
   * If a string is provided, it will be used as a font ligature.
   */
  indeterminateIcon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Properties applied to the `input` element.
   */
  inputProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Use that property to pass a ref callback to the native input component.
   */
  inputRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The input component property `type`.
   */
  inputType: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired,

  /*
   * @ignore
   */
  name: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Callback fired when the state is changed.
   *
   * @param {object} event The event source of the callback
   * @param {boolean} checked The `checked` value of the switch
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  tabIndex: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * The value of the component.
   */
  value: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

// NB: If changed, please update Checkbox, Switch and Radio
// so that the API documentation is updated.

/**
 * @ignore - internal component.
 */
var SwitchBase = function (_React$Component) {
  (0, _inherits3.default)(SwitchBase, _React$Component);

  function SwitchBase() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, SwitchBase);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = SwitchBase.__proto__ || (0, _getPrototypeOf2.default)(SwitchBase)).call.apply(_ref, [this].concat(args))), _this), _this.state = {}, _this.input = null, _this.button = null, _this.isControlled = null, _this.handleInputChange = function (event) {
      var checked = event.target.checked;

      if (!_this.isControlled) {
        _this.setState({ checked: checked });
      }

      if (_this.props.onChange) {
        _this.props.onChange(event, checked);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(SwitchBase, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var props = this.props;


      this.isControlled = props.checked !== undefined;

      if (!this.isControlled) {
        // not controlled, use internal state
        this.setState({
          checked: props.defaultChecked !== undefined ? props.defaultChecked : false
        });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames,
          _this2 = this;

      var _props = this.props,
          checkedProp = _props.checked,
          classes = _props.classes,
          classNameProp = _props.className,
          checkedIcon = _props.checkedIcon,
          disabledProp = _props.disabled,
          iconProp = _props.icon,
          inputProps = _props.inputProps,
          inputRef = _props.inputRef,
          inputType = _props.inputType,
          name = _props.name,
          onChange = _props.onChange,
          tabIndex = _props.tabIndex,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['checked', 'classes', 'className', 'checkedIcon', 'disabled', 'icon', 'inputProps', 'inputRef', 'inputType', 'name', 'onChange', 'tabIndex', 'value']);
      var muiFormControl = this.context.muiFormControl;

      var disabled = disabledProp;

      if (muiFormControl) {
        if (typeof disabled === 'undefined') {
          disabled = muiFormControl.disabled;
        }
      }

      var checked = this.isControlled ? checkedProp : this.state.checked;
      var className = (0, _classnames2.default)(classes.root, classes.default, classNameProp, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.checked, checked), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames));

      var icon = checked ? checkedIcon : iconProp;

      if (typeof icon === 'string') {
        icon = _react2.default.createElement(
          _Icon2.default,
          null,
          icon
        );
      }

      return _react2.default.createElement(
        _IconButton2.default,
        (0, _extends3.default)({
          component: 'span',
          className: className,
          disabled: disabled,
          tabIndex: null,
          role: undefined,
          rootRef: function rootRef(node) {
            _this2.button = node;
          }
        }, other),
        icon,
        _react2.default.createElement('input', (0, _extends3.default)({
          type: inputType,
          name: name,
          checked: this.isControlled ? checkedProp : undefined,
          onChange: this.handleInputChange,
          className: classes.input,
          disabled: disabled,
          tabIndex: tabIndex,
          value: value
        }, inputProps, {
          ref: function ref(node) {
            _this2.input = node;
            if (inputRef) {
              inputRef(node);
            }
          }
        }))
      );
    }
  }]);
  return SwitchBase;
}(_react2.default.Component);

SwitchBase.defaultProps = {
  checkedIcon: _react2.default.createElement(_CheckBox2.default, null), // defaultCheckedIcon
  disableRipple: false,
  icon: _react2.default.createElement(_CheckBoxOutlineBlank2.default, null), // defaultIcon
  inputType: 'checkbox'
};
SwitchBase.contextTypes = {
  muiFormControl: _propTypes2.default.object
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSwitchBase' })(SwitchBase);

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/CheckBox.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/CheckBox.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.11 0 2-.9 2-2V5c0-1.1-.89-2-2-2zm-9 14l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z' });

var CheckBox = function CheckBox(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
CheckBox = (0, _pure2.default)(CheckBox);
CheckBox.muiName = 'SvgIcon';

exports.default = CheckBox;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/CheckBoxOutlineBlank.js":
/*!********************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/CheckBoxOutlineBlank.js ***!
  \********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z' });

var CheckBoxOutlineBlank = function CheckBoxOutlineBlank(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
CheckBoxOutlineBlank = (0, _pure2.default)(CheckBoxOutlineBlank);
CheckBoxOutlineBlank.muiName = 'SvgIcon';

exports.default = CheckBoxOutlineBlank;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/CoursePage/TestYourself/QuestionCreate.js":
/*!*************************************************************************!*\
  !*** ./src/client/containers/CoursePage/TestYourself/QuestionCreate.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Switch = __webpack_require__(/*! material-ui/Switch */ "./node_modules/material-ui/Switch/index.js");

var _Switch2 = _interopRequireDefault(_Switch);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Input = __webpack_require__(/*! ../../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      padding: '10px'
    }
  };
};

var QuestionCreate = function (_Component) {
  (0, _inherits3.default)(QuestionCreate, _Component);

  function QuestionCreate(props) {
    (0, _classCallCheck3.default)(this, QuestionCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (QuestionCreate.__proto__ || (0, _getPrototypeOf2.default)(QuestionCreate)).call(this, props));

    _this.onChangeToggle = function (name) {
      return function (e) {
        _this.setState((0, _defineProperty3.default)({}, name, e.target.checked));

        // console.log(this.state.optionAAnswer);
        // console.log(this.state.optionBAnswer);
        // console.log(this.state.optionCAnswer);
        // console.log(this.state.optionDAnswer);
      };
    };

    _this.onChange = function (name) {
      return function (e) {
        _this.setState((0, _defineProperty3.default)({}, name, e.target.value));
      };
    };

    _this.onSubmit = function (e) {
      e.preventDefault();

      var _this$state = _this.state,
          title = _this$state.title,
          description = _this$state.description,
          optionA = _this$state.optionA,
          optionAAnswer = _this$state.optionAAnswer,
          optionB = _this$state.optionB,
          optionBAnswer = _this$state.optionBAnswer,
          optionC = _this$state.optionC,
          optionCAnswer = _this$state.optionCAnswer,
          optionD = _this$state.optionD,
          optionDAnswer = _this$state.optionDAnswer;


      _this.props.createQuestion({
        title: title,
        description: description,
        options: [{ title: optionA, answer: optionAAnswer }, { title: optionB, answer: optionBAnswer }, { title: optionC, answer: optionCAnswer }, { title: optionD, answer: optionDAnswer }]
      });

      _this.setState({
        title: '',
        description: '',

        optionA: '',
        optionAAnswer: false,

        optionB: '',
        optionBAnswer: false,

        optionC: '',
        optionCAnswer: false,

        optionD: '',
        optionDAnswer: false
      });
    };

    _this.state = {
      title: '',
      titleLength: 0,
      description: '',
      descriptionLength: 0,

      placeholder: 'Start typing here...',

      optionA: '',
      optionALength: 0,
      optionAAnswer: false,
      optionADescription: '',

      optionB: '',
      optionBLength: 0,
      optionBAnswer: false,
      optionBDescription: '',

      optionC: '',
      optionCLength: 0,
      optionCAnswer: false,
      optionCDescription: '',

      optionD: '',
      optionDLength: 0,
      optionDAnswer: false,
      optionDDescription: ''
    };
    return _this;
  }

  (0, _createClass3.default)(QuestionCreate, [{
    key: 'render',
    value: function render() {
      var _state = this.state,
          title = _state.title,
          placeholder = _state.placeholder,
          description = _state.description,
          optionA = _state.optionA,
          optionAAnswer = _state.optionAAnswer,
          optionB = _state.optionB,
          optionBAnswer = _state.optionBAnswer,
          optionC = _state.optionC,
          optionCAnswer = _state.optionCAnswer,
          optionD = _state.optionD,
          optionDAnswer = _state.optionDAnswer;


      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _Card2.default,
          { raised: true },
          _react2.default.createElement(
            _Typography2.default,
            null,
            'Question:'
          ),
          _react2.default.createElement(_Input2.default, {
            value: title,
            placeholder: placeholder,
            onChange: this.onChange('title')
          }),
          _react2.default.createElement(
            _Typography2.default,
            null,
            'Description:'
          ),
          _react2.default.createElement(_Input2.default, {
            value: description,
            placeholder: placeholder,
            onChange: this.onChange('description')
          }),
          _react2.default.createElement(
            _Typography2.default,
            null,
            'Options'
          ),
          _react2.default.createElement(
            'h3',
            null,
            'A'
          ),
          _react2.default.createElement(_Input2.default, {
            value: optionA,
            placeholder: placeholder,
            onChange: this.onChange('optionA')
          }),
          _react2.default.createElement(_Switch2.default, {
            checked: optionAAnswer,
            onChange: this.onChangeToggle('optionAAnswer')
          }),
          _react2.default.createElement(
            'h3',
            null,
            'B'
          ),
          _react2.default.createElement(_Input2.default, {
            value: optionB,
            placeholder: placeholder,
            onChange: this.onChange('optionB')
          }),
          _react2.default.createElement(_Switch2.default, {
            checked: optionBAnswer,
            onChange: this.onChangeToggle('optionBAnswer')
          }),
          _react2.default.createElement(
            'h3',
            null,
            'C'
          ),
          _react2.default.createElement(_Input2.default, {
            value: optionC,
            placeholder: placeholder,
            onChange: this.onChange('optionC')
          }),
          _react2.default.createElement(_Switch2.default, {
            checked: optionCAnswer,
            onChange: this.onChangeToggle('optionCAnswer')
          }),
          _react2.default.createElement(
            'h3',
            null,
            'D'
          ),
          _react2.default.createElement(_Input2.default, {
            value: optionD,
            placeholder: placeholder,
            onChange: this.onChange('optionD')
          }),
          _react2.default.createElement(_Switch2.default, {
            checked: optionDAnswer,
            onChange: this.onChangeToggle('optionDAnswer')
          }),
          _react2.default.createElement(
            _Button2.default,
            { raised: true, onClick: this.onSubmit },
            'Create'
          )
        )
      );
    }
  }]);
  return QuestionCreate;
}(_react.Component);

QuestionCreate.propTypes = {
  createQuestion: _propTypes2.default.func.isRequired
};
exports.default = (0, _withStyles2.default)(styles)(QuestionCreate);

/***/ }),

/***/ "./src/client/containers/CoursePage/TestYourself/index.js":
/*!****************************************************************!*\
  !*** ./src/client/containers/CoursePage/TestYourself/index.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Loading = __webpack_require__(/*! ../../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _QuestionCreate = __webpack_require__(/*! ./QuestionCreate */ "./src/client/containers/CoursePage/TestYourself/QuestionCreate.js");

var _QuestionCreate2 = _interopRequireDefault(_QuestionCreate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AsyncQuestion = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(64).then(__webpack_require__.bind(null, /*! ./Question */ "./src/client/containers/CoursePage/TestYourself/Question.js"));
  },
  modules: ['./Question'],
  loading: _Loading2.default
});

var styles = function styles(theme) {
  return {
    root: {
      padding: '1%'
    }
  };
};

var TestYourself = function (_Component) {
  (0, _inherits3.default)(TestYourself, _Component);

  function TestYourself(props) {
    (0, _classCallCheck3.default)(this, TestYourself);

    var _this = (0, _possibleConstructorReturn3.default)(this, (TestYourself.__proto__ || (0, _getPrototypeOf2.default)(TestYourself)).call(this, props));

    _this.createQuestion = function (_ref) {
      var title = _ref.title,
          description = _ref.description,
          options = _ref.options;
      var questions = _this.state.questions;


      _this.setState({
        questions: [].concat((0, _toConsumableArray3.default)(questions), [{
          qId: 6, // questions.length,
          title: title,
          description: description,
          options: options
        }])
      });
    };

    _this.state = {
      questions: [{
        qId: 1,
        title: 'Question 1',
        description: 'Test Description',
        options: [{
          title: 'Option A',
          answer: true,
          explaination: 'Test Explaination'
        }, {
          title: 'Option B',
          answer: true,
          explaination: 'Test Explaination'
        }, { title: 'Option C', explaination: 'Test Explaination' }, { title: 'Option D', explaination: 'Test Explaination' }]
      }, {
        qId: 2,
        title: 'Question 2',
        description: '2 Test Description',
        options: [{
          title: 'Option A',
          answer: true,
          explaination: 'Test Explaination'
        }, {
          title: 'Option B',
          answer: true,
          explaination: 'Test Explaination'
        }, { title: 'Option C', explaination: 'Test Explaination' }, { title: 'Option D', explaination: 'Test Explaination' }]
      }]
    };
    return _this;
  }

  (0, _createClass3.default)(TestYourself, [{
    key: 'render',
    value: function render() {
      var questions = this.state.questions;


      return _react2.default.createElement(
        'div',
        { className: this.props.classes.root },
        _react2.default.createElement(_QuestionCreate2.default, {
          questions: questions,
          createQuestion: this.createQuestion
        }),
        questions.map(function (q) {
          return _react2.default.createElement(AsyncQuestion, (0, _extends3.default)({ key: q.qId }, q));
        })
      );
    }
  }]);
  return TestYourself;
}(_react.Component);

TestYourself.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired,
  match: _propTypes2.default.object.isRequired,
  location: _propTypes2.default.object.isRequired,
  history: _propTypes2.default.object.isRequired
};
exports.default = (0, _withStyles2.default)(styles)(TestYourself);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Td2l0Y2gvU3dpdGNoLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Td2l0Y2gvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1N3aXRjaEJhc2UuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9DaGVja0JveC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0NoZWNrQm94T3V0bGluZUJsYW5rLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9UZXN0WW91cnNlbGYvUXVlc3Rpb25DcmVhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvVGVzdFlvdXJzZWxmL2luZGV4LmpzIl0sIm5hbWVzIjpbInN0eWxlcyIsInJvb3QiLCJkaXNwbGF5IiwiaW5wdXQiLCJmbGV4R3JvdyIsImJvcmRlciIsImJvcmRlclJhZGl1cyIsInBhZGRpbmciLCJvdXRsaW5lIiwiYm9yZGVyQ29sb3IiLCJib3hTaGFkb3ciLCJJbnB1dCIsImNsYXNzZXMiLCJvbkNoYW5nZSIsInZhbHVlIiwicGxhY2Vob2xkZXIiLCJkaXNhYmxlZCIsInR5cGUiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiZnVuYyIsIm9uZU9mVHlwZSIsInN0cmluZyIsIm51bWJlciIsImJvb2wiLCJ0aGVtZSIsIlF1ZXN0aW9uQ3JlYXRlIiwicHJvcHMiLCJvbkNoYW5nZVRvZ2dsZSIsIm5hbWUiLCJlIiwic2V0U3RhdGUiLCJ0YXJnZXQiLCJjaGVja2VkIiwib25TdWJtaXQiLCJwcmV2ZW50RGVmYXVsdCIsInN0YXRlIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsIm9wdGlvbkEiLCJvcHRpb25BQW5zd2VyIiwib3B0aW9uQiIsIm9wdGlvbkJBbnN3ZXIiLCJvcHRpb25DIiwib3B0aW9uQ0Fuc3dlciIsIm9wdGlvbkQiLCJvcHRpb25EQW5zd2VyIiwiY3JlYXRlUXVlc3Rpb24iLCJvcHRpb25zIiwiYW5zd2VyIiwidGl0bGVMZW5ndGgiLCJkZXNjcmlwdGlvbkxlbmd0aCIsIm9wdGlvbkFMZW5ndGgiLCJvcHRpb25BRGVzY3JpcHRpb24iLCJvcHRpb25CTGVuZ3RoIiwib3B0aW9uQkRlc2NyaXB0aW9uIiwib3B0aW9uQ0xlbmd0aCIsIm9wdGlvbkNEZXNjcmlwdGlvbiIsIm9wdGlvbkRMZW5ndGgiLCJvcHRpb25ERGVzY3JpcHRpb24iLCJBc3luY1F1ZXN0aW9uIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJUZXN0WW91cnNlbGYiLCJxdWVzdGlvbnMiLCJxSWQiLCJleHBsYWluYXRpb24iLCJtYXAiLCJxIiwidXNlciIsImNvdXJzZSIsIm1hdGNoIiwibG9jYXRpb24iLCJoaXN0b3J5Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STtBQUM1STs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RTtBQUM5RTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBLGFBQWEsMEJBQTBCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHdCQUF3QixjOzs7Ozs7Ozs7Ozs7O0FDck83RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsU0FBUzs7QUFFVDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxvREFBb0QsMEJBQTBCOztBQUU5RTtBQUNBO0FBQ0EsS0FBSyxnRUFBZ0U7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsS0FBSztBQUNMLDJDQUEyQyx5QkFBeUI7QUFDcEU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUM1TXpFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxhQUFhO0FBQ2IsYUFBYTtBQUNiO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxRQUFRO0FBQ3JCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBLDhOQUE4TjtBQUM5Tjs7QUFFQTtBQUNBLHdCQUF3QixtQkFBbUI7QUFDM0M7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwrR0FBK0c7O0FBRS9HOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCx3QkFBd0IsYzs7Ozs7Ozs7Ozs7OztBQzVVN0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCwySUFBMkk7O0FBRTdMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ25DQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0Esa0RBQWtELGtHQUFrRzs7QUFFcEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHVDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNUJBOzs7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsU0FBUztBQUNiQyxRQUFNO0FBQ0pDLGFBQVM7QUFETCxHQURPO0FBSWJDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTEMsa0JBQWMsS0FIVDtBQUlMQyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RDLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEY7QUFMTjtBQUpNLENBQWYsQyxDQVpBOzs7Ozs7O0FBNkJBLElBQU1DLFFBQVEsU0FBUkEsS0FBUTtBQUFBLE1BQUdDLE9BQUgsUUFBR0EsT0FBSDtBQUFBLE1BQVlDLFFBQVosUUFBWUEsUUFBWjtBQUFBLE1BQXNCQyxLQUF0QixRQUFzQkEsS0FBdEI7QUFBQSxNQUE2QkMsV0FBN0IsUUFBNkJBLFdBQTdCO0FBQUEsTUFBMENDLFFBQTFDLFFBQTBDQSxRQUExQztBQUFBLE1BQW9EQyxJQUFwRCxRQUFvREEsSUFBcEQ7QUFBQSxTQUNaO0FBQUE7QUFBQSxNQUFLLFdBQVdMLFFBQVFYLElBQXhCO0FBQ0U7QUFDRSxZQUFNZ0IsUUFBUSxNQURoQjtBQUVFLG1CQUFhRixlQUFlLE9BRjlCO0FBR0UsYUFBT0QsS0FIVDtBQUlFLGdCQUFVRCxRQUpaO0FBS0UsaUJBQVdELFFBQVFULEtBTHJCO0FBTUUsZ0JBQVUsQ0FBQyxDQUFDYTtBQU5kO0FBREYsR0FEWTtBQUFBLENBQWQ7O0FBYUFMLE1BQU1PLFNBQU4sR0FBa0I7QUFDaEJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFY7QUFFaEJQLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUQsVUFGVDtBQUdoQk4sU0FBTyxvQkFBVVEsU0FBVixDQUFvQixDQUFDLG9CQUFVQyxNQUFYLEVBQW1CLG9CQUFVQyxNQUE3QixDQUFwQixFQUEwREosVUFIakQ7QUFJaEJMLGVBQWEsb0JBQVVRLE1BSlA7QUFLaEJOLFFBQU0sb0JBQVVNLE1BTEE7QUFNaEJQLFlBQVUsb0JBQVVTO0FBTkosQ0FBbEI7O2tCQVNlLDBCQUFXekIsTUFBWCxFQUFtQlcsS0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25EZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNWCxTQUFTLFNBQVRBLE1BQVMsQ0FBQzBCLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCekIsVUFBTTtBQUNKTSxlQUFTO0FBREw7QUFEbUIsR0FBWjtBQUFBLENBQWY7O0lBTU1vQixjOzs7QUFLSiwwQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLHNKQUNYQSxLQURXOztBQUFBLFVBZ0NuQkMsY0FoQ21CLEdBZ0NGLFVBQUNDLElBQUQ7QUFBQSxhQUFVLFVBQUNDLENBQUQsRUFBTztBQUNoQyxjQUFLQyxRQUFMLG1DQUNHRixJQURILEVBQ1VDLEVBQUVFLE1BQUYsQ0FBU0MsT0FEbkI7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDRCxPQVRnQjtBQUFBLEtBaENFOztBQUFBLFVBMkNuQnJCLFFBM0NtQixHQTJDUixVQUFDaUIsSUFBRDtBQUFBLGFBQVUsVUFBQ0MsQ0FBRCxFQUFPO0FBQzFCLGNBQUtDLFFBQUwsbUNBQ0dGLElBREgsRUFDVUMsRUFBRUUsTUFBRixDQUFTbkIsS0FEbkI7QUFHRCxPQUpVO0FBQUEsS0EzQ1E7O0FBQUEsVUFpRG5CcUIsUUFqRG1CLEdBaURSLFVBQUNKLENBQUQsRUFBTztBQUNoQkEsUUFBRUssY0FBRjs7QUFEZ0Isd0JBY1osTUFBS0MsS0FkTztBQUFBLFVBSWRDLEtBSmMsZUFJZEEsS0FKYztBQUFBLFVBS2RDLFdBTGMsZUFLZEEsV0FMYztBQUFBLFVBTWRDLE9BTmMsZUFNZEEsT0FOYztBQUFBLFVBT2RDLGFBUGMsZUFPZEEsYUFQYztBQUFBLFVBUWRDLE9BUmMsZUFRZEEsT0FSYztBQUFBLFVBU2RDLGFBVGMsZUFTZEEsYUFUYztBQUFBLFVBVWRDLE9BVmMsZUFVZEEsT0FWYztBQUFBLFVBV2RDLGFBWGMsZUFXZEEsYUFYYztBQUFBLFVBWWRDLE9BWmMsZUFZZEEsT0FaYztBQUFBLFVBYWRDLGFBYmMsZUFhZEEsYUFiYzs7O0FBZ0JoQixZQUFLbkIsS0FBTCxDQUFXb0IsY0FBWCxDQUEwQjtBQUN4QlYsb0JBRHdCO0FBRXhCQyxnQ0FGd0I7QUFHeEJVLGlCQUFTLENBQ1AsRUFBRVgsT0FBT0UsT0FBVCxFQUFrQlUsUUFBUVQsYUFBMUIsRUFETyxFQUVQLEVBQUVILE9BQU9JLE9BQVQsRUFBa0JRLFFBQVFQLGFBQTFCLEVBRk8sRUFHUCxFQUFFTCxPQUFPTSxPQUFULEVBQWtCTSxRQUFRTCxhQUExQixFQUhPLEVBSVAsRUFBRVAsT0FBT1EsT0FBVCxFQUFrQkksUUFBUUgsYUFBMUIsRUFKTztBQUhlLE9BQTFCOztBQVdBLFlBQUtmLFFBQUwsQ0FBYztBQUNaTSxlQUFPLEVBREs7QUFFWkMscUJBQWEsRUFGRDs7QUFJWkMsaUJBQVMsRUFKRztBQUtaQyx1QkFBZSxLQUxIOztBQU9aQyxpQkFBUyxFQVBHO0FBUVpDLHVCQUFlLEtBUkg7O0FBVVpDLGlCQUFTLEVBVkc7QUFXWkMsdUJBQWUsS0FYSDs7QUFhWkMsaUJBQVMsRUFiRztBQWNaQyx1QkFBZTtBQWRILE9BQWQ7QUFnQkQsS0E1RmtCOztBQUVqQixVQUFLVixLQUFMLEdBQWE7QUFDWEMsYUFBTyxFQURJO0FBRVhhLG1CQUFhLENBRkY7QUFHWFosbUJBQWEsRUFIRjtBQUlYYSx5QkFBbUIsQ0FKUjs7QUFNWHJDLG1CQUFhLHNCQU5GOztBQVFYeUIsZUFBUyxFQVJFO0FBU1hhLHFCQUFlLENBVEo7QUFVWFoscUJBQWUsS0FWSjtBQVdYYSwwQkFBb0IsRUFYVDs7QUFhWFosZUFBUyxFQWJFO0FBY1hhLHFCQUFlLENBZEo7QUFlWFoscUJBQWUsS0FmSjtBQWdCWGEsMEJBQW9CLEVBaEJUOztBQWtCWFosZUFBUyxFQWxCRTtBQW1CWGEscUJBQWUsQ0FuQko7QUFvQlhaLHFCQUFlLEtBcEJKO0FBcUJYYSwwQkFBb0IsRUFyQlQ7O0FBdUJYWixlQUFTLEVBdkJFO0FBd0JYYSxxQkFBZSxDQXhCSjtBQXlCWFoscUJBQWUsS0F6Qko7QUEwQlhhLDBCQUFvQjtBQTFCVCxLQUFiO0FBRmlCO0FBOEJsQjs7Ozs2QkFnRVE7QUFBQSxtQkFhSCxLQUFLdkIsS0FiRjtBQUFBLFVBRUxDLEtBRkssVUFFTEEsS0FGSztBQUFBLFVBR0x2QixXQUhLLFVBR0xBLFdBSEs7QUFBQSxVQUlMd0IsV0FKSyxVQUlMQSxXQUpLO0FBQUEsVUFLTEMsT0FMSyxVQUtMQSxPQUxLO0FBQUEsVUFNTEMsYUFOSyxVQU1MQSxhQU5LO0FBQUEsVUFPTEMsT0FQSyxVQU9MQSxPQVBLO0FBQUEsVUFRTEMsYUFSSyxVQVFMQSxhQVJLO0FBQUEsVUFTTEMsT0FUSyxVQVNMQSxPQVRLO0FBQUEsVUFVTEMsYUFWSyxVQVVMQSxhQVZLO0FBQUEsVUFXTEMsT0FYSyxVQVdMQSxPQVhLO0FBQUEsVUFZTEMsYUFaSyxVQVlMQSxhQVpLOzs7QUFlUCxhQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxZQUFNLFlBQU47QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREY7QUFFRTtBQUNFLG1CQUFPVCxLQURUO0FBRUUseUJBQWF2QixXQUZmO0FBR0Usc0JBQVUsS0FBS0YsUUFBTCxDQUFjLE9BQWQ7QUFIWixZQUZGO0FBUUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQVJGO0FBU0U7QUFDRSxtQkFBTzBCLFdBRFQ7QUFFRSx5QkFBYXhCLFdBRmY7QUFHRSxzQkFBVSxLQUFLRixRQUFMLENBQWMsYUFBZDtBQUhaLFlBVEY7QUFlRTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBZkY7QUFpQkU7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQWpCRjtBQWtCRTtBQUNFLG1CQUFPMkIsT0FEVDtBQUVFLHlCQUFhekIsV0FGZjtBQUdFLHNCQUFVLEtBQUtGLFFBQUwsQ0FBYyxTQUFkO0FBSFosWUFsQkY7QUF1QkU7QUFDRSxxQkFBUzRCLGFBRFg7QUFFRSxzQkFBVSxLQUFLWixjQUFMLENBQW9CLGVBQXBCO0FBRlosWUF2QkY7QUE0QkU7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQTVCRjtBQTZCRTtBQUNFLG1CQUFPYSxPQURUO0FBRUUseUJBQWEzQixXQUZmO0FBR0Usc0JBQVUsS0FBS0YsUUFBTCxDQUFjLFNBQWQ7QUFIWixZQTdCRjtBQWtDRTtBQUNFLHFCQUFTOEIsYUFEWDtBQUVFLHNCQUFVLEtBQUtkLGNBQUwsQ0FBb0IsZUFBcEI7QUFGWixZQWxDRjtBQXVDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBdkNGO0FBd0NFO0FBQ0UsbUJBQU9lLE9BRFQ7QUFFRSx5QkFBYTdCLFdBRmY7QUFHRSxzQkFBVSxLQUFLRixRQUFMLENBQWMsU0FBZDtBQUhaLFlBeENGO0FBNkNFO0FBQ0UscUJBQVNnQyxhQURYO0FBRUUsc0JBQVUsS0FBS2hCLGNBQUwsQ0FBb0IsZUFBcEI7QUFGWixZQTdDRjtBQWtERTtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBbERGO0FBbURFO0FBQ0UsbUJBQU9pQixPQURUO0FBRUUseUJBQWEvQixXQUZmO0FBR0Usc0JBQVUsS0FBS0YsUUFBTCxDQUFjLFNBQWQ7QUFIWixZQW5ERjtBQXdERTtBQUNFLHFCQUFTa0MsYUFEWDtBQUVFLHNCQUFVLEtBQUtsQixjQUFMLENBQW9CLGVBQXBCO0FBRlosWUF4REY7QUE2REU7QUFBQTtBQUFBLGNBQVEsWUFBUixFQUFlLFNBQVMsS0FBS00sUUFBN0I7QUFBQTtBQUFBO0FBN0RGO0FBREYsT0FERjtBQXFFRDs7Ozs7QUF2TEdSLGMsQ0FDR1QsUyxHQUFZO0FBQ2pCOEIsa0JBQWdCLG9CQUFVM0IsSUFBVixDQUFlRDtBQURkLEM7a0JBeUxOLDBCQUFXcEIsTUFBWCxFQUFtQjJCLGNBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM01mOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTWtDLGdCQUFnQiw2QkFBUztBQUM3QkMsVUFBUTtBQUFBLFdBQU0sNkpBQU47QUFBQSxHQURxQjtBQUU3QkMsV0FBUyxDQUFDLFlBQUQsQ0FGb0I7QUFHN0JDO0FBSDZCLENBQVQsQ0FBdEI7O0FBTUEsSUFBTWhFLFNBQVMsU0FBVEEsTUFBUyxDQUFDMEIsS0FBRDtBQUFBLFNBQVk7QUFDekJ6QixVQUFNO0FBQ0pNLGVBQVM7QUFETDtBQURtQixHQUFaO0FBQUEsQ0FBZjs7SUFNTTBELFk7OztBQVVKLHdCQUFZckMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGtKQUNYQSxLQURXOztBQUFBLFVBOENuQm9CLGNBOUNtQixHQThDRixnQkFBcUM7QUFBQSxVQUFsQ1YsS0FBa0MsUUFBbENBLEtBQWtDO0FBQUEsVUFBM0JDLFdBQTJCLFFBQTNCQSxXQUEyQjtBQUFBLFVBQWRVLE9BQWMsUUFBZEEsT0FBYztBQUFBLFVBQzVDaUIsU0FENEMsR0FDOUIsTUFBSzdCLEtBRHlCLENBQzVDNkIsU0FENEM7OztBQUdwRCxZQUFLbEMsUUFBTCxDQUFjO0FBQ1prQyw4REFDS0EsU0FETCxJQUVFO0FBQ0VDLGVBQUssQ0FEUCxFQUNVO0FBQ1I3QixzQkFGRjtBQUdFQyxrQ0FIRjtBQUlFVTtBQUpGLFNBRkY7QUFEWSxPQUFkO0FBV0QsS0E1RGtCOztBQUVqQixVQUFLWixLQUFMLEdBQWE7QUFDWDZCLGlCQUFXLENBQ1Q7QUFDRUMsYUFBSyxDQURQO0FBRUU3QixlQUFPLFlBRlQ7QUFHRUMscUJBQWEsa0JBSGY7QUFJRVUsaUJBQVMsQ0FDUDtBQUNFWCxpQkFBTyxVQURUO0FBRUVZLGtCQUFRLElBRlY7QUFHRWtCLHdCQUFjO0FBSGhCLFNBRE8sRUFNUDtBQUNFOUIsaUJBQU8sVUFEVDtBQUVFWSxrQkFBUSxJQUZWO0FBR0VrQix3QkFBYztBQUhoQixTQU5PLEVBV1AsRUFBRTlCLE9BQU8sVUFBVCxFQUFxQjhCLGNBQWMsbUJBQW5DLEVBWE8sRUFZUCxFQUFFOUIsT0FBTyxVQUFULEVBQXFCOEIsY0FBYyxtQkFBbkMsRUFaTztBQUpYLE9BRFMsRUFvQlQ7QUFDRUQsYUFBSyxDQURQO0FBRUU3QixlQUFPLFlBRlQ7QUFHRUMscUJBQWEsb0JBSGY7QUFJRVUsaUJBQVMsQ0FDUDtBQUNFWCxpQkFBTyxVQURUO0FBRUVZLGtCQUFRLElBRlY7QUFHRWtCLHdCQUFjO0FBSGhCLFNBRE8sRUFNUDtBQUNFOUIsaUJBQU8sVUFEVDtBQUVFWSxrQkFBUSxJQUZWO0FBR0VrQix3QkFBYztBQUhoQixTQU5PLEVBV1AsRUFBRTlCLE9BQU8sVUFBVCxFQUFxQjhCLGNBQWMsbUJBQW5DLEVBWE8sRUFZUCxFQUFFOUIsT0FBTyxVQUFULEVBQXFCOEIsY0FBYyxtQkFBbkMsRUFaTztBQUpYLE9BcEJTO0FBREEsS0FBYjtBQUZpQjtBQTRDbEI7Ozs7NkJBa0JRO0FBQUEsVUFDQ0YsU0FERCxHQUNlLEtBQUs3QixLQURwQixDQUNDNkIsU0FERDs7O0FBR1AsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFXLEtBQUt0QyxLQUFMLENBQVdoQixPQUFYLENBQW1CWCxJQUFuQztBQUNFO0FBQ0UscUJBQVdpRSxTQURiO0FBRUUsMEJBQWdCLEtBQUtsQjtBQUZ2QixVQURGO0FBTUdrQixrQkFBVUcsR0FBVixDQUFjLFVBQUNDLENBQUQ7QUFBQSxpQkFBTyw4QkFBQyxhQUFELDJCQUFlLEtBQUtBLEVBQUVILEdBQXRCLElBQStCRyxDQUEvQixFQUFQO0FBQUEsU0FBZDtBQU5ILE9BREY7QUFVRDs7Ozs7QUFyRkdMLFksQ0FDRy9DLFMsR0FBWTtBQUNqQk4sV0FBUyxvQkFBVU8sTUFBVixDQUFpQkMsVUFEVDtBQUVqQm1ELFFBQU0sb0JBQVVwRCxNQUFWLENBQWlCQyxVQUZOO0FBR2pCb0QsVUFBUSxvQkFBVXJELE1BQVYsQ0FBaUJDLFVBSFI7QUFJakJxRCxTQUFPLG9CQUFVdEQsTUFBVixDQUFpQkMsVUFKUDtBQUtqQnNELFlBQVUsb0JBQVV2RCxNQUFWLENBQWlCQyxVQUxWO0FBTWpCdUQsV0FBUyxvQkFBVXhELE1BQVYsQ0FBaUJDO0FBTlQsQztrQkF1Rk4sMEJBQVdwQixNQUFYLEVBQW1CaUUsWUFBbkIsQyIsImZpbGUiOiI1NS5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuLi9JY29uJyk7XG5cbnZhciBfSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKTtcblxucmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX3JlYWN0SGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL3JlYWN0SGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBCdXR0b25CYXNlXG5cbi8vIEVuc3VyZSBDU1Mgc3BlY2lmaWNpdHlcblxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgICAgZmxleDogJzAgMCBhdXRvJyxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjQpLFxuICAgICAgd2lkdGg6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBoZWlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBwYWRkaW5nOiAwLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmUsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2JhY2tncm91bmQtY29sb3InLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdFxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIGNvbG9ySW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgZGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgbGFiZWw6IHtcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnaW5oZXJpdCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICB3aWR0aDogJzFlbScsXG4gICAgICBoZWlnaHQ6ICcxZW0nXG4gICAgfSxcbiAgICBrZXlib2FyZEZvY3VzZWQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSBuYXRpdmUgYnV0dG9uIGNvbXBvbmVudC5cbiAgICovXG4gIGJ1dHRvblJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIGVsZW1lbnQuXG4gICAqIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYW4gaWNvbiBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdpbmhlcml0JywgJ3ByaW1hcnknLCAnY29udHJhc3QnLCAnYWNjZW50J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJ1dHRvbiB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHJpcHBsZSB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufTtcblxuLyoqXG4gKiBSZWZlciB0byB0aGUgW0ljb25zXSgvc3R5bGUvaWNvbnMpIHNlY3Rpb24gb2YgdGhlIGRvY3VtZW50YXRpb25cbiAqIHJlZ2FyZGluZyB0aGUgYXZhaWxhYmxlIGljb24gb3B0aW9ucy5cbiAqL1xudmFyIEljb25CdXR0b24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJY29uQnV0dG9uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEljb25CdXR0b24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChJY29uQnV0dG9uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShJY29uQnV0dG9uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYnV0dG9uUmVmID0gX3Byb3BzLmJ1dHRvblJlZixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uUmVmJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2Rpc2FibGVkJywgJ3Jvb3RSZWYnXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2RlZmF1bHQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGlzYWJsZWQsIGRpc2FibGVkKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWUpLFxuICAgICAgICAgIGNlbnRlclJpcHBsZTogdHJ1ZSxcbiAgICAgICAgICBrZXlib2FyZEZvY3VzZWRDbGFzc05hbWU6IGNsYXNzZXMua2V5Ym9hcmRGb2N1c2VkLFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZFxuICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgIHJvb3RSZWY6IGJ1dHRvblJlZixcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5sYWJlbCB9LFxuICAgICAgICAgIHR5cGVvZiBjaGlsZHJlbiA9PT0gJ3N0cmluZycgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuaWNvbiB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApIDogX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgICBpZiAoKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KShjaGlsZCwgWydJY29uJywgJ1N2Z0ljb24nXSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5pY29uLCBjaGlsZC5wcm9wcy5jbGFzc05hbWUpXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgfSlcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb25CdXR0b247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdkZWZhdWx0JyxcbiAgZGlzYWJsZWQ6IGZhbHNlLFxuICBkaXNhYmxlUmlwcGxlOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uQnV0dG9uJyB9KShJY29uQnV0dG9uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb25CdXR0b24gPSByZXF1aXJlKCcuL0ljb25CdXR0b24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbkJ1dHRvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWY7IC8vICB3ZWFrXG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9Td2l0Y2hCYXNlID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvU3dpdGNoQmFzZScpO1xuXG52YXIgX1N3aXRjaEJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3dpdGNoQmFzZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdpbmxpbmUtZmxleCcsXG4gICAgICB3aWR0aDogNjIsXG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGZsZXhTaHJpbms6IDBcbiAgICB9LFxuICAgIGJhcjoge1xuICAgICAgYm9yZGVyUmFkaXVzOiA3LFxuICAgICAgZGlzcGxheTogJ2Jsb2NrJyxcbiAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgd2lkdGg6IDM0LFxuICAgICAgaGVpZ2h0OiAxNCxcbiAgICAgIHRvcDogJzUwJScsXG4gICAgICBtYXJnaW5Ub3A6IC03LFxuICAgICAgbGVmdDogJzUwJScsXG4gICAgICBtYXJnaW5MZWZ0OiAtMTcsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoWydvcGFjaXR5JywgJ2JhY2tncm91bmQtY29sb3InXSwge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlc3RcbiAgICAgIH0pLFxuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnR5cGUgPT09ICdsaWdodCcgPyAnIzAwMCcgOiAnI2ZmZicsXG4gICAgICBvcGFjaXR5OiB0aGVtZS5wYWxldHRlLnR5cGUgPT09ICdsaWdodCcgPyAwLjM4IDogMC4zXG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICBib3hTaGFkb3c6IHRoZW1lLnNoYWRvd3NbMV0sXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6ICdjdXJyZW50Q29sb3InLFxuICAgICAgd2lkdGg6IDIwLFxuICAgICAgaGVpZ2h0OiAyMCxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJSdcbiAgICB9LFxuICAgIC8vIEZvciBTd2l0Y2hCYXNlXG4gICAgZGVmYXVsdDoge1xuICAgICAgekluZGV4OiAxLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudHlwZSA9PT0gJ2xpZ2h0JyA/IHRoZW1lLnBhbGV0dGUuZ3JleVs1MF0gOiB0aGVtZS5wYWxldHRlLmdyZXlbNDAwXSxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgndHJhbnNmb3JtJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlc3RcbiAgICAgIH0pXG4gICAgfSxcbiAgICBjaGVja2VkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0sXG4gICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDE0cHgpJyxcbiAgICAgICcmICsgJGJhcic6IHtcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSxcbiAgICAgICAgb3BhY2l0eTogMC41XG4gICAgICB9XG4gICAgfSxcbiAgICBkaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudHlwZSA9PT0gJ2xpZ2h0JyA/IHRoZW1lLnBhbGV0dGUuZ3JleVs0MDBdIDogdGhlbWUucGFsZXR0ZS5ncmV5WzgwMF0sXG4gICAgICAnJiArICRiYXInOiB7XG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50eXBlID09PSAnbGlnaHQnID8gJyMwMDAnIDogJyNmZmYnLFxuICAgICAgICBvcGFjaXR5OiB0aGVtZS5wYWxldHRlLnR5cGUgPT09ICdsaWdodCcgPyAwLjEyIDogMC4xXG4gICAgICB9XG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgY29tcG9uZW50IGlzIGNoZWNrZWQuXG4gICAqL1xuICBjaGVja2VkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiB0byBkaXNwbGF5IHdoZW4gdGhlIGNvbXBvbmVudCBpcyBjaGVja2VkLlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGEgZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoZWNrZWRJY29uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgZGVmYXVsdENoZWNrZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBzd2l0Y2ggd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgcmlwcGxlIGVmZmVjdCB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIHRvIGRpc3BsYXkgd2hlbiB0aGUgY29tcG9uZW50IGlzIHVuY2hlY2tlZC5cbiAgICogSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBpY29uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFByb3BlcnRpZXMgYXBwbGllZCB0byB0aGUgYGlucHV0YCBlbGVtZW50LlxuICAgKi9cbiAgaW5wdXRQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgbmF0aXZlIGlucHV0IGNvbXBvbmVudC5cbiAgICovXG4gIGlucHV0UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBuYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBzdGF0ZSBpcyBjaGFuZ2VkLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2tcbiAgICogQHBhcmFtIHtib29sZWFufSBjaGVja2VkIFRoZSBgY2hlY2tlZGAgdmFsdWUgb2YgdGhlIHN3aXRjaFxuICAgKi9cbiAgb25DaGFuZ2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICB0YWJJbmRleDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlciwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLFxuXG4gIC8qKlxuICAgKiBUaGUgdmFsdWUgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIHZhbHVlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIFN3aXRjaChwcm9wcykge1xuICB2YXIgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWUgPSBwcm9wcy5jbGFzc05hbWUsXG4gICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKHByb3BzLCBbJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJ10pO1xuXG4gIHZhciBpY29uID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nLCB7IGNsYXNzTmFtZTogY2xhc3Nlcy5pY29uIH0pO1xuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAnc3BhbicsXG4gICAgeyBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWUpIH0sXG4gICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX1N3aXRjaEJhc2UyLmRlZmF1bHQsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgaWNvbjogaWNvbixcbiAgICAgIGNsYXNzZXM6IHtcbiAgICAgICAgZGVmYXVsdDogY2xhc3Nlcy5kZWZhdWx0LFxuICAgICAgICBjaGVja2VkOiBjbGFzc2VzLmNoZWNrZWQsXG4gICAgICAgIGRpc2FibGVkOiBjbGFzc2VzLmRpc2FibGVkXG4gICAgICB9LFxuICAgICAgY2hlY2tlZEljb246IGljb25cbiAgICB9LCBvdGhlcikpLFxuICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdzcGFuJywgeyBjbGFzc05hbWU6IGNsYXNzZXMuYmFyIH0pXG4gICk7XG59XG5cblN3aXRjaC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoX3JlZiA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuICBjaGVja2VkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLFxuICBjaGVja2VkSWNvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdkZWZhdWx0Q2hlY2tlZCcsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2Rpc2FibGVkJywgcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnZGlzYWJsZVJpcHBsZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2ljb24nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnaW5wdXRQcm9wcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnaW5wdXRSZWYnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICduYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdvbkNoYW5nZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ3RhYkluZGV4JywgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlciwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ3ZhbHVlJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpU3dpdGNoJyB9KShTd2l0Y2gpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N3aXRjaC9Td2l0Y2guanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N3aXRjaC9Td2l0Y2guanNcbi8vIG1vZHVsZSBjaHVua3MgPSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9Td2l0Y2ggPSByZXF1aXJlKCcuL1N3aXRjaCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Td2l0Y2gpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N3aXRjaC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3dpdGNoL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9JY29uQnV0dG9uID0gcmVxdWlyZSgnLi4vSWNvbkJ1dHRvbicpO1xuXG52YXIgX0ljb25CdXR0b24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbkJ1dHRvbik7XG5cbnZhciBfQ2hlY2tCb3hPdXRsaW5lQmxhbmsgPSByZXF1aXJlKCcuLi9zdmctaWNvbnMvQ2hlY2tCb3hPdXRsaW5lQmxhbmsnKTtcblxudmFyIF9DaGVja0JveE91dGxpbmVCbGFuazIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9DaGVja0JveE91dGxpbmVCbGFuayk7XG5cbnZhciBfQ2hlY2tCb3ggPSByZXF1aXJlKCcuLi9zdmctaWNvbnMvQ2hlY2tCb3gnKTtcblxudmFyIF9DaGVja0JveDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9DaGVja0JveCk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4uL0ljb24nKTtcblxudmFyIF9JY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBkaXNwbGF5OiAnaW5saW5lLWZsZXgnLFxuICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgIHRyYW5zaXRpb246ICdub25lJ1xuICB9LFxuICBpbnB1dDoge1xuICAgIGN1cnNvcjogJ2luaGVyaXQnLFxuICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgIG9wYWNpdHk6IDAsXG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICB0b3A6IDAsXG4gICAgbGVmdDogMCxcbiAgICBtYXJnaW46IDAsXG4gICAgcGFkZGluZzogMFxuICB9LFxuICBkZWZhdWx0OiB7fSxcbiAgY2hlY2tlZDoge30sXG4gIGRpc2FibGVkOiB7fVxufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgY29tcG9uZW50IGlzIGNoZWNrZWQuXG4gICAqL1xuICBjaGVja2VkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiB0byBkaXNwbGF5IHdoZW4gdGhlIGNvbXBvbmVudCBpcyBjaGVja2VkLlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGEgZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoZWNrZWRJY29uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBkZWZhdWx0Q2hlY2tlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHN3aXRjaCB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSByaXBwbGUgZWZmZWN0IHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlUmlwcGxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiB0byBkaXNwbGF5IHdoZW4gdGhlIGNvbXBvbmVudCBpcyB1bmNoZWNrZWQuXG4gICAqIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYSBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgaWNvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNvbXBvbmVudCBhcHBlYXJzIGluZGV0ZXJtaW5hdGUuXG4gICAqL1xuICBpbmRldGVybWluYXRlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCxcblxuICAvKipcbiAgICogVGhlIGljb24gdG8gZGlzcGxheSB3aGVuIHRoZSBjb21wb25lbnQgaXMgaW5kZXRlcm1pbmF0ZS5cbiAgICogSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBpbmRldGVybWluYXRlSWNvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBpbnB1dGAgZWxlbWVudC5cbiAgICovXG4gIGlucHV0UHJvcHM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIG5hdGl2ZSBpbnB1dCBjb21wb25lbnQuXG4gICAqL1xuICBpbnB1dFJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBpbnB1dCBjb21wb25lbnQgcHJvcGVydHkgYHR5cGVgLlxuICAgKi9cbiAgaW5wdXRUeXBlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWQsXG5cbiAgLypcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgbmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgc3RhdGUgaXMgY2hhbmdlZC5cbiAgICpcbiAgICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IFRoZSBldmVudCBzb3VyY2Ugb2YgdGhlIGNhbGxiYWNrXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gY2hlY2tlZCBUaGUgYGNoZWNrZWRgIHZhbHVlIG9mIHRoZSBzd2l0Y2hcbiAgICovXG4gIG9uQ2hhbmdlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgdGFiSW5kZXg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmddKSxcblxuICAvKipcbiAgICogVGhlIHZhbHVlIG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICB2YWx1ZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxuLy8gTkI6IElmIGNoYW5nZWQsIHBsZWFzZSB1cGRhdGUgQ2hlY2tib3gsIFN3aXRjaCBhbmQgUmFkaW9cbi8vIHNvIHRoYXQgdGhlIEFQSSBkb2N1bWVudGF0aW9uIGlzIHVwZGF0ZWQuXG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIFN3aXRjaEJhc2UgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShTd2l0Y2hCYXNlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBTd2l0Y2hCYXNlKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFN3aXRjaEJhc2UpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gU3dpdGNoQmFzZS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU3dpdGNoQmFzZSkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0ge30sIF90aGlzLmlucHV0ID0gbnVsbCwgX3RoaXMuYnV0dG9uID0gbnVsbCwgX3RoaXMuaXNDb250cm9sbGVkID0gbnVsbCwgX3RoaXMuaGFuZGxlSW5wdXRDaGFuZ2UgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciBjaGVja2VkID0gZXZlbnQudGFyZ2V0LmNoZWNrZWQ7XG5cbiAgICAgIGlmICghX3RoaXMuaXNDb250cm9sbGVkKSB7XG4gICAgICAgIF90aGlzLnNldFN0YXRlKHsgY2hlY2tlZDogY2hlY2tlZCB9KTtcbiAgICAgIH1cblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2hhbmdlKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uQ2hhbmdlKGV2ZW50LCBjaGVja2VkKTtcbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU3dpdGNoQmFzZSwgW3tcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgICB2YXIgcHJvcHMgPSB0aGlzLnByb3BzO1xuXG5cbiAgICAgIHRoaXMuaXNDb250cm9sbGVkID0gcHJvcHMuY2hlY2tlZCAhPT0gdW5kZWZpbmVkO1xuXG4gICAgICBpZiAoIXRoaXMuaXNDb250cm9sbGVkKSB7XG4gICAgICAgIC8vIG5vdCBjb250cm9sbGVkLCB1c2UgaW50ZXJuYWwgc3RhdGVcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgY2hlY2tlZDogcHJvcHMuZGVmYXVsdENoZWNrZWQgIT09IHVuZGVmaW5lZCA/IHByb3BzLmRlZmF1bHRDaGVja2VkIDogZmFsc2VcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzLFxuICAgICAgICAgIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoZWNrZWRQcm9wID0gX3Byb3BzLmNoZWNrZWQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNoZWNrZWRJY29uID0gX3Byb3BzLmNoZWNrZWRJY29uLFxuICAgICAgICAgIGRpc2FibGVkUHJvcCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICBpY29uUHJvcCA9IF9wcm9wcy5pY29uLFxuICAgICAgICAgIGlucHV0UHJvcHMgPSBfcHJvcHMuaW5wdXRQcm9wcyxcbiAgICAgICAgICBpbnB1dFJlZiA9IF9wcm9wcy5pbnB1dFJlZixcbiAgICAgICAgICBpbnB1dFR5cGUgPSBfcHJvcHMuaW5wdXRUeXBlLFxuICAgICAgICAgIG5hbWUgPSBfcHJvcHMubmFtZSxcbiAgICAgICAgICBvbkNoYW5nZSA9IF9wcm9wcy5vbkNoYW5nZSxcbiAgICAgICAgICB0YWJJbmRleCA9IF9wcm9wcy50YWJJbmRleCxcbiAgICAgICAgICB2YWx1ZSA9IF9wcm9wcy52YWx1ZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGVja2VkJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NoZWNrZWRJY29uJywgJ2Rpc2FibGVkJywgJ2ljb24nLCAnaW5wdXRQcm9wcycsICdpbnB1dFJlZicsICdpbnB1dFR5cGUnLCAnbmFtZScsICdvbkNoYW5nZScsICd0YWJJbmRleCcsICd2YWx1ZSddKTtcbiAgICAgIHZhciBtdWlGb3JtQ29udHJvbCA9IHRoaXMuY29udGV4dC5tdWlGb3JtQ29udHJvbDtcblxuICAgICAgdmFyIGRpc2FibGVkID0gZGlzYWJsZWRQcm9wO1xuXG4gICAgICBpZiAobXVpRm9ybUNvbnRyb2wpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBkaXNhYmxlZCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICBkaXNhYmxlZCA9IG11aUZvcm1Db250cm9sLmRpc2FibGVkO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIHZhciBjaGVja2VkID0gdGhpcy5pc0NvbnRyb2xsZWQgPyBjaGVja2VkUHJvcCA6IHRoaXMuc3RhdGUuY2hlY2tlZDtcbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3Nlcy5kZWZhdWx0LCBjbGFzc05hbWVQcm9wLCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuY2hlY2tlZCwgY2hlY2tlZCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmRpc2FibGVkLCBkaXNhYmxlZCksIF9jbGFzc05hbWVzKSk7XG5cbiAgICAgIHZhciBpY29uID0gY2hlY2tlZCA/IGNoZWNrZWRJY29uIDogaWNvblByb3A7XG5cbiAgICAgIGlmICh0eXBlb2YgaWNvbiA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgaWNvbiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgaWNvblxuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9JY29uQnV0dG9uMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjb21wb25lbnQ6ICdzcGFuJyxcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZWQsXG4gICAgICAgICAgdGFiSW5kZXg6IG51bGwsXG4gICAgICAgICAgcm9sZTogdW5kZWZpbmVkLFxuICAgICAgICAgIHJvb3RSZWY6IGZ1bmN0aW9uIHJvb3RSZWYobm9kZSkge1xuICAgICAgICAgICAgX3RoaXMyLmJ1dHRvbiA9IG5vZGU7XG4gICAgICAgICAgfVxuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIGljb24sXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdpbnB1dCcsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIHR5cGU6IGlucHV0VHlwZSxcbiAgICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICAgIGNoZWNrZWQ6IHRoaXMuaXNDb250cm9sbGVkID8gY2hlY2tlZFByb3AgOiB1bmRlZmluZWQsXG4gICAgICAgICAgb25DaGFuZ2U6IHRoaXMuaGFuZGxlSW5wdXRDaGFuZ2UsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc2VzLmlucHV0LFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZCxcbiAgICAgICAgICB0YWJJbmRleDogdGFiSW5kZXgsXG4gICAgICAgICAgdmFsdWU6IHZhbHVlXG4gICAgICAgIH0sIGlucHV0UHJvcHMsIHtcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICBfdGhpczIuaW5wdXQgPSBub2RlO1xuICAgICAgICAgICAgaWYgKGlucHV0UmVmKSB7XG4gICAgICAgICAgICAgIGlucHV0UmVmKG5vZGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfSkpXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gU3dpdGNoQmFzZTtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblN3aXRjaEJhc2UuZGVmYXVsdFByb3BzID0ge1xuICBjaGVja2VkSWNvbjogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0NoZWNrQm94Mi5kZWZhdWx0LCBudWxsKSwgLy8gZGVmYXVsdENoZWNrZWRJY29uXG4gIGRpc2FibGVSaXBwbGU6IGZhbHNlLFxuICBpY29uOiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfQ2hlY2tCb3hPdXRsaW5lQmxhbmsyLmRlZmF1bHQsIG51bGwpLCAvLyBkZWZhdWx0SWNvblxuICBpbnB1dFR5cGU6ICdjaGVja2JveCdcbn07XG5Td2l0Y2hCYXNlLmNvbnRleHRUeXBlcyA9IHtcbiAgbXVpRm9ybUNvbnRyb2w6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVN3aXRjaEJhc2UnIH0pKFN3aXRjaEJhc2UpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1N3aXRjaEJhc2UuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1N3aXRjaEJhc2UuanNcbi8vIG1vZHVsZSBjaHVua3MgPSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJy4uL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTkgM0g1Yy0xLjExIDAtMiAuOS0yIDJ2MTRjMCAxLjEuODkgMiAyIDJoMTRjMS4xMSAwIDItLjkgMi0yVjVjMC0xLjEtLjg5LTItMi0yem0tOSAxNGwtNS01IDEuNDEtMS40MUwxMCAxNC4xN2w3LjU5LTcuNTlMMTkgOGwtOSA5eicgfSk7XG5cbnZhciBDaGVja0JveCA9IGZ1bmN0aW9uIENoZWNrQm94KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuQ2hlY2tCb3ggPSAoMCwgX3B1cmUyLmRlZmF1bHQpKENoZWNrQm94KTtcbkNoZWNrQm94Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENoZWNrQm94O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9DaGVja0JveC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0NoZWNrQm94LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCcuLi9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE5IDV2MTRINVY1aDE0bTAtMkg1Yy0xLjEgMC0yIC45LTIgMnYxNGMwIDEuMS45IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yVjVjMC0xLjEtLjktMi0yLTJ6JyB9KTtcblxudmFyIENoZWNrQm94T3V0bGluZUJsYW5rID0gZnVuY3Rpb24gQ2hlY2tCb3hPdXRsaW5lQmxhbmsocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5DaGVja0JveE91dGxpbmVCbGFuayA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQ2hlY2tCb3hPdXRsaW5lQmxhbmspO1xuQ2hlY2tCb3hPdXRsaW5lQmxhbmsubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ2hlY2tCb3hPdXRsaW5lQmxhbms7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0NoZWNrQm94T3V0bGluZUJsYW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvQ2hlY2tCb3hPdXRsaW5lQmxhbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSA1NSIsIi8qKlxuICogVGhpcyBpbnB1dCBjb21wb25lbnQgaXMgbWFpbmx5IGRldmVsb3BlZCBmb3IgY3JlYXRlIHBvc3QsIGNyZWF0ZVxuICogY291cnNlIGNvbXBvbmVudC5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZGlzcGxheTogJ2ZsZXgnLFxuICB9LFxuICBpbnB1dDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gICAgYm9yZGVyOiAnMnB4IHNvbGlkICNkYWRhZGEnLFxuICAgIGJvcmRlclJhZGl1czogJzdweCcsXG4gICAgcGFkZGluZzogJzhweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbn07XG5cbmNvbnN0IElucHV0ID0gKHsgY2xhc3Nlcywgb25DaGFuZ2UsIHZhbHVlLCBwbGFjZWhvbGRlciwgZGlzYWJsZWQsIHR5cGUgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICA8aW5wdXRcbiAgICAgIHR5cGU9e3R5cGUgfHwgJ3RleHQnfVxuICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyIHx8ICdpbnB1dCd9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMuaW5wdXR9XG4gICAgICBkaXNhYmxlZD17ISFkaXNhYmxlZH1cbiAgICAvPlxuICA8L2Rpdj5cbik7XG5cbklucHV0LnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgdmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKElucHV0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgQ2FyZCBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IFN3aXRjaCBmcm9tICdtYXRlcmlhbC11aS9Td2l0Y2gnO1xuXG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgSW5wdXQgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9JbnB1dCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxMHB4JyxcbiAgfSxcbn0pO1xuXG5jbGFzcyBRdWVzdGlvbkNyZWF0ZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY3JlYXRlUXVlc3Rpb246IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHRpdGxlOiAnJyxcbiAgICAgIHRpdGxlTGVuZ3RoOiAwLFxuICAgICAgZGVzY3JpcHRpb246ICcnLFxuICAgICAgZGVzY3JpcHRpb25MZW5ndGg6IDAsXG5cbiAgICAgIHBsYWNlaG9sZGVyOiAnU3RhcnQgdHlwaW5nIGhlcmUuLi4nLFxuXG4gICAgICBvcHRpb25BOiAnJyxcbiAgICAgIG9wdGlvbkFMZW5ndGg6IDAsXG4gICAgICBvcHRpb25BQW5zd2VyOiBmYWxzZSxcbiAgICAgIG9wdGlvbkFEZXNjcmlwdGlvbjogJycsXG5cbiAgICAgIG9wdGlvbkI6ICcnLFxuICAgICAgb3B0aW9uQkxlbmd0aDogMCxcbiAgICAgIG9wdGlvbkJBbnN3ZXI6IGZhbHNlLFxuICAgICAgb3B0aW9uQkRlc2NyaXB0aW9uOiAnJyxcblxuICAgICAgb3B0aW9uQzogJycsXG4gICAgICBvcHRpb25DTGVuZ3RoOiAwLFxuICAgICAgb3B0aW9uQ0Fuc3dlcjogZmFsc2UsXG4gICAgICBvcHRpb25DRGVzY3JpcHRpb246ICcnLFxuXG4gICAgICBvcHRpb25EOiAnJyxcbiAgICAgIG9wdGlvbkRMZW5ndGg6IDAsXG4gICAgICBvcHRpb25EQW5zd2VyOiBmYWxzZSxcbiAgICAgIG9wdGlvbkREZXNjcmlwdGlvbjogJycsXG4gICAgfTtcbiAgfVxuXG4gIG9uQ2hhbmdlVG9nZ2xlID0gKG5hbWUpID0+IChlKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBbbmFtZV06IGUudGFyZ2V0LmNoZWNrZWQsXG4gICAgfSk7XG5cbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLm9wdGlvbkFBbnN3ZXIpO1xuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuc3RhdGUub3B0aW9uQkFuc3dlcik7XG4gICAgLy8gY29uc29sZS5sb2codGhpcy5zdGF0ZS5vcHRpb25DQW5zd2VyKTtcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLm9wdGlvbkRBbnN3ZXIpO1xuICB9O1xuXG4gIG9uQ2hhbmdlID0gKG5hbWUpID0+IChlKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBbbmFtZV06IGUudGFyZ2V0LnZhbHVlLFxuICAgIH0pO1xuICB9O1xuXG4gIG9uU3VibWl0ID0gKGUpID0+IHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICBjb25zdCB7XG4gICAgICB0aXRsZSxcbiAgICAgIGRlc2NyaXB0aW9uLFxuICAgICAgb3B0aW9uQSxcbiAgICAgIG9wdGlvbkFBbnN3ZXIsXG4gICAgICBvcHRpb25CLFxuICAgICAgb3B0aW9uQkFuc3dlcixcbiAgICAgIG9wdGlvbkMsXG4gICAgICBvcHRpb25DQW5zd2VyLFxuICAgICAgb3B0aW9uRCxcbiAgICAgIG9wdGlvbkRBbnN3ZXIsXG4gICAgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICB0aGlzLnByb3BzLmNyZWF0ZVF1ZXN0aW9uKHtcbiAgICAgIHRpdGxlLFxuICAgICAgZGVzY3JpcHRpb24sXG4gICAgICBvcHRpb25zOiBbXG4gICAgICAgIHsgdGl0bGU6IG9wdGlvbkEsIGFuc3dlcjogb3B0aW9uQUFuc3dlciB9LFxuICAgICAgICB7IHRpdGxlOiBvcHRpb25CLCBhbnN3ZXI6IG9wdGlvbkJBbnN3ZXIgfSxcbiAgICAgICAgeyB0aXRsZTogb3B0aW9uQywgYW5zd2VyOiBvcHRpb25DQW5zd2VyIH0sXG4gICAgICAgIHsgdGl0bGU6IG9wdGlvbkQsIGFuc3dlcjogb3B0aW9uREFuc3dlciB9LFxuICAgICAgXSxcbiAgICB9KTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgdGl0bGU6ICcnLFxuICAgICAgZGVzY3JpcHRpb246ICcnLFxuXG4gICAgICBvcHRpb25BOiAnJyxcbiAgICAgIG9wdGlvbkFBbnN3ZXI6IGZhbHNlLFxuXG4gICAgICBvcHRpb25COiAnJyxcbiAgICAgIG9wdGlvbkJBbnN3ZXI6IGZhbHNlLFxuXG4gICAgICBvcHRpb25DOiAnJyxcbiAgICAgIG9wdGlvbkNBbnN3ZXI6IGZhbHNlLFxuXG4gICAgICBvcHRpb25EOiAnJyxcbiAgICAgIG9wdGlvbkRBbnN3ZXI6IGZhbHNlLFxuICAgIH0pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7XG4gICAgICB0aXRsZSxcbiAgICAgIHBsYWNlaG9sZGVyLFxuICAgICAgZGVzY3JpcHRpb24sXG4gICAgICBvcHRpb25BLFxuICAgICAgb3B0aW9uQUFuc3dlcixcbiAgICAgIG9wdGlvbkIsXG4gICAgICBvcHRpb25CQW5zd2VyLFxuICAgICAgb3B0aW9uQyxcbiAgICAgIG9wdGlvbkNBbnN3ZXIsXG4gICAgICBvcHRpb25ELFxuICAgICAgb3B0aW9uREFuc3dlcixcbiAgICB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8Q2FyZCByYWlzZWQ+XG4gICAgICAgICAgPFR5cG9ncmFwaHk+UXVlc3Rpb246PC9UeXBvZ3JhcGh5PlxuICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgdmFsdWU9e3RpdGxlfVxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2UoJ3RpdGxlJyl9XG4gICAgICAgICAgLz5cblxuICAgICAgICAgIDxUeXBvZ3JhcGh5PkRlc2NyaXB0aW9uOjwvVHlwb2dyYXBoeT5cbiAgICAgICAgICA8SW5wdXRcbiAgICAgICAgICAgIHZhbHVlPXtkZXNjcmlwdGlvbn1cbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlKCdkZXNjcmlwdGlvbicpfVxuICAgICAgICAgIC8+XG5cbiAgICAgICAgICA8VHlwb2dyYXBoeT5PcHRpb25zPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgPGgzPkE8L2gzPlxuICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgdmFsdWU9e29wdGlvbkF9XG4gICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZSgnb3B0aW9uQScpfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPFN3aXRjaFxuICAgICAgICAgICAgY2hlY2tlZD17b3B0aW9uQUFuc3dlcn1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlVG9nZ2xlKCdvcHRpb25BQW5zd2VyJyl9XG4gICAgICAgICAgLz5cblxuICAgICAgICAgIDxoMz5CPC9oMz5cbiAgICAgICAgICA8SW5wdXRcbiAgICAgICAgICAgIHZhbHVlPXtvcHRpb25CfVxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2UoJ29wdGlvbkInKX1cbiAgICAgICAgICAvPlxuICAgICAgICAgIDxTd2l0Y2hcbiAgICAgICAgICAgIGNoZWNrZWQ9e29wdGlvbkJBbnN3ZXJ9XG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZVRvZ2dsZSgnb3B0aW9uQkFuc3dlcicpfVxuICAgICAgICAgIC8+XG5cbiAgICAgICAgICA8aDM+QzwvaDM+XG4gICAgICAgICAgPElucHV0XG4gICAgICAgICAgICB2YWx1ZT17b3B0aW9uQ31cbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlKCdvcHRpb25DJyl9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8U3dpdGNoXG4gICAgICAgICAgICBjaGVja2VkPXtvcHRpb25DQW5zd2VyfVxuICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2VUb2dnbGUoJ29wdGlvbkNBbnN3ZXInKX1cbiAgICAgICAgICAvPlxuXG4gICAgICAgICAgPGgzPkQ8L2gzPlxuICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgdmFsdWU9e29wdGlvbkR9XG4gICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZSgnb3B0aW9uRCcpfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPFN3aXRjaFxuICAgICAgICAgICAgY2hlY2tlZD17b3B0aW9uREFuc3dlcn1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlVG9nZ2xlKCdvcHRpb25EQW5zd2VyJyl9XG4gICAgICAgICAgLz5cblxuICAgICAgICAgIDxCdXR0b24gcmFpc2VkIG9uQ2xpY2s9e3RoaXMub25TdWJtaXR9PlxuICAgICAgICAgICAgQ3JlYXRlXG4gICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgIDwvQ2FyZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFF1ZXN0aW9uQ3JlYXRlKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL1Rlc3RZb3Vyc2VsZi9RdWVzdGlvbkNyZWF0ZS5qcyIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IExvYWRhYmxlIGZyb20gJ3JlYWN0LWxvYWRhYmxlJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5pbXBvcnQgTG9hZGluZyBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0xvYWRpbmcnO1xuaW1wb3J0IFF1ZXN0aW9uQ3JlYXRlIGZyb20gJy4vUXVlc3Rpb25DcmVhdGUnO1xuXG5jb25zdCBBc3luY1F1ZXN0aW9uID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9RdWVzdGlvbicpLFxuICBtb2R1bGVzOiBbJy4vUXVlc3Rpb24nXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxufSk7XG5cbmNsYXNzIFRlc3RZb3Vyc2VsZiBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBjb3Vyc2U6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBtYXRjaDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIGxvY2F0aW9uOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgaGlzdG9yeTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBxdWVzdGlvbnM6IFtcbiAgICAgICAge1xuICAgICAgICAgIHFJZDogMSxcbiAgICAgICAgICB0aXRsZTogJ1F1ZXN0aW9uIDEnLFxuICAgICAgICAgIGRlc2NyaXB0aW9uOiAnVGVzdCBEZXNjcmlwdGlvbicsXG4gICAgICAgICAgb3B0aW9uczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICB0aXRsZTogJ09wdGlvbiBBJyxcbiAgICAgICAgICAgICAgYW5zd2VyOiB0cnVlLFxuICAgICAgICAgICAgICBleHBsYWluYXRpb246ICdUZXN0IEV4cGxhaW5hdGlvbicsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICB0aXRsZTogJ09wdGlvbiBCJyxcbiAgICAgICAgICAgICAgYW5zd2VyOiB0cnVlLFxuICAgICAgICAgICAgICBleHBsYWluYXRpb246ICdUZXN0IEV4cGxhaW5hdGlvbicsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgeyB0aXRsZTogJ09wdGlvbiBDJywgZXhwbGFpbmF0aW9uOiAnVGVzdCBFeHBsYWluYXRpb24nIH0sXG4gICAgICAgICAgICB7IHRpdGxlOiAnT3B0aW9uIEQnLCBleHBsYWluYXRpb246ICdUZXN0IEV4cGxhaW5hdGlvbicgfSxcbiAgICAgICAgICBdLFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgcUlkOiAyLFxuICAgICAgICAgIHRpdGxlOiAnUXVlc3Rpb24gMicsXG4gICAgICAgICAgZGVzY3JpcHRpb246ICcyIFRlc3QgRGVzY3JpcHRpb24nLFxuICAgICAgICAgIG9wdGlvbnM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgdGl0bGU6ICdPcHRpb24gQScsXG4gICAgICAgICAgICAgIGFuc3dlcjogdHJ1ZSxcbiAgICAgICAgICAgICAgZXhwbGFpbmF0aW9uOiAnVGVzdCBFeHBsYWluYXRpb24nLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgdGl0bGU6ICdPcHRpb24gQicsXG4gICAgICAgICAgICAgIGFuc3dlcjogdHJ1ZSxcbiAgICAgICAgICAgICAgZXhwbGFpbmF0aW9uOiAnVGVzdCBFeHBsYWluYXRpb24nLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHsgdGl0bGU6ICdPcHRpb24gQycsIGV4cGxhaW5hdGlvbjogJ1Rlc3QgRXhwbGFpbmF0aW9uJyB9LFxuICAgICAgICAgICAgeyB0aXRsZTogJ09wdGlvbiBEJywgZXhwbGFpbmF0aW9uOiAnVGVzdCBFeHBsYWluYXRpb24nIH0sXG4gICAgICAgICAgXSxcbiAgICAgICAgfSxcbiAgICAgIF0sXG4gICAgfTtcbiAgfVxuXG4gIGNyZWF0ZVF1ZXN0aW9uID0gKHsgdGl0bGUsIGRlc2NyaXB0aW9uLCBvcHRpb25zIH0pID0+IHtcbiAgICBjb25zdCB7IHF1ZXN0aW9ucyB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgcXVlc3Rpb25zOiBbXG4gICAgICAgIC4uLnF1ZXN0aW9ucyxcbiAgICAgICAge1xuICAgICAgICAgIHFJZDogNiwgLy8gcXVlc3Rpb25zLmxlbmd0aCxcbiAgICAgICAgICB0aXRsZSxcbiAgICAgICAgICBkZXNjcmlwdGlvbixcbiAgICAgICAgICBvcHRpb25zLFxuICAgICAgICB9LFxuICAgICAgXSxcbiAgICB9KTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBxdWVzdGlvbnMgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e3RoaXMucHJvcHMuY2xhc3Nlcy5yb290fT5cbiAgICAgICAgPFF1ZXN0aW9uQ3JlYXRlXG4gICAgICAgICAgcXVlc3Rpb25zPXtxdWVzdGlvbnN9XG4gICAgICAgICAgY3JlYXRlUXVlc3Rpb249e3RoaXMuY3JlYXRlUXVlc3Rpb259XG4gICAgICAgIC8+XG5cbiAgICAgICAge3F1ZXN0aW9ucy5tYXAoKHEpID0+IDxBc3luY1F1ZXN0aW9uIGtleT17cS5xSWR9IHsuLi5xfSAvPil9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShUZXN0WW91cnNlbGYpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvVGVzdFlvdXJzZWxmL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==