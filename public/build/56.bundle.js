webpackJsonp([56],{

/***/ "./node_modules/lodash/_baseUniq.js":
/*!******************************************!*\
  !*** ./node_modules/lodash/_baseUniq.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var SetCache = __webpack_require__(/*! ./_SetCache */ "./node_modules/lodash/_SetCache.js"),
    arrayIncludes = __webpack_require__(/*! ./_arrayIncludes */ "./node_modules/lodash/_arrayIncludes.js"),
    arrayIncludesWith = __webpack_require__(/*! ./_arrayIncludesWith */ "./node_modules/lodash/_arrayIncludesWith.js"),
    cacheHas = __webpack_require__(/*! ./_cacheHas */ "./node_modules/lodash/_cacheHas.js"),
    createSet = __webpack_require__(/*! ./_createSet */ "./node_modules/lodash/_createSet.js"),
    setToArray = __webpack_require__(/*! ./_setToArray */ "./node_modules/lodash/_setToArray.js");

/** Used as the size to enable large array optimizations. */
var LARGE_ARRAY_SIZE = 200;

/**
 * The base implementation of `_.uniqBy` without support for iteratee shorthands.
 *
 * @private
 * @param {Array} array The array to inspect.
 * @param {Function} [iteratee] The iteratee invoked per element.
 * @param {Function} [comparator] The comparator invoked per element.
 * @returns {Array} Returns the new duplicate free array.
 */
function baseUniq(array, iteratee, comparator) {
  var index = -1,
      includes = arrayIncludes,
      length = array.length,
      isCommon = true,
      result = [],
      seen = result;

  if (comparator) {
    isCommon = false;
    includes = arrayIncludesWith;
  }
  else if (length >= LARGE_ARRAY_SIZE) {
    var set = iteratee ? null : createSet(array);
    if (set) {
      return setToArray(set);
    }
    isCommon = false;
    includes = cacheHas;
    seen = new SetCache;
  }
  else {
    seen = iteratee ? [] : result;
  }
  outer:
  while (++index < length) {
    var value = array[index],
        computed = iteratee ? iteratee(value) : value;

    value = (comparator || value !== 0) ? value : 0;
    if (isCommon && computed === computed) {
      var seenIndex = seen.length;
      while (seenIndex--) {
        if (seen[seenIndex] === computed) {
          continue outer;
        }
      }
      if (iteratee) {
        seen.push(computed);
      }
      result.push(value);
    }
    else if (!includes(seen, computed, comparator)) {
      if (seen !== result) {
        seen.push(computed);
      }
      result.push(value);
    }
  }
  return result;
}

module.exports = baseUniq;


/***/ }),

/***/ "./node_modules/lodash/_createSet.js":
/*!*******************************************!*\
  !*** ./node_modules/lodash/_createSet.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var Set = __webpack_require__(/*! ./_Set */ "./node_modules/lodash/_Set.js"),
    noop = __webpack_require__(/*! ./noop */ "./node_modules/lodash/noop.js"),
    setToArray = __webpack_require__(/*! ./_setToArray */ "./node_modules/lodash/_setToArray.js");

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/**
 * Creates a set object of `values`.
 *
 * @private
 * @param {Array} values The values to add to the set.
 * @returns {Object} Returns the new set.
 */
var createSet = !(Set && (1 / setToArray(new Set([,-0]))[1]) == INFINITY) ? noop : function(values) {
  return new Set(values);
};

module.exports = createSet;


/***/ }),

/***/ "./node_modules/lodash/noop.js":
/*!*************************************!*\
  !*** ./node_modules/lodash/noop.js ***!
  \*************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * This method returns `undefined`.
 *
 * @static
 * @memberOf _
 * @since 2.3.0
 * @category Util
 * @example
 *
 * _.times(2, _.noop);
 * // => [undefined, undefined]
 */
function noop() {
  // No operation performed.
}

module.exports = noop;


/***/ }),

/***/ "./node_modules/lodash/uniqBy.js":
/*!***************************************!*\
  !*** ./node_modules/lodash/uniqBy.js ***!
  \***************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var baseIteratee = __webpack_require__(/*! ./_baseIteratee */ "./node_modules/lodash/_baseIteratee.js"),
    baseUniq = __webpack_require__(/*! ./_baseUniq */ "./node_modules/lodash/_baseUniq.js");

/**
 * This method is like `_.uniq` except that it accepts `iteratee` which is
 * invoked for each element in `array` to generate the criterion by which
 * uniqueness is computed. The order of result values is determined by the
 * order they occur in the array. The iteratee is invoked with one argument:
 * (value).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Array
 * @param {Array} array The array to inspect.
 * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
 * @returns {Array} Returns the new duplicate free array.
 * @example
 *
 * _.uniqBy([2.1, 1.2, 2.3], Math.floor);
 * // => [2.1, 1.2]
 *
 * // The `_.property` iteratee shorthand.
 * _.uniqBy([{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }], 'x');
 * // => [{ 'x': 1 }, { 'x': 2 }]
 */
function uniqBy(array, iteratee) {
  return (array && array.length) ? baseUniq(array, baseIteratee(iteratee, 2)) : [];
}

module.exports = uniqBy;


/***/ }),

/***/ "./src/client/actions/elements/getAll.js":
/*!***********************************************!*\
  !*** ./src/client/actions/elements/getAll.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

/**
 *
 * @function
 * @param {Object[]} payload -
 * @param {Object} payload[] -
 * @param {number} payload[].id -
 * @param {string} payload[].username -
 * @param {string} payload[].elementType - 'user' or 'circle'
 * @param {string} payload[].circleType - if elementType is 'circle' then
 * 'edu', 'org', 'field', 'location', 'social' else this key not exists.
 * @param {string} payload[].name -
 * @param {string} payload[].avatar -
 * @returns {Object} - redux action type.
 */
var getAll = function getAll(payload) {
  return { type: _elements.ELEMENTS_GET, payload: payload };
}; /** @format */

exports.default = getAll;

/***/ }),

/***/ "./src/client/actions/posts/comments/getAll.js":
/*!*****************************************************!*\
  !*** ./src/client/actions/posts/comments/getAll.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _comments = __webpack_require__(/*! ../../../constants/posts/comments */ "./src/client/constants/posts/comments.js");

/**
 *
 * @function getAll
 * @param {object} payload
 */
var getAll = function getAll(payload) {
  return { type: _comments.POST_COMMENTS_GET, payload: payload };
}; /** @format */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/elements/getAll.js":
/*!*******************************************!*\
  !*** ./src/client/api/elements/getAll.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function getElements
 * @param {Object} payload
 * @param {number} payload.token
 * @param {Object[]} payload.ids
 * @param {number} payload.ids[].id -
 * @returns {Promise}
 *
 * @example
 */
/** @format */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        ids = _ref2.ids;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements?ids=' + (0, _stringify2.default)(ids);
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/posts/comments/getAll.js":
/*!*************************************************!*\
  !*** ./src/client/api/posts/comments/getAll.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        postId = _ref2.postId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/posts/' + postId + '/comments';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/containers/Comment/index.js":
/*!************************************************!*\
  !*** ./src/client/containers/Comment/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _uniqBy = __webpack_require__(/*! lodash/uniqBy */ "./node_modules/lodash/uniqBy.js");

var _uniqBy2 = _interopRequireDefault(_uniqBy);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _styles = __webpack_require__(/*! ./styles */ "./src/client/containers/Comment/styles.js");

var _styles2 = _interopRequireDefault(_styles);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../api/posts/comments/getAll */ "./src/client/api/posts/comments/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../api/elements/getAll */ "./src/client/api/elements/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

var _getAll5 = __webpack_require__(/*! ../../actions/posts/comments/getAll */ "./src/client/actions/posts/comments/getAll.js");

var _getAll6 = _interopRequireDefault(_getAll5);

var _getAll7 = __webpack_require__(/*! ../../actions/elements/getAll */ "./src/client/actions/elements/getAll.js");

var _getAll8 = _interopRequireDefault(_getAll7);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component will display for creating comment and all the comments
 * @format
 */

var AsyncCommentCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(66).then(__webpack_require__.bind(null, /*! ./Create */ "./src/client/containers/Comment/Create.js"));
  },
  modules: ['./Create'],
  loading: _Loading2.default
});

var AsyncCommentTimeline = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(32).then(__webpack_require__.bind(null, /*! ./Timeline */ "./src/client/containers/Comment/Timeline.js"));
  },
  modules: ['./Timeline'],
  loading: _Loading2.default
});

var Comment = function (_Component) {
  (0, _inherits3.default)(Comment, _Component);

  function Comment(props) {
    (0, _classCallCheck3.default)(this, Comment);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Comment.__proto__ || (0, _getPrototypeOf2.default)(Comment)).call(this, props));

    _this.state = {
      message: ''
    };
    return _this;
  }

  (0, _createClass3.default)(Comment, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, user, postId, posts, token, _ref2, statusCode, payload, elements, res;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, user = _props.user, postId = _props.postId, posts = _props.posts;
                token = user.token;

                if (!(posts[postId].comments !== undefined)) {
                  _context.next = 5;
                  break;
                }

                return _context.abrupt('return');

              case 5:
                _context.next = 7;
                return (0, _getAll2.default)({
                  token: token,
                  postId: postId
                });

              case 7:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 12;
                  break;
                }

                return _context.abrupt('return');

              case 12:
                elements = (0, _uniqBy2.default)(payload.map(function (p) {
                  return p.authorId;
                }));
                _context.next = 15;
                return (0, _getAll4.default)({ token: token, ids: elements });

              case 15:
                res = _context.sent;


                /*eslint-disable*/
                if (res.statusCode >= 300) {
                  this.setState({
                    message: 'There is any internal problem while loading comments. Please refresh the page'
                  });
                }
                /* eslint-enable */

                this.props.actionGetAllComment({ postId: postId, payload: payload });

                this.props.actionGetAllElements({ elements: res.payload });
                _context.next = 24;
                break;

              case 21:
                _context.prev = 21;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 24:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 21]]);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          postId = _props2.postId,
          posts = _props2.posts;
      var comments = posts[postId].comments;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
          _react2.default.createElement(AsyncCommentCreate, { postId: postId }),
          comments !== undefined ? (0, _keys2.default)(comments).map(function (commentId) {
            return _react2.default.createElement(AsyncCommentTimeline, {
              key: commentId,
              postId: postId,
              comment: comments[commentId]
            });
          }) : null
        )
      );
    }
  }]);
  return Comment;
}(_react.Component);

Comment.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,
  postId: _ReactPropTypes2.default.number.isRequired,
  user: _ReactPropTypes2.default.object.isRequired,
  posts: _ReactPropTypes2.default.object.isRequired,
  actionGetAllComment: _ReactPropTypes2.default.func.isRequired,
  actionGetAllElements: _ReactPropTypes2.default.func.isRequired
};


var mapStateToProps = function mapStateToProps(_ref3) {
  var posts = _ref3.posts,
      elements = _ref3.elements;
  return {
    posts: posts,
    user: elements.user
  };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionGetAllComment: _getAll6.default,
    actionGetAllElements: _getAll8.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(_styles2.default)(Comment));

/***/ }),

/***/ "./src/client/containers/Comment/styles.js":
/*!*************************************************!*\
  !*** ./src/client/containers/Comment/styles.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var styles = {
  root: {},
  name: {
    cursor: 'pointer',
    textDecoration: 'none'
  },
  avatar: {
    borderRadius: '50%'
  },
  flexGrow: {
    flex: '1 1 auto'
  },
  button: {
    marginTop: '30px'
  }
};

exports.default = styles;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlVW5pcS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbG9kYXNoL19jcmVhdGVTZXQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2xvZGFzaC9ub29wLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9sb2Rhc2gvdW5pcUJ5LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9lbGVtZW50cy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL3Bvc3RzL2NvbW1lbnRzL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvcG9zdHMvY29tbWVudHMvZ2V0QWxsLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db21tZW50L2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db21tZW50L3N0eWxlcy5qcyJdLCJuYW1lcyI6WyJnZXRBbGwiLCJwYXlsb2FkIiwidHlwZSIsInRva2VuIiwiaWRzIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJwb3N0SWQiLCJBc3luY0NvbW1lbnRDcmVhdGUiLCJsb2FkZXIiLCJtb2R1bGVzIiwibG9hZGluZyIsIkFzeW5jQ29tbWVudFRpbWVsaW5lIiwiQ29tbWVudCIsInByb3BzIiwic3RhdGUiLCJtZXNzYWdlIiwidXNlciIsInBvc3RzIiwiY29tbWVudHMiLCJ1bmRlZmluZWQiLCJlbGVtZW50cyIsIm1hcCIsInAiLCJhdXRob3JJZCIsInNldFN0YXRlIiwiYWN0aW9uR2V0QWxsQ29tbWVudCIsImFjdGlvbkdldEFsbEVsZW1lbnRzIiwiY2xhc3NlcyIsInJvb3QiLCJjb21tZW50SWQiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwibnVtYmVyIiwiZnVuYyIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwic3R5bGVzIiwibmFtZSIsImN1cnNvciIsInRleHREZWNvcmF0aW9uIiwiYXZhdGFyIiwiYm9yZGVyUmFkaXVzIiwiZmxleEdyb3ciLCJmbGV4IiwiYnV0dG9uIiwibWFyZ2luVG9wIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLE1BQU07QUFDakIsV0FBVyxTQUFTO0FBQ3BCLFdBQVcsU0FBUztBQUNwQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOzs7Ozs7Ozs7Ozs7O0FDdkVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxNQUFNO0FBQ2pCLGFBQWEsT0FBTztBQUNwQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUNoQkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxNQUFNO0FBQ2pCLFdBQVcsU0FBUztBQUNwQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYyxTQUFTLEdBQUcsU0FBUyxHQUFHLFNBQVM7QUFDL0MsV0FBVyxTQUFTLEdBQUcsU0FBUztBQUNoQztBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1QkE7O0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FBY0EsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUFjLEVBQUVDLDRCQUFGLEVBQXNCRCxnQkFBdEIsRUFBZDtBQUFBLENBQWYsQyxDQWxCQTs7a0JBb0JlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJmOztBQUVBOzs7OztBQUtBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBYyxFQUFFQyxpQ0FBRixFQUEyQkQsZ0JBQTNCLEVBQWQ7QUFBQSxDQUFmLEMsQ0FUQTs7a0JBV2VELE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTmY7Ozs7Ozs7Ozs7OztBQUxBOzs7c0ZBaUJBO0FBQUEsUUFBd0JHLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxHQUEvQixTQUErQkEsR0FBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVix5Q0FFNEMseUJBQWVELEdBQWYsQ0FGNUM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUMsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVOO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBYVlDLGtCQWJaLEdBYW1DRCxHQWJuQyxDQWFZQyxNQWJaLEVBYW9CQyxVQWJwQixHQWFtQ0YsR0FibkMsQ0Fhb0JFLFVBYnBCOztBQUFBLGtCQWVRRCxVQUFVLEdBZmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWhCYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0J1QkYsSUFBSUssSUFBSixFQXRCdkI7O0FBQUE7QUFzQlVBLGdCQXRCVjtBQUFBLHdFQXdCZ0JBLElBeEJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMEJJQyxvQkFBUUYsS0FBUjs7QUExQkosNkNBNEJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWQsTTs7Ozs7QUFmZjs7OztBQUNBOzs7O2tCQWlEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVDZjs7Ozs7Ozs7Ozs7QUFSQTs7Ozs7O3NGQW1CQTtBQUFBLFFBQXdCRyxLQUF4QixTQUF3QkEsS0FBeEI7QUFBQSxRQUErQmMsTUFBL0IsU0FBK0JBLE1BQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVaLGVBRlYsa0NBRXFDWSxNQUZyQztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNWixHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZU47QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJSyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRRixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlZCxNOzs7OztBQWRmOzs7O0FBQ0E7Ozs7a0JBZ0RlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7O0FBQ0E7O0FBRUE7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUF6QkE7Ozs7O0FBMkJBLElBQU1rQixxQkFBcUIsNkJBQVM7QUFDbENDLFVBQVE7QUFBQSxXQUFNLHlJQUFOO0FBQUEsR0FEMEI7QUFFbENDLFdBQVMsQ0FBQyxVQUFELENBRnlCO0FBR2xDQztBQUhrQyxDQUFULENBQTNCOztBQU1BLElBQU1DLHVCQUF1Qiw2QkFBUztBQUNwQ0gsVUFBUTtBQUFBLFdBQU0sNklBQU47QUFBQSxHQUQ0QjtBQUVwQ0MsV0FBUyxDQUFDLFlBQUQsQ0FGMkI7QUFHcENDO0FBSG9DLENBQVQsQ0FBN0I7O0lBTU1FLE87OztBQVVKLG1CQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsd0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYTtBQUNYQyxlQUFTO0FBREUsS0FBYjtBQUZpQjtBQUtsQjs7Ozs7Ozs7Ozs7Ozt5QkFJbUMsS0FBS0YsSyxFQUE3QkcsSSxVQUFBQSxJLEVBQU1WLE0sVUFBQUEsTSxFQUFRVyxLLFVBQUFBLEs7QUFDZHpCLHFCLEdBQVV3QixJLENBQVZ4QixLOztzQkFFSnlCLE1BQU1YLE1BQU4sRUFBY1ksUUFBZCxLQUEyQkMsUzs7Ozs7Ozs7O3VCQUlPLHNCQUFpQjtBQUNyRDNCLDhCQURxRDtBQUVyRGM7QUFGcUQsaUJBQWpCLEM7Ozs7QUFBOUJKLDBCLFNBQUFBLFU7QUFBWVosdUIsU0FBQUEsTzs7c0JBS2hCWSxjQUFjLEc7Ozs7Ozs7O0FBSVprQix3QixHQUFXLHNCQUFPOUIsUUFBUStCLEdBQVIsQ0FBWSxVQUFDQyxDQUFEO0FBQUEseUJBQU9BLEVBQUVDLFFBQVQ7QUFBQSxpQkFBWixDQUFQLEM7O3VCQUVDLHNCQUFpQixFQUFFL0IsWUFBRixFQUFTQyxLQUFLMkIsUUFBZCxFQUFqQixDOzs7QUFBWnJCLG1COzs7QUFFTjtBQUNBLG9CQUFJQSxJQUFJRyxVQUFKLElBQWtCLEdBQXRCLEVBQTJCO0FBQ3pCLHVCQUFLc0IsUUFBTCxDQUFjO0FBQ1pULDZCQUNFO0FBRlUsbUJBQWQ7QUFJRDtBQUNEOztBQUVBLHFCQUFLRixLQUFMLENBQVdZLG1CQUFYLENBQStCLEVBQUVuQixjQUFGLEVBQVVoQixnQkFBVixFQUEvQjs7QUFFQSxxQkFBS3VCLEtBQUwsQ0FBV2Esb0JBQVgsQ0FBZ0MsRUFBRU4sVUFBVXJCLElBQUlULE9BQWhCLEVBQWhDOzs7Ozs7OztBQUVBZSx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQUlLO0FBQUEsb0JBQzRCLEtBQUtVLEtBRGpDO0FBQUEsVUFDQ2MsT0FERCxXQUNDQSxPQUREO0FBQUEsVUFDVXJCLE1BRFYsV0FDVUEsTUFEVjtBQUFBLFVBQ2tCVyxLQURsQixXQUNrQkEsS0FEbEI7QUFBQSxVQUdDQyxRQUhELEdBR2NELE1BQU1YLE1BQU4sQ0FIZCxDQUdDWSxRQUhEOzs7QUFLUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDLEVBQTZDLFdBQVdTLFFBQVFDLElBQWhFO0FBQ0U7QUFBQTtBQUFBLFlBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLEVBQXZCLEVBQTJCLElBQUksRUFBL0IsRUFBbUMsSUFBSSxFQUF2QyxFQUEyQyxJQUFJLEVBQS9DO0FBQ0Usd0NBQUMsa0JBQUQsSUFBb0IsUUFBUXRCLE1BQTVCLEdBREY7QUFHR1ksdUJBQWFDLFNBQWIsR0FDRyxvQkFBWUQsUUFBWixFQUFzQkcsR0FBdEIsQ0FBMEIsVUFBQ1EsU0FBRDtBQUFBLG1CQUMxQiw4QkFBQyxvQkFBRDtBQUNFLG1CQUFLQSxTQURQO0FBRUUsc0JBQVF2QixNQUZWO0FBR0UsdUJBQVNZLFNBQVNXLFNBQVQ7QUFIWCxjQUQwQjtBQUFBLFdBQTFCLENBREgsR0FRRztBQVhOO0FBREYsT0FERjtBQWlCRDs7Ozs7QUE5RUdqQixPLENBQ0drQixTLEdBQVk7QUFDakJILFdBQVMseUJBQVVJLE1BQVYsQ0FBaUJDLFVBRFQ7QUFFakIxQixVQUFRLHlCQUFVMkIsTUFBVixDQUFpQkQsVUFGUjtBQUdqQmhCLFFBQU0seUJBQVVlLE1BQVYsQ0FBaUJDLFVBSE47QUFJakJmLFNBQU8seUJBQVVjLE1BQVYsQ0FBaUJDLFVBSlA7QUFLakJQLHVCQUFxQix5QkFBVVMsSUFBVixDQUFlRixVQUxuQjtBQU1qQk4sd0JBQXNCLHlCQUFVUSxJQUFWLENBQWVGO0FBTnBCLEM7OztBQWdGckIsSUFBTUcsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdsQixLQUFILFNBQUdBLEtBQUg7QUFBQSxNQUFVRyxRQUFWLFNBQVVBLFFBQVY7QUFBQSxTQUEwQjtBQUNoREgsZ0JBRGdEO0FBRWhERCxVQUFNSSxTQUFTSjtBQUZpQyxHQUExQjtBQUFBLENBQXhCOztBQUtBLElBQU1vQixxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRVoseUNBREY7QUFFRUM7QUFGRixHQURGLEVBS0VXLFFBTEYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBU2UseUJBQVFGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDRDQUFtQnhCLE9BQW5CLENBRGEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdElmLElBQU0wQixTQUFTO0FBQ2JWLFFBQU0sRUFETztBQUViVyxRQUFNO0FBQ0pDLFlBQVEsU0FESjtBQUVKQyxvQkFBZ0I7QUFGWixHQUZPO0FBTWJDLFVBQVE7QUFDTkMsa0JBQWM7QUFEUixHQU5LO0FBU2JDLFlBQVU7QUFDUkMsVUFBTTtBQURFLEdBVEc7QUFZYkMsVUFBUTtBQUNOQyxlQUFXO0FBREw7QUFaSyxDQUFmOztrQkFpQmVULE0iLCJmaWxlIjoiNTYuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIFNldENhY2hlID0gcmVxdWlyZSgnLi9fU2V0Q2FjaGUnKSxcbiAgICBhcnJheUluY2x1ZGVzID0gcmVxdWlyZSgnLi9fYXJyYXlJbmNsdWRlcycpLFxuICAgIGFycmF5SW5jbHVkZXNXaXRoID0gcmVxdWlyZSgnLi9fYXJyYXlJbmNsdWRlc1dpdGgnKSxcbiAgICBjYWNoZUhhcyA9IHJlcXVpcmUoJy4vX2NhY2hlSGFzJyksXG4gICAgY3JlYXRlU2V0ID0gcmVxdWlyZSgnLi9fY3JlYXRlU2V0JyksXG4gICAgc2V0VG9BcnJheSA9IHJlcXVpcmUoJy4vX3NldFRvQXJyYXknKTtcblxuLyoqIFVzZWQgYXMgdGhlIHNpemUgdG8gZW5hYmxlIGxhcmdlIGFycmF5IG9wdGltaXphdGlvbnMuICovXG52YXIgTEFSR0VfQVJSQVlfU0laRSA9IDIwMDtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBgXy51bmlxQnlgIHdpdGhvdXQgc3VwcG9ydCBmb3IgaXRlcmF0ZWUgc2hvcnRoYW5kcy5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gYXJyYXkgVGhlIGFycmF5IHRvIGluc3BlY3QuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBbaXRlcmF0ZWVdIFRoZSBpdGVyYXRlZSBpbnZva2VkIHBlciBlbGVtZW50LlxuICogQHBhcmFtIHtGdW5jdGlvbn0gW2NvbXBhcmF0b3JdIFRoZSBjb21wYXJhdG9yIGludm9rZWQgcGVyIGVsZW1lbnQuXG4gKiBAcmV0dXJucyB7QXJyYXl9IFJldHVybnMgdGhlIG5ldyBkdXBsaWNhdGUgZnJlZSBhcnJheS5cbiAqL1xuZnVuY3Rpb24gYmFzZVVuaXEoYXJyYXksIGl0ZXJhdGVlLCBjb21wYXJhdG9yKSB7XG4gIHZhciBpbmRleCA9IC0xLFxuICAgICAgaW5jbHVkZXMgPSBhcnJheUluY2x1ZGVzLFxuICAgICAgbGVuZ3RoID0gYXJyYXkubGVuZ3RoLFxuICAgICAgaXNDb21tb24gPSB0cnVlLFxuICAgICAgcmVzdWx0ID0gW10sXG4gICAgICBzZWVuID0gcmVzdWx0O1xuXG4gIGlmIChjb21wYXJhdG9yKSB7XG4gICAgaXNDb21tb24gPSBmYWxzZTtcbiAgICBpbmNsdWRlcyA9IGFycmF5SW5jbHVkZXNXaXRoO1xuICB9XG4gIGVsc2UgaWYgKGxlbmd0aCA+PSBMQVJHRV9BUlJBWV9TSVpFKSB7XG4gICAgdmFyIHNldCA9IGl0ZXJhdGVlID8gbnVsbCA6IGNyZWF0ZVNldChhcnJheSk7XG4gICAgaWYgKHNldCkge1xuICAgICAgcmV0dXJuIHNldFRvQXJyYXkoc2V0KTtcbiAgICB9XG4gICAgaXNDb21tb24gPSBmYWxzZTtcbiAgICBpbmNsdWRlcyA9IGNhY2hlSGFzO1xuICAgIHNlZW4gPSBuZXcgU2V0Q2FjaGU7XG4gIH1cbiAgZWxzZSB7XG4gICAgc2VlbiA9IGl0ZXJhdGVlID8gW10gOiByZXN1bHQ7XG4gIH1cbiAgb3V0ZXI6XG4gIHdoaWxlICgrK2luZGV4IDwgbGVuZ3RoKSB7XG4gICAgdmFyIHZhbHVlID0gYXJyYXlbaW5kZXhdLFxuICAgICAgICBjb21wdXRlZCA9IGl0ZXJhdGVlID8gaXRlcmF0ZWUodmFsdWUpIDogdmFsdWU7XG5cbiAgICB2YWx1ZSA9IChjb21wYXJhdG9yIHx8IHZhbHVlICE9PSAwKSA/IHZhbHVlIDogMDtcbiAgICBpZiAoaXNDb21tb24gJiYgY29tcHV0ZWQgPT09IGNvbXB1dGVkKSB7XG4gICAgICB2YXIgc2VlbkluZGV4ID0gc2Vlbi5sZW5ndGg7XG4gICAgICB3aGlsZSAoc2VlbkluZGV4LS0pIHtcbiAgICAgICAgaWYgKHNlZW5bc2VlbkluZGV4XSA9PT0gY29tcHV0ZWQpIHtcbiAgICAgICAgICBjb250aW51ZSBvdXRlcjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgaWYgKGl0ZXJhdGVlKSB7XG4gICAgICAgIHNlZW4ucHVzaChjb21wdXRlZCk7XG4gICAgICB9XG4gICAgICByZXN1bHQucHVzaCh2YWx1ZSk7XG4gICAgfVxuICAgIGVsc2UgaWYgKCFpbmNsdWRlcyhzZWVuLCBjb21wdXRlZCwgY29tcGFyYXRvcikpIHtcbiAgICAgIGlmIChzZWVuICE9PSByZXN1bHQpIHtcbiAgICAgICAgc2Vlbi5wdXNoKGNvbXB1dGVkKTtcbiAgICAgIH1cbiAgICAgIHJlc3VsdC5wdXNoKHZhbHVlKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBiYXNlVW5pcTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZVVuaXEuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZVVuaXEuanNcbi8vIG1vZHVsZSBjaHVua3MgPSA1NiIsInZhciBTZXQgPSByZXF1aXJlKCcuL19TZXQnKSxcbiAgICBub29wID0gcmVxdWlyZSgnLi9ub29wJyksXG4gICAgc2V0VG9BcnJheSA9IHJlcXVpcmUoJy4vX3NldFRvQXJyYXknKTtcblxuLyoqIFVzZWQgYXMgcmVmZXJlbmNlcyBmb3IgdmFyaW91cyBgTnVtYmVyYCBjb25zdGFudHMuICovXG52YXIgSU5GSU5JVFkgPSAxIC8gMDtcblxuLyoqXG4gKiBDcmVhdGVzIGEgc2V0IG9iamVjdCBvZiBgdmFsdWVzYC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHtBcnJheX0gdmFsdWVzIFRoZSB2YWx1ZXMgdG8gYWRkIHRvIHRoZSBzZXQuXG4gKiBAcmV0dXJucyB7T2JqZWN0fSBSZXR1cm5zIHRoZSBuZXcgc2V0LlxuICovXG52YXIgY3JlYXRlU2V0ID0gIShTZXQgJiYgKDEgLyBzZXRUb0FycmF5KG5ldyBTZXQoWywtMF0pKVsxXSkgPT0gSU5GSU5JVFkpID8gbm9vcCA6IGZ1bmN0aW9uKHZhbHVlcykge1xuICByZXR1cm4gbmV3IFNldCh2YWx1ZXMpO1xufTtcblxubW9kdWxlLmV4cG9ydHMgPSBjcmVhdGVTZXQ7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2NyZWF0ZVNldC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbG9kYXNoL19jcmVhdGVTZXQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSA1NiIsIi8qKlxuICogVGhpcyBtZXRob2QgcmV0dXJucyBgdW5kZWZpbmVkYC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDIuMy4wXG4gKiBAY2F0ZWdvcnkgVXRpbFxuICogQGV4YW1wbGVcbiAqXG4gKiBfLnRpbWVzKDIsIF8ubm9vcCk7XG4gKiAvLyA9PiBbdW5kZWZpbmVkLCB1bmRlZmluZWRdXG4gKi9cbmZ1bmN0aW9uIG5vb3AoKSB7XG4gIC8vIE5vIG9wZXJhdGlvbiBwZXJmb3JtZWQuXG59XG5cbm1vZHVsZS5leHBvcnRzID0gbm9vcDtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9ub29wLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9sb2Rhc2gvbm9vcC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDU2IiwidmFyIGJhc2VJdGVyYXRlZSA9IHJlcXVpcmUoJy4vX2Jhc2VJdGVyYXRlZScpLFxuICAgIGJhc2VVbmlxID0gcmVxdWlyZSgnLi9fYmFzZVVuaXEnKTtcblxuLyoqXG4gKiBUaGlzIG1ldGhvZCBpcyBsaWtlIGBfLnVuaXFgIGV4Y2VwdCB0aGF0IGl0IGFjY2VwdHMgYGl0ZXJhdGVlYCB3aGljaCBpc1xuICogaW52b2tlZCBmb3IgZWFjaCBlbGVtZW50IGluIGBhcnJheWAgdG8gZ2VuZXJhdGUgdGhlIGNyaXRlcmlvbiBieSB3aGljaFxuICogdW5pcXVlbmVzcyBpcyBjb21wdXRlZC4gVGhlIG9yZGVyIG9mIHJlc3VsdCB2YWx1ZXMgaXMgZGV0ZXJtaW5lZCBieSB0aGVcbiAqIG9yZGVyIHRoZXkgb2NjdXIgaW4gdGhlIGFycmF5LiBUaGUgaXRlcmF0ZWUgaXMgaW52b2tlZCB3aXRoIG9uZSBhcmd1bWVudDpcbiAqICh2YWx1ZSkuXG4gKlxuICogQHN0YXRpY1xuICogQG1lbWJlck9mIF9cbiAqIEBzaW5jZSA0LjAuMFxuICogQGNhdGVnb3J5IEFycmF5XG4gKiBAcGFyYW0ge0FycmF5fSBhcnJheSBUaGUgYXJyYXkgdG8gaW5zcGVjdC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtpdGVyYXRlZT1fLmlkZW50aXR5XSBUaGUgaXRlcmF0ZWUgaW52b2tlZCBwZXIgZWxlbWVudC5cbiAqIEByZXR1cm5zIHtBcnJheX0gUmV0dXJucyB0aGUgbmV3IGR1cGxpY2F0ZSBmcmVlIGFycmF5LlxuICogQGV4YW1wbGVcbiAqXG4gKiBfLnVuaXFCeShbMi4xLCAxLjIsIDIuM10sIE1hdGguZmxvb3IpO1xuICogLy8gPT4gWzIuMSwgMS4yXVxuICpcbiAqIC8vIFRoZSBgXy5wcm9wZXJ0eWAgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy51bmlxQnkoW3sgJ3gnOiAxIH0sIHsgJ3gnOiAyIH0sIHsgJ3gnOiAxIH1dLCAneCcpO1xuICogLy8gPT4gW3sgJ3gnOiAxIH0sIHsgJ3gnOiAyIH1dXG4gKi9cbmZ1bmN0aW9uIHVuaXFCeShhcnJheSwgaXRlcmF0ZWUpIHtcbiAgcmV0dXJuIChhcnJheSAmJiBhcnJheS5sZW5ndGgpID8gYmFzZVVuaXEoYXJyYXksIGJhc2VJdGVyYXRlZShpdGVyYXRlZSwgMikpIDogW107XG59XG5cbm1vZHVsZS5leHBvcnRzID0gdW5pcUJ5O1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbG9kYXNoL3VuaXFCeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbG9kYXNoL3VuaXFCeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDU2IiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IHsgRUxFTUVOVFNfR0VUIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL2VsZW1lbnRzJztcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkW10gLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWRbXS5pZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZFtdLnVzZXJuYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkW10uZWxlbWVudFR5cGUgLSAndXNlcicgb3IgJ2NpcmNsZSdcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkW10uY2lyY2xlVHlwZSAtIGlmIGVsZW1lbnRUeXBlIGlzICdjaXJjbGUnIHRoZW5cbiAqICdlZHUnLCAnb3JnJywgJ2ZpZWxkJywgJ2xvY2F0aW9uJywgJ3NvY2lhbCcgZWxzZSB0aGlzIGtleSBub3QgZXhpc3RzLlxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWRbXS5uYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkW10uYXZhdGFyIC1cbiAqIEByZXR1cm5zIHtPYmplY3R9IC0gcmVkdXggYWN0aW9uIHR5cGUuXG4gKi9cbmNvbnN0IGdldEFsbCA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UU19HRVQsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9lbGVtZW50cy9nZXRBbGwuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgeyBQT1NUX0NPTU1FTlRTX0dFVCB9IGZyb20gJy4uLy4uLy4uL2NvbnN0YW50cy9wb3N0cy9jb21tZW50cyc7XG5cbi8qKlxuICpcbiAqIEBmdW5jdGlvbiBnZXRBbGxcbiAqIEBwYXJhbSB7b2JqZWN0fSBwYXlsb2FkXG4gKi9cbmNvbnN0IGdldEFsbCA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBQT1NUX0NPTU1FTlRTX0dFVCwgcGF5bG9hZCB9KTtcblxuZXhwb3J0IGRlZmF1bHQgZ2V0QWxsO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL3Bvc3RzL2NvbW1lbnRzL2dldEFsbC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBnZXRFbGVtZW50c1xuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkLmlkc1xuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRzW10uaWQgLVxuICogQHJldHVybnMge1Byb21pc2V9XG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdG9rZW4sIGlkcyB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL2VsZW1lbnRzP2lkcz0ke0pTT04uc3RyaW5naWZ5KGlkcyl9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRBbGwuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmaWxlXG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGdldEFsbFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBnZXRBbGwoeyB0b2tlbiwgcG9zdElkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvcG9zdHMvJHtwb3N0SWR9L2NvbW1lbnRzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9wb3N0cy9jb21tZW50cy9nZXRBbGwuanMiLCIvKipcbiAqIFRoaXMgY29tcG9uZW50IHdpbGwgZGlzcGxheSBmb3IgY3JlYXRpbmcgY29tbWVudCBhbmQgYWxsIHRoZSBjb21tZW50c1xuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5cbmltcG9ydCB1bmlxQnkgZnJvbSAnbG9kYXNoL3VuaXFCeSc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCBMb2FkYWJsZSBmcm9tICdyZWFjdC1sb2FkYWJsZSc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuXG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGVzJztcbmltcG9ydCBMb2FkaW5nIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvTG9hZGluZyc7XG5cbmltcG9ydCBhcGlHZXRBbGxDb21tZW50IGZyb20gJy4uLy4uL2FwaS9wb3N0cy9jb21tZW50cy9nZXRBbGwnO1xuaW1wb3J0IGFwaUdldEFsbEVsZW1lbnQgZnJvbSAnLi4vLi4vYXBpL2VsZW1lbnRzL2dldEFsbCc7XG5cbmltcG9ydCBhY3Rpb25HZXRBbGxDb21tZW50IGZyb20gJy4uLy4uL2FjdGlvbnMvcG9zdHMvY29tbWVudHMvZ2V0QWxsJztcbmltcG9ydCBhY3Rpb25HZXRBbGxFbGVtZW50cyBmcm9tICcuLi8uLi9hY3Rpb25zL2VsZW1lbnRzL2dldEFsbCc7XG5cbmNvbnN0IEFzeW5jQ29tbWVudENyZWF0ZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vQ3JlYXRlJyksXG4gIG1vZHVsZXM6IFsnLi9DcmVhdGUnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0NvbW1lbnRUaW1lbGluZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vVGltZWxpbmUnKSxcbiAgbW9kdWxlczogWycuL1RpbWVsaW5lJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY2xhc3MgQ29tbWVudCBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAgIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBwb3N0czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIGFjdGlvbkdldEFsbENvbW1lbnQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gICAgYWN0aW9uR2V0QWxsRWxlbWVudHM6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIG1lc3NhZ2U6ICcnLFxuICAgIH07XG4gIH1cblxuICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyB1c2VyLCBwb3N0SWQsIHBvc3RzIH0gPSB0aGlzLnByb3BzO1xuICAgICAgY29uc3QgeyB0b2tlbiB9ID0gdXNlcjtcblxuICAgICAgaWYgKHBvc3RzW3Bvc3RJZF0uY29tbWVudHMgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpR2V0QWxsQ29tbWVudCh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICBwb3N0SWQsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY29uc3QgZWxlbWVudHMgPSB1bmlxQnkocGF5bG9hZC5tYXAoKHApID0+IHAuYXV0aG9ySWQpKTtcblxuICAgICAgY29uc3QgcmVzID0gYXdhaXQgYXBpR2V0QWxsRWxlbWVudCh7IHRva2VuLCBpZHM6IGVsZW1lbnRzIH0pO1xuXG4gICAgICAvKmVzbGludC1kaXNhYmxlKi9cbiAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgbWVzc2FnZTpcbiAgICAgICAgICAgICdUaGVyZSBpcyBhbnkgaW50ZXJuYWwgcHJvYmxlbSB3aGlsZSBsb2FkaW5nIGNvbW1lbnRzLiBQbGVhc2UgcmVmcmVzaCB0aGUgcGFnZScsXG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgICAgLyogZXNsaW50LWVuYWJsZSAqL1xuXG4gICAgICB0aGlzLnByb3BzLmFjdGlvbkdldEFsbENvbW1lbnQoeyBwb3N0SWQsIHBheWxvYWQgfSk7XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uR2V0QWxsRWxlbWVudHMoeyBlbGVtZW50czogcmVzLnBheWxvYWQgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHBvc3RJZCwgcG9zdHMgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCB7IGNvbW1lbnRzIH0gPSBwb3N0c1twb3N0SWRdO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0gc209ezEyfSBtZD17MTJ9IGxnPXsxMn0geGw9ezEyfT5cbiAgICAgICAgICA8QXN5bmNDb21tZW50Q3JlYXRlIHBvc3RJZD17cG9zdElkfSAvPlxuXG4gICAgICAgICAge2NvbW1lbnRzICE9PSB1bmRlZmluZWRcbiAgICAgICAgICAgID8gT2JqZWN0LmtleXMoY29tbWVudHMpLm1hcCgoY29tbWVudElkKSA9PiAoXG4gICAgICAgICAgICAgIDxBc3luY0NvbW1lbnRUaW1lbGluZVxuICAgICAgICAgICAgICAgIGtleT17Y29tbWVudElkfVxuICAgICAgICAgICAgICAgIHBvc3RJZD17cG9zdElkfVxuICAgICAgICAgICAgICAgIGNvbW1lbnQ9e2NvbW1lbnRzW2NvbW1lbnRJZF19XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICApKVxuICAgICAgICAgICAgOiBudWxsfVxuICAgICAgICA8L0dyaWQ+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBwb3N0cywgZWxlbWVudHMgfSkgPT4gKHtcbiAgcG9zdHMsXG4gIHVzZXI6IGVsZW1lbnRzLnVzZXIsXG59KTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PlxuICBiaW5kQWN0aW9uQ3JlYXRvcnMoXG4gICAge1xuICAgICAgYWN0aW9uR2V0QWxsQ29tbWVudCxcbiAgICAgIGFjdGlvbkdldEFsbEVsZW1lbnRzLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoQ29tbWVudCksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvbW1lbnQvaW5kZXguanMiLCJjb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHt9LFxuICBuYW1lOiB7XG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgYXZhdGFyOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBidXR0b246IHtcbiAgICBtYXJnaW5Ub3A6ICczMHB4JyxcbiAgfSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHN0eWxlcztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Db21tZW50L3N0eWxlcy5qcyJdLCJzb3VyY2VSb290IjoiIn0=