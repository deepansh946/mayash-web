webpackJsonp([34],{

/***/ "./node_modules/dom-helpers/activeElement.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/activeElement.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = activeElement;

var _ownerDocument = __webpack_require__(/*! ./ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function activeElement() {
  var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _ownerDocument2.default)();

  try {
    return doc.activeElement;
  } catch (e) {/* ie throws if no active element */}
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/class/addClass.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/class/addClass.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = addClass;

var _hasClass = __webpack_require__(/*! ./hasClass */ "./node_modules/dom-helpers/class/hasClass.js");

var _hasClass2 = _interopRequireDefault(_hasClass);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function addClass(element, className) {
  if (element.classList) element.classList.add(className);else if (!(0, _hasClass2.default)(element, className)) if (typeof element.className === 'string') element.className = element.className + ' ' + className;else element.setAttribute('class', (element.className && element.className.baseVal || '') + ' ' + className);
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/class/hasClass.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/class/hasClass.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = hasClass;
function hasClass(element, className) {
  if (element.classList) return !!className && element.classList.contains(className);else return (" " + (element.className.baseVal || element.className) + " ").indexOf(" " + className + " ") !== -1;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/class/removeClass.js":
/*!*******************************************************!*\
  !*** ./node_modules/dom-helpers/class/removeClass.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function replaceClassName(origClass, classToRemove) {
  return origClass.replace(new RegExp('(^|\\s)' + classToRemove + '(?:\\s|$)', 'g'), '$1').replace(/\s+/g, ' ').replace(/^\s*|\s*$/g, '');
}

module.exports = function removeClass(element, className) {
  if (element.classList) element.classList.remove(className);else if (typeof element.className === 'string') element.className = replaceClassName(element.className, className);else element.setAttribute('class', replaceClassName(element.className && element.className.baseVal || '', className));
};

/***/ }),

/***/ "./node_modules/dom-helpers/ownerDocument.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/ownerDocument.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ownerDocument;
function ownerDocument(node) {
  return node && node.ownerDocument || document;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/query/isWindow.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/query/isWindow.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getWindow;
function getWindow(node) {
  return node === node.window ? node : node.nodeType === 9 ? node.defaultView || node.parentWindow : false;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/util/scrollbarSize.js":
/*!********************************************************!*\
  !*** ./node_modules/dom-helpers/util/scrollbarSize.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (recalc) {
  if (!size && size !== 0 || recalc) {
    if (_inDOM2.default) {
      var scrollDiv = document.createElement('div');

      scrollDiv.style.position = 'absolute';
      scrollDiv.style.top = '-9999px';
      scrollDiv.style.width = '50px';
      scrollDiv.style.height = '50px';
      scrollDiv.style.overflow = 'scroll';

      document.body.appendChild(scrollDiv);
      size = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
    }
  }

  return size;
};

var _inDOM = __webpack_require__(/*! ./inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = void 0;

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/material-ui-icons/AttachFile.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/AttachFile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z' });

var AttachFile = function AttachFile(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

AttachFile = (0, _pure2.default)(AttachFile);
AttachFile.muiName = 'SvgIcon';

exports.default = AttachFile;

/***/ }),

/***/ "./node_modules/material-ui-icons/Code.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Code.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z' });

var Code = function Code(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Code = (0, _pure2.default)(Code);
Code.muiName = 'SvgIcon';

exports.default = Code;

/***/ }),

/***/ "./node_modules/material-ui-icons/Comment.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui-icons/Comment.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var Comment = function Comment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Comment = (0, _pure2.default)(Comment);
Comment.muiName = 'SvgIcon';

exports.default = Comment;

/***/ }),

/***/ "./node_modules/material-ui-icons/Edit.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Edit.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z' });

var Edit = function Edit(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Edit = (0, _pure2.default)(Edit);
Edit.muiName = 'SvgIcon';

exports.default = Edit;

/***/ }),

/***/ "./node_modules/material-ui-icons/Favorite.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/Favorite.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z' });

var Favorite = function Favorite(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Favorite = (0, _pure2.default)(Favorite);
Favorite.muiName = 'SvgIcon';

exports.default = Favorite;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignCenter.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignCenter.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M7 15v2h10v-2H7zm-4 6h18v-2H3v2zm0-8h18v-2H3v2zm4-6v2h10V7H7zM3 3v2h18V3H3z' });

var FormatAlignCenter = function FormatAlignCenter(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignCenter = (0, _pure2.default)(FormatAlignCenter);
FormatAlignCenter.muiName = 'SvgIcon';

exports.default = FormatAlignCenter;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignJustify.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignJustify.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18V7H3v2zm0-6v2h18V3H3z' });

var FormatAlignJustify = function FormatAlignJustify(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignJustify = (0, _pure2.default)(FormatAlignJustify);
FormatAlignJustify.muiName = 'SvgIcon';

exports.default = FormatAlignJustify;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignLeft.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignLeft.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15 15H3v2h12v-2zm0-8H3v2h12V7zM3 13h18v-2H3v2zm0 8h18v-2H3v2zM3 3v2h18V3H3z' });

var FormatAlignLeft = function FormatAlignLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignLeft = (0, _pure2.default)(FormatAlignLeft);
FormatAlignLeft.muiName = 'SvgIcon';

exports.default = FormatAlignLeft;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignRight.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignRight.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm6-4h12v-2H9v2zm-6-4h18v-2H3v2zm6-4h12V7H9v2zM3 3v2h18V3H3z' });

var FormatAlignRight = function FormatAlignRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignRight = (0, _pure2.default)(FormatAlignRight);
FormatAlignRight.muiName = 'SvgIcon';

exports.default = FormatAlignRight;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatBold.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatBold.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.6 10.79c.97-.67 1.65-1.77 1.65-2.79 0-2.26-1.75-4-4-4H7v14h7.04c2.09 0 3.71-1.7 3.71-3.79 0-1.52-.86-2.82-2.15-3.42zM10 6.5h3c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-3v-3zm3.5 9H10v-3h3.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5z' });

var FormatBold = function FormatBold(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatBold = (0, _pure2.default)(FormatBold);
FormatBold.muiName = 'SvgIcon';

exports.default = FormatBold;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatItalic.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatItalic.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4z' });

var FormatItalic = function FormatItalic(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatItalic = (0, _pure2.default)(FormatItalic);
FormatItalic.muiName = 'SvgIcon';

exports.default = FormatItalic;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListBulleted.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListBulleted.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M4 10.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5-.67-1.5-1.5-1.5zm0-6c-.83 0-1.5.67-1.5 1.5S3.17 7.5 4 7.5 5.5 6.83 5.5 6 4.83 4.5 4 4.5zm0 12c-.83 0-1.5.68-1.5 1.5s.68 1.5 1.5 1.5 1.5-.68 1.5-1.5-.67-1.5-1.5-1.5zM7 19h14v-2H7v2zm0-6h14v-2H7v2zm0-8v2h14V5H7z' });

var FormatListBulleted = function FormatListBulleted(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListBulleted = (0, _pure2.default)(FormatListBulleted);
FormatListBulleted.muiName = 'SvgIcon';

exports.default = FormatListBulleted;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListNumbered.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListNumbered.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z' });

var FormatListNumbered = function FormatListNumbered(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListNumbered = (0, _pure2.default)(FormatListNumbered);
FormatListNumbered.muiName = 'SvgIcon';

exports.default = FormatListNumbered;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatQuote.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatQuote.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' });

var FormatQuote = function FormatQuote(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatQuote = (0, _pure2.default)(FormatQuote);
FormatQuote.muiName = 'SvgIcon';

exports.default = FormatQuote;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatUnderlined.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatUnderlined.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z' });

var FormatUnderlined = function FormatUnderlined(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatUnderlined = (0, _pure2.default)(FormatUnderlined);
FormatUnderlined.muiName = 'SvgIcon';

exports.default = FormatUnderlined;

/***/ }),

/***/ "./node_modules/material-ui-icons/Functions.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Functions.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 4H6v2l6.5 6L6 18v2h12v-3h-7l5-5-5-5h7z' });

var Functions = function Functions(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Functions = (0, _pure2.default)(Functions);
Functions.muiName = 'SvgIcon';

exports.default = Functions;

/***/ }),

/***/ "./node_modules/material-ui-icons/Highlight.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Highlight.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 14l3 3v5h6v-5l3-3V9H6zm5-12h2v3h-2zM3.5 5.875L4.914 4.46l2.12 2.122L5.62 7.997zm13.46.71l2.123-2.12 1.414 1.414L18.375 8z' });

var Highlight = function Highlight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Highlight = (0, _pure2.default)(Highlight);
Highlight.muiName = 'SvgIcon';

exports.default = Highlight;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertComment.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertComment.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var InsertComment = function InsertComment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertComment = (0, _pure2.default)(InsertComment);
InsertComment.muiName = 'SvgIcon';

exports.default = InsertComment;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertEmoticon.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertEmoticon.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });

var InsertEmoticon = function InsertEmoticon(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertEmoticon = (0, _pure2.default)(InsertEmoticon);
InsertEmoticon.muiName = 'SvgIcon';

exports.default = InsertEmoticon;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertLink.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertLink.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z' });

var InsertLink = function InsertLink(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertLink = (0, _pure2.default)(InsertLink);
InsertLink.muiName = 'SvgIcon';

exports.default = InsertLink;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui-icons/MoreVert.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/MoreVert.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });

var MoreVert = function MoreVert(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

MoreVert = (0, _pure2.default)(MoreVert);
MoreVert.muiName = 'SvgIcon';

exports.default = MoreVert;

/***/ }),

/***/ "./node_modules/material-ui-icons/Save.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Save.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z' });

var Save = function Save(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Save = (0, _pure2.default)(Save);
Save.muiName = 'SvgIcon';

exports.default = Save;

/***/ }),

/***/ "./node_modules/material-ui-icons/Share.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Share.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z' });

var Share = function Share(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Share = (0, _pure2.default)(Share);
Share.muiName = 'SvgIcon';

exports.default = Share;

/***/ }),

/***/ "./node_modules/material-ui-icons/Title.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Title.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M5 4v3h5.5v12h3V7H19V4z' });

var Title = function Title(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Title = (0, _pure2.default)(Title);
Title.muiName = 'SvgIcon';

exports.default = Title;

/***/ }),

/***/ "./node_modules/material-ui-icons/TurnedIn.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/TurnedIn.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z' });

var TurnedIn = function TurnedIn(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

TurnedIn = (0, _pure2.default)(TurnedIn);
TurnedIn.muiName = 'SvgIcon';

exports.default = TurnedIn;

/***/ }),

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Badge/Badge.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/Badge.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var RADIUS = 12;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'inline-flex'
    },
    badge: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      top: -RADIUS,
      right: -RADIUS,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeight,
      fontSize: theme.typography.pxToRem(RADIUS),
      width: RADIUS * 2,
      height: RADIUS * 2,
      borderRadius: '50%',
      backgroundColor: theme.palette.color,
      color: theme.palette.textColor,
      zIndex: 1 // Render the badge on top of potential ripples.
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content rendered within the badge.
   */
  badgeContent: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * The badge will be added relative to this node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'primary', 'accent']).isRequired
};

var Badge = function (_React$Component) {
  (0, _inherits3.default)(Badge, _React$Component);

  function Badge() {
    (0, _classCallCheck3.default)(this, Badge);
    return (0, _possibleConstructorReturn3.default)(this, (Badge.__proto__ || (0, _getPrototypeOf2.default)(Badge)).apply(this, arguments));
  }

  (0, _createClass3.default)(Badge, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          badgeContent = _props.badgeContent,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          children = _props.children,
          other = (0, _objectWithoutProperties3.default)(_props, ['badgeContent', 'classes', 'className', 'color', 'children']);

      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var badgeClassName = (0, _classnames2.default)(classes.badge, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'));

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        children,
        _react2.default.createElement(
          'span',
          { className: badgeClassName },
          badgeContent
        )
      );
    }
  }]);
  return Badge;
}(_react2.default.Component);

Badge.defaultProps = {
  color: 'default'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBadge' })(Badge);

/***/ }),

/***/ "./node_modules/material-ui/Badge/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Badge = __webpack_require__(/*! ./Badge */ "./node_modules/material-ui/Badge/Badge.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Badge).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Icon/Icon.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Icon/Icon.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      userSelect: 'none'
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The name of the icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired
};

var Icon = function (_React$Component) {
  (0, _inherits3.default)(Icon, _React$Component);

  function Icon() {
    (0, _classCallCheck3.default)(this, Icon);
    return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
  }

  (0, _createClass3.default)(Icon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color']);


      var className = (0, _classnames2.default)('material-icons', classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'span',
        (0, _extends3.default)({ className: className, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Icon;
}(_react2.default.Component);

Icon.defaultProps = {
  color: 'inherit'
};
Icon.muiName = 'Icon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIcon' })(Icon);

/***/ }),

/***/ "./node_modules/material-ui/Icon/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Icon/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/material-ui/Icon/Icon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Icon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/List/List.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/List/List.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      flex: '1 1 auto',
      listStyle: 'none',
      margin: 0,
      padding: 0,
      position: 'relative'
    },
    padding: {
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit
    },
    dense: {
      paddingTop: theme.spacing.unit / 2,
      paddingBottom: theme.spacing.unit / 2
    },
    subheader: {
      paddingTop: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, compact vertical padding designed for keyboard and mouse input will be used for
   * the list and list items. The property is available to descendant components as the
   * `dense` context.
   */
  dense: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, vertical padding will be removed from the list.
   */
  disablePadding: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The content of the component, normally `ListItem`.
   */
  subheader: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
};

var List = function (_React$Component) {
  (0, _inherits3.default)(List, _React$Component);

  function List() {
    (0, _classCallCheck3.default)(this, List);
    return (0, _possibleConstructorReturn3.default)(this, (List.__proto__ || (0, _getPrototypeOf2.default)(List)).apply(this, arguments));
  }

  (0, _createClass3.default)(List, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        dense: this.props.dense
      };
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          ComponentProp = _props.component,
          disablePadding = _props.disablePadding,
          children = _props.children,
          dense = _props.dense,
          subheader = _props.subheader,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'component', 'disablePadding', 'children', 'dense', 'subheader', 'rootRef']);

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.dense, dense && !disablePadding), (0, _defineProperty3.default)(_classNames, classes.padding, !disablePadding), (0, _defineProperty3.default)(_classNames, classes.subheader, subheader), _classNames), classNameProp);

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other, { ref: rootRef }),
        subheader,
        children
      );
    }
  }]);
  return List;
}(_react2.default.Component);

List.defaultProps = {
  component: 'ul',
  dense: false,
  disablePadding: false
};


List.childContextTypes = {
  dense: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiList' })(List);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItem.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/List/ListItem.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      position: 'relative',
      textDecoration: 'none'
    },
    container: {
      position: 'relative'
    },
    keyboardFocused: {
      background: theme.palette.text.divider
    },
    default: {
      paddingTop: 12,
      paddingBottom: 12
    },
    dense: {
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit
    },
    disabled: {
      opacity: 0.5
    },
    divider: {
      borderBottom: '1px solid ' + theme.palette.text.lightDivider
    },
    gutters: {
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    button: {
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      }),
      '&:hover': {
        textDecoration: 'none',
        backgroundColor: theme.palette.text.divider,
        // Reset on mouse devices
        '@media (hover: none)': {
          backgroundColor: 'transparent'
        },
        '&$disabled': {
          backgroundColor: 'transparent'
        }
      }
    },
    secondaryAction: {
      // Add some space to avoid collision as `ListItemSecondaryAction`
      // is absolutely positionned.
      paddingRight: theme.spacing.unit * 4
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * If `true`, the ListItem will be a button.
   */
  button: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, compact vertical padding designed for keyboard and mouse input will be used.
   */
  dense: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the left and right padding is removed.
   */
  disableGutters: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, a 1px light border is added to the bottom of the list item.
   */
  divider: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var ListItem = function (_React$Component) {
  (0, _inherits3.default)(ListItem, _React$Component);

  function ListItem() {
    (0, _classCallCheck3.default)(this, ListItem);
    return (0, _possibleConstructorReturn3.default)(this, (ListItem.__proto__ || (0, _getPrototypeOf2.default)(ListItem)).apply(this, arguments));
  }

  (0, _createClass3.default)(ListItem, [{
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        dense: this.props.dense || this.context.dense || false
      };
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          button = _props.button,
          childrenProp = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          componentProp = _props.component,
          dense = _props.dense,
          disabled = _props.disabled,
          divider = _props.divider,
          disableGutters = _props.disableGutters,
          other = (0, _objectWithoutProperties3.default)(_props, ['button', 'children', 'classes', 'className', 'component', 'dense', 'disabled', 'divider', 'disableGutters']);

      var isDense = dense || this.context.dense || false;
      var children = _react2.default.Children.toArray(childrenProp);

      var hasAvatar = children.some(function (value) {
        return (0, _reactHelpers.isMuiElement)(value, ['ListItemAvatar']);
      });
      var hasSecondaryAction = children.length && (0, _reactHelpers.isMuiElement)(children[children.length - 1], ['ListItemSecondaryAction']);

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.gutters, !disableGutters), (0, _defineProperty3.default)(_classNames, classes.divider, divider), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), (0, _defineProperty3.default)(_classNames, classes.button, button), (0, _defineProperty3.default)(_classNames, classes.secondaryAction, hasSecondaryAction), (0, _defineProperty3.default)(_classNames, isDense || hasAvatar ? classes.dense : classes.default, true), _classNames), classNameProp);

      var listItemProps = (0, _extends3.default)({ className: className, disabled: disabled }, other);
      var ComponentMain = componentProp;

      if (button) {
        ComponentMain = _ButtonBase2.default;
        listItemProps.component = componentProp || 'li';
        listItemProps.keyboardFocusedClassName = classes.keyboardFocused;
      }

      if (hasSecondaryAction) {
        return _react2.default.createElement(
          'div',
          { className: classes.container },
          _react2.default.createElement(
            ComponentMain,
            listItemProps,
            children
          ),
          children.pop()
        );
      }

      return _react2.default.createElement(
        ComponentMain,
        listItemProps,
        children
      );
    }
  }]);
  return ListItem;
}(_react2.default.Component);

ListItem.defaultProps = {
  button: false,
  component: 'li',
  dense: false,
  disabled: false,
  disableGutters: false,
  divider: false
};


ListItem.contextTypes = {
  dense: _propTypes2.default.bool
};

ListItem.childContextTypes = {
  dense: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItem' })(ListItem);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemAvatar.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemAvatar.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      width: 36,
      height: 36,
      fontSize: theme.typography.pxToRem(18),
      marginRight: 4
    },
    icon: {
      width: 20,
      height: 20,
      fontSize: theme.typography.pxToRem(20)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component, normally `Avatar`.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


/**
 * It's a simple wrapper to apply the `dense` mode styles to `Avatar`.
 */
function ListItemAvatar(props, context) {
  if (context.dense === undefined) {
     true ? (0, _warning2.default)(false, 'Material-UI: <ListItemAvatar> is a simple wrapper to apply the dense styles\n      to <Avatar>. You do not need it unless you are controlling the <List> dense property.') : void 0;
    return props.children;
  }

  var children = props.children,
      classes = props.classes,
      classNameProp = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.cloneElement(children, (0, _extends3.default)({
    className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.root, context.dense), classNameProp, children.props.className),
    childrenClassName: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.icon, context.dense), children.props.childrenClassName)
  }, other));
}

ListItemAvatar.contextTypes = {
  dense: _propTypes2.default.bool
};

ListItemAvatar.muiName = 'ListItemAvatar';

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemAvatar' })(ListItemAvatar);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemIcon.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemIcon.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      height: 24,
      marginRight: theme.spacing.unit * 2,
      width: 24,
      color: theme.palette.action.active,
      flexShrink: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component, normally `Icon`, `SvgIcon`,
   * or a `material-ui-icons` SVG icon component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


/**
 * A simple wrapper to apply `List` styles to an `Icon` or `SvgIcon`.
 */
function ListItemIcon(props) {
  var children = props.children,
      classes = props.classes,
      classNameProp = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.cloneElement(children, (0, _extends3.default)({
    className: (0, _classnames2.default)(classes.root, classNameProp, children.props.className)
  }, other));
}

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemIcon' })(ListItemIcon);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemSecondaryAction.js":
/*!******************************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemSecondaryAction.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _ref; //  weak

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      right: 4,
      top: '50%',
      marginTop: -theme.spacing.unit * 3
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component, normally an `IconButton` or selection control.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function ListItemSecondaryAction(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className;


  return _react2.default.createElement(
    'div',
    { className: (0, _classnames2.default)(classes.root, className) },
    children
  );
}

ListItemSecondaryAction.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
ListItemSecondaryAction.muiName = 'ListItemSecondaryAction';

exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemSecondaryAction' })(ListItemSecondaryAction);

/***/ }),

/***/ "./node_modules/material-ui/List/ListItemText.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/List/ListItemText.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! ../Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      flex: '1 1 auto',
      padding: '0 16px',
      '&:first-child': {
        paddingLeft: 0
      }
    },
    inset: {
      '&:first-child': {
        paddingLeft: theme.spacing.unit * 7
      }
    },
    dense: {
      fontSize: theme.typography.pxToRem(13)
    },
    text: {}, // Present to allow external customization
    textDense: {
      fontSize: 'inherit'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the children won't be wrapped by a typography component.
   * For instance, that can be useful to can render an h4 instead of a
   */
  disableTypography: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the children will be indented.
   * This should be used if there is no left avatar or left icon.
   */
  inset: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,
  primary: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,
  secondary: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired
};

var ListItemText = function (_React$Component) {
  (0, _inherits3.default)(ListItemText, _React$Component);

  function ListItemText() {
    (0, _classCallCheck3.default)(this, ListItemText);
    return (0, _possibleConstructorReturn3.default)(this, (ListItemText.__proto__ || (0, _getPrototypeOf2.default)(ListItemText)).apply(this, arguments));
  }

  (0, _createClass3.default)(ListItemText, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          disableTypography = _props.disableTypography,
          primary = _props.primary,
          secondary = _props.secondary,
          inset = _props.inset,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'disableTypography', 'primary', 'secondary', 'inset']);
      var dense = this.context.dense;

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.dense, dense), (0, _defineProperty3.default)(_classNames, classes.inset, inset), _classNames), classNameProp);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        primary && (disableTypography ? primary : _react2.default.createElement(
          _Typography2.default,
          {
            type: 'subheading',
            className: (0, _classnames2.default)(classes.text, (0, _defineProperty3.default)({}, classes.textDense, dense))
          },
          primary
        )),
        secondary && (disableTypography ? secondary : _react2.default.createElement(
          _Typography2.default,
          {
            color: 'secondary',
            type: 'body1',
            className: (0, _classnames2.default)(classes.text, (0, _defineProperty3.default)({}, classes.textDense, dense))
          },
          secondary
        ))
      );
    }
  }]);
  return ListItemText;
}(_react2.default.Component);

ListItemText.defaultProps = {
  disableTypography: false,
  primary: false,
  secondary: false,
  inset: false
};
ListItemText.contextTypes = {
  dense: _propTypes2.default.bool
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListItemText' })(ListItemText);

/***/ }),

/***/ "./node_modules/material-ui/List/ListSubheader.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/List/ListSubheader.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      boxSizing: 'border-box',
      lineHeight: '48px',
      listStyle: 'none',
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      color: theme.palette.text.secondary,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeightMedium,
      fontSize: theme.typography.pxToRem(theme.typography.fontSize)
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    inset: {
      paddingLeft: theme.spacing.unit * 9
    },
    sticky: {
      position: 'sticky',
      top: 0,
      zIndex: 1,
      backgroundColor: 'inherit'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   * The default value is a `button`.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'primary', 'inherit']),

  /**
   * If `true`, the List Subheader will not stick to the top during scroll.
   */
  disableSticky: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the List Subheader will be indented.
   */
  inset: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var ListSubheader = function (_React$Component) {
  (0, _inherits3.default)(ListSubheader, _React$Component);

  function ListSubheader() {
    (0, _classCallCheck3.default)(this, ListSubheader);
    return (0, _possibleConstructorReturn3.default)(this, (ListSubheader.__proto__ || (0, _getPrototypeOf2.default)(ListSubheader)).apply(this, arguments));
  }

  (0, _createClass3.default)(ListSubheader, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          ComponentProp = _props.component,
          color = _props.color,
          disableSticky = _props.disableSticky,
          inset = _props.inset,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'component', 'color', 'disableSticky', 'inset']);

      var className = (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.inset, inset), (0, _defineProperty3.default)(_classNames, classes.sticky, !disableSticky), _classNames), classNameProp);

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return ListSubheader;
}(_react2.default.Component);

ListSubheader.defaultProps = {
  component: 'li',
  color: 'default',
  disableSticky: false,
  inset: false
};
ListSubheader.muiName = 'ListSubheader';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiListSubheader' })(ListSubheader);

/***/ }),

/***/ "./node_modules/material-ui/List/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/List/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _List = __webpack_require__(/*! ./List */ "./node_modules/material-ui/List/List.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_List).default;
  }
});

var _ListItem = __webpack_require__(/*! ./ListItem */ "./node_modules/material-ui/List/ListItem.js");

Object.defineProperty(exports, 'ListItem', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItem).default;
  }
});

var _ListItemAvatar = __webpack_require__(/*! ./ListItemAvatar */ "./node_modules/material-ui/List/ListItemAvatar.js");

Object.defineProperty(exports, 'ListItemAvatar', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemAvatar).default;
  }
});

var _ListItemText = __webpack_require__(/*! ./ListItemText */ "./node_modules/material-ui/List/ListItemText.js");

Object.defineProperty(exports, 'ListItemText', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemText).default;
  }
});

var _ListItemIcon = __webpack_require__(/*! ./ListItemIcon */ "./node_modules/material-ui/List/ListItemIcon.js");

Object.defineProperty(exports, 'ListItemIcon', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemIcon).default;
  }
});

var _ListItemSecondaryAction = __webpack_require__(/*! ./ListItemSecondaryAction */ "./node_modules/material-ui/List/ListItemSecondaryAction.js");

Object.defineProperty(exports, 'ListItemSecondaryAction', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListItemSecondaryAction).default;
  }
});

var _ListSubheader = __webpack_require__(/*! ./ListSubheader */ "./node_modules/material-ui/List/ListSubheader.js");

Object.defineProperty(exports, 'ListSubheader', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_ListSubheader).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Menu/Menu.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Menu/Menu.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Popover = __webpack_require__(/*! ../Popover */ "./node_modules/material-ui/Popover/index.js");

var _Popover2 = _interopRequireDefault(_Popover);

var _MenuList = __webpack_require__(/*! ./MenuList */ "./node_modules/material-ui/Menu/MenuList.js");

var _MenuList2 = _interopRequireDefault(_MenuList);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Popover

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The DOM element used to set the position of the menu.
   */
  anchorEl: typeof HTMLElement === 'function' ? __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").instanceOf(HTMLElement) : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any,
  // match Popover
  /**
   * Menu contents, normally `MenuItem`s.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Properties applied to the `MenuList` element.
   */
  MenuListProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Callback fired before the Menu enters.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired before the Menu exits.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the Menu has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the menu is visible.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  PaperProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * `classes` property applied to the `Popover` element.
   */
  PopoverClasses: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The length of the transition in `ms`, or 'auto'
   */
  transitionDuration: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]).isRequired
};


var rtlOrigin = {
  vertical: 'top',
  horizontal: 'right'
};

var ltrOrigin = {
  vertical: 'top',
  horizontal: 'left'
};

var styles = exports.styles = {
  paper: {
    // specZ: The maximum height of a simple menu should be one or more rows less than the view
    // height. This ensures a tappable area outside of the simple menu with which to dismiss
    // the menu.
    maxHeight: 'calc(100vh - 96px)',
    // Add iOS momentum scrolling.
    WebkitOverflowScrolling: 'touch'
  }
};

var Menu = function (_React$Component) {
  (0, _inherits3.default)(Menu, _React$Component);

  function Menu() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Menu);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Menu.__proto__ || (0, _getPrototypeOf2.default)(Menu)).call.apply(_ref, [this].concat(args))), _this), _this.getContentAnchorEl = function () {
      if (!_this.menuList || !_this.menuList.selectedItem) {
        // $FlowFixMe
        return (0, _reactDom.findDOMNode)(_this.menuList).firstChild;
      }

      return (0, _reactDom.findDOMNode)(_this.menuList.selectedItem);
    }, _this.menuList = undefined, _this.focus = function () {
      if (_this.menuList && _this.menuList.selectedItem) {
        // $FlowFixMe
        (0, _reactDom.findDOMNode)(_this.menuList.selectedItem).focus();
        return;
      }

      var menuList = (0, _reactDom.findDOMNode)(_this.menuList);
      if (menuList && menuList.firstChild) {
        // $FlowFixMe
        menuList.firstChild.focus();
      }
    }, _this.handleEnter = function (element) {
      var theme = _this.props.theme;


      var menuList = (0, _reactDom.findDOMNode)(_this.menuList);

      // Focus so the scroll computation of the Popover works as expected.
      _this.focus();

      // Let's ignore that piece of logic if users are already overriding the width
      // of the menu.
      // $FlowFixMe
      if (menuList && element.clientHeight < menuList.clientHeight && !menuList.style.width) {
        var size = (0, _scrollbarSize2.default)() + 'px';
        // $FlowFixMe
        menuList.style[theme.direction === 'rtl' ? 'paddingLeft' : 'paddingRight'] = size;
        // $FlowFixMe
        menuList.style.width = 'calc(100% + ' + size + ')';
      }

      if (_this.props.onEnter) {
        _this.props.onEnter(element);
      }
    }, _this.handleListKeyDown = function (event, key) {
      if (key === 'tab') {
        event.preventDefault();

        if (_this.props.onRequestClose) {
          _this.props.onRequestClose(event);
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Menu, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      if (this.props.open) {
        this.focus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.open && this.props.open) {
        // Needs to refocus as when a menu is rendered into another Modal,
        // the first modal might change the focus to prevent any leak.
        this.focus();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          MenuListProps = _props.MenuListProps,
          onEnter = _props.onEnter,
          _props$PaperProps = _props.PaperProps,
          PaperProps = _props$PaperProps === undefined ? {} : _props$PaperProps,
          PopoverClasses = _props.PopoverClasses,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'MenuListProps', 'onEnter', 'PaperProps', 'PopoverClasses', 'theme']);


      var themeDirection = theme && theme.direction;
      return _react2.default.createElement(
        _Popover2.default,
        (0, _extends3.default)({
          getContentAnchorEl: this.getContentAnchorEl,
          classes: PopoverClasses,
          onEnter: this.handleEnter,
          anchorOrigin: themeDirection === 'rtl' ? rtlOrigin : ltrOrigin,
          transformOrigin: themeDirection === 'rtl' ? rtlOrigin : ltrOrigin,
          PaperProps: (0, _extends3.default)({}, PaperProps, {
            classes: (0, _extends3.default)({}, PaperProps.classes, {
              root: classes.paper
            })
          })
        }, other),
        _react2.default.createElement(
          _MenuList2.default,
          (0, _extends3.default)({
            role: 'menu',
            onKeyDown: this.handleListKeyDown
          }, MenuListProps, {
            ref: function ref(node) {
              _this2.menuList = node;
            }
          }),
          children
        )
      );
    }
  }]);
  return Menu;
}(_react2.default.Component);

Menu.defaultProps = {
  open: false,
  transitionDuration: 'auto'
};
exports.default = (0, _withStyles2.default)(styles, { withTheme: true, name: 'MuiMenu' })(Menu);

/***/ }),

/***/ "./node_modules/material-ui/Menu/MenuItem.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Menu/MenuItem.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ListItem = __webpack_require__(/*! ../List/ListItem */ "./node_modules/material-ui/List/ListItem.js");

var _ListItem2 = _interopRequireDefault(_ListItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent ListItem

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends3.default)({}, theme.typography.subheading, {
      height: 24,
      boxSizing: 'content-box',
      background: 'none',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      whiteSpace: 'nowrap',
      '&:focus': {
        background: theme.palette.text.divider
      },
      '&:hover': {
        backgroundColor: theme.palette.text.divider
      }
    }),
    selected: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Menu item contents.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType),

  /**
   * @ignore
   */
  role: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired,

  /**
   * Use to apply selected styling.
   */
  selected: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var MenuItem = function (_React$Component) {
  (0, _inherits3.default)(MenuItem, _React$Component);

  function MenuItem() {
    (0, _classCallCheck3.default)(this, MenuItem);
    return (0, _possibleConstructorReturn3.default)(this, (MenuItem.__proto__ || (0, _getPrototypeOf2.default)(MenuItem)).apply(this, arguments));
  }

  (0, _createClass3.default)(MenuItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          component = _props.component,
          selected = _props.selected,
          role = _props.role,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'component', 'selected', 'role']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.selected, selected), classNameProp);

      return _react2.default.createElement(_ListItem2.default, (0, _extends3.default)({
        button: true,
        role: role,
        tabIndex: -1,
        className: className,
        component: component
      }, other));
    }
  }]);
  return MenuItem;
}(_react2.default.Component);

MenuItem.defaultProps = {
  role: 'menuitem',
  selected: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiMenuItem' })(MenuItem);

/***/ }),

/***/ "./node_modules/material-ui/Menu/MenuList.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Menu/MenuList.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _toConsumableArray2 = __webpack_require__(/*! babel-runtime/helpers/toConsumableArray */ "./node_modules/babel-runtime/helpers/toConsumableArray.js");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _List = __webpack_require__(/*! ../List */ "./node_modules/material-ui/List/index.js");

var _List2 = _interopRequireDefault(_List);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent List

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * MenuList contents, normally `MenuItem`s.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   */
  onBlur: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  onKeyDown: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

var MenuList = function (_React$Component) {
  (0, _inherits3.default)(MenuList, _React$Component);

  function MenuList() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, MenuList);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = MenuList.__proto__ || (0, _getPrototypeOf2.default)(MenuList)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      currentTabIndex: undefined
    }, _this.list = undefined, _this.selectedItem = undefined, _this.blurTimer = undefined, _this.handleBlur = function (event) {
      _this.blurTimer = setTimeout(function () {
        if (_this.list) {
          var list = (0, _reactDom.findDOMNode)(_this.list);
          var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(list));
          if (!(0, _contains2.default)(list, currentFocus)) {
            _this.resetTabIndex();
          }
        }
      }, 30);

      if (_this.props.onBlur) {
        _this.props.onBlur(event);
      }
    }, _this.handleKeyDown = function (event) {
      var list = (0, _reactDom.findDOMNode)(_this.list);
      var key = (0, _keycode2.default)(event);
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(list));

      if ((key === 'up' || key === 'down') && (!currentFocus || currentFocus && !(0, _contains2.default)(list, currentFocus))) {
        if (_this.selectedItem) {
          // $FlowFixMe
          (0, _reactDom.findDOMNode)(_this.selectedItem).focus();
        } else {
          // $FlowFixMe
          list.firstChild.focus();
        }
      } else if (key === 'down') {
        event.preventDefault();
        if (currentFocus.nextElementSibling) {
          currentFocus.nextElementSibling.focus();
        }
      } else if (key === 'up') {
        event.preventDefault();
        if (currentFocus.previousElementSibling) {
          currentFocus.previousElementSibling.focus();
        }
      }

      if (_this.props.onKeyDown) {
        _this.props.onKeyDown(event, key);
      }
    }, _this.handleItemFocus = function (event) {
      var list = (0, _reactDom.findDOMNode)(_this.list);
      if (list) {
        // $FlowFixMe
        for (var i = 0; i < list.children.length; i += 1) {
          // $FlowFixMe
          if (list.children[i] === event.currentTarget) {
            _this.setTabIndex(i);
            break;
          }
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(MenuList, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.resetTabIndex();
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      clearTimeout(this.blurTimer);
    }
  }, {
    key: 'setTabIndex',
    value: function setTabIndex(index) {
      this.setState({ currentTabIndex: index });
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentTabIndex = this.state.currentTabIndex;

      var list = (0, _reactDom.findDOMNode)(this.list);
      if (!list || !list.children || !list.firstChild) {
        return;
      }

      if (currentTabIndex && currentTabIndex >= 0) {
        // $FlowFixMe
        list.children[currentTabIndex].focus();
      } else {
        // $FlowFixMe
        list.firstChild.focus();
      }
    }
  }, {
    key: 'resetTabIndex',
    value: function resetTabIndex() {
      var list = (0, _reactDom.findDOMNode)(this.list);
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(list));
      // $FlowFixMe
      var items = [].concat((0, _toConsumableArray3.default)(list.children));
      var currentFocusIndex = items.indexOf(currentFocus);

      if (currentFocusIndex !== -1) {
        return this.setTabIndex(currentFocusIndex);
      }

      if (this.selectedItem) {
        return this.setTabIndex(items.indexOf((0, _reactDom.findDOMNode)(this.selectedItem)));
      }

      return this.setTabIndex(0);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          children = _props.children,
          className = _props.className,
          onBlur = _props.onBlur,
          onKeyDown = _props.onKeyDown,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'className', 'onBlur', 'onKeyDown']);


      return _react2.default.createElement(
        _List2.default,
        (0, _extends3.default)({
          role: 'menu',
          rootRef: function rootRef(node) {
            _this2.list = node;
          },
          className: className,
          onKeyDown: this.handleKeyDown,
          onBlur: this.handleBlur
        }, other),
        _react2.default.Children.map(children, function (child, index) {
          if (!_react2.default.isValidElement(child)) {
            return null;
          }

          return _react2.default.cloneElement(child, {
            tabIndex: index === _this2.state.currentTabIndex ? 0 : -1,
            ref: child.props.selected ? function (node) {
              _this2.selectedItem = node;
            } : undefined,
            onFocus: _this2.handleItemFocus
          });
        })
      );
    }
  }]);
  return MenuList;
}(_react2.default.Component);

MenuList.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  onBlur: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,
  onKeyDown: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
} : {};
exports.default = MenuList;

/***/ }),

/***/ "./node_modules/material-ui/Menu/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Menu/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Menu = __webpack_require__(/*! ./Menu */ "./node_modules/material-ui/Menu/Menu.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Menu).default;
  }
});

var _MenuList = __webpack_require__(/*! ./MenuList */ "./node_modules/material-ui/Menu/MenuList.js");

Object.defineProperty(exports, 'MenuList', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_MenuList).default;
  }
});

var _MenuItem = __webpack_require__(/*! ./MenuItem */ "./node_modules/material-ui/Menu/MenuItem.js");

Object.defineProperty(exports, 'MenuItem', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_MenuItem).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/Backdrop.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui/Modal/Backdrop.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      zIndex: -1,
      width: '100%',
      height: '100%',
      position: 'fixed',
      top: 0,
      left: 0,
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      backgroundColor: theme.palette.common.lightBlack,
      transition: theme.transitions.create('opacity'),
      willChange: 'opacity',
      opacity: 0
    },
    invisible: {
      backgroundColor: theme.palette.common.transparent
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Can be used, for instance, to render a letter inside the avatar.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the backdrop is invisible.
   */
  invisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * @ignore - internal component.
 */
var Backdrop = function (_React$Component) {
  (0, _inherits3.default)(Backdrop, _React$Component);

  function Backdrop() {
    (0, _classCallCheck3.default)(this, Backdrop);
    return (0, _possibleConstructorReturn3.default)(this, (Backdrop.__proto__ || (0, _getPrototypeOf2.default)(Backdrop)).apply(this, arguments));
  }

  (0, _createClass3.default)(Backdrop, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          invisible = _props.invisible,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'invisible']);


      var backdropClass = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.invisible, invisible), className);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: backdropClass, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Backdrop;
}(_react2.default.Component);

Backdrop.defaultProps = {
  invisible: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBackdrop' })(Backdrop);

/***/ }),

/***/ "./node_modules/material-ui/Modal/Modal.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/Modal.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _addEventListener = __webpack_require__(/*! ../utils/addEventListener */ "./node_modules/material-ui/utils/addEventListener.js");

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _modalManager = __webpack_require__(/*! ./modalManager */ "./node_modules/material-ui/Modal/modalManager.js");

var _modalManager2 = _interopRequireDefault(_modalManager);

var _Backdrop = __webpack_require__(/*! ./Backdrop */ "./node_modules/material-ui/Modal/Backdrop.js");

var _Backdrop2 = _interopRequireDefault(_Backdrop);

var _Portal = __webpack_require__(/*! ../internal/Portal */ "./node_modules/material-ui/internal/Portal.js");

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// Modals don't open on the server so this won't break concurrency.
// Could also put this on context.
var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var modalManager = (0, _modalManager2.default)();

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      position: 'fixed',
      zIndex: theme.zIndex.dialog,
      top: 0,
      left: 0
    },
    hidden: {
      visibility: 'hidden'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the backdrop element.
   */
  BackdropClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Pass a component class to use as the backdrop.
   */
  BackdropComponent: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, the backdrop is invisible.
   */
  BackdropInvisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the backdrop transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  BackdropTransitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Always keep the children in the DOM.
   * This property can be useful in SEO situation or
   * when you want to maximize the responsiveness of the Modal.
   */
  keepMounted: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the backdrop is disabled.
   */
  disableBackdrop: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  modalManager: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * Callback fires when the backdrop is clicked on.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is pressed and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Modal is visible.
   */
  show: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * The modal component provides a solid foundation for creating dialogs,
 * popovers, or whatever else.
 * The component renders its `children` node in front of a backdrop component.
 *
 * The `Modal` offers a few helpful features over using just a `Portal` component and some styles:
 * - Manages dialog stacking when one-at-a-time just isn't enough.
 * - Creates a backdrop, for disabling interaction below the modal.
 * - It properly manages focus; moving to the modal content,
 *   and keeping it there until the modal is closed.
 * - It disables scrolling of the page content while open.
 * - Adds the appropriate ARIA roles are automatically.
 *
 * This component shares many concepts with [react-overlays](https://react-bootstrap.github.io/react-overlays/#modals).
 */
var Modal = function (_React$Component) {
  (0, _inherits3.default)(Modal, _React$Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call.apply(_ref, [this].concat(args))), _this), _initialiseProps.call(_this), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Modal, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (!this.props.show) {
        this.setState({ exited: true });
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.mounted = true;
      if (this.props.show) {
        this.handleShow();
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.show && this.state.exited) {
        this.setState({ exited: false });
      }
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps) {
      if (!this.props.show && nextProps.show) {
        this.checkForFocus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.show && this.props.show) {
        this.handleShow();
      }
      // We are waiting for the onExited callback to call handleHide.
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.props.show || !this.state.exited) {
        this.handleHide();
      }
      this.mounted = false;
    }
  }, {
    key: 'checkForFocus',
    value: function checkForFocus() {
      if (_inDOM2.default) {
        this.lastFocus = (0, _activeElement2.default)();
      }
    }
  }, {
    key: 'restoreLastFocus',
    value: function restoreLastFocus() {
      if (this.lastFocus && this.lastFocus.focus) {
        this.lastFocus.focus();
        this.lastFocus = undefined;
      }
    }
  }, {
    key: 'handleShow',
    value: function handleShow() {
      var doc = (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this));
      this.props.modalManager.add(this);
      this.onDocumentKeyUpListener = (0, _addEventListener2.default)(doc, 'keyup', this.handleDocumentKeyUp);
      this.onFocusListener = (0, _addEventListener2.default)(doc, 'focus', this.handleFocusListener, true);
      this.focus();
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this)));
      var modalContent = this.modal && this.modal.lastChild;
      var focusInModal = currentFocus && (0, _contains2.default)(modalContent, currentFocus);

      if (modalContent && !focusInModal) {
        if (!modalContent.hasAttribute('tabIndex')) {
          modalContent.setAttribute('tabIndex', -1);
           true ? (0, _warning2.default)(false, 'Material-UI: the modal content node does not accept focus. ' + 'For the benefit of assistive technologies, ' + 'the tabIndex of the node is being set to "-1".') : void 0;
        }

        modalContent.focus();
      }
    }
  }, {
    key: 'handleHide',
    value: function handleHide() {
      this.props.modalManager.remove(this);
      if (this.onDocumentKeyUpListener) this.onDocumentKeyUpListener.remove();
      if (this.onFocusListener) this.onFocusListener.remove();
      this.restoreLastFocus();
    }
  }, {
    key: 'renderBackdrop',
    value: function renderBackdrop() {
      var other = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var _props = this.props,
          BackdropComponent = _props.BackdropComponent,
          BackdropClassName = _props.BackdropClassName,
          BackdropTransitionDuration = _props.BackdropTransitionDuration,
          BackdropInvisible = _props.BackdropInvisible,
          show = _props.show;


      return _react2.default.createElement(
        _Fade2.default,
        (0, _extends3.default)({ appear: true, 'in': show, timeout: BackdropTransitionDuration }, other),
        _react2.default.createElement(BackdropComponent, {
          invisible: BackdropInvisible,
          className: BackdropClassName,
          onClick: this.handleBackdropClick
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          disableBackdrop = _props2.disableBackdrop,
          BackdropComponent = _props2.BackdropComponent,
          BackdropClassName = _props2.BackdropClassName,
          BackdropTransitionDuration = _props2.BackdropTransitionDuration,
          BackdropInvisible = _props2.BackdropInvisible,
          ignoreBackdropClick = _props2.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props2.ignoreEscapeKeyUp,
          children = _props2.children,
          classes = _props2.classes,
          className = _props2.className,
          keepMounted = _props2.keepMounted,
          modalManagerProp = _props2.modalManager,
          onBackdropClick = _props2.onBackdropClick,
          onEscapeKeyUp = _props2.onEscapeKeyUp,
          onRequestClose = _props2.onRequestClose,
          onEnter = _props2.onEnter,
          onEntering = _props2.onEntering,
          onEntered = _props2.onEntered,
          onExit = _props2.onExit,
          onExiting = _props2.onExiting,
          onExited = _props2.onExited,
          show = _props2.show,
          other = (0, _objectWithoutProperties3.default)(_props2, ['disableBackdrop', 'BackdropComponent', 'BackdropClassName', 'BackdropTransitionDuration', 'BackdropInvisible', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'children', 'classes', 'className', 'keepMounted', 'modalManager', 'onBackdropClick', 'onEscapeKeyUp', 'onRequestClose', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'show']);


      if (!keepMounted && !show && this.state.exited) {
        return null;
      }

      var transitionCallbacks = {
        onEnter: onEnter,
        onEntering: onEntering,
        onEntered: onEntered,
        onExit: onExit,
        onExiting: onExiting,
        onExited: this.handleTransitionExited
      };

      var modalChild = _react2.default.Children.only(children);
      var _modalChild$props = modalChild.props,
          role = _modalChild$props.role,
          tabIndex = _modalChild$props.tabIndex;

      var childProps = {};

      if (role === undefined) {
        childProps.role = role === undefined ? 'document' : role;
      }

      if (tabIndex === undefined) {
        childProps.tabIndex = tabIndex == null ? -1 : tabIndex;
      }

      var backdropProps = void 0;

      // It's a Transition like component
      if (modalChild.props.hasOwnProperty('in')) {
        (0, _keys2.default)(transitionCallbacks).forEach(function (key) {
          childProps[key] = (0, _helpers.createChainedFunction)(transitionCallbacks[key], modalChild.props[key]);
        });
      } else {
        backdropProps = transitionCallbacks;
      }

      if ((0, _keys2.default)(childProps).length) {
        modalChild = _react2.default.cloneElement(modalChild, childProps);
      }

      return _react2.default.createElement(
        _Portal2.default,
        {
          open: true,
          ref: function ref(node) {
            _this2.mountNode = node ? node.getLayer() : null;
          }
        },
        _react2.default.createElement(
          'div',
          (0, _extends3.default)({
            className: (0, _classnames2.default)(classes.root, className, (0, _defineProperty3.default)({}, classes.hidden, this.state.exited))
          }, other, {
            ref: function ref(node) {
              _this2.modal = node;
            }
          }),
          !disableBackdrop && (!keepMounted || show || !this.state.exited) && this.renderBackdrop(backdropProps),
          modalChild
        )
      );
    }
  }]);
  return Modal;
}(_react2.default.Component);

Modal.defaultProps = {
  BackdropComponent: _Backdrop2.default,
  BackdropTransitionDuration: 300,
  BackdropInvisible: false,
  keepMounted: false,
  disableBackdrop: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  modalManager: modalManager
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.state = {
    exited: false
  };
  this.onDocumentKeyUpListener = null;
  this.onFocusListener = null;
  this.mounted = false;
  this.lastFocus = undefined;
  this.modal = null;
  this.mountNode = null;

  this.handleFocusListener = function () {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(_this3)));
    var modalContent = _this3.modal && _this3.modal.lastChild;

    if (modalContent && modalContent !== currentFocus && !(0, _contains2.default)(modalContent, currentFocus)) {
      modalContent.focus();
    }
  };

  this.handleDocumentKeyUp = function (event) {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    if ((0, _keycode2.default)(event) !== 'esc') {
      return;
    }

    var _props3 = _this3.props,
        onEscapeKeyUp = _props3.onEscapeKeyUp,
        onRequestClose = _props3.onRequestClose,
        ignoreEscapeKeyUp = _props3.ignoreEscapeKeyUp;


    if (onEscapeKeyUp) {
      onEscapeKeyUp(event);
    }

    if (onRequestClose && !ignoreEscapeKeyUp) {
      onRequestClose(event);
    }
  };

  this.handleBackdropClick = function (event) {
    if (event.target !== event.currentTarget) {
      return;
    }

    var _props4 = _this3.props,
        onBackdropClick = _props4.onBackdropClick,
        onRequestClose = _props4.onRequestClose,
        ignoreBackdropClick = _props4.ignoreBackdropClick;


    if (onBackdropClick) {
      onBackdropClick(event);
    }

    if (onRequestClose && !ignoreBackdropClick) {
      onRequestClose(event);
    }
  };

  this.handleTransitionExited = function () {
    if (_this3.props.onExited) {
      var _props5;

      (_props5 = _this3.props).onExited.apply(_props5, arguments);
    }

    _this3.setState({ exited: true });
    _this3.handleHide();
  };
};

exports.default = (0, _withStyles2.default)(styles, { flip: false, name: 'MuiModal' })(Modal);

/***/ }),

/***/ "./node_modules/material-ui/Modal/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Modal = __webpack_require__(/*! ./Modal */ "./node_modules/material-ui/Modal/Modal.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Modal).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/modalManager.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Modal/modalManager.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _isWindow = __webpack_require__(/*! dom-helpers/query/isWindow */ "./node_modules/dom-helpers/query/isWindow.js");

var _isWindow2 = _interopRequireDefault(_isWindow);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _manageAriaHidden = __webpack_require__(/*! ../utils/manageAriaHidden */ "./node_modules/material-ui/utils/manageAriaHidden.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Taken from https://github.com/react-bootstrap/react-overlays/blob/master/src/ModalManager.js

function getPaddingRight(node) {
  return parseInt(node.style.paddingRight || 0, 10);
}

// Do we have a scroll bar?
function bodyIsOverflowing(node) {
  var doc = (0, _ownerDocument2.default)(node);
  var win = (0, _isWindow2.default)(doc);

  // Takes in account potential non zero margin on the body.
  var style = window.getComputedStyle(doc.body);
  var marginLeft = parseInt(style.getPropertyValue('margin-left'), 10);
  var marginRight = parseInt(style.getPropertyValue('margin-right'), 10);

  return marginLeft + doc.body.clientWidth + marginRight < win.innerWidth;
}

function getContainer() {
  var container = _inDOM2.default ? window.document.body : {};
   true ? (0, _warning2.default)(container !== null, '\nMaterial-UI: you are most likely evaluating the code before the\nbrowser has a chance to reach the <body>.\nPlease move the import at the end of the <body>.\n  ') : void 0;
  return container;
}
/**
 * State management helper for modals/layers.
 * Simplified, but inspired by react-overlay's ModalManager class
 *
 * @internal Used by the Modal to ensure proper focus management.
 */
function createModalManager() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$hideSiblingNodes = _ref.hideSiblingNodes,
      hideSiblingNodes = _ref$hideSiblingNodes === undefined ? true : _ref$hideSiblingNodes;

  var modals = [];

  var prevOverflow = void 0;
  var prevPaddings = [];

  function add(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx !== -1) {
      return modalIdx;
    }

    modalIdx = modals.length;
    modals.push(modal);

    if (hideSiblingNodes) {
      (0, _manageAriaHidden.hideSiblings)(container, modal.mountNode);
    }

    if (modals.length === 1) {
      // Save our current overflow so we can revert
      // back to it when all modals are closed!
      prevOverflow = container.style.overflow;

      if (bodyIsOverflowing(container)) {
        prevPaddings = [getPaddingRight(container)];
        var scrollbarSize = (0, _scrollbarSize2.default)();
        container.style.paddingRight = prevPaddings[0] + scrollbarSize + 'px';

        var fixedNodes = document.querySelectorAll('.mui-fixed');
        for (var i = 0; i < fixedNodes.length; i += 1) {
          var paddingRight = getPaddingRight(fixedNodes[i]);
          prevPaddings.push(paddingRight);
          fixedNodes[i].style.paddingRight = paddingRight + scrollbarSize + 'px';
        }
      }

      container.style.overflow = 'hidden';
    }

    return modalIdx;
  }

  function remove(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx === -1) {
      return modalIdx;
    }

    modals.splice(modalIdx, 1);

    if (modals.length === 0) {
      container.style.overflow = prevOverflow;
      container.style.paddingRight = prevPaddings[0];

      var fixedNodes = document.querySelectorAll('.mui-fixed');
      for (var i = 0; i < fixedNodes.length; i += 1) {
        fixedNodes[i].style.paddingRight = prevPaddings[i + 1] + 'px';
      }

      prevOverflow = undefined;
      prevPaddings = [];
      if (hideSiblingNodes) {
        (0, _manageAriaHidden.showSiblings)(container, modal.mountNode);
      }
    } else if (hideSiblingNodes) {
      // otherwise make sure the next top modal is visible to a SR
      (0, _manageAriaHidden.ariaHidden)(false, modals[modals.length - 1].mountNode);
    }

    return modalIdx;
  }

  function isTopModal(modal) {
    return !!modals.length && modals[modals.length - 1] === modal;
  }

  var modalManager = { add: add, remove: remove, isTopModal: isTopModal };

  return modalManager;
}

exports.default = createModalManager;

/***/ }),

/***/ "./node_modules/material-ui/Popover/Popover.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/Popover/Popover.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _debounce = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");

var _debounce2 = _interopRequireDefault(_debounce);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Modal = __webpack_require__(/*! ../Modal */ "./node_modules/material-ui/Modal/index.js");

var _Modal2 = _interopRequireDefault(_Modal);

var _Grow = __webpack_require__(/*! ../transitions/Grow */ "./node_modules/material-ui/transitions/Grow.js");

var _Grow2 = _interopRequireDefault(_Grow);

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Modal

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionClasses || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

function getOffsetTop(rect, vertical) {
  var offset = 0;

  if (typeof vertical === 'number') {
    offset = vertical;
  } else if (vertical === 'center') {
    offset = rect.height / 2;
  } else if (vertical === 'bottom') {
    offset = rect.height;
  }

  return offset;
}

function getOffsetLeft(rect, horizontal) {
  var offset = 0;

  if (typeof horizontal === 'number') {
    offset = horizontal;
  } else if (horizontal === 'center') {
    offset = rect.width / 2;
  } else if (horizontal === 'right') {
    offset = rect.width;
  }

  return offset;
}

function getTransformOriginValue(transformOrigin) {
  return [transformOrigin.horizontal, transformOrigin.vertical].map(function (n) {
    return typeof n === 'number' ? n + 'px' : n;
  }).join(' ');
}

// Sum the scrollTop between two elements.
function getScrollParent(parent, child) {
  var element = child;
  var scrollTop = 0;

  while (element && element !== parent) {
    element = element.parentNode;
    scrollTop += element.scrollTop;
  }
  return scrollTop;
}

var styles = exports.styles = {
  paper: {
    position: 'absolute',
    overflowY: 'auto',
    overflowX: 'hidden',
    // So we see the popover when it's empty.
    // It's most likely on issue on userland.
    minWidth: 16,
    minHeight: 16,
    maxWidth: 'calc(100vw - 32px)',
    maxHeight: 'calc(100vh - 32px)',
    '&:focus': {
      outline: 'none'
    }
  }
};

var babelPluginFlowReactPropTypes_proptype_Origin = {
  horizontal: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['right']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired,
  vertical: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['top']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['bottom']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired
};
var babelPluginFlowReactPropTypes_proptype_Position = {
  top: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
};
var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * This is the DOM element that may be used
   * to set the position of the popover.
   */
  anchorEl: typeof HTMLElement === 'function' ? __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").instanceOf(HTMLElement) : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any,

  /**
   * This is the position that may be used
   * to set the position of the popover.
   * The coordinates are relative to
   * the application's client area.
   */
  anchorPosition: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    top: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
    left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
  }),

  /*
   * This determines which anchor prop to refer to to set
   * the position of the popover.
   */
  anchorReference: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['anchorEl', 'anchorPosition']),

  /**
   * This is the point on the anchor where the popover's
   * `anchorEl` will attach to. This is not used when the
   * anchorReference is 'anchorPosition'.
   *
   * Options:
   * vertical: [top, center, bottom];
   * horizontal: [left, center, right].
   */
  anchorOrigin: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    horizontal: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['right']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired,
    vertical: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['top']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['bottom']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired
  }).isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The elevation of the popover.
   */
  elevation: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,

  /**
   * This function is called in order to retrieve the content anchor element.
   * It's the opposite of the `anchorEl` property.
   * The content anchor element should be an element inside the popover.
   * It's used to correctly scroll and set the position of the popover.
   * The positioning strategy tries to make the content anchor element just above the
   * anchor element.
   */
  getContentAnchorEl: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Specifies how close to the edge of the window the popover can appear.
   */
  marginThreshold: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,

  /**
   * Callback fired before the component is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired before the component is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback.
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the popover is visible.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Properties applied to the `Paper` element.
   */
  PaperProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  role: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * This is the point on the popover which
   * will attach to the anchor's origin.
   *
   * Options:
   * vertical: [top, center, bottom, x(px)];
   * horizontal: [left, center, right, x(px)].
   */
  transformOrigin: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    horizontal: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['right']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired,
    vertical: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['top']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['center']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['bottom']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number]).isRequired
  }).isRequired,

  /**
   * The animation classNames applied to the component as it enters or exits.
   * This property is a direct binding to [`CSSTransition.classNames`](https://reactcommunity.org/react-transition-group/#CSSTransition-prop-classNames).
   */
  transitionClasses: typeof babelPluginFlowReactPropTypes_proptype_TransitionClasses === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionClasses : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionClasses),

  /**
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  transitionDuration: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])])
};

var Popover = function (_React$Component) {
  (0, _inherits3.default)(Popover, _React$Component);

  function Popover() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Popover);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Popover.__proto__ || (0, _getPrototypeOf2.default)(Popover)).call.apply(_ref, [this].concat(args))), _this), _this.componentWillUnmount = function () {
      _this.handleResize.cancel();
    }, _this.setPositioningStyles = function (element) {
      if (element && element.style) {
        var positioning = _this.getPositioningStyle(element);

        element.style.top = positioning.top;
        element.style.left = positioning.left;
        element.style.transformOrigin = positioning.transformOrigin;
      }
    }, _this.getPositioningStyle = function (element) {
      var marginThreshold = _this.props.marginThreshold;

      // Check if the parent has requested anchoring on an inner content node

      var contentAnchorOffset = _this.getContentAnchorOffset(element);
      // Get the offset of of the anchoring element
      var anchorOffset = _this.getAnchorOffset(contentAnchorOffset);

      var elemRect = {
        width: element.clientWidth,
        height: element.clientHeight
      };
      // Get the transform origin point on the element itself
      var transformOrigin = _this.getTransformOrigin(elemRect, contentAnchorOffset);

      // Calculate element positioning
      var top = anchorOffset.top - transformOrigin.vertical;
      var left = anchorOffset.left - transformOrigin.horizontal;
      var bottom = top + elemRect.height;
      var right = left + elemRect.width;

      // Window thresholds taking required margin into account
      var heightThreshold = window.innerHeight - marginThreshold;
      var widthThreshold = window.innerWidth - marginThreshold;

      // Check if the vertical axis needs shifting
      if (top < marginThreshold) {
        var diff = top - marginThreshold;
        top -= diff;
        transformOrigin.vertical += diff;
      } else if (bottom > heightThreshold) {
        var _diff = bottom - heightThreshold;
        top -= _diff;
        transformOrigin.vertical += _diff;
      }

       true ? (0, _warning2.default)(elemRect.height < heightThreshold || !elemRect.height || !heightThreshold, ['Material-UI: the popover component is too tall.', 'Some part of it can not be seen on the screen (' + (elemRect.height - heightThreshold) + 'px).', 'Please consider adding a `max-height` to improve the user-experience.'].join('\n')) : void 0;

      // Check if the horizontal axis needs shifting
      if (left < marginThreshold) {
        var _diff2 = left - marginThreshold;
        left -= _diff2;
        transformOrigin.horizontal += _diff2;
      } else if (right > widthThreshold) {
        var _diff3 = right - widthThreshold;
        left -= _diff3;
        transformOrigin.horizontal += _diff3;
      }

      return {
        top: top + 'px',
        left: left + 'px',
        transformOrigin: getTransformOriginValue(transformOrigin)
      };
    }, _this.transitionEl = undefined, _this.handleGetOffsetTop = getOffsetTop, _this.handleGetOffsetLeft = getOffsetLeft, _this.handleEnter = function (element) {
      if (_this.props.onEnter) {
        _this.props.onEnter(element);
      }

      _this.setPositioningStyles(element);
    }, _this.handleResize = (0, _debounce2.default)(function () {
      var element = _reactDom2.default.findDOMNode(_this.transitionEl);
      _this.setPositioningStyles(element);
    }, 166), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Popover, [{
    key: 'getAnchorOffset',


    // Returns the top/left offset of the position
    // to attach to on the anchor element (or body if none is provided)
    value: function getAnchorOffset(contentAnchorOffset) {
      // $FlowFixMe
      var _props = this.props,
          anchorEl = _props.anchorEl,
          anchorOrigin = _props.anchorOrigin,
          anchorReference = _props.anchorReference,
          anchorPosition = _props.anchorPosition;


      if (anchorReference === 'anchorPosition') {
        return anchorPosition;
      }

      var anchorElement = anchorEl || document.body;
      var anchorRect = anchorElement.getBoundingClientRect();
      var anchorVertical = contentAnchorOffset === 0 ? anchorOrigin.vertical : 'center';

      return {
        top: anchorRect.top + this.handleGetOffsetTop(anchorRect, anchorVertical),
        left: anchorRect.left + this.handleGetOffsetLeft(anchorRect, anchorOrigin.horizontal)
      };
    }

    // Returns the vertical offset of inner content to anchor the transform on if provided

  }, {
    key: 'getContentAnchorOffset',
    value: function getContentAnchorOffset(element) {
      var _props2 = this.props,
          getContentAnchorEl = _props2.getContentAnchorEl,
          anchorReference = _props2.anchorReference;

      var contentAnchorOffset = 0;

      if (getContentAnchorEl && anchorReference === 'anchorEl') {
        var contentAnchorEl = getContentAnchorEl(element);

        if (contentAnchorEl && (0, _contains2.default)(element, contentAnchorEl)) {
          var scrollTop = getScrollParent(element, contentAnchorEl);
          contentAnchorOffset = contentAnchorEl.offsetTop + contentAnchorEl.clientHeight / 2 - scrollTop || 0;
        }

        // != the default value
         true ? (0, _warning2.default)(this.props.anchorOrigin.vertical === 'top', ['Material-UI: you can not change the default `anchorOrigin.vertical` value when also ', 'providing the `getContentAnchorEl` property to the popover component.', 'Only use one of the two properties', 'Set `getContentAnchorEl` to null or left `anchorOrigin.vertical` unchanged'].join()) : void 0;
      }

      return contentAnchorOffset;
    }

    // Return the base transform origin using the element
    // and taking the content anchor offset into account if in use

  }, {
    key: 'getTransformOrigin',
    value: function getTransformOrigin(elemRect) {
      var contentAnchorOffset = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var transformOrigin = this.props.transformOrigin;

      return {
        vertical: this.handleGetOffsetTop(elemRect, transformOrigin.vertical) + contentAnchorOffset,
        horizontal: this.handleGetOffsetLeft(elemRect, transformOrigin.horizontal)
      };
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props3 = this.props,
          anchorEl = _props3.anchorEl,
          anchorReference = _props3.anchorReference,
          anchorPosition = _props3.anchorPosition,
          anchorOrigin = _props3.anchorOrigin,
          children = _props3.children,
          classes = _props3.classes,
          elevation = _props3.elevation,
          getContentAnchorEl = _props3.getContentAnchorEl,
          marginThreshold = _props3.marginThreshold,
          onEnter = _props3.onEnter,
          onEntering = _props3.onEntering,
          onEntered = _props3.onEntered,
          onExit = _props3.onExit,
          onExiting = _props3.onExiting,
          onExited = _props3.onExited,
          open = _props3.open,
          PaperProps = _props3.PaperProps,
          role = _props3.role,
          transformOrigin = _props3.transformOrigin,
          transitionClasses = _props3.transitionClasses,
          transitionDuration = _props3.transitionDuration,
          other = (0, _objectWithoutProperties3.default)(_props3, ['anchorEl', 'anchorReference', 'anchorPosition', 'anchorOrigin', 'children', 'classes', 'elevation', 'getContentAnchorEl', 'marginThreshold', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'open', 'PaperProps', 'role', 'transformOrigin', 'transitionClasses', 'transitionDuration']);


      return _react2.default.createElement(
        _Modal2.default,
        (0, _extends3.default)({ show: open, BackdropInvisible: true }, other),
        _react2.default.createElement(
          _Grow2.default,
          {
            appear: true,
            'in': open,
            onEnter: this.handleEnter,
            onEntering: onEntering,
            onEntered: onEntered,
            onExit: onExit,
            onExiting: onExiting,
            onExited: onExited,
            role: role,
            transitionClasses: transitionClasses,
            timeout: transitionDuration,
            rootRef: function rootRef(node) {
              _this2.transitionEl = node;
            }
          },
          _react2.default.createElement(
            _Paper2.default,
            (0, _extends3.default)({
              className: classes.paper,
              elevation: elevation
            }, PaperProps),
            _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }),
            children
          )
        )
      );
    }
  }]);
  return Popover;
}(_react2.default.Component);

Popover.defaultProps = {
  anchorReference: 'anchorEl',
  anchorOrigin: {
    vertical: 'top',
    horizontal: 'left'
  },
  transformOrigin: {
    vertical: 'top',
    horizontal: 'left'
  },
  transitionDuration: 'auto',
  elevation: 8,
  marginThreshold: 16
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiPopover' })(Popover);

/***/ }),

/***/ "./node_modules/material-ui/Popover/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Popover/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Popover = __webpack_require__(/*! ./Popover */ "./node_modules/material-ui/Popover/Popover.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Popover).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/internal/Portal.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/internal/Portal.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content to portal in order to escape the parent DOM node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * If `true` the children will be mounted into the DOM.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

/**
 * @ignore - internal component.
 */
var Portal = function (_React$Component) {
  (0, _inherits3.default)(Portal, _React$Component);

  function Portal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Portal.__proto__ || (0, _getPrototypeOf2.default)(Portal)).call.apply(_ref, [this].concat(args))), _this), _this.layer = null, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Portal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unrenderLayer();
    }
  }, {
    key: 'getLayer',
    value: function getLayer() {
      if (!this.layer) {
        this.layer = document.createElement('div');
        this.layer.setAttribute('data-mui-portal', 'true');
        if (document.body && this.layer) {
          document.body.appendChild(this.layer);
        }
      }

      return this.layer;
    }
  }, {
    key: 'unrenderLayer',
    value: function unrenderLayer() {
      if (!this.layer) {
        return;
      }

      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        _reactDom2.default.unmountComponentAtNode(this.layer);
      }

      if (document.body) {
        document.body.removeChild(this.layer);
      }
      this.layer = null;
    }
  }, {
    key: 'renderLayer',
    value: function renderLayer() {
      var _props = this.props,
          children = _props.children,
          open = _props.open;


      if (open) {
        // By calling this method in componentDidMount() and
        // componentDidUpdate(), you're effectively creating a "wormhole" that
        // funnels React's hierarchical updates through to a DOM node on an
        // entirely different part of the page.
        var layerElement = _react2.default.Children.only(children);
        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, layerElement, this.getLayer());
      } else {
        this.unrenderLayer();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          children = _props2.children,
          open = _props2.open;

      // Support react@15.x, will be removed at some point

      if (!_reactDom2.default.createPortal) {
        return null;
      }

      // Can't be rendered server-side.
      if (_inDOM2.default) {
        if (open) {
          var layer = this.getLayer();
          // $FlowFixMe layer is non-null
          return _reactDom2.default.createPortal(children, layer);
        }

        this.unrenderLayer();
      }

      return null;
    }
  }]);
  return Portal;
}(_react2.default.Component);

Portal.defaultProps = {
  open: false
};
Portal.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
} : {};
exports.default = Portal;

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Fade.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Fade.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  timeout: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired
};


var reflow = function reflow(node) {
  return node.scrollTop;
};

/**
 * The Fade transition is used by the Modal component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */

var Fade = function (_React$Component) {
  (0, _inherits3.default)(Fade, _React$Component);

  function Fade() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Fade);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Fade.__proto__ || (0, _getPrototypeOf2.default)(Fade)).call.apply(_ref, [this].concat(args))), _this), _this.handleEnter = function (node) {
      node.style.opacity = '0';
      reflow(node);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      node.style.opacity = '1';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      node.style.opacity = '0';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Fade, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          styleProp = _props.style,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'style', 'theme']);


      var style = (0, _extends3.default)({}, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          style: style,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit
        }, other),
        children
      );
    }
  }]);
  return Fade;
}(_react2.default.Component);

Fade.defaultProps = {
  appear: true,
  timeout: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  }
};
exports.default = (0, _withTheme2.default)()(Fade);

/***/ }),

/***/ "./node_modules/material-ui/transitions/Grow.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Grow.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

exports.getScale = getScale;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _CSSTransition = __webpack_require__(/*! react-transition-group/CSSTransition */ "./node_modules/react-transition-group/CSSTransition.js");

var _CSSTransition2 = _interopRequireDefault(_CSSTransition);

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent CSSTransition

// Only exported for tests.
var babelPluginFlowReactPropTypes_proptype_TransitionClasses = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionClasses || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

function getScale(value) {
  return 'scale(' + value + ', ' + Math.pow(value, 2) + ')';
}

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, show the component; triggers the enter or exit animation.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The animation classNames applied to the component as it enters or exits.
   * This property is a direct binding to [`CSSTransition.classNames`](https://reactcommunity.org/react-transition-group/#CSSTransition-prop-classNames).
   */
  transitionClasses: typeof babelPluginFlowReactPropTypes_proptype_TransitionClasses === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionClasses.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionClasses.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionClasses : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionClasses).isRequired,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   *
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  timeout: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]).isRequired
};

/**
 * The Grow transition is used by the Popover component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */
var Grow = function (_React$Component) {
  (0, _inherits3.default)(Grow, _React$Component);

  function Grow() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Grow);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Grow.__proto__ || (0, _getPrototypeOf2.default)(Grow)).call.apply(_ref, [this].concat(args))), _this), _this.autoTimeout = undefined, _this.handleEnter = function (node) {
      node.style.opacity = '0';
      node.style.transform = getScale(0.75);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      var duration = 0;

      if (timeout === 'auto') {
        duration = theme.transitions.getAutoHeightDuration(node.clientHeight);
        _this.autoTimeout = duration;
      } else if (typeof timeout === 'number') {
        duration = timeout;
      } else if (timeout && typeof timeout.enter === 'number') {
        duration = timeout.enter;
      } else {
        // The propType will warn in this case.
      }

      node.style.transition = [theme.transitions.create('opacity', {
        duration: duration
      }), theme.transitions.create('transform', {
        duration: duration * 0.666
      })].join(',');

      node.style.opacity = '1';
      node.style.transform = getScale(1);

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      var duration = 0;

      if (timeout === 'auto') {
        duration = theme.transitions.getAutoHeightDuration(node.clientHeight);
        _this.autoTimeout = duration;
      } else if (typeof timeout === 'number') {
        duration = timeout;
      } else if (timeout && typeof timeout.exit === 'number') {
        duration = timeout.exit;
      } else {
        // The propType will warn in this case.
      }

      node.style.transition = [theme.transitions.create('opacity', {
        duration: duration
      }), theme.transitions.create('transform', {
        duration: duration * 0.666,
        delay: duration * 0.333
      })].join(',');

      node.style.opacity = '0';
      node.style.transform = getScale(0.75);

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _this.addEndListener = function (node, next) {
      if (_this.props.timeout === 'auto') {
        setTimeout(next, _this.autoTimeout || 0);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Grow, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          rootRef = _props.rootRef,
          styleProp = _props.style,
          transitionClasses = _props.transitionClasses,
          timeout = _props.timeout,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'rootRef', 'style', 'transitionClasses', 'timeout', 'theme']);


      var style = (0, _extends3.default)({}, children.props.style, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _CSSTransition2.default,
        (0, _extends3.default)({
          classNames: transitionClasses,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit,
          addEndListener: this.addEndListener,
          appear: appear,
          style: style,
          timeout: timeout === 'auto' ? null : timeout
        }, other, {
          ref: rootRef
        }),
        children
      );
    }
  }]);
  return Grow;
}(_react2.default.Component);

Grow.defaultProps = {
  appear: true,
  timeout: 'auto',
  transitionClasses: {}
};
exports.default = (0, _withTheme2.default)()(Grow);

/***/ }),

/***/ "./node_modules/material-ui/utils/manageAriaHidden.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui/utils/manageAriaHidden.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ariaHidden = ariaHidden;
exports.hideSiblings = hideSiblings;
exports.showSiblings = showSiblings;
//  weak

var BLACKLIST = ['template', 'script', 'style'];

var isHidable = function isHidable(_ref) {
  var nodeType = _ref.nodeType,
      tagName = _ref.tagName;
  return nodeType === 1 && BLACKLIST.indexOf(tagName.toLowerCase()) === -1;
};

var siblings = function siblings(container, mount, cb) {
  mount = [].concat(mount); // eslint-disable-line no-param-reassign
  [].forEach.call(container.children, function (node) {
    if (mount.indexOf(node) === -1 && isHidable(node)) {
      cb(node);
    }
  });
};

function ariaHidden(show, node) {
  if (!node) {
    return;
  }
  if (show) {
    node.setAttribute('aria-hidden', 'true');
  } else {
    node.removeAttribute('aria-hidden');
  }
}

function hideSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(true, node);
  });
}

function showSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(false, node);
  });
}

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/react-transition-group/CSSTransition.js":
/*!**************************************************************!*\
  !*** ./node_modules/react-transition-group/CSSTransition.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var PropTypes = _interopRequireWildcard(_propTypes);

var _addClass = __webpack_require__(/*! dom-helpers/class/addClass */ "./node_modules/dom-helpers/class/addClass.js");

var _addClass2 = _interopRequireDefault(_addClass);

var _removeClass = __webpack_require__(/*! dom-helpers/class/removeClass */ "./node_modules/dom-helpers/class/removeClass.js");

var _removeClass2 = _interopRequireDefault(_removeClass);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! ./Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _PropTypes = __webpack_require__(/*! ./utils/PropTypes */ "./node_modules/react-transition-group/utils/PropTypes.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var addClass = function addClass(node, classes) {
  return classes && classes.split(' ').forEach(function (c) {
    return (0, _addClass2.default)(node, c);
  });
};
var removeClass = function removeClass(node, classes) {
  return classes && classes.split(' ').forEach(function (c) {
    return (0, _removeClass2.default)(node, c);
  });
};

var propTypes = _extends({}, _Transition2.default.propTypes, {

  /**
   * The animation classNames applied to the component as it enters or exits.
   * A single name can be provided and it will be suffixed for each stage: e.g.
   *
   * `classNames="fade"` applies `fade-enter`, `fade-enter-active`,
   * `fade-exit`, `fade-exit-active`, `fade-appear`, and `fade-appear-active`.
   * Each individual classNames can also be specified independently like:
   *
   * ```js
   * classNames={{
   *  appear: 'my-appear',
   *  appearActive: 'my-active-appear',
   *  enter: 'my-enter',
   *  enterActive: 'my-active-enter',
   *  exit: 'my-exit',
   *  exitActive: 'my-active-exit',
   * }}
   * ```
   *
   * @type {string | {
   *  appear?: string,
   *  appearActive?: string,
   *  enter?: string,
   *  enterActive?: string,
   *  exit?: string,
   *  exitActive?: string,
   * }}
   */
  classNames: _PropTypes.classNamesShape,

  /**
   * A `<Transition>` callback fired immediately after the 'enter' or 'appear' class is
   * applied.
   *
   * @type Function(node: HtmlElement, isAppearing: bool)
   */
  onEnter: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'enter-active' or
   * 'appear-active' class is applied.
   *
   * @type Function(node: HtmlElement, isAppearing: bool)
   */
  onEntering: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'enter' or
   * 'appear' classes are **removed** from the DOM node.
   *
   * @type Function(node: HtmlElement, isAppearing: bool)
   */
  onEntered: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'exit' class is
   * applied.
   *
   * @type Function(node: HtmlElement)
   */
  onExit: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'exit-active' is applied.
   *
   * @type Function(node: HtmlElement
   */
  onExiting: PropTypes.func,

  /**
   * A `<Transition>` callback fired immediately after the 'exit' classes
   * are **removed** from the DOM node.
   *
   * @type Function(node: HtmlElement)
   */
  onExited: PropTypes.func
});

/**
 * A `Transition` component using CSS transitions and animations.
 * It's inspired by the excellent [ng-animate](http://www.nganimate.org/) library.
 *
 * `CSSTransition` applies a pair of class names during the `appear`, `enter`,
 * and `exit` stages of the transition. The first class is applied and then a
 * second "active" class in order to activate the css animation.
 *
 * When the `in` prop is toggled to `true` the Component will get
 * the `example-enter` CSS class and the `example-enter-active` CSS class
 * added in the next tick. This is a convention based on the `classNames` prop.
 *
 * ```js
 * import CSSTransition from 'react-transition-group/CSSTransition';
 *
 * const Fade = ({ children, ...props }) => (
 *  <CSSTransition
 *    {...props}
 *    timeout={500}
 *    classNames="fade"
 *  >
 *   {children}
 *  </CSSTransition>
 * );
 *
 * class FadeInAndOut extends React.Component {
 *   constructor(...args) {
 *     super(...args);
 *     this.state= { show: false }
 *
 *     setInterval(() => {
 *       this.setState({ show: !this.state.show })
 *     }, 5000)
 *   }
 *   render() {
 *     return (
 *       <Fade in={this.state.show}>
 *         <div>Hello world</div>
 *       </Fade>
 *     )
 *   }
 * }
 * ```
 *
 * And the coorresponding CSS for the `<Fade>` component:
 *
 * ```css
 * .fade-enter {
 *   opacity: 0.01;
 * }
 *
 * .fade-enter.fade-enter-active {
 *   opacity: 1;
 *   transition: opacity 500ms ease-in;
 * }
 *
 * .fade-exit {
 *   opacity: 1;
 * }
 *
 * .fade-exit.fade-exit-active {
 *   opacity: 0.01;
 *   transition: opacity 300ms ease-in;
 * }
 * ```
 */

var CSSTransition = function (_React$Component) {
  _inherits(CSSTransition, _React$Component);

  function CSSTransition() {
    var _temp, _this, _ret;

    _classCallCheck(this, CSSTransition);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.onEnter = function (node, appearing) {
      var _this$getClassNames = _this.getClassNames(appearing ? 'appear' : 'enter'),
          className = _this$getClassNames.className;

      _this.removeClasses(node, 'exit');
      addClass(node, className);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.onEntering = function (node, appearing) {
      var _this$getClassNames2 = _this.getClassNames(appearing ? 'appear' : 'enter'),
          activeClassName = _this$getClassNames2.activeClassName;

      _this.reflowAndAddClass(node, activeClassName);

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.onEntered = function (node, appearing) {
      _this.removeClasses(node, appearing ? 'appear' : 'enter');

      if (_this.props.onEntered) {
        _this.props.onEntered(node);
      }
    }, _this.onExit = function (node) {
      var _this$getClassNames3 = _this.getClassNames('exit'),
          className = _this$getClassNames3.className;

      _this.removeClasses(node, 'appear');
      _this.removeClasses(node, 'enter');
      addClass(node, className);

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _this.onExiting = function (node) {
      var _this$getClassNames4 = _this.getClassNames('exit'),
          activeClassName = _this$getClassNames4.activeClassName;

      _this.reflowAndAddClass(node, activeClassName);

      if (_this.props.onExiting) {
        _this.props.onExiting(node);
      }
    }, _this.onExited = function (node) {
      _this.removeClasses(node, 'exit');

      if (_this.props.onExited) {
        _this.props.onExited(node);
      }
    }, _this.getClassNames = function (type) {
      var classNames = _this.props.classNames;


      var className = typeof classNames !== 'string' ? classNames[type] : classNames + '-' + type;

      var activeClassName = typeof classNames !== 'string' ? classNames[type + 'Active'] : className + '-active';

      return { className: className, activeClassName: activeClassName };
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  CSSTransition.prototype.removeClasses = function removeClasses(node, type) {
    var _getClassNames = this.getClassNames(type),
        className = _getClassNames.className,
        activeClassName = _getClassNames.activeClassName;

    className && removeClass(node, className);
    activeClassName && removeClass(node, activeClassName);
  };

  CSSTransition.prototype.reflowAndAddClass = function reflowAndAddClass(node, className) {
    // This is for to force a repaint,
    // which is necessary in order to transition styles when adding a class name.
    /* eslint-disable no-unused-expressions */
    node.scrollTop;
    /* eslint-enable no-unused-expressions */
    addClass(node, className);
  };

  CSSTransition.prototype.render = function render() {
    var props = _extends({}, this.props);

    delete props.classNames;

    return _react2.default.createElement(_Transition2.default, _extends({}, props, {
      onEnter: this.onEnter,
      onEntered: this.onEntered,
      onEntering: this.onEntering,
      onExit: this.onExit,
      onExiting: this.onExiting,
      onExited: this.onExited
    }));
  };

  return CSSTransition;
}(_react2.default.Component);

CSSTransition.propTypes =  true ? propTypes : {};

exports.default = CSSTransition;
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/elements/getById.js":
/*!************************************************!*\
  !*** ./src/client/actions/elements/getById.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

var _getById = __webpack_require__(/*! ../../api/elements/getById */ "./src/client/api/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @return {Object}
 */
/** @format */

var getElementStart = function getElementStart(payload) {
  return { type: _elements.ELEMENT_GET_START, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType
 * @param {string} payload.circleType - only if elementType is 'circle'
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @return {Object}
 */
var getElementSuccess = function getElementSuccess(payload) {
  return { type: _elements.ELEMENT_GET_SUCCESS, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
var getElementError = function getElementError(payload) {
  return { type: _elements.ELEMENT_GET_ERROR, payload: payload };
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
var get = function get(_ref) {
  var token = _ref.token,
      id = _ref.id;
  return function (dispatch) {
    dispatch(getElementStart({ id: id }));

    (0, _getById2.default)({ token: token, id: id }).then(function (_ref2) {
      var statusCode = _ref2.statusCode,
          error = _ref2.error,
          payload = _ref2.payload;

      if (statusCode >= 300) {
        dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));

        return;
      }

      dispatch(getElementSuccess((0, _extends3.default)({ id: id, statusCode: statusCode }, payload)));
    }).catch(function (_ref3) {
      var statusCode = _ref3.statusCode,
          error = _ref3.error;

      dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));
    });
  };
};

exports.default = get;

/***/ }),

/***/ "./src/client/actions/posts/update.js":
/*!********************************************!*\
  !*** ./src/client/actions/posts/update.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../constants/posts */ "./src/client/constants/posts.js");

/**
 *
 * @function update
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.data -
 * @param {string} payload.cover -
 *
 * @returns {Object}
 */
var update = function update(payload) {
  return { type: _posts.POST_UPDATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = update;

/***/ }),

/***/ "./src/client/api/elements/getById.js":
/*!********************************************!*\
  !*** ./src/client/api/elements/getById.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @function getById
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
/** @format */

var getById = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        id = _ref2.id;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getById;

/***/ }),

/***/ "./src/client/api/photos/posts/users/create.js":
/*!*****************************************************!*\
  !*** ./src/client/api/photos/posts/users/create.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @async
 * @function
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.postId
 * @param {Object} payload.formData
 * @returns {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

exports.default = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        postId = _ref2.postId,
        formData = _ref2.formData;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts/' + postId + '/photos';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                // 'Content-Type': 'application/json',
                Authorization: token
              },
              body: formData
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  function create(_x) {
    return _ref.apply(this, arguments);
  }

  return create;
}();

/***/ }),

/***/ "./src/client/api/posts/users/update.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/update.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * This function update a post
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.data -
 * @param {string} payload.cover -
 * @param {Boolean} payload.privacy -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file
 */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token,
        postId = _ref2.postId,
        title = _ref2.title,
        description = _ref2.description,
        data = _ref2.data,
        cover = _ref2.cover,
        privacy = _ref2.privacy;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts/' + postId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              // here if any key (title, description or data) is undefined
              // JSON.stringify() will not convert it to string and will removed it.
              body: (0, _stringify2.default)({ title: title, description: description, data: data, cover: cover, privacy: privacy })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/components/Description.js":
/*!**********************************************!*\
  !*** ./src/client/components/Description.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit'
  }
}; /**
    * Description component is for editable component to edit description
    * with textarea.
    * This component should be used with CardHeader subheader place.
    * TODO: make textarea autoresizable.
    *
    * @format
    */

var Description = function Description(_ref) {
  var classes = _ref.classes,
      readOnly = _ref.readOnly,
      placeholder = _ref.placeholder,
      value = _ref.value,
      onChange = _ref.onChange,
      minLength = _ref.minLength,
      maxLength = _ref.maxLength;

  if (readOnly) {
    return _react2.default.createElement(
      'div',
      { className: classes.value },
      value
    );
  }

  return _react2.default.createElement('textarea', {
    placeholder: placeholder,
    value: value,
    onChange: onChange,
    className: classes.textarea,
    minLength: minLength || 1,
    maxLength: maxLength || 300,
    rows: 1,
    readOnly: readOnly
  });
};

Description.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  readOnly: _propTypes2.default.bool.isRequired,
  placeholder: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired,
  minLength: _propTypes2.default.number,
  maxLength: _propTypes2.default.number
};

Description.defaultProps = {
  placeholder: 'Description goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 300
};

exports.default = (0, _styles.withStyles)(styles)(Description);

/***/ }),

/***/ "./src/client/components/Title.js":
/*!****************************************!*\
  !*** ./src/client/components/Title.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit'
  }
}; /**
    * Title component is for editable component to edit title with textarea.
    * This component should be used with CardHeader title place.
    * TODO: make textarea autoresizable.
    *
    * @format
    */

var Title = function Title(_ref) {
  var classes = _ref.classes,
      readOnly = _ref.readOnly,
      value = _ref.value,
      placeholder = _ref.placeholder,
      onChange = _ref.onChange,
      minLength = _ref.minLength,
      maxLength = _ref.maxLength;

  if (readOnly) {
    return _react2.default.createElement(
      'div',
      { className: classes.value },
      value
    );
  }

  return _react2.default.createElement('textarea', {
    placeholder: placeholder,
    value: value,
    onChange: onChange,
    className: classes.textarea,
    minLength: minLength || 1,
    maxLength: maxLength || 148,
    rows: 1,
    readOnly: readOnly
  });
};

Title.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  readOnly: _propTypes2.default.bool.isRequired,
  placeholder: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.string.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  minLength: _propTypes2.default.number,
  maxLength: _propTypes2.default.number
};

Title.defaultProps = {
  value: 'Untitled',
  placeholder: 'Title goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 148
};

exports.default = (0, _styles.withStyles)(styles)(Title);

/***/ }),

/***/ "./src/client/containers/PostPage/Article.js":
/*!***************************************************!*\
  !*** ./src/client/containers/PostPage/Article.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _TurnedIn = __webpack_require__(/*! material-ui-icons/TurnedIn */ "./node_modules/material-ui-icons/TurnedIn.js");

var _TurnedIn2 = _interopRequireDefault(_TurnedIn);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Edit = __webpack_require__(/*! material-ui-icons/Edit */ "./node_modules/material-ui-icons/Edit.js");

var _Edit2 = _interopRequireDefault(_Edit);

var _Save = __webpack_require__(/*! material-ui-icons/Save */ "./node_modules/material-ui-icons/Save.js");

var _Save2 = _interopRequireDefault(_Save);

var _Comment = __webpack_require__(/*! material-ui-icons/Comment */ "./node_modules/material-ui-icons/Comment.js");

var _Comment2 = _interopRequireDefault(_Comment);

var _Badge = __webpack_require__(/*! material-ui/Badge */ "./node_modules/material-ui/Badge/index.js");

var _Badge2 = _interopRequireDefault(_Badge);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _Editor = __webpack_require__(/*! ../../../lib/mayash-editor/Editor */ "./src/lib/mayash-editor/Editor.js");

var _Editor2 = _interopRequireDefault(_Editor);

var _mayashEditor = __webpack_require__(/*! ../../../lib/mayash-editor */ "./src/lib/mayash-editor/index.js");

var _Title = __webpack_require__(/*! ../../components/Title */ "./src/client/components/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _Description = __webpack_require__(/*! ../../components/Description */ "./src/client/components/Description.js");

var _Description2 = _interopRequireDefault(_Description);

var _CardMediaComponent = __webpack_require__(/*! ./CardMediaComponent */ "./src/client/containers/PostPage/CardMediaComponent/index.js");

var _CardMediaComponent2 = _interopRequireDefault(_CardMediaComponent);

var _index = __webpack_require__(/*! ../Privacy/index */ "./src/client/containers/Privacy/index.js");

var _index2 = _interopRequireDefault(_index);

var _ErrorPage = __webpack_require__(/*! ../../components/ErrorPage */ "./src/client/components/ErrorPage.js");

var _ErrorPage2 = _interopRequireDefault(_ErrorPage);

var _update = __webpack_require__(/*! ../../api/posts/users/update */ "./src/client/api/posts/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _create = __webpack_require__(/*! ../../api/photos/posts/users/create */ "./src/client/api/photos/posts/users/create.js");

var _create2 = _interopRequireDefault(_create);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _update3 = __webpack_require__(/*! ../../actions/posts/update */ "./src/client/actions/posts/update.js");

var _update4 = _interopRequireDefault(_update3);

var _ArticleStyles = __webpack_require__(/*! ./ArticleStyles */ "./src/client/containers/PostPage/ArticleStyles.js");

var _ArticleStyles2 = _interopRequireDefault(_ArticleStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component is created to edit content of the article
 *
 * @format
 */

var AsyncComment = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(56).then(__webpack_require__.bind(null, /*! ../Comment/index */ "./src/client/containers/Comment/index.js"));
  },
  modules: ['../Comment/index'],
  loading: _Loading2.default
});

var Article = function (_Component) {
  (0, _inherits3.default)(Article, _Component);

  function Article(props) {
    (0, _classCallCheck3.default)(this, Article);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Article.__proto__ || (0, _getPrototypeOf2.default)(Article)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    var _props$post = props.post,
        title = _props$post.title,
        description = _props$post.description,
        data = _props$post.data;


    _this.state = {
      edit: false,
      title: title,
      description: description,
      data: (0, _mayashEditor.createEditorState)(data),
      commentBox: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onEdit = _this.onEdit.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    _this.onClickCommentIcon = _this.onClickCommentIcon.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Article, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _props = this.props,
          elements = _props.elements,
          post = _props.post;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = post.authorId;


      var author = void 0;
      if (authorId === userId) {
        author = elements.user;
      } else {
        author = elements[authorId];
      }

      if (!author || !author.isFetching && !author.isFetched) {
        this.props.actionElementGetById({ token: token, id: authorId });
      }
    }

    // shouldComponentUpdate(nextProps, nextState) {
    //   return this.state !== nextState || this.props !== nextProps;
    // }

    // This on change function is for MayashEditor

  }, {
    key: 'onChange',
    value: function onChange(data) {
      this.setState({ data: data });
    }

    // it changes the current editing state

  }, {
    key: 'onEdit',
    value: function onEdit() {
      var edit = this.state.edit;

      this.setState({ edit: !edit });
    }
  }, {
    key: 'onSave',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _state, edit, title, description, data, _props2, elements, post, _elements$user2, userId, token, postId, titleProp, descriptionProp, body, _ref2, statusCode, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _state = this.state, edit = _state.edit, title = _state.title, description = _state.description, data = _state.data;
                _props2 = this.props, elements = _props2.elements, post = _props2.post;
                _elements$user2 = elements.user, userId = _elements$user2.id, token = _elements$user2.token;
                postId = post.postId, titleProp = post.title, descriptionProp = post.description;
                body = {};


                if (title !== titleProp) {
                  body = (0, _extends3.default)({}, body, { title: title });
                }

                if (typeof description !== 'undefined' && description.length && description !== descriptionProp) {
                  body = (0, _extends3.default)({}, body, { description: description });
                }

                body.data = (0, _draftJs.convertToRaw)(data.getCurrentContent());

                _context.next = 10;
                return (0, _update2.default)((0, _extends3.default)({
                  userId: userId,
                  postId: postId,
                  token: token
                }, body));

              case 10:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;

                if (!(statusCode >= 300)) {
                  _context.next = 16;
                  break;
                }

                // handle error;
                console.error(error);
                return _context.abrupt('return');

              case 16:

                this.props.actionPostUpdate((0, _extends3.default)({ postId: postId }, body));

                // when editing is done set 'edit' to false
                this.setState({ edit: !edit });

              case 18:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSave() {
        return _ref.apply(this, arguments);
      }

      return onSave;
    }()
  }, {
    key: 'onClickCommentIcon',
    value: function onClickCommentIcon() {
      this.setState({ commentBox: true });
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props3 = this.props,
          classes = _props3.classes,
          post = _props3.post,
          elements = _props3.elements;
      var user = elements.user;
      var _state2 = this.state,
          hover = _state2.hover,
          edit = _state2.edit,
          title = _state2.title,
          description = _state2.description,
          data = _state2.data,
          commentBox = _state2.commentBox;
      var isSignedIn = user.isSignedIn,
          token = user.token,
          userId = user.id;
      var authorId = post.authorId,
          postId = post.postId,
          cover = post.cover,
          comments = post.comments,
          privacy = post.privacy;


      var commentsSize = void 0;

      if (comments !== undefined) {
        commentsSize = (0, _keys2.default)(comments).length;
      }

      var author = { authorId: authorId };

      if (userId === authorId) {
        author = (0, _extends3.default)({}, author, user);
      } else {
        author = (0, _extends3.default)({}, author, elements[authorId]);
      }

      if (authorId !== userId && privacy) return _react2.default.createElement(_ErrorPage2.default, { statusCode: 401 });

      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 11, md: 8, lg: 8, xl: 8 },
          _react2.default.createElement(
            _Card2.default,
            {
              onMouseEnter: this.onMouseEnter,
              onMouseLeave: this.onMouseLeave,
              raised: hover,
              className: classes.card
            },
            _react2.default.createElement(_Card.CardHeader, {
              avatar: _react2.default.createElement(
                _Paper2.default,
                { className: classes.avatar, elevation: 1 },
                _react2.default.createElement(
                  _Link2.default,
                  { to: '/@' + author.username },
                  _react2.default.createElement(_Avatar2.default, {
                    alt: author.name,
                    src: author.avatar || '/public/photos/mayash-logo-transparent.png'
                  })
                )
              ),
              title: _react2.default.createElement(
                _Link2.default,
                { to: '/@' + author.username, className: classes.name },
                author.name
              ),
              subheader: _react2.default.createElement(
                _Link2.default,
                { to: '/@' + author.username, className: classes.username },
                '@',
                author.username
              ),
              action: _react2.default.createElement(
                'div',
                { className: classes.flex },
                _react2.default.createElement(_index2.default, {
                  privacy: privacy,
                  postId: postId,
                  authorId: authorId,
                  token: token,
                  userId: userId,
                  actionPostUpdate: this.props.actionPostUpdate
                }),
                _react2.default.createElement(
                  _Tooltip2.default,
                  { title: 'Coming soon', placement: 'bottom' },
                  _react2.default.createElement(
                    _IconButton2.default,
                    null,
                    _react2.default.createElement(_MoreVert2.default, null)
                  )
                )
              )
            }),
            userId === authorId ? _react2.default.createElement(_Card.CardMedia, {
              className: classes.media,
              component: function component(thisProp) {
                return _react2.default.createElement(_CardMediaComponent2.default, (0, _extends3.default)({}, thisProp, post, {
                  token: token,
                  actionPostUpdate: _this2.props.actionPostUpdate
                }));
              },
              image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
            }) : _react2.default.createElement(_Card.CardMedia, {
              className: classes.media,
              image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
            }),
            _react2.default.createElement(_Card.CardHeader, {
              title: _react2.default.createElement(_Title2.default, {
                readOnly: !edit,
                value: title,
                onChange: function onChange(e) {
                  return _this2.setState({ title: e.target.value });
                }
              }),
              subheader: edit || typeof description !== 'undefined' ? _react2.default.createElement(_Description2.default, {
                readOnly: !edit,
                value: description,
                onChange: function onChange(e) {
                  return _this2.setState({
                    description: e.target.value
                  });
                }
              }) : null
            }),
            edit || typeof post.data !== 'undefined' ? _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(_Editor2.default, {
                editorState: data,
                onChange: this.onChange,
                readOnly: !edit,
                placeholder: 'Content goes here...',
                apiPhotoUpload: function apiPhotoUpload(_ref3) {
                  var formData = _ref3.formData;
                  return (0, _create2.default)({
                    token: token,
                    userId: userId,
                    postId: postId,
                    formData: formData
                  });
                }
              })
            ) : null,
            _react2.default.createElement('div', { className: classes.margin }),
            _react2.default.createElement(
              _Card.CardActions,
              null,
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Add to favorites', disabled: true },
                _react2.default.createElement(_Favorite2.default, null)
              ),
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Book Marks', disabled: true },
                _react2.default.createElement(_TurnedIn2.default, null)
              ),
              _react2.default.createElement(
                _IconButton2.default,
                {
                  'aria-label': 'Comment',
                  onClick: this.onClickCommentIcon
                },
                _react2.default.createElement(
                  _Badge2.default,
                  { badgeContent: commentsSize },
                  _react2.default.createElement(_Comment2.default, null)
                )
              ),
              _react2.default.createElement('div', { className: classes.flexGrow }),
              _react2.default.createElement(
                _Tooltip2.default,
                { title: 'Share', placement: 'bottom' },
                _react2.default.createElement(
                  _IconButton2.default,
                  { 'aria-label': 'Share', disabled: true },
                  _react2.default.createElement(_Share2.default, null)
                )
              )
            )
          ),
          commentBox ? _react2.default.createElement(AsyncComment, { postId: postId }) : null
        ),
        isSignedIn && authorId === userId && _react2.default.createElement(
          _Button2.default,
          {
            fab: true,
            color: 'accent',
            'aria-label': 'edit',
            className: classes.editButton,
            onClick: edit ? this.onSave : this.onEdit
          },
          edit ? _react2.default.createElement(_Save2.default, null) : _react2.default.createElement(_Edit2.default, null)
        )
      );
    }
  }]);
  return Article;
}(_react.Component);

Article.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  elements: _ReactPropTypes2.default.object.isRequired,
  post: _ReactPropTypes2.default.shape({
    postId: _ReactPropTypes2.default.number.isRequired,
    title: _ReactPropTypes2.default.string.isRequired,
    description: _ReactPropTypes2.default.string,
    data: _ReactPropTypes2.default.object
  }),

  actionElementGetById: _ReactPropTypes2.default.func.isRequired,
  actionPostUpdate: _ReactPropTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionElementGetById: _getById2.default,
    actionPostUpdate: _update4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(_ArticleStyles2.default)(Article));

/***/ }),

/***/ "./src/client/containers/PostPage/ArticleStyles.js":
/*!*********************************************************!*\
  !*** ./src/client/containers/PostPage/ArticleStyles.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This file contains all the styles for Article Component.
 *
 * @format
 */

var styles = function styles(theme) {
  var _editButton;

  return {
    root: {
      padding: '1%'
    },
    name: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    avatar: {
      borderRadius: '50%'
    },
    card: {
      borderRadius: '8px'
    },
    username: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    media: {
      height: 194
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    editButton: (_editButton = {
      position: 'fixed',
      zIndex: 1
    }, (0, _defineProperty3.default)(_editButton, theme.breakpoints.up('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('md'), {
      bottom: '30px',
      right: '30px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('sm'), {
      display: 'none',
      bottom: '10px',
      right: '10px'
    }), _editButton),
    margin: {
      border: '0.5px solid rgba(209,209,209,1)',
      marginBottom: '-10px'
    },
    flex: {
      display: 'flex',
      flexDirection: 'row'
    }
  };
};

exports.default = styles;

/***/ }),

/***/ "./src/client/containers/PostPage/CardMediaComponent/index.js":
/*!********************************************************************!*\
  !*** ./src/client/containers/PostPage/CardMediaComponent/index.js ***!
  \********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _update = __webpack_require__(/*! ../../../api/posts/users/update */ "./src/client/api/posts/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _create = __webpack_require__(/*! ../../../api/photos/posts/users/create */ "./src/client/api/photos/posts/users/create.js");

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @format
 */

var CoverImage = function (_Component) {
  (0, _inherits3.default)(CoverImage, _Component);

  function CoverImage(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, CoverImage);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CoverImage.__proto__ || (0, _getPrototypeOf2.default)(CoverImage)).call(this, props));

    _this.onChange = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _this$props, userId, postId, token, _ref2, statusCode, error, payload, cover;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (e) {
                  e.preventDefault();
                }

                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 16;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _this$props = _this.props, userId = _this$props.authorId, postId = _this$props.postId, token = _this$props.token;
                _context.next = 8;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  postId: postId,
                  formData: formData
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;


                if (statusCode >= 300) {
                  // handle Error
                  console.error(statusCode, error);
                }

                cover = payload.photoUrl;


                (0, _update2.default)({
                  token: token,
                  userId: userId,
                  postId: postId,
                  cover: cover
                });

                _this.props.actionPostUpdate({
                  postId: postId,
                  cover: cover
                });

              case 16:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.state = {};

    _this.onClick = _this.onClick.bind(_this);
    // this.onChange = this.onChange.bind(this);
    return _this;
  }

  (0, _createClass3.default)(CoverImage, [{
    key: 'onClick',
    value: function onClick() {
      this.photo.value = null;
      this.photo.click();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      return _react2.default.createElement(
        'div',
        {
          style: (0, _extends3.default)({
            position: 'relative'
          }, this.props.style),
          className: this.props.className
        },
        _react2.default.createElement(
          _IconButton2.default,
          { 'aria-label': 'Insert Photo', onClick: this.onClick },
          _react2.default.createElement(_InsertPhoto2.default, null),
          _react2.default.createElement('input', {
            type: 'file',
            accept: 'image/jpeg|png|gif',
            onChange: this.onChange,
            ref: function ref(photo) {
              _this3.photo = photo;
            },
            style: { display: 'none' }
          })
        )
      );
    }
  }]);
  return CoverImage;
}(_react.Component);

CoverImage.propTypes = {
  style: _propTypes2.default.object.isRequired,
  className: _propTypes2.default.string.isRequired
};
exports.default = CoverImage;

/***/ }),

/***/ "./src/client/containers/Privacy/index.js":
/*!************************************************!*\
  !*** ./src/client/containers/Privacy/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Menu = __webpack_require__(/*! material-ui/Menu */ "./node_modules/material-ui/Menu/index.js");

var _Menu2 = _interopRequireDefault(_Menu);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Public = __webpack_require__(/*! ../../../lib/mayash-icons/Public */ "./src/lib/mayash-icons/Public.js");

var _Public2 = _interopRequireDefault(_Public);

var _Private = __webpack_require__(/*! ../../../lib/mayash-icons/Private */ "./src/lib/mayash-icons/Private.js");

var _Private2 = _interopRequireDefault(_Private);

var _update = __webpack_require__(/*! ../../api/posts/users/update */ "./src/client/api/posts/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {}
};

var Privacy = function (_Component) {
  (0, _inherits3.default)(Privacy, _Component);

  function Privacy(props) {
    (0, _classCallCheck3.default)(this, Privacy);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Privacy.__proto__ || (0, _getPrototypeOf2.default)(Privacy)).call(this, props));

    _this.onClickPrivacy = function (event) {
      _this.setState({ anchorEl: event.currentTarget });
    };

    _this.handleClose = function () {
      _this.setState({ anchorEl: null });
    };

    _this.state = { anchorEl: null, privacy: props.privacy };
    _this.onChangePrivacy = _this.onChangePrivacy.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Privacy, [{
    key: 'onChangePrivacy',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, postId, userId, token, privacy, _ref2, statusCode, message;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, postId = _props.postId, userId = _props.userId, token = _props.token;
                privacy = !this.state.privacy;
                _context.next = 5;
                return (0, _update2.default)({
                  userId: userId,
                  token: token,
                  postId: postId,
                  privacy: privacy
                });

              case 5:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                message = _ref2.message;

                if (!(statusCode >= 300)) {
                  _context.next = 10;
                  break;
                }

                return _context.abrupt('return');

              case 10:

                this.setState({ privacy: privacy });
                this.props.actionPostUpdate({ postId: postId, privacy: privacy });
                _context.next = 17;
                break;

              case 14:
                _context.prev = 14;
                _context.t0 = _context['catch'](0);

                console.log(_context.t0);

              case 17:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 14]]);
      }));

      function onChangePrivacy() {
        return _ref.apply(this, arguments);
      }

      return onChangePrivacy;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          anchorEl = _state.anchorEl,
          privacy = _state.privacy;
      var _props2 = this.props,
          classes = _props2.classes,
          authorId = _props2.authorId,
          userId = _props2.userId;


      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _IconButton2.default,
          {
            'aria-owns': anchorEl ? 'change privacy' : null,
            'aria-haspopup': 'false',
            onClick: this.onClickPrivacy,
            disabled: userId !== authorId
          },
          privacy ? _react2.default.createElement(_Private2.default, null) : _react2.default.createElement(_Public2.default, null)
        ),
        _react2.default.createElement(
          _Menu2.default,
          {
            id: 'change privacy',
            anchorEl: anchorEl,
            open: Boolean(anchorEl),
            PaperProps: {
              style: {
                height: 65,
                width: 65
              }
            }
          },
          _react2.default.createElement(
            _Menu.MenuItem,
            { onClick: this.handleClose },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-owns': anchorEl ? 'change privacy' : null,
                'aria-haspopup': 'false',
                onClick: this.onChangePrivacy
              },
              !privacy ? _react2.default.createElement(_Private2.default, null) : _react2.default.createElement(_Public2.default, null)
            )
          )
        )
      );
    }
  }]);
  return Privacy;
}(_react.Component);

Privacy.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,
  privacy: _ReactPropTypes2.default.bool.isRequired,
  userId: _ReactPropTypes2.default.number.isRequired,
  postId: _ReactPropTypes2.default.number.isRequired,
  token: _ReactPropTypes2.default.string.isRequired,
  actionPostUpdate: _ReactPropTypes2.default.func.isRequired
};
exports.default = (0, _withStyles2.default)(styles)(Privacy);

/***/ }),

/***/ "./src/lib/mayash-editor/Editor.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-editor/Editor.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! material-ui-icons/Title */ "./node_modules/material-ui-icons/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _FormatBold = __webpack_require__(/*! material-ui-icons/FormatBold */ "./node_modules/material-ui-icons/FormatBold.js");

var _FormatBold2 = _interopRequireDefault(_FormatBold);

var _FormatItalic = __webpack_require__(/*! material-ui-icons/FormatItalic */ "./node_modules/material-ui-icons/FormatItalic.js");

var _FormatItalic2 = _interopRequireDefault(_FormatItalic);

var _FormatUnderlined = __webpack_require__(/*! material-ui-icons/FormatUnderlined */ "./node_modules/material-ui-icons/FormatUnderlined.js");

var _FormatUnderlined2 = _interopRequireDefault(_FormatUnderlined);

var _FormatQuote = __webpack_require__(/*! material-ui-icons/FormatQuote */ "./node_modules/material-ui-icons/FormatQuote.js");

var _FormatQuote2 = _interopRequireDefault(_FormatQuote);

var _Code = __webpack_require__(/*! material-ui-icons/Code */ "./node_modules/material-ui-icons/Code.js");

var _Code2 = _interopRequireDefault(_Code);

var _FormatListNumbered = __webpack_require__(/*! material-ui-icons/FormatListNumbered */ "./node_modules/material-ui-icons/FormatListNumbered.js");

var _FormatListNumbered2 = _interopRequireDefault(_FormatListNumbered);

var _FormatListBulleted = __webpack_require__(/*! material-ui-icons/FormatListBulleted */ "./node_modules/material-ui-icons/FormatListBulleted.js");

var _FormatListBulleted2 = _interopRequireDefault(_FormatListBulleted);

var _FormatAlignCenter = __webpack_require__(/*! material-ui-icons/FormatAlignCenter */ "./node_modules/material-ui-icons/FormatAlignCenter.js");

var _FormatAlignCenter2 = _interopRequireDefault(_FormatAlignCenter);

var _FormatAlignLeft = __webpack_require__(/*! material-ui-icons/FormatAlignLeft */ "./node_modules/material-ui-icons/FormatAlignLeft.js");

var _FormatAlignLeft2 = _interopRequireDefault(_FormatAlignLeft);

var _FormatAlignRight = __webpack_require__(/*! material-ui-icons/FormatAlignRight */ "./node_modules/material-ui-icons/FormatAlignRight.js");

var _FormatAlignRight2 = _interopRequireDefault(_FormatAlignRight);

var _FormatAlignJustify = __webpack_require__(/*! material-ui-icons/FormatAlignJustify */ "./node_modules/material-ui-icons/FormatAlignJustify.js");

var _FormatAlignJustify2 = _interopRequireDefault(_FormatAlignJustify);

var _AttachFile = __webpack_require__(/*! material-ui-icons/AttachFile */ "./node_modules/material-ui-icons/AttachFile.js");

var _AttachFile2 = _interopRequireDefault(_AttachFile);

var _InsertLink = __webpack_require__(/*! material-ui-icons/InsertLink */ "./node_modules/material-ui-icons/InsertLink.js");

var _InsertLink2 = _interopRequireDefault(_InsertLink);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _InsertEmoticon = __webpack_require__(/*! material-ui-icons/InsertEmoticon */ "./node_modules/material-ui-icons/InsertEmoticon.js");

var _InsertEmoticon2 = _interopRequireDefault(_InsertEmoticon);

var _InsertComment = __webpack_require__(/*! material-ui-icons/InsertComment */ "./node_modules/material-ui-icons/InsertComment.js");

var _InsertComment2 = _interopRequireDefault(_InsertComment);

var _Highlight = __webpack_require__(/*! material-ui-icons/Highlight */ "./node_modules/material-ui-icons/Highlight.js");

var _Highlight2 = _interopRequireDefault(_Highlight);

var _Functions = __webpack_require__(/*! material-ui-icons/Functions */ "./node_modules/material-ui-icons/Functions.js");

var _Functions2 = _interopRequireDefault(_Functions);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Atomic = __webpack_require__(/*! ./components/Atomic */ "./src/lib/mayash-editor/components/Atomic.js");

var _Atomic2 = _interopRequireDefault(_Atomic);

var _EditorStyles = __webpack_require__(/*! ./EditorStyles */ "./src/lib/mayash-editor/EditorStyles.js");

var _EditorStyles2 = _interopRequireDefault(_EditorStyles);

var _constants = __webpack_require__(/*! ./constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * MayashEditor
 */


// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

var MayashEditor = function (_Component) {
  (0, _inherits3.default)(MayashEditor, _Component);

  /**
   * Mayash Editor's proptypes are defined here.
   */
  function MayashEditor() {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, MayashEditor);

    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    var _this = (0, _possibleConstructorReturn3.default)(this, (MayashEditor.__proto__ || (0, _getPrototypeOf2.default)(MayashEditor)).call(this));

    _this.onChangeInsertPhoto = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _ref2, statusCode, error, payload, src, editorState, contentState, contentStateWithEntity, entityKey, middleEditorState, newEditorState;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // e.preventDefault();
                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 21;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _context.next = 6;
                return _this.props.apiPhotoUpload({
                  formData: formData
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle Error
                console.error(statusCode, error);
                return _context.abrupt('return');

              case 13:
                src = payload.photoUrl;
                editorState = _this.props.editorState;
                contentState = editorState.getCurrentContent();
                contentStateWithEntity = contentState.createEntity(_constants.Blocks.PHOTO, 'IMMUTABLE', { src: src });
                entityKey = contentStateWithEntity.getLastCreatedEntityKey();
                middleEditorState = _draftJs.EditorState.set(editorState, {
                  currentContent: contentStateWithEntity
                });
                newEditorState = _draftJs.AtomicBlockUtils.insertAtomicBlock(middleEditorState, entityKey, ' ');


                _this.onChange(newEditorState);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.onChange = function (editorState) {
      _this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    _this.focus = function () {
      return _this.editorNode.focus();
    };

    _this.onTab = _this.onTab.bind(_this);
    _this.onTitleClick = _this.onTitleClick.bind(_this);
    _this.onCodeClick = _this.onCodeClick.bind(_this);
    _this.onQuoteClick = _this.onQuoteClick.bind(_this);
    _this.onListBulletedClick = _this.onListBulletedClick.bind(_this);
    _this.onListNumberedClick = _this.onListNumberedClick.bind(_this);
    _this.onBoldClick = _this.onBoldClick.bind(_this);
    _this.onItalicClick = _this.onItalicClick.bind(_this);
    _this.onUnderLineClick = _this.onUnderLineClick.bind(_this);
    _this.onHighlightClick = _this.onHighlightClick.bind(_this);
    _this.handleKeyCommand = _this.handleKeyCommand.bind(_this);
    _this.onClickInsertPhoto = _this.onClickInsertPhoto.bind(_this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
    return _this;
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */


  (0, _createClass3.default)(MayashEditor, [{
    key: 'onTab',
    value: function onTab(e) {
      e.preventDefault();
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.onTab(e, editorState, 4);

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Title Button.
     * @function onTitleClick
     */

  }, {
    key: 'onTitleClick',
    value: function onTitleClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'header-one');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Code Button.
     * @function onCodeClick
     */

  }, {
    key: 'onCodeClick',
    value: function onCodeClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'code-block');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Quote Button.
     * @function onQuoteClick
     */

  }, {
    key: 'onQuoteClick',
    value: function onQuoteClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'blockquote');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with unordered list items
     * @function onListBulletedClick
     */

  }, {
    key: 'onListBulletedClick',
    value: function onListBulletedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'unordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with ordered list item.
     * @function onListNumberedClick
     */

  }, {
    key: 'onListNumberedClick',
    value: function onListNumberedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'ordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will give selected content Bold styling.
     * @function onBoldClick
     */

  }, {
    key: 'onBoldClick',
    value: function onBoldClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'BOLD');

      this.onChange(newEditorState);
    }

    /**
     * This function will give italic styling to selected content.
     * @function onItalicClick
     */

  }, {
    key: 'onItalicClick',
    value: function onItalicClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'ITALIC');

      this.onChange(newEditorState);
    }

    /**
     * This function will give underline styling to selected content.
     * @function onUnderLineClick
     */

  }, {
    key: 'onUnderLineClick',
    value: function onUnderLineClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');

      this.onChange(newEditorState);
    }

    /**
     * This function will highlight selected content.
     * @function onHighlightClick
     */

  }, {
    key: 'onHighlightClick',
    value: function onHighlightClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'CODE');

      this.onChange(newEditorState);
    }

    /**
     *
     */

  }, {
    key: 'onClickInsertPhoto',
    value: function onClickInsertPhoto() {
      this.photo.value = null;
      this.photo.click();
    }

    /**
     *
     */

  }, {
    key: 'handleKeyCommand',


    /**
     * This function will give custom handle commands to Editor.
     * @function handleKeyCommand
     * @param {string} command
     * @return {string}
     */
    value: function handleKeyCommand(command) {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.handleKeyCommand(editorState, command);

      if (newEditorState) {
        this.onChange(newEditorState);
        return _constants.HANDLED;
      }

      return _constants.NOT_HANDLED;
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockRendererFn',
    value: function blockRendererFn(block) {
      switch (block.getType()) {
        case _constants.Blocks.ATOMIC:
          return {
            component: _Atomic2.default,
            editable: false
          };

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockStyleFn',
    value: function blockStyleFn(block) {
      switch (block.getType()) {
        case 'blockquote':
          return 'RichEditor-blockquote';

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          readOnly = _props.readOnly,
          onChange = _props.onChange,
          editorState = _props.editorState,
          placeholder = _props.placeholder;

      // Custom overrides for "code" style.

      var styleMap = {
        CODE: {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
          fontSize: 16,
          padding: 2
        }
      };

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        !readOnly ? _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-controls': '' }) },
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Title', id: 'title', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Title', onClick: this.onTitleClick },
              _react2.default.createElement(_Title2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Bold', id: 'bold', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Bold', onClick: this.onBoldClick },
              _react2.default.createElement(_FormatBold2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Italic', id: 'italic', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Italic', onClick: this.onItalicClick },
              _react2.default.createElement(_FormatItalic2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Underline', id: 'underline', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Underline',
                onClick: this.onUnderLineClick
              },
              _react2.default.createElement(_FormatUnderlined2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Code', id: 'code', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Code', onClick: this.onCodeClick },
              _react2.default.createElement(_Code2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Quote', id: 'quote', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Quote', onClick: this.onQuoteClick },
              _react2.default.createElement(_FormatQuote2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Unordered List',
              id: 'unorderd-list',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Unordered List',
                onClick: this.onListBulletedClick
              },
              _react2.default.createElement(_FormatListBulleted2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Ordered List', id: 'ordered-list', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Ordered List',
                onClick: this.onListNumberedClick
              },
              _react2.default.createElement(_FormatListNumbered2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Left', id: 'align-left', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Left', disabled: true },
              _react2.default.createElement(_FormatAlignLeft2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Center', id: 'align-center', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Center', disabled: true },
              _react2.default.createElement(_FormatAlignCenter2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignRight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignJustify2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Attach File', id: 'attach-file', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Attach File', disabled: true },
              _react2.default.createElement(_AttachFile2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Link', id: 'insert-link', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Link', disabled: true },
              _react2.default.createElement(_InsertLink2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Photo', id: 'insert-photo', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Insert Photo',
                disabled: typeof this.props.apiPhotoUpload !== 'function',
                onClick: this.onClickInsertPhoto
              },
              _react2.default.createElement(_InsertPhoto2.default, null),
              _react2.default.createElement('input', {
                type: 'file',
                accept: 'image/jpeg|png|gif',
                onChange: this.onChangeInsertPhoto,
                ref: function ref(photo) {
                  _this3.photo = photo;
                },
                style: { display: 'none' }
              })
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Emoticon',
              id: 'insertE-emoticon',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Emoticon', disabled: true },
              _react2.default.createElement(_InsertEmoticon2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Comment',
              id: 'insert-comment',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Comment', disabled: true },
              _react2.default.createElement(_InsertComment2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Highlight Text',
              id: 'highlight-text',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Highlight Text',
                onClick: this.onHighlightClick
              },
              _react2.default.createElement(_Highlight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Add Functions',
              id: 'add-functions',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Add Functions', disabled: true },
              _react2.default.createElement(_Functions2.default, null)
            )
          )
        ) : null,
        _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-area': '' }) },
          _react2.default.createElement(_draftJs.Editor
          /* Basics */

          , { editorState: editorState,
            onChange: onChange
            /* Presentation */

            , placeholder: placeholder
            // textAlignment="center"

            // textDirectionality="LTR"

            , blockRendererFn: this.blockRendererFn,
            blockStyleFn: this.blockStyleFn,
            customStyleMap: styleMap
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            , readOnly: readOnly,
            spellCheck: true
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            , handleKeyCommand: this.handleKeyCommand
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            , onTab: this.onTab
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            , ref: function ref(node) {
              _this3.editorNode = node;
            }
          })
        )
      );
    }
  }]);
  return MayashEditor;
}(_react.Component);

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

// import Icon from 'material-ui/Icon';
/** @format */

MayashEditor.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  editorState: _ReactPropTypes2.default.object.isRequired,
  onChange: _ReactPropTypes2.default.func.isRequired,

  placeholder: _ReactPropTypes2.default.string,

  readOnly: _ReactPropTypes2.default.bool.isRequired,

  /**
   * This api function should be applied for enabling photo adding
   * feature in Mayash Editor.
   */
  apiPhotoUpload: _ReactPropTypes2.default.func
};
MayashEditor.defaultProps = {
  readOnly: true,
  placeholder: 'Write Here...'
};
exports.default = (0, _withStyles2.default)(_EditorStyles2.default)(MayashEditor);

/***/ }),

/***/ "./src/lib/mayash-editor/EditorState.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/EditorState.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = undefined;

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Decorators = __webpack_require__(/*! ./components/Decorators */ "./src/lib/mayash-editor/components/Decorators.js");

/** @format */

var defaultDecorators = new _draftJs.CompositeDecorator([{
  strategy: _Decorators.handleStrategy,
  component: _Decorators.HandleSpan
}, {
  strategy: _Decorators.hashtagStrategy,
  component: _Decorators.HashtagSpan
}]);

var createEditorState = exports.createEditorState = function createEditorState() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var decorators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDecorators;

  if (content === null) {
    return _draftJs.EditorState.createEmpty(decorators);
  }
  return _draftJs.EditorState.createWithContent((0, _draftJs.convertFromRaw)(content), decorators);
};

exports.default = createEditorState;

/***/ }),

/***/ "./src/lib/mayash-editor/EditorStyles.js":
/*!***********************************************!*\
  !*** ./src/lib/mayash-editor/EditorStyles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the CSS-in-JS styles of Editor component.
 *
 * @format
 */

exports.default = {
  '@global': {
    '.RichEditor-root': {
      background: '#fff',
      border: '1px solid #ddd',
      fontFamily: "'Georgia', serif",
      fontSize: '14px',
      padding: '15px'
    },
    '.RichEditor-editor': {
      borderTop: '1px solid #ddd',
      cursor: 'text',
      fontSize: '16px',
      marginTop: '10px'
    },
    '.public-DraftEditorPlaceholder-root': {
      margin: '0 -15px -15px',
      padding: '15px'
    },
    '.public-DraftEditor-content': {
      margin: '0 -15px -15px',
      padding: '15px'
      // minHeight: '100px',
    },
    '.RichEditor-blockquote': {
      backgroundColor: '5px solid #eee',
      borderLeft: '5px solid #eee',
      color: '#666',
      fontFamily: "'Hoefler Text', 'Georgia', serif",
      fontStyle: 'italic',
      margin: '16px 0',
      padding: '10px 20px'
    },
    '.public-DraftStyleDefault-pre': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: "'Inconsolata', 'Menlo', 'Consolas', monospace",
      fontSize: '16px',
      padding: '20px'
    }
  },
  root: {
    // padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/***/ }),

/***/ "./src/lib/mayash-editor/components/Atomic.js":
/*!****************************************************!*\
  !*** ./src/lib/mayash-editor/components/Atomic.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = __webpack_require__(/*! ../constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial'
  }
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
/**
 *
 * @format
 */

var Atomic = function (_Component) {
  (0, _inherits3.default)(Atomic, _Component);

  function Atomic(props) {
    (0, _classCallCheck3.default)(this, Atomic);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Atomic.__proto__ || (0, _getPrototypeOf2.default)(Atomic)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Atomic, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          contentState = _props.contentState,
          block = _props.block;


      var entity = contentState.getEntity(block.getEntityAt(0));

      var _entity$getData = entity.getData(),
          src = _entity$getData.src;

      var type = entity.getType();

      if (type === _constants.Blocks.PHOTO) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('img', { alt: 'alt', src: src, style: styles.photo })
        );
      }

      return _react2.default.createElement('div', null);
    }
  }]);
  return Atomic;
}(_react.Component);

Atomic.propTypes = {
  contentState: _propTypes2.default.object.isRequired,
  block: _propTypes2.default.any.isRequired
};
exports.default = Atomic;

/***/ }),

/***/ "./src/lib/mayash-editor/components/Decorators.js":
/*!********************************************************!*\
  !*** ./src/lib/mayash-editor/components/Decorators.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashtagSpan = exports.HandleSpan = undefined;
exports.handleStrategy = handleStrategy;
exports.hashtagStrategy = hashtagStrategy;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _style = __webpack_require__(/*! ../style */ "./src/lib/mayash-editor/style/index.js");

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
// eslint is disabled here for now.
var HANDLE_REGEX = /\@[\w]+/g;
var HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function findWithRegex(regex, contentBlock, callback) {
  var text = contentBlock.getText();
  var matchArr = void 0,
      start = void 0;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

var HandleSpan = exports.HandleSpan = function HandleSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.handle },
    props.children
  );
};

var HashtagSpan = exports.HashtagSpan = function HashtagSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.hashtag },
    props.children
  );
};

exports.default = {
  handleStrategy: handleStrategy,
  HandleSpan: HandleSpan,

  hashtagStrategy: hashtagStrategy,
  HashtagSpan: HashtagSpan
};

/***/ }),

/***/ "./src/lib/mayash-editor/constants.js":
/*!********************************************!*\
  !*** ./src/lib/mayash-editor/constants.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
var Blocks = exports.Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video'
};

/**
 * @constant Inline
 */
var Inline = exports.Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT'
};

/**
 * @constant Entity
 */
var Entity = exports.Entity = {
  LINK: 'LINK'
};

/**
 * @constant HYPERLINK
 */
var HYPERLINK = exports.HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
var HANDLED = exports.HANDLED = 'handled';
var NOT_HANDLED = exports.NOT_HANDLED = 'not_handled';

exports.default = {
  Blocks: Blocks,
  Inline: Inline,
  Entity: Entity
};

/***/ }),

/***/ "./src/lib/mayash-editor/index.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-editor/index.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = exports.MayashEditor = undefined;

var _Editor = __webpack_require__(/*! ./Editor */ "./src/lib/mayash-editor/Editor.js");

Object.defineProperty(exports, 'MayashEditor', {
  enumerable: true,
  get: function get() {
    return _Editor.MayashEditor;
  }
});

var _EditorState = __webpack_require__(/*! ./EditorState */ "./src/lib/mayash-editor/EditorState.js");

Object.defineProperty(exports, 'createEditorState', {
  enumerable: true,
  get: function get() {
    return _EditorState.createEditorState;
  }
});

var _Editor2 = _interopRequireDefault(_Editor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Editor2.default;

/***/ }),

/***/ "./src/lib/mayash-editor/style/index.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/style/index.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @format */

var style = exports.style = {
  root: {
    padding: 20,
    width: 600
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10
  },
  button: {
    marginTop: 10,
    textAlign: 'center'
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override'
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)'
  }
};

exports.default = style;

/***/ }),

/***/ "./src/lib/mayash-icons/Private.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-icons/Private.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var Private = function Private(_ref) {
  var style = _ref.style,
      width = _ref.width,
      height = _ref.height;
  return _react2.default.createElement(
    'svg',
    {
      fill: '#000000',
      height: '18',
      viewBox: '0 0 24 24',
      width: '18',
      xmlns: 'http://www.w3.org/2000/svg'
    },
    _react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
    _react2.default.createElement('path', { d: 'M18 8h-1V6c0-2.76-2.24-5-5-5S7 3.24 7 6v2H6c-1.1 0-2 .9-2 2v10c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V10c0-1.1-.9-2-2-2zm-6 9c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2zm3.1-9H8.9V6c0-1.71 1.39-3.1 3.1-3.1 1.71 0 3.1 1.39 3.1 3.1v2z' })
  );
};

Private.propTypes = {
  width: _propTypes2.default.string,
  height: _propTypes2.default.string,
  style: _propTypes2.default.object
};

exports.default = Private;

/***/ }),

/***/ "./src/lib/mayash-icons/Public.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-icons/Public.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var Public = function Public(_ref) {
  var style = _ref.style,
      width = _ref.width,
      height = _ref.height;
  return _react2.default.createElement(
    'svg',
    {
      fill: '#000000',
      height: '18',
      viewBox: '0 0 24 24',
      width: '18',
      xmlns: 'http://www.w3.org/2000/svg'
    },
    _react2.default.createElement('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
    _react2.default.createElement('path', { d: 'M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-1 17.93c-3.95-.49-7-3.85-7-7.93 0-.62.08-1.21.21-1.79L9 15v1c0 1.1.9 2 2 2v1.93zm6.9-2.54c-.26-.81-1-1.39-1.9-1.39h-1v-3c0-.55-.45-1-1-1H8v-2h2c.55 0 1-.45 1-1V7h2c1.1 0 2-.9 2-2v-.41c2.93 1.19 5 4.06 5 7.41 0 2.08-.8 3.97-2.1 5.39z' })
  );
};

Public.propTypes = {
  width: _propTypes2.default.string,
  height: _propTypes2.default.string,
  style: _propTypes2.default.object
};

exports.default = Public;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvY2xhc3MvYWRkQ2xhc3MuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2NsYXNzL2hhc0NsYXNzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9yZW1vdmVDbGFzcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnkuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Qm9sZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SXRhbGljLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0UXVvdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRFbW90aWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0TGluay5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TaGFyZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL0JhZGdlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9JY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbUF2YXRhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbUljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1UZXh0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RTdWJoZWFkZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51SXRlbS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51TGlzdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvQmFja2Ryb3AuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL01vZGFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvbW9kYWxNYW5hZ2VyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Qb3BvdmVyL1BvcG92ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1BvcG92ZXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvUG9ydGFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC90cmFuc2l0aW9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9GYWRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9Hcm93LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS91dGlscy9tYW5hZ2VBcmlhSGlkZGVuLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvQ1NTVHJhbnNpdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9lbGVtZW50cy9nZXRCeUlkLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9wb3N0cy91cGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvZWxlbWVudHMvZ2V0QnlJZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9waG90b3MvcG9zdHMvdXNlcnMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3Bvc3RzL3VzZXJzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvRGVzY3JpcHRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL1RpdGxlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0UGFnZS9BcnRpY2xlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0UGFnZS9BcnRpY2xlU3R5bGVzLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0UGFnZS9DYXJkTWVkaWFDb21wb25lbnQvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1ByaXZhY3kvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3RhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvclN0eWxlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9BdG9taWMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2NvbXBvbmVudHMvRGVjb3JhdG9ycy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29uc3RhbnRzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3Ivc3R5bGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtaWNvbnMvUHJpdmF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1pY29ucy9QdWJsaWMuanMiXSwibmFtZXMiOlsiZ2V0RWxlbWVudFN0YXJ0IiwicGF5bG9hZCIsInR5cGUiLCJnZXRFbGVtZW50U3VjY2VzcyIsImdldEVsZW1lbnRFcnJvciIsImdldCIsInRva2VuIiwiaWQiLCJkaXNwYXRjaCIsInRoZW4iLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJjYXRjaCIsInVwZGF0ZSIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsImpzb24iLCJjb25zb2xlIiwiZ2V0QnlJZCIsInVzZXJJZCIsInBvc3RJZCIsImZvcm1EYXRhIiwiYm9keSIsImNyZWF0ZSIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJkYXRhIiwiY292ZXIiLCJwcml2YWN5Iiwic3R5bGVzIiwidmFsdWUiLCJ0ZXh0YXJlYSIsIndpZHRoIiwiaGVpZ2h0IiwiYm9yZGVyIiwicmVzaXplIiwiZm9udCIsIkRlc2NyaXB0aW9uIiwiY2xhc3NlcyIsInJlYWRPbmx5IiwicGxhY2Vob2xkZXIiLCJvbkNoYW5nZSIsIm1pbkxlbmd0aCIsIm1heExlbmd0aCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJib29sIiwic3RyaW5nIiwiZnVuYyIsIm51bWJlciIsImRlZmF1bHRQcm9wcyIsIlRpdGxlIiwiQXN5bmNDb21tZW50IiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBcnRpY2xlIiwicHJvcHMiLCJvbk1vdXNlRW50ZXIiLCJzZXRTdGF0ZSIsImhvdmVyIiwib25Nb3VzZUxlYXZlIiwicG9zdCIsInN0YXRlIiwiZWRpdCIsImNvbW1lbnRCb3giLCJiaW5kIiwib25FZGl0Iiwib25TYXZlIiwib25DbGlja0NvbW1lbnRJY29uIiwiZWxlbWVudHMiLCJ1c2VyIiwiYXV0aG9ySWQiLCJhdXRob3IiLCJpc0ZldGNoaW5nIiwiaXNGZXRjaGVkIiwiYWN0aW9uRWxlbWVudEdldEJ5SWQiLCJ0aXRsZVByb3AiLCJkZXNjcmlwdGlvblByb3AiLCJsZW5ndGgiLCJnZXRDdXJyZW50Q29udGVudCIsImFjdGlvblBvc3RVcGRhdGUiLCJpc1NpZ25lZEluIiwiY29tbWVudHMiLCJjb21tZW50c1NpemUiLCJ1bmRlZmluZWQiLCJyb290IiwiY2FyZCIsImF2YXRhciIsInVzZXJuYW1lIiwibmFtZSIsImZsZXgiLCJtZWRpYSIsInRoaXNQcm9wIiwiZSIsInRhcmdldCIsIm1hcmdpbiIsImZsZXhHcm93IiwiZWRpdEJ1dHRvbiIsInNoYXBlIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwidGhlbWUiLCJwYWRkaW5nIiwiY3Vyc29yIiwidGV4dERlY29yYXRpb24iLCJib3JkZXJSYWRpdXMiLCJwb3NpdGlvbiIsInpJbmRleCIsImJyZWFrcG9pbnRzIiwidXAiLCJib3R0b20iLCJyaWdodCIsImRvd24iLCJkaXNwbGF5IiwibWFyZ2luQm90dG9tIiwiZmxleERpcmVjdGlvbiIsIkNvdmVySW1hZ2UiLCJwcmV2ZW50RGVmYXVsdCIsImZpbGUiLCJmaWxlcyIsImluZGV4T2YiLCJGb3JtRGF0YSIsImFwcGVuZCIsInBob3RvVXJsIiwib25DbGljayIsInBob3RvIiwiY2xpY2siLCJzdHlsZSIsImNsYXNzTmFtZSIsIlByaXZhY3kiLCJvbkNsaWNrUHJpdmFjeSIsImV2ZW50IiwiYW5jaG9yRWwiLCJjdXJyZW50VGFyZ2V0IiwiaGFuZGxlQ2xvc2UiLCJvbkNoYW5nZVByaXZhY3kiLCJtZXNzYWdlIiwibG9nIiwiQm9vbGVhbiIsIk1heWFzaEVkaXRvciIsIm9uQ2hhbmdlSW5zZXJ0UGhvdG8iLCJhcGlQaG90b1VwbG9hZCIsInNyYyIsImVkaXRvclN0YXRlIiwiY29udGVudFN0YXRlIiwiY29udGVudFN0YXRlV2l0aEVudGl0eSIsImNyZWF0ZUVudGl0eSIsIlBIT1RPIiwiZW50aXR5S2V5IiwiZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkiLCJtaWRkbGVFZGl0b3JTdGF0ZSIsInNldCIsImN1cnJlbnRDb250ZW50IiwibmV3RWRpdG9yU3RhdGUiLCJpbnNlcnRBdG9taWNCbG9jayIsImZvY3VzIiwiZWRpdG9yTm9kZSIsIm9uVGFiIiwib25UaXRsZUNsaWNrIiwib25Db2RlQ2xpY2siLCJvblF1b3RlQ2xpY2siLCJvbkxpc3RCdWxsZXRlZENsaWNrIiwib25MaXN0TnVtYmVyZWRDbGljayIsIm9uQm9sZENsaWNrIiwib25JdGFsaWNDbGljayIsIm9uVW5kZXJMaW5lQ2xpY2siLCJvbkhpZ2hsaWdodENsaWNrIiwiaGFuZGxlS2V5Q29tbWFuZCIsIm9uQ2xpY2tJbnNlcnRQaG90byIsInRvZ2dsZUJsb2NrVHlwZSIsInRvZ2dsZUlubGluZVN0eWxlIiwiY29tbWFuZCIsImJsb2NrIiwiZ2V0VHlwZSIsIkFUT01JQyIsImNvbXBvbmVudCIsImVkaXRhYmxlIiwic3R5bGVNYXAiLCJDT0RFIiwiYmFja2dyb3VuZENvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTaXplIiwiYmxvY2tSZW5kZXJlckZuIiwiYmxvY2tTdHlsZUZuIiwibm9kZSIsImRlZmF1bHREZWNvcmF0b3JzIiwic3RyYXRlZ3kiLCJjcmVhdGVFZGl0b3JTdGF0ZSIsImNvbnRlbnQiLCJkZWNvcmF0b3JzIiwiY3JlYXRlRW1wdHkiLCJjcmVhdGVXaXRoQ29udGVudCIsImJhY2tncm91bmQiLCJib3JkZXJUb3AiLCJtYXJnaW5Ub3AiLCJib3JkZXJMZWZ0IiwiY29sb3IiLCJmb250U3R5bGUiLCJ3aGl0ZVNwYWNlIiwiQXRvbWljIiwiZW50aXR5IiwiZ2V0RW50aXR5IiwiZ2V0RW50aXR5QXQiLCJnZXREYXRhIiwiYW55IiwiaGFuZGxlU3RyYXRlZ3kiLCJoYXNodGFnU3RyYXRlZ3kiLCJIQU5ETEVfUkVHRVgiLCJIQVNIVEFHX1JFR0VYIiwiZmluZFdpdGhSZWdleCIsInJlZ2V4IiwiY29udGVudEJsb2NrIiwiY2FsbGJhY2siLCJ0ZXh0IiwiZ2V0VGV4dCIsIm1hdGNoQXJyIiwic3RhcnQiLCJleGVjIiwiaW5kZXgiLCJIYW5kbGVTcGFuIiwiaGFuZGxlIiwiY2hpbGRyZW4iLCJIYXNodGFnU3BhbiIsImhhc2h0YWciLCJCbG9ja3MiLCJVTlNUWUxFRCIsIlBBUkFHUkFQSCIsIkgxIiwiSDIiLCJIMyIsIkg0IiwiSDUiLCJINiIsIk9MIiwiVUwiLCJCTE9DS1FVT1RFIiwiVklERU8iLCJJbmxpbmUiLCJCT0xEIiwiSVRBTElDIiwiU1RSSUtFVEhST1VHSCIsIlVOREVSTElORSIsIkhJR0hMSUdIVCIsIkVudGl0eSIsIkxJTksiLCJIWVBFUkxJTksiLCJIQU5ETEVEIiwiTk9UX0hBTkRMRUQiLCJlZGl0b3IiLCJtaW5IZWlnaHQiLCJidXR0b24iLCJ0ZXh0QWxpZ24iLCJkaXJlY3Rpb24iLCJ1bmljb2RlQmlkaSIsIlByaXZhdGUiLCJQdWJsaWMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRyxZQUFZO0FBQ2Y7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ3BCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBLDBEQUEwRCwwSkFBMEo7QUFDcE47QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ2hCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQSxxRkFBcUY7QUFDckY7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDZEQUE2RCxtSEFBbUg7QUFDaEwsRTs7Ozs7Ozs7Ozs7OztBQ1JBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9RQUFvUTs7QUFFdFQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtHQUFrRzs7QUFFcEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtJQUFrSTs7QUFFcEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwwQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDZKQUE2Sjs7QUFFL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNMQUFzTDs7QUFFeE87QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1GQUFtRjs7QUFFckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtGQUFrRjs7QUFFcEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9GQUFvRjs7QUFFdEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1GQUFtRjs7QUFFckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDZPQUE2Tzs7QUFFL1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHdEQUF3RDs7QUFFMUc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHVSQUF1Ujs7QUFFelU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtKQUFrSjs7QUFFcE07QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGdEQUFnRDs7QUFFbEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDJIQUEySDs7QUFFN0s7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtEQUFrRDs7QUFFcEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9JQUFvSTs7QUFFdEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNIQUFzSDs7QUFFeEs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlXQUF5Vzs7QUFFM1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDJOQUEyTjs7QUFFN1E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGdJQUFnSTs7QUFFbEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlKQUF5Sjs7QUFFM007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNKQUFzSjs7QUFFeE07QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELCtXQUErVzs7QUFFamE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx3Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELCtCQUErQjs7QUFFakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx3Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1FQUFtRTs7QUFFckg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7QUFDOUY7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLGdDQUFnQztBQUNqRyxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUMvTXpFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsbVBBQTRJOztBQUU1STs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxvR0FBb0c7O0FBRXBHO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLFdBQVcsNEJBQTRCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsbUJBQW1CLFM7Ozs7Ozs7Ozs7Ozs7QUNuS3hFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxnSEFBZ0g7O0FBRWhIO0FBQ0E7QUFDQSxnQ0FBZ0MsOENBQThDO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsa0JBQWtCLFE7Ozs7Ozs7Ozs7Ozs7QUM5SXZFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsbVBBQTRJO0FBQzVJOztBQUVBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0EsOEVBQThFO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxXQUFXLDJCQUEyQjtBQUN0QztBQUNBO0FBQ0EsYUFBYSwwQkFBMEI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjs7QUFFQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsd0JBQXdCLGM7Ozs7Ozs7Ozs7Ozs7QUNyTzdFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtFQUErRTs7QUFFL0U7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUIsVUFBVSxlQUFlO0FBQ2hGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUEscURBQXFELGtCQUFrQixROzs7Ozs7Ozs7Ozs7O0FDeEx2RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQSwrRUFBK0U7O0FBRS9FLGtEQUFrRCwyQ0FBMkM7QUFDN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLCtCQUErQjtBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxxREFBcUQsc0JBQXNCLFk7Ozs7Ozs7Ozs7Ozs7QUN4UTNFOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQSx5RUFBeUU7QUFDekUsaUZBQWlGO0FBQ2pGLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEscURBQXFELDRCQUE0QixrQjs7Ozs7Ozs7Ozs7OztBQ3hHakY7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQSxxREFBcUQsMEJBQTBCLGdCOzs7Ozs7Ozs7Ozs7O0FDN0UvRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBLFNBQVM7O0FBRVQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxLQUFLLGdFQUFnRTtBQUNyRTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBLHFEQUFxRCxxQ0FBcUMsMkI7Ozs7Ozs7Ozs7Ozs7QUM3RTFGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STs7QUFFNUk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtFQUErRTs7QUFFL0U7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRkFBK0Y7QUFDL0YsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0ZBQStGO0FBQy9GLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELDBCQUEwQixnQjs7Ozs7Ozs7Ozs7OztBQzdLL0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaVFBQTBKOztBQUUxSjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtFQUErRTs7QUFFL0U7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCwyQkFBMkIsaUI7Ozs7Ozs7Ozs7Ozs7QUMzS2hGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNyRTdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7O0FBR0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyREFBMkQ7QUFDM0Q7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQ0FBK0M7QUFDL0MsOENBQThDO0FBQzlDO0FBQ0EsYUFBYTtBQUNiLFdBQVc7QUFDWCxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELG1DQUFtQyxROzs7Ozs7Ozs7Ozs7O0FDalR4Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7O0FBRTlGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsc0JBQXNCLFk7Ozs7Ozs7Ozs7Ozs7QUMzSjNFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPOztBQUVQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QiwwQkFBMEI7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EscUJBQXFCLHlCQUF5QjtBQUM5QztBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsV0FBVztBQUNYLFNBQVM7QUFDVDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELDJCOzs7Ozs7Ozs7Ozs7O0FDelFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNqQzdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0Esa0dBQWtHOztBQUVsRztBQUNBO0FBQ0EsZ0NBQWdDLGtEQUFrRDtBQUNsRjtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxzQkFBc0IsWTs7Ozs7Ozs7Ozs7OztBQ3hJM0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixlQUFlO0FBQ3RDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLGdCQUFnQjtBQUN2QztBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLGdDQUFnQyxnRUFBZ0U7QUFDaEc7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSwwR0FBMEc7QUFDMUcsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEscUJBQXFCLGVBQWU7QUFDcEM7QUFDQTtBQUNBOztBQUVBLHFEQUFxRCxnQ0FBZ0MsUzs7Ozs7Ozs7Ozs7OztBQ2psQnJGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1GQUFtRjtBQUNuRjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHVCQUF1Qix1QkFBdUI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsc0JBQXNCOztBQUV0QjtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDdEpBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxHQUFHO0FBQ0g7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsZ0NBQWdDLHNDQUFzQztBQUN0RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IseUVBQXlFLGdEQUFnRDtBQUN6SDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxxQkFBcUIsVzs7Ozs7Ozs7Ozs7OztBQ2hpQjFFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4Rjs7QUFFOUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxxQkFBcUIsVzs7Ozs7Ozs7Ozs7OztBQ2xMMUU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELHlCOzs7Ozs7Ozs7Ozs7O0FDckxBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7O0FDaEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSwyQ0FBMkM7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EOzs7Ozs7Ozs7Ozs7O0FDcE5BOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87O0FBRVA7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsMkNBQTJDOztBQUUzQztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EOzs7Ozs7Ozs7Ozs7O0FDNVJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0gsQzs7Ozs7Ozs7Ozs7OztBQ2hEQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsOENBQThDLGlCQUFpQixxQkFBcUIsb0NBQW9DLDZEQUE2RCxvQkFBb0IsRUFBRSxlQUFlOztBQUUxTixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnRkFBZ0Y7O0FBRWhGOztBQUVBLGdGQUFnRixlQUFlOztBQUUvRix5REFBeUQsVUFBVSx1REFBdUQ7QUFDMUg7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLHVCOzs7Ozs7Ozs7Ozs7O0FDN0dBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix1Q0FBdUMsNkJBQTZCLFlBQVksRUFBRSxPQUFPLGlCQUFpQixtQkFBbUIsdUJBQXVCLDRFQUE0RSxFQUFFLEVBQUUsc0JBQXNCLGVBQWUsRUFBRTs7QUFFM1EsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUEsMkJBQTJCOztBQUUzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IscUJBQXFCO0FBQ3ZDO0FBQ0EsT0FBTztBQUNQLGVBQWU7QUFDZjtBQUNBO0FBQ0EsTUFBTTtBQUNOO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0Esd0JBQXdCLHlCQUF5QjtBQUNqRCxRQUFRO0FBQ1I7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLGdCQUFnQjtBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7O0FBR0E7O0FBRUE7O0FBRUEsY0FBYztBQUNkLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDJCQUEyQjs7QUFFM0I7O0FBRUEsMEVBQTBFO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ3RUQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNmQTs7QUFFQTs7QUFFQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSw4RDs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDekRBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsVUFBVSxxQkFBcUI7QUFDNUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFNQTs7Ozs7O0FBRUE7Ozs7OztBQVZBOztBQWdCQSxJQUFNQSxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUNDLE9BQUQ7QUFBQSxTQUFjLEVBQUVDLGlDQUFGLEVBQTJCRCxnQkFBM0IsRUFBZDtBQUFBLENBQXhCOztBQUVBOzs7Ozs7Ozs7Ozs7O0FBYUEsSUFBTUUsb0JBQW9CLFNBQXBCQSxpQkFBb0IsQ0FBQ0YsT0FBRDtBQUFBLFNBQWMsRUFBRUMsbUNBQUYsRUFBNkJELGdCQUE3QixFQUFkO0FBQUEsQ0FBMUI7O0FBRUE7Ozs7Ozs7O0FBUUEsSUFBTUcsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFDSCxPQUFEO0FBQUEsU0FBYyxFQUFFQyxpQ0FBRixFQUEyQkQsZ0JBQTNCLEVBQWQ7QUFBQSxDQUF4Qjs7QUFFQTs7Ozs7OztBQU9BLElBQU1JLE1BQU0sU0FBTkEsR0FBTTtBQUFBLE1BQUdDLEtBQUgsUUFBR0EsS0FBSDtBQUFBLE1BQVVDLEVBQVYsUUFBVUEsRUFBVjtBQUFBLFNBQW1CLFVBQUNDLFFBQUQsRUFBYztBQUMzQ0EsYUFBU1IsZ0JBQWdCLEVBQUVPLE1BQUYsRUFBaEIsQ0FBVDs7QUFFQSwyQkFBVyxFQUFFRCxZQUFGLEVBQVNDLE1BQVQsRUFBWCxFQUNHRSxJQURILENBQ1EsaUJBQW9DO0FBQUEsVUFBakNDLFVBQWlDLFNBQWpDQSxVQUFpQztBQUFBLFVBQXJCQyxLQUFxQixTQUFyQkEsS0FBcUI7QUFBQSxVQUFkVixPQUFjLFNBQWRBLE9BQWM7O0FBQ3hDLFVBQUlTLGNBQWMsR0FBbEIsRUFBdUI7QUFDckJGLGlCQUFTSixnQkFBZ0IsRUFBRUcsTUFBRixFQUFNRyxzQkFBTixFQUFrQkMsWUFBbEIsRUFBaEIsQ0FBVDs7QUFFQTtBQUNEOztBQUVESCxlQUFTTCwyQ0FBb0JJLE1BQXBCLEVBQXdCRyxzQkFBeEIsSUFBdUNULE9BQXZDLEVBQVQ7QUFDRCxLQVRILEVBVUdXLEtBVkgsQ0FVUyxpQkFBMkI7QUFBQSxVQUF4QkYsVUFBd0IsU0FBeEJBLFVBQXdCO0FBQUEsVUFBWkMsS0FBWSxTQUFaQSxLQUFZOztBQUNoQ0gsZUFBU0osZ0JBQWdCLEVBQUVHLE1BQUYsRUFBTUcsc0JBQU4sRUFBa0JDLFlBQWxCLEVBQWhCLENBQVQ7QUFDRCxLQVpIO0FBYUQsR0FoQlc7QUFBQSxDQUFaOztrQkFrQmVOLEc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvRGY7O0FBZUE7Ozs7Ozs7Ozs7OztBQVlBLElBQU1RLFNBQVMsU0FBVEEsTUFBUyxDQUFDWixPQUFEO0FBQUEsU0FBK0IsRUFBRUMsd0JBQUYsRUFBcUJELGdCQUFyQixFQUEvQjtBQUFBLENBQWYsQyxDQWhDQTs7Ozs7a0JBa0NlWSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0JmOzs7Ozs7O0FBTEE7OztzRkFZQTtBQUFBLFFBQXlCUCxLQUF6QixTQUF5QkEsS0FBekI7QUFBQSxRQUFnQ0MsRUFBaEMsU0FBZ0NBLEVBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVPLGVBRlYscUNBRXdDUCxFQUZ4QztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNTyxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVo7QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVhLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xWLDBCQUFZVSxNQURQO0FBRUxULHFCQUFPVTtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJRyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRWixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlYSxPOzs7OztBQVZmOzs7O0FBQ0E7Ozs7a0JBNENlQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUNmOzs7O0FBRUE7Ozs7QUFvQkE7Ozs7Ozs7Ozs7Ozs7QUEzQkE7Ozs7OztzRkF3Q2U7QUFBQSxRQUNibEIsS0FEYSxTQUNiQSxLQURhO0FBQUEsUUFFYm1CLE1BRmEsU0FFYkEsTUFGYTtBQUFBLFFBR2JDLE1BSGEsU0FHYkEsTUFIYTtBQUFBLFFBSWJDLFFBSmEsU0FJYkEsUUFKYTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9MYixlQVBLLGtDQU9zQlcsTUFQdEIsZUFPc0NDLE1BUHRDO0FBQUE7QUFBQSxtQkFTTywrQkFBTVosR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxNQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUDtBQUNBQywrQkFBZVo7QUFIUixlQUZrQjtBQU8zQnNCLG9CQUFNRDtBQVBxQixhQUFYLENBVFA7O0FBQUE7QUFTTFIsZUFUSztBQW1CSEMsa0JBbkJHLEdBbUJvQkQsR0FuQnBCLENBbUJIQyxNQW5CRyxFQW1CS0MsVUFuQkwsR0FtQm9CRixHQW5CcEIsQ0FtQktFLFVBbkJMOztBQUFBLGtCQXFCUEQsVUFBVSxHQXJCSDtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FzQkY7QUFDTFYsMEJBQVlVLE1BRFA7QUFFTFQscUJBQU9VO0FBRkYsYUF0QkU7O0FBQUE7QUFBQTtBQUFBLG1CQTRCUUYsSUFBSUcsSUFBSixFQTVCUjs7QUFBQTtBQTRCTEEsZ0JBNUJLO0FBQUEsd0VBOEJDQSxJQTlCRDs7QUFBQTtBQUFBO0FBQUE7O0FBZ0NYQyxvQkFBUVosS0FBUjs7QUFoQ1csNkNBa0NKO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQWxDSTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztXQUFla0IsTTs7OztTQUFBQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQzlCOzs7Ozs7Ozs7Ozs7Ozs7OztBQVJBOzs7Ozs7c0ZBeUJBO0FBQUEsUUFDRUosTUFERixTQUNFQSxNQURGO0FBQUEsUUFFRW5CLEtBRkYsU0FFRUEsS0FGRjtBQUFBLFFBR0VvQixNQUhGLFNBR0VBLE1BSEY7QUFBQSxRQUlFSSxLQUpGLFNBSUVBLEtBSkY7QUFBQSxRQUtFQyxXQUxGLFNBS0VBLFdBTEY7QUFBQSxRQU1FQyxJQU5GLFNBTUVBLElBTkY7QUFBQSxRQU9FQyxLQVBGLFNBT0VBLEtBUEY7QUFBQSxRQVFFQyxPQVJGLFNBUUVBLE9BUkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFXVXBCLGVBWFYsa0NBV3FDVyxNQVhyQyxlQVdxREMsTUFYckQ7QUFBQTtBQUFBLG1CQWFzQiwrQkFBTVosR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVaO0FBSFIsZUFGa0I7QUFPM0I7QUFDQTtBQUNBc0Isb0JBQU0seUJBQWUsRUFBRUUsWUFBRixFQUFTQyx3QkFBVCxFQUFzQkMsVUFBdEIsRUFBNEJDLFlBQTVCLEVBQW1DQyxnQkFBbkMsRUFBZjtBQVRxQixhQUFYLENBYnRCOztBQUFBO0FBYVVmLGVBYlY7QUF5QllDLGtCQXpCWixHQXlCbUNELEdBekJuQyxDQXlCWUMsTUF6QlosRUF5Qm9CQyxVQXpCcEIsR0F5Qm1DRixHQXpCbkMsQ0F5Qm9CRSxVQXpCcEI7O0FBQUEsa0JBMkJRRCxVQUFVLEdBM0JsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0E0QmE7QUFDTFYsMEJBQVlVLE1BRFA7QUFFTFQscUJBQU9VO0FBRkYsYUE1QmI7O0FBQUE7QUFBQTtBQUFBLG1CQWtDdUJGLElBQUlHLElBQUosRUFsQ3ZCOztBQUFBO0FBa0NVQSxnQkFsQ1Y7QUFBQSx3RUFvQ2dCQSxJQXBDaEI7O0FBQUE7QUFBQTtBQUFBOztBQXNDSUMsb0JBQVFaLEtBQVI7O0FBdENKLDZDQXdDVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUF4Q1g7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVFLE07Ozs7O0FBcEJmOzs7O0FBQ0E7Ozs7a0JBa0VlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0RmOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBLElBQU1zQixTQUFTO0FBQ2JDLFNBQU8sRUFETTtBQUViQyxZQUFVO0FBQ1JDLFdBQU8sTUFEQztBQUVSQyxZQUFRLE1BRkE7QUFHUkMsWUFBUSxNQUhBO0FBSVI7QUFDQUMsWUFBUSxNQUxBO0FBTVJDLFVBQU07QUFORTtBQUZHLENBQWYsQyxDQWRBOzs7Ozs7Ozs7QUEwQkEsSUFBTUMsY0FBYyxTQUFkQSxXQUFjLE9BUWQ7QUFBQSxNQVBKQyxPQU9JLFFBUEpBLE9BT0k7QUFBQSxNQU5KQyxRQU1JLFFBTkpBLFFBTUk7QUFBQSxNQUxKQyxXQUtJLFFBTEpBLFdBS0k7QUFBQSxNQUpKVixLQUlJLFFBSkpBLEtBSUk7QUFBQSxNQUhKVyxRQUdJLFFBSEpBLFFBR0k7QUFBQSxNQUZKQyxTQUVJLFFBRkpBLFNBRUk7QUFBQSxNQURKQyxTQUNJLFFBREpBLFNBQ0k7O0FBQ0osTUFBSUosUUFBSixFQUFjO0FBQ1osV0FBTztBQUFBO0FBQUEsUUFBSyxXQUFXRCxRQUFRUixLQUF4QjtBQUFnQ0E7QUFBaEMsS0FBUDtBQUNEOztBQUVELFNBQ0U7QUFDRSxpQkFBYVUsV0FEZjtBQUVFLFdBQU9WLEtBRlQ7QUFHRSxjQUFVVyxRQUhaO0FBSUUsZUFBV0gsUUFBUVAsUUFKckI7QUFLRSxlQUFXVyxhQUFhLENBTDFCO0FBTUUsZUFBV0MsYUFBYSxHQU4xQjtBQU9FLFVBQU0sQ0FQUjtBQVFFLGNBQVVKO0FBUlosSUFERjtBQVlELENBekJEOztBQTJCQUYsWUFBWU8sU0FBWixHQUF3QjtBQUN0Qk4sV0FBUyxvQkFBVU8sTUFBVixDQUFpQkMsVUFESjtBQUV0QlAsWUFBVSxvQkFBVVEsSUFBVixDQUFlRCxVQUZIO0FBR3RCTixlQUFhLG9CQUFVUSxNQUFWLENBQWlCRixVQUhSO0FBSXRCaEIsU0FBTyxvQkFBVWtCLE1BSks7QUFLdEJQLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUgsVUFMSDtBQU10QkosYUFBVyxvQkFBVVEsTUFOQztBQU90QlAsYUFBVyxvQkFBVU87QUFQQyxDQUF4Qjs7QUFVQWIsWUFBWWMsWUFBWixHQUEyQjtBQUN6QlgsZUFBYSwwQkFEWTtBQUV6QkQsWUFBVSxJQUZlO0FBR3pCRyxhQUFXLENBSGM7QUFJekJDLGFBQVc7QUFKYyxDQUEzQjs7a0JBT2Usd0JBQVdkLE1BQVgsRUFBbUJRLFdBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5RGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUEsSUFBTVIsU0FBUztBQUNiQyxTQUFPLEVBRE07QUFFYkMsWUFBVTtBQUNSQyxXQUFPLE1BREM7QUFFUkMsWUFBUSxNQUZBO0FBR1JDLFlBQVEsTUFIQTtBQUlSO0FBQ0FDLFlBQVEsTUFMQTtBQU1SQyxVQUFNO0FBTkU7QUFGRyxDQUFmLEMsQ0FiQTs7Ozs7Ozs7QUF5QkEsSUFBTWdCLFFBQVEsU0FBUkEsS0FBUSxPQVFSO0FBQUEsTUFQSmQsT0FPSSxRQVBKQSxPQU9JO0FBQUEsTUFOSkMsUUFNSSxRQU5KQSxRQU1JO0FBQUEsTUFMSlQsS0FLSSxRQUxKQSxLQUtJO0FBQUEsTUFKSlUsV0FJSSxRQUpKQSxXQUlJO0FBQUEsTUFISkMsUUFHSSxRQUhKQSxRQUdJO0FBQUEsTUFGSkMsU0FFSSxRQUZKQSxTQUVJO0FBQUEsTUFESkMsU0FDSSxRQURKQSxTQUNJOztBQUNKLE1BQUlKLFFBQUosRUFBYztBQUNaLFdBQU87QUFBQTtBQUFBLFFBQUssV0FBV0QsUUFBUVIsS0FBeEI7QUFBZ0NBO0FBQWhDLEtBQVA7QUFDRDs7QUFFRCxTQUNFO0FBQ0UsaUJBQWFVLFdBRGY7QUFFRSxXQUFPVixLQUZUO0FBR0UsY0FBVVcsUUFIWjtBQUlFLGVBQVdILFFBQVFQLFFBSnJCO0FBS0UsZUFBV1csYUFBYSxDQUwxQjtBQU1FLGVBQVdDLGFBQWEsR0FOMUI7QUFPRSxVQUFNLENBUFI7QUFRRSxjQUFVSjtBQVJaLElBREY7QUFZRCxDQXpCRDs7QUEyQkFhLE1BQU1SLFNBQU4sR0FBa0I7QUFDaEJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFY7QUFFaEJQLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUQsVUFGVDtBQUdoQk4sZUFBYSxvQkFBVVEsTUFBVixDQUFpQkYsVUFIZDtBQUloQmhCLFNBQU8sb0JBQVVrQixNQUFWLENBQWlCRixVQUpSO0FBS2hCTCxZQUFVLG9CQUFVUSxJQUFWLENBQWVILFVBTFQ7QUFNaEJKLGFBQVcsb0JBQVVRLE1BTkw7QUFPaEJQLGFBQVcsb0JBQVVPO0FBUEwsQ0FBbEI7O0FBVUFFLE1BQU1ELFlBQU4sR0FBcUI7QUFDbkJyQixTQUFPLFVBRFk7QUFFbkJVLGVBQWEsb0JBRk07QUFHbkJELFlBQVUsSUFIUztBQUluQkcsYUFBVyxDQUpRO0FBS25CQyxhQUFXO0FBTFEsQ0FBckI7O2tCQVFlLHdCQUFXZCxNQUFYLEVBQW1CdUIsS0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hFZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQU1BOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUF4REE7Ozs7OztBQTBEQSxJQUFNQyxlQUFlLDZCQUFTO0FBQzVCQyxVQUFRO0FBQUEsV0FBTSxnSkFBTjtBQUFBLEdBRG9CO0FBRTVCQyxXQUFTLENBQUMsa0JBQUQsQ0FGbUI7QUFHNUJDO0FBSDRCLENBQVQsQ0FBckI7O0lBTU1DLE87OztBQUNKLG1CQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsd0lBQ1hBLEtBRFc7O0FBQUEsVUErRm5CQyxZQS9GbUIsR0ErRko7QUFBQSxhQUFNLE1BQUtDLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0EvRkk7O0FBQUEsVUFnR25CQyxZQWhHbUIsR0FnR0o7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0FoR0k7O0FBQUEsc0JBRW9CSCxNQUFNSyxJQUYxQjtBQUFBLFFBRVR2QyxLQUZTLGVBRVRBLEtBRlM7QUFBQSxRQUVGQyxXQUZFLGVBRUZBLFdBRkU7QUFBQSxRQUVXQyxJQUZYLGVBRVdBLElBRlg7OztBQUlqQixVQUFLc0MsS0FBTCxHQUFhO0FBQ1hDLFlBQU0sS0FESztBQUVYekMsa0JBRlc7QUFHWEMsOEJBSFc7QUFJWEMsWUFBTSxxQ0FBa0JBLElBQWxCLENBSks7QUFLWHdDLGtCQUFZO0FBTEQsS0FBYjs7QUFRQSxVQUFLekIsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWMwQixJQUFkLE9BQWhCO0FBQ0EsVUFBS0MsTUFBTCxHQUFjLE1BQUtBLE1BQUwsQ0FBWUQsSUFBWixPQUFkO0FBQ0EsVUFBS0UsTUFBTCxHQUFjLE1BQUtBLE1BQUwsQ0FBWUYsSUFBWixPQUFkO0FBQ0EsVUFBS0csa0JBQUwsR0FBMEIsTUFBS0Esa0JBQUwsQ0FBd0JILElBQXhCLE9BQTFCO0FBZmlCO0FBZ0JsQjs7Ozt3Q0FFbUI7QUFBQSxtQkFDUyxLQUFLVCxLQURkO0FBQUEsVUFDVmEsUUFEVSxVQUNWQSxRQURVO0FBQUEsVUFDQVIsSUFEQSxVQUNBQSxJQURBO0FBQUEsMkJBRVlRLFNBQVNDLElBRnJCO0FBQUEsVUFFVnhFLEtBRlUsa0JBRVZBLEtBRlU7QUFBQSxVQUVDbUIsTUFGRCxrQkFFSGxCLEVBRkc7QUFBQSxVQUdWd0UsUUFIVSxHQUdHVixJQUhILENBR1ZVLFFBSFU7OztBQUtsQixVQUFJQyxlQUFKO0FBQ0EsVUFBSUQsYUFBYXRELE1BQWpCLEVBQXlCO0FBQ3ZCdUQsaUJBQVNILFNBQVNDLElBQWxCO0FBQ0QsT0FGRCxNQUVPO0FBQ0xFLGlCQUFTSCxTQUFTRSxRQUFULENBQVQ7QUFDRDs7QUFFRCxVQUFJLENBQUNDLE1BQUQsSUFBWSxDQUFDQSxPQUFPQyxVQUFSLElBQXNCLENBQUNELE9BQU9FLFNBQTlDLEVBQTBEO0FBQ3hELGFBQUtsQixLQUFMLENBQVdtQixvQkFBWCxDQUFnQyxFQUFFN0UsWUFBRixFQUFTQyxJQUFJd0UsUUFBYixFQUFoQztBQUNEO0FBQ0Y7O0FBRUQ7QUFDQTtBQUNBOztBQUVBOzs7OzZCQUNTL0MsSSxFQUFNO0FBQ2IsV0FBS2tDLFFBQUwsQ0FBYyxFQUFFbEMsVUFBRixFQUFkO0FBQ0Q7O0FBRUQ7Ozs7NkJBQ1M7QUFBQSxVQUNDdUMsSUFERCxHQUNVLEtBQUtELEtBRGYsQ0FDQ0MsSUFERDs7QUFFUCxXQUFLTCxRQUFMLENBQWMsRUFBRUssTUFBTSxDQUFDQSxJQUFULEVBQWQ7QUFDRDs7Ozs7Ozs7Ozs7eUJBRTRDLEtBQUtELEssRUFBeENDLEksVUFBQUEsSSxFQUFNekMsSyxVQUFBQSxLLEVBQU9DLFcsVUFBQUEsVyxFQUFhQyxJLFVBQUFBLEk7MEJBRVAsS0FBS2dDLEssRUFBeEJhLFEsV0FBQUEsUSxFQUFVUixJLFdBQUFBLEk7a0NBQ1lRLFNBQVNDLEksRUFBM0JyRCxNLG1CQUFKbEIsRSxFQUFZRCxLLG1CQUFBQSxLO0FBQ1pvQixzQixHQUEyRDJDLEksQ0FBM0QzQyxNLEVBQWUwRCxTLEdBQTRDZixJLENBQW5EdkMsSyxFQUErQnVELGUsR0FBb0JoQixJLENBQWpDdEMsVztBQUU5Qkgsb0IsR0FBTyxFOzs7QUFFWCxvQkFBSUUsVUFBVXNELFNBQWQsRUFBeUI7QUFDdkJ4RCxvREFBWUEsSUFBWixJQUFrQkUsWUFBbEI7QUFDRDs7QUFFRCxvQkFDRSxPQUFPQyxXQUFQLEtBQXVCLFdBQXZCLElBQ0FBLFlBQVl1RCxNQURaLElBRUF2RCxnQkFBZ0JzRCxlQUhsQixFQUlFO0FBQ0F6RCxvREFBWUEsSUFBWixJQUFrQkcsd0JBQWxCO0FBQ0Q7O0FBRURILHFCQUFLSSxJQUFMLEdBQVksMkJBQWFBLEtBQUt1RCxpQkFBTCxFQUFiLENBQVo7Ozt1QkFFb0M7QUFDbEM5RCxnQ0FEa0M7QUFFbENDLGdDQUZrQztBQUdsQ3BCO0FBSGtDLG1CQUkvQnNCLElBSitCLEU7Ozs7QUFBNUJsQiwwQixTQUFBQSxVO0FBQVlDLHFCLFNBQUFBLEs7O3NCQU9oQkQsY0FBYyxHOzs7OztBQUNoQjtBQUNBYSx3QkFBUVosS0FBUixDQUFjQSxLQUFkOzs7OztBQUlGLHFCQUFLcUQsS0FBTCxDQUFXd0IsZ0JBQVgsMEJBQThCOUQsY0FBOUIsSUFBeUNFLElBQXpDOztBQUVBO0FBQ0EscUJBQUtzQyxRQUFMLENBQWMsRUFBRUssTUFBTSxDQUFDQSxJQUFULEVBQWQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt5Q0FHbUI7QUFDbkIsV0FBS0wsUUFBTCxDQUFjLEVBQUVNLFlBQVksSUFBZCxFQUFkO0FBQ0Q7Ozs2QkFLUTtBQUFBOztBQUFBLG9CQUM2QixLQUFLUixLQURsQztBQUFBLFVBQ0NwQixPQURELFdBQ0NBLE9BREQ7QUFBQSxVQUNVeUIsSUFEVixXQUNVQSxJQURWO0FBQUEsVUFDZ0JRLFFBRGhCLFdBQ2dCQSxRQURoQjtBQUFBLFVBRUNDLElBRkQsR0FFVUQsUUFGVixDQUVDQyxJQUZEO0FBQUEsb0JBR3VELEtBQUtSLEtBSDVEO0FBQUEsVUFHQ0gsS0FIRCxXQUdDQSxLQUhEO0FBQUEsVUFHUUksSUFIUixXQUdRQSxJQUhSO0FBQUEsVUFHY3pDLEtBSGQsV0FHY0EsS0FIZDtBQUFBLFVBR3FCQyxXQUhyQixXQUdxQkEsV0FIckI7QUFBQSxVQUdrQ0MsSUFIbEMsV0FHa0NBLElBSGxDO0FBQUEsVUFHd0N3QyxVQUh4QyxXQUd3Q0EsVUFIeEM7QUFBQSxVQUtDaUIsVUFMRCxHQUttQ1gsSUFMbkMsQ0FLQ1csVUFMRDtBQUFBLFVBS2FuRixLQUxiLEdBS21Dd0UsSUFMbkMsQ0FLYXhFLEtBTGI7QUFBQSxVQUt3Qm1CLE1BTHhCLEdBS21DcUQsSUFMbkMsQ0FLb0J2RSxFQUxwQjtBQUFBLFVBTUN3RSxRQU5ELEdBTWdEVixJQU5oRCxDQU1DVSxRQU5EO0FBQUEsVUFNV3JELE1BTlgsR0FNZ0QyQyxJQU5oRCxDQU1XM0MsTUFOWDtBQUFBLFVBTW1CTyxLQU5uQixHQU1nRG9DLElBTmhELENBTW1CcEMsS0FObkI7QUFBQSxVQU0wQnlELFFBTjFCLEdBTWdEckIsSUFOaEQsQ0FNMEJxQixRQU4xQjtBQUFBLFVBTW9DeEQsT0FOcEMsR0FNZ0RtQyxJQU5oRCxDQU1vQ25DLE9BTnBDOzs7QUFRUCxVQUFJeUQscUJBQUo7O0FBRUEsVUFBSUQsYUFBYUUsU0FBakIsRUFBNEI7QUFDMUJELHVCQUFlLG9CQUFZRCxRQUFaLEVBQXNCSixNQUFyQztBQUNEOztBQUVELFVBQUlOLFNBQVMsRUFBRUQsa0JBQUYsRUFBYjs7QUFFQSxVQUFJdEQsV0FBV3NELFFBQWYsRUFBeUI7QUFDdkJDLDRDQUFjQSxNQUFkLEVBQXlCRixJQUF6QjtBQUNELE9BRkQsTUFFTztBQUNMRSw0Q0FBY0EsTUFBZCxFQUF5QkgsU0FBU0UsUUFBVCxDQUF6QjtBQUNEOztBQUVELFVBQUlBLGFBQWF0RCxNQUFiLElBQXVCUyxPQUEzQixFQUFvQyxPQUFPLHFEQUFXLFlBQVksR0FBdkIsR0FBUDs7QUFFcEMsYUFDRTtBQUFBO0FBQUEsVUFBTSxlQUFOLEVBQWdCLFNBQVEsUUFBeEIsRUFBaUMsU0FBUyxDQUExQyxFQUE2QyxXQUFXVSxRQUFRaUQsSUFBaEU7QUFDRTtBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLElBQUksRUFBdkIsRUFBMkIsSUFBSSxDQUEvQixFQUFrQyxJQUFJLENBQXRDLEVBQXlDLElBQUksQ0FBN0M7QUFDRTtBQUFBO0FBQUE7QUFDRSw0QkFBYyxLQUFLNUIsWUFEckI7QUFFRSw0QkFBYyxLQUFLRyxZQUZyQjtBQUdFLHNCQUFRRCxLQUhWO0FBSUUseUJBQVd2QixRQUFRa0Q7QUFKckI7QUFNRTtBQUNFLHNCQUNFO0FBQUE7QUFBQSxrQkFBTyxXQUFXbEQsUUFBUW1ELE1BQTFCLEVBQWtDLFdBQVcsQ0FBN0M7QUFDRTtBQUFBO0FBQUEsb0JBQU0sV0FBU2YsT0FBT2dCLFFBQXRCO0FBQ0U7QUFDRSx5QkFBS2hCLE9BQU9pQixJQURkO0FBRUUseUJBQ0VqQixPQUFPZSxNQUFQLElBQ0E7QUFKSjtBQURGO0FBREYsZUFGSjtBQWNFLHFCQUNFO0FBQUE7QUFBQSxrQkFBTSxXQUFTZixPQUFPZ0IsUUFBdEIsRUFBa0MsV0FBV3BELFFBQVFxRCxJQUFyRDtBQUNHakIsdUJBQU9pQjtBQURWLGVBZko7QUFtQkUseUJBQ0U7QUFBQTtBQUFBLGtCQUFNLFdBQVNqQixPQUFPZ0IsUUFBdEIsRUFBa0MsV0FBV3BELFFBQVFvRCxRQUFyRDtBQUFBO0FBQ0loQix1QkFBT2dCO0FBRFgsZUFwQko7QUF3QkUsc0JBQ0U7QUFBQTtBQUFBLGtCQUFLLFdBQVdwRCxRQUFRc0QsSUFBeEI7QUFDRTtBQUNFLDJCQUFTaEUsT0FEWDtBQUVFLDBCQUFRUixNQUZWO0FBR0UsNEJBQVlxRCxRQUhkO0FBSUUseUJBQU96RSxLQUpUO0FBS0UsMEJBQVFtQixNQUxWO0FBTUUsb0NBQWtCLEtBQUt1QyxLQUFMLENBQVd3QjtBQU4vQixrQkFERjtBQVNFO0FBQUE7QUFBQSxvQkFBUyxPQUFNLGFBQWYsRUFBNkIsV0FBVSxRQUF2QztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBREY7QUFERjtBQVRGO0FBekJKLGNBTkY7QUFpREcvRCx1QkFBV3NELFFBQVgsR0FDQztBQUNFLHlCQUFXbkMsUUFBUXVELEtBRHJCO0FBRUUseUJBQVcsbUJBQUNDLFFBQUQ7QUFBQSx1QkFDVCx1RkFDTUEsUUFETixFQUVNL0IsSUFGTjtBQUdFLHlCQUFPL0QsS0FIVDtBQUlFLG9DQUFrQixPQUFLMEQsS0FBTCxDQUFXd0I7QUFKL0IsbUJBRFM7QUFBQSxlQUZiO0FBVUUscUJBQ0V2RCxTQUNBO0FBWkosY0FERCxHQWlCQztBQUNFLHlCQUFXVyxRQUFRdUQsS0FEckI7QUFFRSxxQkFDRWxFLFNBQ0E7QUFKSixjQWxFSjtBQTBFRTtBQUNFLHFCQUNFO0FBQ0UsMEJBQVUsQ0FBQ3NDLElBRGI7QUFFRSx1QkFBT3pDLEtBRlQ7QUFHRSwwQkFBVSxrQkFBQ3VFLENBQUQ7QUFBQSx5QkFBTyxPQUFLbkMsUUFBTCxDQUFjLEVBQUVwQyxPQUFPdUUsRUFBRUMsTUFBRixDQUFTbEUsS0FBbEIsRUFBZCxDQUFQO0FBQUE7QUFIWixnQkFGSjtBQVFFLHlCQUNFbUMsUUFBUSxPQUFPeEMsV0FBUCxLQUF1QixXQUEvQixHQUNFO0FBQ0UsMEJBQVUsQ0FBQ3dDLElBRGI7QUFFRSx1QkFBT3hDLFdBRlQ7QUFHRSwwQkFBVSxrQkFBQ3NFLENBQUQ7QUFBQSx5QkFDUixPQUFLbkMsUUFBTCxDQUFjO0FBQ1puQyxpQ0FBYXNFLEVBQUVDLE1BQUYsQ0FBU2xFO0FBRFYsbUJBQWQsQ0FEUTtBQUFBO0FBSFosZ0JBREYsR0FVSTtBQW5CUixjQTFFRjtBQWdHR21DLG9CQUFRLE9BQU9GLEtBQUtyQyxJQUFaLEtBQXFCLFdBQTdCLEdBQ0M7QUFBQTtBQUFBO0FBQ0U7QUFDRSw2QkFBYUEsSUFEZjtBQUVFLDBCQUFVLEtBQUtlLFFBRmpCO0FBR0UsMEJBQVUsQ0FBQ3dCLElBSGI7QUFJRSw2QkFBYSxzQkFKZjtBQUtFLGdDQUFnQjtBQUFBLHNCQUFHNUMsUUFBSCxTQUFHQSxRQUFIO0FBQUEseUJBQ2Qsc0JBQW1CO0FBQ2pCckIsZ0NBRGlCO0FBRWpCbUIsa0NBRmlCO0FBR2pCQyxrQ0FIaUI7QUFJakJDO0FBSmlCLG1CQUFuQixDQURjO0FBQUE7QUFMbEI7QUFERixhQURELEdBaUJHLElBakhOO0FBa0hFLG1EQUFLLFdBQVdpQixRQUFRMkQsTUFBeEIsR0FsSEY7QUFtSEU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLGtCQUFZLGNBQVcsa0JBQXZCLEVBQTBDLGNBQTFDO0FBQ0U7QUFERixlQURGO0FBSUU7QUFBQTtBQUFBLGtCQUFZLGNBQVcsWUFBdkIsRUFBb0MsY0FBcEM7QUFDRTtBQURGLGVBSkY7QUFPRTtBQUFBO0FBQUE7QUFDRSxnQ0FBVyxTQURiO0FBRUUsMkJBQVMsS0FBSzNCO0FBRmhCO0FBSUU7QUFBQTtBQUFBLG9CQUFPLGNBQWNlLFlBQXJCO0FBQ0U7QUFERjtBQUpGLGVBUEY7QUFlRSxxREFBSyxXQUFXL0MsUUFBUTRELFFBQXhCLEdBZkY7QUFnQkU7QUFBQTtBQUFBLGtCQUFTLE9BQU0sT0FBZixFQUF1QixXQUFVLFFBQWpDO0FBQ0U7QUFBQTtBQUFBLG9CQUFZLGNBQVcsT0FBdkIsRUFBK0IsY0FBL0I7QUFDRTtBQURGO0FBREY7QUFoQkY7QUFuSEYsV0FERjtBQTJJR2hDLHVCQUFhLDhCQUFDLFlBQUQsSUFBYyxRQUFROUMsTUFBdEIsR0FBYixHQUFnRDtBQTNJbkQsU0FERjtBQThJRytELHNCQUNDVixhQUFhdEQsTUFEZCxJQUVHO0FBQUE7QUFBQTtBQUNFLHFCQURGO0FBRUUsbUJBQU0sUUFGUjtBQUdFLDBCQUFXLE1BSGI7QUFJRSx1QkFBV21CLFFBQVE2RCxVQUpyQjtBQUtFLHFCQUFTbEMsT0FBTyxLQUFLSSxNQUFaLEdBQXFCLEtBQUtEO0FBTHJDO0FBT0dILGlCQUFPLG1EQUFQLEdBQXNCO0FBUHpCO0FBaEpOLE9BREY7QUE2SkQ7Ozs7O0FBR0hSLFFBQVFiLFNBQVIsR0FBb0I7QUFDbEJOLFdBQVMseUJBQVVPLE1BQVYsQ0FBaUJDLFVBRFI7O0FBR2xCeUIsWUFBVSx5QkFBVTFCLE1BQVYsQ0FBaUJDLFVBSFQ7QUFJbEJpQixRQUFNLHlCQUFVcUMsS0FBVixDQUFnQjtBQUNwQmhGLFlBQVEseUJBQVU4QixNQUFWLENBQWlCSixVQURMO0FBRXBCdEIsV0FBTyx5QkFBVXdCLE1BQVYsQ0FBaUJGLFVBRko7QUFHcEJyQixpQkFBYSx5QkFBVXVCLE1BSEg7QUFJcEJ0QixVQUFNLHlCQUFVbUI7QUFKSSxHQUFoQixDQUpZOztBQVdsQmdDLHdCQUFzQix5QkFBVTVCLElBQVYsQ0FBZUgsVUFYbkI7QUFZbEJvQyxvQkFBa0IseUJBQVVqQyxJQUFWLENBQWVIO0FBWmYsQ0FBcEI7O0FBZUEsSUFBTXVELGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHOUIsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBbUIsRUFBRUEsa0JBQUYsRUFBbkI7QUFBQSxDQUF4Qjs7QUFFQSxJQUFNK0IscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ3BHLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFMkUsMkNBREY7QUFFRUs7QUFGRixHQURGLEVBS0VoRixRQUxGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVNlLHlCQUFRbUcsZUFBUixFQUF5QkMsa0JBQXpCLEVBQ2IsbURBQW1CN0MsT0FBbkIsQ0FEYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDclhmOzs7Ozs7QUFNQSxJQUFNNUIsU0FBUyxTQUFUQSxNQUFTLENBQUMwRSxLQUFEO0FBQUE7O0FBQUEsU0FBWTtBQUN6QmhCLFVBQU07QUFDSmlCLGVBQVM7QUFETCxLQURtQjtBQUl6QmIsVUFBTTtBQUNKO0FBQ0FjLGNBQVEsU0FGSjtBQUdKQyxzQkFBZ0I7QUFIWixLQUptQjtBQVN6QmpCLFlBQVE7QUFDTmtCLG9CQUFjO0FBRFIsS0FUaUI7QUFZekJuQixVQUFNO0FBQ0ptQixvQkFBYztBQURWLEtBWm1CO0FBZXpCakIsY0FBVTtBQUNSO0FBQ0FlLGNBQVEsU0FGQTtBQUdSQyxzQkFBZ0I7QUFIUixLQWZlO0FBb0J6QmIsV0FBTztBQUNMNUQsY0FBUTtBQURILEtBcEJrQjtBQXVCekJpRSxjQUFVO0FBQ1JOLFlBQU07QUFERSxLQXZCZTtBQTBCekJPO0FBQ0VTLGdCQUFVLE9BRFo7QUFFRUMsY0FBUTtBQUZWLGtEQUdHTixNQUFNTyxXQUFOLENBQWtCQyxFQUFsQixDQUFxQixJQUFyQixDQUhILEVBR2dDO0FBQzVCQyxjQUFRLE1BRG9CO0FBRTVCQyxhQUFPO0FBRnFCLEtBSGhDLDhDQU9HVixNQUFNTyxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQVBILEVBT2tDO0FBQzlCRixjQUFRLE1BRHNCO0FBRTlCQyxhQUFPO0FBRnVCLEtBUGxDLDhDQVdHVixNQUFNTyxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQVhILEVBV2tDO0FBQzlCRixjQUFRLE1BRHNCO0FBRTlCQyxhQUFPO0FBRnVCLEtBWGxDLDhDQWVHVixNQUFNTyxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQWZILEVBZWtDO0FBQzlCQyxlQUFTLE1BRHFCO0FBRTlCSCxjQUFRLE1BRnNCO0FBRzlCQyxhQUFPO0FBSHVCLEtBZmxDLGVBMUJ5QjtBQStDekJoQixZQUFRO0FBQ04vRCxjQUFRLGlDQURGO0FBRU5rRixvQkFBYztBQUZSLEtBL0NpQjtBQW1EekJ4QixVQUFNO0FBQ0p1QixlQUFTLE1BREw7QUFFSkUscUJBQWU7QUFGWDtBQW5EbUIsR0FBWjtBQUFBLENBQWY7O2tCQXlEZXhGLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNEZjs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQVhBOzs7O0lBYU15RixVOzs7QUFNSixzQkFBWTVELEtBQVosRUFBbUI7QUFBQTs7QUFBQTs7QUFBQSw4SUFDWEEsS0FEVzs7QUFBQSxVQWFuQmpCLFFBYm1CO0FBQUEsMEZBYVIsaUJBQU9zRCxDQUFQO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDVCxvQkFBSUEsQ0FBSixFQUFPO0FBQ0xBLG9CQUFFd0IsY0FBRjtBQUNEOztBQUVLQyxvQkFMRyxHQUtJekIsRUFBRUMsTUFBRixDQUFTeUIsS0FBVCxDQUFlLENBQWYsQ0FMSjs7QUFBQSxzQkFPTEQsS0FBSzVILElBQUwsQ0FBVThILE9BQVYsQ0FBa0IsUUFBbEIsTUFBZ0MsQ0FQM0I7QUFBQTtBQUFBO0FBQUE7O0FBUURyRyx3QkFSQyxHQVFVLElBQUlzRyxRQUFKLEVBUlY7O0FBU1B0Ryx5QkFBU3VHLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBeUJKLElBQXpCOztBQVRPLDhCQVdxQyxNQUFLOUQsS0FYMUMsRUFXV3ZDLE1BWFgsZUFXQ3NELFFBWEQsRUFXbUJyRCxNQVhuQixlQVdtQkEsTUFYbkIsRUFXMkJwQixLQVgzQixlQVcyQkEsS0FYM0I7QUFBQTtBQUFBLHVCQWFzQyxzQkFBZTtBQUMxREEsOEJBRDBEO0FBRTFEbUIsZ0NBRjBEO0FBRzFEQyxnQ0FIMEQ7QUFJMURDO0FBSjBELGlCQUFmLENBYnRDOztBQUFBO0FBQUE7QUFhQ2pCLDBCQWJELFNBYUNBLFVBYkQ7QUFhYUMscUJBYmIsU0FhYUEsS0FiYjtBQWFvQlYsdUJBYnBCLFNBYW9CQSxPQWJwQjs7O0FBb0JQLG9CQUFJUyxjQUFjLEdBQWxCLEVBQXVCO0FBQ3JCO0FBQ0FhLDBCQUFRWixLQUFSLENBQWNELFVBQWQsRUFBMEJDLEtBQTFCO0FBQ0Q7O0FBRWlCc0IscUJBekJYLEdBeUJxQmhDLE9BekJyQixDQXlCQ2tJLFFBekJEOzs7QUEyQlAsc0NBQWM7QUFDWjdILDhCQURZO0FBRVptQixnQ0FGWTtBQUdaQyxnQ0FIWTtBQUlaTztBQUpZLGlCQUFkOztBQU9BLHNCQUFLK0IsS0FBTCxDQUFXd0IsZ0JBQVgsQ0FBNEI7QUFDMUI5RCxnQ0FEMEI7QUFFMUJPO0FBRjBCLGlCQUE1Qjs7QUFsQ087QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FiUTs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFFakIsVUFBS3FDLEtBQUwsR0FBYSxFQUFiOztBQUVBLFVBQUs4RCxPQUFMLEdBQWUsTUFBS0EsT0FBTCxDQUFhM0QsSUFBYixPQUFmO0FBQ0E7QUFMaUI7QUFNbEI7Ozs7OEJBRVM7QUFDUixXQUFLNEQsS0FBTCxDQUFXakcsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUtpRyxLQUFMLENBQVdDLEtBQVg7QUFDRDs7OzZCQTJDUTtBQUFBOztBQUNQLGFBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRXBCLHNCQUFVO0FBRFosYUFFSyxLQUFLbEQsS0FBTCxDQUFXdUUsS0FGaEIsQ0FERjtBQUtFLHFCQUFXLEtBQUt2RSxLQUFMLENBQVd3RTtBQUx4QjtBQU9FO0FBQUE7QUFBQSxZQUFZLGNBQVcsY0FBdkIsRUFBc0MsU0FBUyxLQUFLSixPQUFwRDtBQUNFLG9FQURGO0FBRUU7QUFDRSxrQkFBSyxNQURQO0FBRUUsb0JBQU8sb0JBRlQ7QUFHRSxzQkFBVSxLQUFLckYsUUFIakI7QUFJRSxpQkFBSyxhQUFDc0YsS0FBRCxFQUFXO0FBQ2QscUJBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNELGFBTkg7QUFPRSxtQkFBTyxFQUFFWixTQUFTLE1BQVg7QUFQVDtBQUZGO0FBUEYsT0FERjtBQXNCRDs7Ozs7QUFuRkdHLFUsQ0FDRzFFLFMsR0FBWTtBQUNqQnFGLFNBQU8sb0JBQVVwRixNQUFWLENBQWlCQyxVQURQO0FBRWpCb0YsYUFBVyxvQkFBVWxGLE1BQVYsQ0FBaUJGO0FBRlgsQztrQkFxRk53RSxVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25HZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNekYsU0FBUztBQUNiMEQsUUFBTTtBQURPLENBQWY7O0lBSU00QyxPOzs7QUFVSixtQkFBWXpFLEtBQVosRUFBbUI7QUFBQTs7QUFBQSx3SUFDWEEsS0FEVzs7QUFBQSxVQU1uQjBFLGNBTm1CLEdBTUYsVUFBQ0MsS0FBRCxFQUFXO0FBQzFCLFlBQUt6RSxRQUFMLENBQWMsRUFBRTBFLFVBQVVELE1BQU1FLGFBQWxCLEVBQWQ7QUFDRCxLQVJrQjs7QUFBQSxVQWtDbkJDLFdBbENtQixHQWtDTCxZQUFNO0FBQ2xCLFlBQUs1RSxRQUFMLENBQWMsRUFBRTBFLFVBQVUsSUFBWixFQUFkO0FBQ0QsS0FwQ2tCOztBQUVqQixVQUFLdEUsS0FBTCxHQUFhLEVBQUVzRSxVQUFVLElBQVosRUFBa0IxRyxTQUFTOEIsTUFBTTlCLE9BQWpDLEVBQWI7QUFDQSxVQUFLNkcsZUFBTCxHQUF1QixNQUFLQSxlQUFMLENBQXFCdEUsSUFBckIsT0FBdkI7QUFIaUI7QUFJbEI7Ozs7Ozs7Ozs7Ozs7eUJBUXFDLEtBQUtULEssRUFBL0J0QyxNLFVBQUFBLE0sRUFBUUQsTSxVQUFBQSxNLEVBQVFuQixLLFVBQUFBLEs7QUFFbEI0Qix1QixHQUFVLENBQUMsS0FBS29DLEtBQUwsQ0FBV3BDLE87O3VCQUVVLHNCQUFjO0FBQ2xEVCxnQ0FEa0Q7QUFFbERuQiw4QkFGa0Q7QUFHbERvQixnQ0FIa0Q7QUFJbERRO0FBSmtELGlCQUFkLEM7Ozs7QUFBOUJ4QiwwQixTQUFBQSxVO0FBQVlzSSx1QixTQUFBQSxPOztzQkFPaEJ0SSxjQUFjLEc7Ozs7Ozs7OztBQUlsQixxQkFBS3dELFFBQUwsQ0FBYyxFQUFFaEMsZ0JBQUYsRUFBZDtBQUNBLHFCQUFLOEIsS0FBTCxDQUFXd0IsZ0JBQVgsQ0FBNEIsRUFBRTlELGNBQUYsRUFBVVEsZ0JBQVYsRUFBNUI7Ozs7Ozs7O0FBRUFYLHdCQUFRMEgsR0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQVFLO0FBQUEsbUJBQ3VCLEtBQUszRSxLQUQ1QjtBQUFBLFVBQ0NzRSxRQURELFVBQ0NBLFFBREQ7QUFBQSxVQUNXMUcsT0FEWCxVQUNXQSxPQURYO0FBQUEsb0JBRTZCLEtBQUs4QixLQUZsQztBQUFBLFVBRUNwQixPQUZELFdBRUNBLE9BRkQ7QUFBQSxVQUVTbUMsUUFGVCxXQUVTQSxRQUZUO0FBQUEsVUFFa0J0RCxNQUZsQixXQUVrQkEsTUFGbEI7OztBQUlQLGFBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQ0UseUJBQVdtSCxXQUFXLGdCQUFYLEdBQThCLElBRDNDO0FBRUUsNkJBQWMsT0FGaEI7QUFHRSxxQkFBUyxLQUFLRixjQUhoQjtBQUlFLHNCQUFVakgsV0FBV3NEO0FBSnZCO0FBTUc3QyxvQkFBVSxzREFBVixHQUE0QjtBQU4vQixTQURGO0FBU0U7QUFBQTtBQUFBO0FBQ0UsZ0JBQUcsZ0JBREw7QUFFRSxzQkFBVTBHLFFBRlo7QUFHRSxrQkFBTU0sUUFBUU4sUUFBUixDQUhSO0FBSUUsd0JBQVk7QUFDVkwscUJBQU87QUFDTGhHLHdCQUFRLEVBREg7QUFFTEQsdUJBQU87QUFGRjtBQURHO0FBSmQ7QUFXRTtBQUFBO0FBQUEsY0FBVSxTQUFTLEtBQUt3RyxXQUF4QjtBQUNFO0FBQUE7QUFBQTtBQUNFLDZCQUFXRixXQUFXLGdCQUFYLEdBQThCLElBRDNDO0FBRUUsaUNBQWMsT0FGaEI7QUFHRSx5QkFBUyxLQUFLRztBQUhoQjtBQUtHLGVBQUM3RyxPQUFELEdBQVcsc0RBQVgsR0FBNkI7QUFMaEM7QUFERjtBQVhGO0FBVEYsT0FERjtBQWlDRDs7Ozs7QUFyRkd1RyxPLENBQ0d2RixTLEdBQVk7QUFDakJOLFdBQVMseUJBQVVPLE1BQVYsQ0FBaUJDLFVBRFQ7QUFFakJsQixXQUFTLHlCQUFVbUIsSUFBVixDQUFlRCxVQUZQO0FBR2pCM0IsVUFBUSx5QkFBVStCLE1BQVYsQ0FBaUJKLFVBSFI7QUFJakIxQixVQUFRLHlCQUFVOEIsTUFBVixDQUFpQkosVUFKUjtBQUtqQjlDLFNBQU8seUJBQVVnRCxNQUFWLENBQWlCRixVQUxQO0FBTWpCb0Msb0JBQWtCLHlCQUFVakMsSUFBVixDQUFlSDtBQU5oQixDO2tCQXVGTiwwQkFBV2pCLE1BQVgsRUFBbUJzRyxPQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RHZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUdBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBVUE7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBUUE7Ozs7QUFDQTs7OztBQUVBOztBQWtCQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7QUFoQ0E7QUFDQTtBQUNBOztBQUVBOztBQTdCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztJQXFETVUsWTs7O0FBQ0o7OztBQXlCQSwwQkFBYztBQUFBOztBQUFBOztBQUVaOztBQUVBOzs7Ozs7QUFKWTs7QUFBQSxVQTZMZEMsbUJBN0xjO0FBQUEsMEZBNkxRLGlCQUFPL0MsQ0FBUDtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ3BCO0FBQ015QixvQkFGYyxHQUVQekIsRUFBRUMsTUFBRixDQUFTeUIsS0FBVCxDQUFlLENBQWYsQ0FGTzs7QUFBQSxzQkFJaEJELEtBQUs1SCxJQUFMLENBQVU4SCxPQUFWLENBQWtCLFFBQWxCLE1BQWdDLENBSmhCO0FBQUE7QUFBQTtBQUFBOztBQUtackcsd0JBTFksR0FLRCxJQUFJc0csUUFBSixFQUxDOztBQU1sQnRHLHlCQUFTdUcsTUFBVCxDQUFnQixPQUFoQixFQUF5QkosSUFBekI7O0FBTmtCO0FBQUEsdUJBUTJCLE1BQUs5RCxLQUFMLENBQVdxRixjQUFYLENBQTBCO0FBQ3JFMUg7QUFEcUUsaUJBQTFCLENBUjNCOztBQUFBO0FBQUE7QUFRVmpCLDBCQVJVLFNBUVZBLFVBUlU7QUFRRUMscUJBUkYsU0FRRUEsS0FSRjtBQVFTVix1QkFSVCxTQVFTQSxPQVJUOztBQUFBLHNCQVlkUyxjQUFjLEdBWkE7QUFBQTtBQUFBO0FBQUE7O0FBYWhCO0FBQ0FhLHdCQUFRWixLQUFSLENBQWNELFVBQWQsRUFBMEJDLEtBQTFCO0FBZGdCOztBQUFBO0FBa0JBMkksbUJBbEJBLEdBa0JRckosT0FsQlIsQ0FrQlZrSSxRQWxCVTtBQW9CVm9CLDJCQXBCVSxHQW9CTSxNQUFLdkYsS0FwQlgsQ0FvQlZ1RixXQXBCVTtBQXNCWkMsNEJBdEJZLEdBc0JHRCxZQUFZaEUsaUJBQVosRUF0Qkg7QUF1QlprRSxzQ0F2QlksR0F1QmFELGFBQWFFLFlBQWIsQ0FDN0Isa0JBQU9DLEtBRHNCLEVBRTdCLFdBRjZCLEVBRzdCLEVBQUVMLFFBQUYsRUFINkIsQ0F2QmI7QUE2QlpNLHlCQTdCWSxHQTZCQUgsdUJBQXVCSSx1QkFBdkIsRUE3QkE7QUErQlpDLGlDQS9CWSxHQStCUSxxQkFBWUMsR0FBWixDQUFnQlIsV0FBaEIsRUFBNkI7QUFDckRTLGtDQUFnQlA7QUFEcUMsaUJBQTdCLENBL0JSO0FBbUNaUSw4QkFuQ1ksR0FtQ0ssMEJBQWlCQyxpQkFBakIsQ0FDckJKLGlCQURxQixFQUVyQkYsU0FGcUIsRUFHckIsR0FIcUIsQ0FuQ0w7OztBQXlDbEIsc0JBQUs3RyxRQUFMLENBQWNrSCxjQUFkOztBQXpDa0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0E3TFI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBVVosVUFBS2xILFFBQUwsR0FBZ0IsVUFBQ3dHLFdBQUQsRUFBaUI7QUFDL0IsWUFBS3ZGLEtBQUwsQ0FBV2pCLFFBQVgsQ0FBb0J3RyxXQUFwQjtBQUNELEtBRkQ7O0FBSUE7Ozs7QUFJQSxVQUFLWSxLQUFMLEdBQWE7QUFBQSxhQUFNLE1BQUtDLFVBQUwsQ0FBZ0JELEtBQWhCLEVBQU47QUFBQSxLQUFiOztBQUVBLFVBQUtFLEtBQUwsR0FBYSxNQUFLQSxLQUFMLENBQVc1RixJQUFYLE9BQWI7QUFDQSxVQUFLNkYsWUFBTCxHQUFvQixNQUFLQSxZQUFMLENBQWtCN0YsSUFBbEIsT0FBcEI7QUFDQSxVQUFLOEYsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCOUYsSUFBakIsT0FBbkI7QUFDQSxVQUFLK0YsWUFBTCxHQUFvQixNQUFLQSxZQUFMLENBQWtCL0YsSUFBbEIsT0FBcEI7QUFDQSxVQUFLZ0csbUJBQUwsR0FBMkIsTUFBS0EsbUJBQUwsQ0FBeUJoRyxJQUF6QixPQUEzQjtBQUNBLFVBQUtpRyxtQkFBTCxHQUEyQixNQUFLQSxtQkFBTCxDQUF5QmpHLElBQXpCLE9BQTNCO0FBQ0EsVUFBS2tHLFdBQUwsR0FBbUIsTUFBS0EsV0FBTCxDQUFpQmxHLElBQWpCLE9BQW5CO0FBQ0EsVUFBS21HLGFBQUwsR0FBcUIsTUFBS0EsYUFBTCxDQUFtQm5HLElBQW5CLE9BQXJCO0FBQ0EsVUFBS29HLGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCcEcsSUFBdEIsT0FBeEI7QUFDQSxVQUFLcUcsZ0JBQUwsR0FBd0IsTUFBS0EsZ0JBQUwsQ0FBc0JyRyxJQUF0QixPQUF4QjtBQUNBLFVBQUtzRyxnQkFBTCxHQUF3QixNQUFLQSxnQkFBTCxDQUFzQnRHLElBQXRCLE9BQXhCO0FBQ0EsVUFBS3VHLGtCQUFMLEdBQTBCLE1BQUtBLGtCQUFMLENBQXdCdkcsSUFBeEIsT0FBMUI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUE1Q1k7QUE2Q2I7O0FBRUQ7Ozs7Ozs7OzswQkFLTTRCLEMsRUFBRztBQUNQQSxRQUFFd0IsY0FBRjtBQURPLFVBRUMwQixXQUZELEdBRWlCLEtBQUt2RixLQUZ0QixDQUVDdUYsV0FGRDs7O0FBSVAsVUFBTVUsaUJBQWlCLG1CQUFVSSxLQUFWLENBQWdCaEUsQ0FBaEIsRUFBbUJrRCxXQUFuQixFQUFnQyxDQUFoQyxDQUF2Qjs7QUFFQSxXQUFLeEcsUUFBTCxDQUFja0gsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O21DQUllO0FBQUEsVUFDTFYsV0FESyxHQUNXLEtBQUt2RixLQURoQixDQUNMdUYsV0FESzs7O0FBR2IsVUFBTVUsaUJBQWlCLG1CQUFVZ0IsZUFBVixDQUEwQjFCLFdBQTFCLEVBQXVDLFlBQXZDLENBQXZCOztBQUVBLFdBQUt4RyxRQUFMLENBQWNrSCxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBS3ZGLEtBRGpCLENBQ0p1RixXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVnQixlQUFWLENBQTBCMUIsV0FBMUIsRUFBdUMsWUFBdkMsQ0FBdkI7O0FBRUEsV0FBS3hHLFFBQUwsQ0FBY2tILGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzttQ0FJZTtBQUFBLFVBQ0xWLFdBREssR0FDVyxLQUFLdkYsS0FEaEIsQ0FDTHVGLFdBREs7OztBQUdiLFVBQU1VLGlCQUFpQixtQkFBVWdCLGVBQVYsQ0FBMEIxQixXQUExQixFQUF1QyxZQUF2QyxDQUF2Qjs7QUFFQSxXQUFLeEcsUUFBTCxDQUFja0gsY0FBZDtBQUNEOztBQUVEOzs7Ozs7OzBDQUlzQjtBQUFBLFVBQ1pWLFdBRFksR0FDSSxLQUFLdkYsS0FEVCxDQUNadUYsV0FEWTs7O0FBR3BCLFVBQU1VLGlCQUFpQixtQkFBVWdCLGVBQVYsQ0FDckIxQixXQURxQixFQUVyQixxQkFGcUIsQ0FBdkI7O0FBS0EsV0FBS3hHLFFBQUwsQ0FBY2tILGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzswQ0FJc0I7QUFBQSxVQUNaVixXQURZLEdBQ0ksS0FBS3ZGLEtBRFQsQ0FDWnVGLFdBRFk7OztBQUdwQixVQUFNVSxpQkFBaUIsbUJBQVVnQixlQUFWLENBQ3JCMUIsV0FEcUIsRUFFckIsbUJBRnFCLENBQXZCOztBQUtBLFdBQUt4RyxRQUFMLENBQWNrSCxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBS3ZGLEtBRGpCLENBQ0p1RixXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVpQixpQkFBVixDQUE0QjNCLFdBQTVCLEVBQXlDLE1BQXpDLENBQXZCOztBQUVBLFdBQUt4RyxRQUFMLENBQWNrSCxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7b0NBSWdCO0FBQUEsVUFDTlYsV0FETSxHQUNVLEtBQUt2RixLQURmLENBQ051RixXQURNOzs7QUFHZCxVQUFNVSxpQkFBaUIsbUJBQVVpQixpQkFBVixDQUE0QjNCLFdBQTVCLEVBQXlDLFFBQXpDLENBQXZCOztBQUVBLFdBQUt4RyxRQUFMLENBQWNrSCxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7dUNBSW1CO0FBQUEsVUFDVFYsV0FEUyxHQUNPLEtBQUt2RixLQURaLENBQ1R1RixXQURTOzs7QUFHakIsVUFBTVUsaUJBQWlCLG1CQUFVaUIsaUJBQVYsQ0FDckIzQixXQURxQixFQUVyQixXQUZxQixDQUF2Qjs7QUFLQSxXQUFLeEcsUUFBTCxDQUFja0gsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O3VDQUltQjtBQUFBLFVBQ1RWLFdBRFMsR0FDTyxLQUFLdkYsS0FEWixDQUNUdUYsV0FEUzs7O0FBR2pCLFVBQU1VLGlCQUFpQixtQkFBVWlCLGlCQUFWLENBQTRCM0IsV0FBNUIsRUFBeUMsTUFBekMsQ0FBdkI7O0FBRUEsV0FBS3hHLFFBQUwsQ0FBY2tILGNBQWQ7QUFDRDs7QUFFRDs7Ozs7O3lDQUdxQjtBQUNuQixXQUFLNUIsS0FBTCxDQUFXakcsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUtpRyxLQUFMLENBQVdDLEtBQVg7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFnREE7Ozs7OztxQ0FNaUI2QyxPLEVBQVM7QUFBQSxVQUNoQjVCLFdBRGdCLEdBQ0EsS0FBS3ZGLEtBREwsQ0FDaEJ1RixXQURnQjs7O0FBR3hCLFVBQU1VLGlCQUFpQixtQkFBVWMsZ0JBQVYsQ0FBMkJ4QixXQUEzQixFQUF3QzRCLE9BQXhDLENBQXZCOztBQUVBLFVBQUlsQixjQUFKLEVBQW9CO0FBQ2xCLGFBQUtsSCxRQUFMLENBQWNrSCxjQUFkO0FBQ0E7QUFDRDs7QUFFRDtBQUNEOztBQUVEOzs7O29DQUNnQm1CLEssRUFBTztBQUNyQixjQUFRQSxNQUFNQyxPQUFOLEVBQVI7QUFDRSxhQUFLLGtCQUFPQyxNQUFaO0FBQ0UsaUJBQU87QUFDTEMsdUNBREs7QUFFTEMsc0JBQVU7QUFGTCxXQUFQOztBQUtGO0FBQ0UsaUJBQU8sSUFBUDtBQVJKO0FBVUQ7QUFDRDs7QUFFQTs7OztpQ0FDYUosSyxFQUFPO0FBQ2xCLGNBQVFBLE1BQU1DLE9BQU4sRUFBUjtBQUNFLGFBQUssWUFBTDtBQUNFLGlCQUFPLHVCQUFQOztBQUVGO0FBQ0UsaUJBQU8sSUFBUDtBQUxKO0FBT0Q7QUFDRDs7Ozs2QkFFUztBQUFBOztBQUFBLG1CQU9ILEtBQUtySCxLQVBGO0FBQUEsVUFFTHBCLE9BRkssVUFFTEEsT0FGSztBQUFBLFVBR0xDLFFBSEssVUFHTEEsUUFISztBQUFBLFVBSUxFLFFBSkssVUFJTEEsUUFKSztBQUFBLFVBS0x3RyxXQUxLLFVBS0xBLFdBTEs7QUFBQSxVQU1MekcsV0FOSyxVQU1MQSxXQU5LOztBQVNQOztBQUNBLFVBQU0ySSxXQUFXO0FBQ2ZDLGNBQU07QUFDSkMsMkJBQWlCLHFCQURiO0FBRUpDLHNCQUFZLCtDQUZSO0FBR0pDLG9CQUFVLEVBSE47QUFJSi9FLG1CQUFTO0FBSkw7QUFEUyxPQUFqQjs7QUFTQSxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdsRSxRQUFRaUQsSUFBeEI7QUFDRyxTQUFDaEQsUUFBRCxHQUNDO0FBQUE7QUFBQSxZQUFLLFdBQVcsMEJBQVcsRUFBRSxtQkFBbUIsRUFBckIsRUFBWCxDQUFoQjtBQUNFO0FBQUE7QUFBQSxjQUFTLE9BQU0sT0FBZixFQUF1QixJQUFHLE9BQTFCLEVBQWtDLFdBQVUsUUFBNUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QixFQUErQixTQUFTLEtBQUt5SCxZQUE3QztBQUNFO0FBREY7QUFERixXQURGO0FBTUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxNQUFmLEVBQXNCLElBQUcsTUFBekIsRUFBZ0MsV0FBVSxRQUExQztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE1BQXZCLEVBQThCLFNBQVMsS0FBS0ssV0FBNUM7QUFDRTtBQURGO0FBREYsV0FORjtBQVdFO0FBQUE7QUFBQSxjQUFTLE9BQU0sUUFBZixFQUF3QixJQUFHLFFBQTNCLEVBQW9DLFdBQVUsUUFBOUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxRQUF2QixFQUFnQyxTQUFTLEtBQUtDLGFBQTlDO0FBQ0U7QUFERjtBQURGLFdBWEY7QUFnQkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxXQUFmLEVBQTJCLElBQUcsV0FBOUIsRUFBMEMsV0FBVSxRQUFwRDtBQUNFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLFdBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFERixXQWhCRjtBQXdCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE1BQWYsRUFBc0IsSUFBRyxNQUF6QixFQUFnQyxXQUFVLFFBQTFDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsTUFBdkIsRUFBOEIsU0FBUyxLQUFLTixXQUE1QztBQUNFO0FBREY7QUFERixXQXhCRjtBQTZCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE9BQWYsRUFBdUIsSUFBRyxPQUExQixFQUFrQyxXQUFVLFFBQTVDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsT0FBdkIsRUFBK0IsU0FBUyxLQUFLQyxZQUE3QztBQUNFO0FBREY7QUFERixXQTdCRjtBQWtDRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGVBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsZ0JBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFMRixXQWxDRjtBQThDRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsY0FEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQURGLFdBOUNGO0FBc0RFO0FBQUE7QUFBQSxjQUFTLE9BQU0sWUFBZixFQUE0QixJQUFHLFlBQS9CLEVBQTRDLFdBQVUsUUFBdEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxZQUF2QixFQUFvQyxjQUFwQztBQUNFO0FBREY7QUFERixXQXRERjtBQTJERTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsY0FBdkIsRUFBc0MsY0FBdEM7QUFDRTtBQURGO0FBREYsV0EzREY7QUFnRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBaEVGO0FBcUVFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQXJFRjtBQTBFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0ExRUY7QUErRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBL0VGO0FBb0ZFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxjQURiO0FBRUUsMEJBQVUsT0FBTyxLQUFLMUcsS0FBTCxDQUFXcUYsY0FBbEIsS0FBcUMsVUFGakQ7QUFHRSx5QkFBUyxLQUFLMkI7QUFIaEI7QUFLRSx3RUFMRjtBQU1FO0FBQ0Usc0JBQUssTUFEUDtBQUVFLHdCQUFPLG9CQUZUO0FBR0UsMEJBQVUsS0FBSzVCLG1CQUhqQjtBQUlFLHFCQUFLLGFBQUNmLEtBQUQsRUFBVztBQUNkLHlCQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDRCxpQkFOSDtBQU9FLHVCQUFPLEVBQUVaLFNBQVMsTUFBWDtBQVBUO0FBTkY7QUFERixXQXBGRjtBQXNHRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxpQkFEUjtBQUVFLGtCQUFHLGtCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGlCQUF2QixFQUF5QyxjQUF6QztBQUNFO0FBREY7QUFMRixXQXRHRjtBQStHRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGdCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGdCQUF2QixFQUF3QyxjQUF4QztBQUNFO0FBREY7QUFMRixXQS9HRjtBQXdIRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGdCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGdCQURiO0FBRUUseUJBQVMsS0FBS3FEO0FBRmhCO0FBSUU7QUFKRjtBQUxGLFdBeEhGO0FBb0lFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGVBRFI7QUFFRSxrQkFBRyxlQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGVBQXZCLEVBQXVDLGNBQXZDO0FBQ0U7QUFERjtBQUxGO0FBcElGLFNBREQsR0ErSUcsSUFoSk47QUFpSkU7QUFBQTtBQUFBLFlBQUssV0FBVywwQkFBVyxFQUFFLGVBQWUsRUFBakIsRUFBWCxDQUFoQjtBQUNFO0FBQ0U7O0FBREYsY0FHRSxhQUFhdkIsV0FIZjtBQUlFLHNCQUFVeEc7QUFDVjs7QUFMRixjQU9FLGFBQWFEO0FBQ2I7O0FBRUE7O0FBVkYsY0FZRSxpQkFBaUIsS0FBS2dKLGVBWnhCO0FBYUUsMEJBQWMsS0FBS0MsWUFickI7QUFjRSw0QkFBZ0JOO0FBQ2hCOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQXZCRixjQXlCRSxVQUFVNUksUUF6Qlo7QUEwQkU7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFuQ0YsY0FxQ0Usa0JBQWtCLEtBQUtrSTtBQUN2Qjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFsREYsY0FvREUsT0FBTyxLQUFLVjtBQUNaOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQTNFRixjQTZFRSxLQUFLLGFBQUMyQixJQUFELEVBQVU7QUFDYixxQkFBSzVCLFVBQUwsR0FBa0I0QixJQUFsQjtBQUNEO0FBL0VIO0FBREY7QUFqSkYsT0FERjtBQXVPRDs7Ozs7QUE1akJIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBM0RBO0FBUkE7O0FBNkVNN0MsWSxDQUlHakcsUyxHQUFZO0FBQ2pCTixXQUFTLHlCQUFVTyxNQUFWLENBQWlCQyxVQURUOztBQUdqQm1HLGVBQWEseUJBQVVwRyxNQUFWLENBQWlCQyxVQUhiO0FBSWpCTCxZQUFVLHlCQUFVUSxJQUFWLENBQWVILFVBSlI7O0FBTWpCTixlQUFhLHlCQUFVUSxNQU5OOztBQVFqQlQsWUFBVSx5QkFBVVEsSUFBVixDQUFlRCxVQVJSOztBQVVqQjs7OztBQUlBaUcsa0JBQWdCLHlCQUFVOUY7QUFkVCxDO0FBSmY0RixZLENBcUJHMUYsWSxHQUFlO0FBQ3BCWixZQUFVLElBRFU7QUFFcEJDLGVBQWE7QUFGTyxDO2tCQTBoQlQsa0RBQW1CcUcsWUFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxbkJmOztBQUVBOztBQUpBOztBQVdBLElBQU04QyxvQkFBb0IsZ0NBQXVCLENBQy9DO0FBQ0VDLHNDQURGO0FBRUVYO0FBRkYsQ0FEK0MsRUFLL0M7QUFDRVcsdUNBREY7QUFFRVg7QUFGRixDQUwrQyxDQUF2QixDQUExQjs7QUFXTyxJQUFNWSxnREFBb0IsU0FBcEJBLGlCQUFvQixHQUc1QjtBQUFBLE1BRkhDLE9BRUcsdUVBRk8sSUFFUDtBQUFBLE1BREhDLFVBQ0csdUVBRFVKLGlCQUNWOztBQUNILE1BQUlHLFlBQVksSUFBaEIsRUFBc0I7QUFDcEIsV0FBTyxxQkFBWUUsV0FBWixDQUF3QkQsVUFBeEIsQ0FBUDtBQUNEO0FBQ0QsU0FBTyxxQkFBWUUsaUJBQVosQ0FBOEIsNkJBQWVILE9BQWYsQ0FBOUIsRUFBdURDLFVBQXZELENBQVA7QUFDRCxDQVJNOztrQkFVUUYsaUI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hDZjs7Ozs7O2tCQU1lO0FBQ2IsYUFBVztBQUNULHdCQUFvQjtBQUNsQkssa0JBQVksTUFETTtBQUVsQmhLLGNBQVEsZ0JBRlU7QUFHbEJvSixrQkFBWSxrQkFITTtBQUlsQkMsZ0JBQVUsTUFKUTtBQUtsQi9FLGVBQVM7QUFMUyxLQURYO0FBUVQsMEJBQXNCO0FBQ3BCMkYsaUJBQVcsZ0JBRFM7QUFFcEIxRixjQUFRLE1BRlk7QUFHcEI4RSxnQkFBVSxNQUhVO0FBSXBCYSxpQkFBVztBQUpTLEtBUmI7QUFjVCwyQ0FBdUM7QUFDckNuRyxjQUFRLGVBRDZCO0FBRXJDTyxlQUFTO0FBRjRCLEtBZDlCO0FBa0JULG1DQUErQjtBQUM3QlAsY0FBUSxlQURxQjtBQUU3Qk8sZUFBUztBQUNUO0FBSDZCLEtBbEJ0QjtBQXVCVCw4QkFBMEI7QUFDeEI2RSx1QkFBaUIsZ0JBRE87QUFFeEJnQixrQkFBWSxnQkFGWTtBQUd4QkMsYUFBTyxNQUhpQjtBQUl4QmhCLGtCQUFZLGtDQUpZO0FBS3hCaUIsaUJBQVcsUUFMYTtBQU14QnRHLGNBQVEsUUFOZ0I7QUFPeEJPLGVBQVM7QUFQZSxLQXZCakI7QUFnQ1QscUNBQWlDO0FBQy9CNkUsdUJBQWlCLHFCQURjO0FBRS9CQyxrQkFBWSwrQ0FGbUI7QUFHL0JDLGdCQUFVLE1BSHFCO0FBSS9CL0UsZUFBUztBQUpzQjtBQWhDeEIsR0FERTtBQXdDYmpCLFFBQU07QUFDSjtBQURJLEdBeENPO0FBMkNiVyxZQUFVO0FBQ1JOLFVBQU07QUFERTtBQTNDRyxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNEZjs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQSxJQUFNL0QsU0FBUztBQUNia0csU0FBTztBQUNML0YsV0FBTyxNQURGO0FBRUw7QUFDQTtBQUNBd0ssZ0JBQVk7QUFKUDtBQURNLENBQWY7O0FBU0E7Ozs7Ozs7O0FBbkJBOzs7OztJQTJCTUMsTTs7O0FBTUosa0JBQVkvSSxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtNLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUVRO0FBQUEsbUJBQ3lCLEtBQUtOLEtBRDlCO0FBQUEsVUFDQ3dGLFlBREQsVUFDQ0EsWUFERDtBQUFBLFVBQ2U0QixLQURmLFVBQ2VBLEtBRGY7OztBQUdQLFVBQU00QixTQUFTeEQsYUFBYXlELFNBQWIsQ0FBdUI3QixNQUFNOEIsV0FBTixDQUFrQixDQUFsQixDQUF2QixDQUFmOztBQUhPLDRCQUlTRixPQUFPRyxPQUFQLEVBSlQ7QUFBQSxVQUlDN0QsR0FKRCxtQkFJQ0EsR0FKRDs7QUFLUCxVQUFNcEosT0FBTzhNLE9BQU8zQixPQUFQLEVBQWI7O0FBRUEsVUFBSW5MLFNBQVMsa0JBQU95SixLQUFwQixFQUEyQjtBQUN6QixlQUNFO0FBQUE7QUFBQTtBQUNFLGlEQUFLLEtBQUssS0FBVixFQUFpQixLQUFLTCxHQUF0QixFQUEyQixPQUFPbkgsT0FBT2tHLEtBQXpDO0FBREYsU0FERjtBQUtEOztBQUVELGFBQU8sMENBQVA7QUFDRDs7Ozs7QUEzQkcwRSxNLENBQ0c3SixTLEdBQVk7QUFDakJzRyxnQkFBYyxvQkFBVXJHLE1BQVYsQ0FBaUJDLFVBRGQ7QUFFakJnSSxTQUFPLG9CQUFVZ0MsR0FBVixDQUFjaEs7QUFGSixDO2tCQTZCTjJKLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7UUN2Q0NNLGMsR0FBQUEsYztRQUlBQyxlLEdBQUFBLGU7O0FBcEJoQjs7OztBQUNBOzs7Ozs7QUFIQTtBQUNBO0FBSUEsSUFBTUMsZUFBZSxVQUFyQjtBQUNBLElBQU1DLGdCQUFnQix1QkFBdEI7O0FBRUEsU0FBU0MsYUFBVCxDQUF1QkMsS0FBdkIsRUFBOEJDLFlBQTlCLEVBQTRDQyxRQUE1QyxFQUFzRDtBQUNwRCxNQUFNQyxPQUFPRixhQUFhRyxPQUFiLEVBQWI7QUFDQSxNQUFJQyxpQkFBSjtBQUFBLE1BQ0VDLGNBREY7QUFFQSxTQUFPLENBQUNELFdBQVdMLE1BQU1PLElBQU4sQ0FBV0osSUFBWCxDQUFaLE1BQWtDLElBQXpDLEVBQStDO0FBQzdDRyxZQUFRRCxTQUFTRyxLQUFqQjtBQUNBTixhQUFTSSxLQUFULEVBQWdCQSxRQUFRRCxTQUFTLENBQVQsRUFBWXpJLE1BQXBDO0FBQ0Q7QUFDRjs7QUFFTSxTQUFTK0gsY0FBVCxDQUF3Qk0sWUFBeEIsRUFBc0NDLFFBQXRDLEVBQWdEcEUsWUFBaEQsRUFBOEQ7QUFDbkVpRSxnQkFBY0YsWUFBZCxFQUE0QkksWUFBNUIsRUFBMENDLFFBQTFDO0FBQ0Q7O0FBRU0sU0FBU04sZUFBVCxDQUF5QkssWUFBekIsRUFBdUNDLFFBQXZDLEVBQWlEcEUsWUFBakQsRUFBK0Q7QUFDcEVpRSxnQkFBY0QsYUFBZCxFQUE2QkcsWUFBN0IsRUFBMkNDLFFBQTNDO0FBQ0Q7O0FBRU0sSUFBTU8sa0NBQWEsU0FBYkEsVUFBYTtBQUFBLFNBQVM7QUFBQTtBQUFBLE1BQU0sT0FBTyxnQkFBTUMsTUFBbkI7QUFBNEJwSyxVQUFNcUs7QUFBbEMsR0FBVDtBQUFBLENBQW5COztBQUVBLElBQU1DLG9DQUFjLFNBQWRBLFdBQWM7QUFBQSxTQUFTO0FBQUE7QUFBQSxNQUFNLE9BQU8sZ0JBQU1DLE9BQW5CO0FBQTZCdkssVUFBTXFLO0FBQW5DLEdBQVQ7QUFBQSxDQUFwQjs7a0JBR1E7QUFDYmhCLGdDQURhO0FBRWJjLHdCQUZhOztBQUliYixrQ0FKYTtBQUtiZ0I7QUFMYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQmY7Ozs7Ozs7QUFPQTs7O0FBR08sSUFBTUUsMEJBQVM7QUFDcEJDLFlBQVUsVUFEVTtBQUVwQkMsYUFBVyxVQUZTOztBQUlwQkMsTUFBSSxZQUpnQjtBQUtwQkMsTUFBSSxZQUxnQjtBQU1wQkMsTUFBSSxjQU5nQjtBQU9wQkMsTUFBSSxhQVBnQjtBQVFwQkMsTUFBSSxhQVJnQjtBQVNwQkMsTUFBSSxZQVRnQjs7QUFXcEJDLE1BQUksbUJBWGdCO0FBWXBCQyxNQUFJLHFCQVpnQjs7QUFjcEJ4RCxRQUFNLFlBZGM7O0FBZ0JwQnlELGNBQVksWUFoQlE7O0FBa0JwQjdELFVBQVEsUUFsQlk7QUFtQnBCM0IsU0FBTyxjQW5CYTtBQW9CcEJ5RixTQUFPO0FBcEJhLENBQWY7O0FBdUJQOzs7QUFHTyxJQUFNQywwQkFBUztBQUNwQkMsUUFBTSxNQURjO0FBRXBCNUQsUUFBTSxNQUZjO0FBR3BCNkQsVUFBUSxRQUhZO0FBSXBCQyxpQkFBZSxlQUpLO0FBS3BCQyxhQUFXLFdBTFM7QUFNcEJDLGFBQVc7QUFOUyxDQUFmOztBQVNQOzs7QUFHTyxJQUFNQywwQkFBUztBQUNwQkMsUUFBTTtBQURjLENBQWY7O0FBSVA7OztBQUdPLElBQU1DLGdDQUFZLFdBQWxCOztBQUVQOzs7QUFHTyxJQUFNQyw0QkFBVSxTQUFoQjtBQUNBLElBQU1DLG9DQUFjLGFBQXBCOztrQkFFUTtBQUNidkIsZ0JBRGE7QUFFYmEsZ0JBRmE7QUFHYk07QUFIYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdEZjs7Ozs7bUJBRVN4RyxZOzs7Ozs7Ozs7d0JBRUFnRCxpQjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOVDs7QUFFTyxJQUFNNUQsd0JBQVE7QUFDbkIxQyxRQUFNO0FBQ0ppQixhQUFTLEVBREw7QUFFSnhFLFdBQU87QUFGSCxHQURhO0FBS25CME4sVUFBUTtBQUNOeE4sWUFBUSxnQkFERjtBQUVOdUUsWUFBUSxNQUZGO0FBR044RSxjQUFVLEVBSEo7QUFJTm9FLGVBQVcsRUFKTDtBQUtObkosYUFBUztBQUxILEdBTFc7QUFZbkJvSixVQUFRO0FBQ054RCxlQUFXLEVBREw7QUFFTnlELGVBQVc7QUFGTCxHQVpXO0FBZ0JuQi9CLFVBQVE7QUFDTnhCLFdBQU8seUJBREQ7QUFFTndELGVBQVcsS0FGTDtBQUdOQyxpQkFBYTtBQUhQLEdBaEJXO0FBcUJuQjlCLFdBQVM7QUFDUDNCLFdBQU87QUFEQTtBQXJCVSxDQUFkOztrQkEwQlFyRSxLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMUJmOzs7O0FBQ0E7Ozs7OztBQUhBOztBQUtBLElBQU0rSCxVQUFVLFNBQVZBLE9BQVU7QUFBQSxNQUFHL0gsS0FBSCxRQUFHQSxLQUFIO0FBQUEsTUFBVWpHLEtBQVYsUUFBVUEsS0FBVjtBQUFBLE1BQWlCQyxNQUFqQixRQUFpQkEsTUFBakI7QUFBQSxTQUNkO0FBQUE7QUFBQTtBQUNFLFlBQUssU0FEUDtBQUVFLGNBQU8sSUFGVDtBQUdFLGVBQVEsV0FIVjtBQUlFLGFBQU0sSUFKUjtBQUtFLGFBQU07QUFMUjtBQU9FLDRDQUFNLEdBQUUsZUFBUixFQUF3QixNQUFLLE1BQTdCLEdBUEY7QUFRRSw0Q0FBTSxHQUFFLG1PQUFSO0FBUkYsR0FEYztBQUFBLENBQWhCOztBQWFBK04sUUFBUXBOLFNBQVIsR0FBb0I7QUFDbEJaLFNBQU8sb0JBQVVnQixNQURDO0FBRWxCZixVQUFRLG9CQUFVZSxNQUZBO0FBR2xCaUYsU0FBTyxvQkFBVXBGO0FBSEMsQ0FBcEI7O2tCQU1lbU4sTzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3RCZjs7OztBQUNBOzs7Ozs7QUFIQTs7QUFLQSxJQUFNQyxTQUFTLFNBQVRBLE1BQVM7QUFBQSxNQUFHaEksS0FBSCxRQUFHQSxLQUFIO0FBQUEsTUFBVWpHLEtBQVYsUUFBVUEsS0FBVjtBQUFBLE1BQWlCQyxNQUFqQixRQUFpQkEsTUFBakI7QUFBQSxTQUNiO0FBQUE7QUFBQTtBQUNFLFlBQUssU0FEUDtBQUVFLGNBQU8sSUFGVDtBQUdFLGVBQVEsV0FIVjtBQUlFLGFBQU0sSUFKUjtBQUtFLGFBQU07QUFMUjtBQU9FLDRDQUFNLEdBQUUsZUFBUixFQUF3QixNQUFLLE1BQTdCLEdBUEY7QUFRRSw0Q0FBTSxHQUFFLDhTQUFSO0FBUkYsR0FEYTtBQUFBLENBQWY7O0FBYUFnTyxPQUFPck4sU0FBUCxHQUFtQjtBQUNqQlosU0FBTyxvQkFBVWdCLE1BREE7QUFFakJmLFVBQVEsb0JBQVVlLE1BRkQ7QUFHakJpRixTQUFPLG9CQUFVcEY7QUFIQSxDQUFuQjs7a0JBTWVvTixNIiwiZmlsZSI6IjM0LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGFjdGl2ZUVsZW1lbnQ7XG5cbnZhciBfb3duZXJEb2N1bWVudCA9IHJlcXVpcmUoJy4vb3duZXJEb2N1bWVudCcpO1xuXG52YXIgX293bmVyRG9jdW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3duZXJEb2N1bWVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIGFjdGl2ZUVsZW1lbnQoKSB7XG4gIHZhciBkb2MgPSBhcmd1bWVudHMubGVuZ3RoID4gMCAmJiBhcmd1bWVudHNbMF0gIT09IHVuZGVmaW5lZCA/IGFyZ3VtZW50c1swXSA6ICgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoKTtcblxuICB0cnkge1xuICAgIHJldHVybiBkb2MuYWN0aXZlRWxlbWVudDtcbiAgfSBjYXRjaCAoZSkgey8qIGllIHRocm93cyBpZiBubyBhY3RpdmUgZWxlbWVudCAqL31cbn1cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1snZGVmYXVsdCddO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gYWRkQ2xhc3M7XG5cbnZhciBfaGFzQ2xhc3MgPSByZXF1aXJlKCcuL2hhc0NsYXNzJyk7XG5cbnZhciBfaGFzQ2xhc3MyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaGFzQ2xhc3MpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBhZGRDbGFzcyhlbGVtZW50LCBjbGFzc05hbWUpIHtcbiAgaWYgKGVsZW1lbnQuY2xhc3NMaXN0KSBlbGVtZW50LmNsYXNzTGlzdC5hZGQoY2xhc3NOYW1lKTtlbHNlIGlmICghKDAsIF9oYXNDbGFzczIuZGVmYXVsdCkoZWxlbWVudCwgY2xhc3NOYW1lKSkgaWYgKHR5cGVvZiBlbGVtZW50LmNsYXNzTmFtZSA9PT0gJ3N0cmluZycpIGVsZW1lbnQuY2xhc3NOYW1lID0gZWxlbWVudC5jbGFzc05hbWUgKyAnICcgKyBjbGFzc05hbWU7ZWxzZSBlbGVtZW50LnNldEF0dHJpYnV0ZSgnY2xhc3MnLCAoZWxlbWVudC5jbGFzc05hbWUgJiYgZWxlbWVudC5jbGFzc05hbWUuYmFzZVZhbCB8fCAnJykgKyAnICcgKyBjbGFzc05hbWUpO1xufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvY2xhc3MvYWRkQ2xhc3MuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL2NsYXNzL2FkZENsYXNzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IGhhc0NsYXNzO1xuZnVuY3Rpb24gaGFzQ2xhc3MoZWxlbWVudCwgY2xhc3NOYW1lKSB7XG4gIGlmIChlbGVtZW50LmNsYXNzTGlzdCkgcmV0dXJuICEhY2xhc3NOYW1lICYmIGVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKGNsYXNzTmFtZSk7ZWxzZSByZXR1cm4gKFwiIFwiICsgKGVsZW1lbnQuY2xhc3NOYW1lLmJhc2VWYWwgfHwgZWxlbWVudC5jbGFzc05hbWUpICsgXCIgXCIpLmluZGV4T2YoXCIgXCIgKyBjbGFzc05hbWUgKyBcIiBcIikgIT09IC0xO1xufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzW1wiZGVmYXVsdFwiXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9oYXNDbGFzcy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvY2xhc3MvaGFzQ2xhc3MuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIid1c2Ugc3RyaWN0JztcblxuZnVuY3Rpb24gcmVwbGFjZUNsYXNzTmFtZShvcmlnQ2xhc3MsIGNsYXNzVG9SZW1vdmUpIHtcbiAgcmV0dXJuIG9yaWdDbGFzcy5yZXBsYWNlKG5ldyBSZWdFeHAoJyhefFxcXFxzKScgKyBjbGFzc1RvUmVtb3ZlICsgJyg/OlxcXFxzfCQpJywgJ2cnKSwgJyQxJykucmVwbGFjZSgvXFxzKy9nLCAnICcpLnJlcGxhY2UoL15cXHMqfFxccyokL2csICcnKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiByZW1vdmVDbGFzcyhlbGVtZW50LCBjbGFzc05hbWUpIHtcbiAgaWYgKGVsZW1lbnQuY2xhc3NMaXN0KSBlbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoY2xhc3NOYW1lKTtlbHNlIGlmICh0eXBlb2YgZWxlbWVudC5jbGFzc05hbWUgPT09ICdzdHJpbmcnKSBlbGVtZW50LmNsYXNzTmFtZSA9IHJlcGxhY2VDbGFzc05hbWUoZWxlbWVudC5jbGFzc05hbWUsIGNsYXNzTmFtZSk7ZWxzZSBlbGVtZW50LnNldEF0dHJpYnV0ZSgnY2xhc3MnLCByZXBsYWNlQ2xhc3NOYW1lKGVsZW1lbnQuY2xhc3NOYW1lICYmIGVsZW1lbnQuY2xhc3NOYW1lLmJhc2VWYWwgfHwgJycsIGNsYXNzTmFtZSkpO1xufTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9jbGFzcy9yZW1vdmVDbGFzcy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvY2xhc3MvcmVtb3ZlQ2xhc3MuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIlwidXNlIHN0cmljdFwiO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gb3duZXJEb2N1bWVudDtcbmZ1bmN0aW9uIG93bmVyRG9jdW1lbnQobm9kZSkge1xuICByZXR1cm4gbm9kZSAmJiBub2RlLm93bmVyRG9jdW1lbnQgfHwgZG9jdW1lbnQ7XG59XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbXCJkZWZhdWx0XCJdO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSBnZXRXaW5kb3c7XG5mdW5jdGlvbiBnZXRXaW5kb3cobm9kZSkge1xuICByZXR1cm4gbm9kZSA9PT0gbm9kZS53aW5kb3cgPyBub2RlIDogbm9kZS5ub2RlVHlwZSA9PT0gOSA/IG5vZGUuZGVmYXVsdFZpZXcgfHwgbm9kZS5wYXJlbnRXaW5kb3cgOiBmYWxzZTtcbn1cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1tcImRlZmF1bHRcIl07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3F1ZXJ5L2lzV2luZG93LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKHJlY2FsYykge1xuICBpZiAoIXNpemUgJiYgc2l6ZSAhPT0gMCB8fCByZWNhbGMpIHtcbiAgICBpZiAoX2luRE9NMi5kZWZhdWx0KSB7XG4gICAgICB2YXIgc2Nyb2xsRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cbiAgICAgIHNjcm9sbERpdi5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUudG9wID0gJy05OTk5cHgnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLndpZHRoID0gJzUwcHgnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLmhlaWdodCA9ICc1MHB4JztcbiAgICAgIHNjcm9sbERpdi5zdHlsZS5vdmVyZmxvdyA9ICdzY3JvbGwnO1xuXG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcm9sbERpdik7XG4gICAgICBzaXplID0gc2Nyb2xsRGl2Lm9mZnNldFdpZHRoIC0gc2Nyb2xsRGl2LmNsaWVudFdpZHRoO1xuICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChzY3JvbGxEaXYpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBzaXplO1xufTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJy4vaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzaXplID0gdm9pZCAwO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy91dGlsL3Njcm9sbGJhclNpemUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNi41IDZ2MTEuNWMwIDIuMjEtMS43OSA0LTQgNHMtNC0xLjc5LTQtNFY1YzAtMS4zOCAxLjEyLTIuNSAyLjUtMi41czIuNSAxLjEyIDIuNSAyLjV2MTAuNWMwIC41NS0uNDUgMS0xIDFzLTEtLjQ1LTEtMVY2SDEwdjkuNWMwIDEuMzggMS4xMiAyLjUgMi41IDIuNXMyLjUtMS4xMiAyLjUtMi41VjVjMC0yLjIxLTEuNzktNC00LTRTNyAyLjc5IDcgNXYxMi41YzAgMy4wNCAyLjQ2IDUuNSA1LjUgNS41czUuNS0yLjQ2IDUuNS01LjVWNmgtMS41eicgfSk7XG5cbnZhciBBdHRhY2hGaWxlID0gZnVuY3Rpb24gQXR0YWNoRmlsZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQXR0YWNoRmlsZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQXR0YWNoRmlsZSk7XG5BdHRhY2hGaWxlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEF0dGFjaEZpbGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ005LjQgMTYuNkw0LjggMTJsNC42LTQuNkw4IDZsLTYgNiA2IDYgMS40LTEuNHptNS4yIDBsNC42LTQuNi00LjYtNC42TDE2IDZsNiA2LTYgNi0xLjQtMS40eicgfSk7XG5cbnZhciBDb2RlID0gZnVuY3Rpb24gQ29kZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQ29kZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQ29kZSk7XG5Db2RlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvZGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMS45OSA0YzAtMS4xLS44OS0yLTEuOTktMkg0Yy0xLjEgMC0yIC45LTIgMnYxMmMwIDEuMS45IDIgMiAyaDE0bDQgNC0uMDEtMTh6TTE4IDE0SDZ2LTJoMTJ2MnptMC0zSDZWOWgxMnYyem0wLTNINlY2aDEydjJ6JyB9KTtcblxudmFyIENvbW1lbnQgPSBmdW5jdGlvbiBDb21tZW50KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Db21tZW50ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShDb21tZW50KTtcbkNvbW1lbnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29tbWVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db21tZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db21tZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zIDE3LjI1VjIxaDMuNzVMMTcuODEgOS45NGwtMy43NS0zLjc1TDMgMTcuMjV6TTIwLjcxIDcuMDRjLjM5LS4zOS4zOS0xLjAyIDAtMS40MWwtMi4zNC0yLjM0Yy0uMzktLjM5LTEuMDItLjM5LTEuNDEgMGwtMS44MyAxLjgzIDMuNzUgMy43NSAxLjgzLTEuODN6JyB9KTtcblxudmFyIEVkaXQgPSBmdW5jdGlvbiBFZGl0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5FZGl0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShFZGl0KTtcbkVkaXQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRWRpdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FZGl0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FZGl0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjYgMzEgMzQgMzggNDQgNDUgNDcgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiAyMS4zNWwtMS40NS0xLjMyQzUuNCAxNS4zNiAyIDEyLjI4IDIgOC41IDIgNS40MiA0LjQyIDMgNy41IDNjMS43NCAwIDMuNDEuODEgNC41IDIuMDlDMTMuMDkgMy44MSAxNC43NiAzIDE2LjUgMyAxOS41OCAzIDIyIDUuNDIgMjIgOC41YzAgMy43OC0zLjQgNi44Ni04LjU1IDExLjU0TDEyIDIxLjM1eicgfSk7XG5cbnZhciBGYXZvcml0ZSA9IGZ1bmN0aW9uIEZhdm9yaXRlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5GYXZvcml0ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRmF2b3JpdGUpO1xuRmF2b3JpdGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRmF2b3JpdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zhdm9yaXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDAgNDEgNDIgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ003IDE1djJoMTB2LTJIN3ptLTQgNmgxOHYtMkgzdjJ6bTAtOGgxOHYtMkgzdjJ6bTQtNnYyaDEwVjdIN3pNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkNlbnRlciA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduQ2VudGVyKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRBbGlnbkNlbnRlciA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25DZW50ZXIpO1xuRm9ybWF0QWxpZ25DZW50ZXIubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0QWxpZ25DZW50ZXI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMgMjFoMTh2LTJIM3Yyem0wLTRoMTh2LTJIM3Yyem0wLTRoMTh2LTJIM3Yyem0wLTRoMThWN0gzdjJ6bTAtNnYyaDE4VjNIM3onIH0pO1xuXG52YXIgRm9ybWF0QWxpZ25KdXN0aWZ5ID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25KdXN0aWZ5KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRBbGlnbkp1c3RpZnkgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEFsaWduSnVzdGlmeSk7XG5Gb3JtYXRBbGlnbkp1c3RpZnkubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0QWxpZ25KdXN0aWZ5O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE1IDE1SDN2MmgxMnYtMnptMC04SDN2MmgxMlY3ek0zIDEzaDE4di0ySDN2MnptMCA4aDE4di0ySDN2MnpNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkxlZnQgPSBmdW5jdGlvbiBGb3JtYXRBbGlnbkxlZnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduTGVmdCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25MZWZ0KTtcbkZvcm1hdEFsaWduTGVmdC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkxlZnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAyMWgxOHYtMkgzdjJ6bTYtNGgxMnYtMkg5djJ6bS02LTRoMTh2LTJIM3Yyem02LTRoMTJWN0g5djJ6TTMgM3YyaDE4VjNIM3onIH0pO1xuXG52YXIgRm9ybWF0QWxpZ25SaWdodCA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduUmlnaHQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduUmlnaHQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEFsaWduUmlnaHQpO1xuRm9ybWF0QWxpZ25SaWdodC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnblJpZ2h0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTUuNiAxMC43OWMuOTctLjY3IDEuNjUtMS43NyAxLjY1LTIuNzkgMC0yLjI2LTEuNzUtNC00LTRIN3YxNGg3LjA0YzIuMDkgMCAzLjcxLTEuNyAzLjcxLTMuNzkgMC0xLjUyLS44Ni0yLjgyLTIuMTUtMy40MnpNMTAgNi41aDNjLjgzIDAgMS41LjY3IDEuNSAxLjVzLS42NyAxLjUtMS41IDEuNWgtM3YtM3ptMy41IDlIMTB2LTNoMy41Yy44MyAwIDEuNS42NyAxLjUgMS41cy0uNjcgMS41LTEuNSAxLjV6JyB9KTtcblxudmFyIEZvcm1hdEJvbGQgPSBmdW5jdGlvbiBGb3JtYXRCb2xkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRCb2xkID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRCb2xkKTtcbkZvcm1hdEJvbGQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0Qm9sZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEwIDR2M2gyLjIxbC0zLjQyIDhINnYzaDh2LTNoLTIuMjFsMy40Mi04SDE4VjR6JyB9KTtcblxudmFyIEZvcm1hdEl0YWxpYyA9IGZ1bmN0aW9uIEZvcm1hdEl0YWxpYyhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0SXRhbGljID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRJdGFsaWMpO1xuRm9ybWF0SXRhbGljLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEl0YWxpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ000IDEwLjVjLS44MyAwLTEuNS42Ny0xLjUgMS41cy42NyAxLjUgMS41IDEuNSAxLjUtLjY3IDEuNS0xLjUtLjY3LTEuNS0xLjUtMS41em0wLTZjLS44MyAwLTEuNS42Ny0xLjUgMS41UzMuMTcgNy41IDQgNy41IDUuNSA2LjgzIDUuNSA2IDQuODMgNC41IDQgNC41em0wIDEyYy0uODMgMC0xLjUuNjgtMS41IDEuNXMuNjggMS41IDEuNSAxLjUgMS41LS42OCAxLjUtMS41LS42Ny0xLjUtMS41LTEuNXpNNyAxOWgxNHYtMkg3djJ6bTAtNmgxNHYtMkg3djJ6bTAtOHYyaDE0VjVIN3onIH0pO1xuXG52YXIgRm9ybWF0TGlzdEJ1bGxldGVkID0gZnVuY3Rpb24gRm9ybWF0TGlzdEJ1bGxldGVkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRMaXN0QnVsbGV0ZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdExpc3RCdWxsZXRlZCk7XG5Gb3JtYXRMaXN0QnVsbGV0ZWQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0TGlzdEJ1bGxldGVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTIgMTdoMnYuNUgzdjFoMXYuNUgydjFoM3YtNEgydjF6bTEtOWgxVjRIMnYxaDF2M3ptLTEgM2gxLjhMMiAxMy4xdi45aDN2LTFIMy4yTDUgMTAuOVYxMEgydjF6bTUtNnYyaDE0VjVIN3ptMCAxNGgxNHYtMkg3djJ6bTAtNmgxNHYtMkg3djJ6JyB9KTtcblxudmFyIEZvcm1hdExpc3ROdW1iZXJlZCA9IGZ1bmN0aW9uIEZvcm1hdExpc3ROdW1iZXJlZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0TGlzdE51bWJlcmVkID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRMaXN0TnVtYmVyZWQpO1xuRm9ybWF0TGlzdE51bWJlcmVkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdExpc3ROdW1iZXJlZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ002IDE3aDNsMi00VjdINXY2aDN6bTggMGgzbDItNFY3aC02djZoM3onIH0pO1xuXG52YXIgRm9ybWF0UXVvdGUgPSBmdW5jdGlvbiBGb3JtYXRRdW90ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0UXVvdGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdFF1b3RlKTtcbkZvcm1hdFF1b3RlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdFF1b3RlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiAxN2MzLjMxIDAgNi0yLjY5IDYtNlYzaC0yLjV2OGMwIDEuOTMtMS41NyAzLjUtMy41IDMuNVM4LjUgMTIuOTMgOC41IDExVjNINnY4YzAgMy4zMSAyLjY5IDYgNiA2em0tNyAydjJoMTR2LTJINXonIH0pO1xuXG52YXIgRm9ybWF0VW5kZXJsaW5lZCA9IGZ1bmN0aW9uIEZvcm1hdFVuZGVybGluZWQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdFVuZGVybGluZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdFVuZGVybGluZWQpO1xuRm9ybWF0VW5kZXJsaW5lZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRVbmRlcmxpbmVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTggNEg2djJsNi41IDZMNiAxOHYyaDEydi0zaC03bDUtNS01LTVoN3onIH0pO1xuXG52YXIgRnVuY3Rpb25zID0gZnVuY3Rpb24gRnVuY3Rpb25zKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5GdW5jdGlvbnMgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZ1bmN0aW9ucyk7XG5GdW5jdGlvbnMubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRnVuY3Rpb25zO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYgMTRsMyAzdjVoNnYtNWwzLTNWOUg2em01LTEyaDJ2M2gtMnpNMy41IDUuODc1TDQuOTE0IDQuNDZsMi4xMiAyLjEyMkw1LjYyIDcuOTk3em0xMy40Ni43MWwyLjEyMy0yLjEyIDEuNDE0IDEuNDE0TDE4LjM3NSA4eicgfSk7XG5cbnZhciBIaWdobGlnaHQgPSBmdW5jdGlvbiBIaWdobGlnaHQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkhpZ2hsaWdodCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSGlnaGxpZ2h0KTtcbkhpZ2hsaWdodC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBIaWdobGlnaHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjAgMkg0Yy0xLjEgMC0yIC45LTIgMnYxMmMwIDEuMS45IDIgMiAyaDE0bDQgNFY0YzAtMS4xLS45LTItMi0yem0tMiAxMkg2di0yaDEydjJ6bTAtM0g2VjloMTJ2MnptMC0zSDZWNmgxMnYyeicgfSk7XG5cbnZhciBJbnNlcnRDb21tZW50ID0gZnVuY3Rpb24gSW5zZXJ0Q29tbWVudChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0Q29tbWVudCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSW5zZXJ0Q29tbWVudCk7XG5JbnNlcnRDb21tZW50Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydENvbW1lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0Q29tbWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0Q29tbWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMS45OSAyQzYuNDcgMiAyIDYuNDggMiAxMnM0LjQ3IDEwIDkuOTkgMTBDMTcuNTIgMjIgMjIgMTcuNTIgMjIgMTJTMTcuNTIgMiAxMS45OSAyek0xMiAyMGMtNC40MiAwLTgtMy41OC04LThzMy41OC04IDgtOCA4IDMuNTggOCA4LTMuNTggOC04IDh6bTMuNS05Yy44MyAwIDEuNS0uNjcgMS41LTEuNVMxNi4zMyA4IDE1LjUgOCAxNCA4LjY3IDE0IDkuNXMuNjcgMS41IDEuNSAxLjV6bS03IDBjLjgzIDAgMS41LS42NyAxLjUtMS41UzkuMzMgOCA4LjUgOCA3IDguNjcgNyA5LjUgNy42NyAxMSA4LjUgMTF6bTMuNSA2LjVjMi4zMyAwIDQuMzEtMS40NiA1LjExLTMuNUg2Ljg5Yy44IDIuMDQgMi43OCAzLjUgNS4xMSAzLjV6JyB9KTtcblxudmFyIEluc2VydEVtb3RpY29uID0gZnVuY3Rpb24gSW5zZXJ0RW1vdGljb24ocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydEVtb3RpY29uID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRFbW90aWNvbik7XG5JbnNlcnRFbW90aWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRFbW90aWNvbjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRFbW90aWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMy45IDEyYzAtMS43MSAxLjM5LTMuMSAzLjEtMy4xaDRWN0g3Yy0yLjc2IDAtNSAyLjI0LTUgNXMyLjI0IDUgNSA1aDR2LTEuOUg3Yy0xLjcxIDAtMy4xLTEuMzktMy4xLTMuMXpNOCAxM2g4di0ySDh2MnptOS02aC00djEuOWg0YzEuNzEgMCAzLjEgMS4zOSAzLjEgMy4xcy0xLjM5IDMuMS0zLjEgMy4xaC00VjE3aDRjMi43NiAwIDUtMi4yNCA1LTVzLTIuMjQtNS01LTV6JyB9KTtcblxudmFyIEluc2VydExpbmsgPSBmdW5jdGlvbiBJbnNlcnRMaW5rKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRMaW5rID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRMaW5rKTtcbkluc2VydExpbmsubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0TGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTIxIDE5VjVjMC0xLjEtLjktMi0yLTJINWMtMS4xIDAtMiAuOS0yIDJ2MTRjMCAxLjEuOSAyIDIgMmgxNGMxLjEgMCAyLS45IDItMnpNOC41IDEzLjVsMi41IDMuMDFMMTQuNSAxMmw0LjUgNkg1bDMuNS00LjV6JyB9KTtcblxudmFyIEluc2VydFBob3RvID0gZnVuY3Rpb24gSW5zZXJ0UGhvdG8ocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydFBob3RvID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRQaG90byk7XG5JbnNlcnRQaG90by5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRQaG90bztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDI0IDMxIDM0IDM4IDM5IDQ0IDQ1IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTIgOGMxLjEgMCAyLS45IDItMnMtLjktMi0yLTItMiAuOS0yIDIgLjkgMiAyIDJ6bTAgMmMtMS4xIDAtMiAuOS0yIDJzLjkgMiAyIDIgMi0uOSAyLTItLjktMi0yLTJ6bTAgNmMtMS4xIDAtMiAuOS0yIDJzLjkgMiAyIDIgMi0uOSAyLTItLjktMi0yLTJ6JyB9KTtcblxudmFyIE1vcmVWZXJ0ID0gZnVuY3Rpb24gTW9yZVZlcnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbk1vcmVWZXJ0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShNb3JlVmVydCk7XG5Nb3JlVmVydC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBNb3JlVmVydDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDI3IDI4IDMxIDM0IDM1IDM3IDQwIDQxIDQyIDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTcgM0g1Yy0xLjExIDAtMiAuOS0yIDJ2MTRjMCAxLjEuODkgMiAyIDJoMTRjMS4xIDAgMi0uOSAyLTJWN2wtNC00em0tNSAxNmMtMS42NiAwLTMtMS4zNC0zLTNzMS4zNC0zIDMtMyAzIDEuMzQgMyAzLTEuMzQgMy0zIDN6bTMtMTBINVY1aDEwdjR6JyB9KTtcblxudmFyIFNhdmUgPSBmdW5jdGlvbiBTYXZlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5TYXZlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTYXZlKTtcblNhdmUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gU2F2ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjYgMzEgMzQgMzggNDQgNDUgNDYgNDcgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xOCAxNi4wOGMtLjc2IDAtMS40NC4zLTEuOTYuNzdMOC45MSAxMi43Yy4wNS0uMjMuMDktLjQ2LjA5LS43cy0uMDQtLjQ3LS4wOS0uN2w3LjA1LTQuMTFjLjU0LjUgMS4yNS44MSAyLjA0LjgxIDEuNjYgMCAzLTEuMzQgMy0zcy0xLjM0LTMtMy0zLTMgMS4zNC0zIDNjMCAuMjQuMDQuNDcuMDkuN0w4LjA0IDkuODFDNy41IDkuMzEgNi43OSA5IDYgOWMtMS42NiAwLTMgMS4zNC0zIDNzMS4zNCAzIDMgM2MuNzkgMCAxLjUtLjMxIDIuMDQtLjgxbDcuMTIgNC4xNmMtLjA1LjIxLS4wOC40My0uMDguNjUgMCAxLjYxIDEuMzEgMi45MiAyLjkyIDIuOTIgMS42MSAwIDIuOTItMS4zMSAyLjkyLTIuOTJzLTEuMzEtMi45Mi0yLjkyLTIuOTJ6JyB9KTtcblxudmFyIFNoYXJlID0gZnVuY3Rpb24gU2hhcmUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblNoYXJlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTaGFyZSk7XG5TaGFyZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTaGFyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TaGFyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MCA0MSA0MiA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTUgNHYzaDUuNXYxMmgzVjdIMTlWNHonIH0pO1xuXG52YXIgVGl0bGUgPSBmdW5jdGlvbiBUaXRsZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuVGl0bGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFRpdGxlKTtcblRpdGxlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFRpdGxlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1RpdGxlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNyAzSDdjLTEuMSAwLTEuOTkuOS0xLjk5IDJMNSAyMWw3LTMgNyAzVjVjMC0xLjEtLjktMi0yLTJ6JyB9KTtcblxudmFyIFR1cm5lZEluID0gZnVuY3Rpb24gVHVybmVkSW4ocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblR1cm5lZEluID0gKDAsIF9wdXJlMi5kZWZhdWx0KShUdXJuZWRJbik7XG5UdXJuZWRJbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBUdXJuZWRJbjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UdXJuZWRJbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVHVybmVkSW4uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MCA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfY29sb3JNYW5pcHVsYXRvciA9IHJlcXVpcmUoJy4uL3N0eWxlcy9jb2xvck1hbmlwdWxhdG9yJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB3aWR0aDogNDAsXG4gICAgICBoZWlnaHQ6IDQwLFxuICAgICAgZm9udEZhbWlseTogdGhlbWUudHlwb2dyYXBoeS5mb250RmFtaWx5LFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyMCksXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckRlZmF1bHQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogKDAsIF9jb2xvck1hbmlwdWxhdG9yLmVtcGhhc2l6ZSkodGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsIDAuMjYpXG4gICAgfSxcbiAgICBpbWc6IHtcbiAgICAgIG1heFdpZHRoOiAnMTAwJScsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnYXV0bycsXG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZCBpbiBjb21iaW5hdGlvbiB3aXRoIGBzcmNgIG9yIGBzcmNTZXRgIHRvXG4gICAqIHByb3ZpZGUgYW4gYWx0IGF0dHJpYnV0ZSBmb3IgdGhlIHJlbmRlcmVkIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBhbHQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWQgdG8gcmVuZGVyIGljb24gb3IgdGV4dCBlbGVtZW50cyBpbnNpZGUgdGhlIEF2YXRhci5cbiAgICogYHNyY2AgYW5kIGBhbHRgIHByb3BzIHdpbGwgbm90IGJlIHVzZWQgYW5kIG5vIGBpbWdgIHdpbGxcbiAgICogYmUgcmVuZGVyZWQgYnkgZGVmYXVsdC5cbiAgICpcbiAgICogVGhpcyBjYW4gYmUgYW4gZWxlbWVudCwgb3IganVzdCBhIHN0cmluZy5cbiAgICovXG4gIGNoaWxkcmVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBUaGUgY2xhc3NOYW1lIG9mIHRoZSBjaGlsZCBlbGVtZW50LlxuICAgKiBVc2VkIGJ5IENoaXAgYW5kIExpc3RJdGVtSWNvbiB0byBzdHlsZSB0aGUgQXZhdGFyIGljb24uXG4gICAqL1xuICBjaGlsZHJlbkNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvcGVydGllcyBhcHBsaWVkIHRvIHRoZSBgaW1nYCBlbGVtZW50IHdoZW4gdGhlIGNvbXBvbmVudFxuICAgKiBpcyB1c2VkIHRvIGRpc3BsYXkgYW4gaW1hZ2UuXG4gICAqL1xuICBpbWdQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGBzaXplc2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNpemVzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyYzogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGBzcmNTZXRgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzcmNTZXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cbnZhciBBdmF0YXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShBdmF0YXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEF2YXRhcigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBBdmF0YXIpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChBdmF0YXIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEF2YXRhcikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQXZhdGFyLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFsdCA9IF9wcm9wcy5hbHQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcCA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjaGlsZHJlbkNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2hpbGRyZW5DbGFzc05hbWUsXG4gICAgICAgICAgQ29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgaW1nUHJvcHMgPSBfcHJvcHMuaW1nUHJvcHMsXG4gICAgICAgICAgc2l6ZXMgPSBfcHJvcHMuc2l6ZXMsXG4gICAgICAgICAgc3JjID0gX3Byb3BzLnNyYyxcbiAgICAgICAgICBzcmNTZXQgPSBfcHJvcHMuc3JjU2V0LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2FsdCcsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjaGlsZHJlbicsICdjaGlsZHJlbkNsYXNzTmFtZScsICdjb21wb25lbnQnLCAnaW1nUHJvcHMnLCAnc2l6ZXMnLCAnc3JjJywgJ3NyY1NldCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmNvbG9yRGVmYXVsdCwgY2hpbGRyZW5Qcm9wICYmICFzcmMgJiYgIXNyY1NldCksIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIGNoaWxkcmVuID0gbnVsbDtcblxuICAgICAgaWYgKGNoaWxkcmVuUHJvcCkge1xuICAgICAgICBpZiAoY2hpbGRyZW5DbGFzc05hbWVQcm9wICYmIHR5cGVvZiBjaGlsZHJlblByb3AgIT09ICdzdHJpbmcnICYmIF9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChjaGlsZHJlblByb3ApKSB7XG4gICAgICAgICAgdmFyIF9jaGlsZHJlbkNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2hpbGRyZW5DbGFzc05hbWVQcm9wLCBjaGlsZHJlblByb3AucHJvcHMuY2xhc3NOYW1lKTtcbiAgICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGRyZW5Qcm9wLCB7IGNsYXNzTmFtZTogX2NoaWxkcmVuQ2xhc3NOYW1lIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNoaWxkcmVuID0gY2hpbGRyZW5Qcm9wO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHNyYyB8fCBzcmNTZXQpIHtcbiAgICAgICAgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnaW1nJywgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgYWx0OiBhbHQsXG4gICAgICAgICAgc3JjOiBzcmMsXG4gICAgICAgICAgc3JjU2V0OiBzcmNTZXQsXG4gICAgICAgICAgc2l6ZXM6IHNpemVzLFxuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3Nlcy5pbWdcbiAgICAgICAgfSwgaW1nUHJvcHMpKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEF2YXRhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkF2YXRhci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbXBvbmVudDogJ2Rpdidcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQXZhdGFyJyB9KShBdmF0YXIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgNiAyNCAyNiAyNyAyOCAzMSAzMyAzNCAzNSAzNiAzNyAzOSA0MCA0MyA0OSA1NCA1NyA1OCA1OSA2MSA2MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9BdmF0YXIgPSByZXF1aXJlKCcuL0F2YXRhcicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BdmF0YXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA0IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuXG52YXIgUkFESVVTID0gMTI7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgZGlzcGxheTogJ2lubGluZS1mbGV4J1xuICAgIH0sXG4gICAgYmFkZ2U6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGZsZXhEaXJlY3Rpb246ICdyb3cnLFxuICAgICAgZmxleFdyYXA6ICd3cmFwJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGFsaWduQ29udGVudDogJ2NlbnRlcicsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgdG9wOiAtUkFESVVTLFxuICAgICAgcmlnaHQ6IC1SQURJVVMsXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250V2VpZ2h0OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRXZWlnaHQsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKFJBRElVUyksXG4gICAgICB3aWR0aDogUkFESVVTICogMixcbiAgICAgIGhlaWdodDogUkFESVVTICogMixcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuY29sb3IsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0Q29sb3IsXG4gICAgICB6SW5kZXg6IDEgLy8gUmVuZGVyIHRoZSBiYWRnZSBvbiB0b3Agb2YgcG90ZW50aWFsIHJpcHBsZXMuXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0sXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDApXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgcmVuZGVyZWQgd2l0aGluIHRoZSBiYWRnZS5cbiAgICovXG4gIGJhZGdlQ29udGVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBiYWRnZSB3aWxsIGJlIGFkZGVkIHJlbGF0aXZlIHRvIHRoaXMgbm9kZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdwcmltYXJ5JywgJ2FjY2VudCddKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgQmFkZ2UgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShCYWRnZSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQmFkZ2UoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQmFkZ2UpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChCYWRnZS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQmFkZ2UpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEJhZGdlLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJhZGdlQ29udGVudCA9IF9wcm9wcy5iYWRnZUNvbnRlbnQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2JhZGdlQ29udGVudCcsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICdjaGlsZHJlbiddKTtcblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWVQcm9wKTtcbiAgICAgIHZhciBiYWRnZUNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5iYWRnZSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnZGVmYXVsdCcpKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW4sXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogYmFkZ2VDbGFzc05hbWUgfSxcbiAgICAgICAgICBiYWRnZUNvbnRlbnRcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEJhZGdlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQmFkZ2UuZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ2RlZmF1bHQnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUJhZGdlJyB9KShCYWRnZSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvQmFkZ2UuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL0JhZGdlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfQmFkZ2UgPSByZXF1aXJlKCcuL0JhZGdlJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0JhZGdlKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQWN0aW9uOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGNvbG9yRXJyb3I6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmVycm9yWzUwMF1cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbG9yID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBpY29uIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgSWNvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEljb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEljb24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgSWNvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEljb24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEljb24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEljb24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCdtYXRlcmlhbC1pY29ucycsIGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnc3BhbicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSwgJ2FyaWEtaGlkZGVuJzogJ3RydWUnIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnaW5oZXJpdCdcbn07XG5JY29uLm11aU5hbWUgPSAnSWNvbic7XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpSWNvbicgfSkoSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9JY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA2IDcgOCA5IDM0IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuL0ljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDYgNyA4IDkgMzQgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuLi9JY29uJyk7XG5cbnZhciBfSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKTtcblxucmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX3JlYWN0SGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL3JlYWN0SGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBCdXR0b25CYXNlXG5cbi8vIEVuc3VyZSBDU1Mgc3BlY2lmaWNpdHlcblxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgICAgZmxleDogJzAgMCBhdXRvJyxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjQpLFxuICAgICAgd2lkdGg6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBoZWlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBwYWRkaW5nOiAwLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmUsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2JhY2tncm91bmQtY29sb3InLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdFxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIGNvbG9ySW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgZGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgbGFiZWw6IHtcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnaW5oZXJpdCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICB3aWR0aDogJzFlbScsXG4gICAgICBoZWlnaHQ6ICcxZW0nXG4gICAgfSxcbiAgICBrZXlib2FyZEZvY3VzZWQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSBuYXRpdmUgYnV0dG9uIGNvbXBvbmVudC5cbiAgICovXG4gIGJ1dHRvblJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIGVsZW1lbnQuXG4gICAqIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYW4gaWNvbiBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdpbmhlcml0JywgJ3ByaW1hcnknLCAnY29udHJhc3QnLCAnYWNjZW50J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJ1dHRvbiB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHJpcHBsZSB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufTtcblxuLyoqXG4gKiBSZWZlciB0byB0aGUgW0ljb25zXSgvc3R5bGUvaWNvbnMpIHNlY3Rpb24gb2YgdGhlIGRvY3VtZW50YXRpb25cbiAqIHJlZ2FyZGluZyB0aGUgYXZhaWxhYmxlIGljb24gb3B0aW9ucy5cbiAqL1xudmFyIEljb25CdXR0b24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJY29uQnV0dG9uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEljb25CdXR0b24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChJY29uQnV0dG9uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShJY29uQnV0dG9uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYnV0dG9uUmVmID0gX3Byb3BzLmJ1dHRvblJlZixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uUmVmJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2Rpc2FibGVkJywgJ3Jvb3RSZWYnXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2RlZmF1bHQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGlzYWJsZWQsIGRpc2FibGVkKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWUpLFxuICAgICAgICAgIGNlbnRlclJpcHBsZTogdHJ1ZSxcbiAgICAgICAgICBrZXlib2FyZEZvY3VzZWRDbGFzc05hbWU6IGNsYXNzZXMua2V5Ym9hcmRGb2N1c2VkLFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZFxuICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgIHJvb3RSZWY6IGJ1dHRvblJlZixcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5sYWJlbCB9LFxuICAgICAgICAgIHR5cGVvZiBjaGlsZHJlbiA9PT0gJ3N0cmluZycgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuaWNvbiB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApIDogX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgICBpZiAoKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KShjaGlsZCwgWydJY29uJywgJ1N2Z0ljb24nXSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5pY29uLCBjaGlsZC5wcm9wcy5jbGFzc05hbWUpXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgfSlcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb25CdXR0b247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdkZWZhdWx0JyxcbiAgZGlzYWJsZWQ6IGZhbHNlLFxuICBkaXNhYmxlUmlwcGxlOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uQnV0dG9uJyB9KShJY29uQnV0dG9uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb25CdXR0b24gPSByZXF1aXJlKCcuL0ljb25CdXR0b24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbkJ1dHRvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZmxleDogJzEgMSBhdXRvJyxcbiAgICAgIGxpc3RTdHlsZTogJ25vbmUnLFxuICAgICAgbWFyZ2luOiAwLFxuICAgICAgcGFkZGluZzogMCxcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnXG4gICAgfSxcbiAgICBwYWRkaW5nOiB7XG4gICAgICBwYWRkaW5nVG9wOiB0aGVtZS5zcGFjaW5nLnVuaXQsXG4gICAgICBwYWRkaW5nQm90dG9tOiB0aGVtZS5zcGFjaW5nLnVuaXRcbiAgICB9LFxuICAgIGRlbnNlOiB7XG4gICAgICBwYWRkaW5nVG9wOiB0aGVtZS5zcGFjaW5nLnVuaXQgLyAyLFxuICAgICAgcGFkZGluZ0JvdHRvbTogdGhlbWUuc3BhY2luZy51bml0IC8gMlxuICAgIH0sXG4gICAgc3ViaGVhZGVyOiB7XG4gICAgICBwYWRkaW5nVG9wOiAwXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIGZvciB0aGUgcm9vdCBub2RlLlxuICAgKiBFaXRoZXIgYSBzdHJpbmcgdG8gdXNlIGEgRE9NIGVsZW1lbnQgb3IgYSBjb21wb25lbnQuXG4gICAqL1xuICBjb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgY29tcGFjdCB2ZXJ0aWNhbCBwYWRkaW5nIGRlc2lnbmVkIGZvciBrZXlib2FyZCBhbmQgbW91c2UgaW5wdXQgd2lsbCBiZSB1c2VkIGZvclxuICAgKiB0aGUgbGlzdCBhbmQgbGlzdCBpdGVtcy4gVGhlIHByb3BlcnR5IGlzIGF2YWlsYWJsZSB0byBkZXNjZW5kYW50IGNvbXBvbmVudHMgYXMgdGhlXG4gICAqIGBkZW5zZWAgY29udGV4dC5cbiAgICovXG4gIGRlbnNlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHZlcnRpY2FsIHBhZGRpbmcgd2lsbCBiZSByZW1vdmVkIGZyb20gdGhlIGxpc3QuXG4gICAqL1xuICBkaXNhYmxlUGFkZGluZzogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudCwgbm9ybWFsbHkgYExpc3RJdGVtYC5cbiAgICovXG4gIHN1YmhlYWRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59O1xuXG52YXIgTGlzdCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKExpc3QsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpc3QoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgTGlzdCk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKExpc3QuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKExpc3QpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKExpc3QsIFt7XG4gICAga2V5OiAnZ2V0Q2hpbGRDb250ZXh0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Q2hpbGRDb250ZXh0KCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZGVuc2U6IHRoaXMucHJvcHMuZGVuc2VcbiAgICAgIH07XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgQ29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgZGlzYWJsZVBhZGRpbmcgPSBfcHJvcHMuZGlzYWJsZVBhZGRpbmcsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgZGVuc2UgPSBfcHJvcHMuZGVuc2UsXG4gICAgICAgICAgc3ViaGVhZGVyID0gX3Byb3BzLnN1YmhlYWRlcixcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29tcG9uZW50JywgJ2Rpc2FibGVQYWRkaW5nJywgJ2NoaWxkcmVuJywgJ2RlbnNlJywgJ3N1YmhlYWRlcicsICdyb290UmVmJ10pO1xuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5kZW5zZSwgZGVuc2UgJiYgIWRpc2FibGVQYWRkaW5nKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMucGFkZGluZywgIWRpc2FibGVQYWRkaW5nKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuc3ViaGVhZGVyLCBzdWJoZWFkZXIpLCBfY2xhc3NOYW1lcyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIENvbXBvbmVudFByb3AsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciwgeyByZWY6IHJvb3RSZWYgfSksXG4gICAgICAgIHN1YmhlYWRlcixcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBMaXN0O1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGlzdC5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbXBvbmVudDogJ3VsJyxcbiAgZGVuc2U6IGZhbHNlLFxuICBkaXNhYmxlUGFkZGluZzogZmFsc2Vcbn07XG5cblxuTGlzdC5jaGlsZENvbnRleHRUeXBlcyA9IHtcbiAgZGVuc2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbFxufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUxpc3QnIH0pKExpc3QpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQgMzYgNDMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9CdXR0b25CYXNlID0gcmVxdWlyZSgnLi4vQnV0dG9uQmFzZScpO1xuXG52YXIgX0J1dHRvbkJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQnV0dG9uQmFzZSk7XG5cbnZhciBfcmVhY3RIZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvcmVhY3RIZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdmbGV4LXN0YXJ0JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICB0ZXh0RGVjb3JhdGlvbjogJ25vbmUnXG4gICAgfSxcbiAgICBjb250YWluZXI6IHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnXG4gICAgfSxcbiAgICBrZXlib2FyZEZvY3VzZWQ6IHtcbiAgICAgIGJhY2tncm91bmQ6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyXG4gICAgfSxcbiAgICBkZWZhdWx0OiB7XG4gICAgICBwYWRkaW5nVG9wOiAxMixcbiAgICAgIHBhZGRpbmdCb3R0b206IDEyXG4gICAgfSxcbiAgICBkZW5zZToge1xuICAgICAgcGFkZGluZ1RvcDogdGhlbWUuc3BhY2luZy51bml0LFxuICAgICAgcGFkZGluZ0JvdHRvbTogdGhlbWUuc3BhY2luZy51bml0XG4gICAgfSxcbiAgICBkaXNhYmxlZDoge1xuICAgICAgb3BhY2l0eTogMC41XG4gICAgfSxcbiAgICBkaXZpZGVyOiB7XG4gICAgICBib3JkZXJCb3R0b206ICcxcHggc29saWQgJyArIHRoZW1lLnBhbGV0dGUudGV4dC5saWdodERpdmlkZXJcbiAgICB9LFxuICAgIGd1dHRlcnM6IHtcbiAgICAgIHBhZGRpbmdMZWZ0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAyLFxuICAgICAgcGFkZGluZ1JpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAyXG4gICAgfSxcbiAgICBidXR0b246IHtcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnYmFja2dyb3VuZC1jb2xvcicsIHtcbiAgICAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXN0XG4gICAgICB9KSxcbiAgICAgICcmOmhvdmVyJzoge1xuICAgICAgICB0ZXh0RGVjb3JhdGlvbjogJ25vbmUnLFxuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyLFxuICAgICAgICAvLyBSZXNldCBvbiBtb3VzZSBkZXZpY2VzXG4gICAgICAgICdAbWVkaWEgKGhvdmVyOiBub25lKSc6IHtcbiAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICd0cmFuc3BhcmVudCdcbiAgICAgICAgfSxcbiAgICAgICAgJyYkZGlzYWJsZWQnOiB7XG4gICAgICAgICAgYmFja2dyb3VuZENvbG9yOiAndHJhbnNwYXJlbnQnXG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuICAgIHNlY29uZGFyeUFjdGlvbjoge1xuICAgICAgLy8gQWRkIHNvbWUgc3BhY2UgdG8gYXZvaWQgY29sbGlzaW9uIGFzIGBMaXN0SXRlbVNlY29uZGFyeUFjdGlvbmBcbiAgICAgIC8vIGlzIGFic29sdXRlbHkgcG9zaXRpb25uZWQuXG4gICAgICBwYWRkaW5nUmlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDRcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBMaXN0SXRlbSB3aWxsIGJlIGEgYnV0dG9uLlxuICAgKi9cbiAgYnV0dG9uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBjb21wYWN0IHZlcnRpY2FsIHBhZGRpbmcgZGVzaWduZWQgZm9yIGtleWJvYXJkIGFuZCBtb3VzZSBpbnB1dCB3aWxsIGJlIHVzZWQuXG4gICAqL1xuICBkZW5zZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGxlZnQgYW5kIHJpZ2h0IHBhZGRpbmcgaXMgcmVtb3ZlZC5cbiAgICovXG4gIGRpc2FibGVHdXR0ZXJzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGEgMXB4IGxpZ2h0IGJvcmRlciBpcyBhZGRlZCB0byB0aGUgYm90dG9tIG9mIHRoZSBsaXN0IGl0ZW0uXG4gICAqL1xuICBkaXZpZGVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkXG59O1xuXG52YXIgTGlzdEl0ZW0gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShMaXN0SXRlbSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGlzdEl0ZW0oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgTGlzdEl0ZW0pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChMaXN0SXRlbS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoTGlzdEl0ZW0pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKExpc3RJdGVtLCBbe1xuICAgIGtleTogJ2dldENoaWxkQ29udGV4dCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldENoaWxkQ29udGV4dCgpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGRlbnNlOiB0aGlzLnByb3BzLmRlbnNlIHx8IHRoaXMuY29udGV4dC5kZW5zZSB8fCBmYWxzZVxuICAgICAgfTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJ1dHRvbiA9IF9wcm9wcy5idXR0b24sXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb21wb25lbnRQcm9wID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBkZW5zZSA9IF9wcm9wcy5kZW5zZSxcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICBkaXZpZGVyID0gX3Byb3BzLmRpdmlkZXIsXG4gICAgICAgICAgZGlzYWJsZUd1dHRlcnMgPSBfcHJvcHMuZGlzYWJsZUd1dHRlcnMsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdkZW5zZScsICdkaXNhYmxlZCcsICdkaXZpZGVyJywgJ2Rpc2FibGVHdXR0ZXJzJ10pO1xuXG4gICAgICB2YXIgaXNEZW5zZSA9IGRlbnNlIHx8IHRoaXMuY29udGV4dC5kZW5zZSB8fCBmYWxzZTtcbiAgICAgIHZhciBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi50b0FycmF5KGNoaWxkcmVuUHJvcCk7XG5cbiAgICAgIHZhciBoYXNBdmF0YXIgPSBjaGlsZHJlbi5zb21lKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICByZXR1cm4gKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KSh2YWx1ZSwgWydMaXN0SXRlbUF2YXRhciddKTtcbiAgICAgIH0pO1xuICAgICAgdmFyIGhhc1NlY29uZGFyeUFjdGlvbiA9IGNoaWxkcmVuLmxlbmd0aCAmJiAoMCwgX3JlYWN0SGVscGVycy5pc011aUVsZW1lbnQpKGNoaWxkcmVuW2NoaWxkcmVuLmxlbmd0aCAtIDFdLCBbJ0xpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uJ10pO1xuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5ndXR0ZXJzLCAhZGlzYWJsZUd1dHRlcnMpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5kaXZpZGVyLCBkaXZpZGVyKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGlzYWJsZWQsIGRpc2FibGVkKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuYnV0dG9uLCBidXR0b24pLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5zZWNvbmRhcnlBY3Rpb24sIGhhc1NlY29uZGFyeUFjdGlvbiksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBpc0RlbnNlIHx8IGhhc0F2YXRhciA/IGNsYXNzZXMuZGVuc2UgOiBjbGFzc2VzLmRlZmF1bHQsIHRydWUpLCBfY2xhc3NOYW1lcyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICB2YXIgbGlzdEl0ZW1Qcm9wcyA9ICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSwgZGlzYWJsZWQ6IGRpc2FibGVkIH0sIG90aGVyKTtcbiAgICAgIHZhciBDb21wb25lbnRNYWluID0gY29tcG9uZW50UHJvcDtcblxuICAgICAgaWYgKGJ1dHRvbikge1xuICAgICAgICBDb21wb25lbnRNYWluID0gX0J1dHRvbkJhc2UyLmRlZmF1bHQ7XG4gICAgICAgIGxpc3RJdGVtUHJvcHMuY29tcG9uZW50ID0gY29tcG9uZW50UHJvcCB8fCAnbGknO1xuICAgICAgICBsaXN0SXRlbVByb3BzLmtleWJvYXJkRm9jdXNlZENsYXNzTmFtZSA9IGNsYXNzZXMua2V5Ym9hcmRGb2N1c2VkO1xuICAgICAgfVxuXG4gICAgICBpZiAoaGFzU2Vjb25kYXJ5QWN0aW9uKSB7XG4gICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5jb250YWluZXIgfSxcbiAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIENvbXBvbmVudE1haW4sXG4gICAgICAgICAgICBsaXN0SXRlbVByb3BzLFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApLFxuICAgICAgICAgIGNoaWxkcmVuLnBvcCgpXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQ29tcG9uZW50TWFpbixcbiAgICAgICAgbGlzdEl0ZW1Qcm9wcyxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBMaXN0SXRlbTtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpc3RJdGVtLmRlZmF1bHRQcm9wcyA9IHtcbiAgYnV0dG9uOiBmYWxzZSxcbiAgY29tcG9uZW50OiAnbGknLFxuICBkZW5zZTogZmFsc2UsXG4gIGRpc2FibGVkOiBmYWxzZSxcbiAgZGlzYWJsZUd1dHRlcnM6IGZhbHNlLFxuICBkaXZpZGVyOiBmYWxzZVxufTtcblxuXG5MaXN0SXRlbS5jb250ZXh0VHlwZXMgPSB7XG4gIGRlbnNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2xcbn07XG5cbkxpc3RJdGVtLmNoaWxkQ29udGV4dFR5cGVzID0ge1xuICBkZW5zZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sXG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpTGlzdEl0ZW0nIH0pKExpc3RJdGVtKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQgMzYgNDMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgd2lkdGg6IDM2LFxuICAgICAgaGVpZ2h0OiAzNixcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMTgpLFxuICAgICAgbWFyZ2luUmlnaHQ6IDRcbiAgICB9LFxuICAgIGljb246IHtcbiAgICAgIHdpZHRoOiAyMCxcbiAgICAgIGhlaWdodDogMjAsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDIwKVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQsIG5vcm1hbGx5IGBBdmF0YXJgLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cblxuLyoqXG4gKiBJdCdzIGEgc2ltcGxlIHdyYXBwZXIgdG8gYXBwbHkgdGhlIGBkZW5zZWAgbW9kZSBzdHlsZXMgdG8gYEF2YXRhcmAuXG4gKi9cbmZ1bmN0aW9uIExpc3RJdGVtQXZhdGFyKHByb3BzLCBjb250ZXh0KSB7XG4gIGlmIChjb250ZXh0LmRlbnNlID09PSB1bmRlZmluZWQpIHtcbiAgICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKGZhbHNlLCAnTWF0ZXJpYWwtVUk6IDxMaXN0SXRlbUF2YXRhcj4gaXMgYSBzaW1wbGUgd3JhcHBlciB0byBhcHBseSB0aGUgZGVuc2Ugc3R5bGVzXFxuICAgICAgdG8gPEF2YXRhcj4uIFlvdSBkbyBub3QgbmVlZCBpdCB1bmxlc3MgeW91IGFyZSBjb250cm9sbGluZyB0aGUgPExpc3Q+IGRlbnNlIHByb3BlcnR5LicpIDogdm9pZCAwO1xuICAgIHJldHVybiBwcm9wcy5jaGlsZHJlbjtcbiAgfVxuXG4gIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWVQcm9wID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShwcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZSddKTtcblxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkcmVuLCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMucm9vdCwgY29udGV4dC5kZW5zZSksIGNsYXNzTmFtZVByb3AsIGNoaWxkcmVuLnByb3BzLmNsYXNzTmFtZSksXG4gICAgY2hpbGRyZW5DbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuaWNvbiwgY29udGV4dC5kZW5zZSksIGNoaWxkcmVuLnByb3BzLmNoaWxkcmVuQ2xhc3NOYW1lKVxuICB9LCBvdGhlcikpO1xufVxuXG5MaXN0SXRlbUF2YXRhci5jb250ZXh0VHlwZXMgPSB7XG4gIGRlbnNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2xcbn07XG5cbkxpc3RJdGVtQXZhdGFyLm11aU5hbWUgPSAnTGlzdEl0ZW1BdmF0YXInO1xuXG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpTGlzdEl0ZW1BdmF0YXInIH0pKExpc3RJdGVtQXZhdGFyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtQXZhdGFyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9MaXN0L0xpc3RJdGVtQXZhdGFyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQgMzYgNDMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBoZWlnaHQ6IDI0LFxuICAgICAgbWFyZ2luUmlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDIsXG4gICAgICB3aWR0aDogMjQsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlLFxuICAgICAgZmxleFNocmluazogMFxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQsIG5vcm1hbGx5IGBJY29uYCwgYFN2Z0ljb25gLFxuICAgKiBvciBhIGBtYXRlcmlhbC11aS1pY29uc2AgU1ZHIGljb24gY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cblxuLyoqXG4gKiBBIHNpbXBsZSB3cmFwcGVyIHRvIGFwcGx5IGBMaXN0YCBzdHlsZXMgdG8gYW4gYEljb25gIG9yIGBTdmdJY29uYC5cbiAqL1xuZnVuY3Rpb24gTGlzdEl0ZW1JY29uKHByb3BzKSB7XG4gIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWVQcm9wID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShwcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZSddKTtcblxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkcmVuLCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWVQcm9wLCBjaGlsZHJlbi5wcm9wcy5jbGFzc05hbWUpXG4gIH0sIG90aGVyKSk7XG59XG5cbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlMaXN0SXRlbUljb24nIH0pKExpc3RJdGVtSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbUljb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1JY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQgMzYgNDMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX3JlZjsgLy8gIHdlYWtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgICAgIHJpZ2h0OiA0LFxuICAgICAgdG9wOiAnNTAlJyxcbiAgICAgIG1hcmdpblRvcDogLXRoZW1lLnNwYWNpbmcudW5pdCAqIDNcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LCBub3JtYWxseSBhbiBgSWNvbkJ1dHRvbmAgb3Igc2VsZWN0aW9uIGNvbnRyb2wuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cblxuZnVuY3Rpb24gTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24ocHJvcHMpIHtcbiAgdmFyIGNoaWxkcmVuID0gcHJvcHMuY2hpbGRyZW4sXG4gICAgICBjbGFzc2VzID0gcHJvcHMuY2xhc3NlcyxcbiAgICAgIGNsYXNzTmFtZSA9IHByb3BzLmNsYXNzTmFtZTtcblxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAnZGl2JyxcbiAgICB7IGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSkgfSxcbiAgICBjaGlsZHJlblxuICApO1xufVxuXG5MaXN0SXRlbVNlY29uZGFyeUFjdGlvbi5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoX3JlZiA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5MaXN0SXRlbVNlY29uZGFyeUFjdGlvbi5tdWlOYW1lID0gJ0xpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUxpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uJyB9KShMaXN0SXRlbVNlY29uZGFyeUFjdGlvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbVNlY29uZGFyeUFjdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbVNlY29uZGFyeUFjdGlvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfVHlwb2dyYXBoeSA9IHJlcXVpcmUoJy4uL1R5cG9ncmFwaHknKTtcblxudmFyIF9UeXBvZ3JhcGh5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1R5cG9ncmFwaHkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICAgICAgcGFkZGluZzogJzAgMTZweCcsXG4gICAgICAnJjpmaXJzdC1jaGlsZCc6IHtcbiAgICAgICAgcGFkZGluZ0xlZnQ6IDBcbiAgICAgIH1cbiAgICB9LFxuICAgIGluc2V0OiB7XG4gICAgICAnJjpmaXJzdC1jaGlsZCc6IHtcbiAgICAgICAgcGFkZGluZ0xlZnQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDdcbiAgICAgIH1cbiAgICB9LFxuICAgIGRlbnNlOiB7XG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDEzKVxuICAgIH0sXG4gICAgdGV4dDoge30sIC8vIFByZXNlbnQgdG8gYWxsb3cgZXh0ZXJuYWwgY3VzdG9taXphdGlvblxuICAgIHRleHREZW5zZToge1xuICAgICAgZm9udFNpemU6ICdpbmhlcml0J1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgY2hpbGRyZW4gd29uJ3QgYmUgd3JhcHBlZCBieSBhIHR5cG9ncmFwaHkgY29tcG9uZW50LlxuICAgKiBGb3IgaW5zdGFuY2UsIHRoYXQgY2FuIGJlIHVzZWZ1bCB0byBjYW4gcmVuZGVyIGFuIGg0IGluc3RlYWQgb2YgYVxuICAgKi9cbiAgZGlzYWJsZVR5cG9ncmFwaHk6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNoaWxkcmVuIHdpbGwgYmUgaW5kZW50ZWQuXG4gICAqIFRoaXMgc2hvdWxkIGJlIHVzZWQgaWYgdGhlcmUgaXMgbm8gbGVmdCBhdmF0YXIgb3IgbGVmdCBpY29uLlxuICAgKi9cbiAgaW5zZXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG4gIHByaW1hcnk6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuICBzZWNvbmRhcnk6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgTGlzdEl0ZW1UZXh0ID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTGlzdEl0ZW1UZXh0LCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaXN0SXRlbVRleHQoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgTGlzdEl0ZW1UZXh0KTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoTGlzdEl0ZW1UZXh0Ll9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShMaXN0SXRlbVRleHQpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKExpc3RJdGVtVGV4dCwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBkaXNhYmxlVHlwb2dyYXBoeSA9IF9wcm9wcy5kaXNhYmxlVHlwb2dyYXBoeSxcbiAgICAgICAgICBwcmltYXJ5ID0gX3Byb3BzLnByaW1hcnksXG4gICAgICAgICAgc2Vjb25kYXJ5ID0gX3Byb3BzLnNlY29uZGFyeSxcbiAgICAgICAgICBpbnNldCA9IF9wcm9wcy5pbnNldCxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdkaXNhYmxlVHlwb2dyYXBoeScsICdwcmltYXJ5JywgJ3NlY29uZGFyeScsICdpbnNldCddKTtcbiAgICAgIHZhciBkZW5zZSA9IHRoaXMuY29udGV4dC5kZW5zZTtcblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGVuc2UsIGRlbnNlKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuaW5zZXQsIGluc2V0KSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgcHJpbWFyeSAmJiAoZGlzYWJsZVR5cG9ncmFwaHkgPyBwcmltYXJ5IDogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgX1R5cG9ncmFwaHkyLmRlZmF1bHQsXG4gICAgICAgICAge1xuICAgICAgICAgICAgdHlwZTogJ3N1YmhlYWRpbmcnLFxuICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMudGV4dCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMudGV4dERlbnNlLCBkZW5zZSkpXG4gICAgICAgICAgfSxcbiAgICAgICAgICBwcmltYXJ5XG4gICAgICAgICkpLFxuICAgICAgICBzZWNvbmRhcnkgJiYgKGRpc2FibGVUeXBvZ3JhcGh5ID8gc2Vjb25kYXJ5IDogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgX1R5cG9ncmFwaHkyLmRlZmF1bHQsXG4gICAgICAgICAge1xuICAgICAgICAgICAgY29sb3I6ICdzZWNvbmRhcnknLFxuICAgICAgICAgICAgdHlwZTogJ2JvZHkxJyxcbiAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnRleHQsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLnRleHREZW5zZSwgZGVuc2UpKVxuICAgICAgICAgIH0sXG4gICAgICAgICAgc2Vjb25kYXJ5XG4gICAgICAgICkpXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTGlzdEl0ZW1UZXh0O1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGlzdEl0ZW1UZXh0LmRlZmF1bHRQcm9wcyA9IHtcbiAgZGlzYWJsZVR5cG9ncmFwaHk6IGZhbHNlLFxuICBwcmltYXJ5OiBmYWxzZSxcbiAgc2Vjb25kYXJ5OiBmYWxzZSxcbiAgaW5zZXQ6IGZhbHNlXG59O1xuTGlzdEl0ZW1UZXh0LmNvbnRleHRUeXBlcyA9IHtcbiAgZGVuc2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlMaXN0SXRlbVRleHQnIH0pKExpc3RJdGVtVGV4dCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0SXRlbVRleHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdEl0ZW1UZXh0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQgMzYgNDMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTsgLy8gIHdlYWtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgYm94U2l6aW5nOiAnYm9yZGVyLWJveCcsXG4gICAgICBsaW5lSGVpZ2h0OiAnNDhweCcsXG4gICAgICBsaXN0U3R5bGU6ICdub25lJyxcbiAgICAgIHBhZGRpbmdMZWZ0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAyLFxuICAgICAgcGFkZGluZ1JpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAyLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5zZWNvbmRhcnksXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250V2VpZ2h0OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRXZWlnaHRNZWRpdW0sXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKHRoZW1lLnR5cG9ncmFwaHkuZm9udFNpemUpXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH0sXG4gICAgY29sb3JJbmhlcml0OiB7XG4gICAgICBjb2xvcjogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBpbnNldDoge1xuICAgICAgcGFkZGluZ0xlZnQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDlcbiAgICB9LFxuICAgIHN0aWNreToge1xuICAgICAgcG9zaXRpb246ICdzdGlja3knLFxuICAgICAgdG9wOiAwLFxuICAgICAgekluZGV4OiAxLFxuICAgICAgYmFja2dyb3VuZENvbG9yOiAnaW5oZXJpdCdcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICogVGhlIGRlZmF1bHQgdmFsdWUgaXMgYSBgYnV0dG9uYC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2RlZmF1bHQnLCAncHJpbWFyeScsICdpbmhlcml0J10pLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBMaXN0IFN1YmhlYWRlciB3aWxsIG5vdCBzdGljayB0byB0aGUgdG9wIGR1cmluZyBzY3JvbGwuXG4gICAqL1xuICBkaXNhYmxlU3RpY2t5OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgTGlzdCBTdWJoZWFkZXIgd2lsbCBiZSBpbmRlbnRlZC5cbiAgICovXG4gIGluc2V0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxudmFyIExpc3RTdWJoZWFkZXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShMaXN0U3ViaGVhZGVyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaXN0U3ViaGVhZGVyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIExpc3RTdWJoZWFkZXIpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChMaXN0U3ViaGVhZGVyLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShMaXN0U3ViaGVhZGVyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShMaXN0U3ViaGVhZGVyLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIENvbXBvbmVudFByb3AgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIGRpc2FibGVTdGlja3kgPSBfcHJvcHMuZGlzYWJsZVN0aWNreSxcbiAgICAgICAgICBpbnNldCA9IF9wcm9wcy5pbnNldCxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb21wb25lbnQnLCAnY29sb3InLCAnZGlzYWJsZVN0aWNreScsICdpbnNldCddKTtcblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnZGVmYXVsdCcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5pbnNldCwgaW5zZXQpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5zdGlja3ksICFkaXNhYmxlU3RpY2t5KSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIExpc3RTdWJoZWFkZXI7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5MaXN0U3ViaGVhZGVyLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29tcG9uZW50OiAnbGknLFxuICBjb2xvcjogJ2RlZmF1bHQnLFxuICBkaXNhYmxlU3RpY2t5OiBmYWxzZSxcbiAgaW5zZXQ6IGZhbHNlXG59O1xuTGlzdFN1YmhlYWRlci5tdWlOYW1lID0gJ0xpc3RTdWJoZWFkZXInO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUxpc3RTdWJoZWFkZXInIH0pKExpc3RTdWJoZWFkZXIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0xpc3QvTGlzdFN1YmhlYWRlci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9MaXN0U3ViaGVhZGVyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQgMzYgNDMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfTGlzdCA9IHJlcXVpcmUoJy4vTGlzdCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaXN0KS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9MaXN0SXRlbSA9IHJlcXVpcmUoJy4vTGlzdEl0ZW0nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdMaXN0SXRlbScsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3RJdGVtKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9MaXN0SXRlbUF2YXRhciA9IHJlcXVpcmUoJy4vTGlzdEl0ZW1BdmF0YXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdMaXN0SXRlbUF2YXRhcicsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3RJdGVtQXZhdGFyKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9MaXN0SXRlbVRleHQgPSByZXF1aXJlKCcuL0xpc3RJdGVtVGV4dCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0xpc3RJdGVtVGV4dCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3RJdGVtVGV4dCkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfTGlzdEl0ZW1JY29uID0gcmVxdWlyZSgnLi9MaXN0SXRlbUljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdMaXN0SXRlbUljb24nLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9MaXN0SXRlbUljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX0xpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uID0gcmVxdWlyZSgnLi9MaXN0SXRlbVNlY29uZGFyeUFjdGlvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0xpc3RJdGVtU2Vjb25kYXJ5QWN0aW9uJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTGlzdEl0ZW1TZWNvbmRhcnlBY3Rpb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX0xpc3RTdWJoZWFkZXIgPSByZXF1aXJlKCcuL0xpc3RTdWJoZWFkZXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdMaXN0U3ViaGVhZGVyJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTGlzdFN1YmhlYWRlcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTGlzdC9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IDM2IDQzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9zY3JvbGxiYXJTaXplID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvdXRpbC9zY3JvbGxiYXJTaXplJyk7XG5cbnZhciBfc2Nyb2xsYmFyU2l6ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zY3JvbGxiYXJTaXplKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX1BvcG92ZXIgPSByZXF1aXJlKCcuLi9Qb3BvdmVyJyk7XG5cbnZhciBfUG9wb3ZlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Qb3BvdmVyKTtcblxudmFyIF9NZW51TGlzdCA9IHJlcXVpcmUoJy4vTWVudUxpc3QnKTtcblxudmFyIF9NZW51TGlzdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9NZW51TGlzdCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IFBvcG92ZXJcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIERPTSBlbGVtZW50IHVzZWQgdG8gc2V0IHRoZSBwb3NpdGlvbiBvZiB0aGUgbWVudS5cbiAgICovXG4gIGFuY2hvckVsOiB0eXBlb2YgSFRNTEVsZW1lbnQgPT09ICdmdW5jdGlvbicgPyByZXF1aXJlKCdwcm9wLXR5cGVzJykuaW5zdGFuY2VPZihIVE1MRWxlbWVudCkgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55LFxuICAvLyBtYXRjaCBQb3BvdmVyXG4gIC8qKlxuICAgKiBNZW51IGNvbnRlbnRzLCBub3JtYWxseSBgTWVudUl0ZW1gcy5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFByb3BlcnRpZXMgYXBwbGllZCB0byB0aGUgYE1lbnVMaXN0YCBlbGVtZW50LlxuICAgKi9cbiAgTWVudUxpc3RQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBNZW51IGVudGVycy5cbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgTWVudSBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgTWVudSBoYXMgZW50ZXJlZC5cbiAgICovXG4gIG9uRW50ZXJlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIE1lbnUgZXhpdHMuXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgTWVudSBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0aW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIE1lbnUgaGFzIGV4aXRlZC5cbiAgICovXG4gIG9uRXhpdGVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCByZXF1ZXN0cyB0byBiZSBjbG9zZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKi9cbiAgb25SZXF1ZXN0Q2xvc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBtZW51IGlzIHZpc2libGUuXG4gICAqL1xuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBQYXBlclByb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBgY2xhc3Nlc2AgcHJvcGVydHkgYXBwbGllZCB0byB0aGUgYFBvcG92ZXJgIGVsZW1lbnQuXG4gICAqL1xuICBQb3BvdmVyQ2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGxlbmd0aCBvZiB0aGUgdHJhbnNpdGlvbiBpbiBgbXNgLCBvciAnYXV0bydcbiAgICovXG4gIHRyYW5zaXRpb25EdXJhdGlvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlciwgcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKHtcbiAgICBlbnRlcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcbiAgICBleGl0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG4gIH0pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhdXRvJ10pXSkuaXNSZXF1aXJlZFxufTtcblxuXG52YXIgcnRsT3JpZ2luID0ge1xuICB2ZXJ0aWNhbDogJ3RvcCcsXG4gIGhvcml6b250YWw6ICdyaWdodCdcbn07XG5cbnZhciBsdHJPcmlnaW4gPSB7XG4gIHZlcnRpY2FsOiAndG9wJyxcbiAgaG9yaXpvbnRhbDogJ2xlZnQnXG59O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSB7XG4gIHBhcGVyOiB7XG4gICAgLy8gc3BlY1o6IFRoZSBtYXhpbXVtIGhlaWdodCBvZiBhIHNpbXBsZSBtZW51IHNob3VsZCBiZSBvbmUgb3IgbW9yZSByb3dzIGxlc3MgdGhhbiB0aGUgdmlld1xuICAgIC8vIGhlaWdodC4gVGhpcyBlbnN1cmVzIGEgdGFwcGFibGUgYXJlYSBvdXRzaWRlIG9mIHRoZSBzaW1wbGUgbWVudSB3aXRoIHdoaWNoIHRvIGRpc21pc3NcbiAgICAvLyB0aGUgbWVudS5cbiAgICBtYXhIZWlnaHQ6ICdjYWxjKDEwMHZoIC0gOTZweCknLFxuICAgIC8vIEFkZCBpT1MgbW9tZW50dW0gc2Nyb2xsaW5nLlxuICAgIFdlYmtpdE92ZXJmbG93U2Nyb2xsaW5nOiAndG91Y2gnXG4gIH1cbn07XG5cbnZhciBNZW51ID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTWVudSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTWVudSgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBNZW51KTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IE1lbnUuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKE1lbnUpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5nZXRDb250ZW50QW5jaG9yRWwgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoIV90aGlzLm1lbnVMaXN0IHx8ICFfdGhpcy5tZW51TGlzdC5zZWxlY3RlZEl0ZW0pIHtcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICByZXR1cm4gKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkoX3RoaXMubWVudUxpc3QpLmZpcnN0Q2hpbGQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKShfdGhpcy5tZW51TGlzdC5zZWxlY3RlZEl0ZW0pO1xuICAgIH0sIF90aGlzLm1lbnVMaXN0ID0gdW5kZWZpbmVkLCBfdGhpcy5mb2N1cyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy5tZW51TGlzdCAmJiBfdGhpcy5tZW51TGlzdC5zZWxlY3RlZEl0ZW0pIHtcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKShfdGhpcy5tZW51TGlzdC5zZWxlY3RlZEl0ZW0pLmZvY3VzKCk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdmFyIG1lbnVMaXN0ID0gKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkoX3RoaXMubWVudUxpc3QpO1xuICAgICAgaWYgKG1lbnVMaXN0ICYmIG1lbnVMaXN0LmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICBtZW51TGlzdC5maXJzdENoaWxkLmZvY3VzKCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgdmFyIHRoZW1lID0gX3RoaXMucHJvcHMudGhlbWU7XG5cblxuICAgICAgdmFyIG1lbnVMaXN0ID0gKDAsIF9yZWFjdERvbS5maW5kRE9NTm9kZSkoX3RoaXMubWVudUxpc3QpO1xuXG4gICAgICAvLyBGb2N1cyBzbyB0aGUgc2Nyb2xsIGNvbXB1dGF0aW9uIG9mIHRoZSBQb3BvdmVyIHdvcmtzIGFzIGV4cGVjdGVkLlxuICAgICAgX3RoaXMuZm9jdXMoKTtcblxuICAgICAgLy8gTGV0J3MgaWdub3JlIHRoYXQgcGllY2Ugb2YgbG9naWMgaWYgdXNlcnMgYXJlIGFscmVhZHkgb3ZlcnJpZGluZyB0aGUgd2lkdGhcbiAgICAgIC8vIG9mIHRoZSBtZW51LlxuICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgaWYgKG1lbnVMaXN0ICYmIGVsZW1lbnQuY2xpZW50SGVpZ2h0IDwgbWVudUxpc3QuY2xpZW50SGVpZ2h0ICYmICFtZW51TGlzdC5zdHlsZS53aWR0aCkge1xuICAgICAgICB2YXIgc2l6ZSA9ICgwLCBfc2Nyb2xsYmFyU2l6ZTIuZGVmYXVsdCkoKSArICdweCc7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgbWVudUxpc3Quc3R5bGVbdGhlbWUuZGlyZWN0aW9uID09PSAncnRsJyA/ICdwYWRkaW5nTGVmdCcgOiAncGFkZGluZ1JpZ2h0J10gPSBzaXplO1xuICAgICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICAgIG1lbnVMaXN0LnN0eWxlLndpZHRoID0gJ2NhbGMoMTAwJSArICcgKyBzaXplICsgJyknO1xuICAgICAgfVxuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyKGVsZW1lbnQpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUxpc3RLZXlEb3duID0gZnVuY3Rpb24gKGV2ZW50LCBrZXkpIHtcbiAgICAgIGlmIChrZXkgPT09ICd0YWInKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgaWYgKF90aGlzLnByb3BzLm9uUmVxdWVzdENsb3NlKSB7XG4gICAgICAgICAgX3RoaXMucHJvcHMub25SZXF1ZXN0Q2xvc2UoZXZlbnQpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKE1lbnUsIFt7XG4gICAga2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLnByb3BzLm9wZW4pIHtcbiAgICAgICAgdGhpcy5mb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgIGlmICghcHJldlByb3BzLm9wZW4gJiYgdGhpcy5wcm9wcy5vcGVuKSB7XG4gICAgICAgIC8vIE5lZWRzIHRvIHJlZm9jdXMgYXMgd2hlbiBhIG1lbnUgaXMgcmVuZGVyZWQgaW50byBhbm90aGVyIE1vZGFsLFxuICAgICAgICAvLyB0aGUgZmlyc3QgbW9kYWwgbWlnaHQgY2hhbmdlIHRoZSBmb2N1cyB0byBwcmV2ZW50IGFueSBsZWFrLlxuICAgICAgICB0aGlzLmZvY3VzKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBNZW51TGlzdFByb3BzID0gX3Byb3BzLk1lbnVMaXN0UHJvcHMsXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wcy5vbkVudGVyLFxuICAgICAgICAgIF9wcm9wcyRQYXBlclByb3BzID0gX3Byb3BzLlBhcGVyUHJvcHMsXG4gICAgICAgICAgUGFwZXJQcm9wcyA9IF9wcm9wcyRQYXBlclByb3BzID09PSB1bmRlZmluZWQgPyB7fSA6IF9wcm9wcyRQYXBlclByb3BzLFxuICAgICAgICAgIFBvcG92ZXJDbGFzc2VzID0gX3Byb3BzLlBvcG92ZXJDbGFzc2VzLFxuICAgICAgICAgIHRoZW1lID0gX3Byb3BzLnRoZW1lLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnTWVudUxpc3RQcm9wcycsICdvbkVudGVyJywgJ1BhcGVyUHJvcHMnLCAnUG9wb3ZlckNsYXNzZXMnLCAndGhlbWUnXSk7XG5cblxuICAgICAgdmFyIHRoZW1lRGlyZWN0aW9uID0gdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uO1xuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfUG9wb3ZlcjIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgZ2V0Q29udGVudEFuY2hvckVsOiB0aGlzLmdldENvbnRlbnRBbmNob3JFbCxcbiAgICAgICAgICBjbGFzc2VzOiBQb3BvdmVyQ2xhc3NlcyxcbiAgICAgICAgICBvbkVudGVyOiB0aGlzLmhhbmRsZUVudGVyLFxuICAgICAgICAgIGFuY2hvck9yaWdpbjogdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnID8gcnRsT3JpZ2luIDogbHRyT3JpZ2luLFxuICAgICAgICAgIHRyYW5zZm9ybU9yaWdpbjogdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnID8gcnRsT3JpZ2luIDogbHRyT3JpZ2luLFxuICAgICAgICAgIFBhcGVyUHJvcHM6ICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe30sIFBhcGVyUHJvcHMsIHtcbiAgICAgICAgICAgIGNsYXNzZXM6ICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe30sIFBhcGVyUHJvcHMuY2xhc3Nlcywge1xuICAgICAgICAgICAgICByb290OiBjbGFzc2VzLnBhcGVyXG4gICAgICAgICAgICB9KVxuICAgICAgICAgIH0pXG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgX01lbnVMaXN0Mi5kZWZhdWx0LFxuICAgICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgICAgcm9sZTogJ21lbnUnLFxuICAgICAgICAgICAgb25LZXlEb3duOiB0aGlzLmhhbmRsZUxpc3RLZXlEb3duXG4gICAgICAgICAgfSwgTWVudUxpc3RQcm9wcywge1xuICAgICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgICBfdGhpczIubWVudUxpc3QgPSBub2RlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pLFxuICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBNZW51O1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTWVudS5kZWZhdWx0UHJvcHMgPSB7XG4gIG9wZW46IGZhbHNlLFxuICB0cmFuc2l0aW9uRHVyYXRpb246ICdhdXRvJ1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IHdpdGhUaGVtZTogdHJ1ZSwgbmFtZTogJ011aU1lbnUnIH0pKE1lbnUpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01lbnUvTWVudS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMzQiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0xpc3RJdGVtID0gcmVxdWlyZSgnLi4vTGlzdC9MaXN0SXRlbScpO1xuXG52YXIgX0xpc3RJdGVtMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0xpc3RJdGVtKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgTGlzdEl0ZW1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCB0aGVtZS50eXBvZ3JhcGh5LnN1YmhlYWRpbmcsIHtcbiAgICAgIGhlaWdodDogMjQsXG4gICAgICBib3hTaXppbmc6ICdjb250ZW50LWJveCcsXG4gICAgICBiYWNrZ3JvdW5kOiAnbm9uZScsXG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICB0ZXh0T3ZlcmZsb3c6ICdlbGxpcHNpcycsXG4gICAgICB3aGl0ZVNwYWNlOiAnbm93cmFwJyxcbiAgICAgICcmOmZvY3VzJzoge1xuICAgICAgICBiYWNrZ3JvdW5kOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlclxuICAgICAgfSxcbiAgICAgICcmOmhvdmVyJzoge1xuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyXG4gICAgICB9XG4gICAgfSksXG4gICAgc2VsZWN0ZWQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBNZW51IGl0ZW0gY29udGVudHMuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHJvbGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRvIGFwcGx5IHNlbGVjdGVkIHN0eWxpbmcuXG4gICAqL1xuICBzZWxlY3RlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxudmFyIE1lbnVJdGVtID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTWVudUl0ZW0sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIE1lbnVJdGVtKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIE1lbnVJdGVtKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoTWVudUl0ZW0uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKE1lbnVJdGVtKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShNZW51SXRlbSwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29tcG9uZW50ID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBzZWxlY3RlZCA9IF9wcm9wcy5zZWxlY3RlZCxcbiAgICAgICAgICByb2xlID0gX3Byb3BzLnJvbGUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29tcG9uZW50JywgJ3NlbGVjdGVkJywgJ3JvbGUnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5zZWxlY3RlZCwgc2VsZWN0ZWQpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9MaXN0SXRlbTIuZGVmYXVsdCwgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgIGJ1dHRvbjogdHJ1ZSxcbiAgICAgICAgcm9sZTogcm9sZSxcbiAgICAgICAgdGFiSW5kZXg6IC0xLFxuICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgY29tcG9uZW50OiBjb21wb25lbnRcbiAgICAgIH0sIG90aGVyKSk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBNZW51SXRlbTtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbk1lbnVJdGVtLmRlZmF1bHRQcm9wcyA9IHtcbiAgcm9sZTogJ21lbnVpdGVtJyxcbiAgc2VsZWN0ZWQ6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aU1lbnVJdGVtJyB9KShNZW51SXRlbSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51SXRlbS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51SXRlbS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzNCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF90b0NvbnN1bWFibGVBcnJheTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvdG9Db25zdW1hYmxlQXJyYXknKTtcblxudmFyIF90b0NvbnN1bWFibGVBcnJheTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF90b0NvbnN1bWFibGVBcnJheTIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3JlYWN0RG9tID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG5cbnZhciBfa2V5Y29kZSA9IHJlcXVpcmUoJ2tleWNvZGUnKTtcblxudmFyIF9rZXljb2RlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2tleWNvZGUpO1xuXG52YXIgX2NvbnRhaW5zID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvcXVlcnkvY29udGFpbnMnKTtcblxudmFyIF9jb250YWluczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jb250YWlucyk7XG5cbnZhciBfYWN0aXZlRWxlbWVudCA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL2FjdGl2ZUVsZW1lbnQnKTtcblxudmFyIF9hY3RpdmVFbGVtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2FjdGl2ZUVsZW1lbnQpO1xuXG52YXIgX293bmVyRG9jdW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxudmFyIF9MaXN0ID0gcmVxdWlyZSgnLi4vTGlzdCcpO1xuXG52YXIgX0xpc3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTGlzdCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IExpc3RcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogTWVudUxpc3QgY29udGVudHMsIG5vcm1hbGx5IGBNZW51SXRlbWBzLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkJsdXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbktleURvd246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jXG59O1xuXG52YXIgTWVudUxpc3QgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShNZW51TGlzdCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTWVudUxpc3QoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgTWVudUxpc3QpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gTWVudUxpc3QuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKE1lbnVMaXN0KSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBjdXJyZW50VGFiSW5kZXg6IHVuZGVmaW5lZFxuICAgIH0sIF90aGlzLmxpc3QgPSB1bmRlZmluZWQsIF90aGlzLnNlbGVjdGVkSXRlbSA9IHVuZGVmaW5lZCwgX3RoaXMuYmx1clRpbWVyID0gdW5kZWZpbmVkLCBfdGhpcy5oYW5kbGVCbHVyID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBfdGhpcy5ibHVyVGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKF90aGlzLmxpc3QpIHtcbiAgICAgICAgICB2YXIgbGlzdCA9ICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKF90aGlzLmxpc3QpO1xuICAgICAgICAgIHZhciBjdXJyZW50Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkobGlzdCkpO1xuICAgICAgICAgIGlmICghKDAsIF9jb250YWluczIuZGVmYXVsdCkobGlzdCwgY3VycmVudEZvY3VzKSkge1xuICAgICAgICAgICAgX3RoaXMucmVzZXRUYWJJbmRleCgpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfSwgMzApO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25CbHVyKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uQmx1cihldmVudCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlS2V5RG93biA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIGxpc3QgPSAoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKShfdGhpcy5saXN0KTtcbiAgICAgIHZhciBrZXkgPSAoMCwgX2tleWNvZGUyLmRlZmF1bHQpKGV2ZW50KTtcbiAgICAgIHZhciBjdXJyZW50Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkobGlzdCkpO1xuXG4gICAgICBpZiAoKGtleSA9PT0gJ3VwJyB8fCBrZXkgPT09ICdkb3duJykgJiYgKCFjdXJyZW50Rm9jdXMgfHwgY3VycmVudEZvY3VzICYmICEoMCwgX2NvbnRhaW5zMi5kZWZhdWx0KShsaXN0LCBjdXJyZW50Rm9jdXMpKSkge1xuICAgICAgICBpZiAoX3RoaXMuc2VsZWN0ZWRJdGVtKSB7XG4gICAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICAgICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKF90aGlzLnNlbGVjdGVkSXRlbSkuZm9jdXMoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICAgICAgbGlzdC5maXJzdENoaWxkLmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoa2V5ID09PSAnZG93bicpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgaWYgKGN1cnJlbnRGb2N1cy5uZXh0RWxlbWVudFNpYmxpbmcpIHtcbiAgICAgICAgICBjdXJyZW50Rm9jdXMubmV4dEVsZW1lbnRTaWJsaW5nLmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoa2V5ID09PSAndXAnKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIGlmIChjdXJyZW50Rm9jdXMucHJldmlvdXNFbGVtZW50U2libGluZykge1xuICAgICAgICAgIGN1cnJlbnRGb2N1cy5wcmV2aW91c0VsZW1lbnRTaWJsaW5nLmZvY3VzKCk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uS2V5RG93bikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbktleURvd24oZXZlbnQsIGtleSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlSXRlbUZvY3VzID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB2YXIgbGlzdCA9ICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKF90aGlzLmxpc3QpO1xuICAgICAgaWYgKGxpc3QpIHtcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxpc3QuY2hpbGRyZW4ubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICAgICAgaWYgKGxpc3QuY2hpbGRyZW5baV0gPT09IGV2ZW50LmN1cnJlbnRUYXJnZXQpIHtcbiAgICAgICAgICAgIF90aGlzLnNldFRhYkluZGV4KGkpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKE1lbnVMaXN0LCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICB0aGlzLnJlc2V0VGFiSW5kZXgoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsVW5tb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuYmx1clRpbWVyKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdzZXRUYWJJbmRleCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHNldFRhYkluZGV4KGluZGV4KSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgY3VycmVudFRhYkluZGV4OiBpbmRleCB9KTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdmb2N1cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGZvY3VzKCkge1xuICAgICAgdmFyIGN1cnJlbnRUYWJJbmRleCA9IHRoaXMuc3RhdGUuY3VycmVudFRhYkluZGV4O1xuXG4gICAgICB2YXIgbGlzdCA9ICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKHRoaXMubGlzdCk7XG4gICAgICBpZiAoIWxpc3QgfHwgIWxpc3QuY2hpbGRyZW4gfHwgIWxpc3QuZmlyc3RDaGlsZCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmIChjdXJyZW50VGFiSW5kZXggJiYgY3VycmVudFRhYkluZGV4ID49IDApIHtcbiAgICAgICAgLy8gJEZsb3dGaXhNZVxuICAgICAgICBsaXN0LmNoaWxkcmVuW2N1cnJlbnRUYWJJbmRleF0uZm9jdXMoKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgICAgbGlzdC5maXJzdENoaWxkLmZvY3VzKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVzZXRUYWJJbmRleCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlc2V0VGFiSW5kZXgoKSB7XG4gICAgICB2YXIgbGlzdCA9ICgwLCBfcmVhY3REb20uZmluZERPTU5vZGUpKHRoaXMubGlzdCk7XG4gICAgICB2YXIgY3VycmVudEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKGxpc3QpKTtcbiAgICAgIC8vICRGbG93Rml4TWVcbiAgICAgIHZhciBpdGVtcyA9IFtdLmNvbmNhdCgoMCwgX3RvQ29uc3VtYWJsZUFycmF5My5kZWZhdWx0KShsaXN0LmNoaWxkcmVuKSk7XG4gICAgICB2YXIgY3VycmVudEZvY3VzSW5kZXggPSBpdGVtcy5pbmRleE9mKGN1cnJlbnRGb2N1cyk7XG5cbiAgICAgIGlmIChjdXJyZW50Rm9jdXNJbmRleCAhPT0gLTEpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0VGFiSW5kZXgoY3VycmVudEZvY3VzSW5kZXgpO1xuICAgICAgfVxuXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZEl0ZW0pIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0VGFiSW5kZXgoaXRlbXMuaW5kZXhPZigoMCwgX3JlYWN0RG9tLmZpbmRET01Ob2RlKSh0aGlzLnNlbGVjdGVkSXRlbSkpKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMuc2V0VGFiSW5kZXgoMCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgb25CbHVyID0gX3Byb3BzLm9uQmx1cixcbiAgICAgICAgICBvbktleURvd24gPSBfcHJvcHMub25LZXlEb3duLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzTmFtZScsICdvbkJsdXInLCAnb25LZXlEb3duJ10pO1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0xpc3QyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIHJvbGU6ICdtZW51JyxcbiAgICAgICAgICByb290UmVmOiBmdW5jdGlvbiByb290UmVmKG5vZGUpIHtcbiAgICAgICAgICAgIF90aGlzMi5saXN0ID0gbm9kZTtcbiAgICAgICAgICB9LFxuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAgIG9uS2V5RG93bjogdGhpcy5oYW5kbGVLZXlEb3duLFxuICAgICAgICAgIG9uQmx1cjogdGhpcy5oYW5kbGVCbHVyXG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkLCBpbmRleCkge1xuICAgICAgICAgIGlmICghX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkKSkge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICAgIHRhYkluZGV4OiBpbmRleCA9PT0gX3RoaXMyLnN0YXRlLmN1cnJlbnRUYWJJbmRleCA/IDAgOiAtMSxcbiAgICAgICAgICAgIHJlZjogY2hpbGQucHJvcHMuc2VsZWN0ZWQgPyBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgICAgICAgICBfdGhpczIuc2VsZWN0ZWRJdGVtID0gbm9kZTtcbiAgICAgICAgICAgIH0gOiB1bmRlZmluZWQsXG4gICAgICAgICAgICBvbkZvY3VzOiBfdGhpczIuaGFuZGxlSXRlbUZvY3VzXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gTWVudUxpc3Q7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5NZW51TGlzdC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyB7XG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcbiAgb25CbHVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcbiAgb25LZXlEb3duOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufSA6IHt9O1xuZXhwb3J0cy5kZWZhdWx0ID0gTWVudUxpc3Q7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51TGlzdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9NZW51TGlzdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX01lbnUgPSByZXF1aXJlKCcuL01lbnUnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTWVudSkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfTWVudUxpc3QgPSByZXF1aXJlKCcuL01lbnVMaXN0Jyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnTWVudUxpc3QnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9NZW51TGlzdCkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfTWVudUl0ZW0gPSByZXF1aXJlKCcuL01lbnVJdGVtJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnTWVudUl0ZW0nLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9NZW51SXRlbSkuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTWVudS9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzNCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHpJbmRleDogLTEsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnMTAwJScsXG4gICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6IDAsXG4gICAgICAvLyBSZW1vdmUgZ3JleSBoaWdobGlnaHRcbiAgICAgIFdlYmtpdFRhcEhpZ2hsaWdodENvbG9yOiB0aGVtZS5wYWxldHRlLmNvbW1vbi50cmFuc3BhcmVudCxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb21tb24ubGlnaHRCbGFjayxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScpLFxuICAgICAgd2lsbENoYW5nZTogJ29wYWNpdHknLFxuICAgICAgb3BhY2l0eTogMFxuICAgIH0sXG4gICAgaW52aXNpYmxlOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuY29tbW9uLnRyYW5zcGFyZW50XG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogQ2FuIGJlIHVzZWQsIGZvciBpbnN0YW5jZSwgdG8gcmVuZGVyIGEgbGV0dGVyIGluc2lkZSB0aGUgYXZhdGFyLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBiYWNrZHJvcCBpcyBpbnZpc2libGUuXG4gICAqL1xuICBpbnZpc2libGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIEJhY2tkcm9wID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoQmFja2Ryb3AsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEJhY2tkcm9wKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEJhY2tkcm9wKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoQmFja2Ryb3AuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEJhY2tkcm9wKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShCYWNrZHJvcCwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBpbnZpc2libGUgPSBfcHJvcHMuaW52aXNpYmxlLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2ludmlzaWJsZSddKTtcblxuXG4gICAgICB2YXIgYmFja2Ryb3BDbGFzcyA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5pbnZpc2libGUsIGludmlzaWJsZSksIGNsYXNzTmFtZSk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGJhY2tkcm9wQ2xhc3MsICdhcmlhLWhpZGRlbic6ICd0cnVlJyB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQmFja2Ryb3A7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5CYWNrZHJvcC5kZWZhdWx0UHJvcHMgPSB7XG4gIGludmlzaWJsZTogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQmFja2Ryb3AnIH0pKEJhY2tkcm9wKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9CYWNrZHJvcC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvQmFja2Ryb3AuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9rZXlzID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9rZXlzJyk7XG5cbnZhciBfa2V5czIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9rZXlzKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9yZWFjdERvbSA9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG52YXIgX3JlYWN0RG9tMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0RG9tKTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX2tleWNvZGUgPSByZXF1aXJlKCdrZXljb2RlJyk7XG5cbnZhciBfa2V5Y29kZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9rZXljb2RlKTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbnZhciBfY29udGFpbnMgPSByZXF1aXJlKCdkb20taGVscGVycy9xdWVyeS9jb250YWlucycpO1xuXG52YXIgX2NvbnRhaW5zMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NvbnRhaW5zKTtcblxudmFyIF9hY3RpdmVFbGVtZW50ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudCcpO1xuXG52YXIgX2FjdGl2ZUVsZW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfYWN0aXZlRWxlbWVudCk7XG5cbnZhciBfb3duZXJEb2N1bWVudCA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQnKTtcblxudmFyIF9vd25lckRvY3VtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX293bmVyRG9jdW1lbnQpO1xuXG52YXIgX2FkZEV2ZW50TGlzdGVuZXIgPSByZXF1aXJlKCcuLi91dGlscy9hZGRFdmVudExpc3RlbmVyJyk7XG5cbnZhciBfYWRkRXZlbnRMaXN0ZW5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9hZGRFdmVudExpc3RlbmVyKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ZhZGUgPSByZXF1aXJlKCcuLi90cmFuc2l0aW9ucy9GYWRlJyk7XG5cbnZhciBfRmFkZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9GYWRlKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX21vZGFsTWFuYWdlciA9IHJlcXVpcmUoJy4vbW9kYWxNYW5hZ2VyJyk7XG5cbnZhciBfbW9kYWxNYW5hZ2VyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX21vZGFsTWFuYWdlcik7XG5cbnZhciBfQmFja2Ryb3AgPSByZXF1aXJlKCcuL0JhY2tkcm9wJyk7XG5cbnZhciBfQmFja2Ryb3AyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQmFja2Ryb3ApO1xuXG52YXIgX1BvcnRhbCA9IHJlcXVpcmUoJy4uL2ludGVybmFsL1BvcnRhbCcpO1xuXG52YXIgX1BvcnRhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Qb3J0YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbi8vIE1vZGFscyBkb24ndCBvcGVuIG9uIHRoZSBzZXJ2ZXIgc28gdGhpcyB3b24ndCBicmVhayBjb25jdXJyZW5jeS5cbi8vIENvdWxkIGFsc28gcHV0IHRoaXMgb24gY29udGV4dC5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBtb2RhbE1hbmFnZXIgPSAoMCwgX21vZGFsTWFuYWdlcjIuZGVmYXVsdCkoKTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJzEwMCUnLFxuICAgICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgICB6SW5kZXg6IHRoZW1lLnpJbmRleC5kaWFsb2csXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAwXG4gICAgfSxcbiAgICBoaWRkZW46IHtcbiAgICAgIHZpc2liaWxpdHk6ICdoaWRkZW4nXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIENTUyBjbGFzcyBuYW1lIG9mIHRoZSBiYWNrZHJvcCBlbGVtZW50LlxuICAgKi9cbiAgQmFja2Ryb3BDbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFBhc3MgYSBjb21wb25lbnQgY2xhc3MgdG8gdXNlIGFzIHRoZSBiYWNrZHJvcC5cbiAgICovXG4gIEJhY2tkcm9wQ29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBiYWNrZHJvcCBpcyBpbnZpc2libGUuXG4gICAqL1xuICBCYWNrZHJvcEludmlzaWJsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGR1cmF0aW9uIGZvciB0aGUgYmFja2Ryb3AgdHJhbnNpdGlvbiwgaW4gbWlsbGlzZWNvbmRzLlxuICAgKiBZb3UgbWF5IHNwZWNpZnkgYSBzaW5nbGUgdGltZW91dCBmb3IgYWxsIHRyYW5zaXRpb25zLCBvciBpbmRpdmlkdWFsbHkgd2l0aCBhbiBvYmplY3QuXG4gICAqL1xuICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBBIHNpbmdsZSBjaGlsZCBjb250ZW50IGVsZW1lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsd2F5cyBrZWVwIHRoZSBjaGlsZHJlbiBpbiB0aGUgRE9NLlxuICAgKiBUaGlzIHByb3BlcnR5IGNhbiBiZSB1c2VmdWwgaW4gU0VPIHNpdHVhdGlvbiBvclxuICAgKiB3aGVuIHlvdSB3YW50IHRvIG1heGltaXplIHRoZSByZXNwb25zaXZlbmVzcyBvZiB0aGUgTW9kYWwuXG4gICAqL1xuICBrZWVwTW91bnRlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYmFja2Ryb3AgaXMgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlQmFja2Ryb3A6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgY2xpY2tpbmcgdGhlIGJhY2tkcm9wIHdpbGwgbm90IGZpcmUgdGhlIGBvblJlcXVlc3RDbG9zZWAgY2FsbGJhY2suXG4gICAqL1xuICBpZ25vcmVCYWNrZHJvcENsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGhpdHRpbmcgZXNjYXBlIHdpbGwgbm90IGZpcmUgdGhlIGBvblJlcXVlc3RDbG9zZWAgY2FsbGJhY2suXG4gICAqL1xuICBpZ25vcmVFc2NhcGVLZXlVcDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgbW9kYWxNYW5hZ2VyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVzIHdoZW4gdGhlIGJhY2tkcm9wIGlzIGNsaWNrZWQgb24uXG4gICAqL1xuICBvbkJhY2tkcm9wQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIG1vZGFsIGlzIGVudGVyaW5nLlxuICAgKi9cbiAgb25FbnRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBtb2RhbCBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaGFzIGVudGVyZWQuXG4gICAqL1xuICBvbkVudGVyZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZXMgd2hlbiB0aGUgZXNjYXBlIGtleSBpcyBwcmVzc2VkIGFuZCB0aGUgbW9kYWwgaXMgaW4gZm9jdXMuXG4gICAqL1xuICBvbkVzY2FwZUtleVVwOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBtb2RhbCBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGlzIGV4aXRpbmcuXG4gICAqL1xuICBvbkV4aXRpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaGFzIGV4aXRlZC5cbiAgICovXG4gIG9uRXhpdGVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCByZXF1ZXN0cyB0byBiZSBjbG9zZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKi9cbiAgb25SZXF1ZXN0Q2xvc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBNb2RhbCBpcyB2aXNpYmxlLlxuICAgKi9cbiAgc2hvdzogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxuLyoqXG4gKiBUaGUgbW9kYWwgY29tcG9uZW50IHByb3ZpZGVzIGEgc29saWQgZm91bmRhdGlvbiBmb3IgY3JlYXRpbmcgZGlhbG9ncyxcbiAqIHBvcG92ZXJzLCBvciB3aGF0ZXZlciBlbHNlLlxuICogVGhlIGNvbXBvbmVudCByZW5kZXJzIGl0cyBgY2hpbGRyZW5gIG5vZGUgaW4gZnJvbnQgb2YgYSBiYWNrZHJvcCBjb21wb25lbnQuXG4gKlxuICogVGhlIGBNb2RhbGAgb2ZmZXJzIGEgZmV3IGhlbHBmdWwgZmVhdHVyZXMgb3ZlciB1c2luZyBqdXN0IGEgYFBvcnRhbGAgY29tcG9uZW50IGFuZCBzb21lIHN0eWxlczpcbiAqIC0gTWFuYWdlcyBkaWFsb2cgc3RhY2tpbmcgd2hlbiBvbmUtYXQtYS10aW1lIGp1c3QgaXNuJ3QgZW5vdWdoLlxuICogLSBDcmVhdGVzIGEgYmFja2Ryb3AsIGZvciBkaXNhYmxpbmcgaW50ZXJhY3Rpb24gYmVsb3cgdGhlIG1vZGFsLlxuICogLSBJdCBwcm9wZXJseSBtYW5hZ2VzIGZvY3VzOyBtb3ZpbmcgdG8gdGhlIG1vZGFsIGNvbnRlbnQsXG4gKiAgIGFuZCBrZWVwaW5nIGl0IHRoZXJlIHVudGlsIHRoZSBtb2RhbCBpcyBjbG9zZWQuXG4gKiAtIEl0IGRpc2FibGVzIHNjcm9sbGluZyBvZiB0aGUgcGFnZSBjb250ZW50IHdoaWxlIG9wZW4uXG4gKiAtIEFkZHMgdGhlIGFwcHJvcHJpYXRlIEFSSUEgcm9sZXMgYXJlIGF1dG9tYXRpY2FsbHkuXG4gKlxuICogVGhpcyBjb21wb25lbnQgc2hhcmVzIG1hbnkgY29uY2VwdHMgd2l0aCBbcmVhY3Qtb3ZlcmxheXNdKGh0dHBzOi8vcmVhY3QtYm9vdHN0cmFwLmdpdGh1Yi5pby9yZWFjdC1vdmVybGF5cy8jbW9kYWxzKS5cbiAqL1xudmFyIE1vZGFsID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTW9kYWwsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIE1vZGFsKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIE1vZGFsKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IE1vZGFsLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShNb2RhbCkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF9pbml0aWFsaXNlUHJvcHMuY2FsbChfdGhpcyksIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShNb2RhbCwgW3tcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgICBpZiAoIXRoaXMucHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZXhpdGVkOiB0cnVlIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICB0aGlzLm1vdW50ZWQgPSB0cnVlO1xuICAgICAgaWYgKHRoaXMucHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLmhhbmRsZVNob3coKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcbiAgICAgIGlmIChuZXh0UHJvcHMuc2hvdyAmJiB0aGlzLnN0YXRlLmV4aXRlZCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZXhpdGVkOiBmYWxzZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgIGlmICghdGhpcy5wcm9wcy5zaG93ICYmIG5leHRQcm9wcy5zaG93KSB7XG4gICAgICAgIHRoaXMuY2hlY2tGb3JGb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgIGlmICghcHJldlByb3BzLnNob3cgJiYgdGhpcy5wcm9wcy5zaG93KSB7XG4gICAgICAgIHRoaXMuaGFuZGxlU2hvdygpO1xuICAgICAgfVxuICAgICAgLy8gV2UgYXJlIHdhaXRpbmcgZm9yIHRoZSBvbkV4aXRlZCBjYWxsYmFjayB0byBjYWxsIGhhbmRsZUhpZGUuXG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLnByb3BzLnNob3cgfHwgIXRoaXMuc3RhdGUuZXhpdGVkKSB7XG4gICAgICAgIHRoaXMuaGFuZGxlSGlkZSgpO1xuICAgICAgfVxuICAgICAgdGhpcy5tb3VudGVkID0gZmFsc2U7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY2hlY2tGb3JGb2N1cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNoZWNrRm9yRm9jdXMoKSB7XG4gICAgICBpZiAoX2luRE9NMi5kZWZhdWx0KSB7XG4gICAgICAgIHRoaXMubGFzdEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3Jlc3RvcmVMYXN0Rm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXN0b3JlTGFzdEZvY3VzKCkge1xuICAgICAgaWYgKHRoaXMubGFzdEZvY3VzICYmIHRoaXMubGFzdEZvY3VzLmZvY3VzKSB7XG4gICAgICAgIHRoaXMubGFzdEZvY3VzLmZvY3VzKCk7XG4gICAgICAgIHRoaXMubGFzdEZvY3VzID0gdW5kZWZpbmVkO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2hhbmRsZVNob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYW5kbGVTaG93KCkge1xuICAgICAgdmFyIGRvYyA9ICgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKHRoaXMpKTtcbiAgICAgIHRoaXMucHJvcHMubW9kYWxNYW5hZ2VyLmFkZCh0aGlzKTtcbiAgICAgIHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIgPSAoMCwgX2FkZEV2ZW50TGlzdGVuZXIyLmRlZmF1bHQpKGRvYywgJ2tleXVwJywgdGhpcy5oYW5kbGVEb2N1bWVudEtleVVwKTtcbiAgICAgIHRoaXMub25Gb2N1c0xpc3RlbmVyID0gKDAsIF9hZGRFdmVudExpc3RlbmVyMi5kZWZhdWx0KShkb2MsICdmb2N1cycsIHRoaXMuaGFuZGxlRm9jdXNMaXN0ZW5lciwgdHJ1ZSk7XG4gICAgICB0aGlzLmZvY3VzKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICAgIHZhciBjdXJyZW50Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKHRoaXMpKSk7XG4gICAgICB2YXIgbW9kYWxDb250ZW50ID0gdGhpcy5tb2RhbCAmJiB0aGlzLm1vZGFsLmxhc3RDaGlsZDtcbiAgICAgIHZhciBmb2N1c0luTW9kYWwgPSBjdXJyZW50Rm9jdXMgJiYgKDAsIF9jb250YWluczIuZGVmYXVsdCkobW9kYWxDb250ZW50LCBjdXJyZW50Rm9jdXMpO1xuXG4gICAgICBpZiAobW9kYWxDb250ZW50ICYmICFmb2N1c0luTW9kYWwpIHtcbiAgICAgICAgaWYgKCFtb2RhbENvbnRlbnQuaGFzQXR0cmlidXRlKCd0YWJJbmRleCcpKSB7XG4gICAgICAgICAgbW9kYWxDb250ZW50LnNldEF0dHJpYnV0ZSgndGFiSW5kZXgnLCAtMSk7XG4gICAgICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KShmYWxzZSwgJ01hdGVyaWFsLVVJOiB0aGUgbW9kYWwgY29udGVudCBub2RlIGRvZXMgbm90IGFjY2VwdCBmb2N1cy4gJyArICdGb3IgdGhlIGJlbmVmaXQgb2YgYXNzaXN0aXZlIHRlY2hub2xvZ2llcywgJyArICd0aGUgdGFiSW5kZXggb2YgdGhlIG5vZGUgaXMgYmVpbmcgc2V0IHRvIFwiLTFcIi4nKSA6IHZvaWQgMDtcbiAgICAgICAgfVxuXG4gICAgICAgIG1vZGFsQ29udGVudC5mb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2hhbmRsZUhpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYW5kbGVIaWRlKCkge1xuICAgICAgdGhpcy5wcm9wcy5tb2RhbE1hbmFnZXIucmVtb3ZlKHRoaXMpO1xuICAgICAgaWYgKHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIpIHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIucmVtb3ZlKCk7XG4gICAgICBpZiAodGhpcy5vbkZvY3VzTGlzdGVuZXIpIHRoaXMub25Gb2N1c0xpc3RlbmVyLnJlbW92ZSgpO1xuICAgICAgdGhpcy5yZXN0b3JlTGFzdEZvY3VzKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyQmFja2Ryb3AnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXJCYWNrZHJvcCgpIHtcbiAgICAgIHZhciBvdGhlciA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBCYWNrZHJvcENvbXBvbmVudCA9IF9wcm9wcy5CYWNrZHJvcENvbXBvbmVudCxcbiAgICAgICAgICBCYWNrZHJvcENsYXNzTmFtZSA9IF9wcm9wcy5CYWNrZHJvcENsYXNzTmFtZSxcbiAgICAgICAgICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiA9IF9wcm9wcy5CYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgICBCYWNrZHJvcEludmlzaWJsZSA9IF9wcm9wcy5CYWNrZHJvcEludmlzaWJsZSxcbiAgICAgICAgICBzaG93ID0gX3Byb3BzLnNob3c7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfRmFkZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGFwcGVhcjogdHJ1ZSwgJ2luJzogc2hvdywgdGltZW91dDogQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gfSwgb3RoZXIpLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChCYWNrZHJvcENvbXBvbmVudCwge1xuICAgICAgICAgIGludmlzaWJsZTogQmFja2Ryb3BJbnZpc2libGUsXG4gICAgICAgICAgY2xhc3NOYW1lOiBCYWNrZHJvcENsYXNzTmFtZSxcbiAgICAgICAgICBvbkNsaWNrOiB0aGlzLmhhbmRsZUJhY2tkcm9wQ2xpY2tcbiAgICAgICAgfSlcbiAgICAgICk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBkaXNhYmxlQmFja2Ryb3AgPSBfcHJvcHMyLmRpc2FibGVCYWNrZHJvcCxcbiAgICAgICAgICBCYWNrZHJvcENvbXBvbmVudCA9IF9wcm9wczIuQmFja2Ryb3BDb21wb25lbnQsXG4gICAgICAgICAgQmFja2Ryb3BDbGFzc05hbWUgPSBfcHJvcHMyLkJhY2tkcm9wQ2xhc3NOYW1lLFxuICAgICAgICAgIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uID0gX3Byb3BzMi5CYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgICBCYWNrZHJvcEludmlzaWJsZSA9IF9wcm9wczIuQmFja2Ryb3BJbnZpc2libGUsXG4gICAgICAgICAgaWdub3JlQmFja2Ryb3BDbGljayA9IF9wcm9wczIuaWdub3JlQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcCA9IF9wcm9wczIuaWdub3JlRXNjYXBlS2V5VXAsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMyLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMyLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzMi5jbGFzc05hbWUsXG4gICAgICAgICAga2VlcE1vdW50ZWQgPSBfcHJvcHMyLmtlZXBNb3VudGVkLFxuICAgICAgICAgIG1vZGFsTWFuYWdlclByb3AgPSBfcHJvcHMyLm1vZGFsTWFuYWdlcixcbiAgICAgICAgICBvbkJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMyLm9uQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBvbkVzY2FwZUtleVVwID0gX3Byb3BzMi5vbkVzY2FwZUtleVVwLFxuICAgICAgICAgIG9uUmVxdWVzdENsb3NlID0gX3Byb3BzMi5vblJlcXVlc3RDbG9zZSxcbiAgICAgICAgICBvbkVudGVyID0gX3Byb3BzMi5vbkVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmcgPSBfcHJvcHMyLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FbnRlcmVkID0gX3Byb3BzMi5vbkVudGVyZWQsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzMi5vbkV4aXQsXG4gICAgICAgICAgb25FeGl0aW5nID0gX3Byb3BzMi5vbkV4aXRpbmcsXG4gICAgICAgICAgb25FeGl0ZWQgPSBfcHJvcHMyLm9uRXhpdGVkLFxuICAgICAgICAgIHNob3cgPSBfcHJvcHMyLnNob3csXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMyLCBbJ2Rpc2FibGVCYWNrZHJvcCcsICdCYWNrZHJvcENvbXBvbmVudCcsICdCYWNrZHJvcENsYXNzTmFtZScsICdCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbicsICdCYWNrZHJvcEludmlzaWJsZScsICdpZ25vcmVCYWNrZHJvcENsaWNrJywgJ2lnbm9yZUVzY2FwZUtleVVwJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2tlZXBNb3VudGVkJywgJ21vZGFsTWFuYWdlcicsICdvbkJhY2tkcm9wQ2xpY2snLCAnb25Fc2NhcGVLZXlVcCcsICdvblJlcXVlc3RDbG9zZScsICdvbkVudGVyJywgJ29uRW50ZXJpbmcnLCAnb25FbnRlcmVkJywgJ29uRXhpdCcsICdvbkV4aXRpbmcnLCAnb25FeGl0ZWQnLCAnc2hvdyddKTtcblxuXG4gICAgICBpZiAoIWtlZXBNb3VudGVkICYmICFzaG93ICYmIHRoaXMuc3RhdGUuZXhpdGVkKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgdHJhbnNpdGlvbkNhbGxiYWNrcyA9IHtcbiAgICAgICAgb25FbnRlcjogb25FbnRlcixcbiAgICAgICAgb25FbnRlcmluZzogb25FbnRlcmluZyxcbiAgICAgICAgb25FbnRlcmVkOiBvbkVudGVyZWQsXG4gICAgICAgIG9uRXhpdDogb25FeGl0LFxuICAgICAgICBvbkV4aXRpbmc6IG9uRXhpdGluZyxcbiAgICAgICAgb25FeGl0ZWQ6IHRoaXMuaGFuZGxlVHJhbnNpdGlvbkV4aXRlZFxuICAgICAgfTtcblxuICAgICAgdmFyIG1vZGFsQ2hpbGQgPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ub25seShjaGlsZHJlbik7XG4gICAgICB2YXIgX21vZGFsQ2hpbGQkcHJvcHMgPSBtb2RhbENoaWxkLnByb3BzLFxuICAgICAgICAgIHJvbGUgPSBfbW9kYWxDaGlsZCRwcm9wcy5yb2xlLFxuICAgICAgICAgIHRhYkluZGV4ID0gX21vZGFsQ2hpbGQkcHJvcHMudGFiSW5kZXg7XG5cbiAgICAgIHZhciBjaGlsZFByb3BzID0ge307XG5cbiAgICAgIGlmIChyb2xlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgY2hpbGRQcm9wcy5yb2xlID0gcm9sZSA9PT0gdW5kZWZpbmVkID8gJ2RvY3VtZW50JyA6IHJvbGU7XG4gICAgICB9XG5cbiAgICAgIGlmICh0YWJJbmRleCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGNoaWxkUHJvcHMudGFiSW5kZXggPSB0YWJJbmRleCA9PSBudWxsID8gLTEgOiB0YWJJbmRleDtcbiAgICAgIH1cblxuICAgICAgdmFyIGJhY2tkcm9wUHJvcHMgPSB2b2lkIDA7XG5cbiAgICAgIC8vIEl0J3MgYSBUcmFuc2l0aW9uIGxpa2UgY29tcG9uZW50XG4gICAgICBpZiAobW9kYWxDaGlsZC5wcm9wcy5oYXNPd25Qcm9wZXJ0eSgnaW4nKSkge1xuICAgICAgICAoMCwgX2tleXMyLmRlZmF1bHQpKHRyYW5zaXRpb25DYWxsYmFja3MpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIGNoaWxkUHJvcHNba2V5XSA9ICgwLCBfaGVscGVycy5jcmVhdGVDaGFpbmVkRnVuY3Rpb24pKHRyYW5zaXRpb25DYWxsYmFja3Nba2V5XSwgbW9kYWxDaGlsZC5wcm9wc1trZXldKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBiYWNrZHJvcFByb3BzID0gdHJhbnNpdGlvbkNhbGxiYWNrcztcbiAgICAgIH1cblxuICAgICAgaWYgKCgwLCBfa2V5czIuZGVmYXVsdCkoY2hpbGRQcm9wcykubGVuZ3RoKSB7XG4gICAgICAgIG1vZGFsQ2hpbGQgPSBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KG1vZGFsQ2hpbGQsIGNoaWxkUHJvcHMpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9Qb3J0YWwyLmRlZmF1bHQsXG4gICAgICAgIHtcbiAgICAgICAgICBvcGVuOiB0cnVlLFxuICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgIF90aGlzMi5tb3VudE5vZGUgPSBub2RlID8gbm9kZS5nZXRMYXllcigpIDogbnVsbDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5oaWRkZW4sIHRoaXMuc3RhdGUuZXhpdGVkKSlcbiAgICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgICBfdGhpczIubW9kYWwgPSBub2RlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pLFxuICAgICAgICAgICFkaXNhYmxlQmFja2Ryb3AgJiYgKCFrZWVwTW91bnRlZCB8fCBzaG93IHx8ICF0aGlzLnN0YXRlLmV4aXRlZCkgJiYgdGhpcy5yZW5kZXJCYWNrZHJvcChiYWNrZHJvcFByb3BzKSxcbiAgICAgICAgICBtb2RhbENoaWxkXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBNb2RhbDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbk1vZGFsLmRlZmF1bHRQcm9wcyA9IHtcbiAgQmFja2Ryb3BDb21wb25lbnQ6IF9CYWNrZHJvcDIuZGVmYXVsdCxcbiAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb246IDMwMCxcbiAgQmFja2Ryb3BJbnZpc2libGU6IGZhbHNlLFxuICBrZWVwTW91bnRlZDogZmFsc2UsXG4gIGRpc2FibGVCYWNrZHJvcDogZmFsc2UsXG4gIGlnbm9yZUJhY2tkcm9wQ2xpY2s6IGZhbHNlLFxuICBpZ25vcmVFc2NhcGVLZXlVcDogZmFsc2UsXG4gIG1vZGFsTWFuYWdlcjogbW9kYWxNYW5hZ2VyXG59O1xuXG52YXIgX2luaXRpYWxpc2VQcm9wcyA9IGZ1bmN0aW9uIF9pbml0aWFsaXNlUHJvcHMoKSB7XG4gIHZhciBfdGhpczMgPSB0aGlzO1xuXG4gIHRoaXMuc3RhdGUgPSB7XG4gICAgZXhpdGVkOiBmYWxzZVxuICB9O1xuICB0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyID0gbnVsbDtcbiAgdGhpcy5vbkZvY3VzTGlzdGVuZXIgPSBudWxsO1xuICB0aGlzLm1vdW50ZWQgPSBmYWxzZTtcbiAgdGhpcy5sYXN0Rm9jdXMgPSB1bmRlZmluZWQ7XG4gIHRoaXMubW9kYWwgPSBudWxsO1xuICB0aGlzLm1vdW50Tm9kZSA9IG51bGw7XG5cbiAgdGhpcy5oYW5kbGVGb2N1c0xpc3RlbmVyID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICghX3RoaXMzLm1vdW50ZWQgfHwgIV90aGlzMy5wcm9wcy5tb2RhbE1hbmFnZXIuaXNUb3BNb2RhbChfdGhpczMpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGN1cnJlbnRGb2N1cyA9ICgwLCBfYWN0aXZlRWxlbWVudDIuZGVmYXVsdCkoKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShfcmVhY3REb20yLmRlZmF1bHQuZmluZERPTU5vZGUoX3RoaXMzKSkpO1xuICAgIHZhciBtb2RhbENvbnRlbnQgPSBfdGhpczMubW9kYWwgJiYgX3RoaXMzLm1vZGFsLmxhc3RDaGlsZDtcblxuICAgIGlmIChtb2RhbENvbnRlbnQgJiYgbW9kYWxDb250ZW50ICE9PSBjdXJyZW50Rm9jdXMgJiYgISgwLCBfY29udGFpbnMyLmRlZmF1bHQpKG1vZGFsQ29udGVudCwgY3VycmVudEZvY3VzKSkge1xuICAgICAgbW9kYWxDb250ZW50LmZvY3VzKCk7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMuaGFuZGxlRG9jdW1lbnRLZXlVcCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIGlmICghX3RoaXMzLm1vdW50ZWQgfHwgIV90aGlzMy5wcm9wcy5tb2RhbE1hbmFnZXIuaXNUb3BNb2RhbChfdGhpczMpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCgwLCBfa2V5Y29kZTIuZGVmYXVsdCkoZXZlbnQpICE9PSAnZXNjJykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBfcHJvcHMzID0gX3RoaXMzLnByb3BzLFxuICAgICAgICBvbkVzY2FwZUtleVVwID0gX3Byb3BzMy5vbkVzY2FwZUtleVVwLFxuICAgICAgICBvblJlcXVlc3RDbG9zZSA9IF9wcm9wczMub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgIGlnbm9yZUVzY2FwZUtleVVwID0gX3Byb3BzMy5pZ25vcmVFc2NhcGVLZXlVcDtcblxuXG4gICAgaWYgKG9uRXNjYXBlS2V5VXApIHtcbiAgICAgIG9uRXNjYXBlS2V5VXAoZXZlbnQpO1xuICAgIH1cblxuICAgIGlmIChvblJlcXVlc3RDbG9zZSAmJiAhaWdub3JlRXNjYXBlS2V5VXApIHtcbiAgICAgIG9uUmVxdWVzdENsb3NlKGV2ZW50KTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5oYW5kbGVCYWNrZHJvcENsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgaWYgKGV2ZW50LnRhcmdldCAhPT0gZXZlbnQuY3VycmVudFRhcmdldCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBfcHJvcHM0ID0gX3RoaXMzLnByb3BzLFxuICAgICAgICBvbkJhY2tkcm9wQ2xpY2sgPSBfcHJvcHM0Lm9uQmFja2Ryb3BDbGljayxcbiAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHM0Lm9uUmVxdWVzdENsb3NlLFxuICAgICAgICBpZ25vcmVCYWNrZHJvcENsaWNrID0gX3Byb3BzNC5pZ25vcmVCYWNrZHJvcENsaWNrO1xuXG5cbiAgICBpZiAob25CYWNrZHJvcENsaWNrKSB7XG4gICAgICBvbkJhY2tkcm9wQ2xpY2soZXZlbnQpO1xuICAgIH1cblxuICAgIGlmIChvblJlcXVlc3RDbG9zZSAmJiAhaWdub3JlQmFja2Ryb3BDbGljaykge1xuICAgICAgb25SZXF1ZXN0Q2xvc2UoZXZlbnQpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLmhhbmRsZVRyYW5zaXRpb25FeGl0ZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKF90aGlzMy5wcm9wcy5vbkV4aXRlZCkge1xuICAgICAgdmFyIF9wcm9wczU7XG5cbiAgICAgIChfcHJvcHM1ID0gX3RoaXMzLnByb3BzKS5vbkV4aXRlZC5hcHBseShfcHJvcHM1LCBhcmd1bWVudHMpO1xuICAgIH1cblxuICAgIF90aGlzMy5zZXRTdGF0ZSh7IGV4aXRlZDogdHJ1ZSB9KTtcbiAgICBfdGhpczMuaGFuZGxlSGlkZSgpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgZmxpcDogZmFsc2UsIG5hbWU6ICdNdWlNb2RhbCcgfSkoTW9kYWwpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL01vZGFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9Nb2RhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfTW9kYWwgPSByZXF1aXJlKCcuL01vZGFsJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX01vZGFsKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3dhcm5pbmcgPSByZXF1aXJlKCd3YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxudmFyIF9pc1dpbmRvdyA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3F1ZXJ5L2lzV2luZG93Jyk7XG5cbnZhciBfaXNXaW5kb3cyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNXaW5kb3cpO1xuXG52YXIgX293bmVyRG9jdW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbnZhciBfc2Nyb2xsYmFyU2l6ZSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZScpO1xuXG52YXIgX3Njcm9sbGJhclNpemUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2Nyb2xsYmFyU2l6ZSk7XG5cbnZhciBfbWFuYWdlQXJpYUhpZGRlbiA9IHJlcXVpcmUoJy4uL3V0aWxzL21hbmFnZUFyaWFIaWRkZW4nKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuLy8gVGFrZW4gZnJvbSBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtYm9vdHN0cmFwL3JlYWN0LW92ZXJsYXlzL2Jsb2IvbWFzdGVyL3NyYy9Nb2RhbE1hbmFnZXIuanNcblxuZnVuY3Rpb24gZ2V0UGFkZGluZ1JpZ2h0KG5vZGUpIHtcbiAgcmV0dXJuIHBhcnNlSW50KG5vZGUuc3R5bGUucGFkZGluZ1JpZ2h0IHx8IDAsIDEwKTtcbn1cblxuLy8gRG8gd2UgaGF2ZSBhIHNjcm9sbCBiYXI/XG5mdW5jdGlvbiBib2R5SXNPdmVyZmxvd2luZyhub2RlKSB7XG4gIHZhciBkb2MgPSAoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKG5vZGUpO1xuICB2YXIgd2luID0gKDAsIF9pc1dpbmRvdzIuZGVmYXVsdCkoZG9jKTtcblxuICAvLyBUYWtlcyBpbiBhY2NvdW50IHBvdGVudGlhbCBub24gemVybyBtYXJnaW4gb24gdGhlIGJvZHkuXG4gIHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvYy5ib2R5KTtcbiAgdmFyIG1hcmdpbkxlZnQgPSBwYXJzZUludChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdtYXJnaW4tbGVmdCcpLCAxMCk7XG4gIHZhciBtYXJnaW5SaWdodCA9IHBhcnNlSW50KHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ21hcmdpbi1yaWdodCcpLCAxMCk7XG5cbiAgcmV0dXJuIG1hcmdpbkxlZnQgKyBkb2MuYm9keS5jbGllbnRXaWR0aCArIG1hcmdpblJpZ2h0IDwgd2luLmlubmVyV2lkdGg7XG59XG5cbmZ1bmN0aW9uIGdldENvbnRhaW5lcigpIHtcbiAgdmFyIGNvbnRhaW5lciA9IF9pbkRPTTIuZGVmYXVsdCA/IHdpbmRvdy5kb2N1bWVudC5ib2R5IDoge307XG4gIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkoY29udGFpbmVyICE9PSBudWxsLCAnXFxuTWF0ZXJpYWwtVUk6IHlvdSBhcmUgbW9zdCBsaWtlbHkgZXZhbHVhdGluZyB0aGUgY29kZSBiZWZvcmUgdGhlXFxuYnJvd3NlciBoYXMgYSBjaGFuY2UgdG8gcmVhY2ggdGhlIDxib2R5Pi5cXG5QbGVhc2UgbW92ZSB0aGUgaW1wb3J0IGF0IHRoZSBlbmQgb2YgdGhlIDxib2R5Pi5cXG4gICcpIDogdm9pZCAwO1xuICByZXR1cm4gY29udGFpbmVyO1xufVxuLyoqXG4gKiBTdGF0ZSBtYW5hZ2VtZW50IGhlbHBlciBmb3IgbW9kYWxzL2xheWVycy5cbiAqIFNpbXBsaWZpZWQsIGJ1dCBpbnNwaXJlZCBieSByZWFjdC1vdmVybGF5J3MgTW9kYWxNYW5hZ2VyIGNsYXNzXG4gKlxuICogQGludGVybmFsIFVzZWQgYnkgdGhlIE1vZGFsIHRvIGVuc3VyZSBwcm9wZXIgZm9jdXMgbWFuYWdlbWVudC5cbiAqL1xuZnVuY3Rpb24gY3JlYXRlTW9kYWxNYW5hZ2VyKCkge1xuICB2YXIgX3JlZiA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge30sXG4gICAgICBfcmVmJGhpZGVTaWJsaW5nTm9kZXMgPSBfcmVmLmhpZGVTaWJsaW5nTm9kZXMsXG4gICAgICBoaWRlU2libGluZ05vZGVzID0gX3JlZiRoaWRlU2libGluZ05vZGVzID09PSB1bmRlZmluZWQgPyB0cnVlIDogX3JlZiRoaWRlU2libGluZ05vZGVzO1xuXG4gIHZhciBtb2RhbHMgPSBbXTtcblxuICB2YXIgcHJldk92ZXJmbG93ID0gdm9pZCAwO1xuICB2YXIgcHJldlBhZGRpbmdzID0gW107XG5cbiAgZnVuY3Rpb24gYWRkKG1vZGFsKSB7XG4gICAgdmFyIGNvbnRhaW5lciA9IGdldENvbnRhaW5lcigpO1xuICAgIHZhciBtb2RhbElkeCA9IG1vZGFscy5pbmRleE9mKG1vZGFsKTtcblxuICAgIGlmIChtb2RhbElkeCAhPT0gLTEpIHtcbiAgICAgIHJldHVybiBtb2RhbElkeDtcbiAgICB9XG5cbiAgICBtb2RhbElkeCA9IG1vZGFscy5sZW5ndGg7XG4gICAgbW9kYWxzLnB1c2gobW9kYWwpO1xuXG4gICAgaWYgKGhpZGVTaWJsaW5nTm9kZXMpIHtcbiAgICAgICgwLCBfbWFuYWdlQXJpYUhpZGRlbi5oaWRlU2libGluZ3MpKGNvbnRhaW5lciwgbW9kYWwubW91bnROb2RlKTtcbiAgICB9XG5cbiAgICBpZiAobW9kYWxzLmxlbmd0aCA9PT0gMSkge1xuICAgICAgLy8gU2F2ZSBvdXIgY3VycmVudCBvdmVyZmxvdyBzbyB3ZSBjYW4gcmV2ZXJ0XG4gICAgICAvLyBiYWNrIHRvIGl0IHdoZW4gYWxsIG1vZGFscyBhcmUgY2xvc2VkIVxuICAgICAgcHJldk92ZXJmbG93ID0gY29udGFpbmVyLnN0eWxlLm92ZXJmbG93O1xuXG4gICAgICBpZiAoYm9keUlzT3ZlcmZsb3dpbmcoY29udGFpbmVyKSkge1xuICAgICAgICBwcmV2UGFkZGluZ3MgPSBbZ2V0UGFkZGluZ1JpZ2h0KGNvbnRhaW5lcildO1xuICAgICAgICB2YXIgc2Nyb2xsYmFyU2l6ZSA9ICgwLCBfc2Nyb2xsYmFyU2l6ZTIuZGVmYXVsdCkoKTtcbiAgICAgICAgY29udGFpbmVyLnN0eWxlLnBhZGRpbmdSaWdodCA9IHByZXZQYWRkaW5nc1swXSArIHNjcm9sbGJhclNpemUgKyAncHgnO1xuXG4gICAgICAgIHZhciBmaXhlZE5vZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm11aS1maXhlZCcpO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZpeGVkTm9kZXMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICB2YXIgcGFkZGluZ1JpZ2h0ID0gZ2V0UGFkZGluZ1JpZ2h0KGZpeGVkTm9kZXNbaV0pO1xuICAgICAgICAgIHByZXZQYWRkaW5ncy5wdXNoKHBhZGRpbmdSaWdodCk7XG4gICAgICAgICAgZml4ZWROb2Rlc1tpXS5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwYWRkaW5nUmlnaHQgKyBzY3JvbGxiYXJTaXplICsgJ3B4JztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBjb250YWluZXIuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcbiAgICB9XG5cbiAgICByZXR1cm4gbW9kYWxJZHg7XG4gIH1cblxuICBmdW5jdGlvbiByZW1vdmUobW9kYWwpIHtcbiAgICB2YXIgY29udGFpbmVyID0gZ2V0Q29udGFpbmVyKCk7XG4gICAgdmFyIG1vZGFsSWR4ID0gbW9kYWxzLmluZGV4T2YobW9kYWwpO1xuXG4gICAgaWYgKG1vZGFsSWR4ID09PSAtMSkge1xuICAgICAgcmV0dXJuIG1vZGFsSWR4O1xuICAgIH1cblxuICAgIG1vZGFscy5zcGxpY2UobW9kYWxJZHgsIDEpO1xuXG4gICAgaWYgKG1vZGFscy5sZW5ndGggPT09IDApIHtcbiAgICAgIGNvbnRhaW5lci5zdHlsZS5vdmVyZmxvdyA9IHByZXZPdmVyZmxvdztcbiAgICAgIGNvbnRhaW5lci5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwcmV2UGFkZGluZ3NbMF07XG5cbiAgICAgIHZhciBmaXhlZE5vZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm11aS1maXhlZCcpO1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaXhlZE5vZGVzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgIGZpeGVkTm9kZXNbaV0uc3R5bGUucGFkZGluZ1JpZ2h0ID0gcHJldlBhZGRpbmdzW2kgKyAxXSArICdweCc7XG4gICAgICB9XG5cbiAgICAgIHByZXZPdmVyZmxvdyA9IHVuZGVmaW5lZDtcbiAgICAgIHByZXZQYWRkaW5ncyA9IFtdO1xuICAgICAgaWYgKGhpZGVTaWJsaW5nTm9kZXMpIHtcbiAgICAgICAgKDAsIF9tYW5hZ2VBcmlhSGlkZGVuLnNob3dTaWJsaW5ncykoY29udGFpbmVyLCBtb2RhbC5tb3VudE5vZGUpO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoaGlkZVNpYmxpbmdOb2Rlcykge1xuICAgICAgLy8gb3RoZXJ3aXNlIG1ha2Ugc3VyZSB0aGUgbmV4dCB0b3AgbW9kYWwgaXMgdmlzaWJsZSB0byBhIFNSXG4gICAgICAoMCwgX21hbmFnZUFyaWFIaWRkZW4uYXJpYUhpZGRlbikoZmFsc2UsIG1vZGFsc1ttb2RhbHMubGVuZ3RoIC0gMV0ubW91bnROb2RlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbW9kYWxJZHg7XG4gIH1cblxuICBmdW5jdGlvbiBpc1RvcE1vZGFsKG1vZGFsKSB7XG4gICAgcmV0dXJuICEhbW9kYWxzLmxlbmd0aCAmJiBtb2RhbHNbbW9kYWxzLmxlbmd0aCAtIDFdID09PSBtb2RhbDtcbiAgfVxuXG4gIHZhciBtb2RhbE1hbmFnZXIgPSB7IGFkZDogYWRkLCByZW1vdmU6IHJlbW92ZSwgaXNUb3BNb2RhbDogaXNUb3BNb2RhbCB9O1xuXG4gIHJldHVybiBtb2RhbE1hbmFnZXI7XG59XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZU1vZGFsTWFuYWdlcjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL21vZGFsTWFuYWdlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9yZWFjdERvbSA9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG52YXIgX3JlYWN0RG9tMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0RG9tKTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfY29udGFpbnMgPSByZXF1aXJlKCdkb20taGVscGVycy9xdWVyeS9jb250YWlucycpO1xuXG52YXIgX2NvbnRhaW5zMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NvbnRhaW5zKTtcblxudmFyIF9kZWJvdW5jZSA9IHJlcXVpcmUoJ2xvZGFzaC9kZWJvdW5jZScpO1xuXG52YXIgX2RlYm91bmNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlYm91bmNlKTtcblxudmFyIF9yZWFjdEV2ZW50TGlzdGVuZXIgPSByZXF1aXJlKCdyZWFjdC1ldmVudC1saXN0ZW5lcicpO1xuXG52YXIgX3JlYWN0RXZlbnRMaXN0ZW5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdEV2ZW50TGlzdGVuZXIpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfTW9kYWwgPSByZXF1aXJlKCcuLi9Nb2RhbCcpO1xuXG52YXIgX01vZGFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX01vZGFsKTtcblxudmFyIF9Hcm93ID0gcmVxdWlyZSgnLi4vdHJhbnNpdGlvbnMvR3JvdycpO1xuXG52YXIgX0dyb3cyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfR3Jvdyk7XG5cbnZhciBfUGFwZXIgPSByZXF1aXJlKCcuLi9QYXBlcicpO1xuXG52YXIgX1BhcGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BhcGVyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgTW9kYWxcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbmZ1bmN0aW9uIGdldE9mZnNldFRvcChyZWN0LCB2ZXJ0aWNhbCkge1xuICB2YXIgb2Zmc2V0ID0gMDtcblxuICBpZiAodHlwZW9mIHZlcnRpY2FsID09PSAnbnVtYmVyJykge1xuICAgIG9mZnNldCA9IHZlcnRpY2FsO1xuICB9IGVsc2UgaWYgKHZlcnRpY2FsID09PSAnY2VudGVyJykge1xuICAgIG9mZnNldCA9IHJlY3QuaGVpZ2h0IC8gMjtcbiAgfSBlbHNlIGlmICh2ZXJ0aWNhbCA9PT0gJ2JvdHRvbScpIHtcbiAgICBvZmZzZXQgPSByZWN0LmhlaWdodDtcbiAgfVxuXG4gIHJldHVybiBvZmZzZXQ7XG59XG5cbmZ1bmN0aW9uIGdldE9mZnNldExlZnQocmVjdCwgaG9yaXpvbnRhbCkge1xuICB2YXIgb2Zmc2V0ID0gMDtcblxuICBpZiAodHlwZW9mIGhvcml6b250YWwgPT09ICdudW1iZXInKSB7XG4gICAgb2Zmc2V0ID0gaG9yaXpvbnRhbDtcbiAgfSBlbHNlIGlmIChob3Jpem9udGFsID09PSAnY2VudGVyJykge1xuICAgIG9mZnNldCA9IHJlY3Qud2lkdGggLyAyO1xuICB9IGVsc2UgaWYgKGhvcml6b250YWwgPT09ICdyaWdodCcpIHtcbiAgICBvZmZzZXQgPSByZWN0LndpZHRoO1xuICB9XG5cbiAgcmV0dXJuIG9mZnNldDtcbn1cblxuZnVuY3Rpb24gZ2V0VHJhbnNmb3JtT3JpZ2luVmFsdWUodHJhbnNmb3JtT3JpZ2luKSB7XG4gIHJldHVybiBbdHJhbnNmb3JtT3JpZ2luLmhvcml6b250YWwsIHRyYW5zZm9ybU9yaWdpbi52ZXJ0aWNhbF0ubWFwKGZ1bmN0aW9uIChuKSB7XG4gICAgcmV0dXJuIHR5cGVvZiBuID09PSAnbnVtYmVyJyA/IG4gKyAncHgnIDogbjtcbiAgfSkuam9pbignICcpO1xufVxuXG4vLyBTdW0gdGhlIHNjcm9sbFRvcCBiZXR3ZWVuIHR3byBlbGVtZW50cy5cbmZ1bmN0aW9uIGdldFNjcm9sbFBhcmVudChwYXJlbnQsIGNoaWxkKSB7XG4gIHZhciBlbGVtZW50ID0gY2hpbGQ7XG4gIHZhciBzY3JvbGxUb3AgPSAwO1xuXG4gIHdoaWxlIChlbGVtZW50ICYmIGVsZW1lbnQgIT09IHBhcmVudCkge1xuICAgIGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudE5vZGU7XG4gICAgc2Nyb2xsVG9wICs9IGVsZW1lbnQuc2Nyb2xsVG9wO1xuICB9XG4gIHJldHVybiBzY3JvbGxUb3A7XG59XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IHtcbiAgcGFwZXI6IHtcbiAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgICBvdmVyZmxvd1k6ICdhdXRvJyxcbiAgICBvdmVyZmxvd1g6ICdoaWRkZW4nLFxuICAgIC8vIFNvIHdlIHNlZSB0aGUgcG9wb3ZlciB3aGVuIGl0J3MgZW1wdHkuXG4gICAgLy8gSXQncyBtb3N0IGxpa2VseSBvbiBpc3N1ZSBvbiB1c2VybGFuZC5cbiAgICBtaW5XaWR0aDogMTYsXG4gICAgbWluSGVpZ2h0OiAxNixcbiAgICBtYXhXaWR0aDogJ2NhbGMoMTAwdncgLSAzMnB4KScsXG4gICAgbWF4SGVpZ2h0OiAnY2FsYygxMDB2aCAtIDMycHgpJyxcbiAgICAnJjpmb2N1cyc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJ1xuICAgIH1cbiAgfVxufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX09yaWdpbiA9IHtcbiAgaG9yaXpvbnRhbDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnbGVmdCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnY2VudGVyJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydyaWdodCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcl0pLmlzUmVxdWlyZWQsXG4gIHZlcnRpY2FsOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWyd0b3AnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2NlbnRlciddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYm90dG9tJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXSkuaXNSZXF1aXJlZFxufTtcbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qb3NpdGlvbiA9IHtcbiAgdG9wOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG4gIGxlZnQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZFxufTtcbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoaXMgaXMgdGhlIERPTSBlbGVtZW50IHRoYXQgbWF5IGJlIHVzZWRcbiAgICogdG8gc2V0IHRoZSBwb3NpdGlvbiBvZiB0aGUgcG9wb3Zlci5cbiAgICovXG4gIGFuY2hvckVsOiB0eXBlb2YgSFRNTEVsZW1lbnQgPT09ICdmdW5jdGlvbicgPyByZXF1aXJlKCdwcm9wLXR5cGVzJykuaW5zdGFuY2VPZihIVE1MRWxlbWVudCkgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55LFxuXG4gIC8qKlxuICAgKiBUaGlzIGlzIHRoZSBwb3NpdGlvbiB0aGF0IG1heSBiZSB1c2VkXG4gICAqIHRvIHNldCB0aGUgcG9zaXRpb24gb2YgdGhlIHBvcG92ZXIuXG4gICAqIFRoZSBjb29yZGluYXRlcyBhcmUgcmVsYXRpdmUgdG9cbiAgICogdGhlIGFwcGxpY2F0aW9uJ3MgY2xpZW50IGFyZWEuXG4gICAqL1xuICBhbmNob3JQb3NpdGlvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKHtcbiAgICB0b3A6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgICBsZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWRcbiAgfSksXG5cbiAgLypcbiAgICogVGhpcyBkZXRlcm1pbmVzIHdoaWNoIGFuY2hvciBwcm9wIHRvIHJlZmVyIHRvIHRvIHNldFxuICAgKiB0aGUgcG9zaXRpb24gb2YgdGhlIHBvcG92ZXIuXG4gICAqL1xuICBhbmNob3JSZWZlcmVuY2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2FuY2hvckVsJywgJ2FuY2hvclBvc2l0aW9uJ10pLFxuXG4gIC8qKlxuICAgKiBUaGlzIGlzIHRoZSBwb2ludCBvbiB0aGUgYW5jaG9yIHdoZXJlIHRoZSBwb3BvdmVyJ3NcbiAgICogYGFuY2hvckVsYCB3aWxsIGF0dGFjaCB0by4gVGhpcyBpcyBub3QgdXNlZCB3aGVuIHRoZVxuICAgKiBhbmNob3JSZWZlcmVuY2UgaXMgJ2FuY2hvclBvc2l0aW9uJy5cbiAgICpcbiAgICogT3B0aW9uczpcbiAgICogdmVydGljYWw6IFt0b3AsIGNlbnRlciwgYm90dG9tXTtcbiAgICogaG9yaXpvbnRhbDogW2xlZnQsIGNlbnRlciwgcmlnaHRdLlxuICAgKi9cbiAgYW5jaG9yT3JpZ2luOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoe1xuICAgIGhvcml6b250YWw6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2xlZnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2NlbnRlciddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncmlnaHQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICAgIHZlcnRpY2FsOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWyd0b3AnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2NlbnRlciddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYm90dG9tJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgZWxldmF0aW9uIG9mIHRoZSBwb3BvdmVyLlxuICAgKi9cbiAgZWxldmF0aW9uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIGlzIGNhbGxlZCBpbiBvcmRlciB0byByZXRyaWV2ZSB0aGUgY29udGVudCBhbmNob3IgZWxlbWVudC5cbiAgICogSXQncyB0aGUgb3Bwb3NpdGUgb2YgdGhlIGBhbmNob3JFbGAgcHJvcGVydHkuXG4gICAqIFRoZSBjb250ZW50IGFuY2hvciBlbGVtZW50IHNob3VsZCBiZSBhbiBlbGVtZW50IGluc2lkZSB0aGUgcG9wb3Zlci5cbiAgICogSXQncyB1c2VkIHRvIGNvcnJlY3RseSBzY3JvbGwgYW5kIHNldCB0aGUgcG9zaXRpb24gb2YgdGhlIHBvcG92ZXIuXG4gICAqIFRoZSBwb3NpdGlvbmluZyBzdHJhdGVneSB0cmllcyB0byBtYWtlIHRoZSBjb250ZW50IGFuY2hvciBlbGVtZW50IGp1c3QgYWJvdmUgdGhlXG4gICAqIGFuY2hvciBlbGVtZW50LlxuICAgKi9cbiAgZ2V0Q29udGVudEFuY2hvckVsOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogU3BlY2lmaWVzIGhvdyBjbG9zZSB0byB0aGUgZWRnZSBvZiB0aGUgd2luZG93IHRoZSBwb3BvdmVyIGNhbiBhcHBlYXIuXG4gICAqL1xuICBtYXJnaW5UaHJlc2hvbGQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBjb21wb25lbnQgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgY29tcG9uZW50IGhhcyBlbnRlcmVkLlxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIGJlZm9yZSB0aGUgY29tcG9uZW50IGlzIGV4aXRpbmcuXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgY29tcG9uZW50IGlzIGV4aXRpbmcuXG4gICAqL1xuICBvbkV4aXRpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgY29tcG9uZW50IGhhcyBleGl0ZWQuXG4gICAqL1xuICBvbkV4aXRlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBjb21wb25lbnQgcmVxdWVzdHMgdG8gYmUgY2xvc2VkLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2suXG4gICAqL1xuICBvblJlcXVlc3RDbG9zZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHBvcG92ZXIgaXMgdmlzaWJsZS5cbiAgICovXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFByb3BlcnRpZXMgYXBwbGllZCB0byB0aGUgYFBhcGVyYCBlbGVtZW50LlxuICAgKi9cbiAgUGFwZXJQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgcm9sZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhpcyBpcyB0aGUgcG9pbnQgb24gdGhlIHBvcG92ZXIgd2hpY2hcbiAgICogd2lsbCBhdHRhY2ggdG8gdGhlIGFuY2hvcidzIG9yaWdpbi5cbiAgICpcbiAgICogT3B0aW9uczpcbiAgICogdmVydGljYWw6IFt0b3AsIGNlbnRlciwgYm90dG9tLCB4KHB4KV07XG4gICAqIGhvcml6b250YWw6IFtsZWZ0LCBjZW50ZXIsIHJpZ2h0LCB4KHB4KV0uXG4gICAqL1xuICB0cmFuc2Zvcm1PcmlnaW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgaG9yaXpvbnRhbDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnbGVmdCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnY2VudGVyJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydyaWdodCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcl0pLmlzUmVxdWlyZWQsXG4gICAgdmVydGljYWw6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3RvcCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnY2VudGVyJ10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydib3R0b20nXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJdKS5pc1JlcXVpcmVkXG4gIH0pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBhbmltYXRpb24gY2xhc3NOYW1lcyBhcHBsaWVkIHRvIHRoZSBjb21wb25lbnQgYXMgaXQgZW50ZXJzIG9yIGV4aXRzLlxuICAgKiBUaGlzIHByb3BlcnR5IGlzIGEgZGlyZWN0IGJpbmRpbmcgdG8gW2BDU1NUcmFuc2l0aW9uLmNsYXNzTmFtZXNgXShodHRwczovL3JlYWN0Y29tbXVuaXR5Lm9yZy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwLyNDU1NUcmFuc2l0aW9uLXByb3AtY2xhc3NOYW1lcykuXG4gICAqL1xuICB0cmFuc2l0aW9uQ2xhc3NlczogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNsYXNzZXMgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNsYXNzZXMpLFxuXG4gIC8qKlxuICAgKiBTZXQgdG8gJ2F1dG8nIHRvIGF1dG9tYXRpY2FsbHkgY2FsY3VsYXRlIHRyYW5zaXRpb24gdGltZSBiYXNlZCBvbiBoZWlnaHQuXG4gICAqL1xuICB0cmFuc2l0aW9uRHVyYXRpb246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgZW50ZXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gICAgZXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxuICB9KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pXG59O1xuXG52YXIgUG9wb3ZlciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFBvcG92ZXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFBvcG92ZXIoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgUG9wb3Zlcik7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBQb3BvdmVyLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShQb3BvdmVyKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuY29tcG9uZW50V2lsbFVubW91bnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy5oYW5kbGVSZXNpemUuY2FuY2VsKCk7XG4gICAgfSwgX3RoaXMuc2V0UG9zaXRpb25pbmdTdHlsZXMgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgaWYgKGVsZW1lbnQgJiYgZWxlbWVudC5zdHlsZSkge1xuICAgICAgICB2YXIgcG9zaXRpb25pbmcgPSBfdGhpcy5nZXRQb3NpdGlvbmluZ1N0eWxlKGVsZW1lbnQpO1xuXG4gICAgICAgIGVsZW1lbnQuc3R5bGUudG9wID0gcG9zaXRpb25pbmcudG9wO1xuICAgICAgICBlbGVtZW50LnN0eWxlLmxlZnQgPSBwb3NpdGlvbmluZy5sZWZ0O1xuICAgICAgICBlbGVtZW50LnN0eWxlLnRyYW5zZm9ybU9yaWdpbiA9IHBvc2l0aW9uaW5nLnRyYW5zZm9ybU9yaWdpbjtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5nZXRQb3NpdGlvbmluZ1N0eWxlID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgIHZhciBtYXJnaW5UaHJlc2hvbGQgPSBfdGhpcy5wcm9wcy5tYXJnaW5UaHJlc2hvbGQ7XG5cbiAgICAgIC8vIENoZWNrIGlmIHRoZSBwYXJlbnQgaGFzIHJlcXVlc3RlZCBhbmNob3Jpbmcgb24gYW4gaW5uZXIgY29udGVudCBub2RlXG5cbiAgICAgIHZhciBjb250ZW50QW5jaG9yT2Zmc2V0ID0gX3RoaXMuZ2V0Q29udGVudEFuY2hvck9mZnNldChlbGVtZW50KTtcbiAgICAgIC8vIEdldCB0aGUgb2Zmc2V0IG9mIG9mIHRoZSBhbmNob3JpbmcgZWxlbWVudFxuICAgICAgdmFyIGFuY2hvck9mZnNldCA9IF90aGlzLmdldEFuY2hvck9mZnNldChjb250ZW50QW5jaG9yT2Zmc2V0KTtcblxuICAgICAgdmFyIGVsZW1SZWN0ID0ge1xuICAgICAgICB3aWR0aDogZWxlbWVudC5jbGllbnRXaWR0aCxcbiAgICAgICAgaGVpZ2h0OiBlbGVtZW50LmNsaWVudEhlaWdodFxuICAgICAgfTtcbiAgICAgIC8vIEdldCB0aGUgdHJhbnNmb3JtIG9yaWdpbiBwb2ludCBvbiB0aGUgZWxlbWVudCBpdHNlbGZcbiAgICAgIHZhciB0cmFuc2Zvcm1PcmlnaW4gPSBfdGhpcy5nZXRUcmFuc2Zvcm1PcmlnaW4oZWxlbVJlY3QsIGNvbnRlbnRBbmNob3JPZmZzZXQpO1xuXG4gICAgICAvLyBDYWxjdWxhdGUgZWxlbWVudCBwb3NpdGlvbmluZ1xuICAgICAgdmFyIHRvcCA9IGFuY2hvck9mZnNldC50b3AgLSB0cmFuc2Zvcm1PcmlnaW4udmVydGljYWw7XG4gICAgICB2YXIgbGVmdCA9IGFuY2hvck9mZnNldC5sZWZ0IC0gdHJhbnNmb3JtT3JpZ2luLmhvcml6b250YWw7XG4gICAgICB2YXIgYm90dG9tID0gdG9wICsgZWxlbVJlY3QuaGVpZ2h0O1xuICAgICAgdmFyIHJpZ2h0ID0gbGVmdCArIGVsZW1SZWN0LndpZHRoO1xuXG4gICAgICAvLyBXaW5kb3cgdGhyZXNob2xkcyB0YWtpbmcgcmVxdWlyZWQgbWFyZ2luIGludG8gYWNjb3VudFxuICAgICAgdmFyIGhlaWdodFRocmVzaG9sZCA9IHdpbmRvdy5pbm5lckhlaWdodCAtIG1hcmdpblRocmVzaG9sZDtcbiAgICAgIHZhciB3aWR0aFRocmVzaG9sZCA9IHdpbmRvdy5pbm5lcldpZHRoIC0gbWFyZ2luVGhyZXNob2xkO1xuXG4gICAgICAvLyBDaGVjayBpZiB0aGUgdmVydGljYWwgYXhpcyBuZWVkcyBzaGlmdGluZ1xuICAgICAgaWYgKHRvcCA8IG1hcmdpblRocmVzaG9sZCkge1xuICAgICAgICB2YXIgZGlmZiA9IHRvcCAtIG1hcmdpblRocmVzaG9sZDtcbiAgICAgICAgdG9wIC09IGRpZmY7XG4gICAgICAgIHRyYW5zZm9ybU9yaWdpbi52ZXJ0aWNhbCArPSBkaWZmO1xuICAgICAgfSBlbHNlIGlmIChib3R0b20gPiBoZWlnaHRUaHJlc2hvbGQpIHtcbiAgICAgICAgdmFyIF9kaWZmID0gYm90dG9tIC0gaGVpZ2h0VGhyZXNob2xkO1xuICAgICAgICB0b3AgLT0gX2RpZmY7XG4gICAgICAgIHRyYW5zZm9ybU9yaWdpbi52ZXJ0aWNhbCArPSBfZGlmZjtcbiAgICAgIH1cblxuICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KShlbGVtUmVjdC5oZWlnaHQgPCBoZWlnaHRUaHJlc2hvbGQgfHwgIWVsZW1SZWN0LmhlaWdodCB8fCAhaGVpZ2h0VGhyZXNob2xkLCBbJ01hdGVyaWFsLVVJOiB0aGUgcG9wb3ZlciBjb21wb25lbnQgaXMgdG9vIHRhbGwuJywgJ1NvbWUgcGFydCBvZiBpdCBjYW4gbm90IGJlIHNlZW4gb24gdGhlIHNjcmVlbiAoJyArIChlbGVtUmVjdC5oZWlnaHQgLSBoZWlnaHRUaHJlc2hvbGQpICsgJ3B4KS4nLCAnUGxlYXNlIGNvbnNpZGVyIGFkZGluZyBhIGBtYXgtaGVpZ2h0YCB0byBpbXByb3ZlIHRoZSB1c2VyLWV4cGVyaWVuY2UuJ10uam9pbignXFxuJykpIDogdm9pZCAwO1xuXG4gICAgICAvLyBDaGVjayBpZiB0aGUgaG9yaXpvbnRhbCBheGlzIG5lZWRzIHNoaWZ0aW5nXG4gICAgICBpZiAobGVmdCA8IG1hcmdpblRocmVzaG9sZCkge1xuICAgICAgICB2YXIgX2RpZmYyID0gbGVmdCAtIG1hcmdpblRocmVzaG9sZDtcbiAgICAgICAgbGVmdCAtPSBfZGlmZjI7XG4gICAgICAgIHRyYW5zZm9ybU9yaWdpbi5ob3Jpem9udGFsICs9IF9kaWZmMjtcbiAgICAgIH0gZWxzZSBpZiAocmlnaHQgPiB3aWR0aFRocmVzaG9sZCkge1xuICAgICAgICB2YXIgX2RpZmYzID0gcmlnaHQgLSB3aWR0aFRocmVzaG9sZDtcbiAgICAgICAgbGVmdCAtPSBfZGlmZjM7XG4gICAgICAgIHRyYW5zZm9ybU9yaWdpbi5ob3Jpem9udGFsICs9IF9kaWZmMztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdG9wOiB0b3AgKyAncHgnLFxuICAgICAgICBsZWZ0OiBsZWZ0ICsgJ3B4JyxcbiAgICAgICAgdHJhbnNmb3JtT3JpZ2luOiBnZXRUcmFuc2Zvcm1PcmlnaW5WYWx1ZSh0cmFuc2Zvcm1PcmlnaW4pXG4gICAgICB9O1xuICAgIH0sIF90aGlzLnRyYW5zaXRpb25FbCA9IHVuZGVmaW5lZCwgX3RoaXMuaGFuZGxlR2V0T2Zmc2V0VG9wID0gZ2V0T2Zmc2V0VG9wLCBfdGhpcy5oYW5kbGVHZXRPZmZzZXRMZWZ0ID0gZ2V0T2Zmc2V0TGVmdCwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXIpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcihlbGVtZW50KTtcbiAgICAgIH1cblxuICAgICAgX3RoaXMuc2V0UG9zaXRpb25pbmdTdHlsZXMoZWxlbWVudCk7XG4gICAgfSwgX3RoaXMuaGFuZGxlUmVzaXplID0gKDAsIF9kZWJvdW5jZTIuZGVmYXVsdCkoZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGVsZW1lbnQgPSBfcmVhY3REb20yLmRlZmF1bHQuZmluZERPTU5vZGUoX3RoaXMudHJhbnNpdGlvbkVsKTtcbiAgICAgIF90aGlzLnNldFBvc2l0aW9uaW5nU3R5bGVzKGVsZW1lbnQpO1xuICAgIH0sIDE2NiksIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShQb3BvdmVyLCBbe1xuICAgIGtleTogJ2dldEFuY2hvck9mZnNldCcsXG5cblxuICAgIC8vIFJldHVybnMgdGhlIHRvcC9sZWZ0IG9mZnNldCBvZiB0aGUgcG9zaXRpb25cbiAgICAvLyB0byBhdHRhY2ggdG8gb24gdGhlIGFuY2hvciBlbGVtZW50IChvciBib2R5IGlmIG5vbmUgaXMgcHJvdmlkZWQpXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGdldEFuY2hvck9mZnNldChjb250ZW50QW5jaG9yT2Zmc2V0KSB7XG4gICAgICAvLyAkRmxvd0ZpeE1lXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhbmNob3JFbCA9IF9wcm9wcy5hbmNob3JFbCxcbiAgICAgICAgICBhbmNob3JPcmlnaW4gPSBfcHJvcHMuYW5jaG9yT3JpZ2luLFxuICAgICAgICAgIGFuY2hvclJlZmVyZW5jZSA9IF9wcm9wcy5hbmNob3JSZWZlcmVuY2UsXG4gICAgICAgICAgYW5jaG9yUG9zaXRpb24gPSBfcHJvcHMuYW5jaG9yUG9zaXRpb247XG5cblxuICAgICAgaWYgKGFuY2hvclJlZmVyZW5jZSA9PT0gJ2FuY2hvclBvc2l0aW9uJykge1xuICAgICAgICByZXR1cm4gYW5jaG9yUG9zaXRpb247XG4gICAgICB9XG5cbiAgICAgIHZhciBhbmNob3JFbGVtZW50ID0gYW5jaG9yRWwgfHwgZG9jdW1lbnQuYm9keTtcbiAgICAgIHZhciBhbmNob3JSZWN0ID0gYW5jaG9yRWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgIHZhciBhbmNob3JWZXJ0aWNhbCA9IGNvbnRlbnRBbmNob3JPZmZzZXQgPT09IDAgPyBhbmNob3JPcmlnaW4udmVydGljYWwgOiAnY2VudGVyJztcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgdG9wOiBhbmNob3JSZWN0LnRvcCArIHRoaXMuaGFuZGxlR2V0T2Zmc2V0VG9wKGFuY2hvclJlY3QsIGFuY2hvclZlcnRpY2FsKSxcbiAgICAgICAgbGVmdDogYW5jaG9yUmVjdC5sZWZ0ICsgdGhpcy5oYW5kbGVHZXRPZmZzZXRMZWZ0KGFuY2hvclJlY3QsIGFuY2hvck9yaWdpbi5ob3Jpem9udGFsKVxuICAgICAgfTtcbiAgICB9XG5cbiAgICAvLyBSZXR1cm5zIHRoZSB2ZXJ0aWNhbCBvZmZzZXQgb2YgaW5uZXIgY29udGVudCB0byBhbmNob3IgdGhlIHRyYW5zZm9ybSBvbiBpZiBwcm92aWRlZFxuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRDb250ZW50QW5jaG9yT2Zmc2V0JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0Q29udGVudEFuY2hvck9mZnNldChlbGVtZW50KSB7XG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgZ2V0Q29udGVudEFuY2hvckVsID0gX3Byb3BzMi5nZXRDb250ZW50QW5jaG9yRWwsXG4gICAgICAgICAgYW5jaG9yUmVmZXJlbmNlID0gX3Byb3BzMi5hbmNob3JSZWZlcmVuY2U7XG5cbiAgICAgIHZhciBjb250ZW50QW5jaG9yT2Zmc2V0ID0gMDtcblxuICAgICAgaWYgKGdldENvbnRlbnRBbmNob3JFbCAmJiBhbmNob3JSZWZlcmVuY2UgPT09ICdhbmNob3JFbCcpIHtcbiAgICAgICAgdmFyIGNvbnRlbnRBbmNob3JFbCA9IGdldENvbnRlbnRBbmNob3JFbChlbGVtZW50KTtcblxuICAgICAgICBpZiAoY29udGVudEFuY2hvckVsICYmICgwLCBfY29udGFpbnMyLmRlZmF1bHQpKGVsZW1lbnQsIGNvbnRlbnRBbmNob3JFbCkpIHtcbiAgICAgICAgICB2YXIgc2Nyb2xsVG9wID0gZ2V0U2Nyb2xsUGFyZW50KGVsZW1lbnQsIGNvbnRlbnRBbmNob3JFbCk7XG4gICAgICAgICAgY29udGVudEFuY2hvck9mZnNldCA9IGNvbnRlbnRBbmNob3JFbC5vZmZzZXRUb3AgKyBjb250ZW50QW5jaG9yRWwuY2xpZW50SGVpZ2h0IC8gMiAtIHNjcm9sbFRvcCB8fCAwO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gIT0gdGhlIGRlZmF1bHQgdmFsdWVcbiAgICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KSh0aGlzLnByb3BzLmFuY2hvck9yaWdpbi52ZXJ0aWNhbCA9PT0gJ3RvcCcsIFsnTWF0ZXJpYWwtVUk6IHlvdSBjYW4gbm90IGNoYW5nZSB0aGUgZGVmYXVsdCBgYW5jaG9yT3JpZ2luLnZlcnRpY2FsYCB2YWx1ZSB3aGVuIGFsc28gJywgJ3Byb3ZpZGluZyB0aGUgYGdldENvbnRlbnRBbmNob3JFbGAgcHJvcGVydHkgdG8gdGhlIHBvcG92ZXIgY29tcG9uZW50LicsICdPbmx5IHVzZSBvbmUgb2YgdGhlIHR3byBwcm9wZXJ0aWVzJywgJ1NldCBgZ2V0Q29udGVudEFuY2hvckVsYCB0byBudWxsIG9yIGxlZnQgYGFuY2hvck9yaWdpbi52ZXJ0aWNhbGAgdW5jaGFuZ2VkJ10uam9pbigpKSA6IHZvaWQgMDtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGNvbnRlbnRBbmNob3JPZmZzZXQ7XG4gICAgfVxuXG4gICAgLy8gUmV0dXJuIHRoZSBiYXNlIHRyYW5zZm9ybSBvcmlnaW4gdXNpbmcgdGhlIGVsZW1lbnRcbiAgICAvLyBhbmQgdGFraW5nIHRoZSBjb250ZW50IGFuY2hvciBvZmZzZXQgaW50byBhY2NvdW50IGlmIGluIHVzZVxuXG4gIH0sIHtcbiAgICBrZXk6ICdnZXRUcmFuc2Zvcm1PcmlnaW4nLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRUcmFuc2Zvcm1PcmlnaW4oZWxlbVJlY3QpIHtcbiAgICAgIHZhciBjb250ZW50QW5jaG9yT2Zmc2V0ID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiAwO1xuICAgICAgdmFyIHRyYW5zZm9ybU9yaWdpbiA9IHRoaXMucHJvcHMudHJhbnNmb3JtT3JpZ2luO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICB2ZXJ0aWNhbDogdGhpcy5oYW5kbGVHZXRPZmZzZXRUb3AoZWxlbVJlY3QsIHRyYW5zZm9ybU9yaWdpbi52ZXJ0aWNhbCkgKyBjb250ZW50QW5jaG9yT2Zmc2V0LFxuICAgICAgICBob3Jpem9udGFsOiB0aGlzLmhhbmRsZUdldE9mZnNldExlZnQoZWxlbVJlY3QsIHRyYW5zZm9ybU9yaWdpbi5ob3Jpem9udGFsKVxuICAgICAgfTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgdmFyIF9wcm9wczMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFuY2hvckVsID0gX3Byb3BzMy5hbmNob3JFbCxcbiAgICAgICAgICBhbmNob3JSZWZlcmVuY2UgPSBfcHJvcHMzLmFuY2hvclJlZmVyZW5jZSxcbiAgICAgICAgICBhbmNob3JQb3NpdGlvbiA9IF9wcm9wczMuYW5jaG9yUG9zaXRpb24sXG4gICAgICAgICAgYW5jaG9yT3JpZ2luID0gX3Byb3BzMy5hbmNob3JPcmlnaW4sXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMzLmNsYXNzZXMsXG4gICAgICAgICAgZWxldmF0aW9uID0gX3Byb3BzMy5lbGV2YXRpb24sXG4gICAgICAgICAgZ2V0Q29udGVudEFuY2hvckVsID0gX3Byb3BzMy5nZXRDb250ZW50QW5jaG9yRWwsXG4gICAgICAgICAgbWFyZ2luVGhyZXNob2xkID0gX3Byb3BzMy5tYXJnaW5UaHJlc2hvbGQsXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wczMub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzMy5vbkVudGVyaW5nLFxuICAgICAgICAgIG9uRW50ZXJlZCA9IF9wcm9wczMub25FbnRlcmVkLFxuICAgICAgICAgIG9uRXhpdCA9IF9wcm9wczMub25FeGl0LFxuICAgICAgICAgIG9uRXhpdGluZyA9IF9wcm9wczMub25FeGl0aW5nLFxuICAgICAgICAgIG9uRXhpdGVkID0gX3Byb3BzMy5vbkV4aXRlZCxcbiAgICAgICAgICBvcGVuID0gX3Byb3BzMy5vcGVuLFxuICAgICAgICAgIFBhcGVyUHJvcHMgPSBfcHJvcHMzLlBhcGVyUHJvcHMsXG4gICAgICAgICAgcm9sZSA9IF9wcm9wczMucm9sZSxcbiAgICAgICAgICB0cmFuc2Zvcm1PcmlnaW4gPSBfcHJvcHMzLnRyYW5zZm9ybU9yaWdpbixcbiAgICAgICAgICB0cmFuc2l0aW9uQ2xhc3NlcyA9IF9wcm9wczMudHJhbnNpdGlvbkNsYXNzZXMsXG4gICAgICAgICAgdHJhbnNpdGlvbkR1cmF0aW9uID0gX3Byb3BzMy50cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMzLCBbJ2FuY2hvckVsJywgJ2FuY2hvclJlZmVyZW5jZScsICdhbmNob3JQb3NpdGlvbicsICdhbmNob3JPcmlnaW4nLCAnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdlbGV2YXRpb24nLCAnZ2V0Q29udGVudEFuY2hvckVsJywgJ21hcmdpblRocmVzaG9sZCcsICdvbkVudGVyJywgJ29uRW50ZXJpbmcnLCAnb25FbnRlcmVkJywgJ29uRXhpdCcsICdvbkV4aXRpbmcnLCAnb25FeGl0ZWQnLCAnb3BlbicsICdQYXBlclByb3BzJywgJ3JvbGUnLCAndHJhbnNmb3JtT3JpZ2luJywgJ3RyYW5zaXRpb25DbGFzc2VzJywgJ3RyYW5zaXRpb25EdXJhdGlvbiddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9Nb2RhbDIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IHNob3c6IG9wZW4sIEJhY2tkcm9wSW52aXNpYmxlOiB0cnVlIH0sIG90aGVyKSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgX0dyb3cyLmRlZmF1bHQsXG4gICAgICAgICAge1xuICAgICAgICAgICAgYXBwZWFyOiB0cnVlLFxuICAgICAgICAgICAgJ2luJzogb3BlbixcbiAgICAgICAgICAgIG9uRW50ZXI6IHRoaXMuaGFuZGxlRW50ZXIsXG4gICAgICAgICAgICBvbkVudGVyaW5nOiBvbkVudGVyaW5nLFxuICAgICAgICAgICAgb25FbnRlcmVkOiBvbkVudGVyZWQsXG4gICAgICAgICAgICBvbkV4aXQ6IG9uRXhpdCxcbiAgICAgICAgICAgIG9uRXhpdGluZzogb25FeGl0aW5nLFxuICAgICAgICAgICAgb25FeGl0ZWQ6IG9uRXhpdGVkLFxuICAgICAgICAgICAgcm9sZTogcm9sZSxcbiAgICAgICAgICAgIHRyYW5zaXRpb25DbGFzc2VzOiB0cmFuc2l0aW9uQ2xhc3NlcyxcbiAgICAgICAgICAgIHRpbWVvdXQ6IHRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgICAgIHJvb3RSZWY6IGZ1bmN0aW9uIHJvb3RSZWYobm9kZSkge1xuICAgICAgICAgICAgICBfdGhpczIudHJhbnNpdGlvbkVsID0gbm9kZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9LFxuICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgX1BhcGVyMi5kZWZhdWx0LFxuICAgICAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3Nlcy5wYXBlcixcbiAgICAgICAgICAgICAgZWxldmF0aW9uOiBlbGV2YXRpb25cbiAgICAgICAgICAgIH0sIFBhcGVyUHJvcHMpLFxuICAgICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX3JlYWN0RXZlbnRMaXN0ZW5lcjIuZGVmYXVsdCwgeyB0YXJnZXQ6ICd3aW5kb3cnLCBvblJlc2l6ZTogdGhpcy5oYW5kbGVSZXNpemUgfSksXG4gICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgIClcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFBvcG92ZXI7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Qb3BvdmVyLmRlZmF1bHRQcm9wcyA9IHtcbiAgYW5jaG9yUmVmZXJlbmNlOiAnYW5jaG9yRWwnLFxuICBhbmNob3JPcmlnaW46IHtcbiAgICB2ZXJ0aWNhbDogJ3RvcCcsXG4gICAgaG9yaXpvbnRhbDogJ2xlZnQnXG4gIH0sXG4gIHRyYW5zZm9ybU9yaWdpbjoge1xuICAgIHZlcnRpY2FsOiAndG9wJyxcbiAgICBob3Jpem9udGFsOiAnbGVmdCdcbiAgfSxcbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiAnYXV0bycsXG4gIGVsZXZhdGlvbjogOCxcbiAgbWFyZ2luVGhyZXNob2xkOiAxNlxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlQb3BvdmVyJyB9KShQb3BvdmVyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Qb3BvdmVyL1BvcG92ZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1BvcG92ZXIvUG9wb3Zlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1BvcG92ZXIgPSByZXF1aXJlKCcuL1BvcG92ZXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUG9wb3ZlcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvUG9wb3Zlci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvUG9wb3Zlci9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWJsb2NrJyxcbiAgICAgIGZpbGw6ICdjdXJyZW50Q29sb3InLFxuICAgICAgaGVpZ2h0OiAyNCxcbiAgICAgIHdpZHRoOiAyNCxcbiAgICAgIHVzZXJTZWxlY3Q6ICdub25lJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2ZpbGwnLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVyXG4gICAgICB9KVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckFjdGlvbjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZVxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JEaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBjb2xvckVycm9yOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5lcnJvcls1MDBdXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db2xvciA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBFbGVtZW50cyBwYXNzZWQgaW50byB0aGUgU1ZHIEljb24uXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFByb3ZpZGVzIGEgaHVtYW4tcmVhZGFibGUgdGl0bGUgZm9yIHRoZSBlbGVtZW50IHRoYXQgY29udGFpbnMgaXQuXG4gICAqIGh0dHBzOi8vd3d3LnczLm9yZy9UUi9TVkctYWNjZXNzLyNFcXVpdmFsZW50XG4gICAqL1xuICB0aXRsZUFjY2VzczogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQWxsb3dzIHlvdSB0byByZWRlZmluZSB3aGF0IHRoZSBjb29yZGluYXRlcyB3aXRob3V0IHVuaXRzIG1lYW4gaW5zaWRlIGFuIHN2ZyBlbGVtZW50LlxuICAgKiBGb3IgZXhhbXBsZSwgaWYgdGhlIFNWRyBlbGVtZW50IGlzIDUwMCAod2lkdGgpIGJ5IDIwMCAoaGVpZ2h0KSxcbiAgICogYW5kIHlvdSBwYXNzIHZpZXdCb3g9XCIwIDAgNTAgMjBcIixcbiAgICogdGhpcyBtZWFucyB0aGF0IHRoZSBjb29yZGluYXRlcyBpbnNpZGUgdGhlIHN2ZyB3aWxsIGdvIGZyb20gdGhlIHRvcCBsZWZ0IGNvcm5lciAoMCwwKVxuICAgKiB0byBib3R0b20gcmlnaHQgKDUwLDIwKSBhbmQgZWFjaCB1bml0IHdpbGwgYmUgd29ydGggMTBweC5cbiAgICovXG4gIHZpZXdCb3g6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcuaXNSZXF1aXJlZFxufTtcblxudmFyIFN2Z0ljb24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShTdmdJY29uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBTdmdJY29uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFN2Z0ljb24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChTdmdJY29uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShTdmdJY29uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShTdmdJY29uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICB0aXRsZUFjY2VzcyA9IF9wcm9wcy50aXRsZUFjY2VzcyxcbiAgICAgICAgICB2aWV3Qm94ID0gX3Byb3BzLnZpZXdCb3gsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAndGl0bGVBY2Nlc3MnLCAndmlld0JveCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2luaGVyaXQnKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ3N2ZycsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAgIGZvY3VzYWJsZTogJ2ZhbHNlJyxcbiAgICAgICAgICB2aWV3Qm94OiB2aWV3Qm94LFxuICAgICAgICAgICdhcmlhLWhpZGRlbic6IHRpdGxlQWNjZXNzID8gJ2ZhbHNlJyA6ICd0cnVlJ1xuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIHRpdGxlQWNjZXNzID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3RpdGxlJyxcbiAgICAgICAgICBudWxsLFxuICAgICAgICAgIHRpdGxlQWNjZXNzXG4gICAgICAgICkgOiBudWxsLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFN2Z0ljb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5TdmdJY29uLmRlZmF1bHRQcm9wcyA9IHtcbiAgdmlld0JveDogJzAgMCAyNCAyNCcsXG4gIGNvbG9yOiAnaW5oZXJpdCdcbn07XG5TdmdJY29uLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpU3ZnSWNvbicgfSkoU3ZnSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCcuL1N2Z0ljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMzQgMzYgMzkgNDMgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9yZWFjdERvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdERvbSk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCB0byBwb3J0YWwgaW4gb3JkZXIgdG8gZXNjYXBlIHRoZSBwYXJlbnQgRE9NIG5vZGUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAgdGhlIGNoaWxkcmVuIHdpbGwgYmUgbW91bnRlZCBpbnRvIHRoZSBET00uXG4gICAqL1xuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgUG9ydGFsID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoUG9ydGFsLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBQb3J0YWwoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgUG9ydGFsKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IFBvcnRhbC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoUG9ydGFsKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMubGF5ZXIgPSBudWxsLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoUG9ydGFsLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJMYXllcigpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcbiAgICAgIGlmICghX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbCkge1xuICAgICAgICB0aGlzLnJlbmRlckxheWVyKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHRoaXMudW5yZW5kZXJMYXllcigpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldExheWVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0TGF5ZXIoKSB7XG4gICAgICBpZiAoIXRoaXMubGF5ZXIpIHtcbiAgICAgICAgdGhpcy5sYXllciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0aGlzLmxheWVyLnNldEF0dHJpYnV0ZSgnZGF0YS1tdWktcG9ydGFsJywgJ3RydWUnKTtcbiAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkgJiYgdGhpcy5sYXllcikge1xuICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5sYXllcik7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMubGF5ZXI7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndW5yZW5kZXJMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVucmVuZGVyTGF5ZXIoKSB7XG4gICAgICBpZiAoIXRoaXMubGF5ZXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgX3JlYWN0RG9tMi5kZWZhdWx0LnVubW91bnRDb21wb25lbnRBdE5vZGUodGhpcy5sYXllcik7XG4gICAgICB9XG5cbiAgICAgIGlmIChkb2N1bWVudC5ib2R5KSB7XG4gICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGhpcy5sYXllcik7XG4gICAgICB9XG4gICAgICB0aGlzLmxheWVyID0gbnVsbDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXJMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckxheWVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb3BlbiA9IF9wcm9wcy5vcGVuO1xuXG5cbiAgICAgIGlmIChvcGVuKSB7XG4gICAgICAgIC8vIEJ5IGNhbGxpbmcgdGhpcyBtZXRob2QgaW4gY29tcG9uZW50RGlkTW91bnQoKSBhbmRcbiAgICAgICAgLy8gY29tcG9uZW50RGlkVXBkYXRlKCksIHlvdSdyZSBlZmZlY3RpdmVseSBjcmVhdGluZyBhIFwid29ybWhvbGVcIiB0aGF0XG4gICAgICAgIC8vIGZ1bm5lbHMgUmVhY3QncyBoaWVyYXJjaGljYWwgdXBkYXRlcyB0aHJvdWdoIHRvIGEgRE9NIG5vZGUgb24gYW5cbiAgICAgICAgLy8gZW50aXJlbHkgZGlmZmVyZW50IHBhcnQgb2YgdGhlIHBhZ2UuXG4gICAgICAgIHZhciBsYXllckVsZW1lbnQgPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ub25seShjaGlsZHJlbik7XG4gICAgICAgIF9yZWFjdERvbTIuZGVmYXVsdC51bnN0YWJsZV9yZW5kZXJTdWJ0cmVlSW50b0NvbnRhaW5lcih0aGlzLCBsYXllckVsZW1lbnQsIHRoaXMuZ2V0TGF5ZXIoKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMyLmNoaWxkcmVuLFxuICAgICAgICAgIG9wZW4gPSBfcHJvcHMyLm9wZW47XG5cbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcblxuICAgICAgaWYgKCFfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICAvLyBDYW4ndCBiZSByZW5kZXJlZCBzZXJ2ZXItc2lkZS5cbiAgICAgIGlmIChfaW5ET00yLmRlZmF1bHQpIHtcbiAgICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgICB2YXIgbGF5ZXIgPSB0aGlzLmdldExheWVyKCk7XG4gICAgICAgICAgLy8gJEZsb3dGaXhNZSBsYXllciBpcyBub24tbnVsbFxuICAgICAgICAgIHJldHVybiBfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKGNoaWxkcmVuLCBsYXllcik7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBQb3J0YWw7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Qb3J0YWwuZGVmYXVsdFByb3BzID0ge1xuICBvcGVuOiBmYWxzZVxufTtcblBvcnRhbC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyB7XG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59IDoge307XG5leHBvcnRzLmRlZmF1bHQgPSBQb3J0YWw7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvUG9ydGFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC9Qb3J0YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5vbmVPZlR5cGUoW3JlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlciwgcmVxdWlyZShcInByb3AtdHlwZXNcIikuc2hhcGUoe1xuICBlbnRlcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLmlzUmVxdWlyZWQsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlci5pc1JlcXVpcmVkXG59KV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZShcInByb3AtdHlwZXNcIikuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0ge1xuICBhcHBlYXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgYXBwZWFyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZXhpdEFjdGl2ZTogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nXG59O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDExIDI3IDI4IDM0IDM1IDM2IDM3IDM4IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX1RyYW5zaXRpb24gPSByZXF1aXJlKCdyZWFjdC10cmFuc2l0aW9uLWdyb3VwL1RyYW5zaXRpb24nKTtcblxudmFyIF9UcmFuc2l0aW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RyYW5zaXRpb24pO1xuXG52YXIgX3RyYW5zaXRpb25zID0gcmVxdWlyZSgnLi4vc3R5bGVzL3RyYW5zaXRpb25zJyk7XG5cbnZhciBfd2l0aFRoZW1lID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhUaGVtZScpO1xuXG52YXIgX3dpdGhUaGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoVGhlbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBUcmFuc2l0aW9uXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGFwcGVhcjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQSBzaW5nbGUgY2hpbGQgY29udGVudCBlbGVtZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjb21wb25lbnQgd2lsbCB0cmFuc2l0aW9uIGluLlxuICAgKi9cbiAgaW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcmluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgdGltZW91dDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uKS5pc1JlcXVpcmVkXG59O1xuXG5cbnZhciByZWZsb3cgPSBmdW5jdGlvbiByZWZsb3cobm9kZSkge1xuICByZXR1cm4gbm9kZS5zY3JvbGxUb3A7XG59O1xuXG4vKipcbiAqIFRoZSBGYWRlIHRyYW5zaXRpb24gaXMgdXNlZCBieSB0aGUgTW9kYWwgY29tcG9uZW50LlxuICogSXQncyB1c2luZyBbcmVhY3QtdHJhbnNpdGlvbi1ncm91cF0oaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0anMvcmVhY3QtdHJhbnNpdGlvbi1ncm91cCkgaW50ZXJuYWxseS5cbiAqL1xuXG52YXIgRmFkZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEZhZGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEZhZGUoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgRmFkZSk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBGYWRlLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShGYWRlKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgcmVmbG93KG5vZGUpO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUVudGVyaW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMudGhlbWUsXG4gICAgICAgICAgdGltZW91dCA9IF90aGlzJHByb3BzLnRpbWVvdXQ7XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmVudGVyXG4gICAgICB9KTtcbiAgICAgIC8vICRGbG93Rml4TWUgLSBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svZmxvdy9wdWxsLzUxNjFcbiAgICAgIG5vZGUuc3R5bGUud2Via2l0VHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmVudGVyXG4gICAgICB9KTtcbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcxJztcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcmluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMyLnRpbWVvdXQ7XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmV4aXRcbiAgICAgIH0pO1xuICAgICAgLy8gJEZsb3dGaXhNZSAtIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9mbG93L3B1bGwvNTE2MVxuICAgICAgbm9kZS5zdHlsZS53ZWJraXRUcmFuc2l0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5Jywge1xuICAgICAgICBkdXJhdGlvbjogdHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInID8gdGltZW91dCA6IHRpbWVvdXQuZXhpdFxuICAgICAgfSk7XG4gICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMCc7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXQpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0KG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShGYWRlLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFwcGVhciA9IF9wcm9wcy5hcHBlYXIsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wcy5vbkVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmcgPSBfcHJvcHMub25FbnRlcmluZyxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMub25FeGl0LFxuICAgICAgICAgIHN0eWxlUHJvcCA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhcHBlYXInLCAnY2hpbGRyZW4nLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRXhpdCcsICdzdHlsZScsICd0aGVtZSddKTtcblxuXG4gICAgICB2YXIgc3R5bGUgPSAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBzdHlsZVByb3ApO1xuXG4gICAgICAvLyBGb3Igc2VydmVyIHNpZGUgcmVuZGVyaW5nLlxuICAgICAgaWYgKCF0aGlzLnByb3BzLmluIHx8IGFwcGVhcikge1xuICAgICAgICBzdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9UcmFuc2l0aW9uMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhcHBlYXI6IGFwcGVhcixcbiAgICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgICAgb25FbnRlcjogdGhpcy5oYW5kbGVFbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nOiB0aGlzLmhhbmRsZUVudGVyaW5nLFxuICAgICAgICAgIG9uRXhpdDogdGhpcy5oYW5kbGVFeGl0XG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBGYWRlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuRmFkZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGFwcGVhcjogdHJ1ZSxcbiAgdGltZW91dDoge1xuICAgIGVudGVyOiBfdHJhbnNpdGlvbnMuZHVyYXRpb24uZW50ZXJpbmdTY3JlZW4sXG4gICAgZXhpdDogX3RyYW5zaXRpb25zLmR1cmF0aW9uLmxlYXZpbmdTY3JlZW5cbiAgfVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFRoZW1lMi5kZWZhdWx0KSgpKEZhZGUpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0ZhZGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0ZhZGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxuZXhwb3J0cy5nZXRTY2FsZSA9IGdldFNjYWxlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfQ1NTVHJhbnNpdGlvbiA9IHJlcXVpcmUoJ3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvQ1NTVHJhbnNpdGlvbicpO1xuXG52YXIgX0NTU1RyYW5zaXRpb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQ1NTVHJhbnNpdGlvbik7XG5cbnZhciBfd2l0aFRoZW1lID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhUaGVtZScpO1xuXG52YXIgX3dpdGhUaGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoVGhlbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBDU1NUcmFuc2l0aW9uXG5cbi8vIE9ubHkgZXhwb3J0ZWQgZm9yIHRlc3RzLlxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbmZ1bmN0aW9uIGdldFNjYWxlKHZhbHVlKSB7XG4gIHJldHVybiAnc2NhbGUoJyArIHZhbHVlICsgJywgJyArIE1hdGgucG93KHZhbHVlLCAyKSArICcpJztcbn1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gIGVudGVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICBleGl0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG59KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBhcHBlYXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEEgc2luZ2xlIGNoaWxkIGNvbnRlbnQgZWxlbWVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBzaG93IHRoZSBjb21wb25lbnQ7IHRyaWdnZXJzIHRoZSBlbnRlciBvciBleGl0IGFuaW1hdGlvbi5cbiAgICovXG4gIGluOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkV4aXRpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FeGl0ZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBhbmltYXRpb24gY2xhc3NOYW1lcyBhcHBsaWVkIHRvIHRoZSBjb21wb25lbnQgYXMgaXQgZW50ZXJzIG9yIGV4aXRzLlxuICAgKiBUaGlzIHByb3BlcnR5IGlzIGEgZGlyZWN0IGJpbmRpbmcgdG8gW2BDU1NUcmFuc2l0aW9uLmNsYXNzTmFtZXNgXShodHRwczovL3JlYWN0Y29tbXVuaXR5Lm9yZy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwLyNDU1NUcmFuc2l0aW9uLXByb3AtY2xhc3NOYW1lcykuXG4gICAqL1xuICB0cmFuc2l0aW9uQ2xhc3NlczogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNsYXNzZXMuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcyA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2xhc3NlcykuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGR1cmF0aW9uIGZvciB0aGUgdHJhbnNpdGlvbiwgaW4gbWlsbGlzZWNvbmRzLlxuICAgKiBZb3UgbWF5IHNwZWNpZnkgYSBzaW5nbGUgdGltZW91dCBmb3IgYWxsIHRyYW5zaXRpb25zLCBvciBpbmRpdmlkdWFsbHkgd2l0aCBhbiBvYmplY3QuXG4gICAqXG4gICAqIFNldCB0byAnYXV0bycgdG8gYXV0b21hdGljYWxseSBjYWxjdWxhdGUgdHJhbnNpdGlvbiB0aW1lIGJhc2VkIG9uIGhlaWdodC5cbiAgICovXG4gIHRpbWVvdXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgZW50ZXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gICAgZXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxuICB9KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogVGhlIEdyb3cgdHJhbnNpdGlvbiBpcyB1c2VkIGJ5IHRoZSBQb3BvdmVyIGNvbXBvbmVudC5cbiAqIEl0J3MgdXNpbmcgW3JlYWN0LXRyYW5zaXRpb24tZ3JvdXBdKGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdGpzL3JlYWN0LXRyYW5zaXRpb24tZ3JvdXApIGludGVybmFsbHkuXG4gKi9cbnZhciBHcm93ID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoR3JvdywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gR3JvdygpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBHcm93KTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IEdyb3cuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEdyb3cpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5hdXRvVGltZW91dCA9IHVuZGVmaW5lZCwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgbm9kZS5zdHlsZS50cmFuc2Zvcm0gPSBnZXRTY2FsZSgwLjc1KTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXIpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcihub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFbnRlcmluZyA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzLnRoZW1lLFxuICAgICAgICAgIHRpbWVvdXQgPSBfdGhpcyRwcm9wcy50aW1lb3V0O1xuXG4gICAgICB2YXIgZHVyYXRpb24gPSAwO1xuXG4gICAgICBpZiAodGltZW91dCA9PT0gJ2F1dG8nKSB7XG4gICAgICAgIGR1cmF0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuZ2V0QXV0b0hlaWdodER1cmF0aW9uKG5vZGUuY2xpZW50SGVpZ2h0KTtcbiAgICAgICAgX3RoaXMuYXV0b1RpbWVvdXQgPSBkdXJhdGlvbjtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIGR1cmF0aW9uID0gdGltZW91dDtcbiAgICAgIH0gZWxzZSBpZiAodGltZW91dCAmJiB0eXBlb2YgdGltZW91dC5lbnRlciA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgZHVyYXRpb24gPSB0aW1lb3V0LmVudGVyO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gVGhlIHByb3BUeXBlIHdpbGwgd2FybiBpbiB0aGlzIGNhc2UuXG4gICAgICB9XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IFt0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiBkdXJhdGlvblxuICAgICAgfSksIHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgndHJhbnNmb3JtJywge1xuICAgICAgICBkdXJhdGlvbjogZHVyYXRpb24gKiAwLjY2NlxuICAgICAgfSldLmpvaW4oJywnKTtcblxuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzEnO1xuICAgICAgbm9kZS5zdHlsZS50cmFuc2Zvcm0gPSBnZXRTY2FsZSgxKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcmluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMyLnRpbWVvdXQ7XG5cbiAgICAgIHZhciBkdXJhdGlvbiA9IDA7XG5cbiAgICAgIGlmICh0aW1lb3V0ID09PSAnYXV0bycpIHtcbiAgICAgICAgZHVyYXRpb24gPSB0aGVtZS50cmFuc2l0aW9ucy5nZXRBdXRvSGVpZ2h0RHVyYXRpb24obm9kZS5jbGllbnRIZWlnaHQpO1xuICAgICAgICBfdGhpcy5hdXRvVGltZW91dCA9IGR1cmF0aW9uO1xuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgdGltZW91dCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgZHVyYXRpb24gPSB0aW1lb3V0O1xuICAgICAgfSBlbHNlIGlmICh0aW1lb3V0ICYmIHR5cGVvZiB0aW1lb3V0LmV4aXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIGR1cmF0aW9uID0gdGltZW91dC5leGl0O1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gVGhlIHByb3BUeXBlIHdpbGwgd2FybiBpbiB0aGlzIGNhc2UuXG4gICAgICB9XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IFt0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiBkdXJhdGlvblxuICAgICAgfSksIHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgndHJhbnNmb3JtJywge1xuICAgICAgICBkdXJhdGlvbjogZHVyYXRpb24gKiAwLjY2NixcbiAgICAgICAgZGVsYXk6IGR1cmF0aW9uICogMC4zMzNcbiAgICAgIH0pXS5qb2luKCcsJyk7XG5cbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcwJztcbiAgICAgIG5vZGUuc3R5bGUudHJhbnNmb3JtID0gZ2V0U2NhbGUoMC43NSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXQpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0KG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmFkZEVuZExpc3RlbmVyID0gZnVuY3Rpb24gKG5vZGUsIG5leHQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy50aW1lb3V0ID09PSAnYXV0bycpIHtcbiAgICAgICAgc2V0VGltZW91dChuZXh0LCBfdGhpcy5hdXRvVGltZW91dCB8fCAwKTtcbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoR3JvdywgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhcHBlYXIgPSBfcHJvcHMuYXBwZWFyLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzLm9uRXhpdCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgc3R5bGVQcm9wID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICAgIHRyYW5zaXRpb25DbGFzc2VzID0gX3Byb3BzLnRyYW5zaXRpb25DbGFzc2VzLFxuICAgICAgICAgIHRpbWVvdXQgPSBfcHJvcHMudGltZW91dCxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhcHBlYXInLCAnY2hpbGRyZW4nLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRXhpdCcsICdyb290UmVmJywgJ3N0eWxlJywgJ3RyYW5zaXRpb25DbGFzc2VzJywgJ3RpbWVvdXQnLCAndGhlbWUnXSk7XG5cblxuICAgICAgdmFyIHN0eWxlID0gKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgY2hpbGRyZW4ucHJvcHMuc3R5bGUsIHN0eWxlUHJvcCk7XG5cbiAgICAgIC8vIEZvciBzZXJ2ZXIgc2lkZSByZW5kZXJpbmcuXG4gICAgICBpZiAoIXRoaXMucHJvcHMuaW4gfHwgYXBwZWFyKSB7XG4gICAgICAgIHN0eWxlLm9wYWNpdHkgPSAnMCc7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0NTU1RyYW5zaXRpb24yLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZXM6IHRyYW5zaXRpb25DbGFzc2VzLFxuICAgICAgICAgIG9uRW50ZXI6IHRoaXMuaGFuZGxlRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZzogdGhpcy5oYW5kbGVFbnRlcmluZyxcbiAgICAgICAgICBvbkV4aXQ6IHRoaXMuaGFuZGxlRXhpdCxcbiAgICAgICAgICBhZGRFbmRMaXN0ZW5lcjogdGhpcy5hZGRFbmRMaXN0ZW5lcixcbiAgICAgICAgICBhcHBlYXI6IGFwcGVhcixcbiAgICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgICAgdGltZW91dDogdGltZW91dCA9PT0gJ2F1dG8nID8gbnVsbCA6IHRpbWVvdXRcbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gR3Jvdztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkdyb3cuZGVmYXVsdFByb3BzID0ge1xuICBhcHBlYXI6IHRydWUsXG4gIHRpbWVvdXQ6ICdhdXRvJyxcbiAgdHJhbnNpdGlvbkNsYXNzZXM6IHt9XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoVGhlbWUyLmRlZmF1bHQpKCkoR3Jvdyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvR3Jvdy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvR3Jvdy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDM0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5hcmlhSGlkZGVuID0gYXJpYUhpZGRlbjtcbmV4cG9ydHMuaGlkZVNpYmxpbmdzID0gaGlkZVNpYmxpbmdzO1xuZXhwb3J0cy5zaG93U2libGluZ3MgPSBzaG93U2libGluZ3M7XG4vLyAgd2Vha1xuXG52YXIgQkxBQ0tMSVNUID0gWyd0ZW1wbGF0ZScsICdzY3JpcHQnLCAnc3R5bGUnXTtcblxudmFyIGlzSGlkYWJsZSA9IGZ1bmN0aW9uIGlzSGlkYWJsZShfcmVmKSB7XG4gIHZhciBub2RlVHlwZSA9IF9yZWYubm9kZVR5cGUsXG4gICAgICB0YWdOYW1lID0gX3JlZi50YWdOYW1lO1xuICByZXR1cm4gbm9kZVR5cGUgPT09IDEgJiYgQkxBQ0tMSVNULmluZGV4T2YodGFnTmFtZS50b0xvd2VyQ2FzZSgpKSA9PT0gLTE7XG59O1xuXG52YXIgc2libGluZ3MgPSBmdW5jdGlvbiBzaWJsaW5ncyhjb250YWluZXIsIG1vdW50LCBjYikge1xuICBtb3VudCA9IFtdLmNvbmNhdChtb3VudCk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgW10uZm9yRWFjaC5jYWxsKGNvbnRhaW5lci5jaGlsZHJlbiwgZnVuY3Rpb24gKG5vZGUpIHtcbiAgICBpZiAobW91bnQuaW5kZXhPZihub2RlKSA9PT0gLTEgJiYgaXNIaWRhYmxlKG5vZGUpKSB7XG4gICAgICBjYihub2RlKTtcbiAgICB9XG4gIH0pO1xufTtcblxuZnVuY3Rpb24gYXJpYUhpZGRlbihzaG93LCBub2RlKSB7XG4gIGlmICghbm9kZSkge1xuICAgIHJldHVybjtcbiAgfVxuICBpZiAoc2hvdykge1xuICAgIG5vZGUuc2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG4gIH0gZWxzZSB7XG4gICAgbm9kZS5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJyk7XG4gIH1cbn1cblxuZnVuY3Rpb24gaGlkZVNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlKSB7XG4gIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBhcmlhSGlkZGVuKHRydWUsIG5vZGUpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gc2hvd1NpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlKSB7XG4gIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBhcmlhSGlkZGVuKGZhbHNlLCBub2RlKTtcbiAgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfaW52YXJpYW50ID0gcmVxdWlyZSgnaW52YXJpYW50Jyk7XG5cbnZhciBfaW52YXJpYW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ludmFyaWFudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgaXNNb2RpZmllZEV2ZW50ID0gZnVuY3Rpb24gaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB7XG4gIHJldHVybiAhIShldmVudC5tZXRhS2V5IHx8IGV2ZW50LmFsdEtleSB8fCBldmVudC5jdHJsS2V5IHx8IGV2ZW50LnNoaWZ0S2V5KTtcbn07XG5cbi8qKlxuICogVGhlIHB1YmxpYyBBUEkgZm9yIHJlbmRlcmluZyBhIGhpc3RvcnktYXdhcmUgPGE+LlxuICovXG5cbnZhciBMaW5rID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKExpbmssIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpbmsoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMaW5rKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5oYW5kbGVDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2xpY2spIF90aGlzLnByb3BzLm9uQ2xpY2soZXZlbnQpO1xuXG4gICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgJiYgLy8gb25DbGljayBwcmV2ZW50ZWQgZGVmYXVsdFxuICAgICAgZXZlbnQuYnV0dG9uID09PSAwICYmIC8vIGlnbm9yZSByaWdodCBjbGlja3NcbiAgICAgICFfdGhpcy5wcm9wcy50YXJnZXQgJiYgLy8gbGV0IGJyb3dzZXIgaGFuZGxlIFwidGFyZ2V0PV9ibGFua1wiIGV0Yy5cbiAgICAgICFpc01vZGlmaWVkRXZlbnQoZXZlbnQpIC8vIGlnbm9yZSBjbGlja3Mgd2l0aCBtb2RpZmllciBrZXlzXG4gICAgICApIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgdmFyIGhpc3RvcnkgPSBfdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5O1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgICByZXBsYWNlID0gX3RoaXMkcHJvcHMucmVwbGFjZSxcbiAgICAgICAgICAgICAgdG8gPSBfdGhpcyRwcm9wcy50bztcblxuXG4gICAgICAgICAgaWYgKHJlcGxhY2UpIHtcbiAgICAgICAgICAgIGhpc3RvcnkucmVwbGFjZSh0byk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGhpc3RvcnkucHVzaCh0byk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBMaW5rLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHJlcGxhY2UgPSBfcHJvcHMucmVwbGFjZSxcbiAgICAgICAgdG8gPSBfcHJvcHMudG8sXG4gICAgICAgIGlubmVyUmVmID0gX3Byb3BzLmlubmVyUmVmLFxuICAgICAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsncmVwbGFjZScsICd0bycsICdpbm5lclJlZiddKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgKDAsIF9pbnZhcmlhbnQyLmRlZmF1bHQpKHRoaXMuY29udGV4dC5yb3V0ZXIsICdZb3Ugc2hvdWxkIG5vdCB1c2UgPExpbms+IG91dHNpZGUgYSA8Um91dGVyPicpO1xuXG4gICAgdmFyIGhyZWYgPSB0aGlzLmNvbnRleHQucm91dGVyLmhpc3RvcnkuY3JlYXRlSHJlZih0eXBlb2YgdG8gPT09ICdzdHJpbmcnID8geyBwYXRobmFtZTogdG8gfSA6IHRvKTtcblxuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnYScsIF9leHRlbmRzKHt9LCBwcm9wcywgeyBvbkNsaWNrOiB0aGlzLmhhbmRsZUNsaWNrLCBocmVmOiBocmVmLCByZWY6IGlubmVyUmVmIH0pKTtcbiAgfTtcblxuICByZXR1cm4gTGluaztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpbmsucHJvcFR5cGVzID0ge1xuICBvbkNsaWNrOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIHRhcmdldDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG4gIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgdG86IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3RdKS5pc1JlcXVpcmVkLFxuICBpbm5lclJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNdKVxufTtcbkxpbmsuZGVmYXVsdFByb3BzID0ge1xuICByZXBsYWNlOiBmYWxzZVxufTtcbkxpbmsuY29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgIGhpc3Rvcnk6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgICAgcHVzaDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIGNyZWF0ZUhyZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkXG4gICAgfSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gTGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI2IDI3IDI4IDMwIDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDU3IDU4IDU5IDYxIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIFByb3BUeXBlcyA9IF9pbnRlcm9wUmVxdWlyZVdpbGRjYXJkKF9wcm9wVHlwZXMpO1xuXG52YXIgX2FkZENsYXNzID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvY2xhc3MvYWRkQ2xhc3MnKTtcblxudmFyIF9hZGRDbGFzczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9hZGRDbGFzcyk7XG5cbnZhciBfcmVtb3ZlQ2xhc3MgPSByZXF1aXJlKCdkb20taGVscGVycy9jbGFzcy9yZW1vdmVDbGFzcycpO1xuXG52YXIgX3JlbW92ZUNsYXNzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlbW92ZUNsYXNzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX1RyYW5zaXRpb24gPSByZXF1aXJlKCcuL1RyYW5zaXRpb24nKTtcblxudmFyIF9UcmFuc2l0aW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RyYW5zaXRpb24pO1xuXG52YXIgX1Byb3BUeXBlcyA9IHJlcXVpcmUoJy4vdXRpbHMvUHJvcFR5cGVzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZVdpbGRjYXJkKG9iaikgeyBpZiAob2JqICYmIG9iai5fX2VzTW9kdWxlKSB7IHJldHVybiBvYmo7IH0gZWxzZSB7IHZhciBuZXdPYmogPSB7fTsgaWYgKG9iaiAhPSBudWxsKSB7IGZvciAodmFyIGtleSBpbiBvYmopIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGtleSkpIG5ld09ialtrZXldID0gb2JqW2tleV07IH0gfSBuZXdPYmouZGVmYXVsdCA9IG9iajsgcmV0dXJuIG5ld09iajsgfSB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGFkZENsYXNzID0gZnVuY3Rpb24gYWRkQ2xhc3Mobm9kZSwgY2xhc3Nlcykge1xuICByZXR1cm4gY2xhc3NlcyAmJiBjbGFzc2VzLnNwbGl0KCcgJykuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiAoMCwgX2FkZENsYXNzMi5kZWZhdWx0KShub2RlLCBjKTtcbiAgfSk7XG59O1xudmFyIHJlbW92ZUNsYXNzID0gZnVuY3Rpb24gcmVtb3ZlQ2xhc3Mobm9kZSwgY2xhc3Nlcykge1xuICByZXR1cm4gY2xhc3NlcyAmJiBjbGFzc2VzLnNwbGl0KCcgJykuZm9yRWFjaChmdW5jdGlvbiAoYykge1xuICAgIHJldHVybiAoMCwgX3JlbW92ZUNsYXNzMi5kZWZhdWx0KShub2RlLCBjKTtcbiAgfSk7XG59O1xuXG52YXIgcHJvcFR5cGVzID0gX2V4dGVuZHMoe30sIF9UcmFuc2l0aW9uMi5kZWZhdWx0LnByb3BUeXBlcywge1xuXG4gIC8qKlxuICAgKiBUaGUgYW5pbWF0aW9uIGNsYXNzTmFtZXMgYXBwbGllZCB0byB0aGUgY29tcG9uZW50IGFzIGl0IGVudGVycyBvciBleGl0cy5cbiAgICogQSBzaW5nbGUgbmFtZSBjYW4gYmUgcHJvdmlkZWQgYW5kIGl0IHdpbGwgYmUgc3VmZml4ZWQgZm9yIGVhY2ggc3RhZ2U6IGUuZy5cbiAgICpcbiAgICogYGNsYXNzTmFtZXM9XCJmYWRlXCJgIGFwcGxpZXMgYGZhZGUtZW50ZXJgLCBgZmFkZS1lbnRlci1hY3RpdmVgLFxuICAgKiBgZmFkZS1leGl0YCwgYGZhZGUtZXhpdC1hY3RpdmVgLCBgZmFkZS1hcHBlYXJgLCBhbmQgYGZhZGUtYXBwZWFyLWFjdGl2ZWAuXG4gICAqIEVhY2ggaW5kaXZpZHVhbCBjbGFzc05hbWVzIGNhbiBhbHNvIGJlIHNwZWNpZmllZCBpbmRlcGVuZGVudGx5IGxpa2U6XG4gICAqXG4gICAqIGBgYGpzXG4gICAqIGNsYXNzTmFtZXM9e3tcbiAgICogIGFwcGVhcjogJ215LWFwcGVhcicsXG4gICAqICBhcHBlYXJBY3RpdmU6ICdteS1hY3RpdmUtYXBwZWFyJyxcbiAgICogIGVudGVyOiAnbXktZW50ZXInLFxuICAgKiAgZW50ZXJBY3RpdmU6ICdteS1hY3RpdmUtZW50ZXInLFxuICAgKiAgZXhpdDogJ215LWV4aXQnLFxuICAgKiAgZXhpdEFjdGl2ZTogJ215LWFjdGl2ZS1leGl0JyxcbiAgICogfX1cbiAgICogYGBgXG4gICAqXG4gICAqIEB0eXBlIHtzdHJpbmcgfCB7XG4gICAqICBhcHBlYXI/OiBzdHJpbmcsXG4gICAqICBhcHBlYXJBY3RpdmU/OiBzdHJpbmcsXG4gICAqICBlbnRlcj86IHN0cmluZyxcbiAgICogIGVudGVyQWN0aXZlPzogc3RyaW5nLFxuICAgKiAgZXhpdD86IHN0cmluZyxcbiAgICogIGV4aXRBY3RpdmU/OiBzdHJpbmcsXG4gICAqIH19XG4gICAqL1xuICBjbGFzc05hbWVzOiBfUHJvcFR5cGVzLmNsYXNzTmFtZXNTaGFwZSxcblxuICAvKipcbiAgICogQSBgPFRyYW5zaXRpb24+YCBjYWxsYmFjayBmaXJlZCBpbW1lZGlhdGVseSBhZnRlciB0aGUgJ2VudGVyJyBvciAnYXBwZWFyJyBjbGFzcyBpc1xuICAgKiBhcHBsaWVkLlxuICAgKlxuICAgKiBAdHlwZSBGdW5jdGlvbihub2RlOiBIdG1sRWxlbWVudCwgaXNBcHBlYXJpbmc6IGJvb2wpXG4gICAqL1xuICBvbkVudGVyOiBQcm9wVHlwZXMuZnVuYyxcblxuICAvKipcbiAgICogQSBgPFRyYW5zaXRpb24+YCBjYWxsYmFjayBmaXJlZCBpbW1lZGlhdGVseSBhZnRlciB0aGUgJ2VudGVyLWFjdGl2ZScgb3JcbiAgICogJ2FwcGVhci1hY3RpdmUnIGNsYXNzIGlzIGFwcGxpZWQuXG4gICAqXG4gICAqIEB0eXBlIEZ1bmN0aW9uKG5vZGU6IEh0bWxFbGVtZW50LCBpc0FwcGVhcmluZzogYm9vbClcbiAgICovXG4gIG9uRW50ZXJpbmc6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZW50ZXInIG9yXG4gICAqICdhcHBlYXInIGNsYXNzZXMgYXJlICoqcmVtb3ZlZCoqIGZyb20gdGhlIERPTSBub2RlLlxuICAgKlxuICAgKiBAdHlwZSBGdW5jdGlvbihub2RlOiBIdG1sRWxlbWVudCwgaXNBcHBlYXJpbmc6IGJvb2wpXG4gICAqL1xuICBvbkVudGVyZWQ6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZXhpdCcgY2xhc3MgaXNcbiAgICogYXBwbGllZC5cbiAgICpcbiAgICogQHR5cGUgRnVuY3Rpb24obm9kZTogSHRtbEVsZW1lbnQpXG4gICAqL1xuICBvbkV4aXQ6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZXhpdC1hY3RpdmUnIGlzIGFwcGxpZWQuXG4gICAqXG4gICAqIEB0eXBlIEZ1bmN0aW9uKG5vZGU6IEh0bWxFbGVtZW50XG4gICAqL1xuICBvbkV4aXRpbmc6IFByb3BUeXBlcy5mdW5jLFxuXG4gIC8qKlxuICAgKiBBIGA8VHJhbnNpdGlvbj5gIGNhbGxiYWNrIGZpcmVkIGltbWVkaWF0ZWx5IGFmdGVyIHRoZSAnZXhpdCcgY2xhc3Nlc1xuICAgKiBhcmUgKipyZW1vdmVkKiogZnJvbSB0aGUgRE9NIG5vZGUuXG4gICAqXG4gICAqIEB0eXBlIEZ1bmN0aW9uKG5vZGU6IEh0bWxFbGVtZW50KVxuICAgKi9cbiAgb25FeGl0ZWQ6IFByb3BUeXBlcy5mdW5jXG59KTtcblxuLyoqXG4gKiBBIGBUcmFuc2l0aW9uYCBjb21wb25lbnQgdXNpbmcgQ1NTIHRyYW5zaXRpb25zIGFuZCBhbmltYXRpb25zLlxuICogSXQncyBpbnNwaXJlZCBieSB0aGUgZXhjZWxsZW50IFtuZy1hbmltYXRlXShodHRwOi8vd3d3Lm5nYW5pbWF0ZS5vcmcvKSBsaWJyYXJ5LlxuICpcbiAqIGBDU1NUcmFuc2l0aW9uYCBhcHBsaWVzIGEgcGFpciBvZiBjbGFzcyBuYW1lcyBkdXJpbmcgdGhlIGBhcHBlYXJgLCBgZW50ZXJgLFxuICogYW5kIGBleGl0YCBzdGFnZXMgb2YgdGhlIHRyYW5zaXRpb24uIFRoZSBmaXJzdCBjbGFzcyBpcyBhcHBsaWVkIGFuZCB0aGVuIGFcbiAqIHNlY29uZCBcImFjdGl2ZVwiIGNsYXNzIGluIG9yZGVyIHRvIGFjdGl2YXRlIHRoZSBjc3MgYW5pbWF0aW9uLlxuICpcbiAqIFdoZW4gdGhlIGBpbmAgcHJvcCBpcyB0b2dnbGVkIHRvIGB0cnVlYCB0aGUgQ29tcG9uZW50IHdpbGwgZ2V0XG4gKiB0aGUgYGV4YW1wbGUtZW50ZXJgIENTUyBjbGFzcyBhbmQgdGhlIGBleGFtcGxlLWVudGVyLWFjdGl2ZWAgQ1NTIGNsYXNzXG4gKiBhZGRlZCBpbiB0aGUgbmV4dCB0aWNrLiBUaGlzIGlzIGEgY29udmVudGlvbiBiYXNlZCBvbiB0aGUgYGNsYXNzTmFtZXNgIHByb3AuXG4gKlxuICogYGBganNcbiAqIGltcG9ydCBDU1NUcmFuc2l0aW9uIGZyb20gJ3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvQ1NTVHJhbnNpdGlvbic7XG4gKlxuICogY29uc3QgRmFkZSA9ICh7IGNoaWxkcmVuLCAuLi5wcm9wcyB9KSA9PiAoXG4gKiAgPENTU1RyYW5zaXRpb25cbiAqICAgIHsuLi5wcm9wc31cbiAqICAgIHRpbWVvdXQ9ezUwMH1cbiAqICAgIGNsYXNzTmFtZXM9XCJmYWRlXCJcbiAqICA+XG4gKiAgIHtjaGlsZHJlbn1cbiAqICA8L0NTU1RyYW5zaXRpb24+XG4gKiApO1xuICpcbiAqIGNsYXNzIEZhZGVJbkFuZE91dCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudCB7XG4gKiAgIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcbiAqICAgICBzdXBlciguLi5hcmdzKTtcbiAqICAgICB0aGlzLnN0YXRlPSB7IHNob3c6IGZhbHNlIH1cbiAqXG4gKiAgICAgc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICogICAgICAgdGhpcy5zZXRTdGF0ZSh7IHNob3c6ICF0aGlzLnN0YXRlLnNob3cgfSlcbiAqICAgICB9LCA1MDAwKVxuICogICB9XG4gKiAgIHJlbmRlcigpIHtcbiAqICAgICByZXR1cm4gKFxuICogICAgICAgPEZhZGUgaW49e3RoaXMuc3RhdGUuc2hvd30+XG4gKiAgICAgICAgIDxkaXY+SGVsbG8gd29ybGQ8L2Rpdj5cbiAqICAgICAgIDwvRmFkZT5cbiAqICAgICApXG4gKiAgIH1cbiAqIH1cbiAqIGBgYFxuICpcbiAqIEFuZCB0aGUgY29vcnJlc3BvbmRpbmcgQ1NTIGZvciB0aGUgYDxGYWRlPmAgY29tcG9uZW50OlxuICpcbiAqIGBgYGNzc1xuICogLmZhZGUtZW50ZXIge1xuICogICBvcGFjaXR5OiAwLjAxO1xuICogfVxuICpcbiAqIC5mYWRlLWVudGVyLmZhZGUtZW50ZXItYWN0aXZlIHtcbiAqICAgb3BhY2l0eTogMTtcbiAqICAgdHJhbnNpdGlvbjogb3BhY2l0eSA1MDBtcyBlYXNlLWluO1xuICogfVxuICpcbiAqIC5mYWRlLWV4aXQge1xuICogICBvcGFjaXR5OiAxO1xuICogfVxuICpcbiAqIC5mYWRlLWV4aXQuZmFkZS1leGl0LWFjdGl2ZSB7XG4gKiAgIG9wYWNpdHk6IDAuMDE7XG4gKiAgIHRyYW5zaXRpb246IG9wYWNpdHkgMzAwbXMgZWFzZS1pbjtcbiAqIH1cbiAqIGBgYFxuICovXG5cbnZhciBDU1NUcmFuc2l0aW9uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKENTU1RyYW5zaXRpb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIENTU1RyYW5zaXRpb24oKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBDU1NUcmFuc2l0aW9uKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5vbkVudGVyID0gZnVuY3Rpb24gKG5vZGUsIGFwcGVhcmluZykge1xuICAgICAgdmFyIF90aGlzJGdldENsYXNzTmFtZXMgPSBfdGhpcy5nZXRDbGFzc05hbWVzKGFwcGVhcmluZyA/ICdhcHBlYXInIDogJ2VudGVyJyksXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3RoaXMkZ2V0Q2xhc3NOYW1lcy5jbGFzc05hbWU7XG5cbiAgICAgIF90aGlzLnJlbW92ZUNsYXNzZXMobm9kZSwgJ2V4aXQnKTtcbiAgICAgIGFkZENsYXNzKG5vZGUsIGNsYXNzTmFtZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXIobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMub25FbnRlcmluZyA9IGZ1bmN0aW9uIChub2RlLCBhcHBlYXJpbmcpIHtcbiAgICAgIHZhciBfdGhpcyRnZXRDbGFzc05hbWVzMiA9IF90aGlzLmdldENsYXNzTmFtZXMoYXBwZWFyaW5nID8gJ2FwcGVhcicgOiAnZW50ZXInKSxcbiAgICAgICAgICBhY3RpdmVDbGFzc05hbWUgPSBfdGhpcyRnZXRDbGFzc05hbWVzMi5hY3RpdmVDbGFzc05hbWU7XG5cbiAgICAgIF90aGlzLnJlZmxvd0FuZEFkZENsYXNzKG5vZGUsIGFjdGl2ZUNsYXNzTmFtZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyaW5nKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJpbmcobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMub25FbnRlcmVkID0gZnVuY3Rpb24gKG5vZGUsIGFwcGVhcmluZykge1xuICAgICAgX3RoaXMucmVtb3ZlQ2xhc3Nlcyhub2RlLCBhcHBlYXJpbmcgPyAnYXBwZWFyJyA6ICdlbnRlcicpO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcmVkKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJlZChub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5vbkV4aXQgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgdmFyIF90aGlzJGdldENsYXNzTmFtZXMzID0gX3RoaXMuZ2V0Q2xhc3NOYW1lcygnZXhpdCcpLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF90aGlzJGdldENsYXNzTmFtZXMzLmNsYXNzTmFtZTtcblxuICAgICAgX3RoaXMucmVtb3ZlQ2xhc3Nlcyhub2RlLCAnYXBwZWFyJyk7XG4gICAgICBfdGhpcy5yZW1vdmVDbGFzc2VzKG5vZGUsICdlbnRlcicpO1xuICAgICAgYWRkQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRXhpdCkge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkV4aXQobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMub25FeGl0aW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRnZXRDbGFzc05hbWVzNCA9IF90aGlzLmdldENsYXNzTmFtZXMoJ2V4aXQnKSxcbiAgICAgICAgICBhY3RpdmVDbGFzc05hbWUgPSBfdGhpcyRnZXRDbGFzc05hbWVzNC5hY3RpdmVDbGFzc05hbWU7XG5cbiAgICAgIF90aGlzLnJlZmxvd0FuZEFkZENsYXNzKG5vZGUsIGFjdGl2ZUNsYXNzTmFtZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXRpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0aW5nKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLm9uRXhpdGVkID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIF90aGlzLnJlbW92ZUNsYXNzZXMobm9kZSwgJ2V4aXQnKTtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRXhpdGVkKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRXhpdGVkKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmdldENsYXNzTmFtZXMgPSBmdW5jdGlvbiAodHlwZSkge1xuICAgICAgdmFyIGNsYXNzTmFtZXMgPSBfdGhpcy5wcm9wcy5jbGFzc05hbWVzO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSB0eXBlb2YgY2xhc3NOYW1lcyAhPT0gJ3N0cmluZycgPyBjbGFzc05hbWVzW3R5cGVdIDogY2xhc3NOYW1lcyArICctJyArIHR5cGU7XG5cbiAgICAgIHZhciBhY3RpdmVDbGFzc05hbWUgPSB0eXBlb2YgY2xhc3NOYW1lcyAhPT0gJ3N0cmluZycgPyBjbGFzc05hbWVzW3R5cGUgKyAnQWN0aXZlJ10gOiBjbGFzc05hbWUgKyAnLWFjdGl2ZSc7XG5cbiAgICAgIHJldHVybiB7IGNsYXNzTmFtZTogY2xhc3NOYW1lLCBhY3RpdmVDbGFzc05hbWU6IGFjdGl2ZUNsYXNzTmFtZSB9O1xuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgQ1NTVHJhbnNpdGlvbi5wcm90b3R5cGUucmVtb3ZlQ2xhc3NlcyA9IGZ1bmN0aW9uIHJlbW92ZUNsYXNzZXMobm9kZSwgdHlwZSkge1xuICAgIHZhciBfZ2V0Q2xhc3NOYW1lcyA9IHRoaXMuZ2V0Q2xhc3NOYW1lcyh0eXBlKSxcbiAgICAgICAgY2xhc3NOYW1lID0gX2dldENsYXNzTmFtZXMuY2xhc3NOYW1lLFxuICAgICAgICBhY3RpdmVDbGFzc05hbWUgPSBfZ2V0Q2xhc3NOYW1lcy5hY3RpdmVDbGFzc05hbWU7XG5cbiAgICBjbGFzc05hbWUgJiYgcmVtb3ZlQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKTtcbiAgICBhY3RpdmVDbGFzc05hbWUgJiYgcmVtb3ZlQ2xhc3Mobm9kZSwgYWN0aXZlQ2xhc3NOYW1lKTtcbiAgfTtcblxuICBDU1NUcmFuc2l0aW9uLnByb3RvdHlwZS5yZWZsb3dBbmRBZGRDbGFzcyA9IGZ1bmN0aW9uIHJlZmxvd0FuZEFkZENsYXNzKG5vZGUsIGNsYXNzTmFtZSkge1xuICAgIC8vIFRoaXMgaXMgZm9yIHRvIGZvcmNlIGEgcmVwYWludCxcbiAgICAvLyB3aGljaCBpcyBuZWNlc3NhcnkgaW4gb3JkZXIgdG8gdHJhbnNpdGlvbiBzdHlsZXMgd2hlbiBhZGRpbmcgYSBjbGFzcyBuYW1lLlxuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXVudXNlZC1leHByZXNzaW9ucyAqL1xuICAgIG5vZGUuc2Nyb2xsVG9wO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tdW51c2VkLWV4cHJlc3Npb25zICovXG4gICAgYWRkQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKTtcbiAgfTtcblxuICBDU1NUcmFuc2l0aW9uLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIHByb3BzID0gX2V4dGVuZHMoe30sIHRoaXMucHJvcHMpO1xuXG4gICAgZGVsZXRlIHByb3BzLmNsYXNzTmFtZXM7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX1RyYW5zaXRpb24yLmRlZmF1bHQsIF9leHRlbmRzKHt9LCBwcm9wcywge1xuICAgICAgb25FbnRlcjogdGhpcy5vbkVudGVyLFxuICAgICAgb25FbnRlcmVkOiB0aGlzLm9uRW50ZXJlZCxcbiAgICAgIG9uRW50ZXJpbmc6IHRoaXMub25FbnRlcmluZyxcbiAgICAgIG9uRXhpdDogdGhpcy5vbkV4aXQsXG4gICAgICBvbkV4aXRpbmc6IHRoaXMub25FeGl0aW5nLFxuICAgICAgb25FeGl0ZWQ6IHRoaXMub25FeGl0ZWRcbiAgICB9KSk7XG4gIH07XG5cbiAgcmV0dXJuIENTU1RyYW5zaXRpb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5DU1NUcmFuc2l0aW9uLnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IHByb3BUeXBlcyA6IHt9O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBDU1NUcmFuc2l0aW9uO1xubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3QtdHJhbnNpdGlvbi1ncm91cC9DU1NUcmFuc2l0aW9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwL0NTU1RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzNCIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gcmVxdWlyZSgnLi91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsJyk7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUZhY3RvcnkgPSBmdW5jdGlvbiBjcmVhdGVGYWN0b3J5KHR5cGUpIHtcbiAgdmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50ID0gKDAsIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Mi5kZWZhdWx0KSh0eXBlKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwLCBjKSB7XG4gICAgcmV0dXJuICgwLCBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIuZGVmYXVsdCkoZmFsc2UsIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwLCBjKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUZhY3Rvcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGdldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gZ2V0RGlzcGxheU5hbWUoQ29tcG9uZW50KSB7XG4gIGlmICh0eXBlb2YgQ29tcG9uZW50ID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBDb21wb25lbnQ7XG4gIH1cblxuICBpZiAoIUNvbXBvbmVudCkge1xuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgaXNDbGFzc0NvbXBvbmVudCA9IGZ1bmN0aW9uIGlzQ2xhc3NDb21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKENvbXBvbmVudCAmJiBDb21wb25lbnQucHJvdG90eXBlICYmIF90eXBlb2YoQ29tcG9uZW50LnByb3RvdHlwZS5pc1JlYWN0Q29tcG9uZW50KSA9PT0gJ29iamVjdCcpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNDbGFzc0NvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc0NsYXNzQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc0NsYXNzQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSBmdW5jdGlvbiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbih0eXBlb2YgQ29tcG9uZW50ID09PSAnZnVuY3Rpb24nICYmICEoMCwgX2lzQ2xhc3NDb21wb25lbnQyLmRlZmF1bHQpKENvbXBvbmVudCkgJiYgIUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgJiYgIUNvbXBvbmVudC5jb250ZXh0VHlwZXMgJiYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicgfHwgIUNvbXBvbmVudC5wcm9wVHlwZXMpKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5ID0gcmVxdWlyZSgnLi9jcmVhdGVFYWdlckZhY3RvcnknKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJGYWN0b3J5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSBmdW5jdGlvbiBjcmVhdGVFYWdlckVsZW1lbnRVdGlsKGhhc0tleSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHByb3BzLCBjaGlsZHJlbikge1xuICBpZiAoIWhhc0tleSAmJiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCkge1xuICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgcmV0dXJuIHR5cGUoX2V4dGVuZHMoe30sIHByb3BzLCB7IGNoaWxkcmVuOiBjaGlsZHJlbiB9KSk7XG4gICAgfVxuICAgIHJldHVybiB0eXBlKHByb3BzKTtcbiAgfVxuXG4gIHZhciBDb21wb25lbnQgPSB0eXBlO1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgIENvbXBvbmVudCxcbiAgICAgIHByb3BzLFxuICAgICAgY2hpbGRyZW5cbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgcHJvcHMpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRWFnZXJFbGVtZW50VXRpbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9nZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vZ2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9nZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciB3cmFwRGlzcGxheU5hbWUgPSBmdW5jdGlvbiB3cmFwRGlzcGxheU5hbWUoQmFzZUNvbXBvbmVudCwgaG9jTmFtZSkge1xuICByZXR1cm4gaG9jTmFtZSArICcoJyArICgwLCBfZ2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpICsgJyknO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd3JhcERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCB7XG4gIEVMRU1FTlRfR0VUX1NUQVJULFxuICBFTEVNRU5UX0dFVF9FUlJPUixcbiAgRUxFTUVOVF9HRVRfU1VDQ0VTUyxcbn0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL2VsZW1lbnRzJztcblxuaW1wb3J0IGFwaUdldEJ5SWQgZnJvbSAnLi4vLi4vYXBpL2VsZW1lbnRzL2dldEJ5SWQnO1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0RWxlbWVudFN0YXJ0ID0gKHBheWxvYWQpID0+ICh7IHR5cGU6IEVMRU1FTlRfR0VUX1NUQVJULCBwYXlsb2FkIH0pO1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnVzZXJuYW1lXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5lbGVtZW50VHlwZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY2lyY2xlVHlwZSAtIG9ubHkgaWYgZWxlbWVudFR5cGUgaXMgJ2NpcmNsZSdcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLm5hbWUgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuYXZhdGFyIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNvdmVyIC1cbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0RWxlbWVudFN1Y2Nlc3MgPSAocGF5bG9hZCkgPT4gKHsgdHlwZTogRUxFTUVOVF9HRVRfU1VDQ0VTUywgcGF5bG9hZCB9KTtcblxuLyoqXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5zdGF0dXNDb2RlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5lcnJvclxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5jb25zdCBnZXRFbGVtZW50RXJyb3IgPSAocGF5bG9hZCkgPT4gKHsgdHlwZTogRUxFTUVOVF9HRVRfRVJST1IsIHBheWxvYWQgfSk7XG5cbi8qKlxuICpcbiAqIEBmdW5jdGlvbiBnZXRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqL1xuY29uc3QgZ2V0ID0gKHsgdG9rZW4sIGlkIH0pID0+IChkaXNwYXRjaCkgPT4ge1xuICBkaXNwYXRjaChnZXRFbGVtZW50U3RhcnQoeyBpZCB9KSk7XG5cbiAgYXBpR2V0QnlJZCh7IHRva2VuLCBpZCB9KVxuICAgIC50aGVuKCh7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0pID0+IHtcbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICBkaXNwYXRjaChnZXRFbGVtZW50RXJyb3IoeyBpZCwgc3RhdHVzQ29kZSwgZXJyb3IgfSkpO1xuXG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgZGlzcGF0Y2goZ2V0RWxlbWVudFN1Y2Nlc3MoeyBpZCwgc3RhdHVzQ29kZSwgLi4ucGF5bG9hZCB9KSk7XG4gICAgfSlcbiAgICAuY2F0Y2goKHsgc3RhdHVzQ29kZSwgZXJyb3IgfSkgPT4ge1xuICAgICAgZGlzcGF0Y2goZ2V0RWxlbWVudEVycm9yKHsgaWQsIHN0YXR1c0NvZGUsIGVycm9yIH0pKTtcbiAgICB9KTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGdldDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9lbGVtZW50cy9nZXRCeUlkLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IFBPU1RfVVBEQVRFIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL3Bvc3RzJztcblxudHlwZSBQYXlsb2FkID0ge1xuICBwb3N0SWQ6IG51bWJlcixcbiAgdGl0bGU/OiBzdHJpbmcsXG4gIGRlc2NyaXB0aW9uPzogc3RyaW5nLFxuICBkYXRhPzogYW55LFxuICBjb3Zlcj86IHN0cmluZyxcbn07XG5cbnR5cGUgQWN0aW9uID0ge1xuICB0eXBlOiBzdHJpbmcsXG4gIHBheWxvYWQ6IFBheWxvYWQsXG59O1xuXG4vKipcbiAqXG4gKiBAZnVuY3Rpb24gdXBkYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZGVzY3JpcHRpb24gLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuZGF0YSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jb3ZlciAtXG4gKlxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgdXBkYXRlID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHsgdHlwZTogUE9TVF9VUERBVEUsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9wb3N0cy91cGRhdGUuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vY29uZmlnJztcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uIGdldEJ5SWRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QnlJZCh7IHRva2VuLCBpZCB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL2VsZW1lbnRzLyR7aWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRCeUlkO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvZWxlbWVudHMvZ2V0QnlJZC5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5cbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHRva2VuOiBzdHJpbmcsXG4gIHVzZXJJZDogbnVtYmVyLFxuICBwb3N0SWQ6IG51bWJlcixcbiAgZm9ybURhdGE6IGFueSxcbn07XG5cbnR5cGUgU3VjY2VzcyA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBtZXNzYWdlOiBzdHJpbmcsXG4gIHBheWxvYWQ6IGFueSxcbn07XG5cbnR5cGUgRXJyb3JSZXNwb25zZSA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBlcnJvcjogc3RyaW5nLFxufTtcblxuLyoqXG4gKlxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb25cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmZvcm1EYXRhXG4gKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5leHBvcnQgZGVmYXVsdCBhc3luYyBmdW5jdGlvbiBjcmVhdGUoe1xuICB0b2tlbixcbiAgdXNlcklkLFxuICBwb3N0SWQsXG4gIGZvcm1EYXRhLFxufTogUGF5bG9hZCk6IFByb21pc2U8U3VjY2VzcyB8IEVycm9yUmVzcG9uc2U+IHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L3Bvc3RzLyR7cG9zdElkfS9waG90b3NgO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIC8vICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IGZvcm1EYXRhLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9waG90b3MvcG9zdHMvdXNlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmlsZVxuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gdXBkYXRlIGEgcG9zdFxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gdXBkYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jaXJjbGVJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZGVzY3JpcHRpb24gLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuZGF0YSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jb3ZlciAtXG4gKiBAcGFyYW0ge0Jvb2xlYW59IHBheWxvYWQucHJpdmFjeSAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gdXBkYXRlKHtcbiAgdXNlcklkLFxuICB0b2tlbixcbiAgcG9zdElkLFxuICB0aXRsZSxcbiAgZGVzY3JpcHRpb24sXG4gIGRhdGEsXG4gIGNvdmVyLFxuICBwcml2YWN5LFxufSkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vcG9zdHMvJHtwb3N0SWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUFVUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIC8vIGhlcmUgaWYgYW55IGtleSAodGl0bGUsIGRlc2NyaXB0aW9uIG9yIGRhdGEpIGlzIHVuZGVmaW5lZFxuICAgICAgLy8gSlNPTi5zdHJpbmdpZnkoKSB3aWxsIG5vdCBjb252ZXJ0IGl0IHRvIHN0cmluZyBhbmQgd2lsbCByZW1vdmVkIGl0LlxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoeyB0aXRsZSwgZGVzY3JpcHRpb24sIGRhdGEsIGNvdmVyLCBwcml2YWN5IH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3Bvc3RzL3VzZXJzL3VwZGF0ZS5qcyIsIi8qKlxuICogRGVzY3JpcHRpb24gY29tcG9uZW50IGlzIGZvciBlZGl0YWJsZSBjb21wb25lbnQgdG8gZWRpdCBkZXNjcmlwdGlvblxuICogd2l0aCB0ZXh0YXJlYS5cbiAqIFRoaXMgY29tcG9uZW50IHNob3VsZCBiZSB1c2VkIHdpdGggQ2FyZEhlYWRlciBzdWJoZWFkZXIgcGxhY2UuXG4gKiBUT0RPOiBtYWtlIHRleHRhcmVhIGF1dG9yZXNpemFibGUuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICB2YWx1ZToge30sXG4gIHRleHRhcmVhOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICBib3JkZXI6ICdub25lJyxcbiAgICAvLyBvdXRsaW5lOiAnbm9uZScsXG4gICAgcmVzaXplOiAnbm9uZScsXG4gICAgZm9udDogJ2luaGVyaXQnLFxuICB9LFxufTtcblxuY29uc3QgRGVzY3JpcHRpb24gPSAoe1xuICBjbGFzc2VzLFxuICByZWFkT25seSxcbiAgcGxhY2Vob2xkZXIsXG4gIHZhbHVlLFxuICBvbkNoYW5nZSxcbiAgbWluTGVuZ3RoLFxuICBtYXhMZW5ndGgsXG59KSA9PiB7XG4gIGlmIChyZWFkT25seSkge1xuICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy52YWx1ZX0+e3ZhbHVlfTwvZGl2PjtcbiAgfVxuXG4gIHJldHVybiAoXG4gICAgPHRleHRhcmVhXG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMudGV4dGFyZWF9XG4gICAgICBtaW5MZW5ndGg9e21pbkxlbmd0aCB8fCAxfVxuICAgICAgbWF4TGVuZ3RoPXttYXhMZW5ndGggfHwgMzAwfVxuICAgICAgcm93cz17MX1cbiAgICAgIHJlYWRPbmx5PXtyZWFkT25seX1cbiAgICAvPlxuICApO1xufTtcblxuRGVzY3JpcHRpb24ucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIG1pbkxlbmd0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgbWF4TGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuRGVzY3JpcHRpb24uZGVmYXVsdFByb3BzID0ge1xuICBwbGFjZWhvbGRlcjogJ0Rlc2NyaXB0aW9uIGdvZXMgaGVyZS4uLicsXG4gIHJlYWRPbmx5OiB0cnVlLFxuICBtaW5MZW5ndGg6IDEsXG4gIG1heExlbmd0aDogMzAwLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKERlc2NyaXB0aW9uKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9EZXNjcmlwdGlvbi5qcyIsIi8qKlxuICogVGl0bGUgY29tcG9uZW50IGlzIGZvciBlZGl0YWJsZSBjb21wb25lbnQgdG8gZWRpdCB0aXRsZSB3aXRoIHRleHRhcmVhLlxuICogVGhpcyBjb21wb25lbnQgc2hvdWxkIGJlIHVzZWQgd2l0aCBDYXJkSGVhZGVyIHRpdGxlIHBsYWNlLlxuICogVE9ETzogbWFrZSB0ZXh0YXJlYSBhdXRvcmVzaXphYmxlLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHsgd2l0aFN0eWxlcyB9IGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgdmFsdWU6IHt9LFxuICB0ZXh0YXJlYToge1xuICAgIHdpZHRoOiAnMTAwJScsXG4gICAgaGVpZ2h0OiAnYXV0bycsXG4gICAgYm9yZGVyOiAnbm9uZScsXG4gICAgLy8gb3V0bGluZTogJ25vbmUnLFxuICAgIHJlc2l6ZTogJ25vbmUnLFxuICAgIGZvbnQ6ICdpbmhlcml0JyxcbiAgfSxcbn07XG5cbmNvbnN0IFRpdGxlID0gKHtcbiAgY2xhc3NlcyxcbiAgcmVhZE9ubHksXG4gIHZhbHVlLFxuICBwbGFjZWhvbGRlcixcbiAgb25DaGFuZ2UsXG4gIG1pbkxlbmd0aCxcbiAgbWF4TGVuZ3RoLFxufSkgPT4ge1xuICBpZiAocmVhZE9ubHkpIHtcbiAgICByZXR1cm4gPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMudmFsdWV9Pnt2YWx1ZX08L2Rpdj47XG4gIH1cblxuICByZXR1cm4gKFxuICAgIDx0ZXh0YXJlYVxuICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnRleHRhcmVhfVxuICAgICAgbWluTGVuZ3RoPXttaW5MZW5ndGggfHwgMX1cbiAgICAgIG1heExlbmd0aD17bWF4TGVuZ3RoIHx8IDE0OH1cbiAgICAgIHJvd3M9ezF9XG4gICAgICByZWFkT25seT17cmVhZE9ubHl9XG4gICAgLz5cbiAgKTtcbn07XG5cblRpdGxlLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICByZWFkT25seTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgdmFsdWU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIG1pbkxlbmd0aDogUHJvcFR5cGVzLm51bWJlcixcbiAgbWF4TGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxufTtcblxuVGl0bGUuZGVmYXVsdFByb3BzID0ge1xuICB2YWx1ZTogJ1VudGl0bGVkJyxcbiAgcGxhY2Vob2xkZXI6ICdUaXRsZSBnb2VzIGhlcmUuLi4nLFxuICByZWFkT25seTogdHJ1ZSxcbiAgbWluTGVuZ3RoOiAxLFxuICBtYXhMZW5ndGg6IDE0OCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShUaXRsZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvVGl0bGUuanMiLCIvKipcbiAqIFRoaXMgY29tcG9uZW50IGlzIGNyZWF0ZWQgdG8gZWRpdCBjb250ZW50IG9mIHRoZSBhcnRpY2xlXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgTG9hZGFibGUgZnJvbSAncmVhY3QtbG9hZGFibGUnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7XG4gIENhcmRIZWFkZXIsXG4gIENhcmRNZWRpYSxcbiAgQ2FyZENvbnRlbnQsXG4gIENhcmRBY3Rpb25zLFxufSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IEF2YXRhciBmcm9tICdtYXRlcmlhbC11aS9BdmF0YXInO1xuaW1wb3J0IFBhcGVyIGZyb20gJ21hdGVyaWFsLXVpL1BhcGVyJztcblxuaW1wb3J0IEZhdm9yaXRlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZSc7XG5pbXBvcnQgU2hhcmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NoYXJlJztcbmltcG9ydCBUdXJuZWRJbkljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVHVybmVkSW4nO1xuaW1wb3J0IE1vcmVWZXJ0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydCc7XG5cbmltcG9ydCBFZGl0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9FZGl0JztcbmltcG9ydCBTYXZlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9TYXZlJztcbmltcG9ydCBDb21tZW50SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Db21tZW50JztcbmltcG9ydCBCYWRnZSBmcm9tICdtYXRlcmlhbC11aS9CYWRnZSc7XG5pbXBvcnQgQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0J1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcbmltcG9ydCB7IGNvbnZlcnRUb1JhdyB9IGZyb20gJ2RyYWZ0LWpzJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcbmltcG9ydCBNYXlhc2hFZGl0b3IgZnJvbSAnLi4vLi4vLi4vbGliL21heWFzaC1lZGl0b3IvRWRpdG9yJztcbmltcG9ydCB7IGNyZWF0ZUVkaXRvclN0YXRlIH0gZnJvbSAnLi4vLi4vLi4vbGliL21heWFzaC1lZGl0b3InO1xuXG5pbXBvcnQgVGl0bGUgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9UaXRsZSc7XG5pbXBvcnQgRGVzY3JpcHRpb24gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9EZXNjcmlwdGlvbic7XG5pbXBvcnQgQ2FyZE1lZGlhQ29tcG9uZW50IGZyb20gJy4vQ2FyZE1lZGlhQ29tcG9uZW50JztcbmltcG9ydCBQcml2YWN5IGZyb20gJy4uL1ByaXZhY3kvaW5kZXgnO1xuaW1wb3J0IEVycm9yUGFnZSBmcm9tICcuLi8uLi9jb21wb25lbnRzL0Vycm9yUGFnZSc7XG5cbmltcG9ydCBhcGlQb3N0VXBkYXRlIGZyb20gJy4uLy4uL2FwaS9wb3N0cy91c2Vycy91cGRhdGUnO1xuaW1wb3J0IGFwaVBob3RvVXNlckNyZWF0ZSBmcm9tICcuLi8uLi9hcGkvcGhvdG9zL3Bvc3RzL3VzZXJzL2NyZWF0ZSc7XG5cbmltcG9ydCBhY3Rpb25FbGVtZW50R2V0QnlJZCBmcm9tICcuLi8uLi9hY3Rpb25zL2VsZW1lbnRzL2dldEJ5SWQnO1xuaW1wb3J0IGFjdGlvblBvc3RVcGRhdGUgZnJvbSAnLi4vLi4vYWN0aW9ucy9wb3N0cy91cGRhdGUnO1xuXG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vQXJ0aWNsZVN0eWxlcyc7XG5cbmNvbnN0IEFzeW5jQ29tbWVudCA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4uL0NvbW1lbnQvaW5kZXgnKSxcbiAgbW9kdWxlczogWycuLi9Db21tZW50L2luZGV4J10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY2xhc3MgQXJ0aWNsZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIGNvbnN0IHsgdGl0bGUsIGRlc2NyaXB0aW9uLCBkYXRhIH0gPSBwcm9wcy5wb3N0O1xuXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGVkaXQ6IGZhbHNlLFxuICAgICAgdGl0bGUsXG4gICAgICBkZXNjcmlwdGlvbixcbiAgICAgIGRhdGE6IGNyZWF0ZUVkaXRvclN0YXRlKGRhdGEpLFxuICAgICAgY29tbWVudEJveDogZmFsc2UsXG4gICAgfTtcblxuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkVkaXQgPSB0aGlzLm9uRWRpdC5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25TYXZlID0gdGhpcy5vblNhdmUuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uQ2xpY2tDb21tZW50SWNvbiA9IHRoaXMub25DbGlja0NvbW1lbnRJY29uLmJpbmQodGhpcyk7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBjb25zdCB7IGVsZW1lbnRzLCBwb3N0IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkOiB1c2VySWQgfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBhdXRob3JJZCB9ID0gcG9zdDtcblxuICAgIGxldCBhdXRob3I7XG4gICAgaWYgKGF1dGhvcklkID09PSB1c2VySWQpIHtcbiAgICAgIGF1dGhvciA9IGVsZW1lbnRzLnVzZXI7XG4gICAgfSBlbHNlIHtcbiAgICAgIGF1dGhvciA9IGVsZW1lbnRzW2F1dGhvcklkXTtcbiAgICB9XG5cbiAgICBpZiAoIWF1dGhvciB8fCAoIWF1dGhvci5pc0ZldGNoaW5nICYmICFhdXRob3IuaXNGZXRjaGVkKSkge1xuICAgICAgdGhpcy5wcm9wcy5hY3Rpb25FbGVtZW50R2V0QnlJZCh7IHRva2VuLCBpZDogYXV0aG9ySWQgfSk7XG4gICAgfVxuICB9XG5cbiAgLy8gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gIC8vICAgcmV0dXJuIHRoaXMuc3RhdGUgIT09IG5leHRTdGF0ZSB8fCB0aGlzLnByb3BzICE9PSBuZXh0UHJvcHM7XG4gIC8vIH1cblxuICAvLyBUaGlzIG9uIGNoYW5nZSBmdW5jdGlvbiBpcyBmb3IgTWF5YXNoRWRpdG9yXG4gIG9uQ2hhbmdlKGRhdGEpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgZGF0YSB9KTtcbiAgfVxuXG4gIC8vIGl0IGNoYW5nZXMgdGhlIGN1cnJlbnQgZWRpdGluZyBzdGF0ZVxuICBvbkVkaXQoKSB7XG4gICAgY29uc3QgeyBlZGl0IH0gPSB0aGlzLnN0YXRlO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBlZGl0OiAhZWRpdCB9KTtcbiAgfVxuICBhc3luYyBvblNhdmUoKSB7XG4gICAgY29uc3QgeyBlZGl0LCB0aXRsZSwgZGVzY3JpcHRpb24sIGRhdGEgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICBjb25zdCB7IGVsZW1lbnRzLCBwb3N0IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBwb3N0SWQsIHRpdGxlOiB0aXRsZVByb3AsIGRlc2NyaXB0aW9uOiBkZXNjcmlwdGlvblByb3AgfSA9IHBvc3Q7XG5cbiAgICBsZXQgYm9keSA9IHt9O1xuXG4gICAgaWYgKHRpdGxlICE9PSB0aXRsZVByb3ApIHtcbiAgICAgIGJvZHkgPSB7IC4uLmJvZHksIHRpdGxlIH07XG4gICAgfVxuXG4gICAgaWYgKFxuICAgICAgdHlwZW9mIGRlc2NyaXB0aW9uICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgZGVzY3JpcHRpb24ubGVuZ3RoICYmXG4gICAgICBkZXNjcmlwdGlvbiAhPT0gZGVzY3JpcHRpb25Qcm9wXG4gICAgKSB7XG4gICAgICBib2R5ID0geyAuLi5ib2R5LCBkZXNjcmlwdGlvbiB9O1xuICAgIH1cblxuICAgIGJvZHkuZGF0YSA9IGNvbnZlcnRUb1JhdyhkYXRhLmdldEN1cnJlbnRDb250ZW50KCkpO1xuXG4gICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciB9ID0gYXdhaXQgYXBpUG9zdFVwZGF0ZSh7XG4gICAgICB1c2VySWQsXG4gICAgICBwb3N0SWQsXG4gICAgICB0b2tlbixcbiAgICAgIC4uLmJvZHksXG4gICAgfSk7XG5cbiAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgIC8vIGhhbmRsZSBlcnJvcjtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMucHJvcHMuYWN0aW9uUG9zdFVwZGF0ZSh7IHBvc3RJZCwgLi4uYm9keSB9KTtcblxuICAgIC8vIHdoZW4gZWRpdGluZyBpcyBkb25lIHNldCAnZWRpdCcgdG8gZmFsc2VcbiAgICB0aGlzLnNldFN0YXRlKHsgZWRpdDogIWVkaXQgfSk7XG4gIH1cblxuICBvbkNsaWNrQ29tbWVudEljb24oKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGNvbW1lbnRCb3g6IHRydWUgfSk7XG4gIH1cblxuICBvbk1vdXNlRW50ZXIgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IHRydWUgfSk7XG4gIG9uTW91c2VMZWF2ZSA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogZmFsc2UgfSk7XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgcG9zdCwgZWxlbWVudHMgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyB1c2VyIH0gPSBlbGVtZW50cztcbiAgICBjb25zdCB7IGhvdmVyLCBlZGl0LCB0aXRsZSwgZGVzY3JpcHRpb24sIGRhdGEsIGNvbW1lbnRCb3ggfSA9IHRoaXMuc3RhdGU7XG5cbiAgICBjb25zdCB7IGlzU2lnbmVkSW4sIHRva2VuLCBpZDogdXNlcklkIH0gPSB1c2VyO1xuICAgIGNvbnN0IHsgYXV0aG9ySWQsIHBvc3RJZCwgY292ZXIsIGNvbW1lbnRzLCBwcml2YWN5IH0gPSBwb3N0O1xuXG4gICAgbGV0IGNvbW1lbnRzU2l6ZTtcblxuICAgIGlmIChjb21tZW50cyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBjb21tZW50c1NpemUgPSBPYmplY3Qua2V5cyhjb21tZW50cykubGVuZ3RoO1xuICAgIH1cblxuICAgIGxldCBhdXRob3IgPSB7IGF1dGhvcklkIH07XG5cbiAgICBpZiAodXNlcklkID09PSBhdXRob3JJZCkge1xuICAgICAgYXV0aG9yID0geyAuLi5hdXRob3IsIC4uLnVzZXIgfTtcbiAgICB9IGVsc2Uge1xuICAgICAgYXV0aG9yID0geyAuLi5hdXRob3IsIC4uLmVsZW1lbnRzW2F1dGhvcklkXSB9O1xuICAgIH1cblxuICAgIGlmIChhdXRob3JJZCAhPT0gdXNlcklkICYmIHByaXZhY3kpIHJldHVybiA8RXJyb3JQYWdlIHN0YXR1c0NvZGU9ezQwMX0gLz47XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEdyaWQgY29udGFpbmVyIGp1c3RpZnk9XCJjZW50ZXJcIiBzcGFjaW5nPXswfSBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBzbT17MTF9IG1kPXs4fSBsZz17OH0geGw9ezh9PlxuICAgICAgICAgIDxDYXJkXG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgICAgICAgIHJhaXNlZD17aG92ZXJ9XG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgICBhdmF0YXI9e1xuICAgICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7YXV0aG9yLnVzZXJuYW1lfWB9PlxuICAgICAgICAgICAgICAgICAgICA8QXZhdGFyXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICAgICAgYXV0aG9yLmF2YXRhciB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZydcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9QYXBlcj5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7YXV0aG9yLnVzZXJuYW1lfWB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5uYW1lfT5cbiAgICAgICAgICAgICAgICAgIHthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgc3ViaGVhZGVyPXtcbiAgICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnVzZXJuYW1lfT5cbiAgICAgICAgICAgICAgICAgIEB7YXV0aG9yLnVzZXJuYW1lfVxuICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBhY3Rpb249e1xuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXh9PlxuICAgICAgICAgICAgICAgICAgPFByaXZhY3lcbiAgICAgICAgICAgICAgICAgICAgcHJpdmFjeT17cHJpdmFjeX1cbiAgICAgICAgICAgICAgICAgICAgcG9zdElkPXtwb3N0SWR9XG4gICAgICAgICAgICAgICAgICAgIGF1dGhvcklkID0ge2F1dGhvcklkfVxuICAgICAgICAgICAgICAgICAgICB0b2tlbj17dG9rZW59XG4gICAgICAgICAgICAgICAgICAgIHVzZXJJZD17dXNlcklkfVxuICAgICAgICAgICAgICAgICAgICBhY3Rpb25Qb3N0VXBkYXRlPXt0aGlzLnByb3BzLmFjdGlvblBvc3RVcGRhdGV9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJDb21pbmcgc29vblwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgICA8TW9yZVZlcnRJY29uIC8+XG4gICAgICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIHsvKiBkb24ndCBjaGFuZ2UgdGhpcyBjb21wb25lbnQgZm9yIG5vdyAqL31cbiAgICAgICAgICAgIHt1c2VySWQgPT09IGF1dGhvcklkID8gKFxuICAgICAgICAgICAgICA8Q2FyZE1lZGlhXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLm1lZGlhfVxuICAgICAgICAgICAgICAgIGNvbXBvbmVudD17KHRoaXNQcm9wKSA9PiAoXG4gICAgICAgICAgICAgICAgICA8Q2FyZE1lZGlhQ29tcG9uZW50XG4gICAgICAgICAgICAgICAgICAgIHsuLi50aGlzUHJvcH1cbiAgICAgICAgICAgICAgICAgICAgey4uLnBvc3R9XG4gICAgICAgICAgICAgICAgICAgIHRva2VuPXt0b2tlbn1cbiAgICAgICAgICAgICAgICAgICAgYWN0aW9uUG9zdFVwZGF0ZT17dGhpcy5wcm9wcy5hY3Rpb25Qb3N0VXBkYXRlfVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgIGltYWdlPXtcbiAgICAgICAgICAgICAgICAgIGNvdmVyIHx8XG4gICAgICAgICAgICAgICAgICAnaHR0cDovL2dpbnZhLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNi8wOC9naW52YV8yMDE2LTA4LTAyXzAyLTMwLTU0LmpwZydcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICA8Q2FyZE1lZGlhXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLm1lZGlhfVxuICAgICAgICAgICAgICAgIGltYWdlPXtcbiAgICAgICAgICAgICAgICAgIGNvdmVyIHx8XG4gICAgICAgICAgICAgICAgICAnaHR0cDovL2dpbnZhLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNi8wOC9naW52YV8yMDE2LTA4LTAyXzAyLTMwLTU0LmpwZydcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICApfVxuICAgICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICAgIDxUaXRsZVxuICAgICAgICAgICAgICAgICAgcmVhZE9ubHk9eyFlZGl0fVxuICAgICAgICAgICAgICAgICAgdmFsdWU9e3RpdGxlfVxuICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiB0aGlzLnNldFN0YXRlKHsgdGl0bGU6IGUudGFyZ2V0LnZhbHVlIH0pfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgc3ViaGVhZGVyPXtcbiAgICAgICAgICAgICAgICBlZGl0IHx8IHR5cGVvZiBkZXNjcmlwdGlvbiAhPT0gJ3VuZGVmaW5lZCcgPyAoXG4gICAgICAgICAgICAgICAgICA8RGVzY3JpcHRpb25cbiAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk9eyFlZGl0fVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17ZGVzY3JpcHRpb259XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT5cbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlc2NyaXB0aW9uOiBlLnRhcmdldC52YWx1ZSxcbiAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICkgOiBudWxsXG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgICB7ZWRpdCB8fCB0eXBlb2YgcG9zdC5kYXRhICE9PSAndW5kZWZpbmVkJyA/IChcbiAgICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICAgIDxNYXlhc2hFZGl0b3JcbiAgICAgICAgICAgICAgICAgIGVkaXRvclN0YXRlPXtkYXRhfVxuICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2V9XG4gICAgICAgICAgICAgICAgICByZWFkT25seT17IWVkaXR9XG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17J0NvbnRlbnQgZ29lcyBoZXJlLi4uJ31cbiAgICAgICAgICAgICAgICAgIGFwaVBob3RvVXBsb2FkPXsoeyBmb3JtRGF0YSB9KSA9PlxuICAgICAgICAgICAgICAgICAgICBhcGlQaG90b1VzZXJDcmVhdGUoe1xuICAgICAgICAgICAgICAgICAgICAgIHRva2VuLFxuICAgICAgICAgICAgICAgICAgICAgIHVzZXJJZCxcbiAgICAgICAgICAgICAgICAgICAgICBwb3N0SWQsXG4gICAgICAgICAgICAgICAgICAgICAgZm9ybURhdGEsXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMubWFyZ2lufSAvPlxuICAgICAgICAgICAgPENhcmRBY3Rpb25zPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWRkIHRvIGZhdm9yaXRlc1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGYXZvcml0ZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQm9vayBNYXJrc1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxUdXJuZWRJbkljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJDb21tZW50XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tDb21tZW50SWNvbn1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxCYWRnZSBiYWRnZUNvbnRlbnQ9e2NvbW1lbnRzU2l6ZX0+XG4gICAgICAgICAgICAgICAgICA8Q29tbWVudEljb24gLz5cbiAgICAgICAgICAgICAgICA8L0JhZGdlPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIlNoYXJlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlNoYXJlXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgICA8U2hhcmVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICB7Y29tbWVudEJveCA/IDxBc3luY0NvbW1lbnQgcG9zdElkPXtwb3N0SWR9IC8+IDogbnVsbH1cbiAgICAgICAgPC9HcmlkPlxuICAgICAgICB7aXNTaWduZWRJbiAmJlxuICAgICAgICAgIGF1dGhvcklkID09PSB1c2VySWQgJiYgKFxuICAgICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAgICBmYWJcbiAgICAgICAgICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiZWRpdFwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5lZGl0QnV0dG9ufVxuICAgICAgICAgICAgICBvbkNsaWNrPXtlZGl0ID8gdGhpcy5vblNhdmUgOiB0aGlzLm9uRWRpdH1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAge2VkaXQgPyA8U2F2ZUljb24gLz4gOiA8RWRpdEljb24gLz59XG4gICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICApfVxuICAgICAgPC9HcmlkPlxuICAgICk7XG4gIH1cbn1cblxuQXJ0aWNsZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBlbGVtZW50czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBwb3N0OiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gICAgZGVzY3JpcHRpb246IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZGF0YTogUHJvcFR5cGVzLm9iamVjdCxcbiAgfSksXG5cbiAgYWN0aW9uRWxlbWVudEdldEJ5SWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGFjdGlvblBvc3RVcGRhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cyB9KSA9PiAoeyBlbGVtZW50cyB9KTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PlxuICBiaW5kQWN0aW9uQ3JlYXRvcnMoXG4gICAge1xuICAgICAgYWN0aW9uRWxlbWVudEdldEJ5SWQsXG4gICAgICBhY3Rpb25Qb3N0VXBkYXRlLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoQXJ0aWNsZSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Bvc3RQYWdlL0FydGljbGUuanMiLCIvKipcbiAqIFRoaXMgZmlsZSBjb250YWlucyBhbGwgdGhlIHN0eWxlcyBmb3IgQXJ0aWNsZSBDb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIG5hbWU6IHtcbiAgICAvLyBjb2xvcjogJyMzNjU4OTknLFxuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICB1c2VybmFtZToge1xuICAgIC8vIGNvbG9yOiAnIzM2NTg5OScsXG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgbWVkaWE6IHtcbiAgICBoZWlnaHQ6IDE5NCxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBlZGl0QnV0dG9uOiB7XG4gICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgekluZGV4OiAxLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHtcbiAgICAgIGJvdHRvbTogJzQwcHgnLFxuICAgICAgcmlnaHQ6ICc0MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge1xuICAgICAgYm90dG9tOiAnNDBweCcsXG4gICAgICByaWdodDogJzQwcHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBib3R0b206ICczMHB4JyxcbiAgICAgIHJpZ2h0OiAnMzBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIGRpc3BsYXk6ICdub25lJyxcbiAgICAgIGJvdHRvbTogJzEwcHgnLFxuICAgICAgcmlnaHQ6ICcxMHB4JyxcbiAgICB9LFxuICB9LFxuICBtYXJnaW46IHtcbiAgICBib3JkZXI6ICcwLjVweCBzb2xpZCByZ2JhKDIwOSwyMDksMjA5LDEpJyxcbiAgICBtYXJnaW5Cb3R0b206ICctMTBweCcsXG4gIH0sXG4gIGZsZXg6IHtcbiAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgZmxleERpcmVjdGlvbjogJ3JvdycsXG4gIH0sXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGVzO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Bvc3RQYWdlL0FydGljbGVTdHlsZXMuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgSW5zZXJ0UGhvdG9JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvJztcblxuaW1wb3J0IGFwaVBvc3RVcGRhdGUgZnJvbSAnLi4vLi4vLi4vYXBpL3Bvc3RzL3VzZXJzL3VwZGF0ZSc7XG5pbXBvcnQgYXBpUGhvdG9VcGxvYWQgZnJvbSAnLi4vLi4vLi4vYXBpL3Bob3Rvcy9wb3N0cy91c2Vycy9jcmVhdGUnO1xuXG5jbGFzcyBDb3ZlckltYWdlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBzdHlsZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcblxuICAgIHRoaXMub25DbGljayA9IHRoaXMub25DbGljay5iaW5kKHRoaXMpO1xuICAgIC8vIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gIH1cblxuICBvbkNsaWNrKCkge1xuICAgIHRoaXMucGhvdG8udmFsdWUgPSBudWxsO1xuICAgIHRoaXMucGhvdG8uY2xpY2soKTtcbiAgfVxuXG4gIG9uQ2hhbmdlID0gYXN5bmMgKGUpID0+IHtcbiAgICBpZiAoZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH1cblxuICAgIGNvbnN0IGZpbGUgPSBlLnRhcmdldC5maWxlc1swXTtcblxuICAgIGlmIChmaWxlLnR5cGUuaW5kZXhPZignaW1hZ2UvJykgPT09IDApIHtcbiAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICBmb3JtRGF0YS5hcHBlbmQoJ3Bob3RvJywgZmlsZSk7XG5cbiAgICAgIGNvbnN0IHsgYXV0aG9ySWQ6IHVzZXJJZCwgcG9zdElkLCB0b2tlbiB9ID0gdGhpcy5wcm9wcztcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpUGhvdG9VcGxvYWQoe1xuICAgICAgICB0b2tlbixcbiAgICAgICAgdXNlcklkLFxuICAgICAgICBwb3N0SWQsXG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgRXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHsgcGhvdG9Vcmw6IGNvdmVyIH0gPSBwYXlsb2FkO1xuXG4gICAgICBhcGlQb3N0VXBkYXRlKHtcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIHVzZXJJZCxcbiAgICAgICAgcG9zdElkLFxuICAgICAgICBjb3ZlcixcbiAgICAgIH0pO1xuXG4gICAgICB0aGlzLnByb3BzLmFjdGlvblBvc3RVcGRhdGUoe1xuICAgICAgICBwb3N0SWQsXG4gICAgICAgIGNvdmVyLFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBzdHlsZT17e1xuICAgICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgICAgIC4uLnRoaXMucHJvcHMuc3R5bGUsXG4gICAgICAgIH19XG4gICAgICAgIGNsYXNzTmFtZT17dGhpcy5wcm9wcy5jbGFzc05hbWV9XG4gICAgICA+XG4gICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgUGhvdG9cIiBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9PlxuICAgICAgICAgIDxJbnNlcnRQaG90b0ljb24gLz5cbiAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcbiAgICAgICAgICAgIGFjY2VwdD1cImltYWdlL2pwZWd8cG5nfGdpZlwiXG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgIHJlZj17KHBob3RvKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMucGhvdG8gPSBwaG90bztcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBzdHlsZT17eyBkaXNwbGF5OiAnbm9uZScgfX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENvdmVySW1hZ2U7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdFBhZ2UvQ2FyZE1lZGlhQ29tcG9uZW50L2luZGV4LmpzIiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncmVhY3QvbGliL1JlYWN0UHJvcFR5cGVzJztcbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuaW1wb3J0IE1lbnUsIHsgTWVudUl0ZW0gfSBmcm9tICdtYXRlcmlhbC11aS9NZW51JztcblxuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgUHVibGljSWNvbiBmcm9tICcuLi8uLi8uLi9saWIvbWF5YXNoLWljb25zL1B1YmxpYyc7XG5pbXBvcnQgUHJpdmF0ZUljb24gZnJvbSAnLi4vLi4vLi4vbGliL21heWFzaC1pY29ucy9Qcml2YXRlJztcblxuaW1wb3J0IGFwaVBvc3RVcGRhdGUgZnJvbSAnLi4vLi4vYXBpL3Bvc3RzL3VzZXJzL3VwZGF0ZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge30sXG59O1xuXG5jbGFzcyBQcml2YWN5IGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgcHJpdmFjeTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgICB1c2VySWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgICBwb3N0SWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgICB0b2tlbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIGFjdGlvblBvc3RVcGRhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHsgYW5jaG9yRWw6IG51bGwsIHByaXZhY3k6IHByb3BzLnByaXZhY3kgfTtcbiAgICB0aGlzLm9uQ2hhbmdlUHJpdmFjeSA9IHRoaXMub25DaGFuZ2VQcml2YWN5LmJpbmQodGhpcyk7XG4gIH1cblxuICBvbkNsaWNrUHJpdmFjeSA9IChldmVudCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBhbmNob3JFbDogZXZlbnQuY3VycmVudFRhcmdldCB9KTtcbiAgfTtcblxuICBhc3luYyBvbkNoYW5nZVByaXZhY3koKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgcG9zdElkLCB1c2VySWQsIHRva2VuIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCBwcml2YWN5ID0gIXRoaXMuc3RhdGUucHJpdmFjeTtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBtZXNzYWdlIH0gPSBhd2FpdCBhcGlQb3N0VXBkYXRlKHtcbiAgICAgICAgdXNlcklkLFxuICAgICAgICB0b2tlbixcbiAgICAgICAgcG9zdElkLFxuICAgICAgICBwcml2YWN5LFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBwcml2YWN5IH0pO1xuICAgICAgdGhpcy5wcm9wcy5hY3Rpb25Qb3N0VXBkYXRlKHsgcG9zdElkLCBwcml2YWN5IH0pO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgIH1cbiAgfVxuXG4gIGhhbmRsZUNsb3NlID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBhbmNob3JFbDogbnVsbCB9KTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBhbmNob3JFbCwgcHJpdmFjeSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGNsYXNzZXMsYXV0aG9ySWQsdXNlcklkIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgYXJpYS1vd25zPXthbmNob3JFbCA/ICdjaGFuZ2UgcHJpdmFjeScgOiBudWxsfVxuICAgICAgICAgIGFyaWEtaGFzcG9wdXA9XCJmYWxzZVwiXG4gICAgICAgICAgb25DbGljaz17dGhpcy5vbkNsaWNrUHJpdmFjeX1cbiAgICAgICAgICBkaXNhYmxlZD17dXNlcklkICE9PSBhdXRob3JJZH1cbiAgICAgICAgPlxuICAgICAgICAgIHtwcml2YWN5ID8gPFByaXZhdGVJY29uIC8+IDogPFB1YmxpY0ljb24gLz59XG4gICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgPE1lbnVcbiAgICAgICAgICBpZD1cImNoYW5nZSBwcml2YWN5XCJcbiAgICAgICAgICBhbmNob3JFbD17YW5jaG9yRWx9XG4gICAgICAgICAgb3Blbj17Qm9vbGVhbihhbmNob3JFbCl9XG4gICAgICAgICAgUGFwZXJQcm9wcz17e1xuICAgICAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICAgICAgaGVpZ2h0OiA2NSxcbiAgICAgICAgICAgICAgd2lkdGg6IDY1LFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICB9fVxuICAgICAgICA+XG4gICAgICAgICAgPE1lbnVJdGVtIG9uQ2xpY2s9e3RoaXMuaGFuZGxlQ2xvc2V9PlxuICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgYXJpYS1vd25zPXthbmNob3JFbCA/ICdjaGFuZ2UgcHJpdmFjeScgOiBudWxsfVxuICAgICAgICAgICAgICBhcmlhLWhhc3BvcHVwPVwiZmFsc2VcIlxuICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2hhbmdlUHJpdmFjeX1cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgeyFwcml2YWN5ID8gPFByaXZhdGVJY29uIC8+IDogPFB1YmxpY0ljb24gLz59XG4gICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgPC9NZW51SXRlbT5cbiAgICAgICAgPC9NZW51PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoUHJpdmFjeSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUHJpdmFjeS9pbmRleC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG4vLyBpbXBvcnQgSWNvbiBmcm9tICdtYXRlcmlhbC11aS9JY29uJztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IFRvb2x0aXAgZnJvbSAnbWF0ZXJpYWwtdWkvVG9vbHRpcCc7XG5cbmltcG9ydCBUaXRsZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUnO1xuaW1wb3J0IEZvcm1hdEJvbGRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQnO1xuaW1wb3J0IEZvcm1hdEl0YWxpYyBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMnO1xuaW1wb3J0IEZvcm1hdFVuZGVybGluZWRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQnO1xuaW1wb3J0IEZvcm1hdFF1b3RlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZSc7XG4vLyBpbXBvcnQgRm9ybWF0Q2xlYXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENsZWFyJztcbi8vIGltcG9ydCBGb3JtYXRDb2xvckZpbGxJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENvbG9yRmlsbCc7XG4vLyBpbXBvcnQgRm9ybWF0Q29sb3JSZXNldEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q29sb3JSZXNldCc7XG4vLyBpbXBvcnQgRm9ybWF0Q29sb3JUZXh0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRDb2xvclRleHQnO1xuLy8gaW1wb3J0IEZvcm1hdEluZGVudERlY3JlYXNlSWNvblxuLy8gICBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJbmRlbnREZWNyZWFzZSc7XG4vLyBpbXBvcnQgRm9ybWF0SW5kZW50SW5jcmVhc2VJY29uXG4vLyAgIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEluZGVudEluY3JlYXNlJztcblxuaW1wb3J0IENvZGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0NvZGUnO1xuXG5pbXBvcnQgRm9ybWF0TGlzdE51bWJlcmVkSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQnO1xuaW1wb3J0IEZvcm1hdExpc3RCdWxsZXRlZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkJztcblxuaW1wb3J0IEZvcm1hdEFsaWduQ2VudGVySWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlcic7XG5pbXBvcnQgRm9ybWF0QWxpZ25MZWZ0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQnO1xuaW1wb3J0IEZvcm1hdEFsaWduUmlnaHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQnO1xuaW1wb3J0IEZvcm1hdEFsaWduSnVzdGlmeUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5JztcblxuaW1wb3J0IEF0dGFjaEZpbGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0F0dGFjaEZpbGUnO1xuaW1wb3J0IEluc2VydExpbmtJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsnO1xuaW1wb3J0IEluc2VydFBob3RvSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90byc7XG5pbXBvcnQgSW5zZXJ0RW1vdGljb25JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uJztcbmltcG9ydCBJbnNlcnRDb21tZW50SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50JztcblxuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25Ub3BJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25Ub3AnO1xuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25Cb3R0b21JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25Cb3R0b20nO1xuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25DZW50ZXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25DZW50ZXInO1xuXG4vLyBpbXBvcnQgV3JhcFRleHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1dyYXBUZXh0JztcblxuaW1wb3J0IEhpZ2hsaWdodEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0JztcbmltcG9ydCBGdW5jdGlvbnNJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucyc7XG5cbmltcG9ydCB7XG4gIEVkaXRvcixcbiAgUmljaFV0aWxzLFxuICAvLyBFbnRpdHksXG4gIC8vIENvbXBvc2l0ZURlY29yYXRvcixcbiAgQXRvbWljQmxvY2tVdGlscyxcbiAgRWRpdG9yU3RhdGUsXG4gIC8vICAgY29udmVydFRvUmF3LFxufSBmcm9tICdkcmFmdC1qcyc7XG5cbi8vIGltcG9ydCB7XG4vLyAgIGhhc2h0YWdTdHJhdGVneSxcbi8vICAgSGFzaHRhZ1NwYW4sXG5cbi8vICAgaGFuZGxlU3RyYXRlZ3ksXG4vLyAgIEhhbmRsZVNwYW4sXG4vLyB9IGZyb20gJy4vY29tcG9uZW50cy9EZWNvcmF0b3JzJztcblxuaW1wb3J0IEF0b21pYyBmcm9tICcuL2NvbXBvbmVudHMvQXRvbWljJztcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi9FZGl0b3JTdHlsZXMnO1xuXG5pbXBvcnQgeyBCbG9ja3MsIEhBTkRMRUQsIE5PVF9IQU5ETEVEIH0gZnJvbSAnLi9jb25zdGFudHMnO1xuXG4vKipcbiAqIE1heWFzaEVkaXRvclxuICovXG5jbGFzcyBNYXlhc2hFZGl0b3IgZXh0ZW5kcyBDb21wb25lbnQge1xuICAvKipcbiAgICogTWF5YXNoIEVkaXRvcidzIHByb3B0eXBlcyBhcmUgZGVmaW5lZCBoZXJlLlxuICAgKi9cbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgICBlZGl0b3JTdGF0ZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuXG4gICAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG5cbiAgICByZWFkT25seTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcblxuICAgIC8qKlxuICAgICAqIFRoaXMgYXBpIGZ1bmN0aW9uIHNob3VsZCBiZSBhcHBsaWVkIGZvciBlbmFibGluZyBwaG90byBhZGRpbmdcbiAgICAgKiBmZWF0dXJlIGluIE1heWFzaCBFZGl0b3IuXG4gICAgICovXG4gICAgYXBpUGhvdG9VcGxvYWQ6IFByb3BUeXBlcy5mdW5jLFxuICB9O1xuXG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgcmVhZE9ubHk6IHRydWUsXG4gICAgcGxhY2Vob2xkZXI6ICdXcml0ZSBIZXJlLi4uJyxcbiAgfTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIC8vIHRoaXMuc3RhdGUgPSB7fTtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBiZSBzdXBwbGllZCB0byBEcmFmdC5qcyBFZGl0b3IgdG8gaGFuZGxlXG4gICAgICogb25DaGFuZ2UgZXZlbnQuXG4gICAgICogQGZ1bmN0aW9uIG9uQ2hhbmdlXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGVkaXRvclN0YXRlXG4gICAgICovXG4gICAgdGhpcy5vbkNoYW5nZSA9IChlZGl0b3JTdGF0ZSkgPT4ge1xuICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZShlZGl0b3JTdGF0ZSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZm9jdXMgZXZlbnQgaW4gRHJhZnQuanMgRWRpdG9yLlxuICAgICAqIEBmdW5jdGlvbiBmb2N1c1xuICAgICAqL1xuICAgIHRoaXMuZm9jdXMgPSAoKSA9PiB0aGlzLmVkaXRvck5vZGUuZm9jdXMoKTtcblxuICAgIHRoaXMub25UYWIgPSB0aGlzLm9uVGFiLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblRpdGxlQ2xpY2sgPSB0aGlzLm9uVGl0bGVDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25Db2RlQ2xpY2sgPSB0aGlzLm9uQ29kZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblF1b3RlQ2xpY2sgPSB0aGlzLm9uUXVvdGVDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25MaXN0QnVsbGV0ZWRDbGljayA9IHRoaXMub25MaXN0QnVsbGV0ZWRDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25MaXN0TnVtYmVyZWRDbGljayA9IHRoaXMub25MaXN0TnVtYmVyZWRDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25Cb2xkQ2xpY2sgPSB0aGlzLm9uQm9sZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkl0YWxpY0NsaWNrID0gdGhpcy5vbkl0YWxpY0NsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblVuZGVyTGluZUNsaWNrID0gdGhpcy5vblVuZGVyTGluZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkhpZ2hsaWdodENsaWNrID0gdGhpcy5vbkhpZ2hsaWdodENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5oYW5kbGVLZXlDb21tYW5kID0gdGhpcy5oYW5kbGVLZXlDb21tYW5kLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNsaWNrSW5zZXJ0UGhvdG8gPSB0aGlzLm9uQ2xpY2tJbnNlcnRQaG90by5iaW5kKHRoaXMpO1xuXG4gICAgLy8gdGhpcy5ibG9ja1JlbmRlcmVyRm4gPSB0aGlzLmJsb2NrUmVuZGVyZXJGbi5iaW5kKHRoaXMpO1xuXG4gICAgLy8gY29uc3QgY29tcG9zaXRlRGVjb3JhdG9yID0gbmV3IENvbXBvc2l0ZURlY29yYXRvcihbXG4gICAgLy8gICB7XG4gICAgLy8gICAgIHN0cmF0ZWd5OiBoYW5kbGVTdHJhdGVneSxcbiAgICAvLyAgICAgY29tcG9uZW50OiBIYW5kbGVTcGFuLFxuICAgIC8vICAgfSxcbiAgICAvLyAgIHtcbiAgICAvLyAgICAgc3RyYXRlZ3k6IGhhc2h0YWdTdHJhdGVneSxcbiAgICAvLyAgICAgY29tcG9uZW50OiBIYXNodGFnU3BhbixcbiAgICAvLyAgIH0sXG4gICAgLy8gXSk7XG4gIH1cblxuICAvKipcbiAgICogb25UYWIoKSB3aWxsIGhhbmRsZSBUYWIgYnV0dG9uIHByZXNzIGV2ZW50IGluIEVkaXRvci5cbiAgICogQGZ1bmN0aW9uIG9uVGFiXG4gICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICovXG4gIG9uVGFiKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLm9uVGFiKGUsIGVkaXRvclN0YXRlLCA0KTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gVGl0bGUgQnV0dG9uLlxuICAgKiBAZnVuY3Rpb24gb25UaXRsZUNsaWNrXG4gICAqL1xuICBvblRpdGxlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2hlYWRlci1vbmUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gQ29kZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvbkNvZGVDbGlja1xuICAgKi9cbiAgb25Db2RlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2NvZGUtYmxvY2snKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gUXVvdGUgQnV0dG9uLlxuICAgKiBAZnVuY3Rpb24gb25RdW90ZUNsaWNrXG4gICAqL1xuICBvblF1b3RlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2Jsb2NrcXVvdGUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCB1cGRhdGUgYmxvY2sgd2l0aCB1bm9yZGVyZWQgbGlzdCBpdGVtc1xuICAgKiBAZnVuY3Rpb24gb25MaXN0QnVsbGV0ZWRDbGlja1xuICAgKi9cbiAgb25MaXN0QnVsbGV0ZWRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICAndW5vcmRlcmVkLWxpc3QtaXRlbScsXG4gICAgKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCB1cGRhdGUgYmxvY2sgd2l0aCBvcmRlcmVkIGxpc3QgaXRlbS5cbiAgICogQGZ1bmN0aW9uIG9uTGlzdE51bWJlcmVkQ2xpY2tcbiAgICovXG4gIG9uTGlzdE51bWJlcmVkQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShcbiAgICAgIGVkaXRvclN0YXRlLFxuICAgICAgJ29yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICApO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGdpdmUgc2VsZWN0ZWQgY29udGVudCBCb2xkIHN0eWxpbmcuXG4gICAqIEBmdW5jdGlvbiBvbkJvbGRDbGlja1xuICAgKi9cbiAgb25Cb2xkQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnQk9MRCcpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGdpdmUgaXRhbGljIHN0eWxpbmcgdG8gc2VsZWN0ZWQgY29udGVudC5cbiAgICogQGZ1bmN0aW9uIG9uSXRhbGljQ2xpY2tcbiAgICovXG4gIG9uSXRhbGljQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnSVRBTElDJyk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSB1bmRlcmxpbmUgc3R5bGluZyB0byBzZWxlY3RlZCBjb250ZW50LlxuICAgKiBAZnVuY3Rpb24gb25VbmRlckxpbmVDbGlja1xuICAgKi9cbiAgb25VbmRlckxpbmVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgICdVTkRFUkxJTkUnLFxuICAgICk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgaGlnaGxpZ2h0IHNlbGVjdGVkIGNvbnRlbnQuXG4gICAqIEBmdW5jdGlvbiBvbkhpZ2hsaWdodENsaWNrXG4gICAqL1xuICBvbkhpZ2hsaWdodENsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVJbmxpbmVTdHlsZShlZGl0b3JTdGF0ZSwgJ0NPREUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqL1xuICBvbkNsaWNrSW5zZXJ0UGhvdG8oKSB7XG4gICAgdGhpcy5waG90by52YWx1ZSA9IG51bGw7XG4gICAgdGhpcy5waG90by5jbGljaygpO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqL1xuICBvbkNoYW5nZUluc2VydFBob3RvID0gYXN5bmMgKGUpID0+IHtcbiAgICAvLyBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xuXG4gICAgaWYgKGZpbGUudHlwZS5pbmRleE9mKCdpbWFnZS8nKSA9PT0gMCkge1xuICAgICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICAgIGZvcm1EYXRhLmFwcGVuZCgncGhvdG8nLCBmaWxlKTtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgdGhpcy5wcm9wcy5hcGlQaG90b1VwbG9hZCh7XG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgRXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY29uc3QgeyBwaG90b1VybDogc3JjIH0gPSBwYXlsb2FkO1xuXG4gICAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCBjb250ZW50U3RhdGUgPSBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgICAgY29uc3QgY29udGVudFN0YXRlV2l0aEVudGl0eSA9IGNvbnRlbnRTdGF0ZS5jcmVhdGVFbnRpdHkoXG4gICAgICAgIEJsb2Nrcy5QSE9UTyxcbiAgICAgICAgJ0lNTVVUQUJMRScsXG4gICAgICAgIHsgc3JjIH0sXG4gICAgICApO1xuXG4gICAgICBjb25zdCBlbnRpdHlLZXkgPSBjb250ZW50U3RhdGVXaXRoRW50aXR5LmdldExhc3RDcmVhdGVkRW50aXR5S2V5KCk7XG5cbiAgICAgIGNvbnN0IG1pZGRsZUVkaXRvclN0YXRlID0gRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7XG4gICAgICAgIGN1cnJlbnRDb250ZW50OiBjb250ZW50U3RhdGVXaXRoRW50aXR5LFxuICAgICAgfSk7XG5cbiAgICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gQXRvbWljQmxvY2tVdGlscy5pbnNlcnRBdG9taWNCbG9jayhcbiAgICAgICAgbWlkZGxlRWRpdG9yU3RhdGUsXG4gICAgICAgIGVudGl0eUtleSxcbiAgICAgICAgJyAnLFxuICAgICAgKTtcblxuICAgICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBjdXN0b20gaGFuZGxlIGNvbW1hbmRzIHRvIEVkaXRvci5cbiAgICogQGZ1bmN0aW9uIGhhbmRsZUtleUNvbW1hbmRcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvbW1hbmRcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgaGFuZGxlS2V5Q29tbWFuZChjb21tYW5kKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLmhhbmRsZUtleUNvbW1hbmQoZWRpdG9yU3RhdGUsIGNvbW1hbmQpO1xuXG4gICAgaWYgKG5ld0VkaXRvclN0YXRlKSB7XG4gICAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICAgIHJldHVybiBIQU5ETEVEO1xuICAgIH1cblxuICAgIHJldHVybiBOT1RfSEFORExFRDtcbiAgfVxuXG4gIC8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbiAgYmxvY2tSZW5kZXJlckZuKGJsb2NrKSB7XG4gICAgc3dpdGNoIChibG9jay5nZXRUeXBlKCkpIHtcbiAgICAgIGNhc2UgQmxvY2tzLkFUT01JQzpcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBjb21wb25lbnQ6IEF0b21pYyxcbiAgICAgICAgICBlZGl0YWJsZTogZmFsc2UsXG4gICAgICAgIH07XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cblxuICAvKiBlc2xpbnQtZGlzYWJsZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzICovXG4gIGJsb2NrU3R5bGVGbihibG9jaykge1xuICAgIHN3aXRjaCAoYmxvY2suZ2V0VHlwZSgpKSB7XG4gICAgICBjYXNlICdibG9ja3F1b3RlJzpcbiAgICAgICAgcmV0dXJuICdSaWNoRWRpdG9yLWJsb2NrcXVvdGUnO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cbiAgLyogZXNsaW50LWVuYWJsZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzICovXG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGNsYXNzZXMsXG4gICAgICByZWFkT25seSxcbiAgICAgIG9uQ2hhbmdlLFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICBwbGFjZWhvbGRlcixcbiAgICB9ID0gdGhpcy5wcm9wcztcblxuICAgIC8vIEN1c3RvbSBvdmVycmlkZXMgZm9yIFwiY29kZVwiIHN0eWxlLlxuICAgIGNvbnN0IHN0eWxlTWFwID0ge1xuICAgICAgQ09ERToge1xuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsIDAsIDAsIDAuMDUpJyxcbiAgICAgICAgZm9udEZhbWlseTogJ1wiSW5jb25zb2xhdGFcIiwgXCJNZW5sb1wiLCBcIkNvbnNvbGFzXCIsIG1vbm9zcGFjZScsXG4gICAgICAgIGZvbnRTaXplOiAxNixcbiAgICAgICAgcGFkZGluZzogMixcbiAgICAgIH0sXG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgeyFyZWFkT25seSA/IChcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyh7ICdlZGl0b3ItY29udHJvbHMnOiAnJyB9KX0+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIlRpdGxlXCIgaWQ9XCJ0aXRsZVwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiVGl0bGVcIiBvbkNsaWNrPXt0aGlzLm9uVGl0bGVDbGlja30+XG4gICAgICAgICAgICAgICAgPFRpdGxlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkJvbGRcIiBpZD1cImJvbGRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvbGRcIiBvbkNsaWNrPXt0aGlzLm9uQm9sZENsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0Qm9sZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJJdGFsaWNcIiBpZD1cIml0YWxpY1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSXRhbGljXCIgb25DbGljaz17dGhpcy5vbkl0YWxpY0NsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0SXRhbGljIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiVW5kZXJsaW5lXCIgaWQ9XCJ1bmRlcmxpbmVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiVW5kZXJsaW5lXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uVW5kZXJMaW5lQ2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0VW5kZXJsaW5lZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJDb2RlXCIgaWQ9XCJjb2RlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJDb2RlXCIgb25DbGljaz17dGhpcy5vbkNvZGVDbGlja30+XG4gICAgICAgICAgICAgICAgPENvZGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiUXVvdGVcIiBpZD1cInF1b3RlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJRdW90ZVwiIG9uQ2xpY2s9e3RoaXMub25RdW90ZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0UXVvdGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgIHRpdGxlPVwiVW5vcmRlcmVkIExpc3RcIlxuICAgICAgICAgICAgICBpZD1cInVub3JkZXJkLWxpc3RcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVbm9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZvcm1hdExpc3RCdWxsZXRlZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJPcmRlcmVkIExpc3RcIiBpZD1cIm9yZGVyZWQtbGlzdFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJPcmRlcmVkIExpc3RcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25MaXN0TnVtYmVyZWRDbGlja31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRMaXN0TnVtYmVyZWRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gTGVmdFwiIGlkPVwiYWxpZ24tbGVmdFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gTGVmdFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkxlZnRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gQ2VudGVyXCIgaWQ9XCJhbGlnbi1jZW50ZXJcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIENlbnRlclwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkNlbnRlckljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBSaWdodFwiIGlkPVwiYWxpZ24tcmlnaHRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIFJpZ2h0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduUmlnaHRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gUmlnaHRcIiBpZD1cImFsaWduLXJpZ2h0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBbGlnbiBSaWdodFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkp1c3RpZnlJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQXR0YWNoIEZpbGVcIiBpZD1cImF0dGFjaC1maWxlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBdHRhY2ggRmlsZVwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxBdHRhY2hGaWxlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkluc2VydCBMaW5rXCIgaWQ9XCJpbnNlcnQtbGlua1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IExpbmtcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8SW5zZXJ0TGlua0ljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJJbnNlcnQgUGhvdG9cIiBpZD1cImluc2VydC1waG90b1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJJbnNlcnQgUGhvdG9cIlxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0eXBlb2YgdGhpcy5wcm9wcy5hcGlQaG90b1VwbG9hZCAhPT0gJ2Z1bmN0aW9uJ31cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tJbnNlcnRQaG90b31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRQaG90b0ljb24gLz5cbiAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcbiAgICAgICAgICAgICAgICAgIGFjY2VwdD1cImltYWdlL2pwZWd8cG5nfGdpZlwiXG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZUluc2VydFBob3RvfVxuICAgICAgICAgICAgICAgICAgcmVmPXsocGhvdG8pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5waG90byA9IHBob3RvO1xuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6ICdub25lJyB9fVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgIHRpdGxlPVwiSW5zZXJ0IEVtb3RpY29uXCJcbiAgICAgICAgICAgICAgaWQ9XCJpbnNlcnRFLWVtb3RpY29uXCJcbiAgICAgICAgICAgICAgcGxhY2VtZW50PVwiYm90dG9tXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkluc2VydCBFbW90aWNvblwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRFbW90aWNvbkljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJJbnNlcnQgQ29tbWVudFwiXG4gICAgICAgICAgICAgIGlkPVwiaW5zZXJ0LWNvbW1lbnRcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IENvbW1lbnRcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8SW5zZXJ0Q29tbWVudEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJIaWdobGlnaHQgVGV4dFwiXG4gICAgICAgICAgICAgIGlkPVwiaGlnaGxpZ2h0LXRleHRcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJIaWdobGlnaHQgVGV4dFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkhpZ2hsaWdodENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEhpZ2hsaWdodEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJBZGQgRnVuY3Rpb25zXCJcbiAgICAgICAgICAgICAgaWQ9XCJhZGQtZnVuY3Rpb25zXCJcbiAgICAgICAgICAgICAgcGxhY2VtZW50PVwiYm90dG9tXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFkZCBGdW5jdGlvbnNcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8RnVuY3Rpb25zSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkgOiBudWxsfVxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyh7ICdlZGl0b3ItYXJlYSc6ICcnIH0pfT5cbiAgICAgICAgICA8RWRpdG9yXG4gICAgICAgICAgICAvKiBCYXNpY3MgKi9cblxuICAgICAgICAgICAgZWRpdG9yU3RhdGU9e2VkaXRvclN0YXRlfVxuICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgLyogUHJlc2VudGF0aW9uICovXG5cbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgIC8vIHRleHRBbGlnbm1lbnQ9XCJjZW50ZXJcIlxuXG4gICAgICAgICAgICAvLyB0ZXh0RGlyZWN0aW9uYWxpdHk9XCJMVFJcIlxuXG4gICAgICAgICAgICBibG9ja1JlbmRlcmVyRm49e3RoaXMuYmxvY2tSZW5kZXJlckZufVxuICAgICAgICAgICAgYmxvY2tTdHlsZUZuPXt0aGlzLmJsb2NrU3R5bGVGbn1cbiAgICAgICAgICAgIGN1c3RvbVN0eWxlTWFwPXtzdHlsZU1hcH1cbiAgICAgICAgICAgIC8vIGN1c3RvbVN0eWxlRm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBCZWhhdmlvciAqL1xuXG4gICAgICAgICAgICAvLyBhdXRvQ2FwaXRhbGl6ZT1cInNlbnRlbmNlc1wiXG5cbiAgICAgICAgICAgIC8vIGF1dG9Db21wbGV0ZT1cIm9mZlwiXG5cbiAgICAgICAgICAgIC8vIGF1dG9Db3JyZWN0PVwib2ZmXCJcblxuICAgICAgICAgICAgcmVhZE9ubHk9e3JlYWRPbmx5fVxuICAgICAgICAgICAgc3BlbGxDaGVja1xuICAgICAgICAgICAgLy8gc3RyaXBQYXN0ZWRTdHlsZXM9e2ZhbHNlfVxuXG4gICAgICAgICAgICAvKiBET00gYW5kIEFjY2Vzc2liaWxpdHkgKi9cblxuICAgICAgICAgICAgLy8gZWRpdG9yS2V5XG5cbiAgICAgICAgICAgIC8qIENhbmNlbGFibGUgSGFuZGxlcnMgKi9cblxuICAgICAgICAgICAgLy8gaGFuZGxlUmV0dXJuPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgaGFuZGxlS2V5Q29tbWFuZD17dGhpcy5oYW5kbGVLZXlDb21tYW5kfVxuICAgICAgICAgICAgLy8gaGFuZGxlQmVmb3JlSW5wdXQ9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVQYXN0ZWRUZXh0PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gaGFuZGxlUGFzdGVkRmlsZXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVEcm9wcGVkRmlsZXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVEcm9wPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogS2V5IEhhbmRsZXJzICovXG5cbiAgICAgICAgICAgIC8vIG9uRXNjYXBlPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgb25UYWI9e3RoaXMub25UYWJ9XG4gICAgICAgICAgICAvLyBvblVwQXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBvblJpZ2h0QXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBvbkRvd25BcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uTGVmdEFycm93PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8ga2V5QmluZGluZ0ZuPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogTW91c2UgRXZlbnQgKi9cblxuICAgICAgICAgICAgLy8gb25Gb2N1cz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uQmx1cj17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8qIE1ldGhvZHMgKi9cblxuICAgICAgICAgICAgLy8gZm9jdXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBibHVyPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogRm9yIFJlZmVyZW5jZSAqL1xuXG4gICAgICAgICAgICByZWY9eyhub2RlKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZWRpdG9yTm9kZSA9IG5vZGU7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTWF5YXNoRWRpdG9yKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3IuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgeyBFZGl0b3JTdGF0ZSwgY29udmVydEZyb21SYXcsIENvbXBvc2l0ZURlY29yYXRvciB9IGZyb20gJ2RyYWZ0LWpzJztcblxuaW1wb3J0IHtcbiAgaGFzaHRhZ1N0cmF0ZWd5LFxuICBIYXNodGFnU3BhbixcbiAgaGFuZGxlU3RyYXRlZ3ksXG4gIEhhbmRsZVNwYW4sXG59IGZyb20gJy4vY29tcG9uZW50cy9EZWNvcmF0b3JzJztcblxuY29uc3QgZGVmYXVsdERlY29yYXRvcnMgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKFtcbiAge1xuICAgIHN0cmF0ZWd5OiBoYW5kbGVTdHJhdGVneSxcbiAgICBjb21wb25lbnQ6IEhhbmRsZVNwYW4sXG4gIH0sXG4gIHtcbiAgICBzdHJhdGVneTogaGFzaHRhZ1N0cmF0ZWd5LFxuICAgIGNvbXBvbmVudDogSGFzaHRhZ1NwYW4sXG4gIH0sXG5dKTtcblxuZXhwb3J0IGNvbnN0IGNyZWF0ZUVkaXRvclN0YXRlID0gKFxuICBjb250ZW50ID0gbnVsbCxcbiAgZGVjb3JhdG9ycyA9IGRlZmF1bHREZWNvcmF0b3JzLFxuKSA9PiB7XG4gIGlmIChjb250ZW50ID09PSBudWxsKSB7XG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLmNyZWF0ZUVtcHR5KGRlY29yYXRvcnMpO1xuICB9XG4gIHJldHVybiBFZGl0b3JTdGF0ZS5jcmVhdGVXaXRoQ29udGVudChjb252ZXJ0RnJvbVJhdyhjb250ZW50KSwgZGVjb3JhdG9ycyk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVFZGl0b3JTdGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdGF0ZS5qcyIsIi8qKlxuICogVGhpcyBmaWxlIGNvbnRhaW5zIGFsbCB0aGUgQ1NTLWluLUpTIHN0eWxlcyBvZiBFZGl0b3IgY29tcG9uZW50LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gICdAZ2xvYmFsJzoge1xuICAgICcuUmljaEVkaXRvci1yb290Jzoge1xuICAgICAgYmFja2dyb3VuZDogJyNmZmYnLFxuICAgICAgYm9yZGVyOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgICAgZm9udEZhbWlseTogXCInR2VvcmdpYScsIHNlcmlmXCIsXG4gICAgICBmb250U2l6ZTogJzE0cHgnLFxuICAgICAgcGFkZGluZzogJzE1cHgnLFxuICAgIH0sXG4gICAgJy5SaWNoRWRpdG9yLWVkaXRvcic6IHtcbiAgICAgIGJvcmRlclRvcDogJzFweCBzb2xpZCAjZGRkJyxcbiAgICAgIGN1cnNvcjogJ3RleHQnLFxuICAgICAgZm9udFNpemU6ICcxNnB4JyxcbiAgICAgIG1hcmdpblRvcDogJzEwcHgnLFxuICAgIH0sXG4gICAgJy5wdWJsaWMtRHJhZnRFZGl0b3JQbGFjZWhvbGRlci1yb290Jzoge1xuICAgICAgbWFyZ2luOiAnMCAtMTVweCAtMTVweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgfSxcbiAgICAnLnB1YmxpYy1EcmFmdEVkaXRvci1jb250ZW50Jzoge1xuICAgICAgbWFyZ2luOiAnMCAtMTVweCAtMTVweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgICAvLyBtaW5IZWlnaHQ6ICcxMDBweCcsXG4gICAgfSxcbiAgICAnLlJpY2hFZGl0b3ItYmxvY2txdW90ZSc6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJzVweCBzb2xpZCAjZWVlJyxcbiAgICAgIGJvcmRlckxlZnQ6ICc1cHggc29saWQgI2VlZScsXG4gICAgICBjb2xvcjogJyM2NjYnLFxuICAgICAgZm9udEZhbWlseTogXCInSG9lZmxlciBUZXh0JywgJ0dlb3JnaWEnLCBzZXJpZlwiLFxuICAgICAgZm9udFN0eWxlOiAnaXRhbGljJyxcbiAgICAgIG1hcmdpbjogJzE2cHggMCcsXG4gICAgICBwYWRkaW5nOiAnMTBweCAyMHB4JyxcbiAgICB9LFxuICAgICcucHVibGljLURyYWZ0U3R5bGVEZWZhdWx0LXByZSc6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwgMCwgMCwgMC4wNSknLFxuICAgICAgZm9udEZhbWlseTogXCInSW5jb25zb2xhdGEnLCAnTWVubG8nLCAnQ29uc29sYXMnLCBtb25vc3BhY2VcIixcbiAgICAgIGZvbnRTaXplOiAnMTZweCcsXG4gICAgICBwYWRkaW5nOiAnMjBweCcsXG4gICAgfSxcbiAgfSxcbiAgcm9vdDoge1xuICAgIC8vIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3R5bGVzLmpzIiwiLyoqXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBCbG9ja3MgfSBmcm9tICcuLi9jb25zdGFudHMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHBob3RvOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICAvLyBGaXggYW4gaXNzdWUgd2l0aCBGaXJlZm94IHJlbmRlcmluZyB2aWRlbyBjb250cm9sc1xuICAgIC8vIHdpdGggJ3ByZS13cmFwJyB3aGl0ZS1zcGFjZVxuICAgIHdoaXRlU3BhY2U6ICdpbml0aWFsJyxcbiAgfSxcbn07XG5cbi8qKlxuICpcbiAqIEBjbGFzcyBBdG9taWMgLSB0aGlzIFJlYWN0IGNvbXBvbmVudCB3aWxsIGJlIHVzZWQgdG8gcmVuZGVyIEF0b21pY1xuICogY29tcG9uZW50cyBvZiBEcmFmdC5qc1xuICpcbiAqIEB0b2RvIC0gY29uZmlndXJlIHRoaXMgZm9yIGF1ZGlvLlxuICogQHRvZG8gLSBjb25maWd1cmUgdGhpcyBmb3IgdmlkZW8uXG4gKi9cbmNsYXNzIEF0b21pYyBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY29udGVudFN0YXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgYmxvY2s6IFByb3BUeXBlcy5hbnkuaXNSZXF1aXJlZCxcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjb250ZW50U3RhdGUsIGJsb2NrIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgZW50aXR5ID0gY29udGVudFN0YXRlLmdldEVudGl0eShibG9jay5nZXRFbnRpdHlBdCgwKSk7XG4gICAgY29uc3QgeyBzcmMgfSA9IGVudGl0eS5nZXREYXRhKCk7XG4gICAgY29uc3QgdHlwZSA9IGVudGl0eS5nZXRUeXBlKCk7XG5cbiAgICBpZiAodHlwZSA9PT0gQmxvY2tzLlBIT1RPKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIDxpbWcgYWx0PXsnYWx0J30gc3JjPXtzcmN9IHN0eWxlPXtzdHlsZXMucGhvdG99IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gPGRpdiAvPjtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBBdG9taWM7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9BdG9taWMuanMiLCIvKiBlc2xpbnQtZGlzYWJsZSAqL1xuLy8gZXNsaW50IGlzIGRpc2FibGVkIGhlcmUgZm9yIG5vdy5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgc3R5bGUgZnJvbSAnLi4vc3R5bGUnO1xuXG5jb25zdCBIQU5ETEVfUkVHRVggPSAvXFxAW1xcd10rL2c7XG5jb25zdCBIQVNIVEFHX1JFR0VYID0gL1xcI1tcXHdcXHUwNTkwLVxcdTA1ZmZdKy9nO1xuXG5mdW5jdGlvbiBmaW5kV2l0aFJlZ2V4KHJlZ2V4LCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKSB7XG4gIGNvbnN0IHRleHQgPSBjb250ZW50QmxvY2suZ2V0VGV4dCgpO1xuICBsZXQgbWF0Y2hBcnIsXG4gICAgc3RhcnQ7XG4gIHdoaWxlICgobWF0Y2hBcnIgPSByZWdleC5leGVjKHRleHQpKSAhPT0gbnVsbCkge1xuICAgIHN0YXJ0ID0gbWF0Y2hBcnIuaW5kZXg7XG4gICAgY2FsbGJhY2soc3RhcnQsIHN0YXJ0ICsgbWF0Y2hBcnJbMF0ubGVuZ3RoKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFuZGxlU3RyYXRlZ3koY29udGVudEJsb2NrLCBjYWxsYmFjaywgY29udGVudFN0YXRlKSB7XG4gIGZpbmRXaXRoUmVnZXgoSEFORExFX1JFR0VYLCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGhhc2h0YWdTdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrLCBjb250ZW50U3RhdGUpIHtcbiAgZmluZFdpdGhSZWdleChIQVNIVEFHX1JFR0VYLCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKTtcbn1cblxuZXhwb3J0IGNvbnN0IEhhbmRsZVNwYW4gPSBwcm9wcyA9PiA8c3BhbiBzdHlsZT17c3R5bGUuaGFuZGxlfT57cHJvcHMuY2hpbGRyZW59PC9zcGFuPjtcblxuZXhwb3J0IGNvbnN0IEhhc2h0YWdTcGFuID0gcHJvcHMgPT4gPHNwYW4gc3R5bGU9e3N0eWxlLmhhc2h0YWd9Pntwcm9wcy5jaGlsZHJlbn08L3NwYW4+O1xuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaGFuZGxlU3RyYXRlZ3ksXG4gIEhhbmRsZVNwYW4sXG5cbiAgaGFzaHRhZ1N0cmF0ZWd5LFxuICBIYXNodGFnU3Bhbixcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9EZWNvcmF0b3JzLmpzIiwiLyoqXG4gKiBTb21lIG9mIHRoZSBjb25zdGFudHMgd2hpY2ggYXJlIHVzZWQgdGhyb3VnaG91dCB0aGlzIHByb2plY3QgaW5zdGVhZCBvZlxuICogZGlyZWN0bHkgdXNpbmcgc3RyaW5nLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG4vKipcbiAqIEBjb25zdGFudCBCbG9ja3NcbiAqL1xuZXhwb3J0IGNvbnN0IEJsb2NrcyA9IHtcbiAgVU5TVFlMRUQ6ICd1bnN0eWxlZCcsXG4gIFBBUkFHUkFQSDogJ3Vuc3R5bGVkJyxcblxuICBIMTogJ2hlYWRlci1vbmUnLFxuICBIMjogJ2hlYWRlci10d28nLFxuICBIMzogJ2hlYWRlci10aHJlZScsXG4gIEg0OiAnaGVhZGVyLWZvdXInLFxuICBINTogJ2hlYWRlci1maXZlJyxcbiAgSDY6ICdoZWFkZXItc2l4JyxcblxuICBPTDogJ29yZGVyZWQtbGlzdC1pdGVtJyxcbiAgVUw6ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcblxuICBDT0RFOiAnY29kZS1ibG9jaycsXG5cbiAgQkxPQ0tRVU9URTogJ2Jsb2NrcXVvdGUnLFxuXG4gIEFUT01JQzogJ2F0b21pYycsXG4gIFBIT1RPOiAnYXRvbWljOnBob3RvJyxcbiAgVklERU86ICdhdG9taWM6dmlkZW8nLFxufTtcblxuLyoqXG4gKiBAY29uc3RhbnQgSW5saW5lXG4gKi9cbmV4cG9ydCBjb25zdCBJbmxpbmUgPSB7XG4gIEJPTEQ6ICdCT0xEJyxcbiAgQ09ERTogJ0NPREUnLFxuICBJVEFMSUM6ICdJVEFMSUMnLFxuICBTVFJJS0VUSFJPVUdIOiAnU1RSSUtFVEhST1VHSCcsXG4gIFVOREVSTElORTogJ1VOREVSTElORScsXG4gIEhJR0hMSUdIVDogJ0hJR0hMSUdIVCcsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBFbnRpdHlcbiAqL1xuZXhwb3J0IGNvbnN0IEVudGl0eSA9IHtcbiAgTElOSzogJ0xJTksnLFxufTtcblxuLyoqXG4gKiBAY29uc3RhbnQgSFlQRVJMSU5LXG4gKi9cbmV4cG9ydCBjb25zdCBIWVBFUkxJTksgPSAnaHlwZXJsaW5rJztcblxuLyoqXG4gKiBDb25zdGFudHMgdG8gaGFuZGxlIGtleSBjb21tYW5kc1xuICovXG5leHBvcnQgY29uc3QgSEFORExFRCA9ICdoYW5kbGVkJztcbmV4cG9ydCBjb25zdCBOT1RfSEFORExFRCA9ICdub3RfaGFuZGxlZCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgQmxvY2tzLFxuICBJbmxpbmUsXG4gIEVudGl0eSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29uc3RhbnRzLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IEVkaXRvciBmcm9tICcuL0VkaXRvcic7XG5cbmV4cG9ydCB7IE1heWFzaEVkaXRvciB9IGZyb20gJy4vRWRpdG9yJztcblxuZXhwb3J0IHsgY3JlYXRlRWRpdG9yU3RhdGUgfSBmcm9tICcuL0VkaXRvclN0YXRlJztcblxuZXhwb3J0IGRlZmF1bHQgRWRpdG9yO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuZXhwb3J0IGNvbnN0IHN0eWxlID0ge1xuICByb290OiB7XG4gICAgcGFkZGluZzogMjAsXG4gICAgd2lkdGg6IDYwMCxcbiAgfSxcbiAgZWRpdG9yOiB7XG4gICAgYm9yZGVyOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgIGN1cnNvcjogJ3RleHQnLFxuICAgIGZvbnRTaXplOiAxNixcbiAgICBtaW5IZWlnaHQ6IDQwLFxuICAgIHBhZGRpbmc6IDEwLFxuICB9LFxuICBidXR0b246IHtcbiAgICBtYXJnaW5Ub3A6IDEwLFxuICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gIH0sXG4gIGhhbmRsZToge1xuICAgIGNvbG9yOiAncmdiYSg5OCwgMTc3LCAyNTQsIDEuMCknLFxuICAgIGRpcmVjdGlvbjogJ2x0cicsXG4gICAgdW5pY29kZUJpZGk6ICdiaWRpLW92ZXJyaWRlJyxcbiAgfSxcbiAgaGFzaHRhZzoge1xuICAgIGNvbG9yOiAncmdiYSg5NSwgMTg0LCAxMzgsIDEuMCknLFxuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3Ivc3R5bGUvaW5kZXguanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuY29uc3QgUHJpdmF0ZSA9ICh7IHN0eWxlLCB3aWR0aCwgaGVpZ2h0IH0pID0+IChcbiAgPHN2Z1xuICAgIGZpbGw9XCIjMDAwMDAwXCJcbiAgICBoZWlnaHQ9XCIxOFwiXG4gICAgdmlld0JveD1cIjAgMCAyNCAyNFwiXG4gICAgd2lkdGg9XCIxOFwiXG4gICAgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Z1wiXG4gID5cbiAgICA8cGF0aCBkPVwiTTAgMGgyNHYyNEgwelwiIGZpbGw9XCJub25lXCIgLz5cbiAgICA8cGF0aCBkPVwiTTE4IDhoLTFWNmMwLTIuNzYtMi4yNC01LTUtNVM3IDMuMjQgNyA2djJINmMtMS4xIDAtMiAuOS0yIDJ2MTBjMCAxLjEuOSAyIDIgMmgxMmMxLjEgMCAyLS45IDItMlYxMGMwLTEuMS0uOS0yLTItMnptLTYgOWMtMS4xIDAtMi0uOS0yLTJzLjktMiAyLTIgMiAuOSAyIDItLjkgMi0yIDJ6bTMuMS05SDguOVY2YzAtMS43MSAxLjM5LTMuMSAzLjEtMy4xIDEuNzEgMCAzLjEgMS4zOSAzLjEgMy4xdjJ6XCIgLz5cbiAgPC9zdmc+XG4pO1xuXG5Qcml2YXRlLnByb3BUeXBlcyA9IHtcbiAgd2lkdGg6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGhlaWdodDogUHJvcFR5cGVzLnN0cmluZyxcbiAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBQcml2YXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtaWNvbnMvUHJpdmF0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5jb25zdCBQdWJsaWMgPSAoeyBzdHlsZSwgd2lkdGgsIGhlaWdodCB9KSA9PiAoXG4gIDxzdmdcbiAgICBmaWxsPVwiIzAwMDAwMFwiXG4gICAgaGVpZ2h0PVwiMThcIlxuICAgIHZpZXdCb3g9XCIwIDAgMjQgMjRcIlxuICAgIHdpZHRoPVwiMThcIlxuICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxuICA+XG4gICAgPHBhdGggZD1cIk0wIDBoMjR2MjRIMHpcIiBmaWxsPVwibm9uZVwiIC8+XG4gICAgPHBhdGggZD1cIk0xMiAyQzYuNDggMiAyIDYuNDggMiAxMnM0LjQ4IDEwIDEwIDEwIDEwLTQuNDggMTAtMTBTMTcuNTIgMiAxMiAyem0tMSAxNy45M2MtMy45NS0uNDktNy0zLjg1LTctNy45MyAwLS42Mi4wOC0xLjIxLjIxLTEuNzlMOSAxNXYxYzAgMS4xLjkgMiAyIDJ2MS45M3ptNi45LTIuNTRjLS4yNi0uODEtMS0xLjM5LTEuOS0xLjM5aC0xdi0zYzAtLjU1LS40NS0xLTEtMUg4di0yaDJjLjU1IDAgMS0uNDUgMS0xVjdoMmMxLjEgMCAyLS45IDItMnYtLjQxYzIuOTMgMS4xOSA1IDQuMDYgNSA3LjQxIDAgMi4wOC0uOCAzLjk3LTIuMSA1LjM5elwiIC8+XG4gIDwvc3ZnPlxuKTtcblxuUHVibGljLnByb3BUeXBlcyA9IHtcbiAgd2lkdGg6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGhlaWdodDogUHJvcFR5cGVzLnN0cmluZyxcbiAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBQdWJsaWM7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1pY29ucy9QdWJsaWMuanMiXSwic291cmNlUm9vdCI6IiJ9