webpackJsonp([61],{

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./src/client/actions/courses/create.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/create.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 * create action will add newly created course to store.
 * @param {Object} payload -
 * @param {number} payload.statusCode -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @param {string} payload.title -
 * @param {string} payload.timestamp -
 *
 * @return {Object} -
 */
var create = function create(payload) {
  return { type: _courses.COURSE_CREATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/api/courses/users/create.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/create.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * create course for user.
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.title -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * 
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/CourseCreate/index.js":
/*!*****************************************************!*\
  !*** ./src/client/containers/CourseCreate/index.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../actions/courses/create */ "./src/client/actions/courses/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../api/courses/users/create */ "./src/client/api/courses/users/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * For creating course
 *
 * @format
 */

var styles = {
  root: {
    padding: '1%'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  cardHeader: {},
  flexGrow: {
    flex: '1 1 auto'
  }
};

var CourseCreate = function (_Component) {
  (0, _inherits3.default)(CourseCreate, _Component);

  function CourseCreate(props) {
    (0, _classCallCheck3.default)(this, CourseCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CourseCreate.__proto__ || (0, _getPrototypeOf2.default)(CourseCreate)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onFocus = function () {
      return _this.setState({ active: true });
    };

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Course Title...',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(CourseCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      this.setState({ title: e.target.value });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props$elements$user, id, token, title, body, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();
                _props$elements$user = this.props.elements.user, id = _props$elements$user.id, token = _props$elements$user.token;
                title = this.state.title;
                body = { title: title };


                this.setState({ message: 'Creating course...' });

                _context.next = 7;
                return (0, _create4.default)({
                  token: token,
                  userId: id,
                  title: title
                });

              case 7:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 14;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 14:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionCourseCreate((0, _extends3.default)({
                  statusCode: statusCode
                }, payload, body));

              case 17:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          active = _state.active,
          placeholder = _state.placeholder,
          title = _state.title,
          message = _state.message;
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements;
      var _elements$user = elements.user,
          name = _elements$user.name,
          avatar = _elements$user.avatar,
          username = _elements$user.username;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: name,
                  src: avatar || '/public/photos/mayash-logo-transparent.png',
                  className: classes.avatar
                })
              )
            ),
            title: _react2.default.createElement(_Input2.default, {
              value: title,
              onChange: this.onChange,
              placeholder: placeholder
            }),
            className: classes.cardHeader,
            onFocus: this.onFocus
          }),
          active && message.length ? _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              null,
              message
            )
          ) : null,
          active ? _react2.default.createElement(
            _Card.CardActions,
            { disableActionSpacing: true },
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onSubmit },
              'Create'
            )
          ) : null
        )
      );
    }
  }]);
  return CourseCreate;
}(_react.Component);

CourseCreate.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,
  actionCourseCreate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionCourseCreate: _create2.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _styles.withStyles)(styles)(CourseCreate));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlQ3JlYXRlL2luZGV4LmpzIl0sIm5hbWVzIjpbImNyZWF0ZSIsInBheWxvYWQiLCJ0eXBlIiwidXNlcklkIiwidG9rZW4iLCJ0aXRsZSIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwiYm9keSIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsInN0eWxlcyIsInJvb3QiLCJkaXNwbGF5IiwiaW5wdXQiLCJmbGV4R3JvdyIsImJvcmRlciIsImJvcmRlclJhZGl1cyIsInBhZGRpbmciLCJvdXRsaW5lIiwiYm9yZGVyQ29sb3IiLCJib3hTaGFkb3ciLCJJbnB1dCIsImNsYXNzZXMiLCJvbkNoYW5nZSIsInZhbHVlIiwicGxhY2Vob2xkZXIiLCJkaXNhYmxlZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwib25lT2ZUeXBlIiwic3RyaW5nIiwibnVtYmVyIiwiYm9vbCIsImNhcmQiLCJhdmF0YXIiLCJjYXJkSGVhZGVyIiwiZmxleCIsIkNvdXJzZUNyZWF0ZSIsInByb3BzIiwib25Nb3VzZUVudGVyIiwic2V0U3RhdGUiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsIm9uRm9jdXMiLCJhY3RpdmUiLCJzdGF0ZSIsInRpdGxlTGVuZ3RoIiwidmFsaWQiLCJtZXNzYWdlIiwiYmluZCIsIm9uU3VibWl0IiwiZSIsInRhcmdldCIsInByZXZlbnREZWZhdWx0IiwiZWxlbWVudHMiLCJ1c2VyIiwiaWQiLCJzZXRUaW1lb3V0IiwiYWN0aW9uQ291cnNlQ3JlYXRlIiwibmFtZSIsInVzZXJuYW1lIiwibGVuZ3RoIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGO0FBQzlGOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlFQUFpRSxnQ0FBZ0M7QUFDakcsU0FBUztBQUNUO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELG9CQUFvQixVOzs7Ozs7Ozs7Ozs7O0FDL016RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Riw4Q0FBOEMsaUJBQWlCLHFCQUFxQixvQ0FBb0MsNkRBQTZELG9CQUFvQixFQUFFLGVBQWU7O0FBRTFOLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdGQUFnRjs7QUFFaEY7O0FBRUEsZ0ZBQWdGLGVBQWU7O0FBRS9GLHlEQUF5RCxVQUFVLHVEQUF1RDtBQUMxSDs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsdUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4R0E7O0FBZUE7Ozs7Ozs7Ozs7O0FBV0EsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQixFQUFFQyw0QkFBRixFQUF1QkQsZ0JBQXZCLEVBQS9CO0FBQUEsQ0FBZixDLENBL0JBOzs7OztrQkFpQ2VELE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkJmOzs7Ozs7Ozs7Ozs7QUFkQTs7Ozs7O3NGQTBCQTtBQUFBLFFBQXdCRyxNQUF4QixTQUF3QkEsTUFBeEI7QUFBQSxRQUFnQ0MsS0FBaEMsU0FBZ0NBLEtBQWhDO0FBQUEsUUFBdUNDLEtBQXZDLFNBQXVDQSxLQUF2QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ0gsTUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUcsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxNQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVOO0FBSFIsZUFGa0I7QUFPM0JPLG9CQUFNLHlCQUFlLEVBQUVOLFlBQUYsRUFBZjtBQVBxQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFjWUMsa0JBZFosR0FjbUNELEdBZG5DLENBY1lDLE1BZFosRUFjb0JDLFVBZHBCLEdBY21DRixHQWRuQyxDQWNvQkUsVUFkcEI7O0FBQUEsa0JBZ0JRRCxVQUFVLEdBaEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FpQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFqQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXVCdUJGLElBQUlLLElBQUosRUF2QnZCOztBQUFBO0FBdUJVQSxnQkF2QlY7QUFBQSx3RUF5QmdCQSxJQXpCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTJCSUMsb0JBQVFGLEtBQVI7O0FBM0JKLDZDQTZCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE3Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVoQixNOzs7OztBQXJCZjs7OztBQUNBOzs7O2tCQXdEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZEZjs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1tQixTQUFTO0FBQ2JDLFFBQU07QUFDSkMsYUFBUztBQURMLEdBRE87QUFJYkMsU0FBTztBQUNMQyxjQUFVLEdBREw7QUFFTEMsWUFBUSxtQkFGSDtBQUdMQyxrQkFBYyxLQUhUO0FBSUxDLGFBQVMsS0FKSjtBQUtMLGVBQVc7QUFDVEMsZUFBUyxNQURBO0FBRVRDLG1CQUFhLFNBRko7QUFHVEMsaUJBQVc7QUFIRjtBQUxOO0FBSk0sQ0FBZixDLENBWkE7Ozs7Ozs7QUE2QkEsSUFBTUMsUUFBUSxTQUFSQSxLQUFRO0FBQUEsTUFBR0MsT0FBSCxRQUFHQSxPQUFIO0FBQUEsTUFBWUMsUUFBWixRQUFZQSxRQUFaO0FBQUEsTUFBc0JDLEtBQXRCLFFBQXNCQSxLQUF0QjtBQUFBLE1BQTZCQyxXQUE3QixRQUE2QkEsV0FBN0I7QUFBQSxNQUEwQ0MsUUFBMUMsUUFBMENBLFFBQTFDO0FBQUEsTUFBb0RqQyxJQUFwRCxRQUFvREEsSUFBcEQ7QUFBQSxTQUNaO0FBQUE7QUFBQSxNQUFLLFdBQVc2QixRQUFRWCxJQUF4QjtBQUNFO0FBQ0UsWUFBTWxCLFFBQVEsTUFEaEI7QUFFRSxtQkFBYWdDLGVBQWUsT0FGOUI7QUFHRSxhQUFPRCxLQUhUO0FBSUUsZ0JBQVVELFFBSlo7QUFLRSxpQkFBV0QsUUFBUVQsS0FMckI7QUFNRSxnQkFBVSxDQUFDLENBQUNhO0FBTmQ7QUFERixHQURZO0FBQUEsQ0FBZDs7QUFhQUwsTUFBTU0sU0FBTixHQUFrQjtBQUNoQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFEVjtBQUVoQk4sWUFBVSxvQkFBVU8sSUFBVixDQUFlRCxVQUZUO0FBR2hCTCxTQUFPLG9CQUFVTyxTQUFWLENBQW9CLENBQUMsb0JBQVVDLE1BQVgsRUFBbUIsb0JBQVVDLE1BQTdCLENBQXBCLEVBQTBESixVQUhqRDtBQUloQkosZUFBYSxvQkFBVU8sTUFKUDtBQUtoQnZDLFFBQU0sb0JBQVV1QyxNQUxBO0FBTWhCTixZQUFVLG9CQUFVUTtBQU5KLENBQWxCOztrQkFTZSwwQkFBV3hCLE1BQVgsRUFBbUJXLEtBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdDZjs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUVBOzs7Ozs7QUF2QkE7Ozs7OztBQXlCQSxJQUFNWCxTQUFTO0FBQ2JDLFFBQU07QUFDSk0sYUFBUztBQURMLEdBRE87QUFJYmtCLFFBQU07QUFDSm5CLGtCQUFjO0FBRFYsR0FKTztBQU9ib0IsVUFBUTtBQUNOcEIsa0JBQWM7QUFEUixHQVBLO0FBVWJxQixjQUFZLEVBVkM7QUFXYnZCLFlBQVU7QUFDUndCLFVBQU07QUFERTtBQVhHLENBQWY7O0lBZ0JNQyxZOzs7QUFDSix3QkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGtKQUNYQSxLQURXOztBQUFBLFVBZ0VuQkMsWUFoRW1CLEdBZ0VKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBaEVJOztBQUFBLFVBaUVuQkMsWUFqRW1CLEdBaUVKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBakVJOztBQUFBLFVBa0VuQkUsT0FsRW1CLEdBa0VUO0FBQUEsYUFBTSxNQUFLSCxRQUFMLENBQWMsRUFBRUksUUFBUSxJQUFWLEVBQWQsQ0FBTjtBQUFBLEtBbEVTOztBQUVqQixVQUFLQyxLQUFMLEdBQWE7QUFDWEQsY0FBUSxLQURHO0FBRVhsRCxhQUFPLEVBRkk7QUFHWG9ELG1CQUFhLENBSEY7QUFJWHZCLG1CQUFhLGlCQUpGOztBQU1Yd0IsYUFBTyxLQU5JO0FBT1gzQyxrQkFBWSxDQVBEO0FBUVhDLGFBQU8sRUFSSTtBQVNYMkMsZUFBUyxFQVRFO0FBVVhQLGFBQU87QUFWSSxLQUFiOztBQWFBLFVBQUtwQixRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBYzRCLElBQWQsT0FBaEI7QUFDQSxVQUFLQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0QsSUFBZCxPQUFoQjtBQWhCaUI7QUFpQmxCOzs7OzZCQUVRRSxDLEVBQUc7QUFDVixXQUFLWCxRQUFMLENBQWMsRUFBRTlDLE9BQU95RCxFQUFFQyxNQUFGLENBQVM5QixLQUFsQixFQUFkO0FBQ0Q7Ozs7MkdBRWM2QixDOzs7Ozs7Ozs7QUFDYkEsa0JBQUVFLGNBQUY7dUNBQ3NCLEtBQUtmLEtBQUwsQ0FBV2dCLFFBQVgsQ0FBb0JDLEksRUFBbENDLEUsd0JBQUFBLEUsRUFBSS9ELEssd0JBQUFBLEs7QUFDSkMscUIsR0FBVSxLQUFLbUQsSyxDQUFmbkQsSztBQUNGTSxvQixHQUFPLEVBQUVOLFlBQUYsRTs7O0FBRWIscUJBQUs4QyxRQUFMLENBQWMsRUFBRVEsU0FBUyxvQkFBWCxFQUFkOzs7dUJBRTZDLHNCQUFnQjtBQUMzRHZELDhCQUQyRDtBQUUzREQsMEJBQVFnRSxFQUZtRDtBQUczRDlEO0FBSDJELGlCQUFoQixDOzs7O0FBQXJDVSwwQixTQUFBQSxVO0FBQVlDLHFCLFNBQUFBLEs7QUFBT2YsdUIsU0FBQUEsTzs7c0JBTXZCYyxjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0EscUJBQUtvQyxRQUFMLENBQWMsRUFBRXBDLHNCQUFGLEVBQWNDLFlBQWQsRUFBZDs7Ozs7QUFJRixxQkFBS21DLFFBQUwsQ0FBYyxFQUFFUSxTQUFTLHVCQUFYLEVBQWQ7O0FBRUFTLDJCQUFXLFlBQU07QUFDZix5QkFBS2pCLFFBQUwsQ0FBYztBQUNaTywyQkFBTyxLQURLO0FBRVozQyxnQ0FBWSxDQUZBO0FBR1pDLDJCQUFPLEVBSEs7QUFJWjJDLDZCQUFTLEVBSkc7O0FBTVp0RCwyQkFBTyxFQU5LO0FBT1pvRCxpQ0FBYTtBQVBELG1CQUFkO0FBU0QsaUJBVkQsRUFVRyxJQVZIOztBQVlBLHFCQUFLUixLQUFMLENBQVdvQixrQkFBWDtBQUNFdEQ7QUFERixtQkFFS2QsT0FGTCxFQUdLVSxJQUhMOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBV087QUFBQSxtQkFDeUMsS0FBSzZDLEtBRDlDO0FBQUEsVUFDQ0QsTUFERCxVQUNDQSxNQUREO0FBQUEsVUFDU3JCLFdBRFQsVUFDU0EsV0FEVDtBQUFBLFVBQ3NCN0IsS0FEdEIsVUFDc0JBLEtBRHRCO0FBQUEsVUFDNkJzRCxPQUQ3QixVQUM2QkEsT0FEN0I7QUFBQSxtQkFFdUIsS0FBS1YsS0FGNUI7QUFBQSxVQUVDbEIsT0FGRCxVQUVDQSxPQUZEO0FBQUEsVUFFVWtDLFFBRlYsVUFFVUEsUUFGVjtBQUFBLDJCQUc0QkEsU0FBU0MsSUFIckM7QUFBQSxVQUdDSSxJQUhELGtCQUdDQSxJQUhEO0FBQUEsVUFHT3pCLE1BSFAsa0JBR09BLE1BSFA7QUFBQSxVQUdlMEIsUUFIZixrQkFHZUEsUUFIZjs7O0FBS1AsYUFDRTtBQUFBO0FBQUE7QUFDRSxxQkFBV3hDLFFBQVFYLElBRHJCO0FBRUUsd0JBQWMsS0FBSzhCLFlBRnJCO0FBR0Usd0JBQWMsS0FBS0c7QUFIckI7QUFLRTtBQUFBO0FBQUEsWUFBTSxRQUFRLEtBQUtHLEtBQUwsQ0FBV0osS0FBekIsRUFBZ0MsV0FBV3JCLFFBQVFhLElBQW5EO0FBQ0U7QUFDRSxvQkFDRTtBQUFBO0FBQUEsZ0JBQU8sV0FBV2IsUUFBUWMsTUFBMUIsRUFBa0MsV0FBVyxDQUE3QztBQUNFO0FBQUE7QUFBQSxrQkFBTSxXQUFTMEIsUUFBZjtBQUNFO0FBQ0UsdUJBQUtELElBRFA7QUFFRSx1QkFBS3pCLFVBQVUsNENBRmpCO0FBR0UsNkJBQVdkLFFBQVFjO0FBSHJCO0FBREY7QUFERixhQUZKO0FBWUUsbUJBQ0U7QUFDRSxxQkFBT3hDLEtBRFQ7QUFFRSx3QkFBVSxLQUFLMkIsUUFGakI7QUFHRSwyQkFBYUU7QUFIZixjQWJKO0FBbUJFLHVCQUFXSCxRQUFRZSxVQW5CckI7QUFvQkUscUJBQVMsS0FBS1E7QUFwQmhCLFlBREY7QUF1QkdDLG9CQUFVSSxRQUFRYSxNQUFsQixHQUNDO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFhYjtBQUFiO0FBREYsV0FERCxHQUlHLElBM0JOO0FBNEJHSixtQkFDQztBQUFBO0FBQUEsY0FBYSwwQkFBYjtBQUNFLG1EQUFLLFdBQVd4QixRQUFRUixRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLGdCQUFRLFlBQVIsRUFBZSxPQUFNLFFBQXJCLEVBQThCLFNBQVMsS0FBS3NDLFFBQTVDO0FBQUE7QUFBQTtBQUZGLFdBREQsR0FPRztBQW5DTjtBQUxGLE9BREY7QUE2Q0Q7Ozs7O0FBR0hiLGFBQWFaLFNBQWIsR0FBeUI7QUFDdkJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBREg7QUFFdkIyQixZQUFVLG9CQUFVNUIsTUFBVixDQUFpQkMsVUFGSjtBQUd2QitCLHNCQUFvQixvQkFBVTlCLElBQVYsQ0FBZUQ7QUFIWixDQUF6Qjs7QUFNQSxJQUFNbUMsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdSLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7QUFDQSxJQUFNUyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRU47QUFERixHQURGLEVBSUVNLFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVFGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLHdCQUFXdkQsTUFBWCxFQUFtQjZCLFlBQW5CLENBRGEsQyIsImZpbGUiOiI2MS5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2NvbG9yTWFuaXB1bGF0b3IgPSByZXF1aXJlKCcuLi9zdHlsZXMvY29sb3JNYW5pcHVsYXRvcicpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgYWxpZ25JdGVtczogJ2NlbnRlcicsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcicsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgd2lkdGg6IDQwLFxuICAgICAgaGVpZ2h0OiA0MCxcbiAgICAgIGZvbnRGYW1pbHk6IHRoZW1lLnR5cG9ncmFwaHkuZm9udEZhbWlseSxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjApLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcbiAgICAgIHVzZXJTZWxlY3Q6ICdub25lJ1xuICAgIH0sXG4gICAgY29sb3JEZWZhdWx0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6ICgwLCBfY29sb3JNYW5pcHVsYXRvci5lbXBoYXNpemUpKHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LCAwLjI2KVxuICAgIH0sXG4gICAgaW1nOiB7XG4gICAgICBtYXhXaWR0aDogJzEwMCUnLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJ2F1dG8nLFxuICAgICAgdGV4dEFsaWduOiAnY2VudGVyJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZWQgaW4gY29tYmluYXRpb24gd2l0aCBgc3JjYCBvciBgc3JjU2V0YCB0b1xuICAgKiBwcm92aWRlIGFuIGFsdCBhdHRyaWJ1dGUgZm9yIHRoZSByZW5kZXJlZCBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgYWx0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VkIHRvIHJlbmRlciBpY29uIG9yIHRleHQgZWxlbWVudHMgaW5zaWRlIHRoZSBBdmF0YXIuXG4gICAqIGBzcmNgIGFuZCBgYWx0YCBwcm9wcyB3aWxsIG5vdCBiZSB1c2VkIGFuZCBubyBgaW1nYCB3aWxsXG4gICAqIGJlIHJlbmRlcmVkIGJ5IGRlZmF1bHQuXG4gICAqXG4gICAqIFRoaXMgY2FuIGJlIGFuIGVsZW1lbnQsIG9yIGp1c3QgYSBzdHJpbmcuXG4gICAqL1xuICBjaGlsZHJlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZywgdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpXSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICogVGhlIGNsYXNzTmFtZSBvZiB0aGUgY2hpbGQgZWxlbWVudC5cbiAgICogVXNlZCBieSBDaGlwIGFuZCBMaXN0SXRlbUljb24gdG8gc3R5bGUgdGhlIEF2YXRhciBpY29uLlxuICAgKi9cbiAgY2hpbGRyZW5DbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIGZvciB0aGUgcm9vdCBub2RlLlxuICAgKiBFaXRoZXIgYSBzdHJpbmcgdG8gdXNlIGEgRE9NIGVsZW1lbnQgb3IgYSBjb21wb25lbnQuXG4gICAqL1xuICBjb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFByb3BlcnRpZXMgYXBwbGllZCB0byB0aGUgYGltZ2AgZWxlbWVudCB3aGVuIHRoZSBjb21wb25lbnRcbiAgICogaXMgdXNlZCB0byBkaXNwbGF5IGFuIGltYWdlLlxuICAgKi9cbiAgaW1nUHJvcHM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBgc2l6ZXNgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzaXplczogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGBzcmNgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzcmM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBgc3JjU2V0YCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc3JjU2V0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG52YXIgQXZhdGFyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoQXZhdGFyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBBdmF0YXIoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQXZhdGFyKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoQXZhdGFyLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShBdmF0YXIpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEF2YXRhciwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhbHQgPSBfcHJvcHMuYWx0LFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjaGlsZHJlblByb3AgPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2hpbGRyZW5DbGFzc05hbWVQcm9wID0gX3Byb3BzLmNoaWxkcmVuQ2xhc3NOYW1lLFxuICAgICAgICAgIENvbXBvbmVudFByb3AgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICAgIGltZ1Byb3BzID0gX3Byb3BzLmltZ1Byb3BzLFxuICAgICAgICAgIHNpemVzID0gX3Byb3BzLnNpemVzLFxuICAgICAgICAgIHNyYyA9IF9wcm9wcy5zcmMsXG4gICAgICAgICAgc3JjU2V0ID0gX3Byb3BzLnNyY1NldCxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhbHQnLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY2hpbGRyZW4nLCAnY2hpbGRyZW5DbGFzc05hbWUnLCAnY29tcG9uZW50JywgJ2ltZ1Byb3BzJywgJ3NpemVzJywgJ3NyYycsICdzcmNTZXQnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5jb2xvckRlZmF1bHQsIGNoaWxkcmVuUHJvcCAmJiAhc3JjICYmICFzcmNTZXQpLCBjbGFzc05hbWVQcm9wKTtcbiAgICAgIHZhciBjaGlsZHJlbiA9IG51bGw7XG5cbiAgICAgIGlmIChjaGlsZHJlblByb3ApIHtcbiAgICAgICAgaWYgKGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCAmJiB0eXBlb2YgY2hpbGRyZW5Qcm9wICE9PSAnc3RyaW5nJyAmJiBfcmVhY3QyLmRlZmF1bHQuaXNWYWxpZEVsZW1lbnQoY2hpbGRyZW5Qcm9wKSkge1xuICAgICAgICAgIHZhciBfY2hpbGRyZW5DbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCwgY2hpbGRyZW5Qcm9wLnByb3BzLmNsYXNzTmFtZSk7XG4gICAgICAgICAgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkcmVuUHJvcCwgeyBjbGFzc05hbWU6IF9jaGlsZHJlbkNsYXNzTmFtZSB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjaGlsZHJlbiA9IGNoaWxkcmVuUHJvcDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChzcmMgfHwgc3JjU2V0KSB7XG4gICAgICAgIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2ltZycsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGFsdDogYWx0LFxuICAgICAgICAgIHNyYzogc3JjLFxuICAgICAgICAgIHNyY1NldDogc3JjU2V0LFxuICAgICAgICAgIHNpemVzOiBzaXplcyxcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzZXMuaW1nXG4gICAgICAgIH0sIGltZ1Byb3BzKSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQ29tcG9uZW50UHJvcCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBBdmF0YXI7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5BdmF0YXIuZGVmYXVsdFByb3BzID0ge1xuICBjb21wb25lbnQ6ICdkaXYnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUF2YXRhcicgfSkoQXZhdGFyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA0IDYgMjQgMjYgMjcgMjggMzEgMzMgMzQgMzUgMzYgMzcgMzkgNDAgNDMgNDkgNTQgNTcgNTggNTkgNjEgNjMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfQXZhdGFyID0gcmVxdWlyZSgnLi9BdmF0YXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQXZhdGFyKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNCAyNCAyNiAyNyAyOCAzMSAzMyAzNCAzNSAzNiAzNyAzOSA0MCA0MyA0OSA1NyA1OCA1OSA2MSA2MyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9pbnZhcmlhbnQgPSByZXF1aXJlKCdpbnZhcmlhbnQnKTtcblxudmFyIF9pbnZhcmlhbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW52YXJpYW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKG9iaiwga2V5cykgeyB2YXIgdGFyZ2V0ID0ge307IGZvciAodmFyIGkgaW4gb2JqKSB7IGlmIChrZXlzLmluZGV4T2YoaSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgaSkpIGNvbnRpbnVlOyB0YXJnZXRbaV0gPSBvYmpbaV07IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBpc01vZGlmaWVkRXZlbnQgPSBmdW5jdGlvbiBpc01vZGlmaWVkRXZlbnQoZXZlbnQpIHtcbiAgcmV0dXJuICEhKGV2ZW50Lm1ldGFLZXkgfHwgZXZlbnQuYWx0S2V5IHx8IGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQuc2hpZnRLZXkpO1xufTtcblxuLyoqXG4gKiBUaGUgcHVibGljIEFQSSBmb3IgcmVuZGVyaW5nIGEgaGlzdG9yeS1hd2FyZSA8YT4uXG4gKi9cblxudmFyIExpbmsgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoTGluaywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGluaygpIHtcbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIExpbmspO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwuYXBwbHkoX1JlYWN0JENvbXBvbmVudCwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmhhbmRsZUNsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25DbGljaykgX3RoaXMucHJvcHMub25DbGljayhldmVudCk7XG5cbiAgICAgIGlmICghZXZlbnQuZGVmYXVsdFByZXZlbnRlZCAmJiAvLyBvbkNsaWNrIHByZXZlbnRlZCBkZWZhdWx0XG4gICAgICBldmVudC5idXR0b24gPT09IDAgJiYgLy8gaWdub3JlIHJpZ2h0IGNsaWNrc1xuICAgICAgIV90aGlzLnByb3BzLnRhcmdldCAmJiAvLyBsZXQgYnJvd3NlciBoYW5kbGUgXCJ0YXJnZXQ9X2JsYW5rXCIgZXRjLlxuICAgICAgIWlzTW9kaWZpZWRFdmVudChldmVudCkgLy8gaWdub3JlIGNsaWNrcyB3aXRoIG1vZGlmaWVyIGtleXNcbiAgICAgICkge1xuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICB2YXIgaGlzdG9yeSA9IF90aGlzLmNvbnRleHQucm91dGVyLmhpc3Rvcnk7XG4gICAgICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgICAgIHJlcGxhY2UgPSBfdGhpcyRwcm9wcy5yZXBsYWNlLFxuICAgICAgICAgICAgICB0byA9IF90aGlzJHByb3BzLnRvO1xuXG5cbiAgICAgICAgICBpZiAocmVwbGFjZSkge1xuICAgICAgICAgICAgaGlzdG9yeS5yZXBsYWNlKHRvKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaGlzdG9yeS5wdXNoKHRvKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gIExpbmsucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgcmVwbGFjZSA9IF9wcm9wcy5yZXBsYWNlLFxuICAgICAgICB0byA9IF9wcm9wcy50byxcbiAgICAgICAgaW5uZXJSZWYgPSBfcHJvcHMuaW5uZXJSZWYsXG4gICAgICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9wcm9wcywgWydyZXBsYWNlJywgJ3RvJywgJ2lubmVyUmVmJ10pOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVudXNlZC12YXJzXG5cbiAgICAoMCwgX2ludmFyaWFudDIuZGVmYXVsdCkodGhpcy5jb250ZXh0LnJvdXRlciwgJ1lvdSBzaG91bGQgbm90IHVzZSA8TGluaz4gb3V0c2lkZSBhIDxSb3V0ZXI+Jyk7XG5cbiAgICB2YXIgaHJlZiA9IHRoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeS5jcmVhdGVIcmVmKHR5cGVvZiB0byA9PT0gJ3N0cmluZycgPyB7IHBhdGhuYW1lOiB0byB9IDogdG8pO1xuXG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdhJywgX2V4dGVuZHMoe30sIHByb3BzLCB7IG9uQ2xpY2s6IHRoaXMuaGFuZGxlQ2xpY2ssIGhyZWY6IGhyZWYsIHJlZjogaW5uZXJSZWYgfSkpO1xuICB9O1xuXG4gIHJldHVybiBMaW5rO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGluay5wcm9wVHlwZXMgPSB7XG4gIG9uQ2xpY2s6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcbiAgdGFyZ2V0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcbiAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICB0bzogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdF0pLmlzUmVxdWlyZWQsXG4gIGlubmVyUmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuY10pXG59O1xuTGluay5kZWZhdWx0UHJvcHMgPSB7XG4gIHJlcGxhY2U6IGZhbHNlXG59O1xuTGluay5jb250ZXh0VHlwZXMgPSB7XG4gIHJvdXRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgaGlzdG9yeTogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgICBwdXNoOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgY3JlYXRlSHJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWRcbiAgICB9KS5pc1JlcXVpcmVkXG4gIH0pLmlzUmVxdWlyZWRcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjYgMjcgMjggMzAgMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNTcgNTggNTkgNjEiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgQ09VUlNFX0NSRUFURSB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBQYXlsb2FkID0ge3xcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICB0aW1lc3RhbXA6IHN0cmluZyxcbnx9O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBjcmVhdGUgYWN0aW9uIHdpbGwgYWRkIG5ld2x5IGNyZWF0ZWQgY291cnNlIHRvIHN0b3JlLlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZSAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5hdXRob3JJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aW1lc3RhbXAgLVxuICpcbiAqIEByZXR1cm4ge09iamVjdH0gLVxuICovXG5jb25zdCBjcmVhdGUgPSAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoeyB0eXBlOiBDT1VSU0VfQ1JFQVRFLCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy9jcmVhdGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbnR5cGUgUGF5bG9hZCA9IHt8XG4gIHVzZXJJZDogbnVtYmVyLFxuICB0b2tlbjogc3RyaW5nLFxuICB0aXRsZTogc3RyaW5nLFxufH07XG5cbi8qKlxuICogY3JlYXRlIGNvdXJzZSBmb3IgdXNlci5cbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGNyZWF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRpdGxlIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBjcmVhdGUoeyB1c2VySWQsIHRva2VuLCB0aXRsZSB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHRpdGxlIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBUaGlzIGlucHV0IGNvbXBvbmVudCBpcyBtYWlubHkgZGV2ZWxvcGVkIGZvciBjcmVhdGUgcG9zdCwgY3JlYXRlXG4gKiBjb3Vyc2UgY29tcG9uZW50LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBkaXNwbGF5OiAnZmxleCcsXG4gIH0sXG4gIGlucHV0OiB7XG4gICAgZmxleEdyb3c6ICcxJyxcbiAgICBib3JkZXI6ICcycHggc29saWQgI2RhZGFkYScsXG4gICAgYm9yZGVyUmFkaXVzOiAnN3B4JyxcbiAgICBwYWRkaW5nOiAnOHB4JyxcbiAgICAnJjpmb2N1cyc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJyxcbiAgICAgIGJvcmRlckNvbG9yOiAnIzllY2FlZCcsXG4gICAgICBib3hTaGFkb3c6ICcwIDAgMTBweCAjOWVjYWVkJyxcbiAgICB9LFxuICB9LFxufTtcblxuY29uc3QgSW5wdXQgPSAoeyBjbGFzc2VzLCBvbkNoYW5nZSwgdmFsdWUsIHBsYWNlaG9sZGVyLCBkaXNhYmxlZCwgdHlwZSB9KSA9PiAoXG4gIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgIDxpbnB1dFxuICAgICAgdHlwZT17dHlwZSB8fCAndGV4dCd9XG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXIgfHwgJ2lucHV0J31cbiAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5pbnB1dH1cbiAgICAgIGRpc2FibGVkPXshIWRpc2FibGVkfVxuICAgIC8+XG4gIDwvZGl2PlxuKTtcblxuSW5wdXQucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm51bWJlcl0pLmlzUmVxdWlyZWQsXG4gIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB0eXBlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoSW5wdXQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwiLyoqXG4gKiBGb3IgY3JlYXRpbmcgY291cnNlXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcbmltcG9ydCBMaW5rIGZyb20gJ3JlYWN0LXJvdXRlci1kb20vTGluayc7XG5cbmltcG9ydCB7IHdpdGhTdHlsZXMgfSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMnO1xuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciwgQ2FyZENvbnRlbnQsIENhcmRBY3Rpb25zIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgQXZhdGFyIGZyb20gJ21hdGVyaWFsLXVpL0F2YXRhcic7XG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdtYXRlcmlhbC11aS9UeXBvZ3JhcGh5JztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcbmltcG9ydCBQYXBlciBmcm9tICdtYXRlcmlhbC11aS9QYXBlcic7XG5cbmltcG9ydCBJbnB1dCBmcm9tICcuLi8uLi9jb21wb25lbnRzL0lucHV0JztcblxuaW1wb3J0IGFjdGlvbkNvdXJzZUNyZWF0ZSBmcm9tICcuLi8uLi9hY3Rpb25zL2NvdXJzZXMvY3JlYXRlJztcblxuaW1wb3J0IGFwaUNvdXJzZUNyZWF0ZSBmcm9tICcuLi8uLi9hcGkvY291cnNlcy91c2Vycy9jcmVhdGUnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgYXZhdGFyOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgfSxcbiAgY2FyZEhlYWRlcjoge30sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbn07XG5cbmNsYXNzIENvdXJzZUNyZWF0ZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBhY3RpdmU6IGZhbHNlLFxuICAgICAgdGl0bGU6ICcnLFxuICAgICAgdGl0bGVMZW5ndGg6IDAsXG4gICAgICBwbGFjZWhvbGRlcjogJ0NvdXJzZSBUaXRsZS4uLicsXG5cbiAgICAgIHZhbGlkOiBmYWxzZSxcbiAgICAgIHN0YXR1c0NvZGU6IDAsXG4gICAgICBlcnJvcjogJycsXG4gICAgICBtZXNzYWdlOiAnJyxcbiAgICAgIGhvdmVyOiBmYWxzZSxcbiAgICB9O1xuXG4gICAgdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2UuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uU3VibWl0ID0gdGhpcy5vblN1Ym1pdC5iaW5kKHRoaXMpO1xuICB9XG5cbiAgb25DaGFuZ2UoZSkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyB0aXRsZTogZS50YXJnZXQudmFsdWUgfSk7XG4gIH1cblxuICBhc3luYyBvblN1Ym1pdChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGNvbnN0IHsgaWQsIHRva2VuIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyB0aXRsZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCBib2R5ID0geyB0aXRsZSB9O1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2U6ICdDcmVhdGluZyBjb3Vyc2UuLi4nIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpQ291cnNlQ3JlYXRlKHtcbiAgICAgIHRva2VuLFxuICAgICAgdXNlcklkOiBpZCxcbiAgICAgIHRpdGxlLFxuICAgIH0pO1xuXG4gICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAvLyBoYW5kbGUgZXJyb3JcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBzdGF0dXNDb2RlLCBlcnJvciB9KTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ1N1Y2Nlc3NmdWxseSBDcmVhdGVkLicgfSk7XG5cbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICB2YWxpZDogZmFsc2UsXG4gICAgICAgIHN0YXR1c0NvZGU6IDAsXG4gICAgICAgIGVycm9yOiAnJyxcbiAgICAgICAgbWVzc2FnZTogJycsXG5cbiAgICAgICAgdGl0bGU6ICcnLFxuICAgICAgICB0aXRsZUxlbmd0aDogMCxcbiAgICAgIH0pO1xuICAgIH0sIDE1MDApO1xuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25Db3Vyc2VDcmVhdGUoe1xuICAgICAgc3RhdHVzQ29kZSxcbiAgICAgIC4uLnBheWxvYWQsXG4gICAgICAuLi5ib2R5LFxuICAgIH0pO1xuICB9XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuICBvbkZvY3VzID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGFjdGl2ZTogdHJ1ZSB9KTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBhY3RpdmUsIHBsYWNlaG9sZGVyLCB0aXRsZSwgbWVzc2FnZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgbmFtZSwgYXZhdGFyLCB1c2VybmFtZSB9ID0gZWxlbWVudHMudXNlcjtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgPlxuICAgICAgICA8Q2FyZCByYWlzZWQ9e3RoaXMuc3RhdGUuaG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgPFBhcGVyIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9IGVsZXZhdGlvbj17MX0+XG4gICAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7dXNlcm5hbWV9YH0+XG4gICAgICAgICAgICAgICAgICA8QXZhdGFyXG4gICAgICAgICAgICAgICAgICAgIGFsdD17bmFtZX1cbiAgICAgICAgICAgICAgICAgICAgc3JjPXthdmF0YXIgfHwgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZyd9XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgPC9QYXBlcj5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRpdGxlPXtcbiAgICAgICAgICAgICAgPElucHV0XG4gICAgICAgICAgICAgICAgdmFsdWU9e3RpdGxlfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfVxuICAgICAgICAgICAgb25Gb2N1cz17dGhpcy5vbkZvY3VzfVxuICAgICAgICAgIC8+XG4gICAgICAgICAge2FjdGl2ZSAmJiBtZXNzYWdlLmxlbmd0aCA/IChcbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHk+e21lc3NhZ2V9PC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICB7YWN0aXZlID8gKFxuICAgICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgICAgPEJ1dHRvbiByYWlzZWQgY29sb3I9XCJhY2NlbnRcIiBvbkNsaWNrPXt0aGlzLm9uU3VibWl0fT5cbiAgICAgICAgICAgICAgICBDcmVhdGVcbiAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICA8L0NhcmQ+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbkNvdXJzZUNyZWF0ZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgYWN0aW9uQ291cnNlQ3JlYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25Db3Vyc2VDcmVhdGUsXG4gICAgfSxcbiAgICBkaXNwYXRjaCxcbiAgKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShDb3Vyc2VDcmVhdGUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VDcmVhdGUvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9