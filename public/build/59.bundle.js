webpackJsonp([59],{

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./src/client/actions/courses/users/modules/create.js":
/*!************************************************************!*\
  !*** ./src/client/actions/courses/users/modules/create.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../../constants/courses */ "./src/client/constants/courses.js");

/**
 * This action will create a module is store
 * @function create
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.title -
 * @returns {Object}
 */
var create = function create(payload) {
  return { type: _courses.MODULE_CREATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/api/courses/users/modules/create.js":
/*!********************************************************!*\
  !*** ./src/client/api/courses/users/modules/create.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Create Modules
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {string} payload.title -
 * @return {Promise}
 *
 * @example
 */
/**
 * @file
 * @format
 * 
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId + '/modules';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/CoursePage/ModuleCreate.js":
/*!**********************************************************!*\
  !*** ./src/client/containers/CoursePage/ModuleCreate.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../actions/courses/users/modules/create */ "./src/client/actions/courses/users/modules/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../api/courses/users/modules/create */ "./src/client/api/courses/users/modules/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Define all styles here for ModuleCreate Component.
/**
 * ModuleCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create
 * post for their profile only.
 *
 * @format
 */

var styles = {
  root: {},
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

var ModuleCreate = function (_Component) {
  (0, _inherits3.default)(ModuleCreate, _Component);

  function ModuleCreate(props) {
    (0, _classCallCheck3.default)(this, ModuleCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ModuleCreate.__proto__ || (0, _getPrototypeOf2.default)(ModuleCreate)).call(this, props));

    _this.onFocus = function () {
      return _this.setState({ active: true });
    };

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Module Title...',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(ModuleCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      var title = e.target.value;
      var titleLength = title.length;
      var valid = titleLength > 0 && titleLength < 148;

      this.setState({
        title: title,
        titleLength: titleLength,
        valid: valid,
        error: !valid ? 'Title length should be in between 1 to 148.' : ''
      });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props, elements, course, _elements$user, userId, token, courseId, title, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, elements = _props.elements, course = _props.course;
                _elements$user = elements.user, userId = _elements$user.id, token = _elements$user.token;
                courseId = course.courseId;
                title = this.state.title;


                this.setState({ message: 'Creating course module...' });

                _context.next = 8;
                return (0, _create4.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  title: title
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 15;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 15:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionModuleCreate((0, _extends3.default)({
                  statusCode: statusCode,
                  authorId: userId
                }, payload, {
                  title: title
                }));
                _context.next = 23;
                break;

              case 20:
                _context.prev = 20;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 23:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 20]]);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          active = _state.active,
          placeholder = _state.placeholder,
          title = _state.title,
          message = _state.message;
      var _props2 = this.props,
          classes = _props2.classes,
          elements = _props2.elements;
      var _elements$user2 = elements.user,
          name = _elements$user2.name,
          avatar = _elements$user2.avatar,
          username = _elements$user2.username;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { className: classes.card, raised: this.state.hover },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { elevation: 1, className: classes.avatar },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: name,
                  src: avatar || '/public/photos/mayash-logo-transparent.png',
                  className: classes.avatar
                })
              )
            ),
            title: _react2.default.createElement(_Input2.default, {
              value: title,
              onChange: this.onChange,
              placeholder: placeholder
            }),
            className: classes.cardHeader,
            onFocus: this.onFocus
          }),
          active && message.length ? _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              null,
              message
            )
          ) : null,
          active ? _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onSubmit },
              'Create'
            )
          ) : null
        )
      );
    }
  }]);
  return ModuleCreate;
}(_react.Component);

ModuleCreate.propTypes = {
  /**
   * Classes object is comming from withStyles.
   */
  classes: _propTypes2.default.object.isRequired,

  /**
   * elements object is comming from Redux store.
   */
  elements: _propTypes2.default.object.isRequired,

  /**
   * Course Object contains course info and is coming from parent component.
   */
  course: _propTypes2.default.object.isRequired,

  /**
   * Action to Create New Course Module.
   */
  actionModuleCreate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionModuleCreate: _create2.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(ModuleCreate));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy9jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvY291cnNlcy91c2Vycy9tb2R1bGVzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvTW9kdWxlQ3JlYXRlLmpzIl0sIm5hbWVzIjpbImNyZWF0ZSIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJ1c2VySWQiLCJjb3Vyc2VJZCIsInRpdGxlIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJib2R5IiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInN0YXR1c0NvZGUiLCJlcnJvciIsImpzb24iLCJjb25zb2xlIiwic3R5bGVzIiwicm9vdCIsImRpc3BsYXkiLCJpbnB1dCIsImZsZXhHcm93IiwiYm9yZGVyIiwiYm9yZGVyUmFkaXVzIiwicGFkZGluZyIsIm91dGxpbmUiLCJib3JkZXJDb2xvciIsImJveFNoYWRvdyIsIklucHV0IiwiY2xhc3NlcyIsIm9uQ2hhbmdlIiwidmFsdWUiLCJwbGFjZWhvbGRlciIsImRpc2FibGVkIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJvbmVPZlR5cGUiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiY2FyZCIsImF2YXRhciIsImZsZXgiLCJNb2R1bGVDcmVhdGUiLCJwcm9wcyIsIm9uRm9jdXMiLCJzZXRTdGF0ZSIsImFjdGl2ZSIsIm9uTW91c2VFbnRlciIsImhvdmVyIiwib25Nb3VzZUxlYXZlIiwic3RhdGUiLCJ0aXRsZUxlbmd0aCIsInZhbGlkIiwibWVzc2FnZSIsImJpbmQiLCJvblN1Ym1pdCIsImUiLCJ0YXJnZXQiLCJsZW5ndGgiLCJlbGVtZW50cyIsImNvdXJzZSIsInVzZXIiLCJpZCIsInNldFRpbWVvdXQiLCJhY3Rpb25Nb2R1bGVDcmVhdGUiLCJhdXRob3JJZCIsIm5hbWUiLCJ1c2VybmFtZSIsImNhcmRIZWFkZXIiLCJtYXBTdGF0ZVRvUHJvcHMiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7QUFDOUY7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLGdDQUFnQztBQUNqRyxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUMvTXpFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDhDQUE4QyxpQkFBaUIscUJBQXFCLG9DQUFvQyw2REFBNkQsb0JBQW9CLEVBQUUsZUFBZTs7QUFFMU4saURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0ZBQWdGOztBQUVoRjs7QUFFQSxnRkFBZ0YsZUFBZTs7QUFFL0YseURBQXlELFVBQVUsdURBQXVEO0FBQzFIOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSx1Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hHQTs7QUFjQTs7Ozs7Ozs7OztBQVVBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLENBQWYsQyxDQTdCQTs7Ozs7a0JBK0JlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2ZmOzs7Ozs7Ozs7Ozs7O0FBaEJBOzs7Ozs7O3NGQTZCQTtBQUFBLFFBQXdCRyxLQUF4QixTQUF3QkEsS0FBeEI7QUFBQSxRQUErQkMsTUFBL0IsU0FBK0JBLE1BQS9CO0FBQUEsUUFBdUNDLFFBQXZDLFNBQXVDQSxRQUF2QztBQUFBLFFBQWlEQyxLQUFqRCxTQUFpREEsS0FBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNILE1BRnJDLGlCQUV1REMsUUFGdkQ7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUUsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxNQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVSO0FBSFIsZUFGa0I7QUFPM0JTLG9CQUFNLHlCQUFlLEVBQUVOLFlBQUYsRUFBZjtBQVBxQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFjWUMsa0JBZFosR0FjbUNELEdBZG5DLENBY1lDLE1BZFosRUFjb0JDLFVBZHBCLEdBY21DRixHQWRuQyxDQWNvQkUsVUFkcEI7O0FBQUEsa0JBZ0JRRCxVQUFVLEdBaEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FpQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFqQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXVCdUJGLElBQUlLLElBQUosRUF2QnZCOztBQUFBO0FBdUJVQSxnQkF2QlY7QUFBQSx3RUF5QmdCQSxJQXpCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTJCSUMsb0JBQVFGLEtBQVI7O0FBM0JKLDZDQTZCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE3Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVqQixNOzs7OztBQXZCZjs7OztBQUNBOzs7O2tCQTBEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFEZjs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1vQixTQUFTO0FBQ2JDLFFBQU07QUFDSkMsYUFBUztBQURMLEdBRE87QUFJYkMsU0FBTztBQUNMQyxjQUFVLEdBREw7QUFFTEMsWUFBUSxtQkFGSDtBQUdMQyxrQkFBYyxLQUhUO0FBSUxDLGFBQVMsS0FKSjtBQUtMLGVBQVc7QUFDVEMsZUFBUyxNQURBO0FBRVRDLG1CQUFhLFNBRko7QUFHVEMsaUJBQVc7QUFIRjtBQUxOO0FBSk0sQ0FBZixDLENBWkE7Ozs7Ozs7QUE2QkEsSUFBTUMsUUFBUSxTQUFSQSxLQUFRO0FBQUEsTUFBR0MsT0FBSCxRQUFHQSxPQUFIO0FBQUEsTUFBWUMsUUFBWixRQUFZQSxRQUFaO0FBQUEsTUFBc0JDLEtBQXRCLFFBQXNCQSxLQUF0QjtBQUFBLE1BQTZCQyxXQUE3QixRQUE2QkEsV0FBN0I7QUFBQSxNQUEwQ0MsUUFBMUMsUUFBMENBLFFBQTFDO0FBQUEsTUFBb0RsQyxJQUFwRCxRQUFvREEsSUFBcEQ7QUFBQSxTQUNaO0FBQUE7QUFBQSxNQUFLLFdBQVc4QixRQUFRWCxJQUF4QjtBQUNFO0FBQ0UsWUFBTW5CLFFBQVEsTUFEaEI7QUFFRSxtQkFBYWlDLGVBQWUsT0FGOUI7QUFHRSxhQUFPRCxLQUhUO0FBSUUsZ0JBQVVELFFBSlo7QUFLRSxpQkFBV0QsUUFBUVQsS0FMckI7QUFNRSxnQkFBVSxDQUFDLENBQUNhO0FBTmQ7QUFERixHQURZO0FBQUEsQ0FBZDs7QUFhQUwsTUFBTU0sU0FBTixHQUFrQjtBQUNoQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFEVjtBQUVoQk4sWUFBVSxvQkFBVU8sSUFBVixDQUFlRCxVQUZUO0FBR2hCTCxTQUFPLG9CQUFVTyxTQUFWLENBQW9CLENBQUMsb0JBQVVDLE1BQVgsRUFBbUIsb0JBQVVDLE1BQTdCLENBQXBCLEVBQTBESixVQUhqRDtBQUloQkosZUFBYSxvQkFBVU8sTUFKUDtBQUtoQnhDLFFBQU0sb0JBQVV3QyxNQUxBO0FBTWhCTixZQUFVLG9CQUFVUTtBQU5KLENBQWxCOztrQkFTZSwwQkFBV3hCLE1BQVgsRUFBbUJXLEtBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hDZjs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQUVBO0FBN0JBOzs7Ozs7Ozs7OztBQThCQSxJQUFNWCxTQUFTO0FBQ2JDLFFBQU0sRUFETztBQUVid0IsUUFBTTtBQUNKbkIsa0JBQWM7QUFEVixHQUZPO0FBS2JvQixVQUFRO0FBQ05wQixrQkFBYztBQURSLEdBTEs7QUFRYkYsWUFBVTtBQUNSdUIsVUFBTTtBQURFO0FBUkcsQ0FBZjs7SUFhTUMsWTs7O0FBQ0osd0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxrSkFDWEEsS0FEVzs7QUFBQSxVQWdGbkJDLE9BaEZtQixHQWdGVDtBQUFBLGFBQU0sTUFBS0MsUUFBTCxDQUFjLEVBQUVDLFFBQVEsSUFBVixFQUFkLENBQU47QUFBQSxLQWhGUzs7QUFBQSxVQWtGbkJDLFlBbEZtQixHQWtGSjtBQUFBLGFBQU0sTUFBS0YsUUFBTCxDQUFjLEVBQUVHLE9BQU8sSUFBVCxFQUFkLENBQU47QUFBQSxLQWxGSTs7QUFBQSxVQW1GbkJDLFlBbkZtQixHQW1GSjtBQUFBLGFBQU0sTUFBS0osUUFBTCxDQUFjLEVBQUVHLE9BQU8sS0FBVCxFQUFkLENBQU47QUFBQSxLQW5GSTs7QUFFakIsVUFBS0UsS0FBTCxHQUFhO0FBQ1hKLGNBQVEsS0FERztBQUVYOUMsYUFBTyxFQUZJO0FBR1htRCxtQkFBYSxDQUhGO0FBSVh0QixtQkFBYSxpQkFKRjs7QUFNWHVCLGFBQU8sS0FOSTtBQU9YMUMsa0JBQVksQ0FQRDtBQVFYQyxhQUFPLEVBUkk7QUFTWDBDLGVBQVMsRUFURTtBQVVYTCxhQUFPO0FBVkksS0FBYjs7QUFhQSxVQUFLckIsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWMyQixJQUFkLE9BQWhCO0FBQ0EsVUFBS0MsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWNELElBQWQsT0FBaEI7QUFoQmlCO0FBaUJsQjs7Ozs2QkFFUUUsQyxFQUFHO0FBQ1YsVUFBTXhELFFBQVF3RCxFQUFFQyxNQUFGLENBQVM3QixLQUF2QjtBQUNBLFVBQU11QixjQUFjbkQsTUFBTTBELE1BQTFCO0FBQ0EsVUFBTU4sUUFBUUQsY0FBYyxDQUFkLElBQW1CQSxjQUFjLEdBQS9DOztBQUVBLFdBQUtOLFFBQUwsQ0FBYztBQUNaN0Msb0JBRFk7QUFFWm1ELGdDQUZZO0FBR1pDLG9CQUhZO0FBSVp6QyxlQUFPLENBQUN5QyxLQUFELEdBQVMsNkNBQVQsR0FBeUQ7QUFKcEQsT0FBZDtBQU1EOzs7OzJHQUVjSSxDOzs7Ozs7Ozs7O3lCQUVrQixLQUFLYixLLEVBQTFCZ0IsUSxVQUFBQSxRLEVBQVVDLE0sVUFBQUEsTTtpQ0FDWUQsU0FBU0UsSSxFQUEzQi9ELE0sa0JBQUpnRSxFLEVBQVlqRSxLLGtCQUFBQSxLO0FBQ1pFLHdCLEdBQWE2RCxNLENBQWI3RCxRO0FBRUFDLHFCLEdBQVUsS0FBS2tELEssQ0FBZmxELEs7OztBQUVSLHFCQUFLNkMsUUFBTCxDQUFjLEVBQUVRLFNBQVMsMkJBQVgsRUFBZDs7O3VCQUU2QyxzQkFBZ0I7QUFDM0R4RCw4QkFEMkQ7QUFFM0RDLGdDQUYyRDtBQUczREMsb0NBSDJEO0FBSTNEQztBQUoyRCxpQkFBaEIsQzs7OztBQUFyQ1UsMEIsU0FBQUEsVTtBQUFZQyxxQixTQUFBQSxLO0FBQU9oQix1QixTQUFBQSxPOztzQkFPdkJlLGNBQWMsRzs7Ozs7QUFDaEI7QUFDQSxxQkFBS21DLFFBQUwsQ0FBYyxFQUFFbkMsc0JBQUYsRUFBY0MsWUFBZCxFQUFkOzs7OztBQUlGLHFCQUFLa0MsUUFBTCxDQUFjLEVBQUVRLFNBQVMsdUJBQVgsRUFBZDs7QUFFQVUsMkJBQVcsWUFBTTtBQUNmLHlCQUFLbEIsUUFBTCxDQUFjO0FBQ1pPLDJCQUFPLEtBREs7QUFFWjFDLGdDQUFZLENBRkE7QUFHWkMsMkJBQU8sRUFISztBQUlaMEMsNkJBQVMsRUFKRzs7QUFNWnJELDJCQUFPLEVBTks7QUFPWm1ELGlDQUFhO0FBUEQsbUJBQWQ7QUFTRCxpQkFWRCxFQVVHLElBVkg7O0FBWUEscUJBQUtSLEtBQUwsQ0FBV3FCLGtCQUFYO0FBQ0V0RCx3Q0FERjtBQUVFdUQsNEJBQVVuRTtBQUZaLG1CQUdLSCxPQUhMO0FBSUVLO0FBSkY7Ozs7Ozs7O0FBT0FhLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBU0s7QUFBQSxtQkFDeUMsS0FBS3VDLEtBRDlDO0FBQUEsVUFDQ0osTUFERCxVQUNDQSxNQUREO0FBQUEsVUFDU2pCLFdBRFQsVUFDU0EsV0FEVDtBQUFBLFVBQ3NCN0IsS0FEdEIsVUFDc0JBLEtBRHRCO0FBQUEsVUFDNkJxRCxPQUQ3QixVQUM2QkEsT0FEN0I7QUFBQSxvQkFFdUIsS0FBS1YsS0FGNUI7QUFBQSxVQUVDakIsT0FGRCxXQUVDQSxPQUZEO0FBQUEsVUFFVWlDLFFBRlYsV0FFVUEsUUFGVjtBQUFBLDRCQUc0QkEsU0FBU0UsSUFIckM7QUFBQSxVQUdDSyxJQUhELG1CQUdDQSxJQUhEO0FBQUEsVUFHTzFCLE1BSFAsbUJBR09BLE1BSFA7QUFBQSxVQUdlMkIsUUFIZixtQkFHZUEsUUFIZjs7O0FBS1AsYUFDRTtBQUFBO0FBQUE7QUFDRSxxQkFBV3pDLFFBQVFYLElBRHJCO0FBRUUsd0JBQWMsS0FBS2dDLFlBRnJCO0FBR0Usd0JBQWMsS0FBS0U7QUFIckI7QUFLRTtBQUFBO0FBQUEsWUFBTSxXQUFXdkIsUUFBUWEsSUFBekIsRUFBK0IsUUFBUSxLQUFLVyxLQUFMLENBQVdGLEtBQWxEO0FBQ0U7QUFDRSxvQkFDRTtBQUFBO0FBQUEsZ0JBQU8sV0FBVyxDQUFsQixFQUFxQixXQUFXdEIsUUFBUWMsTUFBeEM7QUFDRTtBQUFBO0FBQUEsa0JBQU0sV0FBUzJCLFFBQWY7QUFDRTtBQUNFLHVCQUFLRCxJQURQO0FBRUUsdUJBQUsxQixVQUFVLDRDQUZqQjtBQUdFLDZCQUFXZCxRQUFRYztBQUhyQjtBQURGO0FBREYsYUFGSjtBQVlFLG1CQUNFO0FBQ0UscUJBQU94QyxLQURUO0FBRUUsd0JBQVUsS0FBSzJCLFFBRmpCO0FBR0UsMkJBQWFFO0FBSGYsY0FiSjtBQW1CRSx1QkFBV0gsUUFBUTBDLFVBbkJyQjtBQW9CRSxxQkFBUyxLQUFLeEI7QUFwQmhCLFlBREY7QUF1QkdFLG9CQUFVTyxRQUFRSyxNQUFsQixHQUNDO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFhTDtBQUFiO0FBREYsV0FERCxHQUlHLElBM0JOO0FBNEJHUCxtQkFDQztBQUFBO0FBQUE7QUFDRSxtREFBSyxXQUFXcEIsUUFBUVIsUUFBeEIsR0FERjtBQUVFO0FBQUE7QUFBQSxnQkFBUSxZQUFSLEVBQWUsT0FBTSxRQUFyQixFQUE4QixTQUFTLEtBQUtxQyxRQUE1QztBQUFBO0FBQUE7QUFGRixXQURELEdBT0c7QUFuQ047QUFMRixPQURGO0FBNkNEOzs7OztBQUdIYixhQUFhWCxTQUFiLEdBQXlCO0FBQ3ZCOzs7QUFHQUwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFKSDs7QUFNdkI7OztBQUdBMEIsWUFBVSxvQkFBVTNCLE1BQVYsQ0FBaUJDLFVBVEo7O0FBV3ZCOzs7QUFHQTJCLFVBQVEsb0JBQVU1QixNQUFWLENBQWlCQyxVQWRGOztBQWdCdkI7OztBQUdBK0Isc0JBQW9CLG9CQUFVOUIsSUFBVixDQUFlRDtBQW5CWixDQUF6Qjs7QUFzQkEsSUFBTW9DLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHVixRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQixFQUFFQSxrQkFBRixFQUFuQjtBQUFBLENBQXhCOztBQUVBLElBQU1XLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFUDtBQURGLEdBREYsRUFJRU8sUUFKRixDQUR5QjtBQUFBLENBQTNCOztrQkFRZSx5QkFBUUYsZUFBUixFQUF5QkMsa0JBQXpCLEVBQ2IsMEJBQVd4RCxNQUFYLEVBQW1CNEIsWUFBbkIsQ0FEYSxDIiwiZmlsZSI6IjU5LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfY29sb3JNYW5pcHVsYXRvciA9IHJlcXVpcmUoJy4uL3N0eWxlcy9jb2xvck1hbmlwdWxhdG9yJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB3aWR0aDogNDAsXG4gICAgICBoZWlnaHQ6IDQwLFxuICAgICAgZm9udEZhbWlseTogdGhlbWUudHlwb2dyYXBoeS5mb250RmFtaWx5LFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyMCksXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckRlZmF1bHQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogKDAsIF9jb2xvck1hbmlwdWxhdG9yLmVtcGhhc2l6ZSkodGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsIDAuMjYpXG4gICAgfSxcbiAgICBpbWc6IHtcbiAgICAgIG1heFdpZHRoOiAnMTAwJScsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnYXV0bycsXG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZCBpbiBjb21iaW5hdGlvbiB3aXRoIGBzcmNgIG9yIGBzcmNTZXRgIHRvXG4gICAqIHByb3ZpZGUgYW4gYWx0IGF0dHJpYnV0ZSBmb3IgdGhlIHJlbmRlcmVkIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBhbHQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWQgdG8gcmVuZGVyIGljb24gb3IgdGV4dCBlbGVtZW50cyBpbnNpZGUgdGhlIEF2YXRhci5cbiAgICogYHNyY2AgYW5kIGBhbHRgIHByb3BzIHdpbGwgbm90IGJlIHVzZWQgYW5kIG5vIGBpbWdgIHdpbGxcbiAgICogYmUgcmVuZGVyZWQgYnkgZGVmYXVsdC5cbiAgICpcbiAgICogVGhpcyBjYW4gYmUgYW4gZWxlbWVudCwgb3IganVzdCBhIHN0cmluZy5cbiAgICovXG4gIGNoaWxkcmVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBUaGUgY2xhc3NOYW1lIG9mIHRoZSBjaGlsZCBlbGVtZW50LlxuICAgKiBVc2VkIGJ5IENoaXAgYW5kIExpc3RJdGVtSWNvbiB0byBzdHlsZSB0aGUgQXZhdGFyIGljb24uXG4gICAqL1xuICBjaGlsZHJlbkNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvcGVydGllcyBhcHBsaWVkIHRvIHRoZSBgaW1nYCBlbGVtZW50IHdoZW4gdGhlIGNvbXBvbmVudFxuICAgKiBpcyB1c2VkIHRvIGRpc3BsYXkgYW4gaW1hZ2UuXG4gICAqL1xuICBpbWdQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGBzaXplc2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNpemVzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyYzogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGBzcmNTZXRgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzcmNTZXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cbnZhciBBdmF0YXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShBdmF0YXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEF2YXRhcigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBBdmF0YXIpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChBdmF0YXIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEF2YXRhcikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQXZhdGFyLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFsdCA9IF9wcm9wcy5hbHQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcCA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjaGlsZHJlbkNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2hpbGRyZW5DbGFzc05hbWUsXG4gICAgICAgICAgQ29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgaW1nUHJvcHMgPSBfcHJvcHMuaW1nUHJvcHMsXG4gICAgICAgICAgc2l6ZXMgPSBfcHJvcHMuc2l6ZXMsXG4gICAgICAgICAgc3JjID0gX3Byb3BzLnNyYyxcbiAgICAgICAgICBzcmNTZXQgPSBfcHJvcHMuc3JjU2V0LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2FsdCcsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjaGlsZHJlbicsICdjaGlsZHJlbkNsYXNzTmFtZScsICdjb21wb25lbnQnLCAnaW1nUHJvcHMnLCAnc2l6ZXMnLCAnc3JjJywgJ3NyY1NldCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmNvbG9yRGVmYXVsdCwgY2hpbGRyZW5Qcm9wICYmICFzcmMgJiYgIXNyY1NldCksIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIGNoaWxkcmVuID0gbnVsbDtcblxuICAgICAgaWYgKGNoaWxkcmVuUHJvcCkge1xuICAgICAgICBpZiAoY2hpbGRyZW5DbGFzc05hbWVQcm9wICYmIHR5cGVvZiBjaGlsZHJlblByb3AgIT09ICdzdHJpbmcnICYmIF9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChjaGlsZHJlblByb3ApKSB7XG4gICAgICAgICAgdmFyIF9jaGlsZHJlbkNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2hpbGRyZW5DbGFzc05hbWVQcm9wLCBjaGlsZHJlblByb3AucHJvcHMuY2xhc3NOYW1lKTtcbiAgICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGRyZW5Qcm9wLCB7IGNsYXNzTmFtZTogX2NoaWxkcmVuQ2xhc3NOYW1lIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNoaWxkcmVuID0gY2hpbGRyZW5Qcm9wO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHNyYyB8fCBzcmNTZXQpIHtcbiAgICAgICAgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnaW1nJywgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgYWx0OiBhbHQsXG4gICAgICAgICAgc3JjOiBzcmMsXG4gICAgICAgICAgc3JjU2V0OiBzcmNTZXQsXG4gICAgICAgICAgc2l6ZXM6IHNpemVzLFxuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3Nlcy5pbWdcbiAgICAgICAgfSwgaW1nUHJvcHMpKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEF2YXRhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkF2YXRhci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbXBvbmVudDogJ2Rpdidcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQXZhdGFyJyB9KShBdmF0YXIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgNiAyNCAyNiAyNyAyOCAzMSAzMyAzNCAzNSAzNiAzNyAzOSA0MCA0MyA0OSA1NCA1NyA1OCA1OSA2MSA2MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9BdmF0YXIgPSByZXF1aXJlKCcuL0F2YXRhcicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BdmF0YXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA0IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2ludmFyaWFudCA9IHJlcXVpcmUoJ2ludmFyaWFudCcpO1xuXG52YXIgX2ludmFyaWFudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbnZhcmlhbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGlzTW9kaWZpZWRFdmVudCA9IGZ1bmN0aW9uIGlzTW9kaWZpZWRFdmVudChldmVudCkge1xuICByZXR1cm4gISEoZXZlbnQubWV0YUtleSB8fCBldmVudC5hbHRLZXkgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5zaGlmdEtleSk7XG59O1xuXG4vKipcbiAqIFRoZSBwdWJsaWMgQVBJIGZvciByZW5kZXJpbmcgYSBoaXN0b3J5LWF3YXJlIDxhPi5cbiAqL1xuXG52YXIgTGluayA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhMaW5rLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaW5rKCkge1xuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTGluayk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbC5hcHBseShfUmVhY3QkQ29tcG9uZW50LCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlQ2xpY2sgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkNsaWNrKSBfdGhpcy5wcm9wcy5vbkNsaWNrKGV2ZW50KTtcblxuICAgICAgaWYgKCFldmVudC5kZWZhdWx0UHJldmVudGVkICYmIC8vIG9uQ2xpY2sgcHJldmVudGVkIGRlZmF1bHRcbiAgICAgIGV2ZW50LmJ1dHRvbiA9PT0gMCAmJiAvLyBpZ25vcmUgcmlnaHQgY2xpY2tzXG4gICAgICAhX3RoaXMucHJvcHMudGFyZ2V0ICYmIC8vIGxldCBicm93c2VyIGhhbmRsZSBcInRhcmdldD1fYmxhbmtcIiBldGMuXG4gICAgICAhaXNNb2RpZmllZEV2ZW50KGV2ZW50KSAvLyBpZ25vcmUgY2xpY2tzIHdpdGggbW9kaWZpZXIga2V5c1xuICAgICAgKSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgIHZhciBoaXN0b3J5ID0gX3RoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeTtcbiAgICAgICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICAgICAgcmVwbGFjZSA9IF90aGlzJHByb3BzLnJlcGxhY2UsXG4gICAgICAgICAgICAgIHRvID0gX3RoaXMkcHJvcHMudG87XG5cblxuICAgICAgICAgIGlmIChyZXBsYWNlKSB7XG4gICAgICAgICAgICBoaXN0b3J5LnJlcGxhY2UodG8pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBoaXN0b3J5LnB1c2godG8pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgTGluay5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICByZXBsYWNlID0gX3Byb3BzLnJlcGxhY2UsXG4gICAgICAgIHRvID0gX3Byb3BzLnRvLFxuICAgICAgICBpbm5lclJlZiA9IF9wcm9wcy5pbm5lclJlZixcbiAgICAgICAgcHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzLCBbJ3JlcGxhY2UnLCAndG8nLCAnaW5uZXJSZWYnXSk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW51c2VkLXZhcnNcblxuICAgICgwLCBfaW52YXJpYW50Mi5kZWZhdWx0KSh0aGlzLmNvbnRleHQucm91dGVyLCAnWW91IHNob3VsZCBub3QgdXNlIDxMaW5rPiBvdXRzaWRlIGEgPFJvdXRlcj4nKTtcblxuICAgIHZhciBocmVmID0gdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5LmNyZWF0ZUhyZWYodHlwZW9mIHRvID09PSAnc3RyaW5nJyA/IHsgcGF0aG5hbWU6IHRvIH0gOiB0byk7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2EnLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgb25DbGljazogdGhpcy5oYW5kbGVDbGljaywgaHJlZjogaHJlZiwgcmVmOiBpbm5lclJlZiB9KSk7XG4gIH07XG5cbiAgcmV0dXJuIExpbms7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5MaW5rLnByb3BUeXBlcyA9IHtcbiAgb25DbGljazogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLFxuICB0YXJnZXQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIHRvOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XSkuaXNSZXF1aXJlZCxcbiAgaW5uZXJSZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jXSlcbn07XG5MaW5rLmRlZmF1bHRQcm9wcyA9IHtcbiAgcmVwbGFjZTogZmFsc2Vcbn07XG5MaW5rLmNvbnRleHRUeXBlcyA9IHtcbiAgcm91dGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICBoaXN0b3J5OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICAgIHB1c2g6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICBjcmVhdGVIcmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZFxuICAgIH0pLmlzUmVxdWlyZWRcbiAgfSkuaXNSZXF1aXJlZFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9IExpbms7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNiAyNyAyOCAzMCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA1NyA1OCA1OSA2MSIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBNT0RVTEVfQ1JFQVRFIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29uc3RhbnRzL2NvdXJzZXMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIG1vZHVsZUlkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBUaGlzIGFjdGlvbiB3aWxsIGNyZWF0ZSBhIG1vZHVsZSBpcyBzdG9yZVxuICogQGZ1bmN0aW9uIGNyZWF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYXV0aG9ySWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQubW9kdWxlSWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgY3JlYXRlID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHsgdHlwZTogTU9EVUxFX0NSRUFURSwgcGF5bG9hZCB9KTtcblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy9jcmVhdGUuanMiLCIvKipcbiAqIEBmaWxlXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHRva2VuOiBzdHJpbmcsXG4gIHVzZXJJZDogbnVtYmVyLFxuICBjb3Vyc2VJZDogbnVtYmVyLFxuICB0aXRsZTogc3RyaW5nLFxufTtcblxuLyoqXG4gKiBDcmVhdGUgTW9kdWxlc1xuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gY3JlYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHJldHVybiB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBjcmVhdGUoeyB0b2tlbiwgdXNlcklkLCBjb3Vyc2VJZCwgdGl0bGUgfTogUGF5bG9hZCkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vY291cnNlcy8ke2NvdXJzZUlkfS9tb2R1bGVzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHRpdGxlIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy9jcmVhdGUuanMiLCIvKipcbiAqIFRoaXMgaW5wdXQgY29tcG9uZW50IGlzIG1haW5seSBkZXZlbG9wZWQgZm9yIGNyZWF0ZSBwb3N0LCBjcmVhdGVcbiAqIGNvdXJzZSBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgfSxcbiAgaW5wdXQ6IHtcbiAgICBmbGV4R3JvdzogJzEnLFxuICAgIGJvcmRlcjogJzJweCBzb2xpZCAjZGFkYWRhJyxcbiAgICBib3JkZXJSYWRpdXM6ICc3cHgnLFxuICAgIHBhZGRpbmc6ICc4cHgnLFxuICAgICcmOmZvY3VzJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICcjOWVjYWVkJyxcbiAgICAgIGJveFNoYWRvdzogJzAgMCAxMHB4ICM5ZWNhZWQnLFxuICAgIH0sXG4gIH0sXG59O1xuXG5jb25zdCBJbnB1dCA9ICh7IGNsYXNzZXMsIG9uQ2hhbmdlLCB2YWx1ZSwgcGxhY2Vob2xkZXIsIGRpc2FibGVkLCB0eXBlIH0pID0+IChcbiAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgPGlucHV0XG4gICAgICB0eXBlPXt0eXBlIHx8ICd0ZXh0J31cbiAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlciB8fCAnaW5wdXQnfVxuICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgZGlzYWJsZWQ9eyEhZGlzYWJsZWR9XG4gICAgLz5cbiAgPC9kaXY+XG4pO1xuXG5JbnB1dC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIHZhbHVlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSkuaXNSZXF1aXJlZCxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShJbnB1dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCIvKipcbiAqIE1vZHVsZUNyZWF0ZSBjb21wb25lbnQgd2lsbCBiZSB1c2VkIHRvIGNyZWF0ZSBuZXcgcG9zdC5cbiAqXG4gKiBOb3RlOiBiZWZvcmUgYWRkaW5nIHRoaXMgY29tcG9uZW50IGluIGFueSBwbGFjZSwgbWFrZSBzdXJlXG4gKiB0aGUgcGVyc29uIHdobyBpcyBhY2Nlc3NpbmcgdGhpcyBjb21wb25lbnQgc2hvdWxkIGhhdmUgcmlnaHQgdG8gY3JlYXRlIHBvc3QuXG4gKiBpLmUuIHBlcnNvbiBzaG91bGQgYmUgbG9nZ2VkIGluIGFuZCB1c2VyIGNhbiBvbmx5IGNyZWF0ZVxuICogcG9zdCBmb3IgdGhlaXIgcHJvZmlsZSBvbmx5LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgQ2FyZCwgeyBDYXJkSGVhZGVyLCBDYXJkQ29udGVudCwgQ2FyZEFjdGlvbnMgfSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IFBhcGVyIGZyb20gJ21hdGVyaWFsLXVpL1BhcGVyJztcblxuaW1wb3J0IElucHV0IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvSW5wdXQnO1xuXG5pbXBvcnQgYWN0aW9uTW9kdWxlQ3JlYXRlIGZyb20gJy4uLy4uL2FjdGlvbnMvY291cnNlcy91c2Vycy9tb2R1bGVzL2NyZWF0ZSc7XG5pbXBvcnQgYXBpTW9kdWxlQ3JlYXRlIGZyb20gJy4uLy4uL2FwaS9jb3Vyc2VzL3VzZXJzL21vZHVsZXMvY3JlYXRlJztcblxuLy8gRGVmaW5lIGFsbCBzdHlsZXMgaGVyZSBmb3IgTW9kdWxlQ3JlYXRlIENvbXBvbmVudC5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge30sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59O1xuXG5jbGFzcyBNb2R1bGVDcmVhdGUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgYWN0aXZlOiBmYWxzZSxcbiAgICAgIHRpdGxlOiAnJyxcbiAgICAgIHRpdGxlTGVuZ3RoOiAwLFxuICAgICAgcGxhY2Vob2xkZXI6ICdNb2R1bGUgVGl0bGUuLi4nLFxuXG4gICAgICB2YWxpZDogZmFsc2UsXG4gICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgZXJyb3I6ICcnLFxuICAgICAgbWVzc2FnZTogJycsXG4gICAgICBob3ZlcjogZmFsc2UsXG4gICAgfTtcblxuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblN1Ym1pdCA9IHRoaXMub25TdWJtaXQuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKGUpIHtcbiAgICBjb25zdCB0aXRsZSA9IGUudGFyZ2V0LnZhbHVlO1xuICAgIGNvbnN0IHRpdGxlTGVuZ3RoID0gdGl0bGUubGVuZ3RoO1xuICAgIGNvbnN0IHZhbGlkID0gdGl0bGVMZW5ndGggPiAwICYmIHRpdGxlTGVuZ3RoIDwgMTQ4O1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB0aXRsZSxcbiAgICAgIHRpdGxlTGVuZ3RoLFxuICAgICAgdmFsaWQsXG4gICAgICBlcnJvcjogIXZhbGlkID8gJ1RpdGxlIGxlbmd0aCBzaG91bGQgYmUgaW4gYmV0d2VlbiAxIHRvIDE0OC4nIDogJycsXG4gICAgfSk7XG4gIH1cblxuICBhc3luYyBvblN1Ym1pdChlKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgZWxlbWVudHMsIGNvdXJzZSB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgICBjb25zdCB7IGNvdXJzZUlkIH0gPSBjb3Vyc2U7XG5cbiAgICAgIGNvbnN0IHsgdGl0bGUgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnQ3JlYXRpbmcgY291cnNlIG1vZHVsZS4uLicgfSk7XG5cbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IsIHBheWxvYWQgfSA9IGF3YWl0IGFwaU1vZHVsZUNyZWF0ZSh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIGNvdXJzZUlkLFxuICAgICAgICB0aXRsZSxcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgLy8gaGFuZGxlIGVycm9yXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBzdGF0dXNDb2RlLCBlcnJvciB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ1N1Y2Nlc3NmdWxseSBDcmVhdGVkLicgfSk7XG5cbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICB2YWxpZDogZmFsc2UsXG4gICAgICAgICAgc3RhdHVzQ29kZTogMCxcbiAgICAgICAgICBlcnJvcjogJycsXG4gICAgICAgICAgbWVzc2FnZTogJycsXG5cbiAgICAgICAgICB0aXRsZTogJycsXG4gICAgICAgICAgdGl0bGVMZW5ndGg6IDAsXG4gICAgICAgIH0pO1xuICAgICAgfSwgMTUwMCk7XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uTW9kdWxlQ3JlYXRlKHtcbiAgICAgICAgc3RhdHVzQ29kZSxcbiAgICAgICAgYXV0aG9ySWQ6IHVzZXJJZCxcbiAgICAgICAgLi4ucGF5bG9hZCxcbiAgICAgICAgdGl0bGUsXG4gICAgICB9KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgb25Gb2N1cyA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBhY3RpdmU6IHRydWUgfSk7XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGFjdGl2ZSwgcGxhY2Vob2xkZXIsIHRpdGxlLCBtZXNzYWdlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgY2xhc3NlcywgZWxlbWVudHMgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBuYW1lLCBhdmF0YXIsIHVzZXJuYW1lIH0gPSBlbGVtZW50cy51c2VyO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9XG4gICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5vbk1vdXNlRW50ZXJ9XG4gICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbk1vdXNlTGVhdmV9XG4gICAgICA+XG4gICAgICAgIDxDYXJkIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfSByYWlzZWQ9e3RoaXMuc3RhdGUuaG92ZXJ9PlxuICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICBhdmF0YXI9e1xuICAgICAgICAgICAgICA8UGFwZXIgZWxldmF0aW9uPXsxfSBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfT5cbiAgICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHt1c2VybmFtZX1gfT5cbiAgICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgICAgYWx0PXtuYW1lfVxuICAgICAgICAgICAgICAgICAgICBzcmM9e2F2YXRhciB8fCAnL3B1YmxpYy9waG90b3MvbWF5YXNoLWxvZ28tdHJhbnNwYXJlbnQucG5nJ31cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmF2YXRhcn1cbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICA8L1BhcGVyPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICA8SW5wdXRcbiAgICAgICAgICAgICAgICB2YWx1ZT17dGl0bGV9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2V9XG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmRIZWFkZXJ9XG4gICAgICAgICAgICBvbkZvY3VzPXt0aGlzLm9uRm9jdXN9XG4gICAgICAgICAgLz5cbiAgICAgICAgICB7YWN0aXZlICYmIG1lc3NhZ2UubGVuZ3RoID8gKFxuICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICA8VHlwb2dyYXBoeT57bWVzc2FnZX08L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgIHthY3RpdmUgPyAoXG4gICAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgICA8QnV0dG9uIHJhaXNlZCBjb2xvcj1cImFjY2VudFwiIG9uQ2xpY2s9e3RoaXMub25TdWJtaXR9PlxuICAgICAgICAgICAgICAgIENyZWF0ZVxuICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIDwvQ2FyZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuTW9kdWxlQ3JlYXRlLnByb3BUeXBlcyA9IHtcbiAgLyoqXG4gICAqIENsYXNzZXMgb2JqZWN0IGlzIGNvbW1pbmcgZnJvbSB3aXRoU3R5bGVzLlxuICAgKi9cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBlbGVtZW50cyBvYmplY3QgaXMgY29tbWluZyBmcm9tIFJlZHV4IHN0b3JlLlxuICAgKi9cbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQ291cnNlIE9iamVjdCBjb250YWlucyBjb3Vyc2UgaW5mbyBhbmQgaXMgY29taW5nIGZyb20gcGFyZW50IGNvbXBvbmVudC5cbiAgICovXG4gIGNvdXJzZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBBY3Rpb24gdG8gQ3JlYXRlIE5ldyBDb3Vyc2UgTW9kdWxlLlxuICAgKi9cbiAgYWN0aW9uTW9kdWxlQ3JlYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbk1vZHVsZUNyZWF0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKE1vZHVsZUNyZWF0ZSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvTW9kdWxlQ3JlYXRlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==