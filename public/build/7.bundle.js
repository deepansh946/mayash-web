webpackJsonp([7],{

/***/ "./node_modules/material-ui/AppBar/AppBar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/AppBar/AppBar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Paper

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      boxSizing: 'border-box', // Prevent padding issue with the Modal and fixed positioned AppBar.
      zIndex: theme.zIndex.appBar,
      flexShrink: 0
    },
    positionFixed: {
      position: 'fixed',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionAbsolute: {
      position: 'absolute',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionStatic: {
      position: 'static',
      flexShrink: 0
    },
    colorDefault: {
      backgroundColor: theme.palette.background.appBar,
      color: theme.palette.getContrastText(theme.palette.background.appBar)
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']);

var babelPluginFlowReactPropTypes_proptype_Position = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']).isRequired,

  /**
   * The positioning type.
   */
  position: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']).isRequired
};

var AppBar = function (_React$Component) {
  (0, _inherits3.default)(AppBar, _React$Component);

  function AppBar() {
    (0, _classCallCheck3.default)(this, AppBar);
    return (0, _possibleConstructorReturn3.default)(this, (AppBar.__proto__ || (0, _getPrototypeOf2.default)(AppBar)).apply(this, arguments));
  }

  (0, _createClass3.default)(AppBar, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          position = _props.position,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'position']);


      var className = (0, _classnames2.default)(classes.root, classes['position' + (0, _helpers.capitalizeFirstLetter)(position)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), (0, _defineProperty3.default)(_classNames, 'mui-fixed', position === 'fixed'), _classNames), classNameProp);

      return _react2.default.createElement(
        _Paper2.default,
        (0, _extends3.default)({ square: true, component: 'header', elevation: 4, className: className }, other),
        children
      );
    }
  }]);
  return AppBar;
}(_react2.default.Component);

AppBar.defaultProps = {
  color: 'primary',
  position: 'fixed'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAppBar' })(AppBar);

/***/ }),

/***/ "./node_modules/material-ui/AppBar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/AppBar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppBar = __webpack_require__(/*! ./AppBar */ "./node_modules/material-ui/AppBar/AppBar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_AppBar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Icon/Icon.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Icon/Icon.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      userSelect: 'none'
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The name of the icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired
};

var Icon = function (_React$Component) {
  (0, _inherits3.default)(Icon, _React$Component);

  function Icon() {
    (0, _classCallCheck3.default)(this, Icon);
    return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
  }

  (0, _createClass3.default)(Icon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color']);


      var className = (0, _classnames2.default)('material-icons', classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'span',
        (0, _extends3.default)({ className: className, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Icon;
}(_react2.default.Component);

Icon.defaultProps = {
  color: 'inherit'
};
Icon.muiName = 'Icon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIcon' })(Icon);

/***/ }),

/***/ "./node_modules/material-ui/Icon/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Icon/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/material-ui/Icon/Icon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Icon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Tabs/Tab.js":
/*!**********************************************!*\
  !*** ./node_modules/material-ui/Tabs/Tab.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends4 = _interopRequireDefault(_extends3);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent ButtonBase

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends4.default)({}, theme.typography.button, (0, _defineProperty3.default)({
      maxWidth: 264,
      position: 'relative',
      minWidth: 72,
      padding: 0,
      height: 48,
      flex: 'none',
      overflow: 'hidden'
    }, theme.breakpoints.up('md'), {
      minWidth: 160
    })),
    rootLabelIcon: {
      height: 72
    },
    rootAccent: {
      color: theme.palette.text.secondary
    },
    rootAccentSelected: {
      color: theme.palette.secondary.A200
    },
    rootAccentDisabled: {
      color: theme.palette.text.disabled
    },
    rootPrimary: {
      color: theme.palette.text.secondary
    },
    rootPrimarySelected: {
      color: theme.palette.primary[500]
    },
    rootPrimaryDisabled: {
      color: theme.palette.text.disabled
    },
    rootInherit: {
      color: 'inherit',
      opacity: 0.7
    },
    rootInheritSelected: {
      opacity: 1
    },
    rootInheritDisabled: {
      opacity: 0.4
    },
    fullWidth: {
      flexGrow: 1
    },
    wrapper: {
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      flexDirection: 'column'
    },
    labelContainer: (0, _defineProperty3.default)({
      paddingTop: 6,
      paddingBottom: 6,
      paddingLeft: 12,
      paddingRight: 12
    }, theme.breakpoints.up('md'), {
      paddingLeft: theme.spacing.unit * 3,
      paddingRight: theme.spacing.unit * 3
    }),
    label: (0, _defineProperty3.default)({
      fontSize: theme.typography.pxToRem(theme.typography.fontSize),
      whiteSpace: 'normal'
    }, theme.breakpoints.up('md'), {
      fontSize: theme.typography.pxToRem(theme.typography.fontSize - 1)
    }),
    labelWrapped: (0, _defineProperty3.default)({}, theme.breakpoints.down('md'), {
      fontSize: theme.typography.pxToRem(theme.typography.fontSize - 2)
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tab will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * The icon element. If a string is provided, it will be used as a font ligature.
   */
  icon: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * For server side rendering consideration, we let the selected tab
   * render the indicator.
   */
  indicator: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * The label element.
   */
  label: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  selected: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  textColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * You can provide your own value. Otherwise, we fallback to the child position index.
   */
  value: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any
};

var Tab = function (_React$Component) {
  (0, _inherits3.default)(Tab, _React$Component);

  function Tab() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Tab);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Tab.__proto__ || (0, _getPrototypeOf2.default)(Tab)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      wrappedText: false
    }, _this.handleChange = function (event) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          value = _this$props.value,
          onClick = _this$props.onClick;


      if (onChange) {
        onChange(event, value);
      }

      if (onClick) {
        onClick(event);
      }
    }, _this.label = undefined, _this.checkTextWrap = function () {
      if (_this.label) {
        var _wrappedText = _this.label.getClientRects().length > 1;
        if (_this.state.wrappedText !== _wrappedText) {
          _this.setState({ wrappedText: _wrappedText });
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Tab, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.checkTextWrap();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (this.state.wrappedText === prevState.wrappedText) {
        /**
         * At certain text and tab lengths, a larger font size may wrap to two lines while the smaller
         * font size still only requires one line.  This check will prevent an infinite render loop
         * fron occurring in that scenario.
         */
        this.checkTextWrap();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this,
          _classNames2;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          disabled = _props.disabled,
          fullWidth = _props.fullWidth,
          iconProp = _props.icon,
          indicator = _props.indicator,
          labelProp = _props.label,
          onChange = _props.onChange,
          selected = _props.selected,
          styleProp = _props.style,
          textColor = _props.textColor,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'disabled', 'fullWidth', 'icon', 'indicator', 'label', 'onChange', 'selected', 'style', 'textColor', 'value']);


      var icon = void 0;

      if (iconProp !== undefined) {
        icon = _react2.default.isValidElement(iconProp) ? iconProp : _react2.default.createElement(
          _Icon2.default,
          null,
          iconProp
        );
      }

      var label = void 0;

      if (labelProp !== undefined) {
        label = _react2.default.createElement(
          'div',
          { className: classes.labelContainer },
          _react2.default.createElement(
            'span',
            {
              className: (0, _classnames2.default)(classes.label, (0, _defineProperty3.default)({}, classes.labelWrapped, this.state.wrappedText)),
              ref: function ref(node) {
                _this2.label = node;
              }
            },
            labelProp
          )
        );
      }

      var className = (0, _classnames2.default)(classes.root, (_classNames2 = {}, (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor)], true), (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor) + 'Disabled'], disabled), (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor) + 'Selected'], selected), (0, _defineProperty3.default)(_classNames2, classes.rootLabelIcon, icon && label), (0, _defineProperty3.default)(_classNames2, classes.fullWidth, fullWidth), _classNames2), classNameProp);

      var style = {};

      if (textColor !== 'accent' && textColor !== 'inherit') {
        style.color = textColor;
      }

      style = (0, _keys2.default)(style).length > 0 ? (0, _extends4.default)({}, style, styleProp) : styleProp;

      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends4.default)({
          focusRipple: true,
          className: className,
          style: style,
          role: 'tab',
          'aria-selected': selected,
          disabled: disabled
        }, other, {
          onClick: this.handleChange
        }),
        _react2.default.createElement(
          'span',
          { className: classes.wrapper },
          icon,
          label
        ),
        indicator
      );
    }
  }]);
  return Tab;
}(_react2.default.Component);

Tab.defaultProps = {
  disabled: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTab' })(Tab);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/TabIndicator.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/Tabs/TabIndicator.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _ref; //  weak

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      height: 2,
      bottom: 0,
      width: '100%',
      transition: theme.transitions.create(),
      willChange: 'left, width'
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_IndicatorStyle = {
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
};
var babelPluginFlowReactPropTypes_proptype_ProvidedProps = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired
};
var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   * The color of the tab indicator.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired,

  /**
   * @ignore
   * The style of the root element.
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }).isRequired
};


/**
 * @ignore - internal component.
 */
function TabIndicator(props) {
  var classes = props.classes,
      classNameProp = props.className,
      color = props.color,
      styleProp = props.style;

  var colorPredefined = ['primary', 'accent'].indexOf(color) !== -1;
  var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], colorPredefined), classNameProp);

  var style = colorPredefined ? styleProp : (0, _extends3.default)({}, styleProp, {
    backgroundColor: color
  });

  return _react2.default.createElement('div', { className: className, style: style });
}

TabIndicator.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), (0, _defineProperty3.default)(_ref, 'color', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired), (0, _defineProperty3.default)(_ref, 'style', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}).isRequired), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTabIndicator' })(TabIndicator);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/TabScrollButton.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Tabs/TabScrollButton.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _KeyboardArrowLeft = __webpack_require__(/*! ../svg-icons/KeyboardArrowLeft */ "./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js");

var _KeyboardArrowLeft2 = _interopRequireDefault(_KeyboardArrowLeft);

var _KeyboardArrowRight = __webpack_require__(/*! ../svg-icons/KeyboardArrowRight */ "./node_modules/material-ui/svg-icons/KeyboardArrowRight.js");

var _KeyboardArrowRight2 = _interopRequireDefault(_KeyboardArrowRight);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//  weak

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      color: 'inherit',
      flex: '0 0 ' + theme.spacing.unit * 7 + 'px'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Which direction should the button indicate?
   */
  direction: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left', 'right']).isRequired,

  /**
   * Callback to execute for button press.
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Should the button be present or just consume space.
   */
  visible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var _ref = _react2.default.createElement(_KeyboardArrowLeft2.default, null);

var _ref2 = _react2.default.createElement(_KeyboardArrowRight2.default, null);

/**
 * @ignore - internal component.
 */
var TabScrollButton = function (_React$Component) {
  (0, _inherits3.default)(TabScrollButton, _React$Component);

  function TabScrollButton() {
    (0, _classCallCheck3.default)(this, TabScrollButton);
    return (0, _possibleConstructorReturn3.default)(this, (TabScrollButton.__proto__ || (0, _getPrototypeOf2.default)(TabScrollButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(TabScrollButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          direction = _props.direction,
          onClick = _props.onClick,
          visible = _props.visible,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'direction', 'onClick', 'visible']);


      var className = (0, _classnames2.default)(classes.root, classNameProp);

      if (!visible) {
        return _react2.default.createElement('div', { className: className });
      }

      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({ className: className, onClick: onClick, tabIndex: -1 }, other),
        direction === 'left' ? _ref : _ref2
      );
    }
  }]);
  return TabScrollButton;
}(_react2.default.Component);

TabScrollButton.defaultProps = {
  visible: true
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTabScrollButton' })(TabScrollButton);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/Tabs.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Tabs/Tabs.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _isNan = __webpack_require__(/*! babel-runtime/core-js/number/is-nan */ "./node_modules/babel-runtime/core-js/number/is-nan.js");

var _isNan2 = _interopRequireDefault(_isNan);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _debounce = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");

var _debounce2 = _interopRequireDefault(_debounce);

var _reactScrollbarSize = __webpack_require__(/*! react-scrollbar-size */ "./node_modules/react-scrollbar-size/index.js");

var _reactScrollbarSize2 = _interopRequireDefault(_reactScrollbarSize);

var _normalizeScrollLeft = __webpack_require__(/*! normalize-scroll-left */ "./node_modules/normalize-scroll-left/lib/main.js");

var _scroll = __webpack_require__(/*! scroll */ "./node_modules/scroll/index.js");

var _scroll2 = _interopRequireDefault(_scroll);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _TabIndicator = __webpack_require__(/*! ./TabIndicator */ "./node_modules/material-ui/Tabs/TabIndicator.js");

var _TabIndicator2 = _interopRequireDefault(_TabIndicator);

var _TabScrollButton = __webpack_require__(/*! ./TabScrollButton */ "./node_modules/material-ui/Tabs/TabScrollButton.js");

var _TabScrollButton2 = _interopRequireDefault(_TabScrollButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ComponentType = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_IndicatorStyle = __webpack_require__(/*! ./TabIndicator */ "./node_modules/material-ui/Tabs/TabIndicator.js").babelPluginFlowReactPropTypes_proptype_IndicatorStyle || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      overflow: 'hidden',
      minHeight: 48,
      WebkitOverflowScrolling: 'touch' // Add iOS momentum scrolling.
    },
    flexContainer: {
      display: 'flex'
    },
    scrollingContainer: {
      position: 'relative',
      display: 'inline-block',
      flex: '1 1 auto',
      whiteSpace: 'nowrap'
    },
    fixed: {
      overflowX: 'hidden',
      width: '100%'
    },
    scrollable: {
      overflowX: 'scroll'
    },
    centered: {
      justifyContent: 'center'
    },
    buttonAuto: (0, _defineProperty3.default)({}, theme.breakpoints.down('sm'), {
      display: 'none'
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the scroll button elements.
   */
  buttonClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tabs will be centered.
   * This property is intended for large views.
   */
  centered: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tabs will grow to use all the available space.
   * This property is intended for small views, like on mobile.
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The CSS class name of the indicator element.
   */
  indicatorClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Determines the color of the indicator.
   */
  indicatorColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired,

  /**
   * Callback fired when the value changes.
   *
   * @param {object} event The event source of the callback
   * @param {number} value We default to the index of the child
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func.isRequired,

  /**
   * True invokes scrolling properties and allow for horizontally scrolling
   * (or swiping) the tab bar.
   */
  scrollable: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Determine behavior of scroll buttons when tabs are set to scroll
   * `auto` will only present them on medium and larger viewports
   * `on` will always present them
   * `off` will never present them
   */
  scrollButtons: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto', 'on', 'off']).isRequired,

  /**
   * The component used to render the scroll buttons.
   */
  TabScrollButton: typeof babelPluginFlowReactPropTypes_proptype_ComponentType === 'function' ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired : babelPluginFlowReactPropTypes_proptype_ComponentType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ComponentType).isRequired,

  /**
   * Determines the color of the `Tab`.
   */
  textColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent', 'primary', 'inherit']).isRequired,

  /**
   * The value of the currently selected `Tab`.
   * If you don't want any selected `Tab`, you can set this property to `false`.
   */
  value: function value(props, propName, componentName) {
    if (!Object.prototype.hasOwnProperty.call(props, propName)) {
      throw new Error('Prop `' + propName + '` has type \'any\' or \'mixed\', but was not provided to `' + componentName + '`. Pass undefined or any other value.');
    }
  }
};
var babelPluginFlowReactPropTypes_proptype_TabsMeta = {
  clientWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollLeft: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollLeftNormalized: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,

  // ClientRect
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  right: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
};

var Tabs = function (_React$Component) {
  (0, _inherits3.default)(Tabs, _React$Component);

  function Tabs() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Tabs);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Tabs.__proto__ || (0, _getPrototypeOf2.default)(Tabs)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      indicatorStyle: {},
      scrollerStyle: {
        marginBottom: 0
      },
      showLeftScroll: false,
      showRightScroll: false,
      mounted: false
    }, _this.getConditionalElements = function () {
      var _this$props = _this.props,
          classes = _this$props.classes,
          buttonClassName = _this$props.buttonClassName,
          scrollable = _this$props.scrollable,
          scrollButtons = _this$props.scrollButtons,
          TabScrollButtonProp = _this$props.TabScrollButton,
          theme = _this$props.theme;

      var conditionalElements = {};
      conditionalElements.scrollbarSizeListener = scrollable ? _react2.default.createElement(_reactScrollbarSize2.default, {
        onLoad: _this.handleScrollbarSizeChange,
        onChange: _this.handleScrollbarSizeChange
      }) : null;

      var showScrollButtons = scrollable && (scrollButtons === 'auto' || scrollButtons === 'on');

      conditionalElements.scrollButtonLeft = showScrollButtons ? _react2.default.createElement(TabScrollButtonProp, {
        direction: theme && theme.direction === 'rtl' ? 'right' : 'left',
        onClick: _this.handleLeftScrollClick,
        visible: _this.state.showLeftScroll,
        className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.buttonAuto, scrollButtons === 'auto'), buttonClassName)
      }) : null;

      conditionalElements.scrollButtonRight = showScrollButtons ? _react2.default.createElement(TabScrollButtonProp, {
        direction: theme && theme.direction === 'rtl' ? 'left' : 'right',
        onClick: _this.handleRightScrollClick,
        visible: _this.state.showRightScroll,
        className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.buttonAuto, scrollButtons === 'auto'), buttonClassName)
      }) : null;

      return conditionalElements;
    }, _this.getTabsMeta = function (value, direction) {
      var tabsMeta = void 0;
      if (_this.tabs) {
        var rect = _this.tabs.getBoundingClientRect();
        // create a new object with ClientRect class props + scrollLeft
        tabsMeta = {
          clientWidth: _this.tabs ? _this.tabs.clientWidth : 0,
          scrollLeft: _this.tabs ? _this.tabs.scrollLeft : 0,
          scrollLeftNormalized: _this.tabs ? (0, _normalizeScrollLeft.getNormalizedScrollLeft)(_this.tabs, direction) : 0,
          scrollWidth: _this.tabs ? _this.tabs.scrollWidth : 0,
          left: rect.left,
          right: rect.right
        };
      }

      var tabMeta = void 0;
      if (_this.tabs && value !== false) {
        var _children = _this.tabs.children[0].children;

        if (_children.length > 0) {
          var tab = _children[_this.valueToIndex[value]];
           true ? (0, _warning2.default)(Boolean(tab), 'Material-UI: the value provided `' + value + '` is invalid') : void 0;
          tabMeta = tab ? tab.getBoundingClientRect() : null;
        }
      }
      return { tabsMeta: tabsMeta, tabMeta: tabMeta };
    }, _this.tabs = undefined, _this.valueToIndex = {}, _this.handleResize = (0, _debounce2.default)(function () {
      _this.updateIndicatorState(_this.props);
      _this.updateScrollButtonState();
    }, 166), _this.handleLeftScrollClick = function () {
      if (_this.tabs) {
        _this.moveTabsScroll(-_this.tabs.clientWidth);
      }
    }, _this.handleRightScrollClick = function () {
      if (_this.tabs) {
        _this.moveTabsScroll(_this.tabs.clientWidth);
      }
    }, _this.handleScrollbarSizeChange = function (_ref2) {
      var scrollbarHeight = _ref2.scrollbarHeight;

      _this.setState({
        scrollerStyle: {
          marginBottom: -scrollbarHeight
        }
      });
    }, _this.handleTabsScroll = (0, _debounce2.default)(function () {
      _this.updateScrollButtonState();
    }, 166), _this.moveTabsScroll = function (delta) {
      var theme = _this.props.theme;


      if (_this.tabs) {
        var themeDirection = theme && theme.direction;
        var multiplier = themeDirection === 'rtl' ? -1 : 1;
        var nextScrollLeft = _this.tabs.scrollLeft + delta * multiplier;
        // Fix for Edge
        var invert = themeDirection === 'rtl' && (0, _normalizeScrollLeft.detectScrollType)() === 'reverse' ? -1 : 1;
        _scroll2.default.left(_this.tabs, invert * nextScrollLeft);
      }
    }, _this.scrollSelectedIntoView = function () {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          value = _this$props2.value;


      var themeDirection = theme && theme.direction;

      var _this$getTabsMeta = _this.getTabsMeta(value, themeDirection),
          tabsMeta = _this$getTabsMeta.tabsMeta,
          tabMeta = _this$getTabsMeta.tabMeta;

      if (!tabMeta || !tabsMeta) {
        return;
      }

      if (tabMeta.left < tabsMeta.left) {
        // left side of button is out of view
        var nextScrollLeft = tabsMeta.scrollLeft + (tabMeta.left - tabsMeta.left);
        _scroll2.default.left(_this.tabs, nextScrollLeft);
      } else if (tabMeta.right > tabsMeta.right) {
        // right side of button is out of view
        var _nextScrollLeft = tabsMeta.scrollLeft + (tabMeta.right - tabsMeta.right);
        _scroll2.default.left(_this.tabs, _nextScrollLeft);
      }
    }, _this.updateScrollButtonState = function () {
      var _this$props3 = _this.props,
          scrollable = _this$props3.scrollable,
          scrollButtons = _this$props3.scrollButtons,
          theme = _this$props3.theme;

      var themeDirection = theme && theme.direction;

      if (_this.tabs && scrollable && scrollButtons !== 'off') {
        var _this$tabs = _this.tabs,
            _scrollWidth = _this$tabs.scrollWidth,
            _clientWidth = _this$tabs.clientWidth;

        var _scrollLeft = (0, _normalizeScrollLeft.getNormalizedScrollLeft)(_this.tabs, themeDirection);

        var _showLeftScroll = themeDirection === 'rtl' ? _scrollWidth > _clientWidth + _scrollLeft : _scrollLeft > 0;

        var _showRightScroll = themeDirection === 'rtl' ? _scrollLeft > 0 : _scrollWidth > _clientWidth + _scrollLeft;

        if (_showLeftScroll !== _this.state.showLeftScroll || _showRightScroll !== _this.state.showRightScroll) {
          _this.setState({ showLeftScroll: _showLeftScroll, showRightScroll: _showRightScroll });
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Tabs, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState({ mounted: true });
      this.updateIndicatorState(this.props);
      this.updateScrollButtonState();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      this.updateScrollButtonState();

      // The index might have changed at the same time.
      // We need to check again the right indicator position.
      this.updateIndicatorState(this.props);

      if (this.state.indicatorStyle !== prevState.indicatorStyle) {
        this.scrollSelectedIntoView();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.handleResize.cancel();
      this.handleTabsScroll.cancel();
    }
  }, {
    key: 'updateIndicatorState',
    value: function updateIndicatorState(props) {
      var theme = props.theme,
          value = props.value;


      var themeDirection = theme && theme.direction;

      var _getTabsMeta = this.getTabsMeta(value, themeDirection),
          tabsMeta = _getTabsMeta.tabsMeta,
          tabMeta = _getTabsMeta.tabMeta;

      var left = 0;

      if (tabMeta && tabsMeta) {
        var correction = themeDirection === 'rtl' ? tabsMeta.scrollLeftNormalized + tabsMeta.clientWidth - tabsMeta.scrollWidth : tabsMeta.scrollLeft;
        left = tabMeta.left - tabsMeta.left + correction;
      }

      var indicatorStyle = {
        left: left,
        // May be wrong until the font is loaded.
        width: tabMeta ? tabMeta.width : 0
      };

      if ((indicatorStyle.left !== this.state.indicatorStyle.left || indicatorStyle.width !== this.state.indicatorStyle.width) && !(0, _isNan2.default)(indicatorStyle.left) && !(0, _isNan2.default)(indicatorStyle.width)) {
        this.setState({ indicatorStyle: indicatorStyle });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames3,
          _this2 = this;

      var _props = this.props,
          buttonClassName = _props.buttonClassName,
          centered = _props.centered,
          classes = _props.classes,
          childrenProp = _props.children,
          classNameProp = _props.className,
          fullWidth = _props.fullWidth,
          indicatorClassName = _props.indicatorClassName,
          indicatorColor = _props.indicatorColor,
          onChange = _props.onChange,
          scrollable = _props.scrollable,
          scrollButtons = _props.scrollButtons,
          TabScrollButtonProp = _props.TabScrollButton,
          textColor = _props.textColor,
          theme = _props.theme,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonClassName', 'centered', 'classes', 'children', 'className', 'fullWidth', 'indicatorClassName', 'indicatorColor', 'onChange', 'scrollable', 'scrollButtons', 'TabScrollButton', 'textColor', 'theme', 'value']);


      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var scrollerClassName = (0, _classnames2.default)(classes.scrollingContainer, (_classNames3 = {}, (0, _defineProperty3.default)(_classNames3, classes.fixed, !scrollable), (0, _defineProperty3.default)(_classNames3, classes.scrollable, scrollable), _classNames3));
      var tabItemContainerClassName = (0, _classnames2.default)(classes.flexContainer, (0, _defineProperty3.default)({}, classes.centered, centered && !scrollable));

      var indicator = _react2.default.createElement(_TabIndicator2.default, {
        style: this.state.indicatorStyle,
        className: indicatorClassName,
        color: indicatorColor
      });

      this.valueToIndex = {};
      var childIndex = 0;
      var children = _react2.default.Children.map(childrenProp, function (child) {
        if (!_react2.default.isValidElement(child)) {
          return null;
        }

        var childValue = child.props.value || childIndex;
        _this2.valueToIndex[childValue] = childIndex;
        var selected = childValue === value;

        childIndex += 1;
        return _react2.default.cloneElement(child, {
          fullWidth: fullWidth,
          indicator: selected && !_this2.state.mounted && indicator,
          selected: selected,
          onChange: onChange,
          textColor: textColor,
          value: childValue
        });
      });

      var conditionalElements = this.getConditionalElements();

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }),
        conditionalElements.scrollbarSizeListener,
        _react2.default.createElement(
          'div',
          { className: classes.flexContainer },
          conditionalElements.scrollButtonLeft,
          _react2.default.createElement(
            'div',
            {
              className: scrollerClassName,
              style: this.state.scrollerStyle,
              ref: function ref(node) {
                _this2.tabs = node;
              },
              role: 'tablist',
              onScroll: this.handleTabsScroll
            },
            _react2.default.createElement(
              'div',
              { className: tabItemContainerClassName },
              children
            ),
            this.state.mounted && indicator
          ),
          conditionalElements.scrollButtonRight
        )
      );
    }
  }]);
  return Tabs;
}(_react2.default.Component);

Tabs.defaultProps = {
  centered: false,
  fullWidth: false,
  indicatorColor: 'accent',
  scrollable: false,
  scrollButtons: 'auto',
  TabScrollButton: _TabScrollButton2.default,
  textColor: 'inherit'
};
exports.default = (0, _withStyles2.default)(styles, { withTheme: true, name: 'MuiTabs' })(Tabs);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Tabs/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Tabs = __webpack_require__(/*! ./Tabs */ "./node_modules/material-ui/Tabs/Tabs.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Tabs).default;
  }
});

var _Tab = __webpack_require__(/*! ./Tab */ "./node_modules/material-ui/Tabs/Tab.js");

Object.defineProperty(exports, 'Tab', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Tab).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/pure.js":
/*!*****************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/pure.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/material-ui/node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/material-ui/node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js":
/*!***************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/setDisplayName.js ***!
  \***************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/material-ui/node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/setStatic.js":
/*!**********************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/setStatic.js ***!
  \**********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/shallowEqual.js":
/*!*************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/shallowEqual.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/shouldUpdate.js":
/*!*************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/shouldUpdate.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _react.createFactory)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js":
/*!*****************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z' });

var KeyboardArrowLeft = function KeyboardArrowLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
KeyboardArrowLeft = (0, _pure2.default)(KeyboardArrowLeft);
KeyboardArrowLeft.muiName = 'SvgIcon';

exports.default = KeyboardArrowLeft;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/KeyboardArrowRight.js":
/*!******************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/KeyboardArrowRight.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' });

var KeyboardArrowRight = function KeyboardArrowRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
KeyboardArrowRight = (0, _pure2.default)(KeyboardArrowRight);
KeyboardArrowRight.muiName = 'SvgIcon';

exports.default = KeyboardArrowRight;

/***/ }),

/***/ "./node_modules/normalize-scroll-left/lib/main.js":
/*!********************************************************!*\
  !*** ./node_modules/normalize-scroll-left/lib/main.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// Based on https://github.com/react-bootstrap/dom-helpers/blob/master/src/util/inDOM.js
var inDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);
var cachedType;
function _setScrollType(type) {
    cachedType = type;
}
exports._setScrollType = _setScrollType;
// Based on the jquery plugin https://github.com/othree/jquery.rtl-scroll-type
function detectScrollType() {
    if (cachedType) {
        return cachedType;
    }
    if (!inDOM || !window.document.body) {
        return 'indeterminate';
    }
    var dummy = window.document.createElement('div');
    dummy.appendChild(document.createTextNode('ABCD'));
    dummy.dir = 'rtl';
    dummy.style.fontSize = '14px';
    dummy.style.width = '4px';
    dummy.style.height = '1px';
    dummy.style.position = 'absolute';
    dummy.style.top = '-1000px';
    dummy.style.overflow = 'scroll';
    document.body.appendChild(dummy);
    cachedType = 'reverse';
    if (dummy.scrollLeft > 0) {
        cachedType = 'default';
    }
    else {
        dummy.scrollLeft = 1;
        if (dummy.scrollLeft === 0) {
            cachedType = 'negative';
        }
    }
    document.body.removeChild(dummy);
    return cachedType;
}
exports.detectScrollType = detectScrollType;
// Based on https://stackoverflow.com/a/24394376
function getNormalizedScrollLeft(element, direction) {
    var scrollLeft = element.scrollLeft;
    // Perform the calculations only when direction is rtl to avoid messing up the ltr bahavior
    if (direction !== 'rtl') {
        return scrollLeft;
    }
    var type = detectScrollType();
    if (type === 'indeterminate') {
        return Number.NaN;
    }
    switch (type) {
        case 'negative':
            return element.scrollWidth - element.clientWidth + scrollLeft;
        case 'reverse':
            return element.scrollWidth - element.clientWidth - scrollLeft;
    }
    return scrollLeft;
}
exports.getNormalizedScrollLeft = getNormalizedScrollLeft;
function setNormalizedScrollLeft(element, scrollLeft, direction) {
    // Perform the calculations only when direction is rtl to avoid messing up the ltr bahavior
    if (direction !== 'rtl') {
        element.scrollLeft = scrollLeft;
        return;
    }
    var type = detectScrollType();
    if (type === 'indeterminate') {
        return;
    }
    switch (type) {
        case 'negative':
            element.scrollLeft = element.clientWidth - element.scrollWidth + scrollLeft;
            break;
        case 'reverse':
            element.scrollLeft = element.scrollWidth - element.clientWidth - scrollLeft;
            break;
        default:
            element.scrollLeft = scrollLeft;
            break;
    }
}
exports.setNormalizedScrollLeft = setNormalizedScrollLeft;


/***/ }),

/***/ "./node_modules/rafl/index.js":
/*!************************************!*\
  !*** ./node_modules/rafl/index.js ***!
  \************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(/*! global */ "./node_modules/global/window.js")

/**
 * `requestAnimationFrame()`
 */

var request = global.requestAnimationFrame
  || global.webkitRequestAnimationFrame
  || global.mozRequestAnimationFrame
  || fallback

var prev = +new Date
function fallback (fn) {
  var curr = +new Date
  var ms = Math.max(0, 16 - (curr - prev))
  var req = setTimeout(fn, ms)
  return prev = curr, req
}

/**
 * `cancelAnimationFrame()`
 */

var cancel = global.cancelAnimationFrame
  || global.webkitCancelAnimationFrame
  || global.mozCancelAnimationFrame
  || clearTimeout

if (Function.prototype.bind) {
  request = request.bind(global)
  cancel = cancel.bind(global)
}

exports = module.exports = request
exports.cancel = cancel


/***/ }),

/***/ "./node_modules/react-scrollbar-size/ScrollbarSize.js":
/*!************************************************************!*\
  !*** ./node_modules/react-scrollbar-size/ScrollbarSize.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _stifle = __webpack_require__(/*! stifle */ "./node_modules/stifle/index.js");

var _stifle2 = _interopRequireDefault(_stifle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	width: '100px',
	height: '100px',
	position: 'absolute',
	top: '-100000px',
	overflow: 'scroll',
	msOverflowStyle: 'scrollbar'
};

var ScrollbarSize = function (_Component) {
	(0, _inherits3.default)(ScrollbarSize, _Component);

	function ScrollbarSize() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, ScrollbarSize);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = ScrollbarSize.__proto__ || (0, _getPrototypeOf2.default)(ScrollbarSize)).call.apply(_ref, [this].concat(args))), _this), _this.setMeasurements = function () {
			_this.scrollbarHeight = _this.node.offsetHeight - _this.node.clientHeight;
			_this.scrollbarWidth = _this.node.offsetWidth - _this.node.clientWidth;
		}, _this.handleResize = (0, _stifle2.default)(function () {
			var onChange = _this.props.onChange;


			var prevHeight = _this.scrollbarHeight;
			var prevWidth = _this.scrollbarWidth;
			_this.setMeasurements();
			if (prevHeight !== _this.scrollbarHeight || prevWidth !== _this.scrollbarWidth) {
				onChange({ scrollbarHeight: _this.scrollbarHeight, scrollbarWidth: _this.scrollbarWidth });
			}
		}, 166), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(ScrollbarSize, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var onLoad = this.props.onLoad;


			if (onLoad) {
				this.setMeasurements();
				onLoad({ scrollbarHeight: this.scrollbarHeight, scrollbarWidth: this.scrollbarWidth });
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			this.handleResize.cancel();
		}
	}, {
		key: 'render',
		// Corresponds to 10 frames at 60 Hz.

		value: function render() {
			var _this2 = this;

			var onChange = this.props.onChange;


			return _react2.default.createElement(
				'div',
				null,
				onChange ? _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }) : null,
				_react2.default.createElement('div', {
					style: styles,
					ref: function ref(node) {
						_this2.node = node;
					}
				})
			);
		}
	}]);
	return ScrollbarSize;
}(_react.Component);

ScrollbarSize.defaultProps = {
	onLoad: null,
	onChange: null
};
exports.default = ScrollbarSize;

/***/ }),

/***/ "./node_modules/react-scrollbar-size/index.js":
/*!****************************************************!*\
  !*** ./node_modules/react-scrollbar-size/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ScrollbarSize = __webpack_require__(/*! ./ScrollbarSize */ "./node_modules/react-scrollbar-size/ScrollbarSize.js");

var _ScrollbarSize2 = _interopRequireDefault(_ScrollbarSize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ScrollbarSize2.default;

/***/ }),

/***/ "./node_modules/scroll/index.js":
/*!**************************************!*\
  !*** ./node_modules/scroll/index.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var raf = __webpack_require__(/*! rafl */ "./node_modules/rafl/index.js")

function scroll (prop, element, to, options, callback) {
  var start = +new Date
  var from = element[prop]
  var cancelled = false

  var ease = inOutSine
  var duration = 350

  if (typeof options === 'function') {
    callback = options
  }
  else {
    options = options || {}
    ease = options.ease || ease
    duration = options.duration || duration
    callback = callback || function () {}
  }

  if (from === to) {
    return callback(
      new Error('Element already at target scroll position'),
      element[prop]
    )
  }

  function cancel () {
    cancelled = true
  }

  function animate (timestamp) {
    if (cancelled) {
      return callback(
        new Error('Scroll cancelled'),
        element[prop]
      )
    }

    var now = +new Date
    var time = Math.min(1, ((now - start) / duration))
    var eased = ease(time)

    element[prop] = (eased * (to - from)) + from

    time < 1 ? raf(animate) : raf(function () {
      callback(null, element[prop])
    })
  }

  raf(animate)

  return cancel
}

function inOutSine (n) {
  return .5 * (1 - Math.cos(Math.PI * n))
}

module.exports = {
  top: function (element, to, options, callback) {
    return scroll('scrollTop', element, to, options, callback)
  },
  left: function (element, to, options, callback) {
    return scroll('scrollLeft', element, to, options, callback)
  }
}


/***/ }),

/***/ "./node_modules/stifle/index.js":
/*!**************************************!*\
  !*** ./node_modules/stifle/index.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

module.exports = stifle;


function stifle (fn, wait) {
  if (typeof fn !== 'function' || typeof wait !== 'number') {
    throw new Error('stifle(fn, wait) -- expected a function and number of milliseconds, got (' + typeof fn + ', ' + typeof wait + ')');
  }

  var timer;    // Timer to fire after `wait` has elapsed
  var called;   // Keep track if it gets called during the `wait`

  var wrapper = function () {

    // Check if still "cooling down" from a previous call
    if (timer) {
      called = true;
    } else {
      // Start a timer to fire after the `wait` is over
      timer = setTimeout(afterWait, wait);
      // And call the wrapped function
      fn();
    }
  }

  // Add a cancel method, to kill and pending calls
  wrapper.cancel = function () {
    // Clear the called flag, or it would fire twice when called again later
    called = false;

    // Turn off the timer, so it won't fire after the wait expires
    if (timer) {
      clearTimeout(timer);
      timer = undefined;
    }
  }

  function afterWait() {
    // Empty out the timer
    timer = undefined;

    // If it was called during the `wait`, fire it again
    if (called) {
      called = false;
      wrapper();
    }
  }

  return wrapper;
}


/***/ }),

/***/ "./src/client/actions/elements/getById.js":
/*!************************************************!*\
  !*** ./src/client/actions/elements/getById.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

var _getById = __webpack_require__(/*! ../../api/elements/getById */ "./src/client/api/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @return {Object}
 */
/** @format */

var getElementStart = function getElementStart(payload) {
  return { type: _elements.ELEMENT_GET_START, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType
 * @param {string} payload.circleType - only if elementType is 'circle'
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @return {Object}
 */
var getElementSuccess = function getElementSuccess(payload) {
  return { type: _elements.ELEMENT_GET_SUCCESS, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
var getElementError = function getElementError(payload) {
  return { type: _elements.ELEMENT_GET_ERROR, payload: payload };
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
var get = function get(_ref) {
  var token = _ref.token,
      id = _ref.id;
  return function (dispatch) {
    dispatch(getElementStart({ id: id }));

    (0, _getById2.default)({ token: token, id: id }).then(function (_ref2) {
      var statusCode = _ref2.statusCode,
          error = _ref2.error,
          payload = _ref2.payload;

      if (statusCode >= 300) {
        dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));

        return;
      }

      dispatch(getElementSuccess((0, _extends3.default)({ id: id, statusCode: statusCode }, payload)));
    }).catch(function (_ref3) {
      var statusCode = _ref3.statusCode,
          error = _ref3.error;

      dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));
    });
  };
};

exports.default = get;

/***/ }),

/***/ "./src/client/actions/users/update.js":
/*!********************************************!*\
  !*** ./src/client/actions/users/update.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _update = __webpack_require__(/*! ../../constants/users/update */ "./src/client/constants/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * User update action.
 * User can update their info in redux store by it's 'id' only.
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @param {Object} payload.resume -
 *
 * @returns {Object}
 */
var update = function update(payload) {
  return { type: _update2.default, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = update;

/***/ }),

/***/ "./src/client/api/elements/getById.js":
/*!********************************************!*\
  !*** ./src/client/api/elements/getById.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @function getById
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
/** @format */

var getById = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        id = _ref2.id;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getById;

/***/ }),

/***/ "./src/client/constants/users/update.js":
/*!**********************************************!*\
  !*** ./src/client/constants/users/update.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains action constants for User's update
 *
 * @format
 */

var USER_UPDATE = 'USER_UPDATE';

exports.default = USER_UPDATE;

/***/ }),

/***/ "./src/client/containers/ProfilePage/index.js":
/*!****************************************************!*\
  !*** ./src/client/containers/ProfilePage/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _reactRouterDom = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _AppBar = __webpack_require__(/*! material-ui/AppBar */ "./node_modules/material-ui/AppBar/index.js");

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Tabs = __webpack_require__(/*! material-ui/Tabs */ "./node_modules/material-ui/Tabs/index.js");

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _update = __webpack_require__(/*! ../../actions/users/update */ "./src/client/actions/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * profile page component
 * It will render  Account, Billing, Account, Password etc.
 *
 * @format
 */

var AsyncProfile = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(67).then(__webpack_require__.bind(null, /*! ./Profile */ "./src/client/containers/ProfilePage/Profile.js"));
  },
  modules: ['./Profile'],
  loading: _Loading2.default
});

var AsyncAccount = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(69).then(__webpack_require__.bind(null, /*! ./Account */ "./src/client/containers/ProfilePage/Account.js"));
  },
  modules: ['./Account'],
  loading: _Loading2.default
});

var AsyncBilling = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(76).then(__webpack_require__.bind(null, /*! ./Billing */ "./src/client/containers/ProfilePage/Billing.js"));
  },
  modules: ['./Billing'],
  loading: _Loading2.default
});

var AsyncEmail = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(75).then(__webpack_require__.bind(null, /*! ./Email */ "./src/client/containers/ProfilePage/Email.js"));
  },
  modules: ['./Email'],
  loading: _Loading2.default
});

var AsyncPassword = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(62).then(__webpack_require__.bind(null, /*! ./Password */ "./src/client/containers/ProfilePage/Password.js"));
  },
  modules: ['./Password'],
  loading: _Loading2.default
});

var AsyncApplyForGuru = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(70).then(__webpack_require__.bind(null, /*! ./ApplyForGuru */ "./src/client/containers/ProfilePage/ApplyForGuru.js"));
  },
  modules: ['./ApplyForGuru'],
  loading: _Loading2.default
});

var AsyncErrorPage = (0, _reactLoadable2.default)({
  loader: function loader() {
    return new Promise(function(resolve) { resolve(); }).then(__webpack_require__.bind(null, /*! ../../components/ErrorPage */ "./src/client/components/ErrorPage.js"));
  },
  modules: ['../../components/ErrorPage'],
  loading: _Loading2.default
});

var styles = {
  root: {
    flex: '1 0 100%'
  }
};

// this function will return index of route url in Dashboard component page.
var routeStates = function routeStates() {
  var route = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '/profile';

  var routes = {
    '/profile': 0,
    '/profile/account': 1,
    '/profile/billing': 2,
    '/profile/email': 3,
    '/profile/password': 4
  };

  // if routes[index] value is undefined, it will return routes.length;
  return typeof routes[route] !== 'undefined' ? routes[route] : (0, _keys2.default)(routes).length;
};

var ProfilePage = function (_Component) {
  (0, _inherits3.default)(ProfilePage, _Component);

  function ProfilePage(props) {
    (0, _classCallCheck3.default)(this, ProfilePage);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ProfilePage.__proto__ || (0, _getPrototypeOf2.default)(ProfilePage)).call(this, props));

    _this.state = {
      value: routeStates(props.location.pathname)
    };

    _this.onChange = _this.onChange.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(ProfilePage, [{
    key: 'onChange',
    value: function onChange(event, value) {
      this.setState({ value: value });

      var history = this.props.history;


      switch (value) {
        case 0:
          history.push('/profile');
          break;
        case 1:
          history.push('/profile/account');
          break;
        case 2:
          history.push('/profile/billing');
          break;
        case 3:
          history.push('/profile/email');
          break;
        case 4:
          history.push('/profile/password');
          break;
        case 5:
          history.push('/profile/guru');
          break;
        default:
          history.push('/profile');
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements;

      var user = elements.user;

      var value = this.state.value;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _AppBar2.default,
          { position: 'static', color: 'default' },
          _react2.default.createElement(
            _Tabs2.default,
            {
              value: value,
              onChange: this.onChange,
              indicatorColor: 'primary',
              textColor: 'primary',
              scrollable: true,
              scrollButtons: 'auto'
            },
            _react2.default.createElement(_Tabs.Tab, { label: 'Profile' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Account' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Billing', disabled: true }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Email' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Password' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Guru' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Not Found', style: { display: 'none' } })
          )
        ),
        _react2.default.createElement(
          _reactRouterDom.Switch,
          null,
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/profile',
            component: function component(nextProps) {
              return _react2.default.createElement(AsyncProfile, (0, _extends3.default)({}, nextProps, {
                user: user,
                userUpdate: _this2.props.actionUserUpdate
              }));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/profile/account',
            component: function component(nextProps) {
              return _react2.default.createElement(AsyncAccount, (0, _extends3.default)({}, nextProps, {
                user: user,
                userUpdate: _this2.props.actionUserUpdate
              }));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, { exact: true, path: '/profile/billing', component: AsyncBilling }),
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/profile/email',
            component: function component(nextProps) {
              return _react2.default.createElement(AsyncEmail, (0, _extends3.default)({}, nextProps, {
                user: user,
                userUpdate: _this2.props.actionUserUpdate
              }));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/profile/password',
            component: function component(nextProps) {
              return _react2.default.createElement(AsyncPassword, (0, _extends3.default)({}, nextProps, { user: user }));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/profile/guru',
            component: function component(nextProps) {
              return _react2.default.createElement(AsyncApplyForGuru, (0, _extends3.default)({}, nextProps, { user: user }));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, { component: AsyncErrorPage })
        )
      );
    }
  }]);
  return ProfilePage;
}(_react.Component);

ProfilePage.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  location: _propTypes2.default.shape({
    pathname: _propTypes2.default.string.isRequired
  }).isRequired,
  history: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  actionElementGet: _propTypes2.default.func.isRequired,
  actionUserUpdate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref) {
  var elements = _ref.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionElementGet: _getById2.default,
    actionUserUpdate: _update2.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(ProfilePage));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL0FwcEJhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiSW5kaWNhdG9yLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYlNjcm9sbEJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9LZXlib2FyZEFycm93TGVmdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dSaWdodC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLXNjcm9sbC1sZWZ0L2xpYi9tYWluLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yYWZsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9TY3JvbGxiYXJTaXplLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc2Nyb2xsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zdGlmbGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2VsZW1lbnRzL2dldEJ5SWQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL3VzZXJzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRCeUlkLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29uc3RhbnRzL3VzZXJzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUHJvZmlsZVBhZ2UvaW5kZXguanMiXSwibmFtZXMiOlsiZ2V0RWxlbWVudFN0YXJ0IiwicGF5bG9hZCIsInR5cGUiLCJnZXRFbGVtZW50U3VjY2VzcyIsImdldEVsZW1lbnRFcnJvciIsImdldCIsInRva2VuIiwiaWQiLCJkaXNwYXRjaCIsInRoZW4iLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJjYXRjaCIsInVwZGF0ZSIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsImpzb24iLCJjb25zb2xlIiwiZ2V0QnlJZCIsIlVTRVJfVVBEQVRFIiwiQXN5bmNQcm9maWxlIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBc3luY0FjY291bnQiLCJBc3luY0JpbGxpbmciLCJBc3luY0VtYWlsIiwiQXN5bmNQYXNzd29yZCIsIkFzeW5jQXBwbHlGb3JHdXJ1IiwiQXN5bmNFcnJvclBhZ2UiLCJzdHlsZXMiLCJyb290IiwiZmxleCIsInJvdXRlU3RhdGVzIiwicm91dGUiLCJyb3V0ZXMiLCJsZW5ndGgiLCJQcm9maWxlUGFnZSIsInByb3BzIiwic3RhdGUiLCJ2YWx1ZSIsImxvY2F0aW9uIiwicGF0aG5hbWUiLCJvbkNoYW5nZSIsImJpbmQiLCJldmVudCIsInNldFN0YXRlIiwiaGlzdG9yeSIsInB1c2giLCJjbGFzc2VzIiwiZWxlbWVudHMiLCJ1c2VyIiwiZGlzcGxheSIsIm5leHRQcm9wcyIsImFjdGlvblVzZXJVcGRhdGUiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic2hhcGUiLCJzdHJpbmciLCJhY3Rpb25FbGVtZW50R2V0IiwiZnVuYyIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1hcERpc3BhdGNoVG9Qcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxvSkFBb0o7O0FBRXBKO0FBQ0E7QUFDQSxnQ0FBZ0Msd0VBQXdFO0FBQ3hHO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUM1S3pFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxnSEFBZ0g7O0FBRWhIO0FBQ0E7QUFDQSxnQ0FBZ0MsOENBQThDO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsa0JBQWtCLFE7Ozs7Ozs7Ozs7Ozs7QUM5SXZFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4Rjs7QUFFOUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxxQkFBcUIsVzs7Ozs7Ozs7Ozs7OztBQ2xMMUU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsa0RBQWtEO0FBQ2xEO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQiw0QkFBNEI7QUFDdEQ7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxvQ0FBb0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnRkFBZ0Y7O0FBRWhGOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwrRUFBK0U7O0FBRS9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVyw2QkFBNkI7QUFDeEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsaUJBQWlCLE87Ozs7Ozs7Ozs7Ozs7QUN4V3RFOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsU0FBUzs7QUFFVDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDBGQUEwRjs7QUFFMUYscUVBQXFFO0FBQ3JFO0FBQ0EsR0FBRzs7QUFFSCwrQ0FBK0MscUNBQXFDO0FBQ3BGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBLENBQUM7QUFDRCxxREFBcUQsMEJBQTBCLGdCOzs7Ozs7Ozs7Ozs7O0FDaEgvRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBLHFEQUFxRCx1QkFBdUI7QUFDNUU7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1REFBdUQ7QUFDdkY7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsNkJBQTZCLG1COzs7Ozs7Ozs7Ozs7O0FDakpsRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsZ0RBQWdEO0FBQ2hEO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0Esd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZFQUE2RTtBQUM3RSxPQUFPOztBQUVQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkVBQTZFO0FBQzdFLE9BQU87O0FBRVA7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZCxLQUFLLGlEQUFpRDtBQUN0RDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBLDBCQUEwQixxRUFBcUU7QUFDL0Y7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixnQkFBZ0I7QUFDckM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx1QkFBdUIsaUNBQWlDO0FBQ3hEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQSxzR0FBc0c7QUFDdEcsdUhBQXVIOztBQUV2SDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87O0FBRVA7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQscUVBQXFFLGdEQUFnRDtBQUNySDtBQUNBO0FBQ0E7QUFDQSxXQUFXLG1DQUFtQztBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGVBQWUsdUNBQXVDO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxtQ0FBbUMsUTs7Ozs7Ozs7Ozs7OztBQ2ppQnhGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUN4QjdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDckRBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxrREFBa0QsMERBQTBEOztBQUU1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsb0M7Ozs7Ozs7Ozs7Ozs7QUNuQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCx3REFBd0Q7O0FBRTFHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ25DQTtBQUNBLDhDQUE4QyxjQUFjO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbkZBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLGlFQUFpRSxhQUFhO0FBQzlFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsK0VBQStFO0FBQzdGO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsWUFBWSw2RUFBNkU7QUFDekY7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDRFQUE0RSxnREFBZ0Q7QUFDNUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDOzs7Ozs7Ozs7Ozs7O0FDaklBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RiwwQzs7Ozs7Ozs7Ozs7O0FDWkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFlBQVk7QUFDWixhQUFhOztBQUViOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOUNBOztBQU1BOzs7Ozs7QUFFQTs7Ozs7O0FBVkE7O0FBZ0JBLElBQU1BLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ0MsT0FBRDtBQUFBLFNBQWMsRUFBRUMsaUNBQUYsRUFBMkJELGdCQUEzQixFQUFkO0FBQUEsQ0FBeEI7O0FBRUE7Ozs7Ozs7Ozs7Ozs7QUFhQSxJQUFNRSxvQkFBb0IsU0FBcEJBLGlCQUFvQixDQUFDRixPQUFEO0FBQUEsU0FBYyxFQUFFQyxtQ0FBRixFQUE2QkQsZ0JBQTdCLEVBQWQ7QUFBQSxDQUExQjs7QUFFQTs7Ozs7Ozs7QUFRQSxJQUFNRyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUNILE9BQUQ7QUFBQSxTQUFjLEVBQUVDLGlDQUFGLEVBQTJCRCxnQkFBM0IsRUFBZDtBQUFBLENBQXhCOztBQUVBOzs7Ozs7O0FBT0EsSUFBTUksTUFBTSxTQUFOQSxHQUFNO0FBQUEsTUFBR0MsS0FBSCxRQUFHQSxLQUFIO0FBQUEsTUFBVUMsRUFBVixRQUFVQSxFQUFWO0FBQUEsU0FBbUIsVUFBQ0MsUUFBRCxFQUFjO0FBQzNDQSxhQUFTUixnQkFBZ0IsRUFBRU8sTUFBRixFQUFoQixDQUFUOztBQUVBLDJCQUFXLEVBQUVELFlBQUYsRUFBU0MsTUFBVCxFQUFYLEVBQ0dFLElBREgsQ0FDUSxpQkFBb0M7QUFBQSxVQUFqQ0MsVUFBaUMsU0FBakNBLFVBQWlDO0FBQUEsVUFBckJDLEtBQXFCLFNBQXJCQSxLQUFxQjtBQUFBLFVBQWRWLE9BQWMsU0FBZEEsT0FBYzs7QUFDeEMsVUFBSVMsY0FBYyxHQUFsQixFQUF1QjtBQUNyQkYsaUJBQVNKLGdCQUFnQixFQUFFRyxNQUFGLEVBQU1HLHNCQUFOLEVBQWtCQyxZQUFsQixFQUFoQixDQUFUOztBQUVBO0FBQ0Q7O0FBRURILGVBQVNMLDJDQUFvQkksTUFBcEIsRUFBd0JHLHNCQUF4QixJQUF1Q1QsT0FBdkMsRUFBVDtBQUNELEtBVEgsRUFVR1csS0FWSCxDQVVTLGlCQUEyQjtBQUFBLFVBQXhCRixVQUF3QixTQUF4QkEsVUFBd0I7QUFBQSxVQUFaQyxLQUFZLFNBQVpBLEtBQVk7O0FBQ2hDSCxlQUFTSixnQkFBZ0IsRUFBRUcsTUFBRixFQUFNRyxzQkFBTixFQUFrQkMsWUFBbEIsRUFBaEIsQ0FBVDtBQUNELEtBWkg7QUFhRCxHQWhCVztBQUFBLENBQVo7O2tCQWtCZU4sRzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9EZjs7Ozs7O0FBZ0JBOzs7Ozs7Ozs7Ozs7OztBQWNBLElBQU1RLFNBQVMsU0FBVEEsTUFBUyxDQUFDWixPQUFEO0FBQUEsU0FBK0IsRUFBRUMsc0JBQUYsRUFBcUJELGdCQUFyQixFQUEvQjtBQUFBLENBQWYsQyxDQW5DQTs7Ozs7a0JBcUNlWSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaENmOzs7Ozs7O0FBTEE7OztzRkFZQTtBQUFBLFFBQXlCUCxLQUF6QixTQUF5QkEsS0FBekI7QUFBQSxRQUFnQ0MsRUFBaEMsU0FBZ0NBLEVBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVPLGVBRlYscUNBRXdDUCxFQUZ4QztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNTyxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVo7QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVhLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xWLDBCQUFZVSxNQURQO0FBRUxULHFCQUFPVTtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJRyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRWixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlYSxPOzs7OztBQVZmOzs7O0FBQ0E7Ozs7a0JBNENlQSxPOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQ2Y7Ozs7OztBQU1BLElBQU1DLGNBQWMsYUFBcEI7O2tCQUVlQSxXOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0RmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBckJBOzs7Ozs7O0FBdUJBLElBQU1DLGVBQWUsNkJBQVM7QUFDNUJDLFVBQVE7QUFBQSxXQUFNLCtJQUFOO0FBQUEsR0FEb0I7QUFFNUJDLFdBQVMsQ0FBQyxXQUFELENBRm1CO0FBRzVCQztBQUg0QixDQUFULENBQXJCOztBQU1BLElBQU1DLGVBQWUsNkJBQVM7QUFDNUJILFVBQVE7QUFBQSxXQUFNLCtJQUFOO0FBQUEsR0FEb0I7QUFFNUJDLFdBQVMsQ0FBQyxXQUFELENBRm1CO0FBRzVCQztBQUg0QixDQUFULENBQXJCOztBQU1BLElBQU1FLGVBQWUsNkJBQVM7QUFDNUJKLFVBQVE7QUFBQSxXQUFNLCtJQUFOO0FBQUEsR0FEb0I7QUFFNUJDLFdBQVMsQ0FBQyxXQUFELENBRm1CO0FBRzVCQztBQUg0QixDQUFULENBQXJCOztBQU1BLElBQU1HLGFBQWEsNkJBQVM7QUFDMUJMLFVBQVE7QUFBQSxXQUFNLDJJQUFOO0FBQUEsR0FEa0I7QUFFMUJDLFdBQVMsQ0FBQyxTQUFELENBRmlCO0FBRzFCQztBQUgwQixDQUFULENBQW5COztBQU1BLElBQU1JLGdCQUFnQiw2QkFBUztBQUM3Qk4sVUFBUTtBQUFBLFdBQU0saUpBQU47QUFBQSxHQURxQjtBQUU3QkMsV0FBUyxDQUFDLFlBQUQsQ0FGb0I7QUFHN0JDO0FBSDZCLENBQVQsQ0FBdEI7O0FBTUEsSUFBTUssb0JBQW9CLDZCQUFTO0FBQ2pDUCxVQUFRO0FBQUEsV0FBTSx5SkFBTjtBQUFBLEdBRHlCO0FBRWpDQyxXQUFTLENBQUMsZ0JBQUQsQ0FGd0I7QUFHakNDO0FBSGlDLENBQVQsQ0FBMUI7O0FBTUEsSUFBTU0saUJBQWlCLDZCQUFTO0FBQzlCUixVQUFRO0FBQUEsV0FBTSw0SkFBTjtBQUFBLEdBRHNCO0FBRTlCQyxXQUFTLENBQUMsNEJBQUQsQ0FGcUI7QUFHOUJDO0FBSDhCLENBQVQsQ0FBdkI7O0FBTUEsSUFBTU8sU0FBUztBQUNiQyxRQUFNO0FBQ0pDLFVBQU07QUFERjtBQURPLENBQWY7O0FBTUE7QUFDQSxJQUFNQyxjQUFjLFNBQWRBLFdBQWMsR0FBd0I7QUFBQSxNQUF2QkMsS0FBdUIsdUVBQWYsVUFBZTs7QUFDMUMsTUFBTUMsU0FBUztBQUNiLGdCQUFZLENBREM7QUFFYix3QkFBb0IsQ0FGUDtBQUdiLHdCQUFvQixDQUhQO0FBSWIsc0JBQWtCLENBSkw7QUFLYix5QkFBcUI7QUFMUixHQUFmOztBQVFBO0FBQ0EsU0FBTyxPQUFPQSxPQUFPRCxLQUFQLENBQVAsS0FBeUIsV0FBekIsR0FDSEMsT0FBT0QsS0FBUCxDQURHLEdBRUgsb0JBQVlDLE1BQVosRUFBb0JDLE1BRnhCO0FBR0QsQ0FiRDs7SUFlTUMsVzs7O0FBQ0osdUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxnSkFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hDLGFBQU9QLFlBQVlLLE1BQU1HLFFBQU4sQ0FBZUMsUUFBM0I7QUFESSxLQUFiOztBQUlBLFVBQUtDLFFBQUwsR0FBZ0IsTUFBS0EsUUFBTCxDQUFjQyxJQUFkLE9BQWhCO0FBTmlCO0FBT2xCOzs7OzZCQUVRQyxLLEVBQU9MLEssRUFBTztBQUNyQixXQUFLTSxRQUFMLENBQWMsRUFBRU4sWUFBRixFQUFkOztBQURxQixVQUdiTyxPQUhhLEdBR0QsS0FBS1QsS0FISixDQUdiUyxPQUhhOzs7QUFLckIsY0FBUVAsS0FBUjtBQUNFLGFBQUssQ0FBTDtBQUNFTyxrQkFBUUMsSUFBUixDQUFhLFVBQWI7QUFDQTtBQUNGLGFBQUssQ0FBTDtBQUNFRCxrQkFBUUMsSUFBUixDQUFhLGtCQUFiO0FBQ0E7QUFDRixhQUFLLENBQUw7QUFDRUQsa0JBQVFDLElBQVIsQ0FBYSxrQkFBYjtBQUNBO0FBQ0YsYUFBSyxDQUFMO0FBQ0VELGtCQUFRQyxJQUFSLENBQWEsZ0JBQWI7QUFDQTtBQUNGLGFBQUssQ0FBTDtBQUNFRCxrQkFBUUMsSUFBUixDQUFhLG1CQUFiO0FBQ0E7QUFDRixhQUFLLENBQUw7QUFDRUQsa0JBQVFDLElBQVIsQ0FBYSxlQUFiO0FBQ0E7QUFDRjtBQUNFRCxrQkFBUUMsSUFBUixDQUFhLFVBQWI7QUFwQko7QUFzQkQ7Ozs2QkFFUTtBQUFBOztBQUFBLG1CQUN1QixLQUFLVixLQUQ1QjtBQUFBLFVBQ0NXLE9BREQsVUFDQ0EsT0FERDtBQUFBLFVBQ1VDLFFBRFYsVUFDVUEsUUFEVjs7QUFFUCxVQUFNQyxPQUFPRCxTQUFTQyxJQUF0Qjs7QUFGTyxVQUlDWCxLQUpELEdBSVcsS0FBS0QsS0FKaEIsQ0FJQ0MsS0FKRDs7O0FBTVAsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFXUyxRQUFRbEIsSUFBeEI7QUFDRTtBQUFBO0FBQUEsWUFBUSxVQUFTLFFBQWpCLEVBQTBCLE9BQU0sU0FBaEM7QUFDRTtBQUFBO0FBQUE7QUFDRSxxQkFBT1MsS0FEVDtBQUVFLHdCQUFVLEtBQUtHLFFBRmpCO0FBR0UsOEJBQWUsU0FIakI7QUFJRSx5QkFBVSxTQUpaO0FBS0UsOEJBTEY7QUFNRSw2QkFBYztBQU5oQjtBQVFFLHVEQUFLLE9BQU0sU0FBWCxHQVJGO0FBU0UsdURBQUssT0FBTSxTQUFYLEdBVEY7QUFVRSx1REFBSyxPQUFNLFNBQVgsRUFBcUIsY0FBckIsR0FWRjtBQVdFLHVEQUFLLE9BQU0sT0FBWCxHQVhGO0FBWUUsdURBQUssT0FBTSxVQUFYLEdBWkY7QUFhRSx1REFBSyxPQUFNLE1BQVgsR0FiRjtBQWdCRSx1REFBSyxPQUFNLFdBQVgsRUFBdUIsT0FBTyxFQUFFUyxTQUFTLE1BQVgsRUFBOUI7QUFoQkY7QUFERixTQURGO0FBcUJFO0FBQUE7QUFBQTtBQUNFO0FBQ0UsdUJBREY7QUFFRSxrQkFBSyxVQUZQO0FBR0UsdUJBQVcsbUJBQUNDLFNBQUQ7QUFBQSxxQkFDVCw4QkFBQyxZQUFELDZCQUNNQSxTQUROO0FBRUUsc0JBQU1GLElBRlI7QUFHRSw0QkFBWSxPQUFLYixLQUFMLENBQVdnQjtBQUh6QixpQkFEUztBQUFBO0FBSGIsWUFERjtBQVlFO0FBQ0UsdUJBREY7QUFFRSxrQkFBSyxrQkFGUDtBQUdFLHVCQUFXLG1CQUFDRCxTQUFEO0FBQUEscUJBQ1QsOEJBQUMsWUFBRCw2QkFDTUEsU0FETjtBQUVFLHNCQUFNRixJQUZSO0FBR0UsNEJBQVksT0FBS2IsS0FBTCxDQUFXZ0I7QUFIekIsaUJBRFM7QUFBQTtBQUhiLFlBWkY7QUF1QkUsaUVBQU8sV0FBUCxFQUFhLE1BQUssa0JBQWxCLEVBQXFDLFdBQVc3QixZQUFoRCxHQXZCRjtBQXdCRTtBQUNFLHVCQURGO0FBRUUsa0JBQUssZ0JBRlA7QUFHRSx1QkFBVyxtQkFBQzRCLFNBQUQ7QUFBQSxxQkFDVCw4QkFBQyxVQUFELDZCQUNNQSxTQUROO0FBRUUsc0JBQU1GLElBRlI7QUFHRSw0QkFBWSxPQUFLYixLQUFMLENBQVdnQjtBQUh6QixpQkFEUztBQUFBO0FBSGIsWUF4QkY7QUFtQ0U7QUFDRSx1QkFERjtBQUVFLGtCQUFLLG1CQUZQO0FBR0UsdUJBQVcsbUJBQUNELFNBQUQ7QUFBQSxxQkFDVCw4QkFBQyxhQUFELDZCQUFtQkEsU0FBbkIsSUFBOEIsTUFBTUYsSUFBcEMsSUFEUztBQUFBO0FBSGIsWUFuQ0Y7QUEwQ0U7QUFDRSx1QkFERjtBQUVFLGtCQUFLLGVBRlA7QUFHRSx1QkFBVyxtQkFBQ0UsU0FBRDtBQUFBLHFCQUNULDhCQUFDLGlCQUFELDZCQUF1QkEsU0FBdkIsSUFBa0MsTUFBTUYsSUFBeEMsSUFEUztBQUFBO0FBSGIsWUExQ0Y7QUFpREUsaUVBQU8sV0FBV3RCLGNBQWxCO0FBakRGO0FBckJGLE9BREY7QUEyRUQ7Ozs7O0FBR0hRLFlBQVlrQixTQUFaLEdBQXdCO0FBQ3RCTixXQUFTLG9CQUFVTyxNQUFWLENBQWlCQyxVQURKOztBQUd0QmhCLFlBQVUsb0JBQVVpQixLQUFWLENBQWdCO0FBQ3hCaEIsY0FBVSxvQkFBVWlCLE1BQVYsQ0FBaUJGO0FBREgsR0FBaEIsRUFFUEEsVUFMbUI7QUFNdEJWLFdBQVMsb0JBQVVTLE1BQVYsQ0FBaUJDLFVBTko7O0FBUXRCUCxZQUFVLG9CQUFVTSxNQUFWLENBQWlCQyxVQVJMOztBQVV0Qkcsb0JBQWtCLG9CQUFVQyxJQUFWLENBQWVKLFVBVlg7QUFXdEJILG9CQUFrQixvQkFBVU8sSUFBVixDQUFlSjtBQVhYLENBQXhCOztBQWNBLElBQU1LLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHWixRQUFILFFBQUdBLFFBQUg7QUFBQSxTQUFtQixFQUFFQSxrQkFBRixFQUFuQjtBQUFBLENBQXhCOztBQUVBLElBQU1hLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUM3RCxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRTBELHVDQURGO0FBRUVOO0FBRkYsR0FERixFQUtFcEQsUUFMRixDQUR5QjtBQUFBLENBQTNCOztrQkFTZSx5QkFBUTRELGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXakMsTUFBWCxFQUFtQk8sV0FBbkIsQ0FEYSxDIiwiZmlsZSI6IjcuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX1BhcGVyID0gcmVxdWlyZSgnLi4vUGFwZXInKTtcblxudmFyIF9QYXBlcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9QYXBlcik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IFBhcGVyXG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGZsZXhEaXJlY3Rpb246ICdjb2x1bW4nLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGJveFNpemluZzogJ2JvcmRlci1ib3gnLCAvLyBQcmV2ZW50IHBhZGRpbmcgaXNzdWUgd2l0aCB0aGUgTW9kYWwgYW5kIGZpeGVkIHBvc2l0aW9uZWQgQXBwQmFyLlxuICAgICAgekluZGV4OiB0aGVtZS56SW5kZXguYXBwQmFyLFxuICAgICAgZmxleFNocmluazogMFxuICAgIH0sXG4gICAgcG9zaXRpb25GaXhlZDoge1xuICAgICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAnYXV0bycsXG4gICAgICByaWdodDogMFxuICAgIH0sXG4gICAgcG9zaXRpb25BYnNvbHV0ZToge1xuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAnYXV0bycsXG4gICAgICByaWdodDogMFxuICAgIH0sXG4gICAgcG9zaXRpb25TdGF0aWM6IHtcbiAgICAgIHBvc2l0aW9uOiAnc3RhdGljJyxcbiAgICAgIGZsZXhTaHJpbms6IDBcbiAgICB9LFxuICAgIGNvbG9yRGVmYXVsdDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuYXBwQmFyLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5hcHBCYXIpXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0sXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDApXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbG9yID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdwcmltYXJ5JywgJ2FjY2VudCcsICdkZWZhdWx0J10pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUG9zaXRpb24gPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydzdGF0aWMnLCAnZml4ZWQnLCAnYWJzb2x1dGUnXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ3ByaW1hcnknLCAnYWNjZW50JywgJ2RlZmF1bHQnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIHBvc2l0aW9uaW5nIHR5cGUuXG4gICAqL1xuICBwb3NpdGlvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnc3RhdGljJywgJ2ZpeGVkJywgJ2Fic29sdXRlJ10pLmlzUmVxdWlyZWRcbn07XG5cbnZhciBBcHBCYXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShBcHBCYXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEFwcEJhcigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBBcHBCYXIpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChBcHBCYXIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEFwcEJhcikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQXBwQmFyLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIHBvc2l0aW9uID0gX3Byb3BzLnBvc2l0aW9uLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ3Bvc2l0aW9uJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3Nlc1sncG9zaXRpb24nICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikocG9zaXRpb24pXSwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2luaGVyaXQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsICdtdWktZml4ZWQnLCBwb3NpdGlvbiA9PT0gJ2ZpeGVkJyksIF9jbGFzc05hbWVzKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX1BhcGVyMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgc3F1YXJlOiB0cnVlLCBjb21wb25lbnQ6ICdoZWFkZXInLCBlbGV2YXRpb246IDQsIGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBBcHBCYXI7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5BcHBCYXIuZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ3ByaW1hcnknLFxuICBwb3NpdGlvbjogJ2ZpeGVkJ1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlBcHBCYXInIH0pKEFwcEJhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL0FwcEJhci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL0FwcEJhci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9BcHBCYXIgPSByZXF1aXJlKCcuL0FwcEJhcicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BcHBCYXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0FwcEJhci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZSdcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGljb24gZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pLmlzUmVxdWlyZWRcbn07XG5cbnZhciBJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoJ21hdGVyaWFsLWljb25zJywgY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzcGFuJyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lLCAnYXJpYS1oaWRkZW4nOiAndHJ1ZScgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdpbmhlcml0J1xufTtcbkljb24ubXVpTmFtZSA9ICdJY29uJztcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uJyB9KShJY29uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vSWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDYgNyA4IDkgMzQgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4vSWNvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNiA3IDggOSAzNCAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2lubGluZS1ibG9jaycsXG4gICAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcbiAgICAgIGhlaWdodDogMjQsXG4gICAgICB3aWR0aDogMjQsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZScsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdmaWxsJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlclxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRWxlbWVudHMgcGFzc2VkIGludG8gdGhlIFNWRyBJY29uLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm92aWRlcyBhIGh1bWFuLXJlYWRhYmxlIHRpdGxlIGZvciB0aGUgZWxlbWVudCB0aGF0IGNvbnRhaW5zIGl0LlxuICAgKiBodHRwczovL3d3dy53My5vcmcvVFIvU1ZHLWFjY2Vzcy8jRXF1aXZhbGVudFxuICAgKi9cbiAgdGl0bGVBY2Nlc3M6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsbG93cyB5b3UgdG8gcmVkZWZpbmUgd2hhdCB0aGUgY29vcmRpbmF0ZXMgd2l0aG91dCB1bml0cyBtZWFuIGluc2lkZSBhbiBzdmcgZWxlbWVudC5cbiAgICogRm9yIGV4YW1wbGUsIGlmIHRoZSBTVkcgZWxlbWVudCBpcyA1MDAgKHdpZHRoKSBieSAyMDAgKGhlaWdodCksXG4gICAqIGFuZCB5b3UgcGFzcyB2aWV3Qm94PVwiMCAwIDUwIDIwXCIsXG4gICAqIHRoaXMgbWVhbnMgdGhhdCB0aGUgY29vcmRpbmF0ZXMgaW5zaWRlIHRoZSBzdmcgd2lsbCBnbyBmcm9tIHRoZSB0b3AgbGVmdCBjb3JuZXIgKDAsMClcbiAgICogdG8gYm90dG9tIHJpZ2h0ICg1MCwyMCkgYW5kIGVhY2ggdW5pdCB3aWxsIGJlIHdvcnRoIDEwcHguXG4gICAqL1xuICB2aWV3Qm94OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWRcbn07XG5cbnZhciBTdmdJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoU3ZnSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3ZnSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBTdmdJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoU3ZnSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU3ZnSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU3ZnSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgdGl0bGVBY2Nlc3MgPSBfcHJvcHMudGl0bGVBY2Nlc3MsXG4gICAgICAgICAgdmlld0JveCA9IF9wcm9wcy52aWV3Qm94LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ3RpdGxlQWNjZXNzJywgJ3ZpZXdCb3gnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzdmcnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBmb2N1c2FibGU6ICdmYWxzZScsXG4gICAgICAgICAgdmlld0JveDogdmlld0JveCxcbiAgICAgICAgICAnYXJpYS1oaWRkZW4nOiB0aXRsZUFjY2VzcyA/ICdmYWxzZScgOiAndHJ1ZSdcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICB0aXRsZUFjY2VzcyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICd0aXRsZScsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICB0aXRsZUFjY2Vzc1xuICAgICAgICApIDogbnVsbCxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBTdmdJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuU3ZnSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpZXdCb3g6ICcwIDAgMjQgMjQnLFxuICBjb2xvcjogJ2luaGVyaXQnXG59O1xuU3ZnSWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVN2Z0ljb24nIH0pKFN2Z0ljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi9TdmdJY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfa2V5cyA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3Qva2V5cycpO1xuXG52YXIgX2tleXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfa2V5cyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMyA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczQgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMyk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9CdXR0b25CYXNlID0gcmVxdWlyZSgnLi4vQnV0dG9uQmFzZScpO1xuXG52YXIgX0J1dHRvbkJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQnV0dG9uQmFzZSk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9JY29uID0gcmVxdWlyZSgnLi4vSWNvbicpO1xuXG52YXIgX0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IEJ1dHRvbkJhc2VcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDogKDAsIF9leHRlbmRzNC5kZWZhdWx0KSh7fSwgdGhlbWUudHlwb2dyYXBoeS5idXR0b24sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHtcbiAgICAgIG1heFdpZHRoOiAyNjQsXG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIG1pbldpZHRoOiA3MixcbiAgICAgIHBhZGRpbmc6IDAsXG4gICAgICBoZWlnaHQ6IDQ4LFxuICAgICAgZmxleDogJ25vbmUnLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nXG4gICAgfSwgdGhlbWUuYnJlYWtwb2ludHMudXAoJ21kJyksIHtcbiAgICAgIG1pbldpZHRoOiAxNjBcbiAgICB9KSksXG4gICAgcm9vdExhYmVsSWNvbjoge1xuICAgICAgaGVpZ2h0OiA3MlxuICAgIH0sXG4gICAgcm9vdEFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5zZWNvbmRhcnlcbiAgICB9LFxuICAgIHJvb3RBY2NlbnRTZWxlY3RlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIHJvb3RBY2NlbnREaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXNhYmxlZFxuICAgIH0sXG4gICAgcm9vdFByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuc2Vjb25kYXJ5XG4gICAgfSxcbiAgICByb290UHJpbWFyeVNlbGVjdGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIHJvb3RQcmltYXJ5RGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGlzYWJsZWRcbiAgICB9LFxuICAgIHJvb3RJbmhlcml0OiB7XG4gICAgICBjb2xvcjogJ2luaGVyaXQnLFxuICAgICAgb3BhY2l0eTogMC43XG4gICAgfSxcbiAgICByb290SW5oZXJpdFNlbGVjdGVkOiB7XG4gICAgICBvcGFjaXR5OiAxXG4gICAgfSxcbiAgICByb290SW5oZXJpdERpc2FibGVkOiB7XG4gICAgICBvcGFjaXR5OiAwLjRcbiAgICB9LFxuICAgIGZ1bGxXaWR0aDoge1xuICAgICAgZmxleEdyb3c6IDFcbiAgICB9LFxuICAgIHdyYXBwZXI6IHtcbiAgICAgIGRpc3BsYXk6ICdpbmxpbmUtZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJ1xuICAgIH0sXG4gICAgbGFiZWxDb250YWluZXI6ICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHtcbiAgICAgIHBhZGRpbmdUb3A6IDYsXG4gICAgICBwYWRkaW5nQm90dG9tOiA2LFxuICAgICAgcGFkZGluZ0xlZnQ6IDEyLFxuICAgICAgcGFkZGluZ1JpZ2h0OiAxMlxuICAgIH0sIHRoZW1lLmJyZWFrcG9pbnRzLnVwKCdtZCcpLCB7XG4gICAgICBwYWRkaW5nTGVmdDogdGhlbWUuc3BhY2luZy51bml0ICogMyxcbiAgICAgIHBhZGRpbmdSaWdodDogdGhlbWUuc3BhY2luZy51bml0ICogM1xuICAgIH0pLFxuICAgIGxhYmVsOiAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7XG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKHRoZW1lLnR5cG9ncmFwaHkuZm9udFNpemUpLFxuICAgICAgd2hpdGVTcGFjZTogJ25vcm1hbCdcbiAgICB9LCB0aGVtZS5icmVha3BvaW50cy51cCgnbWQnKSwge1xuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSh0aGVtZS50eXBvZ3JhcGh5LmZvbnRTaXplIC0gMSlcbiAgICB9KSxcbiAgICBsYWJlbFdyYXBwZWQ6ICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCB0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpLCB7XG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKHRoZW1lLnR5cG9ncmFwaHkuZm9udFNpemUgLSAyKVxuICAgIH0pXG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHRhYiB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGZ1bGxXaWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIGVsZW1lbnQuIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYSBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgaWNvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZywgdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpXSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICogRm9yIHNlcnZlciBzaWRlIHJlbmRlcmluZyBjb25zaWRlcmF0aW9uLCB3ZSBsZXQgdGhlIHNlbGVjdGVkIHRhYlxuICAgKiByZW5kZXIgdGhlIGluZGljYXRvci5cbiAgICovXG4gIGluZGljYXRvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZywgdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpXSksXG5cbiAgLyoqXG4gICAqIFRoZSBsYWJlbCBlbGVtZW50LlxuICAgKi9cbiAgbGFiZWw6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkNoYW5nZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBzZWxlY3RlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHN0eWxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICB0ZXh0Q29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2FjY2VudCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncHJpbWFyeSddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLFxuXG4gIC8qKlxuICAgKiBZb3UgY2FuIHByb3ZpZGUgeW91ciBvd24gdmFsdWUuIE90aGVyd2lzZSwgd2UgZmFsbGJhY2sgdG8gdGhlIGNoaWxkIHBvc2l0aW9uIGluZGV4LlxuICAgKi9cbiAgdmFsdWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnlcbn07XG5cbnZhciBUYWIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShUYWIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFRhYigpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBUYWIpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gVGFiLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShUYWIpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHdyYXBwZWRUZXh0OiBmYWxzZVxuICAgIH0sIF90aGlzLmhhbmRsZUNoYW5nZSA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgb25DaGFuZ2UgPSBfdGhpcyRwcm9wcy5vbkNoYW5nZSxcbiAgICAgICAgICB2YWx1ZSA9IF90aGlzJHByb3BzLnZhbHVlLFxuICAgICAgICAgIG9uQ2xpY2sgPSBfdGhpcyRwcm9wcy5vbkNsaWNrO1xuXG5cbiAgICAgIGlmIChvbkNoYW5nZSkge1xuICAgICAgICBvbkNoYW5nZShldmVudCwgdmFsdWUpO1xuICAgICAgfVxuXG4gICAgICBpZiAob25DbGljaykge1xuICAgICAgICBvbkNsaWNrKGV2ZW50KTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5sYWJlbCA9IHVuZGVmaW5lZCwgX3RoaXMuY2hlY2tUZXh0V3JhcCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy5sYWJlbCkge1xuICAgICAgICB2YXIgX3dyYXBwZWRUZXh0ID0gX3RoaXMubGFiZWwuZ2V0Q2xpZW50UmVjdHMoKS5sZW5ndGggPiAxO1xuICAgICAgICBpZiAoX3RoaXMuc3RhdGUud3JhcHBlZFRleHQgIT09IF93cmFwcGVkVGV4dCkge1xuICAgICAgICAgIF90aGlzLnNldFN0YXRlKHsgd3JhcHBlZFRleHQ6IF93cmFwcGVkVGV4dCB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShUYWIsIFt7XG4gICAga2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIHRoaXMuY2hlY2tUZXh0V3JhcCgpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgaWYgKHRoaXMuc3RhdGUud3JhcHBlZFRleHQgPT09IHByZXZTdGF0ZS53cmFwcGVkVGV4dCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogQXQgY2VydGFpbiB0ZXh0IGFuZCB0YWIgbGVuZ3RocywgYSBsYXJnZXIgZm9udCBzaXplIG1heSB3cmFwIHRvIHR3byBsaW5lcyB3aGlsZSB0aGUgc21hbGxlclxuICAgICAgICAgKiBmb250IHNpemUgc3RpbGwgb25seSByZXF1aXJlcyBvbmUgbGluZS4gIFRoaXMgY2hlY2sgd2lsbCBwcmV2ZW50IGFuIGluZmluaXRlIHJlbmRlciBsb29wXG4gICAgICAgICAqIGZyb24gb2NjdXJyaW5nIGluIHRoYXQgc2NlbmFyaW8uXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLmNoZWNrVGV4dFdyYXAoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcyxcbiAgICAgICAgICBfY2xhc3NOYW1lczI7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICBmdWxsV2lkdGggPSBfcHJvcHMuZnVsbFdpZHRoLFxuICAgICAgICAgIGljb25Qcm9wID0gX3Byb3BzLmljb24sXG4gICAgICAgICAgaW5kaWNhdG9yID0gX3Byb3BzLmluZGljYXRvcixcbiAgICAgICAgICBsYWJlbFByb3AgPSBfcHJvcHMubGFiZWwsXG4gICAgICAgICAgb25DaGFuZ2UgPSBfcHJvcHMub25DaGFuZ2UsXG4gICAgICAgICAgc2VsZWN0ZWQgPSBfcHJvcHMuc2VsZWN0ZWQsXG4gICAgICAgICAgc3R5bGVQcm9wID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICAgIHRleHRDb2xvciA9IF9wcm9wcy50ZXh0Q29sb3IsXG4gICAgICAgICAgdmFsdWUgPSBfcHJvcHMudmFsdWUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnZGlzYWJsZWQnLCAnZnVsbFdpZHRoJywgJ2ljb24nLCAnaW5kaWNhdG9yJywgJ2xhYmVsJywgJ29uQ2hhbmdlJywgJ3NlbGVjdGVkJywgJ3N0eWxlJywgJ3RleHRDb2xvcicsICd2YWx1ZSddKTtcblxuXG4gICAgICB2YXIgaWNvbiA9IHZvaWQgMDtcblxuICAgICAgaWYgKGljb25Qcm9wICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgaWNvbiA9IF9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChpY29uUHJvcCkgPyBpY29uUHJvcCA6IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgaWNvblByb3BcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGxhYmVsID0gdm9pZCAwO1xuXG4gICAgICBpZiAobGFiZWxQcm9wICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgbGFiZWwgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5sYWJlbENvbnRhaW5lciB9LFxuICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5sYWJlbCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMubGFiZWxXcmFwcGVkLCB0aGlzLnN0YXRlLndyYXBwZWRUZXh0KSksXG4gICAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgICBfdGhpczIubGFiZWwgPSBub2RlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgbGFiZWxQcm9wXG4gICAgICAgICAgKVxuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIChfY2xhc3NOYW1lczIgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMyLCBjbGFzc2VzWydyb290JyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKHRleHRDb2xvcildLCB0cnVlKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMyLCBjbGFzc2VzWydyb290JyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKHRleHRDb2xvcikgKyAnRGlzYWJsZWQnXSwgZGlzYWJsZWQpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczIsIGNsYXNzZXNbJ3Jvb3QnICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikodGV4dENvbG9yKSArICdTZWxlY3RlZCddLCBzZWxlY3RlZCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMiwgY2xhc3Nlcy5yb290TGFiZWxJY29uLCBpY29uICYmIGxhYmVsKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMyLCBjbGFzc2VzLmZ1bGxXaWR0aCwgZnVsbFdpZHRoKSwgX2NsYXNzTmFtZXMyKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHZhciBzdHlsZSA9IHt9O1xuXG4gICAgICBpZiAodGV4dENvbG9yICE9PSAnYWNjZW50JyAmJiB0ZXh0Q29sb3IgIT09ICdpbmhlcml0Jykge1xuICAgICAgICBzdHlsZS5jb2xvciA9IHRleHRDb2xvcjtcbiAgICAgIH1cblxuICAgICAgc3R5bGUgPSAoMCwgX2tleXMyLmRlZmF1bHQpKHN0eWxlKS5sZW5ndGggPiAwID8gKDAsIF9leHRlbmRzNC5kZWZhdWx0KSh7fSwgc3R5bGUsIHN0eWxlUHJvcCkgOiBzdHlsZVByb3A7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0J1dHRvbkJhc2UyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczQuZGVmYXVsdCkoe1xuICAgICAgICAgIGZvY3VzUmlwcGxlOiB0cnVlLFxuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAgIHN0eWxlOiBzdHlsZSxcbiAgICAgICAgICByb2xlOiAndGFiJyxcbiAgICAgICAgICAnYXJpYS1zZWxlY3RlZCc6IHNlbGVjdGVkLFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZFxuICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgIG9uQ2xpY2s6IHRoaXMuaGFuZGxlQ2hhbmdlXG4gICAgICAgIH0pLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnc3BhbicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMud3JhcHBlciB9LFxuICAgICAgICAgIGljb24sXG4gICAgICAgICAgbGFiZWxcbiAgICAgICAgKSxcbiAgICAgICAgaW5kaWNhdG9yXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gVGFiO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuVGFiLmRlZmF1bHRQcm9wcyA9IHtcbiAgZGlzYWJsZWQ6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVRhYicgfSkoVGFiKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfcmVmOyAvLyAgd2Vha1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICBoZWlnaHQ6IDIsXG4gICAgICBib3R0b206IDAsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCksXG4gICAgICB3aWxsQ2hhbmdlOiAnbGVmdCwgd2lkdGgnXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSW5kaWNhdG9yU3R5bGUgPSB7XG4gIGxlZnQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gIHdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG59O1xudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3ZpZGVkUHJvcHMgPSB7XG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZFxufTtcbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBUaGUgY29sb3Igb2YgdGhlIHRhYiBpbmRpY2F0b3IuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYWNjZW50J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydwcmltYXJ5J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBUaGUgc3R5bGUgb2YgdGhlIHJvb3QgZWxlbWVudC5cbiAgICovXG4gIHN0eWxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoe1xuICAgIGxlZnQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gICAgd2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJcbiAgfSkuaXNSZXF1aXJlZFxufTtcblxuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbmZ1bmN0aW9uIFRhYkluZGljYXRvcihwcm9wcykge1xuICB2YXIgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWVQcm9wID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgY29sb3IgPSBwcm9wcy5jb2xvcixcbiAgICAgIHN0eWxlUHJvcCA9IHByb3BzLnN0eWxlO1xuXG4gIHZhciBjb2xvclByZWRlZmluZWQgPSBbJ3ByaW1hcnknLCAnYWNjZW50J10uaW5kZXhPZihjb2xvcikgIT09IC0xO1xuICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvclByZWRlZmluZWQpLCBjbGFzc05hbWVQcm9wKTtcblxuICB2YXIgc3R5bGUgPSBjb2xvclByZWRlZmluZWQgPyBzdHlsZVByb3AgOiAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBzdHlsZVByb3AsIHtcbiAgICBiYWNrZ3JvdW5kQ29sb3I6IGNvbG9yXG4gIH0pO1xuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnZGl2JywgeyBjbGFzc05hbWU6IGNsYXNzTmFtZSwgc3R5bGU6IHN0eWxlIH0pO1xufVxuXG5UYWJJbmRpY2F0b3IucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKF9yZWYgPSB7XG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZFxufSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzZXMnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0KSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NsYXNzTmFtZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY29sb3InLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhY2NlbnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3ByaW1hcnknXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmddKS5pc1JlcXVpcmVkKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ3N0eWxlJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKHtcbiAgbGVmdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcbiAgd2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJcbn0pLmlzUmVxdWlyZWQpLCBfcmVmKSA6IHt9O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVRhYkluZGljYXRvcicgfSkoVGFiSW5kaWNhdG9yKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYkluZGljYXRvci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJJbmRpY2F0b3IuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQnV0dG9uQmFzZSA9IHJlcXVpcmUoJy4uL0J1dHRvbkJhc2UnKTtcblxudmFyIF9CdXR0b25CYXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0J1dHRvbkJhc2UpO1xuXG52YXIgX0tleWJvYXJkQXJyb3dMZWZ0ID0gcmVxdWlyZSgnLi4vc3ZnLWljb25zL0tleWJvYXJkQXJyb3dMZWZ0Jyk7XG5cbnZhciBfS2V5Ym9hcmRBcnJvd0xlZnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfS2V5Ym9hcmRBcnJvd0xlZnQpO1xuXG52YXIgX0tleWJvYXJkQXJyb3dSaWdodCA9IHJlcXVpcmUoJy4uL3N2Zy1pY29ucy9LZXlib2FyZEFycm93UmlnaHQnKTtcblxudmFyIF9LZXlib2FyZEFycm93UmlnaHQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfS2V5Ym9hcmRBcnJvd1JpZ2h0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuLy8gIHdlYWtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0JyxcbiAgICAgIGZsZXg6ICcwIDAgJyArIHRoZW1lLnNwYWNpbmcudW5pdCAqIDcgKyAncHgnXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBXaGljaCBkaXJlY3Rpb24gc2hvdWxkIHRoZSBidXR0b24gaW5kaWNhdGU/XG4gICAqL1xuICBkaXJlY3Rpb246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2xlZnQnLCAncmlnaHQnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgdG8gZXhlY3V0ZSBmb3IgYnV0dG9uIHByZXNzLlxuICAgKi9cbiAgb25DbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFNob3VsZCB0aGUgYnV0dG9uIGJlIHByZXNlbnQgb3IganVzdCBjb25zdW1lIHNwYWNlLlxuICAgKi9cbiAgdmlzaWJsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2xcbn07XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0tleWJvYXJkQXJyb3dMZWZ0Mi5kZWZhdWx0LCBudWxsKTtcblxudmFyIF9yZWYyID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0tleWJvYXJkQXJyb3dSaWdodDIuZGVmYXVsdCwgbnVsbCk7XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIFRhYlNjcm9sbEJ1dHRvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFRhYlNjcm9sbEJ1dHRvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gVGFiU2Nyb2xsQnV0dG9uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFRhYlNjcm9sbEJ1dHRvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKFRhYlNjcm9sbEJ1dHRvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoVGFiU2Nyb2xsQnV0dG9uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShUYWJTY3JvbGxCdXR0b24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGRpcmVjdGlvbiA9IF9wcm9wcy5kaXJlY3Rpb24sXG4gICAgICAgICAgb25DbGljayA9IF9wcm9wcy5vbkNsaWNrLFxuICAgICAgICAgIHZpc2libGUgPSBfcHJvcHMudmlzaWJsZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdkaXJlY3Rpb24nLCAnb25DbGljaycsICd2aXNpYmxlJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIGlmICghdmlzaWJsZSkge1xuICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0J1dHRvbkJhc2UyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSwgb25DbGljazogb25DbGljaywgdGFiSW5kZXg6IC0xIH0sIG90aGVyKSxcbiAgICAgICAgZGlyZWN0aW9uID09PSAnbGVmdCcgPyBfcmVmIDogX3JlZjJcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBUYWJTY3JvbGxCdXR0b247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5UYWJTY3JvbGxCdXR0b24uZGVmYXVsdFByb3BzID0ge1xuICB2aXNpYmxlOiB0cnVlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVRhYlNjcm9sbEJ1dHRvbicgfSkoVGFiU2Nyb2xsQnV0dG9uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYlNjcm9sbEJ1dHRvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJTY3JvbGxCdXR0b24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfaXNOYW4gPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvbnVtYmVyL2lzLW5hbicpO1xuXG52YXIgX2lzTmFuMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzTmFuKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3dhcm5pbmcgPSByZXF1aXJlKCd3YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfcmVhY3RFdmVudExpc3RlbmVyID0gcmVxdWlyZSgncmVhY3QtZXZlbnQtbGlzdGVuZXInKTtcblxudmFyIF9yZWFjdEV2ZW50TGlzdGVuZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3RFdmVudExpc3RlbmVyKTtcblxudmFyIF9kZWJvdW5jZSA9IHJlcXVpcmUoJ2xvZGFzaC9kZWJvdW5jZScpO1xuXG52YXIgX2RlYm91bmNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlYm91bmNlKTtcblxudmFyIF9yZWFjdFNjcm9sbGJhclNpemUgPSByZXF1aXJlKCdyZWFjdC1zY3JvbGxiYXItc2l6ZScpO1xuXG52YXIgX3JlYWN0U2Nyb2xsYmFyU2l6ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdFNjcm9sbGJhclNpemUpO1xuXG52YXIgX25vcm1hbGl6ZVNjcm9sbExlZnQgPSByZXF1aXJlKCdub3JtYWxpemUtc2Nyb2xsLWxlZnQnKTtcblxudmFyIF9zY3JvbGwgPSByZXF1aXJlKCdzY3JvbGwnKTtcblxudmFyIF9zY3JvbGwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2Nyb2xsKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX1RhYkluZGljYXRvciA9IHJlcXVpcmUoJy4vVGFiSW5kaWNhdG9yJyk7XG5cbnZhciBfVGFiSW5kaWNhdG9yMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RhYkluZGljYXRvcik7XG5cbnZhciBfVGFiU2Nyb2xsQnV0dG9uID0gcmVxdWlyZSgnLi9UYWJTY3JvbGxCdXR0b24nKTtcblxudmFyIF9UYWJTY3JvbGxCdXR0b24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVGFiU2Nyb2xsQnV0dG9uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0luZGljYXRvclN0eWxlID0gcmVxdWlyZSgnLi9UYWJJbmRpY2F0b3InKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9JbmRpY2F0b3JTdHlsZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICBtaW5IZWlnaHQ6IDQ4LFxuICAgICAgV2Via2l0T3ZlcmZsb3dTY3JvbGxpbmc6ICd0b3VjaCcgLy8gQWRkIGlPUyBtb21lbnR1bSBzY3JvbGxpbmcuXG4gICAgfSxcbiAgICBmbGV4Q29udGFpbmVyOiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCdcbiAgICB9LFxuICAgIHNjcm9sbGluZ0NvbnRhaW5lcjoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWJsb2NrJyxcbiAgICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gICAgICB3aGl0ZVNwYWNlOiAnbm93cmFwJ1xuICAgIH0sXG4gICAgZml4ZWQ6IHtcbiAgICAgIG92ZXJmbG93WDogJ2hpZGRlbicsXG4gICAgICB3aWR0aDogJzEwMCUnXG4gICAgfSxcbiAgICBzY3JvbGxhYmxlOiB7XG4gICAgICBvdmVyZmxvd1g6ICdzY3JvbGwnXG4gICAgfSxcbiAgICBjZW50ZXJlZDoge1xuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInXG4gICAgfSxcbiAgICBidXR0b25BdXRvOiAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKSwge1xuICAgICAgZGlzcGxheTogJ25vbmUnXG4gICAgfSlcbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBDU1MgY2xhc3MgbmFtZSBvZiB0aGUgc2Nyb2xsIGJ1dHRvbiBlbGVtZW50cy5cbiAgICovXG4gIGJ1dHRvbkNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgdGFicyB3aWxsIGJlIGNlbnRlcmVkLlxuICAgKiBUaGlzIHByb3BlcnR5IGlzIGludGVuZGVkIGZvciBsYXJnZSB2aWV3cy5cbiAgICovXG4gIGNlbnRlcmVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSB0YWJzIHdpbGwgZ3JvdyB0byB1c2UgYWxsIHRoZSBhdmFpbGFibGUgc3BhY2UuXG4gICAqIFRoaXMgcHJvcGVydHkgaXMgaW50ZW5kZWQgZm9yIHNtYWxsIHZpZXdzLCBsaWtlIG9uIG1vYmlsZS5cbiAgICovXG4gIGZ1bGxXaWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIENTUyBjbGFzcyBuYW1lIG9mIHRoZSBpbmRpY2F0b3IgZWxlbWVudC5cbiAgICovXG4gIGluZGljYXRvckNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogRGV0ZXJtaW5lcyB0aGUgY29sb3Igb2YgdGhlIGluZGljYXRvci5cbiAgICovXG4gIGluZGljYXRvckNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhY2NlbnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3ByaW1hcnknXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSB2YWx1ZSBjaGFuZ2VzLlxuICAgKlxuICAgKiBAcGFyYW0ge29iamVjdH0gZXZlbnQgVGhlIGV2ZW50IHNvdXJjZSBvZiB0aGUgY2FsbGJhY2tcbiAgICogQHBhcmFtIHtudW1iZXJ9IHZhbHVlIFdlIGRlZmF1bHQgdG8gdGhlIGluZGV4IG9mIHRoZSBjaGlsZFxuICAgKi9cbiAgb25DaGFuZ2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRydWUgaW52b2tlcyBzY3JvbGxpbmcgcHJvcGVydGllcyBhbmQgYWxsb3cgZm9yIGhvcml6b250YWxseSBzY3JvbGxpbmdcbiAgICogKG9yIHN3aXBpbmcpIHRoZSB0YWIgYmFyLlxuICAgKi9cbiAgc2Nyb2xsYWJsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogRGV0ZXJtaW5lIGJlaGF2aW9yIG9mIHNjcm9sbCBidXR0b25zIHdoZW4gdGFicyBhcmUgc2V0IHRvIHNjcm9sbFxuICAgKiBgYXV0b2Agd2lsbCBvbmx5IHByZXNlbnQgdGhlbSBvbiBtZWRpdW0gYW5kIGxhcmdlciB2aWV3cG9ydHNcbiAgICogYG9uYCB3aWxsIGFsd2F5cyBwcmVzZW50IHRoZW1cbiAgICogYG9mZmAgd2lsbCBuZXZlciBwcmVzZW50IHRoZW1cbiAgICovXG4gIHNjcm9sbEJ1dHRvbnM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2F1dG8nLCAnb24nLCAnb2ZmJ10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCB0byByZW5kZXIgdGhlIHNjcm9sbCBidXR0b25zLlxuICAgKi9cbiAgVGFiU2Nyb2xsQnV0dG9uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogRGV0ZXJtaW5lcyB0aGUgY29sb3Igb2YgdGhlIGBUYWJgLlxuICAgKi9cbiAgdGV4dENvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhY2NlbnQnLCAncHJpbWFyeScsICdpbmhlcml0J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSB2YWx1ZSBvZiB0aGUgY3VycmVudGx5IHNlbGVjdGVkIGBUYWJgLlxuICAgKiBJZiB5b3UgZG9uJ3Qgd2FudCBhbnkgc2VsZWN0ZWQgYFRhYmAsIHlvdSBjYW4gc2V0IHRoaXMgcHJvcGVydHkgdG8gYGZhbHNlYC5cbiAgICovXG4gIHZhbHVlOiBmdW5jdGlvbiB2YWx1ZShwcm9wcywgcHJvcE5hbWUsIGNvbXBvbmVudE5hbWUpIHtcbiAgICBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChwcm9wcywgcHJvcE5hbWUpKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1Byb3AgYCcgKyBwcm9wTmFtZSArICdgIGhhcyB0eXBlIFxcJ2FueVxcJyBvciBcXCdtaXhlZFxcJywgYnV0IHdhcyBub3QgcHJvdmlkZWQgdG8gYCcgKyBjb21wb25lbnROYW1lICsgJ2AuIFBhc3MgdW5kZWZpbmVkIG9yIGFueSBvdGhlciB2YWx1ZS4nKTtcbiAgICB9XG4gIH1cbn07XG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVGFic01ldGEgPSB7XG4gIGNsaWVudFdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHNjcm9sbExlZnQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgc2Nyb2xsTGVmdE5vcm1hbGl6ZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgc2Nyb2xsV2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcblxuICAvLyBDbGllbnRSZWN0XG4gIGxlZnQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgcmlnaHQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZFxufTtcblxudmFyIFRhYnMgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShUYWJzLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBUYWJzKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFRhYnMpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gVGFicy5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoVGFicykpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0ge1xuICAgICAgaW5kaWNhdG9yU3R5bGU6IHt9LFxuICAgICAgc2Nyb2xsZXJTdHlsZToge1xuICAgICAgICBtYXJnaW5Cb3R0b206IDBcbiAgICAgIH0sXG4gICAgICBzaG93TGVmdFNjcm9sbDogZmFsc2UsXG4gICAgICBzaG93UmlnaHRTY3JvbGw6IGZhbHNlLFxuICAgICAgbW91bnRlZDogZmFsc2VcbiAgICB9LCBfdGhpcy5nZXRDb25kaXRpb25hbEVsZW1lbnRzID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgY2xhc3NlcyA9IF90aGlzJHByb3BzLmNsYXNzZXMsXG4gICAgICAgICAgYnV0dG9uQ2xhc3NOYW1lID0gX3RoaXMkcHJvcHMuYnV0dG9uQ2xhc3NOYW1lLFxuICAgICAgICAgIHNjcm9sbGFibGUgPSBfdGhpcyRwcm9wcy5zY3JvbGxhYmxlLFxuICAgICAgICAgIHNjcm9sbEJ1dHRvbnMgPSBfdGhpcyRwcm9wcy5zY3JvbGxCdXR0b25zLFxuICAgICAgICAgIFRhYlNjcm9sbEJ1dHRvblByb3AgPSBfdGhpcyRwcm9wcy5UYWJTY3JvbGxCdXR0b24sXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wcy50aGVtZTtcblxuICAgICAgdmFyIGNvbmRpdGlvbmFsRWxlbWVudHMgPSB7fTtcbiAgICAgIGNvbmRpdGlvbmFsRWxlbWVudHMuc2Nyb2xsYmFyU2l6ZUxpc3RlbmVyID0gc2Nyb2xsYWJsZSA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9yZWFjdFNjcm9sbGJhclNpemUyLmRlZmF1bHQsIHtcbiAgICAgICAgb25Mb2FkOiBfdGhpcy5oYW5kbGVTY3JvbGxiYXJTaXplQ2hhbmdlLFxuICAgICAgICBvbkNoYW5nZTogX3RoaXMuaGFuZGxlU2Nyb2xsYmFyU2l6ZUNoYW5nZVxuICAgICAgfSkgOiBudWxsO1xuXG4gICAgICB2YXIgc2hvd1Njcm9sbEJ1dHRvbnMgPSBzY3JvbGxhYmxlICYmIChzY3JvbGxCdXR0b25zID09PSAnYXV0bycgfHwgc2Nyb2xsQnV0dG9ucyA9PT0gJ29uJyk7XG5cbiAgICAgIGNvbmRpdGlvbmFsRWxlbWVudHMuc2Nyb2xsQnV0dG9uTGVmdCA9IHNob3dTY3JvbGxCdXR0b25zID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoVGFiU2Nyb2xsQnV0dG9uUHJvcCwge1xuICAgICAgICBkaXJlY3Rpb246IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbiA9PT0gJ3J0bCcgPyAncmlnaHQnIDogJ2xlZnQnLFxuICAgICAgICBvbkNsaWNrOiBfdGhpcy5oYW5kbGVMZWZ0U2Nyb2xsQ2xpY2ssXG4gICAgICAgIHZpc2libGU6IF90aGlzLnN0YXRlLnNob3dMZWZ0U2Nyb2xsLFxuICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuYnV0dG9uQXV0bywgc2Nyb2xsQnV0dG9ucyA9PT0gJ2F1dG8nKSwgYnV0dG9uQ2xhc3NOYW1lKVxuICAgICAgfSkgOiBudWxsO1xuXG4gICAgICBjb25kaXRpb25hbEVsZW1lbnRzLnNjcm9sbEJ1dHRvblJpZ2h0ID0gc2hvd1Njcm9sbEJ1dHRvbnMgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChUYWJTY3JvbGxCdXR0b25Qcm9wLCB7XG4gICAgICAgIGRpcmVjdGlvbjogdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uID09PSAncnRsJyA/ICdsZWZ0JyA6ICdyaWdodCcsXG4gICAgICAgIG9uQ2xpY2s6IF90aGlzLmhhbmRsZVJpZ2h0U2Nyb2xsQ2xpY2ssXG4gICAgICAgIHZpc2libGU6IF90aGlzLnN0YXRlLnNob3dSaWdodFNjcm9sbCxcbiAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmJ1dHRvbkF1dG8sIHNjcm9sbEJ1dHRvbnMgPT09ICdhdXRvJyksIGJ1dHRvbkNsYXNzTmFtZSlcbiAgICAgIH0pIDogbnVsbDtcblxuICAgICAgcmV0dXJuIGNvbmRpdGlvbmFsRWxlbWVudHM7XG4gICAgfSwgX3RoaXMuZ2V0VGFic01ldGEgPSBmdW5jdGlvbiAodmFsdWUsIGRpcmVjdGlvbikge1xuICAgICAgdmFyIHRhYnNNZXRhID0gdm9pZCAwO1xuICAgICAgaWYgKF90aGlzLnRhYnMpIHtcbiAgICAgICAgdmFyIHJlY3QgPSBfdGhpcy50YWJzLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpO1xuICAgICAgICAvLyBjcmVhdGUgYSBuZXcgb2JqZWN0IHdpdGggQ2xpZW50UmVjdCBjbGFzcyBwcm9wcyArIHNjcm9sbExlZnRcbiAgICAgICAgdGFic01ldGEgPSB7XG4gICAgICAgICAgY2xpZW50V2lkdGg6IF90aGlzLnRhYnMgPyBfdGhpcy50YWJzLmNsaWVudFdpZHRoIDogMCxcbiAgICAgICAgICBzY3JvbGxMZWZ0OiBfdGhpcy50YWJzID8gX3RoaXMudGFicy5zY3JvbGxMZWZ0IDogMCxcbiAgICAgICAgICBzY3JvbGxMZWZ0Tm9ybWFsaXplZDogX3RoaXMudGFicyA/ICgwLCBfbm9ybWFsaXplU2Nyb2xsTGVmdC5nZXROb3JtYWxpemVkU2Nyb2xsTGVmdCkoX3RoaXMudGFicywgZGlyZWN0aW9uKSA6IDAsXG4gICAgICAgICAgc2Nyb2xsV2lkdGg6IF90aGlzLnRhYnMgPyBfdGhpcy50YWJzLnNjcm9sbFdpZHRoIDogMCxcbiAgICAgICAgICBsZWZ0OiByZWN0LmxlZnQsXG4gICAgICAgICAgcmlnaHQ6IHJlY3QucmlnaHRcbiAgICAgICAgfTtcbiAgICAgIH1cblxuICAgICAgdmFyIHRhYk1ldGEgPSB2b2lkIDA7XG4gICAgICBpZiAoX3RoaXMudGFicyAmJiB2YWx1ZSAhPT0gZmFsc2UpIHtcbiAgICAgICAgdmFyIF9jaGlsZHJlbiA9IF90aGlzLnRhYnMuY2hpbGRyZW5bMF0uY2hpbGRyZW47XG5cbiAgICAgICAgaWYgKF9jaGlsZHJlbi5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgdmFyIHRhYiA9IF9jaGlsZHJlbltfdGhpcy52YWx1ZVRvSW5kZXhbdmFsdWVdXTtcbiAgICAgICAgICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKEJvb2xlYW4odGFiKSwgJ01hdGVyaWFsLVVJOiB0aGUgdmFsdWUgcHJvdmlkZWQgYCcgKyB2YWx1ZSArICdgIGlzIGludmFsaWQnKSA6IHZvaWQgMDtcbiAgICAgICAgICB0YWJNZXRhID0gdGFiID8gdGFiLmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpIDogbnVsbDtcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHsgdGFic01ldGE6IHRhYnNNZXRhLCB0YWJNZXRhOiB0YWJNZXRhIH07XG4gICAgfSwgX3RoaXMudGFicyA9IHVuZGVmaW5lZCwgX3RoaXMudmFsdWVUb0luZGV4ID0ge30sIF90aGlzLmhhbmRsZVJlc2l6ZSA9ICgwLCBfZGVib3VuY2UyLmRlZmF1bHQpKGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLnVwZGF0ZUluZGljYXRvclN0YXRlKF90aGlzLnByb3BzKTtcbiAgICAgIF90aGlzLnVwZGF0ZVNjcm9sbEJ1dHRvblN0YXRlKCk7XG4gICAgfSwgMTY2KSwgX3RoaXMuaGFuZGxlTGVmdFNjcm9sbENsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKF90aGlzLnRhYnMpIHtcbiAgICAgICAgX3RoaXMubW92ZVRhYnNTY3JvbGwoLV90aGlzLnRhYnMuY2xpZW50V2lkdGgpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZVJpZ2h0U2Nyb2xsQ2xpY2sgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMudGFicykge1xuICAgICAgICBfdGhpcy5tb3ZlVGFic1Njcm9sbChfdGhpcy50YWJzLmNsaWVudFdpZHRoKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVTY3JvbGxiYXJTaXplQ2hhbmdlID0gZnVuY3Rpb24gKF9yZWYyKSB7XG4gICAgICB2YXIgc2Nyb2xsYmFySGVpZ2h0ID0gX3JlZjIuc2Nyb2xsYmFySGVpZ2h0O1xuXG4gICAgICBfdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHNjcm9sbGVyU3R5bGU6IHtcbiAgICAgICAgICBtYXJnaW5Cb3R0b206IC1zY3JvbGxiYXJIZWlnaHRcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSwgX3RoaXMuaGFuZGxlVGFic1Njcm9sbCA9ICgwLCBfZGVib3VuY2UyLmRlZmF1bHQpKGZ1bmN0aW9uICgpIHtcbiAgICAgIF90aGlzLnVwZGF0ZVNjcm9sbEJ1dHRvblN0YXRlKCk7XG4gICAgfSwgMTY2KSwgX3RoaXMubW92ZVRhYnNTY3JvbGwgPSBmdW5jdGlvbiAoZGVsdGEpIHtcbiAgICAgIHZhciB0aGVtZSA9IF90aGlzLnByb3BzLnRoZW1lO1xuXG5cbiAgICAgIGlmIChfdGhpcy50YWJzKSB7XG4gICAgICAgIHZhciB0aGVtZURpcmVjdGlvbiA9IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbjtcbiAgICAgICAgdmFyIG11bHRpcGxpZXIgPSB0aGVtZURpcmVjdGlvbiA9PT0gJ3J0bCcgPyAtMSA6IDE7XG4gICAgICAgIHZhciBuZXh0U2Nyb2xsTGVmdCA9IF90aGlzLnRhYnMuc2Nyb2xsTGVmdCArIGRlbHRhICogbXVsdGlwbGllcjtcbiAgICAgICAgLy8gRml4IGZvciBFZGdlXG4gICAgICAgIHZhciBpbnZlcnQgPSB0aGVtZURpcmVjdGlvbiA9PT0gJ3J0bCcgJiYgKDAsIF9ub3JtYWxpemVTY3JvbGxMZWZ0LmRldGVjdFNjcm9sbFR5cGUpKCkgPT09ICdyZXZlcnNlJyA/IC0xIDogMTtcbiAgICAgICAgX3Njcm9sbDIuZGVmYXVsdC5sZWZ0KF90aGlzLnRhYnMsIGludmVydCAqIG5leHRTY3JvbGxMZWZ0KTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5zY3JvbGxTZWxlY3RlZEludG9WaWV3ID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMyLnRoZW1lLFxuICAgICAgICAgIHZhbHVlID0gX3RoaXMkcHJvcHMyLnZhbHVlO1xuXG5cbiAgICAgIHZhciB0aGVtZURpcmVjdGlvbiA9IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbjtcblxuICAgICAgdmFyIF90aGlzJGdldFRhYnNNZXRhID0gX3RoaXMuZ2V0VGFic01ldGEodmFsdWUsIHRoZW1lRGlyZWN0aW9uKSxcbiAgICAgICAgICB0YWJzTWV0YSA9IF90aGlzJGdldFRhYnNNZXRhLnRhYnNNZXRhLFxuICAgICAgICAgIHRhYk1ldGEgPSBfdGhpcyRnZXRUYWJzTWV0YS50YWJNZXRhO1xuXG4gICAgICBpZiAoIXRhYk1ldGEgfHwgIXRhYnNNZXRhKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgaWYgKHRhYk1ldGEubGVmdCA8IHRhYnNNZXRhLmxlZnQpIHtcbiAgICAgICAgLy8gbGVmdCBzaWRlIG9mIGJ1dHRvbiBpcyBvdXQgb2Ygdmlld1xuICAgICAgICB2YXIgbmV4dFNjcm9sbExlZnQgPSB0YWJzTWV0YS5zY3JvbGxMZWZ0ICsgKHRhYk1ldGEubGVmdCAtIHRhYnNNZXRhLmxlZnQpO1xuICAgICAgICBfc2Nyb2xsMi5kZWZhdWx0LmxlZnQoX3RoaXMudGFicywgbmV4dFNjcm9sbExlZnQpO1xuICAgICAgfSBlbHNlIGlmICh0YWJNZXRhLnJpZ2h0ID4gdGFic01ldGEucmlnaHQpIHtcbiAgICAgICAgLy8gcmlnaHQgc2lkZSBvZiBidXR0b24gaXMgb3V0IG9mIHZpZXdcbiAgICAgICAgdmFyIF9uZXh0U2Nyb2xsTGVmdCA9IHRhYnNNZXRhLnNjcm9sbExlZnQgKyAodGFiTWV0YS5yaWdodCAtIHRhYnNNZXRhLnJpZ2h0KTtcbiAgICAgICAgX3Njcm9sbDIuZGVmYXVsdC5sZWZ0KF90aGlzLnRhYnMsIF9uZXh0U2Nyb2xsTGVmdCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMudXBkYXRlU2Nyb2xsQnV0dG9uU3RhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgc2Nyb2xsYWJsZSA9IF90aGlzJHByb3BzMy5zY3JvbGxhYmxlLFxuICAgICAgICAgIHNjcm9sbEJ1dHRvbnMgPSBfdGhpcyRwcm9wczMuc2Nyb2xsQnV0dG9ucyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMy50aGVtZTtcblxuICAgICAgdmFyIHRoZW1lRGlyZWN0aW9uID0gdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uO1xuXG4gICAgICBpZiAoX3RoaXMudGFicyAmJiBzY3JvbGxhYmxlICYmIHNjcm9sbEJ1dHRvbnMgIT09ICdvZmYnKSB7XG4gICAgICAgIHZhciBfdGhpcyR0YWJzID0gX3RoaXMudGFicyxcbiAgICAgICAgICAgIF9zY3JvbGxXaWR0aCA9IF90aGlzJHRhYnMuc2Nyb2xsV2lkdGgsXG4gICAgICAgICAgICBfY2xpZW50V2lkdGggPSBfdGhpcyR0YWJzLmNsaWVudFdpZHRoO1xuXG4gICAgICAgIHZhciBfc2Nyb2xsTGVmdCA9ICgwLCBfbm9ybWFsaXplU2Nyb2xsTGVmdC5nZXROb3JtYWxpemVkU2Nyb2xsTGVmdCkoX3RoaXMudGFicywgdGhlbWVEaXJlY3Rpb24pO1xuXG4gICAgICAgIHZhciBfc2hvd0xlZnRTY3JvbGwgPSB0aGVtZURpcmVjdGlvbiA9PT0gJ3J0bCcgPyBfc2Nyb2xsV2lkdGggPiBfY2xpZW50V2lkdGggKyBfc2Nyb2xsTGVmdCA6IF9zY3JvbGxMZWZ0ID4gMDtcblxuICAgICAgICB2YXIgX3Nob3dSaWdodFNjcm9sbCA9IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyA/IF9zY3JvbGxMZWZ0ID4gMCA6IF9zY3JvbGxXaWR0aCA+IF9jbGllbnRXaWR0aCArIF9zY3JvbGxMZWZ0O1xuXG4gICAgICAgIGlmIChfc2hvd0xlZnRTY3JvbGwgIT09IF90aGlzLnN0YXRlLnNob3dMZWZ0U2Nyb2xsIHx8IF9zaG93UmlnaHRTY3JvbGwgIT09IF90aGlzLnN0YXRlLnNob3dSaWdodFNjcm9sbCkge1xuICAgICAgICAgIF90aGlzLnNldFN0YXRlKHsgc2hvd0xlZnRTY3JvbGw6IF9zaG93TGVmdFNjcm9sbCwgc2hvd1JpZ2h0U2Nyb2xsOiBfc2hvd1JpZ2h0U2Nyb2xsIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFRhYnMsIFt7XG4gICAga2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSByZWFjdC9uby1kaWQtbW91bnQtc2V0LXN0YXRlXG4gICAgICB0aGlzLnNldFN0YXRlKHsgbW91bnRlZDogdHJ1ZSB9KTtcbiAgICAgIHRoaXMudXBkYXRlSW5kaWNhdG9yU3RhdGUodGhpcy5wcm9wcyk7XG4gICAgICB0aGlzLnVwZGF0ZVNjcm9sbEJ1dHRvblN0YXRlKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcywgcHJldlN0YXRlKSB7XG4gICAgICB0aGlzLnVwZGF0ZVNjcm9sbEJ1dHRvblN0YXRlKCk7XG5cbiAgICAgIC8vIFRoZSBpbmRleCBtaWdodCBoYXZlIGNoYW5nZWQgYXQgdGhlIHNhbWUgdGltZS5cbiAgICAgIC8vIFdlIG5lZWQgdG8gY2hlY2sgYWdhaW4gdGhlIHJpZ2h0IGluZGljYXRvciBwb3NpdGlvbi5cbiAgICAgIHRoaXMudXBkYXRlSW5kaWNhdG9yU3RhdGUodGhpcy5wcm9wcyk7XG5cbiAgICAgIGlmICh0aGlzLnN0YXRlLmluZGljYXRvclN0eWxlICE9PSBwcmV2U3RhdGUuaW5kaWNhdG9yU3R5bGUpIHtcbiAgICAgICAgdGhpcy5zY3JvbGxTZWxlY3RlZEludG9WaWV3KCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHRoaXMuaGFuZGxlUmVzaXplLmNhbmNlbCgpO1xuICAgICAgdGhpcy5oYW5kbGVUYWJzU2Nyb2xsLmNhbmNlbCgpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3VwZGF0ZUluZGljYXRvclN0YXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdXBkYXRlSW5kaWNhdG9yU3RhdGUocHJvcHMpIHtcbiAgICAgIHZhciB0aGVtZSA9IHByb3BzLnRoZW1lLFxuICAgICAgICAgIHZhbHVlID0gcHJvcHMudmFsdWU7XG5cblxuICAgICAgdmFyIHRoZW1lRGlyZWN0aW9uID0gdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uO1xuXG4gICAgICB2YXIgX2dldFRhYnNNZXRhID0gdGhpcy5nZXRUYWJzTWV0YSh2YWx1ZSwgdGhlbWVEaXJlY3Rpb24pLFxuICAgICAgICAgIHRhYnNNZXRhID0gX2dldFRhYnNNZXRhLnRhYnNNZXRhLFxuICAgICAgICAgIHRhYk1ldGEgPSBfZ2V0VGFic01ldGEudGFiTWV0YTtcblxuICAgICAgdmFyIGxlZnQgPSAwO1xuXG4gICAgICBpZiAodGFiTWV0YSAmJiB0YWJzTWV0YSkge1xuICAgICAgICB2YXIgY29ycmVjdGlvbiA9IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyA/IHRhYnNNZXRhLnNjcm9sbExlZnROb3JtYWxpemVkICsgdGFic01ldGEuY2xpZW50V2lkdGggLSB0YWJzTWV0YS5zY3JvbGxXaWR0aCA6IHRhYnNNZXRhLnNjcm9sbExlZnQ7XG4gICAgICAgIGxlZnQgPSB0YWJNZXRhLmxlZnQgLSB0YWJzTWV0YS5sZWZ0ICsgY29ycmVjdGlvbjtcbiAgICAgIH1cblxuICAgICAgdmFyIGluZGljYXRvclN0eWxlID0ge1xuICAgICAgICBsZWZ0OiBsZWZ0LFxuICAgICAgICAvLyBNYXkgYmUgd3JvbmcgdW50aWwgdGhlIGZvbnQgaXMgbG9hZGVkLlxuICAgICAgICB3aWR0aDogdGFiTWV0YSA/IHRhYk1ldGEud2lkdGggOiAwXG4gICAgICB9O1xuXG4gICAgICBpZiAoKGluZGljYXRvclN0eWxlLmxlZnQgIT09IHRoaXMuc3RhdGUuaW5kaWNhdG9yU3R5bGUubGVmdCB8fCBpbmRpY2F0b3JTdHlsZS53aWR0aCAhPT0gdGhpcy5zdGF0ZS5pbmRpY2F0b3JTdHlsZS53aWR0aCkgJiYgISgwLCBfaXNOYW4yLmRlZmF1bHQpKGluZGljYXRvclN0eWxlLmxlZnQpICYmICEoMCwgX2lzTmFuMi5kZWZhdWx0KShpbmRpY2F0b3JTdHlsZS53aWR0aCkpIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGluZGljYXRvclN0eWxlOiBpbmRpY2F0b3JTdHlsZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXMzLFxuICAgICAgICAgIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJ1dHRvbkNsYXNzTmFtZSA9IF9wcm9wcy5idXR0b25DbGFzc05hbWUsXG4gICAgICAgICAgY2VudGVyZWQgPSBfcHJvcHMuY2VudGVyZWQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcCA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBmdWxsV2lkdGggPSBfcHJvcHMuZnVsbFdpZHRoLFxuICAgICAgICAgIGluZGljYXRvckNsYXNzTmFtZSA9IF9wcm9wcy5pbmRpY2F0b3JDbGFzc05hbWUsXG4gICAgICAgICAgaW5kaWNhdG9yQ29sb3IgPSBfcHJvcHMuaW5kaWNhdG9yQ29sb3IsXG4gICAgICAgICAgb25DaGFuZ2UgPSBfcHJvcHMub25DaGFuZ2UsXG4gICAgICAgICAgc2Nyb2xsYWJsZSA9IF9wcm9wcy5zY3JvbGxhYmxlLFxuICAgICAgICAgIHNjcm9sbEJ1dHRvbnMgPSBfcHJvcHMuc2Nyb2xsQnV0dG9ucyxcbiAgICAgICAgICBUYWJTY3JvbGxCdXR0b25Qcm9wID0gX3Byb3BzLlRhYlNjcm9sbEJ1dHRvbixcbiAgICAgICAgICB0ZXh0Q29sb3IgPSBfcHJvcHMudGV4dENvbG9yLFxuICAgICAgICAgIHRoZW1lID0gX3Byb3BzLnRoZW1lLFxuICAgICAgICAgIHZhbHVlID0gX3Byb3BzLnZhbHVlLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2J1dHRvbkNsYXNzTmFtZScsICdjZW50ZXJlZCcsICdjbGFzc2VzJywgJ2NoaWxkcmVuJywgJ2NsYXNzTmFtZScsICdmdWxsV2lkdGgnLCAnaW5kaWNhdG9yQ2xhc3NOYW1lJywgJ2luZGljYXRvckNvbG9yJywgJ29uQ2hhbmdlJywgJ3Njcm9sbGFibGUnLCAnc2Nyb2xsQnV0dG9ucycsICdUYWJTY3JvbGxCdXR0b24nLCAndGV4dENvbG9yJywgJ3RoZW1lJywgJ3ZhbHVlJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lUHJvcCk7XG4gICAgICB2YXIgc2Nyb2xsZXJDbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMuc2Nyb2xsaW5nQ29udGFpbmVyLCAoX2NsYXNzTmFtZXMzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMywgY2xhc3Nlcy5maXhlZCwgIXNjcm9sbGFibGUpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczMsIGNsYXNzZXMuc2Nyb2xsYWJsZSwgc2Nyb2xsYWJsZSksIF9jbGFzc05hbWVzMykpO1xuICAgICAgdmFyIHRhYkl0ZW1Db250YWluZXJDbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMuZmxleENvbnRhaW5lciwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuY2VudGVyZWQsIGNlbnRlcmVkICYmICFzY3JvbGxhYmxlKSk7XG5cbiAgICAgIHZhciBpbmRpY2F0b3IgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfVGFiSW5kaWNhdG9yMi5kZWZhdWx0LCB7XG4gICAgICAgIHN0eWxlOiB0aGlzLnN0YXRlLmluZGljYXRvclN0eWxlLFxuICAgICAgICBjbGFzc05hbWU6IGluZGljYXRvckNsYXNzTmFtZSxcbiAgICAgICAgY29sb3I6IGluZGljYXRvckNvbG9yXG4gICAgICB9KTtcblxuICAgICAgdGhpcy52YWx1ZVRvSW5kZXggPSB7fTtcbiAgICAgIHZhciBjaGlsZEluZGV4ID0gMDtcbiAgICAgIHZhciBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW5Qcm9wLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgaWYgKCFfcmVhY3QyLmRlZmF1bHQuaXNWYWxpZEVsZW1lbnQoY2hpbGQpKSB7XG4gICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cblxuICAgICAgICB2YXIgY2hpbGRWYWx1ZSA9IGNoaWxkLnByb3BzLnZhbHVlIHx8IGNoaWxkSW5kZXg7XG4gICAgICAgIF90aGlzMi52YWx1ZVRvSW5kZXhbY2hpbGRWYWx1ZV0gPSBjaGlsZEluZGV4O1xuICAgICAgICB2YXIgc2VsZWN0ZWQgPSBjaGlsZFZhbHVlID09PSB2YWx1ZTtcblxuICAgICAgICBjaGlsZEluZGV4ICs9IDE7XG4gICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgICAgZnVsbFdpZHRoOiBmdWxsV2lkdGgsXG4gICAgICAgICAgaW5kaWNhdG9yOiBzZWxlY3RlZCAmJiAhX3RoaXMyLnN0YXRlLm1vdW50ZWQgJiYgaW5kaWNhdG9yLFxuICAgICAgICAgIHNlbGVjdGVkOiBzZWxlY3RlZCxcbiAgICAgICAgICBvbkNoYW5nZTogb25DaGFuZ2UsXG4gICAgICAgICAgdGV4dENvbG9yOiB0ZXh0Q29sb3IsXG4gICAgICAgICAgdmFsdWU6IGNoaWxkVmFsdWVcbiAgICAgICAgfSk7XG4gICAgICB9KTtcblxuICAgICAgdmFyIGNvbmRpdGlvbmFsRWxlbWVudHMgPSB0aGlzLmdldENvbmRpdGlvbmFsRWxlbWVudHMoKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX3JlYWN0RXZlbnRMaXN0ZW5lcjIuZGVmYXVsdCwgeyB0YXJnZXQ6ICd3aW5kb3cnLCBvblJlc2l6ZTogdGhpcy5oYW5kbGVSZXNpemUgfSksXG4gICAgICAgIGNvbmRpdGlvbmFsRWxlbWVudHMuc2Nyb2xsYmFyU2l6ZUxpc3RlbmVyLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5mbGV4Q29udGFpbmVyIH0sXG4gICAgICAgICAgY29uZGl0aW9uYWxFbGVtZW50cy5zY3JvbGxCdXR0b25MZWZ0LFxuICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogc2Nyb2xsZXJDbGFzc05hbWUsXG4gICAgICAgICAgICAgIHN0eWxlOiB0aGlzLnN0YXRlLnNjcm9sbGVyU3R5bGUsXG4gICAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgICBfdGhpczIudGFicyA9IG5vZGU7XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIHJvbGU6ICd0YWJsaXN0JyxcbiAgICAgICAgICAgICAgb25TY3JvbGw6IHRoaXMuaGFuZGxlVGFic1Njcm9sbFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgICAgeyBjbGFzc05hbWU6IHRhYkl0ZW1Db250YWluZXJDbGFzc05hbWUgfSxcbiAgICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICAgICksXG4gICAgICAgICAgICB0aGlzLnN0YXRlLm1vdW50ZWQgJiYgaW5kaWNhdG9yXG4gICAgICAgICAgKSxcbiAgICAgICAgICBjb25kaXRpb25hbEVsZW1lbnRzLnNjcm9sbEJ1dHRvblJpZ2h0XG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBUYWJzO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuVGFicy5kZWZhdWx0UHJvcHMgPSB7XG4gIGNlbnRlcmVkOiBmYWxzZSxcbiAgZnVsbFdpZHRoOiBmYWxzZSxcbiAgaW5kaWNhdG9yQ29sb3I6ICdhY2NlbnQnLFxuICBzY3JvbGxhYmxlOiBmYWxzZSxcbiAgc2Nyb2xsQnV0dG9uczogJ2F1dG8nLFxuICBUYWJTY3JvbGxCdXR0b246IF9UYWJTY3JvbGxCdXR0b24yLmRlZmF1bHQsXG4gIHRleHRDb2xvcjogJ2luaGVyaXQnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgd2l0aFRoZW1lOiB0cnVlLCBuYW1lOiAnTXVpVGFicycgfSkoVGFicyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJzLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYnMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1RhYnMgPSByZXF1aXJlKCcuL1RhYnMnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVGFicykuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfVGFiID0gcmVxdWlyZSgnLi9UYWInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdUYWInLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UYWIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3Nob3VsZFVwZGF0ZSA9IHJlcXVpcmUoJy4vc2hvdWxkVXBkYXRlJyk7XG5cbnZhciBfc2hvdWxkVXBkYXRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Nob3VsZFVwZGF0ZSk7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnLi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHB1cmUgPSBmdW5jdGlvbiBwdXJlKEJhc2VDb21wb25lbnQpIHtcbiAgdmFyIGhvYyA9ICgwLCBfc2hvdWxkVXBkYXRlMi5kZWZhdWx0KShmdW5jdGlvbiAocHJvcHMsIG5leHRQcm9wcykge1xuICAgIHJldHVybiAhKDAsIF9zaGFsbG93RXF1YWwyLmRlZmF1bHQpKHByb3BzLCBuZXh0UHJvcHMpO1xuICB9KTtcblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3B1cmUnKSkoaG9jKEJhc2VDb21wb25lbnQpKTtcbiAgfVxuXG4gIHJldHVybiBob2MoQmFzZUNvbXBvbmVudCk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBwdXJlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDQgNiA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zZXRTdGF0aWMgPSByZXF1aXJlKCcuL3NldFN0YXRpYycpO1xuXG52YXIgX3NldFN0YXRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRTdGF0aWMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBzZXREaXNwbGF5TmFtZShkaXNwbGF5TmFtZSkge1xuICByZXR1cm4gKDAsIF9zZXRTdGF0aWMyLmRlZmF1bHQpKCdkaXNwbGF5TmFtZScsIGRpc3BsYXlOYW1lKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgNCA2IDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDQgNiA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCdmYmpzL2xpYi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gX3NoYWxsb3dFcXVhbDIuZGVmYXVsdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgNCA2IDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX3JlYWN0LmNyZWF0ZUZhY3RvcnkpKEJhc2VDb21wb25lbnQpO1xuXG4gICAgdmFyIFNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gICAgICBfaW5oZXJpdHMoU2hvdWxkVXBkYXRlLCBfQ29tcG9uZW50KTtcblxuICAgICAgZnVuY3Rpb24gU2hvdWxkVXBkYXRlKCkge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2hvdWxkVXBkYXRlKTtcblxuICAgICAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NvbXBvbmVudC5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgICAgIH1cblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5zaG91bGRDb21wb25lbnRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgICAgIHJldHVybiB0ZXN0KHRoaXMucHJvcHMsIG5leHRQcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIGZhY3RvcnkodGhpcy5wcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICAgIH0oX3JlYWN0LkNvbXBvbmVudCk7XG5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAnc2hvdWxkVXBkYXRlJykpKFNob3VsZFVwZGF0ZSk7XG4gICAgfVxuICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzaG91bGRVcGRhdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDQgNiA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJy4uL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTUuNDEgMTYuMDlsLTQuNTgtNC41OSA0LjU4LTQuNTlMMTQgNS41bC02IDYgNiA2eicgfSk7XG5cbnZhciBLZXlib2FyZEFycm93TGVmdCA9IGZ1bmN0aW9uIEtleWJvYXJkQXJyb3dMZWZ0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuS2V5Ym9hcmRBcnJvd0xlZnQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEtleWJvYXJkQXJyb3dMZWZ0KTtcbktleWJvYXJkQXJyb3dMZWZ0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEtleWJvYXJkQXJyb3dMZWZ0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9LZXlib2FyZEFycm93TGVmdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dMZWZ0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJy4uL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNOC41OSAxNi4zNGw0LjU4LTQuNTktNC41OC00LjU5TDEwIDUuNzVsNiA2LTYgNnonIH0pO1xuXG52YXIgS2V5Ym9hcmRBcnJvd1JpZ2h0ID0gZnVuY3Rpb24gS2V5Ym9hcmRBcnJvd1JpZ2h0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuS2V5Ym9hcmRBcnJvd1JpZ2h0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShLZXlib2FyZEFycm93UmlnaHQpO1xuS2V5Ym9hcmRBcnJvd1JpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEtleWJvYXJkQXJyb3dSaWdodDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd1JpZ2h0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd1JpZ2h0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIlwidXNlIHN0cmljdFwiO1xyXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHsgdmFsdWU6IHRydWUgfSk7XHJcbi8vIEJhc2VkIG9uIGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1ib290c3RyYXAvZG9tLWhlbHBlcnMvYmxvYi9tYXN0ZXIvc3JjL3V0aWwvaW5ET00uanNcclxudmFyIGluRE9NID0gISEodHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCcgJiYgd2luZG93LmRvY3VtZW50ICYmIHdpbmRvdy5kb2N1bWVudC5jcmVhdGVFbGVtZW50KTtcclxudmFyIGNhY2hlZFR5cGU7XHJcbmZ1bmN0aW9uIF9zZXRTY3JvbGxUeXBlKHR5cGUpIHtcclxuICAgIGNhY2hlZFR5cGUgPSB0eXBlO1xyXG59XHJcbmV4cG9ydHMuX3NldFNjcm9sbFR5cGUgPSBfc2V0U2Nyb2xsVHlwZTtcclxuLy8gQmFzZWQgb24gdGhlIGpxdWVyeSBwbHVnaW4gaHR0cHM6Ly9naXRodWIuY29tL290aHJlZS9qcXVlcnkucnRsLXNjcm9sbC10eXBlXHJcbmZ1bmN0aW9uIGRldGVjdFNjcm9sbFR5cGUoKSB7XHJcbiAgICBpZiAoY2FjaGVkVHlwZSkge1xyXG4gICAgICAgIHJldHVybiBjYWNoZWRUeXBlO1xyXG4gICAgfVxyXG4gICAgaWYgKCFpbkRPTSB8fCAhd2luZG93LmRvY3VtZW50LmJvZHkpIHtcclxuICAgICAgICByZXR1cm4gJ2luZGV0ZXJtaW5hdGUnO1xyXG4gICAgfVxyXG4gICAgdmFyIGR1bW15ID0gd2luZG93LmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgZHVtbXkuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoJ0FCQ0QnKSk7XHJcbiAgICBkdW1teS5kaXIgPSAncnRsJztcclxuICAgIGR1bW15LnN0eWxlLmZvbnRTaXplID0gJzE0cHgnO1xyXG4gICAgZHVtbXkuc3R5bGUud2lkdGggPSAnNHB4JztcclxuICAgIGR1bW15LnN0eWxlLmhlaWdodCA9ICcxcHgnO1xyXG4gICAgZHVtbXkuc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xyXG4gICAgZHVtbXkuc3R5bGUudG9wID0gJy0xMDAwcHgnO1xyXG4gICAgZHVtbXkuc3R5bGUub3ZlcmZsb3cgPSAnc2Nyb2xsJztcclxuICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoZHVtbXkpO1xyXG4gICAgY2FjaGVkVHlwZSA9ICdyZXZlcnNlJztcclxuICAgIGlmIChkdW1teS5zY3JvbGxMZWZ0ID4gMCkge1xyXG4gICAgICAgIGNhY2hlZFR5cGUgPSAnZGVmYXVsdCc7XHJcbiAgICB9XHJcbiAgICBlbHNlIHtcclxuICAgICAgICBkdW1teS5zY3JvbGxMZWZ0ID0gMTtcclxuICAgICAgICBpZiAoZHVtbXkuc2Nyb2xsTGVmdCA9PT0gMCkge1xyXG4gICAgICAgICAgICBjYWNoZWRUeXBlID0gJ25lZ2F0aXZlJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKGR1bW15KTtcclxuICAgIHJldHVybiBjYWNoZWRUeXBlO1xyXG59XHJcbmV4cG9ydHMuZGV0ZWN0U2Nyb2xsVHlwZSA9IGRldGVjdFNjcm9sbFR5cGU7XHJcbi8vIEJhc2VkIG9uIGh0dHBzOi8vc3RhY2tvdmVyZmxvdy5jb20vYS8yNDM5NDM3NlxyXG5mdW5jdGlvbiBnZXROb3JtYWxpemVkU2Nyb2xsTGVmdChlbGVtZW50LCBkaXJlY3Rpb24pIHtcclxuICAgIHZhciBzY3JvbGxMZWZ0ID0gZWxlbWVudC5zY3JvbGxMZWZ0O1xyXG4gICAgLy8gUGVyZm9ybSB0aGUgY2FsY3VsYXRpb25zIG9ubHkgd2hlbiBkaXJlY3Rpb24gaXMgcnRsIHRvIGF2b2lkIG1lc3NpbmcgdXAgdGhlIGx0ciBiYWhhdmlvclxyXG4gICAgaWYgKGRpcmVjdGlvbiAhPT0gJ3J0bCcpIHtcclxuICAgICAgICByZXR1cm4gc2Nyb2xsTGVmdDtcclxuICAgIH1cclxuICAgIHZhciB0eXBlID0gZGV0ZWN0U2Nyb2xsVHlwZSgpO1xyXG4gICAgaWYgKHR5cGUgPT09ICdpbmRldGVybWluYXRlJykge1xyXG4gICAgICAgIHJldHVybiBOdW1iZXIuTmFOO1xyXG4gICAgfVxyXG4gICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgY2FzZSAnbmVnYXRpdmUnOlxyXG4gICAgICAgICAgICByZXR1cm4gZWxlbWVudC5zY3JvbGxXaWR0aCAtIGVsZW1lbnQuY2xpZW50V2lkdGggKyBzY3JvbGxMZWZ0O1xyXG4gICAgICAgIGNhc2UgJ3JldmVyc2UnOlxyXG4gICAgICAgICAgICByZXR1cm4gZWxlbWVudC5zY3JvbGxXaWR0aCAtIGVsZW1lbnQuY2xpZW50V2lkdGggLSBzY3JvbGxMZWZ0O1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHNjcm9sbExlZnQ7XHJcbn1cclxuZXhwb3J0cy5nZXROb3JtYWxpemVkU2Nyb2xsTGVmdCA9IGdldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0O1xyXG5mdW5jdGlvbiBzZXROb3JtYWxpemVkU2Nyb2xsTGVmdChlbGVtZW50LCBzY3JvbGxMZWZ0LCBkaXJlY3Rpb24pIHtcclxuICAgIC8vIFBlcmZvcm0gdGhlIGNhbGN1bGF0aW9ucyBvbmx5IHdoZW4gZGlyZWN0aW9uIGlzIHJ0bCB0byBhdm9pZCBtZXNzaW5nIHVwIHRoZSBsdHIgYmFoYXZpb3JcclxuICAgIGlmIChkaXJlY3Rpb24gIT09ICdydGwnKSB7XHJcbiAgICAgICAgZWxlbWVudC5zY3JvbGxMZWZ0ID0gc2Nyb2xsTGVmdDtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICB2YXIgdHlwZSA9IGRldGVjdFNjcm9sbFR5cGUoKTtcclxuICAgIGlmICh0eXBlID09PSAnaW5kZXRlcm1pbmF0ZScpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICB9XHJcbiAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICBjYXNlICduZWdhdGl2ZSc6XHJcbiAgICAgICAgICAgIGVsZW1lbnQuc2Nyb2xsTGVmdCA9IGVsZW1lbnQuY2xpZW50V2lkdGggLSBlbGVtZW50LnNjcm9sbFdpZHRoICsgc2Nyb2xsTGVmdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSAncmV2ZXJzZSc6XHJcbiAgICAgICAgICAgIGVsZW1lbnQuc2Nyb2xsTGVmdCA9IGVsZW1lbnQuc2Nyb2xsV2lkdGggLSBlbGVtZW50LmNsaWVudFdpZHRoIC0gc2Nyb2xsTGVmdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgZWxlbWVudC5zY3JvbGxMZWZ0ID0gc2Nyb2xsTGVmdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICB9XHJcbn1cclxuZXhwb3J0cy5zZXROb3JtYWxpemVkU2Nyb2xsTGVmdCA9IHNldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0O1xyXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUtc2Nyb2xsLWxlZnQvbGliL21haW4uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS1zY3JvbGwtbGVmdC9saWIvbWFpbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCJ2YXIgZ2xvYmFsID0gcmVxdWlyZSgnZ2xvYmFsJylcblxuLyoqXG4gKiBgcmVxdWVzdEFuaW1hdGlvbkZyYW1lKClgXG4gKi9cblxudmFyIHJlcXVlc3QgPSBnbG9iYWwucmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gIHx8IGdsb2JhbC53ZWJraXRSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgfHwgZ2xvYmFsLm1velJlcXVlc3RBbmltYXRpb25GcmFtZVxuICB8fCBmYWxsYmFja1xuXG52YXIgcHJldiA9ICtuZXcgRGF0ZVxuZnVuY3Rpb24gZmFsbGJhY2sgKGZuKSB7XG4gIHZhciBjdXJyID0gK25ldyBEYXRlXG4gIHZhciBtcyA9IE1hdGgubWF4KDAsIDE2IC0gKGN1cnIgLSBwcmV2KSlcbiAgdmFyIHJlcSA9IHNldFRpbWVvdXQoZm4sIG1zKVxuICByZXR1cm4gcHJldiA9IGN1cnIsIHJlcVxufVxuXG4vKipcbiAqIGBjYW5jZWxBbmltYXRpb25GcmFtZSgpYFxuICovXG5cbnZhciBjYW5jZWwgPSBnbG9iYWwuY2FuY2VsQW5pbWF0aW9uRnJhbWVcbiAgfHwgZ2xvYmFsLndlYmtpdENhbmNlbEFuaW1hdGlvbkZyYW1lXG4gIHx8IGdsb2JhbC5tb3pDYW5jZWxBbmltYXRpb25GcmFtZVxuICB8fCBjbGVhclRpbWVvdXRcblxuaWYgKEZ1bmN0aW9uLnByb3RvdHlwZS5iaW5kKSB7XG4gIHJlcXVlc3QgPSByZXF1ZXN0LmJpbmQoZ2xvYmFsKVxuICBjYW5jZWwgPSBjYW5jZWwuYmluZChnbG9iYWwpXG59XG5cbmV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHJlcXVlc3RcbmV4cG9ydHMuY2FuY2VsID0gY2FuY2VsXG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yYWZsL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yYWZsL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG5cdHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfcmVhY3RFdmVudExpc3RlbmVyID0gcmVxdWlyZSgncmVhY3QtZXZlbnQtbGlzdGVuZXInKTtcblxudmFyIF9yZWFjdEV2ZW50TGlzdGVuZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3RFdmVudExpc3RlbmVyKTtcblxudmFyIF9zdGlmbGUgPSByZXF1aXJlKCdzdGlmbGUnKTtcblxudmFyIF9zdGlmbGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc3RpZmxlKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHN0eWxlcyA9IHtcblx0d2lkdGg6ICcxMDBweCcsXG5cdGhlaWdodDogJzEwMHB4Jyxcblx0cG9zaXRpb246ICdhYnNvbHV0ZScsXG5cdHRvcDogJy0xMDAwMDBweCcsXG5cdG92ZXJmbG93OiAnc2Nyb2xsJyxcblx0bXNPdmVyZmxvd1N0eWxlOiAnc2Nyb2xsYmFyJ1xufTtcblxudmFyIFNjcm9sbGJhclNpemUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuXHQoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShTY3JvbGxiYXJTaXplLCBfQ29tcG9uZW50KTtcblxuXHRmdW5jdGlvbiBTY3JvbGxiYXJTaXplKCkge1xuXHRcdHZhciBfcmVmO1xuXG5cdFx0dmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuXHRcdCgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFNjcm9sbGJhclNpemUpO1xuXG5cdFx0Zm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcblx0XHRcdGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG5cdFx0fVxuXG5cdFx0cmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gU2Nyb2xsYmFyU2l6ZS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU2Nyb2xsYmFyU2l6ZSkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnNldE1lYXN1cmVtZW50cyA9IGZ1bmN0aW9uICgpIHtcblx0XHRcdF90aGlzLnNjcm9sbGJhckhlaWdodCA9IF90aGlzLm5vZGUub2Zmc2V0SGVpZ2h0IC0gX3RoaXMubm9kZS5jbGllbnRIZWlnaHQ7XG5cdFx0XHRfdGhpcy5zY3JvbGxiYXJXaWR0aCA9IF90aGlzLm5vZGUub2Zmc2V0V2lkdGggLSBfdGhpcy5ub2RlLmNsaWVudFdpZHRoO1xuXHRcdH0sIF90aGlzLmhhbmRsZVJlc2l6ZSA9ICgwLCBfc3RpZmxlMi5kZWZhdWx0KShmdW5jdGlvbiAoKSB7XG5cdFx0XHR2YXIgb25DaGFuZ2UgPSBfdGhpcy5wcm9wcy5vbkNoYW5nZTtcblxuXG5cdFx0XHR2YXIgcHJldkhlaWdodCA9IF90aGlzLnNjcm9sbGJhckhlaWdodDtcblx0XHRcdHZhciBwcmV2V2lkdGggPSBfdGhpcy5zY3JvbGxiYXJXaWR0aDtcblx0XHRcdF90aGlzLnNldE1lYXN1cmVtZW50cygpO1xuXHRcdFx0aWYgKHByZXZIZWlnaHQgIT09IF90aGlzLnNjcm9sbGJhckhlaWdodCB8fCBwcmV2V2lkdGggIT09IF90aGlzLnNjcm9sbGJhcldpZHRoKSB7XG5cdFx0XHRcdG9uQ2hhbmdlKHsgc2Nyb2xsYmFySGVpZ2h0OiBfdGhpcy5zY3JvbGxiYXJIZWlnaHQsIHNjcm9sbGJhcldpZHRoOiBfdGhpcy5zY3JvbGxiYXJXaWR0aCB9KTtcblx0XHRcdH1cblx0XHR9LCAxNjYpLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuXHR9XG5cblx0KDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU2Nyb2xsYmFyU2l6ZSwgW3tcblx0XHRrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuXHRcdFx0dmFyIG9uTG9hZCA9IHRoaXMucHJvcHMub25Mb2FkO1xuXG5cblx0XHRcdGlmIChvbkxvYWQpIHtcblx0XHRcdFx0dGhpcy5zZXRNZWFzdXJlbWVudHMoKTtcblx0XHRcdFx0b25Mb2FkKHsgc2Nyb2xsYmFySGVpZ2h0OiB0aGlzLnNjcm9sbGJhckhlaWdodCwgc2Nyb2xsYmFyV2lkdGg6IHRoaXMuc2Nyb2xsYmFyV2lkdGggfSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcblx0XHRcdHRoaXMuaGFuZGxlUmVzaXplLmNhbmNlbCgpO1xuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ3JlbmRlcicsXG5cdFx0Ly8gQ29ycmVzcG9uZHMgdG8gMTAgZnJhbWVzIGF0IDYwIEh6LlxuXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcblx0XHRcdHZhciBfdGhpczIgPSB0aGlzO1xuXG5cdFx0XHR2YXIgb25DaGFuZ2UgPSB0aGlzLnByb3BzLm9uQ2hhbmdlO1xuXG5cblx0XHRcdHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcblx0XHRcdFx0J2RpdicsXG5cdFx0XHRcdG51bGwsXG5cdFx0XHRcdG9uQ2hhbmdlID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX3JlYWN0RXZlbnRMaXN0ZW5lcjIuZGVmYXVsdCwgeyB0YXJnZXQ6ICd3aW5kb3cnLCBvblJlc2l6ZTogdGhpcy5oYW5kbGVSZXNpemUgfSkgOiBudWxsLFxuXHRcdFx0XHRfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnZGl2Jywge1xuXHRcdFx0XHRcdHN0eWxlOiBzdHlsZXMsXG5cdFx0XHRcdFx0cmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuXHRcdFx0XHRcdFx0X3RoaXMyLm5vZGUgPSBub2RlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSlcblx0XHRcdCk7XG5cdFx0fVxuXHR9XSk7XG5cdHJldHVybiBTY3JvbGxiYXJTaXplO1xufShfcmVhY3QuQ29tcG9uZW50KTtcblxuU2Nyb2xsYmFyU2l6ZS5kZWZhdWx0UHJvcHMgPSB7XG5cdG9uTG9hZDogbnVsbCxcblx0b25DaGFuZ2U6IG51bGxcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBTY3JvbGxiYXJTaXplO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXNjcm9sbGJhci1zaXplL1Njcm9sbGJhclNpemUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXNjcm9sbGJhci1zaXplL1Njcm9sbGJhclNpemUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1Njcm9sbGJhclNpemUgPSByZXF1aXJlKCcuL1Njcm9sbGJhclNpemUnKTtcblxudmFyIF9TY3JvbGxiYXJTaXplMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1Njcm9sbGJhclNpemUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfU2Nyb2xsYmFyU2l6ZTIuZGVmYXVsdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2Nyb2xsYmFyLXNpemUvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwidmFyIHJhZiA9IHJlcXVpcmUoJ3JhZmwnKVxuXG5mdW5jdGlvbiBzY3JvbGwgKHByb3AsIGVsZW1lbnQsIHRvLCBvcHRpb25zLCBjYWxsYmFjaykge1xuICB2YXIgc3RhcnQgPSArbmV3IERhdGVcbiAgdmFyIGZyb20gPSBlbGVtZW50W3Byb3BdXG4gIHZhciBjYW5jZWxsZWQgPSBmYWxzZVxuXG4gIHZhciBlYXNlID0gaW5PdXRTaW5lXG4gIHZhciBkdXJhdGlvbiA9IDM1MFxuXG4gIGlmICh0eXBlb2Ygb3B0aW9ucyA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIGNhbGxiYWNrID0gb3B0aW9uc1xuICB9XG4gIGVsc2Uge1xuICAgIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9XG4gICAgZWFzZSA9IG9wdGlvbnMuZWFzZSB8fCBlYXNlXG4gICAgZHVyYXRpb24gPSBvcHRpb25zLmR1cmF0aW9uIHx8IGR1cmF0aW9uXG4gICAgY2FsbGJhY2sgPSBjYWxsYmFjayB8fCBmdW5jdGlvbiAoKSB7fVxuICB9XG5cbiAgaWYgKGZyb20gPT09IHRvKSB7XG4gICAgcmV0dXJuIGNhbGxiYWNrKFxuICAgICAgbmV3IEVycm9yKCdFbGVtZW50IGFscmVhZHkgYXQgdGFyZ2V0IHNjcm9sbCBwb3NpdGlvbicpLFxuICAgICAgZWxlbWVudFtwcm9wXVxuICAgIClcbiAgfVxuXG4gIGZ1bmN0aW9uIGNhbmNlbCAoKSB7XG4gICAgY2FuY2VsbGVkID0gdHJ1ZVxuICB9XG5cbiAgZnVuY3Rpb24gYW5pbWF0ZSAodGltZXN0YW1wKSB7XG4gICAgaWYgKGNhbmNlbGxlZCkge1xuICAgICAgcmV0dXJuIGNhbGxiYWNrKFxuICAgICAgICBuZXcgRXJyb3IoJ1Njcm9sbCBjYW5jZWxsZWQnKSxcbiAgICAgICAgZWxlbWVudFtwcm9wXVxuICAgICAgKVxuICAgIH1cblxuICAgIHZhciBub3cgPSArbmV3IERhdGVcbiAgICB2YXIgdGltZSA9IE1hdGgubWluKDEsICgobm93IC0gc3RhcnQpIC8gZHVyYXRpb24pKVxuICAgIHZhciBlYXNlZCA9IGVhc2UodGltZSlcblxuICAgIGVsZW1lbnRbcHJvcF0gPSAoZWFzZWQgKiAodG8gLSBmcm9tKSkgKyBmcm9tXG5cbiAgICB0aW1lIDwgMSA/IHJhZihhbmltYXRlKSA6IHJhZihmdW5jdGlvbiAoKSB7XG4gICAgICBjYWxsYmFjayhudWxsLCBlbGVtZW50W3Byb3BdKVxuICAgIH0pXG4gIH1cblxuICByYWYoYW5pbWF0ZSlcblxuICByZXR1cm4gY2FuY2VsXG59XG5cbmZ1bmN0aW9uIGluT3V0U2luZSAobikge1xuICByZXR1cm4gLjUgKiAoMSAtIE1hdGguY29zKE1hdGguUEkgKiBuKSlcbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIHRvcDogZnVuY3Rpb24gKGVsZW1lbnQsIHRvLCBvcHRpb25zLCBjYWxsYmFjaykge1xuICAgIHJldHVybiBzY3JvbGwoJ3Njcm9sbFRvcCcsIGVsZW1lbnQsIHRvLCBvcHRpb25zLCBjYWxsYmFjaylcbiAgfSxcbiAgbGVmdDogZnVuY3Rpb24gKGVsZW1lbnQsIHRvLCBvcHRpb25zLCBjYWxsYmFjaykge1xuICAgIHJldHVybiBzY3JvbGwoJ3Njcm9sbExlZnQnLCBlbGVtZW50LCB0bywgb3B0aW9ucywgY2FsbGJhY2spXG4gIH1cbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3Njcm9sbC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvc2Nyb2xsL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIm1vZHVsZS5leHBvcnRzID0gc3RpZmxlO1xuXG5cbmZ1bmN0aW9uIHN0aWZsZSAoZm4sIHdhaXQpIHtcbiAgaWYgKHR5cGVvZiBmbiAhPT0gJ2Z1bmN0aW9uJyB8fCB0eXBlb2Ygd2FpdCAhPT0gJ251bWJlcicpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3N0aWZsZShmbiwgd2FpdCkgLS0gZXhwZWN0ZWQgYSBmdW5jdGlvbiBhbmQgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcywgZ290ICgnICsgdHlwZW9mIGZuICsgJywgJyArIHR5cGVvZiB3YWl0ICsgJyknKTtcbiAgfVxuXG4gIHZhciB0aW1lcjsgICAgLy8gVGltZXIgdG8gZmlyZSBhZnRlciBgd2FpdGAgaGFzIGVsYXBzZWRcbiAgdmFyIGNhbGxlZDsgICAvLyBLZWVwIHRyYWNrIGlmIGl0IGdldHMgY2FsbGVkIGR1cmluZyB0aGUgYHdhaXRgXG5cbiAgdmFyIHdyYXBwZXIgPSBmdW5jdGlvbiAoKSB7XG5cbiAgICAvLyBDaGVjayBpZiBzdGlsbCBcImNvb2xpbmcgZG93blwiIGZyb20gYSBwcmV2aW91cyBjYWxsXG4gICAgaWYgKHRpbWVyKSB7XG4gICAgICBjYWxsZWQgPSB0cnVlO1xuICAgIH0gZWxzZSB7XG4gICAgICAvLyBTdGFydCBhIHRpbWVyIHRvIGZpcmUgYWZ0ZXIgdGhlIGB3YWl0YCBpcyBvdmVyXG4gICAgICB0aW1lciA9IHNldFRpbWVvdXQoYWZ0ZXJXYWl0LCB3YWl0KTtcbiAgICAgIC8vIEFuZCBjYWxsIHRoZSB3cmFwcGVkIGZ1bmN0aW9uXG4gICAgICBmbigpO1xuICAgIH1cbiAgfVxuXG4gIC8vIEFkZCBhIGNhbmNlbCBtZXRob2QsIHRvIGtpbGwgYW5kIHBlbmRpbmcgY2FsbHNcbiAgd3JhcHBlci5jYW5jZWwgPSBmdW5jdGlvbiAoKSB7XG4gICAgLy8gQ2xlYXIgdGhlIGNhbGxlZCBmbGFnLCBvciBpdCB3b3VsZCBmaXJlIHR3aWNlIHdoZW4gY2FsbGVkIGFnYWluIGxhdGVyXG4gICAgY2FsbGVkID0gZmFsc2U7XG5cbiAgICAvLyBUdXJuIG9mZiB0aGUgdGltZXIsIHNvIGl0IHdvbid0IGZpcmUgYWZ0ZXIgdGhlIHdhaXQgZXhwaXJlc1xuICAgIGlmICh0aW1lcikge1xuICAgICAgY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgICAgIHRpbWVyID0gdW5kZWZpbmVkO1xuICAgIH1cbiAgfVxuXG4gIGZ1bmN0aW9uIGFmdGVyV2FpdCgpIHtcbiAgICAvLyBFbXB0eSBvdXQgdGhlIHRpbWVyXG4gICAgdGltZXIgPSB1bmRlZmluZWQ7XG5cbiAgICAvLyBJZiBpdCB3YXMgY2FsbGVkIGR1cmluZyB0aGUgYHdhaXRgLCBmaXJlIGl0IGFnYWluXG4gICAgaWYgKGNhbGxlZCkge1xuICAgICAgY2FsbGVkID0gZmFsc2U7XG4gICAgICB3cmFwcGVyKCk7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHdyYXBwZXI7XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zdGlmbGUvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3N0aWZsZS9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQge1xuICBFTEVNRU5UX0dFVF9TVEFSVCxcbiAgRUxFTUVOVF9HRVRfRVJST1IsXG4gIEVMRU1FTlRfR0VUX1NVQ0NFU1MsXG59IGZyb20gJy4uLy4uL2NvbnN0YW50cy9lbGVtZW50cyc7XG5cbmltcG9ydCBhcGlHZXRCeUlkIGZyb20gJy4uLy4uL2FwaS9lbGVtZW50cy9nZXRCeUlkJztcblxuLyoqXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEVsZW1lbnRTdGFydCA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UX0dFVF9TVEFSVCwgcGF5bG9hZCB9KTtcblxuLyoqXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnN0YXR1c0NvZGVcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC51c2VybmFtZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZWxlbWVudFR5cGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNpcmNsZVR5cGUgLSBvbmx5IGlmIGVsZW1lbnRUeXBlIGlzICdjaXJjbGUnXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5uYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmF2YXRhciAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jb3ZlciAtXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEVsZW1lbnRTdWNjZXNzID0gKHBheWxvYWQpID0+ICh7IHR5cGU6IEVMRU1FTlRfR0VUX1NVQ0NFU1MsIHBheWxvYWQgfSk7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZXJyb3JcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0RWxlbWVudEVycm9yID0gKHBheWxvYWQpID0+ICh7IHR5cGU6IEVMRU1FTlRfR0VUX0VSUk9SLCBwYXlsb2FkIH0pO1xuXG4vKipcbiAqXG4gKiBAZnVuY3Rpb24gZ2V0XG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW5cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKi9cbmNvbnN0IGdldCA9ICh7IHRva2VuLCBpZCB9KSA9PiAoZGlzcGF0Y2gpID0+IHtcbiAgZGlzcGF0Y2goZ2V0RWxlbWVudFN0YXJ0KHsgaWQgfSkpO1xuXG4gIGFwaUdldEJ5SWQoeyB0b2tlbiwgaWQgfSlcbiAgICAudGhlbigoeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9KSA9PiB7XG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgZGlzcGF0Y2goZ2V0RWxlbWVudEVycm9yKHsgaWQsIHN0YXR1c0NvZGUsIGVycm9yIH0pKTtcblxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGRpc3BhdGNoKGdldEVsZW1lbnRTdWNjZXNzKHsgaWQsIHN0YXR1c0NvZGUsIC4uLnBheWxvYWQgfSkpO1xuICAgIH0pXG4gICAgLmNhdGNoKCh7IHN0YXR1c0NvZGUsIGVycm9yIH0pID0+IHtcbiAgICAgIGRpc3BhdGNoKGdldEVsZW1lbnRFcnJvcih7IGlkLCBzdGF0dXNDb2RlLCBlcnJvciB9KSk7XG4gICAgfSk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBnZXQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZC5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgVVNFUl9VUERBVEUgZnJvbSAnLi4vLi4vY29uc3RhbnRzL3VzZXJzL3VwZGF0ZSc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgaWQ6IG51bWJlcixcbiAgdXNlcm5hbWU/OiBzdHJpbmcsXG4gIG5hbWU/OiBzdHJpbmcsXG4gIGF2YXRhcj86IHN0cmluZyxcbiAgY292ZXI/OiBzdHJpbmcsXG4gIHJlc3VtZT86IGFueSxcbn07XG5cbnR5cGUgQWN0aW9uID0ge1xuICB0eXBlOiBzdHJpbmcsXG4gIHBheWxvYWQ6IFBheWxvYWQsXG59O1xuXG4vKipcbiAqIFVzZXIgdXBkYXRlIGFjdGlvbi5cbiAqIFVzZXIgY2FuIHVwZGF0ZSB0aGVpciBpbmZvIGluIHJlZHV4IHN0b3JlIGJ5IGl0J3MgJ2lkJyBvbmx5LlxuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudXNlcm5hbWUgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmFtZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5hdmF0YXIgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY292ZXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQucmVzdW1lIC1cbiAqXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICovXG5jb25zdCB1cGRhdGUgPSAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoeyB0eXBlOiBVU0VSX1VQREFURSwgcGF5bG9hZCB9KTtcblxuZXhwb3J0IGRlZmF1bHQgdXBkYXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL3VzZXJzL3VwZGF0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqXG4gKiBAZnVuY3Rpb24gZ2V0QnlJZFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICovXG5hc3luYyBmdW5jdGlvbiBnZXRCeUlkKHsgdG9rZW4sIGlkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvZWxlbWVudHMvJHtpZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdldEJ5SWQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRCeUlkLmpzIiwiLyoqXG4gKiBUaGlzIGZpbGUgY29udGFpbnMgYWN0aW9uIGNvbnN0YW50cyBmb3IgVXNlcidzIHVwZGF0ZVxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5jb25zdCBVU0VSX1VQREFURSA9ICdVU0VSX1VQREFURSc7XG5cbmV4cG9ydCBkZWZhdWx0IFVTRVJfVVBEQVRFO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb25zdGFudHMvdXNlcnMvdXBkYXRlLmpzIiwiLyoqXG4gKiBwcm9maWxlIHBhZ2UgY29tcG9uZW50XG4gKiBJdCB3aWxsIHJlbmRlciAgQWNjb3VudCwgQmlsbGluZywgQWNjb3VudCwgUGFzc3dvcmQgZXRjLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBMb2FkYWJsZSBmcm9tICdyZWFjdC1sb2FkYWJsZSc7XG5pbXBvcnQgeyBSb3V0ZSwgU3dpdGNoIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgQXBwQmFyIGZyb20gJ21hdGVyaWFsLXVpL0FwcEJhcic7XG5pbXBvcnQgVGFicywgeyBUYWIgfSBmcm9tICdtYXRlcmlhbC11aS9UYWJzJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuaW1wb3J0IGFjdGlvbkVsZW1lbnRHZXQgZnJvbSAnLi4vLi4vYWN0aW9ucy9lbGVtZW50cy9nZXRCeUlkJztcbmltcG9ydCBhY3Rpb25Vc2VyVXBkYXRlIGZyb20gJy4uLy4uL2FjdGlvbnMvdXNlcnMvdXBkYXRlJztcblxuY29uc3QgQXN5bmNQcm9maWxlID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9Qcm9maWxlJyksXG4gIG1vZHVsZXM6IFsnLi9Qcm9maWxlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNBY2NvdW50ID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9BY2NvdW50JyksXG4gIG1vZHVsZXM6IFsnLi9BY2NvdW50J10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNCaWxsaW5nID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9CaWxsaW5nJyksXG4gIG1vZHVsZXM6IFsnLi9CaWxsaW5nJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNFbWFpbCA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vRW1haWwnKSxcbiAgbW9kdWxlczogWycuL0VtYWlsJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNQYXNzd29yZCA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vUGFzc3dvcmQnKSxcbiAgbW9kdWxlczogWycuL1Bhc3N3b3JkJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNBcHBseUZvckd1cnUgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuL0FwcGx5Rm9yR3VydScpLFxuICBtb2R1bGVzOiBbJy4vQXBwbHlGb3JHdXJ1J10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNFcnJvclBhZ2UgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuLi8uLi9jb21wb25lbnRzL0Vycm9yUGFnZScpLFxuICBtb2R1bGVzOiBbJy4uLy4uL2NvbXBvbmVudHMvRXJyb3JQYWdlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZmxleDogJzEgMCAxMDAlJyxcbiAgfSxcbn07XG5cbi8vIHRoaXMgZnVuY3Rpb24gd2lsbCByZXR1cm4gaW5kZXggb2Ygcm91dGUgdXJsIGluIERhc2hib2FyZCBjb21wb25lbnQgcGFnZS5cbmNvbnN0IHJvdXRlU3RhdGVzID0gKHJvdXRlID0gJy9wcm9maWxlJykgPT4ge1xuICBjb25zdCByb3V0ZXMgPSB7XG4gICAgJy9wcm9maWxlJzogMCxcbiAgICAnL3Byb2ZpbGUvYWNjb3VudCc6IDEsXG4gICAgJy9wcm9maWxlL2JpbGxpbmcnOiAyLFxuICAgICcvcHJvZmlsZS9lbWFpbCc6IDMsXG4gICAgJy9wcm9maWxlL3Bhc3N3b3JkJzogNCxcbiAgfTtcblxuICAvLyBpZiByb3V0ZXNbaW5kZXhdIHZhbHVlIGlzIHVuZGVmaW5lZCwgaXQgd2lsbCByZXR1cm4gcm91dGVzLmxlbmd0aDtcbiAgcmV0dXJuIHR5cGVvZiByb3V0ZXNbcm91dGVdICE9PSAndW5kZWZpbmVkJ1xuICAgID8gcm91dGVzW3JvdXRlXVxuICAgIDogT2JqZWN0LmtleXMocm91dGVzKS5sZW5ndGg7XG59O1xuXG5jbGFzcyBQcm9maWxlUGFnZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICB2YWx1ZTogcm91dGVTdGF0ZXMocHJvcHMubG9jYXRpb24ucGF0aG5hbWUpLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICB9XG5cbiAgb25DaGFuZ2UoZXZlbnQsIHZhbHVlKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IHZhbHVlIH0pO1xuXG4gICAgY29uc3QgeyBoaXN0b3J5IH0gPSB0aGlzLnByb3BzO1xuXG4gICAgc3dpdGNoICh2YWx1ZSkge1xuICAgICAgY2FzZSAwOlxuICAgICAgICBoaXN0b3J5LnB1c2goJy9wcm9maWxlJyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgY2FzZSAxOlxuICAgICAgICBoaXN0b3J5LnB1c2goJy9wcm9maWxlL2FjY291bnQnKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIDI6XG4gICAgICAgIGhpc3RvcnkucHVzaCgnL3Byb2ZpbGUvYmlsbGluZycpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgMzpcbiAgICAgICAgaGlzdG9yeS5wdXNoKCcvcHJvZmlsZS9lbWFpbCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgNDpcbiAgICAgICAgaGlzdG9yeS5wdXNoKCcvcHJvZmlsZS9wYXNzd29yZCcpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgNTpcbiAgICAgICAgaGlzdG9yeS5wdXNoKCcvcHJvZmlsZS9ndXJ1Jyk7XG4gICAgICAgIGJyZWFrO1xuICAgICAgZGVmYXVsdDpcbiAgICAgICAgaGlzdG9yeS5wdXNoKCcvcHJvZmlsZScpO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHVzZXIgPSBlbGVtZW50cy51c2VyO1xuXG4gICAgY29uc3QgeyB2YWx1ZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPEFwcEJhciBwb3NpdGlvbj1cInN0YXRpY1wiIGNvbG9yPVwiZGVmYXVsdFwiPlxuICAgICAgICAgIDxUYWJzXG4gICAgICAgICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgIGluZGljYXRvckNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICB0ZXh0Q29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgIHNjcm9sbGFibGVcbiAgICAgICAgICAgIHNjcm9sbEJ1dHRvbnM9XCJhdXRvXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiUHJvZmlsZVwiIC8+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiQWNjb3VudFwiIC8+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiQmlsbGluZ1wiIGRpc2FibGVkIC8+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiRW1haWxcIiAvPlxuICAgICAgICAgICAgPFRhYiBsYWJlbD1cIlBhc3N3b3JkXCIgLz5cbiAgICAgICAgICAgIDxUYWIgbGFiZWw9XCJHdXJ1XCIgLz5cbiAgICAgICAgICAgIHsvKiBOb3QgRm91bmQgdGFiIGlzIGhpZGRlbiBhbmQgaXQgd2lsbCBiZSB1c2VmdWxsIHRvIGhhbmRsZSBcbiAgICAgICAgICAgICAgICB1bmtub3duIHJvdXRlcy4gKi99XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiTm90IEZvdW5kXCIgc3R5bGU9e3sgZGlzcGxheTogJ25vbmUnIH19IC8+XG4gICAgICAgICAgPC9UYWJzPlxuICAgICAgICA8L0FwcEJhcj5cbiAgICAgICAgPFN3aXRjaD5cbiAgICAgICAgICA8Um91dGVcbiAgICAgICAgICAgIGV4YWN0XG4gICAgICAgICAgICBwYXRoPVwiL3Byb2ZpbGVcIlxuICAgICAgICAgICAgY29tcG9uZW50PXsobmV4dFByb3BzKSA9PiAoXG4gICAgICAgICAgICAgIDxBc3luY1Byb2ZpbGVcbiAgICAgICAgICAgICAgICB7Li4ubmV4dFByb3BzfVxuICAgICAgICAgICAgICAgIHVzZXI9e3VzZXJ9XG4gICAgICAgICAgICAgICAgdXNlclVwZGF0ZT17dGhpcy5wcm9wcy5hY3Rpb25Vc2VyVXBkYXRlfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICAvPlxuICAgICAgICAgIDxSb3V0ZVxuICAgICAgICAgICAgZXhhY3RcbiAgICAgICAgICAgIHBhdGg9XCIvcHJvZmlsZS9hY2NvdW50XCJcbiAgICAgICAgICAgIGNvbXBvbmVudD17KG5leHRQcm9wcykgPT4gKFxuICAgICAgICAgICAgICA8QXN5bmNBY2NvdW50XG4gICAgICAgICAgICAgICAgey4uLm5leHRQcm9wc31cbiAgICAgICAgICAgICAgICB1c2VyPXt1c2VyfVxuICAgICAgICAgICAgICAgIHVzZXJVcGRhdGU9e3RoaXMucHJvcHMuYWN0aW9uVXNlclVwZGF0ZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Um91dGUgZXhhY3QgcGF0aD1cIi9wcm9maWxlL2JpbGxpbmdcIiBjb21wb25lbnQ9e0FzeW5jQmlsbGluZ30gLz5cbiAgICAgICAgICA8Um91dGVcbiAgICAgICAgICAgIGV4YWN0XG4gICAgICAgICAgICBwYXRoPVwiL3Byb2ZpbGUvZW1haWxcIlxuICAgICAgICAgICAgY29tcG9uZW50PXsobmV4dFByb3BzKSA9PiAoXG4gICAgICAgICAgICAgIDxBc3luY0VtYWlsXG4gICAgICAgICAgICAgICAgey4uLm5leHRQcm9wc31cbiAgICAgICAgICAgICAgICB1c2VyPXt1c2VyfVxuICAgICAgICAgICAgICAgIHVzZXJVcGRhdGU9e3RoaXMucHJvcHMuYWN0aW9uVXNlclVwZGF0ZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Um91dGVcbiAgICAgICAgICAgIGV4YWN0XG4gICAgICAgICAgICBwYXRoPVwiL3Byb2ZpbGUvcGFzc3dvcmRcIlxuICAgICAgICAgICAgY29tcG9uZW50PXsobmV4dFByb3BzKSA9PiAoXG4gICAgICAgICAgICAgIDxBc3luY1Bhc3N3b3JkIHsuLi5uZXh0UHJvcHN9IHVzZXI9e3VzZXJ9IC8+XG4gICAgICAgICAgICApfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPFJvdXRlXG4gICAgICAgICAgICBleGFjdFxuICAgICAgICAgICAgcGF0aD1cIi9wcm9maWxlL2d1cnVcIlxuICAgICAgICAgICAgY29tcG9uZW50PXsobmV4dFByb3BzKSA9PiAoXG4gICAgICAgICAgICAgIDxBc3luY0FwcGx5Rm9yR3VydSB7Li4ubmV4dFByb3BzfSB1c2VyPXt1c2VyfSAvPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICAvPlxuICAgICAgICAgIDxSb3V0ZSBjb21wb25lbnQ9e0FzeW5jRXJyb3JQYWdlfSAvPlxuICAgICAgICA8L1N3aXRjaD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuUHJvZmlsZVBhZ2UucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgbG9jYXRpb246IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgcGF0aG5hbWU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgfSkuaXNSZXF1aXJlZCxcbiAgaGlzdG9yeTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgYWN0aW9uRWxlbWVudEdldDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgYWN0aW9uVXNlclVwZGF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IGVsZW1lbnRzIH0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25FbGVtZW50R2V0LFxuICAgICAgYWN0aW9uVXNlclVwZGF0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKFByb2ZpbGVQYWdlKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUHJvZmlsZVBhZ2UvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9