webpackJsonp([70],{

/***/ "./src/client/api/users/guru/apply.js":
/*!********************************************!*\
  !*** ./src/client/api/users/guru/apply.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function apply
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.userId -
 * @param {string} payload.reason -
 * @return {Promise} -
 */
/** @format */

var apply = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        reason = _ref2.reason;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/gururequest';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ userId: userId, reason: reason })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function apply(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = apply;

/***/ }),

/***/ "./src/client/components/TextArea.js":
/*!*******************************************!*\
  !*** ./src/client/components/TextArea.js ***!
  \*******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    height: '100px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for take feedback and handle guru
    * request
    * @format
    */

var TextArea = function TextArea(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('textarea', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: disabled
    })
  );
};

TextArea.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(TextArea);

/***/ }),

/***/ "./src/client/containers/ProfilePage/ApplyForGuru.js":
/*!***********************************************************!*\
  !*** ./src/client/containers/ProfilePage/ApplyForGuru.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _TextArea = __webpack_require__(/*! ../../components/TextArea */ "./src/client/components/TextArea.js");

var _TextArea2 = _interopRequireDefault(_TextArea);

var _apply = __webpack_require__(/*! ../../api/users/guru/apply */ "./src/client/api/users/guru/apply.js");

var _apply2 = _interopRequireDefault(_apply);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {},
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  gridItem: {
    padding: '1%'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    },
    '&:valid': {
      outline: 'none',
      borderColor: 'green',
      boxShadow: '0 0 10px #9ecaed'
    },
    '&:invalid': {
      outline: 'none',
      borderColor: 'red',
      boxShadow: '0 0 10px #9ecaed'
    }
  },
  errorMessage: {
    color: 'red',
    padding: '5px'
  },
  successMessage: {
    color: 'green',
    padding: '5px'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
}; /** @format */

var ApplyForGuru = function (_Component) {
  (0, _inherits3.default)(ApplyForGuru, _Component);

  function ApplyForGuru(props) {
    (0, _classCallCheck3.default)(this, ApplyForGuru);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ApplyForGuru.__proto__ || (0, _getPrototypeOf2.default)(ApplyForGuru)).call(this, props));

    _this.state = {
      message: '',
      reason: '',
      success: false
    };
    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(ApplyForGuru, [{
    key: 'onChange',
    value: function onChange(e) {
      this.setState({ reason: e.target.value });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var reason, _props$user, userId, token, _ref2, statusCode;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                reason = this.state.reason;
                _props$user = this.props.user, userId = _props$user.userId, token = _props$user.token;
                _context.next = 5;
                return (0, _apply2.default)({ token: token, userId: userId, reason: reason });

              case 5:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;

                if (!(statusCode >= 400)) {
                  _context.next = 11;
                  break;
                }

                this.setState({
                  message: 'There is any server error while you are applying for guru.' + ' Please try again'
                });
                console.log(this.state);
                return _context.abrupt('return');

              case 11:
                this.setState({
                  message: 'You have successfully applied for guru.We willl review you request',
                  success: true
                });
                console.log(this.state);
                _context.next = 18;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 18:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 15]]);
      }));

      function onSubmit() {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          classes = _props.classes,
          user = _props.user;
      var _state = this.state,
          message = _state.message,
          success = _state.success,
          reason = _state.reason;
      var guru = user.guru;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 6,
            xl: 6,
            className: classes.gridItem
          },
          _react2.default.createElement(
            'div',
            {
              onMouseEnter: function onMouseEnter() {
                return _this2.setState({
                  hover: true
                });
              },
              onMouseLeave: function onMouseLeave() {
                return _this2.setState({ hover: false });
              }
            },
            !guru ? _react2.default.createElement(
              _Card2.default,
              { raised: this.state.hover, className: classes.card },
              _react2.default.createElement(
                _Card.CardContent,
                null,
                'Guru feature is already enable to your account'
              ),
              _react2.default.createElement(
                _Card.CardContent,
                null,
                'there is no need to apply again'
              )
            ) : _react2.default.createElement(
              _Card2.default,
              { raised: this.state.hover, className: classes.card },
              _react2.default.createElement(_Card.CardHeader, { title: 'What is guru feature ??' }),
              _react2.default.createElement(
                _Card.CardContent,
                null,
                'To become guru you should have full-fill these requirement:',
                _react2.default.createElement(
                  'ul',
                  null,
                  _react2.default.createElement(
                    'li',
                    null,
                    'you should have complete your resume'
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    'Atleast 10 posts were created by you'
                  ),
                  _react2.default.createElement(
                    'li',
                    null,
                    'You should have experties in any field'
                  )
                ),
                'If you are a guru, you can create your own courses',
                _react2.default.createElement(_TextArea2.default, {
                  onChange: this.onChange,
                  disable: false,
                  placeholder: 'Why do you want to become guru...',
                  value: reason
                })
              ),
              _react2.default.createElement(
                _Typography2.default,
                {
                  className: success ? classes.successMessage : classes.errorMessage
                },
                message || null
              ),
              _react2.default.createElement(
                _Card.CardActions,
                null,
                _react2.default.createElement('div', { className: classes.flexGrow }),
                _react2.default.createElement(
                  _Button2.default,
                  { raised: true, color: 'accent', onClick: this.onSubmit },
                  'Apply For Guru'
                )
              )
            )
          )
        )
      );
    }
  }]);
  return ApplyForGuru;
}(_react.Component);

ApplyForGuru.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    token: _propTypes2.default.string.isRequired
  }).isRequired
};

exports.default = (0, _withStyles2.default)(styles)(ApplyForGuru);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS91c2Vycy9ndXJ1L2FwcGx5LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29tcG9uZW50cy9UZXh0QXJlYS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUHJvZmlsZVBhZ2UvQXBwbHlGb3JHdXJ1LmpzIl0sIm5hbWVzIjpbInRva2VuIiwidXNlcklkIiwicmVhc29uIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJib2R5IiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInN0YXR1c0NvZGUiLCJlcnJvciIsImpzb24iLCJjb25zb2xlIiwiYXBwbHkiLCJzdHlsZXMiLCJyb290IiwiZGlzcGxheSIsImlucHV0IiwiZmxleEdyb3ciLCJib3JkZXIiLCJib3JkZXJSYWRpdXMiLCJwYWRkaW5nIiwiaGVpZ2h0Iiwib3V0bGluZSIsImJvcmRlckNvbG9yIiwiYm94U2hhZG93IiwiVGV4dEFyZWEiLCJjbGFzc2VzIiwib25DaGFuZ2UiLCJ2YWx1ZSIsInBsYWNlaG9sZGVyIiwiZGlzYWJsZWQiLCJ0eXBlIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJvbmVPZlR5cGUiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiY2FyZCIsImF2YXRhciIsImdyaWRJdGVtIiwiZXJyb3JNZXNzYWdlIiwiY29sb3IiLCJzdWNjZXNzTWVzc2FnZSIsImZsZXgiLCJBcHBseUZvckd1cnUiLCJwcm9wcyIsInN0YXRlIiwibWVzc2FnZSIsInN1Y2Nlc3MiLCJiaW5kIiwib25TdWJtaXQiLCJlIiwic2V0U3RhdGUiLCJ0YXJnZXQiLCJ1c2VyIiwibG9nIiwiZ3VydSIsImhvdmVyIiwic2hhcGUiLCJpZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS0E7Ozs7Ozs7Ozs7QUFMQTs7O3NGQWVBO0FBQUEsUUFBdUJBLEtBQXZCLFNBQXVCQSxLQUF2QjtBQUFBLFFBQThCQyxNQUE5QixTQUE4QkEsTUFBOUI7QUFBQSxRQUFzQ0MsTUFBdEMsU0FBc0NBLE1BQXRDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVDLGVBRlYsa0NBRXFDRixNQUZyQztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNRSxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVA7QUFIUixlQUZrQjtBQU8zQlEsb0JBQU0seUJBQWUsRUFBRVAsY0FBRixFQUFVQyxjQUFWLEVBQWY7QUFQcUIsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBY1lDLGtCQWRaLEdBY21DRCxHQWRuQyxDQWNZQyxNQWRaLEVBY29CQyxVQWRwQixHQWNtQ0YsR0FkbkMsQ0Fjb0JFLFVBZHBCOztBQUFBLGtCQWdCUUQsVUFBVSxHQWhCbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBaUJhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBakJiOztBQUFBO0FBQUE7QUFBQSxtQkF1QnVCRixJQUFJSyxJQUFKLEVBdkJ2Qjs7QUFBQTtBQXVCVUEsZ0JBdkJWO0FBQUEsd0VBeUJnQkEsSUF6QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEyQklDLG9CQUFRRixLQUFSOztBQTNCSiw2Q0E2Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBN0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlRyxLOzs7OztBQWJmOzs7O0FBQ0E7Ozs7a0JBZ0RlQSxLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0NmOzs7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUMsU0FBUztBQUNiQyxRQUFNO0FBQ0pDLGFBQVM7QUFETCxHQURPO0FBSWJDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTEMsa0JBQWMsS0FIVDtBQUlMQyxhQUFTLEtBSko7QUFLTEMsWUFBUSxPQUxIO0FBTUwsZUFBVztBQUNUQyxlQUFTLE1BREE7QUFFVEMsbUJBQWEsU0FGSjtBQUdUQyxpQkFBVztBQUhGO0FBTk47QUFKTSxDQUFmLEMsQ0FYQTs7Ozs7O0FBNkJBLElBQU1DLFdBQVcsU0FBWEEsUUFBVztBQUFBLE1BQ2ZDLE9BRGUsUUFDZkEsT0FEZTtBQUFBLE1BRWZDLFFBRmUsUUFFZkEsUUFGZTtBQUFBLE1BR2ZDLEtBSGUsUUFHZkEsS0FIZTtBQUFBLE1BSWZDLFdBSmUsUUFJZkEsV0FKZTtBQUFBLE1BS2ZDLFFBTGUsUUFLZkEsUUFMZTtBQUFBLE1BTWZDLElBTmUsUUFNZkEsSUFOZTtBQUFBLFNBUWY7QUFBQTtBQUFBLE1BQUssV0FBV0wsUUFBUVosSUFBeEI7QUFDRTtBQUNFLFlBQU1pQixRQUFRLE1BRGhCO0FBRUUsbUJBQWFGLGVBQWUsT0FGOUI7QUFHRSxhQUFPRCxLQUhUO0FBSUUsZ0JBQVVELFFBSlo7QUFLRSxpQkFBV0QsUUFBUVYsS0FMckI7QUFNRSxnQkFBVWM7QUFOWjtBQURGLEdBUmU7QUFBQSxDQUFqQjs7QUFvQkFMLFNBQVNPLFNBQVQsR0FBcUI7QUFDbkJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFA7QUFFbkJQLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUQsVUFGTjtBQUduQk4sU0FBTyxvQkFBVVEsU0FBVixDQUFvQixDQUFDLG9CQUFVQyxNQUFYLEVBQW1CLG9CQUFVQyxNQUE3QixDQUFwQixFQUEwREosVUFIOUM7QUFJbkJMLGVBQWEsb0JBQVVRLE1BSko7QUFLbkJOLFFBQU0sb0JBQVVNLE1BTEc7QUFNbkJQLFlBQVUsb0JBQVVTO0FBTkQsQ0FBckI7O2tCQVNlLDBCQUFXMUIsTUFBWCxFQUFtQlksUUFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFNWixTQUFTO0FBQ2JDLFFBQU0sRUFETztBQUViMEIsUUFBTTtBQUNKckIsa0JBQWM7QUFEVixHQUZPO0FBS2JzQixVQUFRO0FBQ050QixrQkFBYztBQURSLEdBTEs7QUFRYnVCLFlBQVU7QUFDUnRCLGFBQVM7QUFERCxHQVJHO0FBV2JKLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTEMsa0JBQWMsS0FIVDtBQUlMQyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RFLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEYsS0FMTjtBQVVMLGVBQVc7QUFDVEYsZUFBUyxNQURBO0FBRVRDLG1CQUFhLE9BRko7QUFHVEMsaUJBQVc7QUFIRixLQVZOO0FBZUwsaUJBQWE7QUFDWEYsZUFBUyxNQURFO0FBRVhDLG1CQUFhLEtBRkY7QUFHWEMsaUJBQVc7QUFIQTtBQWZSLEdBWE07QUFnQ2JtQixnQkFBYztBQUNaQyxXQUFPLEtBREs7QUFFWnhCLGFBQVM7QUFGRyxHQWhDRDtBQW9DYnlCLGtCQUFnQjtBQUNkRCxXQUFPLE9BRE87QUFFZHhCLGFBQVM7QUFGSyxHQXBDSDtBQXdDYkgsWUFBVTtBQUNSNkIsVUFBTTtBQURFO0FBeENHLENBQWYsQyxDQWRBOztJQTJETUMsWTs7O0FBQ0osd0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxrSkFDWEEsS0FEVzs7QUFHakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hDLGVBQVMsRUFERTtBQUVYcEQsY0FBUSxFQUZHO0FBR1hxRCxlQUFTO0FBSEUsS0FBYjtBQUtBLFVBQUt4QixRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY3lCLElBQWQsT0FBaEI7QUFDQSxVQUFLQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0QsSUFBZCxPQUFoQjtBQVRpQjtBQVVsQjs7Ozs2QkFFUUUsQyxFQUFHO0FBQ1YsV0FBS0MsUUFBTCxDQUFjLEVBQUV6RCxRQUFRd0QsRUFBRUUsTUFBRixDQUFTNUIsS0FBbkIsRUFBZDtBQUNEOzs7Ozs7Ozs7Ozs7QUFHVzlCLHNCLEdBQVcsS0FBS21ELEssQ0FBaEJuRCxNOzhCQUNrQixLQUFLa0QsS0FBTCxDQUFXUyxJLEVBQTdCNUQsTSxlQUFBQSxNLEVBQVFELEssZUFBQUEsSzs7dUJBRWEscUJBQWdCLEVBQUVBLFlBQUYsRUFBU0MsY0FBVCxFQUFpQkMsY0FBakIsRUFBaEIsQzs7OztBQUFyQlUsMEIsU0FBQUEsVTs7c0JBQ0pBLGNBQWMsRzs7Ozs7QUFDaEIscUJBQUsrQyxRQUFMLENBQWM7QUFDWkwsMkJBQ0UsK0RBQ0E7QUFIVSxpQkFBZDtBQUtBdkMsd0JBQVErQyxHQUFSLENBQVksS0FBS1QsS0FBakI7Ozs7QUFHRixxQkFBS00sUUFBTCxDQUFjO0FBQ1pMLDJCQUNFLG9FQUZVO0FBR1pDLDJCQUFTO0FBSEcsaUJBQWQ7QUFLQXhDLHdCQUFRK0MsR0FBUixDQUFZLEtBQUtULEtBQWpCOzs7Ozs7OztBQUVBdEMsd0JBQVFGLEtBQVI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFJSztBQUFBOztBQUFBLG1CQUNtQixLQUFLdUMsS0FEeEI7QUFBQSxVQUNDdEIsT0FERCxVQUNDQSxPQUREO0FBQUEsVUFDVStCLElBRFYsVUFDVUEsSUFEVjtBQUFBLG1CQUU4QixLQUFLUixLQUZuQztBQUFBLFVBRUNDLE9BRkQsVUFFQ0EsT0FGRDtBQUFBLFVBRVVDLE9BRlYsVUFFVUEsT0FGVjtBQUFBLFVBRW1CckQsTUFGbkIsVUFFbUJBLE1BRm5CO0FBQUEsVUFJQzZELElBSkQsR0FJVUYsSUFKVixDQUlDRSxJQUpEOzs7QUFNUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDLEVBQTZDLFdBQVdqQyxRQUFRWixJQUFoRTtBQUNFO0FBQUE7QUFBQTtBQUNFLHNCQURGO0FBRUUsZ0JBQUksRUFGTjtBQUdFLGdCQUFJLEVBSE47QUFJRSxnQkFBSSxDQUpOO0FBS0UsZ0JBQUksQ0FMTjtBQU1FLGdCQUFJLENBTk47QUFPRSx1QkFBV1ksUUFBUWdCO0FBUHJCO0FBU0U7QUFBQTtBQUFBO0FBQ0UsNEJBQWM7QUFBQSx1QkFDWixPQUFLYSxRQUFMLENBQWM7QUFDWksseUJBQU87QUFESyxpQkFBZCxDQURZO0FBQUEsZUFEaEI7QUFNRSw0QkFBYztBQUFBLHVCQUFNLE9BQUtMLFFBQUwsQ0FBYyxFQUFFSyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUE7QUFOaEI7QUFRRyxhQUFDRCxJQUFELEdBQ0M7QUFBQTtBQUFBLGdCQUFNLFFBQVEsS0FBS1YsS0FBTCxDQUFXVyxLQUF6QixFQUFnQyxXQUFXbEMsUUFBUWMsSUFBbkQ7QUFDRTtBQUFBO0FBQUE7QUFDRztBQURILGVBREY7QUFJRTtBQUFBO0FBQUE7QUFBYztBQUFkO0FBSkYsYUFERCxHQVFDO0FBQUE7QUFBQSxnQkFBTSxRQUFRLEtBQUtTLEtBQUwsQ0FBV1csS0FBekIsRUFBZ0MsV0FBV2xDLFFBQVFjLElBQW5EO0FBQ0UsZ0VBQVksT0FBTyx5QkFBbkIsR0FERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEYsaUJBRkY7QUFBQTtBQVFFO0FBQ0UsNEJBQVUsS0FBS2IsUUFEakI7QUFFRSwyQkFBUyxLQUZYO0FBR0UsK0JBQVksbUNBSGQ7QUFJRSx5QkFBTzdCO0FBSlQ7QUFSRixlQUZGO0FBaUJFO0FBQUE7QUFBQTtBQUNFLDZCQUNFcUQsVUFBVXpCLFFBQVFtQixjQUFsQixHQUFtQ25CLFFBQVFpQjtBQUYvQztBQUtHTywyQkFBVztBQUxkLGVBakJGO0FBd0JFO0FBQUE7QUFBQTtBQUNFLHVEQUFLLFdBQVd4QixRQUFRVCxRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLG9CQUFRLFlBQVIsRUFBZSxPQUFNLFFBQXJCLEVBQThCLFNBQVMsS0FBS29DLFFBQTVDO0FBQUE7QUFBQTtBQUZGO0FBeEJGO0FBaEJKO0FBVEY7QUFERixPQURGO0FBK0REOzs7OztBQUdITixhQUFhZixTQUFiLEdBQXlCO0FBQ3ZCTixXQUFTLG9CQUFVTyxNQUFWLENBQWlCQyxVQURIO0FBRXZCdUIsUUFBTSxvQkFBVUksS0FBVixDQUFnQjtBQUNwQkMsUUFBSSxvQkFBVXhCLE1BQVYsQ0FBaUJKLFVBREQ7QUFFcEJ0QyxXQUFPLG9CQUFVeUMsTUFBVixDQUFpQkg7QUFGSixHQUFoQixFQUdIQTtBQUxvQixDQUF6Qjs7a0JBUWUsMEJBQVdyQixNQUFYLEVBQW1Ca0MsWUFBbkIsQyIsImZpbGUiOiI3MC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcblxuLyoqXG4gKlxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gYXBwbHlcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5yZWFzb24gLVxuICogQHJldHVybiB7UHJvbWlzZX0gLVxuICovXG5hc3luYyBmdW5jdGlvbiBhcHBseSh7IHRva2VuLCB1c2VySWQsIHJlYXNvbiB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9ndXJ1cmVxdWVzdGA7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ1BVVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHVzZXJJZCwgcmVhc29uIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGFwcGx5O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvdXNlcnMvZ3VydS9hcHBseS5qcyIsIi8qKlxuICogVGhpcyBpbnB1dCBjb21wb25lbnQgaXMgbWFpbmx5IGRldmVsb3BlZCBmb3IgdGFrZSBmZWVkYmFjayBhbmQgaGFuZGxlIGd1cnVcbiAqIHJlcXVlc3RcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBkaXNwbGF5OiAnZmxleCcsXG4gIH0sXG4gIGlucHV0OiB7XG4gICAgZmxleEdyb3c6ICcxJyxcbiAgICBib3JkZXI6ICcycHggc29saWQgI2RhZGFkYScsXG4gICAgYm9yZGVyUmFkaXVzOiAnN3B4JyxcbiAgICBwYWRkaW5nOiAnOHB4JyxcbiAgICBoZWlnaHQ6ICcxMDBweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbn07XG5cbmNvbnN0IFRleHRBcmVhID0gKHtcbiAgY2xhc3NlcyxcbiAgb25DaGFuZ2UsXG4gIHZhbHVlLFxuICBwbGFjZWhvbGRlcixcbiAgZGlzYWJsZWQsXG4gIHR5cGUsXG59KSA9PiAoXG4gIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgIDx0ZXh0YXJlYVxuICAgICAgdHlwZT17dHlwZSB8fCAndGV4dCd9XG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXIgfHwgJ2lucHV0J31cbiAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5pbnB1dH1cbiAgICAgIGRpc2FibGVkPXtkaXNhYmxlZH1cbiAgICAvPlxuICA8L2Rpdj5cbik7XG5cblRleHRBcmVhLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgdmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFRleHRBcmVhKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9UZXh0QXJlYS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnbWF0ZXJpYWwtdWkvVHlwb2dyYXBoeSc7XG5cbmltcG9ydCBUZXh0QXJlYSBmcm9tICcuLi8uLi9jb21wb25lbnRzL1RleHRBcmVhJztcbmltcG9ydCBhcGlBcHBseUZvckd1cnUgZnJvbSAnLi4vLi4vYXBpL3VzZXJzL2d1cnUvYXBwbHknO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHt9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgYXZhdGFyOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgfSxcbiAgZ3JpZEl0ZW06IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBpbnB1dDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gICAgYm9yZGVyOiAnMnB4IHNvbGlkICNkYWRhZGEnLFxuICAgIGJvcmRlclJhZGl1czogJzdweCcsXG4gICAgcGFkZGluZzogJzhweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgICAnJjp2YWxpZCc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJyxcbiAgICAgIGJvcmRlckNvbG9yOiAnZ3JlZW4nLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgICAnJjppbnZhbGlkJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICdyZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbiAgZXJyb3JNZXNzYWdlOiB7XG4gICAgY29sb3I6ICdyZWQnLFxuICAgIHBhZGRpbmc6ICc1cHgnLFxuICB9LFxuICBzdWNjZXNzTWVzc2FnZToge1xuICAgIGNvbG9yOiAnZ3JlZW4nLFxuICAgIHBhZGRpbmc6ICc1cHgnLFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59O1xuXG5jbGFzcyBBcHBseUZvckd1cnUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcblxuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBtZXNzYWdlOiAnJyxcbiAgICAgIHJlYXNvbjogJycsXG4gICAgICBzdWNjZXNzOiBmYWxzZSxcbiAgICB9O1xuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblN1Ym1pdCA9IHRoaXMub25TdWJtaXQuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKGUpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgcmVhc29uOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgfVxuICBhc3luYyBvblN1Ym1pdCgpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyByZWFzb24gfSA9IHRoaXMuc3RhdGU7XG4gICAgICBjb25zdCB7IHVzZXJJZCwgdG9rZW4gfSA9IHRoaXMucHJvcHMudXNlcjtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlIH0gPSBhd2FpdCBhcGlBcHBseUZvckd1cnUoeyB0b2tlbiwgdXNlcklkLCByZWFzb24gfSk7XG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSA0MDApIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgbWVzc2FnZTpcbiAgICAgICAgICAgICdUaGVyZSBpcyBhbnkgc2VydmVyIGVycm9yIHdoaWxlIHlvdSBhcmUgYXBwbHlpbmcgZm9yIGd1cnUuJyArXG4gICAgICAgICAgICAnIFBsZWFzZSB0cnkgYWdhaW4nLFxuICAgICAgICB9KTtcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5zdGF0ZSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBtZXNzYWdlOlxuICAgICAgICAgICdZb3UgaGF2ZSBzdWNjZXNzZnVsbHkgYXBwbGllZCBmb3IgZ3VydS5XZSB3aWxsbCByZXZpZXcgeW91IHJlcXVlc3QnLFxuICAgICAgICBzdWNjZXNzOiB0cnVlLFxuICAgICAgfSk7XG4gICAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlKTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgdXNlciB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IG1lc3NhZ2UsIHN1Y2Nlc3MsIHJlYXNvbiB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGNvbnN0IHsgZ3VydSB9ID0gdXNlcjtcblxuICAgIHJldHVybiAoXG4gICAgICA8R3JpZCBjb250YWluZXIganVzdGlmeT1cImNlbnRlclwiIHNwYWNpbmc9ezB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPEdyaWRcbiAgICAgICAgICBpdGVtXG4gICAgICAgICAgeHM9ezEyfVxuICAgICAgICAgIHNtPXsxMH1cbiAgICAgICAgICBtZD17OH1cbiAgICAgICAgICBsZz17Nn1cbiAgICAgICAgICB4bD17Nn1cbiAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI9eygpID0+XG4gICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgIGhvdmVyOiB0cnVlLFxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXsoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIHshZ3VydSA/IChcbiAgICAgICAgICAgICAgPENhcmQgcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICAgICAgeydHdXJ1IGZlYXR1cmUgaXMgYWxyZWFkeSBlbmFibGUgdG8geW91ciBhY2NvdW50J31cbiAgICAgICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgICAgIDxDYXJkQ29udGVudD57J3RoZXJlIGlzIG5vIG5lZWQgdG8gYXBwbHkgYWdhaW4nfTwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgIDxDYXJkIHJhaXNlZD17dGhpcy5zdGF0ZS5ob3Zlcn0gY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgICAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPXsnV2hhdCBpcyBndXJ1IGZlYXR1cmUgPz8nfSAvPlxuICAgICAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgICAgIFRvIGJlY29tZSBndXJ1IHlvdSBzaG91bGQgaGF2ZSBmdWxsLWZpbGwgdGhlc2UgcmVxdWlyZW1lbnQ6XG4gICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgIDxsaT55b3Ugc2hvdWxkIGhhdmUgY29tcGxldGUgeW91ciByZXN1bWU8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+QXRsZWFzdCAxMCBwb3N0cyB3ZXJlIGNyZWF0ZWQgYnkgeW91PC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPllvdSBzaG91bGQgaGF2ZSBleHBlcnRpZXMgaW4gYW55IGZpZWxkPC9saT5cbiAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgICBJZiB5b3UgYXJlIGEgZ3VydSwgeW91IGNhbiBjcmVhdGUgeW91ciBvd24gY291cnNlc1xuICAgICAgICAgICAgICAgICAgPFRleHRBcmVhXG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlPXtmYWxzZX1cbiAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJXaHkgZG8geW91IHdhbnQgdG8gYmVjb21lIGd1cnUuLi5cIlxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17cmVhc29ufVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgICAgIDxUeXBvZ3JhcGh5XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e1xuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzID8gY2xhc3Nlcy5zdWNjZXNzTWVzc2FnZSA6IGNsYXNzZXMuZXJyb3JNZXNzYWdlXG4gICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAge21lc3NhZ2UgfHwgbnVsbH1cbiAgICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICAgICAgPENhcmRBY3Rpb25zPlxuICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICAgICAgICA8QnV0dG9uIHJhaXNlZCBjb2xvcj1cImFjY2VudFwiIG9uQ2xpY2s9e3RoaXMub25TdWJtaXR9PlxuICAgICAgICAgICAgICAgICAgICBBcHBseSBGb3IgR3VydVxuICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICAgICAgPC9DYXJkPlxuICAgICAgICAgICAgKX1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9HcmlkPlxuICAgICAgPC9HcmlkPlxuICAgICk7XG4gIH1cbn1cblxuQXBwbHlGb3JHdXJ1LnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB1c2VyOiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIGlkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gICAgdG9rZW46IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgfSkuaXNSZXF1aXJlZCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShBcHBseUZvckd1cnUpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Byb2ZpbGVQYWdlL0FwcGx5Rm9yR3VydS5qcyJdLCJzb3VyY2VSb290IjoiIn0=