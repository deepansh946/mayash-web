webpackJsonp([13],{

/***/ "./node_modules/material-ui-icons/NavigateNext.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/NavigateNext.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z' });

var NavigateNext = function NavigateNext(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

NavigateNext = (0, _pure2.default)(NavigateNext);
NavigateNext.muiName = 'SvgIcon';

exports.default = NavigateNext;

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/components/NavigationButtonNext.js":
/*!*******************************************************!*\
  !*** ./src/client/components/NavigationButtonNext.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _NavigateNext = __webpack_require__(/*! material-ui-icons/NavigateNext */ "./node_modules/material-ui-icons/NavigateNext.js");

var _NavigateNext2 = _interopRequireDefault(_NavigateNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component is create to show naviagtion button to move next page
 *
 * @format
 */

var styles = function styles(theme) {
  var _root;

  return {
    root: (_root = {
      position: 'fixed',
      right: '2%',
      bottom: '4%'
    }, (0, _defineProperty3.default)(_root, theme.breakpoints.down('md'), {
      right: '2%',
      bottom: '2%'
    }), (0, _defineProperty3.default)(_root, theme.breakpoints.down('sm'), {
      right: '2%',
      bottom: '1%'
    }), _root)
  };
};

/**
 *
 * @param {object} classes
 * @param {path} to
 */
var NavigateButtonNext = function NavigateButtonNext(_ref) {
  var classes = _ref.classes,
      _ref$to = _ref.to,
      to = _ref$to === undefined ? '/' : _ref$to;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement(
      _Button2.default,
      {
        fab: true,
        color: 'accent',
        component: _Link2.default,
        className: classes.button,
        raised: true,
        to: to
      },
      _react2.default.createElement(_NavigateNext2.default, null)
    )
  );
};

NavigateButtonNext.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  to: _propTypes2.default.string.isRequired
};

NavigateButtonNext.defaultProps = {
  to: '/'
};

exports.default = (0, _styles.withStyles)(styles)(NavigateButtonNext);

/***/ }),

/***/ "./src/client/pages/Terms.js":
/*!***********************************!*\
  !*** ./src/client/pages/Terms.js ***!
  \***********************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _NavigationButtonNext = __webpack_require__(/*! ../components/NavigationButtonNext */ "./src/client/components/NavigationButtonNext.js");

var _NavigationButtonNext2 = _interopRequireDefault(_NavigationButtonNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  var _cardContent;

  return {
    root: {},
    cardHeader: {
      textAlign: 'center',
      padding: '3em'
    },
    media: {
      height: 300,
      width: 300
    },
    medium: {
      padding: '40px'
    },
    cardContent: (_cardContent = {
      paddingLeft: '8em',
      textAlign: 'justify',
      paddingRight: '8em'
    }, (0, _defineProperty3.default)(_cardContent, theme.breakpoints.down('md'), {
      paddingLeft: '2em',
      paddingRight: '2em'
    }), (0, _defineProperty3.default)(_cardContent, theme.breakpoints.down('sm'), {
      padding: '10px'
    }), _cardContent)
  };
}; /**
    * This component represents Donation page for mayash
    *
    * @format
    */

var Temrs = function (_Component) {
  (0, _inherits3.default)(Temrs, _Component);

  function Temrs(props) {
    (0, _classCallCheck3.default)(this, Temrs);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Temrs.__proto__ || (0, _getPrototypeOf2.default)(Temrs)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Temrs, [{
    key: 'render',
    value: function render() {
      var classes = this.props.classes;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _Card2.default,
          null,
          _react2.default.createElement(_Card.CardHeader, {
            title: 'Terms and conditions of use',
            subheader: 'Hbarve technologies (OPC) private limited Terms of Service',
            className: classes.cardHeader
          }),
          _react2.default.createElement(
            _Card.CardContent,
            { className: classes.cardContent },
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '1. Introduction'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'These terms and conditions shall govern your use of our website.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'By using our website, you accept these terms and conditions in full; accordingly, if you disagree with these terms and conditions or any part of these terms and conditions, you must not use our website.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'If you register with our Hbarve Information Technology (OPC) Private Limited, submit any material to our website or use any of our website services, we will ask you to expressly agree to the Privacy Policy.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Our website uses cookies; by using our website or agreeing to these terms and conditions, you consent to our use of cookies in accordance with the terms of our privacy policy.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '2. Credit'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'This document is an SEQ Legal document from Website Contracts https://mayash.io. You must retain the above credit. Use of this document without the credit is an infringement of copyright. However, you can purchase from us an equivalent document that does not include the credit.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '3. Copyright notice'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Copyright (c) [2017(s) of first publication] [Mayash codes].'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Subject to the express provisions of these terms and conditions:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'all the copyright and other intellectual property rights in our website and the material on our website are reserved.'
                    )
                  )
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '4. Licence to use website'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'You may:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'view pages from our website in a web browser;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'download pages from our website for caching in a web browser;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'print pages from our website;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[stream audio and video files from our website]; and'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[use [our website services] by means of a web browser], subject to the other provisions of these terms and conditions.'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Except as expressly permitted by Section 4.1 or the other provisions of these terms and conditions, you must not download any material from our website or save any such material to your computer.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You may only use our website for [your own personal and business purposes], and you must not use our website for any other purposes.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Except as expressly permitted by these terms and conditions, you must not edit or otherwise modify any material on our website.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Unless you own or control the relevant rights in the material, you must not:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'sell, rent or sub-license material from our website;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'exploit material from our website for a commercial purpose; or'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Notwithstanding Section 4.5, you may redistribute [our newsletter] in [print and electronic form] to [any person].'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '5. Acceptable use'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'You must not:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent]'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[access or otherwise interact with our website using any robot, spider or other automated means[, except for the purpose of [search engine indexing]]];'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[violate the directives set out in the robots.txt file for our website]; or'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[use data collected from our website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing)]. [additional list items]'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must not use data collected from our website to contact individuals, companies or other persons or entities.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must ensure that all the information you supply to us through our website, or in relation to our website, is [true, accurate, current, complete and non-misleading].'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '6. Registration and accounts'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'You may register for an account with our website by [completing and submitting the account registration form on our website'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must not allow any other person to use your account to access the website.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must notify us in writing immediately if you become aware of any unauthorised use of your account'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must not use any other person\'s account to access the website[, unless you have that person\'s express permission to do so].'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '7. User login details'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'If you register for an account with our website, [we will provide you with] OR [you will be asked to choose] [a user ID and password].'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Your user ID must not be liable to mislead and must comply with the content rules set out in Section 10; you must not use your account or user ID for or in connection with the impersonation of any person.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must keep your password confidential.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You must notify us in writing immediately if you become aware of any disclosure of your password.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You are responsible for any activity on our website arising out of any failure to keep your password confidential, and may be held liable for any losses arising out of such a failure.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '8. Cancellation and suspension of account'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'We may:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      '[suspend your account];'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[cancel your account]; and/or'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[edit your account details],at any time in our sole discretion without notice or explanation.'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You may cancel your account on our website [using your account control panel on the website].'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '9. Your content: licence'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'In these terms and conditions, "your content" means [all works and materials (including without limitation text, graphics, images, audio material, video material, audio-visual material, scripts, software and files) that you submit to us or our website for storage or publication on, processing by, or transmission via, our website].'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You grant to us a [worldwide, irrevocable, non-exclusive, royalty-free licence] to [use, reproduce, store, adapt, publish, translate and distribute your content in any existing or future media] OR [reproduce, store and publish your content on and in relation to this website and any successor website] OR [reproduce, store and, with your specific consent, publish your content on and in relation to this website]. We can also use it to make big data.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You grant to us the right to sub-license the rights licensed under Section 9.2.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You grant to us the right to bring an action for infringement of the rights licensed under Section 9.2.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You hereby waive all your moral rights in your content to the maximum extent permitted by applicable law; and you warrant and represent that all other moral rights in your content have been waived to the maximum extent permitted by applicable law.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You may edit your content to the extent permitted using the editing functionality made available on our website.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Without prejudice to our other rights under these terms and conditions, if you breach any provision of these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may delete, unpublish or edit any or all of your content.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '10. Limited warranties'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'We do not warrant or represent:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'the completeness or accuracy of the information published on our website;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'that the material on the website is up to date; or'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'that the website or any service on the website will remain available.'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in these terms and conditions, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.these terms and conditions, you must not use our website.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'To the maximum extent permitted by applicable law and subject to Section 12.1, we exclude all representations and warranties relating to the subject matter of these terms and conditions, our website and the use of our website.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '11. Limitations and exclusions of liability'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Nothing in these terms and conditions will:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'limit or exclude any liability for death or personal injury resulting from negligence;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'limit or exclude any liability for fraud or fraudulent misrepresentation;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'limit any liabilities in any way that is not permitted under applicable law; or'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'exclude any liabilities that may not be excluded under applicable law.'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'The limitations and exclusions of liability set out in this Section 12 and elsewhere in these terms and conditions:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'are subject to Section 12.1; and'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'govern all liabilities arising under these terms and conditions or relating to the subject matter of these terms and conditions, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty, except to the extent expressly provided otherwise in these terms and conditions.'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We will not be liable to you in respect of any loss or corruption of any data, database or software.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We will not be liable to you in respect of any special, indirect or consequential loss or damage.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You accept that we have an interest in limiting the personal liability of our officers and employees and, having regard to that interest, you acknowledge that we are a limited liability entity; you agree that you will not bring any claim personally against our officers or employees in respect of any losses you suffer in connection with the website or these terms and conditions (this will not, of course, limit or exclude the liability of the limited liability entity itself for the acts and omissions of our officers and employees).'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '12. Breaches of these terms and conditions'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'send you one or more formal warnings;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'temporarily suspend your access to our website;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'permanently prohibit you from accessing our website;'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[block computers using your IP address from accessing our website];'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[contact any or all of your internet service providers and request that they block your access to our website];'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'commence legal action against you, whether for breach of contract or otherwise; and/or'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      '[suspend or delete your account on our website].',
                      _react2.default.createElement('br', null),
                      '[additional list items]'
                    )
                  )
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Where we suspend or prohibit or block your access to our website or a part of our website, you must not take any action to circumvent such suspension or prohibition or blocking[ (including without limitation [creating and/or using a different account])].'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '13. Variation'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'We may revise these terms and conditions from time to time.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  '[Hbarve technologies (OPC) private limited may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.]'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '14. Assignment'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'You hereby agree that we may assign, transfer, sub-contract or otherwise deal with our rights and/or obligations under these terms and conditions.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You may not without our prior written consent assign, transfer, sub-contract or otherwise deal with any of your rights and/or obligations under these terms and conditions.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '15. Severability'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'If any unlawful and/or unenforceable provision of these terms and conditions would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '16. Third party rights'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'A contract under these terms and conditions is for our benefit and your benefit, and is not intended to benefit or be enforceable by any third party.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'The exercise of the parties\' rights under a contract under these terms and conditions is not subject to the consent of any third party.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '17. Entire agreement'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Subject to Section 12.1, these terms and conditions[, together with [our privacy and cookies policy],] shall constitute the entire agreement between you and us in relation to your use of our website and shall supersede all previous agreements between you and us in relation to your use of our website.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '18. Law and jurisdiction'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'These terms and conditions shall be governed by and construed in accordance with Indian law.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Any disputes relating to these terms and conditions shall be subject to the jurisdiction of the courts of India.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              '19. Our details'
            ),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'display' },
              _react2.default.createElement(
                'ol',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'This website is owned and operated by Honey Barve.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We are registered in [ India ] under registration number [number], and our registered office is at [Ranchi].'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Our principal place of business is at [Gwalior].'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'You can contact us:',
                  _react2.default.createElement(
                    'ul',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'https://docs.google.com/forms/d/e/1FAIpQLScRqqZ_nRF9AhCBM73qFYvVx0L2SwFTtv6t9RxqKJ4zxr-l0w/viewform'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Contact Number:- 8770693644'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'E mail:- contact-us@mayash.io'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Website:- https://Mayash.io.'
                    )
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(_NavigationButtonNext2.default, { to: '/developers-training' })
      );
    }
  }]);
  return Temrs;
}(_react.Component);

Temrs.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Temrs);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTmF2aWdhdGVOZXh0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29tcG9uZW50cy9OYXZpZ2F0aW9uQnV0dG9uTmV4dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L3BhZ2VzL1Rlcm1zLmpzIl0sIm5hbWVzIjpbInN0eWxlcyIsInRoZW1lIiwicm9vdCIsInBvc2l0aW9uIiwicmlnaHQiLCJib3R0b20iLCJicmVha3BvaW50cyIsImRvd24iLCJOYXZpZ2F0ZUJ1dHRvbk5leHQiLCJjbGFzc2VzIiwidG8iLCJidXR0b24iLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiZGVmYXVsdFByb3BzIiwiY2FyZEhlYWRlciIsInRleHRBbGlnbiIsInBhZGRpbmciLCJtZWRpYSIsImhlaWdodCIsIndpZHRoIiwibWVkaXVtIiwiY2FyZENvbnRlbnQiLCJwYWRkaW5nTGVmdCIsInBhZGRpbmdSaWdodCIsIlRlbXJzIiwicHJvcHMiLCJzdGF0ZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNEQUFzRDs7QUFFeEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGOztBQUU5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHFCQUFxQixXOzs7Ozs7Ozs7Ozs7O0FDbEwxRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Riw4Q0FBOEMsaUJBQWlCLHFCQUFxQixvQ0FBb0MsNkRBQTZELG9CQUFvQixFQUFFLGVBQWU7O0FBRTFOLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdGQUFnRjs7QUFFaEY7O0FBRUEsZ0ZBQWdGLGVBQWU7O0FBRS9GLHlEQUF5RCxVQUFVLHVEQUF1RDtBQUMxSDs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsdUI7Ozs7Ozs7Ozs7Ozs7QUM3R0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7OztBQ3JCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDZkE7O0FBRUE7O0FBRUEsb0dBQW9HLG1CQUFtQixFQUFFLG1CQUFtQiw4SEFBOEg7O0FBRTFRO0FBQ0E7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsOEQ7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDWkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLHlDOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ3pEQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFVBQVUscUJBQXFCO0FBQzVEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHlDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDUkE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBYkE7Ozs7OztBQWVBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxLQUFEO0FBQUE7O0FBQUEsU0FBWTtBQUN6QkM7QUFDRUMsZ0JBQVUsT0FEWjtBQUVFQyxhQUFPLElBRlQ7QUFHRUMsY0FBUTtBQUhWLDRDQUlHSixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQUpILEVBSWtDO0FBQzlCSCxhQUFPLElBRHVCO0FBRTlCQyxjQUFRO0FBRnNCLEtBSmxDLHdDQVFHSixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQVJILEVBUWtDO0FBQzlCSCxhQUFPLElBRHVCO0FBRTlCQyxjQUFRO0FBRnNCLEtBUmxDO0FBRHlCLEdBQVo7QUFBQSxDQUFmOztBQWdCQTs7Ozs7QUFLQSxJQUFNRyxxQkFBcUIsU0FBckJBLGtCQUFxQjtBQUFBLE1BQUdDLE9BQUgsUUFBR0EsT0FBSDtBQUFBLHFCQUFZQyxFQUFaO0FBQUEsTUFBWUEsRUFBWiwyQkFBaUIsR0FBakI7QUFBQSxTQUN6QjtBQUFBO0FBQUEsTUFBSyxXQUFXRCxRQUFRUCxJQUF4QjtBQUNFO0FBQUE7QUFBQTtBQUNFLGlCQURGO0FBRUUsZUFBTSxRQUZSO0FBR0UsaUNBSEY7QUFJRSxtQkFBV08sUUFBUUUsTUFKckI7QUFLRSxvQkFMRjtBQU1FLFlBQUlEO0FBTk47QUFRRTtBQVJGO0FBREYsR0FEeUI7QUFBQSxDQUEzQjs7QUFlQUYsbUJBQW1CSSxTQUFuQixHQUErQjtBQUM3QkgsV0FBUyxvQkFBVUksTUFBVixDQUFpQkMsVUFERztBQUU3QkosTUFBSSxvQkFBVUssTUFBVixDQUFpQkQ7QUFGUSxDQUEvQjs7QUFLQU4sbUJBQW1CUSxZQUFuQixHQUFrQztBQUNoQ04sTUFBSTtBQUQ0QixDQUFsQzs7a0JBSWUsd0JBQVdWLE1BQVgsRUFBbUJRLGtCQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdERmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTVIsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNLEVBRG1CO0FBRXpCZSxnQkFBWTtBQUNWQyxpQkFBVyxRQUREO0FBRVZDLGVBQVM7QUFGQyxLQUZhO0FBTXpCQyxXQUFPO0FBQ0xDLGNBQVEsR0FESDtBQUVMQyxhQUFPO0FBRkYsS0FOa0I7QUFVekJDLFlBQVE7QUFDTkosZUFBUztBQURILEtBVmlCO0FBYXpCSztBQUNFQyxtQkFBYSxLQURmO0FBRUVQLGlCQUFXLFNBRmI7QUFHRVEsb0JBQWM7QUFIaEIsbURBSUd6QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQUpILEVBSWtDO0FBQzlCa0IsbUJBQWEsS0FEaUI7QUFFOUJDLG9CQUFjO0FBRmdCLEtBSmxDLCtDQVFHekIsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FSSCxFQVFrQztBQUM5QlksZUFBUztBQURxQixLQVJsQztBQWJ5QixHQUFaO0FBQUEsQ0FBZixDLENBbkJBOzs7Ozs7SUE4Q01RLEs7OztBQUNKLGlCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsb0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUNRO0FBQUEsVUFDQ3BCLE9BREQsR0FDYSxLQUFLbUIsS0FEbEIsQ0FDQ25CLE9BREQ7O0FBRVAsYUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUNFLG1CQUFNLDZCQURSO0FBRUUsdUJBQVcsNERBRmI7QUFHRSx1QkFBV0EsUUFBUVE7QUFIckIsWUFERjtBQU1FO0FBQUE7QUFBQSxjQUFhLFdBQVdSLFFBQVFlLFdBQWhDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUFERjtBQUlFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFMRjtBQVdFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBWEY7QUFpQkU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWpCRjtBQURGLGFBSkY7QUE4QkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUE5QkY7QUFpQ0U7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxTQUFwQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQURGLGFBakNGO0FBNkNFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUFBLGFBN0NGO0FBZ0RFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0U7QUFBQTtBQUFBO0FBQUE7QUFFRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGO0FBTUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU5GO0FBRkY7QUFMRjtBQURGLGFBaERGO0FBdUVFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUFBLGFBdkVGO0FBMEVFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUVFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREY7QUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZGO0FBTUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFORjtBQU9FO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBUEY7QUFVRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVkY7QUFGRixpQkFERjtBQW9CRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXBCRjtBQTBCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQTFCRjtBQStCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQS9CRjtBQW9DRTtBQUFBO0FBQUE7QUFBQTtBQUVFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkY7QUFGRixpQkFwQ0Y7QUFnREU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFoREY7QUFxREU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXJERjtBQURGLGFBMUVGO0FBeUlFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUFBLGFBeklGO0FBNElFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUVFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREY7QUFPRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQVBGO0FBYUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFiRjtBQW9CRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQXBCRjtBQTJCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQTNCRjtBQWdDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQWhDRjtBQW9DRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBcENGO0FBRkYsaUJBREY7QUErQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkEvQ0Y7QUFvREU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQXBERjtBQURGLGFBNUlGO0FBeU1FO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUFBLGFBek1GO0FBNE1FO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBTUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFORjtBQVVFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBVkY7QUFjRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBZEY7QUFERixhQTVNRjtBQWtPRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFVBQXBDO0FBQUE7QUFBQSxhQWxPRjtBQXFPRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFNBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERjtBQU1FO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBTkY7QUFZRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVpGO0FBYUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFiRjtBQWlCRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBakJGO0FBREYsYUFyT0Y7QUFnUUU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUFoUUY7QUFtUUU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxTQUFwQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEY7QUFGRixpQkFERjtBQVlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFaRjtBQURGLGFBblFGO0FBdVJFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUFBLGFBdlJGO0FBMFJFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBU0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFURjtBQW1CRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQW5CRjtBQXVCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXZCRjtBQTJCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQTNCRjtBQWtDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWxDRjtBQXNDRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBdENGO0FBREYsYUExUkY7QUEyVUU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUEzVUY7QUE4VUU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxTQUFwQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERjtBQUtFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBTEY7QUFRRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUkY7QUFGRixpQkFERjtBQWlCRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWpCRjtBQTRCRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBNUJGO0FBREYsYUE5VUY7QUFvWEU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUFwWEY7QUF1WEU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxTQUFwQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERjtBQUtFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBTEY7QUFTRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQVRGO0FBYUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQWJGO0FBRkYsaUJBREY7QUFzQkU7QUFBQTtBQUFBO0FBQUE7QUFFRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGO0FBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZGO0FBRkYsaUJBdEJGO0FBcUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBckNGO0FBMENFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBMUNGO0FBK0NFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBL0NGO0FBc0RFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBdERGO0FBMERFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBMURGO0FBOERFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUE5REY7QUFERixhQXZYRjtBQXFjRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFVBQXBDO0FBQUE7QUFBQSxhQXJjRjtBQXdjRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFNBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFFRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGO0FBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGRjtBQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQU5GO0FBVUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFWRjtBQWVFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBZkY7QUFtQkU7QUFBQTtBQUFBO0FBQUE7QUFFRSwrREFGRjtBQUFBO0FBQUE7QUFuQkY7QUFGRixpQkFERjtBQTZCRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBN0JGO0FBREYsYUF4Y0Y7QUErZUU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUEvZUY7QUFrZkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxTQUFwQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREY7QUFLRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEY7QUFERixhQWxmRjtBQWlnQkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUFqZ0JGO0FBb2dCRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFNBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERjtBQU1FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFORjtBQURGLGFBcGdCRjtBQW9oQkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUFwaEJGO0FBdWhCRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFNBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERjtBQU9FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFQRjtBQURGLGFBdmhCRjtBQXlpQkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUF6aUJGO0FBNGlCRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFNBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERjtBQU1FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFORjtBQURGLGFBNWlCRjtBQTJqQkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUEzakJGO0FBOGpCRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFNBQXBDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBREYsYUE5akJGO0FBMmtCRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFVBQXBDO0FBQUE7QUFBQSxhQTNrQkY7QUE4a0JFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUxGO0FBREYsYUE5a0JGO0FBMmxCRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFVBQXBDO0FBQUE7QUFBQSxhQTNsQkY7QUE4bEJFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssU0FBcEM7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQU1FO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBTkY7QUFPRTtBQUFBO0FBQUE7QUFBQTtBQUVFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUpGO0FBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFMRjtBQU1FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFORjtBQUZGO0FBUEY7QUFERjtBQTlsQkY7QUFORixTQURGO0FBMHVCRSx3RUFBc0IsSUFBSSxzQkFBMUI7QUExdUJGLE9BREY7QUE4dUJEOzs7OztBQUdIRyxNQUFNZixTQUFOLEdBQWtCO0FBQ2hCSCxXQUFTLG9CQUFVSSxNQUFWLENBQWlCQztBQURWLENBQWxCOztrQkFJZSwwQkFBV2QsTUFBWCxFQUFtQjJCLEtBQW5CLEMiLCJmaWxlIjoiMTMuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTAgNkw4LjU5IDcuNDEgMTMuMTcgMTJsLTQuNTggNC41OUwxMCAxOGw2LTZ6JyB9KTtcblxudmFyIE5hdmlnYXRlTmV4dCA9IGZ1bmN0aW9uIE5hdmlnYXRlTmV4dChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuTmF2aWdhdGVOZXh0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShOYXZpZ2F0ZU5leHQpO1xuTmF2aWdhdGVOZXh0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IE5hdmlnYXRlTmV4dDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9OYXZpZ2F0ZU5leHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL05hdmlnYXRlTmV4dC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2lubGluZS1ibG9jaycsXG4gICAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcbiAgICAgIGhlaWdodDogMjQsXG4gICAgICB3aWR0aDogMjQsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZScsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdmaWxsJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlclxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRWxlbWVudHMgcGFzc2VkIGludG8gdGhlIFNWRyBJY29uLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm92aWRlcyBhIGh1bWFuLXJlYWRhYmxlIHRpdGxlIGZvciB0aGUgZWxlbWVudCB0aGF0IGNvbnRhaW5zIGl0LlxuICAgKiBodHRwczovL3d3dy53My5vcmcvVFIvU1ZHLWFjY2Vzcy8jRXF1aXZhbGVudFxuICAgKi9cbiAgdGl0bGVBY2Nlc3M6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsbG93cyB5b3UgdG8gcmVkZWZpbmUgd2hhdCB0aGUgY29vcmRpbmF0ZXMgd2l0aG91dCB1bml0cyBtZWFuIGluc2lkZSBhbiBzdmcgZWxlbWVudC5cbiAgICogRm9yIGV4YW1wbGUsIGlmIHRoZSBTVkcgZWxlbWVudCBpcyA1MDAgKHdpZHRoKSBieSAyMDAgKGhlaWdodCksXG4gICAqIGFuZCB5b3UgcGFzcyB2aWV3Qm94PVwiMCAwIDUwIDIwXCIsXG4gICAqIHRoaXMgbWVhbnMgdGhhdCB0aGUgY29vcmRpbmF0ZXMgaW5zaWRlIHRoZSBzdmcgd2lsbCBnbyBmcm9tIHRoZSB0b3AgbGVmdCBjb3JuZXIgKDAsMClcbiAgICogdG8gYm90dG9tIHJpZ2h0ICg1MCwyMCkgYW5kIGVhY2ggdW5pdCB3aWxsIGJlIHdvcnRoIDEwcHguXG4gICAqL1xuICB2aWV3Qm94OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWRcbn07XG5cbnZhciBTdmdJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoU3ZnSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3ZnSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBTdmdJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoU3ZnSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU3ZnSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU3ZnSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgdGl0bGVBY2Nlc3MgPSBfcHJvcHMudGl0bGVBY2Nlc3MsXG4gICAgICAgICAgdmlld0JveCA9IF9wcm9wcy52aWV3Qm94LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ3RpdGxlQWNjZXNzJywgJ3ZpZXdCb3gnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzdmcnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBmb2N1c2FibGU6ICdmYWxzZScsXG4gICAgICAgICAgdmlld0JveDogdmlld0JveCxcbiAgICAgICAgICAnYXJpYS1oaWRkZW4nOiB0aXRsZUFjY2VzcyA/ICdmYWxzZScgOiAndHJ1ZSdcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICB0aXRsZUFjY2VzcyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICd0aXRsZScsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICB0aXRsZUFjY2Vzc1xuICAgICAgICApIDogbnVsbCxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBTdmdJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuU3ZnSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpZXdCb3g6ICcwIDAgMjQgMjQnLFxuICBjb2xvcjogJ2luaGVyaXQnXG59O1xuU3ZnSWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVN2Z0ljb24nIH0pKFN2Z0ljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi9TdmdJY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2ludmFyaWFudCA9IHJlcXVpcmUoJ2ludmFyaWFudCcpO1xuXG52YXIgX2ludmFyaWFudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbnZhcmlhbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGlzTW9kaWZpZWRFdmVudCA9IGZ1bmN0aW9uIGlzTW9kaWZpZWRFdmVudChldmVudCkge1xuICByZXR1cm4gISEoZXZlbnQubWV0YUtleSB8fCBldmVudC5hbHRLZXkgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5zaGlmdEtleSk7XG59O1xuXG4vKipcbiAqIFRoZSBwdWJsaWMgQVBJIGZvciByZW5kZXJpbmcgYSBoaXN0b3J5LWF3YXJlIDxhPi5cbiAqL1xuXG52YXIgTGluayA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhMaW5rLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaW5rKCkge1xuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTGluayk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbC5hcHBseShfUmVhY3QkQ29tcG9uZW50LCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlQ2xpY2sgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkNsaWNrKSBfdGhpcy5wcm9wcy5vbkNsaWNrKGV2ZW50KTtcblxuICAgICAgaWYgKCFldmVudC5kZWZhdWx0UHJldmVudGVkICYmIC8vIG9uQ2xpY2sgcHJldmVudGVkIGRlZmF1bHRcbiAgICAgIGV2ZW50LmJ1dHRvbiA9PT0gMCAmJiAvLyBpZ25vcmUgcmlnaHQgY2xpY2tzXG4gICAgICAhX3RoaXMucHJvcHMudGFyZ2V0ICYmIC8vIGxldCBicm93c2VyIGhhbmRsZSBcInRhcmdldD1fYmxhbmtcIiBldGMuXG4gICAgICAhaXNNb2RpZmllZEV2ZW50KGV2ZW50KSAvLyBpZ25vcmUgY2xpY2tzIHdpdGggbW9kaWZpZXIga2V5c1xuICAgICAgKSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgIHZhciBoaXN0b3J5ID0gX3RoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeTtcbiAgICAgICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICAgICAgcmVwbGFjZSA9IF90aGlzJHByb3BzLnJlcGxhY2UsXG4gICAgICAgICAgICAgIHRvID0gX3RoaXMkcHJvcHMudG87XG5cblxuICAgICAgICAgIGlmIChyZXBsYWNlKSB7XG4gICAgICAgICAgICBoaXN0b3J5LnJlcGxhY2UodG8pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBoaXN0b3J5LnB1c2godG8pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgTGluay5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICByZXBsYWNlID0gX3Byb3BzLnJlcGxhY2UsXG4gICAgICAgIHRvID0gX3Byb3BzLnRvLFxuICAgICAgICBpbm5lclJlZiA9IF9wcm9wcy5pbm5lclJlZixcbiAgICAgICAgcHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzLCBbJ3JlcGxhY2UnLCAndG8nLCAnaW5uZXJSZWYnXSk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW51c2VkLXZhcnNcblxuICAgICgwLCBfaW52YXJpYW50Mi5kZWZhdWx0KSh0aGlzLmNvbnRleHQucm91dGVyLCAnWW91IHNob3VsZCBub3QgdXNlIDxMaW5rPiBvdXRzaWRlIGEgPFJvdXRlcj4nKTtcblxuICAgIHZhciBocmVmID0gdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5LmNyZWF0ZUhyZWYodHlwZW9mIHRvID09PSAnc3RyaW5nJyA/IHsgcGF0aG5hbWU6IHRvIH0gOiB0byk7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2EnLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgb25DbGljazogdGhpcy5oYW5kbGVDbGljaywgaHJlZjogaHJlZiwgcmVmOiBpbm5lclJlZiB9KSk7XG4gIH07XG5cbiAgcmV0dXJuIExpbms7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5MaW5rLnByb3BUeXBlcyA9IHtcbiAgb25DbGljazogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLFxuICB0YXJnZXQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIHRvOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XSkuaXNSZXF1aXJlZCxcbiAgaW5uZXJSZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jXSlcbn07XG5MaW5rLmRlZmF1bHRQcm9wcyA9IHtcbiAgcmVwbGFjZTogZmFsc2Vcbn07XG5MaW5rLmNvbnRleHRUeXBlcyA9IHtcbiAgcm91dGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICBoaXN0b3J5OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICAgIHB1c2g6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICBjcmVhdGVIcmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZFxuICAgIH0pLmlzUmVxdWlyZWRcbiAgfSkuaXNSZXF1aXJlZFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9IExpbms7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNiAyNyAyOCAzMCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA1NyA1OCA1OSA2MSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gcmVxdWlyZSgnLi91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsJyk7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUZhY3RvcnkgPSBmdW5jdGlvbiBjcmVhdGVGYWN0b3J5KHR5cGUpIHtcbiAgdmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50ID0gKDAsIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Mi5kZWZhdWx0KSh0eXBlKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwLCBjKSB7XG4gICAgcmV0dXJuICgwLCBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIuZGVmYXVsdCkoZmFsc2UsIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwLCBjKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUZhY3Rvcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGdldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gZ2V0RGlzcGxheU5hbWUoQ29tcG9uZW50KSB7XG4gIGlmICh0eXBlb2YgQ29tcG9uZW50ID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBDb21wb25lbnQ7XG4gIH1cblxuICBpZiAoIUNvbXBvbmVudCkge1xuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgaXNDbGFzc0NvbXBvbmVudCA9IGZ1bmN0aW9uIGlzQ2xhc3NDb21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKENvbXBvbmVudCAmJiBDb21wb25lbnQucHJvdG90eXBlICYmIF90eXBlb2YoQ29tcG9uZW50LnByb3RvdHlwZS5pc1JlYWN0Q29tcG9uZW50KSA9PT0gJ29iamVjdCcpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNDbGFzc0NvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc0NsYXNzQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc0NsYXNzQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSBmdW5jdGlvbiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbih0eXBlb2YgQ29tcG9uZW50ID09PSAnZnVuY3Rpb24nICYmICEoMCwgX2lzQ2xhc3NDb21wb25lbnQyLmRlZmF1bHQpKENvbXBvbmVudCkgJiYgIUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgJiYgIUNvbXBvbmVudC5jb250ZXh0VHlwZXMgJiYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicgfHwgIUNvbXBvbmVudC5wcm9wVHlwZXMpKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5ID0gcmVxdWlyZSgnLi9jcmVhdGVFYWdlckZhY3RvcnknKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJGYWN0b3J5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSBmdW5jdGlvbiBjcmVhdGVFYWdlckVsZW1lbnRVdGlsKGhhc0tleSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHByb3BzLCBjaGlsZHJlbikge1xuICBpZiAoIWhhc0tleSAmJiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCkge1xuICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgcmV0dXJuIHR5cGUoX2V4dGVuZHMoe30sIHByb3BzLCB7IGNoaWxkcmVuOiBjaGlsZHJlbiB9KSk7XG4gICAgfVxuICAgIHJldHVybiB0eXBlKHByb3BzKTtcbiAgfVxuXG4gIHZhciBDb21wb25lbnQgPSB0eXBlO1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgIENvbXBvbmVudCxcbiAgICAgIHByb3BzLFxuICAgICAgY2hpbGRyZW5cbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgcHJvcHMpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRWFnZXJFbGVtZW50VXRpbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9nZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vZ2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9nZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciB3cmFwRGlzcGxheU5hbWUgPSBmdW5jdGlvbiB3cmFwRGlzcGxheU5hbWUoQmFzZUNvbXBvbmVudCwgaG9jTmFtZSkge1xuICByZXR1cm4gaG9jTmFtZSArICcoJyArICgwLCBfZ2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpICsgJyknO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd3JhcERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIi8qKlxuICogVGhpcyBjb21wb25lbnQgaXMgY3JlYXRlIHRvIHNob3cgbmF2aWFndGlvbiBidXR0b24gdG8gbW92ZSBuZXh0IHBhZ2VcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMaW5rIGZyb20gJ3JlYWN0LXJvdXRlci1kb20vTGluayc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcblxuaW1wb3J0IE5hdmlnYXRlTmV4dEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvTmF2aWdhdGVOZXh0JztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7XG4gICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgcmlnaHQ6ICcyJScsXG4gICAgYm90dG9tOiAnNCUnLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge1xuICAgICAgcmlnaHQ6ICcyJScsXG4gICAgICBib3R0b206ICcyJScsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIHJpZ2h0OiAnMiUnLFxuICAgICAgYm90dG9tOiAnMSUnLFxuICAgIH0sXG4gIH0sXG59KTtcblxuLyoqXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGNsYXNzZXNcbiAqIEBwYXJhbSB7cGF0aH0gdG9cbiAqL1xuY29uc3QgTmF2aWdhdGVCdXR0b25OZXh0ID0gKHsgY2xhc3NlcywgdG8gPSAnLycgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICA8QnV0dG9uXG4gICAgICBmYWJcbiAgICAgIGNvbG9yPVwiYWNjZW50XCJcbiAgICAgIGNvbXBvbmVudD17TGlua31cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5idXR0b259XG4gICAgICByYWlzZWRcbiAgICAgIHRvPXt0b31cbiAgICA+XG4gICAgICA8TmF2aWdhdGVOZXh0SWNvbiAvPlxuICAgIDwvQnV0dG9uPlxuICA8L2Rpdj5cbik7XG5cbk5hdmlnYXRlQnV0dG9uTmV4dC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgdG86IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbn07XG5cbk5hdmlnYXRlQnV0dG9uTmV4dC5kZWZhdWx0UHJvcHMgPSB7XG4gIHRvOiAnLycsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTmF2aWdhdGVCdXR0b25OZXh0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9OYXZpZ2F0aW9uQnV0dG9uTmV4dC5qcyIsIi8qKlxuICogVGhpcyBjb21wb25lbnQgcmVwcmVzZW50cyBEb25hdGlvbiBwYWdlIGZvciBtYXlhc2hcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnbWF0ZXJpYWwtdWkvVHlwb2dyYXBoeSc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcblxuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciwgQ2FyZE1lZGlhLCBDYXJkQ29udGVudCB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuXG5pbXBvcnQgTmF2aWdhdGlvbkJ1dHRvbk5leHQgZnJvbSAnLi4vY29tcG9uZW50cy9OYXZpZ2F0aW9uQnV0dG9uTmV4dCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge30sXG4gIGNhcmRIZWFkZXI6IHtcbiAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgIHBhZGRpbmc6ICczZW0nLFxuICB9LFxuICBtZWRpYToge1xuICAgIGhlaWdodDogMzAwLFxuICAgIHdpZHRoOiAzMDAsXG4gIH0sXG4gIG1lZGl1bToge1xuICAgIHBhZGRpbmc6ICc0MHB4JyxcbiAgfSxcbiAgY2FyZENvbnRlbnQ6IHtcbiAgICBwYWRkaW5nTGVmdDogJzhlbScsXG4gICAgdGV4dEFsaWduOiAnanVzdGlmeScsXG4gICAgcGFkZGluZ1JpZ2h0OiAnOGVtJyxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHtcbiAgICAgIHBhZGRpbmdMZWZ0OiAnMmVtJyxcbiAgICAgIHBhZGRpbmdSaWdodDogJzJlbScsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIHBhZGRpbmc6ICcxMHB4JyxcbiAgICB9LFxuICB9LFxufSk7XG5cbmNsYXNzIFRlbXJzIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxDYXJkPlxuICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICB0aXRsZT1cIlRlcm1zIGFuZCBjb25kaXRpb25zIG9mIHVzZVwiXG4gICAgICAgICAgICBzdWJoZWFkZXI9eydIYmFydmUgdGVjaG5vbG9naWVzIChPUEMpIHByaXZhdGUgbGltaXRlZCBUZXJtcyBvZiBTZXJ2aWNlJ30gXG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZEhlYWRlcn0gXG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Q2FyZENvbnRlbnQgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmRDb250ZW50fT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAxLiBJbnRyb2R1Y3Rpb25cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBUaGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9ucyBzaGFsbCBnb3Zlcm4geW91ciB1c2Ugb2Ygb3VyXG4gICAgICAgICAgICAgICAgICB3ZWJzaXRlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgQnkgdXNpbmcgb3VyIHdlYnNpdGUsIHlvdSBhY2NlcHQgdGhlc2UgdGVybXMgYW5kXG4gICAgICAgICAgICAgICAgICBjb25kaXRpb25zIGluIGZ1bGw7IGFjY29yZGluZ2x5LCBpZiB5b3UgZGlzYWdyZWUgd2l0aFxuICAgICAgICAgICAgICAgICAgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnMgb3IgYW55IHBhcnQgb2YgdGhlc2UgdGVybXMgYW5kXG4gICAgICAgICAgICAgICAgICBjb25kaXRpb25zLCB5b3UgbXVzdCBub3QgdXNlIG91ciB3ZWJzaXRlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgSWYgeW91IHJlZ2lzdGVyIHdpdGggb3VyIEhiYXJ2ZSBJbmZvcm1hdGlvbiBUZWNobm9sb2d5XG4gICAgICAgICAgICAgICAgICAoT1BDKSBQcml2YXRlIExpbWl0ZWQsIHN1Ym1pdCBhbnkgbWF0ZXJpYWwgdG8gb3VyIHdlYnNpdGVcbiAgICAgICAgICAgICAgICAgIG9yIHVzZSBhbnkgb2Ygb3VyIHdlYnNpdGUgc2VydmljZXMsIHdlIHdpbGwgYXNrIHlvdSB0b1xuICAgICAgICAgICAgICAgICAgZXhwcmVzc2x5IGFncmVlIHRvIHRoZSBQcml2YWN5IFBvbGljeS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIE91ciB3ZWJzaXRlIHVzZXMgY29va2llczsgYnkgdXNpbmcgb3VyIHdlYnNpdGUgb3IgYWdyZWVpbmdcbiAgICAgICAgICAgICAgICAgIHRvIHRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zLCB5b3UgY29uc2VudCB0byBvdXIgdXNlIG9mXG4gICAgICAgICAgICAgICAgICBjb29raWVzIGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgdGVybXMgb2Ygb3VyIHByaXZhY3lcbiAgICAgICAgICAgICAgICAgIHBvbGljeS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L29sPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDIuIENyZWRpdFxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPG9sPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFRoaXMgZG9jdW1lbnQgaXMgYW4gU0VRIExlZ2FsIGRvY3VtZW50IGZyb20gV2Vic2l0ZVxuICAgICAgICAgICAgICAgICAgQ29udHJhY3RzIGh0dHBzOi8vbWF5YXNoLmlvLiBZb3UgbXVzdCByZXRhaW4gdGhlIGFib3ZlXG4gICAgICAgICAgICAgICAgICBjcmVkaXQuIFVzZSBvZiB0aGlzIGRvY3VtZW50IHdpdGhvdXQgdGhlIGNyZWRpdCBpcyBhblxuICAgICAgICAgICAgICAgICAgaW5mcmluZ2VtZW50IG9mIGNvcHlyaWdodC4gSG93ZXZlciwgeW91IGNhbiBwdXJjaGFzZSBmcm9tXG4gICAgICAgICAgICAgICAgICB1cyBhbiBlcXVpdmFsZW50IGRvY3VtZW50IHRoYXQgZG9lcyBub3QgaW5jbHVkZSB0aGVcbiAgICAgICAgICAgICAgICAgIGNyZWRpdC5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L29sPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDMuIENvcHlyaWdodCBub3RpY2VcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBDb3B5cmlnaHQgKGMpIFsyMDE3KHMpIG9mIGZpcnN0IHB1YmxpY2F0aW9uXSBbTWF5YXNoXG4gICAgICAgICAgICAgICAgICBjb2Rlc10uXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBTdWJqZWN0IHRvIHRoZSBleHByZXNzIHByb3Zpc2lvbnMgb2YgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnM6XG4gICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICB3ZSwgdG9nZXRoZXIgd2l0aCBvdXIgbGljZW5zb3JzLCBvd24gYW5kIGNvbnRyb2wgYWxsXG4gICAgICAgICAgICAgICAgICAgICAgdGhlIGNvcHlyaWdodCBhbmQgb3RoZXIgaW50ZWxsZWN0dWFsIHByb3BlcnR5IHJpZ2h0c1xuICAgICAgICAgICAgICAgICAgICAgIGluIG91ciB3ZWJzaXRlIGFuZCB0aGUgbWF0ZXJpYWwgb24gb3VyIHdlYnNpdGU7IGFuZFxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgYWxsIHRoZSBjb3B5cmlnaHQgYW5kIG90aGVyIGludGVsbGVjdHVhbCBwcm9wZXJ0eVxuICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0cyBpbiBvdXIgd2Vic2l0ZSBhbmQgdGhlIG1hdGVyaWFsIG9uIG91ciB3ZWJzaXRlXG4gICAgICAgICAgICAgICAgICAgICAgYXJlIHJlc2VydmVkLlxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L29sPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDQuIExpY2VuY2UgdG8gdXNlIHdlYnNpdGVcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3UgbWF5OlxuICAgICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAgICA8bGk+dmlldyBwYWdlcyBmcm9tIG91ciB3ZWJzaXRlIGluIGEgd2ViIGJyb3dzZXI7PC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIGRvd25sb2FkIHBhZ2VzIGZyb20gb3VyIHdlYnNpdGUgZm9yIGNhY2hpbmcgaW4gYSB3ZWJcbiAgICAgICAgICAgICAgICAgICAgICBicm93c2VyO1xuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+cHJpbnQgcGFnZXMgZnJvbSBvdXIgd2Vic2l0ZTs8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgW3N0cmVhbSBhdWRpbyBhbmQgdmlkZW8gZmlsZXMgZnJvbSBvdXIgd2Vic2l0ZV07IGFuZFxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgW3VzZSBbb3VyIHdlYnNpdGUgc2VydmljZXNdIGJ5IG1lYW5zIG9mIGEgd2ViXG4gICAgICAgICAgICAgICAgICAgICAgYnJvd3Nlcl0sIHN1YmplY3QgdG8gdGhlIG90aGVyIHByb3Zpc2lvbnMgb2YgdGhlc2VcbiAgICAgICAgICAgICAgICAgICAgICB0ZXJtcyBhbmQgY29uZGl0aW9ucy5cbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBFeGNlcHQgYXMgZXhwcmVzc2x5IHBlcm1pdHRlZCBieSBTZWN0aW9uIDQuMSBvciB0aGUgb3RoZXJcbiAgICAgICAgICAgICAgICAgIHByb3Zpc2lvbnMgb2YgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnMsIHlvdSBtdXN0IG5vdFxuICAgICAgICAgICAgICAgICAgZG93bmxvYWQgYW55IG1hdGVyaWFsIGZyb20gb3VyIHdlYnNpdGUgb3Igc2F2ZSBhbnkgc3VjaFxuICAgICAgICAgICAgICAgICAgbWF0ZXJpYWwgdG8geW91ciBjb21wdXRlci5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBtYXkgb25seSB1c2Ugb3VyIHdlYnNpdGUgZm9yIFt5b3VyIG93biBwZXJzb25hbCBhbmRcbiAgICAgICAgICAgICAgICAgIGJ1c2luZXNzIHB1cnBvc2VzXSwgYW5kIHlvdSBtdXN0IG5vdCB1c2Ugb3VyIHdlYnNpdGUgZm9yXG4gICAgICAgICAgICAgICAgICBhbnkgb3RoZXIgcHVycG9zZXMuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBFeGNlcHQgYXMgZXhwcmVzc2x5IHBlcm1pdHRlZCBieSB0aGVzZSB0ZXJtcyBhbmRcbiAgICAgICAgICAgICAgICAgIGNvbmRpdGlvbnMsIHlvdSBtdXN0IG5vdCBlZGl0IG9yIG90aGVyd2lzZSBtb2RpZnkgYW55XG4gICAgICAgICAgICAgICAgICBtYXRlcmlhbCBvbiBvdXIgd2Vic2l0ZS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFVubGVzcyB5b3Ugb3duIG9yIGNvbnRyb2wgdGhlIHJlbGV2YW50IHJpZ2h0cyBpbiB0aGUgbWF0ZXJpYWwsIHlvdSBtdXN0IG5vdDpcbiAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIHNlbGwsIHJlbnQgb3Igc3ViLWxpY2Vuc2UgbWF0ZXJpYWwgZnJvbSBvdXIgd2Vic2l0ZTtcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIGV4cGxvaXQgbWF0ZXJpYWwgZnJvbSBvdXIgd2Vic2l0ZSBmb3IgYSBjb21tZXJjaWFsXG4gICAgICAgICAgICAgICAgICAgICAgcHVycG9zZTsgb3JcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBOb3R3aXRoc3RhbmRpbmcgU2VjdGlvbiA0LjUsIHlvdSBtYXkgcmVkaXN0cmlidXRlIFtvdXJcbiAgICAgICAgICAgICAgICAgIG5ld3NsZXR0ZXJdIGluIFtwcmludCBhbmQgZWxlY3Ryb25pYyBmb3JtXSB0byBbYW55XG4gICAgICAgICAgICAgICAgICBwZXJzb25dLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgV2UgcmVzZXJ2ZSB0aGUgcmlnaHQgdG8gcmVzdHJpY3QgYWNjZXNzIHRvIGFyZWFzIG9mIG91clxuICAgICAgICAgICAgICAgICAgd2Vic2l0ZSwgb3IgaW5kZWVkIG91ciB3aG9sZSB3ZWJzaXRlLCBhdCBvdXIgZGlzY3JldGlvbjtcbiAgICAgICAgICAgICAgICAgIHlvdSBtdXN0IG5vdCBjaXJjdW12ZW50IG9yIGJ5cGFzcywgb3IgYXR0ZW1wdCB0b1xuICAgICAgICAgICAgICAgICAgY2lyY3VtdmVudCBvciBieXBhc3MsIGFueSBhY2Nlc3MgcmVzdHJpY3Rpb24gbWVhc3VyZXMgb25cbiAgICAgICAgICAgICAgICAgIG91ciB3ZWJzaXRlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgNS4gQWNjZXB0YWJsZSB1c2VcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3UgbXVzdCBub3Q6XG4gICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICB1c2Ugb3VyIHdlYnNpdGUgaW4gYW55IHdheSBvciB0YWtlIGFueSBhY3Rpb24gdGhhdFxuICAgICAgICAgICAgICAgICAgICAgIGNhdXNlcywgb3IgbWF5IGNhdXNlLCBkYW1hZ2UgdG8gdGhlIHdlYnNpdGUgb3JcbiAgICAgICAgICAgICAgICAgICAgICBpbXBhaXJtZW50IG9mIHRoZSBwZXJmb3JtYW5jZSwgYXZhaWxhYmlsaXR5IG9yXG4gICAgICAgICAgICAgICAgICAgICAgYWNjZXNzaWJpbGl0eSBvZiB0aGUgd2Vic2l0ZTtcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIHVzZSBvdXIgd2Vic2l0ZSBpbiBhbnkgd2F5IHRoYXQgaXMgdW5sYXdmdWwsIGlsbGVnYWwsXG4gICAgICAgICAgICAgICAgICAgICAgZnJhdWR1bGVudCBvciBoYXJtZnVsLCBvciBpbiBjb25uZWN0aW9uIHdpdGggYW55XG4gICAgICAgICAgICAgICAgICAgICAgdW5sYXdmdWwsIGlsbGVnYWwsIGZyYXVkdWxlbnQgb3IgaGFybWZ1bCBwdXJwb3NlIG9yXG4gICAgICAgICAgICAgICAgICAgICAgYWN0aXZpdHk7XG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICB1c2Ugb3VyIHdlYnNpdGUgdG8gY29weSwgc3RvcmUsIGhvc3QsIHRyYW5zbWl0LCBzZW5kLFxuICAgICAgICAgICAgICAgICAgICAgIHVzZSwgcHVibGlzaCBvciBkaXN0cmlidXRlIGFueSBtYXRlcmlhbCB3aGljaCBjb25zaXN0c1xuICAgICAgICAgICAgICAgICAgICAgIG9mIChvciBpcyBsaW5rZWQgdG8pIGFueSBzcHl3YXJlLCBjb21wdXRlciB2aXJ1cyxcbiAgICAgICAgICAgICAgICAgICAgICBUcm9qYW4gaG9yc2UsIHdvcm0sIGtleXN0cm9rZSBsb2dnZXIsIHJvb3RraXQgb3Igb3RoZXJcbiAgICAgICAgICAgICAgICAgICAgICBtYWxpY2lvdXMgY29tcHV0ZXIgc29mdHdhcmU7XG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBbY29uZHVjdCBhbnkgc3lzdGVtYXRpYyBvciBhdXRvbWF0ZWQgZGF0YSBjb2xsZWN0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgYWN0aXZpdGllcyAoaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbiBzY3JhcGluZyxcbiAgICAgICAgICAgICAgICAgICAgICBkYXRhIG1pbmluZywgZGF0YSBleHRyYWN0aW9uIGFuZCBkYXRhIGhhcnZlc3RpbmcpIG9uXG4gICAgICAgICAgICAgICAgICAgICAgb3IgaW4gcmVsYXRpb24gdG8gb3VyIHdlYnNpdGUgd2l0aG91dCBvdXIgZXhwcmVzc1xuICAgICAgICAgICAgICAgICAgICAgIHdyaXR0ZW4gY29uc2VudF1cbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIFthY2Nlc3Mgb3Igb3RoZXJ3aXNlIGludGVyYWN0IHdpdGggb3VyIHdlYnNpdGUgdXNpbmdcbiAgICAgICAgICAgICAgICAgICAgICBhbnkgcm9ib3QsIHNwaWRlciBvciBvdGhlciBhdXRvbWF0ZWQgbWVhbnNbLCBleGNlcHRcbiAgICAgICAgICAgICAgICAgICAgICBmb3IgdGhlIHB1cnBvc2Ugb2YgW3NlYXJjaCBlbmdpbmUgaW5kZXhpbmddXV07XG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBbdmlvbGF0ZSB0aGUgZGlyZWN0aXZlcyBzZXQgb3V0IGluIHRoZSByb2JvdHMudHh0IGZpbGVcbiAgICAgICAgICAgICAgICAgICAgICBmb3Igb3VyIHdlYnNpdGVdOyBvclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgW3VzZSBkYXRhIGNvbGxlY3RlZCBmcm9tIG91ciB3ZWJzaXRlIGZvciBhbnkgZGlyZWN0XG4gICAgICAgICAgICAgICAgICAgICAgbWFya2V0aW5nIGFjdGl2aXR5IChpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIGVtYWlsXG4gICAgICAgICAgICAgICAgICAgICAgbWFya2V0aW5nLCBTTVMgbWFya2V0aW5nLCB0ZWxlbWFya2V0aW5nIGFuZCBkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICBtYWlsaW5nKV0uIFthZGRpdGlvbmFsIGxpc3QgaXRlbXNdXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IG11c3Qgbm90IHVzZSBkYXRhIGNvbGxlY3RlZCBmcm9tIG91ciB3ZWJzaXRlIHRvXG4gICAgICAgICAgICAgICAgICBjb250YWN0IGluZGl2aWR1YWxzLCBjb21wYW5pZXMgb3Igb3RoZXIgcGVyc29ucyBvclxuICAgICAgICAgICAgICAgICAgZW50aXRpZXMuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3UgbXVzdCBlbnN1cmUgdGhhdCBhbGwgdGhlIGluZm9ybWF0aW9uIHlvdSBzdXBwbHkgdG8gdXNcbiAgICAgICAgICAgICAgICAgIHRocm91Z2ggb3VyIHdlYnNpdGUsIG9yIGluIHJlbGF0aW9uIHRvIG91ciB3ZWJzaXRlLCBpc1xuICAgICAgICAgICAgICAgICAgW3RydWUsIGFjY3VyYXRlLCBjdXJyZW50LCBjb21wbGV0ZSBhbmQgbm9uLW1pc2xlYWRpbmddLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICA2LiBSZWdpc3RyYXRpb24gYW5kIGFjY291bnRzXG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiZGlzcGxheVwiPlxuICAgICAgICAgICAgICA8b2w+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IG1heSByZWdpc3RlciBmb3IgYW4gYWNjb3VudCB3aXRoIG91ciB3ZWJzaXRlIGJ5XG4gICAgICAgICAgICAgICAgICBbY29tcGxldGluZyBhbmQgc3VibWl0dGluZyB0aGUgYWNjb3VudCByZWdpc3RyYXRpb24gZm9ybVxuICAgICAgICAgICAgICAgICAgb24gb3VyIHdlYnNpdGVcbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBtdXN0IG5vdCBhbGxvdyBhbnkgb3RoZXIgcGVyc29uIHRvIHVzZSB5b3VyIGFjY291bnQgdG9cbiAgICAgICAgICAgICAgICAgIGFjY2VzcyB0aGUgd2Vic2l0ZS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBtdXN0IG5vdGlmeSB1cyBpbiB3cml0aW5nIGltbWVkaWF0ZWx5IGlmIHlvdSBiZWNvbWVcbiAgICAgICAgICAgICAgICAgIGF3YXJlIG9mIGFueSB1bmF1dGhvcmlzZWQgdXNlIG9mIHlvdXIgYWNjb3VudFxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IG11c3Qgbm90IHVzZSBhbnkgb3RoZXIgcGVyc29uJ3MgYWNjb3VudCB0byBhY2Nlc3MgdGhlXG4gICAgICAgICAgICAgICAgICB3ZWJzaXRlWywgdW5sZXNzIHlvdSBoYXZlIHRoYXQgcGVyc29uJ3MgZXhwcmVzcyBwZXJtaXNzaW9uXG4gICAgICAgICAgICAgICAgICB0byBkbyBzb10uXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICA3LiBVc2VyIGxvZ2luIGRldGFpbHNcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBJZiB5b3UgcmVnaXN0ZXIgZm9yIGFuIGFjY291bnQgd2l0aCBvdXIgd2Vic2l0ZSwgW3dlIHdpbGxcbiAgICAgICAgICAgICAgICAgIHByb3ZpZGUgeW91IHdpdGhdIE9SIFt5b3Ugd2lsbCBiZSBhc2tlZCB0byBjaG9vc2VdIFthIHVzZXJcbiAgICAgICAgICAgICAgICAgIElEIGFuZCBwYXNzd29yZF0uXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3VyIHVzZXIgSUQgbXVzdCBub3QgYmUgbGlhYmxlIHRvIG1pc2xlYWQgYW5kIG11c3QgY29tcGx5XG4gICAgICAgICAgICAgICAgICB3aXRoIHRoZSBjb250ZW50IHJ1bGVzIHNldCBvdXQgaW4gU2VjdGlvbiAxMDsgeW91IG11c3Qgbm90XG4gICAgICAgICAgICAgICAgICB1c2UgeW91ciBhY2NvdW50IG9yIHVzZXIgSUQgZm9yIG9yIGluIGNvbm5lY3Rpb24gd2l0aCB0aGVcbiAgICAgICAgICAgICAgICAgIGltcGVyc29uYXRpb24gb2YgYW55IHBlcnNvbi5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5Zb3UgbXVzdCBrZWVwIHlvdXIgcGFzc3dvcmQgY29uZmlkZW50aWFsLjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IG11c3Qgbm90aWZ5IHVzIGluIHdyaXRpbmcgaW1tZWRpYXRlbHkgaWYgeW91IGJlY29tZVxuICAgICAgICAgICAgICAgICAgYXdhcmUgb2YgYW55IGRpc2Nsb3N1cmUgb2YgeW91ciBwYXNzd29yZC5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBhcmUgcmVzcG9uc2libGUgZm9yIGFueSBhY3Rpdml0eSBvbiBvdXIgd2Vic2l0ZVxuICAgICAgICAgICAgICAgICAgYXJpc2luZyBvdXQgb2YgYW55IGZhaWx1cmUgdG8ga2VlcCB5b3VyIHBhc3N3b3JkXG4gICAgICAgICAgICAgICAgICBjb25maWRlbnRpYWwsIGFuZCBtYXkgYmUgaGVsZCBsaWFibGUgZm9yIGFueSBsb3NzZXNcbiAgICAgICAgICAgICAgICAgIGFyaXNpbmcgb3V0IG9mIHN1Y2ggYSBmYWlsdXJlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICA4LiBDYW5jZWxsYXRpb24gYW5kIHN1c3BlbnNpb24gb2YgYWNjb3VudFxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPG9sPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFdlIG1heTpcbiAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPltzdXNwZW5kIHlvdXIgYWNjb3VudF07PC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPltjYW5jZWwgeW91ciBhY2NvdW50XTsgYW5kL29yPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIFtlZGl0IHlvdXIgYWNjb3VudCBkZXRhaWxzXSxhdCBhbnkgdGltZSBpbiBvdXIgc29sZVxuICAgICAgICAgICAgICAgICAgICAgIGRpc2NyZXRpb24gd2l0aG91dCBub3RpY2Ugb3IgZXhwbGFuYXRpb24uXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IG1heSBjYW5jZWwgeW91ciBhY2NvdW50IG9uIG91ciB3ZWJzaXRlIFt1c2luZyB5b3VyXG4gICAgICAgICAgICAgICAgICBhY2NvdW50IGNvbnRyb2wgcGFuZWwgb24gdGhlIHdlYnNpdGVdLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICA5LiBZb3VyIGNvbnRlbnQ6IGxpY2VuY2VcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBJbiB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9ucywgXCJ5b3VyIGNvbnRlbnRcIiBtZWFucyBbYWxsXG4gICAgICAgICAgICAgICAgICB3b3JrcyBhbmQgbWF0ZXJpYWxzIChpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRleHQsXG4gICAgICAgICAgICAgICAgICBncmFwaGljcywgaW1hZ2VzLCBhdWRpbyBtYXRlcmlhbCwgdmlkZW8gbWF0ZXJpYWwsXG4gICAgICAgICAgICAgICAgICBhdWRpby12aXN1YWwgbWF0ZXJpYWwsIHNjcmlwdHMsIHNvZnR3YXJlIGFuZCBmaWxlcykgdGhhdFxuICAgICAgICAgICAgICAgICAgeW91IHN1Ym1pdCB0byB1cyBvciBvdXIgd2Vic2l0ZSBmb3Igc3RvcmFnZSBvciBwdWJsaWNhdGlvblxuICAgICAgICAgICAgICAgICAgb24sIHByb2Nlc3NpbmcgYnksIG9yIHRyYW5zbWlzc2lvbiB2aWEsIG91ciB3ZWJzaXRlXS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBncmFudCB0byB1cyBhIFt3b3JsZHdpZGUsIGlycmV2b2NhYmxlLCBub24tZXhjbHVzaXZlLFxuICAgICAgICAgICAgICAgICAgcm95YWx0eS1mcmVlIGxpY2VuY2VdIHRvIFt1c2UsIHJlcHJvZHVjZSwgc3RvcmUsIGFkYXB0LFxuICAgICAgICAgICAgICAgICAgcHVibGlzaCwgdHJhbnNsYXRlIGFuZCBkaXN0cmlidXRlIHlvdXIgY29udGVudCBpbiBhbnlcbiAgICAgICAgICAgICAgICAgIGV4aXN0aW5nIG9yIGZ1dHVyZSBtZWRpYV0gT1IgW3JlcHJvZHVjZSwgc3RvcmUgYW5kIHB1Ymxpc2hcbiAgICAgICAgICAgICAgICAgIHlvdXIgY29udGVudCBvbiBhbmQgaW4gcmVsYXRpb24gdG8gdGhpcyB3ZWJzaXRlIGFuZCBhbnlcbiAgICAgICAgICAgICAgICAgIHN1Y2Nlc3NvciB3ZWJzaXRlXSBPUiBbcmVwcm9kdWNlLCBzdG9yZSBhbmQsIHdpdGggeW91clxuICAgICAgICAgICAgICAgICAgc3BlY2lmaWMgY29uc2VudCwgcHVibGlzaCB5b3VyIGNvbnRlbnQgb24gYW5kIGluIHJlbGF0aW9uXG4gICAgICAgICAgICAgICAgICB0byB0aGlzIHdlYnNpdGVdLiBXZSBjYW4gYWxzbyB1c2UgaXQgdG8gbWFrZSBiaWcgZGF0YS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBncmFudCB0byB1cyB0aGUgcmlnaHQgdG8gc3ViLWxpY2Vuc2UgdGhlIHJpZ2h0c1xuICAgICAgICAgICAgICAgICAgbGljZW5zZWQgdW5kZXIgU2VjdGlvbiA5LjIuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3UgZ3JhbnQgdG8gdXMgdGhlIHJpZ2h0IHRvIGJyaW5nIGFuIGFjdGlvbiBmb3JcbiAgICAgICAgICAgICAgICAgIGluZnJpbmdlbWVudCBvZiB0aGUgcmlnaHRzIGxpY2Vuc2VkIHVuZGVyIFNlY3Rpb24gOS4yLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IGhlcmVieSB3YWl2ZSBhbGwgeW91ciBtb3JhbCByaWdodHMgaW4geW91ciBjb250ZW50IHRvXG4gICAgICAgICAgICAgICAgICB0aGUgbWF4aW11bSBleHRlbnQgcGVybWl0dGVkIGJ5IGFwcGxpY2FibGUgbGF3OyBhbmQgeW91XG4gICAgICAgICAgICAgICAgICB3YXJyYW50IGFuZCByZXByZXNlbnQgdGhhdCBhbGwgb3RoZXIgbW9yYWwgcmlnaHRzIGluIHlvdXJcbiAgICAgICAgICAgICAgICAgIGNvbnRlbnQgaGF2ZSBiZWVuIHdhaXZlZCB0byB0aGUgbWF4aW11bSBleHRlbnQgcGVybWl0dGVkXG4gICAgICAgICAgICAgICAgICBieSBhcHBsaWNhYmxlIGxhdy5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFlvdSBtYXkgZWRpdCB5b3VyIGNvbnRlbnQgdG8gdGhlIGV4dGVudCBwZXJtaXR0ZWQgdXNpbmdcbiAgICAgICAgICAgICAgICAgIHRoZSBlZGl0aW5nIGZ1bmN0aW9uYWxpdHkgbWFkZSBhdmFpbGFibGUgb24gb3VyIHdlYnNpdGUuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBXaXRob3V0IHByZWp1ZGljZSB0byBvdXIgb3RoZXIgcmlnaHRzIHVuZGVyIHRoZXNlIHRlcm1zXG4gICAgICAgICAgICAgICAgICBhbmQgY29uZGl0aW9ucywgaWYgeW91IGJyZWFjaCBhbnkgcHJvdmlzaW9uIG9mIHRoZXNlIHRlcm1zXG4gICAgICAgICAgICAgICAgICBhbmQgY29uZGl0aW9ucyBpbiBhbnkgd2F5LCBvciBpZiB3ZSByZWFzb25hYmx5IHN1c3BlY3RcbiAgICAgICAgICAgICAgICAgIHRoYXQgeW91IGhhdmUgYnJlYWNoZWQgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnMgaW4gYW55XG4gICAgICAgICAgICAgICAgICB3YXksIHdlIG1heSBkZWxldGUsIHVucHVibGlzaCBvciBlZGl0IGFueSBvciBhbGwgb2YgeW91clxuICAgICAgICAgICAgICAgICAgY29udGVudC5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L29sPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDEwLiBMaW1pdGVkIHdhcnJhbnRpZXNcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBXZSBkbyBub3Qgd2FycmFudCBvciByZXByZXNlbnQ6XG4gICAgICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICB0aGUgY29tcGxldGVuZXNzIG9yIGFjY3VyYWN5IG9mIHRoZSBpbmZvcm1hdGlvblxuICAgICAgICAgICAgICAgICAgICAgIHB1Ymxpc2hlZCBvbiBvdXIgd2Vic2l0ZTtcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIHRoYXQgdGhlIG1hdGVyaWFsIG9uIHRoZSB3ZWJzaXRlIGlzIHVwIHRvIGRhdGU7IG9yXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICB0aGF0IHRoZSB3ZWJzaXRlIG9yIGFueSBzZXJ2aWNlIG9uIHRoZSB3ZWJzaXRlIHdpbGxcbiAgICAgICAgICAgICAgICAgICAgICByZW1haW4gYXZhaWxhYmxlLlxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFdlIHJlc2VydmUgdGhlIHJpZ2h0IHRvIGRpc2NvbnRpbnVlIG9yIGFsdGVyIGFueSBvciBhbGwgb2ZcbiAgICAgICAgICAgICAgICAgIG91ciB3ZWJzaXRlIHNlcnZpY2VzLCBhbmQgdG8gc3RvcCBwdWJsaXNoaW5nIG91ciB3ZWJzaXRlLFxuICAgICAgICAgICAgICAgICAgYXQgYW55IHRpbWUgaW4gb3VyIHNvbGUgZGlzY3JldGlvbiB3aXRob3V0IG5vdGljZSBvclxuICAgICAgICAgICAgICAgICAgZXhwbGFuYXRpb247IGFuZCBzYXZlIHRvIHRoZSBleHRlbnQgZXhwcmVzc2x5IHByb3ZpZGVkXG4gICAgICAgICAgICAgICAgICBvdGhlcndpc2UgaW4gdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnMsIHlvdSB3aWxsIG5vdCBiZVxuICAgICAgICAgICAgICAgICAgZW50aXRsZWQgdG8gYW55IGNvbXBlbnNhdGlvbiBvciBvdGhlciBwYXltZW50IHVwb24gdGhlXG4gICAgICAgICAgICAgICAgICBkaXNjb250aW51YW5jZSBvciBhbHRlcmF0aW9uIG9mIGFueSB3ZWJzaXRlIHNlcnZpY2VzLCBvclxuICAgICAgICAgICAgICAgICAgaWYgd2Ugc3RvcCBwdWJsaXNoaW5nIHRoZSB3ZWJzaXRlLnRoZXNlIHRlcm1zIGFuZFxuICAgICAgICAgICAgICAgICAgY29uZGl0aW9ucywgeW91IG11c3Qgbm90IHVzZSBvdXIgd2Vic2l0ZS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFRvIHRoZSBtYXhpbXVtIGV4dGVudCBwZXJtaXR0ZWQgYnkgYXBwbGljYWJsZSBsYXcgYW5kXG4gICAgICAgICAgICAgICAgICBzdWJqZWN0IHRvIFNlY3Rpb24gMTIuMSwgd2UgZXhjbHVkZSBhbGwgcmVwcmVzZW50YXRpb25zXG4gICAgICAgICAgICAgICAgICBhbmQgd2FycmFudGllcyByZWxhdGluZyB0byB0aGUgc3ViamVjdCBtYXR0ZXIgb2YgdGhlc2VcbiAgICAgICAgICAgICAgICAgIHRlcm1zIGFuZCBjb25kaXRpb25zLCBvdXIgd2Vic2l0ZSBhbmQgdGhlIHVzZSBvZiBvdXJcbiAgICAgICAgICAgICAgICAgIHdlYnNpdGUuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAxMS4gTGltaXRhdGlvbnMgYW5kIGV4Y2x1c2lvbnMgb2YgbGlhYmlsaXR5XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiZGlzcGxheVwiPlxuICAgICAgICAgICAgICA8b2w+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgTm90aGluZyBpbiB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9ucyB3aWxsOlxuICAgICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgbGltaXQgb3IgZXhjbHVkZSBhbnkgbGlhYmlsaXR5IGZvciBkZWF0aCBvciBwZXJzb25hbFxuICAgICAgICAgICAgICAgICAgICAgIGluanVyeSByZXN1bHRpbmcgZnJvbSBuZWdsaWdlbmNlO1xuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgbGltaXQgb3IgZXhjbHVkZSBhbnkgbGlhYmlsaXR5IGZvciBmcmF1ZCBvciBmcmF1ZHVsZW50XG4gICAgICAgICAgICAgICAgICAgICAgbWlzcmVwcmVzZW50YXRpb247XG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBsaW1pdCBhbnkgbGlhYmlsaXRpZXMgaW4gYW55IHdheSB0aGF0IGlzIG5vdCBwZXJtaXR0ZWRcbiAgICAgICAgICAgICAgICAgICAgICB1bmRlciBhcHBsaWNhYmxlIGxhdzsgb3JcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIGV4Y2x1ZGUgYW55IGxpYWJpbGl0aWVzIHRoYXQgbWF5IG5vdCBiZSBleGNsdWRlZCB1bmRlclxuICAgICAgICAgICAgICAgICAgICAgIGFwcGxpY2FibGUgbGF3LlxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFRoZSBsaW1pdGF0aW9ucyBhbmQgZXhjbHVzaW9ucyBvZiBsaWFiaWxpdHkgc2V0IG91dCBpbiB0aGlzIFNlY3Rpb24gMTIgYW5kIGVsc2V3aGVyZSBpbiB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9uczpcbiAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPmFyZSBzdWJqZWN0IHRvIFNlY3Rpb24gMTIuMTsgYW5kPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIGdvdmVybiBhbGwgbGlhYmlsaXRpZXMgYXJpc2luZyB1bmRlciB0aGVzZSB0ZXJtcyBhbmRcbiAgICAgICAgICAgICAgICAgICAgICBjb25kaXRpb25zIG9yIHJlbGF0aW5nIHRvIHRoZSBzdWJqZWN0IG1hdHRlciBvZiB0aGVzZVxuICAgICAgICAgICAgICAgICAgICAgIHRlcm1zIGFuZCBjb25kaXRpb25zLCBpbmNsdWRpbmcgbGlhYmlsaXRpZXMgYXJpc2luZyBpblxuICAgICAgICAgICAgICAgICAgICAgIGNvbnRyYWN0LCBpbiB0b3J0IChpbmNsdWRpbmcgbmVnbGlnZW5jZSkgYW5kIGZvclxuICAgICAgICAgICAgICAgICAgICAgIGJyZWFjaCBvZiBzdGF0dXRvcnkgZHV0eSwgZXhjZXB0IHRvIHRoZSBleHRlbnRcbiAgICAgICAgICAgICAgICAgICAgICBleHByZXNzbHkgcHJvdmlkZWQgb3RoZXJ3aXNlIGluIHRoZXNlIHRlcm1zIGFuZFxuICAgICAgICAgICAgICAgICAgICAgIGNvbmRpdGlvbnMuXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgVG8gdGhlIGV4dGVudCB0aGF0IG91ciB3ZWJzaXRlIGFuZCB0aGUgaW5mb3JtYXRpb24gYW5kXG4gICAgICAgICAgICAgICAgICBzZXJ2aWNlcyBvbiBvdXIgd2Vic2l0ZSBhcmUgcHJvdmlkZWQgZnJlZSBvZiBjaGFyZ2UsIHdlXG4gICAgICAgICAgICAgICAgICB3aWxsIG5vdCBiZSBsaWFibGUgZm9yIGFueSBsb3NzIG9yIGRhbWFnZSBvZiBhbnkgbmF0dXJlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgV2Ugd2lsbCBub3QgYmUgbGlhYmxlIHRvIHlvdSBpbiByZXNwZWN0IG9mIGFueSBsb3NzZXNcbiAgICAgICAgICAgICAgICAgIGFyaXNpbmcgb3V0IG9mIGFueSBldmVudCBvciBldmVudHMgYmV5b25kIG91ciByZWFzb25hYmxlXG4gICAgICAgICAgICAgICAgICBjb250cm9sLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgV2Ugd2lsbCBub3QgYmUgbGlhYmxlIHRvIHlvdSBpbiByZXNwZWN0IG9mIGFueSBidXNpbmVzc1xuICAgICAgICAgICAgICAgICAgbG9zc2VzLCBpbmNsdWRpbmcgKHdpdGhvdXQgbGltaXRhdGlvbikgbG9zcyBvZiBvciBkYW1hZ2VcbiAgICAgICAgICAgICAgICAgIHRvIHByb2ZpdHMsIGluY29tZSwgcmV2ZW51ZSwgdXNlLCBwcm9kdWN0aW9uLCBhbnRpY2lwYXRlZFxuICAgICAgICAgICAgICAgICAgc2F2aW5ncywgYnVzaW5lc3MsIGNvbnRyYWN0cywgY29tbWVyY2lhbCBvcHBvcnR1bml0aWVzIG9yXG4gICAgICAgICAgICAgICAgICBnb29kd2lsbC5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFdlIHdpbGwgbm90IGJlIGxpYWJsZSB0byB5b3UgaW4gcmVzcGVjdCBvZiBhbnkgbG9zcyBvclxuICAgICAgICAgICAgICAgICAgY29ycnVwdGlvbiBvZiBhbnkgZGF0YSwgZGF0YWJhc2Ugb3Igc29mdHdhcmUuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBXZSB3aWxsIG5vdCBiZSBsaWFibGUgdG8geW91IGluIHJlc3BlY3Qgb2YgYW55IHNwZWNpYWwsXG4gICAgICAgICAgICAgICAgICBpbmRpcmVjdCBvciBjb25zZXF1ZW50aWFsIGxvc3Mgb3IgZGFtYWdlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IGFjY2VwdCB0aGF0IHdlIGhhdmUgYW4gaW50ZXJlc3QgaW4gbGltaXRpbmcgdGhlXG4gICAgICAgICAgICAgICAgICBwZXJzb25hbCBsaWFiaWxpdHkgb2Ygb3VyIG9mZmljZXJzIGFuZCBlbXBsb3llZXMgYW5kLFxuICAgICAgICAgICAgICAgICAgaGF2aW5nIHJlZ2FyZCB0byB0aGF0IGludGVyZXN0LCB5b3UgYWNrbm93bGVkZ2UgdGhhdCB3ZVxuICAgICAgICAgICAgICAgICAgYXJlIGEgbGltaXRlZCBsaWFiaWxpdHkgZW50aXR5OyB5b3UgYWdyZWUgdGhhdCB5b3Ugd2lsbFxuICAgICAgICAgICAgICAgICAgbm90IGJyaW5nIGFueSBjbGFpbSBwZXJzb25hbGx5IGFnYWluc3Qgb3VyIG9mZmljZXJzIG9yXG4gICAgICAgICAgICAgICAgICBlbXBsb3llZXMgaW4gcmVzcGVjdCBvZiBhbnkgbG9zc2VzIHlvdSBzdWZmZXIgaW5cbiAgICAgICAgICAgICAgICAgIGNvbm5lY3Rpb24gd2l0aCB0aGUgd2Vic2l0ZSBvciB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9uc1xuICAgICAgICAgICAgICAgICAgKHRoaXMgd2lsbCBub3QsIG9mIGNvdXJzZSwgbGltaXQgb3IgZXhjbHVkZSB0aGUgbGlhYmlsaXR5XG4gICAgICAgICAgICAgICAgICBvZiB0aGUgbGltaXRlZCBsaWFiaWxpdHkgZW50aXR5IGl0c2VsZiBmb3IgdGhlIGFjdHMgYW5kXG4gICAgICAgICAgICAgICAgICBvbWlzc2lvbnMgb2Ygb3VyIG9mZmljZXJzIGFuZCBlbXBsb3llZXMpLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAxMi4gQnJlYWNoZXMgb2YgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnNcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBXaXRob3V0IHByZWp1ZGljZSB0byBvdXIgb3RoZXIgcmlnaHRzIHVuZGVyIHRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zLCBpZiB5b3UgYnJlYWNoIHRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zIGluIGFueSB3YXksIG9yIGlmIHdlIHJlYXNvbmFibHkgc3VzcGVjdCB0aGF0IHlvdSBoYXZlIGJyZWFjaGVkIHRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zIGluIGFueSB3YXksIHdlIG1heTpcbiAgICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPnNlbmQgeW91IG9uZSBvciBtb3JlIGZvcm1hbCB3YXJuaW5nczs8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+dGVtcG9yYXJpbHkgc3VzcGVuZCB5b3VyIGFjY2VzcyB0byBvdXIgd2Vic2l0ZTs8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgcGVybWFuZW50bHkgcHJvaGliaXQgeW91IGZyb20gYWNjZXNzaW5nIG91ciB3ZWJzaXRlO1xuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgW2Jsb2NrIGNvbXB1dGVycyB1c2luZyB5b3VyIElQIGFkZHJlc3MgZnJvbSBhY2Nlc3NpbmdcbiAgICAgICAgICAgICAgICAgICAgICBvdXIgd2Vic2l0ZV07XG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBbY29udGFjdCBhbnkgb3IgYWxsIG9mIHlvdXIgaW50ZXJuZXQgc2VydmljZSBwcm92aWRlcnNcbiAgICAgICAgICAgICAgICAgICAgICBhbmQgcmVxdWVzdCB0aGF0IHRoZXkgYmxvY2sgeW91ciBhY2Nlc3MgdG8gb3VyXG4gICAgICAgICAgICAgICAgICAgICAgd2Vic2l0ZV07XG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBjb21tZW5jZSBsZWdhbCBhY3Rpb24gYWdhaW5zdCB5b3UsIHdoZXRoZXIgZm9yIGJyZWFjaFxuICAgICAgICAgICAgICAgICAgICAgIG9mIGNvbnRyYWN0IG9yIG90aGVyd2lzZTsgYW5kL29yXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBbc3VzcGVuZCBvciBkZWxldGUgeW91ciBhY2NvdW50IG9uIG91ciB3ZWJzaXRlXS5cbiAgICAgICAgICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgICAgICAgICBbYWRkaXRpb25hbCBsaXN0IGl0ZW1zXVxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFdoZXJlIHdlIHN1c3BlbmQgb3IgcHJvaGliaXQgb3IgYmxvY2sgeW91ciBhY2Nlc3MgdG8gb3VyXG4gICAgICAgICAgICAgICAgICB3ZWJzaXRlIG9yIGEgcGFydCBvZiBvdXIgd2Vic2l0ZSwgeW91IG11c3Qgbm90IHRha2UgYW55XG4gICAgICAgICAgICAgICAgICBhY3Rpb24gdG8gY2lyY3VtdmVudCBzdWNoIHN1c3BlbnNpb24gb3IgcHJvaGliaXRpb24gb3JcbiAgICAgICAgICAgICAgICAgIGJsb2NraW5nWyAoaW5jbHVkaW5nIHdpdGhvdXQgbGltaXRhdGlvbiBbY3JlYXRpbmcgYW5kL29yXG4gICAgICAgICAgICAgICAgICB1c2luZyBhIGRpZmZlcmVudCBhY2NvdW50XSldLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgMTMuIFZhcmlhdGlvblxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPG9sPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFdlIG1heSByZXZpc2UgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnMgZnJvbSB0aW1lIHRvXG4gICAgICAgICAgICAgICAgICB0aW1lLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgW0hiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIG1heSByZXZpc2VcbiAgICAgICAgICAgICAgICAgIHRoZXNlIHRlcm1zIG9mIHNlcnZpY2UgZm9yIGl0cyB3ZWJzaXRlIGF0IGFueSB0aW1lIHdpdGhvdXRcbiAgICAgICAgICAgICAgICAgIG5vdGljZS4gQnkgdXNpbmcgdGhpcyB3ZWJzaXRlIHlvdSBhcmUgYWdyZWVpbmcgdG8gYmUgYm91bmRcbiAgICAgICAgICAgICAgICAgIGJ5IHRoZSB0aGVuIGN1cnJlbnQgdmVyc2lvbiBvZiB0aGVzZSB0ZXJtcyBvZiBzZXJ2aWNlLl1cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L29sPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgMTQuIEFzc2lnbm1lbnRcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3UgaGVyZWJ5IGFncmVlIHRoYXQgd2UgbWF5IGFzc2lnbiwgdHJhbnNmZXIsXG4gICAgICAgICAgICAgICAgICBzdWItY29udHJhY3Qgb3Igb3RoZXJ3aXNlIGRlYWwgd2l0aCBvdXIgcmlnaHRzIGFuZC9vclxuICAgICAgICAgICAgICAgICAgb2JsaWdhdGlvbnMgdW5kZXIgdGhlc2UgdGVybXMgYW5kIGNvbmRpdGlvbnMuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBZb3UgbWF5IG5vdCB3aXRob3V0IG91ciBwcmlvciB3cml0dGVuIGNvbnNlbnQgYXNzaWduLFxuICAgICAgICAgICAgICAgICAgdHJhbnNmZXIsIHN1Yi1jb250cmFjdCBvciBvdGhlcndpc2UgZGVhbCB3aXRoIGFueSBvZiB5b3VyXG4gICAgICAgICAgICAgICAgICByaWdodHMgYW5kL29yIG9ibGlnYXRpb25zIHVuZGVyIHRoZXNlIHRlcm1zIGFuZFxuICAgICAgICAgICAgICAgICAgY29uZGl0aW9ucy5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L29sPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgMTUuIFNldmVyYWJpbGl0eVxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPG9sPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIElmIGEgcHJvdmlzaW9uIG9mIHRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zIGlzIGRldGVybWluZWRcbiAgICAgICAgICAgICAgICAgIGJ5IGFueSBjb3VydCBvciBvdGhlciBjb21wZXRlbnQgYXV0aG9yaXR5IHRvIGJlIHVubGF3ZnVsXG4gICAgICAgICAgICAgICAgICBhbmQvb3IgdW5lbmZvcmNlYWJsZSwgdGhlIG90aGVyIHByb3Zpc2lvbnMgd2lsbCBjb250aW51ZVxuICAgICAgICAgICAgICAgICAgaW4gZWZmZWN0LlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgSWYgYW55IHVubGF3ZnVsIGFuZC9vciB1bmVuZm9yY2VhYmxlIHByb3Zpc2lvbiBvZiB0aGVzZVxuICAgICAgICAgICAgICAgICAgdGVybXMgYW5kIGNvbmRpdGlvbnMgd291bGQgYmUgbGF3ZnVsIG9yIGVuZm9yY2VhYmxlIGlmXG4gICAgICAgICAgICAgICAgICBwYXJ0IG9mIGl0IHdlcmUgZGVsZXRlZCwgdGhhdCBwYXJ0IHdpbGwgYmUgZGVlbWVkIHRvIGJlXG4gICAgICAgICAgICAgICAgICBkZWxldGVkLCBhbmQgdGhlIHJlc3Qgb2YgdGhlIHByb3Zpc2lvbiB3aWxsIGNvbnRpbnVlIGluXG4gICAgICAgICAgICAgICAgICBlZmZlY3QuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cblxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDE2LiBUaGlyZCBwYXJ0eSByaWdodHNcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBBIGNvbnRyYWN0IHVuZGVyIHRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zIGlzIGZvciBvdXJcbiAgICAgICAgICAgICAgICAgIGJlbmVmaXQgYW5kIHlvdXIgYmVuZWZpdCwgYW5kIGlzIG5vdCBpbnRlbmRlZCB0byBiZW5lZml0XG4gICAgICAgICAgICAgICAgICBvciBiZSBlbmZvcmNlYWJsZSBieSBhbnkgdGhpcmQgcGFydHkuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBUaGUgZXhlcmNpc2Ugb2YgdGhlIHBhcnRpZXMnIHJpZ2h0cyB1bmRlciBhIGNvbnRyYWN0IHVuZGVyXG4gICAgICAgICAgICAgICAgICB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9ucyBpcyBub3Qgc3ViamVjdCB0byB0aGUgY29uc2VudFxuICAgICAgICAgICAgICAgICAgb2YgYW55IHRoaXJkIHBhcnR5LlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAxNy4gRW50aXJlIGFncmVlbWVudFxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPG9sPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFN1YmplY3QgdG8gU2VjdGlvbiAxMi4xLCB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9uc1ssXG4gICAgICAgICAgICAgICAgICB0b2dldGhlciB3aXRoIFtvdXIgcHJpdmFjeSBhbmQgY29va2llcyBwb2xpY3ldLF0gc2hhbGxcbiAgICAgICAgICAgICAgICAgIGNvbnN0aXR1dGUgdGhlIGVudGlyZSBhZ3JlZW1lbnQgYmV0d2VlbiB5b3UgYW5kIHVzIGluXG4gICAgICAgICAgICAgICAgICByZWxhdGlvbiB0byB5b3VyIHVzZSBvZiBvdXIgd2Vic2l0ZSBhbmQgc2hhbGwgc3VwZXJzZWRlXG4gICAgICAgICAgICAgICAgICBhbGwgcHJldmlvdXMgYWdyZWVtZW50cyBiZXR3ZWVuIHlvdSBhbmQgdXMgaW4gcmVsYXRpb24gdG9cbiAgICAgICAgICAgICAgICAgIHlvdXIgdXNlIG9mIG91ciB3ZWJzaXRlLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAxOC4gTGF3IGFuZCBqdXJpc2RpY3Rpb25cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBUaGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9ucyBzaGFsbCBiZSBnb3Zlcm5lZCBieSBhbmRcbiAgICAgICAgICAgICAgICAgIGNvbnN0cnVlZCBpbiBhY2NvcmRhbmNlIHdpdGggSW5kaWFuIGxhdy5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIEFueSBkaXNwdXRlcyByZWxhdGluZyB0byB0aGVzZSB0ZXJtcyBhbmQgY29uZGl0aW9ucyBzaGFsbFxuICAgICAgICAgICAgICAgICAgYmUgc3ViamVjdCB0byB0aGUganVyaXNkaWN0aW9uIG9mIHRoZSBjb3VydHMgb2YgSW5kaWEuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cblxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDE5LiBPdXIgZGV0YWlsc1xuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPG9sPlxuICAgICAgICAgICAgICAgIDxsaT5UaGlzIHdlYnNpdGUgaXMgb3duZWQgYW5kIG9wZXJhdGVkIGJ5IEhvbmV5IEJhcnZlLjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgV2UgYXJlIHJlZ2lzdGVyZWQgaW4gWyBJbmRpYSBdIHVuZGVyIHJlZ2lzdHJhdGlvbiBudW1iZXJcbiAgICAgICAgICAgICAgICAgIFtudW1iZXJdLCBhbmQgb3VyIHJlZ2lzdGVyZWQgb2ZmaWNlIGlzIGF0IFtSYW5jaGldLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPk91ciBwcmluY2lwYWwgcGxhY2Ugb2YgYnVzaW5lc3MgaXMgYXQgW0d3YWxpb3JdLjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgWW91IGNhbiBjb250YWN0IHVzOlxuICAgICAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgaHR0cHM6Ly9kb2NzLmdvb2dsZS5jb20vZm9ybXMvZC9lLzFGQUlwUUxTY1JxcVpfblJGOUFoQ0JNNzNxRll2VngwTDJTd0ZUdHY2dDlSeHFLSjR6eHItbDB3L3ZpZXdmb3JtXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5Db250YWN0IE51bWJlcjotIDg3NzA2OTM2NDQ8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+RSBtYWlsOi0gY29udGFjdC11c0BtYXlhc2guaW88L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+V2Vic2l0ZTotIGh0dHBzOi8vTWF5YXNoLmlvLjwvbGk+XG4gICAgICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgIHsvKlxuICAgICAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPVwiVEVSTVMgT0YgU0VSVklDRVwiIHN1YmhlYWRlcj17J0hiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIFRlcm1zIG9mIFNlcnZpY2UnfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZEhlYWRlcn0gLz5cbiAgICAgICAgICA8Q2FyZENvbnRlbnQgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmRDb250ZW50fT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAxLiBUZXJtc1xuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgQnkgYWNjZXNzaW5nIHRoZSB3ZWJzaXRlIGF0IGh0dHBzOi8vbWF5YXNoLmlvLCB5b3UgYXJlIGFncmVlaW5nIHRvIGJlIGJvdW5kIGJ5IHRoZXNlIHRlcm1zIG9mIHNlcnZpY2UsIGFsbCBhcHBsaWNhYmxlIGxhd3MgYW5kIHJlZ3VsYXRpb25zLCBhbmQgYWdyZWUgdGhhdCB5b3UgYXJlIHJlc3BvbnNpYmxlIGZvciBjb21wbGlhbmNlIHdpdGggYW55IGFwcGxpY2FibGUgbG9jYWwgbGF3cy4gSWYgeW91IGRvIG5vdCBhZ3JlZSB3aXRoIGFueSBvZiB0aGVzZSB0ZXJtcywgeW91IGFyZSBwcm9oaWJpdGVkIGZyb20gdXNpbmcgb3IgYWNjZXNzaW5nIHRoaXMgc2l0ZS4gVGhlIG1hdGVyaWFscyBjb250YWluZWQgaW4gdGhpcyB3ZWJzaXRlIGFyZSBwcm90ZWN0ZWQgYnkgYXBwbGljYWJsZSB0cmFkZW1hcmsgbGF3LlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgMi4gVXNlIExpY2Vuc2VcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICA8b2w+XG4gICAgICAgICAgICAgICAgICAgIFBlcm1pc3Npb24gaXMgZ3JhbnRlZCB0byB0ZW1wb3JhcmlseSBkb3dubG9hZCBvbmUgY29weSBvZiB0aGUgbWF0ZXJpYWxzIChpbmZvcm1hdGlvbiBvciBzb2Z0d2FyZSkgb24gSGJhcnZlIHRlY2hub2xvZ2llcyAoT1BDKSBwcml2YXRlIGxpbWl0ZWQncyB3ZWJzaXRlIGZvciBwZXJzb25hbCwgbm9uLWNvbW1lcmNpYWwgdHJhbnNpdG9yeSB2aWV3aW5nIG9ubHkuIFRoaXMgaXMgdGhlIGdyYW50IG9mIGEgbGljZW5zZSwgbm90IGEgdHJhbnNmZXIgb2YgdGl0bGUsIGFuZCB1bmRlciB0aGlzIGxpY2Vuc2UgeW91IG1heSBub3Q6XG4gICAgICAgICAgICAgICAgICAgIDxsaT5tb2RpZnkgb3IgY29weSB0aGUgbWF0ZXJpYWxzOzwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICB1c2UgdGhlIG1hdGVyaWFscyBmb3IgYW55IGNvbW1lcmNpYWwgcHVycG9zZSwgb3IgZm9yXG4gICAgICAgICAgICAgICAgICAgICAgYW55IHB1YmxpYyBkaXNwbGF5IChjb21tZXJjaWFsIG9yIG5vbi1jb21tZXJjaWFsKTtcbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgICAgIGF0dGVtcHQgdG8gZGVjb21waWxlIG9yIHJldmVyc2UgZW5naW5lZXIgYW55IHNvZnR3YXJlXG4gICAgICAgICAgICAgICAgICAgICAgY29udGFpbmVkIG9uIEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZVxuICAgICAgICAgICAgICAgICAgICAgIGxpbWl0ZWQncyB3ZWJzaXRlO1xuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgcmVtb3ZlIGFueSBjb3B5cmlnaHQgb3Igb3RoZXIgcHJvcHJpZXRhcnkgbm90YXRpb25zXG4gICAgICAgICAgICAgICAgICAgICAgZnJvbSB0aGUgbWF0ZXJpYWxzOyBvclxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICAgICAgdHJhbnNmZXIgdGhlIG1hdGVyaWFscyB0byBhbm90aGVyIHBlcnNvbiBvciBcIm1pcnJvclwiXG4gICAgICAgICAgICAgICAgICAgICAgdGhlIG1hdGVyaWFscyBvbiBhbnkgb3RoZXIgc2VydmVyLlxuICAgICAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFRoaXMgbGljZW5zZSBzaGFsbCBhdXRvbWF0aWNhbGx5IHRlcm1pbmF0ZSBpZiB5b3UgdmlvbGF0ZVxuICAgICAgICAgICAgICAgICAgYW55IG9mIHRoZXNlIHJlc3RyaWN0aW9ucyBhbmQgbWF5IGJlIHRlcm1pbmF0ZWQgYnkgSGJhcnZlXG4gICAgICAgICAgICAgICAgICB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIGF0IGFueSB0aW1lLiBVcG9uXG4gICAgICAgICAgICAgICAgICB0ZXJtaW5hdGluZyB5b3VyIHZpZXdpbmcgb2YgdGhlc2UgbWF0ZXJpYWxzIG9yIHVwb24gdGhlXG4gICAgICAgICAgICAgICAgICB0ZXJtaW5hdGlvbiBvZiB0aGlzIGxpY2Vuc2UsIHlvdSBtdXN0IGRlc3Ryb3kgYW55XG4gICAgICAgICAgICAgICAgICBkb3dubG9hZGVkIG1hdGVyaWFscyBpbiB5b3VyIHBvc3Nlc3Npb24gd2hldGhlciBpblxuICAgICAgICAgICAgICAgICAgZWxlY3Ryb25pYyBvciBwcmludGVkIGZvcm1hdC5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgMy4gRGlzY2xhaW1lclxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFRoZSBtYXRlcmlhbHMgb24gSGJhcnZlIHRlY2hub2xvZ2llcyAoT1BDKSBwcml2YXRlXG4gICAgICAgICAgICAgICAgICBsaW1pdGVkJ3Mgd2Vic2l0ZSBhcmUgcHJvdmlkZWQgb24gYW4gJ2FzIGlzJyBiYXNpcy4gSGJhcnZlXG4gICAgICAgICAgICAgICAgICB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIG1ha2VzIG5vIHdhcnJhbnRpZXMsXG4gICAgICAgICAgICAgICAgICBleHByZXNzZWQgb3IgaW1wbGllZCwgYW5kIGhlcmVieSBkaXNjbGFpbXMgYW5kIG5lZ2F0ZXMgYWxsXG4gICAgICAgICAgICAgICAgICBvdGhlciB3YXJyYW50aWVzIGluY2x1ZGluZywgd2l0aG91dCBsaW1pdGF0aW9uLCBpbXBsaWVkXG4gICAgICAgICAgICAgICAgICB3YXJyYW50aWVzIG9yIGNvbmRpdGlvbnMgb2YgbWVyY2hhbnRhYmlsaXR5LCBmaXRuZXNzIGZvciBhXG4gICAgICAgICAgICAgICAgICBwYXJ0aWN1bGFyIHB1cnBvc2UsIG9yIG5vbi1pbmZyaW5nZW1lbnQgb2YgaW50ZWxsZWN0dWFsXG4gICAgICAgICAgICAgICAgICBwcm9wZXJ0eSBvciBvdGhlciB2aW9sYXRpb24gb2YgcmlnaHRzLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgRnVydGhlciwgSGJhcnZlIHRlY2hub2xvZ2llcyAoT1BDKSBwcml2YXRlIGxpbWl0ZWQgZG9lc1xuICAgICAgICAgICAgICAgICAgbm90IHdhcnJhbnQgb3IgbWFrZSBhbnkgcmVwcmVzZW50YXRpb25zIGNvbmNlcm5pbmcgdGhlXG4gICAgICAgICAgICAgICAgICBhY2N1cmFjeSwgbGlrZWx5IHJlc3VsdHMsIG9yIHJlbGlhYmlsaXR5IG9mIHRoZSB1c2Ugb2YgdGhlXG4gICAgICAgICAgICAgICAgICBtYXRlcmlhbHMgb24gaXRzIHdlYnNpdGUgb3Igb3RoZXJ3aXNlIHJlbGF0aW5nIHRvIHN1Y2hcbiAgICAgICAgICAgICAgICAgIG1hdGVyaWFscyBvciBvbiBhbnkgc2l0ZXMgbGlua2VkIHRvIHRoaXMgc2l0ZS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICA8L3VsPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgNC4gTGltaXRhdGlvbnNcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIEluIG5vIGV2ZW50IHNoYWxsIEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIG9yIGl0cyBzdXBwbGllcnMgYmUgbGlhYmxlIGZvciBhbnkgZGFtYWdlcyAoaW5jbHVkaW5nLCB3aXRob3V0IGxpbWl0YXRpb24sIGRhbWFnZXMgZm9yIGxvc3Mgb2YgZGF0YSBvciBwcm9maXQsIG9yIGR1ZSB0byBidXNpbmVzcyBpbnRlcnJ1cHRpb24pIGFyaXNpbmcgb3V0IG9mIHRoZSB1c2Ugb3IgaW5hYmlsaXR5IHRvIHVzZSB0aGUgbWF0ZXJpYWxzIG9uIEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkJ3Mgd2Vic2l0ZSwgZXZlbiBpZiBIYmFydmUgdGVjaG5vbG9naWVzIChPUEMpIHByaXZhdGUgbGltaXRlZCBvciBhIEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIGF1dGhvcml6ZWQgcmVwcmVzZW50YXRpdmUgaGFzIGJlZW4gbm90aWZpZWQgb3JhbGx5IG9yIGluIHdyaXRpbmcgb2YgdGhlIHBvc3NpYmlsaXR5IG9mIHN1Y2ggZGFtYWdlLiBCZWNhdXNlIHNvbWUganVyaXNkaWN0aW9ucyBkbyBub3QgYWxsb3cgbGltaXRhdGlvbnMgb24gaW1wbGllZCB3YXJyYW50aWVzLCBvciBsaW1pdGF0aW9ucyBvZiBsaWFiaWxpdHkgZm9yIGNvbnNlcXVlbnRpYWwgb3IgaW5jaWRlbnRhbCBkYW1hZ2VzLCB0aGVzZSBsaW1pdGF0aW9ucyBtYXkgbm90IGFwcGx5IHRvIHlvdS5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cblxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDUuIEFjY3VyYWN5IG9mIG1hdGVyaWFsc1xuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImRpc3BsYXlcIj5cbiAgICAgICAgICAgICAgVGhlIG1hdGVyaWFscyBhcHBlYXJpbmcgb24gSGJhcnZlIHRlY2hub2xvZ2llcyAoT1BDKSBwcml2YXRlIGxpbWl0ZWQgd2Vic2l0ZSBjb3VsZCBpbmNsdWRlIHRlY2huaWNhbCwgdHlwb2dyYXBoaWNhbCwgb3IgcGhvdG9ncmFwaGljIGVycm9ycy4gSGJhcnZlIHRlY2hub2xvZ2llcyAoT1BDKSBwcml2YXRlIGxpbWl0ZWQgZG9lcyBub3Qgd2FycmFudCB0aGF0IGFueSBvZiB0aGUgbWF0ZXJpYWxzIG9uIGl0cyB3ZWJzaXRlIGFyZSBhY2N1cmF0ZSwgY29tcGxldGUgb3IgY3VycmVudC4gSGJhcnZlIHRlY2hub2xvZ2llcyAoT1BDKSBwcml2YXRlIGxpbWl0ZWQgbWF5IG1ha2UgY2hhbmdlcyB0byB0aGUgbWF0ZXJpYWxzIGNvbnRhaW5lZCBvbiBpdHMgd2Vic2l0ZSBhdCBhbnkgdGltZSB3aXRob3V0IG5vdGljZS4gSG93ZXZlciBIYmFydmUgdGVjaG5vbG9naWVzIChPUEMpIHByaXZhdGUgbGltaXRlZCBkb2VzIG5vdCBtYWtlIGFueSBjb21taXRtZW50IHRvIHVwZGF0ZSB0aGUgbWF0ZXJpYWxzLlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuXG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiaGVhZGxpbmVcIiB0eXBlPVwiaGVhZGxpbmVcIj5cbiAgICAgICAgICAgICAgNi4gTGlua3NcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIGhhcyBub3QgcmV2aWV3ZWQgYWxsIG9mIHRoZSBzaXRlcyBsaW5rZWQgdG8gaXRzIHdlYnNpdGUgYW5kIGlzIG5vdCByZXNwb25zaWJsZSBmb3IgdGhlIGNvbnRlbnRzIG9mIGFueSBzdWNoIGxpbmtlZCBzaXRlLiBUaGUgaW5jbHVzaW9uIG9mIGFueSBsaW5rIGRvZXMgbm90IGltcGx5IGVuZG9yc2VtZW50IGJ5IEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIG9mIHRoZSBzaXRlLiBVc2Ugb2YgYW55IHN1Y2ggbGlua2VkIHdlYnNpdGUgaXMgYXQgdGhlIHVzZXIncyBvd24gcmlzay5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cblxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDcuIE1vZGlmaWNhdGlvbnNcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIEhiYXJ2ZSB0ZWNobm9sb2dpZXMgKE9QQykgcHJpdmF0ZSBsaW1pdGVkIG1heSByZXZpc2UgdGhlc2UgdGVybXMgb2Ygc2VydmljZSBmb3IgaXRzIHdlYnNpdGUgYXQgYW55IHRpbWUgd2l0aG91dCBub3RpY2UuIEJ5IHVzaW5nIHRoaXMgd2Vic2l0ZSB5b3UgYXJlIGFncmVlaW5nIHRvIGJlIGJvdW5kIGJ5IHRoZSB0aGVuIGN1cnJlbnQgdmVyc2lvbiBvZiB0aGVzZSB0ZXJtcyBvZiBzZXJ2aWNlLlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIDguIEdvdmVybmluZyBMYXdcbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJkaXNwbGF5XCI+XG4gICAgICAgICAgICAgIFRoZXNlIHRlcm1zIGFuZCBjb25kaXRpb25zIGFyZSBnb3Zlcm5lZCBieSBhbmQgY29uc3RydWVkIGluIGFjY29yZGFuY2Ugd2l0aCB0aGUgbGF3cyBvZiBJbmRpYSBhbmQgeW91IGlycmV2b2NhYmx5IHN1Ym1pdCB0byB0aGUgZXhjbHVzaXZlIGp1cmlzZGljdGlvbiBvZiB0aGUgY291cnRzIGluIHRoYXQgU3RhdGUgb3IgbG9jYXRpb24uXG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAqL31cbiAgICAgICAgPC9DYXJkPlxuICAgICAgICA8TmF2aWdhdGlvbkJ1dHRvbk5leHQgdG89eycvZGV2ZWxvcGVycy10cmFpbmluZyd9IC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblRlbXJzLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFRlbXJzKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvcGFnZXMvVGVybXMuanMiXSwic291cmNlUm9vdCI6IiJ9