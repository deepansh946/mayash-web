webpackJsonp([68],{

/***/ "./src/client/actions/courses/users/discussion/answers/create.js":
/*!***********************************************************************!*\
  !*** ./src/client/actions/courses/users/discussion/answers/create.js ***!
  \***********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../../../constants/courses */ "./src/client/constants/courses.js");

/**
 * This action will create an answer in discussion of course's question.
 * @function create
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId - author of a answer.
 * @param {number} payload.questionId -
 * @param {string} payload.title -
 * @param {string} payload.timestamp -
 * @returns {Object}
 */
var create = function create(payload) {
  return {
    type: _courses.ANSWER_CREATE,
    payload: payload
  };
}; /**
    * Create a question in Course Discussion.
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/api/courses/users/discussion/answers/create.js":
/*!*******************************************************************!*\
  !*** ./src/client/api/courses/users/discussion/answers/create.js ***!
  \*******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Create
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @param {string} payload.title -
 * @return {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId,
        questionId = _ref2.questionId,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId + '/discussion/' + questionId + '/answers';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/CoursePage/Discussion/AnswerCreate.js":
/*!*********************************************************************!*\
  !*** ./src/client/containers/CoursePage/Discussion/AnswerCreate.js ***!
  \*********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _CardHeader = __webpack_require__(/*! material-ui/Card/CardHeader */ "./node_modules/material-ui/Card/CardHeader.js");

var _CardHeader2 = _interopRequireDefault(_CardHeader);

var _CardContent = __webpack_require__(/*! material-ui/Card/CardContent */ "./node_modules/material-ui/Card/CardContent.js");

var _CardContent2 = _interopRequireDefault(_CardContent);

var _CardActions = __webpack_require__(/*! material-ui/Card/CardActions */ "./node_modules/material-ui/Card/CardActions.js");

var _CardActions2 = _interopRequireDefault(_CardActions);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Input = __webpack_require__(/*! ../../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../../api/courses/users/discussion/answers/create */ "./src/client/api/courses/users/discussion/answers/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../../actions/courses/users/discussion/answers/create */ "./src/client/actions/courses/users/discussion/answers/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Define all styles here for PostCreate Component.
 */
/**
 * PostCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create post for
 * their profile only.
 *
 * @format
 */

var styles = {
  root: {
    padding: '1%'
  },
  gridContainer: {},
  post: {
    padding: '1%'
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

var AnswerCreate = function (_Component) {
  (0, _inherits3.default)(AnswerCreate, _Component);

  function AnswerCreate(props) {
    (0, _classCallCheck3.default)(this, AnswerCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (AnswerCreate.__proto__ || (0, _getPrototypeOf2.default)(AnswerCreate)).call(this, props));

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,

      placeholder: 'Answer Title...',
      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(AnswerCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      var title = e.target.value;
      var titleLength = title.length;
      var valid = titleLength > 0 && titleLength < 148;

      this.setState({
        title: title,
        titleLength: titleLength,
        valid: valid,
        error: !valid ? 'Title length should be in between 1 to 148.' : ''
      });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props, user, course, questionId, userId, token, courseId, title, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, user = _props.user, course = _props.course, questionId = _props.questionId;
                userId = user.id, token = user.token;
                courseId = course.courseId;
                title = this.state.title;


                this.setState({ message: 'Creating Answer...' });

                _context.next = 8;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  questionId: questionId,
                  title: title
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 15;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 15:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionCreate((0, _extends3.default)({
                  statusCode: statusCode,
                  courseId: courseId,
                  questionId: questionId,
                  authorId: userId,
                  title: title
                }, payload));
                _context.next = 23;
                break;

              case 20:
                _context.prev = 20;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 23:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 20]]);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _state = this.state,
          active = _state.active,
          placeholder = _state.placeholder,
          title = _state.title,
          message = _state.message;
      var _props2 = this.props,
          classes = _props2.classes,
          user = _props2.user;
      var name = user.name,
          avatar = user.avatar;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(_CardHeader2.default, {
          avatar: _react2.default.createElement(_Avatar2.default, {
            alt: name,
            src: avatar || '/favicon.ico',
            className: classes.avatar
          }),
          title: _react2.default.createElement(_Input2.default, {
            value: title,
            onChange: this.onChange,
            placeholder: placeholder
          }),
          className: classes.cardHeader,
          onFocus: function onFocus() {
            return _this3.setState({ active: true });
          }
        }),
        active && message.length ? _react2.default.createElement(
          _CardContent2.default,
          null,
          _react2.default.createElement(
            _Typography2.default,
            null,
            message
          )
        ) : null,
        active ? _react2.default.createElement(
          _CardActions2.default,
          { disableActionSpacing: true },
          _react2.default.createElement('div', { className: classes.flexGrow }),
          _react2.default.createElement(
            _Button2.default,
            { raised: true, color: 'accent', onClick: this.onSubmit },
            'Create'
          )
        ) : null
      );
    }
  }]);
  return AnswerCreate;
}(_react.Component);

AnswerCreate.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired,
  questionId: _propTypes2.default.number.isRequired,

  actionCreate: _propTypes2.default.func.isRequired
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionCreate: _create4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(null, mapDispatchToProps)((0, _withStyles2.default)(styles)(AnswerCreate));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy91c2Vycy9kaXNjdXNzaW9uL2Fuc3dlcnMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvZGlzY3Vzc2lvbi9hbnN3ZXJzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvRGlzY3Vzc2lvbi9BbnN3ZXJDcmVhdGUuanMiXSwibmFtZXMiOlsiY3JlYXRlIiwicGF5bG9hZCIsInR5cGUiLCJ0b2tlbiIsInVzZXJJZCIsImNvdXJzZUlkIiwicXVlc3Rpb25JZCIsInRpdGxlIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJib2R5IiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInN0YXR1c0NvZGUiLCJlcnJvciIsImpzb24iLCJjb25zb2xlIiwic3R5bGVzIiwicm9vdCIsImRpc3BsYXkiLCJpbnB1dCIsImZsZXhHcm93IiwiYm9yZGVyIiwiYm9yZGVyUmFkaXVzIiwicGFkZGluZyIsIm91dGxpbmUiLCJib3JkZXJDb2xvciIsImJveFNoYWRvdyIsIklucHV0IiwiY2xhc3NlcyIsIm9uQ2hhbmdlIiwidmFsdWUiLCJwbGFjZWhvbGRlciIsImRpc2FibGVkIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJvbmVPZlR5cGUiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiZ3JpZENvbnRhaW5lciIsInBvc3QiLCJjYXJkSGVhZGVyIiwiY2FyZCIsImF2YXRhciIsImNhcmRNZWRpYSIsImNhcmRNZWRpYUltYWdlIiwid2lkdGgiLCJmbGV4IiwiQW5zd2VyQ3JlYXRlIiwicHJvcHMiLCJzdGF0ZSIsImFjdGl2ZSIsInRpdGxlTGVuZ3RoIiwidmFsaWQiLCJtZXNzYWdlIiwiaG92ZXIiLCJiaW5kIiwib25TdWJtaXQiLCJlIiwidGFyZ2V0IiwibGVuZ3RoIiwic2V0U3RhdGUiLCJ1c2VyIiwiY291cnNlIiwiaWQiLCJzZXRUaW1lb3V0IiwiYWN0aW9uQ3JlYXRlIiwiYXV0aG9ySWQiLCJuYW1lIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBTUE7O0FBZUE7Ozs7Ozs7Ozs7O0FBV0EsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQjtBQUM1Q0MsZ0NBRDRDO0FBRTVDRDtBQUY0QyxHQUEvQjtBQUFBLENBQWYsQyxDQWhDQTs7Ozs7O2tCQXFDZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyQmY7Ozs7Ozs7Ozs7Ozs7O0FBaEJBOzs7Ozs7c0ZBOEJBO0FBQUEsUUFBd0JHLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxNQUEvQixTQUErQkEsTUFBL0I7QUFBQSxRQUF1Q0MsUUFBdkMsU0FBdUNBLFFBQXZDO0FBQUEsUUFBaURDLFVBQWpELFNBQWlEQSxVQUFqRDtBQUFBLFFBQTZEQyxLQUE3RCxTQUE2REEsS0FBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNKLE1BRnJDLGlCQUV1REMsUUFGdkQsb0JBRThFQyxVQUY5RTtBQUFBO0FBQUEsbUJBSXNCLCtCQUFNRSxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLE1BRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVQ7QUFIUixlQUZrQjtBQU8zQlUsb0JBQU0seUJBQWUsRUFBRU4sWUFBRixFQUFmO0FBUHFCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVU8sZUFKVjtBQWNZQyxrQkFkWixHQWNtQ0QsR0FkbkMsQ0FjWUMsTUFkWixFQWNvQkMsVUFkcEIsR0FjbUNGLEdBZG5DLENBY29CRSxVQWRwQjs7QUFBQSxrQkFnQlFELFVBQVUsR0FoQmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWlCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWpCYjs7QUFBQTtBQUFBO0FBQUEsbUJBdUJ1QkYsSUFBSUssSUFBSixFQXZCdkI7O0FBQUE7QUF1QlVBLGdCQXZCVjtBQUFBLHdFQXlCZ0JBLElBekJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMkJJQyxvQkFBUUYsS0FBUjs7QUEzQkosNkNBNkJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTdCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWxCLE07Ozs7O0FBekJmOzs7O0FBQ0E7Ozs7a0JBNERlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0RmOzs7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTXFCLFNBQVM7QUFDYkMsUUFBTTtBQUNKQyxhQUFTO0FBREwsR0FETztBQUliQyxTQUFPO0FBQ0xDLGNBQVUsR0FETDtBQUVMQyxZQUFRLG1CQUZIO0FBR0xDLGtCQUFjLEtBSFQ7QUFJTEMsYUFBUyxLQUpKO0FBS0wsZUFBVztBQUNUQyxlQUFTLE1BREE7QUFFVEMsbUJBQWEsU0FGSjtBQUdUQyxpQkFBVztBQUhGO0FBTE47QUFKTSxDQUFmLEMsQ0FaQTs7Ozs7OztBQTZCQSxJQUFNQyxRQUFRLFNBQVJBLEtBQVE7QUFBQSxNQUFHQyxPQUFILFFBQUdBLE9BQUg7QUFBQSxNQUFZQyxRQUFaLFFBQVlBLFFBQVo7QUFBQSxNQUFzQkMsS0FBdEIsUUFBc0JBLEtBQXRCO0FBQUEsTUFBNkJDLFdBQTdCLFFBQTZCQSxXQUE3QjtBQUFBLE1BQTBDQyxRQUExQyxRQUEwQ0EsUUFBMUM7QUFBQSxNQUFvRG5DLElBQXBELFFBQW9EQSxJQUFwRDtBQUFBLFNBQ1o7QUFBQTtBQUFBLE1BQUssV0FBVytCLFFBQVFYLElBQXhCO0FBQ0U7QUFDRSxZQUFNcEIsUUFBUSxNQURoQjtBQUVFLG1CQUFha0MsZUFBZSxPQUY5QjtBQUdFLGFBQU9ELEtBSFQ7QUFJRSxnQkFBVUQsUUFKWjtBQUtFLGlCQUFXRCxRQUFRVCxLQUxyQjtBQU1FLGdCQUFVLENBQUMsQ0FBQ2E7QUFOZDtBQURGLEdBRFk7QUFBQSxDQUFkOztBQWFBTCxNQUFNTSxTQUFOLEdBQWtCO0FBQ2hCTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQURWO0FBRWhCTixZQUFVLG9CQUFVTyxJQUFWLENBQWVELFVBRlQ7QUFHaEJMLFNBQU8sb0JBQVVPLFNBQVYsQ0FBb0IsQ0FBQyxvQkFBVUMsTUFBWCxFQUFtQixvQkFBVUMsTUFBN0IsQ0FBcEIsRUFBMERKLFVBSGpEO0FBSWhCSixlQUFhLG9CQUFVTyxNQUpQO0FBS2hCekMsUUFBTSxvQkFBVXlDLE1BTEE7QUFNaEJOLFlBQVUsb0JBQVVRO0FBTkosQ0FBbEI7O2tCQVNlLDBCQUFXeEIsTUFBWCxFQUFtQlcsS0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeENmOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUE7OztBQTdCQTs7Ozs7Ozs7Ozs7QUFnQ0EsSUFBTVgsU0FBUztBQUNiQyxRQUFNO0FBQ0pNLGFBQVM7QUFETCxHQURPO0FBSWJrQixpQkFBZSxFQUpGO0FBS2JDLFFBQU07QUFDSm5CLGFBQVM7QUFETCxHQUxPO0FBUWJvQixjQUFZO0FBQ1Y7QUFEVSxHQVJDO0FBV2JDLFFBQU07QUFDSnRCLGtCQUFjO0FBRFYsR0FYTztBQWNidUIsVUFBUTtBQUNOdkIsa0JBQWM7QUFEUixHQWRLO0FBaUJid0IsYUFBVyxFQWpCRTtBQWtCYkMsa0JBQWdCO0FBQ2RDLFdBQU87QUFETyxHQWxCSDtBQXFCYjVCLFlBQVU7QUFDUjZCLFVBQU07QUFERTtBQXJCRyxDQUFmOztJQTBCTUMsWTs7O0FBQ0osd0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxrSkFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hDLGNBQVEsS0FERztBQUVYbkQsYUFBTyxFQUZJO0FBR1hvRCxtQkFBYSxDQUhGOztBQUtYdkIsbUJBQWEsaUJBTEY7QUFNWHdCLGFBQU8sS0FOSTtBQU9YM0Msa0JBQVksQ0FQRDtBQVFYQyxhQUFPLEVBUkk7QUFTWDJDLGVBQVMsRUFURTtBQVVYQyxhQUFPO0FBVkksS0FBYjs7QUFhQSxVQUFLNUIsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWM2QixJQUFkLE9BQWhCO0FBQ0EsVUFBS0MsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWNELElBQWQsT0FBaEI7QUFoQmlCO0FBaUJsQjs7Ozs2QkFFUUUsQyxFQUFHO0FBQ1YsVUFBTTFELFFBQVEwRCxFQUFFQyxNQUFGLENBQVMvQixLQUF2QjtBQUNBLFVBQU13QixjQUFjcEQsTUFBTTRELE1BQTFCO0FBQ0EsVUFBTVAsUUFBUUQsY0FBYyxDQUFkLElBQW1CQSxjQUFjLEdBQS9DOztBQUVBLFdBQUtTLFFBQUwsQ0FBYztBQUNaN0Qsb0JBRFk7QUFFWm9ELGdDQUZZO0FBR1pDLG9CQUhZO0FBSVoxQyxlQUFPLENBQUMwQyxLQUFELEdBQVMsNkNBQVQsR0FBeUQ7QUFKcEQsT0FBZDtBQU1EOzs7OzJHQUVjSyxDOzs7Ozs7Ozs7O3lCQUUwQixLQUFLVCxLLEVBQWxDYSxJLFVBQUFBLEksRUFBTUMsTSxVQUFBQSxNLEVBQVFoRSxVLFVBQUFBLFU7QUFDVkYsc0IsR0FBa0JpRSxJLENBQXRCRSxFLEVBQVlwRSxLLEdBQVVrRSxJLENBQVZsRSxLO0FBQ1pFLHdCLEdBQWFpRSxNLENBQWJqRSxRO0FBRUFFLHFCLEdBQVUsS0FBS2tELEssQ0FBZmxELEs7OztBQUVSLHFCQUFLNkQsUUFBTCxDQUFjLEVBQUVQLFNBQVMsb0JBQVgsRUFBZDs7O3VCQUU2QyxzQkFBVTtBQUNyRDFELDhCQURxRDtBQUVyREMsZ0NBRnFEO0FBR3JEQyxvQ0FIcUQ7QUFJckRDLHdDQUpxRDtBQUtyREM7QUFMcUQsaUJBQVYsQzs7OztBQUFyQ1UsMEIsU0FBQUEsVTtBQUFZQyxxQixTQUFBQSxLO0FBQU9qQix1QixTQUFBQSxPOztzQkFRdkJnQixjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0EscUJBQUttRCxRQUFMLENBQWMsRUFBRW5ELHNCQUFGLEVBQWNDLFlBQWQsRUFBZDs7Ozs7QUFJRixxQkFBS2tELFFBQUwsQ0FBYyxFQUFFUCxTQUFTLHVCQUFYLEVBQWQ7O0FBRUFXLDJCQUFXLFlBQU07QUFDZix5QkFBS0osUUFBTCxDQUFjO0FBQ1pSLDJCQUFPLEtBREs7QUFFWjNDLGdDQUFZLENBRkE7QUFHWkMsMkJBQU8sRUFISztBQUlaMkMsNkJBQVMsRUFKRzs7QUFNWnRELDJCQUFPLEVBTks7QUFPWm9ELGlDQUFhO0FBUEQsbUJBQWQ7QUFTRCxpQkFWRCxFQVVHLElBVkg7O0FBWUEscUJBQUtILEtBQUwsQ0FBV2lCLFlBQVg7QUFDRXhELHdDQURGO0FBRUVaLG9DQUZGO0FBR0VDLHdDQUhGO0FBSUVvRSw0QkFBVXRFLE1BSlo7QUFLRUc7QUFMRixtQkFNS04sT0FOTDs7Ozs7Ozs7QUFTQW1CLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBSUs7QUFBQTs7QUFBQSxtQkFDeUMsS0FBS3VDLEtBRDlDO0FBQUEsVUFDQ0MsTUFERCxVQUNDQSxNQUREO0FBQUEsVUFDU3RCLFdBRFQsVUFDU0EsV0FEVDtBQUFBLFVBQ3NCN0IsS0FEdEIsVUFDc0JBLEtBRHRCO0FBQUEsVUFDNkJzRCxPQUQ3QixVQUM2QkEsT0FEN0I7QUFBQSxvQkFFbUIsS0FBS0wsS0FGeEI7QUFBQSxVQUVDdkIsT0FGRCxXQUVDQSxPQUZEO0FBQUEsVUFFVW9DLElBRlYsV0FFVUEsSUFGVjtBQUFBLFVBR0NNLElBSEQsR0FHa0JOLElBSGxCLENBR0NNLElBSEQ7QUFBQSxVQUdPekIsTUFIUCxHQUdrQm1CLElBSGxCLENBR09uQixNQUhQOzs7QUFLUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdqQixRQUFRWCxJQUF4QjtBQUNFO0FBQ0Usa0JBQ0U7QUFDRSxpQkFBS3FELElBRFA7QUFFRSxpQkFBS3pCLFVBQVUsY0FGakI7QUFHRSx1QkFBV2pCLFFBQVFpQjtBQUhyQixZQUZKO0FBUUUsaUJBQ0U7QUFDRSxtQkFBTzNDLEtBRFQ7QUFFRSxzQkFBVSxLQUFLMkIsUUFGakI7QUFHRSx5QkFBYUU7QUFIZixZQVRKO0FBZUUscUJBQVdILFFBQVFlLFVBZnJCO0FBZ0JFLG1CQUFTO0FBQUEsbUJBQU0sT0FBS29CLFFBQUwsQ0FBYyxFQUFFVixRQUFRLElBQVYsRUFBZCxDQUFOO0FBQUE7QUFoQlgsVUFERjtBQW1CR0Esa0JBQVVHLFFBQVFNLE1BQWxCLEdBQ0M7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQWFOO0FBQWI7QUFERixTQURELEdBSUcsSUF2Qk47QUF3QkdILGlCQUNDO0FBQUE7QUFBQSxZQUFhLDBCQUFiO0FBQ0UsaURBQUssV0FBV3pCLFFBQVFSLFFBQXhCLEdBREY7QUFFRTtBQUFBO0FBQUEsY0FBUSxZQUFSLEVBQWUsT0FBTSxRQUFyQixFQUE4QixTQUFTLEtBQUt1QyxRQUE1QztBQUFBO0FBQUE7QUFGRixTQURELEdBT0c7QUEvQk4sT0FERjtBQW1DRDs7Ozs7QUFHSFQsYUFBYWpCLFNBQWIsR0FBeUI7QUFDdkJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBREg7O0FBR3ZCNkIsUUFBTSxvQkFBVTlCLE1BQVYsQ0FBaUJDLFVBSEE7QUFJdkI4QixVQUFRLG9CQUFVL0IsTUFBVixDQUFpQkMsVUFKRjtBQUt2QmxDLGNBQVksb0JBQVVzQyxNQUFWLENBQWlCSixVQUxOOztBQU92QmlDLGdCQUFjLG9CQUFVaEMsSUFBVixDQUFlRDtBQVBOLENBQXpCOztBQVVBLElBQU1vQyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRUo7QUFERixHQURGLEVBSUVJLFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVEsSUFBUixFQUFjRCxrQkFBZCxFQUNiLDBCQUFXdkQsTUFBWCxFQUFtQmtDLFlBQW5CLENBRGEsQyIsImZpbGUiOiI2OC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENyZWF0ZSBhIHF1ZXN0aW9uIGluIENvdXJzZSBEaXNjdXNzaW9uLlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBBTlNXRVJfQ1JFQVRFIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vY29uc3RhbnRzL2NvdXJzZXMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIHF1ZXN0aW9uSWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgdGltZXN0YW1wOiBzdHJpbmcsXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBUaGlzIGFjdGlvbiB3aWxsIGNyZWF0ZSBhbiBhbnN3ZXIgaW4gZGlzY3Vzc2lvbiBvZiBjb3Vyc2UncyBxdWVzdGlvbi5cbiAqIEBmdW5jdGlvbiBjcmVhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZUlkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmF1dGhvcklkIC0gYXV0aG9yIG9mIGEgYW5zd2VyLlxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucXVlc3Rpb25JZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aW1lc3RhbXAgLVxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgY3JlYXRlID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHtcbiAgdHlwZTogQU5TV0VSX0NSRUFURSxcbiAgcGF5bG9hZCxcbn0pO1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy91c2Vycy9kaXNjdXNzaW9uL2Fuc3dlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi8uLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHRva2VuOiBzdHJpbmcsXG4gIHVzZXJJZDogbnVtYmVyLFxuICBjb3Vyc2VJZDogbnVtYmVyLFxuICBxdWVzdGlvbklkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG59O1xuXG4vKipcbiAqIENyZWF0ZVxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gY3JlYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucXVlc3Rpb25JZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZSAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfVxuICpcbiAqIEBleGFtcGxlXG4gKi9cbmFzeW5jIGZ1bmN0aW9uIGNyZWF0ZSh7IHRva2VuLCB1c2VySWQsIGNvdXJzZUlkLCBxdWVzdGlvbklkLCB0aXRsZSB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzLyR7Y291cnNlSWR9L2Rpc2N1c3Npb24vJHtxdWVzdGlvbklkfS9hbnN3ZXJzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHRpdGxlIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvZGlzY3Vzc2lvbi9hbnN3ZXJzL2NyZWF0ZS5qcyIsIi8qKlxuICogVGhpcyBpbnB1dCBjb21wb25lbnQgaXMgbWFpbmx5IGRldmVsb3BlZCBmb3IgY3JlYXRlIHBvc3QsIGNyZWF0ZVxuICogY291cnNlIGNvbXBvbmVudC5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZGlzcGxheTogJ2ZsZXgnLFxuICB9LFxuICBpbnB1dDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gICAgYm9yZGVyOiAnMnB4IHNvbGlkICNkYWRhZGEnLFxuICAgIGJvcmRlclJhZGl1czogJzdweCcsXG4gICAgcGFkZGluZzogJzhweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbn07XG5cbmNvbnN0IElucHV0ID0gKHsgY2xhc3Nlcywgb25DaGFuZ2UsIHZhbHVlLCBwbGFjZWhvbGRlciwgZGlzYWJsZWQsIHR5cGUgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICA8aW5wdXRcbiAgICAgIHR5cGU9e3R5cGUgfHwgJ3RleHQnfVxuICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyIHx8ICdpbnB1dCd9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMuaW5wdXR9XG4gICAgICBkaXNhYmxlZD17ISFkaXNhYmxlZH1cbiAgICAvPlxuICA8L2Rpdj5cbik7XG5cbklucHV0LnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgdmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKElucHV0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsIi8qKlxuICogUG9zdENyZWF0ZSBjb21wb25lbnQgd2lsbCBiZSB1c2VkIHRvIGNyZWF0ZSBuZXcgcG9zdC5cbiAqXG4gKiBOb3RlOiBiZWZvcmUgYWRkaW5nIHRoaXMgY29tcG9uZW50IGluIGFueSBwbGFjZSwgbWFrZSBzdXJlXG4gKiB0aGUgcGVyc29uIHdobyBpcyBhY2Nlc3NpbmcgdGhpcyBjb21wb25lbnQgc2hvdWxkIGhhdmUgcmlnaHQgdG8gY3JlYXRlIHBvc3QuXG4gKiBpLmUuIHBlcnNvbiBzaG91bGQgYmUgbG9nZ2VkIGluIGFuZCB1c2VyIGNhbiBvbmx5IGNyZWF0ZSBwb3N0IGZvclxuICogdGhlaXIgcHJvZmlsZSBvbmx5LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBDYXJkSGVhZGVyIGZyb20gJ21hdGVyaWFsLXVpL0NhcmQvQ2FyZEhlYWRlcic7XG5pbXBvcnQgQ2FyZENvbnRlbnQgZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZC9DYXJkQ29udGVudCc7XG5pbXBvcnQgQ2FyZEFjdGlvbnMgZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZC9DYXJkQWN0aW9ucyc7XG5pbXBvcnQgQXZhdGFyIGZyb20gJ21hdGVyaWFsLXVpL0F2YXRhcic7XG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdtYXRlcmlhbC11aS9UeXBvZ3JhcGh5JztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcblxuaW1wb3J0IElucHV0IGZyb20gJy4uLy4uLy4uL2NvbXBvbmVudHMvSW5wdXQnO1xuXG5pbXBvcnQgYXBpQ3JlYXRlIGZyb20gJy4uLy4uLy4uL2FwaS9jb3Vyc2VzL3VzZXJzL2Rpc2N1c3Npb24vYW5zd2Vycy9jcmVhdGUnO1xuaW1wb3J0IGFjdGlvbkNyZWF0ZSBmcm9tICcuLi8uLi8uLi9hY3Rpb25zL2NvdXJzZXMvdXNlcnMvZGlzY3Vzc2lvbi9hbnN3ZXJzL2NyZWF0ZSc7XG5cbi8qKlxuICogRGVmaW5lIGFsbCBzdHlsZXMgaGVyZSBmb3IgUG9zdENyZWF0ZSBDb21wb25lbnQuXG4gKi9cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGdyaWRDb250YWluZXI6IHt9LFxuICBwb3N0OiB7XG4gICAgcGFkZGluZzogJzElJyxcbiAgfSxcbiAgY2FyZEhlYWRlcjoge1xuICAgIC8vIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgfSxcbiAgY2FyZDoge1xuICAgIGJvcmRlclJhZGl1czogJzhweCcsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gIH0sXG4gIGNhcmRNZWRpYToge30sXG4gIGNhcmRNZWRpYUltYWdlOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuY2xhc3MgQW5zd2VyQ3JlYXRlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGFjdGl2ZTogZmFsc2UsXG4gICAgICB0aXRsZTogJycsXG4gICAgICB0aXRsZUxlbmd0aDogMCxcblxuICAgICAgcGxhY2Vob2xkZXI6ICdBbnN3ZXIgVGl0bGUuLi4nLFxuICAgICAgdmFsaWQ6IGZhbHNlLFxuICAgICAgc3RhdHVzQ29kZTogMCxcbiAgICAgIGVycm9yOiAnJyxcbiAgICAgIG1lc3NhZ2U6ICcnLFxuICAgICAgaG92ZXI6IGZhbHNlLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25TdWJtaXQgPSB0aGlzLm9uU3VibWl0LmJpbmQodGhpcyk7XG4gIH1cblxuICBvbkNoYW5nZShlKSB7XG4gICAgY29uc3QgdGl0bGUgPSBlLnRhcmdldC52YWx1ZTtcbiAgICBjb25zdCB0aXRsZUxlbmd0aCA9IHRpdGxlLmxlbmd0aDtcbiAgICBjb25zdCB2YWxpZCA9IHRpdGxlTGVuZ3RoID4gMCAmJiB0aXRsZUxlbmd0aCA8IDE0ODtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgdGl0bGUsXG4gICAgICB0aXRsZUxlbmd0aCxcbiAgICAgIHZhbGlkLFxuICAgICAgZXJyb3I6ICF2YWxpZCA/ICdUaXRsZSBsZW5ndGggc2hvdWxkIGJlIGluIGJldHdlZW4gMSB0byAxNDguJyA6ICcnLFxuICAgIH0pO1xuICB9XG5cbiAgYXN5bmMgb25TdWJtaXQoZSkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IHVzZXIsIGNvdXJzZSwgcXVlc3Rpb25JZCB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IHVzZXI7XG4gICAgICBjb25zdCB7IGNvdXJzZUlkIH0gPSBjb3Vyc2U7XG5cbiAgICAgIGNvbnN0IHsgdGl0bGUgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnQ3JlYXRpbmcgQW5zd2VyLi4uJyB9KTtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpQ3JlYXRlKHtcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIHVzZXJJZCxcbiAgICAgICAgY291cnNlSWQsXG4gICAgICAgIHF1ZXN0aW9uSWQsXG4gICAgICAgIHRpdGxlLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgZXJyb3JcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IHN0YXR1c0NvZGUsIGVycm9yIH0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnU3VjY2Vzc2Z1bGx5IENyZWF0ZWQuJyB9KTtcblxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIHZhbGlkOiBmYWxzZSxcbiAgICAgICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgICAgIGVycm9yOiAnJyxcbiAgICAgICAgICBtZXNzYWdlOiAnJyxcblxuICAgICAgICAgIHRpdGxlOiAnJyxcbiAgICAgICAgICB0aXRsZUxlbmd0aDogMCxcbiAgICAgICAgfSk7XG4gICAgICB9LCAxNTAwKTtcblxuICAgICAgdGhpcy5wcm9wcy5hY3Rpb25DcmVhdGUoe1xuICAgICAgICBzdGF0dXNDb2RlLFxuICAgICAgICBjb3Vyc2VJZCxcbiAgICAgICAgcXVlc3Rpb25JZCxcbiAgICAgICAgYXV0aG9ySWQ6IHVzZXJJZCxcbiAgICAgICAgdGl0bGUsXG4gICAgICAgIC4uLnBheWxvYWQsXG4gICAgICB9KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgYWN0aXZlLCBwbGFjZWhvbGRlciwgdGl0bGUsIG1lc3NhZ2UgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBjbGFzc2VzLCB1c2VyIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgbmFtZSwgYXZhdGFyIH0gPSB1c2VyO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgIGF2YXRhcj17XG4gICAgICAgICAgICA8QXZhdGFyXG4gICAgICAgICAgICAgIGFsdD17bmFtZX1cbiAgICAgICAgICAgICAgc3JjPXthdmF0YXIgfHwgJy9mYXZpY29uLmljbyd9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgIH1cbiAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICA8SW5wdXRcbiAgICAgICAgICAgICAgdmFsdWU9e3RpdGxlfVxuICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICB9XG4gICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmRIZWFkZXJ9XG4gICAgICAgICAgb25Gb2N1cz17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGFjdGl2ZTogdHJ1ZSB9KX1cbiAgICAgICAgLz5cbiAgICAgICAge2FjdGl2ZSAmJiBtZXNzYWdlLmxlbmd0aCA/IChcbiAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeT57bWVzc2FnZX08L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIHthY3RpdmUgPyAoXG4gICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICA8QnV0dG9uIHJhaXNlZCBjb2xvcj1cImFjY2VudFwiIG9uQ2xpY2s9e3RoaXMub25TdWJtaXR9PlxuICAgICAgICAgICAgICBDcmVhdGVcbiAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICkgOiBudWxsfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5BbnN3ZXJDcmVhdGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgdXNlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBjb3Vyc2U6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgcXVlc3Rpb25JZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuXG4gIGFjdGlvbkNyZWF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbkNyZWF0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG51bGwsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShBbnN3ZXJDcmVhdGUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL0Rpc2N1c3Npb24vQW5zd2VyQ3JlYXRlLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==