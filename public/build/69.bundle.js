webpackJsonp([69],{

/***/ "./src/client/api/users/username/update.js":
/*!*************************************************!*\
  !*** ./src/client/api/users/username/update.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.username -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        username = _ref2.username;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/username';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ username: username })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/api/users/username/validate.js":
/*!***************************************************!*\
  !*** ./src/client/api/users/username/validate.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.username -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var validate = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var username = _ref2.username;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements/validate?username=' + username;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function validate(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = validate;

/***/ }),

/***/ "./src/client/containers/ProfilePage/Account.js":
/*!******************************************************!*\
  !*** ./src/client/containers/ProfilePage/Account.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _update = __webpack_require__(/*! ../../api/users/username/update */ "./src/client/api/users/username/update.js");

var _update2 = _interopRequireDefault(_update);

var _validate = __webpack_require__(/*! ../../api/users/username/validate */ "./src/client/api/users/username/validate.js");

var _validate2 = _interopRequireDefault(_validate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {},
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  gridItem: {
    padding: '1%'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    },
    '&:valid': {
      outline: 'none',
      borderColor: 'green',
      boxShadow: '0 0 10px #9ecaed'
    },
    '&:invalid': {
      outline: 'none',
      borderColor: 'red',
      boxShadow: '0 0 10px #9ecaed'
    }
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

// This regular expression is for Username.
/** @format */

var PATTERN = '(?=^.{3,20}$)^[a-zA-Z][a-zA-Z0-9]*[._-]?[a-zA-Z0-9]+$';

var Account = function (_Component) {
  (0, _inherits3.default)(Account, _Component);

  function Account(props) {
    (0, _classCallCheck3.default)(this, Account);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Account.__proto__ || (0, _getPrototypeOf2.default)(Account)).call(this, props));

    _this.state = {
      username: props.user.username,
      focus: false,
      isValid: false,
      isAvailable: false,
      error: '',
      message: ''
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    _this.onClickApply = _this.onClickApply.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Account, [{
    key: 'onChange',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var username, regex, _ref2, statusCode;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                username = e.target.value;
                regex = new RegExp(PATTERN);

                if (regex.test(username)) {
                  _context.next = 6;
                  break;
                }

                this.setState({ username: username, isValid: false });
                return _context.abrupt('return');

              case 6:
                this.setState({ username: username, isValid: true });

                if (!(this.props.user.username === username)) {
                  _context.next = 9;
                  break;
                }

                return _context.abrupt('return');

              case 9:
                _context.next = 11;
                return (0, _validate2.default)({ username: username });

              case 11:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;


                this.setState({ isAvailable: statusCode === 200 });
                _context.next = 19;
                break;

              case 16:
                _context.prev = 16;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 19:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 16]]);
      }));

      function onChange(_x) {
        return _ref.apply(this, arguments);
      }

      return onChange;
    }()

    // before submiting make sure to check everything.

  }, {
    key: 'onSubmit',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _this2 = this;

        var _props$user, userId, token, username, _ref4, statusCode, message, error;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _props$user = this.props.user, userId = _props$user.id, token = _props$user.token;
                username = this.state.username;
                _context2.next = 5;
                return (0, _update2.default)({
                  token: token,
                  userId: userId,
                  username: username
                });

              case 5:
                _ref4 = _context2.sent;
                statusCode = _ref4.statusCode;
                message = _ref4.message;
                error = _ref4.error;

                if (!(statusCode >= 300)) {
                  _context2.next = 12;
                  break;
                }

                this.setState({ message: message });
                return _context2.abrupt('return');

              case 12:

                this.setState({ message: message });

                setTimeout(function () {
                  _this2.setState({ message: '' });
                }, 1000);

                this.props.userUpdate({ userId: userId, username: username });
                _context2.next = 20;
                break;

              case 17:
                _context2.prev = 17;
                _context2.t0 = _context2['catch'](0);

                console.log(_context2.t0);

              case 20:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 17]]);
      }));

      function onSubmit() {
        return _ref3.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'onClickApply',
    value: function onClickApply() {
      console.log(this.props);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          disabled = _props.disabled;
      var _state = this.state,
          focus = _state.focus,
          isValid = _state.isValid,
          isAvailable = _state.isAvailable,
          message = _state.message,
          username = _state.username;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 6,
            xl: 6,
            className: classes.gridItem
          },
          _react2.default.createElement(
            'div',
            {
              onMouseEnter: function onMouseEnter() {
                return _this3.setState({ hover: true });
              },
              onMouseLeave: function onMouseLeave() {
                return _this3.setState({ hover: false });
              }
            },
            _react2.default.createElement(
              _Card2.default,
              { raised: this.state.hover, className: classes.card },
              _react2.default.createElement(_Card.CardHeader, { title: 'Username' }),
              _react2.default.createElement(
                _Card.CardContent,
                {
                  onFocus: function onFocus() {
                    return _this3.setState({ focus: true });
                  },
                  onBlur: function onBlur() {
                    return _this3.setState({
                      focus: !(_this3.props.user.username === username)
                    });
                  },
                  style: { display: 'flex' }
                },
                _react2.default.createElement('input', {
                  placeholder: 'Username',
                  value: username,
                  onChange: this.onChange,
                  minLength: 3,
                  maxLength: 20,
                  pattern: PATTERN,
                  required: true,
                  disabled: disabled,
                  className: classes.input
                })
              ),
              isAvailable ? _react2.default.createElement(
                _Card.CardContent,
                null,
                'Username is already taken.'
              ) : null,
              message.length ? _react2.default.createElement(
                _Card.CardContent,
                null,
                message
              ) : null,
              focus && !(disabled === true) ? _react2.default.createElement(
                _Card.CardActions,
                null,
                _react2.default.createElement('div', { className: classes.flexGrow }),
                _react2.default.createElement(
                  _Button2.default,
                  {
                    raised: true,
                    color: 'accent',
                    onClick: this.onSubmit,
                    disabled: !isValid || isAvailable
                  },
                  'Submit'
                )
              ) : null
            )
          ),
          _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onClickApply },
              'Apply for Guru'
            )
          )
        )
      );
    }
  }]);
  return Account;
}(_react.Component);

Account.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    token: _propTypes2.default.string.isRequired,
    username: _propTypes2.default.string,
    name: _propTypes2.default.string
  }).isRequired,
  userUpdate: _propTypes2.default.func.isRequired,
  disabled: _propTypes2.default.bool.isRequired
};

Account.defaultProps = {
  disabled: false
};

exports.default = (0, _withStyles2.default)(styles)(Account);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS91c2Vycy91c2VybmFtZS91cGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvdXNlcnMvdXNlcm5hbWUvdmFsaWRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1Byb2ZpbGVQYWdlL0FjY291bnQuanMiXSwibmFtZXMiOlsidG9rZW4iLCJ1c2VySWQiLCJ1c2VybmFtZSIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwiYm9keSIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsInVwZGF0ZSIsInZhbGlkYXRlIiwic3R5bGVzIiwicm9vdCIsImNhcmQiLCJib3JkZXJSYWRpdXMiLCJhdmF0YXIiLCJncmlkSXRlbSIsInBhZGRpbmciLCJpbnB1dCIsImZsZXhHcm93IiwiYm9yZGVyIiwib3V0bGluZSIsImJvcmRlckNvbG9yIiwiYm94U2hhZG93IiwiZmxleCIsIlBBVFRFUk4iLCJBY2NvdW50IiwicHJvcHMiLCJzdGF0ZSIsInVzZXIiLCJmb2N1cyIsImlzVmFsaWQiLCJpc0F2YWlsYWJsZSIsIm1lc3NhZ2UiLCJvbkNoYW5nZSIsImJpbmQiLCJvblN1Ym1pdCIsIm9uQ2xpY2tBcHBseSIsImUiLCJ0YXJnZXQiLCJ2YWx1ZSIsInJlZ2V4IiwiUmVnRXhwIiwidGVzdCIsInNldFN0YXRlIiwiaWQiLCJzZXRUaW1lb3V0IiwidXNlclVwZGF0ZSIsImxvZyIsImNsYXNzZXMiLCJkaXNhYmxlZCIsImhvdmVyIiwiZGlzcGxheSIsImxlbmd0aCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJzaGFwZSIsIm51bWJlciIsInN0cmluZyIsIm5hbWUiLCJmdW5jIiwiYm9vbCIsImRlZmF1bHRQcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS0E7Ozs7Ozs7Ozs7OztBQUxBOzs7c0ZBaUJBO0FBQUEsUUFBd0JBLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxNQUEvQixTQUErQkEsTUFBL0I7QUFBQSxRQUF1Q0MsUUFBdkMsU0FBdUNBLFFBQXZDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVDLGVBRlYsa0NBRXFDRixNQUZyQztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNRSxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVA7QUFIUixlQUZrQjtBQU8zQlEsb0JBQU0seUJBQWUsRUFBRU4sa0JBQUYsRUFBZjtBQVBxQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFjWUMsa0JBZFosR0FjbUNELEdBZG5DLENBY1lDLE1BZFosRUFjb0JDLFVBZHBCLEdBY21DRixHQWRuQyxDQWNvQkUsVUFkcEI7O0FBQUEsa0JBZ0JRRCxVQUFVLEdBaEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FpQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFqQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXVCdUJGLElBQUlLLElBQUosRUF2QnZCOztBQUFBO0FBdUJVQSxnQkF2QlY7QUFBQSx3RUF5QmdCQSxJQXpCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTJCSUMsb0JBQVFGLEtBQVI7O0FBM0JKLDZDQTZCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE3Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVHLE07Ozs7O0FBZmY7Ozs7QUFDQTs7OztrQkFrRGVBLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoRGY7Ozs7Ozs7Ozs7QUFMQTs7O3NGQWVBO0FBQUEsUUFBMEJkLFFBQTFCLFNBQTBCQSxRQUExQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLHVEQUUwREQsUUFGMUQ7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUMsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0I7QUFGVDtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVHLGVBSlY7QUFZWUMsa0JBWlosR0FZbUNELEdBWm5DLENBWVlDLE1BWlosRUFZb0JDLFVBWnBCLEdBWW1DRixHQVpuQyxDQVlvQkUsVUFacEI7O0FBQUEsa0JBY1FELFVBQVUsR0FkbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZWE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFmYjs7QUFBQTtBQUFBO0FBQUEsbUJBcUJ1QkYsSUFBSUssSUFBSixFQXJCdkI7O0FBQUE7QUFxQlVBLGdCQXJCVjtBQUFBLHdFQXVCZ0JBLElBdkJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBeUJJQyxvQkFBUUYsS0FBUjs7QUF6QkosNkNBMkJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTNCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZUksUTs7Ozs7QUFiZjs7OztBQUNBOzs7O2tCQThDZUEsUTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQ2Y7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFNQyxTQUFTO0FBQ2JDLFFBQU0sRUFETztBQUViQyxRQUFNO0FBQ0pDLGtCQUFjO0FBRFYsR0FGTztBQUtiQyxVQUFRO0FBQ05ELGtCQUFjO0FBRFIsR0FMSztBQVFiRSxZQUFVO0FBQ1JDLGFBQVM7QUFERCxHQVJHO0FBV2JDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTE4sa0JBQWMsS0FIVDtBQUlMRyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RJLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEYsS0FMTjtBQVVMLGVBQVc7QUFDVEYsZUFBUyxNQURBO0FBRVRDLG1CQUFhLE9BRko7QUFHVEMsaUJBQVc7QUFIRixLQVZOO0FBZUwsaUJBQWE7QUFDWEYsZUFBUyxNQURFO0FBRVhDLG1CQUFhLEtBRkY7QUFHWEMsaUJBQVc7QUFIQTtBQWZSLEdBWE07QUFnQ2JKLFlBQVU7QUFDUkssVUFBTTtBQURFO0FBaENHLENBQWY7O0FBcUNBO0FBbkRBOztBQW9EQSxJQUFNQyxVQUFVLHVEQUFoQjs7SUFFTUMsTzs7O0FBQ0osbUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSx3SUFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hqQyxnQkFBVWdDLE1BQU1FLElBQU4sQ0FBV2xDLFFBRFY7QUFFWG1DLGFBQU8sS0FGSTtBQUdYQyxlQUFTLEtBSEU7QUFJWEMsbUJBQWEsS0FKRjtBQUtYMUIsYUFBTyxFQUxJO0FBTVgyQixlQUFTO0FBTkUsS0FBYjs7QUFTQSxVQUFLQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0MsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLFFBQUwsR0FBZ0IsTUFBS0EsUUFBTCxDQUFjRCxJQUFkLE9BQWhCO0FBQ0EsVUFBS0UsWUFBTCxHQUFvQixNQUFLQSxZQUFMLENBQWtCRixJQUFsQixPQUFwQjtBQWJpQjtBQWNsQjs7Ozs7MkdBRWNHLEM7Ozs7Ozs7O0FBRUwzQyx3QixHQUFXMkMsRUFBRUMsTUFBRixDQUFTQyxLO0FBQ3BCQyxxQixHQUFRLElBQUlDLE1BQUosQ0FBV2pCLE9BQVgsQzs7b0JBRVRnQixNQUFNRSxJQUFOLENBQVdoRCxRQUFYLEM7Ozs7O0FBQ0gscUJBQUtpRCxRQUFMLENBQWMsRUFBRWpELGtCQUFGLEVBQVlvQyxTQUFTLEtBQXJCLEVBQWQ7Ozs7QUFHRixxQkFBS2EsUUFBTCxDQUFjLEVBQUVqRCxrQkFBRixFQUFZb0MsU0FBUyxJQUFyQixFQUFkOztzQkFFSSxLQUFLSixLQUFMLENBQVdFLElBQVgsQ0FBZ0JsQyxRQUFoQixLQUE2QkEsUTs7Ozs7Ozs7O3VCQUlKLHdCQUFvQixFQUFFQSxrQkFBRixFQUFwQixDOzs7O0FBQXJCVSwwQixTQUFBQSxVOzs7QUFFUixxQkFBS3VDLFFBQUwsQ0FBYyxFQUFFWixhQUFhM0IsZUFBZSxHQUE5QixFQUFkOzs7Ozs7OztBQUVBRyx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFJSjs7Ozs7Ozs7Ozs7Ozs7OzhCQUdrQyxLQUFLcUIsS0FBTCxDQUFXRSxJLEVBQTdCbkMsTSxlQUFKbUQsRSxFQUFZcEQsSyxlQUFBQSxLO0FBQ1pFLHdCLEdBQWEsS0FBS2lDLEssQ0FBbEJqQyxROzt1QkFFcUMsc0JBQWtCO0FBQzdERiw4QkFENkQ7QUFFN0RDLGdDQUY2RDtBQUc3REM7QUFINkQsaUJBQWxCLEM7Ozs7QUFBckNVLDBCLFNBQUFBLFU7QUFBWTRCLHVCLFNBQUFBLE87QUFBUzNCLHFCLFNBQUFBLEs7O3NCQU16QkQsY0FBYyxHOzs7OztBQUNoQixxQkFBS3VDLFFBQUwsQ0FBYyxFQUFFWCxnQkFBRixFQUFkOzs7OztBQUlGLHFCQUFLVyxRQUFMLENBQWMsRUFBRVgsZ0JBQUYsRUFBZDs7QUFFQWEsMkJBQVcsWUFBTTtBQUNmLHlCQUFLRixRQUFMLENBQWMsRUFBRVgsU0FBUyxFQUFYLEVBQWQ7QUFDRCxpQkFGRCxFQUVHLElBRkg7O0FBSUEscUJBQUtOLEtBQUwsQ0FBV29CLFVBQVgsQ0FBc0IsRUFBRXJELGNBQUYsRUFBVUMsa0JBQVYsRUFBdEI7Ozs7Ozs7O0FBRUFhLHdCQUFRd0MsR0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7O21DQUlXO0FBQ2J4QyxjQUFRd0MsR0FBUixDQUFZLEtBQUtyQixLQUFqQjtBQUNEOzs7NkJBRVE7QUFBQTs7QUFBQSxtQkFDdUIsS0FBS0EsS0FENUI7QUFBQSxVQUNDc0IsT0FERCxVQUNDQSxPQUREO0FBQUEsVUFDVUMsUUFEVixVQUNVQSxRQURWO0FBQUEsbUJBRW9ELEtBQUt0QixLQUZ6RDtBQUFBLFVBRUNFLEtBRkQsVUFFQ0EsS0FGRDtBQUFBLFVBRVFDLE9BRlIsVUFFUUEsT0FGUjtBQUFBLFVBRWlCQyxXQUZqQixVQUVpQkEsV0FGakI7QUFBQSxVQUU4QkMsT0FGOUIsVUFFOEJBLE9BRjlCO0FBQUEsVUFFdUN0QyxRQUZ2QyxVQUV1Q0EsUUFGdkM7OztBQUlQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBV3NELFFBQVFyQyxJQUFoRTtBQUNFO0FBQUE7QUFBQTtBQUNFLHNCQURGO0FBRUUsZ0JBQUksRUFGTjtBQUdFLGdCQUFJLEVBSE47QUFJRSxnQkFBSSxDQUpOO0FBS0UsZ0JBQUksQ0FMTjtBQU1FLGdCQUFJLENBTk47QUFPRSx1QkFBV3FDLFFBQVFqQztBQVByQjtBQVNFO0FBQUE7QUFBQTtBQUNFLDRCQUFjO0FBQUEsdUJBQU0sT0FBSzRCLFFBQUwsQ0FBYyxFQUFFTyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsZUFEaEI7QUFFRSw0QkFBYztBQUFBLHVCQUFNLE9BQUtQLFFBQUwsQ0FBYyxFQUFFTyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUE7QUFGaEI7QUFJRTtBQUFBO0FBQUEsZ0JBQU0sUUFBUSxLQUFLdkIsS0FBTCxDQUFXdUIsS0FBekIsRUFBZ0MsV0FBV0YsUUFBUXBDLElBQW5EO0FBQ0UsZ0VBQVksT0FBTyxVQUFuQixHQURGO0FBRUU7QUFBQTtBQUFBO0FBQ0UsMkJBQVM7QUFBQSwyQkFBTSxPQUFLK0IsUUFBTCxDQUFjLEVBQUVkLE9BQU8sSUFBVCxFQUFkLENBQU47QUFBQSxtQkFEWDtBQUVFLDBCQUFRO0FBQUEsMkJBQ04sT0FBS2MsUUFBTCxDQUFjO0FBQ1pkLDZCQUFPLEVBQUUsT0FBS0gsS0FBTCxDQUFXRSxJQUFYLENBQWdCbEMsUUFBaEIsS0FBNkJBLFFBQS9CO0FBREsscUJBQWQsQ0FETTtBQUFBLG1CQUZWO0FBT0UseUJBQU8sRUFBRXlELFNBQVMsTUFBWDtBQVBUO0FBU0U7QUFDRSwrQkFBYSxVQURmO0FBRUUseUJBQU96RCxRQUZUO0FBR0UsNEJBQVUsS0FBS3VDLFFBSGpCO0FBSUUsNkJBQVcsQ0FKYjtBQUtFLDZCQUFXLEVBTGI7QUFNRSwyQkFBU1QsT0FOWDtBQU9FLGdDQVBGO0FBUUUsNEJBQVV5QixRQVJaO0FBU0UsNkJBQVdELFFBQVEvQjtBQVRyQjtBQVRGLGVBRkY7QUF1QkdjLDRCQUNDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERCxHQUVHLElBekJOO0FBMEJHQyxzQkFBUW9CLE1BQVIsR0FBaUI7QUFBQTtBQUFBO0FBQWNwQjtBQUFkLGVBQWpCLEdBQXdELElBMUIzRDtBQTJCR0gsdUJBQVMsRUFBRW9CLGFBQWEsSUFBZixDQUFULEdBQ0M7QUFBQTtBQUFBO0FBQ0UsdURBQUssV0FBV0QsUUFBUTlCLFFBQXhCLEdBREY7QUFFRTtBQUFBO0FBQUE7QUFDRSxnQ0FERjtBQUVFLDJCQUFNLFFBRlI7QUFHRSw2QkFBUyxLQUFLaUIsUUFIaEI7QUFJRSw4QkFBVSxDQUFDTCxPQUFELElBQVlDO0FBSnhCO0FBQUE7QUFBQTtBQUZGLGVBREQsR0FZRztBQXZDTjtBQUpGLFdBVEY7QUF1REU7QUFBQTtBQUFBO0FBQ0UsbURBQUssV0FBV2lCLFFBQVE5QixRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLGdCQUFRLFlBQVIsRUFBZSxPQUFNLFFBQXJCLEVBQThCLFNBQVMsS0FBS2tCLFlBQTVDO0FBQUE7QUFBQTtBQUZGO0FBdkRGO0FBREYsT0FERjtBQWtFRDs7Ozs7QUFHSFgsUUFBUTRCLFNBQVIsR0FBb0I7QUFDbEJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBRFI7QUFFbEIzQixRQUFNLG9CQUFVNEIsS0FBVixDQUFnQjtBQUNwQlosUUFBSSxvQkFBVWEsTUFBVixDQUFpQkYsVUFERDtBQUVwQi9ELFdBQU8sb0JBQVVrRSxNQUFWLENBQWlCSCxVQUZKO0FBR3BCN0QsY0FBVSxvQkFBVWdFLE1BSEE7QUFJcEJDLFVBQU0sb0JBQVVEO0FBSkksR0FBaEIsRUFLSEgsVUFQZTtBQVFsQlQsY0FBWSxvQkFBVWMsSUFBVixDQUFlTCxVQVJUO0FBU2xCTixZQUFVLG9CQUFVWSxJQUFWLENBQWVOO0FBVFAsQ0FBcEI7O0FBWUE5QixRQUFRcUMsWUFBUixHQUF1QjtBQUNyQmIsWUFBVTtBQURXLENBQXZCOztrQkFJZSwwQkFBV3ZDLE1BQVgsRUFBbUJlLE9BQW5CLEMiLCJmaWxlIjoiNjkuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJuYW1lIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiB1cGRhdGUoeyB0b2tlbiwgdXNlcklkLCB1c2VybmFtZSB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS91c2VybmFtZWA7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ1BVVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHVzZXJuYW1lIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3VzZXJzL3VzZXJuYW1lL3VwZGF0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiB1cGRhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJuYW1lIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiB2YWxpZGF0ZSh7IHVzZXJuYW1lIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvZWxlbWVudHMvdmFsaWRhdGU/dXNlcm5hbWU9JHt1c2VybmFtZX1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgdmFsaWRhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS91c2Vycy91c2VybmFtZS92YWxpZGF0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuXG5pbXBvcnQgYXBpVXBkYXRlVXNlcm5hbWUgZnJvbSAnLi4vLi4vYXBpL3VzZXJzL3VzZXJuYW1lL3VwZGF0ZSc7XG5pbXBvcnQgYXBpVmFsaWRhdGVVc2VybmFtZSBmcm9tICcuLi8uLi9hcGkvdXNlcnMvdXNlcm5hbWUvdmFsaWRhdGUnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHt9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgYXZhdGFyOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgfSxcbiAgZ3JpZEl0ZW06IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBpbnB1dDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gICAgYm9yZGVyOiAnMnB4IHNvbGlkICNkYWRhZGEnLFxuICAgIGJvcmRlclJhZGl1czogJzdweCcsXG4gICAgcGFkZGluZzogJzhweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgICAnJjp2YWxpZCc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJyxcbiAgICAgIGJvcmRlckNvbG9yOiAnZ3JlZW4nLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgICAnJjppbnZhbGlkJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICdyZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuLy8gVGhpcyByZWd1bGFyIGV4cHJlc3Npb24gaXMgZm9yIFVzZXJuYW1lLlxuY29uc3QgUEFUVEVSTiA9ICcoPz1eLnszLDIwfSQpXlthLXpBLVpdW2EtekEtWjAtOV0qWy5fLV0/W2EtekEtWjAtOV0rJCc7XG5cbmNsYXNzIEFjY291bnQgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgdXNlcm5hbWU6IHByb3BzLnVzZXIudXNlcm5hbWUsXG4gICAgICBmb2N1czogZmFsc2UsXG4gICAgICBpc1ZhbGlkOiBmYWxzZSxcbiAgICAgIGlzQXZhaWxhYmxlOiBmYWxzZSxcbiAgICAgIGVycm9yOiAnJyxcbiAgICAgIG1lc3NhZ2U6ICcnLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25TdWJtaXQgPSB0aGlzLm9uU3VibWl0LmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNsaWNrQXBwbHkgPSB0aGlzLm9uQ2xpY2tBcHBseS5iaW5kKHRoaXMpO1xuICB9XG5cbiAgYXN5bmMgb25DaGFuZ2UoZSkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB1c2VybmFtZSA9IGUudGFyZ2V0LnZhbHVlO1xuICAgICAgY29uc3QgcmVnZXggPSBuZXcgUmVnRXhwKFBBVFRFUk4pO1xuXG4gICAgICBpZiAoIXJlZ2V4LnRlc3QodXNlcm5hbWUpKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyB1c2VybmFtZSwgaXNWYWxpZDogZmFsc2UgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyB1c2VybmFtZSwgaXNWYWxpZDogdHJ1ZSB9KTtcblxuICAgICAgaWYgKHRoaXMucHJvcHMudXNlci51c2VybmFtZSA9PT0gdXNlcm5hbWUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUgfSA9IGF3YWl0IGFwaVZhbGlkYXRlVXNlcm5hbWUoeyB1c2VybmFtZSB9KTtcblxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzQXZhaWxhYmxlOiBzdGF0dXNDb2RlID09PSAyMDAgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH1cbiAgfVxuXG4gIC8vIGJlZm9yZSBzdWJtaXRpbmcgbWFrZSBzdXJlIHRvIGNoZWNrIGV2ZXJ5dGhpbmcuXG4gIGFzeW5jIG9uU3VibWl0KCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGlkOiB1c2VySWQsIHRva2VuIH0gPSB0aGlzLnByb3BzLnVzZXI7XG4gICAgICBjb25zdCB7IHVzZXJuYW1lIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIG1lc3NhZ2UsIGVycm9yIH0gPSBhd2FpdCBhcGlVcGRhdGVVc2VybmFtZSh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIHVzZXJuYW1lLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZSB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZSB9KTtcblxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnJyB9KTtcbiAgICAgIH0sIDEwMDApO1xuXG4gICAgICB0aGlzLnByb3BzLnVzZXJVcGRhdGUoeyB1c2VySWQsIHVzZXJuYW1lIH0pO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgb25DbGlja0FwcGx5KCkge1xuICAgIGNvbnNvbGUubG9nKHRoaXMucHJvcHMpO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgZGlzYWJsZWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBmb2N1cywgaXNWYWxpZCwgaXNBdmFpbGFibGUsIG1lc3NhZ2UsIHVzZXJuYW1lIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZFxuICAgICAgICAgIGl0ZW1cbiAgICAgICAgICB4cz17MTJ9XG4gICAgICAgICAgc209ezEwfVxuICAgICAgICAgIG1kPXs4fVxuICAgICAgICAgIGxnPXs2fVxuICAgICAgICAgIHhsPXs2fVxuICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIG9uTW91c2VFbnRlcj17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pfVxuICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXsoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxDYXJkIHJhaXNlZD17dGhpcy5zdGF0ZS5ob3Zlcn0gY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgICAgICA8Q2FyZEhlYWRlciB0aXRsZT17J1VzZXJuYW1lJ30gLz5cbiAgICAgICAgICAgICAgPENhcmRDb250ZW50XG4gICAgICAgICAgICAgICAgb25Gb2N1cz17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGZvY3VzOiB0cnVlIH0pfVxuICAgICAgICAgICAgICAgIG9uQmx1cj17KCkgPT5cbiAgICAgICAgICAgICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgICAgICAgICAgICBmb2N1czogISh0aGlzLnByb3BzLnVzZXIudXNlcm5hbWUgPT09IHVzZXJuYW1lKSxcbiAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6ICdmbGV4JyB9fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17J1VzZXJuYW1lJ31cbiAgICAgICAgICAgICAgICAgIHZhbHVlPXt1c2VybmFtZX1cbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoPXszfVxuICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoPXsyMH1cbiAgICAgICAgICAgICAgICAgIHBhdHRlcm49e1BBVFRFUk59XG4gICAgICAgICAgICAgICAgICByZXF1aXJlZFxuICAgICAgICAgICAgICAgICAgZGlzYWJsZWQ9e2Rpc2FibGVkfVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIHtpc0F2YWlsYWJsZSA/IChcbiAgICAgICAgICAgICAgICA8Q2FyZENvbnRlbnQ+VXNlcm5hbWUgaXMgYWxyZWFkeSB0YWtlbi48L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICAgICAge21lc3NhZ2UubGVuZ3RoID8gPENhcmRDb250ZW50PnttZXNzYWdlfTwvQ2FyZENvbnRlbnQ+IDogbnVsbH1cbiAgICAgICAgICAgICAge2ZvY3VzICYmICEoZGlzYWJsZWQgPT09IHRydWUpID8gKFxuICAgICAgICAgICAgICAgIDxDYXJkQWN0aW9ucz5cbiAgICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAgICAgICAgICByYWlzZWRcbiAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uU3VibWl0fVxuICAgICAgICAgICAgICAgICAgICBkaXNhYmxlZD17IWlzVmFsaWQgfHwgaXNBdmFpbGFibGV9XG4gICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgIFN1Ym1pdFxuICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPENhcmRBY3Rpb25zPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICA8QnV0dG9uIHJhaXNlZCBjb2xvcj1cImFjY2VudFwiIG9uQ2xpY2s9e3RoaXMub25DbGlja0FwcGx5fT5cbiAgICAgICAgICAgICAgQXBwbHkgZm9yIEd1cnVcbiAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cbkFjY291bnQucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHVzZXI6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgaWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgICB0b2tlbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIG5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIH0pLmlzUmVxdWlyZWQsXG4gIHVzZXJVcGRhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxufTtcblxuQWNjb3VudC5kZWZhdWx0UHJvcHMgPSB7XG4gIGRpc2FibGVkOiBmYWxzZSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShBY2NvdW50KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Qcm9maWxlUGFnZS9BY2NvdW50LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==