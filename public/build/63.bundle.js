webpackJsonp([63],{

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./src/client/actions/courses/users/discussion/create.js":
/*!***************************************************************!*\
  !*** ./src/client/actions/courses/users/discussion/create.js ***!
  \***************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../../constants/courses */ "./src/client/constants/courses.js");

/**
 * This action will create a question in discussion of course
 * @function create
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId - author of a question.
 * @param {number} payload.questionId -
 * @param {string} payload.title -
 * @param {string} payload.timestamp -
 * @returns {Object}
 */
var create = function create(payload) {
  return {
    type: _courses.QUESTION_CREATE,
    payload: payload
  };
}; /**
    * Create a question in Course Discussion.
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/api/courses/users/discussion/create.js":
/*!***********************************************************!*\
  !*** ./src/client/api/courses/users/discussion/create.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Create Modules
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {string} payload.title -
 * @return {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId + '/discussion';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/CoursePage/Discussion/QuestionCreate.js":
/*!***********************************************************************!*\
  !*** ./src/client/containers/CoursePage/Discussion/QuestionCreate.js ***!
  \***********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Input = __webpack_require__(/*! ../../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../../actions/courses/users/discussion/create */ "./src/client/actions/courses/users/discussion/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../../api/courses/users/discussion/create */ "./src/client/api/courses/users/discussion/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Define all styles here for PostCreate Component.
 */
/**
 * QuestionCreate component will be used to create new question.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create question.
 * i.e. person should be logged in and user can create question for any course.
 *
 * @format
 */

var styles = {
  root: {
    padding: '1%'
  },
  gridContainer: {},
  post: {
    padding: '1%'
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

var QuestionCreate = function (_Component) {
  (0, _inherits3.default)(QuestionCreate, _Component);

  function QuestionCreate(props) {
    (0, _classCallCheck3.default)(this, QuestionCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (QuestionCreate.__proto__ || (0, _getPrototypeOf2.default)(QuestionCreate)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,

      placeholder: 'Question Title...',
      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(QuestionCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      var title = e.target.value;
      var titleLength = title.length;
      var valid = titleLength > 0 && titleLength < 148;

      this.setState({
        title: title,
        titleLength: titleLength,
        valid: valid,
        error: !valid ? 'Title length should be in between 1 to 148.' : ''
      });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props, elements, course, _elements$user, userId, token, courseId, title, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, elements = _props.elements, course = _props.course;
                _elements$user = elements.user, userId = _elements$user.id, token = _elements$user.token;
                courseId = course.courseId;
                title = this.state.title;


                this.setState({ message: 'Creating course module...' });

                _context.next = 8;
                return (0, _create4.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  title: title
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 15;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 15:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionQuestionCreate((0, _extends3.default)({
                  statusCode: statusCode,
                  authorId: userId
                }, payload, {
                  title: title
                }));
                _context.next = 23;
                break;

              case 20:
                _context.prev = 20;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 23:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 20]]);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _state = this.state,
          active = _state.active,
          placeholder = _state.placeholder,
          title = _state.title,
          message = _state.message;
      var _props2 = this.props,
          classes = _props2.classes,
          elements = _props2.elements;
      var _elements$user2 = elements.user,
          name = _elements$user2.name,
          avatar = _elements$user2.avatar;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { className: classes.card, raised: this.state.hover },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(_Avatar2.default, {
              alt: name,
              src: avatar || '/favicon.ico',
              className: classes.avatar
            }),
            title: _react2.default.createElement(_Input2.default, {
              value: title,
              onChange: this.onChange,
              placeholder: placeholder
            }),
            className: classes.cardHeader,
            onFocus: function onFocus() {
              return _this3.setState({ active: true });
            }
          }),
          active && message.length ? _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              null,
              message
            )
          ) : null,
          active ? _react2.default.createElement(
            _Card.CardActions,
            { disableActionSpacing: true },
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { onClick: this.onSubmit },
              'Create'
            )
          ) : null
        )
      );
    }
  }]);
  return QuestionCreate;
}(_react.Component);

QuestionCreate.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,

  actionQuestionCreate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionQuestionCreate: _create2.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(QuestionCreate));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VzZXJzL2Rpc2N1c3Npb24vY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvZGlzY3Vzc2lvbi9jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL0Rpc2N1c3Npb24vUXVlc3Rpb25DcmVhdGUuanMiXSwibmFtZXMiOlsiY3JlYXRlIiwicGF5bG9hZCIsInR5cGUiLCJ0b2tlbiIsInVzZXJJZCIsImNvdXJzZUlkIiwidGl0bGUiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsImJvZHkiLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJzdHlsZXMiLCJyb290IiwiZGlzcGxheSIsImlucHV0IiwiZmxleEdyb3ciLCJib3JkZXIiLCJib3JkZXJSYWRpdXMiLCJwYWRkaW5nIiwib3V0bGluZSIsImJvcmRlckNvbG9yIiwiYm94U2hhZG93IiwiSW5wdXQiLCJjbGFzc2VzIiwib25DaGFuZ2UiLCJ2YWx1ZSIsInBsYWNlaG9sZGVyIiwiZGlzYWJsZWQiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiZnVuYyIsIm9uZU9mVHlwZSIsInN0cmluZyIsIm51bWJlciIsImJvb2wiLCJncmlkQ29udGFpbmVyIiwicG9zdCIsImNhcmRIZWFkZXIiLCJjYXJkIiwiYXZhdGFyIiwiY2FyZE1lZGlhIiwiY2FyZE1lZGlhSW1hZ2UiLCJ3aWR0aCIsImZsZXgiLCJRdWVzdGlvbkNyZWF0ZSIsInByb3BzIiwib25Nb3VzZUVudGVyIiwic2V0U3RhdGUiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsInN0YXRlIiwiYWN0aXZlIiwidGl0bGVMZW5ndGgiLCJ2YWxpZCIsIm1lc3NhZ2UiLCJiaW5kIiwib25TdWJtaXQiLCJlIiwidGFyZ2V0IiwibGVuZ3RoIiwiZWxlbWVudHMiLCJjb3Vyc2UiLCJ1c2VyIiwiaWQiLCJzZXRUaW1lb3V0IiwiYWN0aW9uUXVlc3Rpb25DcmVhdGUiLCJhdXRob3JJZCIsIm5hbWUiLCJtYXBTdGF0ZVRvUHJvcHMiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7QUFDOUY7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLGdDQUFnQztBQUNqRyxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUMvTXpFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNUN0Y7O0FBZUE7Ozs7Ozs7Ozs7O0FBV0EsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQjtBQUM1Q0Msa0NBRDRDO0FBRTVDRDtBQUY0QyxHQUEvQjtBQUFBLENBQWYsQyxDQWhDQTs7Ozs7O2tCQXFDZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0QmY7Ozs7Ozs7Ozs7Ozs7QUFmQTs7Ozs7O3NGQTRCQTtBQUFBLFFBQXdCRyxLQUF4QixTQUF3QkEsS0FBeEI7QUFBQSxRQUErQkMsTUFBL0IsU0FBK0JBLE1BQS9CO0FBQUEsUUFBdUNDLFFBQXZDLFNBQXVDQSxRQUF2QztBQUFBLFFBQWlEQyxLQUFqRCxTQUFpREEsS0FBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNILE1BRnJDLGlCQUV1REMsUUFGdkQ7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUUsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxNQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVSO0FBSFIsZUFGa0I7QUFPM0JTLG9CQUFNLHlCQUFlLEVBQUVOLFlBQUYsRUFBZjtBQVBxQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFjWUMsa0JBZFosR0FjbUNELEdBZG5DLENBY1lDLE1BZFosRUFjb0JDLFVBZHBCLEdBY21DRixHQWRuQyxDQWNvQkUsVUFkcEI7O0FBQUEsa0JBZ0JRRCxVQUFVLEdBaEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FpQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFqQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXVCdUJGLElBQUlLLElBQUosRUF2QnZCOztBQUFBO0FBdUJVQSxnQkF2QlY7QUFBQSx3RUF5QmdCQSxJQXpCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTJCSUMsb0JBQVFGLEtBQVI7O0FBM0JKLDZDQTZCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE3Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVqQixNOzs7OztBQXZCZjs7OztBQUNBOzs7O2tCQTBEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pEZjs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1vQixTQUFTO0FBQ2JDLFFBQU07QUFDSkMsYUFBUztBQURMLEdBRE87QUFJYkMsU0FBTztBQUNMQyxjQUFVLEdBREw7QUFFTEMsWUFBUSxtQkFGSDtBQUdMQyxrQkFBYyxLQUhUO0FBSUxDLGFBQVMsS0FKSjtBQUtMLGVBQVc7QUFDVEMsZUFBUyxNQURBO0FBRVRDLG1CQUFhLFNBRko7QUFHVEMsaUJBQVc7QUFIRjtBQUxOO0FBSk0sQ0FBZixDLENBWkE7Ozs7Ozs7QUE2QkEsSUFBTUMsUUFBUSxTQUFSQSxLQUFRO0FBQUEsTUFBR0MsT0FBSCxRQUFHQSxPQUFIO0FBQUEsTUFBWUMsUUFBWixRQUFZQSxRQUFaO0FBQUEsTUFBc0JDLEtBQXRCLFFBQXNCQSxLQUF0QjtBQUFBLE1BQTZCQyxXQUE3QixRQUE2QkEsV0FBN0I7QUFBQSxNQUEwQ0MsUUFBMUMsUUFBMENBLFFBQTFDO0FBQUEsTUFBb0RsQyxJQUFwRCxRQUFvREEsSUFBcEQ7QUFBQSxTQUNaO0FBQUE7QUFBQSxNQUFLLFdBQVc4QixRQUFRWCxJQUF4QjtBQUNFO0FBQ0UsWUFBTW5CLFFBQVEsTUFEaEI7QUFFRSxtQkFBYWlDLGVBQWUsT0FGOUI7QUFHRSxhQUFPRCxLQUhUO0FBSUUsZ0JBQVVELFFBSlo7QUFLRSxpQkFBV0QsUUFBUVQsS0FMckI7QUFNRSxnQkFBVSxDQUFDLENBQUNhO0FBTmQ7QUFERixHQURZO0FBQUEsQ0FBZDs7QUFhQUwsTUFBTU0sU0FBTixHQUFrQjtBQUNoQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFEVjtBQUVoQk4sWUFBVSxvQkFBVU8sSUFBVixDQUFlRCxVQUZUO0FBR2hCTCxTQUFPLG9CQUFVTyxTQUFWLENBQW9CLENBQUMsb0JBQVVDLE1BQVgsRUFBbUIsb0JBQVVDLE1BQTdCLENBQXBCLEVBQTBESixVQUhqRDtBQUloQkosZUFBYSxvQkFBVU8sTUFKUDtBQUtoQnhDLFFBQU0sb0JBQVV3QyxNQUxBO0FBTWhCTixZQUFVLG9CQUFVUTtBQU5KLENBQWxCOztrQkFTZSwwQkFBV3hCLE1BQVgsRUFBbUJXLEtBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pDZjs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUE7OztBQTFCQTs7Ozs7Ozs7OztBQTZCQSxJQUFNWCxTQUFTO0FBQ2JDLFFBQU07QUFDSk0sYUFBUztBQURMLEdBRE87QUFJYmtCLGlCQUFlLEVBSkY7QUFLYkMsUUFBTTtBQUNKbkIsYUFBUztBQURMLEdBTE87QUFRYm9CLGNBQVk7QUFDVjtBQURVLEdBUkM7QUFXYkMsUUFBTTtBQUNKdEIsa0JBQWM7QUFEVixHQVhPO0FBY2J1QixVQUFRO0FBQ052QixrQkFBYztBQURSLEdBZEs7QUFpQmJ3QixhQUFXLEVBakJFO0FBa0JiQyxrQkFBZ0I7QUFDZEMsV0FBTztBQURPLEdBbEJIO0FBcUJiNUIsWUFBVTtBQUNSNkIsVUFBTTtBQURFO0FBckJHLENBQWY7O0lBMEJNQyxjOzs7QUFDSiwwQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLHNKQUNYQSxLQURXOztBQUFBLFVBK0VuQkMsWUEvRW1CLEdBK0VKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBL0VJOztBQUFBLFVBZ0ZuQkMsWUFoRm1CLEdBZ0ZKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBaEZJOztBQUVqQixVQUFLRSxLQUFMLEdBQWE7QUFDWEMsY0FBUSxLQURHO0FBRVh2RCxhQUFPLEVBRkk7QUFHWHdELG1CQUFhLENBSEY7O0FBS1gzQixtQkFBYSxtQkFMRjtBQU1YNEIsYUFBTyxLQU5JO0FBT1gvQyxrQkFBWSxDQVBEO0FBUVhDLGFBQU8sRUFSSTtBQVNYK0MsZUFBUyxFQVRFO0FBVVhOLGFBQU87QUFWSSxLQUFiOztBQWFBLFVBQUt6QixRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY2dDLElBQWQsT0FBaEI7QUFDQSxVQUFLQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0QsSUFBZCxPQUFoQjtBQWhCaUI7QUFpQmxCOzs7OzZCQUVRRSxDLEVBQUc7QUFDVixVQUFNN0QsUUFBUTZELEVBQUVDLE1BQUYsQ0FBU2xDLEtBQXZCO0FBQ0EsVUFBTTRCLGNBQWN4RCxNQUFNK0QsTUFBMUI7QUFDQSxVQUFNTixRQUFRRCxjQUFjLENBQWQsSUFBbUJBLGNBQWMsR0FBL0M7O0FBRUEsV0FBS0wsUUFBTCxDQUFjO0FBQ1puRCxvQkFEWTtBQUVad0QsZ0NBRlk7QUFHWkMsb0JBSFk7QUFJWjlDLGVBQU8sQ0FBQzhDLEtBQUQsR0FBUyw2Q0FBVCxHQUF5RDtBQUpwRCxPQUFkO0FBTUQ7Ozs7MkdBQ2NJLEM7Ozs7Ozs7Ozs7eUJBRWtCLEtBQUtaLEssRUFBMUJlLFEsVUFBQUEsUSxFQUFVQyxNLFVBQUFBLE07aUNBQ1lELFNBQVNFLEksRUFBM0JwRSxNLGtCQUFKcUUsRSxFQUFZdEUsSyxrQkFBQUEsSztBQUNaRSx3QixHQUFha0UsTSxDQUFibEUsUTtBQUVBQyxxQixHQUFVLEtBQUtzRCxLLENBQWZ0RCxLOzs7QUFFUixxQkFBS21ELFFBQUwsQ0FBYyxFQUFFTyxTQUFTLDJCQUFYLEVBQWQ7Ozt1QkFFNkMsc0JBQWtCO0FBQzdEN0QsOEJBRDZEO0FBRTdEQyxnQ0FGNkQ7QUFHN0RDLG9DQUg2RDtBQUk3REM7QUFKNkQsaUJBQWxCLEM7Ozs7QUFBckNVLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPaEIsdUIsU0FBQUEsTzs7c0JBT3ZCZSxjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0EscUJBQUt5QyxRQUFMLENBQWMsRUFBRXpDLHNCQUFGLEVBQWNDLFlBQWQsRUFBZDs7Ozs7QUFJRixxQkFBS3dDLFFBQUwsQ0FBYyxFQUFFTyxTQUFTLHVCQUFYLEVBQWQ7O0FBRUFVLDJCQUFXLFlBQU07QUFDZix5QkFBS2pCLFFBQUwsQ0FBYztBQUNaTSwyQkFBTyxLQURLO0FBRVovQyxnQ0FBWSxDQUZBO0FBR1pDLDJCQUFPLEVBSEs7QUFJWitDLDZCQUFTLEVBSkc7O0FBTVoxRCwyQkFBTyxFQU5LO0FBT1p3RCxpQ0FBYTtBQVBELG1CQUFkO0FBU0QsaUJBVkQsRUFVRyxJQVZIOztBQVlBLHFCQUFLUCxLQUFMLENBQVdvQixvQkFBWDtBQUNFM0Qsd0NBREY7QUFFRTRELDRCQUFVeEU7QUFGWixtQkFHS0gsT0FITDtBQUlFSztBQUpGOzs7Ozs7OztBQU9BYSx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQU9LO0FBQUE7O0FBQUEsbUJBQ3lDLEtBQUsyQyxLQUQ5QztBQUFBLFVBQ0NDLE1BREQsVUFDQ0EsTUFERDtBQUFBLFVBQ1MxQixXQURULFVBQ1NBLFdBRFQ7QUFBQSxVQUNzQjdCLEtBRHRCLFVBQ3NCQSxLQUR0QjtBQUFBLFVBQzZCMEQsT0FEN0IsVUFDNkJBLE9BRDdCO0FBQUEsb0JBRXVCLEtBQUtULEtBRjVCO0FBQUEsVUFFQ3ZCLE9BRkQsV0FFQ0EsT0FGRDtBQUFBLFVBRVVzQyxRQUZWLFdBRVVBLFFBRlY7QUFBQSw0QkFHa0JBLFNBQVNFLElBSDNCO0FBQUEsVUFHQ0ssSUFIRCxtQkFHQ0EsSUFIRDtBQUFBLFVBR081QixNQUhQLG1CQUdPQSxNQUhQOzs7QUFLUCxhQUNFO0FBQUE7QUFBQTtBQUNFLHFCQUFXakIsUUFBUVgsSUFEckI7QUFFRSx3QkFBYyxLQUFLbUMsWUFGckI7QUFHRSx3QkFBYyxLQUFLRztBQUhyQjtBQUtFO0FBQUE7QUFBQSxZQUFNLFdBQVczQixRQUFRZ0IsSUFBekIsRUFBK0IsUUFBUSxLQUFLWSxLQUFMLENBQVdGLEtBQWxEO0FBQ0U7QUFDRSxvQkFDRTtBQUNFLG1CQUFLbUIsSUFEUDtBQUVFLG1CQUFLNUIsVUFBVSxjQUZqQjtBQUdFLHlCQUFXakIsUUFBUWlCO0FBSHJCLGNBRko7QUFRRSxtQkFDRTtBQUNFLHFCQUFPM0MsS0FEVDtBQUVFLHdCQUFVLEtBQUsyQixRQUZqQjtBQUdFLDJCQUFhRTtBQUhmLGNBVEo7QUFlRSx1QkFBV0gsUUFBUWUsVUFmckI7QUFnQkUscUJBQVM7QUFBQSxxQkFBTSxPQUFLVSxRQUFMLENBQWMsRUFBRUksUUFBUSxJQUFWLEVBQWQsQ0FBTjtBQUFBO0FBaEJYLFlBREY7QUFtQkdBLG9CQUFVRyxRQUFRSyxNQUFsQixHQUNDO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFhTDtBQUFiO0FBREYsV0FERCxHQUlHLElBdkJOO0FBd0JHSCxtQkFDQztBQUFBO0FBQUEsY0FBYSwwQkFBYjtBQUNFLG1EQUFLLFdBQVc3QixRQUFRUixRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLGdCQUFRLFNBQVMsS0FBSzBDLFFBQXRCO0FBQUE7QUFBQTtBQUZGLFdBREQsR0FLRztBQTdCTjtBQUxGLE9BREY7QUF1Q0Q7Ozs7O0FBR0haLGVBQWVqQixTQUFmLEdBQTJCO0FBQ3pCTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQUREO0FBRXpCK0IsWUFBVSxvQkFBVWhDLE1BQVYsQ0FBaUJDLFVBRkY7O0FBSXpCb0Msd0JBQXNCLG9CQUFVbkMsSUFBVixDQUFlRDtBQUpaLENBQTNCOztBQU9BLElBQU11QyxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR1IsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBbUIsRUFBRUEsa0JBQUYsRUFBbkI7QUFBQSxDQUF4QjtBQUNBLElBQU1TLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFTDtBQURGLEdBREYsRUFJRUssUUFKRixDQUR5QjtBQUFBLENBQTNCOztrQkFRZSx5QkFBUUYsZUFBUixFQUF5QkMsa0JBQXpCLEVBQ2IsMEJBQVczRCxNQUFYLEVBQW1Ca0MsY0FBbkIsQ0FEYSxDIiwiZmlsZSI6IjYzLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfY29sb3JNYW5pcHVsYXRvciA9IHJlcXVpcmUoJy4uL3N0eWxlcy9jb2xvck1hbmlwdWxhdG9yJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB3aWR0aDogNDAsXG4gICAgICBoZWlnaHQ6IDQwLFxuICAgICAgZm9udEZhbWlseTogdGhlbWUudHlwb2dyYXBoeS5mb250RmFtaWx5LFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyMCksXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckRlZmF1bHQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogKDAsIF9jb2xvck1hbmlwdWxhdG9yLmVtcGhhc2l6ZSkodGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsIDAuMjYpXG4gICAgfSxcbiAgICBpbWc6IHtcbiAgICAgIG1heFdpZHRoOiAnMTAwJScsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnYXV0bycsXG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZCBpbiBjb21iaW5hdGlvbiB3aXRoIGBzcmNgIG9yIGBzcmNTZXRgIHRvXG4gICAqIHByb3ZpZGUgYW4gYWx0IGF0dHJpYnV0ZSBmb3IgdGhlIHJlbmRlcmVkIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBhbHQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWQgdG8gcmVuZGVyIGljb24gb3IgdGV4dCBlbGVtZW50cyBpbnNpZGUgdGhlIEF2YXRhci5cbiAgICogYHNyY2AgYW5kIGBhbHRgIHByb3BzIHdpbGwgbm90IGJlIHVzZWQgYW5kIG5vIGBpbWdgIHdpbGxcbiAgICogYmUgcmVuZGVyZWQgYnkgZGVmYXVsdC5cbiAgICpcbiAgICogVGhpcyBjYW4gYmUgYW4gZWxlbWVudCwgb3IganVzdCBhIHN0cmluZy5cbiAgICovXG4gIGNoaWxkcmVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBUaGUgY2xhc3NOYW1lIG9mIHRoZSBjaGlsZCBlbGVtZW50LlxuICAgKiBVc2VkIGJ5IENoaXAgYW5kIExpc3RJdGVtSWNvbiB0byBzdHlsZSB0aGUgQXZhdGFyIGljb24uXG4gICAqL1xuICBjaGlsZHJlbkNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgZm9yIHRoZSByb290IG5vZGUuXG4gICAqIEVpdGhlciBhIHN0cmluZyB0byB1c2UgYSBET00gZWxlbWVudCBvciBhIGNvbXBvbmVudC5cbiAgICovXG4gIGNvbXBvbmVudDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvcGVydGllcyBhcHBsaWVkIHRvIHRoZSBgaW1nYCBlbGVtZW50IHdoZW4gdGhlIGNvbXBvbmVudFxuICAgKiBpcyB1c2VkIHRvIGRpc3BsYXkgYW4gaW1hZ2UuXG4gICAqL1xuICBpbWdQcm9wczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGBzaXplc2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNpemVzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY2AgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyYzogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGBzcmNTZXRgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzcmNTZXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cbnZhciBBdmF0YXIgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShBdmF0YXIsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEF2YXRhcigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBBdmF0YXIpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChBdmF0YXIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEF2YXRhcikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQXZhdGFyLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFsdCA9IF9wcm9wcy5hbHQsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNoaWxkcmVuUHJvcCA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjaGlsZHJlbkNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2hpbGRyZW5DbGFzc05hbWUsXG4gICAgICAgICAgQ29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgaW1nUHJvcHMgPSBfcHJvcHMuaW1nUHJvcHMsXG4gICAgICAgICAgc2l6ZXMgPSBfcHJvcHMuc2l6ZXMsXG4gICAgICAgICAgc3JjID0gX3Byb3BzLnNyYyxcbiAgICAgICAgICBzcmNTZXQgPSBfcHJvcHMuc3JjU2V0LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2FsdCcsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjaGlsZHJlbicsICdjaGlsZHJlbkNsYXNzTmFtZScsICdjb21wb25lbnQnLCAnaW1nUHJvcHMnLCAnc2l6ZXMnLCAnc3JjJywgJ3NyY1NldCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmNvbG9yRGVmYXVsdCwgY2hpbGRyZW5Qcm9wICYmICFzcmMgJiYgIXNyY1NldCksIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIGNoaWxkcmVuID0gbnVsbDtcblxuICAgICAgaWYgKGNoaWxkcmVuUHJvcCkge1xuICAgICAgICBpZiAoY2hpbGRyZW5DbGFzc05hbWVQcm9wICYmIHR5cGVvZiBjaGlsZHJlblByb3AgIT09ICdzdHJpbmcnICYmIF9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChjaGlsZHJlblByb3ApKSB7XG4gICAgICAgICAgdmFyIF9jaGlsZHJlbkNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2hpbGRyZW5DbGFzc05hbWVQcm9wLCBjaGlsZHJlblByb3AucHJvcHMuY2xhc3NOYW1lKTtcbiAgICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGRyZW5Qcm9wLCB7IGNsYXNzTmFtZTogX2NoaWxkcmVuQ2xhc3NOYW1lIH0pO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGNoaWxkcmVuID0gY2hpbGRyZW5Qcm9wO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgaWYgKHNyYyB8fCBzcmNTZXQpIHtcbiAgICAgICAgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnaW1nJywgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgYWx0OiBhbHQsXG4gICAgICAgICAgc3JjOiBzcmMsXG4gICAgICAgICAgc3JjU2V0OiBzcmNTZXQsXG4gICAgICAgICAgc2l6ZXM6IHNpemVzLFxuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3Nlcy5pbWdcbiAgICAgICAgfSwgaW1nUHJvcHMpKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEF2YXRhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkF2YXRhci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbXBvbmVudDogJ2Rpdidcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQXZhdGFyJyB9KShBdmF0YXIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgNiAyNCAyNiAyNyAyOCAzMSAzMyAzNCAzNSAzNiAzNyAzOSA0MCA0MyA0OSA1NCA1NyA1OCA1OSA2MSA2MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9BdmF0YXIgPSByZXF1aXJlKCcuL0F2YXRhcicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9BdmF0YXIpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA0IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU3IDU4IDU5IDYxIDYzIiwiLyoqXG4gKiBDcmVhdGUgYSBxdWVzdGlvbiBpbiBDb3Vyc2UgRGlzY3Vzc2lvbi5cbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgUVVFU1RJT05fQ1JFQVRFIH0gZnJvbSAnLi4vLi4vLi4vLi4vY29uc3RhbnRzL2NvdXJzZXMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIHF1ZXN0aW9uSWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgdGltZXN0YW1wOiBzdHJpbmcsXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBUaGlzIGFjdGlvbiB3aWxsIGNyZWF0ZSBhIHF1ZXN0aW9uIGluIGRpc2N1c3Npb24gb2YgY291cnNlXG4gKiBAZnVuY3Rpb24gY3JlYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5hdXRob3JJZCAtIGF1dGhvciBvZiBhIHF1ZXN0aW9uLlxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucXVlc3Rpb25JZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aW1lc3RhbXAgLVxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgY3JlYXRlID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHtcbiAgdHlwZTogUVVFU1RJT05fQ1JFQVRFLFxuICBwYXlsb2FkLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VzZXJzL2Rpc2N1c3Npb24vY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHRva2VuOiBzdHJpbmcsXG4gIHVzZXJJZDogbnVtYmVyLFxuICBjb3Vyc2VJZDogbnVtYmVyLFxuICB0aXRsZTogc3RyaW5nLFxufTtcblxuLyoqXG4gKiBDcmVhdGUgTW9kdWxlc1xuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gY3JlYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHJldHVybiB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBjcmVhdGUoeyB0b2tlbiwgdXNlcklkLCBjb3Vyc2VJZCwgdGl0bGUgfTogUGF5bG9hZCkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vY291cnNlcy8ke2NvdXJzZUlkfS9kaXNjdXNzaW9uYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHRpdGxlIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvZGlzY3Vzc2lvbi9jcmVhdGUuanMiLCIvKipcbiAqIFRoaXMgaW5wdXQgY29tcG9uZW50IGlzIG1haW5seSBkZXZlbG9wZWQgZm9yIGNyZWF0ZSBwb3N0LCBjcmVhdGVcbiAqIGNvdXJzZSBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgfSxcbiAgaW5wdXQ6IHtcbiAgICBmbGV4R3JvdzogJzEnLFxuICAgIGJvcmRlcjogJzJweCBzb2xpZCAjZGFkYWRhJyxcbiAgICBib3JkZXJSYWRpdXM6ICc3cHgnLFxuICAgIHBhZGRpbmc6ICc4cHgnLFxuICAgICcmOmZvY3VzJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICcjOWVjYWVkJyxcbiAgICAgIGJveFNoYWRvdzogJzAgMCAxMHB4ICM5ZWNhZWQnLFxuICAgIH0sXG4gIH0sXG59O1xuXG5jb25zdCBJbnB1dCA9ICh7IGNsYXNzZXMsIG9uQ2hhbmdlLCB2YWx1ZSwgcGxhY2Vob2xkZXIsIGRpc2FibGVkLCB0eXBlIH0pID0+IChcbiAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgPGlucHV0XG4gICAgICB0eXBlPXt0eXBlIHx8ICd0ZXh0J31cbiAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlciB8fCAnaW5wdXQnfVxuICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgZGlzYWJsZWQ9eyEhZGlzYWJsZWR9XG4gICAgLz5cbiAgPC9kaXY+XG4pO1xuXG5JbnB1dC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIHZhbHVlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSkuaXNSZXF1aXJlZCxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShJbnB1dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCIvKipcbiAqIFF1ZXN0aW9uQ3JlYXRlIGNvbXBvbmVudCB3aWxsIGJlIHVzZWQgdG8gY3JlYXRlIG5ldyBxdWVzdGlvbi5cbiAqXG4gKiBOb3RlOiBiZWZvcmUgYWRkaW5nIHRoaXMgY29tcG9uZW50IGluIGFueSBwbGFjZSwgbWFrZSBzdXJlXG4gKiB0aGUgcGVyc29uIHdobyBpcyBhY2Nlc3NpbmcgdGhpcyBjb21wb25lbnQgc2hvdWxkIGhhdmUgcmlnaHQgdG8gY3JlYXRlIHF1ZXN0aW9uLlxuICogaS5lLiBwZXJzb24gc2hvdWxkIGJlIGxvZ2dlZCBpbiBhbmQgdXNlciBjYW4gY3JlYXRlIHF1ZXN0aW9uIGZvciBhbnkgY291cnNlLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEF2YXRhciBmcm9tICdtYXRlcmlhbC11aS9BdmF0YXInO1xuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnbWF0ZXJpYWwtdWkvVHlwb2dyYXBoeSc7XG5pbXBvcnQgQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0J1dHRvbic7XG5cbmltcG9ydCBJbnB1dCBmcm9tICcuLi8uLi8uLi9jb21wb25lbnRzL0lucHV0JztcblxuaW1wb3J0IGFjdGlvblF1ZXN0aW9uQ3JlYXRlIGZyb20gJy4uLy4uLy4uL2FjdGlvbnMvY291cnNlcy91c2Vycy9kaXNjdXNzaW9uL2NyZWF0ZSc7XG5pbXBvcnQgYXBpUXVlc3Rpb25DcmVhdGUgZnJvbSAnLi4vLi4vLi4vYXBpL2NvdXJzZXMvdXNlcnMvZGlzY3Vzc2lvbi9jcmVhdGUnO1xuXG4vKipcbiAqIERlZmluZSBhbGwgc3R5bGVzIGhlcmUgZm9yIFBvc3RDcmVhdGUgQ29tcG9uZW50LlxuICovXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBncmlkQ29udGFpbmVyOiB7fSxcbiAgcG9zdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmRIZWFkZXI6IHtcbiAgICAvLyB0ZXh0QWxpZ246ICdjZW50ZXInXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBjYXJkTWVkaWE6IHt9LFxuICBjYXJkTWVkaWFJbWFnZToge1xuICAgIHdpZHRoOiAnMTAwJScsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbn07XG5cbmNsYXNzIFF1ZXN0aW9uQ3JlYXRlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGFjdGl2ZTogZmFsc2UsXG4gICAgICB0aXRsZTogJycsXG4gICAgICB0aXRsZUxlbmd0aDogMCxcblxuICAgICAgcGxhY2Vob2xkZXI6ICdRdWVzdGlvbiBUaXRsZS4uLicsXG4gICAgICB2YWxpZDogZmFsc2UsXG4gICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgZXJyb3I6ICcnLFxuICAgICAgbWVzc2FnZTogJycsXG4gICAgICBob3ZlcjogZmFsc2UsXG4gICAgfTtcblxuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblN1Ym1pdCA9IHRoaXMub25TdWJtaXQuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKGUpIHtcbiAgICBjb25zdCB0aXRsZSA9IGUudGFyZ2V0LnZhbHVlO1xuICAgIGNvbnN0IHRpdGxlTGVuZ3RoID0gdGl0bGUubGVuZ3RoO1xuICAgIGNvbnN0IHZhbGlkID0gdGl0bGVMZW5ndGggPiAwICYmIHRpdGxlTGVuZ3RoIDwgMTQ4O1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB0aXRsZSxcbiAgICAgIHRpdGxlTGVuZ3RoLFxuICAgICAgdmFsaWQsXG4gICAgICBlcnJvcjogIXZhbGlkID8gJ1RpdGxlIGxlbmd0aCBzaG91bGQgYmUgaW4gYmV0d2VlbiAxIHRvIDE0OC4nIDogJycsXG4gICAgfSk7XG4gIH1cbiAgYXN5bmMgb25TdWJtaXQoZSkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGVsZW1lbnRzLCBjb3Vyc2UgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IGlkOiB1c2VySWQsIHRva2VuIH0gPSBlbGVtZW50cy51c2VyO1xuICAgICAgY29uc3QgeyBjb3Vyc2VJZCB9ID0gY291cnNlO1xuXG4gICAgICBjb25zdCB7IHRpdGxlIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ0NyZWF0aW5nIGNvdXJzZSBtb2R1bGUuLi4nIH0pO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCBhcGlRdWVzdGlvbkNyZWF0ZSh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIGNvdXJzZUlkLFxuICAgICAgICB0aXRsZSxcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgLy8gaGFuZGxlIGVycm9yXG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBzdGF0dXNDb2RlLCBlcnJvciB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ1N1Y2Nlc3NmdWxseSBDcmVhdGVkLicgfSk7XG5cbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICB2YWxpZDogZmFsc2UsXG4gICAgICAgICAgc3RhdHVzQ29kZTogMCxcbiAgICAgICAgICBlcnJvcjogJycsXG4gICAgICAgICAgbWVzc2FnZTogJycsXG5cbiAgICAgICAgICB0aXRsZTogJycsXG4gICAgICAgICAgdGl0bGVMZW5ndGg6IDAsXG4gICAgICAgIH0pO1xuICAgICAgfSwgMTUwMCk7XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uUXVlc3Rpb25DcmVhdGUoe1xuICAgICAgICBzdGF0dXNDb2RlLFxuICAgICAgICBhdXRob3JJZDogdXNlcklkLFxuICAgICAgICAuLi5wYXlsb2FkLFxuICAgICAgICB0aXRsZSxcbiAgICAgIH0pO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICB9XG4gIH1cblxuICBvbk1vdXNlRW50ZXIgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IHRydWUgfSk7XG4gIG9uTW91c2VMZWF2ZSA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogZmFsc2UgfSk7XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgYWN0aXZlLCBwbGFjZWhvbGRlciwgdGl0bGUsIG1lc3NhZ2UgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBlbGVtZW50cyB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IG5hbWUsIGF2YXRhciB9ID0gZWxlbWVudHMudXNlcjtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgPlxuICAgICAgICA8Q2FyZCBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0gcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfT5cbiAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgPEF2YXRhclxuICAgICAgICAgICAgICAgIGFsdD17bmFtZX1cbiAgICAgICAgICAgICAgICBzcmM9e2F2YXRhciB8fCAnL2Zhdmljb24uaWNvJ31cbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICA8SW5wdXRcbiAgICAgICAgICAgICAgICB2YWx1ZT17dGl0bGV9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2V9XG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmRIZWFkZXJ9XG4gICAgICAgICAgICBvbkZvY3VzPXsoKSA9PiB0aGlzLnNldFN0YXRlKHsgYWN0aXZlOiB0cnVlIH0pfVxuICAgICAgICAgIC8+XG4gICAgICAgICAge2FjdGl2ZSAmJiBtZXNzYWdlLmxlbmd0aCA/IChcbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHk+e21lc3NhZ2V9PC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICB7YWN0aXZlID8gKFxuICAgICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgICAgPEJ1dHRvbiBvbkNsaWNrPXt0aGlzLm9uU3VibWl0fT5DcmVhdGU8L0J1dHRvbj5cbiAgICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIDwvQ2FyZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuUXVlc3Rpb25DcmVhdGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgYWN0aW9uUXVlc3Rpb25DcmVhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cyB9KSA9PiAoeyBlbGVtZW50cyB9KTtcbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvblF1ZXN0aW9uQ3JlYXRlLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoUXVlc3Rpb25DcmVhdGUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL0Rpc2N1c3Npb24vUXVlc3Rpb25DcmVhdGUuanMiXSwic291cmNlUm9vdCI6IiJ9