webpackJsonp([16],{

/***/ "./node_modules/material-ui-icons/NavigateNext.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/NavigateNext.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z' });

var NavigateNext = function NavigateNext(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

NavigateNext = (0, _pure2.default)(NavigateNext);
NavigateNext.muiName = 'SvgIcon';

exports.default = NavigateNext;

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/components/NavigationButtonNext.js":
/*!*******************************************************!*\
  !*** ./src/client/components/NavigationButtonNext.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _NavigateNext = __webpack_require__(/*! material-ui-icons/NavigateNext */ "./node_modules/material-ui-icons/NavigateNext.js");

var _NavigateNext2 = _interopRequireDefault(_NavigateNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component is create to show naviagtion button to move next page
 *
 * @format
 */

var styles = function styles(theme) {
  var _root;

  return {
    root: (_root = {
      position: 'fixed',
      right: '2%',
      bottom: '4%'
    }, (0, _defineProperty3.default)(_root, theme.breakpoints.down('md'), {
      right: '2%',
      bottom: '2%'
    }), (0, _defineProperty3.default)(_root, theme.breakpoints.down('sm'), {
      right: '2%',
      bottom: '1%'
    }), _root)
  };
};

/**
 *
 * @param {object} classes
 * @param {path} to
 */
var NavigateButtonNext = function NavigateButtonNext(_ref) {
  var classes = _ref.classes,
      _ref$to = _ref.to,
      to = _ref$to === undefined ? '/' : _ref$to;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement(
      _Button2.default,
      {
        fab: true,
        color: 'accent',
        component: _Link2.default,
        className: classes.button,
        raised: true,
        to: to
      },
      _react2.default.createElement(_NavigateNext2.default, null)
    )
  );
};

NavigateButtonNext.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  to: _propTypes2.default.string.isRequired
};

NavigateButtonNext.defaultProps = {
  to: '/'
};

exports.default = (0, _styles.withStyles)(styles)(NavigateButtonNext);

/***/ }),

/***/ "./src/client/pages/Introduction.js":
/*!******************************************!*\
  !*** ./src/client/pages/Introduction.js ***!
  \******************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _NavigationButtonNext = __webpack_require__(/*! ../components/NavigationButtonNext */ "./src/client/components/NavigationButtonNext.js");

var _NavigationButtonNext2 = _interopRequireDefault(_NavigationButtonNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This is the introduction page of mayash.io
 *
 * @format
 */

var styles = function styles(theme) {
  var _content, _about, _title, _subheader;

  return {
    root: {},
    gridItem: {
      paddingBottom: '5px'
    },
    cardHeader: {
      textAlign: 'center',
      height: '20vh',
      backgroundImage: 'url("https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg")',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat'
    },
    paragraph: {
      textAlign: 'center'
    },
    // expand: {
    //   transform: 'rotate(0deg)',
    //   transition: theme.transitions.create('transform', {
    //     duration: theme.transitions.duration.shortest,
    //   }),
    // },
    content: (_content = {
      lineHeight: '25px',
      fontSize: '1rem',
      paddingBottom: '1rem'
    }, (0, _defineProperty3.default)(_content, theme.breakpoints.up('xl'), {
      paddingLeft: '12em',
      paddingRight: '12em'
    }), (0, _defineProperty3.default)(_content, theme.breakpoints.down('xl'), {
      paddingLeft: '12em',
      paddingRight: '12em'
    }), (0, _defineProperty3.default)(_content, theme.breakpoints.down('lg'), {
      paddingLeft: '8em',
      paddingRight: '8em'
    }), (0, _defineProperty3.default)(_content, theme.breakpoints.down('md'), {
      paddingLeft: '3em',
      paddingRight: '3em'
    }), (0, _defineProperty3.default)(_content, theme.breakpoints.down('sm'), {
      paddingLeft: '4px',
      paddingRight: '4px'
    }), (0, _defineProperty3.default)(_content, theme.breakpoints.down('xs'), {
      paddingLeft: '4px',
      paddingRight: '4px'
    }), _content),
    about: (_about = {}, (0, _defineProperty3.default)(_about, theme.breakpoints.up('xl'), {
      paddingLeft: '12rem',
      paddingRight: '12rem'
    }), (0, _defineProperty3.default)(_about, theme.breakpoints.down('xl'), {
      paddingLeft: '12rem',
      paddingRight: '12rem'
    }), (0, _defineProperty3.default)(_about, theme.breakpoints.down('lg'), {
      paddingLeft: '8rem',
      paddingRight: '8rem'
    }), (0, _defineProperty3.default)(_about, theme.breakpoints.down('md'), {
      paddingLeft: '3rem',
      paddingRight: '3rem'
    }), (0, _defineProperty3.default)(_about, theme.breakpoints.down('sm'), {
      paddingLeft: '4px',
      paddingRight: '4px'
    }), (0, _defineProperty3.default)(_about, theme.breakpoints.down('xs'), {
      paddingLeft: '4px',
      paddingRight: '4px'
    }), _about),
    title: (_title = {
      fontWeight: theme.typography.fontWeightMedium,
      color: theme.palette.getContrastText(theme.palette.primary[700])
    }, (0, _defineProperty3.default)(_title, theme.breakpoints.down('sm'), {
      fontSize: 30
    }), (0, _defineProperty3.default)(_title, theme.breakpoints.down('xs'), {
      fontSize: 30
    }), _title),
    subheader: (_subheader = {
      color: theme.palette.getContrastText(theme.palette.primary[700])
    }, (0, _defineProperty3.default)(_subheader, theme.breakpoints.down('md'), {
      fontSize: 16
    }), (0, _defineProperty3.default)(_subheader, theme.breakpoints.down('sm'), {
      fontSize: 16
    }), (0, _defineProperty3.default)(_subheader, theme.breakpoints.down('xs'), {
      fontSize: 16
    }), _subheader)
    // introductionHeader: {
    //   backgroundImage:
    //     'url("https://storage.googleapis.com/mayash-web/drive/26.jpg")',
    //   backgroundPosition: 'center',
    //   backgroundRepeat: 'no-repeat',
    //   [theme.breakpoints.up('xl')]: {},
    //   [theme.breakpoints.down('xl')]: {},
    //   [theme.breakpoints.down('lg')]: {},
    //   [theme.breakpoints.down('md')]: {},
    //   [theme.breakpoints.down('sm')]: {},
    //   [theme.breakpoints.down('xs')]: {},
    // },
    // aboutUsHeader: {
    //   backgroundImage:
    //     'url("https://storage.googleapis.com/mayash-web/drive/27.jpg")',
    //   backgroundAttachment: 'fixed',
    //   backgroundPosition: 'center',
    //   backgroundRepeat: 'no-repeat',
    //   backgroundSize: 'cover',
    //   [theme.breakpoints.up('xl')]: {},
    //   [theme.breakpoints.down('xl')]: {},
    //   [theme.breakpoints.down('lg')]: {},
    //   [theme.breakpoints.down('md')]: {},
    //   [theme.breakpoints.down('sm')]: {},
    //   [theme.breakpoints.down('xs')]: {},
    // },
    // motivationHeader: {
    //   backgroundImage:
    //     'url("https://storage.googleapis.com/mayash-web/drive/28.jpg")',
    //   backgroundAttachment: 'fixed',
    //   backgroundPosition: 'center',
    //   backgroundRepeat: 'no-repeat',
    //   backgroundSize: 'cover',
    //   [theme.breakpoints.up('xl')]: {},
    //   [theme.breakpoints.down('xl')]: {},
    //   [theme.breakpoints.down('lg')]: {},
    //   [theme.breakpoints.down('md')]: {},
    //   [theme.breakpoints.down('sm')]: {},
    //   [theme.breakpoints.down('xs')]: {},
    // },
    // visionHeader: {
    //   backgroundImage:
    //     'url("https://storage.googleapis.com/mayash-web/drive/29.jpg")',
    //   backgroundAttachment: 'fixed',
    //   backgroundPosition: 'center',
    //   backgroundRepeat: 'no-repeat',
    //   backgroundSize: 'cover',
    //   [theme.breakpoints.up('xl')]: {},
    //   [theme.breakpoints.down('xl')]: {},
    //   [theme.breakpoints.down('lg')]: {},
    //   [theme.breakpoints.down('md')]: {},
    //   [theme.breakpoints.down('sm')]: {},
    //   [theme.breakpoints.down('xs')]: {},
    // },
    // missionHeader: {
    //   backgroundImage:
    //     'url("https://storage.googleapis.com/mayash-web/drive/30.jpg")',
    //   backgroundAttachment: 'fixed',
    //   backgroundPosition: 'center',
    //   backgroundRepeat: 'no-repeat',
    //   backgroundSize: 'cover',
    //   [theme.breakpoints.up('xl')]: {},
    //   [theme.breakpoints.down('xl')]: {},
    //   [theme.breakpoints.down('lg')]: {},
    //   [theme.breakpoints.down('md')]: {},
    //   [theme.breakpoints.down('sm')]: {},
    //   [theme.breakpoints.down('xs')]: {},
    // },
  };
};
// import Collapse from 'material-ui/transitions/Collapse';

var Introduction = function (_Component) {
  (0, _inherits3.default)(Introduction, _Component);

  function Introduction(props) {
    (0, _classCallCheck3.default)(this, Introduction);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Introduction.__proto__ || (0, _getPrototypeOf2.default)(Introduction)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Introduction, [{
    key: 'render',
    value: function render() {
      var classes = this.props.classes;


      var Title = function Title(_ref) {
        var children = _ref.children;
        return _react2.default.createElement(
          _Typography2.default,
          { className: classes.title, type: 'display3' },
          children
        );
      };
      var SubHeader = function SubHeader(_ref2) {
        var children = _ref2.children;
        return _react2.default.createElement(
          _Typography2.default,
          { className: classes.subheader, type: 'headline' },
          children
        );
      };

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _Card2.default,
          { raised: true },
          _react2.default.createElement(_Card.CardHeader, {
            title: _react2.default.createElement(
              Title,
              null,
              'MAYASH'
            ),
            subheader: _react2.default.createElement(
              SubHeader,
              null,
              'Our mission is to provide world class education to everyone.'
            ),
            className: classes.cardHeader
            // aria-expanded={intro}
            , 'aria-label': 'Introduction'
          }),
          _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement('hr', null),
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'body1',
                align: 'justify',
                className: classes.content,
                gutterBottom: true
              },
              'Mayash is an educational platform based on the concept of learning and sharing. Mayash strives to provide quality content in every field of education to the students,incorporating the teaching material from the various professors of this country and the latest stuff from the organizations. Mayash is a one stop solution for all your requirements, doubts and queries. We have the top notch people of the country who are ready to help you in the best possible way.'
            )
          ),
          _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'display1',
                className: classes.about
                // align="left"
              },
              'About Us',
              _react2.default.createElement('hr', null)
            ),
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'body1',
                className: classes.content,
                align: 'justify'
              },
              'Mayash is a team of dedicated people who have made it their mission to solve some of the major problems which our society is facing right now. We are making a simple but effective platform for learning and sharing knowledge consisting of two schemas :',
              _react2.default.createElement('br', null)
            ),
            _react2.default.createElement(
              _Typography2.default,
              { type: 'display3', className: classes.content },
              '1. Learn what you need.',
              _react2.default.createElement('br', null),
              '2. share what you can.',
              _react2.default.createElement('br', null)
            ),
            _react2.default.createElement(
              _Typography2.default,
              { className: classes.content },
              'We are working towards the betterment of our educational system, where we will provide updated knowledge and the latest technologies for users to help them upgrade their skills and be ahead in this competitive ecosystem.'
            ),
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'body1',
                className: classes.content,
                align: 'justify'
              },
              'Founded by an IIT Dhanbad dropout and supported by a team of IITians, Mayash is focussed on the improvement of education system of our country by making the users technically competent and hence, making them better than the competition.'
            )
          ),
          _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'body1',
                className: classes.content,
                component: 'div',
                align: 'justify'
              },
              _react2.default.createElement(
                _Typography2.default,
                { type: 'title' },
                'Persisting drawbacks of current Education system :',
                _react2.default.createElement('hr', null)
              ),
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Quantitative orientation of Education system where Quality is compromised.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Decade old syllabus whereas the technology gets manifold changes every year'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Education is focused more on theoretical aspect rather than on practical understanding.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Students prefer to take tuitions, though there are schools present, just to stay ahead in the rat race of getting good marks.'
                )
              )
            ),
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'body1',
                className: classes.content,
                component: 'div'
              },
              _react2.default.createElement(
                _Typography2.default,
                { type: 'title' },
                'Solution - Mayash:',
                _react2.default.createElement('hr', null)
              ),
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'Mayash is a platform which will cover all the fields of education, for all age groups.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We are connecting students/teachers/professionals from multiple colleges/ universities/ corporates on a single platform where they can easily learn and share their knowledge.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'We will create a discussion channel where students can get their doubts and queries cleared.'
                )
              )
            )
          ),
          _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              { type: 'display1', className: classes.about },
              'Why Mayash?',
              _react2.default.createElement('hr', null)
            ),
            _react2.default.createElement(
              _Typography2.default,
              {
                type: 'body1',
                className: classes.content,
                component: 'div'
              },
              'Today the We are facing the following problems :',
              _react2.default.createElement(
                'ul',
                null,
                'Students:-',
                _react2.default.createElement(
                  'li',
                  null,
                  'Costly Education'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Lack of practical understanding.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'High Student-Teacher ratio.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Outdated curriculum.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Rapid change in technology'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Lack of competent teachers.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Lack of field specialist mentors.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Lack of Industrial exposure.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Language barrier..'
                )
              ),
              _react2.default.createElement('br', null),
              _react2.default.createElement(
                'ul',
                null,
                'professors:-',
                _react2.default.createElement(
                  'li',
                  null,
                  'Staying updated with latest technologies is difficult.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'High academic load.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Difficult to address to a large no. of students'
                )
              ),
              _react2.default.createElement('br', null),
              _react2.default.createElement(
                'ul',
                null,
                'Educational Institutes:-',
                _react2.default.createElement(
                  'li',
                  null,
                  'Difficult to ensure skilled faculties.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'It is costly to hire Industrial experts.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'It is costly to create skill development programs.'
                )
              ),
              _react2.default.createElement('br', null),
              _react2.default.createElement(
                'ul',
                null,
                'Organisations:-',
                _react2.default.createElement(
                  'li',
                  null,
                  ' It is costly to train freshers.'
                ),
                _react2.default.createElement(
                  'li',
                  null,
                  'Hiring procedure is long.'
                )
              )
            )
          ),
          _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              { type: 'body1', className: classes.content },
              'Aim of mayash is to solve all the above problems. Our Vision is to improve the education system of our country. Our mission is to bring out best skills of students as well as teachers so that they all can grow collaboratively and bring out the best from every individual connected with Mayash.'
            ),
            _react2.default.createElement('hr', null),
            _react2.default.createElement('hr', null)
          )
        ),
        _react2.default.createElement(_NavigationButtonNext2.default, { to: '/services' })
      );
    }
  }]);
  return Introduction;
}(_react.Component);

Introduction.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Introduction);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTmF2aWdhdGVOZXh0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29tcG9uZW50cy9OYXZpZ2F0aW9uQnV0dG9uTmV4dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L3BhZ2VzL0ludHJvZHVjdGlvbi5qcyJdLCJuYW1lcyI6WyJzdHlsZXMiLCJ0aGVtZSIsInJvb3QiLCJwb3NpdGlvbiIsInJpZ2h0IiwiYm90dG9tIiwiYnJlYWtwb2ludHMiLCJkb3duIiwiTmF2aWdhdGVCdXR0b25OZXh0IiwiY2xhc3NlcyIsInRvIiwiYnV0dG9uIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsInN0cmluZyIsImRlZmF1bHRQcm9wcyIsImdyaWRJdGVtIiwicGFkZGluZ0JvdHRvbSIsImNhcmRIZWFkZXIiLCJ0ZXh0QWxpZ24iLCJoZWlnaHQiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJiYWNrZ3JvdW5kUG9zaXRpb24iLCJiYWNrZ3JvdW5kUmVwZWF0IiwicGFyYWdyYXBoIiwiY29udGVudCIsImxpbmVIZWlnaHQiLCJmb250U2l6ZSIsInVwIiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJhYm91dCIsInRpdGxlIiwiZm9udFdlaWdodCIsInR5cG9ncmFwaHkiLCJmb250V2VpZ2h0TWVkaXVtIiwiY29sb3IiLCJwYWxldHRlIiwiZ2V0Q29udHJhc3RUZXh0IiwicHJpbWFyeSIsInN1YmhlYWRlciIsIkludHJvZHVjdGlvbiIsInByb3BzIiwic3RhdGUiLCJUaXRsZSIsImNoaWxkcmVuIiwiU3ViSGVhZGVyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsc0RBQXNEOztBQUV4RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7O0FBRTlGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQscUJBQXFCLFc7Ozs7Ozs7Ozs7Ozs7QUNsTDFFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDhDQUE4QyxpQkFBaUIscUJBQXFCLG9DQUFvQyw2REFBNkQsb0JBQW9CLEVBQUUsZUFBZTs7QUFFMU4saURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0ZBQWdGOztBQUVoRjs7QUFFQSxnRkFBZ0YsZUFBZTs7QUFFL0YseURBQXlELFVBQVUsdURBQXVEO0FBQzFIOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSx1Qjs7Ozs7Ozs7Ozs7OztBQzdHQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNmQTs7QUFFQTs7QUFFQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSw4RDs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDekRBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsVUFBVSxxQkFBcUI7QUFDNUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7OztBQUVBOzs7Ozs7QUFiQTs7Ozs7O0FBZUEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQztBQUNFQyxnQkFBVSxPQURaO0FBRUVDLGFBQU8sSUFGVDtBQUdFQyxjQUFRO0FBSFYsNENBSUdKLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBSkgsRUFJa0M7QUFDOUJILGFBQU8sSUFEdUI7QUFFOUJDLGNBQVE7QUFGc0IsS0FKbEMsd0NBUUdKLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBUkgsRUFRa0M7QUFDOUJILGFBQU8sSUFEdUI7QUFFOUJDLGNBQVE7QUFGc0IsS0FSbEM7QUFEeUIsR0FBWjtBQUFBLENBQWY7O0FBZ0JBOzs7OztBQUtBLElBQU1HLHFCQUFxQixTQUFyQkEsa0JBQXFCO0FBQUEsTUFBR0MsT0FBSCxRQUFHQSxPQUFIO0FBQUEscUJBQVlDLEVBQVo7QUFBQSxNQUFZQSxFQUFaLDJCQUFpQixHQUFqQjtBQUFBLFNBQ3pCO0FBQUE7QUFBQSxNQUFLLFdBQVdELFFBQVFQLElBQXhCO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsaUJBREY7QUFFRSxlQUFNLFFBRlI7QUFHRSxpQ0FIRjtBQUlFLG1CQUFXTyxRQUFRRSxNQUpyQjtBQUtFLG9CQUxGO0FBTUUsWUFBSUQ7QUFOTjtBQVFFO0FBUkY7QUFERixHQUR5QjtBQUFBLENBQTNCOztBQWVBRixtQkFBbUJJLFNBQW5CLEdBQStCO0FBQzdCSCxXQUFTLG9CQUFVSSxNQUFWLENBQWlCQyxVQURHO0FBRTdCSixNQUFJLG9CQUFVSyxNQUFWLENBQWlCRDtBQUZRLENBQS9COztBQUtBTixtQkFBbUJRLFlBQW5CLEdBQWtDO0FBQ2hDTixNQUFJO0FBRDRCLENBQWxDOztrQkFJZSx3QkFBV1YsTUFBWCxFQUFtQlEsa0JBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0RGY7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFHQTs7Ozs7O0FBaEJBOzs7Ozs7QUFrQkEsSUFBTVIsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNLEVBRG1CO0FBRXpCZSxjQUFVO0FBQ1JDLHFCQUFlO0FBRFAsS0FGZTtBQUt6QkMsZ0JBQVk7QUFDVkMsaUJBQVcsUUFERDtBQUVWQyxjQUFRLE1BRkU7QUFHVkMsdUJBQ0UsK0VBSlE7QUFLVkMsMEJBQW9CLFFBTFY7QUFNVkMsd0JBQWtCO0FBTlIsS0FMYTtBQWF6QkMsZUFBVztBQUNUTCxpQkFBVztBQURGLEtBYmM7QUFnQnpCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBTTtBQUNFQyxrQkFBWSxNQURkO0FBRUVDLGdCQUFVLE1BRlo7QUFHRVYscUJBQWU7QUFIakIsK0NBSUdqQixNQUFNSyxXQUFOLENBQWtCdUIsRUFBbEIsQ0FBcUIsSUFBckIsQ0FKSCxFQUlnQztBQUM1QkMsbUJBQWEsTUFEZTtBQUU1QkMsb0JBQWM7QUFGYyxLQUpoQywyQ0FRRzlCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBUkgsRUFRa0M7QUFDOUJ1QixtQkFBYSxNQURpQjtBQUU5QkMsb0JBQWM7QUFGZ0IsS0FSbEMsMkNBWUc5QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQVpILEVBWWtDO0FBQzlCdUIsbUJBQWEsS0FEaUI7QUFFOUJDLG9CQUFjO0FBRmdCLEtBWmxDLDJDQWdCRzlCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBaEJILEVBZ0JrQztBQUM5QnVCLG1CQUFhLEtBRGlCO0FBRTlCQyxvQkFBYztBQUZnQixLQWhCbEMsMkNBb0JHOUIsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FwQkgsRUFvQmtDO0FBQzlCdUIsbUJBQWEsS0FEaUI7QUFFOUJDLG9CQUFjO0FBRmdCLEtBcEJsQywyQ0F3Qkc5QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQXhCSCxFQXdCa0M7QUFDOUJ1QixtQkFBYSxLQURpQjtBQUU5QkMsb0JBQWM7QUFGZ0IsS0F4QmxDLFlBdEJ5QjtBQW1EekJDLCtEQUNHL0IsTUFBTUssV0FBTixDQUFrQnVCLEVBQWxCLENBQXFCLElBQXJCLENBREgsRUFDZ0M7QUFDNUJDLG1CQUFhLE9BRGU7QUFFNUJDLG9CQUFjO0FBRmMsS0FEaEMseUNBS0c5QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQUxILEVBS2tDO0FBQzlCdUIsbUJBQWEsT0FEaUI7QUFFOUJDLG9CQUFjO0FBRmdCLEtBTGxDLHlDQVNHOUIsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FUSCxFQVNrQztBQUM5QnVCLG1CQUFhLE1BRGlCO0FBRTlCQyxvQkFBYztBQUZnQixLQVRsQyx5Q0FhRzlCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBYkgsRUFha0M7QUFDOUJ1QixtQkFBYSxNQURpQjtBQUU5QkMsb0JBQWM7QUFGZ0IsS0FibEMseUNBaUJHOUIsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FqQkgsRUFpQmtDO0FBQzlCdUIsbUJBQWEsS0FEaUI7QUFFOUJDLG9CQUFjO0FBRmdCLEtBakJsQyx5Q0FxQkc5QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQXJCSCxFQXFCa0M7QUFDOUJ1QixtQkFBYSxLQURpQjtBQUU5QkMsb0JBQWM7QUFGZ0IsS0FyQmxDLFVBbkR5QjtBQTZFekJFO0FBQ0VDLGtCQUFZakMsTUFBTWtDLFVBQU4sQ0FBaUJDLGdCQUQvQjtBQUVFQyxhQUFPcEMsTUFBTXFDLE9BQU4sQ0FBY0MsZUFBZCxDQUE4QnRDLE1BQU1xQyxPQUFOLENBQWNFLE9BQWQsQ0FBc0IsR0FBdEIsQ0FBOUI7QUFGVCw2Q0FHR3ZDLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBSEgsRUFHa0M7QUFDOUJxQixnQkFBVTtBQURvQixLQUhsQyx5Q0FNRzNCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBTkgsRUFNa0M7QUFDOUJxQixnQkFBVTtBQURvQixLQU5sQyxVQTdFeUI7QUF1RnpCYTtBQUNFSixhQUFPcEMsTUFBTXFDLE9BQU4sQ0FBY0MsZUFBZCxDQUE4QnRDLE1BQU1xQyxPQUFOLENBQWNFLE9BQWQsQ0FBc0IsR0FBdEIsQ0FBOUI7QUFEVCxpREFFR3ZDLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBRkgsRUFFa0M7QUFDOUJxQixnQkFBVTtBQURvQixLQUZsQyw2Q0FLRzNCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBTEgsRUFLa0M7QUFDOUJxQixnQkFBVTtBQURvQixLQUxsQyw2Q0FRRzNCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBUkgsRUFRa0M7QUFDOUJxQixnQkFBVTtBQURvQixLQVJsQztBQVlBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUF0S3lCLEdBQVo7QUFBQSxDQUFmO0FBSkE7O0lBNktNYyxZOzs7QUFDSix3QkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGtKQUNYQSxLQURXOztBQUVqQixVQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUZpQjtBQUdsQjs7Ozs2QkFFUTtBQUFBLFVBQ0NuQyxPQURELEdBQ2EsS0FBS2tDLEtBRGxCLENBQ0NsQyxPQUREOzs7QUFHUCxVQUFNb0MsUUFBUSxTQUFSQSxLQUFRO0FBQUEsWUFBR0MsUUFBSCxRQUFHQSxRQUFIO0FBQUEsZUFDWjtBQUFBO0FBQUEsWUFBWSxXQUFXckMsUUFBUXdCLEtBQS9CLEVBQXNDLE1BQUssVUFBM0M7QUFDR2E7QUFESCxTQURZO0FBQUEsT0FBZDtBQUtBLFVBQU1DLFlBQVksU0FBWkEsU0FBWTtBQUFBLFlBQUdELFFBQUgsU0FBR0EsUUFBSDtBQUFBLGVBQ2hCO0FBQUE7QUFBQSxZQUFZLFdBQVdyQyxRQUFRZ0MsU0FBL0IsRUFBMEMsTUFBSyxVQUEvQztBQUNHSztBQURILFNBRGdCO0FBQUEsT0FBbEI7O0FBTUEsYUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsWUFBTSxZQUFOO0FBQ0U7QUFDRSxtQkFBTztBQUFDLG1CQUFEO0FBQUE7QUFBQTtBQUFBLGFBRFQ7QUFFRSx1QkFDRTtBQUFDLHVCQUFEO0FBQUE7QUFBQTtBQUFBLGFBSEo7QUFPRSx1QkFBV3JDLFFBQVFVO0FBQ25CO0FBUkYsY0FTRSxjQUFXO0FBVGIsWUFERjtBQVlFO0FBQUE7QUFBQTtBQWlCRSxxREFqQkY7QUFrQkU7QUFBQTtBQUFBO0FBQ0Usc0JBQUssT0FEUDtBQUVFLHVCQUFNLFNBRlI7QUFHRSwyQkFBV1YsUUFBUWlCLE9BSHJCO0FBSUU7QUFKRjtBQUFBO0FBQUE7QUFsQkYsV0FaRjtBQXVFRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRSxzQkFBSyxVQURQO0FBRUUsMkJBQVdqQixRQUFRdUI7QUFDbkI7QUFIRjtBQUFBO0FBTUU7QUFORixhQURGO0FBU0U7QUFBQTtBQUFBO0FBQ0Usc0JBQUssT0FEUDtBQUVFLDJCQUFXdkIsUUFBUWlCLE9BRnJCO0FBR0UsdUJBQU07QUFIUjtBQUFBO0FBUWdFO0FBUmhFLGFBVEY7QUFtQkU7QUFBQTtBQUFBLGdCQUFZLE1BQU0sVUFBbEIsRUFBOEIsV0FBV2pCLFFBQVFpQixPQUFqRDtBQUFBO0FBQ3lCLHVEQUR6QjtBQUFBO0FBRXdCO0FBRnhCLGFBbkJGO0FBdUJFO0FBQUE7QUFBQSxnQkFBWSxXQUFXakIsUUFBUWlCLE9BQS9CO0FBQUE7QUFBQSxhQXZCRjtBQTZCRTtBQUFBO0FBQUE7QUFDRSxzQkFBSyxPQURQO0FBRUUsMkJBQVdqQixRQUFRaUIsT0FGckI7QUFHRSx1QkFBTTtBQUhSO0FBQUE7QUFBQTtBQTdCRixXQXZFRjtBQStHRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRSxzQkFBSyxPQURQO0FBRUUsMkJBQVdqQixRQUFRaUIsT0FGckI7QUFHRSwyQkFBVSxLQUhaO0FBSUUsdUJBQU07QUFKUjtBQU1FO0FBQUE7QUFBQSxrQkFBWSxNQUFLLE9BQWpCO0FBQUE7QUFFRTtBQUZGLGVBTkY7QUFVRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFMRjtBQVNFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBVEY7QUFhRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBYkY7QUFWRixhQURGO0FBK0JFO0FBQUE7QUFBQTtBQUNFLHNCQUFLLE9BRFA7QUFFRSwyQkFBV2pCLFFBQVFpQixPQUZyQjtBQUdFLDJCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUEsa0JBQVksTUFBSyxPQUFqQjtBQUFBO0FBRUU7QUFGRixlQUxGO0FBU0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERjtBQUtFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBTEY7QUFXRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBWEY7QUFURjtBQS9CRixXQS9HRjtBQWdNRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsZ0JBQVksTUFBSyxVQUFqQixFQUE0QixXQUFXakIsUUFBUXVCLEtBQS9DO0FBQUE7QUFFRTtBQUZGLGFBREY7QUFLRTtBQUFBO0FBQUE7QUFDRSxzQkFBSyxPQURQO0FBRUUsMkJBQVd2QixRQUFRaUIsT0FGckI7QUFHRSwyQkFBVTtBQUhaO0FBQUE7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUhGO0FBSUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFKRjtBQUtFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBTEY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQU5GO0FBT0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFQRjtBQVFFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBUkY7QUFTRTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVRGO0FBVUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVZGLGVBTkY7QUFrQkUsdURBbEJGO0FBbUJFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSEY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkYsZUFuQkY7QUF5QkUsdURBekJGO0FBMEJFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSEY7QUFJRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkYsZUExQkY7QUFnQ0UsdURBaENGO0FBaUNFO0FBQUE7QUFBQTtBQUFBO0FBRUU7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUdFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFIRjtBQWpDRjtBQUxGLFdBaE1GO0FBb1NFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxnQkFBWSxNQUFLLE9BQWpCLEVBQXlCLFdBQVdqQixRQUFRaUIsT0FBNUM7QUFBQTtBQUFBLGFBREY7QUFRRSxxREFSRjtBQVNFO0FBVEY7QUFwU0YsU0FERjtBQWlURSx3RUFBc0IsSUFBSSxXQUExQjtBQWpURixPQURGO0FBcVREOzs7OztBQUdIZ0IsYUFBYTlCLFNBQWIsR0FBeUI7QUFDdkJILFdBQVMsb0JBQVVJLE1BQVYsQ0FBaUJDO0FBREgsQ0FBekI7O2tCQUllLDBCQUFXZCxNQUFYLEVBQW1CMEMsWUFBbkIsQyIsImZpbGUiOiIxNi5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMCA2TDguNTkgNy40MSAxMy4xNyAxMmwtNC41OCA0LjU5TDEwIDE4bDYtNnonIH0pO1xuXG52YXIgTmF2aWdhdGVOZXh0ID0gZnVuY3Rpb24gTmF2aWdhdGVOZXh0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5OYXZpZ2F0ZU5leHQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKE5hdmlnYXRlTmV4dCk7XG5OYXZpZ2F0ZU5leHQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gTmF2aWdhdGVOZXh0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL05hdmlnYXRlTmV4dC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTmF2aWdhdGVOZXh0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWJsb2NrJyxcbiAgICAgIGZpbGw6ICdjdXJyZW50Q29sb3InLFxuICAgICAgaGVpZ2h0OiAyNCxcbiAgICAgIHdpZHRoOiAyNCxcbiAgICAgIHVzZXJTZWxlY3Q6ICdub25lJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2ZpbGwnLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVyXG4gICAgICB9KVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckFjdGlvbjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZVxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JEaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBjb2xvckVycm9yOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5lcnJvcls1MDBdXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db2xvciA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBFbGVtZW50cyBwYXNzZWQgaW50byB0aGUgU1ZHIEljb24uXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFByb3ZpZGVzIGEgaHVtYW4tcmVhZGFibGUgdGl0bGUgZm9yIHRoZSBlbGVtZW50IHRoYXQgY29udGFpbnMgaXQuXG4gICAqIGh0dHBzOi8vd3d3LnczLm9yZy9UUi9TVkctYWNjZXNzLyNFcXVpdmFsZW50XG4gICAqL1xuICB0aXRsZUFjY2VzczogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQWxsb3dzIHlvdSB0byByZWRlZmluZSB3aGF0IHRoZSBjb29yZGluYXRlcyB3aXRob3V0IHVuaXRzIG1lYW4gaW5zaWRlIGFuIHN2ZyBlbGVtZW50LlxuICAgKiBGb3IgZXhhbXBsZSwgaWYgdGhlIFNWRyBlbGVtZW50IGlzIDUwMCAod2lkdGgpIGJ5IDIwMCAoaGVpZ2h0KSxcbiAgICogYW5kIHlvdSBwYXNzIHZpZXdCb3g9XCIwIDAgNTAgMjBcIixcbiAgICogdGhpcyBtZWFucyB0aGF0IHRoZSBjb29yZGluYXRlcyBpbnNpZGUgdGhlIHN2ZyB3aWxsIGdvIGZyb20gdGhlIHRvcCBsZWZ0IGNvcm5lciAoMCwwKVxuICAgKiB0byBib3R0b20gcmlnaHQgKDUwLDIwKSBhbmQgZWFjaCB1bml0IHdpbGwgYmUgd29ydGggMTBweC5cbiAgICovXG4gIHZpZXdCb3g6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcuaXNSZXF1aXJlZFxufTtcblxudmFyIFN2Z0ljb24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShTdmdJY29uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBTdmdJY29uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFN2Z0ljb24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChTdmdJY29uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShTdmdJY29uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShTdmdJY29uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICB0aXRsZUFjY2VzcyA9IF9wcm9wcy50aXRsZUFjY2VzcyxcbiAgICAgICAgICB2aWV3Qm94ID0gX3Byb3BzLnZpZXdCb3gsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAndGl0bGVBY2Nlc3MnLCAndmlld0JveCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2luaGVyaXQnKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ3N2ZycsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAgIGZvY3VzYWJsZTogJ2ZhbHNlJyxcbiAgICAgICAgICB2aWV3Qm94OiB2aWV3Qm94LFxuICAgICAgICAgICdhcmlhLWhpZGRlbic6IHRpdGxlQWNjZXNzID8gJ2ZhbHNlJyA6ICd0cnVlJ1xuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIHRpdGxlQWNjZXNzID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3RpdGxlJyxcbiAgICAgICAgICBudWxsLFxuICAgICAgICAgIHRpdGxlQWNjZXNzXG4gICAgICAgICkgOiBudWxsLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFN2Z0ljb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5TdmdJY29uLmRlZmF1bHRQcm9wcyA9IHtcbiAgdmlld0JveDogJzAgMCAyNCAyNCcsXG4gIGNvbG9yOiAnaW5oZXJpdCdcbn07XG5TdmdJY29uLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpU3ZnSWNvbicgfSkoU3ZnSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCcuL1N2Z0ljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMzQgMzYgMzkgNDMgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfaW52YXJpYW50ID0gcmVxdWlyZSgnaW52YXJpYW50Jyk7XG5cbnZhciBfaW52YXJpYW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ludmFyaWFudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgaXNNb2RpZmllZEV2ZW50ID0gZnVuY3Rpb24gaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB7XG4gIHJldHVybiAhIShldmVudC5tZXRhS2V5IHx8IGV2ZW50LmFsdEtleSB8fCBldmVudC5jdHJsS2V5IHx8IGV2ZW50LnNoaWZ0S2V5KTtcbn07XG5cbi8qKlxuICogVGhlIHB1YmxpYyBBUEkgZm9yIHJlbmRlcmluZyBhIGhpc3RvcnktYXdhcmUgPGE+LlxuICovXG5cbnZhciBMaW5rID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKExpbmssIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpbmsoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMaW5rKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5oYW5kbGVDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2xpY2spIF90aGlzLnByb3BzLm9uQ2xpY2soZXZlbnQpO1xuXG4gICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgJiYgLy8gb25DbGljayBwcmV2ZW50ZWQgZGVmYXVsdFxuICAgICAgZXZlbnQuYnV0dG9uID09PSAwICYmIC8vIGlnbm9yZSByaWdodCBjbGlja3NcbiAgICAgICFfdGhpcy5wcm9wcy50YXJnZXQgJiYgLy8gbGV0IGJyb3dzZXIgaGFuZGxlIFwidGFyZ2V0PV9ibGFua1wiIGV0Yy5cbiAgICAgICFpc01vZGlmaWVkRXZlbnQoZXZlbnQpIC8vIGlnbm9yZSBjbGlja3Mgd2l0aCBtb2RpZmllciBrZXlzXG4gICAgICApIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgdmFyIGhpc3RvcnkgPSBfdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5O1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgICByZXBsYWNlID0gX3RoaXMkcHJvcHMucmVwbGFjZSxcbiAgICAgICAgICAgICAgdG8gPSBfdGhpcyRwcm9wcy50bztcblxuXG4gICAgICAgICAgaWYgKHJlcGxhY2UpIHtcbiAgICAgICAgICAgIGhpc3RvcnkucmVwbGFjZSh0byk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGhpc3RvcnkucHVzaCh0byk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBMaW5rLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHJlcGxhY2UgPSBfcHJvcHMucmVwbGFjZSxcbiAgICAgICAgdG8gPSBfcHJvcHMudG8sXG4gICAgICAgIGlubmVyUmVmID0gX3Byb3BzLmlubmVyUmVmLFxuICAgICAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsncmVwbGFjZScsICd0bycsICdpbm5lclJlZiddKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgKDAsIF9pbnZhcmlhbnQyLmRlZmF1bHQpKHRoaXMuY29udGV4dC5yb3V0ZXIsICdZb3Ugc2hvdWxkIG5vdCB1c2UgPExpbms+IG91dHNpZGUgYSA8Um91dGVyPicpO1xuXG4gICAgdmFyIGhyZWYgPSB0aGlzLmNvbnRleHQucm91dGVyLmhpc3RvcnkuY3JlYXRlSHJlZih0eXBlb2YgdG8gPT09ICdzdHJpbmcnID8geyBwYXRobmFtZTogdG8gfSA6IHRvKTtcblxuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnYScsIF9leHRlbmRzKHt9LCBwcm9wcywgeyBvbkNsaWNrOiB0aGlzLmhhbmRsZUNsaWNrLCBocmVmOiBocmVmLCByZWY6IGlubmVyUmVmIH0pKTtcbiAgfTtcblxuICByZXR1cm4gTGluaztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpbmsucHJvcFR5cGVzID0ge1xuICBvbkNsaWNrOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIHRhcmdldDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG4gIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgdG86IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3RdKS5pc1JlcXVpcmVkLFxuICBpbm5lclJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNdKVxufTtcbkxpbmsuZGVmYXVsdFByb3BzID0ge1xuICByZXBsYWNlOiBmYWxzZVxufTtcbkxpbmsuY29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgIGhpc3Rvcnk6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgICAgcHVzaDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIGNyZWF0ZUhyZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkXG4gICAgfSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gTGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI2IDI3IDI4IDMwIDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDU3IDU4IDU5IDYxIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSByZXF1aXJlKCcuL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwnKTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSByZXF1aXJlKCcuL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQnKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRmFjdG9yeSA9IGZ1bmN0aW9uIGNyZWF0ZUZhY3RvcnkodHlwZSkge1xuICB2YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQgPSAoMCwgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyLmRlZmF1bHQpKHR5cGUpO1xuICByZXR1cm4gZnVuY3Rpb24gKHAsIGMpIHtcbiAgICByZXR1cm4gKDAsIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMi5kZWZhdWx0KShmYWxzZSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHAsIGMpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRmFjdG9yeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgZ2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBnZXREaXNwbGF5TmFtZShDb21wb25lbnQpIHtcbiAgaWYgKHR5cGVvZiBDb21wb25lbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfVxuXG4gIGlmICghQ29tcG9uZW50KSB7XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuXG4gIHJldHVybiBDb21wb25lbnQuZGlzcGxheU5hbWUgfHwgQ29tcG9uZW50Lm5hbWUgfHwgJ0NvbXBvbmVudCc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBnZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XG5cbnZhciBpc0NsYXNzQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNDbGFzc0NvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4oQ29tcG9uZW50ICYmIENvbXBvbmVudC5wcm90b3R5cGUgJiYgX3R5cGVvZihDb21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQpID09PSAnb2JqZWN0Jyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc0NsYXNzQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQgPSByZXF1aXJlKCcuL2lzQ2xhc3NDb21wb25lbnQnKTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzQ2xhc3NDb21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IGZ1bmN0aW9uIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKHR5cGVvZiBDb21wb25lbnQgPT09ICdmdW5jdGlvbicgJiYgISgwLCBfaXNDbGFzc0NvbXBvbmVudDIuZGVmYXVsdCkoQ29tcG9uZW50KSAmJiAhQ29tcG9uZW50LmRlZmF1bHRQcm9wcyAmJiAhQ29tcG9uZW50LmNvbnRleHRUeXBlcyAmJiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdwcm9kdWN0aW9uJyB8fCAhQ29tcG9uZW50LnByb3BUeXBlcykpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaG91bGRVcGRhdGUgPSByZXF1aXJlKCcuL3Nob3VsZFVwZGF0ZScpO1xuXG52YXIgX3Nob3VsZFVwZGF0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaG91bGRVcGRhdGUpO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJy4vc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwdXJlID0gZnVuY3Rpb24gcHVyZShCYXNlQ29tcG9uZW50KSB7XG4gIHZhciBob2MgPSAoMCwgX3Nob3VsZFVwZGF0ZTIuZGVmYXVsdCkoZnVuY3Rpb24gKHByb3BzLCBuZXh0UHJvcHMpIHtcbiAgICByZXR1cm4gISgwLCBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0KShwcm9wcywgbmV4dFByb3BzKTtcbiAgfSk7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdwdXJlJykpKGhvYyhCYXNlQ29tcG9uZW50KSk7XG4gIH1cblxuICByZXR1cm4gaG9jKEJhc2VDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gcHVyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zZXRTdGF0aWMgPSByZXF1aXJlKCcuL3NldFN0YXRpYycpO1xuXG52YXIgX3NldFN0YXRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRTdGF0aWMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBzZXREaXNwbGF5TmFtZShkaXNwbGF5TmFtZSkge1xuICByZXR1cm4gKDAsIF9zZXRTdGF0aWMyLmRlZmF1bHQpKCdkaXNwbGF5TmFtZScsIGRpc3BsYXlOYW1lKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBzZXRTdGF0aWMgPSBmdW5jdGlvbiBzZXRTdGF0aWMoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIEJhc2VDb21wb25lbnRba2V5XSA9IHZhbHVlO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICByZXR1cm4gQmFzZUNvbXBvbmVudDtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldFN0YXRpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnZmJqcy9saWIvc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9zaGFsbG93RXF1YWwyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkgPSByZXF1aXJlKCcuL2NyZWF0ZUVhZ2VyRmFjdG9yeScpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckZhY3RvcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBzaG91bGRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRVcGRhdGUodGVzdCkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICB2YXIgZmFjdG9yeSA9ICgwLCBfY3JlYXRlRWFnZXJGYWN0b3J5Mi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KTtcblxuICAgIHZhciBTaG91bGRVcGRhdGUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgICAgX2luaGVyaXRzKFNob3VsZFVwZGF0ZSwgX0NvbXBvbmVudCk7XG5cbiAgICAgIGZ1bmN0aW9uIFNob3VsZFVwZGF0ZSgpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNob3VsZFVwZGF0ZSk7XG5cbiAgICAgICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgICB9XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgICByZXR1cm4gdGVzdCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiBmYWN0b3J5KHRoaXMucHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgICB9KF9yZWFjdC5Db21wb25lbnQpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3Nob3VsZFVwZGF0ZScpKShTaG91bGRVcGRhdGUpO1xuICAgIH1cbiAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2hvdWxkVXBkYXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IGZ1bmN0aW9uIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwoaGFzS2V5LCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcHJvcHMsIGNoaWxkcmVuKSB7XG4gIGlmICghaGFzS2V5ICYmIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50KSB7XG4gICAgaWYgKGNoaWxkcmVuKSB7XG4gICAgICByZXR1cm4gdHlwZShfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgY2hpbGRyZW46IGNoaWxkcmVuIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHR5cGUocHJvcHMpO1xuICB9XG5cbiAgdmFyIENvbXBvbmVudCA9IHR5cGU7XG5cbiAgaWYgKGNoaWxkcmVuKSB7XG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgQ29tcG9uZW50LFxuICAgICAgcHJvcHMsXG4gICAgICBjaGlsZHJlblxuICAgICk7XG4gIH1cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBwcm9wcyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVFYWdlckVsZW1lbnRVdGlsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2dldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9nZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX2dldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHdyYXBEaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHdyYXBEaXNwbGF5TmFtZShCYXNlQ29tcG9uZW50LCBob2NOYW1lKSB7XG4gIHJldHVybiBob2NOYW1lICsgJygnICsgKDAsIF9nZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCkgKyAnKSc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3cmFwRGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiLyoqXG4gKiBUaGlzIGNvbXBvbmVudCBpcyBjcmVhdGUgdG8gc2hvdyBuYXZpYWd0aW9uIGJ1dHRvbiB0byBtb3ZlIG5leHQgcGFnZVxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IExpbmsgZnJvbSAncmVhY3Qtcm91dGVyLWRvbS9MaW5rJztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IHdpdGhTdHlsZXMgfSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuXG5pbXBvcnQgTmF2aWdhdGVOZXh0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9OYXZpZ2F0ZU5leHQnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICByaWdodDogJzIlJyxcbiAgICBib3R0b206ICc0JScsXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICByaWdodDogJzIlJyxcbiAgICAgIGJvdHRvbTogJzIlJyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgcmlnaHQ6ICcyJScsXG4gICAgICBib3R0b206ICcxJScsXG4gICAgfSxcbiAgfSxcbn0pO1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge29iamVjdH0gY2xhc3Nlc1xuICogQHBhcmFtIHtwYXRofSB0b1xuICovXG5jb25zdCBOYXZpZ2F0ZUJ1dHRvbk5leHQgPSAoeyBjbGFzc2VzLCB0byA9ICcvJyB9KSA9PiAoXG4gIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgIDxCdXR0b25cbiAgICAgIGZhYlxuICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgY29tcG9uZW50PXtMaW5rfVxuICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmJ1dHRvbn1cbiAgICAgIHJhaXNlZFxuICAgICAgdG89e3RvfVxuICAgID5cbiAgICAgIDxOYXZpZ2F0ZU5leHRJY29uIC8+XG4gICAgPC9CdXR0b24+XG4gIDwvZGl2PlxuKTtcblxuTmF2aWdhdGVCdXR0b25OZXh0LnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB0bzogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxufTtcblxuTmF2aWdhdGVCdXR0b25OZXh0LmRlZmF1bHRQcm9wcyA9IHtcbiAgdG86ICcvJyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShOYXZpZ2F0ZUJ1dHRvbk5leHQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb21wb25lbnRzL05hdmlnYXRpb25CdXR0b25OZXh0LmpzIiwiLyoqXG4gKiBUaGlzIGlzIHRoZSBpbnRyb2R1Y3Rpb24gcGFnZSBvZiBtYXlhc2guaW9cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnbWF0ZXJpYWwtdWkvVHlwb2dyYXBoeSc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50IH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG4vLyBpbXBvcnQgQ29sbGFwc2UgZnJvbSAnbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvQ29sbGFwc2UnO1xuXG5pbXBvcnQgTmF2aWdhdGlvbkJ1dHRvbk5leHQgZnJvbSAnLi4vY29tcG9uZW50cy9OYXZpZ2F0aW9uQnV0dG9uTmV4dCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge30sXG4gIGdyaWRJdGVtOiB7XG4gICAgcGFkZGluZ0JvdHRvbTogJzVweCcsXG4gIH0sXG4gIGNhcmRIZWFkZXI6IHtcbiAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgIGhlaWdodDogJzIwdmgnLFxuICAgIGJhY2tncm91bmRJbWFnZTpcbiAgICAgICd1cmwoXCJodHRwczovL2RyaXZlbmxvY2FsLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNS8xMC9NYXRlcmlhbC1kZXNpZ24uanBnXCIpJyxcbiAgICBiYWNrZ3JvdW5kUG9zaXRpb246ICdjZW50ZXInLFxuICAgIGJhY2tncm91bmRSZXBlYXQ6ICduby1yZXBlYXQnLFxuICB9LFxuICBwYXJhZ3JhcGg6IHtcbiAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICB9LFxuICAvLyBleHBhbmQ6IHtcbiAgLy8gICB0cmFuc2Zvcm06ICdyb3RhdGUoMGRlZyknLFxuICAvLyAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgndHJhbnNmb3JtJywge1xuICAvLyAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXN0LFxuICAvLyAgIH0pLFxuICAvLyB9LFxuICBjb250ZW50OiB7XG4gICAgbGluZUhlaWdodDogJzI1cHgnLFxuICAgIGZvbnRTaXplOiAnMXJlbScsXG4gICAgcGFkZGluZ0JvdHRvbTogJzFyZW0nLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHtcbiAgICAgIHBhZGRpbmdMZWZ0OiAnMTJlbScsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICcxMmVtJyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge1xuICAgICAgcGFkZGluZ0xlZnQ6ICcxMmVtJyxcbiAgICAgIHBhZGRpbmdSaWdodDogJzEyZW0nLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ2xnJyldOiB7XG4gICAgICBwYWRkaW5nTGVmdDogJzhlbScsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICc4ZW0nLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBwYWRkaW5nTGVmdDogJzNlbScsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICczZW0nLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyldOiB7XG4gICAgICBwYWRkaW5nTGVmdDogJzRweCcsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICc0cHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7XG4gICAgICBwYWRkaW5nTGVmdDogJzRweCcsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICc0cHgnLFxuICAgIH0sXG4gIH0sXG4gIGFib3V0OiB7XG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLnVwKCd4bCcpXToge1xuICAgICAgcGFkZGluZ0xlZnQ6ICcxMnJlbScsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICcxMnJlbScsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneGwnKV06IHtcbiAgICAgIHBhZGRpbmdMZWZ0OiAnMTJyZW0nLFxuICAgICAgcGFkZGluZ1JpZ2h0OiAnMTJyZW0nLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ2xnJyldOiB7XG4gICAgICBwYWRkaW5nTGVmdDogJzhyZW0nLFxuICAgICAgcGFkZGluZ1JpZ2h0OiAnOHJlbScsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHtcbiAgICAgIHBhZGRpbmdMZWZ0OiAnM3JlbScsXG4gICAgICBwYWRkaW5nUmlnaHQ6ICczcmVtJyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgcGFkZGluZ0xlZnQ6ICc0cHgnLFxuICAgICAgcGFkZGluZ1JpZ2h0OiAnNHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4cycpXToge1xuICAgICAgcGFkZGluZ0xlZnQ6ICc0cHgnLFxuICAgICAgcGFkZGluZ1JpZ2h0OiAnNHB4JyxcbiAgICB9LFxuICB9LFxuICB0aXRsZToge1xuICAgIGZvbnRXZWlnaHQ6IHRoZW1lLnR5cG9ncmFwaHkuZm9udFdlaWdodE1lZGl1bSxcbiAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzcwMF0pLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgZm9udFNpemU6IDMwLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7XG4gICAgICBmb250U2l6ZTogMzAsXG4gICAgfSxcbiAgfSxcbiAgc3ViaGVhZGVyOiB7XG4gICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs3MDBdKSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHtcbiAgICAgIGZvbnRTaXplOiAxNixcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgZm9udFNpemU6IDE2LFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7XG4gICAgICBmb250U2l6ZTogMTYsXG4gICAgfSxcbiAgfSxcbiAgLy8gaW50cm9kdWN0aW9uSGVhZGVyOiB7XG4gIC8vICAgYmFja2dyb3VuZEltYWdlOlxuICAvLyAgICAgJ3VybChcImh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9tYXlhc2gtd2ViL2RyaXZlLzI2LmpwZ1wiKScsXG4gIC8vICAgYmFja2dyb3VuZFBvc2l0aW9uOiAnY2VudGVyJyxcbiAgLy8gICBiYWNrZ3JvdW5kUmVwZWF0OiAnbm8tcmVwZWF0JyxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMudXAoJ3hsJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneGwnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdsZycpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4cycpXToge30sXG4gIC8vIH0sXG4gIC8vIGFib3V0VXNIZWFkZXI6IHtcbiAgLy8gICBiYWNrZ3JvdW5kSW1hZ2U6XG4gIC8vICAgICAndXJsKFwiaHR0cHM6Ly9zdG9yYWdlLmdvb2dsZWFwaXMuY29tL21heWFzaC13ZWIvZHJpdmUvMjcuanBnXCIpJyxcbiAgLy8gICBiYWNrZ3JvdW5kQXR0YWNobWVudDogJ2ZpeGVkJyxcbiAgLy8gICBiYWNrZ3JvdW5kUG9zaXRpb246ICdjZW50ZXInLFxuICAvLyAgIGJhY2tncm91bmRSZXBlYXQ6ICduby1yZXBlYXQnLFxuICAvLyAgIGJhY2tncm91bmRTaXplOiAnY292ZXInLFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ2xnJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7fSxcbiAgLy8gfSxcbiAgLy8gbW90aXZhdGlvbkhlYWRlcjoge1xuICAvLyAgIGJhY2tncm91bmRJbWFnZTpcbiAgLy8gICAgICd1cmwoXCJodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vbWF5YXNoLXdlYi9kcml2ZS8yOC5qcGdcIiknLFxuICAvLyAgIGJhY2tncm91bmRBdHRhY2htZW50OiAnZml4ZWQnLFxuICAvLyAgIGJhY2tncm91bmRQb3NpdGlvbjogJ2NlbnRlcicsXG4gIC8vICAgYmFja2dyb3VuZFJlcGVhdDogJ25vLXJlcGVhdCcsXG4gIC8vICAgYmFja2dyb3VuZFNpemU6ICdjb3ZlcicsXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLnVwKCd4bCcpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hsJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbGcnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneHMnKV06IHt9LFxuICAvLyB9LFxuICAvLyB2aXNpb25IZWFkZXI6IHtcbiAgLy8gICBiYWNrZ3JvdW5kSW1hZ2U6XG4gIC8vICAgICAndXJsKFwiaHR0cHM6Ly9zdG9yYWdlLmdvb2dsZWFwaXMuY29tL21heWFzaC13ZWIvZHJpdmUvMjkuanBnXCIpJyxcbiAgLy8gICBiYWNrZ3JvdW5kQXR0YWNobWVudDogJ2ZpeGVkJyxcbiAgLy8gICBiYWNrZ3JvdW5kUG9zaXRpb246ICdjZW50ZXInLFxuICAvLyAgIGJhY2tncm91bmRSZXBlYXQ6ICduby1yZXBlYXQnLFxuICAvLyAgIGJhY2tncm91bmRTaXplOiAnY292ZXInLFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ2xnJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7fSxcbiAgLy8gfSxcbiAgLy8gbWlzc2lvbkhlYWRlcjoge1xuICAvLyAgIGJhY2tncm91bmRJbWFnZTpcbiAgLy8gICAgICd1cmwoXCJodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vbWF5YXNoLXdlYi9kcml2ZS8zMC5qcGdcIiknLFxuICAvLyAgIGJhY2tncm91bmRBdHRhY2htZW50OiAnZml4ZWQnLFxuICAvLyAgIGJhY2tncm91bmRQb3NpdGlvbjogJ2NlbnRlcicsXG4gIC8vICAgYmFja2dyb3VuZFJlcGVhdDogJ25vLXJlcGVhdCcsXG4gIC8vICAgYmFja2dyb3VuZFNpemU6ICdjb3ZlcicsXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLnVwKCd4bCcpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hsJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbGcnKV06IHt9LFxuICAvLyAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge30sXG4gIC8vICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyldOiB7fSxcbiAgLy8gICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneHMnKV06IHt9LFxuICAvLyB9LFxufSk7XG5cbmNsYXNzIEludHJvZHVjdGlvbiBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBUaXRsZSA9ICh7IGNoaWxkcmVuIH0pID0+IChcbiAgICAgIDxUeXBvZ3JhcGh5IGNsYXNzTmFtZT17Y2xhc3Nlcy50aXRsZX0gdHlwZT1cImRpc3BsYXkzXCI+XG4gICAgICAgIHtjaGlsZHJlbn1cbiAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICApO1xuICAgIGNvbnN0IFN1YkhlYWRlciA9ICh7IGNoaWxkcmVuIH0pID0+IChcbiAgICAgIDxUeXBvZ3JhcGh5IGNsYXNzTmFtZT17Y2xhc3Nlcy5zdWJoZWFkZXJ9IHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICB7Y2hpbGRyZW59XG4gICAgICA8L1R5cG9ncmFwaHk+XG4gICAgKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2PlxuICAgICAgICA8Q2FyZCByYWlzZWQ+XG4gICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgIHRpdGxlPXs8VGl0bGU+TUFZQVNIPC9UaXRsZT59XG4gICAgICAgICAgICBzdWJoZWFkZXI9e1xuICAgICAgICAgICAgICA8U3ViSGVhZGVyPlxuICAgICAgICAgICAgICAgIE91ciBtaXNzaW9uIGlzIHRvIHByb3ZpZGUgd29ybGQgY2xhc3MgZWR1Y2F0aW9uIHRvIGV2ZXJ5b25lLlxuICAgICAgICAgICAgICA8L1N1YkhlYWRlcj5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfVxuICAgICAgICAgICAgLy8gYXJpYS1leHBhbmRlZD17aW50cm99XG4gICAgICAgICAgICBhcmlhLWxhYmVsPVwiSW50cm9kdWN0aW9uXCJcbiAgICAgICAgICAvPlxuICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgIHsvKiA8VHlwb2dyYXBoeVxuICAgICAgICAgICAgICAgIHR5cGU9XCJ0aXRsZVwiXG4gICAgICAgICAgICAgICAgYWxpZ249XCJjZW50ZXJcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5wYXJhZ3JhcGh9XG4gICAgICAgICAgICAgICAgZ3V0dGVyQm90dG9tXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIEFyZSB5b3Ugc2F0aXNmaWVkIHdpdGggdGhlIGN1cnJlbnQgZWR1Y2F0aW9uIHN5c3RlbT9cbiAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgICA8VHlwb2dyYXBoeVxuICAgICAgICAgICAgICAgIHR5cGU9XCJ0aXRsZVwiXG4gICAgICAgICAgICAgICAgYWxpZ249XCJjZW50ZXJcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5wYXJhZ3JhcGh9XG4gICAgICAgICAgICAgICAgZ3V0dGVyQm90dG9tXG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIEFyZSB5b3UgZmVkLXVwIG9mIHByZXNzdXJpemVkIG9ubHkgZm9yIG1hcmtzIGFuZCBncmFkZXM/XG4gICAgICAgICAgICAgIDwvVHlwb2dyYXBoeT4gKi99XG4gICAgICAgICAgICA8aHIgLz5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5XG4gICAgICAgICAgICAgIHR5cGU9XCJib2R5MVwiXG4gICAgICAgICAgICAgIGFsaWduPVwianVzdGlmeVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fVxuICAgICAgICAgICAgICBndXR0ZXJCb3R0b21cbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgTWF5YXNoIGlzIGFuIGVkdWNhdGlvbmFsIHBsYXRmb3JtIGJhc2VkIG9uIHRoZSBjb25jZXB0IG9mIGxlYXJuaW5nXG4gICAgICAgICAgICAgIGFuZCBzaGFyaW5nLiBNYXlhc2ggc3RyaXZlcyB0byBwcm92aWRlIHF1YWxpdHkgY29udGVudCBpbiBldmVyeVxuICAgICAgICAgICAgICBmaWVsZCBvZiBlZHVjYXRpb24gdG8gdGhlIHN0dWRlbnRzLGluY29ycG9yYXRpbmcgdGhlIHRlYWNoaW5nXG4gICAgICAgICAgICAgIG1hdGVyaWFsIGZyb20gdGhlIHZhcmlvdXMgcHJvZmVzc29ycyBvZiB0aGlzIGNvdW50cnkgYW5kIHRoZVxuICAgICAgICAgICAgICBsYXRlc3Qgc3R1ZmYgZnJvbSB0aGUgb3JnYW5pemF0aW9ucy4gTWF5YXNoIGlzIGEgb25lIHN0b3Agc29sdXRpb25cbiAgICAgICAgICAgICAgZm9yIGFsbCB5b3VyIHJlcXVpcmVtZW50cywgZG91YnRzIGFuZCBxdWVyaWVzLiBXZSBoYXZlIHRoZSB0b3BcbiAgICAgICAgICAgICAgbm90Y2ggcGVvcGxlIG9mIHRoZSBjb3VudHJ5IHdobyBhcmUgcmVhZHkgdG8gaGVscCB5b3UgaW4gdGhlIGJlc3RcbiAgICAgICAgICAgICAgcG9zc2libGUgd2F5LlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgey8qIDwvQ2FyZD5cbiAgICAgICAgPC9HcmlkPlxuICAgICAgICA8R3JpZFxuICAgICAgICAgIGl0ZW1cbiAgICAgICAgICB4cz17MTJ9XG4gICAgICAgICAgc209ezEyfVxuICAgICAgICAgIG1kPXsxMn1cbiAgICAgICAgICBsZz17MTJ9XG4gICAgICAgICAgeGw9ezEyfVxuICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX1cbiAgICAgICAgPlxuICAgICAgICAgIDxDYXJkIHJhaXNlZD5cbiAgICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICAgIHRpdGxlPXs8VGl0bGU+QWJvdXQgVXM8L1RpdGxlPn1cbiAgICAgICAgICAgICAgc3ViaGVhZGVyPXtcbiAgICAgICAgICAgICAgICA8U3ViSGVhZGVyPlxuICAgICAgICAgICAgICAgICAgS25vdyBvdXIgYW1iaXRpb24gYW5kIHBhc3Npb24gZm9yIHRoaXMgY291bnRyeS5cbiAgICAgICAgICAgICAgICA8L1N1YkhlYWRlcj5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzbmFtZXMoY2xhc3Nlcy5jYXJkSGVhZGVyLFxuICAgICAgICAgICAgICAgIGNsYXNzZXMuYWJvdXRVc0hlYWRlciwgY2xhc3Nlcy5leHBhbmQsXG4gICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgIGFyaWEtZXhwYW5kZWQ9e2Fib3V0VXN9XG4gICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJBYm91dCBVc1wiXG4gICAgICAgICAgICAvPiAqL31cbiAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeVxuICAgICAgICAgICAgICB0eXBlPVwiZGlzcGxheTFcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuYWJvdXR9XG4gICAgICAgICAgICAgIC8vIGFsaWduPVwibGVmdFwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIEFib3V0IFVzXG4gICAgICAgICAgICAgIDxociAvPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHlcbiAgICAgICAgICAgICAgdHlwZT1cImJvZHkxXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNvbnRlbnR9XG4gICAgICAgICAgICAgIGFsaWduPVwianVzdGlmeVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIE1heWFzaCBpcyBhIHRlYW0gb2YgZGVkaWNhdGVkIHBlb3BsZSB3aG8gaGF2ZSBtYWRlIGl0IHRoZWlyXG4gICAgICAgICAgICAgIG1pc3Npb24gdG8gc29sdmUgc29tZSBvZiB0aGUgbWFqb3IgcHJvYmxlbXMgd2hpY2ggb3VyIHNvY2lldHkgaXNcbiAgICAgICAgICAgICAgZmFjaW5nIHJpZ2h0IG5vdy4gV2UgYXJlIG1ha2luZyBhIHNpbXBsZSBidXQgZWZmZWN0aXZlIHBsYXRmb3JtXG4gICAgICAgICAgICAgIGZvciBsZWFybmluZyBhbmQgc2hhcmluZyBrbm93bGVkZ2UgY29uc2lzdGluZyBvZiB0d28gc2NoZW1hcyA6PGJyIC8+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPXsnZGlzcGxheTMnfSBjbGFzc05hbWU9e2NsYXNzZXMuY29udGVudH0+XG4gICAgICAgICAgICAgIDEuIExlYXJuIHdoYXQgeW91IG5lZWQuPGJyIC8+XG4gICAgICAgICAgICAgIDIuIHNoYXJlIHdoYXQgeW91IGNhbi48YnIgLz5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fT5cbiAgICAgICAgICAgICAgV2UgYXJlIHdvcmtpbmcgdG93YXJkcyB0aGUgYmV0dGVybWVudCBvZiBvdXIgZWR1Y2F0aW9uYWwgc3lzdGVtLFxuICAgICAgICAgICAgICB3aGVyZSB3ZSB3aWxsIHByb3ZpZGUgdXBkYXRlZCBrbm93bGVkZ2UgYW5kIHRoZSBsYXRlc3RcbiAgICAgICAgICAgICAgdGVjaG5vbG9naWVzIGZvciB1c2VycyB0byBoZWxwIHRoZW0gdXBncmFkZSB0aGVpciBza2lsbHMgYW5kIGJlXG4gICAgICAgICAgICAgIGFoZWFkIGluIHRoaXMgY29tcGV0aXRpdmUgZWNvc3lzdGVtLlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHlcbiAgICAgICAgICAgICAgdHlwZT1cImJvZHkxXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNvbnRlbnR9XG4gICAgICAgICAgICAgIGFsaWduPVwianVzdGlmeVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIEZvdW5kZWQgYnkgYW4gSUlUIERoYW5iYWQgZHJvcG91dCBhbmQgc3VwcG9ydGVkIGJ5IGEgdGVhbSBvZlxuICAgICAgICAgICAgICBJSVRpYW5zLCBNYXlhc2ggaXMgZm9jdXNzZWQgb24gdGhlIGltcHJvdmVtZW50IG9mIGVkdWNhdGlvbiBzeXN0ZW1cbiAgICAgICAgICAgICAgb2Ygb3VyIGNvdW50cnkgYnkgbWFraW5nIHRoZSB1c2VycyB0ZWNobmljYWxseSBjb21wZXRlbnQgYW5kXG4gICAgICAgICAgICAgIGhlbmNlLCBtYWtpbmcgdGhlbSBiZXR0ZXIgdGhhbiB0aGUgY29tcGV0aXRpb24uXG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeVxuICAgICAgICAgICAgICB0eXBlPVwiYm9keTFcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuY29udGVudH1cbiAgICAgICAgICAgICAgY29tcG9uZW50PVwiZGl2XCJcbiAgICAgICAgICAgICAgYWxpZ249XCJqdXN0aWZ5XCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cInRpdGxlXCI+XG4gICAgICAgICAgICAgICAgUGVyc2lzdGluZyBkcmF3YmFja3Mgb2YgY3VycmVudCBFZHVjYXRpb24gc3lzdGVtIDpcbiAgICAgICAgICAgICAgICA8aHIgLz5cbiAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgUXVhbnRpdGF0aXZlIG9yaWVudGF0aW9uIG9mIEVkdWNhdGlvbiBzeXN0ZW0gd2hlcmUgUXVhbGl0eSBpc1xuICAgICAgICAgICAgICAgICAgY29tcHJvbWlzZWQuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBEZWNhZGUgb2xkIHN5bGxhYnVzIHdoZXJlYXMgdGhlIHRlY2hub2xvZ3kgZ2V0cyBtYW5pZm9sZFxuICAgICAgICAgICAgICAgICAgY2hhbmdlcyBldmVyeSB5ZWFyXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBFZHVjYXRpb24gaXMgZm9jdXNlZCBtb3JlIG9uIHRoZW9yZXRpY2FsIGFzcGVjdCByYXRoZXIgdGhhbiBvblxuICAgICAgICAgICAgICAgICAgcHJhY3RpY2FsIHVuZGVyc3RhbmRpbmcuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBTdHVkZW50cyBwcmVmZXIgdG8gdGFrZSB0dWl0aW9ucywgdGhvdWdoIHRoZXJlIGFyZSBzY2hvb2xzXG4gICAgICAgICAgICAgICAgICBwcmVzZW50LCBqdXN0IHRvIHN0YXkgYWhlYWQgaW4gdGhlIHJhdCByYWNlIG9mIGdldHRpbmcgZ29vZFxuICAgICAgICAgICAgICAgICAgbWFya3MuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5XG4gICAgICAgICAgICAgIHR5cGU9XCJib2R5MVwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fVxuICAgICAgICAgICAgICBjb21wb25lbnQ9XCJkaXZcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwidGl0bGVcIj5cbiAgICAgICAgICAgICAgICBTb2x1dGlvbiAtIE1heWFzaDpcbiAgICAgICAgICAgICAgICA8aHIgLz5cbiAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgPGxpPlxuICAgICAgICAgICAgICAgICAgTWF5YXNoIGlzIGEgcGxhdGZvcm0gd2hpY2ggd2lsbCBjb3ZlciBhbGwgdGhlIGZpZWxkcyBvZlxuICAgICAgICAgICAgICAgICAgZWR1Y2F0aW9uLCBmb3IgYWxsIGFnZSBncm91cHMuXG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+XG4gICAgICAgICAgICAgICAgICBXZSBhcmUgY29ubmVjdGluZyBzdHVkZW50cy90ZWFjaGVycy9wcm9mZXNzaW9uYWxzIGZyb21cbiAgICAgICAgICAgICAgICAgIG11bHRpcGxlIGNvbGxlZ2VzLyB1bml2ZXJzaXRpZXMvIGNvcnBvcmF0ZXMgb24gYSBzaW5nbGVcbiAgICAgICAgICAgICAgICAgIHBsYXRmb3JtIHdoZXJlIHRoZXkgY2FuIGVhc2lseSBsZWFybiBhbmQgc2hhcmUgdGhlaXJcbiAgICAgICAgICAgICAgICAgIGtub3dsZWRnZS5cbiAgICAgICAgICAgICAgICA8L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgIFdlIHdpbGwgY3JlYXRlIGEgZGlzY3Vzc2lvbiBjaGFubmVsIHdoZXJlIHN0dWRlbnRzIGNhbiBnZXRcbiAgICAgICAgICAgICAgICAgIHRoZWlyIGRvdWJ0cyBhbmQgcXVlcmllcyBjbGVhcmVkLlxuICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICB7LyogPC9DYXJkPlxuICAgICAgICA8L0dyaWQ+XG4gICAgICAgIDxHcmlkXG4gICAgICAgICAgaXRlbVxuICAgICAgICAgIHhzPXsxMn1cbiAgICAgICAgICBzbT17MTJ9XG4gICAgICAgICAgbWQ9ezEyfVxuICAgICAgICAgIGxnPXsxMn1cbiAgICAgICAgICB4bD17MTJ9XG4gICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmdyaWRJdGVtfVxuICAgICAgICA+XG4gICAgICAgICAgPENhcmQgcmFpc2VkPlxuICAgICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgICAgdGl0bGU9ezxUaXRsZT5Nb3RpdmF0aW9uPC9UaXRsZT59XG4gICAgICAgICAgICAgIHN1YmhlYWRlcj17XG4gICAgICAgICAgICAgICAgPFN1YkhlYWRlcj5cbiAgICAgICAgICAgICAgICAgIFRvIHN0YXJ0IHNvbWV0aGluZyBnb29kIG1vdGl2YXRpb24gaXMgbmVjZXNzYXJ5LlxuICAgICAgICAgICAgICAgIDwvU3ViSGVhZGVyPlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NuYW1lcyhjbGFzc2VzLmNhcmRIZWFkZXIsXG4gICAgICAgICAgICAgICAgY2xhc3Nlcy5tb3RpdmF0aW9uSGVhZGVyLCBjbGFzc2VzLmV4cGFuZCxcbiAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIC8+ICovfVxuICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHR5cGU9XCJkaXNwbGF5MVwiIGNsYXNzTmFtZT17Y2xhc3Nlcy5hYm91dH0+XG4gICAgICAgICAgICAgIFdoeSBNYXlhc2g/XG4gICAgICAgICAgICAgIDxociAvPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHlcbiAgICAgICAgICAgICAgdHlwZT1cImJvZHkxXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNvbnRlbnR9XG4gICAgICAgICAgICAgIGNvbXBvbmVudD1cImRpdlwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIFRvZGF5IHRoZSBXZSBhcmUgZmFjaW5nIHRoZSBmb2xsb3dpbmcgcHJvYmxlbXMgOlxuICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgU3R1ZGVudHM6LVxuICAgICAgICAgICAgICAgIDxsaT5Db3N0bHkgRWR1Y2F0aW9uPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+TGFjayBvZiBwcmFjdGljYWwgdW5kZXJzdGFuZGluZy48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5IaWdoIFN0dWRlbnQtVGVhY2hlciByYXRpby48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5PdXRkYXRlZCBjdXJyaWN1bHVtLjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPlJhcGlkIGNoYW5nZSBpbiB0ZWNobm9sb2d5PC9saT5cbiAgICAgICAgICAgICAgICA8bGk+TGFjayBvZiBjb21wZXRlbnQgdGVhY2hlcnMuPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+TGFjayBvZiBmaWVsZCBzcGVjaWFsaXN0IG1lbnRvcnMuPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+TGFjayBvZiBJbmR1c3RyaWFsIGV4cG9zdXJlLjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPkxhbmd1YWdlIGJhcnJpZXIuLjwvbGk+XG4gICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgcHJvZmVzc29yczotXG4gICAgICAgICAgICAgICAgPGxpPlN0YXlpbmcgdXBkYXRlZCB3aXRoIGxhdGVzdCB0ZWNobm9sb2dpZXMgaXMgZGlmZmljdWx0LjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPkhpZ2ggYWNhZGVtaWMgbG9hZC48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5EaWZmaWN1bHQgdG8gYWRkcmVzcyB0byBhIGxhcmdlIG5vLiBvZiBzdHVkZW50czwvbGk+XG4gICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgICA8dWw+XG4gICAgICAgICAgICAgICAgRWR1Y2F0aW9uYWwgSW5zdGl0dXRlczotXG4gICAgICAgICAgICAgICAgPGxpPkRpZmZpY3VsdCB0byBlbnN1cmUgc2tpbGxlZCBmYWN1bHRpZXMuPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+SXQgaXMgY29zdGx5IHRvIGhpcmUgSW5kdXN0cmlhbCBleHBlcnRzLjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPkl0IGlzIGNvc3RseSB0byBjcmVhdGUgc2tpbGwgZGV2ZWxvcG1lbnQgcHJvZ3JhbXMuPC9saT5cbiAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgICAgPGJyIC8+XG4gICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICBPcmdhbmlzYXRpb25zOi1cbiAgICAgICAgICAgICAgICA8bGk+IEl0IGlzIGNvc3RseSB0byB0cmFpbiBmcmVzaGVycy48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5IaXJpbmcgcHJvY2VkdXJlIGlzIGxvbmcuPC9saT5cbiAgICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgIHsvKiA8L0NhcmQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgICAgPEdyaWRcbiAgICAgICAgICBpdGVtXG4gICAgICAgICAgeHM9ezEyfVxuICAgICAgICAgIHNtPXsxMn1cbiAgICAgICAgICBtZD17MTJ9XG4gICAgICAgICAgbGc9ezEyfVxuICAgICAgICAgIHhsPXsxMn1cbiAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19XG4gICAgICAgID5cbiAgICAgICAgICA8Q2FyZCByYWlzZWQ+XG4gICAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgICB0aXRsZT17PFRpdGxlPlZpc2lvbjwvVGl0bGU+fVxuICAgICAgICAgICAgICBzdWJoZWFkZXI9e1xuICAgICAgICAgICAgICAgIDxTdWJIZWFkZXI+XG4gICAgICAgICAgICAgICAgICBPdXIgdmlzaW9uIGlzIHRvIG1ha2Ugd29ybGQgYSBiZXR0ZXIgcGxhY2UgdG8gbGl2ZS5cbiAgICAgICAgICAgICAgICA8L1N1YkhlYWRlcj5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzbmFtZXMoY2xhc3Nlcy5jYXJkSGVhZGVyLFxuICAgICAgICAgICAgICAgIGNsYXNzZXMudmlzaW9uSGVhZGVyLCBjbGFzc2VzLmV4cGFuZCxcbiAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIC8+ICovfVxuICAgICAgICAgIHsvKiA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwiYm9keTFcIiBjbGFzc05hbWU9e2NsYXNzZXMuY29udGVudH0+XG4gICAgICAgICAgICAgICAgICBQcm92aWRpbmcgV29ybGQgY2xhc3MgcXVhbGl0eSBlZHVjYXRpb24gdG8gZXZlcnlvbmUgaW5cbiAgICAgICAgICAgICAgICBhZmZvcmRhYmxlIHByaWNlLiBXZSBhcmUgbWFraW5nIGEgc2ltcGxlIGJ1dCBlZmZlY3RpdmVcbiAgICAgICAgICAgICAgICBwbGF0Zm9ybSBmb3IgbGVhcm5pbmcgYW5kIHNoYXJpbmcga25vd2xlZGdlIHdoZXJlIHBlb3BsZVxuICAgICAgICAgICAgICAgIGNhbiBsZWFybiwgc2hhcmUgYW5kIGdyb3cuXG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD4gKi99XG4gICAgICAgICAgey8qIDwvQ2FyZD5cbiAgICAgICAgPC9HcmlkPlxuICAgICAgICA8R3JpZFxuICAgICAgICAgIGl0ZW1cbiAgICAgICAgICB4cz17MTJ9XG4gICAgICAgICAgc209ezEyfVxuICAgICAgICAgIG1kPXsxMn1cbiAgICAgICAgICBsZz17MTJ9XG4gICAgICAgICAgeGw9ezEyfVxuICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX1cbiAgICAgICAgPlxuICAgICAgICAgIDxDYXJkIHJhaXNlZD5cbiAgICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICAgIHRpdGxlPXs8VGl0bGU+TWlzc2lvbjwvVGl0bGU+fVxuICAgICAgICAgICAgICBzdWJoZWFkZXI9e1xuICAgICAgICAgICAgICAgIDxTdWJIZWFkZXI+XG4gICAgICAgICAgICAgICAgICBPdXIgbWlzc2lvbiBpcyB0byBwcm92aWRlIHdvcmxkIGNsYXNzIGVkdWNhdGlvbiB0byBldmV5b25lLlxuICAgICAgICAgICAgICAgIDwvU3ViSGVhZGVyPlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NuYW1lcyhjbGFzc2VzLmNhcmRIZWFkZXIsXG4gICAgICAgICAgICAgICAgY2xhc3Nlcy5taXNzaW9uSGVhZGVyLCBjbGFzc2VzLmV4cGFuZCxcbiAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IG1pc3Npb246ICFtaXNzaW9uIH0pfVxuICAgICAgICAgICAgLz4gKi99XG4gICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkxXCIgY2xhc3NOYW1lPXtjbGFzc2VzLmNvbnRlbnR9PlxuICAgICAgICAgICAgICBBaW0gb2YgbWF5YXNoIGlzIHRvIHNvbHZlIGFsbCB0aGUgYWJvdmUgcHJvYmxlbXMuIE91ciBWaXNpb24gaXMgdG9cbiAgICAgICAgICAgICAgaW1wcm92ZSB0aGUgZWR1Y2F0aW9uIHN5c3RlbSBvZiBvdXIgY291bnRyeS4gT3VyIG1pc3Npb24gaXMgdG9cbiAgICAgICAgICAgICAgYnJpbmcgb3V0IGJlc3Qgc2tpbGxzIG9mIHN0dWRlbnRzIGFzIHdlbGwgYXMgdGVhY2hlcnMgc28gdGhhdCB0aGV5XG4gICAgICAgICAgICAgIGFsbCBjYW4gZ3JvdyBjb2xsYWJvcmF0aXZlbHkgYW5kIGJyaW5nIG91dCB0aGUgYmVzdCBmcm9tIGV2ZXJ5XG4gICAgICAgICAgICAgIGluZGl2aWR1YWwgY29ubmVjdGVkIHdpdGggTWF5YXNoLlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPGhyIC8+XG4gICAgICAgICAgICA8aHIgLz5cbiAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICA8L0NhcmQ+XG4gICAgICAgIDxOYXZpZ2F0aW9uQnV0dG9uTmV4dCB0bz17Jy9zZXJ2aWNlcyd9IC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbkludHJvZHVjdGlvbi5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShJbnRyb2R1Y3Rpb24pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9wYWdlcy9JbnRyb2R1Y3Rpb24uanMiXSwic291cmNlUm9vdCI6IiJ9