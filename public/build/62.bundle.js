webpackJsonp([62],{

/***/ "./src/client/api/users/password/create.js":
/*!*************************************************!*\
  !*** ./src/client/api/users/password/create.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.userId -
 * @param {string} payload.password -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        password = _ref2.password;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/password';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ password: password })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/api/users/password/get.js":
/*!**********************************************!*\
  !*** ./src/client/api/users/password/get.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function get
 * @param {object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var get = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/password';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function get(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = get;

/***/ }),

/***/ "./src/client/api/users/password/update.js":
/*!*************************************************!*\
  !*** ./src/client/api/users/password/update.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {string} payload.oldPassword -
 * @param {string} payload.newPassword -
 * @param {string} payload.confirmNewPassword -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        oldPassword = _ref2.oldPassword,
        newPassword = _ref2.newPassword,
        confirmPassword = _ref2.confirmPassword;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/password';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/ProfilePage/Password.js":
/*!*******************************************************!*\
  !*** ./src/client/containers/ProfilePage/Password.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _Password = __webpack_require__(/*! ../../../lib/mayash-common/RegExp/Password */ "./src/lib/mayash-common/RegExp/Password.js");

var _Password2 = _interopRequireDefault(_Password);

var _get = __webpack_require__(/*! ../../api/users/password/get */ "./src/client/api/users/password/get.js");

var _get2 = _interopRequireDefault(_get);

var _create = __webpack_require__(/*! ../../api/users/password/create */ "./src/client/api/users/password/create.js");

var _create2 = _interopRequireDefault(_create);

var _update = __webpack_require__(/*! ../../api/users/password/update */ "./src/client/api/users/password/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {},
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  gridItem: {
    padding: '1%'
  },
  flexGrow: {
    flex: '1 1 auto'
  },
  errorMessege: {
    color: 'red'
  },
  cardHeader: {
    textAlign: 'center'
  }
}; /** @format */

var Password = function (_Component) {
  (0, _inherits3.default)(Password, _Component);

  function Password(props) {
    (0, _classCallCheck3.default)(this, Password);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Password.__proto__ || (0, _getPrototypeOf2.default)(Password)).call(this, props));

    _this.state = {
      isPasswordExit: false,
      isValid: false,
      error: '',
      message: '',
      oldPassword: '',
      newPassword: '',
      confirmPassword: ''
    };

    _this.onChangeOld = _this.onChangeOld.bind(_this);
    _this.onChangeNew = _this.onChangeNew.bind(_this);
    _this.onChangeConfirm = _this.onChangeConfirm.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Password, [{
    key: 'componentWillMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props$user, userId, token, _ref2, statusCode;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props$user = this.props.user, userId = _props$user.id, token = _props$user.token;
                _context.next = 4;
                return (0, _get2.default)({ userId: userId, token: token });

              case 4:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;

                this.setState({ isPasswordExit: statusCode === 200 });
                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 12:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 9]]);
      }));

      function componentWillMount() {
        return _ref.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: 'onChangeOld',
    value: function onChangeOld(e) {
      var password = e.target.value;

      this.setState({
        oldPassword: password
      });
    }
  }, {
    key: 'onChangeNew',
    value: function onChangeNew(e) {
      var password = e.target.value;

      this.setState({
        newPassword: password
      });
      if (!_Password2.default.test(password)) {
        this.setState({
          message: 'New Password Should contain atleast one lowercase letter ,' + 'one upper case letter ,one digit and only one special character. ' + 'Minimun and maximum length of password is 5 & 20  ',
          isValid: false
        });
        return;
      }
      this.setState({
        isValid: true,
        message: ''
      });
    }
  }, {
    key: 'onChangeConfirm',
    value: function onChangeConfirm(e) {
      var password = e.target.value;

      this.setState({
        confirmPassword: password
      });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _this2 = this;

        var _state, oldPassword, newPassword, confirmPassword, isPasswordExit, _props$user2, userId, token, statusCode, res, _res;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _state = this.state, oldPassword = _state.oldPassword, newPassword = _state.newPassword, confirmPassword = _state.confirmPassword, isPasswordExit = _state.isPasswordExit;
                _props$user2 = this.props.user, userId = _props$user2.id, token = _props$user2.token;

                if (!(oldPassword === newPassword)) {
                  _context2.next = 6;
                  break;
                }

                this.setState({ message: 'Your new password is same as old' });
                return _context2.abrupt('return');

              case 6:
                if (!(newPassword !== confirmPassword)) {
                  _context2.next = 9;
                  break;
                }

                this.setState({ message: 'Your Confirmed password is not correct' });
                return _context2.abrupt('return');

              case 9:

                this.setState({ message: '' });
                statusCode = void 0;

                if (!isPasswordExit) {
                  _context2.next = 18;
                  break;
                }

                _context2.next = 14;
                return (0, _update2.default)({
                  token: token,
                  userId: userId,
                  oldPassword: oldPassword,
                  newPassword: newPassword,
                  confirmPassword: confirmPassword
                });

              case 14:
                res = _context2.sent;


                statusCode = res.statusCode;
                _context2.next = 22;
                break;

              case 18:
                _context2.next = 20;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  password: newPassword
                });

              case 20:
                _res = _context2.sent;


                statusCode = _res.statusCode;

              case 22:
                if (!(statusCode >= 300)) {
                  _context2.next = 25;
                  break;
                }

                this.setState({ message: 'Sorry Unable to process' });
                return _context2.abrupt('return');

              case 25:

                this.setState({ message: 'Your password updated successfully' });

                setTimeout(function () {
                  _this2.setState({
                    message: '',
                    oldPassword: '',
                    newPassword: '',
                    confirmPassword: '',
                    isValid: false
                  });
                }, 1500);
                _context2.next = 32;
                break;

              case 29:
                _context2.prev = 29;
                _context2.t0 = _context2['catch'](0);

                console.error(_context2.t0);

              case 32:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 29]]);
      }));

      function onSubmit() {
        return _ref3.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var _state2 = this.state,
          oldPassword = _state2.oldPassword,
          isValid = _state2.isValid,
          confirmPassword = _state2.confirmPassword,
          newPassword = _state2.newPassword,
          isPasswordExit = _state2.isPasswordExit,
          message = _state2.message;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 6,
            xl: 6,
            className: classes.gridItem
          },
          _react2.default.createElement(
            _Card2.default,
            { className: classes.card },
            _react2.default.createElement(_Card.CardHeader, {
              title: 'Change your password',
              className: classes.cardHeader
            }),
            isPasswordExit ? _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(_Input2.default, {
                type: 'password',
                value: oldPassword,
                onChange: this.onChangeOld,
                placeholder: 'Enter Old Password'
              })
            ) : null,
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(_Input2.default, {
                type: 'password',
                value: newPassword,
                onChange: this.onChangeNew,
                placeholder: 'Enter New Password'
              })
            ),
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(_Input2.default, {
                type: 'password',
                value: confirmPassword,
                onChange: this.onChangeConfirm,
                placeholder: 'Confirm New Password'
              })
            ),
            message ? _react2.default.createElement(
              _Card.CardContent,
              { className: classes.errorMessege },
              message
            ) : null,
            _react2.default.createElement(
              _Card.CardActions,
              null,
              _react2.default.createElement('div', { className: classes.flexGrow }),
              _react2.default.createElement(
                _Button2.default,
                {
                  raised: true,
                  color: 'accent',
                  onClick: this.onSubmit,
                  disabled: !(newPassword && confirmPassword && isValid)
                },
                'Submit'
              )
            )
          )
        )
      );
    }
  }]);
  return Password;
}(_react.Component);

Password.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    token: _propTypes2.default.string.isRequired,
    username: _propTypes2.default.string
  }).isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Password);

/***/ }),

/***/ "./src/lib/mayash-common/RegExp/Password.js":
/*!**************************************************!*\
  !*** ./src/lib/mayash-common/RegExp/Password.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains the RegExp of password. The password should
 * follow these rules.
 * 1. It should contain atleast one lowercase letter
 * 2. It should contain atleast one uppercase letter
 * 3. It should contain atleast one digit
 * 4. It should contain one special character
 * 5. It's minimum length should be 5.
 * 6. It's maximum length should be 20.
 *
 * @format
 */

exports.default = new RegExp('((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{5,20})');

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS91c2Vycy9wYXNzd29yZC9jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvdXNlcnMvcGFzc3dvcmQvZ2V0LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3VzZXJzL3Bhc3N3b3JkL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1Byb2ZpbGVQYWdlL1Bhc3N3b3JkLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWNvbW1vbi9SZWdFeHAvUGFzc3dvcmQuanMiXSwibmFtZXMiOlsidG9rZW4iLCJ1c2VySWQiLCJwYXNzd29yZCIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwiYm9keSIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsImNyZWF0ZSIsImdldCIsIm9sZFBhc3N3b3JkIiwibmV3UGFzc3dvcmQiLCJjb25maXJtUGFzc3dvcmQiLCJ1cGRhdGUiLCJzdHlsZXMiLCJyb290IiwiZGlzcGxheSIsImlucHV0IiwiZmxleEdyb3ciLCJib3JkZXIiLCJib3JkZXJSYWRpdXMiLCJwYWRkaW5nIiwib3V0bGluZSIsImJvcmRlckNvbG9yIiwiYm94U2hhZG93IiwiSW5wdXQiLCJjbGFzc2VzIiwib25DaGFuZ2UiLCJ2YWx1ZSIsInBsYWNlaG9sZGVyIiwiZGlzYWJsZWQiLCJ0eXBlIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJvbmVPZlR5cGUiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiY2FyZCIsImF2YXRhciIsImdyaWRJdGVtIiwiZmxleCIsImVycm9yTWVzc2VnZSIsImNvbG9yIiwiY2FyZEhlYWRlciIsInRleHRBbGlnbiIsIlBhc3N3b3JkIiwicHJvcHMiLCJzdGF0ZSIsImlzUGFzc3dvcmRFeGl0IiwiaXNWYWxpZCIsIm1lc3NhZ2UiLCJvbkNoYW5nZU9sZCIsImJpbmQiLCJvbkNoYW5nZU5ldyIsIm9uQ2hhbmdlQ29uZmlybSIsIm9uU3VibWl0IiwidXNlciIsImlkIiwic2V0U3RhdGUiLCJlIiwidGFyZ2V0IiwidGVzdCIsInNldFRpbWVvdXQiLCJzaGFwZSIsInVzZXJuYW1lIiwiUmVnRXhwIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFLQTs7Ozs7Ozs7Ozs7O0FBTEE7OztzRkFpQkE7QUFBQSxRQUF3QkEsS0FBeEIsU0FBd0JBLEtBQXhCO0FBQUEsUUFBK0JDLE1BQS9CLFNBQStCQSxNQUEvQjtBQUFBLFFBQXVDQyxRQUF2QyxTQUF1Q0EsUUFBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNGLE1BRnJDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1FLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsTUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlUDtBQUhSLGVBRmtCO0FBTzNCUSxvQkFBTSx5QkFBZSxFQUFFTixrQkFBRixFQUFmO0FBUHFCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVU8sZUFKVjtBQWNZQyxrQkFkWixHQWNtQ0QsR0FkbkMsQ0FjWUMsTUFkWixFQWNvQkMsVUFkcEIsR0FjbUNGLEdBZG5DLENBY29CRSxVQWRwQjs7QUFBQSxrQkFnQlFELFVBQVUsR0FoQmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWlCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWpCYjs7QUFBQTtBQUFBO0FBQUEsbUJBdUJ1QkYsSUFBSUssSUFBSixFQXZCdkI7O0FBQUE7QUF1QlVBLGdCQXZCVjtBQUFBLHdFQXlCZ0JBLElBekJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMkJJQyxvQkFBUUYsS0FBUjs7QUEzQkosNkNBNkJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTdCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZUcsTTs7Ozs7QUFmZjs7OztBQUNBOzs7O2tCQWtEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hEZjs7Ozs7Ozs7Ozs7QUFMQTs7O3NGQWdCQTtBQUFBLFFBQXFCZixNQUFyQixTQUFxQkEsTUFBckI7QUFBQSxRQUE2QkQsS0FBN0IsU0FBNkJBLEtBQTdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVHLGVBRlYsa0NBRXFDRixNQUZyQztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNRSxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVA7QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVTLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJSyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRRixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlSSxHOzs7OztBQWRmOzs7O0FBQ0E7Ozs7a0JBZ0RlQSxHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlDZjs7Ozs7Ozs7Ozs7Ozs7QUFMQTs7O3NGQW1CQTtBQUFBLFFBQ0VqQixLQURGLFNBQ0VBLEtBREY7QUFBQSxRQUVFQyxNQUZGLFNBRUVBLE1BRkY7QUFBQSxRQUdFaUIsV0FIRixTQUdFQSxXQUhGO0FBQUEsUUFJRUMsV0FKRixTQUlFQSxXQUpGO0FBQUEsUUFLRUMsZUFMRixTQUtFQSxlQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBUVVqQixlQVJWLGtDQVFxQ0YsTUFSckM7QUFBQTtBQUFBLG1CQVVzQiwrQkFBTUUsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVQO0FBSFIsZUFGa0I7QUFPM0JRLG9CQUFNLHlCQUFlLEVBQUVVLHdCQUFGLEVBQWVDLHdCQUFmLEVBQTRCQyxnQ0FBNUIsRUFBZjtBQVBxQixhQUFYLENBVnRCOztBQUFBO0FBVVVYLGVBVlY7QUFvQllDLGtCQXBCWixHQW9CbUNELEdBcEJuQyxDQW9CWUMsTUFwQlosRUFvQm9CQyxVQXBCcEIsR0FvQm1DRixHQXBCbkMsQ0FvQm9CRSxVQXBCcEI7O0FBQUEsa0JBc0JRRCxVQUFVLEdBdEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0F1QmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUF2QmI7O0FBQUE7QUFBQTtBQUFBLG1CQTZCdUJGLElBQUlLLElBQUosRUE3QnZCOztBQUFBO0FBNkJVQSxnQkE3QlY7QUFBQSx3RUE4QmdCQSxJQTlCaEI7O0FBQUE7QUFBQTtBQUFBOztBQWdDSUMsb0JBQVFGLEtBQVI7O0FBaENKLDZDQWtDVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUFsQ1g7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVRLE07Ozs7O0FBakJmOzs7O0FBQ0E7Ozs7a0JBeURlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckRmOzs7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUMsU0FBUztBQUNiQyxRQUFNO0FBQ0pDLGFBQVM7QUFETCxHQURPO0FBSWJDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTEMsa0JBQWMsS0FIVDtBQUlMQyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RDLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEY7QUFMTjtBQUpNLENBQWYsQyxDQVpBOzs7Ozs7O0FBNkJBLElBQU1DLFFBQVEsU0FBUkEsS0FBUTtBQUFBLE1BQUdDLE9BQUgsUUFBR0EsT0FBSDtBQUFBLE1BQVlDLFFBQVosUUFBWUEsUUFBWjtBQUFBLE1BQXNCQyxLQUF0QixRQUFzQkEsS0FBdEI7QUFBQSxNQUE2QkMsV0FBN0IsUUFBNkJBLFdBQTdCO0FBQUEsTUFBMENDLFFBQTFDLFFBQTBDQSxRQUExQztBQUFBLE1BQW9EQyxJQUFwRCxRQUFvREEsSUFBcEQ7QUFBQSxTQUNaO0FBQUE7QUFBQSxNQUFLLFdBQVdMLFFBQVFYLElBQXhCO0FBQ0U7QUFDRSxZQUFNZ0IsUUFBUSxNQURoQjtBQUVFLG1CQUFhRixlQUFlLE9BRjlCO0FBR0UsYUFBT0QsS0FIVDtBQUlFLGdCQUFVRCxRQUpaO0FBS0UsaUJBQVdELFFBQVFULEtBTHJCO0FBTUUsZ0JBQVUsQ0FBQyxDQUFDYTtBQU5kO0FBREYsR0FEWTtBQUFBLENBQWQ7O0FBYUFMLE1BQU1PLFNBQU4sR0FBa0I7QUFDaEJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFY7QUFFaEJQLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUQsVUFGVDtBQUdoQk4sU0FBTyxvQkFBVVEsU0FBVixDQUFvQixDQUFDLG9CQUFVQyxNQUFYLEVBQW1CLG9CQUFVQyxNQUE3QixDQUFwQixFQUEwREosVUFIakQ7QUFJaEJMLGVBQWEsb0JBQVVRLE1BSlA7QUFLaEJOLFFBQU0sb0JBQVVNLE1BTEE7QUFNaEJQLFlBQVUsb0JBQVVTO0FBTkosQ0FBbEI7O2tCQVNlLDBCQUFXekIsTUFBWCxFQUFtQlcsS0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTVgsU0FBUztBQUNiQyxRQUFNLEVBRE87QUFFYnlCLFFBQU07QUFDSnBCLGtCQUFjO0FBRFYsR0FGTztBQUticUIsVUFBUTtBQUNOckIsa0JBQWM7QUFEUixHQUxLO0FBUWJzQixZQUFVO0FBQ1JyQixhQUFTO0FBREQsR0FSRztBQVdiSCxZQUFVO0FBQ1J5QixVQUFNO0FBREUsR0FYRztBQWNiQyxnQkFBYztBQUNaQyxXQUFPO0FBREssR0FkRDtBQWlCYkMsY0FBWTtBQUNWQyxlQUFXO0FBREQ7QUFqQkMsQ0FBZixDLENBbEJBOztJQXdDTUMsUTs7O0FBQ0osb0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSwwSUFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hDLHNCQUFnQixLQURMO0FBRVhDLGVBQVMsS0FGRTtBQUdYL0MsYUFBTyxFQUhJO0FBSVhnRCxlQUFTLEVBSkU7QUFLWDNDLG1CQUFhLEVBTEY7QUFNWEMsbUJBQWEsRUFORjtBQU9YQyx1QkFBaUI7QUFQTixLQUFiOztBQVVBLFVBQUswQyxXQUFMLEdBQW1CLE1BQUtBLFdBQUwsQ0FBaUJDLElBQWpCLE9BQW5CO0FBQ0EsVUFBS0MsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCRCxJQUFqQixPQUFuQjtBQUNBLFVBQUtFLGVBQUwsR0FBdUIsTUFBS0EsZUFBTCxDQUFxQkYsSUFBckIsT0FBdkI7QUFDQSxVQUFLRyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0gsSUFBZCxPQUFoQjtBQWZpQjtBQWdCbEI7Ozs7Ozs7Ozs7Ozs7OEJBSWlDLEtBQUtOLEtBQUwsQ0FBV1UsSSxFQUE3QmxFLE0sZUFBSm1FLEUsRUFBWXBFLEssZUFBQUEsSzs7dUJBQ1MsbUJBQWUsRUFBRUMsY0FBRixFQUFVRCxZQUFWLEVBQWYsQzs7OztBQUFyQlksMEIsU0FBQUEsVTs7QUFDUixxQkFBS3lELFFBQUwsQ0FBYyxFQUFFVixnQkFBZ0IvQyxlQUFlLEdBQWpDLEVBQWQ7Ozs7Ozs7O0FBRUFHLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Z0NBSVF5RCxDLEVBQUc7QUFBQSxVQUNFcEUsUUFERixHQUNlb0UsRUFBRUMsTUFEakIsQ0FDTG5DLEtBREs7O0FBRWIsV0FBS2lDLFFBQUwsQ0FBYztBQUNabkQscUJBQWFoQjtBQURELE9BQWQ7QUFHRDs7O2dDQUVXb0UsQyxFQUFHO0FBQUEsVUFDRXBFLFFBREYsR0FDZW9FLEVBQUVDLE1BRGpCLENBQ0xuQyxLQURLOztBQUViLFdBQUtpQyxRQUFMLENBQWM7QUFDWmxELHFCQUFhakI7QUFERCxPQUFkO0FBR0EsVUFBSSxDQUFDLG1CQUFlc0UsSUFBZixDQUFvQnRFLFFBQXBCLENBQUwsRUFBb0M7QUFDbEMsYUFBS21FLFFBQUwsQ0FBYztBQUNaUixtQkFDRSwrREFDQSxtRUFEQSxHQUVBLG9EQUpVO0FBS1pELG1CQUFTO0FBTEcsU0FBZDtBQU9BO0FBQ0Q7QUFDRCxXQUFLUyxRQUFMLENBQWM7QUFDWlQsaUJBQVMsSUFERztBQUVaQyxpQkFBUztBQUZHLE9BQWQ7QUFJRDs7O29DQUVlUyxDLEVBQUc7QUFBQSxVQUNGcEUsUUFERSxHQUNXb0UsRUFBRUMsTUFEYixDQUNUbkMsS0FEUzs7QUFFakIsV0FBS2lDLFFBQUwsQ0FBYztBQUNaakQseUJBQWlCbEI7QUFETCxPQUFkO0FBR0Q7Ozs7Ozs7Ozs7Ozs7O3lCQVNPLEtBQUt3RCxLLEVBSlB4QyxXLFVBQUFBLFcsRUFDQUMsVyxVQUFBQSxXLEVBQ0FDLGUsVUFBQUEsZSxFQUNBdUMsYyxVQUFBQSxjOytCQUU0QixLQUFLRixLQUFMLENBQVdVLEksRUFBN0JsRSxNLGdCQUFKbUUsRSxFQUFZcEUsSyxnQkFBQUEsSzs7c0JBRWhCa0IsZ0JBQWdCQyxXOzs7OztBQUNsQixxQkFBS2tELFFBQUwsQ0FBYyxFQUFFUixTQUFTLGtDQUFYLEVBQWQ7Ozs7c0JBSUUxQyxnQkFBZ0JDLGU7Ozs7O0FBQ2xCLHFCQUFLaUQsUUFBTCxDQUFjLEVBQUVSLFNBQVMsd0NBQVgsRUFBZDs7Ozs7QUFJRixxQkFBS1EsUUFBTCxDQUFjLEVBQUVSLFNBQVMsRUFBWCxFQUFkO0FBQ0lqRCwwQjs7cUJBRUErQyxjOzs7Ozs7dUJBQ2dCLHNCQUFrQjtBQUNsQzNELDhCQURrQztBQUVsQ0MsZ0NBRmtDO0FBR2xDaUIsMENBSGtDO0FBSWxDQywwQ0FKa0M7QUFLbENDO0FBTGtDLGlCQUFsQixDOzs7QUFBWlgsbUI7OztBQVFORyw2QkFBYUgsSUFBSUcsVUFBakI7Ozs7Ozt1QkFFa0Isc0JBQWtCO0FBQ2xDWiw4QkFEa0M7QUFFbENDLGdDQUZrQztBQUdsQ0MsNEJBQVVpQjtBQUh3QixpQkFBbEIsQzs7O0FBQVpWLG9COzs7QUFNTkcsNkJBQWFILEtBQUlHLFVBQWpCOzs7c0JBR0VBLGNBQWMsRzs7Ozs7QUFDaEIscUJBQUt5RCxRQUFMLENBQWMsRUFBRVIsU0FBUyx5QkFBWCxFQUFkOzs7OztBQUlGLHFCQUFLUSxRQUFMLENBQWMsRUFBRVIsU0FBUyxvQ0FBWCxFQUFkOztBQUVBWSwyQkFBVyxZQUFNO0FBQ2YseUJBQUtKLFFBQUwsQ0FBYztBQUNaUiw2QkFBUyxFQURHO0FBRVozQyxpQ0FBYSxFQUZEO0FBR1pDLGlDQUFhLEVBSEQ7QUFJWkMscUNBQWlCLEVBSkw7QUFLWndDLDZCQUFTO0FBTEcsbUJBQWQ7QUFPRCxpQkFSRCxFQVFHLElBUkg7Ozs7Ozs7O0FBVUE3Qyx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQUlLO0FBQUEsVUFDQ3FCLE9BREQsR0FDYSxLQUFLdUIsS0FEbEIsQ0FDQ3ZCLE9BREQ7QUFBQSxvQkFTSCxLQUFLd0IsS0FURjtBQUFBLFVBR0x4QyxXQUhLLFdBR0xBLFdBSEs7QUFBQSxVQUlMMEMsT0FKSyxXQUlMQSxPQUpLO0FBQUEsVUFLTHhDLGVBTEssV0FLTEEsZUFMSztBQUFBLFVBTUxELFdBTkssV0FNTEEsV0FOSztBQUFBLFVBT0x3QyxjQVBLLFdBT0xBLGNBUEs7QUFBQSxVQVFMRSxPQVJLLFdBUUxBLE9BUks7OztBQVdQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBVzNCLFFBQVFYLElBQWhFO0FBQ0U7QUFBQTtBQUFBO0FBQ0Usc0JBREY7QUFFRSxnQkFBSSxFQUZOO0FBR0UsZ0JBQUksRUFITjtBQUlFLGdCQUFJLENBSk47QUFLRSxnQkFBSSxDQUxOO0FBTUUsZ0JBQUksQ0FOTjtBQU9FLHVCQUFXVyxRQUFRZ0I7QUFQckI7QUFTRTtBQUFBO0FBQUEsY0FBTSxXQUFXaEIsUUFBUWMsSUFBekI7QUFDRTtBQUNFLHFCQUFPLHNCQURUO0FBRUUseUJBQVdkLFFBQVFvQjtBQUZyQixjQURGO0FBS0dLLDZCQUNDO0FBQUE7QUFBQTtBQUNFO0FBQ0Usc0JBQU0sVUFEUjtBQUVFLHVCQUFPekMsV0FGVDtBQUdFLDBCQUFVLEtBQUs0QyxXQUhqQjtBQUlFLDZCQUFhO0FBSmY7QUFERixhQURELEdBU0csSUFkTjtBQWVFO0FBQUE7QUFBQTtBQUNFO0FBQ0Usc0JBQU0sVUFEUjtBQUVFLHVCQUFPM0MsV0FGVDtBQUdFLDBCQUFVLEtBQUs2QyxXQUhqQjtBQUlFLDZCQUFhO0FBSmY7QUFERixhQWZGO0FBdUJFO0FBQUE7QUFBQTtBQUNFO0FBQ0Usc0JBQU0sVUFEUjtBQUVFLHVCQUFPNUMsZUFGVDtBQUdFLDBCQUFVLEtBQUs2QyxlQUhqQjtBQUlFLDZCQUFhO0FBSmY7QUFERixhQXZCRjtBQStCR0osc0JBQ0M7QUFBQTtBQUFBLGdCQUFhLFdBQVczQixRQUFRa0IsWUFBaEM7QUFDR1M7QUFESCxhQURELEdBSUcsSUFuQ047QUFvQ0U7QUFBQTtBQUFBO0FBQ0UscURBQUssV0FBVzNCLFFBQVFSLFFBQXhCLEdBREY7QUFFRTtBQUFBO0FBQUE7QUFDRSw4QkFERjtBQUVFLHlCQUFNLFFBRlI7QUFHRSwyQkFBUyxLQUFLd0MsUUFIaEI7QUFJRSw0QkFBVSxFQUFFL0MsZUFBZUMsZUFBZixJQUFrQ3dDLE9BQXBDO0FBSlo7QUFBQTtBQUFBO0FBRkY7QUFwQ0Y7QUFURjtBQURGLE9BREY7QUE4REQ7Ozs7O0FBR0hKLFNBQVNoQixTQUFULEdBQXFCO0FBQ25CTixXQUFTLG9CQUFVTyxNQUFWLENBQWlCQyxVQURQO0FBRW5CeUIsUUFBTSxvQkFBVU8sS0FBVixDQUFnQjtBQUNwQk4sUUFBSSxvQkFBVXRCLE1BQVYsQ0FBaUJKLFVBREQ7QUFFcEIxQyxXQUFPLG9CQUFVNkMsTUFBVixDQUFpQkgsVUFGSjtBQUdwQmlDLGNBQVUsb0JBQVU5QjtBQUhBLEdBQWhCLEVBSUhIO0FBTmdCLENBQXJCOztrQkFTZSwwQkFBV3BCLE1BQVgsRUFBbUJrQyxRQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3UGY7Ozs7Ozs7Ozs7Ozs7a0JBYWUsSUFBSW9CLE1BQUosQ0FDYix3REFEYSxDIiwiZmlsZSI6IjYyLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBjcmVhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5wYXNzd29yZCAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gY3JlYXRlKHsgdG9rZW4sIHVzZXJJZCwgcGFzc3dvcmQgfSkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vcGFzc3dvcmRgO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgcGFzc3dvcmQgfSksXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvdXNlcnMvcGFzc3dvcmQvY3JlYXRlLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGdldFxuICogQHBhcmFtIHtvYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBnZXQoeyB1c2VySWQsIHRva2VuIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L3Bhc3N3b3JkYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS91c2Vycy9wYXNzd29yZC9nZXQuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcblxuLyoqXG4gKlxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gdXBkYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQub2xkUGFzc3dvcmQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmV3UGFzc3dvcmQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY29uZmlybU5ld1Bhc3N3b3JkIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiB1cGRhdGUoe1xuICB0b2tlbixcbiAgdXNlcklkLFxuICBvbGRQYXNzd29yZCxcbiAgbmV3UGFzc3dvcmQsXG4gIGNvbmZpcm1QYXNzd29yZCxcbn0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L3Bhc3N3b3JkYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUFVUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgb2xkUGFzc3dvcmQsIG5ld1Bhc3N3b3JkLCBjb25maXJtUGFzc3dvcmQgfSksXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3VzZXJzL3Bhc3N3b3JkL3VwZGF0ZS5qcyIsIi8qKlxuICogVGhpcyBpbnB1dCBjb21wb25lbnQgaXMgbWFpbmx5IGRldmVsb3BlZCBmb3IgY3JlYXRlIHBvc3QsIGNyZWF0ZVxuICogY291cnNlIGNvbXBvbmVudC5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZGlzcGxheTogJ2ZsZXgnLFxuICB9LFxuICBpbnB1dDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gICAgYm9yZGVyOiAnMnB4IHNvbGlkICNkYWRhZGEnLFxuICAgIGJvcmRlclJhZGl1czogJzdweCcsXG4gICAgcGFkZGluZzogJzhweCcsXG4gICAgJyY6Zm9jdXMnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJyM5ZWNhZWQnLFxuICAgICAgYm94U2hhZG93OiAnMCAwIDEwcHggIzllY2FlZCcsXG4gICAgfSxcbiAgfSxcbn07XG5cbmNvbnN0IElucHV0ID0gKHsgY2xhc3Nlcywgb25DaGFuZ2UsIHZhbHVlLCBwbGFjZWhvbGRlciwgZGlzYWJsZWQsIHR5cGUgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICA8aW5wdXRcbiAgICAgIHR5cGU9e3R5cGUgfHwgJ3RleHQnfVxuICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyIHx8ICdpbnB1dCd9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMuaW5wdXR9XG4gICAgICBkaXNhYmxlZD17ISFkaXNhYmxlZH1cbiAgICAvPlxuICA8L2Rpdj5cbik7XG5cbklucHV0LnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgdmFsdWU6IFByb3BUeXBlcy5vbmVPZlR5cGUoW1Byb3BUeXBlcy5zdHJpbmcsIFByb3BUeXBlcy5udW1iZXJdKS5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgZGlzYWJsZWQ6IFByb3BUeXBlcy5ib29sLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKElucHV0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuXG5pbXBvcnQgSW5wdXQgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9JbnB1dCc7XG5cbmltcG9ydCBSRUdFWF9QQVNTV09SRCBmcm9tICcuLi8uLi8uLi9saWIvbWF5YXNoLWNvbW1vbi9SZWdFeHAvUGFzc3dvcmQnO1xuXG5pbXBvcnQgYXBpUGFzc3dvcmRHZXQgZnJvbSAnLi4vLi4vYXBpL3VzZXJzL3Bhc3N3b3JkL2dldCc7XG5pbXBvcnQgYXBpUGFzc3dvcmRDcmVhdGUgZnJvbSAnLi4vLi4vYXBpL3VzZXJzL3Bhc3N3b3JkL2NyZWF0ZSc7XG5pbXBvcnQgYXBpUGFzc3dvcmRVcGRhdGUgZnJvbSAnLi4vLi4vYXBpL3VzZXJzL3Bhc3N3b3JkL3VwZGF0ZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge30sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBncmlkSXRlbToge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbiAgZXJyb3JNZXNzZWdlOiB7XG4gICAgY29sb3I6ICdyZWQnLFxuICB9LFxuICBjYXJkSGVhZGVyOiB7XG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgfSxcbn07XG5cbmNsYXNzIFBhc3N3b3JkIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGlzUGFzc3dvcmRFeGl0OiBmYWxzZSxcbiAgICAgIGlzVmFsaWQ6IGZhbHNlLFxuICAgICAgZXJyb3I6ICcnLFxuICAgICAgbWVzc2FnZTogJycsXG4gICAgICBvbGRQYXNzd29yZDogJycsXG4gICAgICBuZXdQYXNzd29yZDogJycsXG4gICAgICBjb25maXJtUGFzc3dvcmQ6ICcnLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlT2xkID0gdGhpcy5vbkNoYW5nZU9sZC5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25DaGFuZ2VOZXcgPSB0aGlzLm9uQ2hhbmdlTmV3LmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNoYW5nZUNvbmZpcm0gPSB0aGlzLm9uQ2hhbmdlQ29uZmlybS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25TdWJtaXQgPSB0aGlzLm9uU3VibWl0LmJpbmQodGhpcyk7XG4gIH1cblxuICBhc3luYyBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IHRoaXMucHJvcHMudXNlcjtcbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSB9ID0gYXdhaXQgYXBpUGFzc3dvcmRHZXQoeyB1c2VySWQsIHRva2VuIH0pO1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlzUGFzc3dvcmRFeGl0OiBzdGF0dXNDb2RlID09PSAyMDAgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH1cbiAgfVxuXG4gIG9uQ2hhbmdlT2xkKGUpIHtcbiAgICBjb25zdCB7IHZhbHVlOiBwYXNzd29yZCB9ID0gZS50YXJnZXQ7XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBvbGRQYXNzd29yZDogcGFzc3dvcmQsXG4gICAgfSk7XG4gIH1cblxuICBvbkNoYW5nZU5ldyhlKSB7XG4gICAgY29uc3QgeyB2YWx1ZTogcGFzc3dvcmQgfSA9IGUudGFyZ2V0O1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgbmV3UGFzc3dvcmQ6IHBhc3N3b3JkLFxuICAgIH0pO1xuICAgIGlmICghUkVHRVhfUEFTU1dPUkQudGVzdChwYXNzd29yZCkpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICBtZXNzYWdlOlxuICAgICAgICAgICdOZXcgUGFzc3dvcmQgU2hvdWxkIGNvbnRhaW4gYXRsZWFzdCBvbmUgbG93ZXJjYXNlIGxldHRlciAsJyArXG4gICAgICAgICAgJ29uZSB1cHBlciBjYXNlIGxldHRlciAsb25lIGRpZ2l0IGFuZCBvbmx5IG9uZSBzcGVjaWFsIGNoYXJhY3Rlci4gJyArXG4gICAgICAgICAgJ01pbmltdW4gYW5kIG1heGltdW0gbGVuZ3RoIG9mIHBhc3N3b3JkIGlzIDUgJiAyMCAgJyxcbiAgICAgICAgaXNWYWxpZDogZmFsc2UsXG4gICAgICB9KTtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBpc1ZhbGlkOiB0cnVlLFxuICAgICAgbWVzc2FnZTogJycsXG4gICAgfSk7XG4gIH1cblxuICBvbkNoYW5nZUNvbmZpcm0oZSkge1xuICAgIGNvbnN0IHsgdmFsdWU6IHBhc3N3b3JkIH0gPSBlLnRhcmdldDtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGNvbmZpcm1QYXNzd29yZDogcGFzc3dvcmQsXG4gICAgfSk7XG4gIH1cblxuICBhc3luYyBvblN1Ym1pdCgpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3Qge1xuICAgICAgICBvbGRQYXNzd29yZCxcbiAgICAgICAgbmV3UGFzc3dvcmQsXG4gICAgICAgIGNvbmZpcm1QYXNzd29yZCxcbiAgICAgICAgaXNQYXNzd29yZEV4aXQsXG4gICAgICB9ID0gdGhpcy5zdGF0ZTtcbiAgICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IHRoaXMucHJvcHMudXNlcjtcblxuICAgICAgaWYgKG9sZFBhc3N3b3JkID09PSBuZXdQYXNzd29yZCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ1lvdXIgbmV3IHBhc3N3b3JkIGlzIHNhbWUgYXMgb2xkJyB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAobmV3UGFzc3dvcmQgIT09IGNvbmZpcm1QYXNzd29yZCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ1lvdXIgQ29uZmlybWVkIHBhc3N3b3JkIGlzIG5vdCBjb3JyZWN0JyB9KTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJycgfSk7XG4gICAgICBsZXQgc3RhdHVzQ29kZTtcblxuICAgICAgaWYgKGlzUGFzc3dvcmRFeGl0KSB7XG4gICAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IGFwaVBhc3N3b3JkVXBkYXRlKHtcbiAgICAgICAgICB0b2tlbixcbiAgICAgICAgICB1c2VySWQsXG4gICAgICAgICAgb2xkUGFzc3dvcmQsXG4gICAgICAgICAgbmV3UGFzc3dvcmQsXG4gICAgICAgICAgY29uZmlybVBhc3N3b3JkLFxuICAgICAgICB9KTtcblxuICAgICAgICBzdGF0dXNDb2RlID0gcmVzLnN0YXR1c0NvZGU7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCByZXMgPSBhd2FpdCBhcGlQYXNzd29yZENyZWF0ZSh7XG4gICAgICAgICAgdG9rZW4sXG4gICAgICAgICAgdXNlcklkLFxuICAgICAgICAgIHBhc3N3b3JkOiBuZXdQYXNzd29yZCxcbiAgICAgICAgfSk7XG5cbiAgICAgICAgc3RhdHVzQ29kZSA9IHJlcy5zdGF0dXNDb2RlO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2U6ICdTb3JyeSBVbmFibGUgdG8gcHJvY2VzcycgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2U6ICdZb3VyIHBhc3N3b3JkIHVwZGF0ZWQgc3VjY2Vzc2Z1bGx5JyB9KTtcblxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgICAgIG1lc3NhZ2U6ICcnLFxuICAgICAgICAgIG9sZFBhc3N3b3JkOiAnJyxcbiAgICAgICAgICBuZXdQYXNzd29yZDogJycsXG4gICAgICAgICAgY29uZmlybVBhc3N3b3JkOiAnJyxcbiAgICAgICAgICBpc1ZhbGlkOiBmYWxzZSxcbiAgICAgICAgfSk7XG4gICAgICB9LCAxNTAwKTtcbiAgICB9IGNhdGNoIChlKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGUpO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3Qge1xuICAgICAgb2xkUGFzc3dvcmQsXG4gICAgICBpc1ZhbGlkLFxuICAgICAgY29uZmlybVBhc3N3b3JkLFxuICAgICAgbmV3UGFzc3dvcmQsXG4gICAgICBpc1Bhc3N3b3JkRXhpdCxcbiAgICAgIG1lc3NhZ2UsXG4gICAgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEdyaWQgY29udGFpbmVyIGp1c3RpZnk9XCJjZW50ZXJcIiBzcGFjaW5nPXswfSBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxHcmlkXG4gICAgICAgICAgaXRlbVxuICAgICAgICAgIHhzPXsxMn1cbiAgICAgICAgICBzbT17MTB9XG4gICAgICAgICAgbWQ9ezh9XG4gICAgICAgICAgbGc9ezZ9XG4gICAgICAgICAgeGw9ezZ9XG4gICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmdyaWRJdGVtfVxuICAgICAgICA+XG4gICAgICAgICAgPENhcmQgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgICAgdGl0bGU9eydDaGFuZ2UgeW91ciBwYXNzd29yZCd9XG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIHtpc1Bhc3N3b3JkRXhpdCA/IChcbiAgICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgICAgICAgdHlwZT17J3Bhc3N3b3JkJ31cbiAgICAgICAgICAgICAgICAgIHZhbHVlPXtvbGRQYXNzd29yZH1cbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlT2xkfVxuICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eydFbnRlciBPbGQgUGFzc3dvcmQnfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPElucHV0XG4gICAgICAgICAgICAgICAgdHlwZT17J3Bhc3N3b3JkJ31cbiAgICAgICAgICAgICAgICB2YWx1ZT17bmV3UGFzc3dvcmR9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2VOZXd9XG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eydFbnRlciBOZXcgUGFzc3dvcmQnfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPElucHV0XG4gICAgICAgICAgICAgICAgdHlwZT17J3Bhc3N3b3JkJ31cbiAgICAgICAgICAgICAgICB2YWx1ZT17Y29uZmlybVBhc3N3b3JkfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlQ29uZmlybX1cbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17J0NvbmZpcm0gTmV3IFBhc3N3b3JkJ31cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICB7bWVzc2FnZSA/IChcbiAgICAgICAgICAgICAgPENhcmRDb250ZW50IGNsYXNzTmFtZT17Y2xhc3Nlcy5lcnJvck1lc3NlZ2V9PlxuICAgICAgICAgICAgICAgIHttZXNzYWdlfVxuICAgICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgICAgICAgcmFpc2VkXG4gICAgICAgICAgICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25TdWJtaXR9XG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9eyEobmV3UGFzc3dvcmQgJiYgY29uZmlybVBhc3N3b3JkICYmIGlzVmFsaWQpfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgU3VibWl0XG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cblBhc3N3b3JkLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB1c2VyOiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIGlkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gICAgdG9rZW46IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICB1c2VybmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgfSkuaXNSZXF1aXJlZCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShQYXNzd29yZCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUHJvZmlsZVBhZ2UvUGFzc3dvcmQuanMiLCIvKipcbiAqIFRoaXMgZmlsZSBjb250YWlucyB0aGUgUmVnRXhwIG9mIHBhc3N3b3JkLiBUaGUgcGFzc3dvcmQgc2hvdWxkXG4gKiBmb2xsb3cgdGhlc2UgcnVsZXMuXG4gKiAxLiBJdCBzaG91bGQgY29udGFpbiBhdGxlYXN0IG9uZSBsb3dlcmNhc2UgbGV0dGVyXG4gKiAyLiBJdCBzaG91bGQgY29udGFpbiBhdGxlYXN0IG9uZSB1cHBlcmNhc2UgbGV0dGVyXG4gKiAzLiBJdCBzaG91bGQgY29udGFpbiBhdGxlYXN0IG9uZSBkaWdpdFxuICogNC4gSXQgc2hvdWxkIGNvbnRhaW4gb25lIHNwZWNpYWwgY2hhcmFjdGVyXG4gKiA1LiBJdCdzIG1pbmltdW0gbGVuZ3RoIHNob3VsZCBiZSA1LlxuICogNi4gSXQncyBtYXhpbXVtIGxlbmd0aCBzaG91bGQgYmUgMjAuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmV4cG9ydCBkZWZhdWx0IG5ldyBSZWdFeHAoXG4gICcoKD89LipbMC05XSkoPz0uKlthLXpdKSg/PS4qW0EtWl0pKD89LipbQCMkJV0pLns1LDIwfSknLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWNvbW1vbi9SZWdFeHAvUGFzc3dvcmQuanMiXSwic291cmNlUm9vdCI6IiJ9