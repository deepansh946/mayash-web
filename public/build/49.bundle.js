webpackJsonp([49],{

/***/ "./node_modules/material-ui-icons/Edit.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Edit.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z' });

var Edit = function Edit(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Edit = (0, _pure2.default)(Edit);
Edit.muiName = 'SvgIcon';

exports.default = Edit;

/***/ }),

/***/ "./node_modules/material-ui-icons/Favorite.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/Favorite.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z' });

var Favorite = function Favorite(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Favorite = (0, _pure2.default)(Favorite);
Favorite.muiName = 'SvgIcon';

exports.default = Favorite;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui-icons/MoreVert.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/MoreVert.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });

var MoreVert = function MoreVert(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

MoreVert = (0, _pure2.default)(MoreVert);
MoreVert.muiName = 'SvgIcon';

exports.default = MoreVert;

/***/ }),

/***/ "./node_modules/material-ui-icons/Save.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Save.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z' });

var Save = function Save(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Save = (0, _pure2.default)(Save);
Save.muiName = 'SvgIcon';

exports.default = Save;

/***/ }),

/***/ "./node_modules/material-ui-icons/Share.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Share.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z' });

var Share = function Share(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Share = (0, _pure2.default)(Share);
Share.muiName = 'SvgIcon';

exports.default = Share;

/***/ }),

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/courses/update.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/update.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus -
 * @param {Object[]} payload.courseModules -
 * @param {Object} payload.courseModules[] -
 * @param {number} payload.courseModules[].moduleId -
 *
 * @return {Object} -
 */
var update = function update(payload) {
  return { type: _courses.COURSE_UPDATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = update;

/***/ }),

/***/ "./src/client/api/courses/users/update.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/update.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Update Course
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {string} payload.cover -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus -
 * @param {Object[]} payload.courseModules -
 * @param {number} payload.courseModules[].moduleId -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * 
 */

var updateCourse = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        courseId = _ref2.courseId,
        token = _ref2.token,
        cover = _ref2.cover,
        title = _ref2.title,
        description = _ref2.description,
        syllabus = _ref2.syllabus,
        courseModules = _ref2.courseModules;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({
                cover: cover,
                title: title,
                description: description,
                syllabus: syllabus,
                courseModules: courseModules
              })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function updateCourse(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = updateCourse;

/***/ }),

/***/ "./src/client/api/photos/courses/users/create.js":
/*!*******************************************************!*\
  !*** ./src/client/api/photos/courses/users/create.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @async
 * @function
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.postId
 * @param {Object} payload.formData
 * @returns {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

exports.default = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId,
        formData = _ref2.formData;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId + '/photos';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                // 'Content-Type': 'application/json',
                Authorization: token
              },
              body: formData
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  function create(_x) {
    return _ref.apply(this, arguments);
  }

  return create;
}();

/***/ }),

/***/ "./src/client/components/Description.js":
/*!**********************************************!*\
  !*** ./src/client/components/Description.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit'
  }
}; /**
    * Description component is for editable component to edit description
    * with textarea.
    * This component should be used with CardHeader subheader place.
    * TODO: make textarea autoresizable.
    *
    * @format
    */

var Description = function Description(_ref) {
  var classes = _ref.classes,
      readOnly = _ref.readOnly,
      placeholder = _ref.placeholder,
      value = _ref.value,
      onChange = _ref.onChange,
      minLength = _ref.minLength,
      maxLength = _ref.maxLength;

  if (readOnly) {
    return _react2.default.createElement(
      'div',
      { className: classes.value },
      value
    );
  }

  return _react2.default.createElement('textarea', {
    placeholder: placeholder,
    value: value,
    onChange: onChange,
    className: classes.textarea,
    minLength: minLength || 1,
    maxLength: maxLength || 300,
    rows: 1,
    readOnly: readOnly
  });
};

Description.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  readOnly: _propTypes2.default.bool.isRequired,
  placeholder: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.string,
  onChange: _propTypes2.default.func.isRequired,
  minLength: _propTypes2.default.number,
  maxLength: _propTypes2.default.number
};

Description.defaultProps = {
  placeholder: 'Description goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 300
};

exports.default = (0, _styles.withStyles)(styles)(Description);

/***/ }),

/***/ "./src/client/components/Title.js":
/*!****************************************!*\
  !*** ./src/client/components/Title.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit'
  }
}; /**
    * Title component is for editable component to edit title with textarea.
    * This component should be used with CardHeader title place.
    * TODO: make textarea autoresizable.
    *
    * @format
    */

var Title = function Title(_ref) {
  var classes = _ref.classes,
      readOnly = _ref.readOnly,
      value = _ref.value,
      placeholder = _ref.placeholder,
      onChange = _ref.onChange,
      minLength = _ref.minLength,
      maxLength = _ref.maxLength;

  if (readOnly) {
    return _react2.default.createElement(
      'div',
      { className: classes.value },
      value
    );
  }

  return _react2.default.createElement('textarea', {
    placeholder: placeholder,
    value: value,
    onChange: onChange,
    className: classes.textarea,
    minLength: minLength || 1,
    maxLength: maxLength || 148,
    rows: 1,
    readOnly: readOnly
  });
};

Title.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  readOnly: _propTypes2.default.bool.isRequired,
  placeholder: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.string.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  minLength: _propTypes2.default.number,
  maxLength: _propTypes2.default.number
};

Title.defaultProps = {
  value: 'Untitled',
  placeholder: 'Title goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 148
};

exports.default = (0, _styles.withStyles)(styles)(Title);

/***/ }),

/***/ "./src/client/containers/CoursePage/CoverImage/index.js":
/*!**************************************************************!*\
  !*** ./src/client/containers/CoursePage/CoverImage/index.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _update = __webpack_require__(/*! ../../../api/courses/users/update */ "./src/client/api/courses/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _create = __webpack_require__(/*! ../../../api/photos/courses/users/create */ "./src/client/api/photos/courses/users/create.js");

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @format
 */

var CoverImage = function (_Component) {
  (0, _inherits3.default)(CoverImage, _Component);

  function CoverImage(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, CoverImage);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CoverImage.__proto__ || (0, _getPrototypeOf2.default)(CoverImage)).call(this, props));

    _this.onChange = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _this$props, userId, courseId, token, _ref2, statusCode, error, payload, cover;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (e) {
                  e.preventDefault();
                }

                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 16;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _this$props = _this.props, userId = _this$props.authorId, courseId = _this$props.courseId, token = _this$props.token;
                _context.next = 8;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  formData: formData
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;


                if (statusCode >= 300) {
                  // handle Error
                  console.error(statusCode, error);
                }

                cover = payload.photoUrl;


                (0, _update2.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  cover: cover
                });

                _this.props.actionCourseUpdate({
                  courseId: courseId,
                  cover: cover
                });

              case 16:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.state = {};

    _this.onClick = _this.onClick.bind(_this);
    // this.onChange = this.onChange.bind(this);
    return _this;
  }

  (0, _createClass3.default)(CoverImage, [{
    key: 'onClick',
    value: function onClick() {
      this.photo.value = null;
      this.photo.click();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      return _react2.default.createElement(
        'div',
        {
          style: (0, _extends3.default)({
            position: 'relative'
          }, this.props.style),
          className: this.props.className
        },
        _react2.default.createElement(
          _IconButton2.default,
          { 'aria-label': 'Insert Photo', onClick: this.onClick },
          _react2.default.createElement(_InsertPhoto2.default, null),
          _react2.default.createElement('input', {
            type: 'file',
            accept: 'image/jpeg|png|gif',
            onChange: this.onChange,
            ref: function ref(photo) {
              _this3.photo = photo;
            },
            style: { display: 'none' }
          })
        )
      );
    }
  }]);
  return CoverImage;
}(_react.Component);

CoverImage.propTypes = {
  style: _propTypes2.default.object.isRequired,
  className: _propTypes2.default.string.isRequired,
  actionCourseUpdate: _propTypes2.default.func.isRequired
};
exports.default = CoverImage;

/***/ }),

/***/ "./src/client/containers/CoursePage/Introduction.js":
/*!**********************************************************!*\
  !*** ./src/client/containers/CoursePage/Introduction.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouterDom = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Edit = __webpack_require__(/*! material-ui-icons/Edit */ "./node_modules/material-ui-icons/Edit.js");

var _Edit2 = _interopRequireDefault(_Edit);

var _Save = __webpack_require__(/*! material-ui-icons/Save */ "./node_modules/material-ui-icons/Save.js");

var _Save2 = _interopRequireDefault(_Save);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! ../../components/Title */ "./src/client/components/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _Description = __webpack_require__(/*! ../../components/Description */ "./src/client/components/Description.js");

var _Description2 = _interopRequireDefault(_Description);

var _CoverImage = __webpack_require__(/*! ./CoverImage */ "./src/client/containers/CoursePage/CoverImage/index.js");

var _CoverImage2 = _interopRequireDefault(_CoverImage);

var _update = __webpack_require__(/*! ../../api/courses/users/update */ "./src/client/api/courses/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _update3 = __webpack_require__(/*! ../../actions/courses/update */ "./src/client/actions/courses/update.js");

var _update4 = _interopRequireDefault(_update3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import RatingStar from '../../components/RatingStar';
var styles = function styles(theme) {
  var _editButton;

  return {
    root: {
      padding: '1%'
    },
    card: {
      borderRadius: '8px'
    },
    title: {
      cursor: 'pointer',
      textDecoration: 'none'
    },
    subheader: {
      cursor: 'pointer',
      textDecoration: 'none'
    },
    avatar: {
      borderRadius: '50%'
    },
    media: {
      height: 194,
      borderRadius: '8px 8px 0px 0px'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    margin: {
      border: '0.5px solid rgba(209,209,209,1)',
      marginBottom: '-10px'
    },
    editButton: (_editButton = {
      position: 'fixed'
    }, (0, _defineProperty3.default)(_editButton, theme.breakpoints.up('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('md'), {
      bottom: '30px',
      right: '30px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('sm'), {
      display: 'none',
      bottom: '25px',
      right: '25px'
    }), _editButton)
  };
}; /** @format */

var Introduction = function (_Component) {
  (0, _inherits3.default)(Introduction, _Component);

  function Introduction(props) {
    (0, _classCallCheck3.default)(this, Introduction);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Introduction.__proto__ || (0, _getPrototypeOf2.default)(Introduction)).call(this, props));

    _this.onChangeTitle = function (e) {
      return _this.setState({ title: e.target.value });
    };

    _this.onChangeDescription = function (e) {
      return _this.setState({ description: e.target.value });
    };

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    var userId = _this.props.elements.user.id;
    var _this$props$course = _this.props.course,
        authorId = _this$props$course.authorId,
        title = _this$props$course.title,
        description = _this$props$course.description;


    _this.state = {
      edit: false,
      // this value will be true only if user is the author of course.
      isAuthor: userId === authorId,
      title: title,
      description: description
    };

    _this.onEdit = _this.onEdit.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Introduction, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var _props = this.props,
          elements = _props.elements,
          course = _props.course;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = course.authorId;


      var author = void 0;
      if (authorId === userId) {
        author = elements.user;
      } else {
        author = elements[authorId];
      }

      if (!author || !author.isFetching && !author.isFetched) {
        this.props.actionElementGetById({ token: token, id: authorId });
      }
    }
  }, {
    key: 'onEdit',
    value: function onEdit() {
      var edit = this.state.edit;

      this.setState({ edit: !edit });
    }
  }, {
    key: 'onSave',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var user, userId, token, _props$course, courseId, authorId, _state, edit, title, description, _ref2, statusCode, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                user = this.props.elements.user;
                userId = user.id, token = user.token;
                _props$course = this.props.course, courseId = _props$course.courseId, authorId = _props$course.authorId;
                _state = this.state, edit = _state.edit, title = _state.title, description = _state.description;
                _context.next = 6;
                return (0, _update2.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  title: title,
                  description: description
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;

                if (!(statusCode !== 200)) {
                  _context.next = 12;
                  break;
                }

                // handle error
                console.error(error);
                return _context.abrupt('return');

              case 12:

                this.props.actionCourseUpdate({
                  courseId: courseId,
                  authorId: authorId,
                  title: title,
                  description: description
                });

                this.setState({ edit: !edit });

              case 14:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSave() {
        return _ref.apply(this, arguments);
      }

      return onSave;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          classes = _props2.classes,
          course = _props2.course,
          elements = _props2.elements;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id,
          token = user.token;
      var authorId = course.authorId,
          cover = course.cover;

      var author = { authorId: authorId };

      if (userId === authorId) {
        author = (0, _extends3.default)({}, author, user);
      } else {
        author = (0, _extends3.default)({}, author, elements[authorId]);
      }

      var _state2 = this.state,
          hover = _state2.hover,
          edit = _state2.edit,
          title = _state2.title,
          description = _state2.description;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 12, md: 10, lg: 8, xl: 8 },
          _react2.default.createElement(
            _Card2.default,
            {
              raised: hover,
              className: classes.card,
              onMouseEnter: this.onMouseEnter,
              onMouseLeave: this.onMouseLeave
            },
            userId === authorId ? _react2.default.createElement(_Card.CardMedia, {
              className: classes.media,
              component: function component(thisProp) {
                return _react2.default.createElement(_CoverImage2.default, (0, _extends3.default)({}, thisProp, course, {
                  token: token,
                  actionCourseUpdate: _this2.props.actionCourseUpdate
                }));
              },
              image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
            }) : _react2.default.createElement(_Card.CardMedia, {
              className: classes.media,
              image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
            }),
            _react2.default.createElement(_Card.CardHeader, {
              avatar: _react2.default.createElement(
                _Paper2.default,
                { className: classes.avatar, elevation: 1 },
                _react2.default.createElement(
                  _reactRouterDom.Link,
                  { to: '/@' + author.username },
                  _react2.default.createElement(_Avatar2.default, {
                    alt: author.name,
                    src: author.avatar || '/public/photos/mayash-logo-transparent.png'
                  })
                )
              ),
              action: _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(
                  _Tooltip2.default,
                  { title: 'Coming soon', placement: 'bottom' },
                  _react2.default.createElement(
                    _IconButton2.default,
                    { disabled: true },
                    _react2.default.createElement(_MoreVert2.default, null)
                  )
                )
              ),
              title: _react2.default.createElement(
                _reactRouterDom.Link,
                { to: '/@' + author.username, className: classes.title },
                author.name
              ),
              subheader: _react2.default.createElement(
                _reactRouterDom.Link,
                { to: '/@' + author.username, className: classes.subheader },
                '@' + author.username
              )
            }),
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(
                _Typography2.default,
                { type: 'title', component: 'div' },
                _react2.default.createElement(_Title2.default, {
                  readOnly: !edit,
                  value: title,
                  onChange: this.onChangeTitle
                })
              ),
              _react2.default.createElement(
                _Typography2.default,
                { type: 'body1', component: 'div', gutterBottom: true },
                typeof description === 'string' || edit ? _react2.default.createElement(_Description2.default, {
                  readOnly: !edit,
                  value: description,
                  onChange: this.onChangeDescription
                }) : null
              )
            ),
            _react2.default.createElement('div', { className: classes.margin }),
            _react2.default.createElement(
              _Card.CardActions,
              { disableActionSpacing: true },
              _react2.default.createElement('div', null),
              _react2.default.createElement(
                _Tooltip2.default,
                { title: 'Add to favorites', placement: 'bottom' },
                _react2.default.createElement(
                  _IconButton2.default,
                  { 'aria-label': 'Add to favorites', disabled: true },
                  _react2.default.createElement(_Favorite2.default, null)
                )
              ),
              _react2.default.createElement(
                _Tooltip2.default,
                { title: 'Share', placement: 'bottom' },
                _react2.default.createElement(
                  _IconButton2.default,
                  { 'aria-label': 'Share', disabled: true },
                  _react2.default.createElement(_Share2.default, null)
                )
              )
            )
          )
        ),
        isSignedIn && authorId === userId && _react2.default.createElement(
          _Button2.default,
          {
            fab: true,
            color: 'accent',
            'aria-label': 'Edit',
            className: classes.editButton,
            onClick: edit ? this.onSave : this.onEdit
          },
          edit ? _react2.default.createElement(_Save2.default, null) : _react2.default.createElement(_Edit2.default, null)
        )
      );
    }
  }]);
  return Introduction;
}(_react.Component);

Introduction.propTypes = {
  /** classes object comes from withStyle component wrapper. */
  classes: _propTypes2.default.object.isRequired,

  /** elements comes from Redux store */
  elements: _propTypes2.default.object.isRequired,

  /** course object comes from parent component. */
  course: _propTypes2.default.object.isRequired,

  /** these actions comes from mapDispatchToProps()  */
  actionElementGetById: _propTypes2.default.func.isRequired,
  actionCourseUpdate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionElementGetById: _getById2.default,
    actionCourseUpdate: _update4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(Introduction));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2F2ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvdXBkYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvdXBkYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3Bob3Rvcy9jb3Vyc2VzL3VzZXJzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvRGVzY3JpcHRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL1RpdGxlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL0NvdmVySW1hZ2UvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvSW50cm9kdWN0aW9uLmpzIl0sIm5hbWVzIjpbInVwZGF0ZSIsInBheWxvYWQiLCJ0eXBlIiwidXNlcklkIiwiY291cnNlSWQiLCJ0b2tlbiIsImNvdmVyIiwidGl0bGUiLCJkZXNjcmlwdGlvbiIsInN5bGxhYnVzIiwiY291cnNlTW9kdWxlcyIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwiYm9keSIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsInVwZGF0ZUNvdXJzZSIsImZvcm1EYXRhIiwiY3JlYXRlIiwic3R5bGVzIiwidmFsdWUiLCJ0ZXh0YXJlYSIsIndpZHRoIiwiaGVpZ2h0IiwiYm9yZGVyIiwicmVzaXplIiwiZm9udCIsIkRlc2NyaXB0aW9uIiwiY2xhc3NlcyIsInJlYWRPbmx5IiwicGxhY2Vob2xkZXIiLCJvbkNoYW5nZSIsIm1pbkxlbmd0aCIsIm1heExlbmd0aCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJib29sIiwic3RyaW5nIiwiZnVuYyIsIm51bWJlciIsImRlZmF1bHRQcm9wcyIsIlRpdGxlIiwiQ292ZXJJbWFnZSIsInByb3BzIiwiZSIsInByZXZlbnREZWZhdWx0IiwiZmlsZSIsInRhcmdldCIsImZpbGVzIiwiaW5kZXhPZiIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiYXV0aG9ySWQiLCJwaG90b1VybCIsImFjdGlvbkNvdXJzZVVwZGF0ZSIsInN0YXRlIiwib25DbGljayIsImJpbmQiLCJwaG90byIsImNsaWNrIiwicG9zaXRpb24iLCJzdHlsZSIsImNsYXNzTmFtZSIsImRpc3BsYXkiLCJ0aGVtZSIsInJvb3QiLCJwYWRkaW5nIiwiY2FyZCIsImJvcmRlclJhZGl1cyIsImN1cnNvciIsInRleHREZWNvcmF0aW9uIiwic3ViaGVhZGVyIiwiYXZhdGFyIiwibWVkaWEiLCJmbGV4R3JvdyIsImZsZXgiLCJtYXJnaW4iLCJtYXJnaW5Cb3R0b20iLCJlZGl0QnV0dG9uIiwiYnJlYWtwb2ludHMiLCJ1cCIsImJvdHRvbSIsInJpZ2h0IiwiZG93biIsIkludHJvZHVjdGlvbiIsIm9uQ2hhbmdlVGl0bGUiLCJzZXRTdGF0ZSIsIm9uQ2hhbmdlRGVzY3JpcHRpb24iLCJvbk1vdXNlRW50ZXIiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsImVsZW1lbnRzIiwidXNlciIsImlkIiwiY291cnNlIiwiZWRpdCIsImlzQXV0aG9yIiwib25FZGl0Iiwib25TYXZlIiwiYXV0aG9yIiwiaXNGZXRjaGluZyIsImlzRmV0Y2hlZCIsImFjdGlvbkVsZW1lbnRHZXRCeUlkIiwiaXNTaWduZWRJbiIsInRoaXNQcm9wIiwidXNlcm5hbWUiLCJuYW1lIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCw2SkFBNko7O0FBRS9NO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxzTEFBc0w7O0FBRXhPO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxnSUFBZ0k7O0FBRWxMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsOEI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx5SkFBeUo7O0FBRTNNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxzSkFBc0o7O0FBRXhNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCwrV0FBK1c7O0FBRWphO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsd0I7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGO0FBQzlGOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGlFQUFpRSxnQ0FBZ0M7QUFDakcsU0FBUztBQUNUO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELG9CQUFvQixVOzs7Ozs7Ozs7Ozs7O0FDL016RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STtBQUM1STs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RTtBQUM5RTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBLGFBQWEsMEJBQTBCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHdCQUF3QixjOzs7Ozs7Ozs7Ozs7O0FDck83RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7Ozs7QUNyQkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2ZBOztBQUVBOztBQUVBLG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUTtBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLDhEOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ1pBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix5Qzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUN6REE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixVQUFVLHFCQUFxQjtBQUM1RDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVEE7O0FBbUJBOzs7Ozs7Ozs7Ozs7OztBQWNBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLENBQWYsQyxDQXRDQTs7Ozs7a0JBd0NlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25CZjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBckJBOzs7Ozs7c0ZBdUNBO0FBQUEsUUFDRUcsTUFERixTQUNFQSxNQURGO0FBQUEsUUFFRUMsUUFGRixTQUVFQSxRQUZGO0FBQUEsUUFHRUMsS0FIRixTQUdFQSxLQUhGO0FBQUEsUUFJRUMsS0FKRixTQUlFQSxLQUpGO0FBQUEsUUFLRUMsS0FMRixTQUtFQSxLQUxGO0FBQUEsUUFNRUMsV0FORixTQU1FQSxXQU5GO0FBQUEsUUFPRUMsUUFQRixTQU9FQSxRQVBGO0FBQUEsUUFRRUMsYUFSRixTQVFFQSxhQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBV1VDLGVBWFYsa0NBV3FDUixNQVhyQyxpQkFXdURDLFFBWHZEO0FBQUE7QUFBQSxtQkFhc0IsK0JBQU1PLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlVjtBQUhSLGVBRmtCO0FBTzNCVyxvQkFBTSx5QkFBZTtBQUNuQlYsNEJBRG1CO0FBRW5CQyw0QkFGbUI7QUFHbkJDLHdDQUhtQjtBQUluQkMsa0NBSm1CO0FBS25CQztBQUxtQixlQUFmO0FBUHFCLGFBQVgsQ0FidEI7O0FBQUE7QUFhVU8sZUFiVjtBQTZCWUMsa0JBN0JaLEdBNkJtQ0QsR0E3Qm5DLENBNkJZQyxNQTdCWixFQTZCb0JDLFVBN0JwQixHQTZCbUNGLEdBN0JuQyxDQTZCb0JFLFVBN0JwQjs7QUFBQSxrQkErQlFELFVBQVUsR0EvQmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdDYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWhDYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0N1QkYsSUFBSUssSUFBSixFQXRDdkI7O0FBQUE7QUFzQ1VBLGdCQXRDVjtBQUFBLHdFQXdDZ0JBLElBeENoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMENJQyxvQkFBUUYsS0FBUjs7QUExQ0osNkNBNENXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVDWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZUcsWTs7Ozs7QUFsQ2Y7Ozs7QUFDQTs7OztrQkFvRmVBLFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNyRmY7Ozs7QUFFQTs7OztBQW9CQTs7Ozs7Ozs7Ozs7OztBQTNCQTs7Ozs7O3NGQXdDZTtBQUFBLFFBQ2JuQixLQURhLFNBQ2JBLEtBRGE7QUFBQSxRQUViRixNQUZhLFNBRWJBLE1BRmE7QUFBQSxRQUdiQyxRQUhhLFNBR2JBLFFBSGE7QUFBQSxRQUlicUIsUUFKYSxTQUliQSxRQUphO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBT0xkLGVBUEssa0NBT3NCUixNQVB0QixpQkFPd0NDLFFBUHhDO0FBQUE7QUFBQSxtQkFTTywrQkFBTU8sR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxNQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUDtBQUNBQywrQkFBZVY7QUFIUixlQUZrQjtBQU8zQlcsb0JBQU1TO0FBUHFCLGFBQVgsQ0FUUDs7QUFBQTtBQVNMUixlQVRLO0FBbUJIQyxrQkFuQkcsR0FtQm9CRCxHQW5CcEIsQ0FtQkhDLE1BbkJHLEVBbUJLQyxVQW5CTCxHQW1Cb0JGLEdBbkJwQixDQW1CS0UsVUFuQkw7O0FBQUEsa0JBcUJQRCxVQUFVLEdBckJIO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQXNCRjtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQXRCRTs7QUFBQTtBQUFBO0FBQUEsbUJBNEJRRixJQUFJSyxJQUFKLEVBNUJSOztBQUFBO0FBNEJMQSxnQkE1Qks7QUFBQSx3RUE4QkNBLElBOUJEOztBQUFBO0FBQUE7QUFBQTs7QUFnQ1hDLG9CQUFRRixLQUFSOztBQWhDVyw2Q0FrQ0o7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBbENJOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O1dBQWVLLE07Ozs7U0FBQUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQjlCOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBLElBQU1DLFNBQVM7QUFDYkMsU0FBTyxFQURNO0FBRWJDLFlBQVU7QUFDUkMsV0FBTyxNQURDO0FBRVJDLFlBQVEsTUFGQTtBQUdSQyxZQUFRLE1BSEE7QUFJUjtBQUNBQyxZQUFRLE1BTEE7QUFNUkMsVUFBTTtBQU5FO0FBRkcsQ0FBZixDLENBZEE7Ozs7Ozs7OztBQTBCQSxJQUFNQyxjQUFjLFNBQWRBLFdBQWMsT0FRZDtBQUFBLE1BUEpDLE9BT0ksUUFQSkEsT0FPSTtBQUFBLE1BTkpDLFFBTUksUUFOSkEsUUFNSTtBQUFBLE1BTEpDLFdBS0ksUUFMSkEsV0FLSTtBQUFBLE1BSkpWLEtBSUksUUFKSkEsS0FJSTtBQUFBLE1BSEpXLFFBR0ksUUFISkEsUUFHSTtBQUFBLE1BRkpDLFNBRUksUUFGSkEsU0FFSTtBQUFBLE1BREpDLFNBQ0ksUUFESkEsU0FDSTs7QUFDSixNQUFJSixRQUFKLEVBQWM7QUFDWixXQUFPO0FBQUE7QUFBQSxRQUFLLFdBQVdELFFBQVFSLEtBQXhCO0FBQWdDQTtBQUFoQyxLQUFQO0FBQ0Q7O0FBRUQsU0FDRTtBQUNFLGlCQUFhVSxXQURmO0FBRUUsV0FBT1YsS0FGVDtBQUdFLGNBQVVXLFFBSFo7QUFJRSxlQUFXSCxRQUFRUCxRQUpyQjtBQUtFLGVBQVdXLGFBQWEsQ0FMMUI7QUFNRSxlQUFXQyxhQUFhLEdBTjFCO0FBT0UsVUFBTSxDQVBSO0FBUUUsY0FBVUo7QUFSWixJQURGO0FBWUQsQ0F6QkQ7O0FBMkJBRixZQUFZTyxTQUFaLEdBQXdCO0FBQ3RCTixXQUFTLG9CQUFVTyxNQUFWLENBQWlCQyxVQURKO0FBRXRCUCxZQUFVLG9CQUFVUSxJQUFWLENBQWVELFVBRkg7QUFHdEJOLGVBQWEsb0JBQVVRLE1BQVYsQ0FBaUJGLFVBSFI7QUFJdEJoQixTQUFPLG9CQUFVa0IsTUFKSztBQUt0QlAsWUFBVSxvQkFBVVEsSUFBVixDQUFlSCxVQUxIO0FBTXRCSixhQUFXLG9CQUFVUSxNQU5DO0FBT3RCUCxhQUFXLG9CQUFVTztBQVBDLENBQXhCOztBQVVBYixZQUFZYyxZQUFaLEdBQTJCO0FBQ3pCWCxlQUFhLDBCQURZO0FBRXpCRCxZQUFVLElBRmU7QUFHekJHLGFBQVcsQ0FIYztBQUl6QkMsYUFBVztBQUpjLENBQTNCOztrQkFPZSx3QkFBV2QsTUFBWCxFQUFtQlEsV0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlEZjs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQSxJQUFNUixTQUFTO0FBQ2JDLFNBQU8sRUFETTtBQUViQyxZQUFVO0FBQ1JDLFdBQU8sTUFEQztBQUVSQyxZQUFRLE1BRkE7QUFHUkMsWUFBUSxNQUhBO0FBSVI7QUFDQUMsWUFBUSxNQUxBO0FBTVJDLFVBQU07QUFORTtBQUZHLENBQWYsQyxDQWJBOzs7Ozs7OztBQXlCQSxJQUFNZ0IsUUFBUSxTQUFSQSxLQUFRLE9BUVI7QUFBQSxNQVBKZCxPQU9JLFFBUEpBLE9BT0k7QUFBQSxNQU5KQyxRQU1JLFFBTkpBLFFBTUk7QUFBQSxNQUxKVCxLQUtJLFFBTEpBLEtBS0k7QUFBQSxNQUpKVSxXQUlJLFFBSkpBLFdBSUk7QUFBQSxNQUhKQyxRQUdJLFFBSEpBLFFBR0k7QUFBQSxNQUZKQyxTQUVJLFFBRkpBLFNBRUk7QUFBQSxNQURKQyxTQUNJLFFBREpBLFNBQ0k7O0FBQ0osTUFBSUosUUFBSixFQUFjO0FBQ1osV0FBTztBQUFBO0FBQUEsUUFBSyxXQUFXRCxRQUFRUixLQUF4QjtBQUFnQ0E7QUFBaEMsS0FBUDtBQUNEOztBQUVELFNBQ0U7QUFDRSxpQkFBYVUsV0FEZjtBQUVFLFdBQU9WLEtBRlQ7QUFHRSxjQUFVVyxRQUhaO0FBSUUsZUFBV0gsUUFBUVAsUUFKckI7QUFLRSxlQUFXVyxhQUFhLENBTDFCO0FBTUUsZUFBV0MsYUFBYSxHQU4xQjtBQU9FLFVBQU0sQ0FQUjtBQVFFLGNBQVVKO0FBUlosSUFERjtBQVlELENBekJEOztBQTJCQWEsTUFBTVIsU0FBTixHQUFrQjtBQUNoQk4sV0FBUyxvQkFBVU8sTUFBVixDQUFpQkMsVUFEVjtBQUVoQlAsWUFBVSxvQkFBVVEsSUFBVixDQUFlRCxVQUZUO0FBR2hCTixlQUFhLG9CQUFVUSxNQUFWLENBQWlCRixVQUhkO0FBSWhCaEIsU0FBTyxvQkFBVWtCLE1BQVYsQ0FBaUJGLFVBSlI7QUFLaEJMLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUgsVUFMVDtBQU1oQkosYUFBVyxvQkFBVVEsTUFOTDtBQU9oQlAsYUFBVyxvQkFBVU87QUFQTCxDQUFsQjs7QUFVQUUsTUFBTUQsWUFBTixHQUFxQjtBQUNuQnJCLFNBQU8sVUFEWTtBQUVuQlUsZUFBYSxvQkFGTTtBQUduQkQsWUFBVSxJQUhTO0FBSW5CRyxhQUFXLENBSlE7QUFLbkJDLGFBQVc7QUFMUSxDQUFyQjs7a0JBUWUsd0JBQVdkLE1BQVgsRUFBbUJ1QixLQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsRWY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFYQTs7OztJQWFNQyxVOzs7QUFPSixzQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBOztBQUFBLDhJQUNYQSxLQURXOztBQUFBLFVBYW5CYixRQWJtQjtBQUFBLDBGQWFSLGlCQUFPYyxDQUFQO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDVCxvQkFBSUEsQ0FBSixFQUFPO0FBQ0xBLG9CQUFFQyxjQUFGO0FBQ0Q7O0FBRUtDLG9CQUxHLEdBS0lGLEVBQUVHLE1BQUYsQ0FBU0MsS0FBVCxDQUFlLENBQWYsQ0FMSjs7QUFBQSxzQkFPTEYsS0FBS3JELElBQUwsQ0FBVXdELE9BQVYsQ0FBa0IsUUFBbEIsTUFBZ0MsQ0FQM0I7QUFBQTtBQUFBO0FBQUE7O0FBUURqQyx3QkFSQyxHQVFVLElBQUlrQyxRQUFKLEVBUlY7O0FBU1BsQyx5QkFBU21DLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBeUJMLElBQXpCOztBQVRPLDhCQVd1QyxNQUFLSCxLQVg1QyxFQVdXakQsTUFYWCxlQVdDMEQsUUFYRCxFQVdtQnpELFFBWG5CLGVBV21CQSxRQVhuQixFQVc2QkMsS0FYN0IsZUFXNkJBLEtBWDdCO0FBQUE7QUFBQSx1QkFhc0Msc0JBQWU7QUFDMURBLDhCQUQwRDtBQUUxREYsZ0NBRjBEO0FBRzFEQyxvQ0FIMEQ7QUFJMURxQjtBQUowRCxpQkFBZixDQWJ0Qzs7QUFBQTtBQUFBO0FBYUNMLDBCQWJELFNBYUNBLFVBYkQ7QUFhYUMscUJBYmIsU0FhYUEsS0FiYjtBQWFvQnBCLHVCQWJwQixTQWFvQkEsT0FicEI7OztBQW9CUCxvQkFBSW1CLGNBQWMsR0FBbEIsRUFBdUI7QUFDckI7QUFDQUcsMEJBQVFGLEtBQVIsQ0FBY0QsVUFBZCxFQUEwQkMsS0FBMUI7QUFDRDs7QUFFaUJmLHFCQXpCWCxHQXlCcUJMLE9BekJyQixDQXlCQzZELFFBekJEOzs7QUEyQlAsc0NBQWdCO0FBQ2R6RCw4QkFEYztBQUVkRixnQ0FGYztBQUdkQyxvQ0FIYztBQUlkRTtBQUpjLGlCQUFoQjs7QUFPQSxzQkFBSzhDLEtBQUwsQ0FBV1csa0JBQVgsQ0FBOEI7QUFDNUIzRCxvQ0FENEI7QUFFNUJFO0FBRjRCLGlCQUE5Qjs7QUFsQ087QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0FiUTs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFFakIsVUFBSzBELEtBQUwsR0FBYSxFQUFiOztBQUVBLFVBQUtDLE9BQUwsR0FBZSxNQUFLQSxPQUFMLENBQWFDLElBQWIsT0FBZjtBQUNBO0FBTGlCO0FBTWxCOzs7OzhCQUVTO0FBQ1IsV0FBS0MsS0FBTCxDQUFXdkMsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUt1QyxLQUFMLENBQVdDLEtBQVg7QUFDRDs7OzZCQTJDUTtBQUFBOztBQUNQLGFBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRUMsc0JBQVU7QUFEWixhQUVLLEtBQUtqQixLQUFMLENBQVdrQixLQUZoQixDQURGO0FBS0UscUJBQVcsS0FBS2xCLEtBQUwsQ0FBV21CO0FBTHhCO0FBT0U7QUFBQTtBQUFBLFlBQVksY0FBVyxjQUF2QixFQUFzQyxTQUFTLEtBQUtOLE9BQXBEO0FBQ0Usb0VBREY7QUFFRTtBQUNFLGtCQUFLLE1BRFA7QUFFRSxvQkFBTyxvQkFGVDtBQUdFLHNCQUFVLEtBQUsxQixRQUhqQjtBQUlFLGlCQUFLLGFBQUM0QixLQUFELEVBQVc7QUFDZCxxQkFBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0QsYUFOSDtBQU9FLG1CQUFPLEVBQUVLLFNBQVMsTUFBWDtBQVBUO0FBRkY7QUFQRixPQURGO0FBc0JEOzs7OztBQXBGR3JCLFUsQ0FDR1QsUyxHQUFZO0FBQ2pCNEIsU0FBTyxvQkFBVTNCLE1BQVYsQ0FBaUJDLFVBRFA7QUFFakIyQixhQUFXLG9CQUFVekIsTUFBVixDQUFpQkYsVUFGWDtBQUdqQm1CLHNCQUFvQixvQkFBVWhCLElBQVYsQ0FBZUg7QUFIbEIsQztrQkFzRk5PLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsR2Y7Ozs7QUFDQTs7OztBQUNBOztBQUVBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQU1BOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQU5BO0FBUUEsSUFBTXhCLFNBQVMsU0FBVEEsTUFBUyxDQUFDOEMsS0FBRDtBQUFBOztBQUFBLFNBQVk7QUFDekJDLFVBQU07QUFDSkMsZUFBUztBQURMLEtBRG1CO0FBSXpCQyxVQUFNO0FBQ0pDLG9CQUFjO0FBRFYsS0FKbUI7QUFPekJ0RSxXQUFPO0FBQ0x1RSxjQUFRLFNBREg7QUFFTEMsc0JBQWdCO0FBRlgsS0FQa0I7QUFXekJDLGVBQVc7QUFDVEYsY0FBUSxTQURDO0FBRVRDLHNCQUFnQjtBQUZQLEtBWGM7QUFlekJFLFlBQVE7QUFDTkosb0JBQWM7QUFEUixLQWZpQjtBQWtCekJLLFdBQU87QUFDTG5ELGNBQVEsR0FESDtBQUVMOEMsb0JBQWM7QUFGVCxLQWxCa0I7QUFzQnpCTSxjQUFVO0FBQ1JDLFlBQU07QUFERSxLQXRCZTtBQXlCekJDLFlBQVE7QUFDTnJELGNBQVEsaUNBREY7QUFFTnNELG9CQUFjO0FBRlIsS0F6QmlCO0FBNkJ6QkM7QUFDRWxCLGdCQUFVO0FBRFosa0RBRUdJLE1BQU1lLFdBQU4sQ0FBa0JDLEVBQWxCLENBQXFCLElBQXJCLENBRkgsRUFFZ0M7QUFDNUJDLGNBQVEsTUFEb0I7QUFFNUJDLGFBQU87QUFGcUIsS0FGaEMsOENBTUdsQixNQUFNZSxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQU5ILEVBTWtDO0FBQzlCRixjQUFRLE1BRHNCO0FBRTlCQyxhQUFPO0FBRnVCLEtBTmxDLDhDQVVHbEIsTUFBTWUsV0FBTixDQUFrQkksSUFBbEIsQ0FBdUIsSUFBdkIsQ0FWSCxFQVVrQztBQUM5QkYsY0FBUSxNQURzQjtBQUU5QkMsYUFBTztBQUZ1QixLQVZsQyw4Q0FjR2xCLE1BQU1lLFdBQU4sQ0FBa0JJLElBQWxCLENBQXVCLElBQXZCLENBZEgsRUFja0M7QUFDOUJwQixlQUFTLE1BRHFCO0FBRTlCa0IsY0FBUSxNQUZzQjtBQUc5QkMsYUFBTztBQUh1QixLQWRsQztBQTdCeUIsR0FBWjtBQUFBLENBQWYsQyxDQXhDQTs7SUEyRk1FLFk7OztBQUNKLHdCQUFZekMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGtKQUNYQSxLQURXOztBQUFBLFVBc0VuQjBDLGFBdEVtQixHQXNFSCxVQUFDekMsQ0FBRDtBQUFBLGFBQU8sTUFBSzBDLFFBQUwsQ0FBYyxFQUFFeEYsT0FBTzhDLEVBQUVHLE1BQUYsQ0FBUzVCLEtBQWxCLEVBQWQsQ0FBUDtBQUFBLEtBdEVHOztBQUFBLFVBdUVuQm9FLG1CQXZFbUIsR0F1RUcsVUFBQzNDLENBQUQ7QUFBQSxhQUFPLE1BQUswQyxRQUFMLENBQWMsRUFBRXZGLGFBQWE2QyxFQUFFRyxNQUFGLENBQVM1QixLQUF4QixFQUFkLENBQVA7QUFBQSxLQXZFSDs7QUFBQSxVQXlFbkJxRSxZQXpFbUIsR0F5RUo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFRyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0F6RUk7O0FBQUEsVUEwRW5CQyxZQTFFbUIsR0EwRUo7QUFBQSxhQUFNLE1BQUtKLFFBQUwsQ0FBYyxFQUFFRyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0ExRUk7O0FBQUEsUUFFTC9GLE1BRkssR0FFTSxNQUFLaUQsS0FBTCxDQUFXZ0QsUUFBWCxDQUFvQkMsSUFGMUIsQ0FFVEMsRUFGUztBQUFBLDZCQUd3QixNQUFLbEQsS0FBTCxDQUFXbUQsTUFIbkM7QUFBQSxRQUdUMUMsUUFIUyxzQkFHVEEsUUFIUztBQUFBLFFBR0N0RCxLQUhELHNCQUdDQSxLQUhEO0FBQUEsUUFHUUMsV0FIUixzQkFHUUEsV0FIUjs7O0FBS2pCLFVBQUt3RCxLQUFMLEdBQWE7QUFDWHdDLFlBQU0sS0FESztBQUVYO0FBQ0FDLGdCQUFVdEcsV0FBVzBELFFBSFY7QUFJWHRELGtCQUpXO0FBS1hDO0FBTFcsS0FBYjs7QUFRQSxVQUFLa0csTUFBTCxHQUFjLE1BQUtBLE1BQUwsQ0FBWXhDLElBQVosT0FBZDtBQUNBLFVBQUt5QyxNQUFMLEdBQWMsTUFBS0EsTUFBTCxDQUFZekMsSUFBWixPQUFkO0FBZGlCO0FBZWxCOzs7O3dDQUVtQjtBQUFBLG1CQUNXLEtBQUtkLEtBRGhCO0FBQUEsVUFDVmdELFFBRFUsVUFDVkEsUUFEVTtBQUFBLFVBQ0FHLE1BREEsVUFDQUEsTUFEQTtBQUFBLDJCQUVZSCxTQUFTQyxJQUZyQjtBQUFBLFVBRVZoRyxLQUZVLGtCQUVWQSxLQUZVO0FBQUEsVUFFQ0YsTUFGRCxrQkFFSG1HLEVBRkc7QUFBQSxVQUdWekMsUUFIVSxHQUdHMEMsTUFISCxDQUdWMUMsUUFIVTs7O0FBS2xCLFVBQUkrQyxlQUFKO0FBQ0EsVUFBSS9DLGFBQWExRCxNQUFqQixFQUF5QjtBQUN2QnlHLGlCQUFTUixTQUFTQyxJQUFsQjtBQUNELE9BRkQsTUFFTztBQUNMTyxpQkFBU1IsU0FBU3ZDLFFBQVQsQ0FBVDtBQUNEOztBQUVELFVBQUksQ0FBQytDLE1BQUQsSUFBWSxDQUFDQSxPQUFPQyxVQUFSLElBQXNCLENBQUNELE9BQU9FLFNBQTlDLEVBQTBEO0FBQ3hELGFBQUsxRCxLQUFMLENBQVcyRCxvQkFBWCxDQUFnQyxFQUFFMUcsWUFBRixFQUFTaUcsSUFBSXpDLFFBQWIsRUFBaEM7QUFDRDtBQUNGOzs7NkJBRVE7QUFBQSxVQUNDMkMsSUFERCxHQUNVLEtBQUt4QyxLQURmLENBQ0N3QyxJQUREOztBQUVQLFdBQUtULFFBQUwsQ0FBYyxFQUFFUyxNQUFNLENBQUNBLElBQVQsRUFBZDtBQUNEOzs7Ozs7Ozs7OztBQUdTSCxvQixHQUFTLEtBQUtqRCxLQUFMLENBQVdnRCxRLENBQXBCQyxJO0FBQ0lsRyxzQixHQUFrQmtHLEksQ0FBdEJDLEUsRUFBWWpHLEssR0FBVWdHLEksQ0FBVmhHLEs7Z0NBRVcsS0FBSytDLEtBQUwsQ0FBV21ELE0sRUFBbENuRyxRLGlCQUFBQSxRLEVBQVV5RCxRLGlCQUFBQSxRO3lCQUNtQixLQUFLRyxLLEVBQWxDd0MsSSxVQUFBQSxJLEVBQU1qRyxLLFVBQUFBLEssRUFBT0MsVyxVQUFBQSxXOzt1QkFFZSxzQkFBZ0I7QUFDbERILDhCQURrRDtBQUVsREYsZ0NBRmtEO0FBR2xEQyxvQ0FIa0Q7QUFJbERHLDhCQUprRDtBQUtsREM7QUFMa0QsaUJBQWhCLEM7Ozs7QUFBNUJZLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSzs7c0JBUWhCRCxlQUFlLEc7Ozs7O0FBQ2pCO0FBQ0FHLHdCQUFRRixLQUFSLENBQWNBLEtBQWQ7Ozs7O0FBSUYscUJBQUsrQixLQUFMLENBQVdXLGtCQUFYLENBQThCO0FBQzVCM0Qsb0NBRDRCO0FBRTVCeUQsb0NBRjRCO0FBRzVCdEQsOEJBSDRCO0FBSTVCQztBQUo0QixpQkFBOUI7O0FBT0EscUJBQUt1RixRQUFMLENBQWMsRUFBRVMsTUFBTSxDQUFDQSxJQUFULEVBQWQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFTTztBQUFBOztBQUFBLG9CQUMrQixLQUFLcEQsS0FEcEM7QUFBQSxVQUNDaEIsT0FERCxXQUNDQSxPQUREO0FBQUEsVUFDVW1FLE1BRFYsV0FDVUEsTUFEVjtBQUFBLFVBQ2tCSCxRQURsQixXQUNrQkEsUUFEbEI7QUFBQSxVQUVDQyxJQUZELEdBRVVELFFBRlYsQ0FFQ0MsSUFGRDtBQUFBLFVBR0NXLFVBSEQsR0FHbUNYLElBSG5DLENBR0NXLFVBSEQ7QUFBQSxVQUdpQjdHLE1BSGpCLEdBR21Da0csSUFIbkMsQ0FHYUMsRUFIYjtBQUFBLFVBR3lCakcsS0FIekIsR0FHbUNnRyxJQUhuQyxDQUd5QmhHLEtBSHpCO0FBQUEsVUFJQ3dELFFBSkQsR0FJcUIwQyxNQUpyQixDQUlDMUMsUUFKRDtBQUFBLFVBSVd2RCxLQUpYLEdBSXFCaUcsTUFKckIsQ0FJV2pHLEtBSlg7O0FBS1AsVUFBSXNHLFNBQVMsRUFBRS9DLGtCQUFGLEVBQWI7O0FBRUEsVUFBSTFELFdBQVcwRCxRQUFmLEVBQXlCO0FBQ3ZCK0MsNENBQWNBLE1BQWQsRUFBeUJQLElBQXpCO0FBQ0QsT0FGRCxNQUVPO0FBQ0xPLDRDQUNLQSxNQURMLEVBRUtSLFNBQVN2QyxRQUFULENBRkw7QUFJRDs7QUFkTSxvQkFnQnFDLEtBQUtHLEtBaEIxQztBQUFBLFVBZ0JDa0MsS0FoQkQsV0FnQkNBLEtBaEJEO0FBQUEsVUFnQlFNLElBaEJSLFdBZ0JRQSxJQWhCUjtBQUFBLFVBZ0JjakcsS0FoQmQsV0FnQmNBLEtBaEJkO0FBQUEsVUFnQnFCQyxXQWhCckIsV0FnQnFCQSxXQWhCckI7OztBQWtCUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDLEVBQTZDLFdBQVc0QixRQUFRc0MsSUFBaEU7QUFDRTtBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLElBQUksRUFBdkIsRUFBMkIsSUFBSSxFQUEvQixFQUFtQyxJQUFJLENBQXZDLEVBQTBDLElBQUksQ0FBOUM7QUFDRTtBQUFBO0FBQUE7QUFDRSxzQkFBUXdCLEtBRFY7QUFFRSx5QkFBVzlELFFBQVF3QyxJQUZyQjtBQUdFLDRCQUFjLEtBQUtxQixZQUhyQjtBQUlFLDRCQUFjLEtBQUtFO0FBSnJCO0FBT0doRyx1QkFBVzBELFFBQVgsR0FDQztBQUNFLHlCQUFXekIsUUFBUThDLEtBRHJCO0FBRUUseUJBQVcsbUJBQUMrQixRQUFEO0FBQUEsdUJBQ1QsK0VBQ01BLFFBRE4sRUFFTVYsTUFGTjtBQUdFLHlCQUFPbEcsS0FIVDtBQUlFLHNDQUFvQixPQUFLK0MsS0FBTCxDQUFXVztBQUpqQyxtQkFEUztBQUFBLGVBRmI7QUFVRSxxQkFDRXpELFNBQ0E7QUFaSixjQURELEdBaUJDO0FBQ0UseUJBQVc4QixRQUFROEMsS0FEckI7QUFFRSxxQkFDRTVFLFNBQ0E7QUFKSixjQXhCSjtBQWdDRTtBQUNFLHNCQUNFO0FBQUE7QUFBQSxrQkFBTyxXQUFXOEIsUUFBUTZDLE1BQTFCLEVBQWtDLFdBQVcsQ0FBN0M7QUFDRTtBQUFBO0FBQUEsb0JBQU0sV0FBUzJCLE9BQU9NLFFBQXRCO0FBQ0U7QUFDRSx5QkFBS04sT0FBT08sSUFEZDtBQUVFLHlCQUNFUCxPQUFPM0IsTUFBUCxJQUNBO0FBSko7QUFERjtBQURGLGVBRko7QUFjRSxzQkFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsb0JBQVMsT0FBTSxhQUFmLEVBQTZCLFdBQVUsUUFBdkM7QUFDRTtBQUFBO0FBQUEsc0JBQVksY0FBWjtBQUNFO0FBREY7QUFERjtBQURGLGVBZko7QUF1QkUscUJBQ0U7QUFBQTtBQUFBLGtCQUFNLFdBQVMyQixPQUFPTSxRQUF0QixFQUFrQyxXQUFXOUUsUUFBUTdCLEtBQXJEO0FBQ0dxRyx1QkFBT087QUFEVixlQXhCSjtBQTRCRSx5QkFDRTtBQUFBO0FBQUEsa0JBQU0sV0FBU1AsT0FBT00sUUFBdEIsRUFBa0MsV0FBVzlFLFFBQVE0QyxTQUFyRDtBQUFBLHNCQUNPNEIsT0FBT007QUFEZDtBQTdCSixjQWhDRjtBQWtFRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsa0JBQVksTUFBSyxPQUFqQixFQUF5QixXQUFVLEtBQW5DO0FBQ0U7QUFDRSw0QkFBVSxDQUFDVixJQURiO0FBRUUseUJBQU9qRyxLQUZUO0FBR0UsNEJBQVUsS0FBS3VGO0FBSGpCO0FBREYsZUFERjtBQVFFO0FBQUE7QUFBQSxrQkFBWSxNQUFLLE9BQWpCLEVBQXlCLFdBQVUsS0FBbkMsRUFBeUMsa0JBQXpDO0FBQ0csdUJBQU90RixXQUFQLEtBQXVCLFFBQXZCLElBQW1DZ0csSUFBbkMsR0FDQztBQUNFLDRCQUFVLENBQUNBLElBRGI7QUFFRSx5QkFBT2hHLFdBRlQ7QUFHRSw0QkFBVSxLQUFLd0Y7QUFIakIsa0JBREQsR0FNRztBQVBOO0FBUkYsYUFsRUY7QUFvRkUsbURBQUssV0FBVzVELFFBQVFpRCxNQUF4QixHQXBGRjtBQXFGRTtBQUFBO0FBQUEsZ0JBQWEsMEJBQWI7QUFDRSx3REFERjtBQUVFO0FBQUE7QUFBQSxrQkFBUyxPQUFNLGtCQUFmLEVBQWtDLFdBQVUsUUFBNUM7QUFDRTtBQUFBO0FBQUEsb0JBQVksY0FBVyxrQkFBdkIsRUFBMEMsY0FBMUM7QUFDRTtBQURGO0FBREYsZUFGRjtBQU9FO0FBQUE7QUFBQSxrQkFBUyxPQUFNLE9BQWYsRUFBdUIsV0FBVSxRQUFqQztBQUNFO0FBQUE7QUFBQSxvQkFBWSxjQUFXLE9BQXZCLEVBQStCLGNBQS9CO0FBQ0U7QUFERjtBQURGO0FBUEY7QUFyRkY7QUFERixTQURGO0FBc0dHMkIsc0JBQ0NuRCxhQUFhMUQsTUFEZCxJQUVHO0FBQUE7QUFBQTtBQUNFLHFCQURGO0FBRUUsbUJBQU0sUUFGUjtBQUdFLDBCQUFXLE1BSGI7QUFJRSx1QkFBV2lDLFFBQVFtRCxVQUpyQjtBQUtFLHFCQUFTaUIsT0FBTyxLQUFLRyxNQUFaLEdBQXFCLEtBQUtEO0FBTHJDO0FBT0dGLGlCQUFPLG1EQUFQLEdBQXNCO0FBUHpCO0FBeEdOLE9BREY7QUFxSEQ7Ozs7O0FBR0hYLGFBQWFuRCxTQUFiLEdBQXlCO0FBQ3ZCO0FBQ0FOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRkg7O0FBSXZCO0FBQ0F3RCxZQUFVLG9CQUFVekQsTUFBVixDQUFpQkMsVUFMSjs7QUFPdkI7QUFDQTJELFVBQVEsb0JBQVU1RCxNQUFWLENBQWlCQyxVQVJGOztBQVV2QjtBQUNBbUUsd0JBQXNCLG9CQUFVaEUsSUFBVixDQUFlSCxVQVhkO0FBWXZCbUIsc0JBQW9CLG9CQUFVaEIsSUFBVixDQUFlSDtBQVpaLENBQXpCOztBQWVBLElBQU13RSxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR2hCLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7O0FBRUEsSUFBTWlCLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFUCwyQ0FERjtBQUVFaEQ7QUFGRixHQURGLEVBS0V1RCxRQUxGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVNlLHlCQUFRRixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYiwwQkFBVzFGLE1BQVgsRUFBbUJrRSxZQUFuQixDQURhLEMiLCJmaWxlIjoiNDkuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAxNy4yNVYyMWgzLjc1TDE3LjgxIDkuOTRsLTMuNzUtMy43NUwzIDE3LjI1ek0yMC43MSA3LjA0Yy4zOS0uMzkuMzktMS4wMiAwLTEuNDFsLTIuMzQtMi4zNGMtLjM5LS4zOS0xLjAyLS4zOS0xLjQxIDBsLTEuODMgMS44MyAzLjc1IDMuNzUgMS44My0xLjgzeicgfSk7XG5cbnZhciBFZGl0ID0gZnVuY3Rpb24gRWRpdChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRWRpdCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRWRpdCk7XG5FZGl0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEVkaXQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI2IDMxIDM0IDM4IDQ0IDQ1IDQ3IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTIgMjEuMzVsLTEuNDUtMS4zMkM1LjQgMTUuMzYgMiAxMi4yOCAyIDguNSAyIDUuNDIgNC40MiAzIDcuNSAzYzEuNzQgMCAzLjQxLjgxIDQuNSAyLjA5QzEzLjA5IDMuODEgMTQuNzYgMyAxNi41IDMgMTkuNTggMyAyMiA1LjQyIDIyIDguNWMwIDMuNzgtMy40IDYuODYtOC41NSAxMS41NEwxMiAyMS4zNXonIH0pO1xuXG52YXIgRmF2b3JpdGUgPSBmdW5jdGlvbiBGYXZvcml0ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRmF2b3JpdGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZhdm9yaXRlKTtcbkZhdm9yaXRlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZhdm9yaXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zhdm9yaXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQwIDQxIDQyIDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjEgMTlWNWMwLTEuMS0uOS0yLTItMkg1Yy0xLjEgMC0yIC45LTIgMnYxNGMwIDEuMS45IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yek04LjUgMTMuNWwyLjUgMy4wMUwxNC41IDEybDQuNSA2SDVsMy41LTQuNXonIH0pO1xuXG52YXIgSW5zZXJ0UGhvdG8gPSBmdW5jdGlvbiBJbnNlcnRQaG90byhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0UGhvdG8gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydFBob3RvKTtcbkluc2VydFBob3RvLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydFBob3RvO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMjQgMzEgMzQgMzggMzkgNDQgNDUgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiA4YzEuMSAwIDItLjkgMi0ycy0uOS0yLTItMi0yIC45LTIgMiAuOSAyIDIgMnptMCAyYy0xLjEgMC0yIC45LTIgMnMuOSAyIDIgMiAyLS45IDItMi0uOS0yLTItMnptMCA2Yy0xLjEgMC0yIC45LTIgMnMuOSAyIDIgMiAyLS45IDItMi0uOS0yLTItMnonIH0pO1xuXG52YXIgTW9yZVZlcnQgPSBmdW5jdGlvbiBNb3JlVmVydChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuTW9yZVZlcnQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKE1vcmVWZXJ0KTtcbk1vcmVWZXJ0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IE1vcmVWZXJ0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMjcgMjggMzEgMzQgMzUgMzcgNDAgNDEgNDIgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNyAzSDVjLTEuMTEgMC0yIC45LTIgMnYxNGMwIDEuMS44OSAyIDIgMmgxNGMxLjEgMCAyLS45IDItMlY3bC00LTR6bS01IDE2Yy0xLjY2IDAtMy0xLjM0LTMtM3MxLjM0LTMgMy0zIDMgMS4zNCAzIDMtMS4zNCAzLTMgM3ptMy0xMEg1VjVoMTB2NHonIH0pO1xuXG52YXIgU2F2ZSA9IGZ1bmN0aW9uIFNhdmUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblNhdmUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFNhdmUpO1xuU2F2ZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTYXZlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NhdmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NhdmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNiAzMSAzNCAzOCA0NCA0NSA0NiA0NyA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE4IDE2LjA4Yy0uNzYgMC0xLjQ0LjMtMS45Ni43N0w4LjkxIDEyLjdjLjA1LS4yMy4wOS0uNDYuMDktLjdzLS4wNC0uNDctLjA5LS43bDcuMDUtNC4xMWMuNTQuNSAxLjI1LjgxIDIuMDQuODEgMS42NiAwIDMtMS4zNCAzLTNzLTEuMzQtMy0zLTMtMyAxLjM0LTMgM2MwIC4yNC4wNC40Ny4wOS43TDguMDQgOS44MUM3LjUgOS4zMSA2Ljc5IDkgNiA5Yy0xLjY2IDAtMyAxLjM0LTMgM3MxLjM0IDMgMyAzYy43OSAwIDEuNS0uMzEgMi4wNC0uODFsNy4xMiA0LjE2Yy0uMDUuMjEtLjA4LjQzLS4wOC42NSAwIDEuNjEgMS4zMSAyLjkyIDIuOTIgMi45MiAxLjYxIDAgMi45Mi0xLjMxIDIuOTItMi45MnMtMS4zMS0yLjkyLTIuOTItMi45MnonIH0pO1xuXG52YXIgU2hhcmUgPSBmdW5jdGlvbiBTaGFyZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuU2hhcmUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFNoYXJlKTtcblNoYXJlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFNoYXJlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NoYXJlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TaGFyZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQwIDQxIDQyIDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9jb2xvck1hbmlwdWxhdG9yID0gcmVxdWlyZSgnLi4vc3R5bGVzL2NvbG9yTWFuaXB1bGF0b3InKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgZmxleFNocmluazogMCxcbiAgICAgIHdpZHRoOiA0MCxcbiAgICAgIGhlaWdodDogNDAsXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDIwKSxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZSdcbiAgICB9LFxuICAgIGNvbG9yRGVmYXVsdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LFxuICAgICAgYmFja2dyb3VuZENvbG9yOiAoMCwgX2NvbG9yTWFuaXB1bGF0b3IuZW1waGFzaXplKSh0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCwgMC4yNilcbiAgICB9LFxuICAgIGltZzoge1xuICAgICAgbWF4V2lkdGg6ICcxMDAlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VkIGluIGNvbWJpbmF0aW9uIHdpdGggYHNyY2Agb3IgYHNyY1NldGAgdG9cbiAgICogcHJvdmlkZSBhbiBhbHQgYXR0cmlidXRlIGZvciB0aGUgcmVuZGVyZWQgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIGFsdDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZCB0byByZW5kZXIgaWNvbiBvciB0ZXh0IGVsZW1lbnRzIGluc2lkZSB0aGUgQXZhdGFyLlxuICAgKiBgc3JjYCBhbmQgYGFsdGAgcHJvcHMgd2lsbCBub3QgYmUgdXNlZCBhbmQgbm8gYGltZ2Agd2lsbFxuICAgKiBiZSByZW5kZXJlZCBieSBkZWZhdWx0LlxuICAgKlxuICAgKiBUaGlzIGNhbiBiZSBhbiBlbGVtZW50LCBvciBqdXN0IGEgc3RyaW5nLlxuICAgKi9cbiAgY2hpbGRyZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIFRoZSBjbGFzc05hbWUgb2YgdGhlIGNoaWxkIGVsZW1lbnQuXG4gICAqIFVzZWQgYnkgQ2hpcCBhbmQgTGlzdEl0ZW1JY29uIHRvIHN0eWxlIHRoZSBBdmF0YXIgaWNvbi5cbiAgICovXG4gIGNoaWxkcmVuQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBpbWdgIGVsZW1lbnQgd2hlbiB0aGUgY29tcG9uZW50XG4gICAqIGlzIHVzZWQgdG8gZGlzcGxheSBhbiBpbWFnZS5cbiAgICovXG4gIGltZ1Byb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgYHNpemVzYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc2l6ZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBgc3JjYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc3JjOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY1NldGAgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyY1NldDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxudmFyIEF2YXRhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEF2YXRhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQXZhdGFyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEF2YXRhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEF2YXRhci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQXZhdGFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShBdmF0YXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYWx0ID0gX3Byb3BzLmFsdCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jaGlsZHJlbkNsYXNzTmFtZSxcbiAgICAgICAgICBDb21wb25lbnRQcm9wID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBpbWdQcm9wcyA9IF9wcm9wcy5pbWdQcm9wcyxcbiAgICAgICAgICBzaXplcyA9IF9wcm9wcy5zaXplcyxcbiAgICAgICAgICBzcmMgPSBfcHJvcHMuc3JjLFxuICAgICAgICAgIHNyY1NldCA9IF9wcm9wcy5zcmNTZXQsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYWx0JywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NoaWxkcmVuJywgJ2NoaWxkcmVuQ2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdpbWdQcm9wcycsICdzaXplcycsICdzcmMnLCAnc3JjU2V0J10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuY29sb3JEZWZhdWx0LCBjaGlsZHJlblByb3AgJiYgIXNyYyAmJiAhc3JjU2V0KSwgY2xhc3NOYW1lUHJvcCk7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBudWxsO1xuXG4gICAgICBpZiAoY2hpbGRyZW5Qcm9wKSB7XG4gICAgICAgIGlmIChjaGlsZHJlbkNsYXNzTmFtZVByb3AgJiYgdHlwZW9mIGNoaWxkcmVuUHJvcCAhPT0gJ3N0cmluZycgJiYgX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkcmVuUHJvcCkpIHtcbiAgICAgICAgICB2YXIgX2NoaWxkcmVuQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjaGlsZHJlbkNsYXNzTmFtZVByb3AsIGNoaWxkcmVuUHJvcC5wcm9wcy5jbGFzc05hbWUpO1xuICAgICAgICAgIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZHJlblByb3AsIHsgY2xhc3NOYW1lOiBfY2hpbGRyZW5DbGFzc05hbWUgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlblByb3A7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoc3JjIHx8IHNyY1NldCkge1xuICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdpbWcnLCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhbHQ6IGFsdCxcbiAgICAgICAgICBzcmM6IHNyYyxcbiAgICAgICAgICBzcmNTZXQ6IHNyY1NldCxcbiAgICAgICAgICBzaXplczogc2l6ZXMsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc2VzLmltZ1xuICAgICAgICB9LCBpbWdQcm9wcykpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIENvbXBvbmVudFByb3AsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQXZhdGFyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQXZhdGFyLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29tcG9uZW50OiAnZGl2J1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlBdmF0YXInIH0pKEF2YXRhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNCA2IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU0IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0F2YXRhciA9IHJlcXVpcmUoJy4vQXZhdGFyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0F2YXRhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgMjQgMjYgMjcgMjggMzEgMzMgMzQgMzUgMzYgMzcgMzkgNDAgNDMgNDkgNTcgNTggNTkgNjEgNjMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuLi9JY29uJyk7XG5cbnZhciBfSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKTtcblxucmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX3JlYWN0SGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL3JlYWN0SGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBCdXR0b25CYXNlXG5cbi8vIEVuc3VyZSBDU1Mgc3BlY2lmaWNpdHlcblxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgICAgZmxleDogJzAgMCBhdXRvJyxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjQpLFxuICAgICAgd2lkdGg6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBoZWlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBwYWRkaW5nOiAwLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmUsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2JhY2tncm91bmQtY29sb3InLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdFxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIGNvbG9ySW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgZGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgbGFiZWw6IHtcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnaW5oZXJpdCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICB3aWR0aDogJzFlbScsXG4gICAgICBoZWlnaHQ6ICcxZW0nXG4gICAgfSxcbiAgICBrZXlib2FyZEZvY3VzZWQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSBuYXRpdmUgYnV0dG9uIGNvbXBvbmVudC5cbiAgICovXG4gIGJ1dHRvblJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIGVsZW1lbnQuXG4gICAqIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYW4gaWNvbiBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdpbmhlcml0JywgJ3ByaW1hcnknLCAnY29udHJhc3QnLCAnYWNjZW50J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJ1dHRvbiB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHJpcHBsZSB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufTtcblxuLyoqXG4gKiBSZWZlciB0byB0aGUgW0ljb25zXSgvc3R5bGUvaWNvbnMpIHNlY3Rpb24gb2YgdGhlIGRvY3VtZW50YXRpb25cbiAqIHJlZ2FyZGluZyB0aGUgYXZhaWxhYmxlIGljb24gb3B0aW9ucy5cbiAqL1xudmFyIEljb25CdXR0b24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJY29uQnV0dG9uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEljb25CdXR0b24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChJY29uQnV0dG9uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShJY29uQnV0dG9uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYnV0dG9uUmVmID0gX3Byb3BzLmJ1dHRvblJlZixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uUmVmJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2Rpc2FibGVkJywgJ3Jvb3RSZWYnXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2RlZmF1bHQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGlzYWJsZWQsIGRpc2FibGVkKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWUpLFxuICAgICAgICAgIGNlbnRlclJpcHBsZTogdHJ1ZSxcbiAgICAgICAgICBrZXlib2FyZEZvY3VzZWRDbGFzc05hbWU6IGNsYXNzZXMua2V5Ym9hcmRGb2N1c2VkLFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZFxuICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgIHJvb3RSZWY6IGJ1dHRvblJlZixcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5sYWJlbCB9LFxuICAgICAgICAgIHR5cGVvZiBjaGlsZHJlbiA9PT0gJ3N0cmluZycgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuaWNvbiB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApIDogX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgICBpZiAoKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KShjaGlsZCwgWydJY29uJywgJ1N2Z0ljb24nXSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5pY29uLCBjaGlsZC5wcm9wcy5jbGFzc05hbWUpXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgfSlcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb25CdXR0b247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdkZWZhdWx0JyxcbiAgZGlzYWJsZWQ6IGZhbHNlLFxuICBkaXNhYmxlUmlwcGxlOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uQnV0dG9uJyB9KShJY29uQnV0dG9uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb25CdXR0b24gPSByZXF1aXJlKCcuL0ljb25CdXR0b24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbkJ1dHRvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSByZXF1aXJlKCcuL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwnKTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSByZXF1aXJlKCcuL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQnKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRmFjdG9yeSA9IGZ1bmN0aW9uIGNyZWF0ZUZhY3RvcnkodHlwZSkge1xuICB2YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQgPSAoMCwgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyLmRlZmF1bHQpKHR5cGUpO1xuICByZXR1cm4gZnVuY3Rpb24gKHAsIGMpIHtcbiAgICByZXR1cm4gKDAsIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMi5kZWZhdWx0KShmYWxzZSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHAsIGMpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRmFjdG9yeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgZ2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBnZXREaXNwbGF5TmFtZShDb21wb25lbnQpIHtcbiAgaWYgKHR5cGVvZiBDb21wb25lbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfVxuXG4gIGlmICghQ29tcG9uZW50KSB7XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuXG4gIHJldHVybiBDb21wb25lbnQuZGlzcGxheU5hbWUgfHwgQ29tcG9uZW50Lm5hbWUgfHwgJ0NvbXBvbmVudCc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBnZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XG5cbnZhciBpc0NsYXNzQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNDbGFzc0NvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4oQ29tcG9uZW50ICYmIENvbXBvbmVudC5wcm90b3R5cGUgJiYgX3R5cGVvZihDb21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQpID09PSAnb2JqZWN0Jyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc0NsYXNzQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQgPSByZXF1aXJlKCcuL2lzQ2xhc3NDb21wb25lbnQnKTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzQ2xhc3NDb21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IGZ1bmN0aW9uIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKHR5cGVvZiBDb21wb25lbnQgPT09ICdmdW5jdGlvbicgJiYgISgwLCBfaXNDbGFzc0NvbXBvbmVudDIuZGVmYXVsdCkoQ29tcG9uZW50KSAmJiAhQ29tcG9uZW50LmRlZmF1bHRQcm9wcyAmJiAhQ29tcG9uZW50LmNvbnRleHRUeXBlcyAmJiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdwcm9kdWN0aW9uJyB8fCAhQ29tcG9uZW50LnByb3BUeXBlcykpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaG91bGRVcGRhdGUgPSByZXF1aXJlKCcuL3Nob3VsZFVwZGF0ZScpO1xuXG52YXIgX3Nob3VsZFVwZGF0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaG91bGRVcGRhdGUpO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJy4vc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwdXJlID0gZnVuY3Rpb24gcHVyZShCYXNlQ29tcG9uZW50KSB7XG4gIHZhciBob2MgPSAoMCwgX3Nob3VsZFVwZGF0ZTIuZGVmYXVsdCkoZnVuY3Rpb24gKHByb3BzLCBuZXh0UHJvcHMpIHtcbiAgICByZXR1cm4gISgwLCBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0KShwcm9wcywgbmV4dFByb3BzKTtcbiAgfSk7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdwdXJlJykpKGhvYyhCYXNlQ29tcG9uZW50KSk7XG4gIH1cblxuICByZXR1cm4gaG9jKEJhc2VDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gcHVyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zZXRTdGF0aWMgPSByZXF1aXJlKCcuL3NldFN0YXRpYycpO1xuXG52YXIgX3NldFN0YXRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRTdGF0aWMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBzZXREaXNwbGF5TmFtZShkaXNwbGF5TmFtZSkge1xuICByZXR1cm4gKDAsIF9zZXRTdGF0aWMyLmRlZmF1bHQpKCdkaXNwbGF5TmFtZScsIGRpc3BsYXlOYW1lKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBzZXRTdGF0aWMgPSBmdW5jdGlvbiBzZXRTdGF0aWMoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIEJhc2VDb21wb25lbnRba2V5XSA9IHZhbHVlO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICByZXR1cm4gQmFzZUNvbXBvbmVudDtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldFN0YXRpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnZmJqcy9saWIvc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9zaGFsbG93RXF1YWwyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkgPSByZXF1aXJlKCcuL2NyZWF0ZUVhZ2VyRmFjdG9yeScpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckZhY3RvcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBzaG91bGRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRVcGRhdGUodGVzdCkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICB2YXIgZmFjdG9yeSA9ICgwLCBfY3JlYXRlRWFnZXJGYWN0b3J5Mi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KTtcblxuICAgIHZhciBTaG91bGRVcGRhdGUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgICAgX2luaGVyaXRzKFNob3VsZFVwZGF0ZSwgX0NvbXBvbmVudCk7XG5cbiAgICAgIGZ1bmN0aW9uIFNob3VsZFVwZGF0ZSgpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNob3VsZFVwZGF0ZSk7XG5cbiAgICAgICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgICB9XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgICByZXR1cm4gdGVzdCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiBmYWN0b3J5KHRoaXMucHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgICB9KF9yZWFjdC5Db21wb25lbnQpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3Nob3VsZFVwZGF0ZScpKShTaG91bGRVcGRhdGUpO1xuICAgIH1cbiAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2hvdWxkVXBkYXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IGZ1bmN0aW9uIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwoaGFzS2V5LCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcHJvcHMsIGNoaWxkcmVuKSB7XG4gIGlmICghaGFzS2V5ICYmIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50KSB7XG4gICAgaWYgKGNoaWxkcmVuKSB7XG4gICAgICByZXR1cm4gdHlwZShfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgY2hpbGRyZW46IGNoaWxkcmVuIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHR5cGUocHJvcHMpO1xuICB9XG5cbiAgdmFyIENvbXBvbmVudCA9IHR5cGU7XG5cbiAgaWYgKGNoaWxkcmVuKSB7XG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgQ29tcG9uZW50LFxuICAgICAgcHJvcHMsXG4gICAgICBjaGlsZHJlblxuICAgICk7XG4gIH1cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBwcm9wcyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVFYWdlckVsZW1lbnRVdGlsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2dldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9nZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX2dldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHdyYXBEaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHdyYXBEaXNwbGF5TmFtZShCYXNlQ29tcG9uZW50LCBob2NOYW1lKSB7XG4gIHJldHVybiBob2NOYW1lICsgJygnICsgKDAsIF9nZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCkgKyAnKSc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3cmFwRGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IENPVVJTRV9VUERBVEUgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvY291cnNlcyc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgY292ZXI/OiBzdHJpbmcsXG4gIHRpdGxlPzogc3RyaW5nLFxuICBkZXNjcmlwdGlvbj86IHN0cmluZyxcbiAgc3lsbGFidXM/OiBtaXhlZCxcbiAgY291cnNlTW9kdWxlcz86IEFycmF5PHtcbiAgICBtb2R1bGVJZDogbnVtYmVyLFxuICB9Pixcbn07XG5cbnR5cGUgQWN0aW9uID0ge1xuICB0eXBlOiBzdHJpbmcsXG4gIHBheWxvYWQ6IFBheWxvYWQsXG59O1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5hdXRob3JJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5kZXNjcmlwdGlvbiAtXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5zeWxsYWJ1cyAtXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkLmNvdXJzZU1vZHVsZXMgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuY291cnNlTW9kdWxlc1tdIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZU1vZHVsZXNbXS5tb2R1bGVJZCAtXG4gKlxuICogQHJldHVybiB7T2JqZWN0fSAtXG4gKi9cbmNvbnN0IHVwZGF0ZSA9IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7IHR5cGU6IENPVVJTRV9VUERBVEUsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VwZGF0ZS5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vY29uZmlnJztcblxudHlwZSBQYXlsb2FkID0ge1xuICB0b2tlbjogc3RyaW5nLFxuICB1c2VySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgY292ZXI/OiBzdHJpbmcsXG4gIHRpdGxlPzogc3RyaW5nLFxuICBkZXNjcmlwdGlvbj86IHN0cmluZyxcbiAgc3lsbGFidXM/OiBhbnksXG4gIGNvdXJzZU1vZHVsZXM/OiBBcnJheTx7XG4gICAgbW9kdWxlSWQ6IG51bWJlcixcbiAgfT4sXG59O1xuXG4vKipcbiAqIFVwZGF0ZSBDb3Vyc2VcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZUlkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY292ZXIgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZGVzY3JpcHRpb24gLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuc3lsbGFidXMgLVxuICogQHBhcmFtIHtPYmplY3RbXX0gcGF5bG9hZC5jb3Vyc2VNb2R1bGVzIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZU1vZHVsZXNbXS5tb2R1bGVJZCAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gdXBkYXRlQ291cnNlKHtcbiAgdXNlcklkLFxuICBjb3Vyc2VJZCxcbiAgdG9rZW4sXG4gIGNvdmVyLFxuICB0aXRsZSxcbiAgZGVzY3JpcHRpb24sXG4gIHN5bGxhYnVzLFxuICBjb3Vyc2VNb2R1bGVzLFxufTogUGF5bG9hZCkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vY291cnNlcy8ke2NvdXJzZUlkfWA7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ1BVVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgIGNvdmVyLFxuICAgICAgICB0aXRsZSxcbiAgICAgICAgZGVzY3JpcHRpb24sXG4gICAgICAgIHN5bGxhYnVzLFxuICAgICAgICBjb3Vyc2VNb2R1bGVzLFxuICAgICAgfSksXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgdXBkYXRlQ291cnNlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvY291cnNlcy91c2Vycy91cGRhdGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuXG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vLi4vY29uZmlnJztcblxudHlwZSBQYXlsb2FkID0ge1xuICB0b2tlbjogc3RyaW5nLFxuICB1c2VySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgZm9ybURhdGE6IGFueSxcbn07XG5cbnR5cGUgU3VjY2VzcyA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBtZXNzYWdlOiBzdHJpbmcsXG4gIHBheWxvYWQ/OiBhbnksXG59O1xuXG50eXBlIEVycm9yUmVzcG9uc2UgPSB7XG4gIHN0YXR1c0NvZGU6IG51bWJlcixcbiAgZXJyb3I6IHN0cmluZyxcbn07XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW5cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucG9zdElkXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5mb3JtRGF0YVxuICogQHJldHVybnMge1Byb21pc2V9XG4gKlxuICogQGV4YW1wbGVcbiAqL1xuZXhwb3J0IGRlZmF1bHQgYXN5bmMgZnVuY3Rpb24gY3JlYXRlKHtcbiAgdG9rZW4sXG4gIHVzZXJJZCxcbiAgY291cnNlSWQsXG4gIGZvcm1EYXRhLFxufTogUGF5bG9hZCk6IFByb21pc2U8U3VjY2VzcyB8IEVycm9yUmVzcG9uc2U+IHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L2NvdXJzZXMvJHtjb3Vyc2VJZH0vcGhvdG9zYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAvLyAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBmb3JtRGF0YSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvcGhvdG9zL2NvdXJzZXMvdXNlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBEZXNjcmlwdGlvbiBjb21wb25lbnQgaXMgZm9yIGVkaXRhYmxlIGNvbXBvbmVudCB0byBlZGl0IGRlc2NyaXB0aW9uXG4gKiB3aXRoIHRleHRhcmVhLlxuICogVGhpcyBjb21wb25lbnQgc2hvdWxkIGJlIHVzZWQgd2l0aCBDYXJkSGVhZGVyIHN1YmhlYWRlciBwbGFjZS5cbiAqIFRPRE86IG1ha2UgdGV4dGFyZWEgYXV0b3Jlc2l6YWJsZS5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IHdpdGhTdHlsZXMgfSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHZhbHVlOiB7fSxcbiAgdGV4dGFyZWE6IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIGhlaWdodDogJ2F1dG8nLFxuICAgIGJvcmRlcjogJ25vbmUnLFxuICAgIC8vIG91dGxpbmU6ICdub25lJyxcbiAgICByZXNpemU6ICdub25lJyxcbiAgICBmb250OiAnaW5oZXJpdCcsXG4gIH0sXG59O1xuXG5jb25zdCBEZXNjcmlwdGlvbiA9ICh7XG4gIGNsYXNzZXMsXG4gIHJlYWRPbmx5LFxuICBwbGFjZWhvbGRlcixcbiAgdmFsdWUsXG4gIG9uQ2hhbmdlLFxuICBtaW5MZW5ndGgsXG4gIG1heExlbmd0aCxcbn0pID0+IHtcbiAgaWYgKHJlYWRPbmx5KSB7XG4gICAgcmV0dXJuIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnZhbHVlfT57dmFsdWV9PC9kaXY+O1xuICB9XG5cbiAgcmV0dXJuIChcbiAgICA8dGV4dGFyZWFcbiAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy50ZXh0YXJlYX1cbiAgICAgIG1pbkxlbmd0aD17bWluTGVuZ3RoIHx8IDF9XG4gICAgICBtYXhMZW5ndGg9e21heExlbmd0aCB8fCAzMDB9XG4gICAgICByb3dzPXsxfVxuICAgICAgcmVhZE9ubHk9e3JlYWRPbmx5fVxuICAgIC8+XG4gICk7XG59O1xuXG5EZXNjcmlwdGlvbi5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgcmVhZE9ubHk6IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIHZhbHVlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgbWluTGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICBtYXhMZW5ndGg6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5EZXNjcmlwdGlvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHBsYWNlaG9sZGVyOiAnRGVzY3JpcHRpb24gZ29lcyBoZXJlLi4uJyxcbiAgcmVhZE9ubHk6IHRydWUsXG4gIG1pbkxlbmd0aDogMSxcbiAgbWF4TGVuZ3RoOiAzMDAsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoRGVzY3JpcHRpb24pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb21wb25lbnRzL0Rlc2NyaXB0aW9uLmpzIiwiLyoqXG4gKiBUaXRsZSBjb21wb25lbnQgaXMgZm9yIGVkaXRhYmxlIGNvbXBvbmVudCB0byBlZGl0IHRpdGxlIHdpdGggdGV4dGFyZWEuXG4gKiBUaGlzIGNvbXBvbmVudCBzaG91bGQgYmUgdXNlZCB3aXRoIENhcmRIZWFkZXIgdGl0bGUgcGxhY2UuXG4gKiBUT0RPOiBtYWtlIHRleHRhcmVhIGF1dG9yZXNpemFibGUuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICB2YWx1ZToge30sXG4gIHRleHRhcmVhOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICBib3JkZXI6ICdub25lJyxcbiAgICAvLyBvdXRsaW5lOiAnbm9uZScsXG4gICAgcmVzaXplOiAnbm9uZScsXG4gICAgZm9udDogJ2luaGVyaXQnLFxuICB9LFxufTtcblxuY29uc3QgVGl0bGUgPSAoe1xuICBjbGFzc2VzLFxuICByZWFkT25seSxcbiAgdmFsdWUsXG4gIHBsYWNlaG9sZGVyLFxuICBvbkNoYW5nZSxcbiAgbWluTGVuZ3RoLFxuICBtYXhMZW5ndGgsXG59KSA9PiB7XG4gIGlmIChyZWFkT25seSkge1xuICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy52YWx1ZX0+e3ZhbHVlfTwvZGl2PjtcbiAgfVxuXG4gIHJldHVybiAoXG4gICAgPHRleHRhcmVhXG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMudGV4dGFyZWF9XG4gICAgICBtaW5MZW5ndGg9e21pbkxlbmd0aCB8fCAxfVxuICAgICAgbWF4TGVuZ3RoPXttYXhMZW5ndGggfHwgMTQ4fVxuICAgICAgcm93cz17MX1cbiAgICAgIHJlYWRPbmx5PXtyZWFkT25seX1cbiAgICAvPlxuICApO1xufTtcblxuVGl0bGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgbWluTGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICBtYXhMZW5ndGg6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5UaXRsZS5kZWZhdWx0UHJvcHMgPSB7XG4gIHZhbHVlOiAnVW50aXRsZWQnLFxuICBwbGFjZWhvbGRlcjogJ1RpdGxlIGdvZXMgaGVyZS4uLicsXG4gIHJlYWRPbmx5OiB0cnVlLFxuICBtaW5MZW5ndGg6IDEsXG4gIG1heExlbmd0aDogMTQ4LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFRpdGxlKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9UaXRsZS5qcyIsIi8qKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgSWNvbkJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9JY29uQnV0dG9uJztcbmltcG9ydCBJbnNlcnRQaG90b0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8nO1xuXG5pbXBvcnQgYXBpQ291cnNlVXBkYXRlIGZyb20gJy4uLy4uLy4uL2FwaS9jb3Vyc2VzL3VzZXJzL3VwZGF0ZSc7XG5pbXBvcnQgYXBpUGhvdG9VcGxvYWQgZnJvbSAnLi4vLi4vLi4vYXBpL3Bob3Rvcy9jb3Vyc2VzL3VzZXJzL2NyZWF0ZSc7XG5cbmNsYXNzIENvdmVySW1hZ2UgZXh0ZW5kcyBDb21wb25lbnQge1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIHN0eWxlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gICAgYWN0aW9uQ291cnNlVXBkYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcblxuICAgIHRoaXMub25DbGljayA9IHRoaXMub25DbGljay5iaW5kKHRoaXMpO1xuICAgIC8vIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gIH1cblxuICBvbkNsaWNrKCkge1xuICAgIHRoaXMucGhvdG8udmFsdWUgPSBudWxsO1xuICAgIHRoaXMucGhvdG8uY2xpY2soKTtcbiAgfVxuXG4gIG9uQ2hhbmdlID0gYXN5bmMgKGUpID0+IHtcbiAgICBpZiAoZSkge1xuICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH1cblxuICAgIGNvbnN0IGZpbGUgPSBlLnRhcmdldC5maWxlc1swXTtcblxuICAgIGlmIChmaWxlLnR5cGUuaW5kZXhPZignaW1hZ2UvJykgPT09IDApIHtcbiAgICAgIGNvbnN0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgICBmb3JtRGF0YS5hcHBlbmQoJ3Bob3RvJywgZmlsZSk7XG5cbiAgICAgIGNvbnN0IHsgYXV0aG9ySWQ6IHVzZXJJZCwgY291cnNlSWQsIHRva2VuIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCBhcGlQaG90b1VwbG9hZCh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIGNvdXJzZUlkLFxuICAgICAgICBmb3JtRGF0YSxcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgLy8gaGFuZGxlIEVycm9yXG4gICAgICAgIGNvbnNvbGUuZXJyb3Ioc3RhdHVzQ29kZSwgZXJyb3IpO1xuICAgICAgfVxuXG4gICAgICBjb25zdCB7IHBob3RvVXJsOiBjb3ZlciB9ID0gcGF5bG9hZDtcblxuICAgICAgYXBpQ291cnNlVXBkYXRlKHtcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIHVzZXJJZCxcbiAgICAgICAgY291cnNlSWQsXG4gICAgICAgIGNvdmVyLFxuICAgICAgfSk7XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uQ291cnNlVXBkYXRlKHtcbiAgICAgICAgY291cnNlSWQsXG4gICAgICAgIGNvdmVyLFxuICAgICAgfSk7XG4gICAgfVxuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBzdHlsZT17e1xuICAgICAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgICAgIC4uLnRoaXMucHJvcHMuc3R5bGUsXG4gICAgICAgIH19XG4gICAgICAgIGNsYXNzTmFtZT17dGhpcy5wcm9wcy5jbGFzc05hbWV9XG4gICAgICA+XG4gICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgUGhvdG9cIiBvbkNsaWNrPXt0aGlzLm9uQ2xpY2t9PlxuICAgICAgICAgIDxJbnNlcnRQaG90b0ljb24gLz5cbiAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcbiAgICAgICAgICAgIGFjY2VwdD1cImltYWdlL2pwZWd8cG5nfGdpZlwiXG4gICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgIHJlZj17KHBob3RvKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMucGhvdG8gPSBwaG90bztcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgICBzdHlsZT17eyBkaXNwbGF5OiAnbm9uZScgfX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENvdmVySW1hZ2U7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9Db3ZlckltYWdlL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBMaW5rIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuaW1wb3J0IENhcmQsIHtcbiAgQ2FyZEhlYWRlcixcbiAgQ2FyZE1lZGlhLFxuICBDYXJkQWN0aW9ucyxcbiAgQ2FyZENvbnRlbnQsXG59IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0J1dHRvbic7XG5pbXBvcnQgUGFwZXIgZnJvbSAnbWF0ZXJpYWwtdWkvUGFwZXInO1xuXG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdtYXRlcmlhbC11aS9UeXBvZ3JhcGh5JztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBFZGl0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9FZGl0JztcbmltcG9ydCBTYXZlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9TYXZlJztcbmltcG9ydCBGYXZvcml0ZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUnO1xuaW1wb3J0IE1vcmVWZXJ0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydCc7XG5pbXBvcnQgU2hhcmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NoYXJlJztcbmltcG9ydCBUb29sdGlwIGZyb20gJ21hdGVyaWFsLXVpL1Rvb2x0aXAnO1xuXG5pbXBvcnQgVGl0bGUgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9UaXRsZSc7XG5pbXBvcnQgRGVzY3JpcHRpb24gZnJvbSAnLi4vLi4vY29tcG9uZW50cy9EZXNjcmlwdGlvbic7XG4vLyBpbXBvcnQgUmF0aW5nU3RhciBmcm9tICcuLi8uLi9jb21wb25lbnRzL1JhdGluZ1N0YXInO1xuaW1wb3J0IENvdmVySW1hZ2UgZnJvbSAnLi9Db3ZlckltYWdlJztcblxuaW1wb3J0IGFwaUNvdXJzZVVwZGF0ZSBmcm9tICcuLi8uLi9hcGkvY291cnNlcy91c2Vycy91cGRhdGUnO1xuXG5pbXBvcnQgYWN0aW9uRWxlbWVudEdldEJ5SWQgZnJvbSAnLi4vLi4vYWN0aW9ucy9lbGVtZW50cy9nZXRCeUlkJztcbmltcG9ydCBhY3Rpb25Db3Vyc2VVcGRhdGUgZnJvbSAnLi4vLi4vYWN0aW9ucy9jb3Vyc2VzL3VwZGF0ZSc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICB0aXRsZToge1xuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIHN1YmhlYWRlcjoge1xuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gIH0sXG4gIG1lZGlhOiB7XG4gICAgaGVpZ2h0OiAxOTQsXG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4IDhweCAwcHggMHB4JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBtYXJnaW46IHtcbiAgICBib3JkZXI6ICcwLjVweCBzb2xpZCByZ2JhKDIwOSwyMDksMjA5LDEpJyxcbiAgICBtYXJnaW5Cb3R0b206ICctMTBweCcsXG4gIH0sXG4gIGVkaXRCdXR0b246IHtcbiAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMudXAoJ3hsJyldOiB7XG4gICAgICBib3R0b206ICc0MHB4JyxcbiAgICAgIHJpZ2h0OiAnNDBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneGwnKV06IHtcbiAgICAgIGJvdHRvbTogJzQwcHgnLFxuICAgICAgcmlnaHQ6ICc0MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge1xuICAgICAgYm90dG9tOiAnMzBweCcsXG4gICAgICByaWdodDogJzMwcHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyldOiB7XG4gICAgICBkaXNwbGF5OiAnbm9uZScsXG4gICAgICBib3R0b206ICcyNXB4JyxcbiAgICAgIHJpZ2h0OiAnMjVweCcsXG4gICAgfSxcbiAgfSxcbn0pO1xuXG5jbGFzcyBJbnRyb2R1Y3Rpb24gZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICBjb25zdCB7IGlkOiB1c2VySWQgfSA9IHRoaXMucHJvcHMuZWxlbWVudHMudXNlcjtcbiAgICBjb25zdCB7IGF1dGhvcklkLCB0aXRsZSwgZGVzY3JpcHRpb24gfSA9IHRoaXMucHJvcHMuY291cnNlO1xuXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGVkaXQ6IGZhbHNlLFxuICAgICAgLy8gdGhpcyB2YWx1ZSB3aWxsIGJlIHRydWUgb25seSBpZiB1c2VyIGlzIHRoZSBhdXRob3Igb2YgY291cnNlLlxuICAgICAgaXNBdXRob3I6IHVzZXJJZCA9PT0gYXV0aG9ySWQsXG4gICAgICB0aXRsZSxcbiAgICAgIGRlc2NyaXB0aW9uLFxuICAgIH07XG5cbiAgICB0aGlzLm9uRWRpdCA9IHRoaXMub25FZGl0LmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblNhdmUgPSB0aGlzLm9uU2F2ZS5iaW5kKHRoaXMpO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgeyBlbGVtZW50cywgY291cnNlIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkOiB1c2VySWQgfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBhdXRob3JJZCB9ID0gY291cnNlO1xuXG4gICAgbGV0IGF1dGhvcjtcbiAgICBpZiAoYXV0aG9ySWQgPT09IHVzZXJJZCkge1xuICAgICAgYXV0aG9yID0gZWxlbWVudHMudXNlcjtcbiAgICB9IGVsc2Uge1xuICAgICAgYXV0aG9yID0gZWxlbWVudHNbYXV0aG9ySWRdO1xuICAgIH1cblxuICAgIGlmICghYXV0aG9yIHx8ICghYXV0aG9yLmlzRmV0Y2hpbmcgJiYgIWF1dGhvci5pc0ZldGNoZWQpKSB7XG4gICAgICB0aGlzLnByb3BzLmFjdGlvbkVsZW1lbnRHZXRCeUlkKHsgdG9rZW4sIGlkOiBhdXRob3JJZCB9KTtcbiAgICB9XG4gIH1cblxuICBvbkVkaXQoKSB7XG4gICAgY29uc3QgeyBlZGl0IH0gPSB0aGlzLnN0YXRlO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBlZGl0OiAhZWRpdCB9KTtcbiAgfVxuXG4gIGFzeW5jIG9uU2F2ZSgpIHtcbiAgICBjb25zdCB7IHVzZXIgfSA9IHRoaXMucHJvcHMuZWxlbWVudHM7XG4gICAgY29uc3QgeyBpZDogdXNlcklkLCB0b2tlbiB9ID0gdXNlcjtcblxuICAgIGNvbnN0IHsgY291cnNlSWQsIGF1dGhvcklkIH0gPSB0aGlzLnByb3BzLmNvdXJzZTtcbiAgICBjb25zdCB7IGVkaXQsIHRpdGxlLCBkZXNjcmlwdGlvbiB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IgfSA9IGF3YWl0IGFwaUNvdXJzZVVwZGF0ZSh7XG4gICAgICB0b2tlbixcbiAgICAgIHVzZXJJZCxcbiAgICAgIGNvdXJzZUlkLFxuICAgICAgdGl0bGUsXG4gICAgICBkZXNjcmlwdGlvbixcbiAgICB9KTtcblxuICAgIGlmIChzdGF0dXNDb2RlICE9PSAyMDApIHtcbiAgICAgIC8vIGhhbmRsZSBlcnJvclxuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25Db3Vyc2VVcGRhdGUoe1xuICAgICAgY291cnNlSWQsXG4gICAgICBhdXRob3JJZCxcbiAgICAgIHRpdGxlLFxuICAgICAgZGVzY3JpcHRpb24sXG4gICAgfSk7XG5cbiAgICB0aGlzLnNldFN0YXRlKHsgZWRpdDogIWVkaXQgfSk7XG4gIH1cblxuICBvbkNoYW5nZVRpdGxlID0gKGUpID0+IHRoaXMuc2V0U3RhdGUoeyB0aXRsZTogZS50YXJnZXQudmFsdWUgfSk7XG4gIG9uQ2hhbmdlRGVzY3JpcHRpb24gPSAoZSkgPT4gdGhpcy5zZXRTdGF0ZSh7IGRlc2NyaXB0aW9uOiBlLnRhcmdldC52YWx1ZSB9KTtcblxuICBvbk1vdXNlRW50ZXIgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IHRydWUgfSk7XG4gIG9uTW91c2VMZWF2ZSA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogZmFsc2UgfSk7XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgY291cnNlLCBlbGVtZW50cyB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHVzZXIgfSA9IGVsZW1lbnRzO1xuICAgIGNvbnN0IHsgaXNTaWduZWRJbiwgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IHVzZXI7XG4gICAgY29uc3QgeyBhdXRob3JJZCwgY292ZXIgfSA9IGNvdXJzZTtcbiAgICBsZXQgYXV0aG9yID0geyBhdXRob3JJZCB9O1xuXG4gICAgaWYgKHVzZXJJZCA9PT0gYXV0aG9ySWQpIHtcbiAgICAgIGF1dGhvciA9IHsgLi4uYXV0aG9yLCAuLi51c2VyIH07XG4gICAgfSBlbHNlIHtcbiAgICAgIGF1dGhvciA9IHtcbiAgICAgICAgLi4uYXV0aG9yLFxuICAgICAgICAuLi5lbGVtZW50c1thdXRob3JJZF0sXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IHsgaG92ZXIsIGVkaXQsIHRpdGxlLCBkZXNjcmlwdGlvbiB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8R3JpZCBjb250YWluZXIganVzdGlmeT1cImNlbnRlclwiIHNwYWNpbmc9ezB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPEdyaWQgaXRlbSB4cz17MTJ9IHNtPXsxMn0gbWQ9ezEwfSBsZz17OH0geGw9ezh9PlxuICAgICAgICAgIDxDYXJkXG4gICAgICAgICAgICByYWlzZWQ9e2hvdmVyfVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9XG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICB7LyogZG9uJ3QgY2hhbmdlIHRoaXMgY29tcG9uZW50IGZvciBub3cgKi99XG4gICAgICAgICAgICB7dXNlcklkID09PSBhdXRob3JJZCA/IChcbiAgICAgICAgICAgICAgPENhcmRNZWRpYVxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5tZWRpYX1cbiAgICAgICAgICAgICAgICBjb21wb25lbnQ9eyh0aGlzUHJvcCkgPT4gKFxuICAgICAgICAgICAgICAgICAgPENvdmVySW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgey4uLnRoaXNQcm9wfVxuICAgICAgICAgICAgICAgICAgICB7Li4uY291cnNlfVxuICAgICAgICAgICAgICAgICAgICB0b2tlbj17dG9rZW59XG4gICAgICAgICAgICAgICAgICAgIGFjdGlvbkNvdXJzZVVwZGF0ZT17dGhpcy5wcm9wcy5hY3Rpb25Db3Vyc2VVcGRhdGV9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICl9XG4gICAgICAgICAgICAgICAgaW1hZ2U9e1xuICAgICAgICAgICAgICAgICAgY292ZXIgfHxcbiAgICAgICAgICAgICAgICAgICdodHRwOi8vZ2ludmEuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzA4L2dpbnZhXzIwMTYtMDgtMDJfMDItMzAtNTQuanBnJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICkgOiAoXG4gICAgICAgICAgICAgIDxDYXJkTWVkaWFcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMubWVkaWF9XG4gICAgICAgICAgICAgICAgaW1hZ2U9e1xuICAgICAgICAgICAgICAgICAgY292ZXIgfHxcbiAgICAgICAgICAgICAgICAgICdodHRwOi8vZ2ludmEuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzA4L2dpbnZhXzIwMTYtMDgtMDJfMDItMzAtNTQuanBnJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgICBhdmF0YXI9e1xuICAgICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7YXV0aG9yLnVzZXJuYW1lfWB9PlxuICAgICAgICAgICAgICAgICAgICA8QXZhdGFyXG4gICAgICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICAgICAgYXV0aG9yLmF2YXRhciB8fFxuICAgICAgICAgICAgICAgICAgICAgICAgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZydcbiAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgICAgPC9QYXBlcj5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBhY3Rpb249e1xuICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvbWluZyBzb29uXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgICAgICAgIDxNb3JlVmVydEljb24gLz5cbiAgICAgICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIHRpdGxlPXtcbiAgICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlfT5cbiAgICAgICAgICAgICAgICAgIHthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgc3ViaGVhZGVyPXtcbiAgICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnN1YmhlYWRlcn0+XG4gICAgICAgICAgICAgICAgICB7YEAke2F1dGhvci51c2VybmFtZX1gfVxuICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cInRpdGxlXCIgY29tcG9uZW50PVwiZGl2XCI+XG4gICAgICAgICAgICAgICAgPFRpdGxlXG4gICAgICAgICAgICAgICAgICByZWFkT25seT17IWVkaXR9XG4gICAgICAgICAgICAgICAgICB2YWx1ZT17dGl0bGV9XG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZVRpdGxlfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkxXCIgY29tcG9uZW50PVwiZGl2XCIgZ3V0dGVyQm90dG9tPlxuICAgICAgICAgICAgICAgIHt0eXBlb2YgZGVzY3JpcHRpb24gPT09ICdzdHJpbmcnIHx8IGVkaXQgPyAoXG4gICAgICAgICAgICAgICAgICA8RGVzY3JpcHRpb25cbiAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk9eyFlZGl0fVxuICAgICAgICAgICAgICAgICAgICB2YWx1ZT17ZGVzY3JpcHRpb259XG4gICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlRGVzY3JpcHRpb259XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMubWFyZ2lufSAvPlxuICAgICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgICA8ZGl2IC8+XG4gICAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWRkIHRvIGZhdm9yaXRlc1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBZGQgdG8gZmF2b3JpdGVzXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgICA8RmF2b3JpdGVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiU2hhcmVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiU2hhcmVcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICAgIDxTaGFyZUljb24gLz5cbiAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICAgPC9DYXJkPlxuICAgICAgICA8L0dyaWQ+XG4gICAgICAgIHtpc1NpZ25lZEluICYmXG4gICAgICAgICAgYXV0aG9ySWQgPT09IHVzZXJJZCAmJiAoXG4gICAgICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgICAgIGZhYlxuICAgICAgICAgICAgICBjb2xvcj1cImFjY2VudFwiXG4gICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJFZGl0XCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmVkaXRCdXR0b259XG4gICAgICAgICAgICAgIG9uQ2xpY2s9e2VkaXQgPyB0aGlzLm9uU2F2ZSA6IHRoaXMub25FZGl0fVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICB7ZWRpdCA/IDxTYXZlSWNvbiAvPiA6IDxFZGl0SWNvbiAvPn1cbiAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICl9XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5JbnRyb2R1Y3Rpb24ucHJvcFR5cGVzID0ge1xuICAvKiogY2xhc3NlcyBvYmplY3QgY29tZXMgZnJvbSB3aXRoU3R5bGUgY29tcG9uZW50IHdyYXBwZXIuICovXG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAvKiogZWxlbWVudHMgY29tZXMgZnJvbSBSZWR1eCBzdG9yZSAqL1xuICBlbGVtZW50czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIC8qKiBjb3Vyc2Ugb2JqZWN0IGNvbWVzIGZyb20gcGFyZW50IGNvbXBvbmVudC4gKi9cbiAgY291cnNlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLyoqIHRoZXNlIGFjdGlvbnMgY29tZXMgZnJvbSBtYXBEaXNwYXRjaFRvUHJvcHMoKSAgKi9cbiAgYWN0aW9uRWxlbWVudEdldEJ5SWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGFjdGlvbkNvdXJzZVVwZGF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IGVsZW1lbnRzIH0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25FbGVtZW50R2V0QnlJZCxcbiAgICAgIGFjdGlvbkNvdXJzZVVwZGF0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKEludHJvZHVjdGlvbiksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvSW50cm9kdWN0aW9uLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==