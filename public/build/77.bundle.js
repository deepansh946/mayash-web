webpackJsonp([77],{

/***/ "./src/client/containers/CoursePage/Modules.js":
/*!*****************************************************!*\
  !*** ./src/client/containers/CoursePage/Modules.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {
    // padding: '1%',
  },
  gridItem: {
    paddingTop: '1%',
    paddingLeft: '1%',
    paddingRight: '1%'
  }
};

var AsyncModuleCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(59).then(__webpack_require__.bind(null, /*! ./ModuleCreate.js */ "./src/client/containers/CoursePage/ModuleCreate.js"));
  },
  modules: ['./ModuleCreate'],
  loading: _Loading2.default
});

var AsyncModule = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(38).then(__webpack_require__.bind(null, /*! ./Module.js */ "./src/client/containers/CoursePage/Module.js"));
  },
  modules: ['./Module'],
  loading: _Loading2.default
});

var Modules = function (_Component) {
  (0, _inherits3.default)(Modules, _Component);

  function Modules(props) {
    (0, _classCallCheck3.default)(this, Modules);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Modules.__proto__ || (0, _getPrototypeOf2.default)(Modules)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Modules, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          course = _props.course;
      var isSignedIn = user.isSignedIn,
          userId = user.id;
      var authorId = course.authorId,
          courseModules = course.courseModules;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        isSignedIn && userId === authorId && _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 8,
            xl: 8,
            className: classes.gridItem
          },
          _react2.default.createElement(AsyncModuleCreate, { course: course })
        ),
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 8,
            xl: 8,
            className: classes.gridItem
          },
          typeof courseModules !== 'undefined' && courseModules.map(function (m, i) {
            return _react2.default.createElement(AsyncModule, (0, _extends3.default)({
              key: m.moduleId,
              authorId: authorId,
              index: i + 1
            }, m));
          })
        )
      );
    }
  }]);
  return Modules;
}(_react.Component);

Modules.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Modules);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9Nb2R1bGVzLmpzIl0sIm5hbWVzIjpbInN0eWxlcyIsInJvb3QiLCJncmlkSXRlbSIsInBhZGRpbmdUb3AiLCJwYWRkaW5nTGVmdCIsInBhZGRpbmdSaWdodCIsIkFzeW5jTW9kdWxlQ3JlYXRlIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBc3luY01vZHVsZSIsIk1vZHVsZXMiLCJwcm9wcyIsInN0YXRlIiwiY2xhc3NlcyIsInVzZXIiLCJjb3Vyc2UiLCJpc1NpZ25lZEluIiwidXNlcklkIiwiaWQiLCJhdXRob3JJZCIsImNvdXJzZU1vZHVsZXMiLCJtYXAiLCJtIiwiaSIsIm1vZHVsZUlkIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7OztBQVZBOztBQVlBLElBQU1BLFNBQVM7QUFDYkMsUUFBTTtBQUNKO0FBREksR0FETztBQUliQyxZQUFVO0FBQ1JDLGdCQUFZLElBREo7QUFFUkMsaUJBQWEsSUFGTDtBQUdSQyxrQkFBYztBQUhOO0FBSkcsQ0FBZjs7QUFXQSxJQUFNQyxvQkFBb0IsNkJBQVM7QUFDakNDLFVBQVE7QUFBQSxXQUFNLDJKQUFOO0FBQUEsR0FEeUI7QUFFakNDLFdBQVMsQ0FBQyxnQkFBRCxDQUZ3QjtBQUdqQ0M7QUFIaUMsQ0FBVCxDQUExQjs7QUFNQSxJQUFNQyxjQUFjLDZCQUFTO0FBQzNCSCxVQUFRO0FBQUEsV0FBTSwrSUFBTjtBQUFBLEdBRG1CO0FBRTNCQyxXQUFTLENBQUMsVUFBRCxDQUZrQjtBQUczQkM7QUFIMkIsQ0FBVCxDQUFwQjs7SUFNTUUsTzs7O0FBQ0osbUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSx3SUFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQSxtQkFDMkIsS0FBS0QsS0FEaEM7QUFBQSxVQUNDRSxPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVQyxJQURWLFVBQ1VBLElBRFY7QUFBQSxVQUNnQkMsTUFEaEIsVUFDZ0JBLE1BRGhCO0FBQUEsVUFFQ0MsVUFGRCxHQUU0QkYsSUFGNUIsQ0FFQ0UsVUFGRDtBQUFBLFVBRWlCQyxNQUZqQixHQUU0QkgsSUFGNUIsQ0FFYUksRUFGYjtBQUFBLFVBR0NDLFFBSEQsR0FHNkJKLE1BSDdCLENBR0NJLFFBSEQ7QUFBQSxVQUdXQyxhQUhYLEdBRzZCTCxNQUg3QixDQUdXSyxhQUhYOzs7QUFLUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDLEVBQTZDLFdBQVdQLFFBQVFiLElBQWhFO0FBQ0dnQixzQkFDQ0MsV0FBV0UsUUFEWixJQUVHO0FBQUE7QUFBQTtBQUNFLHNCQURGO0FBRUUsZ0JBQUksRUFGTjtBQUdFLGdCQUFJLEVBSE47QUFJRSxnQkFBSSxDQUpOO0FBS0UsZ0JBQUksQ0FMTjtBQU1FLGdCQUFJLENBTk47QUFPRSx1QkFBV04sUUFBUVo7QUFQckI7QUFTRSx3Q0FBQyxpQkFBRCxJQUFtQixRQUFRYyxNQUEzQjtBQVRGLFNBSE47QUFlRTtBQUFBO0FBQUE7QUFDRSxzQkFERjtBQUVFLGdCQUFJLEVBRk47QUFHRSxnQkFBSSxFQUhOO0FBSUUsZ0JBQUksQ0FKTjtBQUtFLGdCQUFJLENBTE47QUFNRSxnQkFBSSxDQU5OO0FBT0UsdUJBQVdGLFFBQVFaO0FBUHJCO0FBU0csaUJBQU9tQixhQUFQLEtBQXlCLFdBQXpCLElBQ0NBLGNBQWNDLEdBQWQsQ0FBa0IsVUFBQ0MsQ0FBRCxFQUFJQyxDQUFKO0FBQUEsbUJBQ2hCLDhCQUFDLFdBQUQ7QUFDRSxtQkFBS0QsRUFBRUUsUUFEVDtBQUVFLHdCQUFVTCxRQUZaO0FBR0UscUJBQU9JLElBQUk7QUFIYixlQUlNRCxDQUpOLEVBRGdCO0FBQUEsV0FBbEI7QUFWSjtBQWZGLE9BREY7QUFxQ0Q7Ozs7O0FBR0haLFFBQVFlLFNBQVIsR0FBb0I7QUFDbEJaLFdBQVMsb0JBQVVhLE1BQVYsQ0FBaUJDLFVBRFI7O0FBR2xCYixRQUFNLG9CQUFVWSxNQUFWLENBQWlCQyxVQUhMO0FBSWxCWixVQUFRLG9CQUFVVyxNQUFWLENBQWlCQztBQUpQLENBQXBCOztrQkFPZSwwQkFBVzVCLE1BQVgsRUFBbUJXLE9BQW5CLEMiLCJmaWxlIjoiNzcuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuXG5pbXBvcnQgTG9hZGFibGUgZnJvbSAncmVhY3QtbG9hZGFibGUnO1xuXG5pbXBvcnQgTG9hZGluZyBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xvYWRpbmcnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICAvLyBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBncmlkSXRlbToge1xuICAgIHBhZGRpbmdUb3A6ICcxJScsXG4gICAgcGFkZGluZ0xlZnQ6ICcxJScsXG4gICAgcGFkZGluZ1JpZ2h0OiAnMSUnLFxuICB9LFxufTtcblxuY29uc3QgQXN5bmNNb2R1bGVDcmVhdGUgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuL01vZHVsZUNyZWF0ZS5qcycpLFxuICBtb2R1bGVzOiBbJy4vTW9kdWxlQ3JlYXRlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNNb2R1bGUgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuL01vZHVsZS5qcycpLFxuICBtb2R1bGVzOiBbJy4vTW9kdWxlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY2xhc3MgTW9kdWxlcyBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHVzZXIsIGNvdXJzZSB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGlzU2lnbmVkSW4sIGlkOiB1c2VySWQgfSA9IHVzZXI7XG4gICAgY29uc3QgeyBhdXRob3JJZCwgY291cnNlTW9kdWxlcyB9ID0gY291cnNlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7aXNTaWduZWRJbiAmJlxuICAgICAgICAgIHVzZXJJZCA9PT0gYXV0aG9ySWQgJiYgKFxuICAgICAgICAgICAgPEdyaWRcbiAgICAgICAgICAgICAgaXRlbVxuICAgICAgICAgICAgICB4cz17MTJ9XG4gICAgICAgICAgICAgIHNtPXsxMH1cbiAgICAgICAgICAgICAgbWQ9ezh9XG4gICAgICAgICAgICAgIGxnPXs4fVxuICAgICAgICAgICAgICB4bD17OH1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmdyaWRJdGVtfVxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8QXN5bmNNb2R1bGVDcmVhdGUgY291cnNlPXtjb3Vyc2V9IC8+XG4gICAgICAgICAgICA8L0dyaWQ+XG4gICAgICAgICAgKX1cbiAgICAgICAgPEdyaWRcbiAgICAgICAgICBpdGVtXG4gICAgICAgICAgeHM9ezEyfVxuICAgICAgICAgIHNtPXsxMH1cbiAgICAgICAgICBtZD17OH1cbiAgICAgICAgICBsZz17OH1cbiAgICAgICAgICB4bD17OH1cbiAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19XG4gICAgICAgID5cbiAgICAgICAgICB7dHlwZW9mIGNvdXJzZU1vZHVsZXMgIT09ICd1bmRlZmluZWQnICYmXG4gICAgICAgICAgICBjb3Vyc2VNb2R1bGVzLm1hcCgobSwgaSkgPT4gKFxuICAgICAgICAgICAgICA8QXN5bmNNb2R1bGVcbiAgICAgICAgICAgICAgICBrZXk9e20ubW9kdWxlSWR9XG4gICAgICAgICAgICAgICAgYXV0aG9ySWQ9e2F1dGhvcklkfVxuICAgICAgICAgICAgICAgIGluZGV4PXtpICsgMX1cbiAgICAgICAgICAgICAgICB7Li4ubX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICkpfVxuICAgICAgICA8L0dyaWQ+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5Nb2R1bGVzLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY291cnNlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTW9kdWxlcyk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9Nb2R1bGVzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==