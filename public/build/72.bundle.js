webpackJsonp([72],{

/***/ "./src/client/actions/notes/getAll.js":
/*!********************************************!*\
  !*** ./src/client/actions/notes/getAll.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _notes = __webpack_require__(/*! ../../constants/notes */ "./src/client/constants/notes.js");

/**
 * This action will update 'notes' store
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id - element id of which all these notes belong
 * @param {Object[]} payload.notes
 * @param {Object} payload.notes[]
 * @param {number} payload.notes[].noteId
 * @param {number} payload.notes[].authorId
 * @param {string} payload.notes[].title
 * @param {string} payload.notes[].timestamp
 *
 * @returns {Object}
 */
var getAll = function getAll(payload) {
  return { type: _notes.NOTES_GET, payload: payload };
}; /** @format */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/notes/getAll.js":
/*!****************************************!*\
  !*** ./src/client/api/notes/getAll.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @return {Promise} -
 */
/** @format */

var getAllNote = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/notes';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAllNote(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAllNote;

/***/ }),

/***/ "./src/client/containers/NoteTimeline/index.js":
/*!*****************************************************!*\
  !*** ./src/client/containers/NoteTimeline/index.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../api/notes/getAll */ "./src/client/api/notes/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../actions/notes/getAll */ "./src/client/actions/notes/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      align: 'center'
    },
    flexGrow: {
      flex: '1 1 auto'
    }
  };
};

var AsyncNoteCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(58).then(__webpack_require__.bind(null, /*! ../NoteCreate */ "./src/client/containers/NoteCreate/index.js"));
  },
  modules: ['../NoteCreate'],
  loading: _Loading2.default
});

var AsyncNote = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(46).then(__webpack_require__.bind(null, /*! ../Note */ "./src/client/containers/Note/index.js"));
  },
  modules: ['../Note'],
  loading: _Loading2.default
});

var NoteTimeline = function (_Component) {
  (0, _inherits3.default)(NoteTimeline, _Component);

  function NoteTimeline(props) {
    (0, _classCallCheck3.default)(this, NoteTimeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (NoteTimeline.__proto__ || (0, _getPrototypeOf2.default)(NoteTimeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(NoteTimeline, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, elements, element, _elements$user, userId, token, id, elementType, _ref2, statusCode, error, payload, next;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, elements = _props.elements, element = _props.element;
                _elements$user = elements.user, userId = _elements$user.id, token = _elements$user.token;
                id = element.id, elementType = element.elementType;
                _context.next = 6;
                return (0, _getAll2.default)({
                  token: token,
                  userId: userId
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;
                next = _ref2.next;

                if (!(statusCode >= 300)) {
                  _context.next = 14;
                  break;
                }

                // handle error
                console.error(error);
                return _context.abrupt('return');

              case 14:

                this.props.actionNotesGet({
                  id: userId,
                  statusCode: statusCode,
                  notes: payload
                });
                _context.next = 20;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 20:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 17]]);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          elements = _props2.elements,
          element = _props2.element;
      var _elements$user2 = elements.user,
          isSignedIn = _elements$user2.isSignedIn,
          userId = _elements$user2.id;
      var notes = element.notes,
          id = element.id;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { container: true, justify: 'center', spacing: 0 },
          _react2.default.createElement(
            _Grid2.default,
            { item: true, xs: 12, sm: 9, md: 7, lg: 6, xl: 6 },
            _react2.default.createElement(AsyncNoteCreate, null)
          )
        ),
        _react2.default.createElement(
          _Grid2.default,
          { container: true, justify: 'center', spacing: 8 },
          typeof notes !== 'undefined' ? notes.map(function (_ref3) {
            var noteId = _ref3.noteId;
            return _react2.default.createElement(
              _Grid2.default,
              { item: true, key: noteId, xs: 6, sm: 6, md: 4, lg: 3, xl: 3 },
              _react2.default.createElement(AsyncNote, { key: noteId, noteId: noteId })
            );
          }) : null
        )
      );
    }
  }]);
  return NoteTimeline;
}(_react.Component);

NoteTimeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,
  element: _propTypes2.default.shape({
    id: _propTypes2.default.number,
    username: _propTypes2.default.string,
    notes: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      noteId: _propTypes2.default.number
    }))
  }),
  actionNotesGet: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements,
      notes = _ref4.notes;
  return { elements: elements, notes: notes };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionNotesGet: _getAll4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(NoteTimeline));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvbm90ZXMvZ2V0QWxsLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL25vdGVzL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvTm90ZVRpbWVsaW5lL2luZGV4LmpzIl0sIm5hbWVzIjpbImdldEFsbCIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJ1c2VySWQiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsImdldEFsbE5vdGUiLCJzdHlsZXMiLCJ0aGVtZSIsInJvb3QiLCJkaXNwbGF5IiwiZmxleFdyYXAiLCJqdXN0aWZ5Q29udGVudCIsImFsaWduIiwiZmxleEdyb3ciLCJmbGV4IiwiQXN5bmNOb3RlQ3JlYXRlIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBc3luY05vdGUiLCJOb3RlVGltZWxpbmUiLCJwcm9wcyIsInN0YXRlIiwiZWxlbWVudHMiLCJlbGVtZW50IiwidXNlciIsImlkIiwiZWxlbWVudFR5cGUiLCJuZXh0IiwiYWN0aW9uTm90ZXNHZXQiLCJub3RlcyIsImNsYXNzZXMiLCJpc1NpZ25lZEluIiwibWFwIiwibm90ZUlkIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsInNoYXBlIiwibnVtYmVyIiwidXNlcm5hbWUiLCJzdHJpbmciLCJhcnJheU9mIiwiZnVuYyIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUVBOztBQW1CQTs7Ozs7Ozs7Ozs7Ozs7O0FBZUEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQixFQUFFQyxzQkFBRixFQUFtQkQsZ0JBQW5CLEVBQS9CO0FBQUEsQ0FBZixDLENBcENBOztrQkFzQ2VELE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQ2Y7Ozs7OztBQUxBOzs7c0ZBV0E7QUFBQSxRQUE0QkcsS0FBNUIsU0FBNEJBLEtBQTVCO0FBQUEsUUFBbUNDLE1BQW5DLFNBQW1DQSxNQUFuQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ0QsTUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUMsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVOO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBYVlDLGtCQWJaLEdBYW1DRCxHQWJuQyxDQWFZQyxNQWJaLEVBYW9CQyxVQWJwQixHQWFtQ0YsR0FibkMsQ0Fhb0JFLFVBYnBCOztBQUFBLGtCQWNRRCxVQUFVLEdBZGxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWVhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBZmI7O0FBQUE7QUFBQTtBQUFBLG1CQXFCdUJGLElBQUlLLElBQUosRUFyQnZCOztBQUFBO0FBcUJVQSxnQkFyQlY7QUFBQSx3RUF1QmdCQSxJQXZCaEI7O0FBQUE7QUFBQTtBQUFBOztBQXlCSUMsb0JBQVFGLEtBQVI7O0FBekJKLDZDQTJCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUEzQlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVHLFU7Ozs7O0FBVGY7Ozs7QUFDQTs7OztrQkEwQ2VBLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0NmOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQWRBOztBQWdCQSxJQUFNQyxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRDtBQUFBLFNBQVk7QUFDekJDLFVBQU07QUFDSkMsZUFBUyxNQURMO0FBRUpDLGdCQUFVLE1BRk47QUFHSkMsc0JBQWdCLGNBSFo7QUFJSkMsYUFBTztBQUpILEtBRG1CO0FBT3pCQyxjQUFVO0FBQ1JDLFlBQU07QUFERTtBQVBlLEdBQVo7QUFBQSxDQUFmOztBQVlBLElBQU1DLGtCQUFrQiw2QkFBUztBQUMvQkMsVUFBUTtBQUFBLFdBQU0sZ0pBQU47QUFBQSxHQUR1QjtBQUUvQkMsV0FBUyxDQUFDLGVBQUQsQ0FGc0I7QUFHL0JDO0FBSCtCLENBQVQsQ0FBeEI7O0FBTUEsSUFBTUMsWUFBWSw2QkFBUztBQUN6QkgsVUFBUTtBQUFBLFdBQU0sb0lBQU47QUFBQSxHQURpQjtBQUV6QkMsV0FBUyxDQUFDLFNBQUQsQ0FGZ0I7QUFHekJDO0FBSHlCLENBQVQsQ0FBbEI7O0lBTU1FLFk7OztBQUNKLHdCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsa0pBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7Ozs7Ozs7Ozs7O3lCQUlpQyxLQUFLRCxLLEVBQTNCRSxRLFVBQUFBLFEsRUFBVUMsTyxVQUFBQSxPO2lDQUNZRCxTQUFTRSxJLEVBQTNCakMsTSxrQkFBSmtDLEUsRUFBWW5DLEssa0JBQUFBLEs7QUFDWm1DLGtCLEdBQW9CRixPLENBQXBCRSxFLEVBQUlDLFcsR0FBZ0JILE8sQ0FBaEJHLFc7O3VCQUV1QyxzQkFBWTtBQUM3RHBDLDhCQUQ2RDtBQUU3REM7QUFGNkQsaUJBQVosQzs7OztBQUEzQ1MsMEIsU0FBQUEsVTtBQUFZQyxxQixTQUFBQSxLO0FBQU9iLHVCLFNBQUFBLE87QUFBU3VDLG9CLFNBQUFBLEk7O3NCQUtoQzNCLGNBQWMsRzs7Ozs7QUFDaEI7QUFDQUcsd0JBQVFGLEtBQVIsQ0FBY0EsS0FBZDs7Ozs7QUFJRixxQkFBS21CLEtBQUwsQ0FBV1EsY0FBWCxDQUEwQjtBQUN4Qkgsc0JBQUlsQyxNQURvQjtBQUV4QlMsd0NBRndCO0FBR3hCNkIseUJBQU96QztBQUhpQixpQkFBMUI7Ozs7Ozs7O0FBTUFlLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBSUs7QUFBQSxvQkFDZ0MsS0FBS21CLEtBRHJDO0FBQUEsVUFDQ1UsT0FERCxXQUNDQSxPQUREO0FBQUEsVUFDVVIsUUFEVixXQUNVQSxRQURWO0FBQUEsVUFDb0JDLE9BRHBCLFdBQ29CQSxPQURwQjtBQUFBLDRCQUc0QkQsU0FBU0UsSUFIckM7QUFBQSxVQUdDTyxVQUhELG1CQUdDQSxVQUhEO0FBQUEsVUFHaUJ4QyxNQUhqQixtQkFHYWtDLEVBSGI7QUFBQSxVQUlDSSxLQUpELEdBSWVOLE9BSmYsQ0FJQ00sS0FKRDtBQUFBLFVBSVFKLEVBSlIsR0FJZUYsT0FKZixDQUlRRSxFQUpSOzs7QUFNUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdLLFFBQVF2QixJQUF4QjtBQUNFO0FBQUE7QUFBQSxZQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDO0FBQ0U7QUFBQTtBQUFBLGNBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLENBQXZCLEVBQTBCLElBQUksQ0FBOUIsRUFBaUMsSUFBSSxDQUFyQyxFQUF3QyxJQUFJLENBQTVDO0FBQ0UsMENBQUMsZUFBRDtBQURGO0FBREYsU0FERjtBQU1FO0FBQUE7QUFBQSxZQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDO0FBQ0csaUJBQU9zQixLQUFQLEtBQWlCLFdBQWpCLEdBQ0dBLE1BQU1HLEdBQU4sQ0FBVTtBQUFBLGdCQUFHQyxNQUFILFNBQUdBLE1BQUg7QUFBQSxtQkFDVjtBQUFBO0FBQUEsZ0JBQU0sVUFBTixFQUFXLEtBQUtBLE1BQWhCLEVBQXdCLElBQUksQ0FBNUIsRUFBK0IsSUFBSSxDQUFuQyxFQUFzQyxJQUFJLENBQTFDLEVBQTZDLElBQUksQ0FBakQsRUFBb0QsSUFBSSxDQUF4RDtBQUNFLDRDQUFDLFNBQUQsSUFBVyxLQUFLQSxNQUFoQixFQUF3QixRQUFRQSxNQUFoQztBQURGLGFBRFU7QUFBQSxXQUFWLENBREgsR0FNRztBQVBOO0FBTkYsT0FERjtBQWtCRDs7Ozs7QUFHSGQsYUFBYWUsU0FBYixHQUF5QjtBQUN2QkosV0FBUyxvQkFBVUssTUFBVixDQUFpQkMsVUFESDtBQUV2QmQsWUFBVSxvQkFBVWEsTUFBVixDQUFpQkMsVUFGSjtBQUd2QmIsV0FBUyxvQkFBVWMsS0FBVixDQUFnQjtBQUN2QlosUUFBSSxvQkFBVWEsTUFEUztBQUV2QkMsY0FBVSxvQkFBVUMsTUFGRztBQUd2QlgsV0FBTyxvQkFBVVksT0FBVixDQUNMLG9CQUFVSixLQUFWLENBQWdCO0FBQ2RKLGNBQVEsb0JBQVVLO0FBREosS0FBaEIsQ0FESztBQUhnQixHQUFoQixDQUhjO0FBWXZCVixrQkFBZ0Isb0JBQVVjLElBQVYsQ0FBZU47QUFaUixDQUF6Qjs7QUFlQSxJQUFNTyxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR3JCLFFBQUgsU0FBR0EsUUFBSDtBQUFBLE1BQWFPLEtBQWIsU0FBYUEsS0FBYjtBQUFBLFNBQTBCLEVBQUVQLGtCQUFGLEVBQVlPLFlBQVosRUFBMUI7QUFBQSxDQUF4Qjs7QUFFQSxJQUFNZSxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRWpCO0FBREYsR0FERixFQUlFaUIsUUFKRixDQUR5QjtBQUFBLENBQTNCOztrQkFRZSx5QkFBUUYsZUFBUixFQUF5QkMsa0JBQXpCLEVBQ2IsMEJBQVd2QyxNQUFYLEVBQW1CYyxZQUFuQixDQURhLEMiLCJmaWxlIjoiNzIuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IHsgTk9URVNfR0VUIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL25vdGVzJztcblxudHlwZSBOb3RlID0ge1xuICBub3RlSWQ6IG51bWJlcixcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgdGltZXN0YW1wOiBzdHJpbmcsXG59O1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHN0YXR1c0NvZGU6IG51bWJlcixcbiAgbm90ZXM6IEFycmF5PE5vdGU+LFxufTtcblxudHlwZSBSZXR1cm4gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICogVGhpcyBhY3Rpb24gd2lsbCB1cGRhdGUgJ25vdGVzJyBzdG9yZVxuICogQGZ1bmN0aW9uIGdldEFsbFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnN0YXR1c0NvZGVcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkIC0gZWxlbWVudCBpZCBvZiB3aGljaCBhbGwgdGhlc2Ugbm90ZXMgYmVsb25nXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkLm5vdGVzXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5ub3Rlc1tdXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5ub3Rlc1tdLm5vdGVJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQubm90ZXNbXS5hdXRob3JJZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubm90ZXNbXS50aXRsZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubm90ZXNbXS50aW1lc3RhbXBcbiAqXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICovXG5jb25zdCBnZXRBbGwgPSAocGF5bG9hZDogUGF5bG9hZCk6IFJldHVybiA9PiAoeyB0eXBlOiBOT1RFU19HRVQsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9ub3Rlcy9nZXRBbGwuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vY29uZmlnJztcblxuLyoqXG4gKiBAcGFyYW0ge29iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW5cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZFxuICogQHJldHVybiB7UHJvbWlzZX0gLVxuICovXG5hc3luYyBmdW5jdGlvbiBnZXRBbGxOb3RlKHsgdG9rZW4sIHVzZXJJZCB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9ub3Rlc2A7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbE5vdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9ub3Rlcy9nZXRBbGwuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuXG5pbXBvcnQgTG9hZGFibGUgZnJvbSAncmVhY3QtbG9hZGFibGUnO1xuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuaW1wb3J0IGFwaU5vdGVzR2V0IGZyb20gJy4uLy4uL2FwaS9ub3Rlcy9nZXRBbGwnO1xuaW1wb3J0IGFjdGlvbk5vdGVzR2V0IGZyb20gJy4uLy4uL2FjdGlvbnMvbm90ZXMvZ2V0QWxsJztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7XG4gICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgIGZsZXhXcmFwOiAnd3JhcCcsXG4gICAganVzdGlmeUNvbnRlbnQ6ICdzcGFjZS1hcm91bmQnLFxuICAgIGFsaWduOiAnY2VudGVyJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufSk7XG5cbmNvbnN0IEFzeW5jTm90ZUNyZWF0ZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4uL05vdGVDcmVhdGUnKSxcbiAgbW9kdWxlczogWycuLi9Ob3RlQ3JlYXRlJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNOb3RlID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi4vTm90ZScpLFxuICBtb2R1bGVzOiBbJy4uL05vdGUnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jbGFzcyBOb3RlVGltZWxpbmUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICBhc3luYyBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBlbGVtZW50cywgZWxlbWVudCB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgICBjb25zdCB7IGlkLCBlbGVtZW50VHlwZSB9ID0gZWxlbWVudDtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCwgbmV4dCB9ID0gYXdhaXQgYXBpTm90ZXNHZXQoe1xuICAgICAgICB0b2tlbixcbiAgICAgICAgdXNlcklkLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgZXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5wcm9wcy5hY3Rpb25Ob3Rlc0dldCh7XG4gICAgICAgIGlkOiB1c2VySWQsXG4gICAgICAgIHN0YXR1c0NvZGUsXG4gICAgICAgIG5vdGVzOiBwYXlsb2FkLFxuICAgICAgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH1cbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzLCBlbGVtZW50IH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgeyBpc1NpZ25lZEluLCBpZDogdXNlcklkIH0gPSBlbGVtZW50cy51c2VyO1xuICAgIGNvbnN0IHsgbm90ZXMsIGlkIH0gPSBlbGVtZW50O1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZCBjb250YWluZXIganVzdGlmeT1cImNlbnRlclwiIHNwYWNpbmc9ezB9PlxuICAgICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBzbT17OX0gbWQ9ezd9IGxnPXs2fSB4bD17Nn0+XG4gICAgICAgICAgICA8QXN5bmNOb3RlQ3JlYXRlIC8+XG4gICAgICAgICAgPC9HcmlkPlxuICAgICAgICA8L0dyaWQ+XG4gICAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17OH0+XG4gICAgICAgICAge3R5cGVvZiBub3RlcyAhPT0gJ3VuZGVmaW5lZCdcbiAgICAgICAgICAgID8gbm90ZXMubWFwKCh7IG5vdGVJZCB9KSA9PiAoXG4gICAgICAgICAgICAgIDxHcmlkIGl0ZW0ga2V5PXtub3RlSWR9IHhzPXs2fSBzbT17Nn0gbWQ9ezR9IGxnPXszfSB4bD17M30+XG4gICAgICAgICAgICAgICAgPEFzeW5jTm90ZSBrZXk9e25vdGVJZH0gbm90ZUlkPXtub3RlSWR9IC8+XG4gICAgICAgICAgICAgIDwvR3JpZD5cbiAgICAgICAgICAgICkpXG4gICAgICAgICAgICA6IG51bGx9XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuTm90ZVRpbWVsaW5lLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50OiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIGlkOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIG5vdGVzOiBQcm9wVHlwZXMuYXJyYXlPZihcbiAgICAgIFByb3BUeXBlcy5zaGFwZSh7XG4gICAgICAgIG5vdGVJZDogUHJvcFR5cGVzLm51bWJlcixcbiAgICAgIH0pLFxuICAgICksXG4gIH0pLFxuICBhY3Rpb25Ob3Rlc0dldDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzLCBub3RlcyB9KSA9PiAoeyBlbGVtZW50cywgbm90ZXMgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbk5vdGVzR2V0LFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoTm90ZVRpbWVsaW5lKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvTm90ZVRpbWVsaW5lL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==