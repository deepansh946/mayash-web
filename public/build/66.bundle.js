webpackJsonp([66],{

/***/ "./node_modules/material-ui-icons/Send.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Send.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2.01 21L23 12 2.01 3 2 10l15 2-15 2z' });

var Send = function Send(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Send = (0, _pure2.default)(Send);
Send.muiName = 'SvgIcon';

exports.default = Send;

/***/ }),

/***/ "./src/client/actions/posts/comments/create.js":
/*!*****************************************************!*\
  !*** ./src/client/actions/posts/comments/create.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _comments = __webpack_require__(/*! ../../../constants/posts/comments */ "./src/client/constants/posts/comments.js");

/**
 * Create Action
 * @function create
 * @param {object} payload
 */
var create = function create(payload) {
  return { type: _comments.POST_COMMENT_CREATE, payload: payload };
}; /** @format */

exports.default = create;

/***/ }),

/***/ "./src/client/api/posts/comments/create.js":
/*!*************************************************!*\
  !*** ./src/client/api/posts/comments/create.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.postId
 * @param {string} payload.title
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        postId = _ref2.postId,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts/' + postId + '/comments';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/Comment/Create.js":
/*!*************************************************!*\
  !*** ./src/client/containers/Comment/Create.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Send = __webpack_require__(/*! material-ui-icons/Send */ "./node_modules/material-ui-icons/Send.js");

var _Send2 = _interopRequireDefault(_Send);

var _styles = __webpack_require__(/*! ./styles */ "./src/client/containers/Comment/styles.js");

var _styles2 = _interopRequireDefault(_styles);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../api/posts/comments/create */ "./src/client/api/posts/comments/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../actions/posts/comments/create */ "./src/client/actions/posts/comments/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component is created to edit content of the article
 *
 * @format
 */

var CommentCreate = function (_Component) {
  (0, _inherits3.default)(CommentCreate, _Component);

  function CommentCreate(props) {
    (0, _classCallCheck3.default)(this, CommentCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CommentCreate.__proto__ || (0, _getPrototypeOf2.default)(CommentCreate)).call(this, props));

    _this.state = {
      comment: ''
    };
    _this.onChange = _this.onChange.bind(_this);
    _this.onClickPost = _this.onClickPost.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(CommentCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      this.setState({ comment: e.target.value });
    }
  }, {
    key: 'onClickPost',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, user, postId, userId, token, comment, _ref2, statusCode, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, user = _props.user, postId = _props.postId;
                userId = user.userId, token = user.token;
                comment = this.state.comment;
                _context.next = 6;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  postId: postId,
                  title: comment
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 11;
                  break;
                }

                return _context.abrupt('return');

              case 11:

                this.props.actionCreateComment({ payload: payload, postId: postId });
                _context.next = 17;
                break;

              case 14:
                _context.prev = 14;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 17:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 14]]);
      }));

      function onClickPost() {
        return _ref.apply(this, arguments);
      }

      return onClickPost;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          user = _props2.user;
      var comment = this.state.comment;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
          _react2.default.createElement(
            _Card2.default,
            { className: classes.card },
            _react2.default.createElement(_Card.CardHeader, {
              avatar: _react2.default.createElement(
                _Paper2.default,
                { className: classes.avatar, elevation: 1 },
                _react2.default.createElement(
                  _Link2.default,
                  { to: '/@' + user.username },
                  _react2.default.createElement(_Avatar2.default, {
                    alt: user.name,
                    src: user.avatar || '/public/photos/mayash-logo-transparent.png'
                  })
                )
              ),
              title: _react2.default.createElement(
                _Link2.default,
                { to: '/@' + user.username, className: classes.name },
                user.name
              ),
              subheader: _react2.default.createElement(_Input2.default, {
                onChange: this.onChange,
                placeholder: 'Write your comment..',
                value: comment
              }),
              action: _react2.default.createElement(
                _IconButton2.default,
                {
                  color: 'accent',
                  className: classes.button,
                  onClick: this.onClickPost
                },
                _react2.default.createElement(_Send2.default, null)
              )
            })
          )
        )
      );
    }
  }]);
  return CommentCreate;
}(_react.Component);

CommentCreate.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,
  postId: _ReactPropTypes2.default.number.isRequired,
  user: _ReactPropTypes2.default.object.isRequired,
  actionCreateComment: _ReactPropTypes2.default.func.isRequired
};


var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { user: elements.user };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionCreateComment: _create4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(_styles2.default)(CommentCreate));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2VuZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvY29tbWVudHMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3Bvc3RzL2NvbW1lbnRzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvbW1lbnQvQ3JlYXRlLmpzIl0sIm5hbWVzIjpbImNyZWF0ZSIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJ1c2VySWQiLCJwb3N0SWQiLCJ0aXRsZSIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwiYm9keSIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsInN0eWxlcyIsInJvb3QiLCJkaXNwbGF5IiwiaW5wdXQiLCJmbGV4R3JvdyIsImJvcmRlciIsImJvcmRlclJhZGl1cyIsInBhZGRpbmciLCJvdXRsaW5lIiwiYm9yZGVyQ29sb3IiLCJib3hTaGFkb3ciLCJJbnB1dCIsImNsYXNzZXMiLCJvbkNoYW5nZSIsInZhbHVlIiwicGxhY2Vob2xkZXIiLCJkaXNhYmxlZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwib25lT2ZUeXBlIiwic3RyaW5nIiwibnVtYmVyIiwiYm9vbCIsIkNvbW1lbnRDcmVhdGUiLCJwcm9wcyIsInN0YXRlIiwiY29tbWVudCIsImJpbmQiLCJvbkNsaWNrUG9zdCIsImUiLCJzZXRTdGF0ZSIsInRhcmdldCIsInVzZXIiLCJhY3Rpb25DcmVhdGVDb21tZW50IiwiY2FyZCIsImF2YXRhciIsInVzZXJuYW1lIiwibmFtZSIsImJ1dHRvbiIsIm1hcFN0YXRlVG9Qcm9wcyIsImVsZW1lbnRzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCw2Q0FBNkM7O0FBRS9GO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQkE7O0FBRUE7Ozs7O0FBS0EsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUFjLEVBQUVDLG1DQUFGLEVBQTZCRCxnQkFBN0IsRUFBZDtBQUFBLENBQWYsQyxDQVRBOztrQkFXZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNOZjs7Ozs7Ozs7Ozs7OztBQUxBOzs7c0ZBa0JBO0FBQUEsUUFBd0JHLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxNQUEvQixTQUErQkEsTUFBL0I7QUFBQSxRQUF1Q0MsTUFBdkMsU0FBdUNBLE1BQXZDO0FBQUEsUUFBK0NDLEtBQS9DLFNBQStDQSxLQUEvQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ0gsTUFGckMsZUFFcURDLE1BRnJEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1FLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsTUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlUjtBQUhSLGVBRmtCO0FBTzNCUyxvQkFBTSx5QkFBZSxFQUFFTixZQUFGLEVBQWY7QUFQcUIsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBY1lDLGtCQWRaLEdBY21DRCxHQWRuQyxDQWNZQyxNQWRaLEVBY29CQyxVQWRwQixHQWNtQ0YsR0FkbkMsQ0Fjb0JFLFVBZHBCOztBQUFBLGtCQWdCUUQsVUFBVSxHQWhCbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBaUJhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBakJiOztBQUFBO0FBQUE7QUFBQSxtQkF1QnVCRixJQUFJSyxJQUFKLEVBdkJ2Qjs7QUFBQTtBQXVCVUEsZ0JBdkJWO0FBQUEsd0VBeUJnQkEsSUF6QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEyQklDLG9CQUFRRixLQUFSOztBQTNCSiw2Q0E2Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBN0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlakIsTTs7Ozs7QUFoQmY7Ozs7QUFDQTs7OztrQkFtRGVBLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvQ2Y7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNb0IsU0FBUztBQUNiQyxRQUFNO0FBQ0pDLGFBQVM7QUFETCxHQURPO0FBSWJDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTEMsa0JBQWMsS0FIVDtBQUlMQyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RDLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEY7QUFMTjtBQUpNLENBQWYsQyxDQVpBOzs7Ozs7O0FBNkJBLElBQU1DLFFBQVEsU0FBUkEsS0FBUTtBQUFBLE1BQUdDLE9BQUgsUUFBR0EsT0FBSDtBQUFBLE1BQVlDLFFBQVosUUFBWUEsUUFBWjtBQUFBLE1BQXNCQyxLQUF0QixRQUFzQkEsS0FBdEI7QUFBQSxNQUE2QkMsV0FBN0IsUUFBNkJBLFdBQTdCO0FBQUEsTUFBMENDLFFBQTFDLFFBQTBDQSxRQUExQztBQUFBLE1BQW9EbEMsSUFBcEQsUUFBb0RBLElBQXBEO0FBQUEsU0FDWjtBQUFBO0FBQUEsTUFBSyxXQUFXOEIsUUFBUVgsSUFBeEI7QUFDRTtBQUNFLFlBQU1uQixRQUFRLE1BRGhCO0FBRUUsbUJBQWFpQyxlQUFlLE9BRjlCO0FBR0UsYUFBT0QsS0FIVDtBQUlFLGdCQUFVRCxRQUpaO0FBS0UsaUJBQVdELFFBQVFULEtBTHJCO0FBTUUsZ0JBQVUsQ0FBQyxDQUFDYTtBQU5kO0FBREYsR0FEWTtBQUFBLENBQWQ7O0FBYUFMLE1BQU1NLFNBQU4sR0FBa0I7QUFDaEJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBRFY7QUFFaEJOLFlBQVUsb0JBQVVPLElBQVYsQ0FBZUQsVUFGVDtBQUdoQkwsU0FBTyxvQkFBVU8sU0FBVixDQUFvQixDQUFDLG9CQUFVQyxNQUFYLEVBQW1CLG9CQUFVQyxNQUE3QixDQUFwQixFQUEwREosVUFIakQ7QUFJaEJKLGVBQWEsb0JBQVVPLE1BSlA7QUFLaEJ4QyxRQUFNLG9CQUFVd0MsTUFMQTtBQU1oQk4sWUFBVSxvQkFBVVE7QUFOSixDQUFsQjs7a0JBU2UsMEJBQVd4QixNQUFYLEVBQW1CVyxLQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdDZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7Ozs7QUEzQkE7Ozs7OztJQTZCTWMsYTs7O0FBUUoseUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxvSkFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hDLGVBQVM7QUFERSxLQUFiO0FBR0EsVUFBS2YsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWNnQixJQUFkLE9BQWhCO0FBQ0EsVUFBS0MsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCRCxJQUFqQixPQUFuQjtBQU5pQjtBQU9sQjs7Ozs2QkFFUUUsQyxFQUFHO0FBQ1YsV0FBS0MsUUFBTCxDQUFjLEVBQUVKLFNBQVNHLEVBQUVFLE1BQUYsQ0FBU25CLEtBQXBCLEVBQWQ7QUFDRDs7Ozs7Ozs7Ozs7O3lCQUk0QixLQUFLWSxLLEVBQXRCUSxJLFVBQUFBLEksRUFBTWpELE0sVUFBQUEsTTtBQUNORCxzQixHQUFrQmtELEksQ0FBbEJsRCxNLEVBQVFELEssR0FBVW1ELEksQ0FBVm5ELEs7QUFDUjZDLHVCLEdBQVksS0FBS0QsSyxDQUFqQkMsTzs7dUJBRThCLHNCQUFpQjtBQUNyRDdDLDhCQURxRDtBQUVyREMsZ0NBRnFEO0FBR3JEQyxnQ0FIcUQ7QUFJckRDLHlCQUFPMEM7QUFKOEMsaUJBQWpCLEM7Ozs7QUFBOUJoQywwQixTQUFBQSxVO0FBQVlmLHVCLFNBQUFBLE87O3NCQU9oQmUsY0FBYyxHOzs7Ozs7Ozs7QUFJbEIscUJBQUs4QixLQUFMLENBQVdTLG1CQUFYLENBQStCLEVBQUV0RCxnQkFBRixFQUFXSSxjQUFYLEVBQS9COzs7Ozs7OztBQUVBYyx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQUlLO0FBQUEsb0JBQ21CLEtBQUs2QixLQUR4QjtBQUFBLFVBQ0NkLE9BREQsV0FDQ0EsT0FERDtBQUFBLFVBQ1VzQixJQURWLFdBQ1VBLElBRFY7QUFBQSxVQUVDTixPQUZELEdBRWEsS0FBS0QsS0FGbEIsQ0FFQ0MsT0FGRDs7O0FBSVAsYUFDRTtBQUFBO0FBQUEsVUFBTSxlQUFOLEVBQWdCLFNBQVEsUUFBeEIsRUFBaUMsU0FBUyxDQUExQyxFQUE2QyxXQUFXaEIsUUFBUVgsSUFBaEU7QUFDRTtBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLElBQUksRUFBdkIsRUFBMkIsSUFBSSxFQUEvQixFQUFtQyxJQUFJLEVBQXZDLEVBQTJDLElBQUksRUFBL0M7QUFDRTtBQUFBO0FBQUEsY0FBTSxXQUFXVyxRQUFRd0IsSUFBekI7QUFDRTtBQUNFLHNCQUNFO0FBQUE7QUFBQSxrQkFBTyxXQUFXeEIsUUFBUXlCLE1BQTFCLEVBQWtDLFdBQVcsQ0FBN0M7QUFDRTtBQUFBO0FBQUEsb0JBQU0sV0FBU0gsS0FBS0ksUUFBcEI7QUFDRTtBQUNFLHlCQUFLSixLQUFLSyxJQURaO0FBRUUseUJBQ0VMLEtBQUtHLE1BQUwsSUFDQTtBQUpKO0FBREY7QUFERixlQUZKO0FBY0UscUJBQ0U7QUFBQTtBQUFBLGtCQUFNLFdBQVNILEtBQUtJLFFBQXBCLEVBQWdDLFdBQVcxQixRQUFRMkIsSUFBbkQ7QUFDR0wscUJBQUtLO0FBRFIsZUFmSjtBQW1CRSx5QkFDRTtBQUNFLDBCQUFVLEtBQUsxQixRQURqQjtBQUVFLDZCQUFZLHNCQUZkO0FBR0UsdUJBQU9lO0FBSFQsZ0JBcEJKO0FBMEJFLHNCQUNFO0FBQUE7QUFBQTtBQUNFLHlCQUFNLFFBRFI7QUFFRSw2QkFBV2hCLFFBQVE0QixNQUZyQjtBQUdFLDJCQUFTLEtBQUtWO0FBSGhCO0FBS0U7QUFMRjtBQTNCSjtBQURGO0FBREY7QUFERixPQURGO0FBNENEOzs7OztBQTVGR0wsYSxDQUNHUixTLEdBQVk7QUFDakJMLFdBQVMseUJBQVVNLE1BQVYsQ0FBaUJDLFVBRFQ7QUFFakJsQyxVQUFRLHlCQUFVc0MsTUFBVixDQUFpQkosVUFGUjtBQUdqQmUsUUFBTSx5QkFBVWhCLE1BQVYsQ0FBaUJDLFVBSE47QUFJakJnQix1QkFBcUIseUJBQVVmLElBQVYsQ0FBZUQ7QUFKbkIsQzs7O0FBOEZyQixJQUFNc0Isa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdDLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVSLE1BQU1RLFNBQVNSLElBQWpCLEVBQW5CO0FBQUEsQ0FBeEI7O0FBRUEsSUFBTVMscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VUO0FBREYsR0FERixFQUlFUyxRQUpGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVFlLHlCQUFRSCxlQUFSLEVBQXlCRSxrQkFBekIsRUFDYiw0Q0FBbUJsQixhQUFuQixDQURhLEMiLCJmaWxlIjoiNjYuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMi4wMSAyMUwyMyAxMiAyLjAxIDMgMiAxMGwxNSAyLTE1IDJ6JyB9KTtcblxudmFyIFNlbmQgPSBmdW5jdGlvbiBTZW5kKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5TZW5kID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTZW5kKTtcblNlbmQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gU2VuZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TZW5kLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TZW5kLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNCA2NiIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCB7IFBPU1RfQ09NTUVOVF9DUkVBVEUgfSBmcm9tICcuLi8uLi8uLi9jb25zdGFudHMvcG9zdHMvY29tbWVudHMnO1xuXG4vKipcbiAqIENyZWF0ZSBBY3Rpb25cbiAqIEBmdW5jdGlvbiBjcmVhdGVcbiAqIEBwYXJhbSB7b2JqZWN0fSBwYXlsb2FkXG4gKi9cbmNvbnN0IGNyZWF0ZSA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBQT1NUX0NPTU1FTlRfQ1JFQVRFLCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvY29tbWVudHMvY3JlYXRlLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGNyZWF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnBvc3RJZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGVcbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBjcmVhdGUoeyB0b2tlbiwgdXNlcklkLCBwb3N0SWQsIHRpdGxlIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L3Bvc3RzLyR7cG9zdElkfS9jb21tZW50c2A7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoeyB0aXRsZSB9KSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9wb3N0cy9jb21tZW50cy9jcmVhdGUuanMiLCIvKipcbiAqIFRoaXMgaW5wdXQgY29tcG9uZW50IGlzIG1haW5seSBkZXZlbG9wZWQgZm9yIGNyZWF0ZSBwb3N0LCBjcmVhdGVcbiAqIGNvdXJzZSBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgfSxcbiAgaW5wdXQ6IHtcbiAgICBmbGV4R3JvdzogJzEnLFxuICAgIGJvcmRlcjogJzJweCBzb2xpZCAjZGFkYWRhJyxcbiAgICBib3JkZXJSYWRpdXM6ICc3cHgnLFxuICAgIHBhZGRpbmc6ICc4cHgnLFxuICAgICcmOmZvY3VzJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICcjOWVjYWVkJyxcbiAgICAgIGJveFNoYWRvdzogJzAgMCAxMHB4ICM5ZWNhZWQnLFxuICAgIH0sXG4gIH0sXG59O1xuXG5jb25zdCBJbnB1dCA9ICh7IGNsYXNzZXMsIG9uQ2hhbmdlLCB2YWx1ZSwgcGxhY2Vob2xkZXIsIGRpc2FibGVkLCB0eXBlIH0pID0+IChcbiAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgPGlucHV0XG4gICAgICB0eXBlPXt0eXBlIHx8ICd0ZXh0J31cbiAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlciB8fCAnaW5wdXQnfVxuICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgZGlzYWJsZWQ9eyEhZGlzYWJsZWR9XG4gICAgLz5cbiAgPC9kaXY+XG4pO1xuXG5JbnB1dC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIHZhbHVlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSkuaXNSZXF1aXJlZCxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShJbnB1dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCIvKipcbiAqIFRoaXMgY29tcG9uZW50IGlzIGNyZWF0ZWQgdG8gZWRpdCBjb250ZW50IG9mIHRoZSBhcnRpY2xlXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIgfSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBQYXBlciBmcm9tICdtYXRlcmlhbC11aS9QYXBlcic7XG5cbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IFNlbmRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NlbmQnO1xuXG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGVzJztcbmltcG9ydCBJbnB1dCBmcm9tICcuLi8uLi9jb21wb25lbnRzL0lucHV0JztcblxuaW1wb3J0IGFwaUNyZWF0ZUNvbW1lbnQgZnJvbSAnLi4vLi4vYXBpL3Bvc3RzL2NvbW1lbnRzL2NyZWF0ZSc7XG5cbmltcG9ydCBhY3Rpb25DcmVhdGVDb21tZW50IGZyb20gJy4uLy4uL2FjdGlvbnMvcG9zdHMvY29tbWVudHMvY3JlYXRlJztcblxuY2xhc3MgQ29tbWVudENyZWF0ZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAgIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBhY3Rpb25DcmVhdGVDb21tZW50OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBjb21tZW50OiAnJyxcbiAgICB9O1xuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNsaWNrUG9zdCA9IHRoaXMub25DbGlja1Bvc3QuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKGUpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgY29tbWVudDogZS50YXJnZXQudmFsdWUgfSk7XG4gIH1cblxuICBhc3luYyBvbkNsaWNrUG9zdCgpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyB1c2VyLCBwb3N0SWQgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IHVzZXJJZCwgdG9rZW4gfSA9IHVzZXI7XG4gICAgICBjb25zdCB7IGNvbW1lbnQgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpQ3JlYXRlQ29tbWVudCh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIHBvc3RJZCxcbiAgICAgICAgdGl0bGU6IGNvbW1lbnQsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5wcm9wcy5hY3Rpb25DcmVhdGVDb21tZW50KHsgcGF5bG9hZCwgcG9zdElkIH0pO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICB9XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCB1c2VyIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgY29tbWVudCB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8R3JpZCBjb250YWluZXIganVzdGlmeT1cImNlbnRlclwiIHNwYWNpbmc9ezB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPEdyaWQgaXRlbSB4cz17MTJ9IHNtPXsxMn0gbWQ9ezEyfSBsZz17MTJ9IHhsPXsxMn0+XG4gICAgICAgICAgPENhcmQgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgICA8UGFwZXIgY2xhc3NOYW1lPXtjbGFzc2VzLmF2YXRhcn0gZWxldmF0aW9uPXsxfT5cbiAgICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake3VzZXIudXNlcm5hbWV9YH0+XG4gICAgICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgICAgICBhbHQ9e3VzZXIubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgICAgICAgdXNlci5hdmF0YXIgfHxcbiAgICAgICAgICAgICAgICAgICAgICAgICcvcHVibGljL3Bob3Rvcy9tYXlhc2gtbG9nby10cmFuc3BhcmVudC5wbmcnXG4gICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICAgIDwvUGFwZXI+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake3VzZXIudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLm5hbWV9PlxuICAgICAgICAgICAgICAgICAge3VzZXIubmFtZX1cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgc3ViaGVhZGVyPXtcbiAgICAgICAgICAgICAgICA8SW5wdXRcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJXcml0ZSB5b3VyIGNvbW1lbnQuLlwiXG4gICAgICAgICAgICAgICAgICB2YWx1ZT17Y29tbWVudH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIGFjdGlvbj17XG4gICAgICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgICAgIGNvbG9yPVwiYWNjZW50XCJcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5idXR0b259XG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tQb3N0fVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxTZW5kSWNvbiAvPlxuICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IHVzZXI6IGVsZW1lbnRzLnVzZXIgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbkNyZWF0ZUNvbW1lbnQsXG4gICAgfSxcbiAgICBkaXNwYXRjaCxcbiAgKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShDb21tZW50Q3JlYXRlKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ29tbWVudC9DcmVhdGUuanMiXSwic291cmNlUm9vdCI6IiJ9