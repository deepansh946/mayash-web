webpackJsonp([9],{

/***/ "./node_modules/material-ui/AppBar/AppBar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/AppBar/AppBar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Paper

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      boxSizing: 'border-box', // Prevent padding issue with the Modal and fixed positioned AppBar.
      zIndex: theme.zIndex.appBar,
      flexShrink: 0
    },
    positionFixed: {
      position: 'fixed',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionAbsolute: {
      position: 'absolute',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionStatic: {
      position: 'static',
      flexShrink: 0
    },
    colorDefault: {
      backgroundColor: theme.palette.background.appBar,
      color: theme.palette.getContrastText(theme.palette.background.appBar)
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']);

var babelPluginFlowReactPropTypes_proptype_Position = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']).isRequired,

  /**
   * The positioning type.
   */
  position: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']).isRequired
};

var AppBar = function (_React$Component) {
  (0, _inherits3.default)(AppBar, _React$Component);

  function AppBar() {
    (0, _classCallCheck3.default)(this, AppBar);
    return (0, _possibleConstructorReturn3.default)(this, (AppBar.__proto__ || (0, _getPrototypeOf2.default)(AppBar)).apply(this, arguments));
  }

  (0, _createClass3.default)(AppBar, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          position = _props.position,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'position']);


      var className = (0, _classnames2.default)(classes.root, classes['position' + (0, _helpers.capitalizeFirstLetter)(position)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), (0, _defineProperty3.default)(_classNames, 'mui-fixed', position === 'fixed'), _classNames), classNameProp);

      return _react2.default.createElement(
        _Paper2.default,
        (0, _extends3.default)({ square: true, component: 'header', elevation: 4, className: className }, other),
        children
      );
    }
  }]);
  return AppBar;
}(_react2.default.Component);

AppBar.defaultProps = {
  color: 'primary',
  position: 'fixed'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAppBar' })(AppBar);

/***/ }),

/***/ "./node_modules/material-ui/AppBar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/AppBar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppBar = __webpack_require__(/*! ./AppBar */ "./node_modules/material-ui/AppBar/AppBar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_AppBar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Icon/Icon.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Icon/Icon.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      userSelect: 'none'
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The name of the icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired
};

var Icon = function (_React$Component) {
  (0, _inherits3.default)(Icon, _React$Component);

  function Icon() {
    (0, _classCallCheck3.default)(this, Icon);
    return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
  }

  (0, _createClass3.default)(Icon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color']);


      var className = (0, _classnames2.default)('material-icons', classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'span',
        (0, _extends3.default)({ className: className, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Icon;
}(_react2.default.Component);

Icon.defaultProps = {
  color: 'inherit'
};
Icon.muiName = 'Icon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIcon' })(Icon);

/***/ }),

/***/ "./node_modules/material-ui/Icon/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Icon/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/material-ui/Icon/Icon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Icon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Tabs/Tab.js":
/*!**********************************************!*\
  !*** ./node_modules/material-ui/Tabs/Tab.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends4 = _interopRequireDefault(_extends3);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent ButtonBase

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends4.default)({}, theme.typography.button, (0, _defineProperty3.default)({
      maxWidth: 264,
      position: 'relative',
      minWidth: 72,
      padding: 0,
      height: 48,
      flex: 'none',
      overflow: 'hidden'
    }, theme.breakpoints.up('md'), {
      minWidth: 160
    })),
    rootLabelIcon: {
      height: 72
    },
    rootAccent: {
      color: theme.palette.text.secondary
    },
    rootAccentSelected: {
      color: theme.palette.secondary.A200
    },
    rootAccentDisabled: {
      color: theme.palette.text.disabled
    },
    rootPrimary: {
      color: theme.palette.text.secondary
    },
    rootPrimarySelected: {
      color: theme.palette.primary[500]
    },
    rootPrimaryDisabled: {
      color: theme.palette.text.disabled
    },
    rootInherit: {
      color: 'inherit',
      opacity: 0.7
    },
    rootInheritSelected: {
      opacity: 1
    },
    rootInheritDisabled: {
      opacity: 0.4
    },
    fullWidth: {
      flexGrow: 1
    },
    wrapper: {
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      flexDirection: 'column'
    },
    labelContainer: (0, _defineProperty3.default)({
      paddingTop: 6,
      paddingBottom: 6,
      paddingLeft: 12,
      paddingRight: 12
    }, theme.breakpoints.up('md'), {
      paddingLeft: theme.spacing.unit * 3,
      paddingRight: theme.spacing.unit * 3
    }),
    label: (0, _defineProperty3.default)({
      fontSize: theme.typography.pxToRem(theme.typography.fontSize),
      whiteSpace: 'normal'
    }, theme.breakpoints.up('md'), {
      fontSize: theme.typography.pxToRem(theme.typography.fontSize - 1)
    }),
    labelWrapped: (0, _defineProperty3.default)({}, theme.breakpoints.down('md'), {
      fontSize: theme.typography.pxToRem(theme.typography.fontSize - 2)
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tab will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * The icon element. If a string is provided, it will be used as a font ligature.
   */
  icon: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * For server side rendering consideration, we let the selected tab
   * render the indicator.
   */
  indicator: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * The label element.
   */
  label: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  selected: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  textColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * You can provide your own value. Otherwise, we fallback to the child position index.
   */
  value: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any
};

var Tab = function (_React$Component) {
  (0, _inherits3.default)(Tab, _React$Component);

  function Tab() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Tab);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Tab.__proto__ || (0, _getPrototypeOf2.default)(Tab)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      wrappedText: false
    }, _this.handleChange = function (event) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          value = _this$props.value,
          onClick = _this$props.onClick;


      if (onChange) {
        onChange(event, value);
      }

      if (onClick) {
        onClick(event);
      }
    }, _this.label = undefined, _this.checkTextWrap = function () {
      if (_this.label) {
        var _wrappedText = _this.label.getClientRects().length > 1;
        if (_this.state.wrappedText !== _wrappedText) {
          _this.setState({ wrappedText: _wrappedText });
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Tab, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.checkTextWrap();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (this.state.wrappedText === prevState.wrappedText) {
        /**
         * At certain text and tab lengths, a larger font size may wrap to two lines while the smaller
         * font size still only requires one line.  This check will prevent an infinite render loop
         * fron occurring in that scenario.
         */
        this.checkTextWrap();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this,
          _classNames2;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          disabled = _props.disabled,
          fullWidth = _props.fullWidth,
          iconProp = _props.icon,
          indicator = _props.indicator,
          labelProp = _props.label,
          onChange = _props.onChange,
          selected = _props.selected,
          styleProp = _props.style,
          textColor = _props.textColor,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'disabled', 'fullWidth', 'icon', 'indicator', 'label', 'onChange', 'selected', 'style', 'textColor', 'value']);


      var icon = void 0;

      if (iconProp !== undefined) {
        icon = _react2.default.isValidElement(iconProp) ? iconProp : _react2.default.createElement(
          _Icon2.default,
          null,
          iconProp
        );
      }

      var label = void 0;

      if (labelProp !== undefined) {
        label = _react2.default.createElement(
          'div',
          { className: classes.labelContainer },
          _react2.default.createElement(
            'span',
            {
              className: (0, _classnames2.default)(classes.label, (0, _defineProperty3.default)({}, classes.labelWrapped, this.state.wrappedText)),
              ref: function ref(node) {
                _this2.label = node;
              }
            },
            labelProp
          )
        );
      }

      var className = (0, _classnames2.default)(classes.root, (_classNames2 = {}, (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor)], true), (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor) + 'Disabled'], disabled), (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor) + 'Selected'], selected), (0, _defineProperty3.default)(_classNames2, classes.rootLabelIcon, icon && label), (0, _defineProperty3.default)(_classNames2, classes.fullWidth, fullWidth), _classNames2), classNameProp);

      var style = {};

      if (textColor !== 'accent' && textColor !== 'inherit') {
        style.color = textColor;
      }

      style = (0, _keys2.default)(style).length > 0 ? (0, _extends4.default)({}, style, styleProp) : styleProp;

      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends4.default)({
          focusRipple: true,
          className: className,
          style: style,
          role: 'tab',
          'aria-selected': selected,
          disabled: disabled
        }, other, {
          onClick: this.handleChange
        }),
        _react2.default.createElement(
          'span',
          { className: classes.wrapper },
          icon,
          label
        ),
        indicator
      );
    }
  }]);
  return Tab;
}(_react2.default.Component);

Tab.defaultProps = {
  disabled: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTab' })(Tab);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/TabIndicator.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/Tabs/TabIndicator.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _ref; //  weak

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      height: 2,
      bottom: 0,
      width: '100%',
      transition: theme.transitions.create(),
      willChange: 'left, width'
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_IndicatorStyle = {
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
};
var babelPluginFlowReactPropTypes_proptype_ProvidedProps = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired
};
var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   * The color of the tab indicator.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired,

  /**
   * @ignore
   * The style of the root element.
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }).isRequired
};


/**
 * @ignore - internal component.
 */
function TabIndicator(props) {
  var classes = props.classes,
      classNameProp = props.className,
      color = props.color,
      styleProp = props.style;

  var colorPredefined = ['primary', 'accent'].indexOf(color) !== -1;
  var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], colorPredefined), classNameProp);

  var style = colorPredefined ? styleProp : (0, _extends3.default)({}, styleProp, {
    backgroundColor: color
  });

  return _react2.default.createElement('div', { className: className, style: style });
}

TabIndicator.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), (0, _defineProperty3.default)(_ref, 'color', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired), (0, _defineProperty3.default)(_ref, 'style', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}).isRequired), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTabIndicator' })(TabIndicator);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/TabScrollButton.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Tabs/TabScrollButton.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _KeyboardArrowLeft = __webpack_require__(/*! ../svg-icons/KeyboardArrowLeft */ "./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js");

var _KeyboardArrowLeft2 = _interopRequireDefault(_KeyboardArrowLeft);

var _KeyboardArrowRight = __webpack_require__(/*! ../svg-icons/KeyboardArrowRight */ "./node_modules/material-ui/svg-icons/KeyboardArrowRight.js");

var _KeyboardArrowRight2 = _interopRequireDefault(_KeyboardArrowRight);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//  weak

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      color: 'inherit',
      flex: '0 0 ' + theme.spacing.unit * 7 + 'px'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Which direction should the button indicate?
   */
  direction: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left', 'right']).isRequired,

  /**
   * Callback to execute for button press.
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Should the button be present or just consume space.
   */
  visible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var _ref = _react2.default.createElement(_KeyboardArrowLeft2.default, null);

var _ref2 = _react2.default.createElement(_KeyboardArrowRight2.default, null);

/**
 * @ignore - internal component.
 */
var TabScrollButton = function (_React$Component) {
  (0, _inherits3.default)(TabScrollButton, _React$Component);

  function TabScrollButton() {
    (0, _classCallCheck3.default)(this, TabScrollButton);
    return (0, _possibleConstructorReturn3.default)(this, (TabScrollButton.__proto__ || (0, _getPrototypeOf2.default)(TabScrollButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(TabScrollButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          direction = _props.direction,
          onClick = _props.onClick,
          visible = _props.visible,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'direction', 'onClick', 'visible']);


      var className = (0, _classnames2.default)(classes.root, classNameProp);

      if (!visible) {
        return _react2.default.createElement('div', { className: className });
      }

      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({ className: className, onClick: onClick, tabIndex: -1 }, other),
        direction === 'left' ? _ref : _ref2
      );
    }
  }]);
  return TabScrollButton;
}(_react2.default.Component);

TabScrollButton.defaultProps = {
  visible: true
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTabScrollButton' })(TabScrollButton);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/Tabs.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Tabs/Tabs.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _isNan = __webpack_require__(/*! babel-runtime/core-js/number/is-nan */ "./node_modules/babel-runtime/core-js/number/is-nan.js");

var _isNan2 = _interopRequireDefault(_isNan);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _debounce = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");

var _debounce2 = _interopRequireDefault(_debounce);

var _reactScrollbarSize = __webpack_require__(/*! react-scrollbar-size */ "./node_modules/react-scrollbar-size/index.js");

var _reactScrollbarSize2 = _interopRequireDefault(_reactScrollbarSize);

var _normalizeScrollLeft = __webpack_require__(/*! normalize-scroll-left */ "./node_modules/normalize-scroll-left/lib/main.js");

var _scroll = __webpack_require__(/*! scroll */ "./node_modules/scroll/index.js");

var _scroll2 = _interopRequireDefault(_scroll);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _TabIndicator = __webpack_require__(/*! ./TabIndicator */ "./node_modules/material-ui/Tabs/TabIndicator.js");

var _TabIndicator2 = _interopRequireDefault(_TabIndicator);

var _TabScrollButton = __webpack_require__(/*! ./TabScrollButton */ "./node_modules/material-ui/Tabs/TabScrollButton.js");

var _TabScrollButton2 = _interopRequireDefault(_TabScrollButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ComponentType = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_IndicatorStyle = __webpack_require__(/*! ./TabIndicator */ "./node_modules/material-ui/Tabs/TabIndicator.js").babelPluginFlowReactPropTypes_proptype_IndicatorStyle || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      overflow: 'hidden',
      minHeight: 48,
      WebkitOverflowScrolling: 'touch' // Add iOS momentum scrolling.
    },
    flexContainer: {
      display: 'flex'
    },
    scrollingContainer: {
      position: 'relative',
      display: 'inline-block',
      flex: '1 1 auto',
      whiteSpace: 'nowrap'
    },
    fixed: {
      overflowX: 'hidden',
      width: '100%'
    },
    scrollable: {
      overflowX: 'scroll'
    },
    centered: {
      justifyContent: 'center'
    },
    buttonAuto: (0, _defineProperty3.default)({}, theme.breakpoints.down('sm'), {
      display: 'none'
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the scroll button elements.
   */
  buttonClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tabs will be centered.
   * This property is intended for large views.
   */
  centered: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tabs will grow to use all the available space.
   * This property is intended for small views, like on mobile.
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The CSS class name of the indicator element.
   */
  indicatorClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Determines the color of the indicator.
   */
  indicatorColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired,

  /**
   * Callback fired when the value changes.
   *
   * @param {object} event The event source of the callback
   * @param {number} value We default to the index of the child
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func.isRequired,

  /**
   * True invokes scrolling properties and allow for horizontally scrolling
   * (or swiping) the tab bar.
   */
  scrollable: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Determine behavior of scroll buttons when tabs are set to scroll
   * `auto` will only present them on medium and larger viewports
   * `on` will always present them
   * `off` will never present them
   */
  scrollButtons: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto', 'on', 'off']).isRequired,

  /**
   * The component used to render the scroll buttons.
   */
  TabScrollButton: typeof babelPluginFlowReactPropTypes_proptype_ComponentType === 'function' ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired : babelPluginFlowReactPropTypes_proptype_ComponentType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ComponentType).isRequired,

  /**
   * Determines the color of the `Tab`.
   */
  textColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent', 'primary', 'inherit']).isRequired,

  /**
   * The value of the currently selected `Tab`.
   * If you don't want any selected `Tab`, you can set this property to `false`.
   */
  value: function value(props, propName, componentName) {
    if (!Object.prototype.hasOwnProperty.call(props, propName)) {
      throw new Error('Prop `' + propName + '` has type \'any\' or \'mixed\', but was not provided to `' + componentName + '`. Pass undefined or any other value.');
    }
  }
};
var babelPluginFlowReactPropTypes_proptype_TabsMeta = {
  clientWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollLeft: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollLeftNormalized: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,

  // ClientRect
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  right: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
};

var Tabs = function (_React$Component) {
  (0, _inherits3.default)(Tabs, _React$Component);

  function Tabs() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Tabs);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Tabs.__proto__ || (0, _getPrototypeOf2.default)(Tabs)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      indicatorStyle: {},
      scrollerStyle: {
        marginBottom: 0
      },
      showLeftScroll: false,
      showRightScroll: false,
      mounted: false
    }, _this.getConditionalElements = function () {
      var _this$props = _this.props,
          classes = _this$props.classes,
          buttonClassName = _this$props.buttonClassName,
          scrollable = _this$props.scrollable,
          scrollButtons = _this$props.scrollButtons,
          TabScrollButtonProp = _this$props.TabScrollButton,
          theme = _this$props.theme;

      var conditionalElements = {};
      conditionalElements.scrollbarSizeListener = scrollable ? _react2.default.createElement(_reactScrollbarSize2.default, {
        onLoad: _this.handleScrollbarSizeChange,
        onChange: _this.handleScrollbarSizeChange
      }) : null;

      var showScrollButtons = scrollable && (scrollButtons === 'auto' || scrollButtons === 'on');

      conditionalElements.scrollButtonLeft = showScrollButtons ? _react2.default.createElement(TabScrollButtonProp, {
        direction: theme && theme.direction === 'rtl' ? 'right' : 'left',
        onClick: _this.handleLeftScrollClick,
        visible: _this.state.showLeftScroll,
        className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.buttonAuto, scrollButtons === 'auto'), buttonClassName)
      }) : null;

      conditionalElements.scrollButtonRight = showScrollButtons ? _react2.default.createElement(TabScrollButtonProp, {
        direction: theme && theme.direction === 'rtl' ? 'left' : 'right',
        onClick: _this.handleRightScrollClick,
        visible: _this.state.showRightScroll,
        className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.buttonAuto, scrollButtons === 'auto'), buttonClassName)
      }) : null;

      return conditionalElements;
    }, _this.getTabsMeta = function (value, direction) {
      var tabsMeta = void 0;
      if (_this.tabs) {
        var rect = _this.tabs.getBoundingClientRect();
        // create a new object with ClientRect class props + scrollLeft
        tabsMeta = {
          clientWidth: _this.tabs ? _this.tabs.clientWidth : 0,
          scrollLeft: _this.tabs ? _this.tabs.scrollLeft : 0,
          scrollLeftNormalized: _this.tabs ? (0, _normalizeScrollLeft.getNormalizedScrollLeft)(_this.tabs, direction) : 0,
          scrollWidth: _this.tabs ? _this.tabs.scrollWidth : 0,
          left: rect.left,
          right: rect.right
        };
      }

      var tabMeta = void 0;
      if (_this.tabs && value !== false) {
        var _children = _this.tabs.children[0].children;

        if (_children.length > 0) {
          var tab = _children[_this.valueToIndex[value]];
           true ? (0, _warning2.default)(Boolean(tab), 'Material-UI: the value provided `' + value + '` is invalid') : void 0;
          tabMeta = tab ? tab.getBoundingClientRect() : null;
        }
      }
      return { tabsMeta: tabsMeta, tabMeta: tabMeta };
    }, _this.tabs = undefined, _this.valueToIndex = {}, _this.handleResize = (0, _debounce2.default)(function () {
      _this.updateIndicatorState(_this.props);
      _this.updateScrollButtonState();
    }, 166), _this.handleLeftScrollClick = function () {
      if (_this.tabs) {
        _this.moveTabsScroll(-_this.tabs.clientWidth);
      }
    }, _this.handleRightScrollClick = function () {
      if (_this.tabs) {
        _this.moveTabsScroll(_this.tabs.clientWidth);
      }
    }, _this.handleScrollbarSizeChange = function (_ref2) {
      var scrollbarHeight = _ref2.scrollbarHeight;

      _this.setState({
        scrollerStyle: {
          marginBottom: -scrollbarHeight
        }
      });
    }, _this.handleTabsScroll = (0, _debounce2.default)(function () {
      _this.updateScrollButtonState();
    }, 166), _this.moveTabsScroll = function (delta) {
      var theme = _this.props.theme;


      if (_this.tabs) {
        var themeDirection = theme && theme.direction;
        var multiplier = themeDirection === 'rtl' ? -1 : 1;
        var nextScrollLeft = _this.tabs.scrollLeft + delta * multiplier;
        // Fix for Edge
        var invert = themeDirection === 'rtl' && (0, _normalizeScrollLeft.detectScrollType)() === 'reverse' ? -1 : 1;
        _scroll2.default.left(_this.tabs, invert * nextScrollLeft);
      }
    }, _this.scrollSelectedIntoView = function () {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          value = _this$props2.value;


      var themeDirection = theme && theme.direction;

      var _this$getTabsMeta = _this.getTabsMeta(value, themeDirection),
          tabsMeta = _this$getTabsMeta.tabsMeta,
          tabMeta = _this$getTabsMeta.tabMeta;

      if (!tabMeta || !tabsMeta) {
        return;
      }

      if (tabMeta.left < tabsMeta.left) {
        // left side of button is out of view
        var nextScrollLeft = tabsMeta.scrollLeft + (tabMeta.left - tabsMeta.left);
        _scroll2.default.left(_this.tabs, nextScrollLeft);
      } else if (tabMeta.right > tabsMeta.right) {
        // right side of button is out of view
        var _nextScrollLeft = tabsMeta.scrollLeft + (tabMeta.right - tabsMeta.right);
        _scroll2.default.left(_this.tabs, _nextScrollLeft);
      }
    }, _this.updateScrollButtonState = function () {
      var _this$props3 = _this.props,
          scrollable = _this$props3.scrollable,
          scrollButtons = _this$props3.scrollButtons,
          theme = _this$props3.theme;

      var themeDirection = theme && theme.direction;

      if (_this.tabs && scrollable && scrollButtons !== 'off') {
        var _this$tabs = _this.tabs,
            _scrollWidth = _this$tabs.scrollWidth,
            _clientWidth = _this$tabs.clientWidth;

        var _scrollLeft = (0, _normalizeScrollLeft.getNormalizedScrollLeft)(_this.tabs, themeDirection);

        var _showLeftScroll = themeDirection === 'rtl' ? _scrollWidth > _clientWidth + _scrollLeft : _scrollLeft > 0;

        var _showRightScroll = themeDirection === 'rtl' ? _scrollLeft > 0 : _scrollWidth > _clientWidth + _scrollLeft;

        if (_showLeftScroll !== _this.state.showLeftScroll || _showRightScroll !== _this.state.showRightScroll) {
          _this.setState({ showLeftScroll: _showLeftScroll, showRightScroll: _showRightScroll });
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Tabs, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState({ mounted: true });
      this.updateIndicatorState(this.props);
      this.updateScrollButtonState();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      this.updateScrollButtonState();

      // The index might have changed at the same time.
      // We need to check again the right indicator position.
      this.updateIndicatorState(this.props);

      if (this.state.indicatorStyle !== prevState.indicatorStyle) {
        this.scrollSelectedIntoView();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.handleResize.cancel();
      this.handleTabsScroll.cancel();
    }
  }, {
    key: 'updateIndicatorState',
    value: function updateIndicatorState(props) {
      var theme = props.theme,
          value = props.value;


      var themeDirection = theme && theme.direction;

      var _getTabsMeta = this.getTabsMeta(value, themeDirection),
          tabsMeta = _getTabsMeta.tabsMeta,
          tabMeta = _getTabsMeta.tabMeta;

      var left = 0;

      if (tabMeta && tabsMeta) {
        var correction = themeDirection === 'rtl' ? tabsMeta.scrollLeftNormalized + tabsMeta.clientWidth - tabsMeta.scrollWidth : tabsMeta.scrollLeft;
        left = tabMeta.left - tabsMeta.left + correction;
      }

      var indicatorStyle = {
        left: left,
        // May be wrong until the font is loaded.
        width: tabMeta ? tabMeta.width : 0
      };

      if ((indicatorStyle.left !== this.state.indicatorStyle.left || indicatorStyle.width !== this.state.indicatorStyle.width) && !(0, _isNan2.default)(indicatorStyle.left) && !(0, _isNan2.default)(indicatorStyle.width)) {
        this.setState({ indicatorStyle: indicatorStyle });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames3,
          _this2 = this;

      var _props = this.props,
          buttonClassName = _props.buttonClassName,
          centered = _props.centered,
          classes = _props.classes,
          childrenProp = _props.children,
          classNameProp = _props.className,
          fullWidth = _props.fullWidth,
          indicatorClassName = _props.indicatorClassName,
          indicatorColor = _props.indicatorColor,
          onChange = _props.onChange,
          scrollable = _props.scrollable,
          scrollButtons = _props.scrollButtons,
          TabScrollButtonProp = _props.TabScrollButton,
          textColor = _props.textColor,
          theme = _props.theme,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonClassName', 'centered', 'classes', 'children', 'className', 'fullWidth', 'indicatorClassName', 'indicatorColor', 'onChange', 'scrollable', 'scrollButtons', 'TabScrollButton', 'textColor', 'theme', 'value']);


      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var scrollerClassName = (0, _classnames2.default)(classes.scrollingContainer, (_classNames3 = {}, (0, _defineProperty3.default)(_classNames3, classes.fixed, !scrollable), (0, _defineProperty3.default)(_classNames3, classes.scrollable, scrollable), _classNames3));
      var tabItemContainerClassName = (0, _classnames2.default)(classes.flexContainer, (0, _defineProperty3.default)({}, classes.centered, centered && !scrollable));

      var indicator = _react2.default.createElement(_TabIndicator2.default, {
        style: this.state.indicatorStyle,
        className: indicatorClassName,
        color: indicatorColor
      });

      this.valueToIndex = {};
      var childIndex = 0;
      var children = _react2.default.Children.map(childrenProp, function (child) {
        if (!_react2.default.isValidElement(child)) {
          return null;
        }

        var childValue = child.props.value || childIndex;
        _this2.valueToIndex[childValue] = childIndex;
        var selected = childValue === value;

        childIndex += 1;
        return _react2.default.cloneElement(child, {
          fullWidth: fullWidth,
          indicator: selected && !_this2.state.mounted && indicator,
          selected: selected,
          onChange: onChange,
          textColor: textColor,
          value: childValue
        });
      });

      var conditionalElements = this.getConditionalElements();

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }),
        conditionalElements.scrollbarSizeListener,
        _react2.default.createElement(
          'div',
          { className: classes.flexContainer },
          conditionalElements.scrollButtonLeft,
          _react2.default.createElement(
            'div',
            {
              className: scrollerClassName,
              style: this.state.scrollerStyle,
              ref: function ref(node) {
                _this2.tabs = node;
              },
              role: 'tablist',
              onScroll: this.handleTabsScroll
            },
            _react2.default.createElement(
              'div',
              { className: tabItemContainerClassName },
              children
            ),
            this.state.mounted && indicator
          ),
          conditionalElements.scrollButtonRight
        )
      );
    }
  }]);
  return Tabs;
}(_react2.default.Component);

Tabs.defaultProps = {
  centered: false,
  fullWidth: false,
  indicatorColor: 'accent',
  scrollable: false,
  scrollButtons: 'auto',
  TabScrollButton: _TabScrollButton2.default,
  textColor: 'inherit'
};
exports.default = (0, _withStyles2.default)(styles, { withTheme: true, name: 'MuiTabs' })(Tabs);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Tabs/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Tabs = __webpack_require__(/*! ./Tabs */ "./node_modules/material-ui/Tabs/Tabs.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Tabs).default;
  }
});

var _Tab = __webpack_require__(/*! ./Tab */ "./node_modules/material-ui/Tabs/Tab.js");

Object.defineProperty(exports, 'Tab', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Tab).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/pure.js":
/*!*****************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/pure.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/material-ui/node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/material-ui/node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js":
/*!***************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/setDisplayName.js ***!
  \***************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/material-ui/node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/setStatic.js":
/*!**********************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/setStatic.js ***!
  \**********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/shallowEqual.js":
/*!*************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/shallowEqual.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/shouldUpdate.js":
/*!*************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/shouldUpdate.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _react.createFactory)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js":
/*!*****************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z' });

var KeyboardArrowLeft = function KeyboardArrowLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
KeyboardArrowLeft = (0, _pure2.default)(KeyboardArrowLeft);
KeyboardArrowLeft.muiName = 'SvgIcon';

exports.default = KeyboardArrowLeft;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/KeyboardArrowRight.js":
/*!******************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/KeyboardArrowRight.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' });

var KeyboardArrowRight = function KeyboardArrowRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
KeyboardArrowRight = (0, _pure2.default)(KeyboardArrowRight);
KeyboardArrowRight.muiName = 'SvgIcon';

exports.default = KeyboardArrowRight;

/***/ }),

/***/ "./node_modules/normalize-scroll-left/lib/main.js":
/*!********************************************************!*\
  !*** ./node_modules/normalize-scroll-left/lib/main.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// Based on https://github.com/react-bootstrap/dom-helpers/blob/master/src/util/inDOM.js
var inDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);
var cachedType;
function _setScrollType(type) {
    cachedType = type;
}
exports._setScrollType = _setScrollType;
// Based on the jquery plugin https://github.com/othree/jquery.rtl-scroll-type
function detectScrollType() {
    if (cachedType) {
        return cachedType;
    }
    if (!inDOM || !window.document.body) {
        return 'indeterminate';
    }
    var dummy = window.document.createElement('div');
    dummy.appendChild(document.createTextNode('ABCD'));
    dummy.dir = 'rtl';
    dummy.style.fontSize = '14px';
    dummy.style.width = '4px';
    dummy.style.height = '1px';
    dummy.style.position = 'absolute';
    dummy.style.top = '-1000px';
    dummy.style.overflow = 'scroll';
    document.body.appendChild(dummy);
    cachedType = 'reverse';
    if (dummy.scrollLeft > 0) {
        cachedType = 'default';
    }
    else {
        dummy.scrollLeft = 1;
        if (dummy.scrollLeft === 0) {
            cachedType = 'negative';
        }
    }
    document.body.removeChild(dummy);
    return cachedType;
}
exports.detectScrollType = detectScrollType;
// Based on https://stackoverflow.com/a/24394376
function getNormalizedScrollLeft(element, direction) {
    var scrollLeft = element.scrollLeft;
    // Perform the calculations only when direction is rtl to avoid messing up the ltr bahavior
    if (direction !== 'rtl') {
        return scrollLeft;
    }
    var type = detectScrollType();
    if (type === 'indeterminate') {
        return Number.NaN;
    }
    switch (type) {
        case 'negative':
            return element.scrollWidth - element.clientWidth + scrollLeft;
        case 'reverse':
            return element.scrollWidth - element.clientWidth - scrollLeft;
    }
    return scrollLeft;
}
exports.getNormalizedScrollLeft = getNormalizedScrollLeft;
function setNormalizedScrollLeft(element, scrollLeft, direction) {
    // Perform the calculations only when direction is rtl to avoid messing up the ltr bahavior
    if (direction !== 'rtl') {
        element.scrollLeft = scrollLeft;
        return;
    }
    var type = detectScrollType();
    if (type === 'indeterminate') {
        return;
    }
    switch (type) {
        case 'negative':
            element.scrollLeft = element.clientWidth - element.scrollWidth + scrollLeft;
            break;
        case 'reverse':
            element.scrollLeft = element.scrollWidth - element.clientWidth - scrollLeft;
            break;
        default:
            element.scrollLeft = scrollLeft;
            break;
    }
}
exports.setNormalizedScrollLeft = setNormalizedScrollLeft;


/***/ }),

/***/ "./node_modules/rafl/index.js":
/*!************************************!*\
  !*** ./node_modules/rafl/index.js ***!
  \************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(/*! global */ "./node_modules/global/window.js")

/**
 * `requestAnimationFrame()`
 */

var request = global.requestAnimationFrame
  || global.webkitRequestAnimationFrame
  || global.mozRequestAnimationFrame
  || fallback

var prev = +new Date
function fallback (fn) {
  var curr = +new Date
  var ms = Math.max(0, 16 - (curr - prev))
  var req = setTimeout(fn, ms)
  return prev = curr, req
}

/**
 * `cancelAnimationFrame()`
 */

var cancel = global.cancelAnimationFrame
  || global.webkitCancelAnimationFrame
  || global.mozCancelAnimationFrame
  || clearTimeout

if (Function.prototype.bind) {
  request = request.bind(global)
  cancel = cancel.bind(global)
}

exports = module.exports = request
exports.cancel = cancel


/***/ }),

/***/ "./node_modules/react-scrollbar-size/ScrollbarSize.js":
/*!************************************************************!*\
  !*** ./node_modules/react-scrollbar-size/ScrollbarSize.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _stifle = __webpack_require__(/*! stifle */ "./node_modules/stifle/index.js");

var _stifle2 = _interopRequireDefault(_stifle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	width: '100px',
	height: '100px',
	position: 'absolute',
	top: '-100000px',
	overflow: 'scroll',
	msOverflowStyle: 'scrollbar'
};

var ScrollbarSize = function (_Component) {
	(0, _inherits3.default)(ScrollbarSize, _Component);

	function ScrollbarSize() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, ScrollbarSize);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = ScrollbarSize.__proto__ || (0, _getPrototypeOf2.default)(ScrollbarSize)).call.apply(_ref, [this].concat(args))), _this), _this.setMeasurements = function () {
			_this.scrollbarHeight = _this.node.offsetHeight - _this.node.clientHeight;
			_this.scrollbarWidth = _this.node.offsetWidth - _this.node.clientWidth;
		}, _this.handleResize = (0, _stifle2.default)(function () {
			var onChange = _this.props.onChange;


			var prevHeight = _this.scrollbarHeight;
			var prevWidth = _this.scrollbarWidth;
			_this.setMeasurements();
			if (prevHeight !== _this.scrollbarHeight || prevWidth !== _this.scrollbarWidth) {
				onChange({ scrollbarHeight: _this.scrollbarHeight, scrollbarWidth: _this.scrollbarWidth });
			}
		}, 166), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(ScrollbarSize, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var onLoad = this.props.onLoad;


			if (onLoad) {
				this.setMeasurements();
				onLoad({ scrollbarHeight: this.scrollbarHeight, scrollbarWidth: this.scrollbarWidth });
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			this.handleResize.cancel();
		}
	}, {
		key: 'render',
		// Corresponds to 10 frames at 60 Hz.

		value: function render() {
			var _this2 = this;

			var onChange = this.props.onChange;


			return _react2.default.createElement(
				'div',
				null,
				onChange ? _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }) : null,
				_react2.default.createElement('div', {
					style: styles,
					ref: function ref(node) {
						_this2.node = node;
					}
				})
			);
		}
	}]);
	return ScrollbarSize;
}(_react.Component);

ScrollbarSize.defaultProps = {
	onLoad: null,
	onChange: null
};
exports.default = ScrollbarSize;

/***/ }),

/***/ "./node_modules/react-scrollbar-size/index.js":
/*!****************************************************!*\
  !*** ./node_modules/react-scrollbar-size/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ScrollbarSize = __webpack_require__(/*! ./ScrollbarSize */ "./node_modules/react-scrollbar-size/ScrollbarSize.js");

var _ScrollbarSize2 = _interopRequireDefault(_ScrollbarSize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ScrollbarSize2.default;

/***/ }),

/***/ "./node_modules/scroll/index.js":
/*!**************************************!*\
  !*** ./node_modules/scroll/index.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var raf = __webpack_require__(/*! rafl */ "./node_modules/rafl/index.js")

function scroll (prop, element, to, options, callback) {
  var start = +new Date
  var from = element[prop]
  var cancelled = false

  var ease = inOutSine
  var duration = 350

  if (typeof options === 'function') {
    callback = options
  }
  else {
    options = options || {}
    ease = options.ease || ease
    duration = options.duration || duration
    callback = callback || function () {}
  }

  if (from === to) {
    return callback(
      new Error('Element already at target scroll position'),
      element[prop]
    )
  }

  function cancel () {
    cancelled = true
  }

  function animate (timestamp) {
    if (cancelled) {
      return callback(
        new Error('Scroll cancelled'),
        element[prop]
      )
    }

    var now = +new Date
    var time = Math.min(1, ((now - start) / duration))
    var eased = ease(time)

    element[prop] = (eased * (to - from)) + from

    time < 1 ? raf(animate) : raf(function () {
      callback(null, element[prop])
    })
  }

  raf(animate)

  return cancel
}

function inOutSine (n) {
  return .5 * (1 - Math.cos(Math.PI * n))
}

module.exports = {
  top: function (element, to, options, callback) {
    return scroll('scrollTop', element, to, options, callback)
  },
  left: function (element, to, options, callback) {
    return scroll('scrollLeft', element, to, options, callback)
  }
}


/***/ }),

/***/ "./node_modules/stifle/index.js":
/*!**************************************!*\
  !*** ./node_modules/stifle/index.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

module.exports = stifle;


function stifle (fn, wait) {
  if (typeof fn !== 'function' || typeof wait !== 'number') {
    throw new Error('stifle(fn, wait) -- expected a function and number of milliseconds, got (' + typeof fn + ', ' + typeof wait + ')');
  }

  var timer;    // Timer to fire after `wait` has elapsed
  var called;   // Keep track if it gets called during the `wait`

  var wrapper = function () {

    // Check if still "cooling down" from a previous call
    if (timer) {
      called = true;
    } else {
      // Start a timer to fire after the `wait` is over
      timer = setTimeout(afterWait, wait);
      // And call the wrapped function
      fn();
    }
  }

  // Add a cancel method, to kill and pending calls
  wrapper.cancel = function () {
    // Clear the called flag, or it would fire twice when called again later
    called = false;

    // Turn off the timer, so it won't fire after the wait expires
    if (timer) {
      clearTimeout(timer);
      timer = undefined;
    }
  }

  function afterWait() {
    // Empty out the timer
    timer = undefined;

    // If it was called during the `wait`, fire it again
    if (called) {
      called = false;
      wrapper();
    }
  }

  return wrapper;
}


/***/ }),

/***/ "./src/client/containers/Search/index.js":
/*!***********************************************!*\
  !*** ./src/client/containers/Search/index.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _AppBar = __webpack_require__(/*! material-ui/AppBar */ "./node_modules/material-ui/AppBar/index.js");

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Tabs = __webpack_require__(/*! material-ui/Tabs */ "./node_modules/material-ui/Tabs/index.js");

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import actions from '../../actions';

/** @format */ /**
               * search component
               * This component filters all the posts and courses and element
               * and display in different tabs
               *
               * @format
               */

var styles = {
  root: {
    flex: '1 0 100%'
  }
};

// for displaying Element
var ElementCard = function ElementCard(_ref) {
  var type = _ref.type,
      elementType = _ref.elementType,
      id = _ref.id,
      username = _ref.username,
      name = _ref.name,
      guru = _ref.guru,
      classroom = _ref.classroom;
  return _react2.default.createElement(
    'div',
    { style: { padding: '1%' } },
    _react2.default.createElement(
      _Card2.default,
      { raised: true },
      _react2.default.createElement(_Card.CardHeader, { title: name, subheader: '@' + username })
    )
  );
};

ElementCard.propTypes = {
  type: _propTypes2.default.string.isRequired,
  elementType: _propTypes2.default.string.isRequired,
  id: _propTypes2.default.number.isRequired,
  username: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  guru: _propTypes2.default.bool.isRequired,
  classroom: _propTypes2.default.bool.isRequired
};

// for displaying posts
var PostCard = function PostCard(_ref2) {
  var type = _ref2.type,
      postType = _ref2.postType,
      postId = _ref2.postId,
      title = _ref2.title,
      description = _ref2.description;
  return _react2.default.createElement(
    'div',
    { style: { padding: '1%' } },
    _react2.default.createElement(
      _Card2.default,
      { raised: true },
      _react2.default.createElement(_Card.CardHeader, { title: title, subheader: description })
    )
  );
};

PostCard.propTypes = {
  type: _propTypes2.default.string.isRequired,
  postType: _propTypes2.default.string.isRequired,
  postId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  description: _propTypes2.default.string.isRequired
};

// for displaying course
var CourseCard = function CourseCard(_ref3) {
  var type = _ref3.type,
      postType = _ref3.postType,
      courseId = _ref3.courseId,
      title = _ref3.title,
      description = _ref3.description;
  return _react2.default.createElement(
    'div',
    { style: { padding: '1%' } },
    _react2.default.createElement(
      _Card2.default,
      { raised: true },
      _react2.default.createElement(_Card.CardHeader, { title: title, subheader: description })
    )
  );
};

CourseCard.propTypes = {
  type: _propTypes2.default.string.isRequired,
  postType: _propTypes2.default.string.isRequired,
  courseId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  description: _propTypes2.default.string.isRequired
};

// for displaying Timeline (Timeline contains post, course, element)
var Timeline = function Timeline(_ref4) {
  var data = _ref4.data;
  return _react2.default.createElement(
    'div',
    null,
    data.map(function (e, i) {
      if (e.type === 'element') {
        return _react2.default.createElement(ElementCard, (0, _extends3.default)({ key: e.id }, e));
      }
      if (e.type === 'post') {
        return _react2.default.createElement(PostCard, (0, _extends3.default)({ key: e.postId }, e));
      }
      if (e.type === 'course') {
        return _react2.default.createElement(PostCard, (0, _extends3.default)({ key: e.courseId }, e));
      }
      return _react2.default.createElement(
        'div',
        { key: i + 1 },
        e.type
      );
    })
  );
};

Timeline.propTypes = {
  data: _propTypes2.default.array.isRequired
};

var Search = function (_Component) {
  (0, _inherits3.default)(Search, _Component);

  function Search(props) {
    (0, _classCallCheck3.default)(this, Search);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Search.__proto__ || (0, _getPrototypeOf2.default)(Search)).call(this, props));

    _this.state = {
      value: 0,
      data: [{
        type: 'element',
        elementType: 'user',
        id: 1,
        username: 'hbarve1',
        name: 'Himank Barve',
        guru: true,
        classroom: true
      }, {
        type: 'element',
        elementType: 'user',
        id: 2,
        username: 'shubham',
        name: 'Shubham Maurya',
        guru: true
      }, {
        type: 'element',
        elementType: 'user',
        id: 3,
        username: 'deepansh',
        name: 'Deepansh Bhargawa'
      }, {
        type: 'element',
        elementType: 'circle',
        circleType: 'edu',
        id: 4,
        username: 'iitdhn',
        name: 'Indian Institute of Technology(ISM), Dhanbad',
        classroom: true
      }, {
        type: 'element',
        elementType: 'circle',
        circleType: 'field',
        id: 5,
        username: 'javascript',
        name: 'JavaScript',
        classroom: true
      }, {
        type: 'element',
        elementType: 'circle',
        circleType: 'org',
        id: 6,
        username: 'google',
        name: 'Google Inc.',
        classroom: true
      }, {
        type: 'post',
        postType: 'article',
        postId: 1,
        authorId: 1,
        title: 'This is First Post',
        description: 'This is First Post description.'
      }, {
        type: 'post',
        postType: 'article',
        postId: 2,
        authorId: 1,
        title: 'This is Second Post',
        description: 'This is Second Post description.'
      }, {
        type: 'course',
        courseId: 1,
        authorId: 1,
        title: 'This is First Course',
        description: 'This is First Course description.'
      }, {
        type: 'course',
        courseId: 2,
        authorId: 1,
        title: 'This is Second Course',
        description: 'This is First Course description.'
      }]
    };
    return _this;
  }

  (0, _createClass3.default)(Search, [{
    key: 'handleChange',
    value: function handleChange(event, value) {
      this.setState({ value: value });
    }
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var _state = this.state,
          value = _state.value,
          data = _state.data;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _AppBar2.default,
          { position: 'static', color: 'default' },
          _react2.default.createElement(
            _Tabs2.default,
            {
              value: value,
              onChange: this.handleChange,
              indicatorColor: 'primary',
              textColor: 'primary',
              scrollable: true,
              scrollButtons: 'auto'
            },
            _react2.default.createElement(_Tabs.Tab, { label: 'All' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Users' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Circles' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Posts' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Courses' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Classrooms' })
          )
        ),
        value === 0 && _react2.default.createElement(Timeline, { data: data }),
        value === 1 && _react2.default.createElement(Timeline, {
          data: data.filter(function (d) {
            return d.type === 'element' && d.elementType === 'user';
          })
        }),
        value === 2 && _react2.default.createElement(Timeline, {
          data: data.filter(function (d) {
            return d.type === 'element' && d.elementType === 'circle';
          })
        }),
        value === 3 && _react2.default.createElement(Timeline, { data: data.filter(function (d) {
            return d.type === 'post';
          }) }),
        value === 4 && _react2.default.createElement(Timeline, { data: data.filter(function (d) {
            return d.type === 'course';
          }) }),
        value === 5 && _react2.default.createElement(Timeline, {
          data: data.filter(function (d) {
            return d.type === 'element' && d.classroom === true;
          })
        })
      );
    }
  }]);
  return Search;
}(_react.Component);

Search.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

var mapStateToProps = function mapStateToProps(_ref5) {
  var elements = _ref5.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({}, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(Search));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL0FwcEJhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiSW5kaWNhdG9yLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYlNjcm9sbEJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9LZXlib2FyZEFycm93TGVmdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dSaWdodC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLXNjcm9sbC1sZWZ0L2xpYi9tYWluLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yYWZsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9TY3JvbGxiYXJTaXplLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc2Nyb2xsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9zdGlmbGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1NlYXJjaC9pbmRleC5qcyJdLCJuYW1lcyI6WyJzdHlsZXMiLCJyb290IiwiZmxleCIsIkVsZW1lbnRDYXJkIiwidHlwZSIsImVsZW1lbnRUeXBlIiwiaWQiLCJ1c2VybmFtZSIsIm5hbWUiLCJndXJ1IiwiY2xhc3Nyb29tIiwicGFkZGluZyIsInByb3BUeXBlcyIsInN0cmluZyIsImlzUmVxdWlyZWQiLCJudW1iZXIiLCJib29sIiwiUG9zdENhcmQiLCJwb3N0VHlwZSIsInBvc3RJZCIsInRpdGxlIiwiZGVzY3JpcHRpb24iLCJDb3Vyc2VDYXJkIiwiY291cnNlSWQiLCJUaW1lbGluZSIsImRhdGEiLCJtYXAiLCJlIiwiaSIsImFycmF5IiwiU2VhcmNoIiwicHJvcHMiLCJzdGF0ZSIsInZhbHVlIiwiY2lyY2xlVHlwZSIsImF1dGhvcklkIiwiZXZlbnQiLCJzZXRTdGF0ZSIsImNsYXNzZXMiLCJoYW5kbGVDaGFuZ2UiLCJmaWx0ZXIiLCJkIiwib2JqZWN0IiwibWFwU3RhdGVUb1Byb3BzIiwiZWxlbWVudHMiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJkaXNwYXRjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxvSkFBb0o7O0FBRXBKO0FBQ0E7QUFDQSxnQ0FBZ0Msd0VBQXdFO0FBQ3hHO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUM1S3pFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxnSEFBZ0g7O0FBRWhIO0FBQ0E7QUFDQSxnQ0FBZ0MsOENBQThDO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsa0JBQWtCLFE7Ozs7Ozs7Ozs7Ozs7QUM5SXZFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4Rjs7QUFFOUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxxQkFBcUIsVzs7Ozs7Ozs7Ozs7OztBQ2xMMUU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxtQ0FBbUM7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0wsa0RBQWtEO0FBQ2xEO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLDBCQUEwQiw0QkFBNEI7QUFDdEQ7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsV0FBVyxvQ0FBb0M7QUFDL0M7QUFDQTtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnRkFBZ0Y7O0FBRWhGOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSwrRUFBK0U7O0FBRS9FO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVyw2QkFBNkI7QUFDeEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsaUJBQWlCLE87Ozs7Ozs7Ozs7Ozs7QUN4V3RFOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsU0FBUzs7QUFFVDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLDBGQUEwRjs7QUFFMUYscUVBQXFFO0FBQ3JFO0FBQ0EsR0FBRzs7QUFFSCwrQ0FBK0MscUNBQXFDO0FBQ3BGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBLENBQUM7QUFDRCxxREFBcUQsMEJBQTBCLGdCOzs7Ozs7Ozs7Ozs7O0FDaEgvRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBLHFEQUFxRCx1QkFBdUI7QUFDNUU7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1REFBdUQ7QUFDdkY7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsNkJBQTZCLG1COzs7Ozs7Ozs7Ozs7O0FDakpsRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsZ0RBQWdEO0FBQ2hEO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0Esd0JBQXdCO0FBQ3hCO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZFQUE2RTtBQUM3RSxPQUFPOztBQUVQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkVBQTZFO0FBQzdFLE9BQU87O0FBRVA7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZCxLQUFLLGlEQUFpRDtBQUN0RDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBLDBCQUEwQixxRUFBcUU7QUFDL0Y7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixnQkFBZ0I7QUFDckM7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSx1QkFBdUIsaUNBQWlDO0FBQ3hEO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQSxzR0FBc0c7QUFDdEcsdUhBQXVIOztBQUV2SDtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87O0FBRVA7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQscUVBQXFFLGdEQUFnRDtBQUNySDtBQUNBO0FBQ0E7QUFDQSxXQUFXLG1DQUFtQztBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGVBQWUsdUNBQXVDO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxtQ0FBbUMsUTs7Ozs7Ozs7Ozs7OztBQ2ppQnhGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUN4QjdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDckRBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxrREFBa0QsMERBQTBEOztBQUU1RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsb0M7Ozs7Ozs7Ozs7Ozs7QUNuQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCx3REFBd0Q7O0FBRTFHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ25DQTtBQUNBLDhDQUE4QyxjQUFjO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbkZBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLGlFQUFpRSxhQUFhO0FBQzlFO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWMsK0VBQStFO0FBQzdGO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsWUFBWSw2RUFBNkU7QUFDekY7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDRFQUE0RSxnREFBZ0Q7QUFDNUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdDOzs7Ozs7Ozs7Ozs7O0FDaklBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RiwwQzs7Ozs7Ozs7Ozs7O0FDWkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFlBQVk7QUFDWixhQUFhOztBQUViOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4Q0E7Ozs7QUFDQTs7OztBQUNBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQTs7QUFsQkEsYyxDQUFlOzs7Ozs7OztBQW9CZixJQUFNQSxTQUFTO0FBQ2JDLFFBQU07QUFDSkMsVUFBTTtBQURGO0FBRE8sQ0FBZjs7QUFNQTtBQUNBLElBQU1DLGNBQWMsU0FBZEEsV0FBYztBQUFBLE1BQ2xCQyxJQURrQixRQUNsQkEsSUFEa0I7QUFBQSxNQUVsQkMsV0FGa0IsUUFFbEJBLFdBRmtCO0FBQUEsTUFHbEJDLEVBSGtCLFFBR2xCQSxFQUhrQjtBQUFBLE1BSWxCQyxRQUprQixRQUlsQkEsUUFKa0I7QUFBQSxNQUtsQkMsSUFMa0IsUUFLbEJBLElBTGtCO0FBQUEsTUFNbEJDLElBTmtCLFFBTWxCQSxJQU5rQjtBQUFBLE1BT2xCQyxTQVBrQixRQU9sQkEsU0FQa0I7QUFBQSxTQVNsQjtBQUFBO0FBQUEsTUFBSyxPQUFPLEVBQUVDLFNBQVMsSUFBWCxFQUFaO0FBQ0U7QUFBQTtBQUFBLFFBQU0sWUFBTjtBQUNFLHdEQUFZLE9BQU9ILElBQW5CLEVBQXlCLGlCQUFlRCxRQUF4QztBQURGO0FBREYsR0FUa0I7QUFBQSxDQUFwQjs7QUFnQkFKLFlBQVlTLFNBQVosR0FBd0I7QUFDdEJSLFFBQU0sb0JBQVVTLE1BQVYsQ0FBaUJDLFVBREQ7QUFFdEJULGVBQWEsb0JBQVVRLE1BQVYsQ0FBaUJDLFVBRlI7QUFHdEJSLE1BQUksb0JBQVVTLE1BQVYsQ0FBaUJELFVBSEM7QUFJdEJQLFlBQVUsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBSkw7QUFLdEJOLFFBQU0sb0JBQVVLLE1BQVYsQ0FBaUJDLFVBTEQ7QUFNdEJMLFFBQU0sb0JBQVVPLElBQVYsQ0FBZUYsVUFOQztBQU90QkosYUFBVyxvQkFBVU0sSUFBVixDQUFlRjtBQVBKLENBQXhCOztBQVVBO0FBQ0EsSUFBTUcsV0FBVyxTQUFYQSxRQUFXO0FBQUEsTUFBR2IsSUFBSCxTQUFHQSxJQUFIO0FBQUEsTUFBU2MsUUFBVCxTQUFTQSxRQUFUO0FBQUEsTUFBbUJDLE1BQW5CLFNBQW1CQSxNQUFuQjtBQUFBLE1BQTJCQyxLQUEzQixTQUEyQkEsS0FBM0I7QUFBQSxNQUFrQ0MsV0FBbEMsU0FBa0NBLFdBQWxDO0FBQUEsU0FDZjtBQUFBO0FBQUEsTUFBSyxPQUFPLEVBQUVWLFNBQVMsSUFBWCxFQUFaO0FBQ0U7QUFBQTtBQUFBLFFBQU0sWUFBTjtBQUNFLHdEQUFZLE9BQU9TLEtBQW5CLEVBQTBCLFdBQVdDLFdBQXJDO0FBREY7QUFERixHQURlO0FBQUEsQ0FBakI7O0FBUUFKLFNBQVNMLFNBQVQsR0FBcUI7QUFDbkJSLFFBQU0sb0JBQVVTLE1BQVYsQ0FBaUJDLFVBREo7QUFFbkJJLFlBQVUsb0JBQVVMLE1BQVYsQ0FBaUJDLFVBRlI7QUFHbkJLLFVBQVEsb0JBQVVKLE1BQVYsQ0FBaUJELFVBSE47QUFJbkJNLFNBQU8sb0JBQVVQLE1BQVYsQ0FBaUJDLFVBSkw7QUFLbkJPLGVBQWEsb0JBQVVSLE1BQVYsQ0FBaUJDO0FBTFgsQ0FBckI7O0FBUUE7QUFDQSxJQUFNUSxhQUFhLFNBQWJBLFVBQWE7QUFBQSxNQUFHbEIsSUFBSCxTQUFHQSxJQUFIO0FBQUEsTUFBU2MsUUFBVCxTQUFTQSxRQUFUO0FBQUEsTUFBbUJLLFFBQW5CLFNBQW1CQSxRQUFuQjtBQUFBLE1BQTZCSCxLQUE3QixTQUE2QkEsS0FBN0I7QUFBQSxNQUFvQ0MsV0FBcEMsU0FBb0NBLFdBQXBDO0FBQUEsU0FDakI7QUFBQTtBQUFBLE1BQUssT0FBTyxFQUFFVixTQUFTLElBQVgsRUFBWjtBQUNFO0FBQUE7QUFBQSxRQUFNLFlBQU47QUFDRSx3REFBWSxPQUFPUyxLQUFuQixFQUEwQixXQUFXQyxXQUFyQztBQURGO0FBREYsR0FEaUI7QUFBQSxDQUFuQjs7QUFRQUMsV0FBV1YsU0FBWCxHQUF1QjtBQUNyQlIsUUFBTSxvQkFBVVMsTUFBVixDQUFpQkMsVUFERjtBQUVyQkksWUFBVSxvQkFBVUwsTUFBVixDQUFpQkMsVUFGTjtBQUdyQlMsWUFBVSxvQkFBVVIsTUFBVixDQUFpQkQsVUFITjtBQUlyQk0sU0FBTyxvQkFBVVAsTUFBVixDQUFpQkMsVUFKSDtBQUtyQk8sZUFBYSxvQkFBVVIsTUFBVixDQUFpQkM7QUFMVCxDQUF2Qjs7QUFRQTtBQUNBLElBQU1VLFdBQVcsU0FBWEEsUUFBVztBQUFBLE1BQUdDLElBQUgsU0FBR0EsSUFBSDtBQUFBLFNBQ2Y7QUFBQTtBQUFBO0FBQ0dBLFNBQUtDLEdBQUwsQ0FBUyxVQUFDQyxDQUFELEVBQUlDLENBQUosRUFBVTtBQUNsQixVQUFJRCxFQUFFdkIsSUFBRixLQUFXLFNBQWYsRUFBMEI7QUFDeEIsZUFBTyw4QkFBQyxXQUFELDJCQUFhLEtBQUt1QixFQUFFckIsRUFBcEIsSUFBNEJxQixDQUE1QixFQUFQO0FBQ0Q7QUFDRCxVQUFJQSxFQUFFdkIsSUFBRixLQUFXLE1BQWYsRUFBdUI7QUFDckIsZUFBTyw4QkFBQyxRQUFELDJCQUFVLEtBQUt1QixFQUFFUixNQUFqQixJQUE2QlEsQ0FBN0IsRUFBUDtBQUNEO0FBQ0QsVUFBSUEsRUFBRXZCLElBQUYsS0FBVyxRQUFmLEVBQXlCO0FBQ3ZCLGVBQU8sOEJBQUMsUUFBRCwyQkFBVSxLQUFLdUIsRUFBRUosUUFBakIsSUFBK0JJLENBQS9CLEVBQVA7QUFDRDtBQUNELGFBQU87QUFBQTtBQUFBLFVBQUssS0FBS0MsSUFBSSxDQUFkO0FBQWtCRCxVQUFFdkI7QUFBcEIsT0FBUDtBQUNELEtBWEE7QUFESCxHQURlO0FBQUEsQ0FBakI7O0FBaUJBb0IsU0FBU1osU0FBVCxHQUFxQjtBQUNuQmEsUUFBTSxvQkFBVUksS0FBVixDQUFnQmY7QUFESCxDQUFyQjs7SUFJTWdCLE07OztBQUNKLGtCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYTtBQUNYQyxhQUFPLENBREk7QUFFWFIsWUFBTSxDQUNKO0FBQ0VyQixjQUFNLFNBRFI7QUFFRUMscUJBQWEsTUFGZjtBQUdFQyxZQUFJLENBSE47QUFJRUMsa0JBQVUsU0FKWjtBQUtFQyxjQUFNLGNBTFI7QUFNRUMsY0FBTSxJQU5SO0FBT0VDLG1CQUFXO0FBUGIsT0FESSxFQVVKO0FBQ0VOLGNBQU0sU0FEUjtBQUVFQyxxQkFBYSxNQUZmO0FBR0VDLFlBQUksQ0FITjtBQUlFQyxrQkFBVSxTQUpaO0FBS0VDLGNBQU0sZ0JBTFI7QUFNRUMsY0FBTTtBQU5SLE9BVkksRUFrQko7QUFDRUwsY0FBTSxTQURSO0FBRUVDLHFCQUFhLE1BRmY7QUFHRUMsWUFBSSxDQUhOO0FBSUVDLGtCQUFVLFVBSlo7QUFLRUMsY0FBTTtBQUxSLE9BbEJJLEVBeUJKO0FBQ0VKLGNBQU0sU0FEUjtBQUVFQyxxQkFBYSxRQUZmO0FBR0U2QixvQkFBWSxLQUhkO0FBSUU1QixZQUFJLENBSk47QUFLRUMsa0JBQVUsUUFMWjtBQU1FQyxjQUFNLDhDQU5SO0FBT0VFLG1CQUFXO0FBUGIsT0F6QkksRUFrQ0o7QUFDRU4sY0FBTSxTQURSO0FBRUVDLHFCQUFhLFFBRmY7QUFHRTZCLG9CQUFZLE9BSGQ7QUFJRTVCLFlBQUksQ0FKTjtBQUtFQyxrQkFBVSxZQUxaO0FBTUVDLGNBQU0sWUFOUjtBQU9FRSxtQkFBVztBQVBiLE9BbENJLEVBMkNKO0FBQ0VOLGNBQU0sU0FEUjtBQUVFQyxxQkFBYSxRQUZmO0FBR0U2QixvQkFBWSxLQUhkO0FBSUU1QixZQUFJLENBSk47QUFLRUMsa0JBQVUsUUFMWjtBQU1FQyxjQUFNLGFBTlI7QUFPRUUsbUJBQVc7QUFQYixPQTNDSSxFQW9ESjtBQUNFTixjQUFNLE1BRFI7QUFFRWMsa0JBQVUsU0FGWjtBQUdFQyxnQkFBUSxDQUhWO0FBSUVnQixrQkFBVSxDQUpaO0FBS0VmLGVBQU8sb0JBTFQ7QUFNRUMscUJBQWE7QUFOZixPQXBESSxFQTRESjtBQUNFakIsY0FBTSxNQURSO0FBRUVjLGtCQUFVLFNBRlo7QUFHRUMsZ0JBQVEsQ0FIVjtBQUlFZ0Isa0JBQVUsQ0FKWjtBQUtFZixlQUFPLHFCQUxUO0FBTUVDLHFCQUFhO0FBTmYsT0E1REksRUFvRUo7QUFDRWpCLGNBQU0sUUFEUjtBQUVFbUIsa0JBQVUsQ0FGWjtBQUdFWSxrQkFBVSxDQUhaO0FBSUVmLGVBQU8sc0JBSlQ7QUFLRUMscUJBQWE7QUFMZixPQXBFSSxFQTJFSjtBQUNFakIsY0FBTSxRQURSO0FBRUVtQixrQkFBVSxDQUZaO0FBR0VZLGtCQUFVLENBSFo7QUFJRWYsZUFBTyx1QkFKVDtBQUtFQyxxQkFBYTtBQUxmLE9BM0VJO0FBRkssS0FBYjtBQUZpQjtBQXdGbEI7Ozs7aUNBRVllLEssRUFBT0gsSyxFQUFPO0FBQ3pCLFdBQUtJLFFBQUwsQ0FBYyxFQUFFSixZQUFGLEVBQWQ7QUFDRDs7OzZCQUVRO0FBQUEsVUFDQ0ssT0FERCxHQUNhLEtBQUtQLEtBRGxCLENBQ0NPLE9BREQ7QUFBQSxtQkFHaUIsS0FBS04sS0FIdEI7QUFBQSxVQUdDQyxLQUhELFVBR0NBLEtBSEQ7QUFBQSxVQUdRUixJQUhSLFVBR1FBLElBSFI7OztBQUtQLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBV2EsUUFBUXJDLElBQXhCO0FBQ0U7QUFBQTtBQUFBLFlBQVEsVUFBUyxRQUFqQixFQUEwQixPQUFNLFNBQWhDO0FBQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQU9nQyxLQURUO0FBRUUsd0JBQVUsS0FBS00sWUFGakI7QUFHRSw4QkFBZSxTQUhqQjtBQUlFLHlCQUFVLFNBSlo7QUFLRSw4QkFMRjtBQU1FLDZCQUFjO0FBTmhCO0FBUUUsdURBQUssT0FBTSxLQUFYLEdBUkY7QUFTRSx1REFBSyxPQUFNLE9BQVgsR0FURjtBQVVFLHVEQUFLLE9BQU0sU0FBWCxHQVZGO0FBV0UsdURBQUssT0FBTSxPQUFYLEdBWEY7QUFZRSx1REFBSyxPQUFNLFNBQVgsR0FaRjtBQWFFLHVEQUFLLE9BQU0sWUFBWDtBQWJGO0FBREYsU0FERjtBQW1CR04sa0JBQVUsQ0FBVixJQUFlLDhCQUFDLFFBQUQsSUFBVSxNQUFNUixJQUFoQixHQW5CbEI7QUFxQkdRLGtCQUFVLENBQVYsSUFDQyw4QkFBQyxRQUFEO0FBQ0UsZ0JBQU1SLEtBQUtlLE1BQUwsQ0FDSixVQUFDQyxDQUFEO0FBQUEsbUJBQU9BLEVBQUVyQyxJQUFGLEtBQVcsU0FBWCxJQUF3QnFDLEVBQUVwQyxXQUFGLEtBQWtCLE1BQWpEO0FBQUEsV0FESTtBQURSLFVBdEJKO0FBNEJHNEIsa0JBQVUsQ0FBVixJQUNDLDhCQUFDLFFBQUQ7QUFDRSxnQkFBTVIsS0FBS2UsTUFBTCxDQUNKLFVBQUNDLENBQUQ7QUFBQSxtQkFBT0EsRUFBRXJDLElBQUYsS0FBVyxTQUFYLElBQXdCcUMsRUFBRXBDLFdBQUYsS0FBa0IsUUFBakQ7QUFBQSxXQURJO0FBRFIsVUE3Qko7QUFtQ0c0QixrQkFBVSxDQUFWLElBQ0MsOEJBQUMsUUFBRCxJQUFVLE1BQU1SLEtBQUtlLE1BQUwsQ0FBWSxVQUFDQyxDQUFEO0FBQUEsbUJBQU9BLEVBQUVyQyxJQUFGLEtBQVcsTUFBbEI7QUFBQSxXQUFaLENBQWhCLEdBcENKO0FBc0NHNkIsa0JBQVUsQ0FBVixJQUNDLDhCQUFDLFFBQUQsSUFBVSxNQUFNUixLQUFLZSxNQUFMLENBQVksVUFBQ0MsQ0FBRDtBQUFBLG1CQUFPQSxFQUFFckMsSUFBRixLQUFXLFFBQWxCO0FBQUEsV0FBWixDQUFoQixHQXZDSjtBQXlDRzZCLGtCQUFVLENBQVYsSUFDQyw4QkFBQyxRQUFEO0FBQ0UsZ0JBQU1SLEtBQUtlLE1BQUwsQ0FDSixVQUFDQyxDQUFEO0FBQUEsbUJBQU9BLEVBQUVyQyxJQUFGLEtBQVcsU0FBWCxJQUF3QnFDLEVBQUUvQixTQUFGLEtBQWdCLElBQS9DO0FBQUEsV0FESTtBQURSO0FBMUNKLE9BREY7QUFtREQ7Ozs7O0FBR0hvQixPQUFPbEIsU0FBUCxHQUFtQjtBQUNqQjBCLFdBQVMsb0JBQVVJLE1BQVYsQ0FBaUI1QjtBQURULENBQW5COztBQUlBLElBQU02QixrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR0MsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBbUIsRUFBRUEsa0JBQUYsRUFBbkI7QUFBQSxDQUF4Qjs7QUFFQSxJQUFNQyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FBYywrQkFBbUIsRUFBbkIsRUFBdUJBLFFBQXZCLENBQWQ7QUFBQSxDQUEzQjs7a0JBRWUseUJBQVFILGVBQVIsRUFBeUJFLGtCQUF6QixFQUNiLDBCQUFXN0MsTUFBWCxFQUFtQjhCLE1BQW5CLENBRGEsQyIsImZpbGUiOiI5LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9QYXBlciA9IHJlcXVpcmUoJy4uL1BhcGVyJyk7XG5cbnZhciBfUGFwZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUGFwZXIpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBQYXBlclxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBib3hTaXppbmc6ICdib3JkZXItYm94JywgLy8gUHJldmVudCBwYWRkaW5nIGlzc3VlIHdpdGggdGhlIE1vZGFsIGFuZCBmaXhlZCBwb3NpdGlvbmVkIEFwcEJhci5cbiAgICAgIHpJbmRleDogdGhlbWUuekluZGV4LmFwcEJhcixcbiAgICAgIGZsZXhTaHJpbms6IDBcbiAgICB9LFxuICAgIHBvc2l0aW9uRml4ZWQ6IHtcbiAgICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgICAgdG9wOiAwLFxuICAgICAgbGVmdDogJ2F1dG8nLFxuICAgICAgcmlnaHQ6IDBcbiAgICB9LFxuICAgIHBvc2l0aW9uQWJzb2x1dGU6IHtcbiAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgdG9wOiAwLFxuICAgICAgbGVmdDogJ2F1dG8nLFxuICAgICAgcmlnaHQ6IDBcbiAgICB9LFxuICAgIHBvc2l0aW9uU3RhdGljOiB7XG4gICAgICBwb3NpdGlvbjogJ3N0YXRpYycsXG4gICAgICBmbGV4U2hyaW5rOiAwXG4gICAgfSxcbiAgICBjb2xvckRlZmF1bHQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmFwcEJhcixcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuYXBwQmFyKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMCxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwKVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db2xvciA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAncHJpbWFyeScsICdhY2NlbnQnLCAnZGVmYXVsdCddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Bvc2l0aW9uID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnc3RhdGljJywgJ2ZpeGVkJywgJ2Fic29sdXRlJ10pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdwcmltYXJ5JywgJ2FjY2VudCcsICdkZWZhdWx0J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBwb3NpdGlvbmluZyB0eXBlLlxuICAgKi9cbiAgcG9zaXRpb246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3N0YXRpYycsICdmaXhlZCcsICdhYnNvbHV0ZSddKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgQXBwQmFyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoQXBwQmFyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBBcHBCYXIoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQXBwQmFyKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoQXBwQmFyLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShBcHBCYXIpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEFwcEJhciwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBwb3NpdGlvbiA9IF9wcm9wcy5wb3NpdGlvbixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICdwb3NpdGlvbiddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzZXNbJ3Bvc2l0aW9uJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKHBvc2l0aW9uKV0sIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCAnbXVpLWZpeGVkJywgcG9zaXRpb24gPT09ICdmaXhlZCcpLCBfY2xhc3NOYW1lcyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9QYXBlcjIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IHNxdWFyZTogdHJ1ZSwgY29tcG9uZW50OiAnaGVhZGVyJywgZWxldmF0aW9uOiA0LCBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQXBwQmFyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQXBwQmFyLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdwcmltYXJ5JyxcbiAgcG9zaXRpb246ICdmaXhlZCdcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQXBwQmFyJyB9KShBcHBCYXIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0FwcEJhci9BcHBCYXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0FwcEJhci9BcHBCYXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfQXBwQmFyID0gcmVxdWlyZSgnLi9BcHBCYXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQXBwQmFyKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0FwcEJhci9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQWN0aW9uOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGNvbG9yRXJyb3I6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmVycm9yWzUwMF1cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbG9yID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBpY29uIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgSWNvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEljb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEljb24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgSWNvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEljb24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEljb24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEljb24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCdtYXRlcmlhbC1pY29ucycsIGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnc3BhbicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSwgJ2FyaWEtaGlkZGVuJzogJ3RydWUnIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnaW5oZXJpdCdcbn07XG5JY29uLm11aU5hbWUgPSAnSWNvbic7XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpSWNvbicgfSkoSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9JY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA2IDcgOCA5IDM0IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuL0ljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDYgNyA4IDkgMzQgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdpbmxpbmUtYmxvY2snLFxuICAgICAgZmlsbDogJ2N1cnJlbnRDb2xvcicsXG4gICAgICBoZWlnaHQ6IDI0LFxuICAgICAgd2lkdGg6IDI0LFxuICAgICAgdXNlclNlbGVjdDogJ25vbmUnLFxuICAgICAgZmxleFNocmluazogMCxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnZmlsbCcsIHtcbiAgICAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXJcbiAgICAgIH0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQWN0aW9uOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGNvbG9yRXJyb3I6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmVycm9yWzUwMF1cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbG9yID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIEVsZW1lbnRzIHBhc3NlZCBpbnRvIHRoZSBTVkcgSWNvbi5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvdmlkZXMgYSBodW1hbi1yZWFkYWJsZSB0aXRsZSBmb3IgdGhlIGVsZW1lbnQgdGhhdCBjb250YWlucyBpdC5cbiAgICogaHR0cHM6Ly93d3cudzMub3JnL1RSL1NWRy1hY2Nlc3MvI0VxdWl2YWxlbnRcbiAgICovXG4gIHRpdGxlQWNjZXNzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBBbGxvd3MgeW91IHRvIHJlZGVmaW5lIHdoYXQgdGhlIGNvb3JkaW5hdGVzIHdpdGhvdXQgdW5pdHMgbWVhbiBpbnNpZGUgYW4gc3ZnIGVsZW1lbnQuXG4gICAqIEZvciBleGFtcGxlLCBpZiB0aGUgU1ZHIGVsZW1lbnQgaXMgNTAwICh3aWR0aCkgYnkgMjAwIChoZWlnaHQpLFxuICAgKiBhbmQgeW91IHBhc3Mgdmlld0JveD1cIjAgMCA1MCAyMFwiLFxuICAgKiB0aGlzIG1lYW5zIHRoYXQgdGhlIGNvb3JkaW5hdGVzIGluc2lkZSB0aGUgc3ZnIHdpbGwgZ28gZnJvbSB0aGUgdG9wIGxlZnQgY29ybmVyICgwLDApXG4gICAqIHRvIGJvdHRvbSByaWdodCAoNTAsMjApIGFuZCBlYWNoIHVuaXQgd2lsbCBiZSB3b3J0aCAxMHB4LlxuICAgKi9cbiAgdmlld0JveDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZy5pc1JlcXVpcmVkXG59O1xuXG52YXIgU3ZnSWNvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFN2Z0ljb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFN2Z0ljb24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgU3ZnSWNvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKFN2Z0ljb24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFN2Z0ljb24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFN2Z0ljb24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIHRpdGxlQWNjZXNzID0gX3Byb3BzLnRpdGxlQWNjZXNzLFxuICAgICAgICAgIHZpZXdCb3ggPSBfcHJvcHMudmlld0JveCxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICd0aXRsZUFjY2VzcycsICd2aWV3Qm94J10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnc3ZnJyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgICAgZm9jdXNhYmxlOiAnZmFsc2UnLFxuICAgICAgICAgIHZpZXdCb3g6IHZpZXdCb3gsXG4gICAgICAgICAgJ2FyaWEtaGlkZGVuJzogdGl0bGVBY2Nlc3MgPyAnZmFsc2UnIDogJ3RydWUnXG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgdGl0bGVBY2Nlc3MgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAndGl0bGUnLFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgdGl0bGVBY2Nlc3NcbiAgICAgICAgKSA6IG51bGwsXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gU3ZnSWNvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblN2Z0ljb24uZGVmYXVsdFByb3BzID0ge1xuICB2aWV3Qm94OiAnMCAwIDI0IDI0JyxcbiAgY29sb3I6ICdpbmhlcml0J1xufTtcblN2Z0ljb24ubXVpTmFtZSA9ICdTdmdJY29uJztcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlTdmdJY29uJyB9KShTdmdJY29uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMzQgMzYgMzkgNDMgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJy4vU3ZnSWNvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2tleXMgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2tleXMnKTtcblxudmFyIF9rZXlzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2tleXMpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfZXh0ZW5kczMgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHM0ID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczMpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQnV0dG9uQmFzZSA9IHJlcXVpcmUoJy4uL0J1dHRvbkJhc2UnKTtcblxudmFyIF9CdXR0b25CYXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0J1dHRvbkJhc2UpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4uL0ljb24nKTtcblxudmFyIF9JY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBCdXR0b25CYXNlXG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6ICgwLCBfZXh0ZW5kczQuZGVmYXVsdCkoe30sIHRoZW1lLnR5cG9ncmFwaHkuYnV0dG9uLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7XG4gICAgICBtYXhXaWR0aDogMjY0LFxuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBtaW5XaWR0aDogNzIsXG4gICAgICBwYWRkaW5nOiAwLFxuICAgICAgaGVpZ2h0OiA0OCxcbiAgICAgIGZsZXg6ICdub25lJyxcbiAgICAgIG92ZXJmbG93OiAnaGlkZGVuJ1xuICAgIH0sIHRoZW1lLmJyZWFrcG9pbnRzLnVwKCdtZCcpLCB7XG4gICAgICBtaW5XaWR0aDogMTYwXG4gICAgfSkpLFxuICAgIHJvb3RMYWJlbEljb246IHtcbiAgICAgIGhlaWdodDogNzJcbiAgICB9LFxuICAgIHJvb3RBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuc2Vjb25kYXJ5XG4gICAgfSxcbiAgICByb290QWNjZW50U2VsZWN0ZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICByb290QWNjZW50RGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGlzYWJsZWRcbiAgICB9LFxuICAgIHJvb3RQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LnNlY29uZGFyeVxuICAgIH0sXG4gICAgcm9vdFByaW1hcnlTZWxlY3RlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfSxcbiAgICByb290UHJpbWFyeURpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpc2FibGVkXG4gICAgfSxcbiAgICByb290SW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0JyxcbiAgICAgIG9wYWNpdHk6IDAuN1xuICAgIH0sXG4gICAgcm9vdEluaGVyaXRTZWxlY3RlZDoge1xuICAgICAgb3BhY2l0eTogMVxuICAgIH0sXG4gICAgcm9vdEluaGVyaXREaXNhYmxlZDoge1xuICAgICAgb3BhY2l0eTogMC40XG4gICAgfSxcbiAgICBmdWxsV2lkdGg6IHtcbiAgICAgIGZsZXhHcm93OiAxXG4gICAgfSxcbiAgICB3cmFwcGVyOiB7XG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWZsZXgnLFxuICAgICAgYWxpZ25JdGVtczogJ2NlbnRlcicsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcicsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgZmxleERpcmVjdGlvbjogJ2NvbHVtbidcbiAgICB9LFxuICAgIGxhYmVsQ29udGFpbmVyOiAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7XG4gICAgICBwYWRkaW5nVG9wOiA2LFxuICAgICAgcGFkZGluZ0JvdHRvbTogNixcbiAgICAgIHBhZGRpbmdMZWZ0OiAxMixcbiAgICAgIHBhZGRpbmdSaWdodDogMTJcbiAgICB9LCB0aGVtZS5icmVha3BvaW50cy51cCgnbWQnKSwge1xuICAgICAgcGFkZGluZ0xlZnQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDMsXG4gICAgICBwYWRkaW5nUmlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDNcbiAgICB9KSxcbiAgICBsYWJlbDogKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe1xuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSh0aGVtZS50eXBvZ3JhcGh5LmZvbnRTaXplKSxcbiAgICAgIHdoaXRlU3BhY2U6ICdub3JtYWwnXG4gICAgfSwgdGhlbWUuYnJlYWtwb2ludHMudXAoJ21kJyksIHtcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0odGhlbWUudHlwb2dyYXBoeS5mb250U2l6ZSAtIDEpXG4gICAgfSksXG4gICAgbGFiZWxXcmFwcGVkOiAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKSwge1xuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSh0aGVtZS50eXBvZ3JhcGh5LmZvbnRTaXplIC0gMilcbiAgICB9KVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSB0YWIgd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBmdWxsV2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiBlbGVtZW50LiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGEgZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGljb246IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIEZvciBzZXJ2ZXIgc2lkZSByZW5kZXJpbmcgY29uc2lkZXJhdGlvbiwgd2UgbGV0IHRoZSBzZWxlY3RlZCB0YWJcbiAgICogcmVuZGVyIHRoZSBpbmRpY2F0b3IuXG4gICAqL1xuICBpbmRpY2F0b3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBUaGUgbGFiZWwgZWxlbWVudC5cbiAgICovXG4gIGxhYmVsOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25DaGFuZ2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkNsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc2VsZWN0ZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBzdHlsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgdGV4dENvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhY2NlbnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3ByaW1hcnknXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmddKSxcblxuICAvKipcbiAgICogWW91IGNhbiBwcm92aWRlIHlvdXIgb3duIHZhbHVlLiBPdGhlcndpc2UsIHdlIGZhbGxiYWNrIHRvIHRoZSBjaGlsZCBwb3NpdGlvbiBpbmRleC5cbiAgICovXG4gIHZhbHVlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55XG59O1xuXG52YXIgVGFiID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoVGFiLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBUYWIoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgVGFiKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IFRhYi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoVGFiKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuc3RhdGUgPSB7XG4gICAgICB3cmFwcGVkVGV4dDogZmFsc2VcbiAgICB9LCBfdGhpcy5oYW5kbGVDaGFuZ2UgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIG9uQ2hhbmdlID0gX3RoaXMkcHJvcHMub25DaGFuZ2UsXG4gICAgICAgICAgdmFsdWUgPSBfdGhpcyRwcm9wcy52YWx1ZSxcbiAgICAgICAgICBvbkNsaWNrID0gX3RoaXMkcHJvcHMub25DbGljaztcblxuXG4gICAgICBpZiAob25DaGFuZ2UpIHtcbiAgICAgICAgb25DaGFuZ2UoZXZlbnQsIHZhbHVlKTtcbiAgICAgIH1cblxuICAgICAgaWYgKG9uQ2xpY2spIHtcbiAgICAgICAgb25DbGljayhldmVudCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMubGFiZWwgPSB1bmRlZmluZWQsIF90aGlzLmNoZWNrVGV4dFdyYXAgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMubGFiZWwpIHtcbiAgICAgICAgdmFyIF93cmFwcGVkVGV4dCA9IF90aGlzLmxhYmVsLmdldENsaWVudFJlY3RzKCkubGVuZ3RoID4gMTtcbiAgICAgICAgaWYgKF90aGlzLnN0YXRlLndyYXBwZWRUZXh0ICE9PSBfd3JhcHBlZFRleHQpIHtcbiAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7IHdyYXBwZWRUZXh0OiBfd3JhcHBlZFRleHQgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoVGFiLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICB0aGlzLmNoZWNrVGV4dFdyYXAoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzLCBwcmV2U3RhdGUpIHtcbiAgICAgIGlmICh0aGlzLnN0YXRlLndyYXBwZWRUZXh0ID09PSBwcmV2U3RhdGUud3JhcHBlZFRleHQpIHtcbiAgICAgICAgLyoqXG4gICAgICAgICAqIEF0IGNlcnRhaW4gdGV4dCBhbmQgdGFiIGxlbmd0aHMsIGEgbGFyZ2VyIGZvbnQgc2l6ZSBtYXkgd3JhcCB0byB0d28gbGluZXMgd2hpbGUgdGhlIHNtYWxsZXJcbiAgICAgICAgICogZm9udCBzaXplIHN0aWxsIG9ubHkgcmVxdWlyZXMgb25lIGxpbmUuICBUaGlzIGNoZWNrIHdpbGwgcHJldmVudCBhbiBpbmZpbml0ZSByZW5kZXIgbG9vcFxuICAgICAgICAgKiBmcm9uIG9jY3VycmluZyBpbiB0aGF0IHNjZW5hcmlvLlxuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5jaGVja1RleHRXcmFwKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXMsXG4gICAgICAgICAgX2NsYXNzTmFtZXMyO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgICAgZnVsbFdpZHRoID0gX3Byb3BzLmZ1bGxXaWR0aCxcbiAgICAgICAgICBpY29uUHJvcCA9IF9wcm9wcy5pY29uLFxuICAgICAgICAgIGluZGljYXRvciA9IF9wcm9wcy5pbmRpY2F0b3IsXG4gICAgICAgICAgbGFiZWxQcm9wID0gX3Byb3BzLmxhYmVsLFxuICAgICAgICAgIG9uQ2hhbmdlID0gX3Byb3BzLm9uQ2hhbmdlLFxuICAgICAgICAgIHNlbGVjdGVkID0gX3Byb3BzLnNlbGVjdGVkLFxuICAgICAgICAgIHN0eWxlUHJvcCA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICB0ZXh0Q29sb3IgPSBfcHJvcHMudGV4dENvbG9yLFxuICAgICAgICAgIHZhbHVlID0gX3Byb3BzLnZhbHVlLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2Rpc2FibGVkJywgJ2Z1bGxXaWR0aCcsICdpY29uJywgJ2luZGljYXRvcicsICdsYWJlbCcsICdvbkNoYW5nZScsICdzZWxlY3RlZCcsICdzdHlsZScsICd0ZXh0Q29sb3InLCAndmFsdWUnXSk7XG5cblxuICAgICAgdmFyIGljb24gPSB2b2lkIDA7XG5cbiAgICAgIGlmIChpY29uUHJvcCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGljb24gPSBfcmVhY3QyLmRlZmF1bHQuaXNWYWxpZEVsZW1lbnQoaWNvblByb3ApID8gaWNvblByb3AgOiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBfSWNvbjIuZGVmYXVsdCxcbiAgICAgICAgICBudWxsLFxuICAgICAgICAgIGljb25Qcm9wXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIHZhciBsYWJlbCA9IHZvaWQgMDtcblxuICAgICAgaWYgKGxhYmVsUHJvcCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGxhYmVsID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMubGFiZWxDb250YWluZXIgfSxcbiAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMubGFiZWwsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmxhYmVsV3JhcHBlZCwgdGhpcy5zdGF0ZS53cmFwcGVkVGV4dCkpLFxuICAgICAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMyLmxhYmVsID0gbm9kZTtcbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGxhYmVsUHJvcFxuICAgICAgICAgIClcbiAgICAgICAgKTtcbiAgICAgIH1cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMyID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMiwgY2xhc3Nlc1sncm9vdCcgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKSh0ZXh0Q29sb3IpXSwgdHJ1ZSksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMiwgY2xhc3Nlc1sncm9vdCcgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKSh0ZXh0Q29sb3IpICsgJ0Rpc2FibGVkJ10sIGRpc2FibGVkKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMyLCBjbGFzc2VzWydyb290JyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKHRleHRDb2xvcikgKyAnU2VsZWN0ZWQnXSwgc2VsZWN0ZWQpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczIsIGNsYXNzZXMucm9vdExhYmVsSWNvbiwgaWNvbiAmJiBsYWJlbCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMiwgY2xhc3Nlcy5mdWxsV2lkdGgsIGZ1bGxXaWR0aCksIF9jbGFzc05hbWVzMiksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICB2YXIgc3R5bGUgPSB7fTtcblxuICAgICAgaWYgKHRleHRDb2xvciAhPT0gJ2FjY2VudCcgJiYgdGV4dENvbG9yICE9PSAnaW5oZXJpdCcpIHtcbiAgICAgICAgc3R5bGUuY29sb3IgPSB0ZXh0Q29sb3I7XG4gICAgICB9XG5cbiAgICAgIHN0eWxlID0gKDAsIF9rZXlzMi5kZWZhdWx0KShzdHlsZSkubGVuZ3RoID4gMCA/ICgwLCBfZXh0ZW5kczQuZGVmYXVsdCkoe30sIHN0eWxlLCBzdHlsZVByb3ApIDogc3R5bGVQcm9wO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9CdXR0b25CYXNlMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHM0LmRlZmF1bHQpKHtcbiAgICAgICAgICBmb2N1c1JpcHBsZTogdHJ1ZSxcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgICAgcm9sZTogJ3RhYicsXG4gICAgICAgICAgJ2FyaWEtc2VsZWN0ZWQnOiBzZWxlY3RlZCxcbiAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZWRcbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICBvbkNsaWNrOiB0aGlzLmhhbmRsZUNoYW5nZVxuICAgICAgICB9KSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLndyYXBwZXIgfSxcbiAgICAgICAgICBpY29uLFxuICAgICAgICAgIGxhYmVsXG4gICAgICAgICksXG4gICAgICAgIGluZGljYXRvclxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFRhYjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblRhYi5kZWZhdWx0UHJvcHMgPSB7XG4gIGRpc2FibGVkOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlUYWInIH0pKFRhYik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX3JlZjsgLy8gIHdlYWtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuICAgICAgaGVpZ2h0OiAyLFxuICAgICAgYm90dG9tOiAwLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgpLFxuICAgICAgd2lsbENoYW5nZTogJ2xlZnQsIHdpZHRoJ1xuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0luZGljYXRvclN0eWxlID0ge1xuICBsZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICB3aWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxufTtcbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm92aWRlZFByb3BzID0ge1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWRcbn07XG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICogVGhlIGNvbG9yIG9mIHRoZSB0YWIgaW5kaWNhdG9yLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2FjY2VudCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncHJpbWFyeSddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICogVGhlIHN0eWxlIG9mIHRoZSByb290IGVsZW1lbnQuXG4gICAqL1xuICBzdHlsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKHtcbiAgICBsZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICAgIHdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG4gIH0pLmlzUmVxdWlyZWRcbn07XG5cblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG5mdW5jdGlvbiBUYWJJbmRpY2F0b3IocHJvcHMpIHtcbiAgdmFyIGNsYXNzZXMgPSBwcm9wcy5jbGFzc2VzLFxuICAgICAgY2xhc3NOYW1lUHJvcCA9IHByb3BzLmNsYXNzTmFtZSxcbiAgICAgIGNvbG9yID0gcHJvcHMuY29sb3IsXG4gICAgICBzdHlsZVByb3AgPSBwcm9wcy5zdHlsZTtcblxuICB2YXIgY29sb3JQcmVkZWZpbmVkID0gWydwcmltYXJ5JywgJ2FjY2VudCddLmluZGV4T2YoY29sb3IpICE9PSAtMTtcbiAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3JQcmVkZWZpbmVkKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgdmFyIHN0eWxlID0gY29sb3JQcmVkZWZpbmVkID8gc3R5bGVQcm9wIDogKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgc3R5bGVQcm9wLCB7XG4gICAgYmFja2dyb3VuZENvbG9yOiBjb2xvclxuICB9KTtcblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHsgY2xhc3NOYW1lOiBjbGFzc05hbWUsIHN0eWxlOiBzdHlsZSB9KTtcbn1cblxuVGFiSW5kaWNhdG9yLnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IChfcmVmID0ge1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWRcbn0sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjbGFzc2VzJywgcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjbGFzc05hbWUnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX3JlZiwgJ2NvbG9yJywgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYWNjZW50J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydwcmltYXJ5J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXSkuaXNSZXF1aXJlZCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdzdHlsZScsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gIGxlZnQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gIHdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG59KS5pc1JlcXVpcmVkKSwgX3JlZikgOiB7fTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlUYWJJbmRpY2F0b3InIH0pKFRhYkluZGljYXRvcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJJbmRpY2F0b3IuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiSW5kaWNhdG9yLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9LZXlib2FyZEFycm93TGVmdCA9IHJlcXVpcmUoJy4uL3N2Zy1pY29ucy9LZXlib2FyZEFycm93TGVmdCcpO1xuXG52YXIgX0tleWJvYXJkQXJyb3dMZWZ0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0tleWJvYXJkQXJyb3dMZWZ0KTtcblxudmFyIF9LZXlib2FyZEFycm93UmlnaHQgPSByZXF1aXJlKCcuLi9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd1JpZ2h0Jyk7XG5cbnZhciBfS2V5Ym9hcmRBcnJvd1JpZ2h0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0tleWJvYXJkQXJyb3dSaWdodCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8vICB3ZWFrXG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGNvbG9yOiAnaW5oZXJpdCcsXG4gICAgICBmbGV4OiAnMCAwICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgKiA3ICsgJ3B4J1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogV2hpY2ggZGlyZWN0aW9uIHNob3VsZCB0aGUgYnV0dG9uIGluZGljYXRlP1xuICAgKi9cbiAgZGlyZWN0aW9uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydsZWZ0JywgJ3JpZ2h0J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIHRvIGV4ZWN1dGUgZm9yIGJ1dHRvbiBwcmVzcy5cbiAgICovXG4gIG9uQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBTaG91bGQgdGhlIGJ1dHRvbiBiZSBwcmVzZW50IG9yIGp1c3QgY29uc3VtZSBzcGFjZS5cbiAgICovXG4gIHZpc2libGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59O1xuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9LZXlib2FyZEFycm93TGVmdDIuZGVmYXVsdCwgbnVsbCk7XG5cbnZhciBfcmVmMiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9LZXlib2FyZEFycm93UmlnaHQyLmRlZmF1bHQsIG51bGwpO1xuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBUYWJTY3JvbGxCdXR0b24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShUYWJTY3JvbGxCdXR0b24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFRhYlNjcm9sbEJ1dHRvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBUYWJTY3JvbGxCdXR0b24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChUYWJTY3JvbGxCdXR0b24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFRhYlNjcm9sbEJ1dHRvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoVGFiU2Nyb2xsQnV0dG9uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBkaXJlY3Rpb24gPSBfcHJvcHMuZGlyZWN0aW9uLFxuICAgICAgICAgIG9uQ2xpY2sgPSBfcHJvcHMub25DbGljayxcbiAgICAgICAgICB2aXNpYmxlID0gX3Byb3BzLnZpc2libGUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnZGlyZWN0aW9uJywgJ29uQ2xpY2snLCAndmlzaWJsZSddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICBpZiAoIXZpc2libGUpIHtcbiAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdkaXYnLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0pO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9CdXR0b25CYXNlMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUsIG9uQ2xpY2s6IG9uQ2xpY2ssIHRhYkluZGV4OiAtMSB9LCBvdGhlciksXG4gICAgICAgIGRpcmVjdGlvbiA9PT0gJ2xlZnQnID8gX3JlZiA6IF9yZWYyXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gVGFiU2Nyb2xsQnV0dG9uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuVGFiU2Nyb2xsQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgdmlzaWJsZTogdHJ1ZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlUYWJTY3JvbGxCdXR0b24nIH0pKFRhYlNjcm9sbEJ1dHRvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJTY3JvbGxCdXR0b24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiU2Nyb2xsQnV0dG9uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2lzTmFuID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL251bWJlci9pcy1uYW4nKTtcblxudmFyIF9pc05hbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc05hbik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3JlYWN0RXZlbnRMaXN0ZW5lciA9IHJlcXVpcmUoJ3JlYWN0LWV2ZW50LWxpc3RlbmVyJyk7XG5cbnZhciBfcmVhY3RFdmVudExpc3RlbmVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0RXZlbnRMaXN0ZW5lcik7XG5cbnZhciBfZGVib3VuY2UgPSByZXF1aXJlKCdsb2Rhc2gvZGVib3VuY2UnKTtcblxudmFyIF9kZWJvdW5jZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWJvdW5jZSk7XG5cbnZhciBfcmVhY3RTY3JvbGxiYXJTaXplID0gcmVxdWlyZSgncmVhY3Qtc2Nyb2xsYmFyLXNpemUnKTtcblxudmFyIF9yZWFjdFNjcm9sbGJhclNpemUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3RTY3JvbGxiYXJTaXplKTtcblxudmFyIF9ub3JtYWxpemVTY3JvbGxMZWZ0ID0gcmVxdWlyZSgnbm9ybWFsaXplLXNjcm9sbC1sZWZ0Jyk7XG5cbnZhciBfc2Nyb2xsID0gcmVxdWlyZSgnc2Nyb2xsJyk7XG5cbnZhciBfc2Nyb2xsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Njcm9sbCk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9UYWJJbmRpY2F0b3IgPSByZXF1aXJlKCcuL1RhYkluZGljYXRvcicpO1xuXG52YXIgX1RhYkluZGljYXRvcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UYWJJbmRpY2F0b3IpO1xuXG52YXIgX1RhYlNjcm9sbEJ1dHRvbiA9IHJlcXVpcmUoJy4vVGFiU2Nyb2xsQnV0dG9uJyk7XG5cbnZhciBfVGFiU2Nyb2xsQnV0dG9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RhYlNjcm9sbEJ1dHRvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlID0gcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmM7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9JbmRpY2F0b3JTdHlsZSA9IHJlcXVpcmUoJy4vVGFiSW5kaWNhdG9yJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSW5kaWNhdG9yU3R5bGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgbWluSGVpZ2h0OiA0OCxcbiAgICAgIFdlYmtpdE92ZXJmbG93U2Nyb2xsaW5nOiAndG91Y2gnIC8vIEFkZCBpT1MgbW9tZW50dW0gc2Nyb2xsaW5nLlxuICAgIH0sXG4gICAgZmxleENvbnRhaW5lcjoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnXG4gICAgfSxcbiAgICBzY3JvbGxpbmdDb250YWluZXI6IHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgZGlzcGxheTogJ2lubGluZS1ibG9jaycsXG4gICAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICAgICAgd2hpdGVTcGFjZTogJ25vd3JhcCdcbiAgICB9LFxuICAgIGZpeGVkOiB7XG4gICAgICBvdmVyZmxvd1g6ICdoaWRkZW4nLFxuICAgICAgd2lkdGg6ICcxMDAlJ1xuICAgIH0sXG4gICAgc2Nyb2xsYWJsZToge1xuICAgICAgb3ZlcmZsb3dYOiAnc2Nyb2xsJ1xuICAgIH0sXG4gICAgY2VudGVyZWQ6IHtcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnY2VudGVyJ1xuICAgIH0sXG4gICAgYnV0dG9uQXV0bzogKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIHRoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyksIHtcbiAgICAgIGRpc3BsYXk6ICdub25lJ1xuICAgIH0pXG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgQ1NTIGNsYXNzIG5hbWUgb2YgdGhlIHNjcm9sbCBidXR0b24gZWxlbWVudHMuXG4gICAqL1xuICBidXR0b25DbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHRhYnMgd2lsbCBiZSBjZW50ZXJlZC5cbiAgICogVGhpcyBwcm9wZXJ0eSBpcyBpbnRlbmRlZCBmb3IgbGFyZ2Ugdmlld3MuXG4gICAqL1xuICBjZW50ZXJlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgdGFicyB3aWxsIGdyb3cgdG8gdXNlIGFsbCB0aGUgYXZhaWxhYmxlIHNwYWNlLlxuICAgKiBUaGlzIHByb3BlcnR5IGlzIGludGVuZGVkIGZvciBzbWFsbCB2aWV3cywgbGlrZSBvbiBtb2JpbGUuXG4gICAqL1xuICBmdWxsV2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBDU1MgY2xhc3MgbmFtZSBvZiB0aGUgaW5kaWNhdG9yIGVsZW1lbnQuXG4gICAqL1xuICBpbmRpY2F0b3JDbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgdGhlIGNvbG9yIG9mIHRoZSBpbmRpY2F0b3IuXG4gICAqL1xuICBpbmRpY2F0b3JDb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYWNjZW50J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydwcmltYXJ5J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgdmFsdWUgY2hhbmdlcy5cbiAgICpcbiAgICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IFRoZSBldmVudCBzb3VyY2Ugb2YgdGhlIGNhbGxiYWNrXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB2YWx1ZSBXZSBkZWZhdWx0IHRvIHRoZSBpbmRleCBvZiB0aGUgY2hpbGRcbiAgICovXG4gIG9uQ2hhbmdlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYy5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUcnVlIGludm9rZXMgc2Nyb2xsaW5nIHByb3BlcnRpZXMgYW5kIGFsbG93IGZvciBob3Jpem9udGFsbHkgc2Nyb2xsaW5nXG4gICAqIChvciBzd2lwaW5nKSB0aGUgdGFiIGJhci5cbiAgICovXG4gIHNjcm9sbGFibGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIERldGVybWluZSBiZWhhdmlvciBvZiBzY3JvbGwgYnV0dG9ucyB3aGVuIHRhYnMgYXJlIHNldCB0byBzY3JvbGxcbiAgICogYGF1dG9gIHdpbGwgb25seSBwcmVzZW50IHRoZW0gb24gbWVkaXVtIGFuZCBsYXJnZXIgdmlld3BvcnRzXG4gICAqIGBvbmAgd2lsbCBhbHdheXMgcHJlc2VudCB0aGVtXG4gICAqIGBvZmZgIHdpbGwgbmV2ZXIgcHJlc2VudCB0aGVtXG4gICAqL1xuICBzY3JvbGxCdXR0b25zOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhdXRvJywgJ29uJywgJ29mZiddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgY29tcG9uZW50IHVzZWQgdG8gcmVuZGVyIHRoZSBzY3JvbGwgYnV0dG9ucy5cbiAgICovXG4gIFRhYlNjcm9sbEJ1dHRvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIERldGVybWluZXMgdGhlIGNvbG9yIG9mIHRoZSBgVGFiYC5cbiAgICovXG4gIHRleHRDb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYWNjZW50JywgJ3ByaW1hcnknLCAnaW5oZXJpdCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgdmFsdWUgb2YgdGhlIGN1cnJlbnRseSBzZWxlY3RlZCBgVGFiYC5cbiAgICogSWYgeW91IGRvbid0IHdhbnQgYW55IHNlbGVjdGVkIGBUYWJgLCB5b3UgY2FuIHNldCB0aGlzIHByb3BlcnR5IHRvIGBmYWxzZWAuXG4gICAqL1xuICB2YWx1ZTogZnVuY3Rpb24gdmFsdWUocHJvcHMsIHByb3BOYW1lLCBjb21wb25lbnROYW1lKSB7XG4gICAgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocHJvcHMsIHByb3BOYW1lKSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdQcm9wIGAnICsgcHJvcE5hbWUgKyAnYCBoYXMgdHlwZSBcXCdhbnlcXCcgb3IgXFwnbWl4ZWRcXCcsIGJ1dCB3YXMgbm90IHByb3ZpZGVkIHRvIGAnICsgY29tcG9uZW50TmFtZSArICdgLiBQYXNzIHVuZGVmaW5lZCBvciBhbnkgb3RoZXIgdmFsdWUuJyk7XG4gICAgfVxuICB9XG59O1xudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RhYnNNZXRhID0ge1xuICBjbGllbnRXaWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkLFxuICBzY3JvbGxMZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHNjcm9sbExlZnROb3JtYWxpemVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHNjcm9sbFdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG5cbiAgLy8gQ2xpZW50UmVjdFxuICBsZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHJpZ2h0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLmlzUmVxdWlyZWRcbn07XG5cbnZhciBUYWJzID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoVGFicywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gVGFicygpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBUYWJzKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IFRhYnMuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFRhYnMpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGluZGljYXRvclN0eWxlOiB7fSxcbiAgICAgIHNjcm9sbGVyU3R5bGU6IHtcbiAgICAgICAgbWFyZ2luQm90dG9tOiAwXG4gICAgICB9LFxuICAgICAgc2hvd0xlZnRTY3JvbGw6IGZhbHNlLFxuICAgICAgc2hvd1JpZ2h0U2Nyb2xsOiBmYWxzZSxcbiAgICAgIG1vdW50ZWQ6IGZhbHNlXG4gICAgfSwgX3RoaXMuZ2V0Q29uZGl0aW9uYWxFbGVtZW50cyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIGNsYXNzZXMgPSBfdGhpcyRwcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGJ1dHRvbkNsYXNzTmFtZSA9IF90aGlzJHByb3BzLmJ1dHRvbkNsYXNzTmFtZSxcbiAgICAgICAgICBzY3JvbGxhYmxlID0gX3RoaXMkcHJvcHMuc2Nyb2xsYWJsZSxcbiAgICAgICAgICBzY3JvbGxCdXR0b25zID0gX3RoaXMkcHJvcHMuc2Nyb2xsQnV0dG9ucyxcbiAgICAgICAgICBUYWJTY3JvbGxCdXR0b25Qcm9wID0gX3RoaXMkcHJvcHMuVGFiU2Nyb2xsQnV0dG9uLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMudGhlbWU7XG5cbiAgICAgIHZhciBjb25kaXRpb25hbEVsZW1lbnRzID0ge307XG4gICAgICBjb25kaXRpb25hbEVsZW1lbnRzLnNjcm9sbGJhclNpemVMaXN0ZW5lciA9IHNjcm9sbGFibGUgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfcmVhY3RTY3JvbGxiYXJTaXplMi5kZWZhdWx0LCB7XG4gICAgICAgIG9uTG9hZDogX3RoaXMuaGFuZGxlU2Nyb2xsYmFyU2l6ZUNoYW5nZSxcbiAgICAgICAgb25DaGFuZ2U6IF90aGlzLmhhbmRsZVNjcm9sbGJhclNpemVDaGFuZ2VcbiAgICAgIH0pIDogbnVsbDtcblxuICAgICAgdmFyIHNob3dTY3JvbGxCdXR0b25zID0gc2Nyb2xsYWJsZSAmJiAoc2Nyb2xsQnV0dG9ucyA9PT0gJ2F1dG8nIHx8IHNjcm9sbEJ1dHRvbnMgPT09ICdvbicpO1xuXG4gICAgICBjb25kaXRpb25hbEVsZW1lbnRzLnNjcm9sbEJ1dHRvbkxlZnQgPSBzaG93U2Nyb2xsQnV0dG9ucyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFRhYlNjcm9sbEJ1dHRvblByb3AsIHtcbiAgICAgICAgZGlyZWN0aW9uOiB0aGVtZSAmJiB0aGVtZS5kaXJlY3Rpb24gPT09ICdydGwnID8gJ3JpZ2h0JyA6ICdsZWZ0JyxcbiAgICAgICAgb25DbGljazogX3RoaXMuaGFuZGxlTGVmdFNjcm9sbENsaWNrLFxuICAgICAgICB2aXNpYmxlOiBfdGhpcy5zdGF0ZS5zaG93TGVmdFNjcm9sbCxcbiAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmJ1dHRvbkF1dG8sIHNjcm9sbEJ1dHRvbnMgPT09ICdhdXRvJyksIGJ1dHRvbkNsYXNzTmFtZSlcbiAgICAgIH0pIDogbnVsbDtcblxuICAgICAgY29uZGl0aW9uYWxFbGVtZW50cy5zY3JvbGxCdXR0b25SaWdodCA9IHNob3dTY3JvbGxCdXR0b25zID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoVGFiU2Nyb2xsQnV0dG9uUHJvcCwge1xuICAgICAgICBkaXJlY3Rpb246IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbiA9PT0gJ3J0bCcgPyAnbGVmdCcgOiAncmlnaHQnLFxuICAgICAgICBvbkNsaWNrOiBfdGhpcy5oYW5kbGVSaWdodFNjcm9sbENsaWNrLFxuICAgICAgICB2aXNpYmxlOiBfdGhpcy5zdGF0ZS5zaG93UmlnaHRTY3JvbGwsXG4gICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KSgoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5idXR0b25BdXRvLCBzY3JvbGxCdXR0b25zID09PSAnYXV0bycpLCBidXR0b25DbGFzc05hbWUpXG4gICAgICB9KSA6IG51bGw7XG5cbiAgICAgIHJldHVybiBjb25kaXRpb25hbEVsZW1lbnRzO1xuICAgIH0sIF90aGlzLmdldFRhYnNNZXRhID0gZnVuY3Rpb24gKHZhbHVlLCBkaXJlY3Rpb24pIHtcbiAgICAgIHZhciB0YWJzTWV0YSA9IHZvaWQgMDtcbiAgICAgIGlmIChfdGhpcy50YWJzKSB7XG4gICAgICAgIHZhciByZWN0ID0gX3RoaXMudGFicy5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcbiAgICAgICAgLy8gY3JlYXRlIGEgbmV3IG9iamVjdCB3aXRoIENsaWVudFJlY3QgY2xhc3MgcHJvcHMgKyBzY3JvbGxMZWZ0XG4gICAgICAgIHRhYnNNZXRhID0ge1xuICAgICAgICAgIGNsaWVudFdpZHRoOiBfdGhpcy50YWJzID8gX3RoaXMudGFicy5jbGllbnRXaWR0aCA6IDAsXG4gICAgICAgICAgc2Nyb2xsTGVmdDogX3RoaXMudGFicyA/IF90aGlzLnRhYnMuc2Nyb2xsTGVmdCA6IDAsXG4gICAgICAgICAgc2Nyb2xsTGVmdE5vcm1hbGl6ZWQ6IF90aGlzLnRhYnMgPyAoMCwgX25vcm1hbGl6ZVNjcm9sbExlZnQuZ2V0Tm9ybWFsaXplZFNjcm9sbExlZnQpKF90aGlzLnRhYnMsIGRpcmVjdGlvbikgOiAwLFxuICAgICAgICAgIHNjcm9sbFdpZHRoOiBfdGhpcy50YWJzID8gX3RoaXMudGFicy5zY3JvbGxXaWR0aCA6IDAsXG4gICAgICAgICAgbGVmdDogcmVjdC5sZWZ0LFxuICAgICAgICAgIHJpZ2h0OiByZWN0LnJpZ2h0XG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIHZhciB0YWJNZXRhID0gdm9pZCAwO1xuICAgICAgaWYgKF90aGlzLnRhYnMgJiYgdmFsdWUgIT09IGZhbHNlKSB7XG4gICAgICAgIHZhciBfY2hpbGRyZW4gPSBfdGhpcy50YWJzLmNoaWxkcmVuWzBdLmNoaWxkcmVuO1xuXG4gICAgICAgIGlmIChfY2hpbGRyZW4ubGVuZ3RoID4gMCkge1xuICAgICAgICAgIHZhciB0YWIgPSBfY2hpbGRyZW5bX3RoaXMudmFsdWVUb0luZGV4W3ZhbHVlXV07XG4gICAgICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KShCb29sZWFuKHRhYiksICdNYXRlcmlhbC1VSTogdGhlIHZhbHVlIHByb3ZpZGVkIGAnICsgdmFsdWUgKyAnYCBpcyBpbnZhbGlkJykgOiB2b2lkIDA7XG4gICAgICAgICAgdGFiTWV0YSA9IHRhYiA/IHRhYi5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKSA6IG51bGw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIHJldHVybiB7IHRhYnNNZXRhOiB0YWJzTWV0YSwgdGFiTWV0YTogdGFiTWV0YSB9O1xuICAgIH0sIF90aGlzLnRhYnMgPSB1bmRlZmluZWQsIF90aGlzLnZhbHVlVG9JbmRleCA9IHt9LCBfdGhpcy5oYW5kbGVSZXNpemUgPSAoMCwgX2RlYm91bmNlMi5kZWZhdWx0KShmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy51cGRhdGVJbmRpY2F0b3JTdGF0ZShfdGhpcy5wcm9wcyk7XG4gICAgICBfdGhpcy51cGRhdGVTY3JvbGxCdXR0b25TdGF0ZSgpO1xuICAgIH0sIDE2NiksIF90aGlzLmhhbmRsZUxlZnRTY3JvbGxDbGljayA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy50YWJzKSB7XG4gICAgICAgIF90aGlzLm1vdmVUYWJzU2Nyb2xsKC1fdGhpcy50YWJzLmNsaWVudFdpZHRoKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVSaWdodFNjcm9sbENsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKF90aGlzLnRhYnMpIHtcbiAgICAgICAgX3RoaXMubW92ZVRhYnNTY3JvbGwoX3RoaXMudGFicy5jbGllbnRXaWR0aCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlU2Nyb2xsYmFyU2l6ZUNoYW5nZSA9IGZ1bmN0aW9uIChfcmVmMikge1xuICAgICAgdmFyIHNjcm9sbGJhckhlaWdodCA9IF9yZWYyLnNjcm9sbGJhckhlaWdodDtcblxuICAgICAgX3RoaXMuc2V0U3RhdGUoe1xuICAgICAgICBzY3JvbGxlclN0eWxlOiB7XG4gICAgICAgICAgbWFyZ2luQm90dG9tOiAtc2Nyb2xsYmFySGVpZ2h0XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0sIF90aGlzLmhhbmRsZVRhYnNTY3JvbGwgPSAoMCwgX2RlYm91bmNlMi5kZWZhdWx0KShmdW5jdGlvbiAoKSB7XG4gICAgICBfdGhpcy51cGRhdGVTY3JvbGxCdXR0b25TdGF0ZSgpO1xuICAgIH0sIDE2NiksIF90aGlzLm1vdmVUYWJzU2Nyb2xsID0gZnVuY3Rpb24gKGRlbHRhKSB7XG4gICAgICB2YXIgdGhlbWUgPSBfdGhpcy5wcm9wcy50aGVtZTtcblxuXG4gICAgICBpZiAoX3RoaXMudGFicykge1xuICAgICAgICB2YXIgdGhlbWVEaXJlY3Rpb24gPSB0aGVtZSAmJiB0aGVtZS5kaXJlY3Rpb247XG4gICAgICAgIHZhciBtdWx0aXBsaWVyID0gdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnID8gLTEgOiAxO1xuICAgICAgICB2YXIgbmV4dFNjcm9sbExlZnQgPSBfdGhpcy50YWJzLnNjcm9sbExlZnQgKyBkZWx0YSAqIG11bHRpcGxpZXI7XG4gICAgICAgIC8vIEZpeCBmb3IgRWRnZVxuICAgICAgICB2YXIgaW52ZXJ0ID0gdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnICYmICgwLCBfbm9ybWFsaXplU2Nyb2xsTGVmdC5kZXRlY3RTY3JvbGxUeXBlKSgpID09PSAncmV2ZXJzZScgPyAtMSA6IDE7XG4gICAgICAgIF9zY3JvbGwyLmRlZmF1bHQubGVmdChfdGhpcy50YWJzLCBpbnZlcnQgKiBuZXh0U2Nyb2xsTGVmdCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuc2Nyb2xsU2VsZWN0ZWRJbnRvVmlldyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZSxcbiAgICAgICAgICB2YWx1ZSA9IF90aGlzJHByb3BzMi52YWx1ZTtcblxuXG4gICAgICB2YXIgdGhlbWVEaXJlY3Rpb24gPSB0aGVtZSAmJiB0aGVtZS5kaXJlY3Rpb247XG5cbiAgICAgIHZhciBfdGhpcyRnZXRUYWJzTWV0YSA9IF90aGlzLmdldFRhYnNNZXRhKHZhbHVlLCB0aGVtZURpcmVjdGlvbiksXG4gICAgICAgICAgdGFic01ldGEgPSBfdGhpcyRnZXRUYWJzTWV0YS50YWJzTWV0YSxcbiAgICAgICAgICB0YWJNZXRhID0gX3RoaXMkZ2V0VGFic01ldGEudGFiTWV0YTtcblxuICAgICAgaWYgKCF0YWJNZXRhIHx8ICF0YWJzTWV0YSkge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGlmICh0YWJNZXRhLmxlZnQgPCB0YWJzTWV0YS5sZWZ0KSB7XG4gICAgICAgIC8vIGxlZnQgc2lkZSBvZiBidXR0b24gaXMgb3V0IG9mIHZpZXdcbiAgICAgICAgdmFyIG5leHRTY3JvbGxMZWZ0ID0gdGFic01ldGEuc2Nyb2xsTGVmdCArICh0YWJNZXRhLmxlZnQgLSB0YWJzTWV0YS5sZWZ0KTtcbiAgICAgICAgX3Njcm9sbDIuZGVmYXVsdC5sZWZ0KF90aGlzLnRhYnMsIG5leHRTY3JvbGxMZWZ0KTtcbiAgICAgIH0gZWxzZSBpZiAodGFiTWV0YS5yaWdodCA+IHRhYnNNZXRhLnJpZ2h0KSB7XG4gICAgICAgIC8vIHJpZ2h0IHNpZGUgb2YgYnV0dG9uIGlzIG91dCBvZiB2aWV3XG4gICAgICAgIHZhciBfbmV4dFNjcm9sbExlZnQgPSB0YWJzTWV0YS5zY3JvbGxMZWZ0ICsgKHRhYk1ldGEucmlnaHQgLSB0YWJzTWV0YS5yaWdodCk7XG4gICAgICAgIF9zY3JvbGwyLmRlZmF1bHQubGVmdChfdGhpcy50YWJzLCBfbmV4dFNjcm9sbExlZnQpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLnVwZGF0ZVNjcm9sbEJ1dHRvblN0YXRlID0gZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHNjcm9sbGFibGUgPSBfdGhpcyRwcm9wczMuc2Nyb2xsYWJsZSxcbiAgICAgICAgICBzY3JvbGxCdXR0b25zID0gX3RoaXMkcHJvcHMzLnNjcm9sbEJ1dHRvbnMsXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wczMudGhlbWU7XG5cbiAgICAgIHZhciB0aGVtZURpcmVjdGlvbiA9IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbjtcblxuICAgICAgaWYgKF90aGlzLnRhYnMgJiYgc2Nyb2xsYWJsZSAmJiBzY3JvbGxCdXR0b25zICE9PSAnb2ZmJykge1xuICAgICAgICB2YXIgX3RoaXMkdGFicyA9IF90aGlzLnRhYnMsXG4gICAgICAgICAgICBfc2Nyb2xsV2lkdGggPSBfdGhpcyR0YWJzLnNjcm9sbFdpZHRoLFxuICAgICAgICAgICAgX2NsaWVudFdpZHRoID0gX3RoaXMkdGFicy5jbGllbnRXaWR0aDtcblxuICAgICAgICB2YXIgX3Njcm9sbExlZnQgPSAoMCwgX25vcm1hbGl6ZVNjcm9sbExlZnQuZ2V0Tm9ybWFsaXplZFNjcm9sbExlZnQpKF90aGlzLnRhYnMsIHRoZW1lRGlyZWN0aW9uKTtcblxuICAgICAgICB2YXIgX3Nob3dMZWZ0U2Nyb2xsID0gdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnID8gX3Njcm9sbFdpZHRoID4gX2NsaWVudFdpZHRoICsgX3Njcm9sbExlZnQgOiBfc2Nyb2xsTGVmdCA+IDA7XG5cbiAgICAgICAgdmFyIF9zaG93UmlnaHRTY3JvbGwgPSB0aGVtZURpcmVjdGlvbiA9PT0gJ3J0bCcgPyBfc2Nyb2xsTGVmdCA+IDAgOiBfc2Nyb2xsV2lkdGggPiBfY2xpZW50V2lkdGggKyBfc2Nyb2xsTGVmdDtcblxuICAgICAgICBpZiAoX3Nob3dMZWZ0U2Nyb2xsICE9PSBfdGhpcy5zdGF0ZS5zaG93TGVmdFNjcm9sbCB8fCBfc2hvd1JpZ2h0U2Nyb2xsICE9PSBfdGhpcy5zdGF0ZS5zaG93UmlnaHRTY3JvbGwpIHtcbiAgICAgICAgICBfdGhpcy5zZXRTdGF0ZSh7IHNob3dMZWZ0U2Nyb2xsOiBfc2hvd0xlZnRTY3JvbGwsIHNob3dSaWdodFNjcm9sbDogX3Nob3dSaWdodFNjcm9sbCB9KTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShUYWJzLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgcmVhY3Qvbm8tZGlkLW1vdW50LXNldC1zdGF0ZVxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1vdW50ZWQ6IHRydWUgfSk7XG4gICAgICB0aGlzLnVwZGF0ZUluZGljYXRvclN0YXRlKHRoaXMucHJvcHMpO1xuICAgICAgdGhpcy51cGRhdGVTY3JvbGxCdXR0b25TdGF0ZSgpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMsIHByZXZTdGF0ZSkge1xuICAgICAgdGhpcy51cGRhdGVTY3JvbGxCdXR0b25TdGF0ZSgpO1xuXG4gICAgICAvLyBUaGUgaW5kZXggbWlnaHQgaGF2ZSBjaGFuZ2VkIGF0IHRoZSBzYW1lIHRpbWUuXG4gICAgICAvLyBXZSBuZWVkIHRvIGNoZWNrIGFnYWluIHRoZSByaWdodCBpbmRpY2F0b3IgcG9zaXRpb24uXG4gICAgICB0aGlzLnVwZGF0ZUluZGljYXRvclN0YXRlKHRoaXMucHJvcHMpO1xuXG4gICAgICBpZiAodGhpcy5zdGF0ZS5pbmRpY2F0b3JTdHlsZSAhPT0gcHJldlN0YXRlLmluZGljYXRvclN0eWxlKSB7XG4gICAgICAgIHRoaXMuc2Nyb2xsU2VsZWN0ZWRJbnRvVmlldygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG4gICAgICB0aGlzLmhhbmRsZVJlc2l6ZS5jYW5jZWwoKTtcbiAgICAgIHRoaXMuaGFuZGxlVGFic1Njcm9sbC5jYW5jZWwoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd1cGRhdGVJbmRpY2F0b3JTdGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVwZGF0ZUluZGljYXRvclN0YXRlKHByb3BzKSB7XG4gICAgICB2YXIgdGhlbWUgPSBwcm9wcy50aGVtZSxcbiAgICAgICAgICB2YWx1ZSA9IHByb3BzLnZhbHVlO1xuXG5cbiAgICAgIHZhciB0aGVtZURpcmVjdGlvbiA9IHRoZW1lICYmIHRoZW1lLmRpcmVjdGlvbjtcblxuICAgICAgdmFyIF9nZXRUYWJzTWV0YSA9IHRoaXMuZ2V0VGFic01ldGEodmFsdWUsIHRoZW1lRGlyZWN0aW9uKSxcbiAgICAgICAgICB0YWJzTWV0YSA9IF9nZXRUYWJzTWV0YS50YWJzTWV0YSxcbiAgICAgICAgICB0YWJNZXRhID0gX2dldFRhYnNNZXRhLnRhYk1ldGE7XG5cbiAgICAgIHZhciBsZWZ0ID0gMDtcblxuICAgICAgaWYgKHRhYk1ldGEgJiYgdGFic01ldGEpIHtcbiAgICAgICAgdmFyIGNvcnJlY3Rpb24gPSB0aGVtZURpcmVjdGlvbiA9PT0gJ3J0bCcgPyB0YWJzTWV0YS5zY3JvbGxMZWZ0Tm9ybWFsaXplZCArIHRhYnNNZXRhLmNsaWVudFdpZHRoIC0gdGFic01ldGEuc2Nyb2xsV2lkdGggOiB0YWJzTWV0YS5zY3JvbGxMZWZ0O1xuICAgICAgICBsZWZ0ID0gdGFiTWV0YS5sZWZ0IC0gdGFic01ldGEubGVmdCArIGNvcnJlY3Rpb247XG4gICAgICB9XG5cbiAgICAgIHZhciBpbmRpY2F0b3JTdHlsZSA9IHtcbiAgICAgICAgbGVmdDogbGVmdCxcbiAgICAgICAgLy8gTWF5IGJlIHdyb25nIHVudGlsIHRoZSBmb250IGlzIGxvYWRlZC5cbiAgICAgICAgd2lkdGg6IHRhYk1ldGEgPyB0YWJNZXRhLndpZHRoIDogMFxuICAgICAgfTtcblxuICAgICAgaWYgKChpbmRpY2F0b3JTdHlsZS5sZWZ0ICE9PSB0aGlzLnN0YXRlLmluZGljYXRvclN0eWxlLmxlZnQgfHwgaW5kaWNhdG9yU3R5bGUud2lkdGggIT09IHRoaXMuc3RhdGUuaW5kaWNhdG9yU3R5bGUud2lkdGgpICYmICEoMCwgX2lzTmFuMi5kZWZhdWx0KShpbmRpY2F0b3JTdHlsZS5sZWZ0KSAmJiAhKDAsIF9pc05hbjIuZGVmYXVsdCkoaW5kaWNhdG9yU3R5bGUud2lkdGgpKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBpbmRpY2F0b3JTdHlsZTogaW5kaWNhdG9yU3R5bGUgfSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzMyxcbiAgICAgICAgICBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBidXR0b25DbGFzc05hbWUgPSBfcHJvcHMuYnV0dG9uQ2xhc3NOYW1lLFxuICAgICAgICAgIGNlbnRlcmVkID0gX3Byb3BzLmNlbnRlcmVkLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjaGlsZHJlblByb3AgPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZnVsbFdpZHRoID0gX3Byb3BzLmZ1bGxXaWR0aCxcbiAgICAgICAgICBpbmRpY2F0b3JDbGFzc05hbWUgPSBfcHJvcHMuaW5kaWNhdG9yQ2xhc3NOYW1lLFxuICAgICAgICAgIGluZGljYXRvckNvbG9yID0gX3Byb3BzLmluZGljYXRvckNvbG9yLFxuICAgICAgICAgIG9uQ2hhbmdlID0gX3Byb3BzLm9uQ2hhbmdlLFxuICAgICAgICAgIHNjcm9sbGFibGUgPSBfcHJvcHMuc2Nyb2xsYWJsZSxcbiAgICAgICAgICBzY3JvbGxCdXR0b25zID0gX3Byb3BzLnNjcm9sbEJ1dHRvbnMsXG4gICAgICAgICAgVGFiU2Nyb2xsQnV0dG9uUHJvcCA9IF9wcm9wcy5UYWJTY3JvbGxCdXR0b24sXG4gICAgICAgICAgdGV4dENvbG9yID0gX3Byb3BzLnRleHRDb2xvcixcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICB2YWx1ZSA9IF9wcm9wcy52YWx1ZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydidXR0b25DbGFzc05hbWUnLCAnY2VudGVyZWQnLCAnY2xhc3NlcycsICdjaGlsZHJlbicsICdjbGFzc05hbWUnLCAnZnVsbFdpZHRoJywgJ2luZGljYXRvckNsYXNzTmFtZScsICdpbmRpY2F0b3JDb2xvcicsICdvbkNoYW5nZScsICdzY3JvbGxhYmxlJywgJ3Njcm9sbEJ1dHRvbnMnLCAnVGFiU2Nyb2xsQnV0dG9uJywgJ3RleHRDb2xvcicsICd0aGVtZScsICd2YWx1ZSddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIHNjcm9sbGVyQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnNjcm9sbGluZ0NvbnRhaW5lciwgKF9jbGFzc05hbWVzMyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczMsIGNsYXNzZXMuZml4ZWQsICFzY3JvbGxhYmxlKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMzLCBjbGFzc2VzLnNjcm9sbGFibGUsIHNjcm9sbGFibGUpLCBfY2xhc3NOYW1lczMpKTtcbiAgICAgIHZhciB0YWJJdGVtQ29udGFpbmVyQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmZsZXhDb250YWluZXIsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmNlbnRlcmVkLCBjZW50ZXJlZCAmJiAhc2Nyb2xsYWJsZSkpO1xuXG4gICAgICB2YXIgaW5kaWNhdG9yID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX1RhYkluZGljYXRvcjIuZGVmYXVsdCwge1xuICAgICAgICBzdHlsZTogdGhpcy5zdGF0ZS5pbmRpY2F0b3JTdHlsZSxcbiAgICAgICAgY2xhc3NOYW1lOiBpbmRpY2F0b3JDbGFzc05hbWUsXG4gICAgICAgIGNvbG9yOiBpbmRpY2F0b3JDb2xvclxuICAgICAgfSk7XG5cbiAgICAgIHRoaXMudmFsdWVUb0luZGV4ID0ge307XG4gICAgICB2YXIgY2hpbGRJbmRleCA9IDA7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ubWFwKGNoaWxkcmVuUHJvcCwgZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgIGlmICghX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkKSkge1xuICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG5cbiAgICAgICAgdmFyIGNoaWxkVmFsdWUgPSBjaGlsZC5wcm9wcy52YWx1ZSB8fCBjaGlsZEluZGV4O1xuICAgICAgICBfdGhpczIudmFsdWVUb0luZGV4W2NoaWxkVmFsdWVdID0gY2hpbGRJbmRleDtcbiAgICAgICAgdmFyIHNlbGVjdGVkID0gY2hpbGRWYWx1ZSA9PT0gdmFsdWU7XG5cbiAgICAgICAgY2hpbGRJbmRleCArPSAxO1xuICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgIGZ1bGxXaWR0aDogZnVsbFdpZHRoLFxuICAgICAgICAgIGluZGljYXRvcjogc2VsZWN0ZWQgJiYgIV90aGlzMi5zdGF0ZS5tb3VudGVkICYmIGluZGljYXRvcixcbiAgICAgICAgICBzZWxlY3RlZDogc2VsZWN0ZWQsXG4gICAgICAgICAgb25DaGFuZ2U6IG9uQ2hhbmdlLFxuICAgICAgICAgIHRleHRDb2xvcjogdGV4dENvbG9yLFxuICAgICAgICAgIHZhbHVlOiBjaGlsZFZhbHVlXG4gICAgICAgIH0pO1xuICAgICAgfSk7XG5cbiAgICAgIHZhciBjb25kaXRpb25hbEVsZW1lbnRzID0gdGhpcy5nZXRDb25kaXRpb25hbEVsZW1lbnRzKCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9yZWFjdEV2ZW50TGlzdGVuZXIyLmRlZmF1bHQsIHsgdGFyZ2V0OiAnd2luZG93Jywgb25SZXNpemU6IHRoaXMuaGFuZGxlUmVzaXplIH0pLFxuICAgICAgICBjb25kaXRpb25hbEVsZW1lbnRzLnNjcm9sbGJhclNpemVMaXN0ZW5lcixcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuZmxleENvbnRhaW5lciB9LFxuICAgICAgICAgIGNvbmRpdGlvbmFsRWxlbWVudHMuc2Nyb2xsQnV0dG9uTGVmdCxcbiAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICdkaXYnLFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBjbGFzc05hbWU6IHNjcm9sbGVyQ2xhc3NOYW1lLFxuICAgICAgICAgICAgICBzdHlsZTogdGhpcy5zdGF0ZS5zY3JvbGxlclN0eWxlLFxuICAgICAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMyLnRhYnMgPSBub2RlO1xuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICByb2xlOiAndGFibGlzdCcsXG4gICAgICAgICAgICAgIG9uU2Nyb2xsOiB0aGlzLmhhbmRsZVRhYnNTY3JvbGxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgICAgIHsgY2xhc3NOYW1lOiB0YWJJdGVtQ29udGFpbmVyQ2xhc3NOYW1lIH0sXG4gICAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgICApLFxuICAgICAgICAgICAgdGhpcy5zdGF0ZS5tb3VudGVkICYmIGluZGljYXRvclxuICAgICAgICAgICksXG4gICAgICAgICAgY29uZGl0aW9uYWxFbGVtZW50cy5zY3JvbGxCdXR0b25SaWdodFxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gVGFicztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblRhYnMuZGVmYXVsdFByb3BzID0ge1xuICBjZW50ZXJlZDogZmFsc2UsXG4gIGZ1bGxXaWR0aDogZmFsc2UsXG4gIGluZGljYXRvckNvbG9yOiAnYWNjZW50JyxcbiAgc2Nyb2xsYWJsZTogZmFsc2UsXG4gIHNjcm9sbEJ1dHRvbnM6ICdhdXRvJyxcbiAgVGFiU2Nyb2xsQnV0dG9uOiBfVGFiU2Nyb2xsQnV0dG9uMi5kZWZhdWx0LFxuICB0ZXh0Q29sb3I6ICdpbmhlcml0J1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IHdpdGhUaGVtZTogdHJ1ZSwgbmFtZTogJ011aVRhYnMnIH0pKFRhYnMpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFicy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9UYWJzID0gcmVxdWlyZSgnLi9UYWJzJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RhYnMpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX1RhYiA9IHJlcXVpcmUoJy4vVGFiJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnVGFiJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVGFiKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaG91bGRVcGRhdGUgPSByZXF1aXJlKCcuL3Nob3VsZFVwZGF0ZScpO1xuXG52YXIgX3Nob3VsZFVwZGF0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaG91bGRVcGRhdGUpO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJy4vc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwdXJlID0gZnVuY3Rpb24gcHVyZShCYXNlQ29tcG9uZW50KSB7XG4gIHZhciBob2MgPSAoMCwgX3Nob3VsZFVwZGF0ZTIuZGVmYXVsdCkoZnVuY3Rpb24gKHByb3BzLCBuZXh0UHJvcHMpIHtcbiAgICByZXR1cm4gISgwLCBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0KShwcm9wcywgbmV4dFByb3BzKTtcbiAgfSk7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdwdXJlJykpKGhvYyhCYXNlQ29tcG9uZW50KSk7XG4gIH1cblxuICByZXR1cm4gaG9jKEJhc2VDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gcHVyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiA0IDYgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDQgNiA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIHNldFN0YXRpYyA9IGZ1bmN0aW9uIHNldFN0YXRpYyhrZXksIHZhbHVlKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgQmFzZUNvbXBvbmVudFtrZXldID0gdmFsdWU7XG4gICAgLyogZXNsaW50LWVuYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIHJldHVybiBCYXNlQ29tcG9uZW50O1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0U3RhdGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiA0IDYgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnZmJqcy9saWIvc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9zaGFsbG93RXF1YWwyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAyIDQgNiA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIHNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZFVwZGF0ZSh0ZXN0KSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIHZhciBmYWN0b3J5ID0gKDAsIF9yZWFjdC5jcmVhdGVGYWN0b3J5KShCYXNlQ29tcG9uZW50KTtcblxuICAgIHZhciBTaG91bGRVcGRhdGUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgICAgX2luaGVyaXRzKFNob3VsZFVwZGF0ZSwgX0NvbXBvbmVudCk7XG5cbiAgICAgIGZ1bmN0aW9uIFNob3VsZFVwZGF0ZSgpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNob3VsZFVwZGF0ZSk7XG5cbiAgICAgICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgICB9XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgICByZXR1cm4gdGVzdCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiBmYWN0b3J5KHRoaXMucHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgICB9KF9yZWFjdC5Db21wb25lbnQpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3Nob3VsZFVwZGF0ZScpKShTaG91bGRVcGRhdGUpO1xuICAgIH1cbiAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2hvdWxkVXBkYXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiA0IDYgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCcuLi9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE1LjQxIDE2LjA5bC00LjU4LTQuNTkgNC41OC00LjU5TDE0IDUuNWwtNiA2IDYgNnonIH0pO1xuXG52YXIgS2V5Ym9hcmRBcnJvd0xlZnQgPSBmdW5jdGlvbiBLZXlib2FyZEFycm93TGVmdChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcbktleWJvYXJkQXJyb3dMZWZ0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShLZXlib2FyZEFycm93TGVmdCk7XG5LZXlib2FyZEFycm93TGVmdC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBLZXlib2FyZEFycm93TGVmdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd0xlZnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9LZXlib2FyZEFycm93TGVmdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCcuLi9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTguNTkgMTYuMzRsNC41OC00LjU5LTQuNTgtNC41OUwxMCA1Ljc1bDYgNi02IDZ6JyB9KTtcblxudmFyIEtleWJvYXJkQXJyb3dSaWdodCA9IGZ1bmN0aW9uIEtleWJvYXJkQXJyb3dSaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcbktleWJvYXJkQXJyb3dSaWdodCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoS2V5Ym9hcmRBcnJvd1JpZ2h0KTtcbktleWJvYXJkQXJyb3dSaWdodC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBLZXlib2FyZEFycm93UmlnaHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dSaWdodC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dSaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCJcInVzZSBzdHJpY3RcIjtcclxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7IHZhbHVlOiB0cnVlIH0pO1xyXG4vLyBCYXNlZCBvbiBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtYm9vdHN0cmFwL2RvbS1oZWxwZXJzL2Jsb2IvbWFzdGVyL3NyYy91dGlsL2luRE9NLmpzXHJcbnZhciBpbkRPTSA9ICEhKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHdpbmRvdy5kb2N1bWVudCAmJiB3aW5kb3cuZG9jdW1lbnQuY3JlYXRlRWxlbWVudCk7XHJcbnZhciBjYWNoZWRUeXBlO1xyXG5mdW5jdGlvbiBfc2V0U2Nyb2xsVHlwZSh0eXBlKSB7XHJcbiAgICBjYWNoZWRUeXBlID0gdHlwZTtcclxufVxyXG5leHBvcnRzLl9zZXRTY3JvbGxUeXBlID0gX3NldFNjcm9sbFR5cGU7XHJcbi8vIEJhc2VkIG9uIHRoZSBqcXVlcnkgcGx1Z2luIGh0dHBzOi8vZ2l0aHViLmNvbS9vdGhyZWUvanF1ZXJ5LnJ0bC1zY3JvbGwtdHlwZVxyXG5mdW5jdGlvbiBkZXRlY3RTY3JvbGxUeXBlKCkge1xyXG4gICAgaWYgKGNhY2hlZFR5cGUpIHtcclxuICAgICAgICByZXR1cm4gY2FjaGVkVHlwZTtcclxuICAgIH1cclxuICAgIGlmICghaW5ET00gfHwgIXdpbmRvdy5kb2N1bWVudC5ib2R5KSB7XHJcbiAgICAgICAgcmV0dXJuICdpbmRldGVybWluYXRlJztcclxuICAgIH1cclxuICAgIHZhciBkdW1teSA9IHdpbmRvdy5kb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuICAgIGR1bW15LmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZVRleHROb2RlKCdBQkNEJykpO1xyXG4gICAgZHVtbXkuZGlyID0gJ3J0bCc7XHJcbiAgICBkdW1teS5zdHlsZS5mb250U2l6ZSA9ICcxNHB4JztcclxuICAgIGR1bW15LnN0eWxlLndpZHRoID0gJzRweCc7XHJcbiAgICBkdW1teS5zdHlsZS5oZWlnaHQgPSAnMXB4JztcclxuICAgIGR1bW15LnN0eWxlLnBvc2l0aW9uID0gJ2Fic29sdXRlJztcclxuICAgIGR1bW15LnN0eWxlLnRvcCA9ICctMTAwMHB4JztcclxuICAgIGR1bW15LnN0eWxlLm92ZXJmbG93ID0gJ3Njcm9sbCc7XHJcbiAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGR1bW15KTtcclxuICAgIGNhY2hlZFR5cGUgPSAncmV2ZXJzZSc7XHJcbiAgICBpZiAoZHVtbXkuc2Nyb2xsTGVmdCA+IDApIHtcclxuICAgICAgICBjYWNoZWRUeXBlID0gJ2RlZmF1bHQnO1xyXG4gICAgfVxyXG4gICAgZWxzZSB7XHJcbiAgICAgICAgZHVtbXkuc2Nyb2xsTGVmdCA9IDE7XHJcbiAgICAgICAgaWYgKGR1bW15LnNjcm9sbExlZnQgPT09IDApIHtcclxuICAgICAgICAgICAgY2FjaGVkVHlwZSA9ICduZWdhdGl2ZSc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChkdW1teSk7XHJcbiAgICByZXR1cm4gY2FjaGVkVHlwZTtcclxufVxyXG5leHBvcnRzLmRldGVjdFNjcm9sbFR5cGUgPSBkZXRlY3RTY3JvbGxUeXBlO1xyXG4vLyBCYXNlZCBvbiBodHRwczovL3N0YWNrb3ZlcmZsb3cuY29tL2EvMjQzOTQzNzZcclxuZnVuY3Rpb24gZ2V0Tm9ybWFsaXplZFNjcm9sbExlZnQoZWxlbWVudCwgZGlyZWN0aW9uKSB7XHJcbiAgICB2YXIgc2Nyb2xsTGVmdCA9IGVsZW1lbnQuc2Nyb2xsTGVmdDtcclxuICAgIC8vIFBlcmZvcm0gdGhlIGNhbGN1bGF0aW9ucyBvbmx5IHdoZW4gZGlyZWN0aW9uIGlzIHJ0bCB0byBhdm9pZCBtZXNzaW5nIHVwIHRoZSBsdHIgYmFoYXZpb3JcclxuICAgIGlmIChkaXJlY3Rpb24gIT09ICdydGwnKSB7XHJcbiAgICAgICAgcmV0dXJuIHNjcm9sbExlZnQ7XHJcbiAgICB9XHJcbiAgICB2YXIgdHlwZSA9IGRldGVjdFNjcm9sbFR5cGUoKTtcclxuICAgIGlmICh0eXBlID09PSAnaW5kZXRlcm1pbmF0ZScpIHtcclxuICAgICAgICByZXR1cm4gTnVtYmVyLk5hTjtcclxuICAgIH1cclxuICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICAgIGNhc2UgJ25lZ2F0aXZlJzpcclxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnQuc2Nyb2xsV2lkdGggLSBlbGVtZW50LmNsaWVudFdpZHRoICsgc2Nyb2xsTGVmdDtcclxuICAgICAgICBjYXNlICdyZXZlcnNlJzpcclxuICAgICAgICAgICAgcmV0dXJuIGVsZW1lbnQuc2Nyb2xsV2lkdGggLSBlbGVtZW50LmNsaWVudFdpZHRoIC0gc2Nyb2xsTGVmdDtcclxuICAgIH1cclxuICAgIHJldHVybiBzY3JvbGxMZWZ0O1xyXG59XHJcbmV4cG9ydHMuZ2V0Tm9ybWFsaXplZFNjcm9sbExlZnQgPSBnZXROb3JtYWxpemVkU2Nyb2xsTGVmdDtcclxuZnVuY3Rpb24gc2V0Tm9ybWFsaXplZFNjcm9sbExlZnQoZWxlbWVudCwgc2Nyb2xsTGVmdCwgZGlyZWN0aW9uKSB7XHJcbiAgICAvLyBQZXJmb3JtIHRoZSBjYWxjdWxhdGlvbnMgb25seSB3aGVuIGRpcmVjdGlvbiBpcyBydGwgdG8gYXZvaWQgbWVzc2luZyB1cCB0aGUgbHRyIGJhaGF2aW9yXHJcbiAgICBpZiAoZGlyZWN0aW9uICE9PSAncnRsJykge1xyXG4gICAgICAgIGVsZW1lbnQuc2Nyb2xsTGVmdCA9IHNjcm9sbExlZnQ7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgdmFyIHR5cGUgPSBkZXRlY3RTY3JvbGxUeXBlKCk7XHJcbiAgICBpZiAodHlwZSA9PT0gJ2luZGV0ZXJtaW5hdGUnKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG4gICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgY2FzZSAnbmVnYXRpdmUnOlxyXG4gICAgICAgICAgICBlbGVtZW50LnNjcm9sbExlZnQgPSBlbGVtZW50LmNsaWVudFdpZHRoIC0gZWxlbWVudC5zY3JvbGxXaWR0aCArIHNjcm9sbExlZnQ7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGNhc2UgJ3JldmVyc2UnOlxyXG4gICAgICAgICAgICBlbGVtZW50LnNjcm9sbExlZnQgPSBlbGVtZW50LnNjcm9sbFdpZHRoIC0gZWxlbWVudC5jbGllbnRXaWR0aCAtIHNjcm9sbExlZnQ7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIGVsZW1lbnQuc2Nyb2xsTGVmdCA9IHNjcm9sbExlZnQ7XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgfVxyXG59XHJcbmV4cG9ydHMuc2V0Tm9ybWFsaXplZFNjcm9sbExlZnQgPSBzZXROb3JtYWxpemVkU2Nyb2xsTGVmdDtcclxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLXNjcm9sbC1sZWZ0L2xpYi9tYWluLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9ub3JtYWxpemUtc2Nyb2xsLWxlZnQvbGliL21haW4uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwidmFyIGdsb2JhbCA9IHJlcXVpcmUoJ2dsb2JhbCcpXG5cbi8qKlxuICogYHJlcXVlc3RBbmltYXRpb25GcmFtZSgpYFxuICovXG5cbnZhciByZXF1ZXN0ID0gZ2xvYmFsLnJlcXVlc3RBbmltYXRpb25GcmFtZVxuICB8fCBnbG9iYWwud2Via2l0UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gIHx8IGdsb2JhbC5tb3pSZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgfHwgZmFsbGJhY2tcblxudmFyIHByZXYgPSArbmV3IERhdGVcbmZ1bmN0aW9uIGZhbGxiYWNrIChmbikge1xuICB2YXIgY3VyciA9ICtuZXcgRGF0ZVxuICB2YXIgbXMgPSBNYXRoLm1heCgwLCAxNiAtIChjdXJyIC0gcHJldikpXG4gIHZhciByZXEgPSBzZXRUaW1lb3V0KGZuLCBtcylcbiAgcmV0dXJuIHByZXYgPSBjdXJyLCByZXFcbn1cblxuLyoqXG4gKiBgY2FuY2VsQW5pbWF0aW9uRnJhbWUoKWBcbiAqL1xuXG52YXIgY2FuY2VsID0gZ2xvYmFsLmNhbmNlbEFuaW1hdGlvbkZyYW1lXG4gIHx8IGdsb2JhbC53ZWJraXRDYW5jZWxBbmltYXRpb25GcmFtZVxuICB8fCBnbG9iYWwubW96Q2FuY2VsQW5pbWF0aW9uRnJhbWVcbiAgfHwgY2xlYXJUaW1lb3V0XG5cbmlmIChGdW5jdGlvbi5wcm90b3R5cGUuYmluZCkge1xuICByZXF1ZXN0ID0gcmVxdWVzdC5iaW5kKGdsb2JhbClcbiAgY2FuY2VsID0gY2FuY2VsLmJpbmQoZ2xvYmFsKVxufVxuXG5leHBvcnRzID0gbW9kdWxlLmV4cG9ydHMgPSByZXF1ZXN0XG5leHBvcnRzLmNhbmNlbCA9IGNhbmNlbFxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmFmbC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmFmbC9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuXHR2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX3JlYWN0RXZlbnRMaXN0ZW5lciA9IHJlcXVpcmUoJ3JlYWN0LWV2ZW50LWxpc3RlbmVyJyk7XG5cbnZhciBfcmVhY3RFdmVudExpc3RlbmVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0RXZlbnRMaXN0ZW5lcik7XG5cbnZhciBfc3RpZmxlID0gcmVxdWlyZSgnc3RpZmxlJyk7XG5cbnZhciBfc3RpZmxlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3N0aWZsZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzdHlsZXMgPSB7XG5cdHdpZHRoOiAnMTAwcHgnLFxuXHRoZWlnaHQ6ICcxMDBweCcsXG5cdHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxuXHR0b3A6ICctMTAwMDAwcHgnLFxuXHRvdmVyZmxvdzogJ3Njcm9sbCcsXG5cdG1zT3ZlcmZsb3dTdHlsZTogJ3Njcm9sbGJhcidcbn07XG5cbnZhciBTY3JvbGxiYXJTaXplID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcblx0KDAsIF9pbmhlcml0czMuZGVmYXVsdCkoU2Nyb2xsYmFyU2l6ZSwgX0NvbXBvbmVudCk7XG5cblx0ZnVuY3Rpb24gU2Nyb2xsYmFyU2l6ZSgpIHtcblx0XHR2YXIgX3JlZjtcblxuXHRcdHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cblx0XHQoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBTY3JvbGxiYXJTaXplKTtcblxuXHRcdGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG5cdFx0XHRhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuXHRcdH1cblxuXHRcdHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IFNjcm9sbGJhclNpemUuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFNjcm9sbGJhclNpemUpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5zZXRNZWFzdXJlbWVudHMgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRfdGhpcy5zY3JvbGxiYXJIZWlnaHQgPSBfdGhpcy5ub2RlLm9mZnNldEhlaWdodCAtIF90aGlzLm5vZGUuY2xpZW50SGVpZ2h0O1xuXHRcdFx0X3RoaXMuc2Nyb2xsYmFyV2lkdGggPSBfdGhpcy5ub2RlLm9mZnNldFdpZHRoIC0gX3RoaXMubm9kZS5jbGllbnRXaWR0aDtcblx0XHR9LCBfdGhpcy5oYW5kbGVSZXNpemUgPSAoMCwgX3N0aWZsZTIuZGVmYXVsdCkoZnVuY3Rpb24gKCkge1xuXHRcdFx0dmFyIG9uQ2hhbmdlID0gX3RoaXMucHJvcHMub25DaGFuZ2U7XG5cblxuXHRcdFx0dmFyIHByZXZIZWlnaHQgPSBfdGhpcy5zY3JvbGxiYXJIZWlnaHQ7XG5cdFx0XHR2YXIgcHJldldpZHRoID0gX3RoaXMuc2Nyb2xsYmFyV2lkdGg7XG5cdFx0XHRfdGhpcy5zZXRNZWFzdXJlbWVudHMoKTtcblx0XHRcdGlmIChwcmV2SGVpZ2h0ICE9PSBfdGhpcy5zY3JvbGxiYXJIZWlnaHQgfHwgcHJldldpZHRoICE9PSBfdGhpcy5zY3JvbGxiYXJXaWR0aCkge1xuXHRcdFx0XHRvbkNoYW5nZSh7IHNjcm9sbGJhckhlaWdodDogX3RoaXMuc2Nyb2xsYmFySGVpZ2h0LCBzY3JvbGxiYXJXaWR0aDogX3RoaXMuc2Nyb2xsYmFyV2lkdGggfSk7XG5cdFx0XHR9XG5cdFx0fSwgMTY2KSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcblx0fVxuXG5cdCgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFNjcm9sbGJhclNpemUsIFt7XG5cdFx0a2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuXHRcdHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcblx0XHRcdHZhciBvbkxvYWQgPSB0aGlzLnByb3BzLm9uTG9hZDtcblxuXG5cdFx0XHRpZiAob25Mb2FkKSB7XG5cdFx0XHRcdHRoaXMuc2V0TWVhc3VyZW1lbnRzKCk7XG5cdFx0XHRcdG9uTG9hZCh7IHNjcm9sbGJhckhlaWdodDogdGhpcy5zY3JvbGxiYXJIZWlnaHQsIHNjcm9sbGJhcldpZHRoOiB0aGlzLnNjcm9sbGJhcldpZHRoIH0pO1xuXHRcdFx0fVxuXHRcdH1cblx0fSwge1xuXHRcdGtleTogJ2NvbXBvbmVudFdpbGxVbm1vdW50Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVubW91bnQoKSB7XG5cdFx0XHR0aGlzLmhhbmRsZVJlc2l6ZS5jYW5jZWwoKTtcblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdyZW5kZXInLFxuXHRcdC8vIENvcnJlc3BvbmRzIHRvIDEwIGZyYW1lcyBhdCA2MCBIei5cblxuXHRcdHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG5cdFx0XHR2YXIgX3RoaXMyID0gdGhpcztcblxuXHRcdFx0dmFyIG9uQ2hhbmdlID0gdGhpcy5wcm9wcy5vbkNoYW5nZTtcblxuXG5cdFx0XHRyZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG5cdFx0XHRcdCdkaXYnLFxuXHRcdFx0XHRudWxsLFxuXHRcdFx0XHRvbkNoYW5nZSA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9yZWFjdEV2ZW50TGlzdGVuZXIyLmRlZmF1bHQsIHsgdGFyZ2V0OiAnd2luZG93Jywgb25SZXNpemU6IHRoaXMuaGFuZGxlUmVzaXplIH0pIDogbnVsbCxcblx0XHRcdFx0X3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2RpdicsIHtcblx0XHRcdFx0XHRzdHlsZTogc3R5bGVzLFxuXHRcdFx0XHRcdHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcblx0XHRcdFx0XHRcdF90aGlzMi5ub2RlID0gbm9kZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH0pXG5cdFx0XHQpO1xuXHRcdH1cblx0fV0pO1xuXHRyZXR1cm4gU2Nyb2xsYmFyU2l6ZTtcbn0oX3JlYWN0LkNvbXBvbmVudCk7XG5cblNjcm9sbGJhclNpemUuZGVmYXVsdFByb3BzID0ge1xuXHRvbkxvYWQ6IG51bGwsXG5cdG9uQ2hhbmdlOiBudWxsXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gU2Nyb2xsYmFyU2l6ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9TY3JvbGxiYXJTaXplLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9TY3JvbGxiYXJTaXplLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9TY3JvbGxiYXJTaXplID0gcmVxdWlyZSgnLi9TY3JvbGxiYXJTaXplJyk7XG5cbnZhciBfU2Nyb2xsYmFyU2l6ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TY3JvbGxiYXJTaXplKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gX1Njcm9sbGJhclNpemUyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2Nyb2xsYmFyLXNpemUvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXNjcm9sbGJhci1zaXplL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsInZhciByYWYgPSByZXF1aXJlKCdyYWZsJylcblxuZnVuY3Rpb24gc2Nyb2xsIChwcm9wLCBlbGVtZW50LCB0bywgb3B0aW9ucywgY2FsbGJhY2spIHtcbiAgdmFyIHN0YXJ0ID0gK25ldyBEYXRlXG4gIHZhciBmcm9tID0gZWxlbWVudFtwcm9wXVxuICB2YXIgY2FuY2VsbGVkID0gZmFsc2VcblxuICB2YXIgZWFzZSA9IGluT3V0U2luZVxuICB2YXIgZHVyYXRpb24gPSAzNTBcblxuICBpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICBjYWxsYmFjayA9IG9wdGlvbnNcbiAgfVxuICBlbHNlIHtcbiAgICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fVxuICAgIGVhc2UgPSBvcHRpb25zLmVhc2UgfHwgZWFzZVxuICAgIGR1cmF0aW9uID0gb3B0aW9ucy5kdXJhdGlvbiB8fCBkdXJhdGlvblxuICAgIGNhbGxiYWNrID0gY2FsbGJhY2sgfHwgZnVuY3Rpb24gKCkge31cbiAgfVxuXG4gIGlmIChmcm9tID09PSB0bykge1xuICAgIHJldHVybiBjYWxsYmFjayhcbiAgICAgIG5ldyBFcnJvcignRWxlbWVudCBhbHJlYWR5IGF0IHRhcmdldCBzY3JvbGwgcG9zaXRpb24nKSxcbiAgICAgIGVsZW1lbnRbcHJvcF1cbiAgICApXG4gIH1cblxuICBmdW5jdGlvbiBjYW5jZWwgKCkge1xuICAgIGNhbmNlbGxlZCA9IHRydWVcbiAgfVxuXG4gIGZ1bmN0aW9uIGFuaW1hdGUgKHRpbWVzdGFtcCkge1xuICAgIGlmIChjYW5jZWxsZWQpIHtcbiAgICAgIHJldHVybiBjYWxsYmFjayhcbiAgICAgICAgbmV3IEVycm9yKCdTY3JvbGwgY2FuY2VsbGVkJyksXG4gICAgICAgIGVsZW1lbnRbcHJvcF1cbiAgICAgIClcbiAgICB9XG5cbiAgICB2YXIgbm93ID0gK25ldyBEYXRlXG4gICAgdmFyIHRpbWUgPSBNYXRoLm1pbigxLCAoKG5vdyAtIHN0YXJ0KSAvIGR1cmF0aW9uKSlcbiAgICB2YXIgZWFzZWQgPSBlYXNlKHRpbWUpXG5cbiAgICBlbGVtZW50W3Byb3BdID0gKGVhc2VkICogKHRvIC0gZnJvbSkpICsgZnJvbVxuXG4gICAgdGltZSA8IDEgPyByYWYoYW5pbWF0ZSkgOiByYWYoZnVuY3Rpb24gKCkge1xuICAgICAgY2FsbGJhY2sobnVsbCwgZWxlbWVudFtwcm9wXSlcbiAgICB9KVxuICB9XG5cbiAgcmFmKGFuaW1hdGUpXG5cbiAgcmV0dXJuIGNhbmNlbFxufVxuXG5mdW5jdGlvbiBpbk91dFNpbmUgKG4pIHtcbiAgcmV0dXJuIC41ICogKDEgLSBNYXRoLmNvcyhNYXRoLlBJICogbikpXG59XG5cbm1vZHVsZS5leHBvcnRzID0ge1xuICB0b3A6IGZ1bmN0aW9uIChlbGVtZW50LCB0bywgb3B0aW9ucywgY2FsbGJhY2spIHtcbiAgICByZXR1cm4gc2Nyb2xsKCdzY3JvbGxUb3AnLCBlbGVtZW50LCB0bywgb3B0aW9ucywgY2FsbGJhY2spXG4gIH0sXG4gIGxlZnQ6IGZ1bmN0aW9uIChlbGVtZW50LCB0bywgb3B0aW9ucywgY2FsbGJhY2spIHtcbiAgICByZXR1cm4gc2Nyb2xsKCdzY3JvbGxMZWZ0JywgZWxlbWVudCwgdG8sIG9wdGlvbnMsIGNhbGxiYWNrKVxuICB9XG59XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9zY3JvbGwvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3Njcm9sbC9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCJtb2R1bGUuZXhwb3J0cyA9IHN0aWZsZTtcblxuXG5mdW5jdGlvbiBzdGlmbGUgKGZuLCB3YWl0KSB7XG4gIGlmICh0eXBlb2YgZm4gIT09ICdmdW5jdGlvbicgfHwgdHlwZW9mIHdhaXQgIT09ICdudW1iZXInKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdzdGlmbGUoZm4sIHdhaXQpIC0tIGV4cGVjdGVkIGEgZnVuY3Rpb24gYW5kIG51bWJlciBvZiBtaWxsaXNlY29uZHMsIGdvdCAoJyArIHR5cGVvZiBmbiArICcsICcgKyB0eXBlb2Ygd2FpdCArICcpJyk7XG4gIH1cblxuICB2YXIgdGltZXI7ICAgIC8vIFRpbWVyIHRvIGZpcmUgYWZ0ZXIgYHdhaXRgIGhhcyBlbGFwc2VkXG4gIHZhciBjYWxsZWQ7ICAgLy8gS2VlcCB0cmFjayBpZiBpdCBnZXRzIGNhbGxlZCBkdXJpbmcgdGhlIGB3YWl0YFxuXG4gIHZhciB3cmFwcGVyID0gZnVuY3Rpb24gKCkge1xuXG4gICAgLy8gQ2hlY2sgaWYgc3RpbGwgXCJjb29saW5nIGRvd25cIiBmcm9tIGEgcHJldmlvdXMgY2FsbFxuICAgIGlmICh0aW1lcikge1xuICAgICAgY2FsbGVkID0gdHJ1ZTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gU3RhcnQgYSB0aW1lciB0byBmaXJlIGFmdGVyIHRoZSBgd2FpdGAgaXMgb3ZlclxuICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KGFmdGVyV2FpdCwgd2FpdCk7XG4gICAgICAvLyBBbmQgY2FsbCB0aGUgd3JhcHBlZCBmdW5jdGlvblxuICAgICAgZm4oKTtcbiAgICB9XG4gIH1cblxuICAvLyBBZGQgYSBjYW5jZWwgbWV0aG9kLCB0byBraWxsIGFuZCBwZW5kaW5nIGNhbGxzXG4gIHdyYXBwZXIuY2FuY2VsID0gZnVuY3Rpb24gKCkge1xuICAgIC8vIENsZWFyIHRoZSBjYWxsZWQgZmxhZywgb3IgaXQgd291bGQgZmlyZSB0d2ljZSB3aGVuIGNhbGxlZCBhZ2FpbiBsYXRlclxuICAgIGNhbGxlZCA9IGZhbHNlO1xuXG4gICAgLy8gVHVybiBvZmYgdGhlIHRpbWVyLCBzbyBpdCB3b24ndCBmaXJlIGFmdGVyIHRoZSB3YWl0IGV4cGlyZXNcbiAgICBpZiAodGltZXIpIHtcbiAgICAgIGNsZWFyVGltZW91dCh0aW1lcik7XG4gICAgICB0aW1lciA9IHVuZGVmaW5lZDtcbiAgICB9XG4gIH1cblxuICBmdW5jdGlvbiBhZnRlcldhaXQoKSB7XG4gICAgLy8gRW1wdHkgb3V0IHRoZSB0aW1lclxuICAgIHRpbWVyID0gdW5kZWZpbmVkO1xuXG4gICAgLy8gSWYgaXQgd2FzIGNhbGxlZCBkdXJpbmcgdGhlIGB3YWl0YCwgZmlyZSBpdCBhZ2FpblxuICAgIGlmIChjYWxsZWQpIHtcbiAgICAgIGNhbGxlZCA9IGZhbHNlO1xuICAgICAgd3JhcHBlcigpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB3cmFwcGVyO1xufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc3RpZmxlL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9zdGlmbGUvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiLyoqIEBmb3JtYXQgKi8gLyoqXG4gKiBzZWFyY2ggY29tcG9uZW50XG4gKiBUaGlzIGNvbXBvbmVudCBmaWx0ZXJzIGFsbCB0aGUgcG9zdHMgYW5kIGNvdXJzZXMgYW5kIGVsZW1lbnRcbiAqIGFuZCBkaXNwbGF5IGluIGRpZmZlcmVudCB0YWJzXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuaW1wb3J0IEFwcEJhciBmcm9tICdtYXRlcmlhbC11aS9BcHBCYXInO1xuaW1wb3J0IFRhYnMsIHsgVGFiIH0gZnJvbSAnbWF0ZXJpYWwtdWkvVGFicyc7XG5pbXBvcnQgQ2FyZCwgeyBDYXJkSGVhZGVyIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5cbi8vIGltcG9ydCBhY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBmbGV4OiAnMSAwIDEwMCUnLFxuICB9LFxufTtcblxuLy8gZm9yIGRpc3BsYXlpbmcgRWxlbWVudFxuY29uc3QgRWxlbWVudENhcmQgPSAoe1xuICB0eXBlLFxuICBlbGVtZW50VHlwZSxcbiAgaWQsXG4gIHVzZXJuYW1lLFxuICBuYW1lLFxuICBndXJ1LFxuICBjbGFzc3Jvb20sXG59KSA9PiAoXG4gIDxkaXYgc3R5bGU9e3sgcGFkZGluZzogJzElJyB9fT5cbiAgICA8Q2FyZCByYWlzZWQ+XG4gICAgICA8Q2FyZEhlYWRlciB0aXRsZT17bmFtZX0gc3ViaGVhZGVyPXtgQCR7dXNlcm5hbWV9YH0gLz5cbiAgICA8L0NhcmQ+XG4gIDwvZGl2PlxuKTtcblxuRWxlbWVudENhcmQucHJvcFR5cGVzID0ge1xuICB0eXBlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIGVsZW1lbnRUeXBlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIGlkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIG5hbWU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgZ3VydTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgY2xhc3Nyb29tOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxufTtcblxuLy8gZm9yIGRpc3BsYXlpbmcgcG9zdHNcbmNvbnN0IFBvc3RDYXJkID0gKHsgdHlwZSwgcG9zdFR5cGUsIHBvc3RJZCwgdGl0bGUsIGRlc2NyaXB0aW9uIH0pID0+IChcbiAgPGRpdiBzdHlsZT17eyBwYWRkaW5nOiAnMSUnIH19PlxuICAgIDxDYXJkIHJhaXNlZD5cbiAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPXt0aXRsZX0gc3ViaGVhZGVyPXtkZXNjcmlwdGlvbn0gLz5cbiAgICA8L0NhcmQ+XG4gIDwvZGl2PlxuKTtcblxuUG9zdENhcmQucHJvcFR5cGVzID0ge1xuICB0eXBlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIHBvc3RUeXBlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBkZXNjcmlwdGlvbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxufTtcblxuLy8gZm9yIGRpc3BsYXlpbmcgY291cnNlXG5jb25zdCBDb3Vyc2VDYXJkID0gKHsgdHlwZSwgcG9zdFR5cGUsIGNvdXJzZUlkLCB0aXRsZSwgZGVzY3JpcHRpb24gfSkgPT4gKFxuICA8ZGl2IHN0eWxlPXt7IHBhZGRpbmc6ICcxJScgfX0+XG4gICAgPENhcmQgcmFpc2VkPlxuICAgICAgPENhcmRIZWFkZXIgdGl0bGU9e3RpdGxlfSBzdWJoZWFkZXI9e2Rlc2NyaXB0aW9ufSAvPlxuICAgIDwvQ2FyZD5cbiAgPC9kaXY+XG4pO1xuXG5Db3Vyc2VDYXJkLnByb3BUeXBlcyA9IHtcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBwb3N0VHlwZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBjb3Vyc2VJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBkZXNjcmlwdGlvbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxufTtcblxuLy8gZm9yIGRpc3BsYXlpbmcgVGltZWxpbmUgKFRpbWVsaW5lIGNvbnRhaW5zIHBvc3QsIGNvdXJzZSwgZWxlbWVudClcbmNvbnN0IFRpbWVsaW5lID0gKHsgZGF0YSB9KSA9PiAoXG4gIDxkaXY+XG4gICAge2RhdGEubWFwKChlLCBpKSA9PiB7XG4gICAgICBpZiAoZS50eXBlID09PSAnZWxlbWVudCcpIHtcbiAgICAgICAgcmV0dXJuIDxFbGVtZW50Q2FyZCBrZXk9e2UuaWR9IHsuLi5lfSAvPjtcbiAgICAgIH1cbiAgICAgIGlmIChlLnR5cGUgPT09ICdwb3N0Jykge1xuICAgICAgICByZXR1cm4gPFBvc3RDYXJkIGtleT17ZS5wb3N0SWR9IHsuLi5lfSAvPjtcbiAgICAgIH1cbiAgICAgIGlmIChlLnR5cGUgPT09ICdjb3Vyc2UnKSB7XG4gICAgICAgIHJldHVybiA8UG9zdENhcmQga2V5PXtlLmNvdXJzZUlkfSB7Li4uZX0gLz47XG4gICAgICB9XG4gICAgICByZXR1cm4gPGRpdiBrZXk9e2kgKyAxfT57ZS50eXBlfTwvZGl2PjtcbiAgICB9KX1cbiAgPC9kaXY+XG4pO1xuXG5UaW1lbGluZS5wcm9wVHlwZXMgPSB7XG4gIGRhdGE6IFByb3BUeXBlcy5hcnJheS5pc1JlcXVpcmVkLFxufTtcblxuY2xhc3MgU2VhcmNoIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHZhbHVlOiAwLFxuICAgICAgZGF0YTogW1xuICAgICAgICB7XG4gICAgICAgICAgdHlwZTogJ2VsZW1lbnQnLFxuICAgICAgICAgIGVsZW1lbnRUeXBlOiAndXNlcicsXG4gICAgICAgICAgaWQ6IDEsXG4gICAgICAgICAgdXNlcm5hbWU6ICdoYmFydmUxJyxcbiAgICAgICAgICBuYW1lOiAnSGltYW5rIEJhcnZlJyxcbiAgICAgICAgICBndXJ1OiB0cnVlLFxuICAgICAgICAgIGNsYXNzcm9vbTogdHJ1ZSxcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHR5cGU6ICdlbGVtZW50JyxcbiAgICAgICAgICBlbGVtZW50VHlwZTogJ3VzZXInLFxuICAgICAgICAgIGlkOiAyLFxuICAgICAgICAgIHVzZXJuYW1lOiAnc2h1YmhhbScsXG4gICAgICAgICAgbmFtZTogJ1NodWJoYW0gTWF1cnlhJyxcbiAgICAgICAgICBndXJ1OiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdHlwZTogJ2VsZW1lbnQnLFxuICAgICAgICAgIGVsZW1lbnRUeXBlOiAndXNlcicsXG4gICAgICAgICAgaWQ6IDMsXG4gICAgICAgICAgdXNlcm5hbWU6ICdkZWVwYW5zaCcsXG4gICAgICAgICAgbmFtZTogJ0RlZXBhbnNoIEJoYXJnYXdhJyxcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHR5cGU6ICdlbGVtZW50JyxcbiAgICAgICAgICBlbGVtZW50VHlwZTogJ2NpcmNsZScsXG4gICAgICAgICAgY2lyY2xlVHlwZTogJ2VkdScsXG4gICAgICAgICAgaWQ6IDQsXG4gICAgICAgICAgdXNlcm5hbWU6ICdpaXRkaG4nLFxuICAgICAgICAgIG5hbWU6ICdJbmRpYW4gSW5zdGl0dXRlIG9mIFRlY2hub2xvZ3koSVNNKSwgRGhhbmJhZCcsXG4gICAgICAgICAgY2xhc3Nyb29tOiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdHlwZTogJ2VsZW1lbnQnLFxuICAgICAgICAgIGVsZW1lbnRUeXBlOiAnY2lyY2xlJyxcbiAgICAgICAgICBjaXJjbGVUeXBlOiAnZmllbGQnLFxuICAgICAgICAgIGlkOiA1LFxuICAgICAgICAgIHVzZXJuYW1lOiAnamF2YXNjcmlwdCcsXG4gICAgICAgICAgbmFtZTogJ0phdmFTY3JpcHQnLFxuICAgICAgICAgIGNsYXNzcm9vbTogdHJ1ZSxcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHR5cGU6ICdlbGVtZW50JyxcbiAgICAgICAgICBlbGVtZW50VHlwZTogJ2NpcmNsZScsXG4gICAgICAgICAgY2lyY2xlVHlwZTogJ29yZycsXG4gICAgICAgICAgaWQ6IDYsXG4gICAgICAgICAgdXNlcm5hbWU6ICdnb29nbGUnLFxuICAgICAgICAgIG5hbWU6ICdHb29nbGUgSW5jLicsXG4gICAgICAgICAgY2xhc3Nyb29tOiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdHlwZTogJ3Bvc3QnLFxuICAgICAgICAgIHBvc3RUeXBlOiAnYXJ0aWNsZScsXG4gICAgICAgICAgcG9zdElkOiAxLFxuICAgICAgICAgIGF1dGhvcklkOiAxLFxuICAgICAgICAgIHRpdGxlOiAnVGhpcyBpcyBGaXJzdCBQb3N0JyxcbiAgICAgICAgICBkZXNjcmlwdGlvbjogJ1RoaXMgaXMgRmlyc3QgUG9zdCBkZXNjcmlwdGlvbi4nLFxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdHlwZTogJ3Bvc3QnLFxuICAgICAgICAgIHBvc3RUeXBlOiAnYXJ0aWNsZScsXG4gICAgICAgICAgcG9zdElkOiAyLFxuICAgICAgICAgIGF1dGhvcklkOiAxLFxuICAgICAgICAgIHRpdGxlOiAnVGhpcyBpcyBTZWNvbmQgUG9zdCcsXG4gICAgICAgICAgZGVzY3JpcHRpb246ICdUaGlzIGlzIFNlY29uZCBQb3N0IGRlc2NyaXB0aW9uLicsXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICB0eXBlOiAnY291cnNlJyxcbiAgICAgICAgICBjb3Vyc2VJZDogMSxcbiAgICAgICAgICBhdXRob3JJZDogMSxcbiAgICAgICAgICB0aXRsZTogJ1RoaXMgaXMgRmlyc3QgQ291cnNlJyxcbiAgICAgICAgICBkZXNjcmlwdGlvbjogJ1RoaXMgaXMgRmlyc3QgQ291cnNlIGRlc2NyaXB0aW9uLicsXG4gICAgICAgIH0sXG4gICAgICAgIHtcbiAgICAgICAgICB0eXBlOiAnY291cnNlJyxcbiAgICAgICAgICBjb3Vyc2VJZDogMixcbiAgICAgICAgICBhdXRob3JJZDogMSxcbiAgICAgICAgICB0aXRsZTogJ1RoaXMgaXMgU2Vjb25kIENvdXJzZScsXG4gICAgICAgICAgZGVzY3JpcHRpb246ICdUaGlzIGlzIEZpcnN0IENvdXJzZSBkZXNjcmlwdGlvbi4nLFxuICAgICAgICB9LFxuICAgICAgXSxcbiAgICB9O1xuICB9XG5cbiAgaGFuZGxlQ2hhbmdlKGV2ZW50LCB2YWx1ZSkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyB2YWx1ZSB9KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCB7IHZhbHVlLCBkYXRhIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8QXBwQmFyIHBvc2l0aW9uPVwic3RhdGljXCIgY29sb3I9XCJkZWZhdWx0XCI+XG4gICAgICAgICAgPFRhYnNcbiAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLmhhbmRsZUNoYW5nZX1cbiAgICAgICAgICAgIGluZGljYXRvckNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICB0ZXh0Q29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgIHNjcm9sbGFibGVcbiAgICAgICAgICAgIHNjcm9sbEJ1dHRvbnM9XCJhdXRvXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiQWxsXCIgLz5cbiAgICAgICAgICAgIDxUYWIgbGFiZWw9XCJVc2Vyc1wiIC8+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiQ2lyY2xlc1wiIC8+XG4gICAgICAgICAgICA8VGFiIGxhYmVsPVwiUG9zdHNcIiAvPlxuICAgICAgICAgICAgPFRhYiBsYWJlbD1cIkNvdXJzZXNcIiAvPlxuICAgICAgICAgICAgPFRhYiBsYWJlbD1cIkNsYXNzcm9vbXNcIiAvPlxuICAgICAgICAgIDwvVGFicz5cbiAgICAgICAgPC9BcHBCYXI+XG4gICAgICAgIHsvKiBUaGlzIHdpbGwgc2hvdyBhbGwgc2VhcmNoZWQgZWxlbWVudCAqL31cbiAgICAgICAge3ZhbHVlID09PSAwICYmIDxUaW1lbGluZSBkYXRhPXtkYXRhfSAvPn1cbiAgICAgICAgey8qIFRoaXMgd2lsbCBzaG93IGZpbHRlcmVkIHNlYXJjaGVkIGVsZW1lbnQgb2YgZWxlbWVudFR5cGUgKi99XG4gICAgICAgIHt2YWx1ZSA9PT0gMSAmJiAoXG4gICAgICAgICAgPFRpbWVsaW5lXG4gICAgICAgICAgICBkYXRhPXtkYXRhLmZpbHRlcihcbiAgICAgICAgICAgICAgKGQpID0+IGQudHlwZSA9PT0gJ2VsZW1lbnQnICYmIGQuZWxlbWVudFR5cGUgPT09ICd1c2VyJyxcbiAgICAgICAgICAgICl9XG4gICAgICAgICAgLz5cbiAgICAgICAgKX1cbiAgICAgICAge3ZhbHVlID09PSAyICYmIChcbiAgICAgICAgICA8VGltZWxpbmVcbiAgICAgICAgICAgIGRhdGE9e2RhdGEuZmlsdGVyKFxuICAgICAgICAgICAgICAoZCkgPT4gZC50eXBlID09PSAnZWxlbWVudCcgJiYgZC5lbGVtZW50VHlwZSA9PT0gJ2NpcmNsZScsXG4gICAgICAgICAgICApfVxuICAgICAgICAgIC8+XG4gICAgICAgICl9XG4gICAgICAgIHt2YWx1ZSA9PT0gMyAmJiAoXG4gICAgICAgICAgPFRpbWVsaW5lIGRhdGE9e2RhdGEuZmlsdGVyKChkKSA9PiBkLnR5cGUgPT09ICdwb3N0Jyl9IC8+XG4gICAgICAgICl9XG4gICAgICAgIHt2YWx1ZSA9PT0gNCAmJiAoXG4gICAgICAgICAgPFRpbWVsaW5lIGRhdGE9e2RhdGEuZmlsdGVyKChkKSA9PiBkLnR5cGUgPT09ICdjb3Vyc2UnKX0gLz5cbiAgICAgICAgKX1cbiAgICAgICAge3ZhbHVlID09PSA1ICYmIChcbiAgICAgICAgICA8VGltZWxpbmVcbiAgICAgICAgICAgIGRhdGE9e2RhdGEuZmlsdGVyKFxuICAgICAgICAgICAgICAoZCkgPT4gZC50eXBlID09PSAnZWxlbWVudCcgJiYgZC5jbGFzc3Jvb20gPT09IHRydWUsXG4gICAgICAgICAgICApfVxuICAgICAgICAgIC8+XG4gICAgICAgICl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblNlYXJjaC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IGVsZW1lbnRzIH0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+IGJpbmRBY3Rpb25DcmVhdG9ycyh7fSwgZGlzcGF0Y2gpO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKFNlYXJjaCksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1NlYXJjaC9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=