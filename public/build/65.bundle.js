webpackJsonp([65],{

/***/ "./node_modules/babel-runtime/core-js/object/values.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/values.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/values */ "./node_modules/core-js/library/fn/object/values.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/values.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/values.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../modules/es7.object.values */ "./node_modules/core-js/library/modules/es7.object.values.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.values;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-to-array.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-to-array.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var isEnum = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) if (isEnum.call(O, key = keys[i++])) {
      result.push(isEntries ? [key, O[key]] : O[key]);
    } return result;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.object.values.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.object.values.js ***!
  \*******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var $values = __webpack_require__(/*! ./_object-to-array */ "./node_modules/core-js/library/modules/_object-to-array.js")(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ }),

/***/ "./src/client/containers/CoursePage/Discussion/QuestionTimeline.js":
/*!*************************************************************************!*\
  !*** ./src/client/containers/CoursePage/Discussion/QuestionTimeline.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {
    // padding: '1%',
  },
  gridItem: {
    paddingTop: '1%',
    paddingLeft: '1%',
    paddingRight: '1%'
  }
};

var AsyncQuestionCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(63).then(__webpack_require__.bind(null, /*! ./QuestionCreate.js */ "./src/client/containers/CoursePage/Discussion/QuestionCreate.js"));
  },
  modules: ['./QuestionCreate'],
  loading: _Loading2.default
});

var AsyncQuestionCard = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(33).then(__webpack_require__.bind(null, /*! ./QuestionCard.js */ "./src/client/containers/CoursePage/Discussion/QuestionCard.js"));
  },
  modules: ['./QuestionCard'],
  loading: _Loading2.default
});

var QuestionTimeline = function (_Component) {
  (0, _inherits3.default)(QuestionTimeline, _Component);

  function QuestionTimeline(props) {
    (0, _classCallCheck3.default)(this, QuestionTimeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (QuestionTimeline.__proto__ || (0, _getPrototypeOf2.default)(QuestionTimeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(QuestionTimeline, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          course = _props.course;
      var isSignedIn = user.isSignedIn;
      var discussion = course.discussion;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        isSignedIn && _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 8,
            xl: 8,
            className: classes.gridItem
          },
          _react2.default.createElement(AsyncQuestionCreate, { course: course })
        ),
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 8,
            xl: 8,
            className: classes.gridItem
          },
          typeof discussion !== 'undefined' && (0, _values2.default)(discussion).map(function (q) {
            return _react2.default.createElement(AsyncQuestionCard, (0, _extends3.default)({ key: q.questionId }, q));
          })
        )
      );
    }
  }]);
  return QuestionTimeline;
}(_react.Component);

QuestionTimeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(QuestionTimeline);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC92YWx1ZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczcub2JqZWN0LnZhbHVlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9EaXNjdXNzaW9uL1F1ZXN0aW9uVGltZWxpbmUuanMiXSwibmFtZXMiOlsic3R5bGVzIiwicm9vdCIsImdyaWRJdGVtIiwicGFkZGluZ1RvcCIsInBhZGRpbmdMZWZ0IiwicGFkZGluZ1JpZ2h0IiwiQXN5bmNRdWVzdGlvbkNyZWF0ZSIsImxvYWRlciIsIm1vZHVsZXMiLCJsb2FkaW5nIiwiQXN5bmNRdWVzdGlvbkNhcmQiLCJRdWVzdGlvblRpbWVsaW5lIiwicHJvcHMiLCJzdGF0ZSIsImNsYXNzZXMiLCJ1c2VyIiwiY291cnNlIiwiaXNTaWduZWRJbiIsImRpc2N1c3Npb24iLCJtYXAiLCJxIiwicXVlc3Rpb25JZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxrQkFBa0Isa0o7Ozs7Ozs7Ozs7OztBQ0FsQjtBQUNBOzs7Ozs7Ozs7Ozs7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2ZBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05EOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBVkE7O0FBWUEsSUFBTUEsU0FBUztBQUNiQyxRQUFNO0FBQ0o7QUFESSxHQURPO0FBSWJDLFlBQVU7QUFDUkMsZ0JBQVksSUFESjtBQUVSQyxpQkFBYSxJQUZMO0FBR1JDLGtCQUFjO0FBSE47QUFKRyxDQUFmOztBQVdBLElBQU1DLHNCQUFzQiw2QkFBUztBQUNuQ0MsVUFBUTtBQUFBLFdBQU0sMEtBQU47QUFBQSxHQUQyQjtBQUVuQ0MsV0FBUyxDQUFDLGtCQUFELENBRjBCO0FBR25DQztBQUhtQyxDQUFULENBQTVCOztBQU1BLElBQU1DLG9CQUFvQiw2QkFBUztBQUNqQ0gsVUFBUTtBQUFBLFdBQU0sc0tBQU47QUFBQSxHQUR5QjtBQUVqQ0MsV0FBUyxDQUFDLGdCQUFELENBRndCO0FBR2pDQztBQUhpQyxDQUFULENBQTFCOztJQU1NRSxnQjs7O0FBQ0osNEJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSwwSkFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQSxtQkFDMkIsS0FBS0QsS0FEaEM7QUFBQSxVQUNDRSxPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVQyxJQURWLFVBQ1VBLElBRFY7QUFBQSxVQUNnQkMsTUFEaEIsVUFDZ0JBLE1BRGhCO0FBQUEsVUFFQ0MsVUFGRCxHQUVnQkYsSUFGaEIsQ0FFQ0UsVUFGRDtBQUFBLFVBR0NDLFVBSEQsR0FHZ0JGLE1BSGhCLENBR0NFLFVBSEQ7OztBQUtQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBV0osUUFBUWIsSUFBaEU7QUFDR2dCLHNCQUNDO0FBQUE7QUFBQTtBQUNFLHNCQURGO0FBRUUsZ0JBQUksRUFGTjtBQUdFLGdCQUFJLEVBSE47QUFJRSxnQkFBSSxDQUpOO0FBS0UsZ0JBQUksQ0FMTjtBQU1FLGdCQUFJLENBTk47QUFPRSx1QkFBV0gsUUFBUVo7QUFQckI7QUFTRSx3Q0FBQyxtQkFBRCxJQUFxQixRQUFRYyxNQUE3QjtBQVRGLFNBRko7QUFjRTtBQUFBO0FBQUE7QUFDRSxzQkFERjtBQUVFLGdCQUFJLEVBRk47QUFHRSxnQkFBSSxFQUhOO0FBSUUsZ0JBQUksQ0FKTjtBQUtFLGdCQUFJLENBTE47QUFNRSxnQkFBSSxDQU5OO0FBT0UsdUJBQVdGLFFBQVFaO0FBUHJCO0FBU0csaUJBQU9nQixVQUFQLEtBQXNCLFdBQXRCLElBQ0Msc0JBQWNBLFVBQWQsRUFBMEJDLEdBQTFCLENBQThCLFVBQUNDLENBQUQ7QUFBQSxtQkFDNUIsOEJBQUMsaUJBQUQsMkJBQW1CLEtBQUtBLEVBQUVDLFVBQTFCLElBQTBDRCxDQUExQyxFQUQ0QjtBQUFBLFdBQTlCO0FBVko7QUFkRixPQURGO0FBK0JEOzs7OztBQUdIVCxpQkFBaUJXLFNBQWpCLEdBQTZCO0FBQzNCUixXQUFTLG9CQUFVUyxNQUFWLENBQWlCQyxVQURDOztBQUczQlQsUUFBTSxvQkFBVVEsTUFBVixDQUFpQkMsVUFISTtBQUkzQlIsVUFBUSxvQkFBVU8sTUFBVixDQUFpQkM7QUFKRSxDQUE3Qjs7a0JBT2UsMEJBQVd4QixNQUFYLEVBQW1CVyxnQkFBbkIsQyIsImZpbGUiOiI2NS5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IHsgXCJkZWZhdWx0XCI6IHJlcXVpcmUoXCJjb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L3ZhbHVlc1wiKSwgX19lc01vZHVsZTogdHJ1ZSB9O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvdmFsdWVzLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9iYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L3ZhbHVlcy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMTggNjAgNjUiLCJyZXF1aXJlKCcuLi8uLi9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzJyk7XG5tb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoJy4uLy4uL21vZHVsZXMvX2NvcmUnKS5PYmplY3QudmFsdWVzO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC92YWx1ZXMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsInZhciBnZXRLZXlzID0gcmVxdWlyZSgnLi9fb2JqZWN0LWtleXMnKTtcbnZhciB0b0lPYmplY3QgPSByZXF1aXJlKCcuL190by1pb2JqZWN0Jyk7XG52YXIgaXNFbnVtID0gcmVxdWlyZSgnLi9fb2JqZWN0LXBpZScpLmY7XG5tb2R1bGUuZXhwb3J0cyA9IGZ1bmN0aW9uIChpc0VudHJpZXMpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChpdCkge1xuICAgIHZhciBPID0gdG9JT2JqZWN0KGl0KTtcbiAgICB2YXIga2V5cyA9IGdldEtleXMoTyk7XG4gICAgdmFyIGxlbmd0aCA9IGtleXMubGVuZ3RoO1xuICAgIHZhciBpID0gMDtcbiAgICB2YXIgcmVzdWx0ID0gW107XG4gICAgdmFyIGtleTtcbiAgICB3aGlsZSAobGVuZ3RoID4gaSkgaWYgKGlzRW51bS5jYWxsKE8sIGtleSA9IGtleXNbaSsrXSkpIHtcbiAgICAgIHJlc3VsdC5wdXNoKGlzRW50cmllcyA/IFtrZXksIE9ba2V5XV0gOiBPW2tleV0pO1xuICAgIH0gcmV0dXJuIHJlc3VsdDtcbiAgfTtcbn07XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS90YzM5L3Byb3Bvc2FsLW9iamVjdC12YWx1ZXMtZW50cmllc1xudmFyICRleHBvcnQgPSByZXF1aXJlKCcuL19leHBvcnQnKTtcbnZhciAkdmFsdWVzID0gcmVxdWlyZSgnLi9fb2JqZWN0LXRvLWFycmF5JykoZmFsc2UpO1xuXG4kZXhwb3J0KCRleHBvcnQuUywgJ09iamVjdCcsIHtcbiAgdmFsdWVzOiBmdW5jdGlvbiB2YWx1ZXMoaXQpIHtcbiAgICByZXR1cm4gJHZhbHVlcyhpdCk7XG4gIH1cbn0pO1xuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM3Lm9iamVjdC52YWx1ZXMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL2VzNy5vYmplY3QudmFsdWVzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcblxuaW1wb3J0IExvYWRhYmxlIGZyb20gJ3JlYWN0LWxvYWRhYmxlJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgLy8gcGFkZGluZzogJzElJyxcbiAgfSxcbiAgZ3JpZEl0ZW06IHtcbiAgICBwYWRkaW5nVG9wOiAnMSUnLFxuICAgIHBhZGRpbmdMZWZ0OiAnMSUnLFxuICAgIHBhZGRpbmdSaWdodDogJzElJyxcbiAgfSxcbn07XG5cbmNvbnN0IEFzeW5jUXVlc3Rpb25DcmVhdGUgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuL1F1ZXN0aW9uQ3JlYXRlLmpzJyksXG4gIG1vZHVsZXM6IFsnLi9RdWVzdGlvbkNyZWF0ZSddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNvbnN0IEFzeW5jUXVlc3Rpb25DYXJkID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9RdWVzdGlvbkNhcmQuanMnKSxcbiAgbW9kdWxlczogWycuL1F1ZXN0aW9uQ2FyZCddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNsYXNzIFF1ZXN0aW9uVGltZWxpbmUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCB1c2VyLCBjb3Vyc2UgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluIH0gPSB1c2VyO1xuICAgIGNvbnN0IHsgZGlzY3Vzc2lvbiB9ID0gY291cnNlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7aXNTaWduZWRJbiAmJiAoXG4gICAgICAgICAgPEdyaWRcbiAgICAgICAgICAgIGl0ZW1cbiAgICAgICAgICAgIHhzPXsxMn1cbiAgICAgICAgICAgIHNtPXsxMH1cbiAgICAgICAgICAgIG1kPXs4fVxuICAgICAgICAgICAgbGc9ezh9XG4gICAgICAgICAgICB4bD17OH1cbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8QXN5bmNRdWVzdGlvbkNyZWF0ZSBjb3Vyc2U9e2NvdXJzZX0gLz5cbiAgICAgICAgICA8L0dyaWQ+XG4gICAgICAgICl9XG4gICAgICAgIDxHcmlkXG4gICAgICAgICAgaXRlbVxuICAgICAgICAgIHhzPXsxMn1cbiAgICAgICAgICBzbT17MTB9XG4gICAgICAgICAgbWQ9ezh9XG4gICAgICAgICAgbGc9ezh9XG4gICAgICAgICAgeGw9ezh9XG4gICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmdyaWRJdGVtfVxuICAgICAgICA+XG4gICAgICAgICAge3R5cGVvZiBkaXNjdXNzaW9uICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICAgICAgT2JqZWN0LnZhbHVlcyhkaXNjdXNzaW9uKS5tYXAoKHEpID0+IChcbiAgICAgICAgICAgICAgPEFzeW5jUXVlc3Rpb25DYXJkIGtleT17cS5xdWVzdGlvbklkfSB7Li4ucX0gLz5cbiAgICAgICAgICAgICkpfVxuICAgICAgICA8L0dyaWQ+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5RdWVzdGlvblRpbWVsaW5lLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY291cnNlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoUXVlc3Rpb25UaW1lbGluZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9EaXNjdXNzaW9uL1F1ZXN0aW9uVGltZWxpbmUuanMiXSwic291cmNlUm9vdCI6IiJ9