webpackJsonp([74],{

/***/ "./src/client/actions/courses/discussion/getAll.js":
/*!*********************************************************!*\
  !*** ./src/client/actions/courses/discussion/getAll.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.statusCode
 * @param {Object[]} payload.discussion
 * @param {Object} payload.discussion[]
 * @param {number} payload.discussion[].courseId
 * @param {number} payload.discussion[].authorId - author of a question.
 * @param {number} payload.discussion[].questionId
 * @param {string} payload.discussion[].title
 * @param {Object} payload.discussion[].data - draft.js raw content state.
 * @param {string} payload.discussion[].timestamp
 *
 * @returns {Object}
 */
var getAll = function getAll(payload) {
  return { type: _courses.QUESTIONS_GET, payload: payload };
}; /**
    * Get all course discussion's questions.
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/courses/discussions/getAll.js":
/*!******************************************************!*\
  !*** ./src/client/api/courses/discussions/getAll.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * get all modules of a course with courseId.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.courseId -
 *
 * @return {Promise}
 */
/**
 * @format
 * 
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        courseId = _ref2.courseId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/courses/' + courseId + '/discussion';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/containers/CoursePage/Discussion/index.js":
/*!**************************************************************!*\
  !*** ./src/client/containers/CoursePage/Discussion/index.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouterDom = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../../api/courses/discussions/getAll */ "./src/client/api/courses/discussions/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../../actions/courses/discussion/getAll */ "./src/client/actions/courses/discussion/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var AsyncErrorPage = (0, _reactLoadable2.default)({
  loader: function loader() {
    return new Promise(function(resolve) { resolve(); }).then(__webpack_require__.bind(null, /*! ../../../components/ErrorPage */ "./src/client/components/ErrorPage.js"));
  },
  modules: ['../../../components/ErrorPage'],
  loading: _Loading2.default
});

var styles = {
  root: {}
};

var AsyncQuestionTimeline = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(65).then(__webpack_require__.bind(null, /*! ./QuestionTimeline.js */ "./src/client/containers/CoursePage/Discussion/QuestionTimeline.js"));
  },
  modules: ['./QuestionTimeline'],
  loading: _Loading2.default
});

var AsyncQuestion = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(31).then(__webpack_require__.bind(null, /*! ./Question.js */ "./src/client/containers/CoursePage/Discussion/Question.js"));
  },
  modules: ['./Question'],
  loading: _Loading2.default
});

var DiscussionRoute = function (_Component) {
  (0, _inherits3.default)(DiscussionRoute, _Component);

  function DiscussionRoute(props) {
    (0, _classCallCheck3.default)(this, DiscussionRoute);

    var _this = (0, _possibleConstructorReturn3.default)(this, (DiscussionRoute.__proto__ || (0, _getPrototypeOf2.default)(DiscussionRoute)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(DiscussionRoute, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var token, _props$course, courseId, discussion, _ref2, statusCode, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                token = this.props.user.token;
                _props$course = this.props.course, courseId = _props$course.courseId, discussion = _props$course.discussion;

                if (!discussion) {
                  _context.next = 4;
                  break;
                }

                return _context.abrupt('return');

              case 4:
                _context.next = 6;
                return (0, _getAll2.default)({
                  token: token,
                  courseId: courseId
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 11;
                  break;
                }

                return _context.abrupt('return');

              case 11:

                this.props.actionGetAllQuestions({
                  courseId: courseId,
                  statusCode: statusCode,
                  discussion: payload
                });

              case 12:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          course = _props.course;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _reactRouterDom.Switch,
          null,
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/courses/:courseId/discussion',
            component: function component(restProps) {
              return _react2.default.createElement(AsyncQuestionTimeline, (0, _extends3.default)({
                user: user,
                course: course
              }, restProps));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, {
            exact: true,
            path: '/courses/:courseId/discussion/:questionId',
            component: function component(restProps) {
              return _react2.default.createElement(AsyncQuestion, (0, _extends3.default)({ user: user, course: course }, restProps));
            }
          }),
          _react2.default.createElement(_reactRouterDom.Route, { component: AsyncErrorPage })
        )
      );
    }
  }]);
  return DiscussionRoute;
}(_react.Component);

DiscussionRoute.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired,

  actionGetAllQuestions: _propTypes2.default.func.isRequired
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionGetAllQuestions: _getAll4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(null, mapDispatchToProps)((0, _withStyles2.default)(styles)(DiscussionRoute));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy9kaXNjdXNzaW9uL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL2Rpc2N1c3Npb25zL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9EaXNjdXNzaW9uL2luZGV4LmpzIl0sIm5hbWVzIjpbImdldEFsbCIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJjb3Vyc2VJZCIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInN0YXR1c0NvZGUiLCJlcnJvciIsImpzb24iLCJjb25zb2xlIiwiQXN5bmNFcnJvclBhZ2UiLCJsb2FkZXIiLCJtb2R1bGVzIiwibG9hZGluZyIsInN0eWxlcyIsInJvb3QiLCJBc3luY1F1ZXN0aW9uVGltZWxpbmUiLCJBc3luY1F1ZXN0aW9uIiwiRGlzY3Vzc2lvblJvdXRlIiwicHJvcHMiLCJzdGF0ZSIsInVzZXIiLCJjb3Vyc2UiLCJkaXNjdXNzaW9uIiwiYWN0aW9uR2V0QWxsUXVlc3Rpb25zIiwiY2xhc3NlcyIsInJlc3RQcm9wcyIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBTUE7O0FBc0JBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLENBQWYsQyxDQTVDQTs7Ozs7O2tCQThDZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pDZjs7Ozs7Ozs7OztBQWJBOzs7Ozs7c0ZBdUJBO0FBQUEsUUFBd0JHLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxRQUEvQixTQUErQkEsUUFBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixvQ0FFdUNELFFBRnZDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlTjtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVU8sZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlLLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFGLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVkLE07Ozs7O0FBbEJmOzs7O0FBQ0E7Ozs7a0JBb0RlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RGY7Ozs7QUFDQTs7OztBQUNBOztBQUNBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQWRBOztBQWdCQSxJQUFNaUIsaUJBQWlCLDZCQUFTO0FBQzlCQyxVQUFRO0FBQUEsV0FBTSwrSkFBTjtBQUFBLEdBRHNCO0FBRTlCQyxXQUFTLENBQUMsK0JBQUQsQ0FGcUI7QUFHOUJDO0FBSDhCLENBQVQsQ0FBdkI7O0FBTUEsSUFBTUMsU0FBUztBQUNiQyxRQUFNO0FBRE8sQ0FBZjs7QUFJQSxJQUFNQyx3QkFBd0IsNkJBQVM7QUFDckNMLFVBQVE7QUFBQSxXQUFNLDhLQUFOO0FBQUEsR0FENkI7QUFFckNDLFdBQVMsQ0FBQyxvQkFBRCxDQUY0QjtBQUdyQ0M7QUFIcUMsQ0FBVCxDQUE5Qjs7QUFNQSxJQUFNSSxnQkFBZ0IsNkJBQVM7QUFDN0JOLFVBQVE7QUFBQSxXQUFNLDhKQUFOO0FBQUEsR0FEcUI7QUFFN0JDLFdBQVMsQ0FBQyxZQUFELENBRm9CO0FBRzdCQztBQUg2QixDQUFULENBQXRCOztJQU1NSyxlOzs7QUFDSiwyQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLHdKQUNYQSxLQURXOztBQUVqQixVQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUZpQjtBQUdsQjs7Ozs7Ozs7Ozs7O0FBR1N4QixxQixHQUFVLEtBQUt1QixLQUFMLENBQVdFLEksQ0FBckJ6QixLO2dDQUN5QixLQUFLdUIsS0FBTCxDQUFXRyxNLEVBQXBDekIsUSxpQkFBQUEsUSxFQUFVMEIsVSxpQkFBQUEsVTs7cUJBRWRBLFU7Ozs7Ozs7Ozt1QkFJa0Msc0JBQW1CO0FBQ3ZEM0IsOEJBRHVEO0FBRXZEQztBQUZ1RCxpQkFBbkIsQzs7OztBQUE5QlMsMEIsU0FBQUEsVTtBQUFZWix1QixTQUFBQSxPOztzQkFLaEJZLGNBQWMsRzs7Ozs7Ozs7O0FBSWxCLHFCQUFLYSxLQUFMLENBQVdLLHFCQUFYLENBQWlDO0FBQy9CM0Isb0NBRCtCO0FBRS9CUyx3Q0FGK0I7QUFHL0JpQiw4QkFBWTdCO0FBSG1CLGlCQUFqQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQU9PO0FBQUEsbUJBQzJCLEtBQUt5QixLQURoQztBQUFBLFVBQ0NNLE9BREQsVUFDQ0EsT0FERDtBQUFBLFVBQ1VKLElBRFYsVUFDVUEsSUFEVjtBQUFBLFVBQ2dCQyxNQURoQixVQUNnQkEsTUFEaEI7OztBQUdQLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBV0csUUFBUVYsSUFBeEI7QUFDRTtBQUFBO0FBQUE7QUFDRTtBQUNFLHVCQURGO0FBRUUsa0JBQUssK0JBRlA7QUFHRSx1QkFBVyxtQkFBQ1csU0FBRDtBQUFBLHFCQUNULDhCQUFDLHFCQUFEO0FBQ0Usc0JBQU1MLElBRFI7QUFFRSx3QkFBUUM7QUFGVixpQkFHTUksU0FITixFQURTO0FBQUE7QUFIYixZQURGO0FBWUU7QUFDRSx1QkFERjtBQUVFLGtCQUFLLDJDQUZQO0FBR0UsdUJBQVcsbUJBQUNBLFNBQUQ7QUFBQSxxQkFDVCw4QkFBQyxhQUFELDJCQUFlLE1BQU1MLElBQXJCLEVBQTJCLFFBQVFDLE1BQW5DLElBQStDSSxTQUEvQyxFQURTO0FBQUE7QUFIYixZQVpGO0FBbUJFLGlFQUFPLFdBQVdoQixjQUFsQjtBQW5CRjtBQURGLE9BREY7QUF5QkQ7Ozs7O0FBR0hRLGdCQUFnQlMsU0FBaEIsR0FBNEI7QUFDMUJGLFdBQVMsb0JBQVVHLE1BQVYsQ0FBaUJDLFVBREE7O0FBRzFCUixRQUFNLG9CQUFVTyxNQUFWLENBQWlCQyxVQUhHO0FBSTFCUCxVQUFRLG9CQUFVTSxNQUFWLENBQWlCQyxVQUpDOztBQU0xQkwseUJBQXVCLG9CQUFVTSxJQUFWLENBQWVEO0FBTlosQ0FBNUI7O0FBU0EsSUFBTUUscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VSO0FBREYsR0FERixFQUlFUSxRQUpGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVFlLHlCQUFRLElBQVIsRUFBY0Qsa0JBQWQsRUFDYiwwQkFBV2pCLE1BQVgsRUFBbUJJLGVBQW5CLENBRGEsQyIsImZpbGUiOiI3NC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEdldCBhbGwgY291cnNlIGRpc2N1c3Npb24ncyBxdWVzdGlvbnMuXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IFFVRVNUSU9OU19HRVQgfSBmcm9tICcuLi8uLi8uLi9jb25zdGFudHMvY291cnNlcyc7XG5cbnR5cGUgUXVlc3Rpb24gPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIHF1ZXN0aW9uSWQ6IG51bWJlcixcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgZGF0YT86IGFueSxcbiAgdGltZXN0YW1wOiBzdHJpbmcsXG59O1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIHN0YXR1c0NvZGU6IG51bWJlcixcbiAgZGlzY3Vzc2lvbjogQXJyYXk8UXVlc3Rpb24+LFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtPYmplY3RbXX0gcGF5bG9hZC5kaXNjdXNzaW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5kaXNjdXNzaW9uW11cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmRpc2N1c3Npb25bXS5jb3Vyc2VJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZGlzY3Vzc2lvbltdLmF1dGhvcklkIC0gYXV0aG9yIG9mIGEgcXVlc3Rpb24uXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5kaXNjdXNzaW9uW10ucXVlc3Rpb25JZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZGlzY3Vzc2lvbltdLnRpdGxlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5kaXNjdXNzaW9uW10uZGF0YSAtIGRyYWZ0LmpzIHJhdyBjb250ZW50IHN0YXRlLlxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZGlzY3Vzc2lvbltdLnRpbWVzdGFtcFxuICpcbiAqIEByZXR1cm5zIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEFsbCA9IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7IHR5cGU6IFFVRVNUSU9OU19HRVQsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL2Rpc2N1c3Npb24vZ2V0QWxsLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHRva2VuOiBzdHJpbmcsXG4gIGNvdXJzZUlkOiBudW1iZXIsXG59O1xuXG4vKipcbiAqIGdldCBhbGwgbW9kdWxlcyBvZiBhIGNvdXJzZSB3aXRoIGNvdXJzZUlkLlxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gZ2V0QWxsXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKlxuICogQHJldHVybiB7UHJvbWlzZX1cbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdG9rZW4sIGNvdXJzZUlkIH06IFBheWxvYWQpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvY291cnNlcy8ke2NvdXJzZUlkfS9kaXNjdXNzaW9uYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL2Rpc2N1c3Npb25zL2dldEFsbC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgUm91dGUsIFN3aXRjaCB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nO1xuaW1wb3J0IHsgYmluZEFjdGlvbkNyZWF0b3JzIH0gZnJvbSAncmVkdXgnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuaW1wb3J0IExvYWRhYmxlIGZyb20gJ3JlYWN0LWxvYWRhYmxlJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuaW1wb3J0IGFwaUdldEFsbFF1ZXN0aW9ucyBmcm9tICcuLi8uLi8uLi9hcGkvY291cnNlcy9kaXNjdXNzaW9ucy9nZXRBbGwnO1xuaW1wb3J0IGFjdGlvbkdldEFsbFF1ZXN0aW9ucyBmcm9tICcuLi8uLi8uLi9hY3Rpb25zL2NvdXJzZXMvZGlzY3Vzc2lvbi9nZXRBbGwnO1xuXG5jb25zdCBBc3luY0Vycm9yUGFnZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4uLy4uLy4uL2NvbXBvbmVudHMvRXJyb3JQYWdlJyksXG4gIG1vZHVsZXM6IFsnLi4vLi4vLi4vY29tcG9uZW50cy9FcnJvclBhZ2UnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHt9LFxufTtcblxuY29uc3QgQXN5bmNRdWVzdGlvblRpbWVsaW5lID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9RdWVzdGlvblRpbWVsaW5lLmpzJyksXG4gIG1vZHVsZXM6IFsnLi9RdWVzdGlvblRpbWVsaW5lJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNRdWVzdGlvbiA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vUXVlc3Rpb24uanMnKSxcbiAgbW9kdWxlczogWycuL1F1ZXN0aW9uJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY2xhc3MgRGlzY3Vzc2lvblJvdXRlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgeyB0b2tlbiB9ID0gdGhpcy5wcm9wcy51c2VyO1xuICAgIGNvbnN0IHsgY291cnNlSWQsIGRpc2N1c3Npb24gfSA9IHRoaXMucHJvcHMuY291cnNlO1xuXG4gICAgaWYgKGRpc2N1c3Npb24pIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBjb25zdCB7IHN0YXR1c0NvZGUsIHBheWxvYWQgfSA9IGF3YWl0IGFwaUdldEFsbFF1ZXN0aW9ucyh7XG4gICAgICB0b2tlbixcbiAgICAgIGNvdXJzZUlkLFxuICAgIH0pO1xuXG4gICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25HZXRBbGxRdWVzdGlvbnMoe1xuICAgICAgY291cnNlSWQsXG4gICAgICBzdGF0dXNDb2RlLFxuICAgICAgZGlzY3Vzc2lvbjogcGF5bG9hZCxcbiAgICB9KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHVzZXIsIGNvdXJzZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPFN3aXRjaD5cbiAgICAgICAgICA8Um91dGVcbiAgICAgICAgICAgIGV4YWN0XG4gICAgICAgICAgICBwYXRoPVwiL2NvdXJzZXMvOmNvdXJzZUlkL2Rpc2N1c3Npb25cIlxuICAgICAgICAgICAgY29tcG9uZW50PXsocmVzdFByb3BzKSA9PiAoXG4gICAgICAgICAgICAgIDxBc3luY1F1ZXN0aW9uVGltZWxpbmVcbiAgICAgICAgICAgICAgICB1c2VyPXt1c2VyfVxuICAgICAgICAgICAgICAgIGNvdXJzZT17Y291cnNlfVxuICAgICAgICAgICAgICAgIHsuLi5yZXN0UHJvcHN9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICApfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPFJvdXRlXG4gICAgICAgICAgICBleGFjdFxuICAgICAgICAgICAgcGF0aD1cIi9jb3Vyc2VzLzpjb3Vyc2VJZC9kaXNjdXNzaW9uLzpxdWVzdGlvbklkXCJcbiAgICAgICAgICAgIGNvbXBvbmVudD17KHJlc3RQcm9wcykgPT4gKFxuICAgICAgICAgICAgICA8QXN5bmNRdWVzdGlvbiB1c2VyPXt1c2VyfSBjb3Vyc2U9e2NvdXJzZX0gey4uLnJlc3RQcm9wc30gLz5cbiAgICAgICAgICAgICl9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Um91dGUgY29tcG9uZW50PXtBc3luY0Vycm9yUGFnZX0gLz5cbiAgICAgICAgPC9Td2l0Y2g+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbkRpc2N1c3Npb25Sb3V0ZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICB1c2VyOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGNvdXJzZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGFjdGlvbkdldEFsbFF1ZXN0aW9uczogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbkdldEFsbFF1ZXN0aW9ucyxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG51bGwsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShEaXNjdXNzaW9uUm91dGUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL0Rpc2N1c3Npb24vaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9