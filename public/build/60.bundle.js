webpackJsonp([60],{

/***/ "./node_modules/babel-runtime/core-js/object/values.js":
/*!*************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/values.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(/*! core-js/library/fn/object/values */ "./node_modules/core-js/library/fn/object/values.js"), __esModule: true };

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/values.js":
/*!**********************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/values.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../../modules/es7.object.values */ "./node_modules/core-js/library/modules/es7.object.values.js");
module.exports = __webpack_require__(/*! ../../modules/_core */ "./node_modules/core-js/library/modules/_core.js").Object.values;


/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-to-array.js":
/*!******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-to-array.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var getKeys = __webpack_require__(/*! ./_object-keys */ "./node_modules/core-js/library/modules/_object-keys.js");
var toIObject = __webpack_require__(/*! ./_to-iobject */ "./node_modules/core-js/library/modules/_to-iobject.js");
var isEnum = __webpack_require__(/*! ./_object-pie */ "./node_modules/core-js/library/modules/_object-pie.js").f;
module.exports = function (isEntries) {
  return function (it) {
    var O = toIObject(it);
    var keys = getKeys(O);
    var length = keys.length;
    var i = 0;
    var result = [];
    var key;
    while (length > i) if (isEnum.call(O, key = keys[i++])) {
      result.push(isEntries ? [key, O[key]] : O[key]);
    } return result;
  };
};


/***/ }),

/***/ "./node_modules/core-js/library/modules/es7.object.values.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es7.object.values.js ***!
  \*******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

// https://github.com/tc39/proposal-object-values-entries
var $export = __webpack_require__(/*! ./_export */ "./node_modules/core-js/library/modules/_export.js");
var $values = __webpack_require__(/*! ./_object-to-array */ "./node_modules/core-js/library/modules/_object-to-array.js")(false);

$export($export.S, 'Object', {
  values: function values(it) {
    return $values(it);
  }
});


/***/ }),

/***/ "./src/client/actions/courses/discussion/answers/getAll.js":
/*!*****************************************************************!*\
  !*** ./src/client/actions/courses/discussion/answers/getAll.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.statusCode
 * @param {Object[]} payload.answers
 * @param {Object} payload.answers[]
 * @param {number} payload.answers[].courseId
 * @param {number} payload.answers[].authorId - author of a question.
 * @param {number} payload.answers[].questionId
 * @param {number} payload.answers[].answerId
 * @param {string} payload.answers[].title
 * @param {Object} payload.answers[].data - draft.js raw content state.
 * @param {string} payload.answers[].timestamp
 *
 * @returns {Object}
 */
var getAll = function getAll(payload) {
  return { type: _courses.ANSWERS_GET, payload: payload };
}; /**
    * Get all course discussion's questions.
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/courses/discussions/answers/getAll.js":
/*!**************************************************************!*\
  !*** ./src/client/api/courses/discussions/answers/getAll.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * get all modules of a course with courseId.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 *
 * @return {Promise}
 */
/**
 * @format
 * 
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        courseId = _ref2.courseId,
        questionId = _ref2.questionId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/courses/' + courseId + '/discussion/' + questionId + '/answers';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/containers/CoursePage/Discussion/AnswerTimeline.js":
/*!***********************************************************************!*\
  !*** ./src/client/containers/CoursePage/Discussion/AnswerTimeline.js ***!
  \***********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _values = __webpack_require__(/*! babel-runtime/core-js/object/values */ "./node_modules/babel-runtime/core-js/object/values.js");

var _values2 = _interopRequireDefault(_values);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../../api/courses/discussions/answers/getAll */ "./src/client/api/courses/discussions/answers/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../../actions/courses/discussion/answers/getAll */ "./src/client/actions/courses/discussion/answers/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {
    // padding: '1%',
  },
  gridItem: {
    paddingTop: '1%',
    paddingLeft: '1%',
    paddingRight: '1%'
  }
};

var AsyncAnswerCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(68).then(__webpack_require__.bind(null, /*! ./AnswerCreate.js */ "./src/client/containers/CoursePage/Discussion/AnswerCreate.js"));
  },
  modules: ['./AnswerCreate'],
  loading: _Loading2.default
});

var AsyncAnswerCard = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(78).then(__webpack_require__.bind(null, /*! ./AnswerCard.js */ "./src/client/containers/CoursePage/Discussion/AnswerCard.js"));
  },
  modules: ['./AnswerCard'],
  loading: _Loading2.default
});

var AnswerTimeline = function (_Component) {
  (0, _inherits3.default)(AnswerTimeline, _Component);

  function AnswerTimeline(props) {
    (0, _classCallCheck3.default)(this, AnswerTimeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (AnswerTimeline.__proto__ || (0, _getPrototypeOf2.default)(AnswerTimeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(AnswerTimeline, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, user, course, questionId, token, courseId, _ref2, statusCode, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _props = this.props, user = _props.user, course = _props.course, questionId = _props.questionId;
                token = user.token;
                courseId = course.courseId;

                if (!course.discussion[questionId].answers) {
                  _context.next = 5;
                  break;
                }

                return _context.abrupt('return');

              case 5:
                _context.next = 7;
                return (0, _getAll2.default)({
                  token: token,
                  courseId: courseId,
                  questionId: questionId
                });

              case 7:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                payload = _ref2.payload;


                this.props.actionGetAll({
                  statusCode: statusCode,
                  courseId: courseId,
                  questionId: questionId,
                  answers: payload
                });

              case 11:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          user = _props2.user,
          course = _props2.course,
          questionId = _props2.questionId;
      var isSignedIn = user.isSignedIn;
      var answers = course.discussion[questionId].answers;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        isSignedIn && _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, className: classes.gridItem },
          _react2.default.createElement(AsyncAnswerCreate, this.props)
        ),
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, className: classes.gridItem },
          typeof answers !== 'undefined' && (0, _values2.default)(answers).map(function (answer) {
            return _react2.default.createElement(AsyncAnswerCard, (0, _extends3.default)({ key: answer.answerId }, answer));
          })
        )
      );
    }
  }]);
  return AnswerTimeline;
}(_react.Component);

AnswerTimeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  course: _propTypes2.default.object.isRequired,
  questionId: _propTypes2.default.number.isRequired,

  actionGetAll: _propTypes2.default.func.isRequired
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionGetAll: _getAll4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(null, mapDispatchToProps)((0, _withStyles2.default)(styles)(AnswerTimeline));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC92YWx1ZXMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9fb2JqZWN0LXRvLWFycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczcub2JqZWN0LnZhbHVlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy9kaXNjdXNzaW9uL2Fuc3dlcnMvZ2V0QWxsLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvZGlzY3Vzc2lvbnMvYW5zd2Vycy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvRGlzY3Vzc2lvbi9BbnN3ZXJUaW1lbGluZS5qcyJdLCJuYW1lcyI6WyJnZXRBbGwiLCJwYXlsb2FkIiwidHlwZSIsInRva2VuIiwiY291cnNlSWQiLCJxdWVzdGlvbklkIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJzdHlsZXMiLCJyb290IiwiZ3JpZEl0ZW0iLCJwYWRkaW5nVG9wIiwicGFkZGluZ0xlZnQiLCJwYWRkaW5nUmlnaHQiLCJBc3luY0Fuc3dlckNyZWF0ZSIsImxvYWRlciIsIm1vZHVsZXMiLCJsb2FkaW5nIiwiQXN5bmNBbnN3ZXJDYXJkIiwiQW5zd2VyVGltZWxpbmUiLCJwcm9wcyIsInN0YXRlIiwidXNlciIsImNvdXJzZSIsImRpc2N1c3Npb24iLCJhbnN3ZXJzIiwiYWN0aW9uR2V0QWxsIiwiY2xhc3NlcyIsImlzU2lnbmVkSW4iLCJtYXAiLCJhbnN3ZXIiLCJhbnN3ZXJJZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJudW1iZXIiLCJmdW5jIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSxrQkFBa0Isa0o7Ozs7Ozs7Ozs7OztBQ0FsQjtBQUNBOzs7Ozs7Ozs7Ozs7O0FDREE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7Ozs7Ozs7Ozs7OztBQ2ZBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRkQ7O0FBd0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQixFQUFFQywwQkFBRixFQUFxQkQsZ0JBQXJCLEVBQS9CO0FBQUEsQ0FBZixDLENBaERBOzs7Ozs7a0JBa0RlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcENmOzs7Ozs7Ozs7OztBQWRBOzs7Ozs7c0ZBeUJBO0FBQUEsUUFBd0JHLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxRQUEvQixTQUErQkEsUUFBL0I7QUFBQSxRQUF5Q0MsVUFBekMsU0FBeUNBLFVBQXpDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVDLGVBRlYsb0NBRXVDRixRQUZ2QyxvQkFFOERDLFVBRjlEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlUDtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVVEsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlLLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFGLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVmLE07Ozs7O0FBcEJmOzs7O0FBQ0E7Ozs7a0JBc0RlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMURmOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQWhCQTs7QUFrQkEsSUFBTWtCLFNBQVM7QUFDYkMsUUFBTTtBQUNKO0FBREksR0FETztBQUliQyxZQUFVO0FBQ1JDLGdCQUFZLElBREo7QUFFUkMsaUJBQWEsSUFGTDtBQUdSQyxrQkFBYztBQUhOO0FBSkcsQ0FBZjs7QUFXQSxJQUFNQyxvQkFBb0IsNkJBQVM7QUFDakNDLFVBQVE7QUFBQSxXQUFNLHNLQUFOO0FBQUEsR0FEeUI7QUFFakNDLFdBQVMsQ0FBQyxnQkFBRCxDQUZ3QjtBQUdqQ0M7QUFIaUMsQ0FBVCxDQUExQjs7QUFNQSxJQUFNQyxrQkFBa0IsNkJBQVM7QUFDL0JILFVBQVE7QUFBQSxXQUFNLGtLQUFOO0FBQUEsR0FEdUI7QUFFL0JDLFdBQVMsQ0FBQyxjQUFELENBRnNCO0FBRy9CQztBQUgrQixDQUFULENBQXhCOztJQU1NRSxjOzs7QUFDSiwwQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLHNKQUNYQSxLQURXOztBQUVqQixVQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUZpQjtBQUdsQjs7Ozs7Ozs7Ozs7O3lCQUdzQyxLQUFLRCxLLEVBQWxDRSxJLFVBQUFBLEksRUFBTUMsTSxVQUFBQSxNLEVBQVE1QixVLFVBQUFBLFU7QUFDZEYscUIsR0FBVTZCLEksQ0FBVjdCLEs7QUFDQUMsd0IsR0FBYTZCLE0sQ0FBYjdCLFE7O3FCQUVKNkIsT0FBT0MsVUFBUCxDQUFrQjdCLFVBQWxCLEVBQThCOEIsTzs7Ozs7Ozs7O3VCQUlJLHNCQUFVO0FBQzlDaEMsOEJBRDhDO0FBRTlDQyxvQ0FGOEM7QUFHOUNDO0FBSDhDLGlCQUFWLEM7Ozs7QUFBOUJTLDBCLFNBQUFBLFU7QUFBWWIsdUIsU0FBQUEsTzs7O0FBTXBCLHFCQUFLNkIsS0FBTCxDQUFXTSxZQUFYLENBQXdCO0FBQ3RCdEIsd0NBRHNCO0FBRXRCVixvQ0FGc0I7QUFHdEJDLHdDQUhzQjtBQUl0QjhCLDJCQUFTbEM7QUFKYSxpQkFBeEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFRTztBQUFBLG9CQUN1QyxLQUFLNkIsS0FENUM7QUFBQSxVQUNDTyxPQURELFdBQ0NBLE9BREQ7QUFBQSxVQUNVTCxJQURWLFdBQ1VBLElBRFY7QUFBQSxVQUNnQkMsTUFEaEIsV0FDZ0JBLE1BRGhCO0FBQUEsVUFDd0I1QixVQUR4QixXQUN3QkEsVUFEeEI7QUFBQSxVQUVDaUMsVUFGRCxHQUVnQk4sSUFGaEIsQ0FFQ00sVUFGRDtBQUFBLFVBR0NILE9BSEQsR0FHYUYsT0FBT0MsVUFBUCxDQUFrQjdCLFVBQWxCLENBSGIsQ0FHQzhCLE9BSEQ7OztBQUtQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBV0UsUUFBUWxCLElBQWhFO0FBQ0dtQixzQkFDQztBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLFdBQVdELFFBQVFqQixRQUF0QztBQUNFLHdDQUFDLGlCQUFELEVBQXVCLEtBQUtVLEtBQTVCO0FBREYsU0FGSjtBQU1FO0FBQUE7QUFBQSxZQUFNLFVBQU4sRUFBVyxJQUFJLEVBQWYsRUFBbUIsV0FBV08sUUFBUWpCLFFBQXRDO0FBQ0csaUJBQU9lLE9BQVAsS0FBbUIsV0FBbkIsSUFDQyxzQkFBY0EsT0FBZCxFQUF1QkksR0FBdkIsQ0FBMkIsVUFBQ0MsTUFBRDtBQUFBLG1CQUN6Qiw4QkFBQyxlQUFELDJCQUFpQixLQUFLQSxPQUFPQyxRQUE3QixJQUEyQ0QsTUFBM0MsRUFEeUI7QUFBQSxXQUEzQjtBQUZKO0FBTkYsT0FERjtBQWVEOzs7OztBQUdIWCxlQUFlYSxTQUFmLEdBQTJCO0FBQ3pCTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQUREOztBQUd6QlosUUFBTSxvQkFBVVcsTUFBVixDQUFpQkMsVUFIRTtBQUl6QlgsVUFBUSxvQkFBVVUsTUFBVixDQUFpQkMsVUFKQTtBQUt6QnZDLGNBQVksb0JBQVV3QyxNQUFWLENBQWlCRCxVQUxKOztBQU96QlIsZ0JBQWMsb0JBQVVVLElBQVYsQ0FBZUY7QUFQSixDQUEzQjs7QUFVQSxJQUFNRyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRVo7QUFERixHQURGLEVBSUVZLFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVEsSUFBUixFQUFjRCxrQkFBZCxFQUNiLDBCQUFXN0IsTUFBWCxFQUFtQlcsY0FBbkIsQ0FEYSxDIiwiZmlsZSI6IjYwLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIm1vZHVsZS5leHBvcnRzID0geyBcImRlZmF1bHRcIjogcmVxdWlyZShcImNvcmUtanMvbGlicmFyeS9mbi9vYmplY3QvdmFsdWVzXCIpLCBfX2VzTW9kdWxlOiB0cnVlIH07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC92YWx1ZXMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvdmFsdWVzLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxOCA2MCA2NSIsInJlcXVpcmUoJy4uLy4uL21vZHVsZXMvZXM3Lm9iamVjdC52YWx1ZXMnKTtcbm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZSgnLi4vLi4vbW9kdWxlcy9fY29yZScpLk9iamVjdC52YWx1ZXM7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvZm4vb2JqZWN0L3ZhbHVlcy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L2ZuL29iamVjdC92YWx1ZXMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDE4IDYwIDY1IiwidmFyIGdldEtleXMgPSByZXF1aXJlKCcuL19vYmplY3Qta2V5cycpO1xudmFyIHRvSU9iamVjdCA9IHJlcXVpcmUoJy4vX3RvLWlvYmplY3QnKTtcbnZhciBpc0VudW0gPSByZXF1aXJlKCcuL19vYmplY3QtcGllJykuZjtcbm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24gKGlzRW50cmllcykge1xuICByZXR1cm4gZnVuY3Rpb24gKGl0KSB7XG4gICAgdmFyIE8gPSB0b0lPYmplY3QoaXQpO1xuICAgIHZhciBrZXlzID0gZ2V0S2V5cyhPKTtcbiAgICB2YXIgbGVuZ3RoID0ga2V5cy5sZW5ndGg7XG4gICAgdmFyIGkgPSAwO1xuICAgIHZhciByZXN1bHQgPSBbXTtcbiAgICB2YXIga2V5O1xuICAgIHdoaWxlIChsZW5ndGggPiBpKSBpZiAoaXNFbnVtLmNhbGwoTywga2V5ID0ga2V5c1tpKytdKSkge1xuICAgICAgcmVzdWx0LnB1c2goaXNFbnRyaWVzID8gW2tleSwgT1trZXldXSA6IE9ba2V5XSk7XG4gICAgfSByZXR1cm4gcmVzdWx0O1xuICB9O1xufTtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtdG8tYXJyYXkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2NvcmUtanMvbGlicmFyeS9tb2R1bGVzL19vYmplY3QtdG8tYXJyYXkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDE4IDYwIDY1IiwiLy8gaHR0cHM6Ly9naXRodWIuY29tL3RjMzkvcHJvcG9zYWwtb2JqZWN0LXZhbHVlcy1lbnRyaWVzXG52YXIgJGV4cG9ydCA9IHJlcXVpcmUoJy4vX2V4cG9ydCcpO1xudmFyICR2YWx1ZXMgPSByZXF1aXJlKCcuL19vYmplY3QtdG8tYXJyYXknKShmYWxzZSk7XG5cbiRleHBvcnQoJGV4cG9ydC5TLCAnT2JqZWN0Jywge1xuICB2YWx1ZXM6IGZ1bmN0aW9uIHZhbHVlcyhpdCkge1xuICAgIHJldHVybiAkdmFsdWVzKGl0KTtcbiAgfVxufSk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9jb3JlLWpzL2xpYnJhcnkvbW9kdWxlcy9lczcub2JqZWN0LnZhbHVlcy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvY29yZS1qcy9saWJyYXJ5L21vZHVsZXMvZXM3Lm9iamVjdC52YWx1ZXMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDE4IDYwIDY1IiwiLyoqXG4gKiBHZXQgYWxsIGNvdXJzZSBkaXNjdXNzaW9uJ3MgcXVlc3Rpb25zLlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBBTlNXRVJTX0dFVCB9IGZyb20gJy4uLy4uLy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBBbnN3ZXIgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIHF1ZXN0aW9uSWQ6IG51bWJlcixcbiAgYW5zd2VySWQ6IG51bWJlcixcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgZGF0YT86IGFueSxcbiAgdGltZXN0YW1wOiBzdHJpbmcsXG59O1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIHF1ZXN0aW9uSWQ6IG51bWJlcixcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBkaXNjdXNzaW9uOiBBcnJheTxBbnN3ZXI+LFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucXVlc3Rpb25JZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtPYmplY3RbXX0gcGF5bG9hZC5hbnN3ZXJzXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5hbnN3ZXJzW11cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmFuc3dlcnNbXS5jb3Vyc2VJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYW5zd2Vyc1tdLmF1dGhvcklkIC0gYXV0aG9yIG9mIGEgcXVlc3Rpb24uXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5hbnN3ZXJzW10ucXVlc3Rpb25JZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYW5zd2Vyc1tdLmFuc3dlcklkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5hbnN3ZXJzW10udGl0bGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmFuc3dlcnNbXS5kYXRhIC0gZHJhZnQuanMgcmF3IGNvbnRlbnQgc3RhdGUuXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5hbnN3ZXJzW10udGltZXN0YW1wXG4gKlxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0QWxsID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHsgdHlwZTogQU5TV0VSU19HRVQsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL2Rpc2N1c3Npb24vYW5zd2Vycy9nZXRBbGwuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uLy4uL2NvbmZpZyc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgdG9rZW46IHN0cmluZyxcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgcXVlc3Rpb25JZDogbnVtYmVyLFxufTtcblxuLyoqXG4gKiBnZXQgYWxsIG1vZHVsZXMgb2YgYSBjb3Vyc2Ugd2l0aCBjb3Vyc2VJZC5cbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGdldEFsbFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucXVlc3Rpb25JZCAtXG4gKlxuICogQHJldHVybiB7UHJvbWlzZX1cbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdG9rZW4sIGNvdXJzZUlkLCBxdWVzdGlvbklkIH06IFBheWxvYWQpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvY291cnNlcy8ke2NvdXJzZUlkfS9kaXNjdXNzaW9uLyR7cXVlc3Rpb25JZH0vYW5zd2Vyc2A7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZ2V0QWxsO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvY291cnNlcy9kaXNjdXNzaW9ucy9hbnN3ZXJzL2dldEFsbC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcblxuaW1wb3J0IExvYWRhYmxlIGZyb20gJ3JlYWN0LWxvYWRhYmxlJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuaW1wb3J0IGFwaUdldEFsbCBmcm9tICcuLi8uLi8uLi9hcGkvY291cnNlcy9kaXNjdXNzaW9ucy9hbnN3ZXJzL2dldEFsbCc7XG5pbXBvcnQgYWN0aW9uR2V0QWxsIGZyb20gJy4uLy4uLy4uL2FjdGlvbnMvY291cnNlcy9kaXNjdXNzaW9uL2Fuc3dlcnMvZ2V0QWxsJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgLy8gcGFkZGluZzogJzElJyxcbiAgfSxcbiAgZ3JpZEl0ZW06IHtcbiAgICBwYWRkaW5nVG9wOiAnMSUnLFxuICAgIHBhZGRpbmdMZWZ0OiAnMSUnLFxuICAgIHBhZGRpbmdSaWdodDogJzElJyxcbiAgfSxcbn07XG5cbmNvbnN0IEFzeW5jQW5zd2VyQ3JlYXRlID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi9BbnN3ZXJDcmVhdGUuanMnKSxcbiAgbW9kdWxlczogWycuL0Fuc3dlckNyZWF0ZSddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNvbnN0IEFzeW5jQW5zd2VyQ2FyZCA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vQW5zd2VyQ2FyZC5qcycpLFxuICBtb2R1bGVzOiBbJy4vQW5zd2VyQ2FyZCddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNsYXNzIEFuc3dlclRpbWVsaW5lIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgeyB1c2VyLCBjb3Vyc2UsIHF1ZXN0aW9uSWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyB0b2tlbiB9ID0gdXNlcjtcbiAgICBjb25zdCB7IGNvdXJzZUlkIH0gPSBjb3Vyc2U7XG5cbiAgICBpZiAoY291cnNlLmRpc2N1c3Npb25bcXVlc3Rpb25JZF0uYW5zd2Vycykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpR2V0QWxsKHtcbiAgICAgIHRva2VuLFxuICAgICAgY291cnNlSWQsXG4gICAgICBxdWVzdGlvbklkLFxuICAgIH0pO1xuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25HZXRBbGwoe1xuICAgICAgc3RhdHVzQ29kZSxcbiAgICAgIGNvdXJzZUlkLFxuICAgICAgcXVlc3Rpb25JZCxcbiAgICAgIGFuc3dlcnM6IHBheWxvYWQsXG4gICAgfSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCB1c2VyLCBjb3Vyc2UsIHF1ZXN0aW9uSWQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluIH0gPSB1c2VyO1xuICAgIGNvbnN0IHsgYW5zd2VycyB9ID0gY291cnNlLmRpc2N1c3Npb25bcXVlc3Rpb25JZF07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEdyaWQgY29udGFpbmVyIGp1c3RpZnk9XCJjZW50ZXJcIiBzcGFjaW5nPXswfSBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIHtpc1NpZ25lZEluICYmIChcbiAgICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0gY2xhc3NOYW1lPXtjbGFzc2VzLmdyaWRJdGVtfT5cbiAgICAgICAgICAgIDxBc3luY0Fuc3dlckNyZWF0ZSB7Li4udGhpcy5wcm9wc30gLz5cbiAgICAgICAgICA8L0dyaWQ+XG4gICAgICAgICl9XG4gICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19PlxuICAgICAgICAgIHt0eXBlb2YgYW5zd2VycyAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICAgICAgICAgIE9iamVjdC52YWx1ZXMoYW5zd2VycykubWFwKChhbnN3ZXIpID0+IChcbiAgICAgICAgICAgICAgPEFzeW5jQW5zd2VyQ2FyZCBrZXk9e2Fuc3dlci5hbnN3ZXJJZH0gey4uLmFuc3dlcn0gLz5cbiAgICAgICAgICAgICkpfVxuICAgICAgICA8L0dyaWQ+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5BbnN3ZXJUaW1lbGluZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICB1c2VyOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGNvdXJzZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBxdWVzdGlvbklkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG5cbiAgYWN0aW9uR2V0QWxsOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PlxuICBiaW5kQWN0aW9uQ3JlYXRvcnMoXG4gICAge1xuICAgICAgYWN0aW9uR2V0QWxsLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobnVsbCwgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKEFuc3dlclRpbWVsaW5lKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9EaXNjdXNzaW9uL0Fuc3dlclRpbWVsaW5lLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==