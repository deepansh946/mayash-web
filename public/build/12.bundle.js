webpackJsonp([12],{

/***/ "./node_modules/material-ui-icons/Email.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Email.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 4l-8 5-8-5V6l8 5 8-5v2z' });

var Email = function Email(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Email = (0, _pure2.default)(Email);
Email.muiName = 'SvgIcon';

exports.default = Email;

/***/ }),

/***/ "./node_modules/material-ui-icons/NavigateNext.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/NavigateNext.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z' });

var NavigateNext = function NavigateNext(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

NavigateNext = (0, _pure2.default)(NavigateNext);
NavigateNext.muiName = 'SvgIcon';

exports.default = NavigateNext;

/***/ }),

/***/ "./node_modules/material-ui-icons/Phone.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Phone.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6.62 10.79c1.44 2.83 3.76 5.14 6.59 6.59l2.2-2.2c.27-.27.67-.36 1.02-.24 1.12.37 2.33.57 3.57.57.55 0 1 .45 1 1V20c0 .55-.45 1-1 1-9.39 0-17-7.61-17-17 0-.55.45-1 1-1h3.5c.55 0 1 .45 1 1 0 1.25.2 2.45.57 3.57.11.35.03.74-.25 1.02l-2.2 2.2z' });

var Phone = function Phone(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Phone = (0, _pure2.default)(Phone);
Phone.muiName = 'SvgIcon';

exports.default = Phone;

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/components/NavigationButtonNext.js":
/*!*******************************************************!*\
  !*** ./src/client/components/NavigationButtonNext.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _NavigateNext = __webpack_require__(/*! material-ui-icons/NavigateNext */ "./node_modules/material-ui-icons/NavigateNext.js");

var _NavigateNext2 = _interopRequireDefault(_NavigateNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component is create to show naviagtion button to move next page
 *
 * @format
 */

var styles = function styles(theme) {
  var _root;

  return {
    root: (_root = {
      position: 'fixed',
      right: '2%',
      bottom: '4%'
    }, (0, _defineProperty3.default)(_root, theme.breakpoints.down('md'), {
      right: '2%',
      bottom: '2%'
    }), (0, _defineProperty3.default)(_root, theme.breakpoints.down('sm'), {
      right: '2%',
      bottom: '1%'
    }), _root)
  };
};

/**
 *
 * @param {object} classes
 * @param {path} to
 */
var NavigateButtonNext = function NavigateButtonNext(_ref) {
  var classes = _ref.classes,
      _ref$to = _ref.to,
      to = _ref$to === undefined ? '/' : _ref$to;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement(
      _Button2.default,
      {
        fab: true,
        color: 'accent',
        component: _Link2.default,
        className: classes.button,
        raised: true,
        to: to
      },
      _react2.default.createElement(_NavigateNext2.default, null)
    )
  );
};

NavigateButtonNext.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  to: _propTypes2.default.string.isRequired
};

NavigateButtonNext.defaultProps = {
  to: '/'
};

exports.default = (0, _styles.withStyles)(styles)(NavigateButtonNext);

/***/ }),

/***/ "./src/client/pages/Refund.js":
/*!************************************!*\
  !*** ./src/client/pages/Refund.js ***!
  \************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Phone = __webpack_require__(/*! material-ui-icons/Phone */ "./node_modules/material-ui-icons/Phone.js");

var _Phone2 = _interopRequireDefault(_Phone);

var _Email = __webpack_require__(/*! material-ui-icons/Email */ "./node_modules/material-ui-icons/Email.js");

var _Email2 = _interopRequireDefault(_Email);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _NavigationButtonNext = __webpack_require__(/*! ../components/NavigationButtonNext */ "./src/client/components/NavigationButtonNext.js");

var _NavigationButtonNext2 = _interopRequireDefault(_NavigationButtonNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component represents Donation page for mayash
 *
 * @format
 */

var styles = function styles(theme) {
  var _cardContent;

  return {
    root: {},
    cardHeader: {
      textAlign: 'center',
      padding: '1em'
    },
    media: {
      height: 300,
      width: 300
    },
    medium: {
      padding: '40px'
    },
    cardContent: (_cardContent = {
      paddingLeft: '8em',
      textAlign: 'justify',
      paddingRight: '8em'
    }, (0, _defineProperty3.default)(_cardContent, theme.breakpoints.down('md'), {
      paddingLeft: '2em',
      paddingRight: '2em'
    }), (0, _defineProperty3.default)(_cardContent, theme.breakpoints.down('sm'), {
      padding: '10px'
    }), _cardContent)
  };
};

var Refund = function (_Component) {
  (0, _inherits3.default)(Refund, _Component);

  function Refund(props) {
    (0, _classCallCheck3.default)(this, Refund);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Refund.__proto__ || (0, _getPrototypeOf2.default)(Refund)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Refund, [{
    key: 'render',
    value: function render() {
      var classes = this.props.classes;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          _Card2.default,
          null,
          _react2.default.createElement(_Card.CardHeader, { title: 'Refund Policy', className: classes.cardHeader }),
          _react2.default.createElement(
            _Card.CardContent,
            { className: classes.cardContent },
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              'Thank you for using Mayash. Please read this policy carefully. This is the Return and Refund Policy of Mayash. Return and Refund Policy for Mayash based on the',
              _react2.default.createElement(
                'a',
                { href: 'https://termsfeed.com/blog/sample-return-policy-ecommerce-stores/' },
                ' ',
                'Return Policy example from TermsFeed.'
              )
            ),
            _react2.default.createElement(_Card.CardHeader, {
              title: 'Digital products',
              className: classes.cardHeader
            }),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              'We issue refunds for digital services within 7 days of the original purchase of the Services applicable in certain cases.',
              _react2.default.createElement(
                'ul',
                null,
                _react2.default.createElement(
                  'li',
                  null,
                  'When Subscription for service is made by mistake, '
                )
              ),
              'We recommend contacting us for assistance if you experience any issues receiving or downloading our products.'
            ),
            _react2.default.createElement(_Card.CardHeader, {
              title: 'Digital Services',
              className: classes.cardHeader
            }),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              'We do not issue refunds for digital Services once the order is confirmed and the Service is sent. We recommend contacting us for assistance if you experience any issues receiving or Using our services.'
            ),
            _react2.default.createElement(_Card.CardHeader, { title: 'Contact us', className: classes.cardHeader }),
            _react2.default.createElement(
              _Typography2.default,
              { variant: 'headline', type: 'headline' },
              'If you have any questions about our Returns and Refunds Policy, please contact us:',
              _react2.default.createElement(
                _Typography2.default,
                null,
                _react2.default.createElement(_Phone2.default, null),
                _react2.default.createElement(
                  'a',
                  { href: 'tel:8770693644', className: classes.link },
                  '  ',
                  '+91-8770693644'
                )
              ),
              _react2.default.createElement(
                _Typography2.default,
                { type: 'headline' },
                _react2.default.createElement(_Email2.default, null),
                _react2.default.createElement(
                  'a',
                  { href: 'mailto:hbarve1@mayash.io', className: classes.link },
                  '  ',
                  'hbarve1@mayash.io'
                )
              )
            )
          )
        ),
        _react2.default.createElement(_NavigationButtonNext2.default, { to: '/contact-us' })
      );
    }
  }]);
  return Refund;
}(_react.Component);

Refund.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Refund);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRW1haWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL05hdmlnYXRlTmV4dC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvUGhvbmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL05hdmlnYXRpb25CdXR0b25OZXh0LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvcGFnZXMvUmVmdW5kLmpzIl0sIm5hbWVzIjpbInN0eWxlcyIsInRoZW1lIiwicm9vdCIsInBvc2l0aW9uIiwicmlnaHQiLCJib3R0b20iLCJicmVha3BvaW50cyIsImRvd24iLCJOYXZpZ2F0ZUJ1dHRvbk5leHQiLCJjbGFzc2VzIiwidG8iLCJidXR0b24iLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic3RyaW5nIiwiZGVmYXVsdFByb3BzIiwiY2FyZEhlYWRlciIsInRleHRBbGlnbiIsInBhZGRpbmciLCJtZWRpYSIsImhlaWdodCIsIndpZHRoIiwibWVkaXVtIiwiY2FyZENvbnRlbnQiLCJwYWRkaW5nTGVmdCIsInBhZGRpbmdSaWdodCIsIlJlZnVuZCIsInByb3BzIiwic3RhdGUiLCJsaW5rIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QscUhBQXFIOztBQUV2SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHdCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsc0RBQXNEOztBQUV4RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsd1BBQXdQOztBQUUxUztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHdCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7O0FBRTlGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxREFBcUQscUJBQXFCLFc7Ozs7Ozs7Ozs7Ozs7QUNsTDFFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDhDQUE4QyxpQkFBaUIscUJBQXFCLG9DQUFvQyw2REFBNkQsb0JBQW9CLEVBQUUsZUFBZTs7QUFFMU4saURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0ZBQWdGOztBQUVoRjs7QUFFQSxnRkFBZ0YsZUFBZTs7QUFFL0YseURBQXlELFVBQVUsdURBQXVEO0FBQzFIOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSx1Qjs7Ozs7Ozs7Ozs7OztBQzdHQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNmQTs7QUFFQTs7QUFFQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSw4RDs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDekRBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsVUFBVSxxQkFBcUI7QUFDNUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNSQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7OztBQUVBOzs7Ozs7QUFiQTs7Ozs7O0FBZUEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQztBQUNFQyxnQkFBVSxPQURaO0FBRUVDLGFBQU8sSUFGVDtBQUdFQyxjQUFRO0FBSFYsNENBSUdKLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBSkgsRUFJa0M7QUFDOUJILGFBQU8sSUFEdUI7QUFFOUJDLGNBQVE7QUFGc0IsS0FKbEMsd0NBUUdKLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBUkgsRUFRa0M7QUFDOUJILGFBQU8sSUFEdUI7QUFFOUJDLGNBQVE7QUFGc0IsS0FSbEM7QUFEeUIsR0FBWjtBQUFBLENBQWY7O0FBZ0JBOzs7OztBQUtBLElBQU1HLHFCQUFxQixTQUFyQkEsa0JBQXFCO0FBQUEsTUFBR0MsT0FBSCxRQUFHQSxPQUFIO0FBQUEscUJBQVlDLEVBQVo7QUFBQSxNQUFZQSxFQUFaLDJCQUFpQixHQUFqQjtBQUFBLFNBQ3pCO0FBQUE7QUFBQSxNQUFLLFdBQVdELFFBQVFQLElBQXhCO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsaUJBREY7QUFFRSxlQUFNLFFBRlI7QUFHRSxpQ0FIRjtBQUlFLG1CQUFXTyxRQUFRRSxNQUpyQjtBQUtFLG9CQUxGO0FBTUUsWUFBSUQ7QUFOTjtBQVFFO0FBUkY7QUFERixHQUR5QjtBQUFBLENBQTNCOztBQWVBRixtQkFBbUJJLFNBQW5CLEdBQStCO0FBQzdCSCxXQUFTLG9CQUFVSSxNQUFWLENBQWlCQyxVQURHO0FBRTdCSixNQUFJLG9CQUFVSyxNQUFWLENBQWlCRDtBQUZRLENBQS9COztBQUtBTixtQkFBbUJRLFlBQW5CLEdBQWtDO0FBQ2hDTixNQUFJO0FBRDRCLENBQWxDOztrQkFJZSx3QkFBV1YsTUFBWCxFQUFtQlEsa0JBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN0RGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBaEJBOzs7Ozs7QUFrQkEsSUFBTVIsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNLEVBRG1CO0FBRXpCZSxnQkFBWTtBQUNWQyxpQkFBVyxRQUREO0FBRVZDLGVBQVM7QUFGQyxLQUZhO0FBTXpCQyxXQUFPO0FBQ0xDLGNBQVEsR0FESDtBQUVMQyxhQUFPO0FBRkYsS0FOa0I7QUFVekJDLFlBQVE7QUFDTkosZUFBUztBQURILEtBVmlCO0FBYXpCSztBQUNFQyxtQkFBYSxLQURmO0FBRUVQLGlCQUFXLFNBRmI7QUFHRVEsb0JBQWM7QUFIaEIsbURBSUd6QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQUpILEVBSWtDO0FBQzlCa0IsbUJBQWEsS0FEaUI7QUFFOUJDLG9CQUFjO0FBRmdCLEtBSmxDLCtDQVFHekIsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FSSCxFQVFrQztBQUM5QlksZUFBUztBQURxQixLQVJsQztBQWJ5QixHQUFaO0FBQUEsQ0FBZjs7SUEyQk1RLE07OztBQUNKLGtCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUNRO0FBQUEsVUFDQ3BCLE9BREQsR0FDYSxLQUFLbUIsS0FEbEIsQ0FDQ25CLE9BREQ7O0FBRVAsYUFDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUE7QUFDRSw0REFBWSxPQUFNLGVBQWxCLEVBQWtDLFdBQVdBLFFBQVFRLFVBQXJELEdBREY7QUFFRTtBQUFBO0FBQUEsY0FBYSxXQUFXUixRQUFRZSxXQUFoQztBQUNFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUlFO0FBQUE7QUFBQSxrQkFBRyxNQUFLLG1FQUFSO0FBQ0csbUJBREg7QUFBQTtBQUFBO0FBSkYsYUFERjtBQVVFO0FBQ0UscUJBQU0sa0JBRFI7QUFFRSx5QkFBV2YsUUFBUVE7QUFGckIsY0FWRjtBQWNFO0FBQUE7QUFBQSxnQkFBWSxTQUFRLFVBQXBCLEVBQStCLE1BQUssVUFBcEM7QUFBQTtBQUdFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERixlQUhGO0FBQUE7QUFBQSxhQWRGO0FBdUJFO0FBQ0UscUJBQU0sa0JBRFI7QUFFRSx5QkFBV1IsUUFBUVE7QUFGckIsY0F2QkY7QUEyQkU7QUFBQTtBQUFBLGdCQUFZLFNBQVEsVUFBcEIsRUFBK0IsTUFBSyxVQUFwQztBQUFBO0FBQUEsYUEzQkY7QUFpQ0UsOERBQVksT0FBTSxZQUFsQixFQUErQixXQUFXUixRQUFRUSxVQUFsRCxHQWpDRjtBQWtDRTtBQUFBO0FBQUEsZ0JBQVksU0FBUSxVQUFwQixFQUErQixNQUFLLFVBQXBDO0FBQUE7QUFHRTtBQUFBO0FBQUE7QUFDRSxvRUFERjtBQUVFO0FBQUE7QUFBQSxvQkFBRyxNQUFLLGdCQUFSLEVBQXlCLFdBQVdSLFFBQVFxQixJQUE1QztBQUNHLHNCQURIO0FBQUE7QUFBQTtBQUZGLGVBSEY7QUFVRTtBQUFBO0FBQUEsa0JBQVksTUFBSyxVQUFqQjtBQUNFLG9FQURGO0FBRUU7QUFBQTtBQUFBLG9CQUFHLE1BQUssMEJBQVIsRUFBbUMsV0FBV3JCLFFBQVFxQixJQUF0RDtBQUNHLHNCQURIO0FBQUE7QUFBQTtBQUZGO0FBVkY7QUFsQ0Y7QUFGRixTQURGO0FBMkRFLHdFQUFzQixJQUFJLGFBQTFCO0FBM0RGLE9BREY7QUErREQ7Ozs7O0FBR0hILE9BQU9mLFNBQVAsR0FBbUI7QUFDakJILFdBQVMsb0JBQVVJLE1BQVYsQ0FBaUJDO0FBRFQsQ0FBbkI7O2tCQUllLDBCQUFXZCxNQUFYLEVBQW1CMkIsTUFBbkIsQyIsImZpbGUiOiIxMi5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMCA0SDRjLTEuMSAwLTEuOTkuOS0xLjk5IDJMMiAxOGMwIDEuMS45IDIgMiAyaDE2YzEuMSAwIDItLjkgMi0yVjZjMC0xLjEtLjktMi0yLTJ6bTAgNGwtOCA1LTgtNVY2bDggNSA4LTV2MnonIH0pO1xuXG52YXIgRW1haWwgPSBmdW5jdGlvbiBFbWFpbChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRW1haWwgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEVtYWlsKTtcbkVtYWlsLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEVtYWlsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0VtYWlsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FbWFpbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEwIDEyIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTAgNkw4LjU5IDcuNDEgMTMuMTcgMTJsLTQuNTggNC41OUwxMCAxOGw2LTZ6JyB9KTtcblxudmFyIE5hdmlnYXRlTmV4dCA9IGZ1bmN0aW9uIE5hdmlnYXRlTmV4dChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuTmF2aWdhdGVOZXh0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShOYXZpZ2F0ZU5leHQpO1xuTmF2aWdhdGVOZXh0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IE5hdmlnYXRlTmV4dDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9OYXZpZ2F0ZU5leHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL05hdmlnYXRlTmV4dC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYuNjIgMTAuNzljMS40NCAyLjgzIDMuNzYgNS4xNCA2LjU5IDYuNTlsMi4yLTIuMmMuMjctLjI3LjY3LS4zNiAxLjAyLS4yNCAxLjEyLjM3IDIuMzMuNTcgMy41Ny41Ny41NSAwIDEgLjQ1IDEgMVYyMGMwIC41NS0uNDUgMS0xIDEtOS4zOSAwLTE3LTcuNjEtMTctMTcgMC0uNTUuNDUtMSAxLTFoMy41Yy41NSAwIDEgLjQ1IDEgMSAwIDEuMjUuMiAyLjQ1LjU3IDMuNTcuMTEuMzUuMDMuNzQtLjI1IDEuMDJsLTIuMiAyLjJ6JyB9KTtcblxudmFyIFBob25lID0gZnVuY3Rpb24gUGhvbmUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblBob25lID0gKDAsIF9wdXJlMi5kZWZhdWx0KShQaG9uZSk7XG5QaG9uZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBQaG9uZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9QaG9uZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvUGhvbmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxMCAxMiAzNiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2lubGluZS1ibG9jaycsXG4gICAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcbiAgICAgIGhlaWdodDogMjQsXG4gICAgICB3aWR0aDogMjQsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZScsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdmaWxsJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlclxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRWxlbWVudHMgcGFzc2VkIGludG8gdGhlIFNWRyBJY29uLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm92aWRlcyBhIGh1bWFuLXJlYWRhYmxlIHRpdGxlIGZvciB0aGUgZWxlbWVudCB0aGF0IGNvbnRhaW5zIGl0LlxuICAgKiBodHRwczovL3d3dy53My5vcmcvVFIvU1ZHLWFjY2Vzcy8jRXF1aXZhbGVudFxuICAgKi9cbiAgdGl0bGVBY2Nlc3M6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsbG93cyB5b3UgdG8gcmVkZWZpbmUgd2hhdCB0aGUgY29vcmRpbmF0ZXMgd2l0aG91dCB1bml0cyBtZWFuIGluc2lkZSBhbiBzdmcgZWxlbWVudC5cbiAgICogRm9yIGV4YW1wbGUsIGlmIHRoZSBTVkcgZWxlbWVudCBpcyA1MDAgKHdpZHRoKSBieSAyMDAgKGhlaWdodCksXG4gICAqIGFuZCB5b3UgcGFzcyB2aWV3Qm94PVwiMCAwIDUwIDIwXCIsXG4gICAqIHRoaXMgbWVhbnMgdGhhdCB0aGUgY29vcmRpbmF0ZXMgaW5zaWRlIHRoZSBzdmcgd2lsbCBnbyBmcm9tIHRoZSB0b3AgbGVmdCBjb3JuZXIgKDAsMClcbiAgICogdG8gYm90dG9tIHJpZ2h0ICg1MCwyMCkgYW5kIGVhY2ggdW5pdCB3aWxsIGJlIHdvcnRoIDEwcHguXG4gICAqL1xuICB2aWV3Qm94OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWRcbn07XG5cbnZhciBTdmdJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoU3ZnSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3ZnSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBTdmdJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoU3ZnSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU3ZnSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU3ZnSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgdGl0bGVBY2Nlc3MgPSBfcHJvcHMudGl0bGVBY2Nlc3MsXG4gICAgICAgICAgdmlld0JveCA9IF9wcm9wcy52aWV3Qm94LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ3RpdGxlQWNjZXNzJywgJ3ZpZXdCb3gnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzdmcnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBmb2N1c2FibGU6ICdmYWxzZScsXG4gICAgICAgICAgdmlld0JveDogdmlld0JveCxcbiAgICAgICAgICAnYXJpYS1oaWRkZW4nOiB0aXRsZUFjY2VzcyA/ICdmYWxzZScgOiAndHJ1ZSdcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICB0aXRsZUFjY2VzcyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICd0aXRsZScsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICB0aXRsZUFjY2Vzc1xuICAgICAgICApIDogbnVsbCxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBTdmdJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuU3ZnSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpZXdCb3g6ICcwIDAgMjQgMjQnLFxuICBjb2xvcjogJ2luaGVyaXQnXG59O1xuU3ZnSWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVN2Z0ljb24nIH0pKFN2Z0ljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi9TdmdJY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2ludmFyaWFudCA9IHJlcXVpcmUoJ2ludmFyaWFudCcpO1xuXG52YXIgX2ludmFyaWFudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbnZhcmlhbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGlzTW9kaWZpZWRFdmVudCA9IGZ1bmN0aW9uIGlzTW9kaWZpZWRFdmVudChldmVudCkge1xuICByZXR1cm4gISEoZXZlbnQubWV0YUtleSB8fCBldmVudC5hbHRLZXkgfHwgZXZlbnQuY3RybEtleSB8fCBldmVudC5zaGlmdEtleSk7XG59O1xuXG4vKipcbiAqIFRoZSBwdWJsaWMgQVBJIGZvciByZW5kZXJpbmcgYSBoaXN0b3J5LWF3YXJlIDxhPi5cbiAqL1xuXG52YXIgTGluayA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gIF9pbmhlcml0cyhMaW5rLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBMaW5rKCkge1xuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgTGluayk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9SZWFjdCRDb21wb25lbnQuY2FsbC5hcHBseShfUmVhY3QkQ29tcG9uZW50LCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlQ2xpY2sgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkNsaWNrKSBfdGhpcy5wcm9wcy5vbkNsaWNrKGV2ZW50KTtcblxuICAgICAgaWYgKCFldmVudC5kZWZhdWx0UHJldmVudGVkICYmIC8vIG9uQ2xpY2sgcHJldmVudGVkIGRlZmF1bHRcbiAgICAgIGV2ZW50LmJ1dHRvbiA9PT0gMCAmJiAvLyBpZ25vcmUgcmlnaHQgY2xpY2tzXG4gICAgICAhX3RoaXMucHJvcHMudGFyZ2V0ICYmIC8vIGxldCBicm93c2VyIGhhbmRsZSBcInRhcmdldD1fYmxhbmtcIiBldGMuXG4gICAgICAhaXNNb2RpZmllZEV2ZW50KGV2ZW50KSAvLyBpZ25vcmUgY2xpY2tzIHdpdGggbW9kaWZpZXIga2V5c1xuICAgICAgKSB7XG4gICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcblxuICAgICAgICAgIHZhciBoaXN0b3J5ID0gX3RoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeTtcbiAgICAgICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICAgICAgcmVwbGFjZSA9IF90aGlzJHByb3BzLnJlcGxhY2UsXG4gICAgICAgICAgICAgIHRvID0gX3RoaXMkcHJvcHMudG87XG5cblxuICAgICAgICAgIGlmIChyZXBsYWNlKSB7XG4gICAgICAgICAgICBoaXN0b3J5LnJlcGxhY2UodG8pO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBoaXN0b3J5LnB1c2godG8pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgTGluay5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICByZXBsYWNlID0gX3Byb3BzLnJlcGxhY2UsXG4gICAgICAgIHRvID0gX3Byb3BzLnRvLFxuICAgICAgICBpbm5lclJlZiA9IF9wcm9wcy5pbm5lclJlZixcbiAgICAgICAgcHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMoX3Byb3BzLCBbJ3JlcGxhY2UnLCAndG8nLCAnaW5uZXJSZWYnXSk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tdW51c2VkLXZhcnNcblxuICAgICgwLCBfaW52YXJpYW50Mi5kZWZhdWx0KSh0aGlzLmNvbnRleHQucm91dGVyLCAnWW91IHNob3VsZCBub3QgdXNlIDxMaW5rPiBvdXRzaWRlIGEgPFJvdXRlcj4nKTtcblxuICAgIHZhciBocmVmID0gdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5LmNyZWF0ZUhyZWYodHlwZW9mIHRvID09PSAnc3RyaW5nJyA/IHsgcGF0aG5hbWU6IHRvIH0gOiB0byk7XG5cbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2EnLCBfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgb25DbGljazogdGhpcy5oYW5kbGVDbGljaywgaHJlZjogaHJlZiwgcmVmOiBpbm5lclJlZiB9KSk7XG4gIH07XG5cbiAgcmV0dXJuIExpbms7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5MaW5rLnByb3BUeXBlcyA9IHtcbiAgb25DbGljazogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLFxuICB0YXJnZXQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIHRvOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XSkuaXNSZXF1aXJlZCxcbiAgaW5uZXJSZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jXSlcbn07XG5MaW5rLmRlZmF1bHRQcm9wcyA9IHtcbiAgcmVwbGFjZTogZmFsc2Vcbn07XG5MaW5rLmNvbnRleHRUeXBlcyA9IHtcbiAgcm91dGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICBoaXN0b3J5OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICAgIHB1c2g6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICBjcmVhdGVIcmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZFxuICAgIH0pLmlzUmVxdWlyZWRcbiAgfSkuaXNSZXF1aXJlZFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9IExpbms7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNiAyNyAyOCAzMCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA1NyA1OCA1OSA2MSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gcmVxdWlyZSgnLi91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsJyk7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUZhY3RvcnkgPSBmdW5jdGlvbiBjcmVhdGVGYWN0b3J5KHR5cGUpIHtcbiAgdmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50ID0gKDAsIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Mi5kZWZhdWx0KSh0eXBlKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwLCBjKSB7XG4gICAgcmV0dXJuICgwLCBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIuZGVmYXVsdCkoZmFsc2UsIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwLCBjKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUZhY3Rvcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGdldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gZ2V0RGlzcGxheU5hbWUoQ29tcG9uZW50KSB7XG4gIGlmICh0eXBlb2YgQ29tcG9uZW50ID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBDb21wb25lbnQ7XG4gIH1cblxuICBpZiAoIUNvbXBvbmVudCkge1xuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgaXNDbGFzc0NvbXBvbmVudCA9IGZ1bmN0aW9uIGlzQ2xhc3NDb21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKENvbXBvbmVudCAmJiBDb21wb25lbnQucHJvdG90eXBlICYmIF90eXBlb2YoQ29tcG9uZW50LnByb3RvdHlwZS5pc1JlYWN0Q29tcG9uZW50KSA9PT0gJ29iamVjdCcpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNDbGFzc0NvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc0NsYXNzQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc0NsYXNzQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSBmdW5jdGlvbiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbih0eXBlb2YgQ29tcG9uZW50ID09PSAnZnVuY3Rpb24nICYmICEoMCwgX2lzQ2xhc3NDb21wb25lbnQyLmRlZmF1bHQpKENvbXBvbmVudCkgJiYgIUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgJiYgIUNvbXBvbmVudC5jb250ZXh0VHlwZXMgJiYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicgfHwgIUNvbXBvbmVudC5wcm9wVHlwZXMpKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5ID0gcmVxdWlyZSgnLi9jcmVhdGVFYWdlckZhY3RvcnknKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJGYWN0b3J5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSBmdW5jdGlvbiBjcmVhdGVFYWdlckVsZW1lbnRVdGlsKGhhc0tleSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHByb3BzLCBjaGlsZHJlbikge1xuICBpZiAoIWhhc0tleSAmJiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCkge1xuICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgcmV0dXJuIHR5cGUoX2V4dGVuZHMoe30sIHByb3BzLCB7IGNoaWxkcmVuOiBjaGlsZHJlbiB9KSk7XG4gICAgfVxuICAgIHJldHVybiB0eXBlKHByb3BzKTtcbiAgfVxuXG4gIHZhciBDb21wb25lbnQgPSB0eXBlO1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgIENvbXBvbmVudCxcbiAgICAgIHByb3BzLFxuICAgICAgY2hpbGRyZW5cbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgcHJvcHMpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRWFnZXJFbGVtZW50VXRpbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9nZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vZ2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9nZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciB3cmFwRGlzcGxheU5hbWUgPSBmdW5jdGlvbiB3cmFwRGlzcGxheU5hbWUoQmFzZUNvbXBvbmVudCwgaG9jTmFtZSkge1xuICByZXR1cm4gaG9jTmFtZSArICcoJyArICgwLCBfZ2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpICsgJyknO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd3JhcERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIi8qKlxuICogVGhpcyBjb21wb25lbnQgaXMgY3JlYXRlIHRvIHNob3cgbmF2aWFndGlvbiBidXR0b24gdG8gbW92ZSBuZXh0IHBhZ2VcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBMaW5rIGZyb20gJ3JlYWN0LXJvdXRlci1kb20vTGluayc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcblxuaW1wb3J0IE5hdmlnYXRlTmV4dEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvTmF2aWdhdGVOZXh0JztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7XG4gICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgcmlnaHQ6ICcyJScsXG4gICAgYm90dG9tOiAnNCUnLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge1xuICAgICAgcmlnaHQ6ICcyJScsXG4gICAgICBib3R0b206ICcyJScsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIHJpZ2h0OiAnMiUnLFxuICAgICAgYm90dG9tOiAnMSUnLFxuICAgIH0sXG4gIH0sXG59KTtcblxuLyoqXG4gKlxuICogQHBhcmFtIHtvYmplY3R9IGNsYXNzZXNcbiAqIEBwYXJhbSB7cGF0aH0gdG9cbiAqL1xuY29uc3QgTmF2aWdhdGVCdXR0b25OZXh0ID0gKHsgY2xhc3NlcywgdG8gPSAnLycgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICA8QnV0dG9uXG4gICAgICBmYWJcbiAgICAgIGNvbG9yPVwiYWNjZW50XCJcbiAgICAgIGNvbXBvbmVudD17TGlua31cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5idXR0b259XG4gICAgICByYWlzZWRcbiAgICAgIHRvPXt0b31cbiAgICA+XG4gICAgICA8TmF2aWdhdGVOZXh0SWNvbiAvPlxuICAgIDwvQnV0dG9uPlxuICA8L2Rpdj5cbik7XG5cbk5hdmlnYXRlQnV0dG9uTmV4dC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgdG86IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbn07XG5cbk5hdmlnYXRlQnV0dG9uTmV4dC5kZWZhdWx0UHJvcHMgPSB7XG4gIHRvOiAnLycsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTmF2aWdhdGVCdXR0b25OZXh0KTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9OYXZpZ2F0aW9uQnV0dG9uTmV4dC5qcyIsIi8qKlxuICogVGhpcyBjb21wb25lbnQgcmVwcmVzZW50cyBEb25hdGlvbiBwYWdlIGZvciBtYXlhc2hcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IFBob25lSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9QaG9uZSc7XG5pbXBvcnQgRW1haWxJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0VtYWlsJztcblxuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciwgQ2FyZENvbnRlbnQsIENhcmRBY3Rpb25zIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5cbmltcG9ydCBOYXZpZ2F0aW9uQnV0dG9uTmV4dCBmcm9tICcuLi9jb21wb25lbnRzL05hdmlnYXRpb25CdXR0b25OZXh0JztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7fSxcbiAgY2FyZEhlYWRlcjoge1xuICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gICAgcGFkZGluZzogJzFlbScsXG4gIH0sXG4gIG1lZGlhOiB7XG4gICAgaGVpZ2h0OiAzMDAsXG4gICAgd2lkdGg6IDMwMCxcbiAgfSxcbiAgbWVkaXVtOiB7XG4gICAgcGFkZGluZzogJzQwcHgnLFxuICB9LFxuICBjYXJkQ29udGVudDoge1xuICAgIHBhZGRpbmdMZWZ0OiAnOGVtJyxcbiAgICB0ZXh0QWxpZ246ICdqdXN0aWZ5JyxcbiAgICBwYWRkaW5nUmlnaHQ6ICc4ZW0nLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge1xuICAgICAgcGFkZGluZ0xlZnQ6ICcyZW0nLFxuICAgICAgcGFkZGluZ1JpZ2h0OiAnMmVtJyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgcGFkZGluZzogJzEwcHgnLFxuICAgIH0sXG4gIH0sXG59KTtcblxuY2xhc3MgUmVmdW5kIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMgfSA9IHRoaXMucHJvcHM7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXY+XG4gICAgICAgIDxDYXJkPlxuICAgICAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPVwiUmVmdW5kIFBvbGljeVwiIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfSAvPlxuICAgICAgICAgIDxDYXJkQ29udGVudCBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZENvbnRlbnR9PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIFRoYW5rIHlvdSBmb3IgdXNpbmcgTWF5YXNoLiBQbGVhc2UgcmVhZCB0aGlzIHBvbGljeSBjYXJlZnVsbHkuXG4gICAgICAgICAgICAgIFRoaXMgaXMgdGhlIFJldHVybiBhbmQgUmVmdW5kIFBvbGljeSBvZiBNYXlhc2guIFJldHVybiBhbmQgUmVmdW5kXG4gICAgICAgICAgICAgIFBvbGljeSBmb3IgTWF5YXNoIGJhc2VkIG9uIHRoZVxuICAgICAgICAgICAgICA8YSBocmVmPVwiaHR0cHM6Ly90ZXJtc2ZlZWQuY29tL2Jsb2cvc2FtcGxlLXJldHVybi1wb2xpY3ktZWNvbW1lcmNlLXN0b3Jlcy9cIj5cbiAgICAgICAgICAgICAgICB7JyAnfVxuICAgICAgICAgICAgICAgIFJldHVybiBQb2xpY3kgZXhhbXBsZSBmcm9tIFRlcm1zRmVlZC5cbiAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgICAgdGl0bGU9XCJEaWdpdGFsIHByb2R1Y3RzXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmRIZWFkZXJ9XG4gICAgICAgICAgICAvPlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIFdlIGlzc3VlIHJlZnVuZHMgZm9yIGRpZ2l0YWwgc2VydmljZXMgd2l0aGluIDcgZGF5cyBvZiB0aGVcbiAgICAgICAgICAgICAgb3JpZ2luYWwgcHVyY2hhc2Ugb2YgdGhlIFNlcnZpY2VzIGFwcGxpY2FibGUgaW4gY2VydGFpbiBjYXNlcy5cbiAgICAgICAgICAgICAgPHVsPlxuICAgICAgICAgICAgICAgIDxsaT5XaGVuIFN1YnNjcmlwdGlvbiBmb3Igc2VydmljZSBpcyBtYWRlIGJ5IG1pc3Rha2UsIDwvbGk+XG4gICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgIFdlIHJlY29tbWVuZCBjb250YWN0aW5nIHVzIGZvciBhc3Npc3RhbmNlIGlmIHlvdSBleHBlcmllbmNlIGFueVxuICAgICAgICAgICAgICBpc3N1ZXMgcmVjZWl2aW5nIG9yIGRvd25sb2FkaW5nIG91ciBwcm9kdWN0cy5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICAgIHRpdGxlPVwiRGlnaXRhbCBTZXJ2aWNlc1wiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfVxuICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHZhcmlhbnQ9XCJoZWFkbGluZVwiIHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICBXZSBkbyBub3QgaXNzdWUgcmVmdW5kcyBmb3IgZGlnaXRhbCBTZXJ2aWNlcyBvbmNlIHRoZSBvcmRlciBpc1xuICAgICAgICAgICAgICBjb25maXJtZWQgYW5kIHRoZSBTZXJ2aWNlIGlzIHNlbnQuIFdlIHJlY29tbWVuZCBjb250YWN0aW5nIHVzIGZvclxuICAgICAgICAgICAgICBhc3Npc3RhbmNlIGlmIHlvdSBleHBlcmllbmNlIGFueSBpc3N1ZXMgcmVjZWl2aW5nIG9yIFVzaW5nIG91clxuICAgICAgICAgICAgICBzZXJ2aWNlcy5cbiAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPVwiQ29udGFjdCB1c1wiIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfSAvPlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdmFyaWFudD1cImhlYWRsaW5lXCIgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgIElmIHlvdSBoYXZlIGFueSBxdWVzdGlvbnMgYWJvdXQgb3VyIFJldHVybnMgYW5kIFJlZnVuZHMgUG9saWN5LFxuICAgICAgICAgICAgICBwbGVhc2UgY29udGFjdCB1czpcbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHk+XG4gICAgICAgICAgICAgICAgPFBob25lSWNvbiAvPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJ0ZWw6ODc3MDY5MzY0NFwiIGNsYXNzTmFtZT17Y2xhc3Nlcy5saW5rfT5cbiAgICAgICAgICAgICAgICAgIHsnICAnfVxuICAgICAgICAgICAgICAgICAgKzkxLTg3NzA2OTM2NDRcbiAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImhlYWRsaW5lXCI+XG4gICAgICAgICAgICAgICAgPEVtYWlsSWNvbiAvPlxuICAgICAgICAgICAgICAgIDxhIGhyZWY9XCJtYWlsdG86aGJhcnZlMUBtYXlhc2guaW9cIiBjbGFzc05hbWU9e2NsYXNzZXMubGlua30+XG4gICAgICAgICAgICAgICAgICB7JyAgJ31cbiAgICAgICAgICAgICAgICAgIGhiYXJ2ZTFAbWF5YXNoLmlvXG4gICAgICAgICAgICAgICAgPC9hPlxuICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICAgIHsvKiBCeSB2aXNpdGluZyB0aGlzIHBhZ2Ugb24gKi99XG4gICAgICAgICAgICAgIHsvKiBvdXIgd2Vic2l0ZTogaHR0cHM6Ly9nb28uZ2wvZm9ybXMvbEJPMk9JREZyeVhVVmpNdzEgKi99XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgPC9DYXJkPlxuICAgICAgICA8TmF2aWdhdGlvbkJ1dHRvbk5leHQgdG89eycvY29udGFjdC11cyd9IC8+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblJlZnVuZC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShSZWZ1bmQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9wYWdlcy9SZWZ1bmQuanMiXSwic291cmNlUm9vdCI6IiJ9