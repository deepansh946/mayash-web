webpackJsonp([28,35,37,40,42,61],{

/***/ "./node_modules/dom-helpers/activeElement.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/activeElement.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = activeElement;

var _ownerDocument = __webpack_require__(/*! ./ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function activeElement() {
  var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _ownerDocument2.default)();

  try {
    return doc.activeElement;
  } catch (e) {/* ie throws if no active element */}
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/ownerDocument.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/ownerDocument.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ownerDocument;
function ownerDocument(node) {
  return node && node.ownerDocument || document;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/query/isWindow.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/query/isWindow.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getWindow;
function getWindow(node) {
  return node === node.window ? node : node.nodeType === 9 ? node.defaultView || node.parentWindow : false;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/util/scrollbarSize.js":
/*!********************************************************!*\
  !*** ./node_modules/dom-helpers/util/scrollbarSize.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (recalc) {
  if (!size && size !== 0 || recalc) {
    if (_inDOM2.default) {
      var scrollDiv = document.createElement('div');

      scrollDiv.style.position = 'absolute';
      scrollDiv.style.top = '-9999px';
      scrollDiv.style.width = '50px';
      scrollDiv.style.height = '50px';
      scrollDiv.style.overflow = 'scroll';

      document.body.appendChild(scrollDiv);
      size = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
    }
  }

  return size;
};

var _inDOM = __webpack_require__(/*! ./inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = void 0;

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/material-ui-icons/Comment.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui-icons/Comment.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var Comment = function Comment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Comment = (0, _pure2.default)(Comment);
Comment.muiName = 'SvgIcon';

exports.default = Comment;

/***/ }),

/***/ "./node_modules/material-ui-icons/Delete.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui-icons/Delete.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z' });

var Delete = function Delete(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Delete = (0, _pure2.default)(Delete);
Delete.muiName = 'SvgIcon';

exports.default = Delete;

/***/ }),

/***/ "./node_modules/material-ui-icons/Favorite.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/Favorite.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z' });

var Favorite = function Favorite(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Favorite = (0, _pure2.default)(Favorite);
Favorite.muiName = 'SvgIcon';

exports.default = Favorite;

/***/ }),

/***/ "./node_modules/material-ui-icons/MoreVert.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/MoreVert.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });

var MoreVert = function MoreVert(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

MoreVert = (0, _pure2.default)(MoreVert);
MoreVert.muiName = 'SvgIcon';

exports.default = MoreVert;

/***/ }),

/***/ "./node_modules/material-ui-icons/RemoveRedEye.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/RemoveRedEye.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z' });

var RemoveRedEye = function RemoveRedEye(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

RemoveRedEye = (0, _pure2.default)(RemoveRedEye);
RemoveRedEye.muiName = 'SvgIcon';

exports.default = RemoveRedEye;

/***/ }),

/***/ "./node_modules/material-ui-icons/Share.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Share.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z' });

var Share = function Share(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Share = (0, _pure2.default)(Share);
Share.muiName = 'SvgIcon';

exports.default = Share;

/***/ }),

/***/ "./node_modules/material-ui-icons/TurnedIn.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/TurnedIn.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z' });

var TurnedIn = function TurnedIn(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

TurnedIn = (0, _pure2.default)(TurnedIn);
TurnedIn.muiName = 'SvgIcon';

exports.default = TurnedIn;

/***/ }),

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Badge/Badge.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/Badge.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var RADIUS = 12;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'inline-flex'
    },
    badge: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      top: -RADIUS,
      right: -RADIUS,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeight,
      fontSize: theme.typography.pxToRem(RADIUS),
      width: RADIUS * 2,
      height: RADIUS * 2,
      borderRadius: '50%',
      backgroundColor: theme.palette.color,
      color: theme.palette.textColor,
      zIndex: 1 // Render the badge on top of potential ripples.
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content rendered within the badge.
   */
  badgeContent: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * The badge will be added relative to this node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'primary', 'accent']).isRequired
};

var Badge = function (_React$Component) {
  (0, _inherits3.default)(Badge, _React$Component);

  function Badge() {
    (0, _classCallCheck3.default)(this, Badge);
    return (0, _possibleConstructorReturn3.default)(this, (Badge.__proto__ || (0, _getPrototypeOf2.default)(Badge)).apply(this, arguments));
  }

  (0, _createClass3.default)(Badge, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          badgeContent = _props.badgeContent,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          children = _props.children,
          other = (0, _objectWithoutProperties3.default)(_props, ['badgeContent', 'classes', 'className', 'color', 'children']);

      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var badgeClassName = (0, _classnames2.default)(classes.badge, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'));

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        children,
        _react2.default.createElement(
          'span',
          { className: badgeClassName },
          badgeContent
        )
      );
    }
  }]);
  return Badge;
}(_react2.default.Component);

Badge.defaultProps = {
  color: 'default'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBadge' })(Badge);

/***/ }),

/***/ "./node_modules/material-ui/Badge/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Badge = __webpack_require__(/*! ./Badge */ "./node_modules/material-ui/Badge/Badge.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Badge).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Dialog/Dialog.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Dialog/Dialog.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Modal = __webpack_require__(/*! ../Modal */ "./node_modules/material-ui/Modal/index.js");

var _Modal2 = _interopRequireDefault(_Modal);

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Modal

var babelPluginFlowReactPropTypes_proptype_ComponentType = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    paper: {
      display: 'flex',
      margin: theme.spacing.unit * 4,
      flexDirection: 'column',
      flex: '0 1 auto',
      position: 'relative',
      maxHeight: '90vh',
      overflowY: 'auto', // Fix IE11 issue, to remove at some point.
      '&:focus': {
        outline: 'none'
      }
    },
    paperWidthXs: {
      maxWidth: Math.max(theme.breakpoints.values.xs, 360)
    },
    paperWidthSm: {
      maxWidth: theme.breakpoints.values.sm
    },
    paperWidthMd: {
      maxWidth: theme.breakpoints.values.md
    },
    fullWidth: {
      width: '100%'
    },
    fullScreen: {
      margin: 0,
      width: '100%',
      maxWidth: '100%',
      height: '100%',
      maxHeight: '100%',
      borderRadius: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Dialog children, usually the included sub-components.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, it will be full-screen
   */
  fullScreen: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  transitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * Determine the max width of the dialog.
   * The dialog width grows with the size of the screen, this property is useful
   * on the desktop where you might need some coherent different width size across your
   * application.
   */
  maxWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['xs', 'sm', 'md']).isRequired,

  /**
   * If specified, stretches dialog to max width.
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Callback fired when the backdrop is clicked.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the dialog enters.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is released and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the dialog exits.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Dialog is open.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Transition component.
   */
  transition: typeof babelPluginFlowReactPropTypes_proptype_ComponentType === 'function' ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired : babelPluginFlowReactPropTypes_proptype_ComponentType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ComponentType).isRequired
};

/**
 * Dialogs are overlaid modal paper based components with a backdrop.
 */
var Dialog = function (_React$Component) {
  (0, _inherits3.default)(Dialog, _React$Component);

  function Dialog() {
    (0, _classCallCheck3.default)(this, Dialog);
    return (0, _possibleConstructorReturn3.default)(this, (Dialog.__proto__ || (0, _getPrototypeOf2.default)(Dialog)).apply(this, arguments));
  }

  (0, _createClass3.default)(Dialog, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          fullScreen = _props.fullScreen,
          ignoreBackdropClick = _props.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props.ignoreEscapeKeyUp,
          transitionDuration = _props.transitionDuration,
          maxWidth = _props.maxWidth,
          fullWidth = _props.fullWidth,
          open = _props.open,
          onBackdropClick = _props.onBackdropClick,
          onEscapeKeyUp = _props.onEscapeKeyUp,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onEntered = _props.onEntered,
          onExit = _props.onExit,
          onExiting = _props.onExiting,
          onExited = _props.onExited,
          onRequestClose = _props.onRequestClose,
          TransitionProp = _props.transition,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'fullScreen', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'transitionDuration', 'maxWidth', 'fullWidth', 'open', 'onBackdropClick', 'onEscapeKeyUp', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'onRequestClose', 'transition']);


      return _react2.default.createElement(
        _Modal2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, className),
          BackdropTransitionDuration: transitionDuration,
          ignoreBackdropClick: ignoreBackdropClick,
          ignoreEscapeKeyUp: ignoreEscapeKeyUp,
          onBackdropClick: onBackdropClick,
          onEscapeKeyUp: onEscapeKeyUp,
          onRequestClose: onRequestClose,
          show: open
        }, other),
        _react2.default.createElement(
          TransitionProp,
          {
            appear: true,
            'in': open,
            timeout: transitionDuration,
            onEnter: onEnter,
            onEntering: onEntering,
            onEntered: onEntered,
            onExit: onExit,
            onExiting: onExiting,
            onExited: onExited
          },
          _react2.default.createElement(
            _Paper2.default,
            {
              elevation: 24,
              className: (0, _classnames2.default)(classes.paper, classes['paperWidth' + (0, _helpers.capitalizeFirstLetter)(maxWidth)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.fullScreen, fullScreen), (0, _defineProperty3.default)(_classNames, classes.fullWidth, fullWidth), _classNames))
            },
            children
          )
        )
      );
    }
  }]);
  return Dialog;
}(_react2.default.Component);

Dialog.defaultProps = {
  fullScreen: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  transitionDuration: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  },
  maxWidth: 'sm',
  fullWidth: false,
  open: false,
  transition: _Fade2.default
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialog' })(Dialog);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogActions.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogActions.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

__webpack_require__(/*! ../Button */ "./node_modules/material-ui/Button/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// So we don't have any override priority issue.

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      margin: theme.spacing.unit + 'px ' + theme.spacing.unit / 2 + 'px',
      flex: '0 0 auto'
    },
    action: {
      margin: '0 ' + theme.spacing.unit / 2 + 'px'
    },
    button: {
      minWidth: 64
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogActions(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.createElement(
    'div',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    _react2.default.Children.map(children, function (child) {
      if (!_react2.default.isValidElement(child)) {
        return null;
      }

      return _react2.default.createElement(
        'div',
        { className: classes.action },
        _react2.default.cloneElement(child, {
          className: (0, _classnames2.default)(classes.button, child.props.className)
        })
      );
    })
  );
}

DialogActions.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * @ignore
   */
  theme: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogActions' })(DialogActions);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogContent.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogContent.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  var spacing = theme.spacing.unit * 3;
  return {
    root: {
      flex: '1 1 auto',
      overflowY: 'auto',
      padding: '0 ' + spacing + 'px ' + spacing + 'px ' + spacing + 'px',
      '&:first-child': {
        paddingTop: spacing
      }
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogContent(props) {
  var classes = props.classes,
      children = props.children,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['classes', 'children', 'className']);


  return _react2.default.createElement(
    'div',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    children
  );
}

DialogContent.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogContent' })(DialogContent);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogContentText.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogContentText.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends3.default)({}, theme.typography.subheading, {
      color: theme.palette.text.secondary,
      margin: 0
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogContentText(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.createElement(
    'p',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    children
  );
}

DialogContentText.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * @ignore
   */
  theme: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogContentText' })(DialogContentText);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogTitle.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogTitle.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! ../Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      margin: 0,
      padding: theme.spacing.unit * 3 + 'px ' + theme.spacing.unit * 3 + 'px       20px ' + theme.spacing.unit * 3 + 'px',
      flex: '0 0 auto'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the children won't be wrapped by a typography component.
   * For instance, this can be useful to render an h4 instead of the default h2.
   */
  disableTypography: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var DialogTitle = function (_React$Component) {
  (0, _inherits3.default)(DialogTitle, _React$Component);

  function DialogTitle() {
    (0, _classCallCheck3.default)(this, DialogTitle);
    return (0, _possibleConstructorReturn3.default)(this, (DialogTitle.__proto__ || (0, _getPrototypeOf2.default)(DialogTitle)).apply(this, arguments));
  }

  (0, _createClass3.default)(DialogTitle, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          disableTypography = _props.disableTypography,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'disableTypography']);


      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
        disableTypography ? children : _react2.default.createElement(
          _Typography2.default,
          { type: 'title' },
          children
        )
      );
    }
  }]);
  return DialogTitle;
}(_react2.default.Component);

DialogTitle.defaultProps = {
  disableTypography: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogTitle' })(DialogTitle);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Dialog/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Dialog = __webpack_require__(/*! ./Dialog */ "./node_modules/material-ui/Dialog/Dialog.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Dialog).default;
  }
});

var _DialogActions = __webpack_require__(/*! ./DialogActions */ "./node_modules/material-ui/Dialog/DialogActions.js");

Object.defineProperty(exports, 'DialogActions', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogActions).default;
  }
});

var _DialogTitle = __webpack_require__(/*! ./DialogTitle */ "./node_modules/material-ui/Dialog/DialogTitle.js");

Object.defineProperty(exports, 'DialogTitle', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogTitle).default;
  }
});

var _DialogContent = __webpack_require__(/*! ./DialogContent */ "./node_modules/material-ui/Dialog/DialogContent.js");

Object.defineProperty(exports, 'DialogContent', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogContent).default;
  }
});

var _DialogContentText = __webpack_require__(/*! ./DialogContentText */ "./node_modules/material-ui/Dialog/DialogContentText.js");

Object.defineProperty(exports, 'DialogContentText', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogContentText).default;
  }
});

var _withMobileDialog = __webpack_require__(/*! ./withMobileDialog */ "./node_modules/material-ui/Dialog/withMobileDialog.js");

Object.defineProperty(exports, 'withMobileDialog', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_withMobileDialog).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Dialog/withMobileDialog.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui/Dialog/withMobileDialog.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _wrapDisplayName = __webpack_require__(/*! recompose/wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _withWidth = __webpack_require__(/*! ../utils/withWidth */ "./node_modules/material-ui/utils/withWidth.js");

var _withWidth2 = _interopRequireDefault(_withWidth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_HigherOrderComponent = __webpack_require__(/*! react-flow-types */ "./node_modules/react-flow-types/index.js").babelPluginFlowReactPropTypes_proptype_HigherOrderComponent || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Breakpoint = __webpack_require__(/*! ../styles/createBreakpoints */ "./node_modules/material-ui/styles/createBreakpoints.js").babelPluginFlowReactPropTypes_proptype_Breakpoint || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_InjectedProps = {
  /**
   * If isWidthDown(options.breakpoint), return true.
   */
  fullScreen: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};


/**
 * Dialog will responsively be full screen *at or below* the given breakpoint
 * (defaults to 'sm' for mobile devices).
 * Notice that this Higher-order Component is incompatible with server side rendering.
 */
var withMobileDialog = function withMobileDialog() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { breakpoint: 'sm' };
  return function (Component) {
    var breakpoint = options.breakpoint;


    function WithMobileDialog(props) {
      return _react2.default.createElement(Component, (0, _extends3.default)({ fullScreen: (0, _withWidth.isWidthDown)(breakpoint, props.width) }, props));
    }

    WithMobileDialog.propTypes =  true ? {
      width: typeof babelPluginFlowReactPropTypes_proptype_Breakpoint === 'function' ? babelPluginFlowReactPropTypes_proptype_Breakpoint.isRequired ? babelPluginFlowReactPropTypes_proptype_Breakpoint.isRequired : babelPluginFlowReactPropTypes_proptype_Breakpoint : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Breakpoint).isRequired
    } : {};
    if (true) {
      WithMobileDialog.displayName = (0, _wrapDisplayName2.default)(Component, 'withMobileDialog');
    }

    return (0, _withWidth2.default)()(WithMobileDialog);
  };
};

exports.default = withMobileDialog;

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/Backdrop.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui/Modal/Backdrop.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      zIndex: -1,
      width: '100%',
      height: '100%',
      position: 'fixed',
      top: 0,
      left: 0,
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      backgroundColor: theme.palette.common.lightBlack,
      transition: theme.transitions.create('opacity'),
      willChange: 'opacity',
      opacity: 0
    },
    invisible: {
      backgroundColor: theme.palette.common.transparent
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Can be used, for instance, to render a letter inside the avatar.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the backdrop is invisible.
   */
  invisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * @ignore - internal component.
 */
var Backdrop = function (_React$Component) {
  (0, _inherits3.default)(Backdrop, _React$Component);

  function Backdrop() {
    (0, _classCallCheck3.default)(this, Backdrop);
    return (0, _possibleConstructorReturn3.default)(this, (Backdrop.__proto__ || (0, _getPrototypeOf2.default)(Backdrop)).apply(this, arguments));
  }

  (0, _createClass3.default)(Backdrop, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          invisible = _props.invisible,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'invisible']);


      var backdropClass = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.invisible, invisible), className);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: backdropClass, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Backdrop;
}(_react2.default.Component);

Backdrop.defaultProps = {
  invisible: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBackdrop' })(Backdrop);

/***/ }),

/***/ "./node_modules/material-ui/Modal/Modal.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/Modal.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _addEventListener = __webpack_require__(/*! ../utils/addEventListener */ "./node_modules/material-ui/utils/addEventListener.js");

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _modalManager = __webpack_require__(/*! ./modalManager */ "./node_modules/material-ui/Modal/modalManager.js");

var _modalManager2 = _interopRequireDefault(_modalManager);

var _Backdrop = __webpack_require__(/*! ./Backdrop */ "./node_modules/material-ui/Modal/Backdrop.js");

var _Backdrop2 = _interopRequireDefault(_Backdrop);

var _Portal = __webpack_require__(/*! ../internal/Portal */ "./node_modules/material-ui/internal/Portal.js");

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// Modals don't open on the server so this won't break concurrency.
// Could also put this on context.
var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var modalManager = (0, _modalManager2.default)();

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      position: 'fixed',
      zIndex: theme.zIndex.dialog,
      top: 0,
      left: 0
    },
    hidden: {
      visibility: 'hidden'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the backdrop element.
   */
  BackdropClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Pass a component class to use as the backdrop.
   */
  BackdropComponent: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, the backdrop is invisible.
   */
  BackdropInvisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the backdrop transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  BackdropTransitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Always keep the children in the DOM.
   * This property can be useful in SEO situation or
   * when you want to maximize the responsiveness of the Modal.
   */
  keepMounted: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the backdrop is disabled.
   */
  disableBackdrop: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  modalManager: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * Callback fires when the backdrop is clicked on.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is pressed and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Modal is visible.
   */
  show: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * The modal component provides a solid foundation for creating dialogs,
 * popovers, or whatever else.
 * The component renders its `children` node in front of a backdrop component.
 *
 * The `Modal` offers a few helpful features over using just a `Portal` component and some styles:
 * - Manages dialog stacking when one-at-a-time just isn't enough.
 * - Creates a backdrop, for disabling interaction below the modal.
 * - It properly manages focus; moving to the modal content,
 *   and keeping it there until the modal is closed.
 * - It disables scrolling of the page content while open.
 * - Adds the appropriate ARIA roles are automatically.
 *
 * This component shares many concepts with [react-overlays](https://react-bootstrap.github.io/react-overlays/#modals).
 */
var Modal = function (_React$Component) {
  (0, _inherits3.default)(Modal, _React$Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call.apply(_ref, [this].concat(args))), _this), _initialiseProps.call(_this), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Modal, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (!this.props.show) {
        this.setState({ exited: true });
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.mounted = true;
      if (this.props.show) {
        this.handleShow();
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.show && this.state.exited) {
        this.setState({ exited: false });
      }
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps) {
      if (!this.props.show && nextProps.show) {
        this.checkForFocus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.show && this.props.show) {
        this.handleShow();
      }
      // We are waiting for the onExited callback to call handleHide.
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.props.show || !this.state.exited) {
        this.handleHide();
      }
      this.mounted = false;
    }
  }, {
    key: 'checkForFocus',
    value: function checkForFocus() {
      if (_inDOM2.default) {
        this.lastFocus = (0, _activeElement2.default)();
      }
    }
  }, {
    key: 'restoreLastFocus',
    value: function restoreLastFocus() {
      if (this.lastFocus && this.lastFocus.focus) {
        this.lastFocus.focus();
        this.lastFocus = undefined;
      }
    }
  }, {
    key: 'handleShow',
    value: function handleShow() {
      var doc = (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this));
      this.props.modalManager.add(this);
      this.onDocumentKeyUpListener = (0, _addEventListener2.default)(doc, 'keyup', this.handleDocumentKeyUp);
      this.onFocusListener = (0, _addEventListener2.default)(doc, 'focus', this.handleFocusListener, true);
      this.focus();
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this)));
      var modalContent = this.modal && this.modal.lastChild;
      var focusInModal = currentFocus && (0, _contains2.default)(modalContent, currentFocus);

      if (modalContent && !focusInModal) {
        if (!modalContent.hasAttribute('tabIndex')) {
          modalContent.setAttribute('tabIndex', -1);
           true ? (0, _warning2.default)(false, 'Material-UI: the modal content node does not accept focus. ' + 'For the benefit of assistive technologies, ' + 'the tabIndex of the node is being set to "-1".') : void 0;
        }

        modalContent.focus();
      }
    }
  }, {
    key: 'handleHide',
    value: function handleHide() {
      this.props.modalManager.remove(this);
      if (this.onDocumentKeyUpListener) this.onDocumentKeyUpListener.remove();
      if (this.onFocusListener) this.onFocusListener.remove();
      this.restoreLastFocus();
    }
  }, {
    key: 'renderBackdrop',
    value: function renderBackdrop() {
      var other = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var _props = this.props,
          BackdropComponent = _props.BackdropComponent,
          BackdropClassName = _props.BackdropClassName,
          BackdropTransitionDuration = _props.BackdropTransitionDuration,
          BackdropInvisible = _props.BackdropInvisible,
          show = _props.show;


      return _react2.default.createElement(
        _Fade2.default,
        (0, _extends3.default)({ appear: true, 'in': show, timeout: BackdropTransitionDuration }, other),
        _react2.default.createElement(BackdropComponent, {
          invisible: BackdropInvisible,
          className: BackdropClassName,
          onClick: this.handleBackdropClick
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          disableBackdrop = _props2.disableBackdrop,
          BackdropComponent = _props2.BackdropComponent,
          BackdropClassName = _props2.BackdropClassName,
          BackdropTransitionDuration = _props2.BackdropTransitionDuration,
          BackdropInvisible = _props2.BackdropInvisible,
          ignoreBackdropClick = _props2.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props2.ignoreEscapeKeyUp,
          children = _props2.children,
          classes = _props2.classes,
          className = _props2.className,
          keepMounted = _props2.keepMounted,
          modalManagerProp = _props2.modalManager,
          onBackdropClick = _props2.onBackdropClick,
          onEscapeKeyUp = _props2.onEscapeKeyUp,
          onRequestClose = _props2.onRequestClose,
          onEnter = _props2.onEnter,
          onEntering = _props2.onEntering,
          onEntered = _props2.onEntered,
          onExit = _props2.onExit,
          onExiting = _props2.onExiting,
          onExited = _props2.onExited,
          show = _props2.show,
          other = (0, _objectWithoutProperties3.default)(_props2, ['disableBackdrop', 'BackdropComponent', 'BackdropClassName', 'BackdropTransitionDuration', 'BackdropInvisible', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'children', 'classes', 'className', 'keepMounted', 'modalManager', 'onBackdropClick', 'onEscapeKeyUp', 'onRequestClose', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'show']);


      if (!keepMounted && !show && this.state.exited) {
        return null;
      }

      var transitionCallbacks = {
        onEnter: onEnter,
        onEntering: onEntering,
        onEntered: onEntered,
        onExit: onExit,
        onExiting: onExiting,
        onExited: this.handleTransitionExited
      };

      var modalChild = _react2.default.Children.only(children);
      var _modalChild$props = modalChild.props,
          role = _modalChild$props.role,
          tabIndex = _modalChild$props.tabIndex;

      var childProps = {};

      if (role === undefined) {
        childProps.role = role === undefined ? 'document' : role;
      }

      if (tabIndex === undefined) {
        childProps.tabIndex = tabIndex == null ? -1 : tabIndex;
      }

      var backdropProps = void 0;

      // It's a Transition like component
      if (modalChild.props.hasOwnProperty('in')) {
        (0, _keys2.default)(transitionCallbacks).forEach(function (key) {
          childProps[key] = (0, _helpers.createChainedFunction)(transitionCallbacks[key], modalChild.props[key]);
        });
      } else {
        backdropProps = transitionCallbacks;
      }

      if ((0, _keys2.default)(childProps).length) {
        modalChild = _react2.default.cloneElement(modalChild, childProps);
      }

      return _react2.default.createElement(
        _Portal2.default,
        {
          open: true,
          ref: function ref(node) {
            _this2.mountNode = node ? node.getLayer() : null;
          }
        },
        _react2.default.createElement(
          'div',
          (0, _extends3.default)({
            className: (0, _classnames2.default)(classes.root, className, (0, _defineProperty3.default)({}, classes.hidden, this.state.exited))
          }, other, {
            ref: function ref(node) {
              _this2.modal = node;
            }
          }),
          !disableBackdrop && (!keepMounted || show || !this.state.exited) && this.renderBackdrop(backdropProps),
          modalChild
        )
      );
    }
  }]);
  return Modal;
}(_react2.default.Component);

Modal.defaultProps = {
  BackdropComponent: _Backdrop2.default,
  BackdropTransitionDuration: 300,
  BackdropInvisible: false,
  keepMounted: false,
  disableBackdrop: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  modalManager: modalManager
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.state = {
    exited: false
  };
  this.onDocumentKeyUpListener = null;
  this.onFocusListener = null;
  this.mounted = false;
  this.lastFocus = undefined;
  this.modal = null;
  this.mountNode = null;

  this.handleFocusListener = function () {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(_this3)));
    var modalContent = _this3.modal && _this3.modal.lastChild;

    if (modalContent && modalContent !== currentFocus && !(0, _contains2.default)(modalContent, currentFocus)) {
      modalContent.focus();
    }
  };

  this.handleDocumentKeyUp = function (event) {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    if ((0, _keycode2.default)(event) !== 'esc') {
      return;
    }

    var _props3 = _this3.props,
        onEscapeKeyUp = _props3.onEscapeKeyUp,
        onRequestClose = _props3.onRequestClose,
        ignoreEscapeKeyUp = _props3.ignoreEscapeKeyUp;


    if (onEscapeKeyUp) {
      onEscapeKeyUp(event);
    }

    if (onRequestClose && !ignoreEscapeKeyUp) {
      onRequestClose(event);
    }
  };

  this.handleBackdropClick = function (event) {
    if (event.target !== event.currentTarget) {
      return;
    }

    var _props4 = _this3.props,
        onBackdropClick = _props4.onBackdropClick,
        onRequestClose = _props4.onRequestClose,
        ignoreBackdropClick = _props4.ignoreBackdropClick;


    if (onBackdropClick) {
      onBackdropClick(event);
    }

    if (onRequestClose && !ignoreBackdropClick) {
      onRequestClose(event);
    }
  };

  this.handleTransitionExited = function () {
    if (_this3.props.onExited) {
      var _props5;

      (_props5 = _this3.props).onExited.apply(_props5, arguments);
    }

    _this3.setState({ exited: true });
    _this3.handleHide();
  };
};

exports.default = (0, _withStyles2.default)(styles, { flip: false, name: 'MuiModal' })(Modal);

/***/ }),

/***/ "./node_modules/material-ui/Modal/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Modal = __webpack_require__(/*! ./Modal */ "./node_modules/material-ui/Modal/Modal.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Modal).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/modalManager.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Modal/modalManager.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _isWindow = __webpack_require__(/*! dom-helpers/query/isWindow */ "./node_modules/dom-helpers/query/isWindow.js");

var _isWindow2 = _interopRequireDefault(_isWindow);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _manageAriaHidden = __webpack_require__(/*! ../utils/manageAriaHidden */ "./node_modules/material-ui/utils/manageAriaHidden.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Taken from https://github.com/react-bootstrap/react-overlays/blob/master/src/ModalManager.js

function getPaddingRight(node) {
  return parseInt(node.style.paddingRight || 0, 10);
}

// Do we have a scroll bar?
function bodyIsOverflowing(node) {
  var doc = (0, _ownerDocument2.default)(node);
  var win = (0, _isWindow2.default)(doc);

  // Takes in account potential non zero margin on the body.
  var style = window.getComputedStyle(doc.body);
  var marginLeft = parseInt(style.getPropertyValue('margin-left'), 10);
  var marginRight = parseInt(style.getPropertyValue('margin-right'), 10);

  return marginLeft + doc.body.clientWidth + marginRight < win.innerWidth;
}

function getContainer() {
  var container = _inDOM2.default ? window.document.body : {};
   true ? (0, _warning2.default)(container !== null, '\nMaterial-UI: you are most likely evaluating the code before the\nbrowser has a chance to reach the <body>.\nPlease move the import at the end of the <body>.\n  ') : void 0;
  return container;
}
/**
 * State management helper for modals/layers.
 * Simplified, but inspired by react-overlay's ModalManager class
 *
 * @internal Used by the Modal to ensure proper focus management.
 */
function createModalManager() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$hideSiblingNodes = _ref.hideSiblingNodes,
      hideSiblingNodes = _ref$hideSiblingNodes === undefined ? true : _ref$hideSiblingNodes;

  var modals = [];

  var prevOverflow = void 0;
  var prevPaddings = [];

  function add(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx !== -1) {
      return modalIdx;
    }

    modalIdx = modals.length;
    modals.push(modal);

    if (hideSiblingNodes) {
      (0, _manageAriaHidden.hideSiblings)(container, modal.mountNode);
    }

    if (modals.length === 1) {
      // Save our current overflow so we can revert
      // back to it when all modals are closed!
      prevOverflow = container.style.overflow;

      if (bodyIsOverflowing(container)) {
        prevPaddings = [getPaddingRight(container)];
        var scrollbarSize = (0, _scrollbarSize2.default)();
        container.style.paddingRight = prevPaddings[0] + scrollbarSize + 'px';

        var fixedNodes = document.querySelectorAll('.mui-fixed');
        for (var i = 0; i < fixedNodes.length; i += 1) {
          var paddingRight = getPaddingRight(fixedNodes[i]);
          prevPaddings.push(paddingRight);
          fixedNodes[i].style.paddingRight = paddingRight + scrollbarSize + 'px';
        }
      }

      container.style.overflow = 'hidden';
    }

    return modalIdx;
  }

  function remove(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx === -1) {
      return modalIdx;
    }

    modals.splice(modalIdx, 1);

    if (modals.length === 0) {
      container.style.overflow = prevOverflow;
      container.style.paddingRight = prevPaddings[0];

      var fixedNodes = document.querySelectorAll('.mui-fixed');
      for (var i = 0; i < fixedNodes.length; i += 1) {
        fixedNodes[i].style.paddingRight = prevPaddings[i + 1] + 'px';
      }

      prevOverflow = undefined;
      prevPaddings = [];
      if (hideSiblingNodes) {
        (0, _manageAriaHidden.showSiblings)(container, modal.mountNode);
      }
    } else if (hideSiblingNodes) {
      // otherwise make sure the next top modal is visible to a SR
      (0, _manageAriaHidden.ariaHidden)(false, modals[modals.length - 1].mountNode);
    }

    return modalIdx;
  }

  function isTopModal(modal) {
    return !!modals.length && modals[modals.length - 1] === modal;
  }

  var modalManager = { add: add, remove: remove, isTopModal: isTopModal };

  return modalManager;
}

exports.default = createModalManager;

/***/ }),

/***/ "./node_modules/material-ui/internal/Portal.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/internal/Portal.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content to portal in order to escape the parent DOM node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * If `true` the children will be mounted into the DOM.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

/**
 * @ignore - internal component.
 */
var Portal = function (_React$Component) {
  (0, _inherits3.default)(Portal, _React$Component);

  function Portal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Portal.__proto__ || (0, _getPrototypeOf2.default)(Portal)).call.apply(_ref, [this].concat(args))), _this), _this.layer = null, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Portal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unrenderLayer();
    }
  }, {
    key: 'getLayer',
    value: function getLayer() {
      if (!this.layer) {
        this.layer = document.createElement('div');
        this.layer.setAttribute('data-mui-portal', 'true');
        if (document.body && this.layer) {
          document.body.appendChild(this.layer);
        }
      }

      return this.layer;
    }
  }, {
    key: 'unrenderLayer',
    value: function unrenderLayer() {
      if (!this.layer) {
        return;
      }

      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        _reactDom2.default.unmountComponentAtNode(this.layer);
      }

      if (document.body) {
        document.body.removeChild(this.layer);
      }
      this.layer = null;
    }
  }, {
    key: 'renderLayer',
    value: function renderLayer() {
      var _props = this.props,
          children = _props.children,
          open = _props.open;


      if (open) {
        // By calling this method in componentDidMount() and
        // componentDidUpdate(), you're effectively creating a "wormhole" that
        // funnels React's hierarchical updates through to a DOM node on an
        // entirely different part of the page.
        var layerElement = _react2.default.Children.only(children);
        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, layerElement, this.getLayer());
      } else {
        this.unrenderLayer();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          children = _props2.children,
          open = _props2.open;

      // Support react@15.x, will be removed at some point

      if (!_reactDom2.default.createPortal) {
        return null;
      }

      // Can't be rendered server-side.
      if (_inDOM2.default) {
        if (open) {
          var layer = this.getLayer();
          // $FlowFixMe layer is non-null
          return _reactDom2.default.createPortal(children, layer);
        }

        this.unrenderLayer();
      }

      return null;
    }
  }]);
  return Portal;
}(_react2.default.Component);

Portal.defaultProps = {
  open: false
};
Portal.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
} : {};
exports.default = Portal;

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Fade.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Fade.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  timeout: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired
};


var reflow = function reflow(node) {
  return node.scrollTop;
};

/**
 * The Fade transition is used by the Modal component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */

var Fade = function (_React$Component) {
  (0, _inherits3.default)(Fade, _React$Component);

  function Fade() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Fade);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Fade.__proto__ || (0, _getPrototypeOf2.default)(Fade)).call.apply(_ref, [this].concat(args))), _this), _this.handleEnter = function (node) {
      node.style.opacity = '0';
      reflow(node);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      node.style.opacity = '1';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      node.style.opacity = '0';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Fade, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          styleProp = _props.style,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'style', 'theme']);


      var style = (0, _extends3.default)({}, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          style: style,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit
        }, other),
        children
      );
    }
  }]);
  return Fade;
}(_react2.default.Component);

Fade.defaultProps = {
  appear: true,
  timeout: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  }
};
exports.default = (0, _withTheme2.default)()(Fade);

/***/ }),

/***/ "./node_modules/material-ui/utils/manageAriaHidden.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui/utils/manageAriaHidden.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ariaHidden = ariaHidden;
exports.hideSiblings = hideSiblings;
exports.showSiblings = showSiblings;
//  weak

var BLACKLIST = ['template', 'script', 'style'];

var isHidable = function isHidable(_ref) {
  var nodeType = _ref.nodeType,
      tagName = _ref.tagName;
  return nodeType === 1 && BLACKLIST.indexOf(tagName.toLowerCase()) === -1;
};

var siblings = function siblings(container, mount, cb) {
  mount = [].concat(mount); // eslint-disable-line no-param-reassign
  [].forEach.call(container.children, function (node) {
    if (mount.indexOf(node) === -1 && isHidable(node)) {
      cb(node);
    }
  });
};

function ariaHidden(show, node) {
  if (!node) {
    return;
  }
  if (show) {
    node.setAttribute('aria-hidden', 'true');
  } else {
    node.removeAttribute('aria-hidden');
  }
}

function hideSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(true, node);
  });
}

function showSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(false, node);
  });
}

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/react-router/Route.js":
/*!********************************************!*\
  !*** ./node_modules/react-router/Route.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _matchPath = __webpack_require__(/*! ./matchPath */ "./node_modules/react-router/matchPath.js");

var _matchPath2 = _interopRequireDefault(_matchPath);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isEmptyChildren = function isEmptyChildren(children) {
  return _react2.default.Children.count(children) === 0;
};

/**
 * The public API for matching a single path and rendering.
 */

var Route = function (_React$Component) {
  _inherits(Route, _React$Component);

  function Route() {
    var _temp, _this, _ret;

    _classCallCheck(this, Route);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
      match: _this.computeMatch(_this.props, _this.context.router)
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Route.prototype.getChildContext = function getChildContext() {
    return {
      router: _extends({}, this.context.router, {
        route: {
          location: this.props.location || this.context.router.route.location,
          match: this.state.match
        }
      })
    };
  };

  Route.prototype.computeMatch = function computeMatch(_ref, router) {
    var computedMatch = _ref.computedMatch,
        location = _ref.location,
        path = _ref.path,
        strict = _ref.strict,
        exact = _ref.exact,
        sensitive = _ref.sensitive;

    if (computedMatch) return computedMatch; // <Switch> already computed the match for us

    (0, _invariant2.default)(router, 'You should not use <Route> or withRouter() outside a <Router>');

    var route = router.route;

    var pathname = (location || route.location).pathname;

    return path ? (0, _matchPath2.default)(pathname, { path: path, strict: strict, exact: exact, sensitive: sensitive }) : route.match;
  };

  Route.prototype.componentWillMount = function componentWillMount() {
    (0, _warning2.default)(!(this.props.component && this.props.render), 'You should not use <Route component> and <Route render> in the same route; <Route render> will be ignored');

    (0, _warning2.default)(!(this.props.component && this.props.children && !isEmptyChildren(this.props.children)), 'You should not use <Route component> and <Route children> in the same route; <Route children> will be ignored');

    (0, _warning2.default)(!(this.props.render && this.props.children && !isEmptyChildren(this.props.children)), 'You should not use <Route render> and <Route children> in the same route; <Route children> will be ignored');
  };

  Route.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps, nextContext) {
    (0, _warning2.default)(!(nextProps.location && !this.props.location), '<Route> elements should not change from uncontrolled to controlled (or vice versa). You initially used no "location" prop and then provided one on a subsequent render.');

    (0, _warning2.default)(!(!nextProps.location && this.props.location), '<Route> elements should not change from controlled to uncontrolled (or vice versa). You provided a "location" prop initially but omitted it on a subsequent render.');

    this.setState({
      match: this.computeMatch(nextProps, nextContext.router)
    });
  };

  Route.prototype.render = function render() {
    var match = this.state.match;
    var _props = this.props,
        children = _props.children,
        component = _props.component,
        render = _props.render;
    var _context$router = this.context.router,
        history = _context$router.history,
        route = _context$router.route,
        staticContext = _context$router.staticContext;

    var location = this.props.location || route.location;
    var props = { match: match, location: location, history: history, staticContext: staticContext };

    return component ? // component prop gets first priority, only called if there's a match
    match ? _react2.default.createElement(component, props) : null : render ? // render prop is next, only called if there's a match
    match ? render(props) : null : children ? // children come last, always called
    typeof children === 'function' ? children(props) : !isEmptyChildren(children) ? _react2.default.Children.only(children) : null : null;
  };

  return Route;
}(_react2.default.Component);

Route.propTypes = {
  computedMatch: _propTypes2.default.object, // private, from <Switch>
  path: _propTypes2.default.string,
  exact: _propTypes2.default.bool,
  strict: _propTypes2.default.bool,
  sensitive: _propTypes2.default.bool,
  component: _propTypes2.default.func,
  render: _propTypes2.default.func,
  children: _propTypes2.default.oneOfType([_propTypes2.default.func, _propTypes2.default.node]),
  location: _propTypes2.default.object
};
Route.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.object.isRequired,
    route: _propTypes2.default.object.isRequired,
    staticContext: _propTypes2.default.object
  })
};
Route.childContextTypes = {
  router: _propTypes2.default.object.isRequired
};
exports.default = Route;

/***/ }),

/***/ "./node_modules/react-router/matchPath.js":
/*!************************************************!*\
  !*** ./node_modules/react-router/matchPath.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _pathToRegexp = __webpack_require__(/*! path-to-regexp */ "./node_modules/path-to-regexp/index.js");

var _pathToRegexp2 = _interopRequireDefault(_pathToRegexp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var patternCache = {};
var cacheLimit = 10000;
var cacheCount = 0;

var compilePath = function compilePath(pattern, options) {
  var cacheKey = '' + options.end + options.strict + options.sensitive;
  var cache = patternCache[cacheKey] || (patternCache[cacheKey] = {});

  if (cache[pattern]) return cache[pattern];

  var keys = [];
  var re = (0, _pathToRegexp2.default)(pattern, keys, options);
  var compiledPattern = { re: re, keys: keys };

  if (cacheCount < cacheLimit) {
    cache[pattern] = compiledPattern;
    cacheCount++;
  }

  return compiledPattern;
};

/**
 * Public API for matching a URL pathname to a path pattern.
 */
var matchPath = function matchPath(pathname) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (typeof options === 'string') options = { path: options };

  var _options = options,
      _options$path = _options.path,
      path = _options$path === undefined ? '/' : _options$path,
      _options$exact = _options.exact,
      exact = _options$exact === undefined ? false : _options$exact,
      _options$strict = _options.strict,
      strict = _options$strict === undefined ? false : _options$strict,
      _options$sensitive = _options.sensitive,
      sensitive = _options$sensitive === undefined ? false : _options$sensitive;

  var _compilePath = compilePath(path, { end: exact, strict: strict, sensitive: sensitive }),
      re = _compilePath.re,
      keys = _compilePath.keys;

  var match = re.exec(pathname);

  if (!match) return null;

  var url = match[0],
      values = match.slice(1);

  var isExact = pathname === url;

  if (exact && !isExact) return null;

  return {
    path: path, // the path pattern used to match
    url: path === '/' && url === '' ? '/' : url, // the matched portion of the URL
    isExact: isExact, // whether or not we matched exactly
    params: keys.reduce(function (memo, key, index) {
      memo[key.name] = values[index];
      return memo;
    }, {})
  };
};

exports.default = matchPath;

/***/ }),

/***/ "./node_modules/react-router/withRouter.js":
/*!*************************************************!*\
  !*** ./node_modules/react-router/withRouter.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _hoistNonReactStatics = __webpack_require__(/*! hoist-non-react-statics */ "./node_modules/hoist-non-react-statics/index.js");

var _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics);

var _Route = __webpack_require__(/*! ./Route */ "./node_modules/react-router/Route.js");

var _Route2 = _interopRequireDefault(_Route);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

/**
 * A public higher-order component to access the imperative API
 */
var withRouter = function withRouter(Component) {
  var C = function C(props) {
    var wrappedComponentRef = props.wrappedComponentRef,
        remainingProps = _objectWithoutProperties(props, ['wrappedComponentRef']);

    return _react2.default.createElement(_Route2.default, { render: function render(routeComponentProps) {
        return _react2.default.createElement(Component, _extends({}, remainingProps, routeComponentProps, { ref: wrappedComponentRef }));
      } });
  };

  C.displayName = 'withRouter(' + (Component.displayName || Component.name) + ')';
  C.WrappedComponent = Component;
  C.propTypes = {
    wrappedComponentRef: _propTypes2.default.func
  };

  return (0, _hoistNonReactStatics2.default)(C, Component);
};

exports.default = withRouter;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/courses/create.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/create.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 * create action will add newly created course to store.
 * @param {Object} payload -
 * @param {number} payload.statusCode -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @param {string} payload.title -
 * @param {string} payload.timestamp -
 *
 * @return {Object} -
 */
var create = function create(payload) {
  return { type: _courses.COURSE_CREATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/actions/courses/delete.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/delete.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @function deleteCourse
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @return {Object} -
 */
exports.default = function (payload) {
  return { type: _courses.COURSE_DELETE, payload: payload };
}; /**
    * @format
    * 
    */

/***/ }),

/***/ "./src/client/actions/elements/getById.js":
/*!************************************************!*\
  !*** ./src/client/actions/elements/getById.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

var _getById = __webpack_require__(/*! ../../api/elements/getById */ "./src/client/api/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @return {Object}
 */
/** @format */

var getElementStart = function getElementStart(payload) {
  return { type: _elements.ELEMENT_GET_START, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType
 * @param {string} payload.circleType - only if elementType is 'circle'
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @return {Object}
 */
var getElementSuccess = function getElementSuccess(payload) {
  return { type: _elements.ELEMENT_GET_SUCCESS, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
var getElementError = function getElementError(payload) {
  return { type: _elements.ELEMENT_GET_ERROR, payload: payload };
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
var get = function get(_ref) {
  var token = _ref.token,
      id = _ref.id;
  return function (dispatch) {
    dispatch(getElementStart({ id: id }));

    (0, _getById2.default)({ token: token, id: id }).then(function (_ref2) {
      var statusCode = _ref2.statusCode,
          error = _ref2.error,
          payload = _ref2.payload;

      if (statusCode >= 300) {
        dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));

        return;
      }

      dispatch(getElementSuccess((0, _extends3.default)({ id: id, statusCode: statusCode }, payload)));
    }).catch(function (_ref3) {
      var statusCode = _ref3.statusCode,
          error = _ref3.error;

      dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));
    });
  };
};

exports.default = get;

/***/ }),

/***/ "./src/client/actions/posts/delete.js":
/*!********************************************!*\
  !*** ./src/client/actions/posts/delete.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../constants/posts */ "./src/client/constants/posts.js");

/**
 *
 * @function deletePost
 * @param {Object} payload
 * @param {number} payload.postId
 * @returns {Object}
 */
var deletePost = function deletePost(payload) {
  return {
    type: _posts.POST_DELETE,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = deletePost;

/***/ }),

/***/ "./src/client/api/courses/users/create.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/create.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * create course for user.
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.title -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * 
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/api/courses/users/delete.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/delete.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * delete course
 * @async
 * @function deleteCourse
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.courseId
 * @returns {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

var deleteCourse = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deleteCourse(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deleteCourse;

/***/ }),

/***/ "./src/client/api/elements/getById.js":
/*!********************************************!*\
  !*** ./src/client/api/elements/getById.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @function getById
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
/** @format */

var getById = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        id = _ref2.id;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getById;

/***/ }),

/***/ "./src/client/api/posts/users/delete.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/delete.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * delete post of user
 * @async
 * @function deletePost
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file delete post of user.
 */

var deletePost = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token,
        postId = _ref2.postId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts/' + postId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deletePost(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deletePost;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/Course/index.js":
/*!***********************************************!*\
  !*** ./src/client/containers/Course/index.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withRouter = __webpack_require__(/*! react-router/withRouter */ "./node_modules/react-router/withRouter.js");

var _withRouter2 = _interopRequireDefault(_withRouter);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Dialog = __webpack_require__(/*! material-ui/Dialog */ "./node_modules/material-ui/Dialog/index.js");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _TurnedIn = __webpack_require__(/*! material-ui-icons/TurnedIn */ "./node_modules/material-ui-icons/TurnedIn.js");

var _TurnedIn2 = _interopRequireDefault(_TurnedIn);

var _Delete = __webpack_require__(/*! material-ui-icons/Delete */ "./node_modules/material-ui-icons/Delete.js");

var _Delete2 = _interopRequireDefault(_Delete);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _delete = __webpack_require__(/*! ../../api/courses/users/delete */ "./src/client/api/courses/users/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _delete3 = __webpack_require__(/*! ../../actions/courses/delete */ "./src/client/actions/courses/delete.js");

var _delete4 = _interopRequireDefault(_delete3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    padding: '1%'
  },
  gridContainer: {},
  name: {
    cursor: 'pointer',
    textDecoration: 'none'
  },
  username: {
    cursor: 'pointer',
    textDecoration: 'none'
  },
  post: {
    padding: '1%'
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  cardMedia: {},
  media: {
    height: 194
  },
  cardMediaImage: {
    width: '100%'
  },
  margin: {
    border: '0.5px solid rgba(209,209,209,1)',
    marginBottom: '-10px'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
}; /**
    * course page component
    * This component will display onlu one course information
    *
    * @format
    */

var Course = function (_Component) {
  (0, _inherits3.default)(Course, _Component);

  function Course(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, Course);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Course.__proto__ || (0, _getPrototypeOf2.default)(Course)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onClickContent = function () {
      var courseId = _this.props.courseId;


      _this.props.history.push('/courses/' + courseId);
    };

    _this.deleteDialogOpen = function () {
      _this.setState({ deleteDialog: true });
    };

    _this.deleteDialogClose = function () {
      _this.setState({ deleteDialog: false });
    };

    _this.deleteCourseClick = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var _this$props$elements$, token, id, courseId, _ref2, statusCode, error;

      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this$props$elements$ = _this.props.elements.user, token = _this$props$elements$.token, id = _this$props$elements$.id;
              courseId = _this.props.courseId;
              _context.next = 4;
              return (0, _delete2.default)({
                token: token,
                userId: id,
                courseId: courseId
              });

            case 4:
              _ref2 = _context.sent;
              statusCode = _ref2.statusCode;
              error = _ref2.error;

              if (!(statusCode !== 200)) {
                _context.next = 10;
                break;
              }

              // show some error message.
              console.log(error);
              return _context.abrupt('return');

            case 10:

              _this.props.actionCourseDelete({ courseId: courseId });
              _this.setState({ deleteDialog: false });

            case 12:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this2);
    }));

    _this.state = {
      hover: false,
      deleteDialog: false
    };
    return _this;
  }

  (0, _createClass3.default)(Course, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _props = this.props,
          elements = _props.elements,
          courses = _props.courses,
          courseId = _props.courseId;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = courses[courseId].authorId;

      var author = elements[authorId];

      // if author is not found
      if (userId !== authorId && !author) {
        this.props.actionGetElementById({ id: authorId, token: token });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          courses = _props2.courses,
          elements = _props2.elements,
          courseId = _props2.courseId;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id;


      var course = courses[courseId];

      if (typeof course === 'undefined') {
        return _react2.default.createElement('div', null);
      }

      var title = course.title,
          description = course.description,
          authorId = course.authorId,
          cover = course.cover;

      var author = authorId === userId ? user : elements[authorId];

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(_Avatar2.default, {
                alt: author.name,
                src: author.avatar || '/public/photos/mayash-logo-transparent.png'
              })
            ),
            action: _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _Tooltip2.default,
                { title: 'Coming soon', placement: 'bottom' },
                _react2.default.createElement(
                  _IconButton2.default,
                  { disabled: true },
                  _react2.default.createElement(_MoreVert2.default, null)
                )
              )
            ),
            title: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.name },
              author.name
            ),
            subheader: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.username },
              '@' + author.username
            )
          }),
          _react2.default.createElement(_Card.CardMedia, {
            className: classes.media,
            image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
          }),
          _react2.default.createElement(
            _Card.CardContent,
            { onClick: this.onClickContent },
            _react2.default.createElement(
              _Typography2.default,
              { type: 'title', gutterBottom: true },
              title
            ),
            typeof description === 'string' ? _react2.default.createElement(
              _Typography2.default,
              { type: 'body1', gutterBottom: true },
              description
            ) : null
          ),
          _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Add to favorites', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Add to favorites', disabled: true },
                _react2.default.createElement(_Favorite2.default, null)
              )
            ),
            _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Bookmark', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Book Marks', disabled: true },
                _react2.default.createElement(_TurnedIn2.default, null)
              )
            ),
            isSignedIn && userId === author.id ? _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Delete', onClick: this.deleteDialogOpen },
                _react2.default.createElement(_Delete2.default, null)
              ),
              _react2.default.createElement(
                _Dialog2.default,
                {
                  open: this.state.deleteDialog,
                  onClose: this.deleteDialogClose,
                  'aria-labelledby': 'course-' + courseId + '-delete-dialog'
                },
                _react2.default.createElement(
                  _Dialog.DialogTitle,
                  { id: 'course-' + courseId + '-delete-dialog' },
                  'Are you sure?'
                ),
                _react2.default.createElement(
                  _Dialog.DialogActions,
                  null,
                  _react2.default.createElement(
                    _Button2.default,
                    { onClick: this.deleteDialogClose, color: 'primary' },
                    'Cancel'
                  ),
                  _react2.default.createElement(
                    _Button2.default,
                    {
                      onClick: this.deleteCourseClick,
                      color: 'primary',
                      autoFocus: true
                    },
                    'Delete'
                  )
                )
              )
            ) : null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share', disabled: true },
              _react2.default.createElement(_Share2.default, null)
            )
          )
        )
      );
    }
  }]);
  return Course;
}(_react.Component);

Course.propTypes = {
  history: _propTypes2.default.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,
  courses: _propTypes2.default.object.isRequired,
  courseId: _propTypes2.default.number.isRequired,

  actionGetElementById: _propTypes2.default.func.isRequired,
  actionCourseDelete: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements,
      courses = _ref3.courses;
  return { elements: elements, courses: courses };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionGetElementById: _getById2.default,
    actionCourseDelete: _delete4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)((0, _withRouter2.default)(Course)));

/***/ }),

/***/ "./src/client/containers/CourseCreate/index.js":
/*!*****************************************************!*\
  !*** ./src/client/containers/CourseCreate/index.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../actions/courses/create */ "./src/client/actions/courses/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../api/courses/users/create */ "./src/client/api/courses/users/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * For creating course
 *
 * @format
 */

var styles = {
  root: {
    padding: '1%'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  cardHeader: {},
  flexGrow: {
    flex: '1 1 auto'
  }
};

var CourseCreate = function (_Component) {
  (0, _inherits3.default)(CourseCreate, _Component);

  function CourseCreate(props) {
    (0, _classCallCheck3.default)(this, CourseCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CourseCreate.__proto__ || (0, _getPrototypeOf2.default)(CourseCreate)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onFocus = function () {
      return _this.setState({ active: true });
    };

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Course Title...',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(CourseCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      this.setState({ title: e.target.value });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props$elements$user, id, token, title, body, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();
                _props$elements$user = this.props.elements.user, id = _props$elements$user.id, token = _props$elements$user.token;
                title = this.state.title;
                body = { title: title };


                this.setState({ message: 'Creating course...' });

                _context.next = 7;
                return (0, _create4.default)({
                  token: token,
                  userId: id,
                  title: title
                });

              case 7:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 14;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 14:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionCourseCreate((0, _extends3.default)({
                  statusCode: statusCode
                }, payload, body));

              case 17:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          active = _state.active,
          placeholder = _state.placeholder,
          title = _state.title,
          message = _state.message;
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements;
      var _elements$user = elements.user,
          name = _elements$user.name,
          avatar = _elements$user.avatar,
          username = _elements$user.username;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: name,
                  src: avatar || '/public/photos/mayash-logo-transparent.png',
                  className: classes.avatar
                })
              )
            ),
            title: _react2.default.createElement(_Input2.default, {
              value: title,
              onChange: this.onChange,
              placeholder: placeholder
            }),
            className: classes.cardHeader,
            onFocus: this.onFocus
          }),
          active && message.length ? _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              null,
              message
            )
          ) : null,
          active ? _react2.default.createElement(
            _Card.CardActions,
            { disableActionSpacing: true },
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onSubmit },
              'Create'
            )
          ) : null
        )
      );
    }
  }]);
  return CourseCreate;
}(_react.Component);

CourseCreate.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,
  actionCourseCreate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionCourseCreate: _create2.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _styles.withStyles)(styles)(CourseCreate));

/***/ }),

/***/ "./src/client/containers/ElementPage/Tabs/TabCourses.js":
/*!**************************************************************!*\
  !*** ./src/client/containers/ElementPage/Tabs/TabCourses.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _CourseCreate = __webpack_require__(/*! ../../CourseCreate */ "./src/client/containers/CourseCreate/index.js");

var _CourseCreate2 = _interopRequireDefault(_CourseCreate);

var _Timeline = __webpack_require__(/*! ../../Timeline */ "./src/client/containers/Timeline/index.js");

var _Timeline2 = _interopRequireDefault(_Timeline);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {}
};

var TabCourses = function (_Component) {
  (0, _inherits3.default)(TabCourses, _Component);

  function TabCourses(props) {
    (0, _classCallCheck3.default)(this, TabCourses);

    var _this = (0, _possibleConstructorReturn3.default)(this, (TabCourses.__proto__ || (0, _getPrototypeOf2.default)(TabCourses)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(TabCourses, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          element = _props.element,
          courses = _props.courses;
      var isSignedIn = user.isSignedIn,
          id = user.id;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { container: true, justify: 'center', spacing: 0 },
          _react2.default.createElement(
            _Grid2.default,
            { item: true, xs: 12, sm: 9, md: 7, lg: 6, xl: 6 },
            isSignedIn && id === element.id ? _react2.default.createElement(_CourseCreate2.default, null) : null,
            _react2.default.createElement(_Timeline2.default, { type: 'courses', courses: courses, element: element })
          )
        )
      );
    }
  }]);
  return TabCourses;
}(_react.Component);

TabCourses.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.object.isRequired,
  element: _propTypes2.default.object.isRequired,
  courses: _propTypes2.default.object.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return state;
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({}, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _styles.withStyles)(styles)(TabCourses));

/***/ }),

/***/ "./src/client/containers/Post/index.js":
/*!*********************************************!*\
  !*** ./src/client/containers/Post/index.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withRouter = __webpack_require__(/*! react-router/withRouter */ "./node_modules/react-router/withRouter.js");

var _withRouter2 = _interopRequireDefault(_withRouter);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _classnames2 = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames3 = _interopRequireDefault(_classnames2);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Dialog = __webpack_require__(/*! material-ui/Dialog */ "./node_modules/material-ui/Dialog/index.js");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Badge = __webpack_require__(/*! material-ui/Badge */ "./node_modules/material-ui/Badge/index.js");

var _Badge2 = _interopRequireDefault(_Badge);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _TurnedIn = __webpack_require__(/*! material-ui-icons/TurnedIn */ "./node_modules/material-ui-icons/TurnedIn.js");

var _TurnedIn2 = _interopRequireDefault(_TurnedIn);

var _Comment = __webpack_require__(/*! material-ui-icons/Comment */ "./node_modules/material-ui-icons/Comment.js");

var _Comment2 = _interopRequireDefault(_Comment);

var _Delete = __webpack_require__(/*! material-ui-icons/Delete */ "./node_modules/material-ui-icons/Delete.js");

var _Delete2 = _interopRequireDefault(_Delete);

var _RemoveRedEye = __webpack_require__(/*! material-ui-icons/RemoveRedEye */ "./node_modules/material-ui-icons/RemoveRedEye.js");

var _RemoveRedEye2 = _interopRequireDefault(_RemoveRedEye);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _delete = __webpack_require__(/*! ../../api/posts/users/delete */ "./src/client/api/posts/users/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _delete3 = __webpack_require__(/*! ../../actions/posts/delete */ "./src/client/actions/posts/delete.js");

var _delete4 = _interopRequireDefault(_delete3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      padding: '1%'
    },
    card: {
      borderRadius: '8px'
    },
    avatar: {
      borderRadius: '50%'
    },
    title: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    subheader: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    media: {
      height: 194
    },
    favoriteIconActive: {
      color: '#F50057'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    badge: {
      margin: '0 ' + theme.spacing.unit * 2 + 'px',
      color: 'blue'
    },
    margin: {
      border: '0.5px solid rgba(209,209,209,1)',
      marginBottom: '-10px'
    },
    content: {
      textAlign: 'justify'
    }
  };
};

// All actions are defined here.
/**
 * Post container component
 * It will display one post with some of actions (eg. Bookmarks, Share, Delete)
 *
 * @format
 */

var Post = function (_Component) {
  (0, _inherits3.default)(Post, _Component);

  function Post(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, Post);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Post.__proto__ || (0, _getPrototypeOf2.default)(Post)).call(this, props));

    _this.onClickFavorite = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var favorite;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              favorite = _this.state.favorite;


              _this.setState({ favorite: !favorite });

            case 2:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this2);
    }));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onClickPost = function () {
      return _this.props.history.push('/posts/' + _this.props.postId);
    };

    _this.deleteDialogOpen = function () {
      _this.setState({ deleteDialog: true });
    };

    _this.deleteDialogClose = function () {
      _this.setState({ deleteDialog: false });
    };

    _this.deletePostClick = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
      var _this$props$elements$, token, id, postId, _ref3, statusCode, error;

      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _this$props$elements$ = _this.props.elements.user, token = _this$props$elements$.token, id = _this$props$elements$.id;
              postId = _this.props.postId;
              _context2.next = 4;
              return (0, _delete2.default)({
                token: token,
                userId: id,
                postId: postId
              });

            case 4:
              _ref3 = _context2.sent;
              statusCode = _ref3.statusCode;
              error = _ref3.error;

              if (!(statusCode !== 200)) {
                _context2.next = 10;
                break;
              }

              // show some error message.
              console.log(error);
              return _context2.abrupt('return');

            case 10:

              _this.props.actionPostDelete({ postId: postId });
              _this.setState({ deleteDialog: false });

            case 12:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }));

    _this.state = {
      hover: false,
      favorite: false,
      deleteDialog: false
    };
    return _this;
  }

  // componentWillMount is executed before rendering


  (0, _createClass3.default)(Post, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _props = this.props,
          elements = _props.elements,
          posts = _props.posts,
          postId = _props.postId;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = posts[postId].authorId;


      var author = elements[authorId];

      // if author is not found
      if (userId !== authorId && !author) {
        this.props.getElementById({ id: authorId, token: token });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          posts = _props2.posts,
          elements = _props2.elements,
          postId = _props2.postId;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id;


      var post = posts[postId];

      var favorite = this.state.favorite;


      if (typeof post === 'undefined') {
        return _react2.default.createElement('div', null);
      }

      var cover = post.cover,
          title = post.title,
          description = post.description,
          authorId = post.authorId;

      var author = authorId === userId ? user : elements[authorId];

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + author.username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: author.name,
                  src: '/public/photos/mayash-logo-transparent.png'
                })
              )
            ),
            action: _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Coming soon', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { disabled: true },
                _react2.default.createElement(_MoreVert2.default, null)
              )
            ),
            title: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.title },
              author.name
            ),
            subheader: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.subheader },
              '@',
              author.username
            )
          }),
          _react2.default.createElement(_Card.CardMedia, {
            className: classes.media,
            image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg',
            title: title
          }),
          _react2.default.createElement(
            _Card.CardContent,
            { onClick: this.onClickPost },
            _react2.default.createElement(
              _Typography2.default,
              { type: 'title' },
              title
            ),
            typeof description === 'string' ? _react2.default.createElement(
              _Typography2.default,
              { type: 'body1', gutterBottom: true, className: classes.content },
              description
            ) : null
          ),
          _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement(
              _IconButton2.default,
              { onClick: this.onClickFavorite },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_Favorite2.default, {
                  className: (0, _classnames3.default)((0, _defineProperty3.default)({}, '' + classes.favoriteIconActive, favorite))
                })
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Comment' },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_Comment2.default, null)
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Book Marks' },
              _react2.default.createElement(_TurnedIn2.default, null)
            ),
            isSignedIn && userId === authorId ? _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Delete', onClick: this.deleteDialogOpen },
                _react2.default.createElement(_Delete2.default, null)
              ),
              _react2.default.createElement(
                _Dialog2.default,
                {
                  open: this.state.deleteDialog,
                  onClose: this.deleteDialogClose,
                  'aria-labelledby': 'post-' + postId + '-delete-dialog'
                },
                _react2.default.createElement(
                  _Dialog.DialogTitle,
                  { id: 'post-' + postId + '-delete-dialog' },
                  'Are you sure?'
                ),
                _react2.default.createElement(
                  _Dialog.DialogActions,
                  null,
                  _react2.default.createElement(
                    _Button2.default,
                    { onClick: this.deleteDialogClose, color: 'primary' },
                    'Cancel'
                  ),
                  _react2.default.createElement(
                    _Button2.default,
                    {
                      onClick: this.deletePostClick,
                      color: 'primary',
                      autoFocus: true
                    },
                    'Delete'
                  )
                )
              )
            ) : null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share' },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_RemoveRedEye2.default, null)
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share', disabled: true },
              _react2.default.createElement(_Share2.default, null)
            )
          )
        )
      );
    }
  }]);
  return Post;
}(_react.Component);

Post.propTypes = {
  history: _propTypes2.default.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object.isRequired,

  // post: PropTypes.shape({
  //   authorId: PropTypes.number.isRequired,
  //   postId: PropTypes.number.isRequired,
  //   title: PropTypes.string.isRequired,
  //   description: PropTypes.string,
  // }).isRequired,

  postId: _propTypes2.default.number.isRequired,

  getElementById: _propTypes2.default.func.isRequired,
  actionPostDelete: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements,
      posts = _ref4.posts;
  return { elements: elements, posts: posts };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    getElementById: _getById2.default,
    actionPostDelete: _delete4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)((0, _withRouter2.default)(Post)));

/***/ }),

/***/ "./src/client/containers/Timeline/index.js":
/*!*************************************************!*\
  !*** ./src/client/containers/Timeline/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Post = __webpack_require__(/*! ../Post */ "./src/client/containers/Post/index.js");

var _Post2 = _interopRequireDefault(_Post);

var _Course = __webpack_require__(/*! ../Course */ "./src/client/containers/Course/index.js");

var _Course2 = _interopRequireDefault(_Course);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    flexGrow: '1'
  }
}; /**
    * Timeline Display component
    * This component will display either course or Post timeline
    *
    * @format
    */

var Timeline = function (_Component) {
  (0, _inherits3.default)(Timeline, _Component);

  function Timeline(props) {
    (0, _classCallCheck3.default)(this, Timeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Timeline.__proto__ || (0, _getPrototypeOf2.default)(Timeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Timeline, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          type = _props.type,
          posts = _props.posts,
          courses = _props.courses,
          element = _props.element;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        type === 'posts' ? element.posts.map(function (p) {
          return _react2.default.createElement(_Post2.default, { key: p.postId, postId: p.postId });
        }) : element.courses.map(function (c) {
          return _react2.default.createElement(_Course2.default, { key: c.courseId, courseId: c.courseId });
        })
      );
    }
  }]);
  return Timeline;
}(_react.Component);

Timeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  type: _propTypes2.default.string.isRequired, // 'posts' or 'courses'
  element: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object,
  courses: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(Timeline);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1JlbW92ZVJlZEV5ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL0JhZGdlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0FjdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nQ29udGVudFRleHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dUaXRsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvd2l0aE1vYmlsZURpYWxvZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9CYWNrZHJvcC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvTW9kYWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1BvcnRhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvRmFkZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvUm91dGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9tYXRjaFBhdGguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci93aXRoUm91dGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL2RlbGV0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvZGVsZXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvZGVsZXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2VsZW1lbnRzL2dldEJ5SWQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvcG9zdHMvdXNlcnMvZGVsZXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29tcG9uZW50cy9JbnB1dC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VDcmVhdGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0VsZW1lbnRQYWdlL1RhYnMvVGFiQ291cnNlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvVGltZWxpbmUvaW5kZXguanMiXSwibmFtZXMiOlsiY3JlYXRlIiwicGF5bG9hZCIsInR5cGUiLCJnZXRFbGVtZW50U3RhcnQiLCJnZXRFbGVtZW50U3VjY2VzcyIsImdldEVsZW1lbnRFcnJvciIsImdldCIsInRva2VuIiwiaWQiLCJkaXNwYXRjaCIsInRoZW4iLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJjYXRjaCIsImRlbGV0ZVBvc3QiLCJ1c2VySWQiLCJ0aXRsZSIsInVybCIsIm1ldGhvZCIsImhlYWRlcnMiLCJBY2NlcHQiLCJBdXRob3JpemF0aW9uIiwiYm9keSIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJqc29uIiwiY29uc29sZSIsImNvdXJzZUlkIiwiZGVsZXRlQ291cnNlIiwiZ2V0QnlJZCIsInBvc3RJZCIsInN0eWxlcyIsInJvb3QiLCJkaXNwbGF5IiwiaW5wdXQiLCJmbGV4R3JvdyIsImJvcmRlciIsImJvcmRlclJhZGl1cyIsInBhZGRpbmciLCJvdXRsaW5lIiwiYm9yZGVyQ29sb3IiLCJib3hTaGFkb3ciLCJJbnB1dCIsImNsYXNzZXMiLCJvbkNoYW5nZSIsInZhbHVlIiwicGxhY2Vob2xkZXIiLCJkaXNhYmxlZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwib25lT2ZUeXBlIiwic3RyaW5nIiwibnVtYmVyIiwiYm9vbCIsImdyaWRDb250YWluZXIiLCJuYW1lIiwiY3Vyc29yIiwidGV4dERlY29yYXRpb24iLCJ1c2VybmFtZSIsInBvc3QiLCJjYXJkSGVhZGVyIiwiY2FyZCIsImF2YXRhciIsImNhcmRNZWRpYSIsIm1lZGlhIiwiaGVpZ2h0IiwiY2FyZE1lZGlhSW1hZ2UiLCJ3aWR0aCIsIm1hcmdpbiIsIm1hcmdpbkJvdHRvbSIsImZsZXgiLCJDb3Vyc2UiLCJwcm9wcyIsIm9uTW91c2VFbnRlciIsInNldFN0YXRlIiwiaG92ZXIiLCJvbk1vdXNlTGVhdmUiLCJvbkNsaWNrQ29udGVudCIsImhpc3RvcnkiLCJwdXNoIiwiZGVsZXRlRGlhbG9nT3BlbiIsImRlbGV0ZURpYWxvZyIsImRlbGV0ZURpYWxvZ0Nsb3NlIiwiZGVsZXRlQ291cnNlQ2xpY2siLCJlbGVtZW50cyIsInVzZXIiLCJsb2ciLCJhY3Rpb25Db3Vyc2VEZWxldGUiLCJzdGF0ZSIsImNvdXJzZXMiLCJhdXRob3JJZCIsImF1dGhvciIsImFjdGlvbkdldEVsZW1lbnRCeUlkIiwiaXNTaWduZWRJbiIsImNvdXJzZSIsImRlc2NyaXB0aW9uIiwiY292ZXIiLCJtYXBTdGF0ZVRvUHJvcHMiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJDb3Vyc2VDcmVhdGUiLCJvbkZvY3VzIiwiYWN0aXZlIiwidGl0bGVMZW5ndGgiLCJ2YWxpZCIsIm1lc3NhZ2UiLCJiaW5kIiwib25TdWJtaXQiLCJlIiwidGFyZ2V0IiwicHJldmVudERlZmF1bHQiLCJzZXRUaW1lb3V0IiwiYWN0aW9uQ291cnNlQ3JlYXRlIiwibGVuZ3RoIiwiVGFiQ291cnNlcyIsImVsZW1lbnQiLCJ0aGVtZSIsInN1YmhlYWRlciIsImZhdm9yaXRlSWNvbkFjdGl2ZSIsImNvbG9yIiwiYmFkZ2UiLCJzcGFjaW5nIiwidW5pdCIsImNvbnRlbnQiLCJ0ZXh0QWxpZ24iLCJQb3N0Iiwib25DbGlja0Zhdm9yaXRlIiwiZmF2b3JpdGUiLCJvbkNsaWNrUG9zdCIsImRlbGV0ZVBvc3RDbGljayIsImFjdGlvblBvc3REZWxldGUiLCJwb3N0cyIsImdldEVsZW1lbnRCeUlkIiwiVGltZWxpbmUiLCJtYXAiLCJwIiwiYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBLG9DOzs7Ozs7Ozs7Ozs7O0FDcEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtJQUFrSTs7QUFFcEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwwQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHFGQUFxRjs7QUFFdkk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNMQUFzTDs7QUFFeE87QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlKQUF5Sjs7QUFFM007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlOQUF5Tjs7QUFFM1E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELCtXQUErVzs7QUFFamE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx3Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1FQUFtRTs7QUFFckg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSw4RkFBOEY7QUFDOUY7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsaUVBQWlFLGdDQUFnQztBQUNqRyxTQUFTO0FBQ1Q7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsb0JBQW9CLFU7Ozs7Ozs7Ozs7Ozs7QUMvTXpFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsbVBBQTRJOztBQUU1STs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxvR0FBb0c7O0FBRXBHO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBLFdBQVcsNEJBQTRCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQsbUJBQW1CLFM7Ozs7Ozs7Ozs7Ozs7QUNuS3hFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLDBKQUEwSjtBQUMxSixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxvQkFBb0IsVTs7Ozs7Ozs7Ozs7OztBQ2hVekU7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsNEJBQTRCLGdFQUFnRTtBQUM1RjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsU0FBUyw0QkFBNEI7QUFDckM7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxxREFBcUQsMkJBQTJCLGlCOzs7Ozs7Ozs7Ozs7O0FDaEhoRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSw0QkFBNEIsZ0VBQWdFO0FBQzVGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QscURBQXFELDJCQUEyQixpQjs7Ozs7Ozs7Ozs7OztBQ3ZGaEY7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsNEJBQTRCLGdFQUFnRTtBQUM1RjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELHFEQUFxRCwrQkFBK0IscUI7Ozs7Ozs7Ozs7Ozs7QUN2RnBGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLGdDQUFnQyxnRUFBZ0U7QUFDaEc7QUFDQTtBQUNBLFdBQVcsZ0JBQWdCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxxREFBcUQseUJBQXlCLGU7Ozs7Ozs7Ozs7Ozs7QUM1SDlFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUM1RDdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFGQUFxRjtBQUNyRjtBQUNBOzs7QUFHQTtBQUNBLDhFQUE4RSxtRUFBbUU7QUFDako7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDaEVBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STtBQUM1STs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RTtBQUM5RTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBLGFBQWEsMEJBQTBCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHdCQUF3QixjOzs7Ozs7Ozs7Ozs7O0FDck83RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0Esa0dBQWtHOztBQUVsRztBQUNBO0FBQ0EsZ0NBQWdDLGtEQUFrRDtBQUNsRjtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxzQkFBc0IsWTs7Ozs7Ozs7Ozs7OztBQ3hJM0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0I7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixlQUFlO0FBQ3RDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLGdCQUFnQjtBQUN2QztBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLGdDQUFnQyxnRUFBZ0U7QUFDaEc7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSwwR0FBMEc7QUFDMUcsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEscUJBQXFCLGVBQWU7QUFDcEM7QUFDQTtBQUNBOztBQUVBLHFEQUFxRCxnQ0FBZ0MsUzs7Ozs7Ozs7Ozs7OztBQ2psQnJGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1GQUFtRjtBQUNuRjtBQUNBOztBQUVBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHVCQUF1Qix1QkFBdUI7QUFDOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxxQkFBcUIsdUJBQXVCO0FBQzVDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsc0JBQXNCOztBQUV0QjtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDdEpBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QseUI7Ozs7Ozs7Ozs7Ozs7QUNyTEE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEU7Ozs7Ozs7Ozs7Ozs7QUNoQkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDJDQUEyQzs7QUFFM0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUQ7Ozs7Ozs7Ozs7Ozs7QUNwTkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMkJBQTJCO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSCxDOzs7Ozs7Ozs7Ozs7O0FDaERBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Riw4Q0FBOEMsaUJBQWlCLHFCQUFxQixvQ0FBb0MsNkRBQTZELG9CQUFvQixFQUFFLGVBQWU7O0FBRTFOLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdGQUFnRjs7QUFFaEY7O0FBRUEsZ0ZBQWdGLGVBQWU7O0FBRS9GLHlEQUF5RCxVQUFVLHVEQUF1RDtBQUMxSDs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsR0FBRztBQUNIO0FBQ0EsdUI7Ozs7Ozs7Ozs7Ozs7QUM3R0E7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Q0FBNEM7O0FBRTVDOztBQUVBOztBQUVBOztBQUVBLHNEQUFzRCxpRUFBaUU7QUFDdkg7O0FBRUE7QUFDQSxvSkFBb0o7O0FBRXBKLGlNQUFpTTs7QUFFak0sMkxBQTJMO0FBQzNMOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLGlCQUFpQjs7QUFFakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Qjs7Ozs7Ozs7Ozs7OztBQ3ZKQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxvRUFBb0U7O0FBRXBFOztBQUVBO0FBQ0E7QUFDQSx5QkFBeUI7O0FBRXpCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw4Q0FBOEM7O0FBRTlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx3Q0FBd0MsbURBQW1EO0FBQzNGO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUssSUFBSTtBQUNUO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUM1RUE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDhDQUE4QyxpQkFBaUIscUJBQXFCLG9DQUFvQyw2REFBNkQsb0JBQW9CLEVBQUUsZUFBZTs7QUFFMU47QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsMkRBQTJEO0FBQzNELG1FQUFtRSx3Q0FBd0MsMkJBQTJCO0FBQ3RJLE9BQU8sRUFBRTtBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2hEQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNmQTs7QUFFQTs7QUFFQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSw4RDs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDekRBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsVUFBVSxxQkFBcUI7QUFDNUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1RBOztBQWVBOzs7Ozs7Ozs7OztBQVdBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLENBQWYsQyxDQS9CQTs7Ozs7a0JBaUNlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDNUJmOztBQVlBOzs7Ozs7OztrQkFRZSxVQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLEMsRUF6QmY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRUE7O0FBTUE7Ozs7OztBQUVBOzs7Ozs7QUFWQTs7QUFnQkEsSUFBTUUsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFDRixPQUFEO0FBQUEsU0FBYyxFQUFFQyxpQ0FBRixFQUEyQkQsZ0JBQTNCLEVBQWQ7QUFBQSxDQUF4Qjs7QUFFQTs7Ozs7Ozs7Ozs7OztBQWFBLElBQU1HLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQUNILE9BQUQ7QUFBQSxTQUFjLEVBQUVDLG1DQUFGLEVBQTZCRCxnQkFBN0IsRUFBZDtBQUFBLENBQTFCOztBQUVBOzs7Ozs7OztBQVFBLElBQU1JLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ0osT0FBRDtBQUFBLFNBQWMsRUFBRUMsaUNBQUYsRUFBMkJELGdCQUEzQixFQUFkO0FBQUEsQ0FBeEI7O0FBRUE7Ozs7Ozs7QUFPQSxJQUFNSyxNQUFNLFNBQU5BLEdBQU07QUFBQSxNQUFHQyxLQUFILFFBQUdBLEtBQUg7QUFBQSxNQUFVQyxFQUFWLFFBQVVBLEVBQVY7QUFBQSxTQUFtQixVQUFDQyxRQUFELEVBQWM7QUFDM0NBLGFBQVNOLGdCQUFnQixFQUFFSyxNQUFGLEVBQWhCLENBQVQ7O0FBRUEsMkJBQVcsRUFBRUQsWUFBRixFQUFTQyxNQUFULEVBQVgsRUFDR0UsSUFESCxDQUNRLGlCQUFvQztBQUFBLFVBQWpDQyxVQUFpQyxTQUFqQ0EsVUFBaUM7QUFBQSxVQUFyQkMsS0FBcUIsU0FBckJBLEtBQXFCO0FBQUEsVUFBZFgsT0FBYyxTQUFkQSxPQUFjOztBQUN4QyxVQUFJVSxjQUFjLEdBQWxCLEVBQXVCO0FBQ3JCRixpQkFBU0osZ0JBQWdCLEVBQUVHLE1BQUYsRUFBTUcsc0JBQU4sRUFBa0JDLFlBQWxCLEVBQWhCLENBQVQ7O0FBRUE7QUFDRDs7QUFFREgsZUFBU0wsMkNBQW9CSSxNQUFwQixFQUF3Qkcsc0JBQXhCLElBQXVDVixPQUF2QyxFQUFUO0FBQ0QsS0FUSCxFQVVHWSxLQVZILENBVVMsaUJBQTJCO0FBQUEsVUFBeEJGLFVBQXdCLFNBQXhCQSxVQUF3QjtBQUFBLFVBQVpDLEtBQVksU0FBWkEsS0FBWTs7QUFDaENILGVBQVNKLGdCQUFnQixFQUFFRyxNQUFGLEVBQU1HLHNCQUFOLEVBQWtCQyxZQUFsQixFQUFoQixDQUFUO0FBQ0QsS0FaSDtBQWFELEdBaEJXO0FBQUEsQ0FBWjs7a0JBa0JlTixHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0RmOztBQVlBOzs7Ozs7O0FBT0EsSUFBTVEsYUFBYSxTQUFiQSxVQUFhLENBQUNiLE9BQUQ7QUFBQSxTQUErQjtBQUNoREMsNEJBRGdEO0FBRWhERDtBQUZnRCxHQUEvQjtBQUFBLENBQW5CLEMsQ0F4QkE7Ozs7O2tCQTZCZWEsVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmZjs7Ozs7Ozs7Ozs7O0FBZEE7Ozs7OztzRkEwQkE7QUFBQSxRQUF3QkMsTUFBeEIsU0FBd0JBLE1BQXhCO0FBQUEsUUFBZ0NSLEtBQWhDLFNBQWdDQSxLQUFoQztBQUFBLFFBQXVDUyxLQUF2QyxTQUF1Q0EsS0FBdkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNGLE1BRnJDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1FLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsTUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlZDtBQUhSLGVBRmtCO0FBTzNCZSxvQkFBTSx5QkFBZSxFQUFFTixZQUFGLEVBQWY7QUFQcUIsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBY1lDLGtCQWRaLEdBY21DRCxHQWRuQyxDQWNZQyxNQWRaLEVBY29CQyxVQWRwQixHQWNtQ0YsR0FkbkMsQ0Fjb0JFLFVBZHBCOztBQUFBLGtCQWdCUUQsVUFBVSxHQWhCbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBaUJhO0FBQ0xiLDBCQUFZYSxNQURQO0FBRUxaLHFCQUFPYTtBQUZGLGFBakJiOztBQUFBO0FBQUE7QUFBQSxtQkF1QnVCRixJQUFJRyxJQUFKLEVBdkJ2Qjs7QUFBQTtBQXVCVUEsZ0JBdkJWO0FBQUEsd0VBeUJnQkEsSUF6QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEyQklDLG9CQUFRZixLQUFSOztBQTNCSiw2Q0E2Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBN0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlWixNOzs7OztBQXJCZjs7OztBQUNBOzs7O2tCQXdEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hEZjs7Ozs7Ozs7Ozs7O0FBZEE7Ozs7OztzRkEwQkE7QUFBQSxRQUE4Qk8sS0FBOUIsU0FBOEJBLEtBQTlCO0FBQUEsUUFBcUNRLE1BQXJDLFNBQXFDQSxNQUFyQztBQUFBLFFBQTZDYSxRQUE3QyxTQUE2Q0EsUUFBN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVVgsZUFGVixrQ0FFcUNGLE1BRnJDLGlCQUV1RGEsUUFGdkQ7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTVgsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxRQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVkO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVZ0IsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTGIsMEJBQVlhLE1BRFA7QUFFTFoscUJBQU9hO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlHLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFmLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVpQixZOzs7OztBQXJCZjs7OztBQUNBOzs7O2tCQXVEZUEsWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hEZjs7Ozs7OztBQUxBOzs7c0ZBWUE7QUFBQSxRQUF5QnRCLEtBQXpCLFNBQXlCQSxLQUF6QjtBQUFBLFFBQWdDQyxFQUFoQyxTQUFnQ0EsRUFBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVVMsZUFGVixxQ0FFd0NULEVBRnhDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1TLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlZDtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVWdCLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xiLDBCQUFZYSxNQURQO0FBRUxaLHFCQUFPYTtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJRyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRZixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFla0IsTzs7Ozs7QUFWZjs7OztBQUNBOzs7O2tCQTRDZUEsTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZDZjs7Ozs7Ozs7Ozs7O0FBUkE7Ozs7OztzRkFvQkE7QUFBQSxRQUE0QmYsTUFBNUIsU0FBNEJBLE1BQTVCO0FBQUEsUUFBb0NSLEtBQXBDLFNBQW9DQSxLQUFwQztBQUFBLFFBQTJDd0IsTUFBM0MsU0FBMkNBLE1BQTNDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVkLGVBRlYsa0NBRXFDRixNQUZyQyxlQUVxRGdCLE1BRnJEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1kLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsUUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlZDtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVWdCLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xiLDBCQUFZYSxNQURQO0FBRUxaLHFCQUFPYTtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJRyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRZixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlRSxVOzs7OztBQWZmOzs7O0FBQ0E7Ozs7a0JBaURlQSxVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaERmOzs7O0FBQ0E7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTWtCLFNBQVM7QUFDYkMsUUFBTTtBQUNKQyxhQUFTO0FBREwsR0FETztBQUliQyxTQUFPO0FBQ0xDLGNBQVUsR0FETDtBQUVMQyxZQUFRLG1CQUZIO0FBR0xDLGtCQUFjLEtBSFQ7QUFJTEMsYUFBUyxLQUpKO0FBS0wsZUFBVztBQUNUQyxlQUFTLE1BREE7QUFFVEMsbUJBQWEsU0FGSjtBQUdUQyxpQkFBVztBQUhGO0FBTE47QUFKTSxDQUFmLEMsQ0FaQTs7Ozs7OztBQTZCQSxJQUFNQyxRQUFRLFNBQVJBLEtBQVE7QUFBQSxNQUFHQyxPQUFILFFBQUdBLE9BQUg7QUFBQSxNQUFZQyxRQUFaLFFBQVlBLFFBQVo7QUFBQSxNQUFzQkMsS0FBdEIsUUFBc0JBLEtBQXRCO0FBQUEsTUFBNkJDLFdBQTdCLFFBQTZCQSxXQUE3QjtBQUFBLE1BQTBDQyxRQUExQyxRQUEwQ0EsUUFBMUM7QUFBQSxNQUFvRDlDLElBQXBELFFBQW9EQSxJQUFwRDtBQUFBLFNBQ1o7QUFBQTtBQUFBLE1BQUssV0FBVzBDLFFBQVFYLElBQXhCO0FBQ0U7QUFDRSxZQUFNL0IsUUFBUSxNQURoQjtBQUVFLG1CQUFhNkMsZUFBZSxPQUY5QjtBQUdFLGFBQU9ELEtBSFQ7QUFJRSxnQkFBVUQsUUFKWjtBQUtFLGlCQUFXRCxRQUFRVCxLQUxyQjtBQU1FLGdCQUFVLENBQUMsQ0FBQ2E7QUFOZDtBQURGLEdBRFk7QUFBQSxDQUFkOztBQWFBTCxNQUFNTSxTQUFOLEdBQWtCO0FBQ2hCTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQURWO0FBRWhCTixZQUFVLG9CQUFVTyxJQUFWLENBQWVELFVBRlQ7QUFHaEJMLFNBQU8sb0JBQVVPLFNBQVYsQ0FBb0IsQ0FBQyxvQkFBVUMsTUFBWCxFQUFtQixvQkFBVUMsTUFBN0IsQ0FBcEIsRUFBMERKLFVBSGpEO0FBSWhCSixlQUFhLG9CQUFVTyxNQUpQO0FBS2hCcEQsUUFBTSxvQkFBVW9ELE1BTEE7QUFNaEJOLFlBQVUsb0JBQVVRO0FBTkosQ0FBbEI7O2tCQVNlLDBCQUFXeEIsTUFBWCxFQUFtQlcsS0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1Q2Y7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBTUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFNWCxTQUFTO0FBQ2JDLFFBQU07QUFDSk0sYUFBUztBQURMLEdBRE87QUFJYmtCLGlCQUFlLEVBSkY7QUFLYkMsUUFBTTtBQUNKQyxZQUFRLFNBREo7QUFFSkMsb0JBQWdCO0FBRlosR0FMTztBQVNiQyxZQUFVO0FBQ1JGLFlBQVEsU0FEQTtBQUVSQyxvQkFBZ0I7QUFGUixHQVRHO0FBYWJFLFFBQU07QUFDSnZCLGFBQVM7QUFETCxHQWJPO0FBZ0Jid0IsY0FBWTtBQUNWO0FBRFUsR0FoQkM7QUFtQmJDLFFBQU07QUFDSjFCLGtCQUFjO0FBRFYsR0FuQk87QUFzQmIyQixVQUFRO0FBQ04zQixrQkFBYztBQURSLEdBdEJLO0FBeUJiNEIsYUFBVyxFQXpCRTtBQTBCYkMsU0FBTztBQUNMQyxZQUFRO0FBREgsR0ExQk07QUE2QmJDLGtCQUFnQjtBQUNkQyxXQUFPO0FBRE8sR0E3Qkg7QUFnQ2JDLFVBQVE7QUFDTmxDLFlBQVEsaUNBREY7QUFFTm1DLGtCQUFjO0FBRlIsR0FoQ0s7QUFvQ2JwQyxZQUFVO0FBQ1JxQyxVQUFNO0FBREU7QUFwQ0csQ0FBZixDLENBeENBOzs7Ozs7O0lBaUZNQyxNOzs7QUFDSixrQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBOztBQUFBLHNJQUNYQSxLQURXOztBQUFBLFVBcUJuQkMsWUFyQm1CLEdBcUJKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBckJJOztBQUFBLFVBc0JuQkMsWUF0Qm1CLEdBc0JKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBdEJJOztBQUFBLFVBd0JuQkUsY0F4Qm1CLEdBd0JGLFlBQU07QUFBQSxVQUNicEQsUUFEYSxHQUNBLE1BQUsrQyxLQURMLENBQ2IvQyxRQURhOzs7QUFHckIsWUFBSytDLEtBQUwsQ0FBV00sT0FBWCxDQUFtQkMsSUFBbkIsZUFBb0N0RCxRQUFwQztBQUNELEtBNUJrQjs7QUFBQSxVQThCbkJ1RCxnQkE5Qm1CLEdBOEJBLFlBQU07QUFDdkIsWUFBS04sUUFBTCxDQUFjLEVBQUVPLGNBQWMsSUFBaEIsRUFBZDtBQUNELEtBaENrQjs7QUFBQSxVQWtDbkJDLGlCQWxDbUIsR0FrQ0MsWUFBTTtBQUN4QixZQUFLUixRQUFMLENBQWMsRUFBRU8sY0FBYyxLQUFoQixFQUFkO0FBQ0QsS0FwQ2tCOztBQUFBLFVBc0NuQkUsaUJBdENtQiw0RUFzQ0M7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNDQUNJLE1BQUtYLEtBQUwsQ0FBV1ksUUFBWCxDQUFvQkMsSUFEeEIsRUFDVmpGLEtBRFUseUJBQ1ZBLEtBRFUsRUFDSEMsRUFERyx5QkFDSEEsRUFERztBQUVWb0Isc0JBRlUsR0FFRyxNQUFLK0MsS0FGUixDQUVWL0MsUUFGVTtBQUFBO0FBQUEscUJBSWtCLHNCQUFnQjtBQUNsRHJCLDRCQURrRDtBQUVsRFEsd0JBQVFQLEVBRjBDO0FBR2xEb0I7QUFIa0QsZUFBaEIsQ0FKbEI7O0FBQUE7QUFBQTtBQUlWakIsd0JBSlUsU0FJVkEsVUFKVTtBQUlFQyxtQkFKRixTQUlFQSxLQUpGOztBQUFBLG9CQVNkRCxlQUFlLEdBVEQ7QUFBQTtBQUFBO0FBQUE7O0FBVWhCO0FBQ0FnQixzQkFBUThELEdBQVIsQ0FBWTdFLEtBQVo7QUFYZ0I7O0FBQUE7O0FBZWxCLG9CQUFLK0QsS0FBTCxDQUFXZSxrQkFBWCxDQUE4QixFQUFFOUQsa0JBQUYsRUFBOUI7QUFDQSxvQkFBS2lELFFBQUwsQ0FBYyxFQUFFTyxjQUFjLEtBQWhCLEVBQWQ7O0FBaEJrQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQXRDRDs7QUFFakIsVUFBS08sS0FBTCxHQUFhO0FBQ1hiLGFBQU8sS0FESTtBQUVYTSxvQkFBYztBQUZILEtBQWI7QUFGaUI7QUFNbEI7Ozs7eUNBRW9CO0FBQUEsbUJBQ3FCLEtBQUtULEtBRDFCO0FBQUEsVUFDWFksUUFEVyxVQUNYQSxRQURXO0FBQUEsVUFDREssT0FEQyxVQUNEQSxPQURDO0FBQUEsVUFDUWhFLFFBRFIsVUFDUUEsUUFEUjtBQUFBLDJCQUVXMkQsU0FBU0MsSUFGcEI7QUFBQSxVQUVYakYsS0FGVyxrQkFFWEEsS0FGVztBQUFBLFVBRUFRLE1BRkEsa0JBRUpQLEVBRkk7QUFBQSxVQUlYcUYsUUFKVyxHQUlFRCxRQUFRaEUsUUFBUixDQUpGLENBSVhpRSxRQUpXOztBQUtuQixVQUFNQyxTQUFTUCxTQUFTTSxRQUFULENBQWY7O0FBRUE7QUFDQSxVQUFJOUUsV0FBVzhFLFFBQVgsSUFBdUIsQ0FBQ0MsTUFBNUIsRUFBb0M7QUFDbEMsYUFBS25CLEtBQUwsQ0FBV29CLG9CQUFYLENBQWdDLEVBQUV2RixJQUFJcUYsUUFBTixFQUFnQnRGLFlBQWhCLEVBQWhDO0FBQ0Q7QUFDRjs7OzZCQXNDUTtBQUFBLG9CQUMwQyxLQUFLb0UsS0FEL0M7QUFBQSxVQUNDL0IsT0FERCxXQUNDQSxPQUREO0FBQUEsVUFDVWdELE9BRFYsV0FDVUEsT0FEVjtBQUFBLFVBQ21CTCxRQURuQixXQUNtQkEsUUFEbkI7QUFBQSxVQUM2QjNELFFBRDdCLFdBQzZCQSxRQUQ3QjtBQUFBLFVBR0M0RCxJQUhELEdBR1VELFFBSFYsQ0FHQ0MsSUFIRDtBQUFBLFVBSUNRLFVBSkQsR0FJNEJSLElBSjVCLENBSUNRLFVBSkQ7QUFBQSxVQUlpQmpGLE1BSmpCLEdBSTRCeUUsSUFKNUIsQ0FJYWhGLEVBSmI7OztBQU1QLFVBQU15RixTQUFTTCxRQUFRaEUsUUFBUixDQUFmOztBQUVBLFVBQUksT0FBT3FFLE1BQVAsS0FBa0IsV0FBdEIsRUFBbUM7QUFDakMsZUFBTywwQ0FBUDtBQUNEOztBQVZNLFVBWUNqRixLQVpELEdBWXlDaUYsTUFaekMsQ0FZQ2pGLEtBWkQ7QUFBQSxVQVlRa0YsV0FaUixHQVl5Q0QsTUFaekMsQ0FZUUMsV0FaUjtBQUFBLFVBWXFCTCxRQVpyQixHQVl5Q0ksTUFaekMsQ0FZcUJKLFFBWnJCO0FBQUEsVUFZK0JNLEtBWi9CLEdBWXlDRixNQVp6QyxDQVkrQkUsS0FaL0I7O0FBYVAsVUFBTUwsU0FBU0QsYUFBYTlFLE1BQWIsR0FBc0J5RSxJQUF0QixHQUE2QkQsU0FBU00sUUFBVCxDQUE1Qzs7QUFFQSxhQUNFO0FBQUE7QUFBQTtBQUNFLHFCQUFXakQsUUFBUVgsSUFEckI7QUFFRSx3QkFBYyxLQUFLMkMsWUFGckI7QUFHRSx3QkFBYyxLQUFLRztBQUhyQjtBQUtFO0FBQUE7QUFBQSxZQUFNLFFBQVEsS0FBS1ksS0FBTCxDQUFXYixLQUF6QixFQUFnQyxXQUFXbEMsUUFBUW9CLElBQW5EO0FBQ0U7QUFDRSxvQkFDRTtBQUFBO0FBQUEsZ0JBQU8sV0FBV3BCLFFBQVFxQixNQUExQixFQUFrQyxXQUFXLENBQTdDO0FBQ0U7QUFDRSxxQkFBSzZCLE9BQU9wQyxJQURkO0FBRUUscUJBQ0VvQyxPQUFPN0IsTUFBUCxJQUNBO0FBSko7QUFERixhQUZKO0FBWUUsb0JBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLGtCQUFTLE9BQU0sYUFBZixFQUE2QixXQUFVLFFBQXZDO0FBQ0U7QUFBQTtBQUFBLG9CQUFZLGNBQVo7QUFDRTtBQURGO0FBREY7QUFERixhQWJKO0FBcUJFLG1CQUNFO0FBQUE7QUFBQSxnQkFBTSxXQUFTNkIsT0FBT2pDLFFBQXRCLEVBQWtDLFdBQVdqQixRQUFRYyxJQUFyRDtBQUNHb0MscUJBQU9wQztBQURWLGFBdEJKO0FBMEJFLHVCQUNFO0FBQUE7QUFBQSxnQkFBTSxXQUFTb0MsT0FBT2pDLFFBQXRCLEVBQWtDLFdBQVdqQixRQUFRaUIsUUFBckQ7QUFBQSxvQkFDT2lDLE9BQU9qQztBQURkO0FBM0JKLFlBREY7QUFpQ0U7QUFDRSx1QkFBV2pCLFFBQVF1QixLQURyQjtBQUVFLG1CQUNFZ0MsU0FDQTtBQUpKLFlBakNGO0FBd0NFO0FBQUE7QUFBQSxjQUFhLFNBQVMsS0FBS25CLGNBQTNCO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLE1BQUssT0FBakIsRUFBeUIsa0JBQXpCO0FBQ0doRTtBQURILGFBREY7QUFJRyxtQkFBT2tGLFdBQVAsS0FBdUIsUUFBdkIsR0FDQztBQUFBO0FBQUEsZ0JBQVksTUFBSyxPQUFqQixFQUF5QixrQkFBekI7QUFDR0E7QUFESCxhQURELEdBSUc7QUFSTixXQXhDRjtBQWtERTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsZ0JBQVMsT0FBTSxrQkFBZixFQUFrQyxXQUFVLFFBQTVDO0FBQ0U7QUFBQTtBQUFBLGtCQUFZLGNBQVcsa0JBQXZCLEVBQTBDLGNBQTFDO0FBQ0U7QUFERjtBQURGLGFBREY7QUFNRTtBQUFBO0FBQUEsZ0JBQVMsT0FBTSxVQUFmLEVBQTBCLFdBQVUsUUFBcEM7QUFDRTtBQUFBO0FBQUEsa0JBQVksY0FBVyxZQUF2QixFQUFvQyxjQUFwQztBQUNFO0FBREY7QUFERixhQU5GO0FBV0dGLDBCQUFjakYsV0FBVytFLE9BQU90RixFQUFoQyxHQUNDO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxrQkFBWSxjQUFXLFFBQXZCLEVBQWdDLFNBQVMsS0FBSzJFLGdCQUE5QztBQUNFO0FBREYsZUFERjtBQUlFO0FBQUE7QUFBQTtBQUNFLHdCQUFNLEtBQUtRLEtBQUwsQ0FBV1AsWUFEbkI7QUFFRSwyQkFBUyxLQUFLQyxpQkFGaEI7QUFHRSxpREFBMkJ6RCxRQUEzQjtBQUhGO0FBS0U7QUFBQTtBQUFBLG9CQUFhLGdCQUFjQSxRQUFkLG1CQUFiO0FBQUE7QUFBQSxpQkFMRjtBQVFFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxzQkFBUSxTQUFTLEtBQUt5RCxpQkFBdEIsRUFBeUMsT0FBTSxTQUEvQztBQUFBO0FBQUEsbUJBREY7QUFJRTtBQUFBO0FBQUE7QUFDRSwrQkFBUyxLQUFLQyxpQkFEaEI7QUFFRSw2QkFBTSxTQUZSO0FBR0U7QUFIRjtBQUFBO0FBQUE7QUFKRjtBQVJGO0FBSkYsYUFERCxHQTJCRyxJQXRDTjtBQXVDRSxtREFBSyxXQUFXMUMsUUFBUVIsUUFBeEIsR0F2Q0Y7QUF3Q0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsT0FBdkIsRUFBK0IsY0FBL0I7QUFDRTtBQURGO0FBeENGO0FBbERGO0FBTEYsT0FERjtBQXVHRDs7Ozs7QUFHSHNDLE9BQU96QixTQUFQLEdBQW1CO0FBQ2pCZ0MsV0FBUyxvQkFBVS9CLE1BQVYsQ0FBaUJDLFVBRFQ7O0FBR2pCUCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQUhUOztBQUtqQm9DLFlBQVUsb0JBQVVyQyxNQUFWLENBQWlCQyxVQUxWO0FBTWpCeUMsV0FBUyxvQkFBVTFDLE1BQVYsQ0FBaUJDLFVBTlQ7QUFPakJ2QixZQUFVLG9CQUFVMkIsTUFBVixDQUFpQkosVUFQVjs7QUFTakI0Qyx3QkFBc0Isb0JBQVUzQyxJQUFWLENBQWVELFVBVHBCO0FBVWpCdUMsc0JBQW9CLG9CQUFVdEMsSUFBVixDQUFlRDtBQVZsQixDQUFuQjs7QUFhQSxJQUFNaUQsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdiLFFBQUgsU0FBR0EsUUFBSDtBQUFBLE1BQWFLLE9BQWIsU0FBYUEsT0FBYjtBQUFBLFNBQTRCLEVBQUVMLGtCQUFGLEVBQVlLLGdCQUFaLEVBQTVCO0FBQUEsQ0FBeEI7QUFDQSxJQUFNUyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDNUYsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VzRiwyQ0FERjtBQUVFTDtBQUZGLEdBREYsRUFLRWpGLFFBTEYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBU2UseUJBQVEyRixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYiwwQkFBV3JFLE1BQVgsRUFBbUIsMEJBQVcwQyxNQUFYLENBQW5CLENBRGEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDclJmOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7QUFDQTs7OztBQUVBOztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBRUE7Ozs7OztBQXZCQTs7Ozs7O0FBeUJBLElBQU0xQyxTQUFTO0FBQ2JDLFFBQU07QUFDSk0sYUFBUztBQURMLEdBRE87QUFJYnlCLFFBQU07QUFDSjFCLGtCQUFjO0FBRFYsR0FKTztBQU9iMkIsVUFBUTtBQUNOM0Isa0JBQWM7QUFEUixHQVBLO0FBVWJ5QixjQUFZLEVBVkM7QUFXYjNCLFlBQVU7QUFDUnFDLFVBQU07QUFERTtBQVhHLENBQWY7O0lBZ0JNNkIsWTs7O0FBQ0osd0JBQVkzQixLQUFaLEVBQW1CO0FBQUE7O0FBQUEsa0pBQ1hBLEtBRFc7O0FBQUEsVUFnRW5CQyxZQWhFbUIsR0FnRUo7QUFBQSxhQUFNLE1BQUtDLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0FoRUk7O0FBQUEsVUFpRW5CQyxZQWpFbUIsR0FpRUo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0FqRUk7O0FBQUEsVUFrRW5CeUIsT0FsRW1CLEdBa0VUO0FBQUEsYUFBTSxNQUFLMUIsUUFBTCxDQUFjLEVBQUUyQixRQUFRLElBQVYsRUFBZCxDQUFOO0FBQUEsS0FsRVM7O0FBRWpCLFVBQUtiLEtBQUwsR0FBYTtBQUNYYSxjQUFRLEtBREc7QUFFWHhGLGFBQU8sRUFGSTtBQUdYeUYsbUJBQWEsQ0FIRjtBQUlYMUQsbUJBQWEsaUJBSkY7O0FBTVgyRCxhQUFPLEtBTkk7QUFPWC9GLGtCQUFZLENBUEQ7QUFRWEMsYUFBTyxFQVJJO0FBU1grRixlQUFTLEVBVEU7QUFVWDdCLGFBQU87QUFWSSxLQUFiOztBQWFBLFVBQUtqQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBYytELElBQWQsT0FBaEI7QUFDQSxVQUFLQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0QsSUFBZCxPQUFoQjtBQWhCaUI7QUFpQmxCOzs7OzZCQUVRRSxDLEVBQUc7QUFDVixXQUFLakMsUUFBTCxDQUFjLEVBQUU3RCxPQUFPOEYsRUFBRUMsTUFBRixDQUFTakUsS0FBbEIsRUFBZDtBQUNEOzs7OzJHQUVjZ0UsQzs7Ozs7Ozs7O0FBQ2JBLGtCQUFFRSxjQUFGO3VDQUNzQixLQUFLckMsS0FBTCxDQUFXWSxRQUFYLENBQW9CQyxJLEVBQWxDaEYsRSx3QkFBQUEsRSxFQUFJRCxLLHdCQUFBQSxLO0FBQ0pTLHFCLEdBQVUsS0FBSzJFLEssQ0FBZjNFLEs7QUFDRk0sb0IsR0FBTyxFQUFFTixZQUFGLEU7OztBQUViLHFCQUFLNkQsUUFBTCxDQUFjLEVBQUU4QixTQUFTLG9CQUFYLEVBQWQ7Ozt1QkFFNkMsc0JBQWdCO0FBQzNEcEcsOEJBRDJEO0FBRTNEUSwwQkFBUVAsRUFGbUQ7QUFHM0RRO0FBSDJELGlCQUFoQixDOzs7O0FBQXJDTCwwQixTQUFBQSxVO0FBQVlDLHFCLFNBQUFBLEs7QUFBT1gsdUIsU0FBQUEsTzs7c0JBTXZCVSxjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0EscUJBQUtrRSxRQUFMLENBQWMsRUFBRWxFLHNCQUFGLEVBQWNDLFlBQWQsRUFBZDs7Ozs7QUFJRixxQkFBS2lFLFFBQUwsQ0FBYyxFQUFFOEIsU0FBUyx1QkFBWCxFQUFkOztBQUVBTSwyQkFBVyxZQUFNO0FBQ2YseUJBQUtwQyxRQUFMLENBQWM7QUFDWjZCLDJCQUFPLEtBREs7QUFFWi9GLGdDQUFZLENBRkE7QUFHWkMsMkJBQU8sRUFISztBQUlaK0YsNkJBQVMsRUFKRzs7QUFNWjNGLDJCQUFPLEVBTks7QUFPWnlGLGlDQUFhO0FBUEQsbUJBQWQ7QUFTRCxpQkFWRCxFQVVHLElBVkg7O0FBWUEscUJBQUs5QixLQUFMLENBQVd1QyxrQkFBWDtBQUNFdkc7QUFERixtQkFFS1YsT0FGTCxFQUdLcUIsSUFITDs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQVdPO0FBQUEsbUJBQ3lDLEtBQUtxRSxLQUQ5QztBQUFBLFVBQ0NhLE1BREQsVUFDQ0EsTUFERDtBQUFBLFVBQ1N6RCxXQURULFVBQ1NBLFdBRFQ7QUFBQSxVQUNzQi9CLEtBRHRCLFVBQ3NCQSxLQUR0QjtBQUFBLFVBQzZCMkYsT0FEN0IsVUFDNkJBLE9BRDdCO0FBQUEsbUJBRXVCLEtBQUtoQyxLQUY1QjtBQUFBLFVBRUMvQixPQUZELFVBRUNBLE9BRkQ7QUFBQSxVQUVVMkMsUUFGVixVQUVVQSxRQUZWO0FBQUEsMkJBRzRCQSxTQUFTQyxJQUhyQztBQUFBLFVBR0M5QixJQUhELGtCQUdDQSxJQUhEO0FBQUEsVUFHT08sTUFIUCxrQkFHT0EsTUFIUDtBQUFBLFVBR2VKLFFBSGYsa0JBR2VBLFFBSGY7OztBQUtQLGFBQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQVdqQixRQUFRWCxJQURyQjtBQUVFLHdCQUFjLEtBQUsyQyxZQUZyQjtBQUdFLHdCQUFjLEtBQUtHO0FBSHJCO0FBS0U7QUFBQTtBQUFBLFlBQU0sUUFBUSxLQUFLWSxLQUFMLENBQVdiLEtBQXpCLEVBQWdDLFdBQVdsQyxRQUFRb0IsSUFBbkQ7QUFDRTtBQUNFLG9CQUNFO0FBQUE7QUFBQSxnQkFBTyxXQUFXcEIsUUFBUXFCLE1BQTFCLEVBQWtDLFdBQVcsQ0FBN0M7QUFDRTtBQUFBO0FBQUEsa0JBQU0sV0FBU0osUUFBZjtBQUNFO0FBQ0UsdUJBQUtILElBRFA7QUFFRSx1QkFBS08sVUFBVSw0Q0FGakI7QUFHRSw2QkFBV3JCLFFBQVFxQjtBQUhyQjtBQURGO0FBREYsYUFGSjtBQVlFLG1CQUNFO0FBQ0UscUJBQU9qRCxLQURUO0FBRUUsd0JBQVUsS0FBSzZCLFFBRmpCO0FBR0UsMkJBQWFFO0FBSGYsY0FiSjtBQW1CRSx1QkFBV0gsUUFBUW1CLFVBbkJyQjtBQW9CRSxxQkFBUyxLQUFLd0M7QUFwQmhCLFlBREY7QUF1QkdDLG9CQUFVRyxRQUFRUSxNQUFsQixHQUNDO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFhUjtBQUFiO0FBREYsV0FERCxHQUlHLElBM0JOO0FBNEJHSCxtQkFDQztBQUFBO0FBQUEsY0FBYSwwQkFBYjtBQUNFLG1EQUFLLFdBQVc1RCxRQUFRUixRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLGdCQUFRLFlBQVIsRUFBZSxPQUFNLFFBQXJCLEVBQThCLFNBQVMsS0FBS3lFLFFBQTVDO0FBQUE7QUFBQTtBQUZGLFdBREQsR0FPRztBQW5DTjtBQUxGLE9BREY7QUE2Q0Q7Ozs7O0FBR0hQLGFBQWFyRCxTQUFiLEdBQXlCO0FBQ3ZCTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQURIO0FBRXZCb0MsWUFBVSxvQkFBVXJDLE1BQVYsQ0FBaUJDLFVBRko7QUFHdkIrRCxzQkFBb0Isb0JBQVU5RCxJQUFWLENBQWVEO0FBSFosQ0FBekI7O0FBTUEsSUFBTWlELGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHYixRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQixFQUFFQSxrQkFBRixFQUFuQjtBQUFBLENBQXhCO0FBQ0EsSUFBTWMscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQzVGLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFeUc7QUFERixHQURGLEVBSUV6RyxRQUpGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVFlLHlCQUFRMkYsZUFBUixFQUF5QkMsa0JBQXpCLEVBQ2Isd0JBQVdyRSxNQUFYLEVBQW1Cc0UsWUFBbkIsQ0FEYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoTGY7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUVBOztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBWkE7O0FBY0EsSUFBTXRFLFNBQVM7QUFDYkMsUUFBTTtBQURPLENBQWY7O0lBSU1tRixVOzs7QUFDSixzQkFBWXpDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSw4SUFDWEEsS0FEVzs7QUFFakIsVUFBS2dCLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUVRO0FBQUEsbUJBQ3FDLEtBQUtoQixLQUQxQztBQUFBLFVBQ0MvQixPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVNEMsSUFEVixVQUNVQSxJQURWO0FBQUEsVUFDZ0I2QixPQURoQixVQUNnQkEsT0FEaEI7QUFBQSxVQUN5QnpCLE9BRHpCLFVBQ3lCQSxPQUR6QjtBQUFBLFVBRUNJLFVBRkQsR0FFb0JSLElBRnBCLENBRUNRLFVBRkQ7QUFBQSxVQUVheEYsRUFGYixHQUVvQmdGLElBRnBCLENBRWFoRixFQUZiOzs7QUFJUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdvQyxRQUFRWCxJQUF4QjtBQUNFO0FBQUE7QUFBQSxZQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDO0FBQ0U7QUFBQTtBQUFBLGNBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLENBQXZCLEVBQTBCLElBQUksQ0FBOUIsRUFBaUMsSUFBSSxDQUFyQyxFQUF3QyxJQUFJLENBQTVDO0FBQ0crRCwwQkFBY3hGLE9BQU82RyxRQUFRN0csRUFBN0IsR0FBa0MsMkRBQWxDLEdBQXFELElBRHhEO0FBRUUsZ0VBQVUsTUFBTSxTQUFoQixFQUEyQixTQUFTb0YsT0FBcEMsRUFBNkMsU0FBU3lCLE9BQXREO0FBRkY7QUFERjtBQURGLE9BREY7QUFVRDs7Ozs7QUFHSEQsV0FBV25FLFNBQVgsR0FBdUI7QUFDckJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBREw7QUFFckJxQyxRQUFNLG9CQUFVdEMsTUFBVixDQUFpQkMsVUFGRjtBQUdyQmtFLFdBQVMsb0JBQVVuRSxNQUFWLENBQWlCQyxVQUhMO0FBSXJCeUMsV0FBUyxvQkFBVTFDLE1BQVYsQ0FBaUJDO0FBSkwsQ0FBdkI7O0FBT0EsSUFBTWlELGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ1QsS0FBRDtBQUFBLFNBQVdBLEtBQVg7QUFBQSxDQUF4QjtBQUNBLElBQU1VLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUM1RixRQUFEO0FBQUEsU0FBYywrQkFBbUIsRUFBbkIsRUFBdUJBLFFBQXZCLENBQWQ7QUFBQSxDQUEzQjs7a0JBRWUseUJBQVEyRixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYix3QkFBV3JFLE1BQVgsRUFBbUJvRixVQUFuQixDQURhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVDZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQU1BOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUdBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTXBGLFNBQVMsU0FBVEEsTUFBUyxDQUFDc0YsS0FBRDtBQUFBLFNBQVk7QUFDekJyRixVQUFNO0FBQ0pNLGVBQVM7QUFETCxLQURtQjtBQUl6QnlCLFVBQU07QUFDSjFCLG9CQUFjO0FBRFYsS0FKbUI7QUFPekIyQixZQUFRO0FBQ04zQixvQkFBYztBQURSLEtBUGlCO0FBVXpCdEIsV0FBTztBQUNMO0FBQ0EyQyxjQUFRLFNBRkg7QUFHTEMsc0JBQWdCO0FBSFgsS0FWa0I7QUFlekIyRCxlQUFXO0FBQ1Q7QUFDQTVELGNBQVEsU0FGQztBQUdUQyxzQkFBZ0I7QUFIUCxLQWZjO0FBb0J6Qk8sV0FBTztBQUNMQyxjQUFRO0FBREgsS0FwQmtCO0FBdUJ6Qm9ELHdCQUFvQjtBQUNsQkMsYUFBTztBQURXLEtBdkJLO0FBMEJ6QnJGLGNBQVU7QUFDUnFDLFlBQU07QUFERSxLQTFCZTtBQTZCekJpRCxXQUFPO0FBQ0xuRCxxQkFBYStDLE1BQU1LLE9BQU4sQ0FBY0MsSUFBZCxHQUFxQixDQUFsQyxPQURLO0FBRUxILGFBQU87QUFGRixLQTdCa0I7QUFpQ3pCbEQsWUFBUTtBQUNObEMsY0FBUSxpQ0FERjtBQUVObUMsb0JBQWM7QUFGUixLQWpDaUI7QUFxQ3pCcUQsYUFBUztBQUNQQyxpQkFBVztBQURKO0FBckNnQixHQUFaO0FBQUEsQ0FBZjs7QUFOQTtBQXpDQTs7Ozs7OztJQXlGTUMsSTs7O0FBQ0osZ0JBQVlwRCxLQUFaLEVBQW1CO0FBQUE7O0FBQUE7O0FBQUEsa0lBQ1hBLEtBRFc7O0FBQUEsVUF1Qm5CcUQsZUF2Qm1CLDRFQXVCRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDUkMsc0JBRFEsR0FDSyxNQUFLdEMsS0FEVixDQUNSc0MsUUFEUTs7O0FBR2hCLG9CQUFLcEQsUUFBTCxDQUFjLEVBQUVvRCxVQUFVLENBQUNBLFFBQWIsRUFBZDs7QUFIZ0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0F2QkM7O0FBQUEsVUE2Qm5CckQsWUE3Qm1CLEdBNkJKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBN0JJOztBQUFBLFVBOEJuQkMsWUE5Qm1CLEdBOEJKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBOUJJOztBQUFBLFVBZ0NuQm9ELFdBaENtQixHQWdDTDtBQUFBLGFBQU0sTUFBS3ZELEtBQUwsQ0FBV00sT0FBWCxDQUFtQkMsSUFBbkIsYUFBa0MsTUFBS1AsS0FBTCxDQUFXNUMsTUFBN0MsQ0FBTjtBQUFBLEtBaENLOztBQUFBLFVBa0NuQm9ELGdCQWxDbUIsR0FrQ0EsWUFBTTtBQUN2QixZQUFLTixRQUFMLENBQWMsRUFBRU8sY0FBYyxJQUFoQixFQUFkO0FBQ0QsS0FwQ2tCOztBQUFBLFVBc0NuQkMsaUJBdENtQixHQXNDQyxZQUFNO0FBQ3hCLFlBQUtSLFFBQUwsQ0FBYyxFQUFFTyxjQUFjLEtBQWhCLEVBQWQ7QUFDRCxLQXhDa0I7O0FBQUEsVUEwQ25CK0MsZUExQ21CLDRFQTBDRDtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBQ00sTUFBS3hELEtBQUwsQ0FBV1ksUUFBWCxDQUFvQkMsSUFEMUIsRUFDUmpGLEtBRFEseUJBQ1JBLEtBRFEsRUFDREMsRUFEQyx5QkFDREEsRUFEQztBQUVSdUIsb0JBRlEsR0FFRyxNQUFLNEMsS0FGUixDQUVSNUMsTUFGUTtBQUFBO0FBQUEscUJBSW9CLHNCQUFjO0FBQ2hEeEIsNEJBRGdEO0FBRWhEUSx3QkFBUVAsRUFGd0M7QUFHaER1QjtBQUhnRCxlQUFkLENBSnBCOztBQUFBO0FBQUE7QUFJUnBCLHdCQUpRLFNBSVJBLFVBSlE7QUFJSUMsbUJBSkosU0FJSUEsS0FKSjs7QUFBQSxvQkFVWkQsZUFBZSxHQVZIO0FBQUE7QUFBQTtBQUFBOztBQVdkO0FBQ0FnQixzQkFBUThELEdBQVIsQ0FBWTdFLEtBQVo7QUFaYzs7QUFBQTs7QUFnQmhCLG9CQUFLK0QsS0FBTCxDQUFXeUQsZ0JBQVgsQ0FBNEIsRUFBRXJHLGNBQUYsRUFBNUI7QUFDQSxvQkFBSzhDLFFBQUwsQ0FBYyxFQUFFTyxjQUFjLEtBQWhCLEVBQWQ7O0FBakJnQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQTFDQzs7QUFFakIsVUFBS08sS0FBTCxHQUFhO0FBQ1hiLGFBQU8sS0FESTtBQUVYbUQsZ0JBQVUsS0FGQztBQUdYN0Msb0JBQWM7QUFISCxLQUFiO0FBRmlCO0FBT2xCOztBQUVEOzs7Ozt5Q0FDcUI7QUFBQSxtQkFDaUIsS0FBS1QsS0FEdEI7QUFBQSxVQUNYWSxRQURXLFVBQ1hBLFFBRFc7QUFBQSxVQUNEOEMsS0FEQyxVQUNEQSxLQURDO0FBQUEsVUFDTXRHLE1BRE4sVUFDTUEsTUFETjtBQUFBLDJCQUVXd0QsU0FBU0MsSUFGcEI7QUFBQSxVQUVYakYsS0FGVyxrQkFFWEEsS0FGVztBQUFBLFVBRUFRLE1BRkEsa0JBRUpQLEVBRkk7QUFBQSxVQUdYcUYsUUFIVyxHQUdFd0MsTUFBTXRHLE1BQU4sQ0FIRixDQUdYOEQsUUFIVzs7O0FBS25CLFVBQU1DLFNBQVNQLFNBQVNNLFFBQVQsQ0FBZjs7QUFFQTtBQUNBLFVBQUk5RSxXQUFXOEUsUUFBWCxJQUF1QixDQUFDQyxNQUE1QixFQUFvQztBQUNsQyxhQUFLbkIsS0FBTCxDQUFXMkQsY0FBWCxDQUEwQixFQUFFOUgsSUFBSXFGLFFBQU4sRUFBZ0J0RixZQUFoQixFQUExQjtBQUNEO0FBQ0Y7Ozs2QkF5Q1E7QUFBQSxvQkFDc0MsS0FBS29FLEtBRDNDO0FBQUEsVUFDQy9CLE9BREQsV0FDQ0EsT0FERDtBQUFBLFVBQ1V5RixLQURWLFdBQ1VBLEtBRFY7QUFBQSxVQUNpQjlDLFFBRGpCLFdBQ2lCQSxRQURqQjtBQUFBLFVBQzJCeEQsTUFEM0IsV0FDMkJBLE1BRDNCO0FBQUEsVUFFQ3lELElBRkQsR0FFVUQsUUFGVixDQUVDQyxJQUZEO0FBQUEsVUFHQ1EsVUFIRCxHQUc0QlIsSUFINUIsQ0FHQ1EsVUFIRDtBQUFBLFVBR2lCakYsTUFIakIsR0FHNEJ5RSxJQUg1QixDQUdhaEYsRUFIYjs7O0FBS1AsVUFBTXNELE9BQU91RSxNQUFNdEcsTUFBTixDQUFiOztBQUxPLFVBT0NrRyxRQVBELEdBT2MsS0FBS3RDLEtBUG5CLENBT0NzQyxRQVBEOzs7QUFTUCxVQUFJLE9BQU9uRSxJQUFQLEtBQWdCLFdBQXBCLEVBQWlDO0FBQy9CLGVBQU8sMENBQVA7QUFDRDs7QUFYTSxVQWFDcUMsS0FiRCxHQWF5Q3JDLElBYnpDLENBYUNxQyxLQWJEO0FBQUEsVUFhUW5GLEtBYlIsR0FheUM4QyxJQWJ6QyxDQWFROUMsS0FiUjtBQUFBLFVBYWVrRixXQWJmLEdBYXlDcEMsSUFiekMsQ0FhZW9DLFdBYmY7QUFBQSxVQWE0QkwsUUFiNUIsR0FheUMvQixJQWJ6QyxDQWE0QitCLFFBYjVCOztBQWNQLFVBQU1DLFNBQVNELGFBQWE5RSxNQUFiLEdBQXNCeUUsSUFBdEIsR0FBNkJELFNBQVNNLFFBQVQsQ0FBNUM7O0FBRUEsYUFDRTtBQUFBO0FBQUE7QUFDRSxxQkFBV2pELFFBQVFYLElBRHJCO0FBRUUsd0JBQWMsS0FBSzJDLFlBRnJCO0FBR0Usd0JBQWMsS0FBS0c7QUFIckI7QUFLRTtBQUFBO0FBQUEsWUFBTSxRQUFRLEtBQUtZLEtBQUwsQ0FBV2IsS0FBekIsRUFBZ0MsV0FBV2xDLFFBQVFvQixJQUFuRDtBQUNFO0FBQ0Usb0JBQ0U7QUFBQTtBQUFBLGdCQUFPLFdBQVdwQixRQUFRcUIsTUFBMUIsRUFBa0MsV0FBVyxDQUE3QztBQUNFO0FBQUE7QUFBQSxrQkFBTSxXQUFTNkIsT0FBT2pDLFFBQXRCO0FBQ0U7QUFDRSx1QkFBS2lDLE9BQU9wQyxJQURkO0FBRUUsdUJBQUs7QUFGUDtBQURGO0FBREYsYUFGSjtBQVdFLG9CQUNFO0FBQUE7QUFBQSxnQkFBUyxPQUFNLGFBQWYsRUFBNkIsV0FBVSxRQUF2QztBQUNFO0FBQUE7QUFBQSxrQkFBWSxjQUFaO0FBQ0U7QUFERjtBQURGLGFBWko7QUFrQkUsbUJBQ0U7QUFBQTtBQUFBLGdCQUFNLFdBQVNvQyxPQUFPakMsUUFBdEIsRUFBa0MsV0FBV2pCLFFBQVE1QixLQUFyRDtBQUNHOEUscUJBQU9wQztBQURWLGFBbkJKO0FBdUJFLHVCQUNFO0FBQUE7QUFBQSxnQkFBTSxXQUFTb0MsT0FBT2pDLFFBQXRCLEVBQWtDLFdBQVdqQixRQUFRMkUsU0FBckQ7QUFBQTtBQUNJekIscUJBQU9qQztBQURYO0FBeEJKLFlBREY7QUE4QkU7QUFDRSx1QkFBV2pCLFFBQVF1QixLQURyQjtBQUVFLG1CQUNFZ0MsU0FDQSwyRUFKSjtBQU1FLG1CQUFPbkY7QUFOVCxZQTlCRjtBQXNDRTtBQUFBO0FBQUEsY0FBYSxTQUFTLEtBQUtrSCxXQUEzQjtBQUNFO0FBQUE7QUFBQSxnQkFBWSxNQUFLLE9BQWpCO0FBQTBCbEg7QUFBMUIsYUFERjtBQUVHLG1CQUFPa0YsV0FBUCxLQUF1QixRQUF2QixHQUNDO0FBQUE7QUFBQSxnQkFBWSxNQUFLLE9BQWpCLEVBQXlCLGtCQUF6QixFQUFzQyxXQUFXdEQsUUFBUWlGLE9BQXpEO0FBQ0czQjtBQURILGFBREQsR0FJRztBQU5OLFdBdENGO0FBK0NFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxnQkFBWSxTQUFTLEtBQUs4QixlQUExQjtBQUNFO0FBQUE7QUFBQSxrQkFBTyxjQUFjLENBQXJCO0FBQ0U7QUFDRSw2QkFBVyxpRUFDTHBGLFFBQVE0RSxrQkFESCxFQUMwQlMsUUFEMUI7QUFEYjtBQURGO0FBREYsYUFERjtBQVVFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLFNBQXZCO0FBQ0U7QUFBQTtBQUFBLGtCQUFPLGNBQWMsQ0FBckI7QUFDRTtBQURGO0FBREYsYUFWRjtBQWVFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLFlBQXZCO0FBQ0U7QUFERixhQWZGO0FBa0JHakMsMEJBQWNqRixXQUFXOEUsUUFBekIsR0FDQztBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsa0JBQVksY0FBVyxRQUF2QixFQUFnQyxTQUFTLEtBQUtWLGdCQUE5QztBQUNFO0FBREYsZUFERjtBQUlFO0FBQUE7QUFBQTtBQUNFLHdCQUFNLEtBQUtRLEtBQUwsQ0FBV1AsWUFEbkI7QUFFRSwyQkFBUyxLQUFLQyxpQkFGaEI7QUFHRSwrQ0FBeUJ0RCxNQUF6QjtBQUhGO0FBS0U7QUFBQTtBQUFBLG9CQUFhLGNBQVlBLE1BQVosbUJBQWI7QUFBQTtBQUFBLGlCQUxGO0FBUUU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLHNCQUFRLFNBQVMsS0FBS3NELGlCQUF0QixFQUF5QyxPQUFNLFNBQS9DO0FBQUE7QUFBQSxtQkFERjtBQUlFO0FBQUE7QUFBQTtBQUNFLCtCQUFTLEtBQUs4QyxlQURoQjtBQUVFLDZCQUFNLFNBRlI7QUFHRTtBQUhGO0FBQUE7QUFBQTtBQUpGO0FBUkY7QUFKRixhQURELEdBMkJHLElBN0NOO0FBOENFLG1EQUFLLFdBQVd2RixRQUFRUixRQUF4QixHQTlDRjtBQStDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QjtBQUNFO0FBQUE7QUFBQSxrQkFBTyxjQUFjLENBQXJCO0FBQ0U7QUFERjtBQURGLGFBL0NGO0FBb0RFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE9BQXZCLEVBQStCLGNBQS9CO0FBQ0U7QUFERjtBQXBERjtBQS9DRjtBQUxGLE9BREY7QUFnSEQ7Ozs7O0FBR0gyRixLQUFLOUUsU0FBTCxHQUFpQjtBQUNmZ0MsV0FBUyxvQkFBVS9CLE1BQVYsQ0FBaUJDLFVBRFg7O0FBR2ZQLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBSFg7O0FBS2ZvQyxZQUFVLG9CQUFVckMsTUFBVixDQUFpQkMsVUFMWjtBQU1ma0YsU0FBTyxvQkFBVW5GLE1BQVYsQ0FBaUJDLFVBTlQ7O0FBUWY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBcEIsVUFBUSxvQkFBVXdCLE1BQVYsQ0FBaUJKLFVBZlY7O0FBaUJmbUYsa0JBQWdCLG9CQUFVbEYsSUFBVixDQUFlRCxVQWpCaEI7QUFrQmZpRixvQkFBa0Isb0JBQVVoRixJQUFWLENBQWVEO0FBbEJsQixDQUFqQjs7QUFxQkEsSUFBTWlELGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHYixRQUFILFNBQUdBLFFBQUg7QUFBQSxNQUFhOEMsS0FBYixTQUFhQSxLQUFiO0FBQUEsU0FBMEIsRUFBRTlDLGtCQUFGLEVBQVk4QyxZQUFaLEVBQTFCO0FBQUEsQ0FBeEI7O0FBRUEsSUFBTWhDLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUM1RixRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRTZILHFDQURGO0FBRUVGO0FBRkYsR0FERixFQUtFM0gsUUFMRixDQUR5QjtBQUFBLENBQTNCOztrQkFTZSx5QkFBUTJGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXckUsTUFBWCxFQUFtQiwwQkFBVytGLElBQVgsQ0FBbkIsQ0FEYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwVGY7Ozs7QUFDQTs7OztBQUVBOztBQUVBOzs7O0FBQ0E7Ozs7OztBQUVBLElBQU0vRixTQUFTO0FBQ2JDLFFBQU07QUFDSkcsY0FBVTtBQUROO0FBRE8sQ0FBZixDLENBZkE7Ozs7Ozs7SUFxQk1tRyxROzs7QUFDSixvQkFBWTVELEtBQVosRUFBbUI7QUFBQTs7QUFBQSwwSUFDWEEsS0FEVzs7QUFFakIsVUFBS2dCLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUNRO0FBQUEsbUJBQzRDLEtBQUtoQixLQURqRDtBQUFBLFVBQ0MvQixPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVMUMsSUFEVixVQUNVQSxJQURWO0FBQUEsVUFDZ0JtSSxLQURoQixVQUNnQkEsS0FEaEI7QUFBQSxVQUN1QnpDLE9BRHZCLFVBQ3VCQSxPQUR2QjtBQUFBLFVBQ2dDeUIsT0FEaEMsVUFDZ0NBLE9BRGhDOzs7QUFHUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVd6RSxRQUFRWCxJQUF4QjtBQUNHL0IsaUJBQVMsT0FBVCxHQUNHbUgsUUFBUWdCLEtBQVIsQ0FBY0csR0FBZCxDQUFrQixVQUFDQyxDQUFEO0FBQUEsaUJBQU8sZ0RBQU0sS0FBS0EsRUFBRTFHLE1BQWIsRUFBcUIsUUFBUTBHLEVBQUUxRyxNQUEvQixHQUFQO0FBQUEsU0FBbEIsQ0FESCxHQUVHc0YsUUFBUXpCLE9BQVIsQ0FBZ0I0QyxHQUFoQixDQUFvQixVQUFDRSxDQUFEO0FBQUEsaUJBQ3BCLGtEQUFRLEtBQUtBLEVBQUU5RyxRQUFmLEVBQXlCLFVBQVU4RyxFQUFFOUcsUUFBckMsR0FEb0I7QUFBQSxTQUFwQjtBQUhOLE9BREY7QUFTRDs7Ozs7QUFHSDJHLFNBQVN0RixTQUFULEdBQXFCO0FBQ25CTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQURQO0FBRW5CakQsUUFBTSxvQkFBVW9ELE1BQVYsQ0FBaUJILFVBRkosRUFFZ0I7QUFDbkNrRSxXQUFTLG9CQUFVbkUsTUFBVixDQUFpQkMsVUFIUDtBQUluQmtGLFNBQU8sb0JBQVVuRixNQUpFO0FBS25CMEMsV0FBUyxvQkFBVTFDO0FBTEEsQ0FBckI7O2tCQVFlLHdCQUFXbEIsTUFBWCxFQUFtQnVHLFFBQW5CLEMiLCJmaWxlIjoiMjguYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gYWN0aXZlRWxlbWVudDtcblxudmFyIF9vd25lckRvY3VtZW50ID0gcmVxdWlyZSgnLi9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gYWN0aXZlRWxlbWVudCgpIHtcbiAgdmFyIGRvYyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KSgpO1xuXG4gIHRyeSB7XG4gICAgcmV0dXJuIGRvYy5hY3RpdmVFbGVtZW50O1xuICB9IGNhdGNoIChlKSB7LyogaWUgdGhyb3dzIGlmIG5vIGFjdGl2ZSBlbGVtZW50ICovfVxufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCJcInVzZSBzdHJpY3RcIjtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuZGVmYXVsdCA9IG93bmVyRG9jdW1lbnQ7XG5mdW5jdGlvbiBvd25lckRvY3VtZW50KG5vZGUpIHtcbiAgcmV0dXJuIG5vZGUgJiYgbm9kZS5vd25lckRvY3VtZW50IHx8IGRvY3VtZW50O1xufVxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzW1wiZGVmYXVsdFwiXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9vd25lckRvY3VtZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9vd25lckRvY3VtZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIlwidXNlIHN0cmljdFwiO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0V2luZG93O1xuZnVuY3Rpb24gZ2V0V2luZG93KG5vZGUpIHtcbiAgcmV0dXJuIG5vZGUgPT09IG5vZGUud2luZG93ID8gbm9kZSA6IG5vZGUubm9kZVR5cGUgPT09IDkgPyBub2RlLmRlZmF1bHRWaWV3IHx8IG5vZGUucGFyZW50V2luZG93IDogZmFsc2U7XG59XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbXCJkZWZhdWx0XCJdO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3F1ZXJ5L2lzV2luZG93LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9xdWVyeS9pc1dpbmRvdy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGZ1bmN0aW9uIChyZWNhbGMpIHtcbiAgaWYgKCFzaXplICYmIHNpemUgIT09IDAgfHwgcmVjYWxjKSB7XG4gICAgaWYgKF9pbkRPTTIuZGVmYXVsdCkge1xuICAgICAgdmFyIHNjcm9sbERpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuXG4gICAgICBzY3JvbGxEaXYuc3R5bGUucG9zaXRpb24gPSAnYWJzb2x1dGUnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLnRvcCA9ICctOTk5OXB4JztcbiAgICAgIHNjcm9sbERpdi5zdHlsZS53aWR0aCA9ICc1MHB4JztcbiAgICAgIHNjcm9sbERpdi5zdHlsZS5oZWlnaHQgPSAnNTBweCc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUub3ZlcmZsb3cgPSAnc2Nyb2xsJztcblxuICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChzY3JvbGxEaXYpO1xuICAgICAgc2l6ZSA9IHNjcm9sbERpdi5vZmZzZXRXaWR0aCAtIHNjcm9sbERpdi5jbGllbnRXaWR0aDtcbiAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoc2Nyb2xsRGl2KTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gc2l6ZTtcbn07XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCcuL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2l6ZSA9IHZvaWQgMDtcblxubW9kdWxlLmV4cG9ydHMgPSBleHBvcnRzWydkZWZhdWx0J107XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvdXRpbC9zY3JvbGxiYXJTaXplLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy91dGlsL3Njcm9sbGJhclNpemUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjEuOTkgNGMwLTEuMS0uODktMi0xLjk5LTJINGMtMS4xIDAtMiAuOS0yIDJ2MTJjMCAxLjEuOSAyIDIgMmgxNGw0IDQtLjAxLTE4ek0xOCAxNEg2di0yaDEydjJ6bTAtM0g2VjloMTJ2MnptMC0zSDZWNmgxMnYyeicgfSk7XG5cbnZhciBDb21tZW50ID0gZnVuY3Rpb24gQ29tbWVudChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQ29tbWVudCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQ29tbWVudCk7XG5Db21tZW50Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvbW1lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNiAxOWMwIDEuMS45IDIgMiAyaDhjMS4xIDAgMi0uOSAyLTJWN0g2djEyek0xOSA0aC0zLjVsLTEtMWgtNWwtMSAxSDV2MmgxNFY0eicgfSk7XG5cbnZhciBEZWxldGUgPSBmdW5jdGlvbiBEZWxldGUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkRlbGV0ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRGVsZXRlKTtcbkRlbGV0ZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBEZWxldGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9EZWxldGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNiAyNyAyOCAzMiAzNSAzNyAzOCA0MCA0MSA0MiA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDIxLjM1bC0xLjQ1LTEuMzJDNS40IDE1LjM2IDIgMTIuMjggMiA4LjUgMiA1LjQyIDQuNDIgMyA3LjUgM2MxLjc0IDAgMy40MS44MSA0LjUgMi4wOUMxMy4wOSAzLjgxIDE0Ljc2IDMgMTYuNSAzIDE5LjU4IDMgMjIgNS40MiAyMiA4LjVjMCAzLjc4LTMuNCA2Ljg2LTguNTUgMTEuNTRMMTIgMjEuMzV6JyB9KTtcblxudmFyIEZhdm9yaXRlID0gZnVuY3Rpb24gRmF2b3JpdGUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZhdm9yaXRlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGYXZvcml0ZSk7XG5GYXZvcml0ZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGYXZvcml0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MCA0MSA0MiA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDhjMS4xIDAgMi0uOSAyLTJzLS45LTItMi0yLTIgLjktMiAyIC45IDIgMiAyem0wIDJjLTEuMSAwLTIgLjktMiAycy45IDIgMiAyIDItLjkgMi0yLS45LTItMi0yem0wIDZjLTEuMSAwLTIgLjktMiAycy45IDIgMiAyIDItLjkgMi0yLS45LTItMi0yeicgfSk7XG5cbnZhciBNb3JlVmVydCA9IGZ1bmN0aW9uIE1vcmVWZXJ0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Nb3JlVmVydCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoTW9yZVZlcnQpO1xuTW9yZVZlcnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gTW9yZVZlcnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAyNyAyOCAzMSAzNCAzNSAzNyA0MCA0MSA0MiA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDQuNUM3IDQuNSAyLjczIDcuNjEgMSAxMmMxLjczIDQuMzkgNiA3LjUgMTEgNy41czkuMjctMy4xMSAxMS03LjVjLTEuNzMtNC4zOS02LTcuNS0xMS03LjV6TTEyIDE3Yy0yLjc2IDAtNS0yLjI0LTUtNXMyLjI0LTUgNS01IDUgMi4yNCA1IDUtMi4yNCA1LTUgNXptMC04Yy0xLjY2IDAtMyAxLjM0LTMgM3MxLjM0IDMgMyAzIDMtMS4zNCAzLTMtMS4zNC0zLTMtM3onIH0pO1xuXG52YXIgUmVtb3ZlUmVkRXllID0gZnVuY3Rpb24gUmVtb3ZlUmVkRXllKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5SZW1vdmVSZWRFeWUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFJlbW92ZVJlZEV5ZSk7XG5SZW1vdmVSZWRFeWUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gUmVtb3ZlUmVkRXllO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1JlbW92ZVJlZEV5ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvUmVtb3ZlUmVkRXllLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzUgMzcgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xOCAxNi4wOGMtLjc2IDAtMS40NC4zLTEuOTYuNzdMOC45MSAxMi43Yy4wNS0uMjMuMDktLjQ2LjA5LS43cy0uMDQtLjQ3LS4wOS0uN2w3LjA1LTQuMTFjLjU0LjUgMS4yNS44MSAyLjA0LjgxIDEuNjYgMCAzLTEuMzQgMy0zcy0xLjM0LTMtMy0zLTMgMS4zNC0zIDNjMCAuMjQuMDQuNDcuMDkuN0w4LjA0IDkuODFDNy41IDkuMzEgNi43OSA5IDYgOWMtMS42NiAwLTMgMS4zNC0zIDNzMS4zNCAzIDMgM2MuNzkgMCAxLjUtLjMxIDIuMDQtLjgxbDcuMTIgNC4xNmMtLjA1LjIxLS4wOC40My0uMDguNjUgMCAxLjYxIDEuMzEgMi45MiAyLjkyIDIuOTIgMS42MSAwIDIuOTItMS4zMSAyLjkyLTIuOTJzLTEuMzEtMi45Mi0yLjkyLTIuOTJ6JyB9KTtcblxudmFyIFNoYXJlID0gZnVuY3Rpb24gU2hhcmUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblNoYXJlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTaGFyZSk7XG5TaGFyZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTaGFyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TaGFyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MCA0MSA0MiA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE3IDNIN2MtMS4xIDAtMS45OS45LTEuOTkgMkw1IDIxbDctMyA3IDNWNWMwLTEuMS0uOS0yLTItMnonIH0pO1xuXG52YXIgVHVybmVkSW4gPSBmdW5jdGlvbiBUdXJuZWRJbihwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuVHVybmVkSW4gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFR1cm5lZEluKTtcblR1cm5lZEluLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFR1cm5lZEluO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UdXJuZWRJbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQwIDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9jb2xvck1hbmlwdWxhdG9yID0gcmVxdWlyZSgnLi4vc3R5bGVzL2NvbG9yTWFuaXB1bGF0b3InKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgZmxleFNocmluazogMCxcbiAgICAgIHdpZHRoOiA0MCxcbiAgICAgIGhlaWdodDogNDAsXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDIwKSxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZSdcbiAgICB9LFxuICAgIGNvbG9yRGVmYXVsdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LFxuICAgICAgYmFja2dyb3VuZENvbG9yOiAoMCwgX2NvbG9yTWFuaXB1bGF0b3IuZW1waGFzaXplKSh0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCwgMC4yNilcbiAgICB9LFxuICAgIGltZzoge1xuICAgICAgbWF4V2lkdGg6ICcxMDAlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VkIGluIGNvbWJpbmF0aW9uIHdpdGggYHNyY2Agb3IgYHNyY1NldGAgdG9cbiAgICogcHJvdmlkZSBhbiBhbHQgYXR0cmlidXRlIGZvciB0aGUgcmVuZGVyZWQgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIGFsdDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZCB0byByZW5kZXIgaWNvbiBvciB0ZXh0IGVsZW1lbnRzIGluc2lkZSB0aGUgQXZhdGFyLlxuICAgKiBgc3JjYCBhbmQgYGFsdGAgcHJvcHMgd2lsbCBub3QgYmUgdXNlZCBhbmQgbm8gYGltZ2Agd2lsbFxuICAgKiBiZSByZW5kZXJlZCBieSBkZWZhdWx0LlxuICAgKlxuICAgKiBUaGlzIGNhbiBiZSBhbiBlbGVtZW50LCBvciBqdXN0IGEgc3RyaW5nLlxuICAgKi9cbiAgY2hpbGRyZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIFRoZSBjbGFzc05hbWUgb2YgdGhlIGNoaWxkIGVsZW1lbnQuXG4gICAqIFVzZWQgYnkgQ2hpcCBhbmQgTGlzdEl0ZW1JY29uIHRvIHN0eWxlIHRoZSBBdmF0YXIgaWNvbi5cbiAgICovXG4gIGNoaWxkcmVuQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBpbWdgIGVsZW1lbnQgd2hlbiB0aGUgY29tcG9uZW50XG4gICAqIGlzIHVzZWQgdG8gZGlzcGxheSBhbiBpbWFnZS5cbiAgICovXG4gIGltZ1Byb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgYHNpemVzYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc2l6ZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBgc3JjYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc3JjOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY1NldGAgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyY1NldDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxudmFyIEF2YXRhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEF2YXRhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQXZhdGFyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEF2YXRhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEF2YXRhci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQXZhdGFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShBdmF0YXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYWx0ID0gX3Byb3BzLmFsdCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jaGlsZHJlbkNsYXNzTmFtZSxcbiAgICAgICAgICBDb21wb25lbnRQcm9wID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBpbWdQcm9wcyA9IF9wcm9wcy5pbWdQcm9wcyxcbiAgICAgICAgICBzaXplcyA9IF9wcm9wcy5zaXplcyxcbiAgICAgICAgICBzcmMgPSBfcHJvcHMuc3JjLFxuICAgICAgICAgIHNyY1NldCA9IF9wcm9wcy5zcmNTZXQsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYWx0JywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NoaWxkcmVuJywgJ2NoaWxkcmVuQ2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdpbWdQcm9wcycsICdzaXplcycsICdzcmMnLCAnc3JjU2V0J10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuY29sb3JEZWZhdWx0LCBjaGlsZHJlblByb3AgJiYgIXNyYyAmJiAhc3JjU2V0KSwgY2xhc3NOYW1lUHJvcCk7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBudWxsO1xuXG4gICAgICBpZiAoY2hpbGRyZW5Qcm9wKSB7XG4gICAgICAgIGlmIChjaGlsZHJlbkNsYXNzTmFtZVByb3AgJiYgdHlwZW9mIGNoaWxkcmVuUHJvcCAhPT0gJ3N0cmluZycgJiYgX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkcmVuUHJvcCkpIHtcbiAgICAgICAgICB2YXIgX2NoaWxkcmVuQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjaGlsZHJlbkNsYXNzTmFtZVByb3AsIGNoaWxkcmVuUHJvcC5wcm9wcy5jbGFzc05hbWUpO1xuICAgICAgICAgIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZHJlblByb3AsIHsgY2xhc3NOYW1lOiBfY2hpbGRyZW5DbGFzc05hbWUgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlblByb3A7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoc3JjIHx8IHNyY1NldCkge1xuICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdpbWcnLCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhbHQ6IGFsdCxcbiAgICAgICAgICBzcmM6IHNyYyxcbiAgICAgICAgICBzcmNTZXQ6IHNyY1NldCxcbiAgICAgICAgICBzaXplczogc2l6ZXMsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc2VzLmltZ1xuICAgICAgICB9LCBpbWdQcm9wcykpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIENvbXBvbmVudFByb3AsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQXZhdGFyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQXZhdGFyLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29tcG9uZW50OiAnZGl2J1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlBdmF0YXInIH0pKEF2YXRhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNCA2IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU0IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0F2YXRhciA9IHJlcXVpcmUoJy4vQXZhdGFyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0F2YXRhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgMjQgMjYgMjcgMjggMzEgMzMgMzQgMzUgMzYgMzcgMzkgNDAgNDMgNDkgNTcgNTggNTkgNjEgNjMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7IC8vICB3ZWFrXG5cbnZhciBSQURJVVMgPSAxMjtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWZsZXgnXG4gICAgfSxcbiAgICBiYWRnZToge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgZmxleERpcmVjdGlvbjogJ3JvdycsXG4gICAgICBmbGV4V3JhcDogJ3dyYXAnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgYWxpZ25Db250ZW50OiAnY2VudGVyJyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICB0b3A6IC1SQURJVVMsXG4gICAgICByaWdodDogLVJBRElVUyxcbiAgICAgIGZvbnRGYW1pbHk6IHRoZW1lLnR5cG9ncmFwaHkuZm9udEZhbWlseSxcbiAgICAgIGZvbnRXZWlnaHQ6IHRoZW1lLnR5cG9ncmFwaHkuZm9udFdlaWdodCxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oUkFESVVTKSxcbiAgICAgIHdpZHRoOiBSQURJVVMgKiAyLFxuICAgICAgaGVpZ2h0OiBSQURJVVMgKiAyLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb2xvcixcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHRDb2xvcixcbiAgICAgIHpJbmRleDogMSAvLyBSZW5kZXIgdGhlIGJhZGdlIG9uIHRvcCBvZiBwb3RlbnRpYWwgcmlwcGxlcy5cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDAsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMClcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCByZW5kZXJlZCB3aXRoaW4gdGhlIGJhZGdlLlxuICAgKi9cbiAgYmFkZ2VDb250ZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGJhZGdlIHdpbGwgYmUgYWRkZWQgcmVsYXRpdmUgdG8gdGhpcyBub2RlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydkZWZhdWx0JywgJ3ByaW1hcnknLCAnYWNjZW50J10pLmlzUmVxdWlyZWRcbn07XG5cbnZhciBCYWRnZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEJhZGdlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBCYWRnZSgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBCYWRnZSk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEJhZGdlLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShCYWRnZSkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQmFkZ2UsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYmFkZ2VDb250ZW50ID0gX3Byb3BzLmJhZGdlQ29udGVudCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYmFkZ2VDb250ZW50JywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2NoaWxkcmVuJ10pO1xuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIGJhZGdlQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmJhZGdlLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdkZWZhdWx0JykpO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlbixcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBiYWRnZUNsYXNzTmFtZSB9LFxuICAgICAgICAgIGJhZGdlQ29udGVudFxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQmFkZ2U7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5CYWRnZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnZGVmYXVsdCdcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQmFkZ2UnIH0pKEJhZGdlKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9CYWRnZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvQmFkZ2UuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9CYWRnZSA9IHJlcXVpcmUoJy4vQmFkZ2UnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQmFkZ2UpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX01vZGFsID0gcmVxdWlyZSgnLi4vTW9kYWwnKTtcblxudmFyIF9Nb2RhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Nb2RhbCk7XG5cbnZhciBfRmFkZSA9IHJlcXVpcmUoJy4uL3RyYW5zaXRpb25zL0ZhZGUnKTtcblxudmFyIF9GYWRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ZhZGUpO1xuXG52YXIgX3RyYW5zaXRpb25zID0gcmVxdWlyZSgnLi4vc3R5bGVzL3RyYW5zaXRpb25zJyk7XG5cbnZhciBfUGFwZXIgPSByZXF1aXJlKCcuLi9QYXBlcicpO1xuXG52YXIgX1BhcGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BhcGVyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgTW9kYWxcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgYWxpZ25JdGVtczogJ2NlbnRlcidcbiAgICB9LFxuICAgIHBhcGVyOiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBtYXJnaW46IHRoZW1lLnNwYWNpbmcudW5pdCAqIDQsXG4gICAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJyxcbiAgICAgIGZsZXg6ICcwIDEgYXV0bycsXG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIG1heEhlaWdodDogJzkwdmgnLFxuICAgICAgb3ZlcmZsb3dZOiAnYXV0bycsIC8vIEZpeCBJRTExIGlzc3VlLCB0byByZW1vdmUgYXQgc29tZSBwb2ludC5cbiAgICAgICcmOmZvY3VzJzoge1xuICAgICAgICBvdXRsaW5lOiAnbm9uZSdcbiAgICAgIH1cbiAgICB9LFxuICAgIHBhcGVyV2lkdGhYczoge1xuICAgICAgbWF4V2lkdGg6IE1hdGgubWF4KHRoZW1lLmJyZWFrcG9pbnRzLnZhbHVlcy54cywgMzYwKVxuICAgIH0sXG4gICAgcGFwZXJXaWR0aFNtOiB7XG4gICAgICBtYXhXaWR0aDogdGhlbWUuYnJlYWtwb2ludHMudmFsdWVzLnNtXG4gICAgfSxcbiAgICBwYXBlcldpZHRoTWQ6IHtcbiAgICAgIG1heFdpZHRoOiB0aGVtZS5icmVha3BvaW50cy52YWx1ZXMubWRcbiAgICB9LFxuICAgIGZ1bGxXaWR0aDoge1xuICAgICAgd2lkdGg6ICcxMDAlJ1xuICAgIH0sXG4gICAgZnVsbFNjcmVlbjoge1xuICAgICAgbWFyZ2luOiAwLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIG1heFdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIG1heEhlaWdodDogJzEwMCUnLFxuICAgICAgYm9yZGVyUmFkaXVzOiAwXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRGlhbG9nIGNoaWxkcmVuLCB1c3VhbGx5IHRoZSBpbmNsdWRlZCBzdWItY29tcG9uZW50cy5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBpdCB3aWxsIGJlIGZ1bGwtc2NyZWVuXG4gICAqL1xuICBmdWxsU2NyZWVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGNsaWNraW5nIHRoZSBiYWNrZHJvcCB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlQmFja2Ryb3BDbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBoaXR0aW5nIGVzY2FwZSB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIERldGVybWluZSB0aGUgbWF4IHdpZHRoIG9mIHRoZSBkaWFsb2cuXG4gICAqIFRoZSBkaWFsb2cgd2lkdGggZ3Jvd3Mgd2l0aCB0aGUgc2l6ZSBvZiB0aGUgc2NyZWVuLCB0aGlzIHByb3BlcnR5IGlzIHVzZWZ1bFxuICAgKiBvbiB0aGUgZGVza3RvcCB3aGVyZSB5b3UgbWlnaHQgbmVlZCBzb21lIGNvaGVyZW50IGRpZmZlcmVudCB3aWR0aCBzaXplIGFjcm9zcyB5b3VyXG4gICAqIGFwcGxpY2F0aW9uLlxuICAgKi9cbiAgbWF4V2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3hzJywgJ3NtJywgJ21kJ10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIHNwZWNpZmllZCwgc3RyZXRjaGVzIGRpYWxvZyB0byBtYXggd2lkdGguXG4gICAqL1xuICBmdWxsV2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGJhY2tkcm9wIGlzIGNsaWNrZWQuXG4gICAqL1xuICBvbkJhY2tkcm9wQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIGRpYWxvZyBlbnRlcnMuXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGRpYWxvZyBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgZGlhbG9nIGhhcyBlbnRlcmVkLlxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVzIHdoZW4gdGhlIGVzY2FwZSBrZXkgaXMgcmVsZWFzZWQgYW5kIHRoZSBtb2RhbCBpcyBpbiBmb2N1cy5cbiAgICovXG4gIG9uRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIGRpYWxvZyBleGl0cy5cbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBkaWFsb2cgaXMgZXhpdGluZy5cbiAgICovXG4gIG9uRXhpdGluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBkaWFsb2cgaGFzIGV4aXRlZC5cbiAgICovXG4gIG9uRXhpdGVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCByZXF1ZXN0cyB0byBiZSBjbG9zZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKi9cbiAgb25SZXF1ZXN0Q2xvc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBEaWFsb2cgaXMgb3Blbi5cbiAgICovXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRyYW5zaXRpb24gY29tcG9uZW50LlxuICAgKi9cbiAgdHJhbnNpdGlvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUpLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogRGlhbG9ncyBhcmUgb3ZlcmxhaWQgbW9kYWwgcGFwZXIgYmFzZWQgY29tcG9uZW50cyB3aXRoIGEgYmFja2Ryb3AuXG4gKi9cbnZhciBEaWFsb2cgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShEaWFsb2csIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERpYWxvZygpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBEaWFsb2cpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChEaWFsb2cuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKERpYWxvZykpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoRGlhbG9nLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZnVsbFNjcmVlbiA9IF9wcm9wcy5mdWxsU2NyZWVuLFxuICAgICAgICAgIGlnbm9yZUJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMuaWdub3JlQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcCA9IF9wcm9wcy5pZ25vcmVFc2NhcGVLZXlVcCxcbiAgICAgICAgICB0cmFuc2l0aW9uRHVyYXRpb24gPSBfcHJvcHMudHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgIG1heFdpZHRoID0gX3Byb3BzLm1heFdpZHRoLFxuICAgICAgICAgIGZ1bGxXaWR0aCA9IF9wcm9wcy5mdWxsV2lkdGgsXG4gICAgICAgICAgb3BlbiA9IF9wcm9wcy5vcGVuLFxuICAgICAgICAgIG9uQmFja2Ryb3BDbGljayA9IF9wcm9wcy5vbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgb25Fc2NhcGVLZXlVcCA9IF9wcm9wcy5vbkVzY2FwZUtleVVwLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FbnRlcmVkID0gX3Byb3BzLm9uRW50ZXJlZCxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMub25FeGl0LFxuICAgICAgICAgIG9uRXhpdGluZyA9IF9wcm9wcy5vbkV4aXRpbmcsXG4gICAgICAgICAgb25FeGl0ZWQgPSBfcHJvcHMub25FeGl0ZWQsXG4gICAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHMub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgICAgVHJhbnNpdGlvblByb3AgPSBfcHJvcHMudHJhbnNpdGlvbixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdmdWxsU2NyZWVuJywgJ2lnbm9yZUJhY2tkcm9wQ2xpY2snLCAnaWdub3JlRXNjYXBlS2V5VXAnLCAndHJhbnNpdGlvbkR1cmF0aW9uJywgJ21heFdpZHRoJywgJ2Z1bGxXaWR0aCcsICdvcGVuJywgJ29uQmFja2Ryb3BDbGljaycsICdvbkVzY2FwZUtleVVwJywgJ29uRW50ZXInLCAnb25FbnRlcmluZycsICdvbkVudGVyZWQnLCAnb25FeGl0JywgJ29uRXhpdGluZycsICdvbkV4aXRlZCcsICdvblJlcXVlc3RDbG9zZScsICd0cmFuc2l0aW9uJ10pO1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX01vZGFsMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWUpLFxuICAgICAgICAgIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uOiB0cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgaWdub3JlQmFja2Ryb3BDbGljazogaWdub3JlQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcDogaWdub3JlRXNjYXBlS2V5VXAsXG4gICAgICAgICAgb25CYWNrZHJvcENsaWNrOiBvbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgb25Fc2NhcGVLZXlVcDogb25Fc2NhcGVLZXlVcCxcbiAgICAgICAgICBvblJlcXVlc3RDbG9zZTogb25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgICAgc2hvdzogb3BlblxuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIFRyYW5zaXRpb25Qcm9wLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFwcGVhcjogdHJ1ZSxcbiAgICAgICAgICAgICdpbic6IG9wZW4sXG4gICAgICAgICAgICB0aW1lb3V0OiB0cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgICBvbkVudGVyOiBvbkVudGVyLFxuICAgICAgICAgICAgb25FbnRlcmluZzogb25FbnRlcmluZyxcbiAgICAgICAgICAgIG9uRW50ZXJlZDogb25FbnRlcmVkLFxuICAgICAgICAgICAgb25FeGl0OiBvbkV4aXQsXG4gICAgICAgICAgICBvbkV4aXRpbmc6IG9uRXhpdGluZyxcbiAgICAgICAgICAgIG9uRXhpdGVkOiBvbkV4aXRlZFxuICAgICAgICAgIH0sXG4gICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBfUGFwZXIyLmRlZmF1bHQsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGVsZXZhdGlvbjogMjQsXG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnBhcGVyLCBjbGFzc2VzWydwYXBlcldpZHRoJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKG1heFdpZHRoKV0sIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5mdWxsU2NyZWVuLCBmdWxsU2NyZWVuKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZnVsbFdpZHRoLCBmdWxsV2lkdGgpLCBfY2xhc3NOYW1lcykpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBEaWFsb2c7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5EaWFsb2cuZGVmYXVsdFByb3BzID0ge1xuICBmdWxsU2NyZWVuOiBmYWxzZSxcbiAgaWdub3JlQmFja2Ryb3BDbGljazogZmFsc2UsXG4gIGlnbm9yZUVzY2FwZUtleVVwOiBmYWxzZSxcbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiB7XG4gICAgZW50ZXI6IF90cmFuc2l0aW9ucy5kdXJhdGlvbi5lbnRlcmluZ1NjcmVlbixcbiAgICBleGl0OiBfdHJhbnNpdGlvbnMuZHVyYXRpb24ubGVhdmluZ1NjcmVlblxuICB9LFxuICBtYXhXaWR0aDogJ3NtJyxcbiAgZnVsbFdpZHRoOiBmYWxzZSxcbiAgb3BlbjogZmFsc2UsXG4gIHRyYW5zaXRpb246IF9GYWRlMi5kZWZhdWx0XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aURpYWxvZycgfSkoRGlhbG9nKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWY7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxucmVxdWlyZSgnLi4vQnV0dG9uJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbi8vIFNvIHdlIGRvbid0IGhhdmUgYW55IG92ZXJyaWRlIHByaW9yaXR5IGlzc3VlLlxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2ZsZXgtZW5kJyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAgbWFyZ2luOiB0aGVtZS5zcGFjaW5nLnVuaXQgKyAncHggJyArIHRoZW1lLnNwYWNpbmcudW5pdCAvIDIgKyAncHgnLFxuICAgICAgZmxleDogJzAgMCBhdXRvJ1xuICAgIH0sXG4gICAgYWN0aW9uOiB7XG4gICAgICBtYXJnaW46ICcwICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgLyAyICsgJ3B4J1xuICAgIH0sXG4gICAgYnV0dG9uOiB7XG4gICAgICBtaW5XaWR0aDogNjRcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIERpYWxvZ0FjdGlvbnMocHJvcHMpIHtcbiAgdmFyIGNoaWxkcmVuID0gcHJvcHMuY2hpbGRyZW4sXG4gICAgICBjbGFzc2VzID0gcHJvcHMuY2xhc3NlcyxcbiAgICAgIGNsYXNzTmFtZSA9IHByb3BzLmNsYXNzTmFtZSxcbiAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkocHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnXSk7XG5cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgJ2RpdicsXG4gICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSkgfSwgb3RoZXIpLFxuICAgIF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgaWYgKCFfcmVhY3QyLmRlZmF1bHQuaXNWYWxpZEVsZW1lbnQoY2hpbGQpKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5hY3Rpb24gfSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmJ1dHRvbiwgY2hpbGQucHJvcHMuY2xhc3NOYW1lKVxuICAgICAgICB9KVxuICAgICAgKTtcbiAgICB9KVxuICApO1xufVxuXG5EaWFsb2dBY3Rpb25zLnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IChfcmVmID0ge1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHRoZW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpRGlhbG9nQWN0aW9ucycgfSkoRGlhbG9nQWN0aW9ucyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0FjdGlvbnMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dBY3Rpb25zLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWY7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHZhciBzcGFjaW5nID0gdGhlbWUuc3BhY2luZy51bml0ICogMztcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICAgICAgb3ZlcmZsb3dZOiAnYXV0bycsXG4gICAgICBwYWRkaW5nOiAnMCAnICsgc3BhY2luZyArICdweCAnICsgc3BhY2luZyArICdweCAnICsgc3BhY2luZyArICdweCcsXG4gICAgICAnJjpmaXJzdC1jaGlsZCc6IHtcbiAgICAgICAgcGFkZGluZ1RvcDogc3BhY2luZ1xuICAgICAgfVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cblxuZnVuY3Rpb24gRGlhbG9nQ29udGVudChwcm9wcykge1xuICB2YXIgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NOYW1lID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShwcm9wcywgWydjbGFzc2VzJywgJ2NoaWxkcmVuJywgJ2NsYXNzTmFtZSddKTtcblxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAnZGl2JyxcbiAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSB9LCBvdGhlciksXG4gICAgY2hpbGRyZW5cbiAgKTtcbn1cblxuRGlhbG9nQ29udGVudC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoX3JlZiA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpRGlhbG9nQ29udGVudCcgfSkoRGlhbG9nQ29udGVudCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0NvbnRlbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9yZWY7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDogKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgdGhlbWUudHlwb2dyYXBoeS5zdWJoZWFkaW5nLCB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LnNlY29uZGFyeSxcbiAgICAgIG1hcmdpbjogMFxuICAgIH0pXG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIERpYWxvZ0NvbnRlbnRUZXh0KHByb3BzKSB7XG4gIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWUgPSBwcm9wcy5jbGFzc05hbWUsXG4gICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKHByb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJ10pO1xuXG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICdwJyxcbiAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSB9LCBvdGhlciksXG4gICAgY2hpbGRyZW5cbiAgKTtcbn1cblxuRGlhbG9nQ29udGVudFRleHQucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKF9yZWYgPSB7XG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgdGhlbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSlcbn0sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjbGFzc2VzJywgcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjbGFzc05hbWUnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nKSwgX3JlZikgOiB7fTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlEaWFsb2dDb250ZW50VGV4dCcgfSkoRGlhbG9nQ29udGVudFRleHQpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50VGV4dC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0NvbnRlbnRUZXh0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfVHlwb2dyYXBoeSA9IHJlcXVpcmUoJy4uL1R5cG9ncmFwaHknKTtcblxudmFyIF9UeXBvZ3JhcGh5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1R5cG9ncmFwaHkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBtYXJnaW46IDAsXG4gICAgICBwYWRkaW5nOiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzICsgJ3B4ICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzICsgJ3B4ICAgICAgIDIwcHggJyArIHRoZW1lLnNwYWNpbmcudW5pdCAqIDMgKyAncHgnLFxuICAgICAgZmxleDogJzAgMCBhdXRvJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNoaWxkcmVuIHdvbid0IGJlIHdyYXBwZWQgYnkgYSB0eXBvZ3JhcGh5IGNvbXBvbmVudC5cbiAgICogRm9yIGluc3RhbmNlLCB0aGlzIGNhbiBiZSB1c2VmdWwgdG8gcmVuZGVyIGFuIGg0IGluc3RlYWQgb2YgdGhlIGRlZmF1bHQgaDIuXG4gICAqL1xuICBkaXNhYmxlVHlwb2dyYXBoeTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxudmFyIERpYWxvZ1RpdGxlID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoRGlhbG9nVGl0bGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERpYWxvZ1RpdGxlKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIERpYWxvZ1RpdGxlKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoRGlhbG9nVGl0bGUuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKERpYWxvZ1RpdGxlKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShEaWFsb2dUaXRsZSwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBkaXNhYmxlVHlwb2dyYXBoeSA9IF9wcm9wcy5kaXNhYmxlVHlwb2dyYXBoeSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdkaXNhYmxlVHlwb2dyYXBoeSddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSB9LCBvdGhlciksXG4gICAgICAgIGRpc2FibGVUeXBvZ3JhcGh5ID8gY2hpbGRyZW4gOiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBfVHlwb2dyYXBoeTIuZGVmYXVsdCxcbiAgICAgICAgICB7IHR5cGU6ICd0aXRsZScgfSxcbiAgICAgICAgICBjaGlsZHJlblxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gRGlhbG9nVGl0bGU7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5EaWFsb2dUaXRsZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGRpc2FibGVUeXBvZ3JhcGh5OiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlEaWFsb2dUaXRsZScgfSkoRGlhbG9nVGl0bGUpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dUaXRsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ1RpdGxlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0RpYWxvZyA9IHJlcXVpcmUoJy4vRGlhbG9nJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZykuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfRGlhbG9nQWN0aW9ucyA9IHJlcXVpcmUoJy4vRGlhbG9nQWN0aW9ucycpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0RpYWxvZ0FjdGlvbnMnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9EaWFsb2dBY3Rpb25zKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9EaWFsb2dUaXRsZSA9IHJlcXVpcmUoJy4vRGlhbG9nVGl0bGUnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdEaWFsb2dUaXRsZScsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZ1RpdGxlKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9EaWFsb2dDb250ZW50ID0gcmVxdWlyZSgnLi9EaWFsb2dDb250ZW50Jyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnRGlhbG9nQ29udGVudCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZ0NvbnRlbnQpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX0RpYWxvZ0NvbnRlbnRUZXh0ID0gcmVxdWlyZSgnLi9EaWFsb2dDb250ZW50VGV4dCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0RpYWxvZ0NvbnRlbnRUZXh0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRGlhbG9nQ29udGVudFRleHQpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX3dpdGhNb2JpbGVEaWFsb2cgPSByZXF1aXJlKCcuL3dpdGhNb2JpbGVEaWFsb2cnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICd3aXRoTW9iaWxlRGlhbG9nJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aE1vYmlsZURpYWxvZykuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDUgMjcgMjggMzUgMzcgNDAgNDEgNDIgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgncmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG52YXIgX3dpdGhXaWR0aCA9IHJlcXVpcmUoJy4uL3V0aWxzL3dpdGhXaWR0aCcpO1xuXG52YXIgX3dpdGhXaWR0aDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoV2lkdGgpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSGlnaGVyT3JkZXJDb21wb25lbnQgPSByZXF1aXJlKCdyZWFjdC1mbG93LXR5cGVzJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSGlnaGVyT3JkZXJDb21wb25lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0JyZWFrcG9pbnQgPSByZXF1aXJlKCcuLi9zdHlsZXMvY3JlYXRlQnJlYWtwb2ludHMnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9JbmplY3RlZFByb3BzID0ge1xuICAvKipcbiAgICogSWYgaXNXaWR0aERvd24ob3B0aW9ucy5icmVha3BvaW50KSwgcmV0dXJuIHRydWUuXG4gICAqL1xuICBmdWxsU2NyZWVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxuXG4vKipcbiAqIERpYWxvZyB3aWxsIHJlc3BvbnNpdmVseSBiZSBmdWxsIHNjcmVlbiAqYXQgb3IgYmVsb3cqIHRoZSBnaXZlbiBicmVha3BvaW50XG4gKiAoZGVmYXVsdHMgdG8gJ3NtJyBmb3IgbW9iaWxlIGRldmljZXMpLlxuICogTm90aWNlIHRoYXQgdGhpcyBIaWdoZXItb3JkZXIgQ29tcG9uZW50IGlzIGluY29tcGF0aWJsZSB3aXRoIHNlcnZlciBzaWRlIHJlbmRlcmluZy5cbiAqL1xudmFyIHdpdGhNb2JpbGVEaWFsb2cgPSBmdW5jdGlvbiB3aXRoTW9iaWxlRGlhbG9nKCkge1xuICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogeyBicmVha3BvaW50OiAnc20nIH07XG4gIHJldHVybiBmdW5jdGlvbiAoQ29tcG9uZW50KSB7XG4gICAgdmFyIGJyZWFrcG9pbnQgPSBvcHRpb25zLmJyZWFrcG9pbnQ7XG5cblxuICAgIGZ1bmN0aW9uIFdpdGhNb2JpbGVEaWFsb2cocHJvcHMpIHtcbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChDb21wb25lbnQsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBmdWxsU2NyZWVuOiAoMCwgX3dpdGhXaWR0aC5pc1dpZHRoRG93bikoYnJlYWtwb2ludCwgcHJvcHMud2lkdGgpIH0sIHByb3BzKSk7XG4gICAgfVxuXG4gICAgV2l0aE1vYmlsZURpYWxvZy5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyB7XG4gICAgICB3aWR0aDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0JyZWFrcG9pbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50LmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50LmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0JyZWFrcG9pbnQpLmlzUmVxdWlyZWRcbiAgICB9IDoge307XG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIFdpdGhNb2JpbGVEaWFsb2cuZGlzcGxheU5hbWUgPSAoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQ29tcG9uZW50LCAnd2l0aE1vYmlsZURpYWxvZycpO1xuICAgIH1cblxuICAgIHJldHVybiAoMCwgX3dpdGhXaWR0aDIuZGVmYXVsdCkoKShXaXRoTW9iaWxlRGlhbG9nKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHdpdGhNb2JpbGVEaWFsb2c7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL3dpdGhNb2JpbGVEaWFsb2cuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy93aXRoTW9iaWxlRGlhbG9nLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9CdXR0b25CYXNlID0gcmVxdWlyZSgnLi4vQnV0dG9uQmFzZScpO1xuXG52YXIgX0J1dHRvbkJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQnV0dG9uQmFzZSk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxudmFyIF9JY29uID0gcmVxdWlyZSgnLi4vSWNvbicpO1xuXG52YXIgX0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbik7XG5cbnJlcXVpcmUoJy4uL1N2Z0ljb24nKTtcblxudmFyIF9yZWFjdEhlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9yZWFjdEhlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTsgLy8gIHdlYWtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgQnV0dG9uQmFzZVxuXG4vLyBFbnN1cmUgQ1NTIHNwZWNpZmljaXR5XG5cblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgICAgIGZsZXg6ICcwIDAgYXV0bycsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDI0KSxcbiAgICAgIHdpZHRoOiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA2LFxuICAgICAgaGVpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiA2LFxuICAgICAgcGFkZGluZzogMCxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdiYWNrZ3JvdW5kLWNvbG9yJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlc3RcbiAgICAgIH0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfSxcbiAgICBjb2xvckluaGVyaXQ6IHtcbiAgICAgIGNvbG9yOiAnaW5oZXJpdCdcbiAgICB9LFxuICAgIGRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGxhYmVsOiB7XG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgYWxpZ25JdGVtczogJ2luaGVyaXQnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgaWNvbjoge1xuICAgICAgd2lkdGg6ICcxZW0nLFxuICAgICAgaGVpZ2h0OiAnMWVtJ1xuICAgIH0sXG4gICAga2V5Ym9hcmRGb2N1c2VkOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXZpZGVyXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgbmF0aXZlIGJ1dHRvbiBjb21wb25lbnQuXG4gICAqL1xuICBidXR0b25SZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiBlbGVtZW50LlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGFuIGljb24gZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2RlZmF1bHQnLCAnaW5oZXJpdCcsICdwcmltYXJ5JywgJ2NvbnRyYXN0JywgJ2FjY2VudCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBidXR0b24gd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSByaXBwbGUgd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVSaXBwbGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIHJvb3QgY29tcG9uZW50LlxuICAgKi9cbiAgcm9vdFJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmNcbn07XG5cbi8qKlxuICogUmVmZXIgdG8gdGhlIFtJY29uc10oL3N0eWxlL2ljb25zKSBzZWN0aW9uIG9mIHRoZSBkb2N1bWVudGF0aW9uXG4gKiByZWdhcmRpbmcgdGhlIGF2YWlsYWJsZSBpY29uIG9wdGlvbnMuXG4gKi9cbnZhciBJY29uQnV0dG9uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoSWNvbkJ1dHRvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gSWNvbkJ1dHRvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBJY29uQnV0dG9uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoSWNvbkJ1dHRvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoSWNvbkJ1dHRvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoSWNvbkJ1dHRvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX2NsYXNzTmFtZXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGJ1dHRvblJlZiA9IF9wcm9wcy5idXR0b25SZWYsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgZGlzYWJsZWQgPSBfcHJvcHMuZGlzYWJsZWQsXG4gICAgICAgICAgcm9vdFJlZiA9IF9wcm9wcy5yb290UmVmLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2J1dHRvblJlZicsICdjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICdkaXNhYmxlZCcsICdyb290UmVmJ10pO1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX0J1dHRvbkJhc2UyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdkZWZhdWx0JyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzLmRpc2FibGVkLCBkaXNhYmxlZCksIF9jbGFzc05hbWVzKSwgY2xhc3NOYW1lKSxcbiAgICAgICAgICBjZW50ZXJSaXBwbGU6IHRydWUsXG4gICAgICAgICAga2V5Ym9hcmRGb2N1c2VkQ2xhc3NOYW1lOiBjbGFzc2VzLmtleWJvYXJkRm9jdXNlZCxcbiAgICAgICAgICBkaXNhYmxlZDogZGlzYWJsZWRcbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICByb290UmVmOiBidXR0b25SZWYsXG4gICAgICAgICAgcmVmOiByb290UmVmXG4gICAgICAgIH0pLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnc3BhbicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMubGFiZWwgfSxcbiAgICAgICAgICB0eXBlb2YgY2hpbGRyZW4gPT09ICdzdHJpbmcnID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBfSWNvbjIuZGVmYXVsdCxcbiAgICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmljb24gfSxcbiAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgKSA6IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICAgICAgaWYgKCgwLCBfcmVhY3RIZWxwZXJzLmlzTXVpRWxlbWVudCkoY2hpbGQsIFsnSWNvbicsICdTdmdJY29uJ10pKSB7XG4gICAgICAgICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkLCB7XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMuaWNvbiwgY2hpbGQucHJvcHMuY2xhc3NOYW1lKVxuICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIGNoaWxkO1xuICAgICAgICAgIH0pXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBJY29uQnV0dG9uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuSWNvbkJ1dHRvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnZGVmYXVsdCcsXG4gIGRpc2FibGVkOiBmYWxzZSxcbiAgZGlzYWJsZVJpcHBsZTogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpSWNvbkJ1dHRvbicgfSkoSWNvbkJ1dHRvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDMgNiAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNyAzOCAzOSA0MCA0NCA0NSA0NiA0OSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9JY29uQnV0dG9uID0gcmVxdWlyZSgnLi9JY29uQnV0dG9uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb25CdXR0b24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDMgNiAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNyAzOCAzOSA0MCA0NCA0NSA0NiA0OSA1NSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHpJbmRleDogLTEsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnMTAwJScsXG4gICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6IDAsXG4gICAgICAvLyBSZW1vdmUgZ3JleSBoaWdobGlnaHRcbiAgICAgIFdlYmtpdFRhcEhpZ2hsaWdodENvbG9yOiB0aGVtZS5wYWxldHRlLmNvbW1vbi50cmFuc3BhcmVudCxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb21tb24ubGlnaHRCbGFjayxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScpLFxuICAgICAgd2lsbENoYW5nZTogJ29wYWNpdHknLFxuICAgICAgb3BhY2l0eTogMFxuICAgIH0sXG4gICAgaW52aXNpYmxlOiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuY29tbW9uLnRyYW5zcGFyZW50XG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogQ2FuIGJlIHVzZWQsIGZvciBpbnN0YW5jZSwgdG8gcmVuZGVyIGEgbGV0dGVyIGluc2lkZSB0aGUgYXZhdGFyLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBiYWNrZHJvcCBpcyBpbnZpc2libGUuXG4gICAqL1xuICBpbnZpc2libGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIEJhY2tkcm9wID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoQmFja2Ryb3AsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEJhY2tkcm9wKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEJhY2tkcm9wKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoQmFja2Ryb3AuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEJhY2tkcm9wKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShCYWNrZHJvcCwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBpbnZpc2libGUgPSBfcHJvcHMuaW52aXNpYmxlLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2ludmlzaWJsZSddKTtcblxuXG4gICAgICB2YXIgYmFja2Ryb3BDbGFzcyA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5pbnZpc2libGUsIGludmlzaWJsZSksIGNsYXNzTmFtZSk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ2RpdicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGJhY2tkcm9wQ2xhc3MsICdhcmlhLWhpZGRlbic6ICd0cnVlJyB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQmFja2Ryb3A7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5CYWNrZHJvcC5kZWZhdWx0UHJvcHMgPSB7XG4gIGludmlzaWJsZTogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQmFja2Ryb3AnIH0pKEJhY2tkcm9wKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9CYWNrZHJvcC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvQmFja2Ryb3AuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9rZXlzID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9rZXlzJyk7XG5cbnZhciBfa2V5czIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9rZXlzKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9yZWFjdERvbSA9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG52YXIgX3JlYWN0RG9tMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0RG9tKTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX2tleWNvZGUgPSByZXF1aXJlKCdrZXljb2RlJyk7XG5cbnZhciBfa2V5Y29kZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9rZXljb2RlKTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbnZhciBfY29udGFpbnMgPSByZXF1aXJlKCdkb20taGVscGVycy9xdWVyeS9jb250YWlucycpO1xuXG52YXIgX2NvbnRhaW5zMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NvbnRhaW5zKTtcblxudmFyIF9hY3RpdmVFbGVtZW50ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudCcpO1xuXG52YXIgX2FjdGl2ZUVsZW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfYWN0aXZlRWxlbWVudCk7XG5cbnZhciBfb3duZXJEb2N1bWVudCA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQnKTtcblxudmFyIF9vd25lckRvY3VtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX293bmVyRG9jdW1lbnQpO1xuXG52YXIgX2FkZEV2ZW50TGlzdGVuZXIgPSByZXF1aXJlKCcuLi91dGlscy9hZGRFdmVudExpc3RlbmVyJyk7XG5cbnZhciBfYWRkRXZlbnRMaXN0ZW5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9hZGRFdmVudExpc3RlbmVyKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ZhZGUgPSByZXF1aXJlKCcuLi90cmFuc2l0aW9ucy9GYWRlJyk7XG5cbnZhciBfRmFkZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9GYWRlKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX21vZGFsTWFuYWdlciA9IHJlcXVpcmUoJy4vbW9kYWxNYW5hZ2VyJyk7XG5cbnZhciBfbW9kYWxNYW5hZ2VyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX21vZGFsTWFuYWdlcik7XG5cbnZhciBfQmFja2Ryb3AgPSByZXF1aXJlKCcuL0JhY2tkcm9wJyk7XG5cbnZhciBfQmFja2Ryb3AyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQmFja2Ryb3ApO1xuXG52YXIgX1BvcnRhbCA9IHJlcXVpcmUoJy4uL2ludGVybmFsL1BvcnRhbCcpO1xuXG52YXIgX1BvcnRhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Qb3J0YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbi8vIE1vZGFscyBkb24ndCBvcGVuIG9uIHRoZSBzZXJ2ZXIgc28gdGhpcyB3b24ndCBicmVhayBjb25jdXJyZW5jeS5cbi8vIENvdWxkIGFsc28gcHV0IHRoaXMgb24gY29udGV4dC5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBtb2RhbE1hbmFnZXIgPSAoMCwgX21vZGFsTWFuYWdlcjIuZGVmYXVsdCkoKTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJzEwMCUnLFxuICAgICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgICB6SW5kZXg6IHRoZW1lLnpJbmRleC5kaWFsb2csXG4gICAgICB0b3A6IDAsXG4gICAgICBsZWZ0OiAwXG4gICAgfSxcbiAgICBoaWRkZW46IHtcbiAgICAgIHZpc2liaWxpdHk6ICdoaWRkZW4nXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIENTUyBjbGFzcyBuYW1lIG9mIHRoZSBiYWNrZHJvcCBlbGVtZW50LlxuICAgKi9cbiAgQmFja2Ryb3BDbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFBhc3MgYSBjb21wb25lbnQgY2xhc3MgdG8gdXNlIGFzIHRoZSBiYWNrZHJvcC5cbiAgICovXG4gIEJhY2tkcm9wQ29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBiYWNrZHJvcCBpcyBpbnZpc2libGUuXG4gICAqL1xuICBCYWNrZHJvcEludmlzaWJsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGR1cmF0aW9uIGZvciB0aGUgYmFja2Ryb3AgdHJhbnNpdGlvbiwgaW4gbWlsbGlzZWNvbmRzLlxuICAgKiBZb3UgbWF5IHNwZWNpZnkgYSBzaW5nbGUgdGltZW91dCBmb3IgYWxsIHRyYW5zaXRpb25zLCBvciBpbmRpdmlkdWFsbHkgd2l0aCBhbiBvYmplY3QuXG4gICAqL1xuICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBBIHNpbmdsZSBjaGlsZCBjb250ZW50IGVsZW1lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsd2F5cyBrZWVwIHRoZSBjaGlsZHJlbiBpbiB0aGUgRE9NLlxuICAgKiBUaGlzIHByb3BlcnR5IGNhbiBiZSB1c2VmdWwgaW4gU0VPIHNpdHVhdGlvbiBvclxuICAgKiB3aGVuIHlvdSB3YW50IHRvIG1heGltaXplIHRoZSByZXNwb25zaXZlbmVzcyBvZiB0aGUgTW9kYWwuXG4gICAqL1xuICBrZWVwTW91bnRlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYmFja2Ryb3AgaXMgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlQmFja2Ryb3A6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgY2xpY2tpbmcgdGhlIGJhY2tkcm9wIHdpbGwgbm90IGZpcmUgdGhlIGBvblJlcXVlc3RDbG9zZWAgY2FsbGJhY2suXG4gICAqL1xuICBpZ25vcmVCYWNrZHJvcENsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGhpdHRpbmcgZXNjYXBlIHdpbGwgbm90IGZpcmUgdGhlIGBvblJlcXVlc3RDbG9zZWAgY2FsbGJhY2suXG4gICAqL1xuICBpZ25vcmVFc2NhcGVLZXlVcDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgbW9kYWxNYW5hZ2VyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVzIHdoZW4gdGhlIGJhY2tkcm9wIGlzIGNsaWNrZWQgb24uXG4gICAqL1xuICBvbkJhY2tkcm9wQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIG1vZGFsIGlzIGVudGVyaW5nLlxuICAgKi9cbiAgb25FbnRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBtb2RhbCBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaGFzIGVudGVyZWQuXG4gICAqL1xuICBvbkVudGVyZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZXMgd2hlbiB0aGUgZXNjYXBlIGtleSBpcyBwcmVzc2VkIGFuZCB0aGUgbW9kYWwgaXMgaW4gZm9jdXMuXG4gICAqL1xuICBvbkVzY2FwZUtleVVwOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgYmVmb3JlIHRoZSBtb2RhbCBpcyBleGl0aW5nLlxuICAgKi9cbiAgb25FeGl0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGlzIGV4aXRpbmcuXG4gICAqL1xuICBvbkV4aXRpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaGFzIGV4aXRlZC5cbiAgICovXG4gIG9uRXhpdGVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCByZXF1ZXN0cyB0byBiZSBjbG9zZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKi9cbiAgb25SZXF1ZXN0Q2xvc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBNb2RhbCBpcyB2aXNpYmxlLlxuICAgKi9cbiAgc2hvdzogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxuLyoqXG4gKiBUaGUgbW9kYWwgY29tcG9uZW50IHByb3ZpZGVzIGEgc29saWQgZm91bmRhdGlvbiBmb3IgY3JlYXRpbmcgZGlhbG9ncyxcbiAqIHBvcG92ZXJzLCBvciB3aGF0ZXZlciBlbHNlLlxuICogVGhlIGNvbXBvbmVudCByZW5kZXJzIGl0cyBgY2hpbGRyZW5gIG5vZGUgaW4gZnJvbnQgb2YgYSBiYWNrZHJvcCBjb21wb25lbnQuXG4gKlxuICogVGhlIGBNb2RhbGAgb2ZmZXJzIGEgZmV3IGhlbHBmdWwgZmVhdHVyZXMgb3ZlciB1c2luZyBqdXN0IGEgYFBvcnRhbGAgY29tcG9uZW50IGFuZCBzb21lIHN0eWxlczpcbiAqIC0gTWFuYWdlcyBkaWFsb2cgc3RhY2tpbmcgd2hlbiBvbmUtYXQtYS10aW1lIGp1c3QgaXNuJ3QgZW5vdWdoLlxuICogLSBDcmVhdGVzIGEgYmFja2Ryb3AsIGZvciBkaXNhYmxpbmcgaW50ZXJhY3Rpb24gYmVsb3cgdGhlIG1vZGFsLlxuICogLSBJdCBwcm9wZXJseSBtYW5hZ2VzIGZvY3VzOyBtb3ZpbmcgdG8gdGhlIG1vZGFsIGNvbnRlbnQsXG4gKiAgIGFuZCBrZWVwaW5nIGl0IHRoZXJlIHVudGlsIHRoZSBtb2RhbCBpcyBjbG9zZWQuXG4gKiAtIEl0IGRpc2FibGVzIHNjcm9sbGluZyBvZiB0aGUgcGFnZSBjb250ZW50IHdoaWxlIG9wZW4uXG4gKiAtIEFkZHMgdGhlIGFwcHJvcHJpYXRlIEFSSUEgcm9sZXMgYXJlIGF1dG9tYXRpY2FsbHkuXG4gKlxuICogVGhpcyBjb21wb25lbnQgc2hhcmVzIG1hbnkgY29uY2VwdHMgd2l0aCBbcmVhY3Qtb3ZlcmxheXNdKGh0dHBzOi8vcmVhY3QtYm9vdHN0cmFwLmdpdGh1Yi5pby9yZWFjdC1vdmVybGF5cy8jbW9kYWxzKS5cbiAqL1xudmFyIE1vZGFsID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoTW9kYWwsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIE1vZGFsKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIE1vZGFsKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IE1vZGFsLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShNb2RhbCkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF9pbml0aWFsaXNlUHJvcHMuY2FsbChfdGhpcyksIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShNb2RhbCwgW3tcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgICBpZiAoIXRoaXMucHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZXhpdGVkOiB0cnVlIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICB0aGlzLm1vdW50ZWQgPSB0cnVlO1xuICAgICAgaWYgKHRoaXMucHJvcHMuc2hvdykge1xuICAgICAgICB0aGlzLmhhbmRsZVNob3coKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMpIHtcbiAgICAgIGlmIChuZXh0UHJvcHMuc2hvdyAmJiB0aGlzLnN0YXRlLmV4aXRlZCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZXhpdGVkOiBmYWxzZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50V2lsbFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgIGlmICghdGhpcy5wcm9wcy5zaG93ICYmIG5leHRQcm9wcy5zaG93KSB7XG4gICAgICAgIHRoaXMuY2hlY2tGb3JGb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZShwcmV2UHJvcHMpIHtcbiAgICAgIGlmICghcHJldlByb3BzLnNob3cgJiYgdGhpcy5wcm9wcy5zaG93KSB7XG4gICAgICAgIHRoaXMuaGFuZGxlU2hvdygpO1xuICAgICAgfVxuICAgICAgLy8gV2UgYXJlIHdhaXRpbmcgZm9yIHRoZSBvbkV4aXRlZCBjYWxsYmFjayB0byBjYWxsIGhhbmRsZUhpZGUuXG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIGlmICh0aGlzLnByb3BzLnNob3cgfHwgIXRoaXMuc3RhdGUuZXhpdGVkKSB7XG4gICAgICAgIHRoaXMuaGFuZGxlSGlkZSgpO1xuICAgICAgfVxuICAgICAgdGhpcy5tb3VudGVkID0gZmFsc2U7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY2hlY2tGb3JGb2N1cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNoZWNrRm9yRm9jdXMoKSB7XG4gICAgICBpZiAoX2luRE9NMi5kZWZhdWx0KSB7XG4gICAgICAgIHRoaXMubGFzdEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3Jlc3RvcmVMYXN0Rm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZXN0b3JlTGFzdEZvY3VzKCkge1xuICAgICAgaWYgKHRoaXMubGFzdEZvY3VzICYmIHRoaXMubGFzdEZvY3VzLmZvY3VzKSB7XG4gICAgICAgIHRoaXMubGFzdEZvY3VzLmZvY3VzKCk7XG4gICAgICAgIHRoaXMubGFzdEZvY3VzID0gdW5kZWZpbmVkO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2hhbmRsZVNob3cnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYW5kbGVTaG93KCkge1xuICAgICAgdmFyIGRvYyA9ICgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKHRoaXMpKTtcbiAgICAgIHRoaXMucHJvcHMubW9kYWxNYW5hZ2VyLmFkZCh0aGlzKTtcbiAgICAgIHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIgPSAoMCwgX2FkZEV2ZW50TGlzdGVuZXIyLmRlZmF1bHQpKGRvYywgJ2tleXVwJywgdGhpcy5oYW5kbGVEb2N1bWVudEtleVVwKTtcbiAgICAgIHRoaXMub25Gb2N1c0xpc3RlbmVyID0gKDAsIF9hZGRFdmVudExpc3RlbmVyMi5kZWZhdWx0KShkb2MsICdmb2N1cycsIHRoaXMuaGFuZGxlRm9jdXNMaXN0ZW5lciwgdHJ1ZSk7XG4gICAgICB0aGlzLmZvY3VzKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZm9jdXMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBmb2N1cygpIHtcbiAgICAgIHZhciBjdXJyZW50Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkoX3JlYWN0RG9tMi5kZWZhdWx0LmZpbmRET01Ob2RlKHRoaXMpKSk7XG4gICAgICB2YXIgbW9kYWxDb250ZW50ID0gdGhpcy5tb2RhbCAmJiB0aGlzLm1vZGFsLmxhc3RDaGlsZDtcbiAgICAgIHZhciBmb2N1c0luTW9kYWwgPSBjdXJyZW50Rm9jdXMgJiYgKDAsIF9jb250YWluczIuZGVmYXVsdCkobW9kYWxDb250ZW50LCBjdXJyZW50Rm9jdXMpO1xuXG4gICAgICBpZiAobW9kYWxDb250ZW50ICYmICFmb2N1c0luTW9kYWwpIHtcbiAgICAgICAgaWYgKCFtb2RhbENvbnRlbnQuaGFzQXR0cmlidXRlKCd0YWJJbmRleCcpKSB7XG4gICAgICAgICAgbW9kYWxDb250ZW50LnNldEF0dHJpYnV0ZSgndGFiSW5kZXgnLCAtMSk7XG4gICAgICAgICAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KShmYWxzZSwgJ01hdGVyaWFsLVVJOiB0aGUgbW9kYWwgY29udGVudCBub2RlIGRvZXMgbm90IGFjY2VwdCBmb2N1cy4gJyArICdGb3IgdGhlIGJlbmVmaXQgb2YgYXNzaXN0aXZlIHRlY2hub2xvZ2llcywgJyArICd0aGUgdGFiSW5kZXggb2YgdGhlIG5vZGUgaXMgYmVpbmcgc2V0IHRvIFwiLTFcIi4nKSA6IHZvaWQgMDtcbiAgICAgICAgfVxuXG4gICAgICAgIG1vZGFsQ29udGVudC5mb2N1cygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2hhbmRsZUhpZGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBoYW5kbGVIaWRlKCkge1xuICAgICAgdGhpcy5wcm9wcy5tb2RhbE1hbmFnZXIucmVtb3ZlKHRoaXMpO1xuICAgICAgaWYgKHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIpIHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIucmVtb3ZlKCk7XG4gICAgICBpZiAodGhpcy5vbkZvY3VzTGlzdGVuZXIpIHRoaXMub25Gb2N1c0xpc3RlbmVyLnJlbW92ZSgpO1xuICAgICAgdGhpcy5yZXN0b3JlTGFzdEZvY3VzKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyQmFja2Ryb3AnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXJCYWNrZHJvcCgpIHtcbiAgICAgIHZhciBvdGhlciA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge307XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBCYWNrZHJvcENvbXBvbmVudCA9IF9wcm9wcy5CYWNrZHJvcENvbXBvbmVudCxcbiAgICAgICAgICBCYWNrZHJvcENsYXNzTmFtZSA9IF9wcm9wcy5CYWNrZHJvcENsYXNzTmFtZSxcbiAgICAgICAgICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiA9IF9wcm9wcy5CYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgICBCYWNrZHJvcEludmlzaWJsZSA9IF9wcm9wcy5CYWNrZHJvcEludmlzaWJsZSxcbiAgICAgICAgICBzaG93ID0gX3Byb3BzLnNob3c7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfRmFkZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGFwcGVhcjogdHJ1ZSwgJ2luJzogc2hvdywgdGltZW91dDogQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gfSwgb3RoZXIpLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChCYWNrZHJvcENvbXBvbmVudCwge1xuICAgICAgICAgIGludmlzaWJsZTogQmFja2Ryb3BJbnZpc2libGUsXG4gICAgICAgICAgY2xhc3NOYW1lOiBCYWNrZHJvcENsYXNzTmFtZSxcbiAgICAgICAgICBvbkNsaWNrOiB0aGlzLmhhbmRsZUJhY2tkcm9wQ2xpY2tcbiAgICAgICAgfSlcbiAgICAgICk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBkaXNhYmxlQmFja2Ryb3AgPSBfcHJvcHMyLmRpc2FibGVCYWNrZHJvcCxcbiAgICAgICAgICBCYWNrZHJvcENvbXBvbmVudCA9IF9wcm9wczIuQmFja2Ryb3BDb21wb25lbnQsXG4gICAgICAgICAgQmFja2Ryb3BDbGFzc05hbWUgPSBfcHJvcHMyLkJhY2tkcm9wQ2xhc3NOYW1lLFxuICAgICAgICAgIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uID0gX3Byb3BzMi5CYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbixcbiAgICAgICAgICBCYWNrZHJvcEludmlzaWJsZSA9IF9wcm9wczIuQmFja2Ryb3BJbnZpc2libGUsXG4gICAgICAgICAgaWdub3JlQmFja2Ryb3BDbGljayA9IF9wcm9wczIuaWdub3JlQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcCA9IF9wcm9wczIuaWdub3JlRXNjYXBlS2V5VXAsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMyLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMyLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzMi5jbGFzc05hbWUsXG4gICAgICAgICAga2VlcE1vdW50ZWQgPSBfcHJvcHMyLmtlZXBNb3VudGVkLFxuICAgICAgICAgIG1vZGFsTWFuYWdlclByb3AgPSBfcHJvcHMyLm1vZGFsTWFuYWdlcixcbiAgICAgICAgICBvbkJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMyLm9uQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBvbkVzY2FwZUtleVVwID0gX3Byb3BzMi5vbkVzY2FwZUtleVVwLFxuICAgICAgICAgIG9uUmVxdWVzdENsb3NlID0gX3Byb3BzMi5vblJlcXVlc3RDbG9zZSxcbiAgICAgICAgICBvbkVudGVyID0gX3Byb3BzMi5vbkVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmcgPSBfcHJvcHMyLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FbnRlcmVkID0gX3Byb3BzMi5vbkVudGVyZWQsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzMi5vbkV4aXQsXG4gICAgICAgICAgb25FeGl0aW5nID0gX3Byb3BzMi5vbkV4aXRpbmcsXG4gICAgICAgICAgb25FeGl0ZWQgPSBfcHJvcHMyLm9uRXhpdGVkLFxuICAgICAgICAgIHNob3cgPSBfcHJvcHMyLnNob3csXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMyLCBbJ2Rpc2FibGVCYWNrZHJvcCcsICdCYWNrZHJvcENvbXBvbmVudCcsICdCYWNrZHJvcENsYXNzTmFtZScsICdCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbicsICdCYWNrZHJvcEludmlzaWJsZScsICdpZ25vcmVCYWNrZHJvcENsaWNrJywgJ2lnbm9yZUVzY2FwZUtleVVwJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2tlZXBNb3VudGVkJywgJ21vZGFsTWFuYWdlcicsICdvbkJhY2tkcm9wQ2xpY2snLCAnb25Fc2NhcGVLZXlVcCcsICdvblJlcXVlc3RDbG9zZScsICdvbkVudGVyJywgJ29uRW50ZXJpbmcnLCAnb25FbnRlcmVkJywgJ29uRXhpdCcsICdvbkV4aXRpbmcnLCAnb25FeGl0ZWQnLCAnc2hvdyddKTtcblxuXG4gICAgICBpZiAoIWtlZXBNb3VudGVkICYmICFzaG93ICYmIHRoaXMuc3RhdGUuZXhpdGVkKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgdHJhbnNpdGlvbkNhbGxiYWNrcyA9IHtcbiAgICAgICAgb25FbnRlcjogb25FbnRlcixcbiAgICAgICAgb25FbnRlcmluZzogb25FbnRlcmluZyxcbiAgICAgICAgb25FbnRlcmVkOiBvbkVudGVyZWQsXG4gICAgICAgIG9uRXhpdDogb25FeGl0LFxuICAgICAgICBvbkV4aXRpbmc6IG9uRXhpdGluZyxcbiAgICAgICAgb25FeGl0ZWQ6IHRoaXMuaGFuZGxlVHJhbnNpdGlvbkV4aXRlZFxuICAgICAgfTtcblxuICAgICAgdmFyIG1vZGFsQ2hpbGQgPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ub25seShjaGlsZHJlbik7XG4gICAgICB2YXIgX21vZGFsQ2hpbGQkcHJvcHMgPSBtb2RhbENoaWxkLnByb3BzLFxuICAgICAgICAgIHJvbGUgPSBfbW9kYWxDaGlsZCRwcm9wcy5yb2xlLFxuICAgICAgICAgIHRhYkluZGV4ID0gX21vZGFsQ2hpbGQkcHJvcHMudGFiSW5kZXg7XG5cbiAgICAgIHZhciBjaGlsZFByb3BzID0ge307XG5cbiAgICAgIGlmIChyb2xlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgY2hpbGRQcm9wcy5yb2xlID0gcm9sZSA9PT0gdW5kZWZpbmVkID8gJ2RvY3VtZW50JyA6IHJvbGU7XG4gICAgICB9XG5cbiAgICAgIGlmICh0YWJJbmRleCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGNoaWxkUHJvcHMudGFiSW5kZXggPSB0YWJJbmRleCA9PSBudWxsID8gLTEgOiB0YWJJbmRleDtcbiAgICAgIH1cblxuICAgICAgdmFyIGJhY2tkcm9wUHJvcHMgPSB2b2lkIDA7XG5cbiAgICAgIC8vIEl0J3MgYSBUcmFuc2l0aW9uIGxpa2UgY29tcG9uZW50XG4gICAgICBpZiAobW9kYWxDaGlsZC5wcm9wcy5oYXNPd25Qcm9wZXJ0eSgnaW4nKSkge1xuICAgICAgICAoMCwgX2tleXMyLmRlZmF1bHQpKHRyYW5zaXRpb25DYWxsYmFja3MpLmZvckVhY2goZnVuY3Rpb24gKGtleSkge1xuICAgICAgICAgIGNoaWxkUHJvcHNba2V5XSA9ICgwLCBfaGVscGVycy5jcmVhdGVDaGFpbmVkRnVuY3Rpb24pKHRyYW5zaXRpb25DYWxsYmFja3Nba2V5XSwgbW9kYWxDaGlsZC5wcm9wc1trZXldKTtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBiYWNrZHJvcFByb3BzID0gdHJhbnNpdGlvbkNhbGxiYWNrcztcbiAgICAgIH1cblxuICAgICAgaWYgKCgwLCBfa2V5czIuZGVmYXVsdCkoY2hpbGRQcm9wcykubGVuZ3RoKSB7XG4gICAgICAgIG1vZGFsQ2hpbGQgPSBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KG1vZGFsQ2hpbGQsIGNoaWxkUHJvcHMpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9Qb3J0YWwyLmRlZmF1bHQsXG4gICAgICAgIHtcbiAgICAgICAgICBvcGVuOiB0cnVlLFxuICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgIF90aGlzMi5tb3VudE5vZGUgPSBub2RlID8gbm9kZS5nZXRMYXllcigpIDogbnVsbDtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5oaWRkZW4sIHRoaXMuc3RhdGUuZXhpdGVkKSlcbiAgICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgICBfdGhpczIubW9kYWwgPSBub2RlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH0pLFxuICAgICAgICAgICFkaXNhYmxlQmFja2Ryb3AgJiYgKCFrZWVwTW91bnRlZCB8fCBzaG93IHx8ICF0aGlzLnN0YXRlLmV4aXRlZCkgJiYgdGhpcy5yZW5kZXJCYWNrZHJvcChiYWNrZHJvcFByb3BzKSxcbiAgICAgICAgICBtb2RhbENoaWxkXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBNb2RhbDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbk1vZGFsLmRlZmF1bHRQcm9wcyA9IHtcbiAgQmFja2Ryb3BDb21wb25lbnQ6IF9CYWNrZHJvcDIuZGVmYXVsdCxcbiAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb246IDMwMCxcbiAgQmFja2Ryb3BJbnZpc2libGU6IGZhbHNlLFxuICBrZWVwTW91bnRlZDogZmFsc2UsXG4gIGRpc2FibGVCYWNrZHJvcDogZmFsc2UsXG4gIGlnbm9yZUJhY2tkcm9wQ2xpY2s6IGZhbHNlLFxuICBpZ25vcmVFc2NhcGVLZXlVcDogZmFsc2UsXG4gIG1vZGFsTWFuYWdlcjogbW9kYWxNYW5hZ2VyXG59O1xuXG52YXIgX2luaXRpYWxpc2VQcm9wcyA9IGZ1bmN0aW9uIF9pbml0aWFsaXNlUHJvcHMoKSB7XG4gIHZhciBfdGhpczMgPSB0aGlzO1xuXG4gIHRoaXMuc3RhdGUgPSB7XG4gICAgZXhpdGVkOiBmYWxzZVxuICB9O1xuICB0aGlzLm9uRG9jdW1lbnRLZXlVcExpc3RlbmVyID0gbnVsbDtcbiAgdGhpcy5vbkZvY3VzTGlzdGVuZXIgPSBudWxsO1xuICB0aGlzLm1vdW50ZWQgPSBmYWxzZTtcbiAgdGhpcy5sYXN0Rm9jdXMgPSB1bmRlZmluZWQ7XG4gIHRoaXMubW9kYWwgPSBudWxsO1xuICB0aGlzLm1vdW50Tm9kZSA9IG51bGw7XG5cbiAgdGhpcy5oYW5kbGVGb2N1c0xpc3RlbmVyID0gZnVuY3Rpb24gKCkge1xuICAgIGlmICghX3RoaXMzLm1vdW50ZWQgfHwgIV90aGlzMy5wcm9wcy5tb2RhbE1hbmFnZXIuaXNUb3BNb2RhbChfdGhpczMpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIGN1cnJlbnRGb2N1cyA9ICgwLCBfYWN0aXZlRWxlbWVudDIuZGVmYXVsdCkoKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShfcmVhY3REb20yLmRlZmF1bHQuZmluZERPTU5vZGUoX3RoaXMzKSkpO1xuICAgIHZhciBtb2RhbENvbnRlbnQgPSBfdGhpczMubW9kYWwgJiYgX3RoaXMzLm1vZGFsLmxhc3RDaGlsZDtcblxuICAgIGlmIChtb2RhbENvbnRlbnQgJiYgbW9kYWxDb250ZW50ICE9PSBjdXJyZW50Rm9jdXMgJiYgISgwLCBfY29udGFpbnMyLmRlZmF1bHQpKG1vZGFsQ29udGVudCwgY3VycmVudEZvY3VzKSkge1xuICAgICAgbW9kYWxDb250ZW50LmZvY3VzKCk7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMuaGFuZGxlRG9jdW1lbnRLZXlVcCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgIGlmICghX3RoaXMzLm1vdW50ZWQgfHwgIV90aGlzMy5wcm9wcy5tb2RhbE1hbmFnZXIuaXNUb3BNb2RhbChfdGhpczMpKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKCgwLCBfa2V5Y29kZTIuZGVmYXVsdCkoZXZlbnQpICE9PSAnZXNjJykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBfcHJvcHMzID0gX3RoaXMzLnByb3BzLFxuICAgICAgICBvbkVzY2FwZUtleVVwID0gX3Byb3BzMy5vbkVzY2FwZUtleVVwLFxuICAgICAgICBvblJlcXVlc3RDbG9zZSA9IF9wcm9wczMub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgIGlnbm9yZUVzY2FwZUtleVVwID0gX3Byb3BzMy5pZ25vcmVFc2NhcGVLZXlVcDtcblxuXG4gICAgaWYgKG9uRXNjYXBlS2V5VXApIHtcbiAgICAgIG9uRXNjYXBlS2V5VXAoZXZlbnQpO1xuICAgIH1cblxuICAgIGlmIChvblJlcXVlc3RDbG9zZSAmJiAhaWdub3JlRXNjYXBlS2V5VXApIHtcbiAgICAgIG9uUmVxdWVzdENsb3NlKGV2ZW50KTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5oYW5kbGVCYWNrZHJvcENsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgaWYgKGV2ZW50LnRhcmdldCAhPT0gZXZlbnQuY3VycmVudFRhcmdldCkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHZhciBfcHJvcHM0ID0gX3RoaXMzLnByb3BzLFxuICAgICAgICBvbkJhY2tkcm9wQ2xpY2sgPSBfcHJvcHM0Lm9uQmFja2Ryb3BDbGljayxcbiAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHM0Lm9uUmVxdWVzdENsb3NlLFxuICAgICAgICBpZ25vcmVCYWNrZHJvcENsaWNrID0gX3Byb3BzNC5pZ25vcmVCYWNrZHJvcENsaWNrO1xuXG5cbiAgICBpZiAob25CYWNrZHJvcENsaWNrKSB7XG4gICAgICBvbkJhY2tkcm9wQ2xpY2soZXZlbnQpO1xuICAgIH1cblxuICAgIGlmIChvblJlcXVlc3RDbG9zZSAmJiAhaWdub3JlQmFja2Ryb3BDbGljaykge1xuICAgICAgb25SZXF1ZXN0Q2xvc2UoZXZlbnQpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLmhhbmRsZVRyYW5zaXRpb25FeGl0ZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKF90aGlzMy5wcm9wcy5vbkV4aXRlZCkge1xuICAgICAgdmFyIF9wcm9wczU7XG5cbiAgICAgIChfcHJvcHM1ID0gX3RoaXMzLnByb3BzKS5vbkV4aXRlZC5hcHBseShfcHJvcHM1LCBhcmd1bWVudHMpO1xuICAgIH1cblxuICAgIF90aGlzMy5zZXRTdGF0ZSh7IGV4aXRlZDogdHJ1ZSB9KTtcbiAgICBfdGhpczMuaGFuZGxlSGlkZSgpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgZmxpcDogZmFsc2UsIG5hbWU6ICdNdWlNb2RhbCcgfSkoTW9kYWwpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL01vZGFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9Nb2RhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfTW9kYWwgPSByZXF1aXJlKCcuL01vZGFsJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX01vZGFsKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3dhcm5pbmcgPSByZXF1aXJlKCd3YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxudmFyIF9pc1dpbmRvdyA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3F1ZXJ5L2lzV2luZG93Jyk7XG5cbnZhciBfaXNXaW5kb3cyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNXaW5kb3cpO1xuXG52YXIgX293bmVyRG9jdW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9vd25lckRvY3VtZW50Jyk7XG5cbnZhciBfb3duZXJEb2N1bWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vd25lckRvY3VtZW50KTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbnZhciBfc2Nyb2xsYmFyU2l6ZSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZScpO1xuXG52YXIgX3Njcm9sbGJhclNpemUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2Nyb2xsYmFyU2l6ZSk7XG5cbnZhciBfbWFuYWdlQXJpYUhpZGRlbiA9IHJlcXVpcmUoJy4uL3V0aWxzL21hbmFnZUFyaWFIaWRkZW4nKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuLy8gVGFrZW4gZnJvbSBodHRwczovL2dpdGh1Yi5jb20vcmVhY3QtYm9vdHN0cmFwL3JlYWN0LW92ZXJsYXlzL2Jsb2IvbWFzdGVyL3NyYy9Nb2RhbE1hbmFnZXIuanNcblxuZnVuY3Rpb24gZ2V0UGFkZGluZ1JpZ2h0KG5vZGUpIHtcbiAgcmV0dXJuIHBhcnNlSW50KG5vZGUuc3R5bGUucGFkZGluZ1JpZ2h0IHx8IDAsIDEwKTtcbn1cblxuLy8gRG8gd2UgaGF2ZSBhIHNjcm9sbCBiYXI/XG5mdW5jdGlvbiBib2R5SXNPdmVyZmxvd2luZyhub2RlKSB7XG4gIHZhciBkb2MgPSAoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKG5vZGUpO1xuICB2YXIgd2luID0gKDAsIF9pc1dpbmRvdzIuZGVmYXVsdCkoZG9jKTtcblxuICAvLyBUYWtlcyBpbiBhY2NvdW50IHBvdGVudGlhbCBub24gemVybyBtYXJnaW4gb24gdGhlIGJvZHkuXG4gIHZhciBzdHlsZSA9IHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlKGRvYy5ib2R5KTtcbiAgdmFyIG1hcmdpbkxlZnQgPSBwYXJzZUludChzdHlsZS5nZXRQcm9wZXJ0eVZhbHVlKCdtYXJnaW4tbGVmdCcpLCAxMCk7XG4gIHZhciBtYXJnaW5SaWdodCA9IHBhcnNlSW50KHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ21hcmdpbi1yaWdodCcpLCAxMCk7XG5cbiAgcmV0dXJuIG1hcmdpbkxlZnQgKyBkb2MuYm9keS5jbGllbnRXaWR0aCArIG1hcmdpblJpZ2h0IDwgd2luLmlubmVyV2lkdGg7XG59XG5cbmZ1bmN0aW9uIGdldENvbnRhaW5lcigpIHtcbiAgdmFyIGNvbnRhaW5lciA9IF9pbkRPTTIuZGVmYXVsdCA/IHdpbmRvdy5kb2N1bWVudC5ib2R5IDoge307XG4gIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkoY29udGFpbmVyICE9PSBudWxsLCAnXFxuTWF0ZXJpYWwtVUk6IHlvdSBhcmUgbW9zdCBsaWtlbHkgZXZhbHVhdGluZyB0aGUgY29kZSBiZWZvcmUgdGhlXFxuYnJvd3NlciBoYXMgYSBjaGFuY2UgdG8gcmVhY2ggdGhlIDxib2R5Pi5cXG5QbGVhc2UgbW92ZSB0aGUgaW1wb3J0IGF0IHRoZSBlbmQgb2YgdGhlIDxib2R5Pi5cXG4gICcpIDogdm9pZCAwO1xuICByZXR1cm4gY29udGFpbmVyO1xufVxuLyoqXG4gKiBTdGF0ZSBtYW5hZ2VtZW50IGhlbHBlciBmb3IgbW9kYWxzL2xheWVycy5cbiAqIFNpbXBsaWZpZWQsIGJ1dCBpbnNwaXJlZCBieSByZWFjdC1vdmVybGF5J3MgTW9kYWxNYW5hZ2VyIGNsYXNzXG4gKlxuICogQGludGVybmFsIFVzZWQgYnkgdGhlIE1vZGFsIHRvIGVuc3VyZSBwcm9wZXIgZm9jdXMgbWFuYWdlbWVudC5cbiAqL1xuZnVuY3Rpb24gY3JlYXRlTW9kYWxNYW5hZ2VyKCkge1xuICB2YXIgX3JlZiA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDoge30sXG4gICAgICBfcmVmJGhpZGVTaWJsaW5nTm9kZXMgPSBfcmVmLmhpZGVTaWJsaW5nTm9kZXMsXG4gICAgICBoaWRlU2libGluZ05vZGVzID0gX3JlZiRoaWRlU2libGluZ05vZGVzID09PSB1bmRlZmluZWQgPyB0cnVlIDogX3JlZiRoaWRlU2libGluZ05vZGVzO1xuXG4gIHZhciBtb2RhbHMgPSBbXTtcblxuICB2YXIgcHJldk92ZXJmbG93ID0gdm9pZCAwO1xuICB2YXIgcHJldlBhZGRpbmdzID0gW107XG5cbiAgZnVuY3Rpb24gYWRkKG1vZGFsKSB7XG4gICAgdmFyIGNvbnRhaW5lciA9IGdldENvbnRhaW5lcigpO1xuICAgIHZhciBtb2RhbElkeCA9IG1vZGFscy5pbmRleE9mKG1vZGFsKTtcblxuICAgIGlmIChtb2RhbElkeCAhPT0gLTEpIHtcbiAgICAgIHJldHVybiBtb2RhbElkeDtcbiAgICB9XG5cbiAgICBtb2RhbElkeCA9IG1vZGFscy5sZW5ndGg7XG4gICAgbW9kYWxzLnB1c2gobW9kYWwpO1xuXG4gICAgaWYgKGhpZGVTaWJsaW5nTm9kZXMpIHtcbiAgICAgICgwLCBfbWFuYWdlQXJpYUhpZGRlbi5oaWRlU2libGluZ3MpKGNvbnRhaW5lciwgbW9kYWwubW91bnROb2RlKTtcbiAgICB9XG5cbiAgICBpZiAobW9kYWxzLmxlbmd0aCA9PT0gMSkge1xuICAgICAgLy8gU2F2ZSBvdXIgY3VycmVudCBvdmVyZmxvdyBzbyB3ZSBjYW4gcmV2ZXJ0XG4gICAgICAvLyBiYWNrIHRvIGl0IHdoZW4gYWxsIG1vZGFscyBhcmUgY2xvc2VkIVxuICAgICAgcHJldk92ZXJmbG93ID0gY29udGFpbmVyLnN0eWxlLm92ZXJmbG93O1xuXG4gICAgICBpZiAoYm9keUlzT3ZlcmZsb3dpbmcoY29udGFpbmVyKSkge1xuICAgICAgICBwcmV2UGFkZGluZ3MgPSBbZ2V0UGFkZGluZ1JpZ2h0KGNvbnRhaW5lcildO1xuICAgICAgICB2YXIgc2Nyb2xsYmFyU2l6ZSA9ICgwLCBfc2Nyb2xsYmFyU2l6ZTIuZGVmYXVsdCkoKTtcbiAgICAgICAgY29udGFpbmVyLnN0eWxlLnBhZGRpbmdSaWdodCA9IHByZXZQYWRkaW5nc1swXSArIHNjcm9sbGJhclNpemUgKyAncHgnO1xuXG4gICAgICAgIHZhciBmaXhlZE5vZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm11aS1maXhlZCcpO1xuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZpeGVkTm9kZXMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICB2YXIgcGFkZGluZ1JpZ2h0ID0gZ2V0UGFkZGluZ1JpZ2h0KGZpeGVkTm9kZXNbaV0pO1xuICAgICAgICAgIHByZXZQYWRkaW5ncy5wdXNoKHBhZGRpbmdSaWdodCk7XG4gICAgICAgICAgZml4ZWROb2Rlc1tpXS5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwYWRkaW5nUmlnaHQgKyBzY3JvbGxiYXJTaXplICsgJ3B4JztcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBjb250YWluZXIuc3R5bGUub3ZlcmZsb3cgPSAnaGlkZGVuJztcbiAgICB9XG5cbiAgICByZXR1cm4gbW9kYWxJZHg7XG4gIH1cblxuICBmdW5jdGlvbiByZW1vdmUobW9kYWwpIHtcbiAgICB2YXIgY29udGFpbmVyID0gZ2V0Q29udGFpbmVyKCk7XG4gICAgdmFyIG1vZGFsSWR4ID0gbW9kYWxzLmluZGV4T2YobW9kYWwpO1xuXG4gICAgaWYgKG1vZGFsSWR4ID09PSAtMSkge1xuICAgICAgcmV0dXJuIG1vZGFsSWR4O1xuICAgIH1cblxuICAgIG1vZGFscy5zcGxpY2UobW9kYWxJZHgsIDEpO1xuXG4gICAgaWYgKG1vZGFscy5sZW5ndGggPT09IDApIHtcbiAgICAgIGNvbnRhaW5lci5zdHlsZS5vdmVyZmxvdyA9IHByZXZPdmVyZmxvdztcbiAgICAgIGNvbnRhaW5lci5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwcmV2UGFkZGluZ3NbMF07XG5cbiAgICAgIHZhciBmaXhlZE5vZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLm11aS1maXhlZCcpO1xuICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaXhlZE5vZGVzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICAgIGZpeGVkTm9kZXNbaV0uc3R5bGUucGFkZGluZ1JpZ2h0ID0gcHJldlBhZGRpbmdzW2kgKyAxXSArICdweCc7XG4gICAgICB9XG5cbiAgICAgIHByZXZPdmVyZmxvdyA9IHVuZGVmaW5lZDtcbiAgICAgIHByZXZQYWRkaW5ncyA9IFtdO1xuICAgICAgaWYgKGhpZGVTaWJsaW5nTm9kZXMpIHtcbiAgICAgICAgKDAsIF9tYW5hZ2VBcmlhSGlkZGVuLnNob3dTaWJsaW5ncykoY29udGFpbmVyLCBtb2RhbC5tb3VudE5vZGUpO1xuICAgICAgfVxuICAgIH0gZWxzZSBpZiAoaGlkZVNpYmxpbmdOb2Rlcykge1xuICAgICAgLy8gb3RoZXJ3aXNlIG1ha2Ugc3VyZSB0aGUgbmV4dCB0b3AgbW9kYWwgaXMgdmlzaWJsZSB0byBhIFNSXG4gICAgICAoMCwgX21hbmFnZUFyaWFIaWRkZW4uYXJpYUhpZGRlbikoZmFsc2UsIG1vZGFsc1ttb2RhbHMubGVuZ3RoIC0gMV0ubW91bnROb2RlKTtcbiAgICB9XG5cbiAgICByZXR1cm4gbW9kYWxJZHg7XG4gIH1cblxuICBmdW5jdGlvbiBpc1RvcE1vZGFsKG1vZGFsKSB7XG4gICAgcmV0dXJuICEhbW9kYWxzLmxlbmd0aCAmJiBtb2RhbHNbbW9kYWxzLmxlbmd0aCAtIDFdID09PSBtb2RhbDtcbiAgfVxuXG4gIHZhciBtb2RhbE1hbmFnZXIgPSB7IGFkZDogYWRkLCByZW1vdmU6IHJlbW92ZSwgaXNUb3BNb2RhbDogaXNUb3BNb2RhbCB9O1xuXG4gIHJldHVybiBtb2RhbE1hbmFnZXI7XG59XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZU1vZGFsTWFuYWdlcjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9tb2RhbE1hbmFnZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL21vZGFsTWFuYWdlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcmVhY3REb20gPSByZXF1aXJlKCdyZWFjdC1kb20nKTtcblxudmFyIF9yZWFjdERvbTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdERvbSk7XG5cbnZhciBfaW5ET00gPSByZXF1aXJlKCdkb20taGVscGVycy91dGlsL2luRE9NJyk7XG5cbnZhciBfaW5ET00yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5ET00pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCB0byBwb3J0YWwgaW4gb3JkZXIgdG8gZXNjYXBlIHRoZSBwYXJlbnQgRE9NIG5vZGUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAgdGhlIGNoaWxkcmVuIHdpbGwgYmUgbW91bnRlZCBpbnRvIHRoZSBET00uXG4gICAqL1xuICBvcGVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgUG9ydGFsID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoUG9ydGFsLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBQb3J0YWwoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgUG9ydGFsKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IFBvcnRhbC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoUG9ydGFsKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMubGF5ZXIgPSBudWxsLCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoUG9ydGFsLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudERpZE1vdW50JyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgdGhpcy5yZW5kZXJMYXllcigpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudERpZFVwZGF0ZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZFVwZGF0ZSgpIHtcbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcbiAgICAgIGlmICghX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbCkge1xuICAgICAgICB0aGlzLnJlbmRlckxheWVyKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50V2lsbFVubW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVW5tb3VudCgpIHtcbiAgICAgIHRoaXMudW5yZW5kZXJMYXllcigpO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2dldExheWVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gZ2V0TGF5ZXIoKSB7XG4gICAgICBpZiAoIXRoaXMubGF5ZXIpIHtcbiAgICAgICAgdGhpcy5sYXllciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0aGlzLmxheWVyLnNldEF0dHJpYnV0ZSgnZGF0YS1tdWktcG9ydGFsJywgJ3RydWUnKTtcbiAgICAgICAgaWYgKGRvY3VtZW50LmJvZHkgJiYgdGhpcy5sYXllcikge1xuICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQodGhpcy5sYXllcik7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMubGF5ZXI7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndW5yZW5kZXJMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHVucmVuZGVyTGF5ZXIoKSB7XG4gICAgICBpZiAoIXRoaXMubGF5ZXIpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICAvLyBTdXBwb3J0IHJlYWN0QDE1LngsIHdpbGwgYmUgcmVtb3ZlZCBhdCBzb21lIHBvaW50XG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgX3JlYWN0RG9tMi5kZWZhdWx0LnVubW91bnRDb21wb25lbnRBdE5vZGUodGhpcy5sYXllcik7XG4gICAgICB9XG5cbiAgICAgIGlmIChkb2N1bWVudC5ib2R5KSB7XG4gICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQodGhpcy5sYXllcik7XG4gICAgICB9XG4gICAgICB0aGlzLmxheWVyID0gbnVsbDtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXJMYXllcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckxheWVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb3BlbiA9IF9wcm9wcy5vcGVuO1xuXG5cbiAgICAgIGlmIChvcGVuKSB7XG4gICAgICAgIC8vIEJ5IGNhbGxpbmcgdGhpcyBtZXRob2QgaW4gY29tcG9uZW50RGlkTW91bnQoKSBhbmRcbiAgICAgICAgLy8gY29tcG9uZW50RGlkVXBkYXRlKCksIHlvdSdyZSBlZmZlY3RpdmVseSBjcmVhdGluZyBhIFwid29ybWhvbGVcIiB0aGF0XG4gICAgICAgIC8vIGZ1bm5lbHMgUmVhY3QncyBoaWVyYXJjaGljYWwgdXBkYXRlcyB0aHJvdWdoIHRvIGEgRE9NIG5vZGUgb24gYW5cbiAgICAgICAgLy8gZW50aXJlbHkgZGlmZmVyZW50IHBhcnQgb2YgdGhlIHBhZ2UuXG4gICAgICAgIHZhciBsYXllckVsZW1lbnQgPSBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ub25seShjaGlsZHJlbik7XG4gICAgICAgIF9yZWFjdERvbTIuZGVmYXVsdC51bnN0YWJsZV9yZW5kZXJTdWJ0cmVlSW50b0NvbnRhaW5lcih0aGlzLCBsYXllckVsZW1lbnQsIHRoaXMuZ2V0TGF5ZXIoKSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzMiA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMyLmNoaWxkcmVuLFxuICAgICAgICAgIG9wZW4gPSBfcHJvcHMyLm9wZW47XG5cbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcblxuICAgICAgaWYgKCFfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICAvLyBDYW4ndCBiZSByZW5kZXJlZCBzZXJ2ZXItc2lkZS5cbiAgICAgIGlmIChfaW5ET00yLmRlZmF1bHQpIHtcbiAgICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgICB2YXIgbGF5ZXIgPSB0aGlzLmdldExheWVyKCk7XG4gICAgICAgICAgLy8gJEZsb3dGaXhNZSBsYXllciBpcyBub24tbnVsbFxuICAgICAgICAgIHJldHVybiBfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKGNoaWxkcmVuLCBsYXllcik7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnVucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBQb3J0YWw7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Qb3J0YWwuZGVmYXVsdFByb3BzID0ge1xuICBvcGVuOiBmYWxzZVxufTtcblBvcnRhbC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyB7XG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59IDoge307XG5leHBvcnRzLmRlZmF1bHQgPSBQb3J0YWw7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvUG9ydGFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC9Qb3J0YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5vbmVPZlR5cGUoW3JlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlciwgcmVxdWlyZShcInByb3AtdHlwZXNcIikuc2hhcGUoe1xuICBlbnRlcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLmlzUmVxdWlyZWQsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlci5pc1JlcXVpcmVkXG59KV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZShcInByb3AtdHlwZXNcIikuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0ge1xuICBhcHBlYXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgYXBwZWFyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZXhpdEFjdGl2ZTogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nXG59O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDExIDI3IDI4IDM0IDM1IDM2IDM3IDM4IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX1RyYW5zaXRpb24gPSByZXF1aXJlKCdyZWFjdC10cmFuc2l0aW9uLWdyb3VwL1RyYW5zaXRpb24nKTtcblxudmFyIF9UcmFuc2l0aW9uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RyYW5zaXRpb24pO1xuXG52YXIgX3RyYW5zaXRpb25zID0gcmVxdWlyZSgnLi4vc3R5bGVzL3RyYW5zaXRpb25zJyk7XG5cbnZhciBfd2l0aFRoZW1lID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhUaGVtZScpO1xuXG52YXIgX3dpdGhUaGVtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoVGhlbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBUcmFuc2l0aW9uXG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGFwcGVhcjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQSBzaW5nbGUgY2hpbGQgY29udGVudCBlbGVtZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudC5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjb21wb25lbnQgd2lsbCB0cmFuc2l0aW9uIGluLlxuICAgKi9cbiAgaW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcmluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgdGltZW91dDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbi5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uKS5pc1JlcXVpcmVkXG59O1xuXG5cbnZhciByZWZsb3cgPSBmdW5jdGlvbiByZWZsb3cobm9kZSkge1xuICByZXR1cm4gbm9kZS5zY3JvbGxUb3A7XG59O1xuXG4vKipcbiAqIFRoZSBGYWRlIHRyYW5zaXRpb24gaXMgdXNlZCBieSB0aGUgTW9kYWwgY29tcG9uZW50LlxuICogSXQncyB1c2luZyBbcmVhY3QtdHJhbnNpdGlvbi1ncm91cF0oaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0anMvcmVhY3QtdHJhbnNpdGlvbi1ncm91cCkgaW50ZXJuYWxseS5cbiAqL1xuXG52YXIgRmFkZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEZhZGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEZhZGUoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgRmFkZSk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBGYWRlLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShGYWRlKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgcmVmbG93KG5vZGUpO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUVudGVyaW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMudGhlbWUsXG4gICAgICAgICAgdGltZW91dCA9IF90aGlzJHByb3BzLnRpbWVvdXQ7XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmVudGVyXG4gICAgICB9KTtcbiAgICAgIC8vICRGbG93Rml4TWUgLSBodHRwczovL2dpdGh1Yi5jb20vZmFjZWJvb2svZmxvdy9wdWxsLzUxNjFcbiAgICAgIG5vZGUuc3R5bGUud2Via2l0VHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmVudGVyXG4gICAgICB9KTtcbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcxJztcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcmluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMyLnRpbWVvdXQ7XG5cbiAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbiA9IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnb3BhY2l0eScsIHtcbiAgICAgICAgZHVyYXRpb246IHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJyA/IHRpbWVvdXQgOiB0aW1lb3V0LmV4aXRcbiAgICAgIH0pO1xuICAgICAgLy8gJEZsb3dGaXhNZSAtIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9mbG93L3B1bGwvNTE2MVxuICAgICAgbm9kZS5zdHlsZS53ZWJraXRUcmFuc2l0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5Jywge1xuICAgICAgICBkdXJhdGlvbjogdHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInID8gdGltZW91dCA6IHRpbWVvdXQuZXhpdFxuICAgICAgfSk7XG4gICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMCc7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXQpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0KG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShGYWRlLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFwcGVhciA9IF9wcm9wcy5hcHBlYXIsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wcy5vbkVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmcgPSBfcHJvcHMub25FbnRlcmluZyxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMub25FeGl0LFxuICAgICAgICAgIHN0eWxlUHJvcCA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhcHBlYXInLCAnY2hpbGRyZW4nLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRXhpdCcsICdzdHlsZScsICd0aGVtZSddKTtcblxuXG4gICAgICB2YXIgc3R5bGUgPSAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBzdHlsZVByb3ApO1xuXG4gICAgICAvLyBGb3Igc2VydmVyIHNpZGUgcmVuZGVyaW5nLlxuICAgICAgaWYgKCF0aGlzLnByb3BzLmluIHx8IGFwcGVhcikge1xuICAgICAgICBzdHlsZS5vcGFjaXR5ID0gJzAnO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9UcmFuc2l0aW9uMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhcHBlYXI6IGFwcGVhcixcbiAgICAgICAgICBzdHlsZTogc3R5bGUsXG4gICAgICAgICAgb25FbnRlcjogdGhpcy5oYW5kbGVFbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nOiB0aGlzLmhhbmRsZUVudGVyaW5nLFxuICAgICAgICAgIG9uRXhpdDogdGhpcy5oYW5kbGVFeGl0XG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBGYWRlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuRmFkZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGFwcGVhcjogdHJ1ZSxcbiAgdGltZW91dDoge1xuICAgIGVudGVyOiBfdHJhbnNpdGlvbnMuZHVyYXRpb24uZW50ZXJpbmdTY3JlZW4sXG4gICAgZXhpdDogX3RyYW5zaXRpb25zLmR1cmF0aW9uLmxlYXZpbmdTY3JlZW5cbiAgfVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFRoZW1lMi5kZWZhdWx0KSgpKEZhZGUpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0ZhZGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0ZhZGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5hcmlhSGlkZGVuID0gYXJpYUhpZGRlbjtcbmV4cG9ydHMuaGlkZVNpYmxpbmdzID0gaGlkZVNpYmxpbmdzO1xuZXhwb3J0cy5zaG93U2libGluZ3MgPSBzaG93U2libGluZ3M7XG4vLyAgd2Vha1xuXG52YXIgQkxBQ0tMSVNUID0gWyd0ZW1wbGF0ZScsICdzY3JpcHQnLCAnc3R5bGUnXTtcblxudmFyIGlzSGlkYWJsZSA9IGZ1bmN0aW9uIGlzSGlkYWJsZShfcmVmKSB7XG4gIHZhciBub2RlVHlwZSA9IF9yZWYubm9kZVR5cGUsXG4gICAgICB0YWdOYW1lID0gX3JlZi50YWdOYW1lO1xuICByZXR1cm4gbm9kZVR5cGUgPT09IDEgJiYgQkxBQ0tMSVNULmluZGV4T2YodGFnTmFtZS50b0xvd2VyQ2FzZSgpKSA9PT0gLTE7XG59O1xuXG52YXIgc2libGluZ3MgPSBmdW5jdGlvbiBzaWJsaW5ncyhjb250YWluZXIsIG1vdW50LCBjYikge1xuICBtb3VudCA9IFtdLmNvbmNhdChtb3VudCk7IC8vIGVzbGludC1kaXNhYmxlLWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgW10uZm9yRWFjaC5jYWxsKGNvbnRhaW5lci5jaGlsZHJlbiwgZnVuY3Rpb24gKG5vZGUpIHtcbiAgICBpZiAobW91bnQuaW5kZXhPZihub2RlKSA9PT0gLTEgJiYgaXNIaWRhYmxlKG5vZGUpKSB7XG4gICAgICBjYihub2RlKTtcbiAgICB9XG4gIH0pO1xufTtcblxuZnVuY3Rpb24gYXJpYUhpZGRlbihzaG93LCBub2RlKSB7XG4gIGlmICghbm9kZSkge1xuICAgIHJldHVybjtcbiAgfVxuICBpZiAoc2hvdykge1xuICAgIG5vZGUuc2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XG4gIH0gZWxzZSB7XG4gICAgbm9kZS5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJyk7XG4gIH1cbn1cblxuZnVuY3Rpb24gaGlkZVNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlKSB7XG4gIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBhcmlhSGlkZGVuKHRydWUsIG5vZGUpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gc2hvd1NpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlKSB7XG4gIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnROb2RlLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIHJldHVybiBhcmlhSGlkZGVuKGZhbHNlLCBub2RlKTtcbiAgfSk7XG59XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdXRpbHMvbWFuYWdlQXJpYUhpZGRlbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfaW52YXJpYW50ID0gcmVxdWlyZSgnaW52YXJpYW50Jyk7XG5cbnZhciBfaW52YXJpYW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ludmFyaWFudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgaXNNb2RpZmllZEV2ZW50ID0gZnVuY3Rpb24gaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB7XG4gIHJldHVybiAhIShldmVudC5tZXRhS2V5IHx8IGV2ZW50LmFsdEtleSB8fCBldmVudC5jdHJsS2V5IHx8IGV2ZW50LnNoaWZ0S2V5KTtcbn07XG5cbi8qKlxuICogVGhlIHB1YmxpYyBBUEkgZm9yIHJlbmRlcmluZyBhIGhpc3RvcnktYXdhcmUgPGE+LlxuICovXG5cbnZhciBMaW5rID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKExpbmssIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpbmsoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMaW5rKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5oYW5kbGVDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2xpY2spIF90aGlzLnByb3BzLm9uQ2xpY2soZXZlbnQpO1xuXG4gICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgJiYgLy8gb25DbGljayBwcmV2ZW50ZWQgZGVmYXVsdFxuICAgICAgZXZlbnQuYnV0dG9uID09PSAwICYmIC8vIGlnbm9yZSByaWdodCBjbGlja3NcbiAgICAgICFfdGhpcy5wcm9wcy50YXJnZXQgJiYgLy8gbGV0IGJyb3dzZXIgaGFuZGxlIFwidGFyZ2V0PV9ibGFua1wiIGV0Yy5cbiAgICAgICFpc01vZGlmaWVkRXZlbnQoZXZlbnQpIC8vIGlnbm9yZSBjbGlja3Mgd2l0aCBtb2RpZmllciBrZXlzXG4gICAgICApIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgdmFyIGhpc3RvcnkgPSBfdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5O1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgICByZXBsYWNlID0gX3RoaXMkcHJvcHMucmVwbGFjZSxcbiAgICAgICAgICAgICAgdG8gPSBfdGhpcyRwcm9wcy50bztcblxuXG4gICAgICAgICAgaWYgKHJlcGxhY2UpIHtcbiAgICAgICAgICAgIGhpc3RvcnkucmVwbGFjZSh0byk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGhpc3RvcnkucHVzaCh0byk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBMaW5rLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHJlcGxhY2UgPSBfcHJvcHMucmVwbGFjZSxcbiAgICAgICAgdG8gPSBfcHJvcHMudG8sXG4gICAgICAgIGlubmVyUmVmID0gX3Byb3BzLmlubmVyUmVmLFxuICAgICAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsncmVwbGFjZScsICd0bycsICdpbm5lclJlZiddKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgKDAsIF9pbnZhcmlhbnQyLmRlZmF1bHQpKHRoaXMuY29udGV4dC5yb3V0ZXIsICdZb3Ugc2hvdWxkIG5vdCB1c2UgPExpbms+IG91dHNpZGUgYSA8Um91dGVyPicpO1xuXG4gICAgdmFyIGhyZWYgPSB0aGlzLmNvbnRleHQucm91dGVyLmhpc3RvcnkuY3JlYXRlSHJlZih0eXBlb2YgdG8gPT09ICdzdHJpbmcnID8geyBwYXRobmFtZTogdG8gfSA6IHRvKTtcblxuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnYScsIF9leHRlbmRzKHt9LCBwcm9wcywgeyBvbkNsaWNrOiB0aGlzLmhhbmRsZUNsaWNrLCBocmVmOiBocmVmLCByZWY6IGlubmVyUmVmIH0pKTtcbiAgfTtcblxuICByZXR1cm4gTGluaztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpbmsucHJvcFR5cGVzID0ge1xuICBvbkNsaWNrOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIHRhcmdldDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG4gIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgdG86IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3RdKS5pc1JlcXVpcmVkLFxuICBpbm5lclJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNdKVxufTtcbkxpbmsuZGVmYXVsdFByb3BzID0ge1xuICByZXBsYWNlOiBmYWxzZVxufTtcbkxpbmsuY29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgIGhpc3Rvcnk6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgICAgcHVzaDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIGNyZWF0ZUhyZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkXG4gICAgfSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gTGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI2IDI3IDI4IDMwIDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDU3IDU4IDU5IDYxIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3dhcm5pbmcgPSByZXF1aXJlKCd3YXJuaW5nJyk7XG5cbnZhciBfd2FybmluZzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93YXJuaW5nKTtcblxudmFyIF9pbnZhcmlhbnQgPSByZXF1aXJlKCdpbnZhcmlhbnQnKTtcblxudmFyIF9pbnZhcmlhbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW52YXJpYW50KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9tYXRjaFBhdGggPSByZXF1aXJlKCcuL21hdGNoUGF0aCcpO1xuXG52YXIgX21hdGNoUGF0aDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9tYXRjaFBhdGgpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBpc0VtcHR5Q2hpbGRyZW4gPSBmdW5jdGlvbiBpc0VtcHR5Q2hpbGRyZW4oY2hpbGRyZW4pIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5jb3VudChjaGlsZHJlbikgPT09IDA7XG59O1xuXG4vKipcbiAqIFRoZSBwdWJsaWMgQVBJIGZvciBtYXRjaGluZyBhIHNpbmdsZSBwYXRoIGFuZCByZW5kZXJpbmcuXG4gKi9cblxudmFyIFJvdXRlID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKFJvdXRlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBSb3V0ZSgpIHtcbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFJvdXRlKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5zdGF0ZSA9IHtcbiAgICAgIG1hdGNoOiBfdGhpcy5jb21wdXRlTWF0Y2goX3RoaXMucHJvcHMsIF90aGlzLmNvbnRleHQucm91dGVyKVxuICAgIH0sIF90ZW1wKSwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgUm91dGUucHJvdG90eXBlLmdldENoaWxkQ29udGV4dCA9IGZ1bmN0aW9uIGdldENoaWxkQ29udGV4dCgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgcm91dGVyOiBfZXh0ZW5kcyh7fSwgdGhpcy5jb250ZXh0LnJvdXRlciwge1xuICAgICAgICByb3V0ZToge1xuICAgICAgICAgIGxvY2F0aW9uOiB0aGlzLnByb3BzLmxvY2F0aW9uIHx8IHRoaXMuY29udGV4dC5yb3V0ZXIucm91dGUubG9jYXRpb24sXG4gICAgICAgICAgbWF0Y2g6IHRoaXMuc3RhdGUubWF0Y2hcbiAgICAgICAgfVxuICAgICAgfSlcbiAgICB9O1xuICB9O1xuXG4gIFJvdXRlLnByb3RvdHlwZS5jb21wdXRlTWF0Y2ggPSBmdW5jdGlvbiBjb21wdXRlTWF0Y2goX3JlZiwgcm91dGVyKSB7XG4gICAgdmFyIGNvbXB1dGVkTWF0Y2ggPSBfcmVmLmNvbXB1dGVkTWF0Y2gsXG4gICAgICAgIGxvY2F0aW9uID0gX3JlZi5sb2NhdGlvbixcbiAgICAgICAgcGF0aCA9IF9yZWYucGF0aCxcbiAgICAgICAgc3RyaWN0ID0gX3JlZi5zdHJpY3QsXG4gICAgICAgIGV4YWN0ID0gX3JlZi5leGFjdCxcbiAgICAgICAgc2Vuc2l0aXZlID0gX3JlZi5zZW5zaXRpdmU7XG5cbiAgICBpZiAoY29tcHV0ZWRNYXRjaCkgcmV0dXJuIGNvbXB1dGVkTWF0Y2g7IC8vIDxTd2l0Y2g+IGFscmVhZHkgY29tcHV0ZWQgdGhlIG1hdGNoIGZvciB1c1xuXG4gICAgKDAsIF9pbnZhcmlhbnQyLmRlZmF1bHQpKHJvdXRlciwgJ1lvdSBzaG91bGQgbm90IHVzZSA8Um91dGU+IG9yIHdpdGhSb3V0ZXIoKSBvdXRzaWRlIGEgPFJvdXRlcj4nKTtcblxuICAgIHZhciByb3V0ZSA9IHJvdXRlci5yb3V0ZTtcblxuICAgIHZhciBwYXRobmFtZSA9IChsb2NhdGlvbiB8fCByb3V0ZS5sb2NhdGlvbikucGF0aG5hbWU7XG5cbiAgICByZXR1cm4gcGF0aCA/ICgwLCBfbWF0Y2hQYXRoMi5kZWZhdWx0KShwYXRobmFtZSwgeyBwYXRoOiBwYXRoLCBzdHJpY3Q6IHN0cmljdCwgZXhhY3Q6IGV4YWN0LCBzZW5zaXRpdmU6IHNlbnNpdGl2ZSB9KSA6IHJvdXRlLm1hdGNoO1xuICB9O1xuXG4gIFJvdXRlLnByb3RvdHlwZS5jb21wb25lbnRXaWxsTW91bnQgPSBmdW5jdGlvbiBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgKDAsIF93YXJuaW5nMi5kZWZhdWx0KSghKHRoaXMucHJvcHMuY29tcG9uZW50ICYmIHRoaXMucHJvcHMucmVuZGVyKSwgJ1lvdSBzaG91bGQgbm90IHVzZSA8Um91dGUgY29tcG9uZW50PiBhbmQgPFJvdXRlIHJlbmRlcj4gaW4gdGhlIHNhbWUgcm91dGU7IDxSb3V0ZSByZW5kZXI+IHdpbGwgYmUgaWdub3JlZCcpO1xuXG4gICAgKDAsIF93YXJuaW5nMi5kZWZhdWx0KSghKHRoaXMucHJvcHMuY29tcG9uZW50ICYmIHRoaXMucHJvcHMuY2hpbGRyZW4gJiYgIWlzRW1wdHlDaGlsZHJlbih0aGlzLnByb3BzLmNoaWxkcmVuKSksICdZb3Ugc2hvdWxkIG5vdCB1c2UgPFJvdXRlIGNvbXBvbmVudD4gYW5kIDxSb3V0ZSBjaGlsZHJlbj4gaW4gdGhlIHNhbWUgcm91dGU7IDxSb3V0ZSBjaGlsZHJlbj4gd2lsbCBiZSBpZ25vcmVkJyk7XG5cbiAgICAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKCEodGhpcy5wcm9wcy5yZW5kZXIgJiYgdGhpcy5wcm9wcy5jaGlsZHJlbiAmJiAhaXNFbXB0eUNoaWxkcmVuKHRoaXMucHJvcHMuY2hpbGRyZW4pKSwgJ1lvdSBzaG91bGQgbm90IHVzZSA8Um91dGUgcmVuZGVyPiBhbmQgPFJvdXRlIGNoaWxkcmVuPiBpbiB0aGUgc2FtZSByb3V0ZTsgPFJvdXRlIGNoaWxkcmVuPiB3aWxsIGJlIGlnbm9yZWQnKTtcbiAgfTtcblxuICBSb3V0ZS5wcm90b3R5cGUuY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyA9IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMobmV4dFByb3BzLCBuZXh0Q29udGV4dCkge1xuICAgICgwLCBfd2FybmluZzIuZGVmYXVsdCkoIShuZXh0UHJvcHMubG9jYXRpb24gJiYgIXRoaXMucHJvcHMubG9jYXRpb24pLCAnPFJvdXRlPiBlbGVtZW50cyBzaG91bGQgbm90IGNoYW5nZSBmcm9tIHVuY29udHJvbGxlZCB0byBjb250cm9sbGVkIChvciB2aWNlIHZlcnNhKS4gWW91IGluaXRpYWxseSB1c2VkIG5vIFwibG9jYXRpb25cIiBwcm9wIGFuZCB0aGVuIHByb3ZpZGVkIG9uZSBvbiBhIHN1YnNlcXVlbnQgcmVuZGVyLicpO1xuXG4gICAgKDAsIF93YXJuaW5nMi5kZWZhdWx0KSghKCFuZXh0UHJvcHMubG9jYXRpb24gJiYgdGhpcy5wcm9wcy5sb2NhdGlvbiksICc8Um91dGU+IGVsZW1lbnRzIHNob3VsZCBub3QgY2hhbmdlIGZyb20gY29udHJvbGxlZCB0byB1bmNvbnRyb2xsZWQgKG9yIHZpY2UgdmVyc2EpLiBZb3UgcHJvdmlkZWQgYSBcImxvY2F0aW9uXCIgcHJvcCBpbml0aWFsbHkgYnV0IG9taXR0ZWQgaXQgb24gYSBzdWJzZXF1ZW50IHJlbmRlci4nKTtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgbWF0Y2g6IHRoaXMuY29tcHV0ZU1hdGNoKG5leHRQcm9wcywgbmV4dENvbnRleHQucm91dGVyKVxuICAgIH0pO1xuICB9O1xuXG4gIFJvdXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIG1hdGNoID0gdGhpcy5zdGF0ZS5tYXRjaDtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgIGNvbXBvbmVudCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgIHJlbmRlciA9IF9wcm9wcy5yZW5kZXI7XG4gICAgdmFyIF9jb250ZXh0JHJvdXRlciA9IHRoaXMuY29udGV4dC5yb3V0ZXIsXG4gICAgICAgIGhpc3RvcnkgPSBfY29udGV4dCRyb3V0ZXIuaGlzdG9yeSxcbiAgICAgICAgcm91dGUgPSBfY29udGV4dCRyb3V0ZXIucm91dGUsXG4gICAgICAgIHN0YXRpY0NvbnRleHQgPSBfY29udGV4dCRyb3V0ZXIuc3RhdGljQ29udGV4dDtcblxuICAgIHZhciBsb2NhdGlvbiA9IHRoaXMucHJvcHMubG9jYXRpb24gfHwgcm91dGUubG9jYXRpb247XG4gICAgdmFyIHByb3BzID0geyBtYXRjaDogbWF0Y2gsIGxvY2F0aW9uOiBsb2NhdGlvbiwgaGlzdG9yeTogaGlzdG9yeSwgc3RhdGljQ29udGV4dDogc3RhdGljQ29udGV4dCB9O1xuXG4gICAgcmV0dXJuIGNvbXBvbmVudCA/IC8vIGNvbXBvbmVudCBwcm9wIGdldHMgZmlyc3QgcHJpb3JpdHksIG9ubHkgY2FsbGVkIGlmIHRoZXJlJ3MgYSBtYXRjaFxuICAgIG1hdGNoID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoY29tcG9uZW50LCBwcm9wcykgOiBudWxsIDogcmVuZGVyID8gLy8gcmVuZGVyIHByb3AgaXMgbmV4dCwgb25seSBjYWxsZWQgaWYgdGhlcmUncyBhIG1hdGNoXG4gICAgbWF0Y2ggPyByZW5kZXIocHJvcHMpIDogbnVsbCA6IGNoaWxkcmVuID8gLy8gY2hpbGRyZW4gY29tZSBsYXN0LCBhbHdheXMgY2FsbGVkXG4gICAgdHlwZW9mIGNoaWxkcmVuID09PSAnZnVuY3Rpb24nID8gY2hpbGRyZW4ocHJvcHMpIDogIWlzRW1wdHlDaGlsZHJlbihjaGlsZHJlbikgPyBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ub25seShjaGlsZHJlbikgOiBudWxsIDogbnVsbDtcbiAgfTtcblxuICByZXR1cm4gUm91dGU7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Sb3V0ZS5wcm9wVHlwZXMgPSB7XG4gIGNvbXB1dGVkTWF0Y2g6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0LCAvLyBwcml2YXRlLCBmcm9tIDxTd2l0Y2g+XG4gIHBhdGg6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLFxuICBleGFjdDogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICBzdHJpY3Q6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgc2Vuc2l0aXZlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIGNvbXBvbmVudDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLFxuICByZW5kZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcbiAgY2hpbGRyZW46IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsIF9wcm9wVHlwZXMyLmRlZmF1bHQubm9kZV0pLFxuICBsb2NhdGlvbjogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3Rcbn07XG5Sb3V0ZS5jb250ZXh0VHlwZXMgPSB7XG4gIHJvdXRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgaGlzdG9yeTogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICByb3V0ZTogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBzdGF0aWNDb250ZXh0OiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdFxuICB9KVxufTtcblJvdXRlLmNoaWxkQ29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0LmlzUmVxdWlyZWRcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBSb3V0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvUm91dGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9Sb3V0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM1IDM3IDQwIDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3BhdGhUb1JlZ2V4cCA9IHJlcXVpcmUoJ3BhdGgtdG8tcmVnZXhwJyk7XG5cbnZhciBfcGF0aFRvUmVnZXhwMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3BhdGhUb1JlZ2V4cCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwYXR0ZXJuQ2FjaGUgPSB7fTtcbnZhciBjYWNoZUxpbWl0ID0gMTAwMDA7XG52YXIgY2FjaGVDb3VudCA9IDA7XG5cbnZhciBjb21waWxlUGF0aCA9IGZ1bmN0aW9uIGNvbXBpbGVQYXRoKHBhdHRlcm4sIG9wdGlvbnMpIHtcbiAgdmFyIGNhY2hlS2V5ID0gJycgKyBvcHRpb25zLmVuZCArIG9wdGlvbnMuc3RyaWN0ICsgb3B0aW9ucy5zZW5zaXRpdmU7XG4gIHZhciBjYWNoZSA9IHBhdHRlcm5DYWNoZVtjYWNoZUtleV0gfHwgKHBhdHRlcm5DYWNoZVtjYWNoZUtleV0gPSB7fSk7XG5cbiAgaWYgKGNhY2hlW3BhdHRlcm5dKSByZXR1cm4gY2FjaGVbcGF0dGVybl07XG5cbiAgdmFyIGtleXMgPSBbXTtcbiAgdmFyIHJlID0gKDAsIF9wYXRoVG9SZWdleHAyLmRlZmF1bHQpKHBhdHRlcm4sIGtleXMsIG9wdGlvbnMpO1xuICB2YXIgY29tcGlsZWRQYXR0ZXJuID0geyByZTogcmUsIGtleXM6IGtleXMgfTtcblxuICBpZiAoY2FjaGVDb3VudCA8IGNhY2hlTGltaXQpIHtcbiAgICBjYWNoZVtwYXR0ZXJuXSA9IGNvbXBpbGVkUGF0dGVybjtcbiAgICBjYWNoZUNvdW50Kys7XG4gIH1cblxuICByZXR1cm4gY29tcGlsZWRQYXR0ZXJuO1xufTtcblxuLyoqXG4gKiBQdWJsaWMgQVBJIGZvciBtYXRjaGluZyBhIFVSTCBwYXRobmFtZSB0byBhIHBhdGggcGF0dGVybi5cbiAqL1xudmFyIG1hdGNoUGF0aCA9IGZ1bmN0aW9uIG1hdGNoUGF0aChwYXRobmFtZSkge1xuICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAxICYmIGFyZ3VtZW50c1sxXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzFdIDoge307XG5cbiAgaWYgKHR5cGVvZiBvcHRpb25zID09PSAnc3RyaW5nJykgb3B0aW9ucyA9IHsgcGF0aDogb3B0aW9ucyB9O1xuXG4gIHZhciBfb3B0aW9ucyA9IG9wdGlvbnMsXG4gICAgICBfb3B0aW9ucyRwYXRoID0gX29wdGlvbnMucGF0aCxcbiAgICAgIHBhdGggPSBfb3B0aW9ucyRwYXRoID09PSB1bmRlZmluZWQgPyAnLycgOiBfb3B0aW9ucyRwYXRoLFxuICAgICAgX29wdGlvbnMkZXhhY3QgPSBfb3B0aW9ucy5leGFjdCxcbiAgICAgIGV4YWN0ID0gX29wdGlvbnMkZXhhY3QgPT09IHVuZGVmaW5lZCA/IGZhbHNlIDogX29wdGlvbnMkZXhhY3QsXG4gICAgICBfb3B0aW9ucyRzdHJpY3QgPSBfb3B0aW9ucy5zdHJpY3QsXG4gICAgICBzdHJpY3QgPSBfb3B0aW9ucyRzdHJpY3QgPT09IHVuZGVmaW5lZCA/IGZhbHNlIDogX29wdGlvbnMkc3RyaWN0LFxuICAgICAgX29wdGlvbnMkc2Vuc2l0aXZlID0gX29wdGlvbnMuc2Vuc2l0aXZlLFxuICAgICAgc2Vuc2l0aXZlID0gX29wdGlvbnMkc2Vuc2l0aXZlID09PSB1bmRlZmluZWQgPyBmYWxzZSA6IF9vcHRpb25zJHNlbnNpdGl2ZTtcblxuICB2YXIgX2NvbXBpbGVQYXRoID0gY29tcGlsZVBhdGgocGF0aCwgeyBlbmQ6IGV4YWN0LCBzdHJpY3Q6IHN0cmljdCwgc2Vuc2l0aXZlOiBzZW5zaXRpdmUgfSksXG4gICAgICByZSA9IF9jb21waWxlUGF0aC5yZSxcbiAgICAgIGtleXMgPSBfY29tcGlsZVBhdGgua2V5cztcblxuICB2YXIgbWF0Y2ggPSByZS5leGVjKHBhdGhuYW1lKTtcblxuICBpZiAoIW1hdGNoKSByZXR1cm4gbnVsbDtcblxuICB2YXIgdXJsID0gbWF0Y2hbMF0sXG4gICAgICB2YWx1ZXMgPSBtYXRjaC5zbGljZSgxKTtcblxuICB2YXIgaXNFeGFjdCA9IHBhdGhuYW1lID09PSB1cmw7XG5cbiAgaWYgKGV4YWN0ICYmICFpc0V4YWN0KSByZXR1cm4gbnVsbDtcblxuICByZXR1cm4ge1xuICAgIHBhdGg6IHBhdGgsIC8vIHRoZSBwYXRoIHBhdHRlcm4gdXNlZCB0byBtYXRjaFxuICAgIHVybDogcGF0aCA9PT0gJy8nICYmIHVybCA9PT0gJycgPyAnLycgOiB1cmwsIC8vIHRoZSBtYXRjaGVkIHBvcnRpb24gb2YgdGhlIFVSTFxuICAgIGlzRXhhY3Q6IGlzRXhhY3QsIC8vIHdoZXRoZXIgb3Igbm90IHdlIG1hdGNoZWQgZXhhY3RseVxuICAgIHBhcmFtczoga2V5cy5yZWR1Y2UoZnVuY3Rpb24gKG1lbW8sIGtleSwgaW5kZXgpIHtcbiAgICAgIG1lbW9ba2V5Lm5hbWVdID0gdmFsdWVzW2luZGV4XTtcbiAgICAgIHJldHVybiBtZW1vO1xuICAgIH0sIHt9KVxuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gbWF0Y2hQYXRoO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9tYXRjaFBhdGguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9tYXRjaFBhdGguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNSAzNyA0MCA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9ob2lzdE5vblJlYWN0U3RhdGljcyA9IHJlcXVpcmUoJ2hvaXN0LW5vbi1yZWFjdC1zdGF0aWNzJyk7XG5cbnZhciBfaG9pc3ROb25SZWFjdFN0YXRpY3MyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaG9pc3ROb25SZWFjdFN0YXRpY3MpO1xuXG52YXIgX1JvdXRlID0gcmVxdWlyZSgnLi9Sb3V0ZScpO1xuXG52YXIgX1JvdXRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1JvdXRlKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKG9iaiwga2V5cykgeyB2YXIgdGFyZ2V0ID0ge307IGZvciAodmFyIGkgaW4gb2JqKSB7IGlmIChrZXlzLmluZGV4T2YoaSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgaSkpIGNvbnRpbnVlOyB0YXJnZXRbaV0gPSBvYmpbaV07IH0gcmV0dXJuIHRhcmdldDsgfVxuXG4vKipcbiAqIEEgcHVibGljIGhpZ2hlci1vcmRlciBjb21wb25lbnQgdG8gYWNjZXNzIHRoZSBpbXBlcmF0aXZlIEFQSVxuICovXG52YXIgd2l0aFJvdXRlciA9IGZ1bmN0aW9uIHdpdGhSb3V0ZXIoQ29tcG9uZW50KSB7XG4gIHZhciBDID0gZnVuY3Rpb24gQyhwcm9wcykge1xuICAgIHZhciB3cmFwcGVkQ29tcG9uZW50UmVmID0gcHJvcHMud3JhcHBlZENvbXBvbmVudFJlZixcbiAgICAgICAgcmVtYWluaW5nUHJvcHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMocHJvcHMsIFsnd3JhcHBlZENvbXBvbmVudFJlZiddKTtcblxuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfUm91dGUyLmRlZmF1bHQsIHsgcmVuZGVyOiBmdW5jdGlvbiByZW5kZXIocm91dGVDb21wb25lbnRQcm9wcykge1xuICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBfZXh0ZW5kcyh7fSwgcmVtYWluaW5nUHJvcHMsIHJvdXRlQ29tcG9uZW50UHJvcHMsIHsgcmVmOiB3cmFwcGVkQ29tcG9uZW50UmVmIH0pKTtcbiAgICAgIH0gfSk7XG4gIH07XG5cbiAgQy5kaXNwbGF5TmFtZSA9ICd3aXRoUm91dGVyKCcgKyAoQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lKSArICcpJztcbiAgQy5XcmFwcGVkQ29tcG9uZW50ID0gQ29tcG9uZW50O1xuICBDLnByb3BUeXBlcyA9IHtcbiAgICB3cmFwcGVkQ29tcG9uZW50UmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNcbiAgfTtcblxuICByZXR1cm4gKDAsIF9ob2lzdE5vblJlYWN0U3RhdGljczIuZGVmYXVsdCkoQywgQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHdpdGhSb3V0ZXI7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL3dpdGhSb3V0ZXIuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci93aXRoUm91dGVyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzUgMzcgNDAgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IHJlcXVpcmUoJy4vdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbCcpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJFbGVtZW50VXRpbCk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IHJlcXVpcmUoJy4vaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCcpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBjcmVhdGVGYWN0b3J5ID0gZnVuY3Rpb24gY3JlYXRlRmFjdG9yeSh0eXBlKSB7XG4gIHZhciBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCA9ICgwLCBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIuZGVmYXVsdCkodHlwZSk7XG4gIHJldHVybiBmdW5jdGlvbiAocCwgYykge1xuICAgIHJldHVybiAoMCwgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwyLmRlZmF1bHQpKGZhbHNlLCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcCwgYyk7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVGYWN0b3J5O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBnZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIGdldERpc3BsYXlOYW1lKENvbXBvbmVudCkge1xuICBpZiAodHlwZW9mIENvbXBvbmVudCA9PT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gQ29tcG9uZW50O1xuICB9XG5cbiAgaWYgKCFDb21wb25lbnQpIHtcbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9XG5cbiAgcmV0dXJuIENvbXBvbmVudC5kaXNwbGF5TmFtZSB8fCBDb21wb25lbnQubmFtZSB8fCAnQ29tcG9uZW50Jztcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGdldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxudmFyIGlzQ2xhc3NDb21wb25lbnQgPSBmdW5jdGlvbiBpc0NsYXNzQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbihDb21wb25lbnQgJiYgQ29tcG9uZW50LnByb3RvdHlwZSAmJiBfdHlwZW9mKENvbXBvbmVudC5wcm90b3R5cGUuaXNSZWFjdENvbXBvbmVudCkgPT09ICdvYmplY3QnKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzQ2xhc3NDb21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudCA9IHJlcXVpcmUoJy4vaXNDbGFzc0NvbXBvbmVudCcpO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNDbGFzc0NvbXBvbmVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4odHlwZW9mIENvbXBvbmVudCA9PT0gJ2Z1bmN0aW9uJyAmJiAhKDAsIF9pc0NsYXNzQ29tcG9uZW50Mi5kZWZhdWx0KShDb21wb25lbnQpICYmICFDb21wb25lbnQuZGVmYXVsdFByb3BzICYmICFDb21wb25lbnQuY29udGV4dFR5cGVzICYmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ3Byb2R1Y3Rpb24nIHx8ICFDb21wb25lbnQucHJvcFR5cGVzKSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3Nob3VsZFVwZGF0ZSA9IHJlcXVpcmUoJy4vc2hvdWxkVXBkYXRlJyk7XG5cbnZhciBfc2hvdWxkVXBkYXRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Nob3VsZFVwZGF0ZSk7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnLi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHB1cmUgPSBmdW5jdGlvbiBwdXJlKEJhc2VDb21wb25lbnQpIHtcbiAgdmFyIGhvYyA9ICgwLCBfc2hvdWxkVXBkYXRlMi5kZWZhdWx0KShmdW5jdGlvbiAocHJvcHMsIG5leHRQcm9wcykge1xuICAgIHJldHVybiAhKDAsIF9zaGFsbG93RXF1YWwyLmRlZmF1bHQpKHByb3BzLCBuZXh0UHJvcHMpO1xuICB9KTtcblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3B1cmUnKSkoaG9jKEJhc2VDb21wb25lbnQpKTtcbiAgfVxuXG4gIHJldHVybiBob2MoQmFzZUNvbXBvbmVudCk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBwdXJlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NldFN0YXRpYyA9IHJlcXVpcmUoJy4vc2V0U3RhdGljJyk7XG5cbnZhciBfc2V0U3RhdGljMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldFN0YXRpYyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHNldERpc3BsYXlOYW1lKGRpc3BsYXlOYW1lKSB7XG4gIHJldHVybiAoMCwgX3NldFN0YXRpYzIuZGVmYXVsdCkoJ2Rpc3BsYXlOYW1lJywgZGlzcGxheU5hbWUpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIHNldFN0YXRpYyA9IGZ1bmN0aW9uIHNldFN0YXRpYyhrZXksIHZhbHVlKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgQmFzZUNvbXBvbmVudFtrZXldID0gdmFsdWU7XG4gICAgLyogZXNsaW50LWVuYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIHJldHVybiBCYXNlQ29tcG9uZW50O1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0U3RhdGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCdmYmpzL2xpYi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gX3NoYWxsb3dFcXVhbDIuZGVmYXVsdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeSA9IHJlcXVpcmUoJy4vY3JlYXRlRWFnZXJGYWN0b3J5Jyk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRmFjdG9yeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIHNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZFVwZGF0ZSh0ZXN0KSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIHZhciBmYWN0b3J5ID0gKDAsIF9jcmVhdGVFYWdlckZhY3RvcnkyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpO1xuXG4gICAgdmFyIFNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gICAgICBfaW5oZXJpdHMoU2hvdWxkVXBkYXRlLCBfQ29tcG9uZW50KTtcblxuICAgICAgZnVuY3Rpb24gU2hvdWxkVXBkYXRlKCkge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2hvdWxkVXBkYXRlKTtcblxuICAgICAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NvbXBvbmVudC5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgICAgIH1cblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5zaG91bGRDb21wb25lbnRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgICAgIHJldHVybiB0ZXN0KHRoaXMucHJvcHMsIG5leHRQcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIGZhY3RvcnkodGhpcy5wcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICAgIH0oX3JlYWN0LkNvbXBvbmVudCk7XG5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAnc2hvdWxkVXBkYXRlJykpKFNob3VsZFVwZGF0ZSk7XG4gICAgfVxuICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzaG91bGRVcGRhdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBjcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gZnVuY3Rpb24gY3JlYXRlRWFnZXJFbGVtZW50VXRpbChoYXNLZXksIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwcm9wcywgY2hpbGRyZW4pIHtcbiAgaWYgKCFoYXNLZXkgJiYgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQpIHtcbiAgICBpZiAoY2hpbGRyZW4pIHtcbiAgICAgIHJldHVybiB0eXBlKF9leHRlbmRzKHt9LCBwcm9wcywgeyBjaGlsZHJlbjogY2hpbGRyZW4gfSkpO1xuICAgIH1cbiAgICByZXR1cm4gdHlwZShwcm9wcyk7XG4gIH1cblxuICB2YXIgQ29tcG9uZW50ID0gdHlwZTtcblxuICBpZiAoY2hpbGRyZW4pIHtcbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICBDb21wb25lbnQsXG4gICAgICBwcm9wcyxcbiAgICAgIGNoaWxkcmVuXG4gICAgKTtcbiAgfVxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChDb21wb25lbnQsIHByb3BzKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWw7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZ2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL2dldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfZ2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0RGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgd3JhcERpc3BsYXlOYW1lID0gZnVuY3Rpb24gd3JhcERpc3BsYXlOYW1lKEJhc2VDb21wb25lbnQsIGhvY05hbWUpIHtcbiAgcmV0dXJuIGhvY05hbWUgKyAnKCcgKyAoMCwgX2dldERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KSArICcpJztcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHdyYXBEaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgQ09VUlNFX0NSRUFURSB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBQYXlsb2FkID0ge3xcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICB0aW1lc3RhbXA6IHN0cmluZyxcbnx9O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBjcmVhdGUgYWN0aW9uIHdpbGwgYWRkIG5ld2x5IGNyZWF0ZWQgY291cnNlIHRvIHN0b3JlLlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZSAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5hdXRob3JJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aW1lc3RhbXAgLVxuICpcbiAqIEByZXR1cm4ge09iamVjdH0gLVxuICovXG5jb25zdCBjcmVhdGUgPSAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoeyB0eXBlOiBDT1VSU0VfQ1JFQVRFLCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy9jcmVhdGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgQ09VUlNFX0RFTEVURSB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBQYXlsb2FkID0ge1xuICBjb3Vyc2VJZDogbnVtYmVyLFxuICBhdXRob3JJZDogbnVtYmVyLFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICpcbiAqIEBmdW5jdGlvbiBkZWxldGVDb3Vyc2VcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZUlkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmF1dGhvcklkIC1cbiAqIEByZXR1cm4ge09iamVjdH0gLVxuICovXG5leHBvcnQgZGVmYXVsdCAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoeyB0eXBlOiBDT1VSU0VfREVMRVRFLCBwYXlsb2FkIH0pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvZGVsZXRlLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IHtcbiAgRUxFTUVOVF9HRVRfU1RBUlQsXG4gIEVMRU1FTlRfR0VUX0VSUk9SLFxuICBFTEVNRU5UX0dFVF9TVUNDRVNTLFxufSBmcm9tICcuLi8uLi9jb25zdGFudHMvZWxlbWVudHMnO1xuXG5pbXBvcnQgYXBpR2V0QnlJZCBmcm9tICcuLi8uLi9hcGkvZWxlbWVudHMvZ2V0QnlJZCc7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5jb25zdCBnZXRFbGVtZW50U3RhcnQgPSAocGF5bG9hZCkgPT4gKHsgdHlwZTogRUxFTUVOVF9HRVRfU1RBUlQsIHBheWxvYWQgfSk7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5zdGF0dXNDb2RlXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudXNlcm5hbWVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmVsZW1lbnRUeXBlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jaXJjbGVUeXBlIC0gb25seSBpZiBlbGVtZW50VHlwZSBpcyAnY2lyY2xlJ1xuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmFtZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5hdmF0YXIgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY292ZXIgLVxuICogQHJldHVybiB7T2JqZWN0fVxuICovXG5jb25zdCBnZXRFbGVtZW50U3VjY2VzcyA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UX0dFVF9TVUNDRVNTLCBwYXlsb2FkIH0pO1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnN0YXR1c0NvZGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmVycm9yXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEVsZW1lbnRFcnJvciA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UX0dFVF9FUlJPUiwgcGF5bG9hZCB9KTtcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uIGdldFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICovXG5jb25zdCBnZXQgPSAoeyB0b2tlbiwgaWQgfSkgPT4gKGRpc3BhdGNoKSA9PiB7XG4gIGRpc3BhdGNoKGdldEVsZW1lbnRTdGFydCh7IGlkIH0pKTtcblxuICBhcGlHZXRCeUlkKHsgdG9rZW4sIGlkIH0pXG4gICAgLnRoZW4oKHsgc3RhdHVzQ29kZSwgZXJyb3IsIHBheWxvYWQgfSkgPT4ge1xuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIGRpc3BhdGNoKGdldEVsZW1lbnRFcnJvcih7IGlkLCBzdGF0dXNDb2RlLCBlcnJvciB9KSk7XG5cbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBkaXNwYXRjaChnZXRFbGVtZW50U3VjY2Vzcyh7IGlkLCBzdGF0dXNDb2RlLCAuLi5wYXlsb2FkIH0pKTtcbiAgICB9KVxuICAgIC5jYXRjaCgoeyBzdGF0dXNDb2RlLCBlcnJvciB9KSA9PiB7XG4gICAgICBkaXNwYXRjaChnZXRFbGVtZW50RXJyb3IoeyBpZCwgc3RhdHVzQ29kZSwgZXJyb3IgfSkpO1xuICAgIH0pO1xufTtcblxuZXhwb3J0IGRlZmF1bHQgZ2V0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2VsZW1lbnRzL2dldEJ5SWQuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgUE9TVF9ERUxFVEUgfSBmcm9tICcuLi8uLi9jb25zdGFudHMvcG9zdHMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHBvc3RJZDogbnVtYmVyLFxuICBhdXRob3JJZDogbnVtYmVyLFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICpcbiAqIEBmdW5jdGlvbiBkZWxldGVQb3N0XG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQucG9zdElkXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICovXG5jb25zdCBkZWxldGVQb3N0ID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHtcbiAgdHlwZTogUE9TVF9ERUxFVEUsXG4gIHBheWxvYWQsXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgZGVsZXRlUG9zdDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9wb3N0cy9kZWxldGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbnR5cGUgUGF5bG9hZCA9IHt8XG4gIHVzZXJJZDogbnVtYmVyLFxuICB0b2tlbjogc3RyaW5nLFxuICB0aXRsZTogc3RyaW5nLFxufH07XG5cbi8qKlxuICogY3JlYXRlIGNvdXJzZSBmb3IgdXNlci5cbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGNyZWF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRpdGxlIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBjcmVhdGUoeyB1c2VySWQsIHRva2VuLCB0aXRsZSB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHRpdGxlIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7fFxuICB0b2tlbjogc3RyaW5nLFxuICB1c2VySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbnx9O1xuXG4vKipcbiAqIGRlbGV0ZSBjb3Vyc2VcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGRlbGV0ZUNvdXJzZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZUlkXG4gKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBkZWxldGVDb3Vyc2UoeyB0b2tlbiwgdXNlcklkLCBjb3Vyc2VJZCB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzLyR7Y291cnNlSWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnREVMRVRFJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBkZWxldGVDb3Vyc2U7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL2RlbGV0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqXG4gKiBAZnVuY3Rpb24gZ2V0QnlJZFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICovXG5hc3luYyBmdW5jdGlvbiBnZXRCeUlkKHsgdG9rZW4sIGlkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvZWxlbWVudHMvJHtpZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdldEJ5SWQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRCeUlkLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmlsZSBkZWxldGUgcG9zdCBvZiB1c2VyLlxuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIGRlbGV0ZSBwb3N0IG9mIHVzZXJcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGRlbGV0ZVBvc3RcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWQgLVxuICogQHJldHVybiB7UHJvbWlzZX0gLVxuICpcbiAqIEBleGFtcGxlXG4gKi9cbmFzeW5jIGZ1bmN0aW9uIGRlbGV0ZVBvc3QoeyB1c2VySWQsIHRva2VuLCBwb3N0SWQgfSkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vcG9zdHMvJHtwb3N0SWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnREVMRVRFJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBkZWxldGVQb3N0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvcG9zdHMvdXNlcnMvZGVsZXRlLmpzIiwiLyoqXG4gKiBUaGlzIGlucHV0IGNvbXBvbmVudCBpcyBtYWlubHkgZGV2ZWxvcGVkIGZvciBjcmVhdGUgcG9zdCwgY3JlYXRlXG4gKiBjb3Vyc2UgY29tcG9uZW50LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBkaXNwbGF5OiAnZmxleCcsXG4gIH0sXG4gIGlucHV0OiB7XG4gICAgZmxleEdyb3c6ICcxJyxcbiAgICBib3JkZXI6ICcycHggc29saWQgI2RhZGFkYScsXG4gICAgYm9yZGVyUmFkaXVzOiAnN3B4JyxcbiAgICBwYWRkaW5nOiAnOHB4JyxcbiAgICAnJjpmb2N1cyc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJyxcbiAgICAgIGJvcmRlckNvbG9yOiAnIzllY2FlZCcsXG4gICAgICBib3hTaGFkb3c6ICcwIDAgMTBweCAjOWVjYWVkJyxcbiAgICB9LFxuICB9LFxufTtcblxuY29uc3QgSW5wdXQgPSAoeyBjbGFzc2VzLCBvbkNoYW5nZSwgdmFsdWUsIHBsYWNlaG9sZGVyLCBkaXNhYmxlZCwgdHlwZSB9KSA9PiAoXG4gIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgIDxpbnB1dFxuICAgICAgdHlwZT17dHlwZSB8fCAndGV4dCd9XG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXIgfHwgJ2lucHV0J31cbiAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5pbnB1dH1cbiAgICAgIGRpc2FibGVkPXshIWRpc2FibGVkfVxuICAgIC8+XG4gIDwvZGl2PlxuKTtcblxuSW5wdXQucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm51bWJlcl0pLmlzUmVxdWlyZWQsXG4gIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB0eXBlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoSW5wdXQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwiLyoqXG4gKiBjb3Vyc2UgcGFnZSBjb21wb25lbnRcbiAqIFRoaXMgY29tcG9uZW50IHdpbGwgZGlzcGxheSBvbmx1IG9uZSBjb3Vyc2UgaW5mb3JtYXRpb25cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFJvdXRlciBmcm9tICdyZWFjdC1yb3V0ZXIvd2l0aFJvdXRlcic7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgQ2FyZCwge1xuICBDYXJkTWVkaWEsXG4gIENhcmRIZWFkZXIsXG4gIENhcmRDb250ZW50LFxuICBDYXJkQWN0aW9ucyxcbn0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgRGlhbG9nLCB7IERpYWxvZ0FjdGlvbnMsIERpYWxvZ1RpdGxlIH0gZnJvbSAnbWF0ZXJpYWwtdWkvRGlhbG9nJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcbmltcG9ydCBQYXBlciBmcm9tICdtYXRlcmlhbC11aS9QYXBlcic7XG5cbmltcG9ydCBNb3JlVmVydEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQnO1xuaW1wb3J0IEZhdm9yaXRlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZSc7XG5pbXBvcnQgVHVybmVkSW5JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluJztcbmltcG9ydCBEZWxldGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0RlbGV0ZSc7XG5pbXBvcnQgU2hhcmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NoYXJlJztcblxuaW1wb3J0IGFwaUNvdXJzZURlbGV0ZSBmcm9tICcuLi8uLi9hcGkvY291cnNlcy91c2Vycy9kZWxldGUnO1xuaW1wb3J0IGFjdGlvbkdldEVsZW1lbnRCeUlkIGZyb20gJy4uLy4uL2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZCc7XG5pbXBvcnQgYWN0aW9uQ291cnNlRGVsZXRlIGZyb20gJy4uLy4uL2FjdGlvbnMvY291cnNlcy9kZWxldGUnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBncmlkQ29udGFpbmVyOiB7fSxcbiAgbmFtZToge1xuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIHVzZXJuYW1lOiB7XG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgcG9zdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmRIZWFkZXI6IHtcbiAgICAvLyB0ZXh0QWxpZ246ICdjZW50ZXInXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBjYXJkTWVkaWE6IHt9LFxuICBtZWRpYToge1xuICAgIGhlaWdodDogMTk0LFxuICB9LFxuICBjYXJkTWVkaWFJbWFnZToge1xuICAgIHdpZHRoOiAnMTAwJScsXG4gIH0sXG4gIG1hcmdpbjoge1xuICAgIGJvcmRlcjogJzAuNXB4IHNvbGlkIHJnYmEoMjA5LDIwOSwyMDksMSknLFxuICAgIG1hcmdpbkJvdHRvbTogJy0xMHB4JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuY2xhc3MgQ291cnNlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGhvdmVyOiBmYWxzZSxcbiAgICAgIGRlbGV0ZURpYWxvZzogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxNb3VudCgpIHtcbiAgICBjb25zdCB7IGVsZW1lbnRzLCBjb3Vyc2VzLCBjb3Vyc2VJZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHRva2VuLCBpZDogdXNlcklkIH0gPSBlbGVtZW50cy51c2VyO1xuXG4gICAgY29uc3QgeyBhdXRob3JJZCB9ID0gY291cnNlc1tjb3Vyc2VJZF07XG4gICAgY29uc3QgYXV0aG9yID0gZWxlbWVudHNbYXV0aG9ySWRdO1xuXG4gICAgLy8gaWYgYXV0aG9yIGlzIG5vdCBmb3VuZFxuICAgIGlmICh1c2VySWQgIT09IGF1dGhvcklkICYmICFhdXRob3IpIHtcbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uR2V0RWxlbWVudEJ5SWQoeyBpZDogYXV0aG9ySWQsIHRva2VuIH0pO1xuICAgIH1cbiAgfVxuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcblxuICBvbkNsaWNrQ29udGVudCA9ICgpID0+IHtcbiAgICBjb25zdCB7IGNvdXJzZUlkIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goYC9jb3Vyc2VzLyR7Y291cnNlSWR9YCk7XG4gIH07XG5cbiAgZGVsZXRlRGlhbG9nT3BlbiA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgZGVsZXRlRGlhbG9nOiB0cnVlIH0pO1xuICB9O1xuXG4gIGRlbGV0ZURpYWxvZ0Nsb3NlID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IGZhbHNlIH0pO1xuICB9O1xuXG4gIGRlbGV0ZUNvdXJzZUNsaWNrID0gYXN5bmMgKCkgPT4ge1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBjb3Vyc2VJZCB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IgfSA9IGF3YWl0IGFwaUNvdXJzZURlbGV0ZSh7XG4gICAgICB0b2tlbixcbiAgICAgIHVzZXJJZDogaWQsXG4gICAgICBjb3Vyc2VJZCxcbiAgICB9KTtcbiAgICBpZiAoc3RhdHVzQ29kZSAhPT0gMjAwKSB7XG4gICAgICAvLyBzaG93IHNvbWUgZXJyb3IgbWVzc2FnZS5cbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLnByb3BzLmFjdGlvbkNvdXJzZURlbGV0ZSh7IGNvdXJzZUlkIH0pO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IGZhbHNlIH0pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGNvdXJzZXMsIGVsZW1lbnRzLCBjb3Vyc2VJZCB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IHsgdXNlciB9ID0gZWxlbWVudHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluLCBpZDogdXNlcklkIH0gPSB1c2VyO1xuXG4gICAgY29uc3QgY291cnNlID0gY291cnNlc1tjb3Vyc2VJZF07XG5cbiAgICBpZiAodHlwZW9mIGNvdXJzZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiA8ZGl2IC8+O1xuICAgIH1cblxuICAgIGNvbnN0IHsgdGl0bGUsIGRlc2NyaXB0aW9uLCBhdXRob3JJZCwgY292ZXIgfSA9IGNvdXJzZTtcbiAgICBjb25zdCBhdXRob3IgPSBhdXRob3JJZCA9PT0gdXNlcklkID8gdXNlciA6IGVsZW1lbnRzW2F1dGhvcklkXTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgPlxuICAgICAgICA8Q2FyZCByYWlzZWQ9e3RoaXMuc3RhdGUuaG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgPFBhcGVyIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9IGVsZXZhdGlvbj17MX0+XG4gICAgICAgICAgICAgICAgPEF2YXRhclxuICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAgIGF1dGhvci5hdmF0YXIgfHxcbiAgICAgICAgICAgICAgICAgICAgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZydcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L1BhcGVyPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYWN0aW9uPXtcbiAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvbWluZyBzb29uXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICAgICAgPE1vcmVWZXJ0SWNvbiAvPlxuICAgICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfSBjbGFzc05hbWU9e2NsYXNzZXMubmFtZX0+XG4gICAgICAgICAgICAgICAge2F1dGhvci5uYW1lfVxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzdWJoZWFkZXI9e1xuICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnVzZXJuYW1lfT5cbiAgICAgICAgICAgICAgICB7YEAke2F1dGhvci51c2VybmFtZX1gfVxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Q2FyZE1lZGlhXG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMubWVkaWF9XG4gICAgICAgICAgICBpbWFnZT17XG4gICAgICAgICAgICAgIGNvdmVyIHx8XG4gICAgICAgICAgICAgICdodHRwOi8vZ2ludmEuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzA4L2dpbnZhXzIwMTYtMDgtMDJfMDItMzAtNTQuanBnJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPENhcmRDb250ZW50IG9uQ2xpY2s9e3RoaXMub25DbGlja0NvbnRlbnR9PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cInRpdGxlXCIgZ3V0dGVyQm90dG9tPlxuICAgICAgICAgICAgICB7dGl0bGV9XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICB7dHlwZW9mIGRlc2NyaXB0aW9uID09PSAnc3RyaW5nJyA/IChcbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkxXCIgZ3V0dGVyQm90dG9tPlxuICAgICAgICAgICAgICAgIHtkZXNjcmlwdGlvbn1cbiAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkFkZCB0byBmYXZvcml0ZXNcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFkZCB0byBmYXZvcml0ZXNcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8RmF2b3JpdGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQm9va21hcmtcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvb2sgTWFya3NcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8VHVybmVkSW5JY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIHtpc1NpZ25lZEluICYmIHVzZXJJZCA9PT0gYXV0aG9yLmlkID8gKFxuICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJEZWxldGVcIiBvbkNsaWNrPXt0aGlzLmRlbGV0ZURpYWxvZ09wZW59PlxuICAgICAgICAgICAgICAgICAgPERlbGV0ZUljb24gLz5cbiAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgICAgPERpYWxvZ1xuICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5kZWxldGVEaWFsb2d9XG4gICAgICAgICAgICAgICAgICBvbkNsb3NlPXt0aGlzLmRlbGV0ZURpYWxvZ0Nsb3NlfVxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PXtgY291cnNlLSR7Y291cnNlSWR9LWRlbGV0ZS1kaWFsb2dgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxEaWFsb2dUaXRsZSBpZD17YGNvdXJzZS0ke2NvdXJzZUlkfS1kZWxldGUtZGlhbG9nYH0+XG4gICAgICAgICAgICAgICAgICAgIEFyZSB5b3Ugc3VyZT9cbiAgICAgICAgICAgICAgICAgIDwvRGlhbG9nVGl0bGU+XG4gICAgICAgICAgICAgICAgICA8RGlhbG9nQWN0aW9ucz5cbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBvbkNsaWNrPXt0aGlzLmRlbGV0ZURpYWxvZ0Nsb3NlfSBjb2xvcj1cInByaW1hcnlcIj5cbiAgICAgICAgICAgICAgICAgICAgICBDYW5jZWxcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmRlbGV0ZUNvdXJzZUNsaWNrfVxuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBEZWxldGVcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICA8L0RpYWxvZ0FjdGlvbnM+XG4gICAgICAgICAgICAgICAgPC9EaWFsb2c+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJTaGFyZVwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICA8U2hhcmVJY29uIC8+XG4gICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5Db3Vyc2UucHJvcFR5cGVzID0ge1xuICBoaXN0b3J5OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGNvdXJzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY291cnNlSWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcblxuICBhY3Rpb25HZXRFbGVtZW50QnlJZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgYWN0aW9uQ291cnNlRGVsZXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMsIGNvdXJzZXMgfSkgPT4gKHsgZWxlbWVudHMsIGNvdXJzZXMgfSk7XG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25HZXRFbGVtZW50QnlJZCxcbiAgICAgIGFjdGlvbkNvdXJzZURlbGV0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKHdpdGhSb3V0ZXIoQ291cnNlKSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZS9pbmRleC5qcyIsIi8qKlxuICogRm9yIGNyZWF0aW5nIGNvdXJzZVxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEF2YXRhciBmcm9tICdtYXRlcmlhbC11aS9BdmF0YXInO1xuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnbWF0ZXJpYWwtdWkvVHlwb2dyYXBoeSc7XG5pbXBvcnQgQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0J1dHRvbic7XG5pbXBvcnQgUGFwZXIgZnJvbSAnbWF0ZXJpYWwtdWkvUGFwZXInO1xuXG5pbXBvcnQgSW5wdXQgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9JbnB1dCc7XG5cbmltcG9ydCBhY3Rpb25Db3Vyc2VDcmVhdGUgZnJvbSAnLi4vLi4vYWN0aW9ucy9jb3Vyc2VzL2NyZWF0ZSc7XG5cbmltcG9ydCBhcGlDb3Vyc2VDcmVhdGUgZnJvbSAnLi4vLi4vYXBpL2NvdXJzZXMvdXNlcnMvY3JlYXRlJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgcGFkZGluZzogJzElJyxcbiAgfSxcbiAgY2FyZDoge1xuICAgIGJvcmRlclJhZGl1czogJzhweCcsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gIH0sXG4gIGNhcmRIZWFkZXI6IHt9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59O1xuXG5jbGFzcyBDb3Vyc2VDcmVhdGUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgYWN0aXZlOiBmYWxzZSxcbiAgICAgIHRpdGxlOiAnJyxcbiAgICAgIHRpdGxlTGVuZ3RoOiAwLFxuICAgICAgcGxhY2Vob2xkZXI6ICdDb3Vyc2UgVGl0bGUuLi4nLFxuXG4gICAgICB2YWxpZDogZmFsc2UsXG4gICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgZXJyb3I6ICcnLFxuICAgICAgbWVzc2FnZTogJycsXG4gICAgICBob3ZlcjogZmFsc2UsXG4gICAgfTtcblxuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblN1Ym1pdCA9IHRoaXMub25TdWJtaXQuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKGUpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgdGl0bGU6IGUudGFyZ2V0LnZhbHVlIH0pO1xuICB9XG5cbiAgYXN5bmMgb25TdWJtaXQoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCB7IGlkLCB0b2tlbiB9ID0gdGhpcy5wcm9wcy5lbGVtZW50cy51c2VyO1xuICAgIGNvbnN0IHsgdGl0bGUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgYm9keSA9IHsgdGl0bGUgfTtcblxuICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnQ3JlYXRpbmcgY291cnNlLi4uJyB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IsIHBheWxvYWQgfSA9IGF3YWl0IGFwaUNvdXJzZUNyZWF0ZSh7XG4gICAgICB0b2tlbixcbiAgICAgIHVzZXJJZDogaWQsXG4gICAgICB0aXRsZSxcbiAgICB9KTtcblxuICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgLy8gaGFuZGxlIGVycm9yXG4gICAgICB0aGlzLnNldFN0YXRlKHsgc3RhdHVzQ29kZSwgZXJyb3IgfSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2U6ICdTdWNjZXNzZnVsbHkgQ3JlYXRlZC4nIH0pO1xuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgdmFsaWQ6IGZhbHNlLFxuICAgICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgICBlcnJvcjogJycsXG4gICAgICAgIG1lc3NhZ2U6ICcnLFxuXG4gICAgICAgIHRpdGxlOiAnJyxcbiAgICAgICAgdGl0bGVMZW5ndGg6IDAsXG4gICAgICB9KTtcbiAgICB9LCAxNTAwKTtcblxuICAgIHRoaXMucHJvcHMuYWN0aW9uQ291cnNlQ3JlYXRlKHtcbiAgICAgIHN0YXR1c0NvZGUsXG4gICAgICAuLi5wYXlsb2FkLFxuICAgICAgLi4uYm9keSxcbiAgICB9KTtcbiAgfVxuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcbiAgb25Gb2N1cyA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBhY3RpdmU6IHRydWUgfSk7XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgYWN0aXZlLCBwbGFjZWhvbGRlciwgdGl0bGUsIG1lc3NhZ2UgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBlbGVtZW50cyB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IG5hbWUsIGF2YXRhciwgdXNlcm5hbWUgfSA9IGVsZW1lbnRzLnVzZXI7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH1cbiAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLm9uTW91c2VFbnRlcn1cbiAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgID5cbiAgICAgICAgPENhcmQgcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgIGF2YXRhcj17XG4gICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake3VzZXJuYW1lfWB9PlxuICAgICAgICAgICAgICAgICAgPEF2YXRhclxuICAgICAgICAgICAgICAgICAgICBhbHQ9e25hbWV9XG4gICAgICAgICAgICAgICAgICAgIHNyYz17YXZhdGFyIHx8ICcvcHVibGljL3Bob3Rvcy9tYXlhc2gtbG9nby10cmFuc3BhcmVudC5wbmcnfVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIDwvUGFwZXI+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgICAgIHZhbHVlPXt0aXRsZX1cbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZEhlYWRlcn1cbiAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMub25Gb2N1c31cbiAgICAgICAgICAvPlxuICAgICAgICAgIHthY3RpdmUgJiYgbWVzc2FnZS5sZW5ndGggPyAoXG4gICAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIDxUeXBvZ3JhcGh5PnttZXNzYWdlfTwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAge2FjdGl2ZSA/IChcbiAgICAgICAgICAgIDxDYXJkQWN0aW9ucyBkaXNhYmxlQWN0aW9uU3BhY2luZz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICAgIDxCdXR0b24gcmFpc2VkIGNvbG9yPVwiYWNjZW50XCIgb25DbGljaz17dGhpcy5vblN1Ym1pdH0+XG4gICAgICAgICAgICAgICAgQ3JlYXRlXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5Db3Vyc2VDcmVhdGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGFjdGlvbkNvdXJzZUNyZWF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IGVsZW1lbnRzIH0pO1xuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PlxuICBiaW5kQWN0aW9uQ3JlYXRvcnMoXG4gICAge1xuICAgICAgYWN0aW9uQ291cnNlQ3JlYXRlLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoQ291cnNlQ3JlYXRlKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlQ3JlYXRlL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB7IHdpdGhTdHlsZXMgfSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMnO1xuaW1wb3J0IEdyaWQgZnJvbSAnbWF0ZXJpYWwtdWkvR3JpZCc7XG5cbmltcG9ydCBDb3Vyc2VDcmVhdGUgZnJvbSAnLi4vLi4vQ291cnNlQ3JlYXRlJztcbmltcG9ydCBUaW1lbGluZSBmcm9tICcuLi8uLi9UaW1lbGluZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge30sXG59O1xuXG5jbGFzcyBUYWJDb3Vyc2VzIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgdXNlciwgZWxlbWVudCwgY291cnNlcyB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGlzU2lnbmVkSW4sIGlkIH0gPSB1c2VyO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZCBjb250YWluZXIganVzdGlmeT1cImNlbnRlclwiIHNwYWNpbmc9ezB9PlxuICAgICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBzbT17OX0gbWQ9ezd9IGxnPXs2fSB4bD17Nn0+XG4gICAgICAgICAgICB7aXNTaWduZWRJbiAmJiBpZCA9PT0gZWxlbWVudC5pZCA/IDxDb3Vyc2VDcmVhdGUgLz4gOiBudWxsfVxuICAgICAgICAgICAgPFRpbWVsaW5lIHR5cGU9eydjb3Vyc2VzJ30gY291cnNlcz17Y291cnNlc30gZWxlbWVudD17ZWxlbWVudH0gLz5cbiAgICAgICAgICA8L0dyaWQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuVGFiQ291cnNlcy5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgdXNlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGNvdXJzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9IChzdGF0ZSkgPT4gc3RhdGU7XG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+IGJpbmRBY3Rpb25DcmVhdG9ycyh7fSwgZGlzcGF0Y2gpO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKFRhYkNvdXJzZXMpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9FbGVtZW50UGFnZS9UYWJzL1RhYkNvdXJzZXMuanMiLCIvKipcbiAqIFBvc3QgY29udGFpbmVyIGNvbXBvbmVudFxuICogSXQgd2lsbCBkaXNwbGF5IG9uZSBwb3N0IHdpdGggc29tZSBvZiBhY3Rpb25zIChlZy4gQm9va21hcmtzLCBTaGFyZSwgRGVsZXRlKVxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoUm91dGVyIGZyb20gJ3JlYWN0LXJvdXRlci93aXRoUm91dGVyJztcbmltcG9ydCBMaW5rIGZyb20gJ3JlYWN0LXJvdXRlci1kb20vTGluayc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5pbXBvcnQgQ2FyZCwge1xuICBDYXJkSGVhZGVyLFxuICBDYXJkTWVkaWEsXG4gIENhcmRDb250ZW50LFxuICBDYXJkQWN0aW9ucyxcbn0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgRGlhbG9nLCB7IERpYWxvZ0FjdGlvbnMsIERpYWxvZ1RpdGxlIH0gZnJvbSAnbWF0ZXJpYWwtdWkvRGlhbG9nJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJhZGdlIGZyb20gJ21hdGVyaWFsLXVpL0JhZGdlJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IFBhcGVyIGZyb20gJ21hdGVyaWFsLXVpL1BhcGVyJztcbmltcG9ydCBUb29sdGlwIGZyb20gJ21hdGVyaWFsLXVpL1Rvb2x0aXAnO1xuXG5pbXBvcnQgTW9yZVZlcnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0JztcbmltcG9ydCBGYXZvcml0ZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUnO1xuaW1wb3J0IFR1cm5lZEluSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9UdXJuZWRJbic7XG5pbXBvcnQgQ29tbWVudEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudCc7XG5pbXBvcnQgRGVsZXRlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9EZWxldGUnO1xuaW1wb3J0IFJlbW92ZVJlZEV5ZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvUmVtb3ZlUmVkRXllJztcbmltcG9ydCBTaGFyZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUnO1xuXG4vLyBBbGwgYWN0aW9ucyBhcmUgZGVmaW5lZCBoZXJlLlxuaW1wb3J0IGdldEVsZW1lbnRCeUlkIGZyb20gJy4uLy4uL2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZCc7XG5cbmltcG9ydCBhcGlQb3N0RGVsZXRlIGZyb20gJy4uLy4uL2FwaS9wb3N0cy91c2Vycy9kZWxldGUnO1xuaW1wb3J0IGFjdGlvblBvc3REZWxldGUgZnJvbSAnLi4vLi4vYWN0aW9ucy9wb3N0cy9kZWxldGUnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgYXZhdGFyOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgfSxcbiAgdGl0bGU6IHtcbiAgICAvLyBjb2xvcjogJyMzNjU4OTknLFxuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIHN1YmhlYWRlcjoge1xuICAgIC8vIGNvbG9yOiAnIzM2NTg5OScsXG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgbWVkaWE6IHtcbiAgICBoZWlnaHQ6IDE5NCxcbiAgfSxcbiAgZmF2b3JpdGVJY29uQWN0aXZlOiB7XG4gICAgY29sb3I6ICcjRjUwMDU3JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBiYWRnZToge1xuICAgIG1hcmdpbjogYDAgJHt0aGVtZS5zcGFjaW5nLnVuaXQgKiAyfXB4YCxcbiAgICBjb2xvcjogJ2JsdWUnLFxuICB9LFxuICBtYXJnaW46IHtcbiAgICBib3JkZXI6ICcwLjVweCBzb2xpZCByZ2JhKDIwOSwyMDksMjA5LDEpJyxcbiAgICBtYXJnaW5Cb3R0b206ICctMTBweCcsXG4gIH0sXG4gIGNvbnRlbnQ6IHtcbiAgICB0ZXh0QWxpZ246ICdqdXN0aWZ5JyxcbiAgfSxcbn0pO1xuXG5jbGFzcyBQb3N0IGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGhvdmVyOiBmYWxzZSxcbiAgICAgIGZhdm9yaXRlOiBmYWxzZSxcbiAgICAgIGRlbGV0ZURpYWxvZzogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIC8vIGNvbXBvbmVudFdpbGxNb3VudCBpcyBleGVjdXRlZCBiZWZvcmUgcmVuZGVyaW5nXG4gIGNvbXBvbmVudFdpbGxNb3VudCgpIHtcbiAgICBjb25zdCB7IGVsZW1lbnRzLCBwb3N0cywgcG9zdElkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkOiB1c2VySWQgfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBhdXRob3JJZCB9ID0gcG9zdHNbcG9zdElkXTtcblxuICAgIGNvbnN0IGF1dGhvciA9IGVsZW1lbnRzW2F1dGhvcklkXTtcblxuICAgIC8vIGlmIGF1dGhvciBpcyBub3QgZm91bmRcbiAgICBpZiAodXNlcklkICE9PSBhdXRob3JJZCAmJiAhYXV0aG9yKSB7XG4gICAgICB0aGlzLnByb3BzLmdldEVsZW1lbnRCeUlkKHsgaWQ6IGF1dGhvcklkLCB0b2tlbiB9KTtcbiAgICB9XG4gIH1cblxuICBvbkNsaWNrRmF2b3JpdGUgPSBhc3luYyAoKSA9PiB7XG4gICAgY29uc3QgeyBmYXZvcml0ZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHRoaXMuc2V0U3RhdGUoeyBmYXZvcml0ZTogIWZhdm9yaXRlIH0pO1xuICB9O1xuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcblxuICBvbkNsaWNrUG9zdCA9ICgpID0+IHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKGAvcG9zdHMvJHt0aGlzLnByb3BzLnBvc3RJZH1gKTtcblxuICBkZWxldGVEaWFsb2dPcGVuID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IHRydWUgfSk7XG4gIH07XG5cbiAgZGVsZXRlRGlhbG9nQ2xvc2UgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGRlbGV0ZURpYWxvZzogZmFsc2UgfSk7XG4gIH07XG5cbiAgZGVsZXRlUG9zdENsaWNrID0gYXN5bmMgKCkgPT4ge1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBwb3N0SWQgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yIH0gPSBhd2FpdCBhcGlQb3N0RGVsZXRlKHtcbiAgICAgIHRva2VuLFxuICAgICAgdXNlcklkOiBpZCxcbiAgICAgIHBvc3RJZCxcbiAgICB9KTtcblxuICAgIGlmIChzdGF0dXNDb2RlICE9PSAyMDApIHtcbiAgICAgIC8vIHNob3cgc29tZSBlcnJvciBtZXNzYWdlLlxuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMucHJvcHMuYWN0aW9uUG9zdERlbGV0ZSh7IHBvc3RJZCB9KTtcbiAgICB0aGlzLnNldFN0YXRlKHsgZGVsZXRlRGlhbG9nOiBmYWxzZSB9KTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBwb3N0cywgZWxlbWVudHMsIHBvc3RJZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHVzZXIgfSA9IGVsZW1lbnRzO1xuICAgIGNvbnN0IHsgaXNTaWduZWRJbiwgaWQ6IHVzZXJJZCB9ID0gdXNlcjtcblxuICAgIGNvbnN0IHBvc3QgPSBwb3N0c1twb3N0SWRdO1xuXG4gICAgY29uc3QgeyBmYXZvcml0ZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGlmICh0eXBlb2YgcG9zdCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiA8ZGl2IC8+O1xuICAgIH1cblxuICAgIGNvbnN0IHsgY292ZXIsIHRpdGxlLCBkZXNjcmlwdGlvbiwgYXV0aG9ySWQgfSA9IHBvc3Q7XG4gICAgY29uc3QgYXV0aG9yID0gYXV0aG9ySWQgPT09IHVzZXJJZCA/IHVzZXIgOiBlbGVtZW50c1thdXRob3JJZF07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH1cbiAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLm9uTW91c2VFbnRlcn1cbiAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgID5cbiAgICAgICAgPENhcmQgcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgIGF2YXRhcj17XG4gICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfT5cbiAgICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgc3JjPXsnL3B1YmxpYy9waG90b3MvbWF5YXNoLWxvZ28tdHJhbnNwYXJlbnQucG5nJ31cbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICA8L1BhcGVyPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYWN0aW9uPXtcbiAgICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJDb21pbmcgc29vblwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgICAgPE1vcmVWZXJ0SWNvbiAvPlxuICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlfT5cbiAgICAgICAgICAgICAgICB7YXV0aG9yLm5hbWV9XG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHN1YmhlYWRlcj17XG4gICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfSBjbGFzc05hbWU9e2NsYXNzZXMuc3ViaGVhZGVyfT5cbiAgICAgICAgICAgICAgICBAe2F1dGhvci51c2VybmFtZX1cbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPENhcmRNZWRpYVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLm1lZGlhfVxuICAgICAgICAgICAgaW1hZ2U9e1xuICAgICAgICAgICAgICBjb3ZlciB8fFxuICAgICAgICAgICAgICAnaHR0cDovL2dpbnZhLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNi8wOC9naW52YV8yMDE2LTA4LTAyXzAyLTMwLTU0LmpwZydcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRpdGxlPXt0aXRsZX1cbiAgICAgICAgICAvPlxuICAgICAgICAgIDxDYXJkQ29udGVudCBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tQb3N0fT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHR5cGU9XCJ0aXRsZVwiPnt0aXRsZX08L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICB7dHlwZW9mIGRlc2NyaXB0aW9uID09PSAnc3RyaW5nJyA/IChcbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkxXCIgZ3V0dGVyQm90dG9tIGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fT5cbiAgICAgICAgICAgICAgICB7ZGVzY3JpcHRpb259XG4gICAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgey8qIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLm1hcmdpbn0gLz4gKi99XG4gICAgICAgICAgPENhcmRBY3Rpb25zPlxuICAgICAgICAgICAgPEljb25CdXR0b24gb25DbGljaz17dGhpcy5vbkNsaWNrRmF2b3JpdGV9PlxuICAgICAgICAgICAgICA8QmFkZ2UgYmFkZ2VDb250ZW50PXswfT5cbiAgICAgICAgICAgICAgICA8RmF2b3JpdGVJY29uXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzbmFtZXMoe1xuICAgICAgICAgICAgICAgICAgICBbYCR7Y2xhc3Nlcy5mYXZvcml0ZUljb25BY3RpdmV9YF06IGZhdm9yaXRlLFxuICAgICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9CYWRnZT5cbiAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJDb21tZW50XCI+XG4gICAgICAgICAgICAgIDxCYWRnZSBiYWRnZUNvbnRlbnQ9ezB9PlxuICAgICAgICAgICAgICAgIDxDb21tZW50SWNvbiAvPlxuICAgICAgICAgICAgICA8L0JhZGdlPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvb2sgTWFya3NcIj5cbiAgICAgICAgICAgICAgPFR1cm5lZEluSWNvbiAvPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAge2lzU2lnbmVkSW4gJiYgdXNlcklkID09PSBhdXRob3JJZCA/IChcbiAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiRGVsZXRlXCIgb25DbGljaz17dGhpcy5kZWxldGVEaWFsb2dPcGVufT5cbiAgICAgICAgICAgICAgICAgIDxEZWxldGVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgIDxEaWFsb2dcbiAgICAgICAgICAgICAgICAgIG9wZW49e3RoaXMuc3RhdGUuZGVsZXRlRGlhbG9nfVxuICAgICAgICAgICAgICAgICAgb25DbG9zZT17dGhpcy5kZWxldGVEaWFsb2dDbG9zZX1cbiAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWxsZWRieT17YHBvc3QtJHtwb3N0SWR9LWRlbGV0ZS1kaWFsb2dgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxEaWFsb2dUaXRsZSBpZD17YHBvc3QtJHtwb3N0SWR9LWRlbGV0ZS1kaWFsb2dgfT5cbiAgICAgICAgICAgICAgICAgICAgQXJlIHlvdSBzdXJlP1xuICAgICAgICAgICAgICAgICAgPC9EaWFsb2dUaXRsZT5cbiAgICAgICAgICAgICAgICAgIDxEaWFsb2dBY3Rpb25zPlxuICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIG9uQ2xpY2s9e3RoaXMuZGVsZXRlRGlhbG9nQ2xvc2V9IGNvbG9yPVwicHJpbWFyeVwiPlxuICAgICAgICAgICAgICAgICAgICAgIENhbmNlbFxuICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuZGVsZXRlUG9zdENsaWNrfVxuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBEZWxldGVcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICA8L0RpYWxvZ0FjdGlvbnM+XG4gICAgICAgICAgICAgICAgPC9EaWFsb2c+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJTaGFyZVwiPlxuICAgICAgICAgICAgICA8QmFkZ2UgYmFkZ2VDb250ZW50PXswfT5cbiAgICAgICAgICAgICAgICA8UmVtb3ZlUmVkRXllSWNvbiAvPlxuICAgICAgICAgICAgICA8L0JhZGdlPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlNoYXJlXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgIDxTaGFyZUljb24gLz5cbiAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICA8L0NhcmQ+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblBvc3QucHJvcFR5cGVzID0ge1xuICBoaXN0b3J5OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHBvc3RzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLy8gcG9zdDogUHJvcFR5cGVzLnNoYXBlKHtcbiAgLy8gICBhdXRob3JJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAvLyAgIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAvLyAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIC8vICAgZGVzY3JpcHRpb246IFByb3BUeXBlcy5zdHJpbmcsXG4gIC8vIH0pLmlzUmVxdWlyZWQsXG5cbiAgcG9zdElkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG5cbiAgZ2V0RWxlbWVudEJ5SWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGFjdGlvblBvc3REZWxldGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cywgcG9zdHMgfSkgPT4gKHsgZWxlbWVudHMsIHBvc3RzIH0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBnZXRFbGVtZW50QnlJZCxcbiAgICAgIGFjdGlvblBvc3REZWxldGUsXG4gICAgfSxcbiAgICBkaXNwYXRjaCxcbiAgKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKSh3aXRoUm91dGVyKFBvc3QpKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdC9pbmRleC5qcyIsIi8qKlxuICogVGltZWxpbmUgRGlzcGxheSBjb21wb25lbnRcbiAqIFRoaXMgY29tcG9uZW50IHdpbGwgZGlzcGxheSBlaXRoZXIgY291cnNlIG9yIFBvc3QgdGltZWxpbmVcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IHdpdGhTdHlsZXMgfSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMnO1xuXG5pbXBvcnQgUG9zdCBmcm9tICcuLi9Qb3N0JztcbmltcG9ydCBDb3Vyc2UgZnJvbSAnLi4vQ291cnNlJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZmxleEdyb3c6ICcxJyxcbiAgfSxcbn07XG5cbmNsYXNzIFRpbWVsaW5lIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHR5cGUsIHBvc3RzLCBjb3Vyc2VzLCBlbGVtZW50IH0gPSB0aGlzLnByb3BzO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7dHlwZSA9PT0gJ3Bvc3RzJ1xuICAgICAgICAgID8gZWxlbWVudC5wb3N0cy5tYXAoKHApID0+IDxQb3N0IGtleT17cC5wb3N0SWR9IHBvc3RJZD17cC5wb3N0SWR9IC8+KVxuICAgICAgICAgIDogZWxlbWVudC5jb3Vyc2VzLm1hcCgoYykgPT4gKFxuICAgICAgICAgICAgPENvdXJzZSBrZXk9e2MuY291cnNlSWR9IGNvdXJzZUlkPXtjLmNvdXJzZUlkfSAvPlxuICAgICAgICAgICkpfVxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5UaW1lbGluZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgdHlwZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLCAvLyAncG9zdHMnIG9yICdjb3Vyc2VzJ1xuICBlbGVtZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHBvc3RzOiBQcm9wVHlwZXMub2JqZWN0LFxuICBjb3Vyc2VzOiBQcm9wVHlwZXMub2JqZWN0LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFRpbWVsaW5lKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9UaW1lbGluZS9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=