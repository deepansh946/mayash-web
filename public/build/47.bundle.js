webpackJsonp([47],{

/***/ "./node_modules/material-ui-icons/AttachFile.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/AttachFile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z' });

var AttachFile = function AttachFile(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

AttachFile = (0, _pure2.default)(AttachFile);
AttachFile.muiName = 'SvgIcon';

exports.default = AttachFile;

/***/ }),

/***/ "./node_modules/material-ui-icons/Code.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Code.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z' });

var Code = function Code(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Code = (0, _pure2.default)(Code);
Code.muiName = 'SvgIcon';

exports.default = Code;

/***/ }),

/***/ "./node_modules/material-ui-icons/Edit.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Edit.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z' });

var Edit = function Edit(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Edit = (0, _pure2.default)(Edit);
Edit.muiName = 'SvgIcon';

exports.default = Edit;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignCenter.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignCenter.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M7 15v2h10v-2H7zm-4 6h18v-2H3v2zm0-8h18v-2H3v2zm4-6v2h10V7H7zM3 3v2h18V3H3z' });

var FormatAlignCenter = function FormatAlignCenter(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignCenter = (0, _pure2.default)(FormatAlignCenter);
FormatAlignCenter.muiName = 'SvgIcon';

exports.default = FormatAlignCenter;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignJustify.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignJustify.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18V7H3v2zm0-6v2h18V3H3z' });

var FormatAlignJustify = function FormatAlignJustify(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignJustify = (0, _pure2.default)(FormatAlignJustify);
FormatAlignJustify.muiName = 'SvgIcon';

exports.default = FormatAlignJustify;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignLeft.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignLeft.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15 15H3v2h12v-2zm0-8H3v2h12V7zM3 13h18v-2H3v2zm0 8h18v-2H3v2zM3 3v2h18V3H3z' });

var FormatAlignLeft = function FormatAlignLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignLeft = (0, _pure2.default)(FormatAlignLeft);
FormatAlignLeft.muiName = 'SvgIcon';

exports.default = FormatAlignLeft;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignRight.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignRight.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm6-4h12v-2H9v2zm-6-4h18v-2H3v2zm6-4h12V7H9v2zM3 3v2h18V3H3z' });

var FormatAlignRight = function FormatAlignRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignRight = (0, _pure2.default)(FormatAlignRight);
FormatAlignRight.muiName = 'SvgIcon';

exports.default = FormatAlignRight;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatBold.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatBold.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.6 10.79c.97-.67 1.65-1.77 1.65-2.79 0-2.26-1.75-4-4-4H7v14h7.04c2.09 0 3.71-1.7 3.71-3.79 0-1.52-.86-2.82-2.15-3.42zM10 6.5h3c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-3v-3zm3.5 9H10v-3h3.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5z' });

var FormatBold = function FormatBold(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatBold = (0, _pure2.default)(FormatBold);
FormatBold.muiName = 'SvgIcon';

exports.default = FormatBold;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatItalic.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatItalic.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4z' });

var FormatItalic = function FormatItalic(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatItalic = (0, _pure2.default)(FormatItalic);
FormatItalic.muiName = 'SvgIcon';

exports.default = FormatItalic;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListBulleted.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListBulleted.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M4 10.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5-.67-1.5-1.5-1.5zm0-6c-.83 0-1.5.67-1.5 1.5S3.17 7.5 4 7.5 5.5 6.83 5.5 6 4.83 4.5 4 4.5zm0 12c-.83 0-1.5.68-1.5 1.5s.68 1.5 1.5 1.5 1.5-.68 1.5-1.5-.67-1.5-1.5-1.5zM7 19h14v-2H7v2zm0-6h14v-2H7v2zm0-8v2h14V5H7z' });

var FormatListBulleted = function FormatListBulleted(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListBulleted = (0, _pure2.default)(FormatListBulleted);
FormatListBulleted.muiName = 'SvgIcon';

exports.default = FormatListBulleted;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListNumbered.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListNumbered.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z' });

var FormatListNumbered = function FormatListNumbered(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListNumbered = (0, _pure2.default)(FormatListNumbered);
FormatListNumbered.muiName = 'SvgIcon';

exports.default = FormatListNumbered;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatQuote.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatQuote.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' });

var FormatQuote = function FormatQuote(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatQuote = (0, _pure2.default)(FormatQuote);
FormatQuote.muiName = 'SvgIcon';

exports.default = FormatQuote;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatUnderlined.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatUnderlined.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z' });

var FormatUnderlined = function FormatUnderlined(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatUnderlined = (0, _pure2.default)(FormatUnderlined);
FormatUnderlined.muiName = 'SvgIcon';

exports.default = FormatUnderlined;

/***/ }),

/***/ "./node_modules/material-ui-icons/Functions.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Functions.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 4H6v2l6.5 6L6 18v2h12v-3h-7l5-5-5-5h7z' });

var Functions = function Functions(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Functions = (0, _pure2.default)(Functions);
Functions.muiName = 'SvgIcon';

exports.default = Functions;

/***/ }),

/***/ "./node_modules/material-ui-icons/Highlight.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Highlight.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 14l3 3v5h6v-5l3-3V9H6zm5-12h2v3h-2zM3.5 5.875L4.914 4.46l2.12 2.122L5.62 7.997zm13.46.71l2.123-2.12 1.414 1.414L18.375 8z' });

var Highlight = function Highlight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Highlight = (0, _pure2.default)(Highlight);
Highlight.muiName = 'SvgIcon';

exports.default = Highlight;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertComment.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertComment.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var InsertComment = function InsertComment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertComment = (0, _pure2.default)(InsertComment);
InsertComment.muiName = 'SvgIcon';

exports.default = InsertComment;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertEmoticon.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertEmoticon.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });

var InsertEmoticon = function InsertEmoticon(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertEmoticon = (0, _pure2.default)(InsertEmoticon);
InsertEmoticon.muiName = 'SvgIcon';

exports.default = InsertEmoticon;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertLink.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertLink.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z' });

var InsertLink = function InsertLink(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertLink = (0, _pure2.default)(InsertLink);
InsertLink.muiName = 'SvgIcon';

exports.default = InsertLink;

/***/ }),

/***/ "./node_modules/material-ui-icons/Save.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Save.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z' });

var Save = function Save(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Save = (0, _pure2.default)(Save);
Save.muiName = 'SvgIcon';

exports.default = Save;

/***/ }),

/***/ "./node_modules/material-ui-icons/Title.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Title.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M5 4v3h5.5v12h3V7H19V4z' });

var Title = function Title(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Title = (0, _pure2.default)(Title);
Title.muiName = 'SvgIcon';

exports.default = Title;

/***/ }),

/***/ "./src/client/containers/ElementPage/Tabs/TabResume.js":
/*!*************************************************************!*\
  !*** ./src/client/containers/ElementPage/Tabs/TabResume.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Edit = __webpack_require__(/*! material-ui-icons/Edit */ "./node_modules/material-ui-icons/Edit.js");

var _Edit2 = _interopRequireDefault(_Edit);

var _Save = __webpack_require__(/*! material-ui-icons/Save */ "./node_modules/material-ui-icons/Save.js");

var _Save2 = _interopRequireDefault(_Save);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _mayashEditor = __webpack_require__(/*! ../../../../lib/mayash-editor */ "./src/lib/mayash-editor/index.js");

var _mayashEditor2 = _interopRequireDefault(_mayashEditor);

var _update = __webpack_require__(/*! ../../../api/users/update */ "./src/client/api/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  var _editButton;

  return {
    root: {
      padding: '1%'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    card: {
      borderRadius: '8px'
    },
    editButton: (_editButton = {
      position: 'fixed',
      zIndex: 1
    }, (0, _defineProperty3.default)(_editButton, theme.breakpoints.up('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('md'), {
      bottom: '20px',
      right: '20px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('sm'), {
      display: 'none',
      bottom: '10px',
      right: '10px'
    }), _editButton)
  };
}; /** @format */

var TabResume = function (_Component) {
  (0, _inherits3.default)(TabResume, _Component);

  function TabResume(props) {
    (0, _classCallCheck3.default)(this, TabResume);

    var _this = (0, _possibleConstructorReturn3.default)(this, (TabResume.__proto__ || (0, _getPrototypeOf2.default)(TabResume)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.state = {
      resume: (0, _mayashEditor.createEditorState)(props.element.resume)
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onEdit = _this.onEdit.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    return _this;
  }

  // This on change function is for MayashEditor


  (0, _createClass3.default)(TabResume, [{
    key: 'onChange',
    value: function onChange(resume) {
      this.setState({ resume: resume });
    }
  }, {
    key: 'onEdit',
    value: function onEdit() {
      this.setState({ edit: !this.state.edit });
    }
  }, {
    key: 'onSave',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var resume, _props$user, userId, token, _ref2, statusCode, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                resume = this.state.resume;
                _props$user = this.props.user, userId = _props$user.id, token = _props$user.token;
                _context.next = 4;
                return (0, _update2.default)({
                  id: userId,
                  token: token,
                  resume: (0, _draftJs.convertToRaw)(resume.getCurrentContent())
                });

              case 4:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;

                if (!(statusCode !== 200)) {
                  _context.next = 10;
                  break;
                }

                // error handle
                console.error(error);
                return _context.abrupt('return');

              case 10:

                this.props.actionUserUpdate({
                  id: userId,
                  resume: (0, _draftJs.convertToRaw)(resume.getCurrentContent())
                });

                this.setState({ edit: false });

              case 12:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSave() {
        return _ref.apply(this, arguments);
      }

      return onSave;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          element = _props.element,
          user = _props.user;
      var elementId = element.id,
          isSignedIn = element.isSignedIn;
      var userId = user.id;
      var _state = this.state,
          hover = _state.hover,
          edit = _state.edit,
          resume = _state.resume;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, spacing: 0, justify: 'center', className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 10,
            lg: 9,
            xl: 9,
            className: classes.gridItem
          },
          _react2.default.createElement(
            'div',
            {
              onMouseEnter: this.onMouseEnter,
              onMouseLeave: this.onMouseLeave
            },
            _react2.default.createElement(
              _Card2.default,
              { raised: hover, className: classes.card },
              _react2.default.createElement(
                _Card.CardContent,
                null,
                _react2.default.createElement(_mayashEditor2.default, {
                  editorState: resume,
                  onChange: this.onChange,
                  readOnly: !edit
                })
              )
            ),
            isSignedIn && userId === elementId ? _react2.default.createElement(
              _Button2.default,
              {
                fab: true,
                color: 'accent',
                'aria-label': 'Edit',
                className: classes.editButton,
                onClick: edit ? this.onSave : this.onEdit
              },
              edit ? _react2.default.createElement(_Save2.default, null) : _react2.default.createElement(_Edit2.default, null)
            ) : null
          )
        )
      );
    }
  }]);
  return TabResume;
}(_react.Component);

TabResume.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  element: _propTypes2.default.object.isRequired,

  actionUserUpdate: _propTypes2.default.func
};

exports.default = (0, _withStyles2.default)(styles)(TabResume);

/***/ }),

/***/ "./src/lib/mayash-editor/Editor.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-editor/Editor.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! material-ui-icons/Title */ "./node_modules/material-ui-icons/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _FormatBold = __webpack_require__(/*! material-ui-icons/FormatBold */ "./node_modules/material-ui-icons/FormatBold.js");

var _FormatBold2 = _interopRequireDefault(_FormatBold);

var _FormatItalic = __webpack_require__(/*! material-ui-icons/FormatItalic */ "./node_modules/material-ui-icons/FormatItalic.js");

var _FormatItalic2 = _interopRequireDefault(_FormatItalic);

var _FormatUnderlined = __webpack_require__(/*! material-ui-icons/FormatUnderlined */ "./node_modules/material-ui-icons/FormatUnderlined.js");

var _FormatUnderlined2 = _interopRequireDefault(_FormatUnderlined);

var _FormatQuote = __webpack_require__(/*! material-ui-icons/FormatQuote */ "./node_modules/material-ui-icons/FormatQuote.js");

var _FormatQuote2 = _interopRequireDefault(_FormatQuote);

var _Code = __webpack_require__(/*! material-ui-icons/Code */ "./node_modules/material-ui-icons/Code.js");

var _Code2 = _interopRequireDefault(_Code);

var _FormatListNumbered = __webpack_require__(/*! material-ui-icons/FormatListNumbered */ "./node_modules/material-ui-icons/FormatListNumbered.js");

var _FormatListNumbered2 = _interopRequireDefault(_FormatListNumbered);

var _FormatListBulleted = __webpack_require__(/*! material-ui-icons/FormatListBulleted */ "./node_modules/material-ui-icons/FormatListBulleted.js");

var _FormatListBulleted2 = _interopRequireDefault(_FormatListBulleted);

var _FormatAlignCenter = __webpack_require__(/*! material-ui-icons/FormatAlignCenter */ "./node_modules/material-ui-icons/FormatAlignCenter.js");

var _FormatAlignCenter2 = _interopRequireDefault(_FormatAlignCenter);

var _FormatAlignLeft = __webpack_require__(/*! material-ui-icons/FormatAlignLeft */ "./node_modules/material-ui-icons/FormatAlignLeft.js");

var _FormatAlignLeft2 = _interopRequireDefault(_FormatAlignLeft);

var _FormatAlignRight = __webpack_require__(/*! material-ui-icons/FormatAlignRight */ "./node_modules/material-ui-icons/FormatAlignRight.js");

var _FormatAlignRight2 = _interopRequireDefault(_FormatAlignRight);

var _FormatAlignJustify = __webpack_require__(/*! material-ui-icons/FormatAlignJustify */ "./node_modules/material-ui-icons/FormatAlignJustify.js");

var _FormatAlignJustify2 = _interopRequireDefault(_FormatAlignJustify);

var _AttachFile = __webpack_require__(/*! material-ui-icons/AttachFile */ "./node_modules/material-ui-icons/AttachFile.js");

var _AttachFile2 = _interopRequireDefault(_AttachFile);

var _InsertLink = __webpack_require__(/*! material-ui-icons/InsertLink */ "./node_modules/material-ui-icons/InsertLink.js");

var _InsertLink2 = _interopRequireDefault(_InsertLink);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _InsertEmoticon = __webpack_require__(/*! material-ui-icons/InsertEmoticon */ "./node_modules/material-ui-icons/InsertEmoticon.js");

var _InsertEmoticon2 = _interopRequireDefault(_InsertEmoticon);

var _InsertComment = __webpack_require__(/*! material-ui-icons/InsertComment */ "./node_modules/material-ui-icons/InsertComment.js");

var _InsertComment2 = _interopRequireDefault(_InsertComment);

var _Highlight = __webpack_require__(/*! material-ui-icons/Highlight */ "./node_modules/material-ui-icons/Highlight.js");

var _Highlight2 = _interopRequireDefault(_Highlight);

var _Functions = __webpack_require__(/*! material-ui-icons/Functions */ "./node_modules/material-ui-icons/Functions.js");

var _Functions2 = _interopRequireDefault(_Functions);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Atomic = __webpack_require__(/*! ./components/Atomic */ "./src/lib/mayash-editor/components/Atomic.js");

var _Atomic2 = _interopRequireDefault(_Atomic);

var _EditorStyles = __webpack_require__(/*! ./EditorStyles */ "./src/lib/mayash-editor/EditorStyles.js");

var _EditorStyles2 = _interopRequireDefault(_EditorStyles);

var _constants = __webpack_require__(/*! ./constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * MayashEditor
 */


// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

var MayashEditor = function (_Component) {
  (0, _inherits3.default)(MayashEditor, _Component);

  /**
   * Mayash Editor's proptypes are defined here.
   */
  function MayashEditor() {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, MayashEditor);

    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    var _this = (0, _possibleConstructorReturn3.default)(this, (MayashEditor.__proto__ || (0, _getPrototypeOf2.default)(MayashEditor)).call(this));

    _this.onChangeInsertPhoto = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _ref2, statusCode, error, payload, src, editorState, contentState, contentStateWithEntity, entityKey, middleEditorState, newEditorState;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // e.preventDefault();
                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 21;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _context.next = 6;
                return _this.props.apiPhotoUpload({
                  formData: formData
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle Error
                console.error(statusCode, error);
                return _context.abrupt('return');

              case 13:
                src = payload.photoUrl;
                editorState = _this.props.editorState;
                contentState = editorState.getCurrentContent();
                contentStateWithEntity = contentState.createEntity(_constants.Blocks.PHOTO, 'IMMUTABLE', { src: src });
                entityKey = contentStateWithEntity.getLastCreatedEntityKey();
                middleEditorState = _draftJs.EditorState.set(editorState, {
                  currentContent: contentStateWithEntity
                });
                newEditorState = _draftJs.AtomicBlockUtils.insertAtomicBlock(middleEditorState, entityKey, ' ');


                _this.onChange(newEditorState);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.onChange = function (editorState) {
      _this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    _this.focus = function () {
      return _this.editorNode.focus();
    };

    _this.onTab = _this.onTab.bind(_this);
    _this.onTitleClick = _this.onTitleClick.bind(_this);
    _this.onCodeClick = _this.onCodeClick.bind(_this);
    _this.onQuoteClick = _this.onQuoteClick.bind(_this);
    _this.onListBulletedClick = _this.onListBulletedClick.bind(_this);
    _this.onListNumberedClick = _this.onListNumberedClick.bind(_this);
    _this.onBoldClick = _this.onBoldClick.bind(_this);
    _this.onItalicClick = _this.onItalicClick.bind(_this);
    _this.onUnderLineClick = _this.onUnderLineClick.bind(_this);
    _this.onHighlightClick = _this.onHighlightClick.bind(_this);
    _this.handleKeyCommand = _this.handleKeyCommand.bind(_this);
    _this.onClickInsertPhoto = _this.onClickInsertPhoto.bind(_this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
    return _this;
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */


  (0, _createClass3.default)(MayashEditor, [{
    key: 'onTab',
    value: function onTab(e) {
      e.preventDefault();
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.onTab(e, editorState, 4);

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Title Button.
     * @function onTitleClick
     */

  }, {
    key: 'onTitleClick',
    value: function onTitleClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'header-one');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Code Button.
     * @function onCodeClick
     */

  }, {
    key: 'onCodeClick',
    value: function onCodeClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'code-block');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Quote Button.
     * @function onQuoteClick
     */

  }, {
    key: 'onQuoteClick',
    value: function onQuoteClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'blockquote');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with unordered list items
     * @function onListBulletedClick
     */

  }, {
    key: 'onListBulletedClick',
    value: function onListBulletedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'unordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with ordered list item.
     * @function onListNumberedClick
     */

  }, {
    key: 'onListNumberedClick',
    value: function onListNumberedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'ordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will give selected content Bold styling.
     * @function onBoldClick
     */

  }, {
    key: 'onBoldClick',
    value: function onBoldClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'BOLD');

      this.onChange(newEditorState);
    }

    /**
     * This function will give italic styling to selected content.
     * @function onItalicClick
     */

  }, {
    key: 'onItalicClick',
    value: function onItalicClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'ITALIC');

      this.onChange(newEditorState);
    }

    /**
     * This function will give underline styling to selected content.
     * @function onUnderLineClick
     */

  }, {
    key: 'onUnderLineClick',
    value: function onUnderLineClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');

      this.onChange(newEditorState);
    }

    /**
     * This function will highlight selected content.
     * @function onHighlightClick
     */

  }, {
    key: 'onHighlightClick',
    value: function onHighlightClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'CODE');

      this.onChange(newEditorState);
    }

    /**
     *
     */

  }, {
    key: 'onClickInsertPhoto',
    value: function onClickInsertPhoto() {
      this.photo.value = null;
      this.photo.click();
    }

    /**
     *
     */

  }, {
    key: 'handleKeyCommand',


    /**
     * This function will give custom handle commands to Editor.
     * @function handleKeyCommand
     * @param {string} command
     * @return {string}
     */
    value: function handleKeyCommand(command) {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.handleKeyCommand(editorState, command);

      if (newEditorState) {
        this.onChange(newEditorState);
        return _constants.HANDLED;
      }

      return _constants.NOT_HANDLED;
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockRendererFn',
    value: function blockRendererFn(block) {
      switch (block.getType()) {
        case _constants.Blocks.ATOMIC:
          return {
            component: _Atomic2.default,
            editable: false
          };

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockStyleFn',
    value: function blockStyleFn(block) {
      switch (block.getType()) {
        case 'blockquote':
          return 'RichEditor-blockquote';

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          readOnly = _props.readOnly,
          onChange = _props.onChange,
          editorState = _props.editorState,
          placeholder = _props.placeholder;

      // Custom overrides for "code" style.

      var styleMap = {
        CODE: {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
          fontSize: 16,
          padding: 2
        }
      };

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        !readOnly ? _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-controls': '' }) },
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Title', id: 'title', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Title', onClick: this.onTitleClick },
              _react2.default.createElement(_Title2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Bold', id: 'bold', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Bold', onClick: this.onBoldClick },
              _react2.default.createElement(_FormatBold2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Italic', id: 'italic', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Italic', onClick: this.onItalicClick },
              _react2.default.createElement(_FormatItalic2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Underline', id: 'underline', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Underline',
                onClick: this.onUnderLineClick
              },
              _react2.default.createElement(_FormatUnderlined2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Code', id: 'code', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Code', onClick: this.onCodeClick },
              _react2.default.createElement(_Code2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Quote', id: 'quote', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Quote', onClick: this.onQuoteClick },
              _react2.default.createElement(_FormatQuote2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Unordered List',
              id: 'unorderd-list',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Unordered List',
                onClick: this.onListBulletedClick
              },
              _react2.default.createElement(_FormatListBulleted2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Ordered List', id: 'ordered-list', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Ordered List',
                onClick: this.onListNumberedClick
              },
              _react2.default.createElement(_FormatListNumbered2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Left', id: 'align-left', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Left', disabled: true },
              _react2.default.createElement(_FormatAlignLeft2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Center', id: 'align-center', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Center', disabled: true },
              _react2.default.createElement(_FormatAlignCenter2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignRight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignJustify2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Attach File', id: 'attach-file', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Attach File', disabled: true },
              _react2.default.createElement(_AttachFile2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Link', id: 'insert-link', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Link', disabled: true },
              _react2.default.createElement(_InsertLink2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Photo', id: 'insert-photo', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Insert Photo',
                disabled: typeof this.props.apiPhotoUpload !== 'function',
                onClick: this.onClickInsertPhoto
              },
              _react2.default.createElement(_InsertPhoto2.default, null),
              _react2.default.createElement('input', {
                type: 'file',
                accept: 'image/jpeg|png|gif',
                onChange: this.onChangeInsertPhoto,
                ref: function ref(photo) {
                  _this3.photo = photo;
                },
                style: { display: 'none' }
              })
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Emoticon',
              id: 'insertE-emoticon',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Emoticon', disabled: true },
              _react2.default.createElement(_InsertEmoticon2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Comment',
              id: 'insert-comment',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Comment', disabled: true },
              _react2.default.createElement(_InsertComment2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Highlight Text',
              id: 'highlight-text',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Highlight Text',
                onClick: this.onHighlightClick
              },
              _react2.default.createElement(_Highlight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Add Functions',
              id: 'add-functions',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Add Functions', disabled: true },
              _react2.default.createElement(_Functions2.default, null)
            )
          )
        ) : null,
        _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-area': '' }) },
          _react2.default.createElement(_draftJs.Editor
          /* Basics */

          , { editorState: editorState,
            onChange: onChange
            /* Presentation */

            , placeholder: placeholder
            // textAlignment="center"

            // textDirectionality="LTR"

            , blockRendererFn: this.blockRendererFn,
            blockStyleFn: this.blockStyleFn,
            customStyleMap: styleMap
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            , readOnly: readOnly,
            spellCheck: true
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            , handleKeyCommand: this.handleKeyCommand
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            , onTab: this.onTab
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            , ref: function ref(node) {
              _this3.editorNode = node;
            }
          })
        )
      );
    }
  }]);
  return MayashEditor;
}(_react.Component);

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

// import Icon from 'material-ui/Icon';
/** @format */

MayashEditor.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  editorState: _ReactPropTypes2.default.object.isRequired,
  onChange: _ReactPropTypes2.default.func.isRequired,

  placeholder: _ReactPropTypes2.default.string,

  readOnly: _ReactPropTypes2.default.bool.isRequired,

  /**
   * This api function should be applied for enabling photo adding
   * feature in Mayash Editor.
   */
  apiPhotoUpload: _ReactPropTypes2.default.func
};
MayashEditor.defaultProps = {
  readOnly: true,
  placeholder: 'Write Here...'
};
exports.default = (0, _withStyles2.default)(_EditorStyles2.default)(MayashEditor);

/***/ }),

/***/ "./src/lib/mayash-editor/EditorState.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/EditorState.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = undefined;

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Decorators = __webpack_require__(/*! ./components/Decorators */ "./src/lib/mayash-editor/components/Decorators.js");

/** @format */

var defaultDecorators = new _draftJs.CompositeDecorator([{
  strategy: _Decorators.handleStrategy,
  component: _Decorators.HandleSpan
}, {
  strategy: _Decorators.hashtagStrategy,
  component: _Decorators.HashtagSpan
}]);

var createEditorState = exports.createEditorState = function createEditorState() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var decorators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDecorators;

  if (content === null) {
    return _draftJs.EditorState.createEmpty(decorators);
  }
  return _draftJs.EditorState.createWithContent((0, _draftJs.convertFromRaw)(content), decorators);
};

exports.default = createEditorState;

/***/ }),

/***/ "./src/lib/mayash-editor/EditorStyles.js":
/*!***********************************************!*\
  !*** ./src/lib/mayash-editor/EditorStyles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the CSS-in-JS styles of Editor component.
 *
 * @format
 */

exports.default = {
  '@global': {
    '.RichEditor-root': {
      background: '#fff',
      border: '1px solid #ddd',
      fontFamily: "'Georgia', serif",
      fontSize: '14px',
      padding: '15px'
    },
    '.RichEditor-editor': {
      borderTop: '1px solid #ddd',
      cursor: 'text',
      fontSize: '16px',
      marginTop: '10px'
    },
    '.public-DraftEditorPlaceholder-root': {
      margin: '0 -15px -15px',
      padding: '15px'
    },
    '.public-DraftEditor-content': {
      margin: '0 -15px -15px',
      padding: '15px'
      // minHeight: '100px',
    },
    '.RichEditor-blockquote': {
      backgroundColor: '5px solid #eee',
      borderLeft: '5px solid #eee',
      color: '#666',
      fontFamily: "'Hoefler Text', 'Georgia', serif",
      fontStyle: 'italic',
      margin: '16px 0',
      padding: '10px 20px'
    },
    '.public-DraftStyleDefault-pre': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: "'Inconsolata', 'Menlo', 'Consolas', monospace",
      fontSize: '16px',
      padding: '20px'
    }
  },
  root: {
    // padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/***/ }),

/***/ "./src/lib/mayash-editor/components/Atomic.js":
/*!****************************************************!*\
  !*** ./src/lib/mayash-editor/components/Atomic.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = __webpack_require__(/*! ../constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial'
  }
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
/**
 *
 * @format
 */

var Atomic = function (_Component) {
  (0, _inherits3.default)(Atomic, _Component);

  function Atomic(props) {
    (0, _classCallCheck3.default)(this, Atomic);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Atomic.__proto__ || (0, _getPrototypeOf2.default)(Atomic)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Atomic, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          contentState = _props.contentState,
          block = _props.block;


      var entity = contentState.getEntity(block.getEntityAt(0));

      var _entity$getData = entity.getData(),
          src = _entity$getData.src;

      var type = entity.getType();

      if (type === _constants.Blocks.PHOTO) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('img', { alt: 'alt', src: src, style: styles.photo })
        );
      }

      return _react2.default.createElement('div', null);
    }
  }]);
  return Atomic;
}(_react.Component);

Atomic.propTypes = {
  contentState: _propTypes2.default.object.isRequired,
  block: _propTypes2.default.any.isRequired
};
exports.default = Atomic;

/***/ }),

/***/ "./src/lib/mayash-editor/components/Decorators.js":
/*!********************************************************!*\
  !*** ./src/lib/mayash-editor/components/Decorators.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashtagSpan = exports.HandleSpan = undefined;
exports.handleStrategy = handleStrategy;
exports.hashtagStrategy = hashtagStrategy;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _style = __webpack_require__(/*! ../style */ "./src/lib/mayash-editor/style/index.js");

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
// eslint is disabled here for now.
var HANDLE_REGEX = /\@[\w]+/g;
var HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function findWithRegex(regex, contentBlock, callback) {
  var text = contentBlock.getText();
  var matchArr = void 0,
      start = void 0;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

var HandleSpan = exports.HandleSpan = function HandleSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.handle },
    props.children
  );
};

var HashtagSpan = exports.HashtagSpan = function HashtagSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.hashtag },
    props.children
  );
};

exports.default = {
  handleStrategy: handleStrategy,
  HandleSpan: HandleSpan,

  hashtagStrategy: hashtagStrategy,
  HashtagSpan: HashtagSpan
};

/***/ }),

/***/ "./src/lib/mayash-editor/constants.js":
/*!********************************************!*\
  !*** ./src/lib/mayash-editor/constants.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
var Blocks = exports.Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video'
};

/**
 * @constant Inline
 */
var Inline = exports.Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT'
};

/**
 * @constant Entity
 */
var Entity = exports.Entity = {
  LINK: 'LINK'
};

/**
 * @constant HYPERLINK
 */
var HYPERLINK = exports.HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
var HANDLED = exports.HANDLED = 'handled';
var NOT_HANDLED = exports.NOT_HANDLED = 'not_handled';

exports.default = {
  Blocks: Blocks,
  Inline: Inline,
  Entity: Entity
};

/***/ }),

/***/ "./src/lib/mayash-editor/index.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-editor/index.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = exports.MayashEditor = undefined;

var _Editor = __webpack_require__(/*! ./Editor */ "./src/lib/mayash-editor/Editor.js");

Object.defineProperty(exports, 'MayashEditor', {
  enumerable: true,
  get: function get() {
    return _Editor.MayashEditor;
  }
});

var _EditorState = __webpack_require__(/*! ./EditorState */ "./src/lib/mayash-editor/EditorState.js");

Object.defineProperty(exports, 'createEditorState', {
  enumerable: true,
  get: function get() {
    return _EditorState.createEditorState;
  }
});

var _Editor2 = _interopRequireDefault(_Editor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Editor2.default;

/***/ }),

/***/ "./src/lib/mayash-editor/style/index.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/style/index.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @format */

var style = exports.style = {
  root: {
    padding: 20,
    width: 600
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10
  },
  button: {
    marginTop: 10,
    textAlign: 'center'
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override'
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)'
  }
};

exports.default = style;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnblJpZ2h0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRWxlbWVudFBhZ2UvVGFicy9UYWJSZXN1bWUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3RhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvclN0eWxlcy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9BdG9taWMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2NvbXBvbmVudHMvRGVjb3JhdG9ycy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29uc3RhbnRzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3Ivc3R5bGUvaW5kZXguanMiXSwibmFtZXMiOlsic3R5bGVzIiwidGhlbWUiLCJyb290IiwicGFkZGluZyIsImZsZXhHcm93IiwiZmxleCIsImNhcmQiLCJib3JkZXJSYWRpdXMiLCJlZGl0QnV0dG9uIiwicG9zaXRpb24iLCJ6SW5kZXgiLCJicmVha3BvaW50cyIsInVwIiwiYm90dG9tIiwicmlnaHQiLCJkb3duIiwiZGlzcGxheSIsIlRhYlJlc3VtZSIsInByb3BzIiwib25Nb3VzZUVudGVyIiwic2V0U3RhdGUiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsInN0YXRlIiwicmVzdW1lIiwiZWxlbWVudCIsIm9uQ2hhbmdlIiwiYmluZCIsIm9uRWRpdCIsIm9uU2F2ZSIsImVkaXQiLCJ1c2VyIiwidXNlcklkIiwiaWQiLCJ0b2tlbiIsImdldEN1cnJlbnRDb250ZW50Iiwic3RhdHVzQ29kZSIsImVycm9yIiwiY29uc29sZSIsImFjdGlvblVzZXJVcGRhdGUiLCJjbGFzc2VzIiwiZWxlbWVudElkIiwiaXNTaWduZWRJbiIsImdyaWRJdGVtIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJNYXlhc2hFZGl0b3IiLCJvbkNoYW5nZUluc2VydFBob3RvIiwiZSIsImZpbGUiLCJ0YXJnZXQiLCJmaWxlcyIsInR5cGUiLCJpbmRleE9mIiwiZm9ybURhdGEiLCJGb3JtRGF0YSIsImFwcGVuZCIsImFwaVBob3RvVXBsb2FkIiwicGF5bG9hZCIsInNyYyIsInBob3RvVXJsIiwiZWRpdG9yU3RhdGUiLCJjb250ZW50U3RhdGUiLCJjb250ZW50U3RhdGVXaXRoRW50aXR5IiwiY3JlYXRlRW50aXR5IiwiUEhPVE8iLCJlbnRpdHlLZXkiLCJnZXRMYXN0Q3JlYXRlZEVudGl0eUtleSIsIm1pZGRsZUVkaXRvclN0YXRlIiwic2V0IiwiY3VycmVudENvbnRlbnQiLCJuZXdFZGl0b3JTdGF0ZSIsImluc2VydEF0b21pY0Jsb2NrIiwiZm9jdXMiLCJlZGl0b3JOb2RlIiwib25UYWIiLCJvblRpdGxlQ2xpY2siLCJvbkNvZGVDbGljayIsIm9uUXVvdGVDbGljayIsIm9uTGlzdEJ1bGxldGVkQ2xpY2siLCJvbkxpc3ROdW1iZXJlZENsaWNrIiwib25Cb2xkQ2xpY2siLCJvbkl0YWxpY0NsaWNrIiwib25VbmRlckxpbmVDbGljayIsIm9uSGlnaGxpZ2h0Q2xpY2siLCJoYW5kbGVLZXlDb21tYW5kIiwib25DbGlja0luc2VydFBob3RvIiwicHJldmVudERlZmF1bHQiLCJ0b2dnbGVCbG9ja1R5cGUiLCJ0b2dnbGVJbmxpbmVTdHlsZSIsInBob3RvIiwidmFsdWUiLCJjbGljayIsImNvbW1hbmQiLCJibG9jayIsImdldFR5cGUiLCJBVE9NSUMiLCJjb21wb25lbnQiLCJlZGl0YWJsZSIsInJlYWRPbmx5IiwicGxhY2Vob2xkZXIiLCJzdHlsZU1hcCIsIkNPREUiLCJiYWNrZ3JvdW5kQ29sb3IiLCJmb250RmFtaWx5IiwiZm9udFNpemUiLCJibG9ja1JlbmRlcmVyRm4iLCJibG9ja1N0eWxlRm4iLCJub2RlIiwic3RyaW5nIiwiYm9vbCIsImRlZmF1bHRQcm9wcyIsImRlZmF1bHREZWNvcmF0b3JzIiwic3RyYXRlZ3kiLCJjcmVhdGVFZGl0b3JTdGF0ZSIsImNvbnRlbnQiLCJkZWNvcmF0b3JzIiwiY3JlYXRlRW1wdHkiLCJjcmVhdGVXaXRoQ29udGVudCIsImJhY2tncm91bmQiLCJib3JkZXIiLCJib3JkZXJUb3AiLCJjdXJzb3IiLCJtYXJnaW5Ub3AiLCJtYXJnaW4iLCJib3JkZXJMZWZ0IiwiY29sb3IiLCJmb250U3R5bGUiLCJ3aWR0aCIsIndoaXRlU3BhY2UiLCJBdG9taWMiLCJlbnRpdHkiLCJnZXRFbnRpdHkiLCJnZXRFbnRpdHlBdCIsImdldERhdGEiLCJhbnkiLCJoYW5kbGVTdHJhdGVneSIsImhhc2h0YWdTdHJhdGVneSIsIkhBTkRMRV9SRUdFWCIsIkhBU0hUQUdfUkVHRVgiLCJmaW5kV2l0aFJlZ2V4IiwicmVnZXgiLCJjb250ZW50QmxvY2siLCJjYWxsYmFjayIsInRleHQiLCJnZXRUZXh0IiwibWF0Y2hBcnIiLCJzdGFydCIsImV4ZWMiLCJpbmRleCIsImxlbmd0aCIsIkhhbmRsZVNwYW4iLCJoYW5kbGUiLCJjaGlsZHJlbiIsIkhhc2h0YWdTcGFuIiwiaGFzaHRhZyIsIkJsb2NrcyIsIlVOU1RZTEVEIiwiUEFSQUdSQVBIIiwiSDEiLCJIMiIsIkgzIiwiSDQiLCJINSIsIkg2IiwiT0wiLCJVTCIsIkJMT0NLUVVPVEUiLCJWSURFTyIsIklubGluZSIsIkJPTEQiLCJJVEFMSUMiLCJTVFJJS0VUSFJPVUdIIiwiVU5ERVJMSU5FIiwiSElHSExJR0hUIiwiRW50aXR5IiwiTElOSyIsIkhZUEVSTElOSyIsIkhBTkRMRUQiLCJOT1RfSEFORExFRCIsInN0eWxlIiwiZWRpdG9yIiwibWluSGVpZ2h0IiwiYnV0dG9uIiwidGV4dEFsaWduIiwiZGlyZWN0aW9uIiwidW5pY29kZUJpZGkiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxvUUFBb1E7O0FBRXRUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsNkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxrR0FBa0c7O0FBRXBKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCw2SkFBNko7O0FBRS9NO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxtRkFBbUY7O0FBRXJJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsb0M7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxrRkFBa0Y7O0FBRXBJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEscUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxvRkFBb0Y7O0FBRXRJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxtRkFBbUY7O0FBRXJJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCw2T0FBNk87O0FBRS9SO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsNkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx3REFBd0Q7O0FBRTFHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx1UkFBdVI7O0FBRXpVO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEscUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxrSkFBa0o7O0FBRXBNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEscUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxnREFBZ0Q7O0FBRWxHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsOEI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCwySEFBMkg7O0FBRTdLO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxrREFBa0Q7O0FBRXBHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxvSUFBb0k7O0FBRXRMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxzSEFBc0g7O0FBRXhLO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCx5V0FBeVc7O0FBRTNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCwyTkFBMk47O0FBRTdRO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsNkI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxzSkFBc0o7O0FBRXhNO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCwrQkFBK0I7O0FBRWpGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsd0I7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pDLGVBQVM7QUFETCxLQURtQjtBQUl6QkMsY0FBVTtBQUNSQyxZQUFNO0FBREUsS0FKZTtBQU96QkMsVUFBTTtBQUNKQyxvQkFBYztBQURWLEtBUG1CO0FBVXpCQztBQUNFQyxnQkFBVSxPQURaO0FBRUVDLGNBQVE7QUFGVixrREFHR1QsTUFBTVUsV0FBTixDQUFrQkMsRUFBbEIsQ0FBcUIsSUFBckIsQ0FISCxFQUdnQztBQUM1QkMsY0FBUSxNQURvQjtBQUU1QkMsYUFBTztBQUZxQixLQUhoQyw4Q0FPR2IsTUFBTVUsV0FBTixDQUFrQkksSUFBbEIsQ0FBdUIsSUFBdkIsQ0FQSCxFQU9rQztBQUM5QkYsY0FBUSxNQURzQjtBQUU5QkMsYUFBTztBQUZ1QixLQVBsQyw4Q0FXR2IsTUFBTVUsV0FBTixDQUFrQkksSUFBbEIsQ0FBdUIsSUFBdkIsQ0FYSCxFQVdrQztBQUM5QkYsY0FBUSxNQURzQjtBQUU5QkMsYUFBTztBQUZ1QixLQVhsQyw4Q0FlR2IsTUFBTVUsV0FBTixDQUFrQkksSUFBbEIsQ0FBdUIsSUFBdkIsQ0FmSCxFQWVrQztBQUM5QkMsZUFBUyxNQURxQjtBQUU5QkgsY0FBUSxNQUZzQjtBQUc5QkMsYUFBTztBQUh1QixLQWZsQztBQVZ5QixHQUFaO0FBQUEsQ0FBZixDLENBbkJBOztJQW9ETUcsUzs7O0FBQ0oscUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSw0SUFDWEEsS0FEVzs7QUFBQSxVQWdCbkJDLFlBaEJtQixHQWdCSjtBQUFBLGFBQU0sTUFBS0MsUUFBTCxDQUFjLEVBQUVDLE9BQU8sSUFBVCxFQUFkLENBQU47QUFBQSxLQWhCSTs7QUFBQSxVQWlCbkJDLFlBakJtQixHQWlCSjtBQUFBLGFBQU0sTUFBS0YsUUFBTCxDQUFjLEVBQUVDLE9BQU8sS0FBVCxFQUFkLENBQU47QUFBQSxLQWpCSTs7QUFFakIsVUFBS0UsS0FBTCxHQUFhO0FBQ1hDLGNBQVEscUNBQWtCTixNQUFNTyxPQUFOLENBQWNELE1BQWhDO0FBREcsS0FBYjs7QUFJQSxVQUFLRSxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0MsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlELElBQVosT0FBZDtBQUNBLFVBQUtFLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlGLElBQVosT0FBZDtBQVJpQjtBQVNsQjs7QUFFRDs7Ozs7NkJBQ1NILE0sRUFBUTtBQUNmLFdBQUtKLFFBQUwsQ0FBYyxFQUFFSSxjQUFGLEVBQWQ7QUFDRDs7OzZCQUtRO0FBQ1AsV0FBS0osUUFBTCxDQUFjLEVBQUVVLE1BQU0sQ0FBQyxLQUFLUCxLQUFMLENBQVdPLElBQXBCLEVBQWQ7QUFDRDs7Ozs7Ozs7Ozs7QUFFU04sc0IsR0FBVyxLQUFLRCxLLENBQWhCQyxNOzhCQUNzQixLQUFLTixLQUFMLENBQVdhLEksRUFBN0JDLE0sZUFBSkMsRSxFQUFZQyxLLGVBQUFBLEs7O3VCQUVnQixzQkFBYztBQUNoREQsc0JBQUlELE1BRDRDO0FBRWhERSw4QkFGZ0Q7QUFHaERWLDBCQUFRLDJCQUFhQSxPQUFPVyxpQkFBUCxFQUFiO0FBSHdDLGlCQUFkLEM7Ozs7QUFBNUJDLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSzs7c0JBTWhCRCxlQUFlLEc7Ozs7O0FBQ2pCO0FBQ0FFLHdCQUFRRCxLQUFSLENBQWNBLEtBQWQ7Ozs7O0FBSUYscUJBQUtuQixLQUFMLENBQVdxQixnQkFBWCxDQUE0QjtBQUMxQk4sc0JBQUlELE1BRHNCO0FBRTFCUiwwQkFBUSwyQkFBYUEsT0FBT1csaUJBQVAsRUFBYjtBQUZrQixpQkFBNUI7O0FBS0EscUJBQUtmLFFBQUwsQ0FBYyxFQUFFVSxNQUFNLEtBQVIsRUFBZDs7Ozs7Ozs7Ozs7Ozs7Ozs7OzZCQUdPO0FBQUEsbUJBQzRCLEtBQUtaLEtBRGpDO0FBQUEsVUFDQ3NCLE9BREQsVUFDQ0EsT0FERDtBQUFBLFVBQ1VmLE9BRFYsVUFDVUEsT0FEVjtBQUFBLFVBQ21CTSxJQURuQixVQUNtQkEsSUFEbkI7QUFBQSxVQUdLVSxTQUhMLEdBRytCaEIsT0FIL0IsQ0FHQ1EsRUFIRDtBQUFBLFVBR2dCUyxVQUhoQixHQUcrQmpCLE9BSC9CLENBR2dCaUIsVUFIaEI7QUFBQSxVQUlLVixNQUpMLEdBSWdCRCxJQUpoQixDQUlDRSxFQUpEO0FBQUEsbUJBTXlCLEtBQUtWLEtBTjlCO0FBQUEsVUFNQ0YsS0FORCxVQU1DQSxLQU5EO0FBQUEsVUFNUVMsSUFOUixVQU1RQSxJQU5SO0FBQUEsVUFNY04sTUFOZCxVQU1jQSxNQU5kOzs7QUFRUCxhQUNFO0FBQUE7QUFBQSxVQUFNLGVBQU4sRUFBZ0IsU0FBUyxDQUF6QixFQUE0QixTQUFRLFFBQXBDLEVBQTZDLFdBQVdnQixRQUFRdEMsSUFBaEU7QUFDRTtBQUFBO0FBQUE7QUFDRSxzQkFERjtBQUVFLGdCQUFJLEVBRk47QUFHRSxnQkFBSSxFQUhOO0FBSUUsZ0JBQUksRUFKTjtBQUtFLGdCQUFJLENBTE47QUFNRSxnQkFBSSxDQU5OO0FBT0UsdUJBQVdzQyxRQUFRRztBQVByQjtBQVNFO0FBQUE7QUFBQTtBQUNFLDRCQUFjLEtBQUt4QixZQURyQjtBQUVFLDRCQUFjLEtBQUtHO0FBRnJCO0FBSUU7QUFBQTtBQUFBLGdCQUFNLFFBQVFELEtBQWQsRUFBcUIsV0FBV21CLFFBQVFsQyxJQUF4QztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQ0UsK0JBQWFrQixNQURmO0FBRUUsNEJBQVUsS0FBS0UsUUFGakI7QUFHRSw0QkFBVSxDQUFDSTtBQUhiO0FBREY7QUFERixhQUpGO0FBYUdZLDBCQUFjVixXQUFXUyxTQUF6QixHQUNDO0FBQUE7QUFBQTtBQUNFLHlCQURGO0FBRUUsdUJBQU0sUUFGUjtBQUdFLDhCQUFXLE1BSGI7QUFJRSwyQkFBV0QsUUFBUWhDLFVBSnJCO0FBS0UseUJBQVNzQixPQUFPLEtBQUtELE1BQVosR0FBcUIsS0FBS0Q7QUFMckM7QUFPR0UscUJBQU8sbURBQVAsR0FBc0I7QUFQekIsYUFERCxHQVVHO0FBdkJOO0FBVEY7QUFERixPQURGO0FBdUNEOzs7OztBQUdIYixVQUFVMkIsU0FBVixHQUFzQjtBQUNwQkosV0FBUyxvQkFBVUssTUFBVixDQUFpQkMsVUFETjs7QUFHcEJmLFFBQU0sb0JBQVVjLE1BQVYsQ0FBaUJDLFVBSEg7QUFJcEJyQixXQUFTLG9CQUFVb0IsTUFBVixDQUFpQkMsVUFKTjs7QUFNcEJQLG9CQUFrQixvQkFBVVE7QUFOUixDQUF0Qjs7a0JBU2UsMEJBQVcvQyxNQUFYLEVBQW1CaUIsU0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM1SmY7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFHQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQVVBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQVFBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFrQkE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7O0FBaENBO0FBQ0E7QUFDQTs7QUFFQTs7QUE3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7SUFxRE0rQixZOzs7QUFDSjs7O0FBeUJBLDBCQUFjO0FBQUE7O0FBQUE7O0FBRVo7O0FBRUE7Ozs7OztBQUpZOztBQUFBLFVBNkxkQyxtQkE3TGM7QUFBQSwwRkE2TFEsaUJBQU9DLENBQVA7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNwQjtBQUNNQyxvQkFGYyxHQUVQRCxFQUFFRSxNQUFGLENBQVNDLEtBQVQsQ0FBZSxDQUFmLENBRk87O0FBQUEsc0JBSWhCRixLQUFLRyxJQUFMLENBQVVDLE9BQVYsQ0FBa0IsUUFBbEIsTUFBZ0MsQ0FKaEI7QUFBQTtBQUFBO0FBQUE7O0FBS1pDLHdCQUxZLEdBS0QsSUFBSUMsUUFBSixFQUxDOztBQU1sQkQseUJBQVNFLE1BQVQsQ0FBZ0IsT0FBaEIsRUFBeUJQLElBQXpCOztBQU5rQjtBQUFBLHVCQVEyQixNQUFLakMsS0FBTCxDQUFXeUMsY0FBWCxDQUEwQjtBQUNyRUg7QUFEcUUsaUJBQTFCLENBUjNCOztBQUFBO0FBQUE7QUFRVnBCLDBCQVJVLFNBUVZBLFVBUlU7QUFRRUMscUJBUkYsU0FRRUEsS0FSRjtBQVFTdUIsdUJBUlQsU0FRU0EsT0FSVDs7QUFBQSxzQkFZZHhCLGNBQWMsR0FaQTtBQUFBO0FBQUE7QUFBQTs7QUFhaEI7QUFDQUUsd0JBQVFELEtBQVIsQ0FBY0QsVUFBZCxFQUEwQkMsS0FBMUI7QUFkZ0I7O0FBQUE7QUFrQkF3QixtQkFsQkEsR0FrQlFELE9BbEJSLENBa0JWRSxRQWxCVTtBQW9CVkMsMkJBcEJVLEdBb0JNLE1BQUs3QyxLQXBCWCxDQW9CVjZDLFdBcEJVO0FBc0JaQyw0QkF0QlksR0FzQkdELFlBQVk1QixpQkFBWixFQXRCSDtBQXVCWjhCLHNDQXZCWSxHQXVCYUQsYUFBYUUsWUFBYixDQUM3QixrQkFBT0MsS0FEc0IsRUFFN0IsV0FGNkIsRUFHN0IsRUFBRU4sUUFBRixFQUg2QixDQXZCYjtBQTZCWk8seUJBN0JZLEdBNkJBSCx1QkFBdUJJLHVCQUF2QixFQTdCQTtBQStCWkMsaUNBL0JZLEdBK0JRLHFCQUFZQyxHQUFaLENBQWdCUixXQUFoQixFQUE2QjtBQUNyRFMsa0NBQWdCUDtBQURxQyxpQkFBN0IsQ0EvQlI7QUFtQ1pRLDhCQW5DWSxHQW1DSywwQkFBaUJDLGlCQUFqQixDQUNyQkosaUJBRHFCLEVBRXJCRixTQUZxQixFQUdyQixHQUhxQixDQW5DTDs7O0FBeUNsQixzQkFBSzFDLFFBQUwsQ0FBYytDLGNBQWQ7O0FBekNrQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQTdMUjs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFVWixVQUFLL0MsUUFBTCxHQUFnQixVQUFDcUMsV0FBRCxFQUFpQjtBQUMvQixZQUFLN0MsS0FBTCxDQUFXUSxRQUFYLENBQW9CcUMsV0FBcEI7QUFDRCxLQUZEOztBQUlBOzs7O0FBSUEsVUFBS1ksS0FBTCxHQUFhO0FBQUEsYUFBTSxNQUFLQyxVQUFMLENBQWdCRCxLQUFoQixFQUFOO0FBQUEsS0FBYjs7QUFFQSxVQUFLRSxLQUFMLEdBQWEsTUFBS0EsS0FBTCxDQUFXbEQsSUFBWCxPQUFiO0FBQ0EsVUFBS21ELFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQm5ELElBQWxCLE9BQXBCO0FBQ0EsVUFBS29ELFdBQUwsR0FBbUIsTUFBS0EsV0FBTCxDQUFpQnBELElBQWpCLE9BQW5CO0FBQ0EsVUFBS3FELFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQnJELElBQWxCLE9BQXBCO0FBQ0EsVUFBS3NELG1CQUFMLEdBQTJCLE1BQUtBLG1CQUFMLENBQXlCdEQsSUFBekIsT0FBM0I7QUFDQSxVQUFLdUQsbUJBQUwsR0FBMkIsTUFBS0EsbUJBQUwsQ0FBeUJ2RCxJQUF6QixPQUEzQjtBQUNBLFVBQUt3RCxXQUFMLEdBQW1CLE1BQUtBLFdBQUwsQ0FBaUJ4RCxJQUFqQixPQUFuQjtBQUNBLFVBQUt5RCxhQUFMLEdBQXFCLE1BQUtBLGFBQUwsQ0FBbUJ6RCxJQUFuQixPQUFyQjtBQUNBLFVBQUswRCxnQkFBTCxHQUF3QixNQUFLQSxnQkFBTCxDQUFzQjFELElBQXRCLE9BQXhCO0FBQ0EsVUFBSzJELGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCM0QsSUFBdEIsT0FBeEI7QUFDQSxVQUFLNEQsZ0JBQUwsR0FBd0IsTUFBS0EsZ0JBQUwsQ0FBc0I1RCxJQUF0QixPQUF4QjtBQUNBLFVBQUs2RCxrQkFBTCxHQUEwQixNQUFLQSxrQkFBTCxDQUF3QjdELElBQXhCLE9BQTFCOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNUNZO0FBNkNiOztBQUVEOzs7Ozs7Ozs7MEJBS011QixDLEVBQUc7QUFDUEEsUUFBRXVDLGNBQUY7QUFETyxVQUVDMUIsV0FGRCxHQUVpQixLQUFLN0MsS0FGdEIsQ0FFQzZDLFdBRkQ7OztBQUlQLFVBQU1VLGlCQUFpQixtQkFBVUksS0FBVixDQUFnQjNCLENBQWhCLEVBQW1CYSxXQUFuQixFQUFnQyxDQUFoQyxDQUF2Qjs7QUFFQSxXQUFLckMsUUFBTCxDQUFjK0MsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O21DQUllO0FBQUEsVUFDTFYsV0FESyxHQUNXLEtBQUs3QyxLQURoQixDQUNMNkMsV0FESzs7O0FBR2IsVUFBTVUsaUJBQWlCLG1CQUFVaUIsZUFBVixDQUEwQjNCLFdBQTFCLEVBQXVDLFlBQXZDLENBQXZCOztBQUVBLFdBQUtyQyxRQUFMLENBQWMrQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBSzdDLEtBRGpCLENBQ0o2QyxXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQTBCM0IsV0FBMUIsRUFBdUMsWUFBdkMsQ0FBdkI7O0FBRUEsV0FBS3JDLFFBQUwsQ0FBYytDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzttQ0FJZTtBQUFBLFVBQ0xWLFdBREssR0FDVyxLQUFLN0MsS0FEaEIsQ0FDTDZDLFdBREs7OztBQUdiLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FBMEIzQixXQUExQixFQUF1QyxZQUF2QyxDQUF2Qjs7QUFFQSxXQUFLckMsUUFBTCxDQUFjK0MsY0FBZDtBQUNEOztBQUVEOzs7Ozs7OzBDQUlzQjtBQUFBLFVBQ1pWLFdBRFksR0FDSSxLQUFLN0MsS0FEVCxDQUNaNkMsV0FEWTs7O0FBR3BCLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FDckIzQixXQURxQixFQUVyQixxQkFGcUIsQ0FBdkI7O0FBS0EsV0FBS3JDLFFBQUwsQ0FBYytDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzswQ0FJc0I7QUFBQSxVQUNaVixXQURZLEdBQ0ksS0FBSzdDLEtBRFQsQ0FDWjZDLFdBRFk7OztBQUdwQixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQ3JCM0IsV0FEcUIsRUFFckIsbUJBRnFCLENBQXZCOztBQUtBLFdBQUtyQyxRQUFMLENBQWMrQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBSzdDLEtBRGpCLENBQ0o2QyxXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLE1BQXpDLENBQXZCOztBQUVBLFdBQUtyQyxRQUFMLENBQWMrQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7b0NBSWdCO0FBQUEsVUFDTlYsV0FETSxHQUNVLEtBQUs3QyxLQURmLENBQ042QyxXQURNOzs7QUFHZCxVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLFFBQXpDLENBQXZCOztBQUVBLFdBQUtyQyxRQUFMLENBQWMrQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7dUNBSW1CO0FBQUEsVUFDVFYsV0FEUyxHQUNPLEtBQUs3QyxLQURaLENBQ1Q2QyxXQURTOzs7QUFHakIsVUFBTVUsaUJBQWlCLG1CQUFVa0IsaUJBQVYsQ0FDckI1QixXQURxQixFQUVyQixXQUZxQixDQUF2Qjs7QUFLQSxXQUFLckMsUUFBTCxDQUFjK0MsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O3VDQUltQjtBQUFBLFVBQ1RWLFdBRFMsR0FDTyxLQUFLN0MsS0FEWixDQUNUNkMsV0FEUzs7O0FBR2pCLFVBQU1VLGlCQUFpQixtQkFBVWtCLGlCQUFWLENBQTRCNUIsV0FBNUIsRUFBeUMsTUFBekMsQ0FBdkI7O0FBRUEsV0FBS3JDLFFBQUwsQ0FBYytDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7O3lDQUdxQjtBQUNuQixXQUFLbUIsS0FBTCxDQUFXQyxLQUFYLEdBQW1CLElBQW5CO0FBQ0EsV0FBS0QsS0FBTCxDQUFXRSxLQUFYO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBZ0RBOzs7Ozs7cUNBTWlCQyxPLEVBQVM7QUFBQSxVQUNoQmhDLFdBRGdCLEdBQ0EsS0FBSzdDLEtBREwsQ0FDaEI2QyxXQURnQjs7O0FBR3hCLFVBQU1VLGlCQUFpQixtQkFBVWMsZ0JBQVYsQ0FBMkJ4QixXQUEzQixFQUF3Q2dDLE9BQXhDLENBQXZCOztBQUVBLFVBQUl0QixjQUFKLEVBQW9CO0FBQ2xCLGFBQUsvQyxRQUFMLENBQWMrQyxjQUFkO0FBQ0E7QUFDRDs7QUFFRDtBQUNEOztBQUVEOzs7O29DQUNnQnVCLEssRUFBTztBQUNyQixjQUFRQSxNQUFNQyxPQUFOLEVBQVI7QUFDRSxhQUFLLGtCQUFPQyxNQUFaO0FBQ0UsaUJBQU87QUFDTEMsdUNBREs7QUFFTEMsc0JBQVU7QUFGTCxXQUFQOztBQUtGO0FBQ0UsaUJBQU8sSUFBUDtBQVJKO0FBVUQ7QUFDRDs7QUFFQTs7OztpQ0FDYUosSyxFQUFPO0FBQ2xCLGNBQVFBLE1BQU1DLE9BQU4sRUFBUjtBQUNFLGFBQUssWUFBTDtBQUNFLGlCQUFPLHVCQUFQOztBQUVGO0FBQ0UsaUJBQU8sSUFBUDtBQUxKO0FBT0Q7QUFDRDs7Ozs2QkFFUztBQUFBOztBQUFBLG1CQU9ILEtBQUsvRSxLQVBGO0FBQUEsVUFFTHNCLE9BRkssVUFFTEEsT0FGSztBQUFBLFVBR0w2RCxRQUhLLFVBR0xBLFFBSEs7QUFBQSxVQUlMM0UsUUFKSyxVQUlMQSxRQUpLO0FBQUEsVUFLTHFDLFdBTEssVUFLTEEsV0FMSztBQUFBLFVBTUx1QyxXQU5LLFVBTUxBLFdBTks7O0FBU1A7O0FBQ0EsVUFBTUMsV0FBVztBQUNmQyxjQUFNO0FBQ0pDLDJCQUFpQixxQkFEYjtBQUVKQyxzQkFBWSwrQ0FGUjtBQUdKQyxvQkFBVSxFQUhOO0FBSUp4RyxtQkFBUztBQUpMO0FBRFMsT0FBakI7O0FBU0EsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFXcUMsUUFBUXRDLElBQXhCO0FBQ0csU0FBQ21HLFFBQUQsR0FDQztBQUFBO0FBQUEsWUFBSyxXQUFXLDBCQUFXLEVBQUUsbUJBQW1CLEVBQXJCLEVBQVgsQ0FBaEI7QUFDRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE9BQWYsRUFBdUIsSUFBRyxPQUExQixFQUFrQyxXQUFVLFFBQTVDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsT0FBdkIsRUFBK0IsU0FBUyxLQUFLdkIsWUFBN0M7QUFDRTtBQURGO0FBREYsV0FERjtBQU1FO0FBQUE7QUFBQSxjQUFTLE9BQU0sTUFBZixFQUFzQixJQUFHLE1BQXpCLEVBQWdDLFdBQVUsUUFBMUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxNQUF2QixFQUE4QixTQUFTLEtBQUtLLFdBQTVDO0FBQ0U7QUFERjtBQURGLFdBTkY7QUFXRTtBQUFBO0FBQUEsY0FBUyxPQUFNLFFBQWYsRUFBd0IsSUFBRyxRQUEzQixFQUFvQyxXQUFVLFFBQTlDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsUUFBdkIsRUFBZ0MsU0FBUyxLQUFLQyxhQUE5QztBQUNFO0FBREY7QUFERixXQVhGO0FBZ0JFO0FBQUE7QUFBQSxjQUFTLE9BQU0sV0FBZixFQUEyQixJQUFHLFdBQTlCLEVBQTBDLFdBQVUsUUFBcEQ7QUFDRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxXQURiO0FBRUUseUJBQVMsS0FBS0M7QUFGaEI7QUFJRTtBQUpGO0FBREYsV0FoQkY7QUF3QkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxNQUFmLEVBQXNCLElBQUcsTUFBekIsRUFBZ0MsV0FBVSxRQUExQztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE1BQXZCLEVBQThCLFNBQVMsS0FBS04sV0FBNUM7QUFDRTtBQURGO0FBREYsV0F4QkY7QUE2QkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxPQUFmLEVBQXVCLElBQUcsT0FBMUIsRUFBa0MsV0FBVSxRQUE1QztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE9BQXZCLEVBQStCLFNBQVMsS0FBS0MsWUFBN0M7QUFDRTtBQURGO0FBREYsV0E3QkY7QUFrQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQU0sZ0JBRFI7QUFFRSxrQkFBRyxlQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGdCQURiO0FBRUUseUJBQVMsS0FBS0M7QUFGaEI7QUFJRTtBQUpGO0FBTEYsV0FsQ0Y7QUE4Q0U7QUFBQTtBQUFBLGNBQVMsT0FBTSxjQUFmLEVBQThCLElBQUcsY0FBakMsRUFBZ0QsV0FBVSxRQUExRDtBQUNFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGNBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFERixXQTlDRjtBQXNERTtBQUFBO0FBQUEsY0FBUyxPQUFNLFlBQWYsRUFBNEIsSUFBRyxZQUEvQixFQUE0QyxXQUFVLFFBQXREO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsWUFBdkIsRUFBb0MsY0FBcEM7QUFDRTtBQURGO0FBREYsV0F0REY7QUEyREU7QUFBQTtBQUFBLGNBQVMsT0FBTSxjQUFmLEVBQThCLElBQUcsY0FBakMsRUFBZ0QsV0FBVSxRQUExRDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGNBQXZCLEVBQXNDLGNBQXRDO0FBQ0U7QUFERjtBQURGLFdBM0RGO0FBZ0VFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQWhFRjtBQXFFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0FyRUY7QUEwRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBMUVGO0FBK0VFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQS9FRjtBQW9GRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsY0FEYjtBQUVFLDBCQUFVLE9BQU8sS0FBS2hFLEtBQUwsQ0FBV3lDLGNBQWxCLEtBQXFDLFVBRmpEO0FBR0UseUJBQVMsS0FBSzZCO0FBSGhCO0FBS0Usd0VBTEY7QUFNRTtBQUNFLHNCQUFLLE1BRFA7QUFFRSx3QkFBTyxvQkFGVDtBQUdFLDBCQUFVLEtBQUt2QyxtQkFIakI7QUFJRSxxQkFBSyxhQUFDMkMsS0FBRCxFQUFXO0FBQ2QseUJBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNELGlCQU5IO0FBT0UsdUJBQU8sRUFBRTVFLFNBQVMsTUFBWDtBQVBUO0FBTkY7QUFERixXQXBGRjtBQXNHRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxpQkFEUjtBQUVFLGtCQUFHLGtCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGlCQUF2QixFQUF5QyxjQUF6QztBQUNFO0FBREY7QUFMRixXQXRHRjtBQStHRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGdCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGdCQUF2QixFQUF3QyxjQUF4QztBQUNFO0FBREY7QUFMRixXQS9HRjtBQXdIRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGdCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGdCQURiO0FBRUUseUJBQVMsS0FBS3NFO0FBRmhCO0FBSUU7QUFKRjtBQUxGLFdBeEhGO0FBb0lFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGVBRFI7QUFFRSxrQkFBRyxlQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGVBQXZCLEVBQXVDLGNBQXZDO0FBQ0U7QUFERjtBQUxGO0FBcElGLFNBREQsR0ErSUcsSUFoSk47QUFpSkU7QUFBQTtBQUFBLFlBQUssV0FBVywwQkFBVyxFQUFFLGVBQWUsRUFBakIsRUFBWCxDQUFoQjtBQUNFO0FBQ0U7O0FBREYsY0FHRSxhQUFhdkIsV0FIZjtBQUlFLHNCQUFVckM7QUFDVjs7QUFMRixjQU9FLGFBQWE0RTtBQUNiOztBQUVBOztBQVZGLGNBWUUsaUJBQWlCLEtBQUtNLGVBWnhCO0FBYUUsMEJBQWMsS0FBS0MsWUFickI7QUFjRSw0QkFBZ0JOO0FBQ2hCOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQXZCRixjQXlCRSxVQUFVRixRQXpCWjtBQTBCRTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQW5DRixjQXFDRSxrQkFBa0IsS0FBS2Q7QUFDdkI7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBbERGLGNBb0RFLE9BQU8sS0FBS1Y7QUFDWjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUEzRUYsY0E2RUUsS0FBSyxhQUFDaUMsSUFBRCxFQUFVO0FBQ2IscUJBQUtsQyxVQUFMLEdBQWtCa0MsSUFBbEI7QUFDRDtBQS9FSDtBQURGO0FBakpGLE9BREY7QUF1T0Q7Ozs7O0FBNWpCSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQTNEQTtBQVJBOztBQTZFTTlELFksQ0FJR0osUyxHQUFZO0FBQ2pCSixXQUFTLHlCQUFVSyxNQUFWLENBQWlCQyxVQURUOztBQUdqQmlCLGVBQWEseUJBQVVsQixNQUFWLENBQWlCQyxVQUhiO0FBSWpCcEIsWUFBVSx5QkFBVXFCLElBQVYsQ0FBZUQsVUFKUjs7QUFNakJ3RCxlQUFhLHlCQUFVUyxNQU5OOztBQVFqQlYsWUFBVSx5QkFBVVcsSUFBVixDQUFlbEUsVUFSUjs7QUFVakI7Ozs7QUFJQWEsa0JBQWdCLHlCQUFVWjtBQWRULEM7QUFKZkMsWSxDQXFCR2lFLFksR0FBZTtBQUNwQlosWUFBVSxJQURVO0FBRXBCQyxlQUFhO0FBRk8sQztrQkEwaEJULGtEQUFtQnRELFlBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMW5CZjs7QUFFQTs7QUFKQTs7QUFXQSxJQUFNa0Usb0JBQW9CLGdDQUF1QixDQUMvQztBQUNFQyxzQ0FERjtBQUVFaEI7QUFGRixDQUQrQyxFQUsvQztBQUNFZ0IsdUNBREY7QUFFRWhCO0FBRkYsQ0FMK0MsQ0FBdkIsQ0FBMUI7O0FBV08sSUFBTWlCLGdEQUFvQixTQUFwQkEsaUJBQW9CLEdBRzVCO0FBQUEsTUFGSEMsT0FFRyx1RUFGTyxJQUVQO0FBQUEsTUFESEMsVUFDRyx1RUFEVUosaUJBQ1Y7O0FBQ0gsTUFBSUcsWUFBWSxJQUFoQixFQUFzQjtBQUNwQixXQUFPLHFCQUFZRSxXQUFaLENBQXdCRCxVQUF4QixDQUFQO0FBQ0Q7QUFDRCxTQUFPLHFCQUFZRSxpQkFBWixDQUE4Qiw2QkFBZUgsT0FBZixDQUE5QixFQUF1REMsVUFBdkQsQ0FBUDtBQUNELENBUk07O2tCQVVRRixpQjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaENmOzs7Ozs7a0JBTWU7QUFDYixhQUFXO0FBQ1Qsd0JBQW9CO0FBQ2xCSyxrQkFBWSxNQURNO0FBRWxCQyxjQUFRLGdCQUZVO0FBR2xCaEIsa0JBQVksa0JBSE07QUFJbEJDLGdCQUFVLE1BSlE7QUFLbEJ4RyxlQUFTO0FBTFMsS0FEWDtBQVFULDBCQUFzQjtBQUNwQndILGlCQUFXLGdCQURTO0FBRXBCQyxjQUFRLE1BRlk7QUFHcEJqQixnQkFBVSxNQUhVO0FBSXBCa0IsaUJBQVc7QUFKUyxLQVJiO0FBY1QsMkNBQXVDO0FBQ3JDQyxjQUFRLGVBRDZCO0FBRXJDM0gsZUFBUztBQUY0QixLQWQ5QjtBQWtCVCxtQ0FBK0I7QUFDN0IySCxjQUFRLGVBRHFCO0FBRTdCM0gsZUFBUztBQUNUO0FBSDZCLEtBbEJ0QjtBQXVCVCw4QkFBMEI7QUFDeEJzRyx1QkFBaUIsZ0JBRE87QUFFeEJzQixrQkFBWSxnQkFGWTtBQUd4QkMsYUFBTyxNQUhpQjtBQUl4QnRCLGtCQUFZLGtDQUpZO0FBS3hCdUIsaUJBQVcsUUFMYTtBQU14QkgsY0FBUSxRQU5nQjtBQU94QjNILGVBQVM7QUFQZSxLQXZCakI7QUFnQ1QscUNBQWlDO0FBQy9Cc0csdUJBQWlCLHFCQURjO0FBRS9CQyxrQkFBWSwrQ0FGbUI7QUFHL0JDLGdCQUFVLE1BSHFCO0FBSS9CeEcsZUFBUztBQUpzQjtBQWhDeEIsR0FERTtBQXdDYkQsUUFBTTtBQUNKO0FBREksR0F4Q087QUEyQ2JFLFlBQVU7QUFDUkMsVUFBTTtBQURFO0FBM0NHLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0RmOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBLElBQU1MLFNBQVM7QUFDYjRGLFNBQU87QUFDTHNDLFdBQU8sTUFERjtBQUVMO0FBQ0E7QUFDQUMsZ0JBQVk7QUFKUDtBQURNLENBQWY7O0FBU0E7Ozs7Ozs7O0FBbkJBOzs7OztJQTJCTUMsTTs7O0FBTUosa0JBQVlsSCxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtLLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUVRO0FBQUEsbUJBQ3lCLEtBQUtMLEtBRDlCO0FBQUEsVUFDQzhDLFlBREQsVUFDQ0EsWUFERDtBQUFBLFVBQ2VnQyxLQURmLFVBQ2VBLEtBRGY7OztBQUdQLFVBQU1xQyxTQUFTckUsYUFBYXNFLFNBQWIsQ0FBdUJ0QyxNQUFNdUMsV0FBTixDQUFrQixDQUFsQixDQUF2QixDQUFmOztBQUhPLDRCQUlTRixPQUFPRyxPQUFQLEVBSlQ7QUFBQSxVQUlDM0UsR0FKRCxtQkFJQ0EsR0FKRDs7QUFLUCxVQUFNUCxPQUFPK0UsT0FBT3BDLE9BQVAsRUFBYjs7QUFFQSxVQUFJM0MsU0FBUyxrQkFBT2EsS0FBcEIsRUFBMkI7QUFDekIsZUFDRTtBQUFBO0FBQUE7QUFDRSxpREFBSyxLQUFLLEtBQVYsRUFBaUIsS0FBS04sR0FBdEIsRUFBMkIsT0FBTzdELE9BQU80RixLQUF6QztBQURGLFNBREY7QUFLRDs7QUFFRCxhQUFPLDBDQUFQO0FBQ0Q7Ozs7O0FBM0JHd0MsTSxDQUNHeEYsUyxHQUFZO0FBQ2pCb0IsZ0JBQWMsb0JBQVVuQixNQUFWLENBQWlCQyxVQURkO0FBRWpCa0QsU0FBTyxvQkFBVXlDLEdBQVYsQ0FBYzNGO0FBRkosQztrQkE2Qk5zRixNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDdkNDTSxjLEdBQUFBLGM7UUFJQUMsZSxHQUFBQSxlOztBQXBCaEI7Ozs7QUFDQTs7Ozs7O0FBSEE7QUFDQTtBQUlBLElBQU1DLGVBQWUsVUFBckI7QUFDQSxJQUFNQyxnQkFBZ0IsdUJBQXRCOztBQUVBLFNBQVNDLGFBQVQsQ0FBdUJDLEtBQXZCLEVBQThCQyxZQUE5QixFQUE0Q0MsUUFBNUMsRUFBc0Q7QUFDcEQsTUFBTUMsT0FBT0YsYUFBYUcsT0FBYixFQUFiO0FBQ0EsTUFBSUMsaUJBQUo7QUFBQSxNQUNFQyxjQURGO0FBRUEsU0FBTyxDQUFDRCxXQUFXTCxNQUFNTyxJQUFOLENBQVdKLElBQVgsQ0FBWixNQUFrQyxJQUF6QyxFQUErQztBQUM3Q0csWUFBUUQsU0FBU0csS0FBakI7QUFDQU4sYUFBU0ksS0FBVCxFQUFnQkEsUUFBUUQsU0FBUyxDQUFULEVBQVlJLE1BQXBDO0FBQ0Q7QUFDRjs7QUFFTSxTQUFTZCxjQUFULENBQXdCTSxZQUF4QixFQUFzQ0MsUUFBdEMsRUFBZ0RqRixZQUFoRCxFQUE4RDtBQUNuRThFLGdCQUFjRixZQUFkLEVBQTRCSSxZQUE1QixFQUEwQ0MsUUFBMUM7QUFDRDs7QUFFTSxTQUFTTixlQUFULENBQXlCSyxZQUF6QixFQUF1Q0MsUUFBdkMsRUFBaURqRixZQUFqRCxFQUErRDtBQUNwRThFLGdCQUFjRCxhQUFkLEVBQTZCRyxZQUE3QixFQUEyQ0MsUUFBM0M7QUFDRDs7QUFFTSxJQUFNUSxrQ0FBYSxTQUFiQSxVQUFhO0FBQUEsU0FBUztBQUFBO0FBQUEsTUFBTSxPQUFPLGdCQUFNQyxNQUFuQjtBQUE0QnhJLFVBQU15STtBQUFsQyxHQUFUO0FBQUEsQ0FBbkI7O0FBRUEsSUFBTUMsb0NBQWMsU0FBZEEsV0FBYztBQUFBLFNBQVM7QUFBQTtBQUFBLE1BQU0sT0FBTyxnQkFBTUMsT0FBbkI7QUFBNkIzSSxVQUFNeUk7QUFBbkMsR0FBVDtBQUFBLENBQXBCOztrQkFHUTtBQUNiakIsZ0NBRGE7QUFFYmUsd0JBRmE7O0FBSWJkLGtDQUphO0FBS2JpQjtBQUxhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CZjs7Ozs7OztBQU9BOzs7QUFHTyxJQUFNRSwwQkFBUztBQUNwQkMsWUFBVSxVQURVO0FBRXBCQyxhQUFXLFVBRlM7O0FBSXBCQyxNQUFJLFlBSmdCO0FBS3BCQyxNQUFJLFlBTGdCO0FBTXBCQyxNQUFJLGNBTmdCO0FBT3BCQyxNQUFJLGFBUGdCO0FBUXBCQyxNQUFJLGFBUmdCO0FBU3BCQyxNQUFJLFlBVGdCOztBQVdwQkMsTUFBSSxtQkFYZ0I7QUFZcEJDLE1BQUkscUJBWmdCOztBQWNwQmhFLFFBQU0sWUFkYzs7QUFnQnBCaUUsY0FBWSxZQWhCUTs7QUFrQnBCdkUsVUFBUSxRQWxCWTtBQW1CcEIvQixTQUFPLGNBbkJhO0FBb0JwQnVHLFNBQU87QUFwQmEsQ0FBZjs7QUF1QlA7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNLE1BRGM7QUFFcEJwRSxRQUFNLE1BRmM7QUFHcEJxRSxVQUFRLFFBSFk7QUFJcEJDLGlCQUFlLGVBSks7QUFLcEJDLGFBQVcsV0FMUztBQU1wQkMsYUFBVztBQU5TLENBQWY7O0FBU1A7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNO0FBRGMsQ0FBZjs7QUFJUDs7O0FBR08sSUFBTUMsZ0NBQVksV0FBbEI7O0FBRVA7OztBQUdPLElBQU1DLDRCQUFVLFNBQWhCO0FBQ0EsSUFBTUMsb0NBQWMsYUFBcEI7O2tCQUVRO0FBQ2J2QixnQkFEYTtBQUViYSxnQkFGYTtBQUdiTTtBQUhhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0RmOzs7OzttQkFFU2pJLFk7Ozs7Ozs7Ozt3QkFFQW9FLGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05UOztBQUVPLElBQU1rRSx3QkFBUTtBQUNuQnBMLFFBQU07QUFDSkMsYUFBUyxFQURMO0FBRUorSCxXQUFPO0FBRkgsR0FEYTtBQUtuQnFELFVBQVE7QUFDTjdELFlBQVEsZ0JBREY7QUFFTkUsWUFBUSxNQUZGO0FBR05qQixjQUFVLEVBSEo7QUFJTjZFLGVBQVcsRUFKTDtBQUtOckwsYUFBUztBQUxILEdBTFc7QUFZbkJzTCxVQUFRO0FBQ041RCxlQUFXLEVBREw7QUFFTjZELGVBQVc7QUFGTCxHQVpXO0FBZ0JuQmhDLFVBQVE7QUFDTjFCLFdBQU8seUJBREQ7QUFFTjJELGVBQVcsS0FGTDtBQUdOQyxpQkFBYTtBQUhQLEdBaEJXO0FBcUJuQi9CLFdBQVM7QUFDUDdCLFdBQU87QUFEQTtBQXJCVSxDQUFkOztrQkEwQlFzRCxLIiwiZmlsZSI6IjQ3LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE2LjUgNnYxMS41YzAgMi4yMS0xLjc5IDQtNCA0cy00LTEuNzktNC00VjVjMC0xLjM4IDEuMTItMi41IDIuNS0yLjVzMi41IDEuMTIgMi41IDIuNXYxMC41YzAgLjU1LS40NSAxLTEgMXMtMS0uNDUtMS0xVjZIMTB2OS41YzAgMS4zOCAxLjEyIDIuNSAyLjUgMi41czIuNS0xLjEyIDIuNS0yLjVWNWMwLTIuMjEtMS43OS00LTQtNFM3IDIuNzkgNyA1djEyLjVjMCAzLjA0IDIuNDYgNS41IDUuNSA1LjVzNS41LTIuNDYgNS41LTUuNVY2aC0xLjV6JyB9KTtcblxudmFyIEF0dGFjaEZpbGUgPSBmdW5jdGlvbiBBdHRhY2hGaWxlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5BdHRhY2hGaWxlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShBdHRhY2hGaWxlKTtcbkF0dGFjaEZpbGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQXR0YWNoRmlsZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9BdHRhY2hGaWxlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9BdHRhY2hGaWxlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTkuNCAxNi42TDQuOCAxMmw0LjYtNC42TDggNmwtNiA2IDYgNiAxLjQtMS40em01LjIgMGw0LjYtNC42LTQuNi00LjZMMTYgNmw2IDYtNiA2LTEuNC0xLjR6JyB9KTtcblxudmFyIENvZGUgPSBmdW5jdGlvbiBDb2RlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Db2RlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShDb2RlKTtcbkNvZGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29kZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db2RlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db2RlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMgMTcuMjVWMjFoMy43NUwxNy44MSA5Ljk0bC0zLjc1LTMuNzVMMyAxNy4yNXpNMjAuNzEgNy4wNGMuMzktLjM5LjM5LTEuMDIgMC0xLjQxbC0yLjM0LTIuMzRjLS4zOS0uMzktMS4wMi0uMzktMS40MSAwbC0xLjgzIDEuODMgMy43NSAzLjc1IDEuODMtMS44M3onIH0pO1xuXG52YXIgRWRpdCA9IGZ1bmN0aW9uIEVkaXQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkVkaXQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEVkaXQpO1xuRWRpdC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBFZGl0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0VkaXQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0VkaXQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNiAzMSAzNCAzOCA0NCA0NSA0NyA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTcgMTV2MmgxMHYtMkg3em0tNCA2aDE4di0ySDN2MnptMC04aDE4di0ySDN2MnptNC02djJoMTBWN0g3ek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduQ2VudGVyID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25DZW50ZXIocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduQ2VudGVyID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkNlbnRlcik7XG5Gb3JtYXRBbGlnbkNlbnRlci5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkNlbnRlcjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAyMWgxOHYtMkgzdjJ6bTAtNGgxOHYtMkgzdjJ6bTAtNGgxOHYtMkgzdjJ6bTAtNGgxOFY3SDN2MnptMC02djJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkp1c3RpZnkgPSBmdW5jdGlvbiBGb3JtYXRBbGlnbkp1c3RpZnkocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduSnVzdGlmeSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25KdXN0aWZ5KTtcbkZvcm1hdEFsaWduSnVzdGlmeS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkp1c3RpZnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTUgMTVIM3YyaDEydi0yem0wLThIM3YyaDEyVjd6TTMgMTNoMTh2LTJIM3Yyem0wIDhoMTh2LTJIM3Yyek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduTGVmdCA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduTGVmdChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25MZWZ0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkxlZnQpO1xuRm9ybWF0QWxpZ25MZWZ0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduTGVmdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zIDIxaDE4di0ySDN2MnptNi00aDEydi0ySDl2MnptLTYtNGgxOHYtMkgzdjJ6bTYtNGgxMlY3SDl2MnpNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnblJpZ2h0ID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25SaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25SaWdodCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25SaWdodCk7XG5Gb3JtYXRBbGlnblJpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduUmlnaHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNS42IDEwLjc5Yy45Ny0uNjcgMS42NS0xLjc3IDEuNjUtMi43OSAwLTIuMjYtMS43NS00LTQtNEg3djE0aDcuMDRjMi4wOSAwIDMuNzEtMS43IDMuNzEtMy43OSAwLTEuNTItLjg2LTIuODItMi4xNS0zLjQyek0xMCA2LjVoM2MuODMgMCAxLjUuNjcgMS41IDEuNXMtLjY3IDEuNS0xLjUgMS41aC0zdi0zem0zLjUgOUgxMHYtM2gzLjVjLjgzIDAgMS41LjY3IDEuNSAxLjVzLS42NyAxLjUtMS41IDEuNXonIH0pO1xuXG52YXIgRm9ybWF0Qm9sZCA9IGZ1bmN0aW9uIEZvcm1hdEJvbGQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEJvbGQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEJvbGQpO1xuRm9ybWF0Qm9sZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRCb2xkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTAgNHYzaDIuMjFsLTMuNDIgOEg2djNoOHYtM2gtMi4yMWwzLjQyLThIMThWNHonIH0pO1xuXG52YXIgRm9ybWF0SXRhbGljID0gZnVuY3Rpb24gRm9ybWF0SXRhbGljKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRJdGFsaWMgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEl0YWxpYyk7XG5Gb3JtYXRJdGFsaWMubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0SXRhbGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SXRhbGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTQgMTAuNWMtLjgzIDAtMS41LjY3LTEuNSAxLjVzLjY3IDEuNSAxLjUgMS41IDEuNS0uNjcgMS41LTEuNS0uNjctMS41LTEuNS0xLjV6bTAtNmMtLjgzIDAtMS41LjY3LTEuNSAxLjVTMy4xNyA3LjUgNCA3LjUgNS41IDYuODMgNS41IDYgNC44MyA0LjUgNCA0LjV6bTAgMTJjLS44MyAwLTEuNS42OC0xLjUgMS41cy42OCAxLjUgMS41IDEuNSAxLjUtLjY4IDEuNS0xLjUtLjY3LTEuNS0xLjUtMS41ek03IDE5aDE0di0ySDd2MnptMC02aDE0di0ySDd2MnptMC04djJoMTRWNUg3eicgfSk7XG5cbnZhciBGb3JtYXRMaXN0QnVsbGV0ZWQgPSBmdW5jdGlvbiBGb3JtYXRMaXN0QnVsbGV0ZWQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdExpc3RCdWxsZXRlZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0TGlzdEJ1bGxldGVkKTtcbkZvcm1hdExpc3RCdWxsZXRlZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRMaXN0QnVsbGV0ZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMiAxN2gydi41SDN2MWgxdi41SDJ2MWgzdi00SDJ2MXptMS05aDFWNEgydjFoMXYzem0tMSAzaDEuOEwyIDEzLjF2LjloM3YtMUgzLjJMNSAxMC45VjEwSDJ2MXptNS02djJoMTRWNUg3em0wIDE0aDE0di0ySDd2MnptMC02aDE0di0ySDd2MnonIH0pO1xuXG52YXIgRm9ybWF0TGlzdE51bWJlcmVkID0gZnVuY3Rpb24gRm9ybWF0TGlzdE51bWJlcmVkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRMaXN0TnVtYmVyZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdExpc3ROdW1iZXJlZCk7XG5Gb3JtYXRMaXN0TnVtYmVyZWQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0TGlzdE51bWJlcmVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYgMTdoM2wyLTRWN0g1djZoM3ptOCAwaDNsMi00VjdoLTZ2NmgzeicgfSk7XG5cbnZhciBGb3JtYXRRdW90ZSA9IGZ1bmN0aW9uIEZvcm1hdFF1b3RlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRRdW90ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0UXVvdGUpO1xuRm9ybWF0UXVvdGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0UXVvdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0UXVvdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDE3YzMuMzEgMCA2LTIuNjkgNi02VjNoLTIuNXY4YzAgMS45My0xLjU3IDMuNS0zLjUgMy41UzguNSAxMi45MyA4LjUgMTFWM0g2djhjMCAzLjMxIDIuNjkgNiA2IDZ6bS03IDJ2MmgxNHYtMkg1eicgfSk7XG5cbnZhciBGb3JtYXRVbmRlcmxpbmVkID0gZnVuY3Rpb24gRm9ybWF0VW5kZXJsaW5lZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0VW5kZXJsaW5lZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0VW5kZXJsaW5lZCk7XG5Gb3JtYXRVbmRlcmxpbmVkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdFVuZGVybGluZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xOCA0SDZ2Mmw2LjUgNkw2IDE4djJoMTJ2LTNoLTdsNS01LTUtNWg3eicgfSk7XG5cbnZhciBGdW5jdGlvbnMgPSBmdW5jdGlvbiBGdW5jdGlvbnMocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZ1bmN0aW9ucyA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRnVuY3Rpb25zKTtcbkZ1bmN0aW9ucy5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGdW5jdGlvbnM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GdW5jdGlvbnMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNiAxNGwzIDN2NWg2di01bDMtM1Y5SDZ6bTUtMTJoMnYzaC0yek0zLjUgNS44NzVMNC45MTQgNC40NmwyLjEyIDIuMTIyTDUuNjIgNy45OTd6bTEzLjQ2LjcxbDIuMTIzLTIuMTIgMS40MTQgMS40MTRMMTguMzc1IDh6JyB9KTtcblxudmFyIEhpZ2hsaWdodCA9IGZ1bmN0aW9uIEhpZ2hsaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSGlnaGxpZ2h0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShIaWdobGlnaHQpO1xuSGlnaGxpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEhpZ2hsaWdodDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0hpZ2hsaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMCAySDRjLTEuMSAwLTIgLjktMiAydjEyYzAgMS4xLjkgMiAyIDJoMTRsNCA0VjRjMC0xLjEtLjktMi0yLTJ6bS0yIDEySDZ2LTJoMTJ2MnptMC0zSDZWOWgxMnYyem0wLTNINlY2aDEydjJ6JyB9KTtcblxudmFyIEluc2VydENvbW1lbnQgPSBmdW5jdGlvbiBJbnNlcnRDb21tZW50KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRDb21tZW50ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRDb21tZW50KTtcbkluc2VydENvbW1lbnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0Q29tbWVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTExLjk5IDJDNi40NyAyIDIgNi40OCAyIDEyczQuNDcgMTAgOS45OSAxMEMxNy41MiAyMiAyMiAxNy41MiAyMiAxMlMxNy41MiAyIDExLjk5IDJ6TTEyIDIwYy00LjQyIDAtOC0zLjU4LTgtOHMzLjU4LTggOC04IDggMy41OCA4IDgtMy41OCA4LTggOHptMy41LTljLjgzIDAgMS41LS42NyAxLjUtMS41UzE2LjMzIDggMTUuNSA4IDE0IDguNjcgMTQgOS41cy42NyAxLjUgMS41IDEuNXptLTcgMGMuODMgMCAxLjUtLjY3IDEuNS0xLjVTOS4zMyA4IDguNSA4IDcgOC42NyA3IDkuNSA3LjY3IDExIDguNSAxMXptMy41IDYuNWMyLjMzIDAgNC4zMS0xLjQ2IDUuMTEtMy41SDYuODljLjggMi4wNCAyLjc4IDMuNSA1LjExIDMuNXonIH0pO1xuXG52YXIgSW5zZXJ0RW1vdGljb24gPSBmdW5jdGlvbiBJbnNlcnRFbW90aWNvbihwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0RW1vdGljb24gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydEVtb3RpY29uKTtcbkluc2VydEVtb3RpY29uLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydEVtb3RpY29uO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRFbW90aWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zLjkgMTJjMC0xLjcxIDEuMzktMy4xIDMuMS0zLjFoNFY3SDdjLTIuNzYgMC01IDIuMjQtNSA1czIuMjQgNSA1IDVoNHYtMS45SDdjLTEuNzEgMC0zLjEtMS4zOS0zLjEtMy4xek04IDEzaDh2LTJIOHYyem05LTZoLTR2MS45aDRjMS43MSAwIDMuMSAxLjM5IDMuMSAzLjFzLTEuMzkgMy4xLTMuMSAzLjFoLTRWMTdoNGMyLjc2IDAgNS0yLjI0IDUtNXMtMi4yNC01LTUtNXonIH0pO1xuXG52YXIgSW5zZXJ0TGluayA9IGZ1bmN0aW9uIEluc2VydExpbmsocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydExpbmsgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydExpbmspO1xuSW5zZXJ0TGluay5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTcgM0g1Yy0xLjExIDAtMiAuOS0yIDJ2MTRjMCAxLjEuODkgMiAyIDJoMTRjMS4xIDAgMi0uOSAyLTJWN2wtNC00em0tNSAxNmMtMS42NiAwLTMtMS4zNC0zLTNzMS4zNC0zIDMtMyAzIDEuMzQgMyAzLTEuMzQgMy0zIDN6bTMtMTBINVY1aDEwdjR6JyB9KTtcblxudmFyIFNhdmUgPSBmdW5jdGlvbiBTYXZlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5TYXZlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShTYXZlKTtcblNhdmUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gU2F2ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjYgMzEgMzQgMzggNDQgNDUgNDYgNDcgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ001IDR2M2g1LjV2MTJoM1Y3SDE5VjR6JyB9KTtcblxudmFyIFRpdGxlID0gZnVuY3Rpb24gVGl0bGUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblRpdGxlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShUaXRsZSk7XG5UaXRsZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBUaXRsZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuaW1wb3J0IENhcmQsIHsgQ2FyZENvbnRlbnQgfSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcblxuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IEVkaXRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0VkaXQnO1xuaW1wb3J0IFNhdmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NhdmUnO1xuXG5pbXBvcnQgeyBjb252ZXJ0VG9SYXcgfSBmcm9tICdkcmFmdC1qcyc7XG5cbmltcG9ydCBNYXlhc2hFZGl0b3IsIHsgY3JlYXRlRWRpdG9yU3RhdGUgfSBmcm9tICcuLi8uLi8uLi8uLi9saWIvbWF5YXNoLWVkaXRvcic7XG5cbmltcG9ydCBhcGlVc2VyVXBkYXRlIGZyb20gJy4uLy4uLy4uL2FwaS91c2Vycy91cGRhdGUnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBlZGl0QnV0dG9uOiB7XG4gICAgcG9zaXRpb246ICdmaXhlZCcsXG4gICAgekluZGV4OiAxLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHtcbiAgICAgIGJvdHRvbTogJzQwcHgnLFxuICAgICAgcmlnaHQ6ICc0MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge1xuICAgICAgYm90dG9tOiAnNDBweCcsXG4gICAgICByaWdodDogJzQwcHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBib3R0b206ICcyMHB4JyxcbiAgICAgIHJpZ2h0OiAnMjBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIGRpc3BsYXk6ICdub25lJyxcbiAgICAgIGJvdHRvbTogJzEwcHgnLFxuICAgICAgcmlnaHQ6ICcxMHB4JyxcbiAgICB9LFxuICB9LFxufSk7XG5cbmNsYXNzIFRhYlJlc3VtZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICByZXN1bWU6IGNyZWF0ZUVkaXRvclN0YXRlKHByb3BzLmVsZW1lbnQucmVzdW1lKSxcbiAgICB9O1xuXG4gICAgdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2UuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uRWRpdCA9IHRoaXMub25FZGl0LmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblNhdmUgPSB0aGlzLm9uU2F2ZS5iaW5kKHRoaXMpO1xuICB9XG5cbiAgLy8gVGhpcyBvbiBjaGFuZ2UgZnVuY3Rpb24gaXMgZm9yIE1heWFzaEVkaXRvclxuICBvbkNoYW5nZShyZXN1bWUpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgcmVzdW1lIH0pO1xuICB9XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuXG4gIG9uRWRpdCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgZWRpdDogIXRoaXMuc3RhdGUuZWRpdCB9KTtcbiAgfVxuICBhc3luYyBvblNhdmUoKSB7XG4gICAgY29uc3QgeyByZXN1bWUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBpZDogdXNlcklkLCB0b2tlbiB9ID0gdGhpcy5wcm9wcy51c2VyO1xuXG4gICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciB9ID0gYXdhaXQgYXBpVXNlclVwZGF0ZSh7XG4gICAgICBpZDogdXNlcklkLFxuICAgICAgdG9rZW4sXG4gICAgICByZXN1bWU6IGNvbnZlcnRUb1JhdyhyZXN1bWUuZ2V0Q3VycmVudENvbnRlbnQoKSksXG4gICAgfSk7XG5cbiAgICBpZiAoc3RhdHVzQ29kZSAhPT0gMjAwKSB7XG4gICAgICAvLyBlcnJvciBoYW5kbGVcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMucHJvcHMuYWN0aW9uVXNlclVwZGF0ZSh7XG4gICAgICBpZDogdXNlcklkLFxuICAgICAgcmVzdW1lOiBjb252ZXJ0VG9SYXcocmVzdW1lLmdldEN1cnJlbnRDb250ZW50KCkpLFxuICAgIH0pO1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXQ6IGZhbHNlIH0pO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgZWxlbWVudCwgdXNlciB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IHsgaWQ6IGVsZW1lbnRJZCwgaXNTaWduZWRJbiB9ID0gZWxlbWVudDtcbiAgICBjb25zdCB7IGlkOiB1c2VySWQgfSA9IHVzZXI7XG5cbiAgICBjb25zdCB7IGhvdmVyLCBlZGl0LCByZXN1bWUgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEdyaWQgY29udGFpbmVyIHNwYWNpbmc9ezB9IGp1c3RpZnk9XCJjZW50ZXJcIiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxHcmlkXG4gICAgICAgICAgaXRlbVxuICAgICAgICAgIHhzPXsxMn1cbiAgICAgICAgICBzbT17MTB9XG4gICAgICAgICAgbWQ9ezEwfVxuICAgICAgICAgIGxnPXs5fVxuICAgICAgICAgIHhsPXs5fVxuICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX1cbiAgICAgICAgPlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5vbk1vdXNlRW50ZXJ9XG4gICAgICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgICAgID5cbiAgICAgICAgICAgIDxDYXJkIHJhaXNlZD17aG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICAgIDxNYXlhc2hFZGl0b3JcbiAgICAgICAgICAgICAgICAgIGVkaXRvclN0YXRlPXtyZXN1bWV9XG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgICAgICAgIHJlYWRPbmx5PXshZWRpdH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgPC9DYXJkPlxuICAgICAgICAgICAge2lzU2lnbmVkSW4gJiYgdXNlcklkID09PSBlbGVtZW50SWQgPyAoXG4gICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICBmYWJcbiAgICAgICAgICAgICAgICBjb2xvcj1cImFjY2VudFwiXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkVkaXRcIlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5lZGl0QnV0dG9ufVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e2VkaXQgPyB0aGlzLm9uU2F2ZSA6IHRoaXMub25FZGl0fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAge2VkaXQgPyA8U2F2ZUljb24gLz4gOiA8RWRpdEljb24gLz59XG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cblRhYlJlc3VtZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICB1c2VyOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGVsZW1lbnQ6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBhY3Rpb25Vc2VyVXBkYXRlOiBQcm9wVHlwZXMuZnVuYyxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShUYWJSZXN1bWUpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0VsZW1lbnRQYWdlL1RhYnMvVGFiUmVzdW1lLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncmVhY3QvbGliL1JlYWN0UHJvcFR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbi8vIGltcG9ydCBJY29uIGZyb20gJ21hdGVyaWFsLXVpL0ljb24nO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcblxuaW1wb3J0IFRpdGxlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9UaXRsZSc7XG5pbXBvcnQgRm9ybWF0Qm9sZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Qm9sZCc7XG5pbXBvcnQgRm9ybWF0SXRhbGljIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYyc7XG5pbXBvcnQgRm9ybWF0VW5kZXJsaW5lZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZCc7XG5pbXBvcnQgRm9ybWF0UXVvdGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlJztcbi8vIGltcG9ydCBGb3JtYXRDbGVhckljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q2xlYXInO1xuLy8gaW1wb3J0IEZvcm1hdENvbG9yRmlsbEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q29sb3JGaWxsJztcbi8vIGltcG9ydCBGb3JtYXRDb2xvclJlc2V0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRDb2xvclJlc2V0Jztcbi8vIGltcG9ydCBGb3JtYXRDb2xvclRleHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENvbG9yVGV4dCc7XG4vLyBpbXBvcnQgRm9ybWF0SW5kZW50RGVjcmVhc2VJY29uXG4vLyAgIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEluZGVudERlY3JlYXNlJztcbi8vIGltcG9ydCBGb3JtYXRJbmRlbnRJbmNyZWFzZUljb25cbi8vICAgZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SW5kZW50SW5jcmVhc2UnO1xuXG5pbXBvcnQgQ29kZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQ29kZSc7XG5cbmltcG9ydCBGb3JtYXRMaXN0TnVtYmVyZWRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZCc7XG5pbXBvcnQgRm9ybWF0TGlzdEJ1bGxldGVkSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQnO1xuXG5pbXBvcnQgRm9ybWF0QWxpZ25DZW50ZXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyJztcbmltcG9ydCBGb3JtYXRBbGlnbkxlZnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdCc7XG5pbXBvcnQgRm9ybWF0QWxpZ25SaWdodEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodCc7XG5pbXBvcnQgRm9ybWF0QWxpZ25KdXN0aWZ5SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnknO1xuXG5pbXBvcnQgQXR0YWNoRmlsZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZSc7XG5pbXBvcnQgSW5zZXJ0TGlua0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0TGluayc7XG5pbXBvcnQgSW5zZXJ0UGhvdG9JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvJztcbmltcG9ydCBJbnNlcnRFbW90aWNvbkljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24nO1xuaW1wb3J0IEluc2VydENvbW1lbnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQnO1xuXG4vLyBpbXBvcnQgVmVydGljYWxBbGlnblRvcEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnblRvcCc7XG4vLyBpbXBvcnQgVmVydGljYWxBbGlnbkJvdHRvbUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnbkJvdHRvbSc7XG4vLyBpbXBvcnQgVmVydGljYWxBbGlnbkNlbnRlckljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnbkNlbnRlcic7XG5cbi8vIGltcG9ydCBXcmFwVGV4dEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvV3JhcFRleHQnO1xuXG5pbXBvcnQgSGlnaGxpZ2h0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQnO1xuaW1wb3J0IEZ1bmN0aW9uc0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zJztcblxuaW1wb3J0IHtcbiAgRWRpdG9yLFxuICBSaWNoVXRpbHMsXG4gIC8vIEVudGl0eSxcbiAgLy8gQ29tcG9zaXRlRGVjb3JhdG9yLFxuICBBdG9taWNCbG9ja1V0aWxzLFxuICBFZGl0b3JTdGF0ZSxcbiAgLy8gICBjb252ZXJ0VG9SYXcsXG59IGZyb20gJ2RyYWZ0LWpzJztcblxuLy8gaW1wb3J0IHtcbi8vICAgaGFzaHRhZ1N0cmF0ZWd5LFxuLy8gICBIYXNodGFnU3BhbixcblxuLy8gICBoYW5kbGVTdHJhdGVneSxcbi8vICAgSGFuZGxlU3Bhbixcbi8vIH0gZnJvbSAnLi9jb21wb25lbnRzL0RlY29yYXRvcnMnO1xuXG5pbXBvcnQgQXRvbWljIGZyb20gJy4vY29tcG9uZW50cy9BdG9taWMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL0VkaXRvclN0eWxlcyc7XG5cbmltcG9ydCB7IEJsb2NrcywgSEFORExFRCwgTk9UX0hBTkRMRUQgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5cbi8qKlxuICogTWF5YXNoRWRpdG9yXG4gKi9cbmNsYXNzIE1heWFzaEVkaXRvciBleHRlbmRzIENvbXBvbmVudCB7XG4gIC8qKlxuICAgKiBNYXlhc2ggRWRpdG9yJ3MgcHJvcHR5cGVzIGFyZSBkZWZpbmVkIGhlcmUuXG4gICAqL1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAgIGVkaXRvclN0YXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG5cbiAgICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcblxuICAgIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuXG4gICAgLyoqXG4gICAgICogVGhpcyBhcGkgZnVuY3Rpb24gc2hvdWxkIGJlIGFwcGxpZWQgZm9yIGVuYWJsaW5nIHBob3RvIGFkZGluZ1xuICAgICAqIGZlYXR1cmUgaW4gTWF5YXNoIEVkaXRvci5cbiAgICAgKi9cbiAgICBhcGlQaG90b1VwbG9hZDogUHJvcFR5cGVzLmZ1bmMsXG4gIH07XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICByZWFkT25seTogdHJ1ZSxcbiAgICBwbGFjZWhvbGRlcjogJ1dyaXRlIEhlcmUuLi4nLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgLy8gdGhpcy5zdGF0ZSA9IHt9O1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGJlIHN1cHBsaWVkIHRvIERyYWZ0LmpzIEVkaXRvciB0byBoYW5kbGVcbiAgICAgKiBvbkNoYW5nZSBldmVudC5cbiAgICAgKiBAZnVuY3Rpb24gb25DaGFuZ2VcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZWRpdG9yU3RhdGVcbiAgICAgKi9cbiAgICB0aGlzLm9uQ2hhbmdlID0gKGVkaXRvclN0YXRlKSA9PiB7XG4gICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlKGVkaXRvclN0YXRlKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBmb2N1cyBldmVudCBpbiBEcmFmdC5qcyBFZGl0b3IuXG4gICAgICogQGZ1bmN0aW9uIGZvY3VzXG4gICAgICovXG4gICAgdGhpcy5mb2N1cyA9ICgpID0+IHRoaXMuZWRpdG9yTm9kZS5mb2N1cygpO1xuXG4gICAgdGhpcy5vblRhYiA9IHRoaXMub25UYWIuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uVGl0bGVDbGljayA9IHRoaXMub25UaXRsZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNvZGVDbGljayA9IHRoaXMub25Db2RlQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uUXVvdGVDbGljayA9IHRoaXMub25RdW90ZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrID0gdGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrID0gdGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkJvbGRDbGljayA9IHRoaXMub25Cb2xkQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uSXRhbGljQ2xpY2sgPSB0aGlzLm9uSXRhbGljQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uVW5kZXJMaW5lQ2xpY2sgPSB0aGlzLm9uVW5kZXJMaW5lQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uSGlnaGxpZ2h0Q2xpY2sgPSB0aGlzLm9uSGlnaGxpZ2h0Q2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUtleUNvbW1hbmQgPSB0aGlzLmhhbmRsZUtleUNvbW1hbmQuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uQ2xpY2tJbnNlcnRQaG90byA9IHRoaXMub25DbGlja0luc2VydFBob3RvLmJpbmQodGhpcyk7XG5cbiAgICAvLyB0aGlzLmJsb2NrUmVuZGVyZXJGbiA9IHRoaXMuYmxvY2tSZW5kZXJlckZuLmJpbmQodGhpcyk7XG5cbiAgICAvLyBjb25zdCBjb21wb3NpdGVEZWNvcmF0b3IgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKFtcbiAgICAvLyAgIHtcbiAgICAvLyAgICAgc3RyYXRlZ3k6IGhhbmRsZVN0cmF0ZWd5LFxuICAgIC8vICAgICBjb21wb25lbnQ6IEhhbmRsZVNwYW4sXG4gICAgLy8gICB9LFxuICAgIC8vICAge1xuICAgIC8vICAgICBzdHJhdGVneTogaGFzaHRhZ1N0cmF0ZWd5LFxuICAgIC8vICAgICBjb21wb25lbnQ6IEhhc2h0YWdTcGFuLFxuICAgIC8vICAgfSxcbiAgICAvLyBdKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBvblRhYigpIHdpbGwgaGFuZGxlIFRhYiBidXR0b24gcHJlc3MgZXZlbnQgaW4gRWRpdG9yLlxuICAgKiBAZnVuY3Rpb24gb25UYWJcbiAgICogQHBhcmFtIHtFdmVudH0gZVxuICAgKi9cbiAgb25UYWIoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMub25UYWIoZSwgZWRpdG9yU3RhdGUsIDQpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBUaXRsZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvblRpdGxlQ2xpY2tcbiAgICovXG4gIG9uVGl0bGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnaGVhZGVyLW9uZScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBDb2RlIEJ1dHRvbi5cbiAgICogQGZ1bmN0aW9uIG9uQ29kZUNsaWNrXG4gICAqL1xuICBvbkNvZGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnY29kZS1ibG9jaycpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBRdW90ZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvblF1b3RlQ2xpY2tcbiAgICovXG4gIG9uUXVvdGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnYmxvY2txdW90ZScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIHVwZGF0ZSBibG9jayB3aXRoIHVub3JkZXJlZCBsaXN0IGl0ZW1zXG4gICAqIEBmdW5jdGlvbiBvbkxpc3RCdWxsZXRlZENsaWNrXG4gICAqL1xuICBvbkxpc3RCdWxsZXRlZENsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVCbG9ja1R5cGUoXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICApO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIHVwZGF0ZSBibG9jayB3aXRoIG9yZGVyZWQgbGlzdCBpdGVtLlxuICAgKiBAZnVuY3Rpb24gb25MaXN0TnVtYmVyZWRDbGlja1xuICAgKi9cbiAgb25MaXN0TnVtYmVyZWRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICAnb3JkZXJlZC1saXN0LWl0ZW0nLFxuICAgICk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBzZWxlY3RlZCBjb250ZW50IEJvbGQgc3R5bGluZy5cbiAgICogQGZ1bmN0aW9uIG9uQm9sZENsaWNrXG4gICAqL1xuICBvbkJvbGRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdCT0xEJyk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBpdGFsaWMgc3R5bGluZyB0byBzZWxlY3RlZCBjb250ZW50LlxuICAgKiBAZnVuY3Rpb24gb25JdGFsaWNDbGlja1xuICAgKi9cbiAgb25JdGFsaWNDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdJVEFMSUMnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBnaXZlIHVuZGVybGluZSBzdHlsaW5nIHRvIHNlbGVjdGVkIGNvbnRlbnQuXG4gICAqIEBmdW5jdGlvbiBvblVuZGVyTGluZUNsaWNrXG4gICAqL1xuICBvblVuZGVyTGluZUNsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVJbmxpbmVTdHlsZShcbiAgICAgIGVkaXRvclN0YXRlLFxuICAgICAgJ1VOREVSTElORScsXG4gICAgKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoaWdobGlnaHQgc2VsZWN0ZWQgY29udGVudC5cbiAgICogQGZ1bmN0aW9uIG9uSGlnaGxpZ2h0Q2xpY2tcbiAgICovXG4gIG9uSGlnaGxpZ2h0Q2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnQ09ERScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICpcbiAgICovXG4gIG9uQ2xpY2tJbnNlcnRQaG90bygpIHtcbiAgICB0aGlzLnBob3RvLnZhbHVlID0gbnVsbDtcbiAgICB0aGlzLnBob3RvLmNsaWNrKCk7XG4gIH1cblxuICAvKipcbiAgICpcbiAgICovXG4gIG9uQ2hhbmdlSW5zZXJ0UGhvdG8gPSBhc3luYyAoZSkgPT4ge1xuICAgIC8vIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCBmaWxlID0gZS50YXJnZXQuZmlsZXNbMF07XG5cbiAgICBpZiAoZmlsZS50eXBlLmluZGV4T2YoJ2ltYWdlLycpID09PSAwKSB7XG4gICAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgZm9ybURhdGEuYXBwZW5kKCdwaG90bycsIGZpbGUpO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCB0aGlzLnByb3BzLmFwaVBob3RvVXBsb2FkKHtcbiAgICAgICAgZm9ybURhdGEsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIC8vIGhhbmRsZSBFcnJvclxuICAgICAgICBjb25zb2xlLmVycm9yKHN0YXR1c0NvZGUsIGVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCB7IHBob3RvVXJsOiBzcmMgfSA9IHBheWxvYWQ7XG5cbiAgICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICAgIGNvbnN0IGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgICBjb25zdCBjb250ZW50U3RhdGVXaXRoRW50aXR5ID0gY29udGVudFN0YXRlLmNyZWF0ZUVudGl0eShcbiAgICAgICAgQmxvY2tzLlBIT1RPLFxuICAgICAgICAnSU1NVVRBQkxFJyxcbiAgICAgICAgeyBzcmMgfSxcbiAgICAgICk7XG5cbiAgICAgIGNvbnN0IGVudGl0eUtleSA9IGNvbnRlbnRTdGF0ZVdpdGhFbnRpdHkuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKTtcblxuICAgICAgY29uc3QgbWlkZGxlRWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5zZXQoZWRpdG9yU3RhdGUsIHtcbiAgICAgICAgY3VycmVudENvbnRlbnQ6IGNvbnRlbnRTdGF0ZVdpdGhFbnRpdHksXG4gICAgICB9KTtcblxuICAgICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBBdG9taWNCbG9ja1V0aWxzLmluc2VydEF0b21pY0Jsb2NrKFxuICAgICAgICBtaWRkbGVFZGl0b3JTdGF0ZSxcbiAgICAgICAgZW50aXR5S2V5LFxuICAgICAgICAnICcsXG4gICAgICApO1xuXG4gICAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBnaXZlIGN1c3RvbSBoYW5kbGUgY29tbWFuZHMgdG8gRWRpdG9yLlxuICAgKiBAZnVuY3Rpb24gaGFuZGxlS2V5Q29tbWFuZFxuICAgKiBAcGFyYW0ge3N0cmluZ30gY29tbWFuZFxuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqL1xuICBoYW5kbGVLZXlDb21tYW5kKGNvbW1hbmQpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMuaGFuZGxlS2V5Q29tbWFuZChlZGl0b3JTdGF0ZSwgY29tbWFuZCk7XG5cbiAgICBpZiAobmV3RWRpdG9yU3RhdGUpIHtcbiAgICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICAgICAgcmV0dXJuIEhBTkRMRUQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIE5PVF9IQU5ETEVEO1xuICB9XG5cbiAgLyogZXNsaW50LWRpc2FibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuICBibG9ja1JlbmRlcmVyRm4oYmxvY2spIHtcbiAgICBzd2l0Y2ggKGJsb2NrLmdldFR5cGUoKSkge1xuICAgICAgY2FzZSBCbG9ja3MuQVRPTUlDOlxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNvbXBvbmVudDogQXRvbWljLFxuICAgICAgICAgIGVkaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgfTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG4gIC8qIGVzbGludC1lbmFibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuXG4gIC8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbiAgYmxvY2tTdHlsZUZuKGJsb2NrKSB7XG4gICAgc3dpdGNoIChibG9jay5nZXRUeXBlKCkpIHtcbiAgICAgIGNhc2UgJ2Jsb2NrcXVvdGUnOlxuICAgICAgICByZXR1cm4gJ1JpY2hFZGl0b3ItYmxvY2txdW90ZSc7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3Qge1xuICAgICAgY2xhc3NlcyxcbiAgICAgIHJlYWRPbmx5LFxuICAgICAgb25DaGFuZ2UsXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgIHBsYWNlaG9sZGVyLFxuICAgIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgLy8gQ3VzdG9tIG92ZXJyaWRlcyBmb3IgXCJjb2RlXCIgc3R5bGUuXG4gICAgY29uc3Qgc3R5bGVNYXAgPSB7XG4gICAgICBDT0RFOiB7XG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwgMCwgMCwgMC4wNSknLFxuICAgICAgICBmb250RmFtaWx5OiAnXCJJbmNvbnNvbGF0YVwiLCBcIk1lbmxvXCIsIFwiQ29uc29sYXNcIiwgbW9ub3NwYWNlJyxcbiAgICAgICAgZm9udFNpemU6IDE2LFxuICAgICAgICBwYWRkaW5nOiAyLFxuICAgICAgfSxcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7IXJlYWRPbmx5ID8gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHsgJ2VkaXRvci1jb250cm9scyc6ICcnIH0pfT5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiVGl0bGVcIiBpZD1cInRpdGxlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJUaXRsZVwiIG9uQ2xpY2s9e3RoaXMub25UaXRsZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8VGl0bGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQm9sZFwiIGlkPVwiYm9sZFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQm9sZFwiIG9uQ2xpY2s9e3RoaXMub25Cb2xkQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRCb2xkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkl0YWxpY1wiIGlkPVwiaXRhbGljXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJdGFsaWNcIiBvbkNsaWNrPXt0aGlzLm9uSXRhbGljQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRJdGFsaWMgLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJVbmRlcmxpbmVcIiBpZD1cInVuZGVybGluZVwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVbmRlcmxpbmVcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25VbmRlckxpbmVDbGlja31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRVbmRlcmxpbmVkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvZGVcIiBpZD1cImNvZGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkNvZGVcIiBvbkNsaWNrPXt0aGlzLm9uQ29kZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8Q29kZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJRdW90ZVwiIGlkPVwicXVvdGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlF1b3RlXCIgb25DbGljaz17dGhpcy5vblF1b3RlQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRRdW90ZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJVbm9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgIGlkPVwidW5vcmRlcmQtbGlzdFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIlVub3JkZXJlZCBMaXN0XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uTGlzdEJ1bGxldGVkQ2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0TGlzdEJ1bGxldGVkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIk9yZGVyZWQgTGlzdFwiIGlkPVwib3JkZXJlZC1saXN0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIk9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZvcm1hdExpc3ROdW1iZXJlZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBMZWZ0XCIgaWQ9XCJhbGlnbi1sZWZ0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBbGlnbiBMZWZ0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduTGVmdEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBDZW50ZXJcIiBpZD1cImFsaWduLWNlbnRlclwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gQ2VudGVyXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduQ2VudGVySWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkFsaWduIFJpZ2h0XCIgaWQ9XCJhbGlnbi1yaWdodFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gUmlnaHRcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0QWxpZ25SaWdodEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBSaWdodFwiIGlkPVwiYWxpZ24tcmlnaHRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIFJpZ2h0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduSnVzdGlmeUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBdHRhY2ggRmlsZVwiIGlkPVwiYXR0YWNoLWZpbGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkF0dGFjaCBGaWxlXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEF0dGFjaEZpbGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiSW5zZXJ0IExpbmtcIiBpZD1cImluc2VydC1saW5rXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgTGlua1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRMaW5rSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkluc2VydCBQaG90b1wiIGlkPVwiaW5zZXJ0LXBob3RvXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkluc2VydCBQaG90b1wiXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3R5cGVvZiB0aGlzLnByb3BzLmFwaVBob3RvVXBsb2FkICE9PSAnZnVuY3Rpb24nfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25DbGlja0luc2VydFBob3RvfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEluc2VydFBob3RvSWNvbiAvPlxuICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgdHlwZT1cImZpbGVcIlxuICAgICAgICAgICAgICAgICAgYWNjZXB0PVwiaW1hZ2UvanBlZ3xwbmd8Z2lmXCJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlSW5zZXJ0UGhvdG99XG4gICAgICAgICAgICAgICAgICByZWY9eyhwaG90bykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnBob3RvID0gcGhvdG87XG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgZGlzcGxheTogJ25vbmUnIH19XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJJbnNlcnQgRW1vdGljb25cIlxuICAgICAgICAgICAgICBpZD1cImluc2VydEUtZW1vdGljb25cIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IEVtb3RpY29uXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEluc2VydEVtb3RpY29uSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkluc2VydCBDb21tZW50XCJcbiAgICAgICAgICAgICAgaWQ9XCJpbnNlcnQtY29tbWVudFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgQ29tbWVudFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRDb21tZW50SWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkhpZ2hsaWdodCBUZXh0XCJcbiAgICAgICAgICAgICAgaWQ9XCJoaWdobGlnaHQtdGV4dFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkhpZ2hsaWdodCBUZXh0XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uSGlnaGxpZ2h0Q2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8SGlnaGxpZ2h0SWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkFkZCBGdW5jdGlvbnNcIlxuICAgICAgICAgICAgICBpZD1cImFkZC1mdW5jdGlvbnNcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWRkIEZ1bmN0aW9uc1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGdW5jdGlvbnNJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHsgJ2VkaXRvci1hcmVhJzogJycgfSl9PlxuICAgICAgICAgIDxFZGl0b3JcbiAgICAgICAgICAgIC8qIEJhc2ljcyAqL1xuXG4gICAgICAgICAgICBlZGl0b3JTdGF0ZT17ZWRpdG9yU3RhdGV9XG4gICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICAgICAgICAvKiBQcmVzZW50YXRpb24gKi9cblxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgLy8gdGV4dEFsaWdubWVudD1cImNlbnRlclwiXG5cbiAgICAgICAgICAgIC8vIHRleHREaXJlY3Rpb25hbGl0eT1cIkxUUlwiXG5cbiAgICAgICAgICAgIGJsb2NrUmVuZGVyZXJGbj17dGhpcy5ibG9ja1JlbmRlcmVyRm59XG4gICAgICAgICAgICBibG9ja1N0eWxlRm49e3RoaXMuYmxvY2tTdHlsZUZufVxuICAgICAgICAgICAgY3VzdG9tU3R5bGVNYXA9e3N0eWxlTWFwfVxuICAgICAgICAgICAgLy8gY3VzdG9tU3R5bGVGbj17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8qIEJlaGF2aW9yICovXG5cbiAgICAgICAgICAgIC8vIGF1dG9DYXBpdGFsaXplPVwic2VudGVuY2VzXCJcblxuICAgICAgICAgICAgLy8gYXV0b0NvbXBsZXRlPVwib2ZmXCJcblxuICAgICAgICAgICAgLy8gYXV0b0NvcnJlY3Q9XCJvZmZcIlxuXG4gICAgICAgICAgICByZWFkT25seT17cmVhZE9ubHl9XG4gICAgICAgICAgICBzcGVsbENoZWNrXG4gICAgICAgICAgICAvLyBzdHJpcFBhc3RlZFN0eWxlcz17ZmFsc2V9XG5cbiAgICAgICAgICAgIC8qIERPTSBhbmQgQWNjZXNzaWJpbGl0eSAqL1xuXG4gICAgICAgICAgICAvLyBlZGl0b3JLZXlcblxuICAgICAgICAgICAgLyogQ2FuY2VsYWJsZSBIYW5kbGVycyAqL1xuXG4gICAgICAgICAgICAvLyBoYW5kbGVSZXR1cm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICBoYW5kbGVLZXlDb21tYW5kPXt0aGlzLmhhbmRsZUtleUNvbW1hbmR9XG4gICAgICAgICAgICAvLyBoYW5kbGVCZWZvcmVJbnB1dD17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZVBhc3RlZFRleHQ9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVQYXN0ZWRGaWxlcz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZURyb3BwZWRGaWxlcz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZURyb3A9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBLZXkgSGFuZGxlcnMgKi9cblxuICAgICAgICAgICAgLy8gb25Fc2NhcGU9eygpID0+IHt9fVxuXG4gICAgICAgICAgICBvblRhYj17dGhpcy5vblRhYn1cbiAgICAgICAgICAgIC8vIG9uVXBBcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uUmlnaHRBcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uRG93bkFycm93PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gb25MZWZ0QXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBrZXlCaW5kaW5nRm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBNb3VzZSBFdmVudCAqL1xuXG4gICAgICAgICAgICAvLyBvbkZvY3VzPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gb25CbHVyPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogTWV0aG9kcyAqL1xuXG4gICAgICAgICAgICAvLyBmb2N1cz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGJsdXI9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBGb3IgUmVmZXJlbmNlICovXG5cbiAgICAgICAgICAgIHJlZj17KG5vZGUpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5lZGl0b3JOb2RlID0gbm9kZTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShNYXlhc2hFZGl0b3IpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvci5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCB7IEVkaXRvclN0YXRlLCBjb252ZXJ0RnJvbVJhdywgQ29tcG9zaXRlRGVjb3JhdG9yIH0gZnJvbSAnZHJhZnQtanMnO1xuXG5pbXBvcnQge1xuICBoYXNodGFnU3RyYXRlZ3ksXG4gIEhhc2h0YWdTcGFuLFxuICBoYW5kbGVTdHJhdGVneSxcbiAgSGFuZGxlU3Bhbixcbn0gZnJvbSAnLi9jb21wb25lbnRzL0RlY29yYXRvcnMnO1xuXG5jb25zdCBkZWZhdWx0RGVjb3JhdG9ycyA9IG5ldyBDb21wb3NpdGVEZWNvcmF0b3IoW1xuICB7XG4gICAgc3RyYXRlZ3k6IGhhbmRsZVN0cmF0ZWd5LFxuICAgIGNvbXBvbmVudDogSGFuZGxlU3BhbixcbiAgfSxcbiAge1xuICAgIHN0cmF0ZWd5OiBoYXNodGFnU3RyYXRlZ3ksXG4gICAgY29tcG9uZW50OiBIYXNodGFnU3BhbixcbiAgfSxcbl0pO1xuXG5leHBvcnQgY29uc3QgY3JlYXRlRWRpdG9yU3RhdGUgPSAoXG4gIGNvbnRlbnQgPSBudWxsLFxuICBkZWNvcmF0b3JzID0gZGVmYXVsdERlY29yYXRvcnMsXG4pID0+IHtcbiAgaWYgKGNvbnRlbnQgPT09IG51bGwpIHtcbiAgICByZXR1cm4gRWRpdG9yU3RhdGUuY3JlYXRlRW1wdHkoZGVjb3JhdG9ycyk7XG4gIH1cbiAgcmV0dXJuIEVkaXRvclN0YXRlLmNyZWF0ZVdpdGhDb250ZW50KGNvbnZlcnRGcm9tUmF3KGNvbnRlbnQpLCBkZWNvcmF0b3JzKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUVkaXRvclN0YXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvclN0YXRlLmpzIiwiLyoqXG4gKiBUaGlzIGZpbGUgY29udGFpbnMgYWxsIHRoZSBDU1MtaW4tSlMgc3R5bGVzIG9mIEVkaXRvciBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgJ0BnbG9iYWwnOiB7XG4gICAgJy5SaWNoRWRpdG9yLXJvb3QnOiB7XG4gICAgICBiYWNrZ3JvdW5kOiAnI2ZmZicsXG4gICAgICBib3JkZXI6ICcxcHggc29saWQgI2RkZCcsXG4gICAgICBmb250RmFtaWx5OiBcIidHZW9yZ2lhJywgc2VyaWZcIixcbiAgICAgIGZvbnRTaXplOiAnMTRweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgfSxcbiAgICAnLlJpY2hFZGl0b3ItZWRpdG9yJzoge1xuICAgICAgYm9yZGVyVG9wOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgICAgY3Vyc29yOiAndGV4dCcsXG4gICAgICBmb250U2l6ZTogJzE2cHgnLFxuICAgICAgbWFyZ2luVG9wOiAnMTBweCcsXG4gICAgfSxcbiAgICAnLnB1YmxpYy1EcmFmdEVkaXRvclBsYWNlaG9sZGVyLXJvb3QnOiB7XG4gICAgICBtYXJnaW46ICcwIC0xNXB4IC0xNXB4JyxcbiAgICAgIHBhZGRpbmc6ICcxNXB4JyxcbiAgICB9LFxuICAgICcucHVibGljLURyYWZ0RWRpdG9yLWNvbnRlbnQnOiB7XG4gICAgICBtYXJnaW46ICcwIC0xNXB4IC0xNXB4JyxcbiAgICAgIHBhZGRpbmc6ICcxNXB4JyxcbiAgICAgIC8vIG1pbkhlaWdodDogJzEwMHB4JyxcbiAgICB9LFxuICAgICcuUmljaEVkaXRvci1ibG9ja3F1b3RlJzoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiAnNXB4IHNvbGlkICNlZWUnLFxuICAgICAgYm9yZGVyTGVmdDogJzVweCBzb2xpZCAjZWVlJyxcbiAgICAgIGNvbG9yOiAnIzY2NicsXG4gICAgICBmb250RmFtaWx5OiBcIidIb2VmbGVyIFRleHQnLCAnR2VvcmdpYScsIHNlcmlmXCIsXG4gICAgICBmb250U3R5bGU6ICdpdGFsaWMnLFxuICAgICAgbWFyZ2luOiAnMTZweCAwJyxcbiAgICAgIHBhZGRpbmc6ICcxMHB4IDIwcHgnLFxuICAgIH0sXG4gICAgJy5wdWJsaWMtRHJhZnRTdHlsZURlZmF1bHQtcHJlJzoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSgwLCAwLCAwLCAwLjA1KScsXG4gICAgICBmb250RmFtaWx5OiBcIidJbmNvbnNvbGF0YScsICdNZW5sbycsICdDb25zb2xhcycsIG1vbm9zcGFjZVwiLFxuICAgICAgZm9udFNpemU6ICcxNnB4JyxcbiAgICAgIHBhZGRpbmc6ICcyMHB4JyxcbiAgICB9LFxuICB9LFxuICByb290OiB7XG4gICAgLy8gcGFkZGluZzogJzElJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdHlsZXMuanMiLCIvKipcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IEJsb2NrcyB9IGZyb20gJy4uL2NvbnN0YW50cyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcGhvdG86IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIC8vIEZpeCBhbiBpc3N1ZSB3aXRoIEZpcmVmb3ggcmVuZGVyaW5nIHZpZGVvIGNvbnRyb2xzXG4gICAgLy8gd2l0aCAncHJlLXdyYXAnIHdoaXRlLXNwYWNlXG4gICAgd2hpdGVTcGFjZTogJ2luaXRpYWwnLFxuICB9LFxufTtcblxuLyoqXG4gKlxuICogQGNsYXNzIEF0b21pYyAtIHRoaXMgUmVhY3QgY29tcG9uZW50IHdpbGwgYmUgdXNlZCB0byByZW5kZXIgQXRvbWljXG4gKiBjb21wb25lbnRzIG9mIERyYWZ0LmpzXG4gKlxuICogQHRvZG8gLSBjb25maWd1cmUgdGhpcyBmb3IgYXVkaW8uXG4gKiBAdG9kbyAtIGNvbmZpZ3VyZSB0aGlzIGZvciB2aWRlby5cbiAqL1xuY2xhc3MgQXRvbWljIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjb250ZW50U3RhdGU6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBibG9jazogUHJvcFR5cGVzLmFueS5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNvbnRlbnRTdGF0ZSwgYmxvY2sgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBlbnRpdHkgPSBjb250ZW50U3RhdGUuZ2V0RW50aXR5KGJsb2NrLmdldEVudGl0eUF0KDApKTtcbiAgICBjb25zdCB7IHNyYyB9ID0gZW50aXR5LmdldERhdGEoKTtcbiAgICBjb25zdCB0eXBlID0gZW50aXR5LmdldFR5cGUoKTtcblxuICAgIGlmICh0eXBlID09PSBCbG9ja3MuUEhPVE8pIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGltZyBhbHQ9eydhbHQnfSBzcmM9e3NyY30gc3R5bGU9e3N0eWxlcy5waG90b30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiA8ZGl2IC8+O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF0b21pYztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0F0b21pYy5qcyIsIi8qIGVzbGludC1kaXNhYmxlICovXG4vLyBlc2xpbnQgaXMgZGlzYWJsZWQgaGVyZSBmb3Igbm93LlxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBzdHlsZSBmcm9tICcuLi9zdHlsZSc7XG5cbmNvbnN0IEhBTkRMRV9SRUdFWCA9IC9cXEBbXFx3XSsvZztcbmNvbnN0IEhBU0hUQUdfUkVHRVggPSAvXFwjW1xcd1xcdTA1OTAtXFx1MDVmZl0rL2c7XG5cbmZ1bmN0aW9uIGZpbmRXaXRoUmVnZXgocmVnZXgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spIHtcbiAgY29uc3QgdGV4dCA9IGNvbnRlbnRCbG9jay5nZXRUZXh0KCk7XG4gIGxldCBtYXRjaEFycixcbiAgICBzdGFydDtcbiAgd2hpbGUgKChtYXRjaEFyciA9IHJlZ2V4LmV4ZWModGV4dCkpICE9PSBudWxsKSB7XG4gICAgc3RhcnQgPSBtYXRjaEFyci5pbmRleDtcbiAgICBjYWxsYmFjayhzdGFydCwgc3RhcnQgKyBtYXRjaEFyclswXS5sZW5ndGgpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBoYW5kbGVTdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrLCBjb250ZW50U3RhdGUpIHtcbiAgZmluZFdpdGhSZWdleChIQU5ETEVfUkVHRVgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFzaHRhZ1N0cmF0ZWd5KGNvbnRlbnRCbG9jaywgY2FsbGJhY2ssIGNvbnRlbnRTdGF0ZSkge1xuICBmaW5kV2l0aFJlZ2V4KEhBU0hUQUdfUkVHRVgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spO1xufVxuXG5leHBvcnQgY29uc3QgSGFuZGxlU3BhbiA9IHByb3BzID0+IDxzcGFuIHN0eWxlPXtzdHlsZS5oYW5kbGV9Pntwcm9wcy5jaGlsZHJlbn08L3NwYW4+O1xuXG5leHBvcnQgY29uc3QgSGFzaHRhZ1NwYW4gPSBwcm9wcyA9PiA8c3BhbiBzdHlsZT17c3R5bGUuaGFzaHRhZ30+e3Byb3BzLmNoaWxkcmVufTwvc3Bhbj47XG5cblxuZXhwb3J0IGRlZmF1bHQge1xuICBoYW5kbGVTdHJhdGVneSxcbiAgSGFuZGxlU3BhbixcblxuICBoYXNodGFnU3RyYXRlZ3ksXG4gIEhhc2h0YWdTcGFuLFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0RlY29yYXRvcnMuanMiLCIvKipcbiAqIFNvbWUgb2YgdGhlIGNvbnN0YW50cyB3aGljaCBhcmUgdXNlZCB0aHJvdWdob3V0IHRoaXMgcHJvamVjdCBpbnN0ZWFkIG9mXG4gKiBkaXJlY3RseSB1c2luZyBzdHJpbmcuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbi8qKlxuICogQGNvbnN0YW50IEJsb2Nrc1xuICovXG5leHBvcnQgY29uc3QgQmxvY2tzID0ge1xuICBVTlNUWUxFRDogJ3Vuc3R5bGVkJyxcbiAgUEFSQUdSQVBIOiAndW5zdHlsZWQnLFxuXG4gIEgxOiAnaGVhZGVyLW9uZScsXG4gIEgyOiAnaGVhZGVyLXR3bycsXG4gIEgzOiAnaGVhZGVyLXRocmVlJyxcbiAgSDQ6ICdoZWFkZXItZm91cicsXG4gIEg1OiAnaGVhZGVyLWZpdmUnLFxuICBINjogJ2hlYWRlci1zaXgnLFxuXG4gIE9MOiAnb3JkZXJlZC1saXN0LWl0ZW0nLFxuICBVTDogJ3Vub3JkZXJlZC1saXN0LWl0ZW0nLFxuXG4gIENPREU6ICdjb2RlLWJsb2NrJyxcblxuICBCTE9DS1FVT1RFOiAnYmxvY2txdW90ZScsXG5cbiAgQVRPTUlDOiAnYXRvbWljJyxcbiAgUEhPVE86ICdhdG9taWM6cGhvdG8nLFxuICBWSURFTzogJ2F0b21pYzp2aWRlbycsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBJbmxpbmVcbiAqL1xuZXhwb3J0IGNvbnN0IElubGluZSA9IHtcbiAgQk9MRDogJ0JPTEQnLFxuICBDT0RFOiAnQ09ERScsXG4gIElUQUxJQzogJ0lUQUxJQycsXG4gIFNUUklLRVRIUk9VR0g6ICdTVFJJS0VUSFJPVUdIJyxcbiAgVU5ERVJMSU5FOiAnVU5ERVJMSU5FJyxcbiAgSElHSExJR0hUOiAnSElHSExJR0hUJyxcbn07XG5cbi8qKlxuICogQGNvbnN0YW50IEVudGl0eVxuICovXG5leHBvcnQgY29uc3QgRW50aXR5ID0ge1xuICBMSU5LOiAnTElOSycsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBIWVBFUkxJTktcbiAqL1xuZXhwb3J0IGNvbnN0IEhZUEVSTElOSyA9ICdoeXBlcmxpbmsnO1xuXG4vKipcbiAqIENvbnN0YW50cyB0byBoYW5kbGUga2V5IGNvbW1hbmRzXG4gKi9cbmV4cG9ydCBjb25zdCBIQU5ETEVEID0gJ2hhbmRsZWQnO1xuZXhwb3J0IGNvbnN0IE5PVF9IQU5ETEVEID0gJ25vdF9oYW5kbGVkJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBCbG9ja3MsXG4gIElubGluZSxcbiAgRW50aXR5LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb25zdGFudHMuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgRWRpdG9yIGZyb20gJy4vRWRpdG9yJztcblxuZXhwb3J0IHsgTWF5YXNoRWRpdG9yIH0gZnJvbSAnLi9FZGl0b3InO1xuXG5leHBvcnQgeyBjcmVhdGVFZGl0b3JTdGF0ZSB9IGZyb20gJy4vRWRpdG9yU3RhdGUnO1xuXG5leHBvcnQgZGVmYXVsdCBFZGl0b3I7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvaW5kZXguanMiLCIvKiogQGZvcm1hdCAqL1xuXG5leHBvcnQgY29uc3Qgc3R5bGUgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAyMCxcbiAgICB3aWR0aDogNjAwLFxuICB9LFxuICBlZGl0b3I6IHtcbiAgICBib3JkZXI6ICcxcHggc29saWQgI2RkZCcsXG4gICAgY3Vyc29yOiAndGV4dCcsXG4gICAgZm9udFNpemU6IDE2LFxuICAgIG1pbkhlaWdodDogNDAsXG4gICAgcGFkZGluZzogMTAsXG4gIH0sXG4gIGJ1dHRvbjoge1xuICAgIG1hcmdpblRvcDogMTAsXG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgfSxcbiAgaGFuZGxlOiB7XG4gICAgY29sb3I6ICdyZ2JhKDk4LCAxNzcsIDI1NCwgMS4wKScsXG4gICAgZGlyZWN0aW9uOiAnbHRyJyxcbiAgICB1bmljb2RlQmlkaTogJ2JpZGktb3ZlcnJpZGUnLFxuICB9LFxuICBoYXNodGFnOiB7XG4gICAgY29sb3I6ICdyZ2JhKDk1LCAxODQsIDEzOCwgMS4wKScsXG4gIH0sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBzdHlsZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9zdHlsZS9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=