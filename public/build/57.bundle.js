webpackJsonp([57],{

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./src/client/actions/posts/users/create.js":
/*!**************************************************!*\
  !*** ./src/client/actions/posts/users/create.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../../constants/posts */ "./src/client/constants/posts.js");

/**
 * This action will create post for user in store.
 * @function create
 * @param {Object} payload
 * @param {number} payload.postId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {string} payload.timestamp
 *
 * @param {number} payload.statusCode
 * @param {string} payload.message
 *
 * @returns {Object}
 */
var create = function create(payload) {
  return { type: _posts.POST_CREATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/api/posts/users/create.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/create.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * This function will create post by user.
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {string} payload.postType
 * @param {string} payload.title
 * @return {Promise} -
 *
 * @example
 * create({
 *   userId: 12314123134,
 *   token: 'head.body.tail',
 *   postType: 'article',
 *   title: 'This is awesome article',
 * })
 *   .then(res => console.log(res))
 *   .catch(err => console.error(err));
 */
/**
 * @format
 * @file create post for user
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        postType = _ref2.postType,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ postType: postType, title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/PostCreate/index.js":
/*!***************************************************!*\
  !*** ./src/client/containers/PostCreate/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../api/posts/users/create */ "./src/client/api/posts/users/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../actions/posts/users/create */ "./src/client/actions/posts/users/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * PostCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create post for their
 * profile only.
 *
 * @format
 */

var styles = {
  root: {
    padding: '1%'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  gridContainer: {},
  post: {
    padding: '1%'
  },
  cardHeader: {
    // textAlign: 'center'
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/**
 * PostCreate component will create a new post for signed in user.
 * @class PostCreate
 * @version 1.0.0
 */

var PostCreate = function (_Component) {
  (0, _inherits3.default)(PostCreate, _Component);

  function PostCreate(props) {
    (0, _classCallCheck3.default)(this, PostCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (PostCreate.__proto__ || (0, _getPrototypeOf2.default)(PostCreate)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onFocus = function () {
      return _this.setState({ active: true });
    };

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Title...',
      postType: 'article',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(PostCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      var title = e.target.value;
      var titleLength = title.length;
      var valid = titleLength > 0 && titleLength < 148;

      this.setState({
        title: title,
        titleLength: titleLength,
        valid: valid,
        error: !valid ? 'Title length should be in between 1 to 148.' : ''
      });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props$elements$user, token, userId, _state, postType, title, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();
                _props$elements$user = this.props.elements.user, token = _props$elements$user.token, userId = _props$elements$user.id;
                _state = this.state, postType = _state.postType, title = _state.title;


                this.setState({ message: 'Creating post...' });

                _context.next = 6;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  postType: postType,
                  title: title
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 13:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    postType: 'article',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionPostCreate((0, _extends3.default)({
                  statusCode: statusCode
                }, payload, {
                  title: title,
                  postType: postType
                }));

              case 16:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _state2 = this.state,
          hover = _state2.hover,
          active = _state2.active,
          placeholder = _state2.placeholder,
          title = _state2.title,
          message = _state2.message;
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements;
      var _elements$user = elements.user,
          name = _elements$user.name,
          avatar = _elements$user.avatar,
          username = _elements$user.username;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: name,
                  src: avatar || '/public/photos/mayash-logo-transparent.png',
                  className: classes.avatar
                })
              )
            ),
            title: _react2.default.createElement(_Input2.default, {
              value: title,
              onChange: this.onChange,
              placeholder: placeholder
            }),
            className: classes.cardHeader,
            onFocus: this.onFocus
          }),
          active && message.length ? _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              null,
              message
            )
          ) : null,
          active ? _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onSubmit },
              'Create'
            )
          ) : null
        )
      );
    }
  }]);
  return PostCreate;
}(_react.Component);

PostCreate.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  actionPostCreate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionPostCreate: _create4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(PostCreate));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL3Bvc3RzL3VzZXJzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9wb3N0cy91c2Vycy9jcmVhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0Q3JlYXRlL2luZGV4LmpzIl0sIm5hbWVzIjpbImNyZWF0ZSIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJ1c2VySWQiLCJwb3N0VHlwZSIsInRpdGxlIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJib2R5IiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInN0YXR1c0NvZGUiLCJlcnJvciIsImpzb24iLCJjb25zb2xlIiwic3R5bGVzIiwicm9vdCIsImRpc3BsYXkiLCJpbnB1dCIsImZsZXhHcm93IiwiYm9yZGVyIiwiYm9yZGVyUmFkaXVzIiwicGFkZGluZyIsIm91dGxpbmUiLCJib3JkZXJDb2xvciIsImJveFNoYWRvdyIsIklucHV0IiwiY2xhc3NlcyIsIm9uQ2hhbmdlIiwidmFsdWUiLCJwbGFjZWhvbGRlciIsImRpc2FibGVkIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsImZ1bmMiLCJvbmVPZlR5cGUiLCJzdHJpbmciLCJudW1iZXIiLCJib29sIiwiY2FyZCIsImF2YXRhciIsImdyaWRDb250YWluZXIiLCJwb3N0IiwiY2FyZEhlYWRlciIsImNhcmRNZWRpYSIsImNhcmRNZWRpYUltYWdlIiwid2lkdGgiLCJmbGV4IiwiUG9zdENyZWF0ZSIsInByb3BzIiwib25Nb3VzZUVudGVyIiwic2V0U3RhdGUiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsIm9uRm9jdXMiLCJhY3RpdmUiLCJzdGF0ZSIsInRpdGxlTGVuZ3RoIiwidmFsaWQiLCJtZXNzYWdlIiwiYmluZCIsIm9uU3VibWl0IiwiZSIsInRhcmdldCIsImxlbmd0aCIsInByZXZlbnREZWZhdWx0IiwiZWxlbWVudHMiLCJ1c2VyIiwiaWQiLCJzZXRUaW1lb3V0IiwiYWN0aW9uUG9zdENyZWF0ZSIsIm5hbWUiLCJ1c2VybmFtZSIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4RjtBQUM5Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUUsZ0NBQWdDO0FBQ2pHLFNBQVM7QUFDVDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0EsZ0NBQWdDLHVCQUF1QjtBQUN2RDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxvQkFBb0IsVTs7Ozs7Ozs7Ozs7OztBQy9NekU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsOENBQThDLGlCQUFpQixxQkFBcUIsb0NBQW9DLDZEQUE2RCxvQkFBb0IsRUFBRSxlQUFlOztBQUUxTixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnRkFBZ0Y7O0FBRWhGOztBQUVBLGdGQUFnRixlQUFlOztBQUUvRix5REFBeUQsVUFBVSx1REFBdUQ7QUFDMUg7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLHVCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeEdBOztBQWlCQTs7Ozs7Ozs7Ozs7Ozs7QUFjQSxJQUFNQSxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsT0FBRDtBQUFBLFNBQStCLEVBQUVDLHdCQUFGLEVBQXFCRCxnQkFBckIsRUFBL0I7QUFBQSxDQUFmLEMsQ0FwQ0E7Ozs7O2tCQXNDZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5QmY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVJBOzs7Ozs7c0ZBNkJBO0FBQUEsUUFBd0JHLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxNQUEvQixTQUErQkEsTUFBL0I7QUFBQSxRQUF1Q0MsUUFBdkMsU0FBdUNBLFFBQXZDO0FBQUEsUUFBaURDLEtBQWpELFNBQWlEQSxLQUFqRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ0gsTUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUcsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxNQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVSO0FBSFIsZUFGa0I7QUFPM0JTLG9CQUFNLHlCQUFlLEVBQUVQLGtCQUFGLEVBQVlDLFlBQVosRUFBZjtBQVBxQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFjWUMsa0JBZFosR0FjbUNELEdBZG5DLENBY1lDLE1BZFosRUFjb0JDLFVBZHBCLEdBY21DRixHQWRuQyxDQWNvQkUsVUFkcEI7O0FBQUEsa0JBZ0JRRCxVQUFVLEdBaEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FpQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFqQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXVCdUJGLElBQUlLLElBQUosRUF2QnZCOztBQUFBO0FBdUJVQSxnQkF2QlY7QUFBQSx3RUF5QmdCQSxJQXpCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTJCSUMsb0JBQVFGLEtBQVI7O0FBM0JKLDZDQTZCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE3Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVqQixNOzs7OztBQXhCZjs7OztBQUNBOzs7O2tCQTJEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFEZjs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBLElBQU1vQixTQUFTO0FBQ2JDLFFBQU07QUFDSkMsYUFBUztBQURMLEdBRE87QUFJYkMsU0FBTztBQUNMQyxjQUFVLEdBREw7QUFFTEMsWUFBUSxtQkFGSDtBQUdMQyxrQkFBYyxLQUhUO0FBSUxDLGFBQVMsS0FKSjtBQUtMLGVBQVc7QUFDVEMsZUFBUyxNQURBO0FBRVRDLG1CQUFhLFNBRko7QUFHVEMsaUJBQVc7QUFIRjtBQUxOO0FBSk0sQ0FBZixDLENBWkE7Ozs7Ozs7QUE2QkEsSUFBTUMsUUFBUSxTQUFSQSxLQUFRO0FBQUEsTUFBR0MsT0FBSCxRQUFHQSxPQUFIO0FBQUEsTUFBWUMsUUFBWixRQUFZQSxRQUFaO0FBQUEsTUFBc0JDLEtBQXRCLFFBQXNCQSxLQUF0QjtBQUFBLE1BQTZCQyxXQUE3QixRQUE2QkEsV0FBN0I7QUFBQSxNQUEwQ0MsUUFBMUMsUUFBMENBLFFBQTFDO0FBQUEsTUFBb0RsQyxJQUFwRCxRQUFvREEsSUFBcEQ7QUFBQSxTQUNaO0FBQUE7QUFBQSxNQUFLLFdBQVc4QixRQUFRWCxJQUF4QjtBQUNFO0FBQ0UsWUFBTW5CLFFBQVEsTUFEaEI7QUFFRSxtQkFBYWlDLGVBQWUsT0FGOUI7QUFHRSxhQUFPRCxLQUhUO0FBSUUsZ0JBQVVELFFBSlo7QUFLRSxpQkFBV0QsUUFBUVQsS0FMckI7QUFNRSxnQkFBVSxDQUFDLENBQUNhO0FBTmQ7QUFERixHQURZO0FBQUEsQ0FBZDs7QUFhQUwsTUFBTU0sU0FBTixHQUFrQjtBQUNoQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFEVjtBQUVoQk4sWUFBVSxvQkFBVU8sSUFBVixDQUFlRCxVQUZUO0FBR2hCTCxTQUFPLG9CQUFVTyxTQUFWLENBQW9CLENBQUMsb0JBQVVDLE1BQVgsRUFBbUIsb0JBQVVDLE1BQTdCLENBQXBCLEVBQTBESixVQUhqRDtBQUloQkosZUFBYSxvQkFBVU8sTUFKUDtBQUtoQnhDLFFBQU0sb0JBQVV3QyxNQUxBO0FBTWhCTixZQUFVLG9CQUFVUTtBQU5KLENBQWxCOztrQkFTZSwwQkFBV3hCLE1BQVgsRUFBbUJXLEtBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hDZjs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQTNCQTs7Ozs7Ozs7Ozs7QUE2QkEsSUFBTVgsU0FBUztBQUNiQyxRQUFNO0FBQ0pNLGFBQVM7QUFETCxHQURPO0FBSWJrQixRQUFNO0FBQ0puQixrQkFBYztBQURWLEdBSk87QUFPYm9CLFVBQVE7QUFDTnBCLGtCQUFjO0FBRFIsR0FQSztBQVVicUIsaUJBQWUsRUFWRjtBQVdiQyxRQUFNO0FBQ0pyQixhQUFTO0FBREwsR0FYTztBQWNic0IsY0FBWTtBQUNWO0FBRFUsR0FkQztBQWlCYkMsYUFBVyxFQWpCRTtBQWtCYkMsa0JBQWdCO0FBQ2RDLFdBQU87QUFETyxHQWxCSDtBQXFCYjVCLFlBQVU7QUFDUjZCLFVBQU07QUFERTtBQXJCRyxDQUFmOztBQTBCQTs7Ozs7O0lBS01DLFU7OztBQUNKLHNCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsOElBQ1hBLEtBRFc7O0FBQUEsVUE2RW5CQyxZQTdFbUIsR0E2RUo7QUFBQSxhQUFNLE1BQUtDLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0E3RUk7O0FBQUEsVUE4RW5CQyxZQTlFbUIsR0E4RUo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0E5RUk7O0FBQUEsVUErRW5CRSxPQS9FbUIsR0ErRVQ7QUFBQSxhQUFNLE1BQUtILFFBQUwsQ0FBYyxFQUFFSSxRQUFRLElBQVYsRUFBZCxDQUFOO0FBQUEsS0EvRVM7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYTtBQUNYRCxjQUFRLEtBREc7QUFFWHZELGFBQU8sRUFGSTtBQUdYeUQsbUJBQWEsQ0FIRjtBQUlYNUIsbUJBQWEsVUFKRjtBQUtYOUIsZ0JBQVUsU0FMQzs7QUFPWDJELGFBQU8sS0FQSTtBQVFYaEQsa0JBQVksQ0FSRDtBQVNYQyxhQUFPLEVBVEk7QUFVWGdELGVBQVMsRUFWRTtBQVdYUCxhQUFPO0FBWEksS0FBYjs7QUFjQSxVQUFLekIsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWNpQyxJQUFkLE9BQWhCO0FBQ0EsVUFBS0MsUUFBTCxHQUFnQixNQUFLQSxRQUFMLENBQWNELElBQWQsT0FBaEI7QUFqQmlCO0FBa0JsQjs7Ozs2QkFFUUUsQyxFQUFHO0FBQ1YsVUFBTTlELFFBQVE4RCxFQUFFQyxNQUFGLENBQVNuQyxLQUF2QjtBQUNBLFVBQU02QixjQUFjekQsTUFBTWdFLE1BQTFCO0FBQ0EsVUFBTU4sUUFBUUQsY0FBYyxDQUFkLElBQW1CQSxjQUFjLEdBQS9DOztBQUVBLFdBQUtOLFFBQUwsQ0FBYztBQUNabkQsb0JBRFk7QUFFWnlELGdDQUZZO0FBR1pDLG9CQUhZO0FBSVovQyxlQUFPLENBQUMrQyxLQUFELEdBQVMsNkNBQVQsR0FBeUQ7QUFKcEQsT0FBZDtBQU1EOzs7OzJHQUVjSSxDOzs7Ozs7Ozs7QUFDYkEsa0JBQUVHLGNBQUY7dUNBQzhCLEtBQUtoQixLQUFMLENBQVdpQixRQUFYLENBQW9CQyxJLEVBQTFDdEUsSyx3QkFBQUEsSyxFQUFXQyxNLHdCQUFKc0UsRTt5QkFDYSxLQUFLWixLLEVBQXpCekQsUSxVQUFBQSxRLEVBQVVDLEssVUFBQUEsSzs7O0FBRWxCLHFCQUFLbUQsUUFBTCxDQUFjLEVBQUVRLFNBQVMsa0JBQVgsRUFBZDs7O3VCQUU2QyxzQkFBYztBQUN6RDlELDhCQUR5RDtBQUV6REMsZ0NBRnlEO0FBR3pEQyxvQ0FIeUQ7QUFJekRDO0FBSnlELGlCQUFkLEM7Ozs7QUFBckNVLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPaEIsdUIsU0FBQUEsTzs7c0JBT3ZCZSxjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0EscUJBQUt5QyxRQUFMLENBQWMsRUFBRXpDLHNCQUFGLEVBQWNDLFlBQWQsRUFBZDs7Ozs7QUFJRixxQkFBS3dDLFFBQUwsQ0FBYyxFQUFFUSxTQUFTLHVCQUFYLEVBQWQ7O0FBRUFVLDJCQUFXLFlBQU07QUFDZix5QkFBS2xCLFFBQUwsQ0FBYztBQUNaTywyQkFBTyxLQURLO0FBRVpoRCxnQ0FBWSxDQUZBO0FBR1pDLDJCQUFPLEVBSEs7QUFJWmdELDZCQUFTLEVBSkc7O0FBTVo1RCw4QkFBVSxTQU5FOztBQVFaQywyQkFBTyxFQVJLO0FBU1p5RCxpQ0FBYTtBQVRELG1CQUFkO0FBV0QsaUJBWkQsRUFZRyxJQVpIOztBQWNBLHFCQUFLUixLQUFMLENBQVdxQixnQkFBWDtBQUNFNUQ7QUFERixtQkFFS2YsT0FGTDtBQUdFSyw4QkFIRjtBQUlFRDtBQUpGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBWU87QUFBQSxvQkFDZ0QsS0FBS3lELEtBRHJEO0FBQUEsVUFDQ0osS0FERCxXQUNDQSxLQUREO0FBQUEsVUFDUUcsTUFEUixXQUNRQSxNQURSO0FBQUEsVUFDZ0IxQixXQURoQixXQUNnQkEsV0FEaEI7QUFBQSxVQUM2QjdCLEtBRDdCLFdBQzZCQSxLQUQ3QjtBQUFBLFVBQ29DMkQsT0FEcEMsV0FDb0NBLE9BRHBDO0FBQUEsbUJBRXVCLEtBQUtWLEtBRjVCO0FBQUEsVUFFQ3ZCLE9BRkQsVUFFQ0EsT0FGRDtBQUFBLFVBRVV3QyxRQUZWLFVBRVVBLFFBRlY7QUFBQSwyQkFHNEJBLFNBQVNDLElBSHJDO0FBQUEsVUFHQ0ksSUFIRCxrQkFHQ0EsSUFIRDtBQUFBLFVBR08vQixNQUhQLGtCQUdPQSxNQUhQO0FBQUEsVUFHZWdDLFFBSGYsa0JBR2VBLFFBSGY7OztBQUtQLGFBQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQVc5QyxRQUFRWCxJQURyQjtBQUVFLHdCQUFjLEtBQUttQyxZQUZyQjtBQUdFLHdCQUFjLEtBQUtHO0FBSHJCO0FBS0U7QUFBQTtBQUFBLFlBQU0sUUFBUUQsS0FBZCxFQUFxQixXQUFXMUIsUUFBUWEsSUFBeEM7QUFDRTtBQUNFLG9CQUNFO0FBQUE7QUFBQSxnQkFBTyxXQUFXYixRQUFRYyxNQUExQixFQUFrQyxXQUFXLENBQTdDO0FBQ0U7QUFBQTtBQUFBLGtCQUFNLFdBQVNnQyxRQUFmO0FBQ0U7QUFDRSx1QkFBS0QsSUFEUDtBQUVFLHVCQUFLL0IsVUFBVSw0Q0FGakI7QUFHRSw2QkFBV2QsUUFBUWM7QUFIckI7QUFERjtBQURGLGFBRko7QUFZRSxtQkFDRTtBQUNFLHFCQUFPeEMsS0FEVDtBQUVFLHdCQUFVLEtBQUsyQixRQUZqQjtBQUdFLDJCQUFhRTtBQUhmLGNBYko7QUFtQkUsdUJBQVdILFFBQVFpQixVQW5CckI7QUFvQkUscUJBQVMsS0FBS1c7QUFwQmhCLFlBREY7QUF1QkdDLG9CQUFVSSxRQUFRSyxNQUFsQixHQUNDO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFhTDtBQUFiO0FBREYsV0FERCxHQUlHLElBM0JOO0FBNEJHSixtQkFDQztBQUFBO0FBQUE7QUFDRSxtREFBSyxXQUFXN0IsUUFBUVIsUUFBeEIsR0FERjtBQUVFO0FBQUE7QUFBQSxnQkFBUSxZQUFSLEVBQWUsT0FBTSxRQUFyQixFQUE4QixTQUFTLEtBQUsyQyxRQUE1QztBQUFBO0FBQUE7QUFGRixXQURELEdBT0c7QUFuQ047QUFMRixPQURGO0FBNkNEOzs7OztBQUdIYixXQUFXakIsU0FBWCxHQUF1QjtBQUNyQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFETDs7QUFHckJpQyxZQUFVLG9CQUFVbEMsTUFBVixDQUFpQkMsVUFITjs7QUFLckJxQyxvQkFBa0Isb0JBQVVwQyxJQUFWLENBQWVEO0FBTFosQ0FBdkI7O0FBUUEsSUFBTXdDLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHUCxRQUFILFNBQUdBLFFBQUg7QUFBQSxTQUFtQixFQUFFQSxrQkFBRixFQUFuQjtBQUFBLENBQXhCO0FBQ0EsSUFBTVEscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VMO0FBREYsR0FERixFQUlFSyxRQUpGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVFlLHlCQUFRRixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYiwwQkFBVzVELE1BQVgsRUFBbUJrQyxVQUFuQixDQURhLEMiLCJmaWxlIjoiNTcuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9jb2xvck1hbmlwdWxhdG9yID0gcmVxdWlyZSgnLi4vc3R5bGVzL2NvbG9yTWFuaXB1bGF0b3InKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgZmxleFNocmluazogMCxcbiAgICAgIHdpZHRoOiA0MCxcbiAgICAgIGhlaWdodDogNDAsXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDIwKSxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZSdcbiAgICB9LFxuICAgIGNvbG9yRGVmYXVsdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LFxuICAgICAgYmFja2dyb3VuZENvbG9yOiAoMCwgX2NvbG9yTWFuaXB1bGF0b3IuZW1waGFzaXplKSh0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCwgMC4yNilcbiAgICB9LFxuICAgIGltZzoge1xuICAgICAgbWF4V2lkdGg6ICcxMDAlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VkIGluIGNvbWJpbmF0aW9uIHdpdGggYHNyY2Agb3IgYHNyY1NldGAgdG9cbiAgICogcHJvdmlkZSBhbiBhbHQgYXR0cmlidXRlIGZvciB0aGUgcmVuZGVyZWQgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIGFsdDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZCB0byByZW5kZXIgaWNvbiBvciB0ZXh0IGVsZW1lbnRzIGluc2lkZSB0aGUgQXZhdGFyLlxuICAgKiBgc3JjYCBhbmQgYGFsdGAgcHJvcHMgd2lsbCBub3QgYmUgdXNlZCBhbmQgbm8gYGltZ2Agd2lsbFxuICAgKiBiZSByZW5kZXJlZCBieSBkZWZhdWx0LlxuICAgKlxuICAgKiBUaGlzIGNhbiBiZSBhbiBlbGVtZW50LCBvciBqdXN0IGEgc3RyaW5nLlxuICAgKi9cbiAgY2hpbGRyZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIFRoZSBjbGFzc05hbWUgb2YgdGhlIGNoaWxkIGVsZW1lbnQuXG4gICAqIFVzZWQgYnkgQ2hpcCBhbmQgTGlzdEl0ZW1JY29uIHRvIHN0eWxlIHRoZSBBdmF0YXIgaWNvbi5cbiAgICovXG4gIGNoaWxkcmVuQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBpbWdgIGVsZW1lbnQgd2hlbiB0aGUgY29tcG9uZW50XG4gICAqIGlzIHVzZWQgdG8gZGlzcGxheSBhbiBpbWFnZS5cbiAgICovXG4gIGltZ1Byb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgYHNpemVzYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc2l6ZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBgc3JjYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc3JjOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY1NldGAgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyY1NldDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxudmFyIEF2YXRhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEF2YXRhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQXZhdGFyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEF2YXRhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEF2YXRhci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQXZhdGFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShBdmF0YXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYWx0ID0gX3Byb3BzLmFsdCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jaGlsZHJlbkNsYXNzTmFtZSxcbiAgICAgICAgICBDb21wb25lbnRQcm9wID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBpbWdQcm9wcyA9IF9wcm9wcy5pbWdQcm9wcyxcbiAgICAgICAgICBzaXplcyA9IF9wcm9wcy5zaXplcyxcbiAgICAgICAgICBzcmMgPSBfcHJvcHMuc3JjLFxuICAgICAgICAgIHNyY1NldCA9IF9wcm9wcy5zcmNTZXQsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYWx0JywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NoaWxkcmVuJywgJ2NoaWxkcmVuQ2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdpbWdQcm9wcycsICdzaXplcycsICdzcmMnLCAnc3JjU2V0J10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuY29sb3JEZWZhdWx0LCBjaGlsZHJlblByb3AgJiYgIXNyYyAmJiAhc3JjU2V0KSwgY2xhc3NOYW1lUHJvcCk7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBudWxsO1xuXG4gICAgICBpZiAoY2hpbGRyZW5Qcm9wKSB7XG4gICAgICAgIGlmIChjaGlsZHJlbkNsYXNzTmFtZVByb3AgJiYgdHlwZW9mIGNoaWxkcmVuUHJvcCAhPT0gJ3N0cmluZycgJiYgX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkcmVuUHJvcCkpIHtcbiAgICAgICAgICB2YXIgX2NoaWxkcmVuQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjaGlsZHJlbkNsYXNzTmFtZVByb3AsIGNoaWxkcmVuUHJvcC5wcm9wcy5jbGFzc05hbWUpO1xuICAgICAgICAgIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZHJlblByb3AsIHsgY2xhc3NOYW1lOiBfY2hpbGRyZW5DbGFzc05hbWUgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlblByb3A7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoc3JjIHx8IHNyY1NldCkge1xuICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdpbWcnLCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhbHQ6IGFsdCxcbiAgICAgICAgICBzcmM6IHNyYyxcbiAgICAgICAgICBzcmNTZXQ6IHNyY1NldCxcbiAgICAgICAgICBzaXplczogc2l6ZXMsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc2VzLmltZ1xuICAgICAgICB9LCBpbWdQcm9wcykpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIENvbXBvbmVudFByb3AsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQXZhdGFyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQXZhdGFyLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29tcG9uZW50OiAnZGl2J1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlBdmF0YXInIH0pKEF2YXRhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNCA2IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU0IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0F2YXRhciA9IHJlcXVpcmUoJy4vQXZhdGFyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0F2YXRhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDQgMjQgMjYgMjcgMjggMzEgMzMgMzQgMzUgMzYgMzcgMzkgNDAgNDMgNDkgNTcgNTggNTkgNjEgNjMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wcm9wVHlwZXMgPSByZXF1aXJlKCdwcm9wLXR5cGVzJyk7XG5cbnZhciBfcHJvcFR5cGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Byb3BUeXBlcyk7XG5cbnZhciBfaW52YXJpYW50ID0gcmVxdWlyZSgnaW52YXJpYW50Jyk7XG5cbnZhciBfaW52YXJpYW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2ludmFyaWFudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhvYmosIGtleXMpIHsgdmFyIHRhcmdldCA9IHt9OyBmb3IgKHZhciBpIGluIG9iaikgeyBpZiAoa2V5cy5pbmRleE9mKGkpID49IDApIGNvbnRpbnVlOyBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmosIGkpKSBjb250aW51ZTsgdGFyZ2V0W2ldID0gb2JqW2ldOyB9IHJldHVybiB0YXJnZXQ7IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgaXNNb2RpZmllZEV2ZW50ID0gZnVuY3Rpb24gaXNNb2RpZmllZEV2ZW50KGV2ZW50KSB7XG4gIHJldHVybiAhIShldmVudC5tZXRhS2V5IHx8IGV2ZW50LmFsdEtleSB8fCBldmVudC5jdHJsS2V5IHx8IGV2ZW50LnNoaWZ0S2V5KTtcbn07XG5cbi8qKlxuICogVGhlIHB1YmxpYyBBUEkgZm9yIHJlbmRlcmluZyBhIGhpc3RvcnktYXdhcmUgPGE+LlxuICovXG5cbnZhciBMaW5rID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgX2luaGVyaXRzKExpbmssIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIExpbmsoKSB7XG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBMaW5rKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX1JlYWN0JENvbXBvbmVudC5jYWxsLmFwcGx5KF9SZWFjdCRDb21wb25lbnQsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5oYW5kbGVDbGljayA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLm9uQ2xpY2spIF90aGlzLnByb3BzLm9uQ2xpY2soZXZlbnQpO1xuXG4gICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQgJiYgLy8gb25DbGljayBwcmV2ZW50ZWQgZGVmYXVsdFxuICAgICAgZXZlbnQuYnV0dG9uID09PSAwICYmIC8vIGlnbm9yZSByaWdodCBjbGlja3NcbiAgICAgICFfdGhpcy5wcm9wcy50YXJnZXQgJiYgLy8gbGV0IGJyb3dzZXIgaGFuZGxlIFwidGFyZ2V0PV9ibGFua1wiIGV0Yy5cbiAgICAgICFpc01vZGlmaWVkRXZlbnQoZXZlbnQpIC8vIGlnbm9yZSBjbGlja3Mgd2l0aCBtb2RpZmllciBrZXlzXG4gICAgICApIHtcbiAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgdmFyIGhpc3RvcnkgPSBfdGhpcy5jb250ZXh0LnJvdXRlci5oaXN0b3J5O1xuICAgICAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgICAgICByZXBsYWNlID0gX3RoaXMkcHJvcHMucmVwbGFjZSxcbiAgICAgICAgICAgICAgdG8gPSBfdGhpcyRwcm9wcy50bztcblxuXG4gICAgICAgICAgaWYgKHJlcGxhY2UpIHtcbiAgICAgICAgICAgIGhpc3RvcnkucmVwbGFjZSh0byk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGhpc3RvcnkucHVzaCh0byk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBMaW5rLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgIHJlcGxhY2UgPSBfcHJvcHMucmVwbGFjZSxcbiAgICAgICAgdG8gPSBfcHJvcHMudG8sXG4gICAgICAgIGlubmVyUmVmID0gX3Byb3BzLmlubmVyUmVmLFxuICAgICAgICBwcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhfcHJvcHMsIFsncmVwbGFjZScsICd0bycsICdpbm5lclJlZiddKTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby11bnVzZWQtdmFyc1xuXG4gICAgKDAsIF9pbnZhcmlhbnQyLmRlZmF1bHQpKHRoaXMuY29udGV4dC5yb3V0ZXIsICdZb3Ugc2hvdWxkIG5vdCB1c2UgPExpbms+IG91dHNpZGUgYSA8Um91dGVyPicpO1xuXG4gICAgdmFyIGhyZWYgPSB0aGlzLmNvbnRleHQucm91dGVyLmhpc3RvcnkuY3JlYXRlSHJlZih0eXBlb2YgdG8gPT09ICdzdHJpbmcnID8geyBwYXRobmFtZTogdG8gfSA6IHRvKTtcblxuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnYScsIF9leHRlbmRzKHt9LCBwcm9wcywgeyBvbkNsaWNrOiB0aGlzLmhhbmRsZUNsaWNrLCBocmVmOiBocmVmLCByZWY6IGlubmVyUmVmIH0pKTtcbiAgfTtcblxuICByZXR1cm4gTGluaztcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkxpbmsucHJvcFR5cGVzID0ge1xuICBvbkNsaWNrOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIHRhcmdldDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG4gIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgdG86IF9wcm9wVHlwZXMyLmRlZmF1bHQub25lT2ZUeXBlKFtfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZywgX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3RdKS5pc1JlcXVpcmVkLFxuICBpbm5lclJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmNdKVxufTtcbkxpbmsuZGVmYXVsdFByb3BzID0ge1xuICByZXBsYWNlOiBmYWxzZVxufTtcbkxpbmsuY29udGV4dFR5cGVzID0ge1xuICByb3V0ZXI6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgIGhpc3Rvcnk6IF9wcm9wVHlwZXMyLmRlZmF1bHQuc2hhcGUoe1xuICAgICAgcHVzaDogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWQsXG4gICAgICByZXBsYWNlOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIGNyZWF0ZUhyZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkXG4gICAgfSkuaXNSZXF1aXJlZFxuICB9KS5pc1JlcXVpcmVkXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gTGluaztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI2IDI3IDI4IDMwIDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDU3IDU4IDU5IDYxIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IFBPU1RfQ1JFQVRFIH0gZnJvbSAnLi4vLi4vLi4vY29uc3RhbnRzL3Bvc3RzJztcblxudHlwZSBQYXlsb2FkID0ge1xuICBwb3N0SWQ6IG51bWJlcixcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgdGl0bGU6IHN0cmluZyxcbiAgdGltZXN0YW1wOiBzdHJpbmcsXG5cbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBtZXNzYWdlOiBzdHJpbmcsXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBUaGlzIGFjdGlvbiB3aWxsIGNyZWF0ZSBwb3N0IGZvciB1c2VyIGluIHN0b3JlLlxuICogQGZ1bmN0aW9uIGNyZWF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnBvc3RJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYXV0aG9ySWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRpdGxlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aW1lc3RhbXBcbiAqXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5zdGF0dXNDb2RlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5tZXNzYWdlXG4gKlxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgY3JlYXRlID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHsgdHlwZTogUE9TVF9DUkVBVEUsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9wb3N0cy91c2Vycy9jcmVhdGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmaWxlIGNyZWF0ZSBwb3N0IGZvciB1c2VyXG4gKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uLy4uL2NvbmZpZyc7XG5cbi8qKlxuICogVGhpcyBmdW5jdGlvbiB3aWxsIGNyZWF0ZSBwb3N0IGJ5IHVzZXIuXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBjcmVhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5wb3N0VHlwZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGVcbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICogY3JlYXRlKHtcbiAqICAgdXNlcklkOiAxMjMxNDEyMzEzNCxcbiAqICAgdG9rZW46ICdoZWFkLmJvZHkudGFpbCcsXG4gKiAgIHBvc3RUeXBlOiAnYXJ0aWNsZScsXG4gKiAgIHRpdGxlOiAnVGhpcyBpcyBhd2Vzb21lIGFydGljbGUnLFxuICogfSlcbiAqICAgLnRoZW4ocmVzID0+IGNvbnNvbGUubG9nKHJlcykpXG4gKiAgIC5jYXRjaChlcnIgPT4gY29uc29sZS5lcnJvcihlcnIpKTtcbiAqL1xuYXN5bmMgZnVuY3Rpb24gY3JlYXRlKHsgdG9rZW4sIHVzZXJJZCwgcG9zdFR5cGUsIHRpdGxlIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L3Bvc3RzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUE9TVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgICBib2R5OiBKU09OLnN0cmluZ2lmeSh7IHBvc3RUeXBlLCB0aXRsZSB9KSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9wb3N0cy91c2Vycy9jcmVhdGUuanMiLCIvKipcbiAqIFRoaXMgaW5wdXQgY29tcG9uZW50IGlzIG1haW5seSBkZXZlbG9wZWQgZm9yIGNyZWF0ZSBwb3N0LCBjcmVhdGVcbiAqIGNvdXJzZSBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgfSxcbiAgaW5wdXQ6IHtcbiAgICBmbGV4R3JvdzogJzEnLFxuICAgIGJvcmRlcjogJzJweCBzb2xpZCAjZGFkYWRhJyxcbiAgICBib3JkZXJSYWRpdXM6ICc3cHgnLFxuICAgIHBhZGRpbmc6ICc4cHgnLFxuICAgICcmOmZvY3VzJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICcjOWVjYWVkJyxcbiAgICAgIGJveFNoYWRvdzogJzAgMCAxMHB4ICM5ZWNhZWQnLFxuICAgIH0sXG4gIH0sXG59O1xuXG5jb25zdCBJbnB1dCA9ICh7IGNsYXNzZXMsIG9uQ2hhbmdlLCB2YWx1ZSwgcGxhY2Vob2xkZXIsIGRpc2FibGVkLCB0eXBlIH0pID0+IChcbiAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgPGlucHV0XG4gICAgICB0eXBlPXt0eXBlIHx8ICd0ZXh0J31cbiAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlciB8fCAnaW5wdXQnfVxuICAgICAgdmFsdWU9e3ZhbHVlfVxuICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgZGlzYWJsZWQ9eyEhZGlzYWJsZWR9XG4gICAgLz5cbiAgPC9kaXY+XG4pO1xuXG5JbnB1dC5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIHZhbHVlOiBQcm9wVHlwZXMub25lT2ZUeXBlKFtQcm9wVHlwZXMuc3RyaW5nLCBQcm9wVHlwZXMubnVtYmVyXSkuaXNSZXF1aXJlZCxcbiAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG4gIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShJbnB1dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvSW5wdXQuanMiLCIvKipcbiAqIFBvc3RDcmVhdGUgY29tcG9uZW50IHdpbGwgYmUgdXNlZCB0byBjcmVhdGUgbmV3IHBvc3QuXG4gKlxuICogTm90ZTogYmVmb3JlIGFkZGluZyB0aGlzIGNvbXBvbmVudCBpbiBhbnkgcGxhY2UsIG1ha2Ugc3VyZVxuICogdGhlIHBlcnNvbiB3aG8gaXMgYWNjZXNzaW5nIHRoaXMgY29tcG9uZW50IHNob3VsZCBoYXZlIHJpZ2h0IHRvIGNyZWF0ZSBwb3N0LlxuICogaS5lLiBwZXJzb24gc2hvdWxkIGJlIGxvZ2dlZCBpbiBhbmQgdXNlciBjYW4gb25seSBjcmVhdGUgcG9zdCBmb3IgdGhlaXJcbiAqIHByb2ZpbGUgb25seS5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IExpbmsgZnJvbSAncmVhY3Qtcm91dGVyLWRvbS9MaW5rJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciwgQ2FyZENvbnRlbnQsIENhcmRBY3Rpb25zIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgQXZhdGFyIGZyb20gJ21hdGVyaWFsLXVpL0F2YXRhcic7XG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdtYXRlcmlhbC11aS9UeXBvZ3JhcGh5JztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcbmltcG9ydCBQYXBlciBmcm9tICdtYXRlcmlhbC11aS9QYXBlcic7XG5cbmltcG9ydCBJbnB1dCBmcm9tICcuLi8uLi9jb21wb25lbnRzL0lucHV0JztcblxuaW1wb3J0IGFwaVBvc3RDcmVhdGUgZnJvbSAnLi4vLi4vYXBpL3Bvc3RzL3VzZXJzL2NyZWF0ZSc7XG5pbXBvcnQgYWN0aW9uUG9zdENyZWF0ZSBmcm9tICcuLi8uLi9hY3Rpb25zL3Bvc3RzL3VzZXJzL2NyZWF0ZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBncmlkQ29udGFpbmVyOiB7fSxcbiAgcG9zdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmRIZWFkZXI6IHtcbiAgICAvLyB0ZXh0QWxpZ246ICdjZW50ZXInXG4gIH0sXG4gIGNhcmRNZWRpYToge30sXG4gIGNhcmRNZWRpYUltYWdlOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuLyoqXG4gKiBQb3N0Q3JlYXRlIGNvbXBvbmVudCB3aWxsIGNyZWF0ZSBhIG5ldyBwb3N0IGZvciBzaWduZWQgaW4gdXNlci5cbiAqIEBjbGFzcyBQb3N0Q3JlYXRlXG4gKiBAdmVyc2lvbiAxLjAuMFxuICovXG5jbGFzcyBQb3N0Q3JlYXRlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGFjdGl2ZTogZmFsc2UsXG4gICAgICB0aXRsZTogJycsXG4gICAgICB0aXRsZUxlbmd0aDogMCxcbiAgICAgIHBsYWNlaG9sZGVyOiAnVGl0bGUuLi4nLFxuICAgICAgcG9zdFR5cGU6ICdhcnRpY2xlJyxcblxuICAgICAgdmFsaWQ6IGZhbHNlLFxuICAgICAgc3RhdHVzQ29kZTogMCxcbiAgICAgIGVycm9yOiAnJyxcbiAgICAgIG1lc3NhZ2U6ICcnLFxuICAgICAgaG92ZXI6IGZhbHNlLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25TdWJtaXQgPSB0aGlzLm9uU3VibWl0LmJpbmQodGhpcyk7XG4gIH1cblxuICBvbkNoYW5nZShlKSB7XG4gICAgY29uc3QgdGl0bGUgPSBlLnRhcmdldC52YWx1ZTtcbiAgICBjb25zdCB0aXRsZUxlbmd0aCA9IHRpdGxlLmxlbmd0aDtcbiAgICBjb25zdCB2YWxpZCA9IHRpdGxlTGVuZ3RoID4gMCAmJiB0aXRsZUxlbmd0aCA8IDE0ODtcblxuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgdGl0bGUsXG4gICAgICB0aXRsZUxlbmd0aCxcbiAgICAgIHZhbGlkLFxuICAgICAgZXJyb3I6ICF2YWxpZCA/ICdUaXRsZSBsZW5ndGggc2hvdWxkIGJlIGluIGJldHdlZW4gMSB0byAxNDguJyA6ICcnLFxuICAgIH0pO1xuICB9XG5cbiAgYXN5bmMgb25TdWJtaXQoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCB7IHRva2VuLCBpZDogdXNlcklkIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBwb3N0VHlwZSwgdGl0bGUgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogJ0NyZWF0aW5nIHBvc3QuLi4nIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpUG9zdENyZWF0ZSh7XG4gICAgICB0b2tlbixcbiAgICAgIHVzZXJJZCxcbiAgICAgIHBvc3RUeXBlLFxuICAgICAgdGl0bGUsXG4gICAgfSk7XG5cbiAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgIC8vIGhhbmRsZSBlcnJvclxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IHN0YXR1c0NvZGUsIGVycm9yIH0pO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnU3VjY2Vzc2Z1bGx5IENyZWF0ZWQuJyB9KTtcblxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgIHZhbGlkOiBmYWxzZSxcbiAgICAgICAgc3RhdHVzQ29kZTogMCxcbiAgICAgICAgZXJyb3I6ICcnLFxuICAgICAgICBtZXNzYWdlOiAnJyxcblxuICAgICAgICBwb3N0VHlwZTogJ2FydGljbGUnLFxuXG4gICAgICAgIHRpdGxlOiAnJyxcbiAgICAgICAgdGl0bGVMZW5ndGg6IDAsXG4gICAgICB9KTtcbiAgICB9LCAxNTAwKTtcblxuICAgIHRoaXMucHJvcHMuYWN0aW9uUG9zdENyZWF0ZSh7XG4gICAgICBzdGF0dXNDb2RlLFxuICAgICAgLi4ucGF5bG9hZCxcbiAgICAgIHRpdGxlLFxuICAgICAgcG9zdFR5cGUsXG4gICAgfSk7XG4gIH1cblxuICBvbk1vdXNlRW50ZXIgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IHRydWUgfSk7XG4gIG9uTW91c2VMZWF2ZSA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogZmFsc2UgfSk7XG4gIG9uRm9jdXMgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgYWN0aXZlOiB0cnVlIH0pO1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGhvdmVyLCBhY3RpdmUsIHBsYWNlaG9sZGVyLCB0aXRsZSwgbWVzc2FnZSB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgbmFtZSwgYXZhdGFyLCB1c2VybmFtZSB9ID0gZWxlbWVudHMudXNlcjtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgPlxuICAgICAgICA8Q2FyZCByYWlzZWQ9e2hvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgIGF2YXRhcj17XG4gICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake3VzZXJuYW1lfWB9PlxuICAgICAgICAgICAgICAgICAgPEF2YXRhclxuICAgICAgICAgICAgICAgICAgICBhbHQ9e25hbWV9XG4gICAgICAgICAgICAgICAgICAgIHNyYz17YXZhdGFyIHx8ICcvcHVibGljL3Bob3Rvcy9tYXlhc2gtbG9nby10cmFuc3BhcmVudC5wbmcnfVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIDwvUGFwZXI+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgIDxJbnB1dFxuICAgICAgICAgICAgICAgIHZhbHVlPXt0aXRsZX1cbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZEhlYWRlcn1cbiAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMub25Gb2N1c31cbiAgICAgICAgICAvPlxuICAgICAgICAgIHthY3RpdmUgJiYgbWVzc2FnZS5sZW5ndGggPyAoXG4gICAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIDxUeXBvZ3JhcGh5PnttZXNzYWdlfTwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAge2FjdGl2ZSA/IChcbiAgICAgICAgICAgIDxDYXJkQWN0aW9ucz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICAgIDxCdXR0b24gcmFpc2VkIGNvbG9yPVwiYWNjZW50XCIgb25DbGljaz17dGhpcy5vblN1Ym1pdH0+XG4gICAgICAgICAgICAgICAgQ3JlYXRlXG4gICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5Qb3N0Q3JlYXRlLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgYWN0aW9uUG9zdENyZWF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbn07XG5cbmNvbnN0IG1hcFN0YXRlVG9Qcm9wcyA9ICh7IGVsZW1lbnRzIH0pID0+ICh7IGVsZW1lbnRzIH0pO1xuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PlxuICBiaW5kQWN0aW9uQ3JlYXRvcnMoXG4gICAge1xuICAgICAgYWN0aW9uUG9zdENyZWF0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKFBvc3RDcmVhdGUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0Q3JlYXRlL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==