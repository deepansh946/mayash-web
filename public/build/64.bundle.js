webpackJsonp([64],{

/***/ "./node_modules/material-ui/Checkbox/Checkbox.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/Checkbox/Checkbox.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _SwitchBase = __webpack_require__(/*! ../internal/SwitchBase */ "./node_modules/material-ui/internal/SwitchBase.js");

var _SwitchBase2 = _interopRequireDefault(_SwitchBase);

var _IndeterminateCheckBox = __webpack_require__(/*! ../svg-icons/IndeterminateCheckBox */ "./node_modules/material-ui/svg-icons/IndeterminateCheckBox.js");

var _IndeterminateCheckBox2 = _interopRequireDefault(_IndeterminateCheckBox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    default: {
      color: theme.palette.text.secondary
    },
    checked: {
      color: theme.palette.primary[500]
    },
    disabled: {
      color: theme.palette.action.disabled
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * If `true`, the component is checked.
   */
  checked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * The icon to display when the component is checked.
   * If a string is provided, it will be used as a font ligature.
   */
  checkedIcon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   */
  defaultChecked: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the switch will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * If `true`, the ripple effect will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * The icon to display when the component is unchecked.
   * If a string is provided, it will be used as a font ligature.
   */
  icon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * If `true`, the component appears indeterminate.
   */
  indeterminate: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The icon to display when the component is indeterminate.
   * If a string is provided, it will be used as a font ligature.
   */
  indeterminateIcon: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Properties applied to the `input` element.
   */
  inputProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Use that property to pass a ref callback to the native input component.
   */
  inputRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /*
   * @ignore
   */
  name: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Callback fired when the state is changed.
   *
   * @param {object} event The event source of the callback
   * @param {boolean} checked The `checked` value of the switch
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  tabIndex: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * The value of the component.
   */
  value: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Checkbox = function (_React$Component) {
  (0, _inherits3.default)(Checkbox, _React$Component);

  function Checkbox() {
    (0, _classCallCheck3.default)(this, Checkbox);
    return (0, _possibleConstructorReturn3.default)(this, (Checkbox.__proto__ || (0, _getPrototypeOf2.default)(Checkbox)).apply(this, arguments));
  }

  (0, _createClass3.default)(Checkbox, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          checkedIcon = _props.checkedIcon,
          icon = _props.icon,
          indeterminate = _props.indeterminate,
          indeterminateIcon = _props.indeterminateIcon,
          other = (0, _objectWithoutProperties3.default)(_props, ['checkedIcon', 'icon', 'indeterminate', 'indeterminateIcon']);


      return _react2.default.createElement(_SwitchBase2.default, (0, _extends3.default)({
        checkedIcon: indeterminate ? indeterminateIcon : checkedIcon,
        icon: indeterminate ? indeterminateIcon : icon
      }, other));
    }
  }]);
  return Checkbox;
}(_react2.default.Component);

Checkbox.defaultProps = {
  indeterminate: false,
  indeterminateIcon: _react2.default.createElement(_IndeterminateCheckBox2.default, null)
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiCheckbox' })(Checkbox);

/***/ }),

/***/ "./node_modules/material-ui/Checkbox/index.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui/Checkbox/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Checkbox = __webpack_require__(/*! ./Checkbox */ "./node_modules/material-ui/Checkbox/Checkbox.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Checkbox).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/IndeterminateCheckBox.js":
/*!*********************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/IndeterminateCheckBox.js ***!
  \*********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M19 3H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-2 10H7v-2h10v2z' });

var IndeterminateCheckBox = function IndeterminateCheckBox(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
IndeterminateCheckBox = (0, _pure2.default)(IndeterminateCheckBox);
IndeterminateCheckBox.muiName = 'SvgIcon';

exports.default = IndeterminateCheckBox;

/***/ }),

/***/ "./src/client/containers/CoursePage/TestYourself/Answer.js":
/*!*****************************************************************!*\
  !*** ./src/client/containers/CoursePage/TestYourself/Answer.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Checkbox = __webpack_require__(/*! material-ui/Checkbox */ "./node_modules/material-ui/Checkbox/index.js");

var _Checkbox2 = _interopRequireDefault(_Checkbox);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      flexGrow: 1
    }
  };
};

var Answer = function (_Component) {
  (0, _inherits3.default)(Answer, _Component);

  function Answer(props) {
    (0, _classCallCheck3.default)(this, Answer);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Answer.__proto__ || (0, _getPrototypeOf2.default)(Answer)).call(this, props));

    _this.onChange = function (e) {
      _this.setState({
        dirty: true,
        answer: e.target.value
      });
    };

    _this.state = {
      dirty: false,
      answer: false
    };
    return _this;
  }

  (0, _createClass3.default)(Answer, [{
    key: 'render',
    value: function render() {
      // console.log(this.props);
      var dirty = this.state.dirty;
      var _props = this.props,
          title = _props.title,
          explaination = _props.explaination;


      return _react2.default.createElement(
        'div',
        { className: this.props.classes.root },
        _react2.default.createElement(_Checkbox2.default, { value: this.props.answer, onChange: this.onChange }),
        title,
        _react2.default.createElement(
          'div',
          null,
          dirty ? explaination : null
        )
      );
    }
  }]);
  return Answer;
}(_react.Component);

Answer.propTypes = {
  title: _propTypes2.default.string.isRequired,
  answer: _propTypes2.default.bool,
  explaination: _propTypes2.default.string.isRequired
};
Answer.defaultProps = {
  answer: false
};
exports.default = (0, _withStyles2.default)(styles)(Answer);

/***/ }),

/***/ "./src/client/containers/CoursePage/TestYourself/Question.js":
/*!*******************************************************************!*\
  !*** ./src/client/containers/CoursePage/TestYourself/Question.js ***!
  \*******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Card = __webpack_require__(/*! material-ui/Card/Card */ "./node_modules/material-ui/Card/Card.js");

var _Card2 = _interopRequireDefault(_Card);

var _CardHeader = __webpack_require__(/*! material-ui/Card/CardHeader */ "./node_modules/material-ui/Card/CardHeader.js");

var _CardHeader2 = _interopRequireDefault(_CardHeader);

var _CardContent = __webpack_require__(/*! material-ui/Card/CardContent */ "./node_modules/material-ui/Card/CardContent.js");

var _CardContent2 = _interopRequireDefault(_CardContent);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Answer = __webpack_require__(/*! ./Answer */ "./src/client/containers/CoursePage/TestYourself/Answer.js");

var _Answer2 = _interopRequireDefault(_Answer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      padding: '1px'
    }
  };
};

var Question = function (_Component) {
  (0, _inherits3.default)(Question, _Component);

  function Question(props) {
    (0, _classCallCheck3.default)(this, Question);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Question.__proto__ || (0, _getPrototypeOf2.default)(Question)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Question, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: this.props.classes.root },
        _react2.default.createElement(
          _Card2.default,
          { raised: true },
          _react2.default.createElement(_CardHeader2.default, { title: this.props.title }),
          _react2.default.createElement(
            _CardContent2.default,
            null,
            this.props.options.map(function (a, i) {
              return _react2.default.createElement(_Answer2.default, (0, _extends3.default)({ key: i + 1 }, a));
            })
          )
        )
      );
    }
  }]);
  return Question;
}(_react.Component);

exports.default = (0, _withStyles2.default)(styles)(Question);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQ2hlY2tib3gvQ2hlY2tib3guanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0NoZWNrYm94L2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvSW5kZXRlcm1pbmF0ZUNoZWNrQm94LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2VQYWdlL1Rlc3RZb3Vyc2VsZi9BbnN3ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvVGVzdFlvdXJzZWxmL1F1ZXN0aW9uLmpzIl0sIm5hbWVzIjpbInN0eWxlcyIsInRoZW1lIiwicm9vdCIsImZsZXhHcm93IiwiQW5zd2VyIiwicHJvcHMiLCJvbkNoYW5nZSIsImUiLCJzZXRTdGF0ZSIsImRpcnR5IiwiYW5zd2VyIiwidGFyZ2V0IiwidmFsdWUiLCJzdGF0ZSIsInRpdGxlIiwiZXhwbGFpbmF0aW9uIiwiY2xhc3NlcyIsInByb3BUeXBlcyIsInN0cmluZyIsImlzUmVxdWlyZWQiLCJib29sIiwiZGVmYXVsdFByb3BzIiwicGFkZGluZyIsIlF1ZXN0aW9uIiwib3B0aW9ucyIsIm1hcCIsImEiLCJpIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQixhQUFhLFFBQVE7QUFDckI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHNCQUFzQixZOzs7Ozs7Ozs7Ozs7O0FDN0wzRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxrREFBa0QscUdBQXFHOztBQUV2SjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsd0M7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ25DQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pDLGdCQUFVO0FBRE47QUFEbUIsR0FBWjtBQUFBLENBQWY7O0lBTU1DLE07OztBQVdKLGtCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBQUEsVUFRbkJDLFFBUm1CLEdBUVIsVUFBQ0MsQ0FBRCxFQUFPO0FBQ2hCLFlBQUtDLFFBQUwsQ0FBYztBQUNaQyxlQUFPLElBREs7QUFFWkMsZ0JBQVFILEVBQUVJLE1BQUYsQ0FBU0M7QUFGTCxPQUFkO0FBSUQsS0Fia0I7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYTtBQUNYSixhQUFPLEtBREk7QUFFWEMsY0FBUTtBQUZHLEtBQWI7QUFGaUI7QUFNbEI7Ozs7NkJBU1E7QUFDUDtBQURPLFVBRUNELEtBRkQsR0FFVyxLQUFLSSxLQUZoQixDQUVDSixLQUZEO0FBQUEsbUJBR3lCLEtBQUtKLEtBSDlCO0FBQUEsVUFHQ1MsS0FIRCxVQUdDQSxLQUhEO0FBQUEsVUFHUUMsWUFIUixVQUdRQSxZQUhSOzs7QUFLUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVcsS0FBS1YsS0FBTCxDQUFXVyxPQUFYLENBQW1CZCxJQUFuQztBQUNFLDREQUFVLE9BQU8sS0FBS0csS0FBTCxDQUFXSyxNQUE1QixFQUFvQyxVQUFVLEtBQUtKLFFBQW5ELEdBREY7QUFFR1EsYUFGSDtBQUdFO0FBQUE7QUFBQTtBQUFNTCxrQkFBUU0sWUFBUixHQUF1QjtBQUE3QjtBQUhGLE9BREY7QUFPRDs7Ozs7QUF0Q0dYLE0sQ0FDR2EsUyxHQUFZO0FBQ2pCSCxTQUFPLG9CQUFVSSxNQUFWLENBQWlCQyxVQURQO0FBRWpCVCxVQUFRLG9CQUFVVSxJQUZEO0FBR2pCTCxnQkFBYyxvQkFBVUcsTUFBVixDQUFpQkM7QUFIZCxDO0FBRGZmLE0sQ0FPR2lCLFksR0FBZTtBQUNwQlgsVUFBUTtBQURZLEM7a0JBa0NULDBCQUFXVixNQUFYLEVBQW1CSSxNQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdERmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUosU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pvQixlQUFTO0FBREw7QUFEbUIsR0FBWjtBQUFBLENBQWY7O0lBTU1DLFE7OztBQUNKLG9CQUFZbEIsS0FBWixFQUFtQjtBQUFBOztBQUFBLDBJQUNYQSxLQURXOztBQUVqQixVQUFLUSxLQUFMLEdBQWEsRUFBYjtBQUZpQjtBQUdsQjs7Ozs2QkFFUTtBQUNQLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBVyxLQUFLUixLQUFMLENBQVdXLE9BQVgsQ0FBbUJkLElBQW5DO0FBQ0U7QUFBQTtBQUFBLFlBQU0sWUFBTjtBQUNFLGdFQUFZLE9BQU8sS0FBS0csS0FBTCxDQUFXUyxLQUE5QixHQURGO0FBR0U7QUFBQTtBQUFBO0FBQ0csaUJBQUtULEtBQUwsQ0FBV21CLE9BQVgsQ0FBbUJDLEdBQW5CLENBQXVCLFVBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLHFCQUFVLHlFQUFRLEtBQUtBLElBQUksQ0FBakIsSUFBd0JELENBQXhCLEVBQVY7QUFBQSxhQUF2QjtBQURIO0FBSEY7QUFERixPQURGO0FBV0Q7Ozs7O2tCQUdZLDBCQUFXMUIsTUFBWCxFQUFtQnVCLFFBQW5CLEMiLCJmaWxlIjoiNjQuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9Td2l0Y2hCYXNlID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvU3dpdGNoQmFzZScpO1xuXG52YXIgX1N3aXRjaEJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3dpdGNoQmFzZSk7XG5cbnZhciBfSW5kZXRlcm1pbmF0ZUNoZWNrQm94ID0gcmVxdWlyZSgnLi4vc3ZnLWljb25zL0luZGV0ZXJtaW5hdGVDaGVja0JveCcpO1xuXG52YXIgX0luZGV0ZXJtaW5hdGVDaGVja0JveDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JbmRldGVybWluYXRlQ2hlY2tCb3gpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICBkZWZhdWx0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LnNlY29uZGFyeVxuICAgIH0sXG4gICAgY2hlY2tlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfSxcbiAgICBkaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgY29tcG9uZW50IGlzIGNoZWNrZWQuXG4gICAqL1xuICBjaGVja2VkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiB0byBkaXNwbGF5IHdoZW4gdGhlIGNvbXBvbmVudCBpcyBjaGVja2VkLlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGEgZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoZWNrZWRJY29uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgZGVmYXVsdENoZWNrZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBzd2l0Y2ggd2lsbCBiZSBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgcmlwcGxlIGVmZmVjdCB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIHRvIGRpc3BsYXkgd2hlbiB0aGUgY29tcG9uZW50IGlzIHVuY2hlY2tlZC5cbiAgICogSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBpY29uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNvbXBvbmVudCBhcHBlYXJzIGluZGV0ZXJtaW5hdGUuXG4gICAqL1xuICBpbmRldGVybWluYXRlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgaWNvbiB0byBkaXNwbGF5IHdoZW4gdGhlIGNvbXBvbmVudCBpcyBpbmRldGVybWluYXRlLlxuICAgKiBJZiBhIHN0cmluZyBpcyBwcm92aWRlZCwgaXQgd2lsbCBiZSB1c2VkIGFzIGEgZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGluZGV0ZXJtaW5hdGVJY29uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvcGVydGllcyBhcHBsaWVkIHRvIHRoZSBgaW5wdXRgIGVsZW1lbnQuXG4gICAqL1xuICBpbnB1dFByb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSBuYXRpdmUgaW5wdXQgY29tcG9uZW50LlxuICAgKi9cbiAgaW5wdXRSZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG5hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIHN0YXRlIGlzIGNoYW5nZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKiBAcGFyYW0ge2Jvb2xlYW59IGNoZWNrZWQgVGhlIGBjaGVja2VkYCB2YWx1ZSBvZiB0aGUgc3dpdGNoXG4gICAqL1xuICBvbkNoYW5nZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHRhYkluZGV4OiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXSksXG5cbiAgLyoqXG4gICAqIFRoZSB2YWx1ZSBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgdmFsdWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cbnZhciBDaGVja2JveCA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKENoZWNrYm94LCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBDaGVja2JveCgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBDaGVja2JveCk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKENoZWNrYm94Ll9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShDaGVja2JveCkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQ2hlY2tib3gsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hlY2tlZEljb24gPSBfcHJvcHMuY2hlY2tlZEljb24sXG4gICAgICAgICAgaWNvbiA9IF9wcm9wcy5pY29uLFxuICAgICAgICAgIGluZGV0ZXJtaW5hdGUgPSBfcHJvcHMuaW5kZXRlcm1pbmF0ZSxcbiAgICAgICAgICBpbmRldGVybWluYXRlSWNvbiA9IF9wcm9wcy5pbmRldGVybWluYXRlSWNvbixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGVja2VkSWNvbicsICdpY29uJywgJ2luZGV0ZXJtaW5hdGUnLCAnaW5kZXRlcm1pbmF0ZUljb24nXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9Td2l0Y2hCYXNlMi5kZWZhdWx0LCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgY2hlY2tlZEljb246IGluZGV0ZXJtaW5hdGUgPyBpbmRldGVybWluYXRlSWNvbiA6IGNoZWNrZWRJY29uLFxuICAgICAgICBpY29uOiBpbmRldGVybWluYXRlID8gaW5kZXRlcm1pbmF0ZUljb24gOiBpY29uXG4gICAgICB9LCBvdGhlcikpO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQ2hlY2tib3g7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5DaGVja2JveC5kZWZhdWx0UHJvcHMgPSB7XG4gIGluZGV0ZXJtaW5hdGU6IGZhbHNlLFxuICBpbmRldGVybWluYXRlSWNvbjogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0luZGV0ZXJtaW5hdGVDaGVja0JveDIuZGVmYXVsdCwgbnVsbClcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQ2hlY2tib3gnIH0pKENoZWNrYm94KTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9DaGVja2JveC9DaGVja2JveC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQ2hlY2tib3gvQ2hlY2tib3guanNcbi8vIG1vZHVsZSBjaHVua3MgPSA2NCIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9DaGVja2JveCA9IHJlcXVpcmUoJy4vQ2hlY2tib3gnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQ2hlY2tib3gpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0NoZWNrYm94L2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9DaGVja2JveC9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDY0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xOSAzSDVjLTEuMSAwLTIgLjktMiAydjE0YzAgMS4xLjkgMiAyIDJoMTRjMS4xIDAgMi0uOSAyLTJWNWMwLTEuMS0uOS0yLTItMnptLTIgMTBIN3YtMmgxMHYyeicgfSk7XG5cbnZhciBJbmRldGVybWluYXRlQ2hlY2tCb3ggPSBmdW5jdGlvbiBJbmRldGVybWluYXRlQ2hlY2tCb3gocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5JbmRldGVybWluYXRlQ2hlY2tCb3ggPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluZGV0ZXJtaW5hdGVDaGVja0JveCk7XG5JbmRldGVybWluYXRlQ2hlY2tCb3gubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5kZXRlcm1pbmF0ZUNoZWNrQm94O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9JbmRldGVybWluYXRlQ2hlY2tCb3guanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9JbmRldGVybWluYXRlQ2hlY2tCb3guanNcbi8vIG1vZHVsZSBjaHVua3MgPSA2NCIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmltcG9ydCBDaGVja2JveCBmcm9tICdtYXRlcmlhbC11aS9DaGVja2JveCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIGZsZXhHcm93OiAxLFxuICB9LFxufSk7XG5cbmNsYXNzIEFuc3dlciBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgdGl0bGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgICBhbnN3ZXI6IFByb3BUeXBlcy5ib29sLFxuICAgIGV4cGxhaW5hdGlvbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgYW5zd2VyOiBmYWxzZSxcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgZGlydHk6IGZhbHNlLFxuICAgICAgYW5zd2VyOiBmYWxzZSxcbiAgICB9O1xuICB9XG5cbiAgb25DaGFuZ2UgPSAoZSkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoe1xuICAgICAgZGlydHk6IHRydWUsXG4gICAgICBhbnN3ZXI6IGUudGFyZ2V0LnZhbHVlLFxuICAgIH0pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnByb3BzKTtcbiAgICBjb25zdCB7IGRpcnR5IH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgdGl0bGUsIGV4cGxhaW5hdGlvbiB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17dGhpcy5wcm9wcy5jbGFzc2VzLnJvb3R9PlxuICAgICAgICA8Q2hlY2tib3ggdmFsdWU9e3RoaXMucHJvcHMuYW5zd2VyfSBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX0gLz5cbiAgICAgICAge3RpdGxlfVxuICAgICAgICA8ZGl2PntkaXJ0eSA/IGV4cGxhaW5hdGlvbiA6IG51bGx9PC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShBbnN3ZXIpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvVGVzdFlvdXJzZWxmL0Fuc3dlci5qcyIsImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgQ2FyZCBmcm9tICdtYXRlcmlhbC11aS9DYXJkL0NhcmQnO1xuaW1wb3J0IENhcmRIZWFkZXIgZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZC9DYXJkSGVhZGVyJztcbmltcG9ydCBDYXJkQ29udGVudCBmcm9tICdtYXRlcmlhbC11aS9DYXJkL0NhcmRDb250ZW50JztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5pbXBvcnQgQW5zd2VyIGZyb20gJy4vQW5zd2VyJztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7XG4gICAgcGFkZGluZzogJzFweCcsXG4gIH0sXG59KTtcblxuY2xhc3MgUXVlc3Rpb24gZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXt0aGlzLnByb3BzLmNsYXNzZXMucm9vdH0+XG4gICAgICAgIDxDYXJkIHJhaXNlZD5cbiAgICAgICAgICA8Q2FyZEhlYWRlciB0aXRsZT17dGhpcy5wcm9wcy50aXRsZX0gLz5cblxuICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgIHt0aGlzLnByb3BzLm9wdGlvbnMubWFwKChhLCBpKSA9PiA8QW5zd2VyIGtleT17aSArIDF9IHsuLi5hfSAvPil9XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoUXVlc3Rpb24pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvVGVzdFlvdXJzZWxmL1F1ZXN0aW9uLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==