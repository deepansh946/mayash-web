webpackJsonp([41],{

/***/ "./node_modules/dom-helpers/activeElement.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/activeElement.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = activeElement;

var _ownerDocument = __webpack_require__(/*! ./ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function activeElement() {
  var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : (0, _ownerDocument2.default)();

  try {
    return doc.activeElement;
  } catch (e) {/* ie throws if no active element */}
}
module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/dom-helpers/ownerDocument.js":
/*!***************************************************!*\
  !*** ./node_modules/dom-helpers/ownerDocument.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ownerDocument;
function ownerDocument(node) {
  return node && node.ownerDocument || document;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/query/isWindow.js":
/*!****************************************************!*\
  !*** ./node_modules/dom-helpers/query/isWindow.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getWindow;
function getWindow(node) {
  return node === node.window ? node : node.nodeType === 9 ? node.defaultView || node.parentWindow : false;
}
module.exports = exports["default"];

/***/ }),

/***/ "./node_modules/dom-helpers/util/scrollbarSize.js":
/*!********************************************************!*\
  !*** ./node_modules/dom-helpers/util/scrollbarSize.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (recalc) {
  if (!size && size !== 0 || recalc) {
    if (_inDOM2.default) {
      var scrollDiv = document.createElement('div');

      scrollDiv.style.position = 'absolute';
      scrollDiv.style.top = '-9999px';
      scrollDiv.style.width = '50px';
      scrollDiv.style.height = '50px';
      scrollDiv.style.overflow = 'scroll';

      document.body.appendChild(scrollDiv);
      size = scrollDiv.offsetWidth - scrollDiv.clientWidth;
      document.body.removeChild(scrollDiv);
    }
  }

  return size;
};

var _inDOM = __webpack_require__(/*! ./inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var size = void 0;

module.exports = exports['default'];

/***/ }),

/***/ "./node_modules/material-ui-icons/Comment.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui-icons/Comment.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21.99 4c0-1.1-.89-2-1.99-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4-.01-18zM18 14H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var Comment = function Comment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Comment = (0, _pure2.default)(Comment);
Comment.muiName = 'SvgIcon';

exports.default = Comment;

/***/ }),

/***/ "./node_modules/material-ui-icons/Delete.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui-icons/Delete.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z' });

var Delete = function Delete(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Delete = (0, _pure2.default)(Delete);
Delete.muiName = 'SvgIcon';

exports.default = Delete;

/***/ }),

/***/ "./node_modules/material-ui-icons/Favorite.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/Favorite.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z' });

var Favorite = function Favorite(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Favorite = (0, _pure2.default)(Favorite);
Favorite.muiName = 'SvgIcon';

exports.default = Favorite;

/***/ }),

/***/ "./node_modules/material-ui-icons/MoreVert.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/MoreVert.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' });

var MoreVert = function MoreVert(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

MoreVert = (0, _pure2.default)(MoreVert);
MoreVert.muiName = 'SvgIcon';

exports.default = MoreVert;

/***/ }),

/***/ "./node_modules/material-ui-icons/RemoveRedEye.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/RemoveRedEye.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z' });

var RemoveRedEye = function RemoveRedEye(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

RemoveRedEye = (0, _pure2.default)(RemoveRedEye);
RemoveRedEye.muiName = 'SvgIcon';

exports.default = RemoveRedEye;

/***/ }),

/***/ "./node_modules/material-ui-icons/Share.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Share.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 16.08c-.76 0-1.44.3-1.96.77L8.91 12.7c.05-.23.09-.46.09-.7s-.04-.47-.09-.7l7.05-4.11c.54.5 1.25.81 2.04.81 1.66 0 3-1.34 3-3s-1.34-3-3-3-3 1.34-3 3c0 .24.04.47.09.7L8.04 9.81C7.5 9.31 6.79 9 6 9c-1.66 0-3 1.34-3 3s1.34 3 3 3c.79 0 1.5-.31 2.04-.81l7.12 4.16c-.05.21-.08.43-.08.65 0 1.61 1.31 2.92 2.92 2.92 1.61 0 2.92-1.31 2.92-2.92s-1.31-2.92-2.92-2.92z' });

var Share = function Share(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Share = (0, _pure2.default)(Share);
Share.muiName = 'SvgIcon';

exports.default = Share;

/***/ }),

/***/ "./node_modules/material-ui-icons/TurnedIn.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui-icons/TurnedIn.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H7c-1.1 0-1.99.9-1.99 2L5 21l7-3 7 3V5c0-1.1-.9-2-2-2z' });

var TurnedIn = function TurnedIn(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

TurnedIn = (0, _pure2.default)(TurnedIn);
TurnedIn.muiName = 'SvgIcon';

exports.default = TurnedIn;

/***/ }),

/***/ "./node_modules/material-ui/Badge/Badge.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/Badge.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak

var RADIUS = 12;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'inline-flex'
    },
    badge: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      top: -RADIUS,
      right: -RADIUS,
      fontFamily: theme.typography.fontFamily,
      fontWeight: theme.typography.fontWeight,
      fontSize: theme.typography.pxToRem(RADIUS),
      width: RADIUS * 2,
      height: RADIUS * 2,
      borderRadius: '50%',
      backgroundColor: theme.palette.color,
      color: theme.palette.textColor,
      zIndex: 1 // Render the badge on top of potential ripples.
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content rendered within the badge.
   */
  badgeContent: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * The badge will be added relative to this node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'primary', 'accent']).isRequired
};

var Badge = function (_React$Component) {
  (0, _inherits3.default)(Badge, _React$Component);

  function Badge() {
    (0, _classCallCheck3.default)(this, Badge);
    return (0, _possibleConstructorReturn3.default)(this, (Badge.__proto__ || (0, _getPrototypeOf2.default)(Badge)).apply(this, arguments));
  }

  (0, _createClass3.default)(Badge, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          badgeContent = _props.badgeContent,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          children = _props.children,
          other = (0, _objectWithoutProperties3.default)(_props, ['badgeContent', 'classes', 'className', 'color', 'children']);

      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var badgeClassName = (0, _classnames2.default)(classes.badge, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'));

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        children,
        _react2.default.createElement(
          'span',
          { className: badgeClassName },
          badgeContent
        )
      );
    }
  }]);
  return Badge;
}(_react2.default.Component);

Badge.defaultProps = {
  color: 'default'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBadge' })(Badge);

/***/ }),

/***/ "./node_modules/material-ui/Badge/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Badge/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Badge = __webpack_require__(/*! ./Badge */ "./node_modules/material-ui/Badge/Badge.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Badge).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Dialog/Dialog.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Dialog/Dialog.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Modal = __webpack_require__(/*! ../Modal */ "./node_modules/material-ui/Modal/index.js");

var _Modal2 = _interopRequireDefault(_Modal);

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Modal

var babelPluginFlowReactPropTypes_proptype_ComponentType = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      justifyContent: 'center',
      alignItems: 'center'
    },
    paper: {
      display: 'flex',
      margin: theme.spacing.unit * 4,
      flexDirection: 'column',
      flex: '0 1 auto',
      position: 'relative',
      maxHeight: '90vh',
      overflowY: 'auto', // Fix IE11 issue, to remove at some point.
      '&:focus': {
        outline: 'none'
      }
    },
    paperWidthXs: {
      maxWidth: Math.max(theme.breakpoints.values.xs, 360)
    },
    paperWidthSm: {
      maxWidth: theme.breakpoints.values.sm
    },
    paperWidthMd: {
      maxWidth: theme.breakpoints.values.md
    },
    fullWidth: {
      width: '100%'
    },
    fullScreen: {
      margin: 0,
      width: '100%',
      maxWidth: '100%',
      height: '100%',
      maxHeight: '100%',
      borderRadius: 0
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Dialog children, usually the included sub-components.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, it will be full-screen
   */
  fullScreen: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  transitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * Determine the max width of the dialog.
   * The dialog width grows with the size of the screen, this property is useful
   * on the desktop where you might need some coherent different width size across your
   * application.
   */
  maxWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['xs', 'sm', 'md']).isRequired,

  /**
   * If specified, stretches dialog to max width.
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Callback fired when the backdrop is clicked.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the dialog enters.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is released and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the dialog exits.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the dialog has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Dialog is open.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Transition component.
   */
  transition: typeof babelPluginFlowReactPropTypes_proptype_ComponentType === 'function' ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired : babelPluginFlowReactPropTypes_proptype_ComponentType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ComponentType).isRequired
};

/**
 * Dialogs are overlaid modal paper based components with a backdrop.
 */
var Dialog = function (_React$Component) {
  (0, _inherits3.default)(Dialog, _React$Component);

  function Dialog() {
    (0, _classCallCheck3.default)(this, Dialog);
    return (0, _possibleConstructorReturn3.default)(this, (Dialog.__proto__ || (0, _getPrototypeOf2.default)(Dialog)).apply(this, arguments));
  }

  (0, _createClass3.default)(Dialog, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          fullScreen = _props.fullScreen,
          ignoreBackdropClick = _props.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props.ignoreEscapeKeyUp,
          transitionDuration = _props.transitionDuration,
          maxWidth = _props.maxWidth,
          fullWidth = _props.fullWidth,
          open = _props.open,
          onBackdropClick = _props.onBackdropClick,
          onEscapeKeyUp = _props.onEscapeKeyUp,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onEntered = _props.onEntered,
          onExit = _props.onExit,
          onExiting = _props.onExiting,
          onExited = _props.onExited,
          onRequestClose = _props.onRequestClose,
          TransitionProp = _props.transition,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'fullScreen', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'transitionDuration', 'maxWidth', 'fullWidth', 'open', 'onBackdropClick', 'onEscapeKeyUp', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'onRequestClose', 'transition']);


      return _react2.default.createElement(
        _Modal2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, className),
          BackdropTransitionDuration: transitionDuration,
          ignoreBackdropClick: ignoreBackdropClick,
          ignoreEscapeKeyUp: ignoreEscapeKeyUp,
          onBackdropClick: onBackdropClick,
          onEscapeKeyUp: onEscapeKeyUp,
          onRequestClose: onRequestClose,
          show: open
        }, other),
        _react2.default.createElement(
          TransitionProp,
          {
            appear: true,
            'in': open,
            timeout: transitionDuration,
            onEnter: onEnter,
            onEntering: onEntering,
            onEntered: onEntered,
            onExit: onExit,
            onExiting: onExiting,
            onExited: onExited
          },
          _react2.default.createElement(
            _Paper2.default,
            {
              elevation: 24,
              className: (0, _classnames2.default)(classes.paper, classes['paperWidth' + (0, _helpers.capitalizeFirstLetter)(maxWidth)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes.fullScreen, fullScreen), (0, _defineProperty3.default)(_classNames, classes.fullWidth, fullWidth), _classNames))
            },
            children
          )
        )
      );
    }
  }]);
  return Dialog;
}(_react2.default.Component);

Dialog.defaultProps = {
  fullScreen: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  transitionDuration: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  },
  maxWidth: 'sm',
  fullWidth: false,
  open: false,
  transition: _Fade2.default
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialog' })(Dialog);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogActions.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogActions.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

__webpack_require__(/*! ../Button */ "./node_modules/material-ui/Button/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// So we don't have any override priority issue.

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      margin: theme.spacing.unit + 'px ' + theme.spacing.unit / 2 + 'px',
      flex: '0 0 auto'
    },
    action: {
      margin: '0 ' + theme.spacing.unit / 2 + 'px'
    },
    button: {
      minWidth: 64
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogActions(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.createElement(
    'div',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    _react2.default.Children.map(children, function (child) {
      if (!_react2.default.isValidElement(child)) {
        return null;
      }

      return _react2.default.createElement(
        'div',
        { className: classes.action },
        _react2.default.cloneElement(child, {
          className: (0, _classnames2.default)(classes.button, child.props.className)
        })
      );
    })
  );
}

DialogActions.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * @ignore
   */
  theme: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogActions' })(DialogActions);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogContent.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogContent.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  var spacing = theme.spacing.unit * 3;
  return {
    root: {
      flex: '1 1 auto',
      overflowY: 'auto',
      padding: '0 ' + spacing + 'px ' + spacing + 'px ' + spacing + 'px',
      '&:first-child': {
        paddingTop: spacing
      }
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogContent(props) {
  var classes = props.classes,
      children = props.children,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['classes', 'children', 'className']);


  return _react2.default.createElement(
    'div',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    children
  );
}

DialogContent.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogContent' })(DialogContent);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogContentText.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogContentText.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _ref;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends3.default)({}, theme.typography.subheading, {
      color: theme.palette.text.secondary,
      margin: 0
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};


function DialogContentText(props) {
  var children = props.children,
      classes = props.classes,
      className = props.className,
      other = (0, _objectWithoutProperties3.default)(props, ['children', 'classes', 'className']);


  return _react2.default.createElement(
    'p',
    (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
    children
  );
}

DialogContentText.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * @ignore
   */
  theme: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node)
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogContentText' })(DialogContentText);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/DialogTitle.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Dialog/DialogTitle.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! ../Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      margin: 0,
      padding: theme.spacing.unit * 3 + 'px ' + theme.spacing.unit * 3 + 'px       20px ' + theme.spacing.unit * 3 + 'px',
      flex: '0 0 auto'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the children won't be wrapped by a typography component.
   * For instance, this can be useful to render an h4 instead of the default h2.
   */
  disableTypography: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

var DialogTitle = function (_React$Component) {
  (0, _inherits3.default)(DialogTitle, _React$Component);

  function DialogTitle() {
    (0, _classCallCheck3.default)(this, DialogTitle);
    return (0, _possibleConstructorReturn3.default)(this, (DialogTitle.__proto__ || (0, _getPrototypeOf2.default)(DialogTitle)).apply(this, arguments));
  }

  (0, _createClass3.default)(DialogTitle, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          disableTypography = _props.disableTypography,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'disableTypography']);


      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: (0, _classnames2.default)(classes.root, className) }, other),
        disableTypography ? children : _react2.default.createElement(
          _Typography2.default,
          { type: 'title' },
          children
        )
      );
    }
  }]);
  return DialogTitle;
}(_react2.default.Component);

DialogTitle.defaultProps = {
  disableTypography: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiDialogTitle' })(DialogTitle);

/***/ }),

/***/ "./node_modules/material-ui/Dialog/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Dialog/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Dialog = __webpack_require__(/*! ./Dialog */ "./node_modules/material-ui/Dialog/Dialog.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Dialog).default;
  }
});

var _DialogActions = __webpack_require__(/*! ./DialogActions */ "./node_modules/material-ui/Dialog/DialogActions.js");

Object.defineProperty(exports, 'DialogActions', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogActions).default;
  }
});

var _DialogTitle = __webpack_require__(/*! ./DialogTitle */ "./node_modules/material-ui/Dialog/DialogTitle.js");

Object.defineProperty(exports, 'DialogTitle', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogTitle).default;
  }
});

var _DialogContent = __webpack_require__(/*! ./DialogContent */ "./node_modules/material-ui/Dialog/DialogContent.js");

Object.defineProperty(exports, 'DialogContent', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogContent).default;
  }
});

var _DialogContentText = __webpack_require__(/*! ./DialogContentText */ "./node_modules/material-ui/Dialog/DialogContentText.js");

Object.defineProperty(exports, 'DialogContentText', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_DialogContentText).default;
  }
});

var _withMobileDialog = __webpack_require__(/*! ./withMobileDialog */ "./node_modules/material-ui/Dialog/withMobileDialog.js");

Object.defineProperty(exports, 'withMobileDialog', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_withMobileDialog).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Dialog/withMobileDialog.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui/Dialog/withMobileDialog.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _wrapDisplayName = __webpack_require__(/*! recompose/wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _withWidth = __webpack_require__(/*! ../utils/withWidth */ "./node_modules/material-ui/utils/withWidth.js");

var _withWidth2 = _interopRequireDefault(_withWidth);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_HigherOrderComponent = __webpack_require__(/*! react-flow-types */ "./node_modules/react-flow-types/index.js").babelPluginFlowReactPropTypes_proptype_HigherOrderComponent || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Breakpoint = __webpack_require__(/*! ../styles/createBreakpoints */ "./node_modules/material-ui/styles/createBreakpoints.js").babelPluginFlowReactPropTypes_proptype_Breakpoint || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_InjectedProps = {
  /**
   * If isWidthDown(options.breakpoint), return true.
   */
  fullScreen: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};


/**
 * Dialog will responsively be full screen *at or below* the given breakpoint
 * (defaults to 'sm' for mobile devices).
 * Notice that this Higher-order Component is incompatible with server side rendering.
 */
var withMobileDialog = function withMobileDialog() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { breakpoint: 'sm' };
  return function (Component) {
    var breakpoint = options.breakpoint;


    function WithMobileDialog(props) {
      return _react2.default.createElement(Component, (0, _extends3.default)({ fullScreen: (0, _withWidth.isWidthDown)(breakpoint, props.width) }, props));
    }

    WithMobileDialog.propTypes =  true ? {
      width: typeof babelPluginFlowReactPropTypes_proptype_Breakpoint === 'function' ? babelPluginFlowReactPropTypes_proptype_Breakpoint.isRequired ? babelPluginFlowReactPropTypes_proptype_Breakpoint.isRequired : babelPluginFlowReactPropTypes_proptype_Breakpoint : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Breakpoint).isRequired
    } : {};
    if (true) {
      WithMobileDialog.displayName = (0, _wrapDisplayName2.default)(Component, 'withMobileDialog');
    }

    return (0, _withWidth2.default)()(WithMobileDialog);
  };
};

exports.default = withMobileDialog;

/***/ }),

/***/ "./node_modules/material-ui/Modal/Backdrop.js":
/*!****************************************************!*\
  !*** ./node_modules/material-ui/Modal/Backdrop.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      zIndex: -1,
      width: '100%',
      height: '100%',
      position: 'fixed',
      top: 0,
      left: 0,
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      backgroundColor: theme.palette.common.lightBlack,
      transition: theme.transitions.create('opacity'),
      willChange: 'opacity',
      opacity: 0
    },
    invisible: {
      backgroundColor: theme.palette.common.transparent
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Can be used, for instance, to render a letter inside the avatar.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the backdrop is invisible.
   */
  invisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * @ignore - internal component.
 */
var Backdrop = function (_React$Component) {
  (0, _inherits3.default)(Backdrop, _React$Component);

  function Backdrop() {
    (0, _classCallCheck3.default)(this, Backdrop);
    return (0, _possibleConstructorReturn3.default)(this, (Backdrop.__proto__ || (0, _getPrototypeOf2.default)(Backdrop)).apply(this, arguments));
  }

  (0, _createClass3.default)(Backdrop, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          invisible = _props.invisible,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'invisible']);


      var backdropClass = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.invisible, invisible), className);

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: backdropClass, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Backdrop;
}(_react2.default.Component);

Backdrop.defaultProps = {
  invisible: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiBackdrop' })(Backdrop);

/***/ }),

/***/ "./node_modules/material-ui/Modal/Modal.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/Modal.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _contains = __webpack_require__(/*! dom-helpers/query/contains */ "./node_modules/dom-helpers/query/contains.js");

var _contains2 = _interopRequireDefault(_contains);

var _activeElement = __webpack_require__(/*! dom-helpers/activeElement */ "./node_modules/dom-helpers/activeElement.js");

var _activeElement2 = _interopRequireDefault(_activeElement);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _addEventListener = __webpack_require__(/*! ../utils/addEventListener */ "./node_modules/material-ui/utils/addEventListener.js");

var _addEventListener2 = _interopRequireDefault(_addEventListener);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Fade = __webpack_require__(/*! ../transitions/Fade */ "./node_modules/material-ui/transitions/Fade.js");

var _Fade2 = _interopRequireDefault(_Fade);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _modalManager = __webpack_require__(/*! ./modalManager */ "./node_modules/material-ui/Modal/modalManager.js");

var _modalManager2 = _interopRequireDefault(_modalManager);

var _Backdrop = __webpack_require__(/*! ./Backdrop */ "./node_modules/material-ui/Modal/Backdrop.js");

var _Backdrop2 = _interopRequireDefault(_Backdrop);

var _Portal = __webpack_require__(/*! ../internal/Portal */ "./node_modules/material-ui/internal/Portal.js");

var _Portal2 = _interopRequireDefault(_Portal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

// Modals don't open on the server so this won't break concurrency.
// Could also put this on context.
var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var modalManager = (0, _modalManager2.default)();

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      width: '100%',
      height: '100%',
      position: 'fixed',
      zIndex: theme.zIndex.dialog,
      top: 0,
      left: 0
    },
    hidden: {
      visibility: 'hidden'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the backdrop element.
   */
  BackdropClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Pass a component class to use as the backdrop.
   */
  BackdropComponent: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * If `true`, the backdrop is invisible.
   */
  BackdropInvisible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The duration for the backdrop transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  BackdropTransitionDuration: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Always keep the children in the DOM.
   * This property can be useful in SEO situation or
   * when you want to maximize the responsiveness of the Modal.
   */
  keepMounted: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the backdrop is disabled.
   */
  disableBackdrop: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, clicking the backdrop will not fire the `onRequestClose` callback.
   */
  ignoreBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, hitting escape will not fire the `onRequestClose` callback.
   */
  ignoreEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  modalManager: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired,

  /**
   * Callback fires when the backdrop is clicked on.
   */
  onBackdropClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is entering.
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is entering.
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has entered.
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fires when the escape key is pressed and the modal is in focus.
   */
  onEscapeKeyUp: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback fired before the modal is exiting.
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal is exiting.
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the modal has exited.
   */
  onExited: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * Callback fired when the component requests to be closed.
   *
   * @param {object} event The event source of the callback
   */
  onRequestClose: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * If `true`, the Modal is visible.
   */
  show: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired
};

/**
 * The modal component provides a solid foundation for creating dialogs,
 * popovers, or whatever else.
 * The component renders its `children` node in front of a backdrop component.
 *
 * The `Modal` offers a few helpful features over using just a `Portal` component and some styles:
 * - Manages dialog stacking when one-at-a-time just isn't enough.
 * - Creates a backdrop, for disabling interaction below the modal.
 * - It properly manages focus; moving to the modal content,
 *   and keeping it there until the modal is closed.
 * - It disables scrolling of the page content while open.
 * - Adds the appropriate ARIA roles are automatically.
 *
 * This component shares many concepts with [react-overlays](https://react-bootstrap.github.io/react-overlays/#modals).
 */
var Modal = function (_React$Component) {
  (0, _inherits3.default)(Modal, _React$Component);

  function Modal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Modal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Modal.__proto__ || (0, _getPrototypeOf2.default)(Modal)).call.apply(_ref, [this].concat(args))), _this), _initialiseProps.call(_this), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Modal, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      if (!this.props.show) {
        this.setState({ exited: true });
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.mounted = true;
      if (this.props.show) {
        this.handleShow();
      }
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      if (nextProps.show && this.state.exited) {
        this.setState({ exited: false });
      }
    }
  }, {
    key: 'componentWillUpdate',
    value: function componentWillUpdate(nextProps) {
      if (!this.props.show && nextProps.show) {
        this.checkForFocus();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (!prevProps.show && this.props.show) {
        this.handleShow();
      }
      // We are waiting for the onExited callback to call handleHide.
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      if (this.props.show || !this.state.exited) {
        this.handleHide();
      }
      this.mounted = false;
    }
  }, {
    key: 'checkForFocus',
    value: function checkForFocus() {
      if (_inDOM2.default) {
        this.lastFocus = (0, _activeElement2.default)();
      }
    }
  }, {
    key: 'restoreLastFocus',
    value: function restoreLastFocus() {
      if (this.lastFocus && this.lastFocus.focus) {
        this.lastFocus.focus();
        this.lastFocus = undefined;
      }
    }
  }, {
    key: 'handleShow',
    value: function handleShow() {
      var doc = (0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this));
      this.props.modalManager.add(this);
      this.onDocumentKeyUpListener = (0, _addEventListener2.default)(doc, 'keyup', this.handleDocumentKeyUp);
      this.onFocusListener = (0, _addEventListener2.default)(doc, 'focus', this.handleFocusListener, true);
      this.focus();
    }
  }, {
    key: 'focus',
    value: function focus() {
      var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(this)));
      var modalContent = this.modal && this.modal.lastChild;
      var focusInModal = currentFocus && (0, _contains2.default)(modalContent, currentFocus);

      if (modalContent && !focusInModal) {
        if (!modalContent.hasAttribute('tabIndex')) {
          modalContent.setAttribute('tabIndex', -1);
           true ? (0, _warning2.default)(false, 'Material-UI: the modal content node does not accept focus. ' + 'For the benefit of assistive technologies, ' + 'the tabIndex of the node is being set to "-1".') : void 0;
        }

        modalContent.focus();
      }
    }
  }, {
    key: 'handleHide',
    value: function handleHide() {
      this.props.modalManager.remove(this);
      if (this.onDocumentKeyUpListener) this.onDocumentKeyUpListener.remove();
      if (this.onFocusListener) this.onFocusListener.remove();
      this.restoreLastFocus();
    }
  }, {
    key: 'renderBackdrop',
    value: function renderBackdrop() {
      var other = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var _props = this.props,
          BackdropComponent = _props.BackdropComponent,
          BackdropClassName = _props.BackdropClassName,
          BackdropTransitionDuration = _props.BackdropTransitionDuration,
          BackdropInvisible = _props.BackdropInvisible,
          show = _props.show;


      return _react2.default.createElement(
        _Fade2.default,
        (0, _extends3.default)({ appear: true, 'in': show, timeout: BackdropTransitionDuration }, other),
        _react2.default.createElement(BackdropComponent, {
          invisible: BackdropInvisible,
          className: BackdropClassName,
          onClick: this.handleBackdropClick
        })
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          disableBackdrop = _props2.disableBackdrop,
          BackdropComponent = _props2.BackdropComponent,
          BackdropClassName = _props2.BackdropClassName,
          BackdropTransitionDuration = _props2.BackdropTransitionDuration,
          BackdropInvisible = _props2.BackdropInvisible,
          ignoreBackdropClick = _props2.ignoreBackdropClick,
          ignoreEscapeKeyUp = _props2.ignoreEscapeKeyUp,
          children = _props2.children,
          classes = _props2.classes,
          className = _props2.className,
          keepMounted = _props2.keepMounted,
          modalManagerProp = _props2.modalManager,
          onBackdropClick = _props2.onBackdropClick,
          onEscapeKeyUp = _props2.onEscapeKeyUp,
          onRequestClose = _props2.onRequestClose,
          onEnter = _props2.onEnter,
          onEntering = _props2.onEntering,
          onEntered = _props2.onEntered,
          onExit = _props2.onExit,
          onExiting = _props2.onExiting,
          onExited = _props2.onExited,
          show = _props2.show,
          other = (0, _objectWithoutProperties3.default)(_props2, ['disableBackdrop', 'BackdropComponent', 'BackdropClassName', 'BackdropTransitionDuration', 'BackdropInvisible', 'ignoreBackdropClick', 'ignoreEscapeKeyUp', 'children', 'classes', 'className', 'keepMounted', 'modalManager', 'onBackdropClick', 'onEscapeKeyUp', 'onRequestClose', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'onExited', 'show']);


      if (!keepMounted && !show && this.state.exited) {
        return null;
      }

      var transitionCallbacks = {
        onEnter: onEnter,
        onEntering: onEntering,
        onEntered: onEntered,
        onExit: onExit,
        onExiting: onExiting,
        onExited: this.handleTransitionExited
      };

      var modalChild = _react2.default.Children.only(children);
      var _modalChild$props = modalChild.props,
          role = _modalChild$props.role,
          tabIndex = _modalChild$props.tabIndex;

      var childProps = {};

      if (role === undefined) {
        childProps.role = role === undefined ? 'document' : role;
      }

      if (tabIndex === undefined) {
        childProps.tabIndex = tabIndex == null ? -1 : tabIndex;
      }

      var backdropProps = void 0;

      // It's a Transition like component
      if (modalChild.props.hasOwnProperty('in')) {
        (0, _keys2.default)(transitionCallbacks).forEach(function (key) {
          childProps[key] = (0, _helpers.createChainedFunction)(transitionCallbacks[key], modalChild.props[key]);
        });
      } else {
        backdropProps = transitionCallbacks;
      }

      if ((0, _keys2.default)(childProps).length) {
        modalChild = _react2.default.cloneElement(modalChild, childProps);
      }

      return _react2.default.createElement(
        _Portal2.default,
        {
          open: true,
          ref: function ref(node) {
            _this2.mountNode = node ? node.getLayer() : null;
          }
        },
        _react2.default.createElement(
          'div',
          (0, _extends3.default)({
            className: (0, _classnames2.default)(classes.root, className, (0, _defineProperty3.default)({}, classes.hidden, this.state.exited))
          }, other, {
            ref: function ref(node) {
              _this2.modal = node;
            }
          }),
          !disableBackdrop && (!keepMounted || show || !this.state.exited) && this.renderBackdrop(backdropProps),
          modalChild
        )
      );
    }
  }]);
  return Modal;
}(_react2.default.Component);

Modal.defaultProps = {
  BackdropComponent: _Backdrop2.default,
  BackdropTransitionDuration: 300,
  BackdropInvisible: false,
  keepMounted: false,
  disableBackdrop: false,
  ignoreBackdropClick: false,
  ignoreEscapeKeyUp: false,
  modalManager: modalManager
};

var _initialiseProps = function _initialiseProps() {
  var _this3 = this;

  this.state = {
    exited: false
  };
  this.onDocumentKeyUpListener = null;
  this.onFocusListener = null;
  this.mounted = false;
  this.lastFocus = undefined;
  this.modal = null;
  this.mountNode = null;

  this.handleFocusListener = function () {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    var currentFocus = (0, _activeElement2.default)((0, _ownerDocument2.default)(_reactDom2.default.findDOMNode(_this3)));
    var modalContent = _this3.modal && _this3.modal.lastChild;

    if (modalContent && modalContent !== currentFocus && !(0, _contains2.default)(modalContent, currentFocus)) {
      modalContent.focus();
    }
  };

  this.handleDocumentKeyUp = function (event) {
    if (!_this3.mounted || !_this3.props.modalManager.isTopModal(_this3)) {
      return;
    }

    if ((0, _keycode2.default)(event) !== 'esc') {
      return;
    }

    var _props3 = _this3.props,
        onEscapeKeyUp = _props3.onEscapeKeyUp,
        onRequestClose = _props3.onRequestClose,
        ignoreEscapeKeyUp = _props3.ignoreEscapeKeyUp;


    if (onEscapeKeyUp) {
      onEscapeKeyUp(event);
    }

    if (onRequestClose && !ignoreEscapeKeyUp) {
      onRequestClose(event);
    }
  };

  this.handleBackdropClick = function (event) {
    if (event.target !== event.currentTarget) {
      return;
    }

    var _props4 = _this3.props,
        onBackdropClick = _props4.onBackdropClick,
        onRequestClose = _props4.onRequestClose,
        ignoreBackdropClick = _props4.ignoreBackdropClick;


    if (onBackdropClick) {
      onBackdropClick(event);
    }

    if (onRequestClose && !ignoreBackdropClick) {
      onRequestClose(event);
    }
  };

  this.handleTransitionExited = function () {
    if (_this3.props.onExited) {
      var _props5;

      (_props5 = _this3.props).onExited.apply(_props5, arguments);
    }

    _this3.setState({ exited: true });
    _this3.handleHide();
  };
};

exports.default = (0, _withStyles2.default)(styles, { flip: false, name: 'MuiModal' })(Modal);

/***/ }),

/***/ "./node_modules/material-ui/Modal/index.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui/Modal/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Modal = __webpack_require__(/*! ./Modal */ "./node_modules/material-ui/Modal/Modal.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Modal).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Modal/modalManager.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui/Modal/modalManager.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _isWindow = __webpack_require__(/*! dom-helpers/query/isWindow */ "./node_modules/dom-helpers/query/isWindow.js");

var _isWindow2 = _interopRequireDefault(_isWindow);

var _ownerDocument = __webpack_require__(/*! dom-helpers/ownerDocument */ "./node_modules/dom-helpers/ownerDocument.js");

var _ownerDocument2 = _interopRequireDefault(_ownerDocument);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

var _scrollbarSize = __webpack_require__(/*! dom-helpers/util/scrollbarSize */ "./node_modules/dom-helpers/util/scrollbarSize.js");

var _scrollbarSize2 = _interopRequireDefault(_scrollbarSize);

var _manageAriaHidden = __webpack_require__(/*! ../utils/manageAriaHidden */ "./node_modules/material-ui/utils/manageAriaHidden.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Taken from https://github.com/react-bootstrap/react-overlays/blob/master/src/ModalManager.js

function getPaddingRight(node) {
  return parseInt(node.style.paddingRight || 0, 10);
}

// Do we have a scroll bar?
function bodyIsOverflowing(node) {
  var doc = (0, _ownerDocument2.default)(node);
  var win = (0, _isWindow2.default)(doc);

  // Takes in account potential non zero margin on the body.
  var style = window.getComputedStyle(doc.body);
  var marginLeft = parseInt(style.getPropertyValue('margin-left'), 10);
  var marginRight = parseInt(style.getPropertyValue('margin-right'), 10);

  return marginLeft + doc.body.clientWidth + marginRight < win.innerWidth;
}

function getContainer() {
  var container = _inDOM2.default ? window.document.body : {};
   true ? (0, _warning2.default)(container !== null, '\nMaterial-UI: you are most likely evaluating the code before the\nbrowser has a chance to reach the <body>.\nPlease move the import at the end of the <body>.\n  ') : void 0;
  return container;
}
/**
 * State management helper for modals/layers.
 * Simplified, but inspired by react-overlay's ModalManager class
 *
 * @internal Used by the Modal to ensure proper focus management.
 */
function createModalManager() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$hideSiblingNodes = _ref.hideSiblingNodes,
      hideSiblingNodes = _ref$hideSiblingNodes === undefined ? true : _ref$hideSiblingNodes;

  var modals = [];

  var prevOverflow = void 0;
  var prevPaddings = [];

  function add(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx !== -1) {
      return modalIdx;
    }

    modalIdx = modals.length;
    modals.push(modal);

    if (hideSiblingNodes) {
      (0, _manageAriaHidden.hideSiblings)(container, modal.mountNode);
    }

    if (modals.length === 1) {
      // Save our current overflow so we can revert
      // back to it when all modals are closed!
      prevOverflow = container.style.overflow;

      if (bodyIsOverflowing(container)) {
        prevPaddings = [getPaddingRight(container)];
        var scrollbarSize = (0, _scrollbarSize2.default)();
        container.style.paddingRight = prevPaddings[0] + scrollbarSize + 'px';

        var fixedNodes = document.querySelectorAll('.mui-fixed');
        for (var i = 0; i < fixedNodes.length; i += 1) {
          var paddingRight = getPaddingRight(fixedNodes[i]);
          prevPaddings.push(paddingRight);
          fixedNodes[i].style.paddingRight = paddingRight + scrollbarSize + 'px';
        }
      }

      container.style.overflow = 'hidden';
    }

    return modalIdx;
  }

  function remove(modal) {
    var container = getContainer();
    var modalIdx = modals.indexOf(modal);

    if (modalIdx === -1) {
      return modalIdx;
    }

    modals.splice(modalIdx, 1);

    if (modals.length === 0) {
      container.style.overflow = prevOverflow;
      container.style.paddingRight = prevPaddings[0];

      var fixedNodes = document.querySelectorAll('.mui-fixed');
      for (var i = 0; i < fixedNodes.length; i += 1) {
        fixedNodes[i].style.paddingRight = prevPaddings[i + 1] + 'px';
      }

      prevOverflow = undefined;
      prevPaddings = [];
      if (hideSiblingNodes) {
        (0, _manageAriaHidden.showSiblings)(container, modal.mountNode);
      }
    } else if (hideSiblingNodes) {
      // otherwise make sure the next top modal is visible to a SR
      (0, _manageAriaHidden.ariaHidden)(false, modals[modals.length - 1].mountNode);
    }

    return modalIdx;
  }

  function isTopModal(modal) {
    return !!modals.length && modals[modals.length - 1] === modal;
  }

  var modalManager = { add: add, remove: remove, isTopModal: isTopModal };

  return modalManager;
}

exports.default = createModalManager;

/***/ }),

/***/ "./node_modules/material-ui/internal/Portal.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/internal/Portal.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _reactDom = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var _reactDom2 = _interopRequireDefault(_reactDom);

var _inDOM = __webpack_require__(/*! dom-helpers/util/inDOM */ "./node_modules/dom-helpers/util/inDOM.js");

var _inDOM2 = _interopRequireDefault(_inDOM);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content to portal in order to escape the parent DOM node.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * If `true` the children will be mounted into the DOM.
   */
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

/**
 * @ignore - internal component.
 */
var Portal = function (_React$Component) {
  (0, _inherits3.default)(Portal, _React$Component);

  function Portal() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Portal);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Portal.__proto__ || (0, _getPrototypeOf2.default)(Portal)).call.apply(_ref, [this].concat(args))), _this), _this.layer = null, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Portal, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {
      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        this.renderLayer();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.unrenderLayer();
    }
  }, {
    key: 'getLayer',
    value: function getLayer() {
      if (!this.layer) {
        this.layer = document.createElement('div');
        this.layer.setAttribute('data-mui-portal', 'true');
        if (document.body && this.layer) {
          document.body.appendChild(this.layer);
        }
      }

      return this.layer;
    }
  }, {
    key: 'unrenderLayer',
    value: function unrenderLayer() {
      if (!this.layer) {
        return;
      }

      // Support react@15.x, will be removed at some point
      if (!_reactDom2.default.createPortal) {
        _reactDom2.default.unmountComponentAtNode(this.layer);
      }

      if (document.body) {
        document.body.removeChild(this.layer);
      }
      this.layer = null;
    }
  }, {
    key: 'renderLayer',
    value: function renderLayer() {
      var _props = this.props,
          children = _props.children,
          open = _props.open;


      if (open) {
        // By calling this method in componentDidMount() and
        // componentDidUpdate(), you're effectively creating a "wormhole" that
        // funnels React's hierarchical updates through to a DOM node on an
        // entirely different part of the page.
        var layerElement = _react2.default.Children.only(children);
        _reactDom2.default.unstable_renderSubtreeIntoContainer(this, layerElement, this.getLayer());
      } else {
        this.unrenderLayer();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          children = _props2.children,
          open = _props2.open;

      // Support react@15.x, will be removed at some point

      if (!_reactDom2.default.createPortal) {
        return null;
      }

      // Can't be rendered server-side.
      if (_inDOM2.default) {
        if (open) {
          var layer = this.getLayer();
          // $FlowFixMe layer is non-null
          return _reactDom2.default.createPortal(children, layer);
        }

        this.unrenderLayer();
      }

      return null;
    }
  }]);
  return Portal;
}(_react2.default.Component);

Portal.defaultProps = {
  open: false
};
Portal.propTypes =  true ? {
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),
  open: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
} : {};
exports.default = Portal;

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Fade.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/transitions/Fade.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

var _withTheme = __webpack_require__(/*! ../styles/withTheme */ "./node_modules/material-ui/styles/withTheme.js");

var _withTheme2 = _interopRequireDefault(_withTheme);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionDuration || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * A single child content element.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element.isRequired ? babelPluginFlowReactPropTypes_proptype_Element.isRequired : babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element).isRequired,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   */
  timeout: typeof babelPluginFlowReactPropTypes_proptype_TransitionDuration === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired ? babelPluginFlowReactPropTypes_proptype_TransitionDuration.isRequired : babelPluginFlowReactPropTypes_proptype_TransitionDuration : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionDuration).isRequired
};


var reflow = function reflow(node) {
  return node.scrollTop;
};

/**
 * The Fade transition is used by the Modal component.
 * It's using [react-transition-group](https://github.com/reactjs/react-transition-group) internally.
 */

var Fade = function (_React$Component) {
  (0, _inherits3.default)(Fade, _React$Component);

  function Fade() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Fade);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Fade.__proto__ || (0, _getPrototypeOf2.default)(Fade)).call.apply(_ref, [this].concat(args))), _this), _this.handleEnter = function (node) {
      node.style.opacity = '0';
      reflow(node);

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          theme = _this$props.theme,
          timeout = _this$props.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.enter
      });
      node.style.opacity = '1';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleExit = function (node) {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          timeout = _this$props2.timeout;

      node.style.transition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      // $FlowFixMe - https://github.com/facebook/flow/pull/5161
      node.style.webkitTransition = theme.transitions.create('opacity', {
        duration: typeof timeout === 'number' ? timeout : timeout.exit
      });
      node.style.opacity = '0';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Fade, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onExit = _props.onExit,
          styleProp = _props.style,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'onEnter', 'onEntering', 'onExit', 'style', 'theme']);


      var style = (0, _extends3.default)({}, styleProp);

      // For server side rendering.
      if (!this.props.in || appear) {
        style.opacity = '0';
      }

      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          style: style,
          onEnter: this.handleEnter,
          onEntering: this.handleEntering,
          onExit: this.handleExit
        }, other),
        children
      );
    }
  }]);
  return Fade;
}(_react2.default.Component);

Fade.defaultProps = {
  appear: true,
  timeout: {
    enter: _transitions.duration.enteringScreen,
    exit: _transitions.duration.leavingScreen
  }
};
exports.default = (0, _withTheme2.default)()(Fade);

/***/ }),

/***/ "./node_modules/material-ui/utils/manageAriaHidden.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui/utils/manageAriaHidden.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ariaHidden = ariaHidden;
exports.hideSiblings = hideSiblings;
exports.showSiblings = showSiblings;
//  weak

var BLACKLIST = ['template', 'script', 'style'];

var isHidable = function isHidable(_ref) {
  var nodeType = _ref.nodeType,
      tagName = _ref.tagName;
  return nodeType === 1 && BLACKLIST.indexOf(tagName.toLowerCase()) === -1;
};

var siblings = function siblings(container, mount, cb) {
  mount = [].concat(mount); // eslint-disable-line no-param-reassign
  [].forEach.call(container.children, function (node) {
    if (mount.indexOf(node) === -1 && isHidable(node)) {
      cb(node);
    }
  });
};

function ariaHidden(show, node) {
  if (!node) {
    return;
  }
  if (show) {
    node.setAttribute('aria-hidden', 'true');
  } else {
    node.removeAttribute('aria-hidden');
  }
}

function hideSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(true, node);
  });
}

function showSiblings(container, mountNode) {
  siblings(container, mountNode, function (node) {
    return ariaHidden(false, node);
  });
}

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/react-router/Route.js":
/*!********************************************!*\
  !*** ./node_modules/react-router/Route.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _matchPath = __webpack_require__(/*! ./matchPath */ "./node_modules/react-router/matchPath.js");

var _matchPath2 = _interopRequireDefault(_matchPath);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isEmptyChildren = function isEmptyChildren(children) {
  return _react2.default.Children.count(children) === 0;
};

/**
 * The public API for matching a single path and rendering.
 */

var Route = function (_React$Component) {
  _inherits(Route, _React$Component);

  function Route() {
    var _temp, _this, _ret;

    _classCallCheck(this, Route);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.state = {
      match: _this.computeMatch(_this.props, _this.context.router)
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Route.prototype.getChildContext = function getChildContext() {
    return {
      router: _extends({}, this.context.router, {
        route: {
          location: this.props.location || this.context.router.route.location,
          match: this.state.match
        }
      })
    };
  };

  Route.prototype.computeMatch = function computeMatch(_ref, router) {
    var computedMatch = _ref.computedMatch,
        location = _ref.location,
        path = _ref.path,
        strict = _ref.strict,
        exact = _ref.exact,
        sensitive = _ref.sensitive;

    if (computedMatch) return computedMatch; // <Switch> already computed the match for us

    (0, _invariant2.default)(router, 'You should not use <Route> or withRouter() outside a <Router>');

    var route = router.route;

    var pathname = (location || route.location).pathname;

    return path ? (0, _matchPath2.default)(pathname, { path: path, strict: strict, exact: exact, sensitive: sensitive }) : route.match;
  };

  Route.prototype.componentWillMount = function componentWillMount() {
    (0, _warning2.default)(!(this.props.component && this.props.render), 'You should not use <Route component> and <Route render> in the same route; <Route render> will be ignored');

    (0, _warning2.default)(!(this.props.component && this.props.children && !isEmptyChildren(this.props.children)), 'You should not use <Route component> and <Route children> in the same route; <Route children> will be ignored');

    (0, _warning2.default)(!(this.props.render && this.props.children && !isEmptyChildren(this.props.children)), 'You should not use <Route render> and <Route children> in the same route; <Route children> will be ignored');
  };

  Route.prototype.componentWillReceiveProps = function componentWillReceiveProps(nextProps, nextContext) {
    (0, _warning2.default)(!(nextProps.location && !this.props.location), '<Route> elements should not change from uncontrolled to controlled (or vice versa). You initially used no "location" prop and then provided one on a subsequent render.');

    (0, _warning2.default)(!(!nextProps.location && this.props.location), '<Route> elements should not change from controlled to uncontrolled (or vice versa). You provided a "location" prop initially but omitted it on a subsequent render.');

    this.setState({
      match: this.computeMatch(nextProps, nextContext.router)
    });
  };

  Route.prototype.render = function render() {
    var match = this.state.match;
    var _props = this.props,
        children = _props.children,
        component = _props.component,
        render = _props.render;
    var _context$router = this.context.router,
        history = _context$router.history,
        route = _context$router.route,
        staticContext = _context$router.staticContext;

    var location = this.props.location || route.location;
    var props = { match: match, location: location, history: history, staticContext: staticContext };

    return component ? // component prop gets first priority, only called if there's a match
    match ? _react2.default.createElement(component, props) : null : render ? // render prop is next, only called if there's a match
    match ? render(props) : null : children ? // children come last, always called
    typeof children === 'function' ? children(props) : !isEmptyChildren(children) ? _react2.default.Children.only(children) : null : null;
  };

  return Route;
}(_react2.default.Component);

Route.propTypes = {
  computedMatch: _propTypes2.default.object, // private, from <Switch>
  path: _propTypes2.default.string,
  exact: _propTypes2.default.bool,
  strict: _propTypes2.default.bool,
  sensitive: _propTypes2.default.bool,
  component: _propTypes2.default.func,
  render: _propTypes2.default.func,
  children: _propTypes2.default.oneOfType([_propTypes2.default.func, _propTypes2.default.node]),
  location: _propTypes2.default.object
};
Route.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.object.isRequired,
    route: _propTypes2.default.object.isRequired,
    staticContext: _propTypes2.default.object
  })
};
Route.childContextTypes = {
  router: _propTypes2.default.object.isRequired
};
exports.default = Route;

/***/ }),

/***/ "./node_modules/react-router/matchPath.js":
/*!************************************************!*\
  !*** ./node_modules/react-router/matchPath.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _pathToRegexp = __webpack_require__(/*! path-to-regexp */ "./node_modules/path-to-regexp/index.js");

var _pathToRegexp2 = _interopRequireDefault(_pathToRegexp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var patternCache = {};
var cacheLimit = 10000;
var cacheCount = 0;

var compilePath = function compilePath(pattern, options) {
  var cacheKey = '' + options.end + options.strict + options.sensitive;
  var cache = patternCache[cacheKey] || (patternCache[cacheKey] = {});

  if (cache[pattern]) return cache[pattern];

  var keys = [];
  var re = (0, _pathToRegexp2.default)(pattern, keys, options);
  var compiledPattern = { re: re, keys: keys };

  if (cacheCount < cacheLimit) {
    cache[pattern] = compiledPattern;
    cacheCount++;
  }

  return compiledPattern;
};

/**
 * Public API for matching a URL pathname to a path pattern.
 */
var matchPath = function matchPath(pathname) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  if (typeof options === 'string') options = { path: options };

  var _options = options,
      _options$path = _options.path,
      path = _options$path === undefined ? '/' : _options$path,
      _options$exact = _options.exact,
      exact = _options$exact === undefined ? false : _options$exact,
      _options$strict = _options.strict,
      strict = _options$strict === undefined ? false : _options$strict,
      _options$sensitive = _options.sensitive,
      sensitive = _options$sensitive === undefined ? false : _options$sensitive;

  var _compilePath = compilePath(path, { end: exact, strict: strict, sensitive: sensitive }),
      re = _compilePath.re,
      keys = _compilePath.keys;

  var match = re.exec(pathname);

  if (!match) return null;

  var url = match[0],
      values = match.slice(1);

  var isExact = pathname === url;

  if (exact && !isExact) return null;

  return {
    path: path, // the path pattern used to match
    url: path === '/' && url === '' ? '/' : url, // the matched portion of the URL
    isExact: isExact, // whether or not we matched exactly
    params: keys.reduce(function (memo, key, index) {
      memo[key.name] = values[index];
      return memo;
    }, {})
  };
};

exports.default = matchPath;

/***/ }),

/***/ "./node_modules/react-router/withRouter.js":
/*!*************************************************!*\
  !*** ./node_modules/react-router/withRouter.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _hoistNonReactStatics = __webpack_require__(/*! hoist-non-react-statics */ "./node_modules/hoist-non-react-statics/index.js");

var _hoistNonReactStatics2 = _interopRequireDefault(_hoistNonReactStatics);

var _Route = __webpack_require__(/*! ./Route */ "./node_modules/react-router/Route.js");

var _Route2 = _interopRequireDefault(_Route);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

/**
 * A public higher-order component to access the imperative API
 */
var withRouter = function withRouter(Component) {
  var C = function C(props) {
    var wrappedComponentRef = props.wrappedComponentRef,
        remainingProps = _objectWithoutProperties(props, ['wrappedComponentRef']);

    return _react2.default.createElement(_Route2.default, { render: function render(routeComponentProps) {
        return _react2.default.createElement(Component, _extends({}, remainingProps, routeComponentProps, { ref: wrappedComponentRef }));
      } });
  };

  C.displayName = 'withRouter(' + (Component.displayName || Component.name) + ')';
  C.WrappedComponent = Component;
  C.propTypes = {
    wrappedComponentRef: _propTypes2.default.func
  };

  return (0, _hoistNonReactStatics2.default)(C, Component);
};

exports.default = withRouter;

/***/ }),

/***/ "./src/client/actions/courses/delete.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/delete.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @function deleteCourse
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @return {Object} -
 */
exports.default = function (payload) {
  return { type: _courses.COURSE_DELETE, payload: payload };
}; /**
    * @format
    * 
    */

/***/ }),

/***/ "./src/client/actions/elements/getById.js":
/*!************************************************!*\
  !*** ./src/client/actions/elements/getById.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

var _getById = __webpack_require__(/*! ../../api/elements/getById */ "./src/client/api/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @return {Object}
 */
/** @format */

var getElementStart = function getElementStart(payload) {
  return { type: _elements.ELEMENT_GET_START, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType
 * @param {string} payload.circleType - only if elementType is 'circle'
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @return {Object}
 */
var getElementSuccess = function getElementSuccess(payload) {
  return { type: _elements.ELEMENT_GET_SUCCESS, payload: payload };
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
var getElementError = function getElementError(payload) {
  return { type: _elements.ELEMENT_GET_ERROR, payload: payload };
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
var get = function get(_ref) {
  var token = _ref.token,
      id = _ref.id;
  return function (dispatch) {
    dispatch(getElementStart({ id: id }));

    (0, _getById2.default)({ token: token, id: id }).then(function (_ref2) {
      var statusCode = _ref2.statusCode,
          error = _ref2.error,
          payload = _ref2.payload;

      if (statusCode >= 300) {
        dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));

        return;
      }

      dispatch(getElementSuccess((0, _extends3.default)({ id: id, statusCode: statusCode }, payload)));
    }).catch(function (_ref3) {
      var statusCode = _ref3.statusCode,
          error = _ref3.error;

      dispatch(getElementError({ id: id, statusCode: statusCode, error: error }));
    });
  };
};

exports.default = get;

/***/ }),

/***/ "./src/client/actions/posts/delete.js":
/*!********************************************!*\
  !*** ./src/client/actions/posts/delete.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../constants/posts */ "./src/client/constants/posts.js");

/**
 *
 * @function deletePost
 * @param {Object} payload
 * @param {number} payload.postId
 * @returns {Object}
 */
var deletePost = function deletePost(payload) {
  return {
    type: _posts.POST_DELETE,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = deletePost;

/***/ }),

/***/ "./src/client/actions/posts/users/create.js":
/*!**************************************************!*\
  !*** ./src/client/actions/posts/users/create.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../../constants/posts */ "./src/client/constants/posts.js");

/**
 * This action will create post for user in store.
 * @function create
 * @param {Object} payload
 * @param {number} payload.postId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {string} payload.timestamp
 *
 * @param {number} payload.statusCode
 * @param {string} payload.message
 *
 * @returns {Object}
 */
var create = function create(payload) {
  return { type: _posts.POST_CREATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = create;

/***/ }),

/***/ "./src/client/api/courses/users/delete.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/delete.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * delete course
 * @async
 * @function deleteCourse
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.courseId
 * @returns {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

var deleteCourse = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deleteCourse(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deleteCourse;

/***/ }),

/***/ "./src/client/api/elements/getById.js":
/*!********************************************!*\
  !*** ./src/client/api/elements/getById.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @function getById
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
/** @format */

var getById = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        id = _ref2.id;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/elements/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getById(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getById;

/***/ }),

/***/ "./src/client/api/posts/users/create.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/create.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * This function will create post by user.
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {string} payload.postType
 * @param {string} payload.title
 * @return {Promise} -
 *
 * @example
 * create({
 *   userId: 12314123134,
 *   token: 'head.body.tail',
 *   postType: 'article',
 *   title: 'This is awesome article',
 * })
 *   .then(res => console.log(res))
 *   .catch(err => console.error(err));
 */
/**
 * @format
 * @file create post for user
 */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        postType = _ref2.postType,
        title = _ref2.title;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ postType: postType, title: title })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/api/posts/users/delete.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/delete.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * delete post of user
 * @async
 * @function deletePost
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file delete post of user.
 */

var deletePost = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token,
        postId = _ref2.postId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts/' + postId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deletePost(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deletePost;

/***/ }),

/***/ "./src/client/components/Input.js":
/*!****************************************!*\
  !*** ./src/client/components/Input.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    display: 'flex'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    }
  }
}; /**
    * This input component is mainly developed for create post, create
    * course component.
    *
    * @format
    */

var Input = function Input(_ref) {
  var classes = _ref.classes,
      onChange = _ref.onChange,
      value = _ref.value,
      placeholder = _ref.placeholder,
      disabled = _ref.disabled,
      type = _ref.type;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement('input', {
      type: type || 'text',
      placeholder: placeholder || 'input',
      value: value,
      onChange: onChange,
      className: classes.input,
      disabled: !!disabled
    })
  );
};

Input.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  value: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]).isRequired,
  placeholder: _propTypes2.default.string,
  type: _propTypes2.default.string,
  disabled: _propTypes2.default.bool
};

exports.default = (0, _withStyles2.default)(styles)(Input);

/***/ }),

/***/ "./src/client/containers/Course/index.js":
/*!***********************************************!*\
  !*** ./src/client/containers/Course/index.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withRouter = __webpack_require__(/*! react-router/withRouter */ "./node_modules/react-router/withRouter.js");

var _withRouter2 = _interopRequireDefault(_withRouter);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Dialog = __webpack_require__(/*! material-ui/Dialog */ "./node_modules/material-ui/Dialog/index.js");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _TurnedIn = __webpack_require__(/*! material-ui-icons/TurnedIn */ "./node_modules/material-ui-icons/TurnedIn.js");

var _TurnedIn2 = _interopRequireDefault(_TurnedIn);

var _Delete = __webpack_require__(/*! material-ui-icons/Delete */ "./node_modules/material-ui-icons/Delete.js");

var _Delete2 = _interopRequireDefault(_Delete);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _delete = __webpack_require__(/*! ../../api/courses/users/delete */ "./src/client/api/courses/users/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _delete3 = __webpack_require__(/*! ../../actions/courses/delete */ "./src/client/actions/courses/delete.js");

var _delete4 = _interopRequireDefault(_delete3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    padding: '1%'
  },
  gridContainer: {},
  name: {
    cursor: 'pointer',
    textDecoration: 'none'
  },
  username: {
    cursor: 'pointer',
    textDecoration: 'none'
  },
  post: {
    padding: '1%'
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  cardMedia: {},
  media: {
    height: 194
  },
  cardMediaImage: {
    width: '100%'
  },
  margin: {
    border: '0.5px solid rgba(209,209,209,1)',
    marginBottom: '-10px'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
}; /**
    * course page component
    * This component will display onlu one course information
    *
    * @format
    */

var Course = function (_Component) {
  (0, _inherits3.default)(Course, _Component);

  function Course(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, Course);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Course.__proto__ || (0, _getPrototypeOf2.default)(Course)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onClickContent = function () {
      var courseId = _this.props.courseId;


      _this.props.history.push('/courses/' + courseId);
    };

    _this.deleteDialogOpen = function () {
      _this.setState({ deleteDialog: true });
    };

    _this.deleteDialogClose = function () {
      _this.setState({ deleteDialog: false });
    };

    _this.deleteCourseClick = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var _this$props$elements$, token, id, courseId, _ref2, statusCode, error;

      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this$props$elements$ = _this.props.elements.user, token = _this$props$elements$.token, id = _this$props$elements$.id;
              courseId = _this.props.courseId;
              _context.next = 4;
              return (0, _delete2.default)({
                token: token,
                userId: id,
                courseId: courseId
              });

            case 4:
              _ref2 = _context.sent;
              statusCode = _ref2.statusCode;
              error = _ref2.error;

              if (!(statusCode !== 200)) {
                _context.next = 10;
                break;
              }

              // show some error message.
              console.log(error);
              return _context.abrupt('return');

            case 10:

              _this.props.actionCourseDelete({ courseId: courseId });
              _this.setState({ deleteDialog: false });

            case 12:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this2);
    }));

    _this.state = {
      hover: false,
      deleteDialog: false
    };
    return _this;
  }

  (0, _createClass3.default)(Course, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _props = this.props,
          elements = _props.elements,
          courses = _props.courses,
          courseId = _props.courseId;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = courses[courseId].authorId;

      var author = elements[authorId];

      // if author is not found
      if (userId !== authorId && !author) {
        this.props.actionGetElementById({ id: authorId, token: token });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          courses = _props2.courses,
          elements = _props2.elements,
          courseId = _props2.courseId;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id;


      var course = courses[courseId];

      if (typeof course === 'undefined') {
        return _react2.default.createElement('div', null);
      }

      var title = course.title,
          description = course.description,
          authorId = course.authorId,
          cover = course.cover;

      var author = authorId === userId ? user : elements[authorId];

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(_Avatar2.default, {
                alt: author.name,
                src: author.avatar || '/public/photos/mayash-logo-transparent.png'
              })
            ),
            action: _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _Tooltip2.default,
                { title: 'Coming soon', placement: 'bottom' },
                _react2.default.createElement(
                  _IconButton2.default,
                  { disabled: true },
                  _react2.default.createElement(_MoreVert2.default, null)
                )
              )
            ),
            title: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.name },
              author.name
            ),
            subheader: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.username },
              '@' + author.username
            )
          }),
          _react2.default.createElement(_Card.CardMedia, {
            className: classes.media,
            image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
          }),
          _react2.default.createElement(
            _Card.CardContent,
            { onClick: this.onClickContent },
            _react2.default.createElement(
              _Typography2.default,
              { type: 'title', gutterBottom: true },
              title
            ),
            typeof description === 'string' ? _react2.default.createElement(
              _Typography2.default,
              { type: 'body1', gutterBottom: true },
              description
            ) : null
          ),
          _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Add to favorites', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Add to favorites', disabled: true },
                _react2.default.createElement(_Favorite2.default, null)
              )
            ),
            _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Bookmark', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Book Marks', disabled: true },
                _react2.default.createElement(_TurnedIn2.default, null)
              )
            ),
            isSignedIn && userId === author.id ? _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Delete', onClick: this.deleteDialogOpen },
                _react2.default.createElement(_Delete2.default, null)
              ),
              _react2.default.createElement(
                _Dialog2.default,
                {
                  open: this.state.deleteDialog,
                  onClose: this.deleteDialogClose,
                  'aria-labelledby': 'course-' + courseId + '-delete-dialog'
                },
                _react2.default.createElement(
                  _Dialog.DialogTitle,
                  { id: 'course-' + courseId + '-delete-dialog' },
                  'Are you sure?'
                ),
                _react2.default.createElement(
                  _Dialog.DialogActions,
                  null,
                  _react2.default.createElement(
                    _Button2.default,
                    { onClick: this.deleteDialogClose, color: 'primary' },
                    'Cancel'
                  ),
                  _react2.default.createElement(
                    _Button2.default,
                    {
                      onClick: this.deleteCourseClick,
                      color: 'primary',
                      autoFocus: true
                    },
                    'Delete'
                  )
                )
              )
            ) : null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share', disabled: true },
              _react2.default.createElement(_Share2.default, null)
            )
          )
        )
      );
    }
  }]);
  return Course;
}(_react.Component);

Course.propTypes = {
  history: _propTypes2.default.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,
  courses: _propTypes2.default.object.isRequired,
  courseId: _propTypes2.default.number.isRequired,

  actionGetElementById: _propTypes2.default.func.isRequired,
  actionCourseDelete: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements,
      courses = _ref3.courses;
  return { elements: elements, courses: courses };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionGetElementById: _getById2.default,
    actionCourseDelete: _delete4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)((0, _withRouter2.default)(Course)));

/***/ }),

/***/ "./src/client/containers/ElementPage/Tabs/TabPosts.js":
/*!************************************************************!*\
  !*** ./src/client/containers/ElementPage/Tabs/TabPosts.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _PostCreate = __webpack_require__(/*! ../../PostCreate */ "./src/client/containers/PostCreate/index.js");

var _PostCreate2 = _interopRequireDefault(_PostCreate);

var _Timeline = __webpack_require__(/*! ../../Timeline */ "./src/client/containers/Timeline/index.js");

var _Timeline2 = _interopRequireDefault(_Timeline);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {}
};

var TabPosts = function (_Component) {
  (0, _inherits3.default)(TabPosts, _Component);

  function TabPosts(props) {
    (0, _classCallCheck3.default)(this, TabPosts);

    var _this = (0, _possibleConstructorReturn3.default)(this, (TabPosts.__proto__ || (0, _getPrototypeOf2.default)(TabPosts)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(TabPosts, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          element = _props.element,
          posts = _props.posts;
      var isSignedIn = user.isSignedIn,
          id = user.id;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { container: true, justify: 'center', spacing: 0 },
          _react2.default.createElement(
            _Grid2.default,
            { item: true, xs: 12, sm: 9, md: 7, lg: 6, xl: 6 },
            isSignedIn && id === element.id ? _react2.default.createElement(_PostCreate2.default, null) : null,
            _react2.default.createElement(_Timeline2.default, { type: 'posts', posts: posts, element: element })
          )
        )
      );
    }
  }]);
  return TabPosts;
}(_react.Component);

TabPosts.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,
  element: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object.isRequired
};

var mapStateToProps = function mapStateToProps(state) {
  return state;
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({}, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _styles.withStyles)(styles)(TabPosts));

/***/ }),

/***/ "./src/client/containers/Post/index.js":
/*!*********************************************!*\
  !*** ./src/client/containers/Post/index.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withRouter = __webpack_require__(/*! react-router/withRouter */ "./node_modules/react-router/withRouter.js");

var _withRouter2 = _interopRequireDefault(_withRouter);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _classnames2 = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames3 = _interopRequireDefault(_classnames2);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Dialog = __webpack_require__(/*! material-ui/Dialog */ "./node_modules/material-ui/Dialog/index.js");

var _Dialog2 = _interopRequireDefault(_Dialog);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Badge = __webpack_require__(/*! material-ui/Badge */ "./node_modules/material-ui/Badge/index.js");

var _Badge2 = _interopRequireDefault(_Badge);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _MoreVert = __webpack_require__(/*! material-ui-icons/MoreVert */ "./node_modules/material-ui-icons/MoreVert.js");

var _MoreVert2 = _interopRequireDefault(_MoreVert);

var _Favorite = __webpack_require__(/*! material-ui-icons/Favorite */ "./node_modules/material-ui-icons/Favorite.js");

var _Favorite2 = _interopRequireDefault(_Favorite);

var _TurnedIn = __webpack_require__(/*! material-ui-icons/TurnedIn */ "./node_modules/material-ui-icons/TurnedIn.js");

var _TurnedIn2 = _interopRequireDefault(_TurnedIn);

var _Comment = __webpack_require__(/*! material-ui-icons/Comment */ "./node_modules/material-ui-icons/Comment.js");

var _Comment2 = _interopRequireDefault(_Comment);

var _Delete = __webpack_require__(/*! material-ui-icons/Delete */ "./node_modules/material-ui-icons/Delete.js");

var _Delete2 = _interopRequireDefault(_Delete);

var _RemoveRedEye = __webpack_require__(/*! material-ui-icons/RemoveRedEye */ "./node_modules/material-ui-icons/RemoveRedEye.js");

var _RemoveRedEye2 = _interopRequireDefault(_RemoveRedEye);

var _Share = __webpack_require__(/*! material-ui-icons/Share */ "./node_modules/material-ui-icons/Share.js");

var _Share2 = _interopRequireDefault(_Share);

var _getById = __webpack_require__(/*! ../../actions/elements/getById */ "./src/client/actions/elements/getById.js");

var _getById2 = _interopRequireDefault(_getById);

var _delete = __webpack_require__(/*! ../../api/posts/users/delete */ "./src/client/api/posts/users/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _delete3 = __webpack_require__(/*! ../../actions/posts/delete */ "./src/client/actions/posts/delete.js");

var _delete4 = _interopRequireDefault(_delete3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      padding: '1%'
    },
    card: {
      borderRadius: '8px'
    },
    avatar: {
      borderRadius: '50%'
    },
    title: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    subheader: {
      // color: '#365899',
      cursor: 'pointer',
      textDecoration: 'none'
    },
    media: {
      height: 194
    },
    favoriteIconActive: {
      color: '#F50057'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    badge: {
      margin: '0 ' + theme.spacing.unit * 2 + 'px',
      color: 'blue'
    },
    margin: {
      border: '0.5px solid rgba(209,209,209,1)',
      marginBottom: '-10px'
    },
    content: {
      textAlign: 'justify'
    }
  };
};

// All actions are defined here.
/**
 * Post container component
 * It will display one post with some of actions (eg. Bookmarks, Share, Delete)
 *
 * @format
 */

var Post = function (_Component) {
  (0, _inherits3.default)(Post, _Component);

  function Post(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, Post);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Post.__proto__ || (0, _getPrototypeOf2.default)(Post)).call(this, props));

    _this.onClickFavorite = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
      var favorite;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              favorite = _this.state.favorite;


              _this.setState({ favorite: !favorite });

            case 2:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this2);
    }));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onClickPost = function () {
      return _this.props.history.push('/posts/' + _this.props.postId);
    };

    _this.deleteDialogOpen = function () {
      _this.setState({ deleteDialog: true });
    };

    _this.deleteDialogClose = function () {
      _this.setState({ deleteDialog: false });
    };

    _this.deletePostClick = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
      var _this$props$elements$, token, id, postId, _ref3, statusCode, error;

      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _this$props$elements$ = _this.props.elements.user, token = _this$props$elements$.token, id = _this$props$elements$.id;
              postId = _this.props.postId;
              _context2.next = 4;
              return (0, _delete2.default)({
                token: token,
                userId: id,
                postId: postId
              });

            case 4:
              _ref3 = _context2.sent;
              statusCode = _ref3.statusCode;
              error = _ref3.error;

              if (!(statusCode !== 200)) {
                _context2.next = 10;
                break;
              }

              // show some error message.
              console.log(error);
              return _context2.abrupt('return');

            case 10:

              _this.props.actionPostDelete({ postId: postId });
              _this.setState({ deleteDialog: false });

            case 12:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }));

    _this.state = {
      hover: false,
      favorite: false,
      deleteDialog: false
    };
    return _this;
  }

  // componentWillMount is executed before rendering


  (0, _createClass3.default)(Post, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      var _props = this.props,
          elements = _props.elements,
          posts = _props.posts,
          postId = _props.postId;
      var _elements$user = elements.user,
          token = _elements$user.token,
          userId = _elements$user.id;
      var authorId = posts[postId].authorId;


      var author = elements[authorId];

      // if author is not found
      if (userId !== authorId && !author) {
        this.props.getElementById({ id: authorId, token: token });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          posts = _props2.posts,
          elements = _props2.elements,
          postId = _props2.postId;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id;


      var post = posts[postId];

      var favorite = this.state.favorite;


      if (typeof post === 'undefined') {
        return _react2.default.createElement('div', null);
      }

      var cover = post.cover,
          title = post.title,
          description = post.description,
          authorId = post.authorId;

      var author = authorId === userId ? user : elements[authorId];

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + author.username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: author.name,
                  src: '/public/photos/mayash-logo-transparent.png'
                })
              )
            ),
            action: _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Coming soon', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                { disabled: true },
                _react2.default.createElement(_MoreVert2.default, null)
              )
            ),
            title: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.title },
              author.name
            ),
            subheader: _react2.default.createElement(
              _Link2.default,
              { to: '/@' + author.username, className: classes.subheader },
              '@',
              author.username
            )
          }),
          _react2.default.createElement(_Card.CardMedia, {
            className: classes.media,
            image: cover || 'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg',
            title: title
          }),
          _react2.default.createElement(
            _Card.CardContent,
            { onClick: this.onClickPost },
            _react2.default.createElement(
              _Typography2.default,
              { type: 'title' },
              title
            ),
            typeof description === 'string' ? _react2.default.createElement(
              _Typography2.default,
              { type: 'body1', gutterBottom: true, className: classes.content },
              description
            ) : null
          ),
          _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement(
              _IconButton2.default,
              { onClick: this.onClickFavorite },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_Favorite2.default, {
                  className: (0, _classnames3.default)((0, _defineProperty3.default)({}, '' + classes.favoriteIconActive, favorite))
                })
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Comment' },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_Comment2.default, null)
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Book Marks' },
              _react2.default.createElement(_TurnedIn2.default, null)
            ),
            isSignedIn && userId === authorId ? _react2.default.createElement(
              'div',
              null,
              _react2.default.createElement(
                _IconButton2.default,
                { 'aria-label': 'Delete', onClick: this.deleteDialogOpen },
                _react2.default.createElement(_Delete2.default, null)
              ),
              _react2.default.createElement(
                _Dialog2.default,
                {
                  open: this.state.deleteDialog,
                  onClose: this.deleteDialogClose,
                  'aria-labelledby': 'post-' + postId + '-delete-dialog'
                },
                _react2.default.createElement(
                  _Dialog.DialogTitle,
                  { id: 'post-' + postId + '-delete-dialog' },
                  'Are you sure?'
                ),
                _react2.default.createElement(
                  _Dialog.DialogActions,
                  null,
                  _react2.default.createElement(
                    _Button2.default,
                    { onClick: this.deleteDialogClose, color: 'primary' },
                    'Cancel'
                  ),
                  _react2.default.createElement(
                    _Button2.default,
                    {
                      onClick: this.deletePostClick,
                      color: 'primary',
                      autoFocus: true
                    },
                    'Delete'
                  )
                )
              )
            ) : null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share' },
              _react2.default.createElement(
                _Badge2.default,
                { badgeContent: 0 },
                _react2.default.createElement(_RemoveRedEye2.default, null)
              )
            ),
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Share', disabled: true },
              _react2.default.createElement(_Share2.default, null)
            )
          )
        )
      );
    }
  }]);
  return Post;
}(_react.Component);

Post.propTypes = {
  history: _propTypes2.default.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object.isRequired,

  // post: PropTypes.shape({
  //   authorId: PropTypes.number.isRequired,
  //   postId: PropTypes.number.isRequired,
  //   title: PropTypes.string.isRequired,
  //   description: PropTypes.string,
  // }).isRequired,

  postId: _propTypes2.default.number.isRequired,

  getElementById: _propTypes2.default.func.isRequired,
  actionPostDelete: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements,
      posts = _ref4.posts;
  return { elements: elements, posts: posts };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    getElementById: _getById2.default,
    actionPostDelete: _delete4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)((0, _withRouter2.default)(Post)));

/***/ }),

/***/ "./src/client/containers/PostCreate/index.js":
/*!***************************************************!*\
  !*** ./src/client/containers/PostCreate/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Input = __webpack_require__(/*! ../../components/Input */ "./src/client/components/Input.js");

var _Input2 = _interopRequireDefault(_Input);

var _create = __webpack_require__(/*! ../../api/posts/users/create */ "./src/client/api/posts/users/create.js");

var _create2 = _interopRequireDefault(_create);

var _create3 = __webpack_require__(/*! ../../actions/posts/users/create */ "./src/client/actions/posts/users/create.js");

var _create4 = _interopRequireDefault(_create3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * PostCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create post for their
 * profile only.
 *
 * @format
 */

var styles = {
  root: {
    padding: '1%'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  gridContainer: {},
  post: {
    padding: '1%'
  },
  cardHeader: {
    // textAlign: 'center'
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%'
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/**
 * PostCreate component will create a new post for signed in user.
 * @class PostCreate
 * @version 1.0.0
 */

var PostCreate = function (_Component) {
  (0, _inherits3.default)(PostCreate, _Component);

  function PostCreate(props) {
    (0, _classCallCheck3.default)(this, PostCreate);

    var _this = (0, _possibleConstructorReturn3.default)(this, (PostCreate.__proto__ || (0, _getPrototypeOf2.default)(PostCreate)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onFocus = function () {
      return _this.setState({ active: true });
    };

    _this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Title...',
      postType: 'article',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(PostCreate, [{
    key: 'onChange',
    value: function onChange(e) {
      var title = e.target.value;
      var titleLength = title.length;
      var valid = titleLength > 0 && titleLength < 148;

      this.setState({
        title: title,
        titleLength: titleLength,
        valid: valid,
        error: !valid ? 'Title length should be in between 1 to 148.' : ''
      });
    }
  }, {
    key: 'onSubmit',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var _this2 = this;

        var _props$elements$user, token, userId, _state, postType, title, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();
                _props$elements$user = this.props.elements.user, token = _props$elements$user.token, userId = _props$elements$user.id;
                _state = this.state, postType = _state.postType, title = _state.title;


                this.setState({ message: 'Creating post...' });

                _context.next = 6;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  postType: postType,
                  title: title
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle error
                this.setState({ statusCode: statusCode, error: error });
                return _context.abrupt('return');

              case 13:

                this.setState({ message: 'Successfully Created.' });

                setTimeout(function () {
                  _this2.setState({
                    valid: false,
                    statusCode: 0,
                    error: '',
                    message: '',

                    postType: 'article',

                    title: '',
                    titleLength: 0
                  });
                }, 1500);

                this.props.actionPostCreate((0, _extends3.default)({
                  statusCode: statusCode
                }, payload, {
                  title: title,
                  postType: postType
                }));

              case 16:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSubmit(_x) {
        return _ref.apply(this, arguments);
      }

      return onSubmit;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _state2 = this.state,
          hover = _state2.hover,
          active = _state2.active,
          placeholder = _state2.placeholder,
          title = _state2.title,
          message = _state2.message;
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements;
      var _elements$user = elements.user,
          name = _elements$user.name,
          avatar = _elements$user.avatar,
          username = _elements$user.username;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _Link2.default,
                { to: '/@' + username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: name,
                  src: avatar || '/public/photos/mayash-logo-transparent.png',
                  className: classes.avatar
                })
              )
            ),
            title: _react2.default.createElement(_Input2.default, {
              value: title,
              onChange: this.onChange,
              placeholder: placeholder
            }),
            className: classes.cardHeader,
            onFocus: this.onFocus
          }),
          active && message.length ? _react2.default.createElement(
            _Card.CardContent,
            null,
            _react2.default.createElement(
              _Typography2.default,
              null,
              message
            )
          ) : null,
          active ? _react2.default.createElement(
            _Card.CardActions,
            null,
            _react2.default.createElement('div', { className: classes.flexGrow }),
            _react2.default.createElement(
              _Button2.default,
              { raised: true, color: 'accent', onClick: this.onSubmit },
              'Create'
            )
          ) : null
        )
      );
    }
  }]);
  return PostCreate;
}(_react.Component);

PostCreate.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  actionPostCreate: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionPostCreate: _create4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(PostCreate));

/***/ }),

/***/ "./src/client/containers/Timeline/index.js":
/*!*************************************************!*\
  !*** ./src/client/containers/Timeline/index.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Post = __webpack_require__(/*! ../Post */ "./src/client/containers/Post/index.js");

var _Post2 = _interopRequireDefault(_Post);

var _Course = __webpack_require__(/*! ../Course */ "./src/client/containers/Course/index.js");

var _Course2 = _interopRequireDefault(_Course);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {
    flexGrow: '1'
  }
}; /**
    * Timeline Display component
    * This component will display either course or Post timeline
    *
    * @format
    */

var Timeline = function (_Component) {
  (0, _inherits3.default)(Timeline, _Component);

  function Timeline(props) {
    (0, _classCallCheck3.default)(this, Timeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Timeline.__proto__ || (0, _getPrototypeOf2.default)(Timeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Timeline, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          type = _props.type,
          posts = _props.posts,
          courses = _props.courses,
          element = _props.element;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        type === 'posts' ? element.posts.map(function (p) {
          return _react2.default.createElement(_Post2.default, { key: p.postId, postId: p.postId });
        }) : element.courses.map(function (c) {
          return _react2.default.createElement(_Course2.default, { key: c.courseId, courseId: c.courseId });
        })
      );
    }
  }]);
  return Timeline;
}(_react.Component);

Timeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  type: _propTypes2.default.string.isRequired, // 'posts' or 'courses'
  element: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object,
  courses: _propTypes2.default.object
};

exports.default = (0, _styles.withStyles)(styles)(Timeline);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvYWN0aXZlRWxlbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1JlbW92ZVJlZEV5ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9CYWRnZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dBY3Rpb25zLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nQ29udGVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0NvbnRlbnRUZXh0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nVGl0bGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL3dpdGhNb2JpbGVEaWFsb2cuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL0JhY2tkcm9wLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9Nb2RhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL21vZGFsTWFuYWdlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvUG9ydGFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC90cmFuc2l0aW9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9GYWRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS91dGlscy9tYW5hZ2VBcmlhSGlkZGVuLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9Sb3V0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL21hdGNoUGF0aC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL3dpdGhSb3V0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvZGVsZXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9lbGVtZW50cy9nZXRCeUlkLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9wb3N0cy9kZWxldGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hY3Rpb25zL3Bvc3RzL3VzZXJzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL2RlbGV0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRCeUlkLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3Bvc3RzL3VzZXJzL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9wb3N0cy91c2Vycy9kZWxldGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Db3Vyc2UvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0VsZW1lbnRQYWdlL1RhYnMvVGFiUG9zdHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1Bvc3QvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1Bvc3RDcmVhdGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL1RpbWVsaW5lL2luZGV4LmpzIl0sIm5hbWVzIjpbInBheWxvYWQiLCJ0eXBlIiwiZ2V0RWxlbWVudFN0YXJ0IiwiZ2V0RWxlbWVudFN1Y2Nlc3MiLCJnZXRFbGVtZW50RXJyb3IiLCJnZXQiLCJ0b2tlbiIsImlkIiwiZGlzcGF0Y2giLCJ0aGVuIiwic3RhdHVzQ29kZSIsImVycm9yIiwiY2F0Y2giLCJkZWxldGVQb3N0IiwiY3JlYXRlIiwidXNlcklkIiwiY291cnNlSWQiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJqc29uIiwiY29uc29sZSIsImRlbGV0ZUNvdXJzZSIsImdldEJ5SWQiLCJwb3N0VHlwZSIsInRpdGxlIiwiYm9keSIsInBvc3RJZCIsInN0eWxlcyIsInJvb3QiLCJkaXNwbGF5IiwiaW5wdXQiLCJmbGV4R3JvdyIsImJvcmRlciIsImJvcmRlclJhZGl1cyIsInBhZGRpbmciLCJvdXRsaW5lIiwiYm9yZGVyQ29sb3IiLCJib3hTaGFkb3ciLCJJbnB1dCIsImNsYXNzZXMiLCJvbkNoYW5nZSIsInZhbHVlIiwicGxhY2Vob2xkZXIiLCJkaXNhYmxlZCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJmdW5jIiwib25lT2ZUeXBlIiwic3RyaW5nIiwibnVtYmVyIiwiYm9vbCIsImdyaWRDb250YWluZXIiLCJuYW1lIiwiY3Vyc29yIiwidGV4dERlY29yYXRpb24iLCJ1c2VybmFtZSIsInBvc3QiLCJjYXJkSGVhZGVyIiwiY2FyZCIsImF2YXRhciIsImNhcmRNZWRpYSIsIm1lZGlhIiwiaGVpZ2h0IiwiY2FyZE1lZGlhSW1hZ2UiLCJ3aWR0aCIsIm1hcmdpbiIsIm1hcmdpbkJvdHRvbSIsImZsZXgiLCJDb3Vyc2UiLCJwcm9wcyIsIm9uTW91c2VFbnRlciIsInNldFN0YXRlIiwiaG92ZXIiLCJvbk1vdXNlTGVhdmUiLCJvbkNsaWNrQ29udGVudCIsImhpc3RvcnkiLCJwdXNoIiwiZGVsZXRlRGlhbG9nT3BlbiIsImRlbGV0ZURpYWxvZyIsImRlbGV0ZURpYWxvZ0Nsb3NlIiwiZGVsZXRlQ291cnNlQ2xpY2siLCJlbGVtZW50cyIsInVzZXIiLCJsb2ciLCJhY3Rpb25Db3Vyc2VEZWxldGUiLCJzdGF0ZSIsImNvdXJzZXMiLCJhdXRob3JJZCIsImF1dGhvciIsImFjdGlvbkdldEVsZW1lbnRCeUlkIiwiaXNTaWduZWRJbiIsImNvdXJzZSIsImRlc2NyaXB0aW9uIiwiY292ZXIiLCJtYXBTdGF0ZVRvUHJvcHMiLCJtYXBEaXNwYXRjaFRvUHJvcHMiLCJUYWJQb3N0cyIsImVsZW1lbnQiLCJwb3N0cyIsInRoZW1lIiwic3ViaGVhZGVyIiwiZmF2b3JpdGVJY29uQWN0aXZlIiwiY29sb3IiLCJiYWRnZSIsInNwYWNpbmciLCJ1bml0IiwiY29udGVudCIsInRleHRBbGlnbiIsIlBvc3QiLCJvbkNsaWNrRmF2b3JpdGUiLCJmYXZvcml0ZSIsIm9uQ2xpY2tQb3N0IiwiZGVsZXRlUG9zdENsaWNrIiwiYWN0aW9uUG9zdERlbGV0ZSIsImdldEVsZW1lbnRCeUlkIiwiUG9zdENyZWF0ZSIsIm9uRm9jdXMiLCJhY3RpdmUiLCJ0aXRsZUxlbmd0aCIsInZhbGlkIiwibWVzc2FnZSIsImJpbmQiLCJvblN1Ym1pdCIsImUiLCJ0YXJnZXQiLCJsZW5ndGgiLCJwcmV2ZW50RGVmYXVsdCIsInNldFRpbWVvdXQiLCJhY3Rpb25Qb3N0Q3JlYXRlIiwiVGltZWxpbmUiLCJtYXAiLCJwIiwiYyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHLFlBQVk7QUFDZjtBQUNBLG9DOzs7Ozs7Ozs7Ozs7O0FDcEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQzs7Ozs7Ozs7Ozs7OztBQ1RBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtJQUFrSTs7QUFFcEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwwQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHFGQUFxRjs7QUFFdkk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNMQUFzTDs7QUFFeE87QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlKQUF5Sjs7QUFFM007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlOQUF5Tjs7QUFFM1E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELCtXQUErVzs7QUFFamE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx3Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1FQUFtRTs7QUFFckg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwyQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixtUEFBNEk7O0FBRTVJOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLG9HQUFvRzs7QUFFcEc7QUFDQTtBQUNBLGdDQUFnQyx1QkFBdUI7QUFDdkQ7QUFDQTtBQUNBO0FBQ0EsV0FBVyw0QkFBNEI7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxtQkFBbUIsUzs7Ozs7Ozs7Ozs7OztBQ25LeEU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEpBQTBKO0FBQzFKLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELG9CQUFvQixVOzs7Ozs7Ozs7Ozs7O0FDaFV6RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSw0QkFBNEIsZ0VBQWdFO0FBQzVGO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxTQUFTLDRCQUE0QjtBQUNyQztBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELHFEQUFxRCwyQkFBMkIsaUI7Ozs7Ozs7Ozs7Ozs7QUNoSGhGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBLDRCQUE0QixnRUFBZ0U7QUFDNUY7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCxxREFBcUQsMkJBQTJCLGlCOzs7Ozs7Ozs7Ozs7O0FDdkZoRjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0EsbUNBQW1DO0FBQ25DO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSw0QkFBNEIsZ0VBQWdFO0FBQzVGO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QscURBQXFELCtCQUErQixxQjs7Ozs7Ozs7Ozs7OztBQ3ZGcEY7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsZ0NBQWdDLGdFQUFnRTtBQUNoRztBQUNBO0FBQ0EsV0FBVyxnQkFBZ0I7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCx5QkFBeUIsZTs7Ozs7Ozs7Ozs7OztBQzVIOUU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQzVEN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUZBQXFGO0FBQ3JGO0FBQ0E7OztBQUdBO0FBQ0EsOEVBQThFLG1FQUFtRTtBQUNqSjs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNoRUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQSxrR0FBa0c7O0FBRWxHO0FBQ0E7QUFDQSxnQ0FBZ0Msa0RBQWtEO0FBQ2xGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EscURBQXFELHNCQUFzQixZOzs7Ozs7Ozs7Ozs7O0FDeEkzRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGFBQWEsT0FBTztBQUNwQjtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLGVBQWU7QUFDdEM7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsZ0JBQWdCO0FBQ3ZDO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsZ0NBQWdDLGdFQUFnRTtBQUNoRztBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLDBHQUEwRztBQUMxRyxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQkFBcUIsZUFBZTtBQUNwQztBQUNBO0FBQ0E7O0FBRUEscURBQXFELGdDQUFnQyxTOzs7Ozs7Ozs7Ozs7O0FDamxCckY7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUZBQW1GO0FBQ25GO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsdUJBQXVCLHVCQUF1QjtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHFCQUFxQix1QkFBdUI7QUFDNUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxzQkFBc0I7O0FBRXRCO0FBQ0E7O0FBRUEscUM7Ozs7Ozs7Ozs7Ozs7QUN0SkE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsbUVBQW1FLGFBQWE7QUFDaEY7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRCx5Qjs7Ozs7Ozs7Ozs7OztBQ3JMQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRTs7Ozs7Ozs7Ozs7OztBQ2hCQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsMkNBQTJDOztBQUUzQztBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtRDs7Ozs7Ozs7Ozs7OztBQ3BOQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNILEM7Ozs7Ozs7Ozs7Ozs7QUNoREE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDhDQUE4QyxpQkFBaUIscUJBQXFCLG9DQUFvQyw2REFBNkQsb0JBQW9CLEVBQUUsZUFBZTs7QUFFMU4saURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0ZBQWdGOztBQUVoRjs7QUFFQSxnRkFBZ0YsZUFBZTs7QUFFL0YseURBQXlELFVBQVUsdURBQXVEO0FBQzFIOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSx1Qjs7Ozs7Ozs7Ozs7OztBQzdHQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDRDQUE0Qzs7QUFFNUM7O0FBRUE7O0FBRUE7O0FBRUEsc0RBQXNELGlFQUFpRTtBQUN2SDs7QUFFQTtBQUNBLG9KQUFvSjs7QUFFcEosaU1BQWlNOztBQUVqTSwyTEFBMkw7QUFDM0w7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCOztBQUVqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCOzs7Ozs7Ozs7Ozs7O0FDdkpBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG9FQUFvRTs7QUFFcEU7O0FBRUE7QUFDQTtBQUNBLHlCQUF5Qjs7QUFFekI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDhDQUE4Qzs7QUFFOUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHdDQUF3QyxtREFBbUQ7QUFDM0Y7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxJQUFJO0FBQ1Q7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQzVFQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsOENBQThDLGlCQUFpQixxQkFBcUIsb0NBQW9DLDZEQUE2RCxvQkFBb0IsRUFBRSxlQUFlOztBQUUxTjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwyREFBMkQ7QUFDM0QsbUVBQW1FLHdDQUF3QywyQkFBMkI7QUFDdEksT0FBTyxFQUFFO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0NBOztBQVlBOzs7Ozs7OztrQkFRZSxVQUFDQSxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsNEJBQUYsRUFBdUJELGdCQUF2QixFQUEvQjtBQUFBLEMsRUF6QmY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRUE7O0FBTUE7Ozs7OztBQUVBOzs7Ozs7QUFWQTs7QUFnQkEsSUFBTUUsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFDRixPQUFEO0FBQUEsU0FBYyxFQUFFQyxpQ0FBRixFQUEyQkQsZ0JBQTNCLEVBQWQ7QUFBQSxDQUF4Qjs7QUFFQTs7Ozs7Ozs7Ozs7OztBQWFBLElBQU1HLG9CQUFvQixTQUFwQkEsaUJBQW9CLENBQUNILE9BQUQ7QUFBQSxTQUFjLEVBQUVDLG1DQUFGLEVBQTZCRCxnQkFBN0IsRUFBZDtBQUFBLENBQTFCOztBQUVBOzs7Ozs7OztBQVFBLElBQU1JLGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ0osT0FBRDtBQUFBLFNBQWMsRUFBRUMsaUNBQUYsRUFBMkJELGdCQUEzQixFQUFkO0FBQUEsQ0FBeEI7O0FBRUE7Ozs7Ozs7QUFPQSxJQUFNSyxNQUFNLFNBQU5BLEdBQU07QUFBQSxNQUFHQyxLQUFILFFBQUdBLEtBQUg7QUFBQSxNQUFVQyxFQUFWLFFBQVVBLEVBQVY7QUFBQSxTQUFtQixVQUFDQyxRQUFELEVBQWM7QUFDM0NBLGFBQVNOLGdCQUFnQixFQUFFSyxNQUFGLEVBQWhCLENBQVQ7O0FBRUEsMkJBQVcsRUFBRUQsWUFBRixFQUFTQyxNQUFULEVBQVgsRUFDR0UsSUFESCxDQUNRLGlCQUFvQztBQUFBLFVBQWpDQyxVQUFpQyxTQUFqQ0EsVUFBaUM7QUFBQSxVQUFyQkMsS0FBcUIsU0FBckJBLEtBQXFCO0FBQUEsVUFBZFgsT0FBYyxTQUFkQSxPQUFjOztBQUN4QyxVQUFJVSxjQUFjLEdBQWxCLEVBQXVCO0FBQ3JCRixpQkFBU0osZ0JBQWdCLEVBQUVHLE1BQUYsRUFBTUcsc0JBQU4sRUFBa0JDLFlBQWxCLEVBQWhCLENBQVQ7O0FBRUE7QUFDRDs7QUFFREgsZUFBU0wsMkNBQW9CSSxNQUFwQixFQUF3Qkcsc0JBQXhCLElBQXVDVixPQUF2QyxFQUFUO0FBQ0QsS0FUSCxFQVVHWSxLQVZILENBVVMsaUJBQTJCO0FBQUEsVUFBeEJGLFVBQXdCLFNBQXhCQSxVQUF3QjtBQUFBLFVBQVpDLEtBQVksU0FBWkEsS0FBWTs7QUFDaENILGVBQVNKLGdCQUFnQixFQUFFRyxNQUFGLEVBQU1HLHNCQUFOLEVBQWtCQyxZQUFsQixFQUFoQixDQUFUO0FBQ0QsS0FaSDtBQWFELEdBaEJXO0FBQUEsQ0FBWjs7a0JBa0JlTixHOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0RmOztBQVlBOzs7Ozs7O0FBT0EsSUFBTVEsYUFBYSxTQUFiQSxVQUFhLENBQUNiLE9BQUQ7QUFBQSxTQUErQjtBQUNoREMsNEJBRGdEO0FBRWhERDtBQUZnRCxHQUEvQjtBQUFBLENBQW5CLEMsQ0F4QkE7Ozs7O2tCQTZCZWEsVTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hCZjs7QUFpQkE7Ozs7Ozs7Ozs7Ozs7O0FBY0EsSUFBTUMsU0FBUyxTQUFUQSxNQUFTLENBQUNkLE9BQUQ7QUFBQSxTQUErQixFQUFFQyx3QkFBRixFQUFxQkQsZ0JBQXJCLEVBQS9CO0FBQUEsQ0FBZixDLENBcENBOzs7OztrQkFzQ2VjLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4QmY7Ozs7Ozs7Ozs7OztBQWRBOzs7Ozs7c0ZBMEJBO0FBQUEsUUFBOEJSLEtBQTlCLFNBQThCQSxLQUE5QjtBQUFBLFFBQXFDUyxNQUFyQyxTQUFxQ0EsTUFBckM7QUFBQSxRQUE2Q0MsUUFBN0MsU0FBNkNBLFFBQTdDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVDLGVBRlYsa0NBRXFDRixNQUZyQyxpQkFFdURDLFFBRnZEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsUUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlZjtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVWdCLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xiLDBCQUFZYSxNQURQO0FBRUxaLHFCQUFPYTtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJRyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRZixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlZ0IsWTs7Ozs7QUFyQmY7Ozs7QUFDQTs7OztrQkF1RGVBLFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RGY7Ozs7Ozs7QUFMQTs7O3NGQVlBO0FBQUEsUUFBeUJyQixLQUF6QixTQUF5QkEsS0FBekI7QUFBQSxRQUFnQ0MsRUFBaEMsU0FBZ0NBLEVBQWhDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVVLGVBRlYscUNBRXdDVixFQUZ4QztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNVSxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZWY7QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVnQixlQUpWO0FBYVlDLGtCQWJaLEdBYW1DRCxHQWJuQyxDQWFZQyxNQWJaLEVBYW9CQyxVQWJwQixHQWFtQ0YsR0FibkMsQ0Fhb0JFLFVBYnBCOztBQUFBLGtCQWVRRCxVQUFVLEdBZmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdCYTtBQUNMYiwwQkFBWWEsTUFEUDtBQUVMWixxQkFBT2E7QUFGRixhQWhCYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0J1QkYsSUFBSUcsSUFBSixFQXRCdkI7O0FBQUE7QUFzQlVBLGdCQXRCVjtBQUFBLHdFQXdCZ0JBLElBeEJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMEJJQyxvQkFBUWYsS0FBUjs7QUExQkosNkNBNEJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWlCLE87Ozs7O0FBVmY7Ozs7QUFDQTs7OztrQkE0Q2VBLE87Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkNmOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFSQTs7Ozs7O3NGQTZCQTtBQUFBLFFBQXdCdEIsS0FBeEIsU0FBd0JBLEtBQXhCO0FBQUEsUUFBK0JTLE1BQS9CLFNBQStCQSxNQUEvQjtBQUFBLFFBQXVDYyxRQUF2QyxTQUF1Q0EsUUFBdkM7QUFBQSxRQUFpREMsS0FBakQsU0FBaURBLEtBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVViLGVBRlYsa0NBRXFDRixNQUZyQztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNRSxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLE1BRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZWY7QUFIUixlQUZrQjtBQU8zQnlCLG9CQUFNLHlCQUFlLEVBQUVGLGtCQUFGLEVBQVlDLFlBQVosRUFBZjtBQVBxQixhQUFYLENBSnRCOztBQUFBO0FBSVVSLGVBSlY7QUFjWUMsa0JBZFosR0FjbUNELEdBZG5DLENBY1lDLE1BZFosRUFjb0JDLFVBZHBCLEdBY21DRixHQWRuQyxDQWNvQkUsVUFkcEI7O0FBQUEsa0JBZ0JRRCxVQUFVLEdBaEJsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FpQmE7QUFDTGIsMEJBQVlhLE1BRFA7QUFFTFoscUJBQU9hO0FBRkYsYUFqQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXVCdUJGLElBQUlHLElBQUosRUF2QnZCOztBQUFBO0FBdUJVQSxnQkF2QlY7QUFBQSx3RUF5QmdCQSxJQXpCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTJCSUMsb0JBQVFmLEtBQVI7O0FBM0JKLDZDQTZCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE3Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVHLE07Ozs7O0FBeEJmOzs7O0FBQ0E7Ozs7a0JBMkRlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekRmOzs7Ozs7Ozs7Ozs7QUFSQTs7Ozs7O3NGQW9CQTtBQUFBLFFBQTRCQyxNQUE1QixTQUE0QkEsTUFBNUI7QUFBQSxRQUFvQ1QsS0FBcEMsU0FBb0NBLEtBQXBDO0FBQUEsUUFBMkMwQixNQUEzQyxTQUEyQ0EsTUFBM0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVWYsZUFGVixrQ0FFcUNGLE1BRnJDLGVBRXFEaUIsTUFGckQ7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTWYsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxRQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVmO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVZ0IsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTGIsMEJBQVlhLE1BRFA7QUFFTFoscUJBQU9hO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlHLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFmLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVFLFU7Ozs7O0FBZmY7Ozs7QUFDQTs7OztrQkFpRGVBLFU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoRGY7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNb0IsU0FBUztBQUNiQyxRQUFNO0FBQ0pDLGFBQVM7QUFETCxHQURPO0FBSWJDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTEMsa0JBQWMsS0FIVDtBQUlMQyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RDLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEY7QUFMTjtBQUpNLENBQWYsQyxDQVpBOzs7Ozs7O0FBNkJBLElBQU1DLFFBQVEsU0FBUkEsS0FBUTtBQUFBLE1BQUdDLE9BQUgsUUFBR0EsT0FBSDtBQUFBLE1BQVlDLFFBQVosUUFBWUEsUUFBWjtBQUFBLE1BQXNCQyxLQUF0QixRQUFzQkEsS0FBdEI7QUFBQSxNQUE2QkMsV0FBN0IsUUFBNkJBLFdBQTdCO0FBQUEsTUFBMENDLFFBQTFDLFFBQTBDQSxRQUExQztBQUFBLE1BQW9EaEQsSUFBcEQsUUFBb0RBLElBQXBEO0FBQUEsU0FDWjtBQUFBO0FBQUEsTUFBSyxXQUFXNEMsUUFBUVgsSUFBeEI7QUFDRTtBQUNFLFlBQU1qQyxRQUFRLE1BRGhCO0FBRUUsbUJBQWErQyxlQUFlLE9BRjlCO0FBR0UsYUFBT0QsS0FIVDtBQUlFLGdCQUFVRCxRQUpaO0FBS0UsaUJBQVdELFFBQVFULEtBTHJCO0FBTUUsZ0JBQVUsQ0FBQyxDQUFDYTtBQU5kO0FBREYsR0FEWTtBQUFBLENBQWQ7O0FBYUFMLE1BQU1NLFNBQU4sR0FBa0I7QUFDaEJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBRFY7QUFFaEJOLFlBQVUsb0JBQVVPLElBQVYsQ0FBZUQsVUFGVDtBQUdoQkwsU0FBTyxvQkFBVU8sU0FBVixDQUFvQixDQUFDLG9CQUFVQyxNQUFYLEVBQW1CLG9CQUFVQyxNQUE3QixDQUFwQixFQUEwREosVUFIakQ7QUFJaEJKLGVBQWEsb0JBQVVPLE1BSlA7QUFLaEJ0RCxRQUFNLG9CQUFVc0QsTUFMQTtBQU1oQk4sWUFBVSxvQkFBVVE7QUFOSixDQUFsQjs7a0JBU2UsMEJBQVd4QixNQUFYLEVBQW1CVyxLQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVDZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFNQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7OztBQUVBLElBQU1YLFNBQVM7QUFDYkMsUUFBTTtBQUNKTSxhQUFTO0FBREwsR0FETztBQUlia0IsaUJBQWUsRUFKRjtBQUtiQyxRQUFNO0FBQ0pDLFlBQVEsU0FESjtBQUVKQyxvQkFBZ0I7QUFGWixHQUxPO0FBU2JDLFlBQVU7QUFDUkYsWUFBUSxTQURBO0FBRVJDLG9CQUFnQjtBQUZSLEdBVEc7QUFhYkUsUUFBTTtBQUNKdkIsYUFBUztBQURMLEdBYk87QUFnQmJ3QixjQUFZO0FBQ1Y7QUFEVSxHQWhCQztBQW1CYkMsUUFBTTtBQUNKMUIsa0JBQWM7QUFEVixHQW5CTztBQXNCYjJCLFVBQVE7QUFDTjNCLGtCQUFjO0FBRFIsR0F0Qks7QUF5QmI0QixhQUFXLEVBekJFO0FBMEJiQyxTQUFPO0FBQ0xDLFlBQVE7QUFESCxHQTFCTTtBQTZCYkMsa0JBQWdCO0FBQ2RDLFdBQU87QUFETyxHQTdCSDtBQWdDYkMsVUFBUTtBQUNObEMsWUFBUSxpQ0FERjtBQUVObUMsa0JBQWM7QUFGUixHQWhDSztBQW9DYnBDLFlBQVU7QUFDUnFDLFVBQU07QUFERTtBQXBDRyxDQUFmLEMsQ0F4Q0E7Ozs7Ozs7SUFpRk1DLE07OztBQUNKLGtCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBQUEsVUFxQm5CQyxZQXJCbUIsR0FxQko7QUFBQSxhQUFNLE1BQUtDLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0FyQkk7O0FBQUEsVUFzQm5CQyxZQXRCbUIsR0FzQko7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0F0Qkk7O0FBQUEsVUF3Qm5CRSxjQXhCbUIsR0F3QkYsWUFBTTtBQUFBLFVBQ2JqRSxRQURhLEdBQ0EsTUFBSzRELEtBREwsQ0FDYjVELFFBRGE7OztBQUdyQixZQUFLNEQsS0FBTCxDQUFXTSxPQUFYLENBQW1CQyxJQUFuQixlQUFvQ25FLFFBQXBDO0FBQ0QsS0E1QmtCOztBQUFBLFVBOEJuQm9FLGdCQTlCbUIsR0E4QkEsWUFBTTtBQUN2QixZQUFLTixRQUFMLENBQWMsRUFBRU8sY0FBYyxJQUFoQixFQUFkO0FBQ0QsS0FoQ2tCOztBQUFBLFVBa0NuQkMsaUJBbENtQixHQWtDQyxZQUFNO0FBQ3hCLFlBQUtSLFFBQUwsQ0FBYyxFQUFFTyxjQUFjLEtBQWhCLEVBQWQ7QUFDRCxLQXBDa0I7O0FBQUEsVUFzQ25CRSxpQkF0Q21CLDRFQXNDQztBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsc0NBQ0ksTUFBS1gsS0FBTCxDQUFXWSxRQUFYLENBQW9CQyxJQUR4QixFQUNWbkYsS0FEVSx5QkFDVkEsS0FEVSxFQUNIQyxFQURHLHlCQUNIQSxFQURHO0FBRVZTLHNCQUZVLEdBRUcsTUFBSzRELEtBRlIsQ0FFVjVELFFBRlU7QUFBQTtBQUFBLHFCQUlrQixzQkFBZ0I7QUFDbERWLDRCQURrRDtBQUVsRFMsd0JBQVFSLEVBRjBDO0FBR2xEUztBQUhrRCxlQUFoQixDQUpsQjs7QUFBQTtBQUFBO0FBSVZOLHdCQUpVLFNBSVZBLFVBSlU7QUFJRUMsbUJBSkYsU0FJRUEsS0FKRjs7QUFBQSxvQkFTZEQsZUFBZSxHQVREO0FBQUE7QUFBQTtBQUFBOztBQVVoQjtBQUNBZ0Isc0JBQVFnRSxHQUFSLENBQVkvRSxLQUFaO0FBWGdCOztBQUFBOztBQWVsQixvQkFBS2lFLEtBQUwsQ0FBV2Usa0JBQVgsQ0FBOEIsRUFBRTNFLGtCQUFGLEVBQTlCO0FBQ0Esb0JBQUs4RCxRQUFMLENBQWMsRUFBRU8sY0FBYyxLQUFoQixFQUFkOztBQWhCa0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsS0F0Q0Q7O0FBRWpCLFVBQUtPLEtBQUwsR0FBYTtBQUNYYixhQUFPLEtBREk7QUFFWE0sb0JBQWM7QUFGSCxLQUFiO0FBRmlCO0FBTWxCOzs7O3lDQUVvQjtBQUFBLG1CQUNxQixLQUFLVCxLQUQxQjtBQUFBLFVBQ1hZLFFBRFcsVUFDWEEsUUFEVztBQUFBLFVBQ0RLLE9BREMsVUFDREEsT0FEQztBQUFBLFVBQ1E3RSxRQURSLFVBQ1FBLFFBRFI7QUFBQSwyQkFFV3dFLFNBQVNDLElBRnBCO0FBQUEsVUFFWG5GLEtBRlcsa0JBRVhBLEtBRlc7QUFBQSxVQUVBUyxNQUZBLGtCQUVKUixFQUZJO0FBQUEsVUFJWHVGLFFBSlcsR0FJRUQsUUFBUTdFLFFBQVIsQ0FKRixDQUlYOEUsUUFKVzs7QUFLbkIsVUFBTUMsU0FBU1AsU0FBU00sUUFBVCxDQUFmOztBQUVBO0FBQ0EsVUFBSS9FLFdBQVcrRSxRQUFYLElBQXVCLENBQUNDLE1BQTVCLEVBQW9DO0FBQ2xDLGFBQUtuQixLQUFMLENBQVdvQixvQkFBWCxDQUFnQyxFQUFFekYsSUFBSXVGLFFBQU4sRUFBZ0J4RixZQUFoQixFQUFoQztBQUNEO0FBQ0Y7Ozs2QkFzQ1E7QUFBQSxvQkFDMEMsS0FBS3NFLEtBRC9DO0FBQUEsVUFDQy9CLE9BREQsV0FDQ0EsT0FERDtBQUFBLFVBQ1VnRCxPQURWLFdBQ1VBLE9BRFY7QUFBQSxVQUNtQkwsUUFEbkIsV0FDbUJBLFFBRG5CO0FBQUEsVUFDNkJ4RSxRQUQ3QixXQUM2QkEsUUFEN0I7QUFBQSxVQUdDeUUsSUFIRCxHQUdVRCxRQUhWLENBR0NDLElBSEQ7QUFBQSxVQUlDUSxVQUpELEdBSTRCUixJQUo1QixDQUlDUSxVQUpEO0FBQUEsVUFJaUJsRixNQUpqQixHQUk0QjBFLElBSjVCLENBSWFsRixFQUpiOzs7QUFNUCxVQUFNMkYsU0FBU0wsUUFBUTdFLFFBQVIsQ0FBZjs7QUFFQSxVQUFJLE9BQU9rRixNQUFQLEtBQWtCLFdBQXRCLEVBQW1DO0FBQ2pDLGVBQU8sMENBQVA7QUFDRDs7QUFWTSxVQVlDcEUsS0FaRCxHQVl5Q29FLE1BWnpDLENBWUNwRSxLQVpEO0FBQUEsVUFZUXFFLFdBWlIsR0FZeUNELE1BWnpDLENBWVFDLFdBWlI7QUFBQSxVQVlxQkwsUUFackIsR0FZeUNJLE1BWnpDLENBWXFCSixRQVpyQjtBQUFBLFVBWStCTSxLQVovQixHQVl5Q0YsTUFaekMsQ0FZK0JFLEtBWi9COztBQWFQLFVBQU1MLFNBQVNELGFBQWEvRSxNQUFiLEdBQXNCMEUsSUFBdEIsR0FBNkJELFNBQVNNLFFBQVQsQ0FBNUM7O0FBRUEsYUFDRTtBQUFBO0FBQUE7QUFDRSxxQkFBV2pELFFBQVFYLElBRHJCO0FBRUUsd0JBQWMsS0FBSzJDLFlBRnJCO0FBR0Usd0JBQWMsS0FBS0c7QUFIckI7QUFLRTtBQUFBO0FBQUEsWUFBTSxRQUFRLEtBQUtZLEtBQUwsQ0FBV2IsS0FBekIsRUFBZ0MsV0FBV2xDLFFBQVFvQixJQUFuRDtBQUNFO0FBQ0Usb0JBQ0U7QUFBQTtBQUFBLGdCQUFPLFdBQVdwQixRQUFRcUIsTUFBMUIsRUFBa0MsV0FBVyxDQUE3QztBQUNFO0FBQ0UscUJBQUs2QixPQUFPcEMsSUFEZDtBQUVFLHFCQUNFb0MsT0FBTzdCLE1BQVAsSUFDQTtBQUpKO0FBREYsYUFGSjtBQVlFLG9CQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxrQkFBUyxPQUFNLGFBQWYsRUFBNkIsV0FBVSxRQUF2QztBQUNFO0FBQUE7QUFBQSxvQkFBWSxjQUFaO0FBQ0U7QUFERjtBQURGO0FBREYsYUFiSjtBQXFCRSxtQkFDRTtBQUFBO0FBQUEsZ0JBQU0sV0FBUzZCLE9BQU9qQyxRQUF0QixFQUFrQyxXQUFXakIsUUFBUWMsSUFBckQ7QUFDR29DLHFCQUFPcEM7QUFEVixhQXRCSjtBQTBCRSx1QkFDRTtBQUFBO0FBQUEsZ0JBQU0sV0FBU29DLE9BQU9qQyxRQUF0QixFQUFrQyxXQUFXakIsUUFBUWlCLFFBQXJEO0FBQUEsb0JBQ09pQyxPQUFPakM7QUFEZDtBQTNCSixZQURGO0FBaUNFO0FBQ0UsdUJBQVdqQixRQUFRdUIsS0FEckI7QUFFRSxtQkFDRWdDLFNBQ0E7QUFKSixZQWpDRjtBQXdDRTtBQUFBO0FBQUEsY0FBYSxTQUFTLEtBQUtuQixjQUEzQjtBQUNFO0FBQUE7QUFBQSxnQkFBWSxNQUFLLE9BQWpCLEVBQXlCLGtCQUF6QjtBQUNHbkQ7QUFESCxhQURGO0FBSUcsbUJBQU9xRSxXQUFQLEtBQXVCLFFBQXZCLEdBQ0M7QUFBQTtBQUFBLGdCQUFZLE1BQUssT0FBakIsRUFBeUIsa0JBQXpCO0FBQ0dBO0FBREgsYUFERCxHQUlHO0FBUk4sV0F4Q0Y7QUFrREU7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLGdCQUFTLE9BQU0sa0JBQWYsRUFBa0MsV0FBVSxRQUE1QztBQUNFO0FBQUE7QUFBQSxrQkFBWSxjQUFXLGtCQUF2QixFQUEwQyxjQUExQztBQUNFO0FBREY7QUFERixhQURGO0FBTUU7QUFBQTtBQUFBLGdCQUFTLE9BQU0sVUFBZixFQUEwQixXQUFVLFFBQXBDO0FBQ0U7QUFBQTtBQUFBLGtCQUFZLGNBQVcsWUFBdkIsRUFBb0MsY0FBcEM7QUFDRTtBQURGO0FBREYsYUFORjtBQVdHRiwwQkFBY2xGLFdBQVdnRixPQUFPeEYsRUFBaEMsR0FDQztBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsa0JBQVksY0FBVyxRQUF2QixFQUFnQyxTQUFTLEtBQUs2RSxnQkFBOUM7QUFDRTtBQURGLGVBREY7QUFJRTtBQUFBO0FBQUE7QUFDRSx3QkFBTSxLQUFLUSxLQUFMLENBQVdQLFlBRG5CO0FBRUUsMkJBQVMsS0FBS0MsaUJBRmhCO0FBR0UsaURBQTJCdEUsUUFBM0I7QUFIRjtBQUtFO0FBQUE7QUFBQSxvQkFBYSxnQkFBY0EsUUFBZCxtQkFBYjtBQUFBO0FBQUEsaUJBTEY7QUFRRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsc0JBQVEsU0FBUyxLQUFLc0UsaUJBQXRCLEVBQXlDLE9BQU0sU0FBL0M7QUFBQTtBQUFBLG1CQURGO0FBSUU7QUFBQTtBQUFBO0FBQ0UsK0JBQVMsS0FBS0MsaUJBRGhCO0FBRUUsNkJBQU0sU0FGUjtBQUdFO0FBSEY7QUFBQTtBQUFBO0FBSkY7QUFSRjtBQUpGLGFBREQsR0EyQkcsSUF0Q047QUF1Q0UsbURBQUssV0FBVzFDLFFBQVFSLFFBQXhCLEdBdkNGO0FBd0NFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE9BQXZCLEVBQStCLGNBQS9CO0FBQ0U7QUFERjtBQXhDRjtBQWxERjtBQUxGLE9BREY7QUF1R0Q7Ozs7O0FBR0hzQyxPQUFPekIsU0FBUCxHQUFtQjtBQUNqQmdDLFdBQVMsb0JBQVUvQixNQUFWLENBQWlCQyxVQURUOztBQUdqQlAsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFIVDs7QUFLakJvQyxZQUFVLG9CQUFVckMsTUFBVixDQUFpQkMsVUFMVjtBQU1qQnlDLFdBQVMsb0JBQVUxQyxNQUFWLENBQWlCQyxVQU5UO0FBT2pCcEMsWUFBVSxvQkFBVXdDLE1BQVYsQ0FBaUJKLFVBUFY7O0FBU2pCNEMsd0JBQXNCLG9CQUFVM0MsSUFBVixDQUFlRCxVQVRwQjtBQVVqQnVDLHNCQUFvQixvQkFBVXRDLElBQVYsQ0FBZUQ7QUFWbEIsQ0FBbkI7O0FBYUEsSUFBTWlELGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHYixRQUFILFNBQUdBLFFBQUg7QUFBQSxNQUFhSyxPQUFiLFNBQWFBLE9BQWI7QUFBQSxTQUE0QixFQUFFTCxrQkFBRixFQUFZSyxnQkFBWixFQUE1QjtBQUFBLENBQXhCO0FBQ0EsSUFBTVMscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQzlGLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFd0YsMkNBREY7QUFFRUw7QUFGRixHQURGLEVBS0VuRixRQUxGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVNlLHlCQUFRNkYsZUFBUixFQUF5QkMsa0JBQXpCLEVBQ2IsMEJBQVdyRSxNQUFYLEVBQW1CLDBCQUFXMEMsTUFBWCxDQUFuQixDQURhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3pSZjs7OztBQUNBOzs7O0FBRUE7O0FBQ0E7O0FBRUE7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFaQTs7QUFjQSxJQUFNMUMsU0FBUztBQUNiQyxRQUFNO0FBRE8sQ0FBZjs7SUFJTXFFLFE7OztBQUNKLG9CQUFZM0IsS0FBWixFQUFtQjtBQUFBOztBQUFBLDBJQUNYQSxLQURXOztBQUVqQixVQUFLZ0IsS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQSxtQkFDbUMsS0FBS2hCLEtBRHhDO0FBQUEsVUFDQy9CLE9BREQsVUFDQ0EsT0FERDtBQUFBLFVBQ1U0QyxJQURWLFVBQ1VBLElBRFY7QUFBQSxVQUNnQmUsT0FEaEIsVUFDZ0JBLE9BRGhCO0FBQUEsVUFDeUJDLEtBRHpCLFVBQ3lCQSxLQUR6QjtBQUFBLFVBRUNSLFVBRkQsR0FFb0JSLElBRnBCLENBRUNRLFVBRkQ7QUFBQSxVQUVhMUYsRUFGYixHQUVvQmtGLElBRnBCLENBRWFsRixFQUZiOzs7QUFJUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdzQyxRQUFRWCxJQUF4QjtBQUNFO0FBQUE7QUFBQSxZQUFNLGVBQU4sRUFBZ0IsU0FBUSxRQUF4QixFQUFpQyxTQUFTLENBQTFDO0FBQ0U7QUFBQTtBQUFBLGNBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLENBQXZCLEVBQTBCLElBQUksQ0FBOUIsRUFBaUMsSUFBSSxDQUFyQyxFQUF3QyxJQUFJLENBQTVDO0FBQ0crRCwwQkFBYzFGLE9BQU9pRyxRQUFRakcsRUFBN0IsR0FBa0MseURBQWxDLEdBQW1ELElBRHREO0FBRUUsZ0VBQVUsTUFBTSxPQUFoQixFQUF5QixPQUFPa0csS0FBaEMsRUFBdUMsU0FBU0QsT0FBaEQ7QUFGRjtBQURGO0FBREYsT0FERjtBQVVEOzs7OztBQUdIRCxTQUFTckQsU0FBVCxHQUFxQjtBQUNuQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFEUDs7QUFHbkJxQyxRQUFNLG9CQUFVdEMsTUFBVixDQUFpQkMsVUFISjtBQUluQm9ELFdBQVMsb0JBQVVyRCxNQUFWLENBQWlCQyxVQUpQO0FBS25CcUQsU0FBTyxvQkFBVXRELE1BQVYsQ0FBaUJDO0FBTEwsQ0FBckI7O0FBUUEsSUFBTWlELGtCQUFrQixTQUFsQkEsZUFBa0IsQ0FBQ1QsS0FBRDtBQUFBLFNBQVdBLEtBQVg7QUFBQSxDQUF4QjtBQUNBLElBQU1VLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUM5RixRQUFEO0FBQUEsU0FBYywrQkFBbUIsRUFBbkIsRUFBdUJBLFFBQXZCLENBQWQ7QUFBQSxDQUEzQjs7a0JBRWUseUJBQVE2RixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYix3QkFBV3JFLE1BQVgsRUFBbUJzRSxRQUFuQixDQURhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdDZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQU1BOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUdBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTXRFLFNBQVMsU0FBVEEsTUFBUyxDQUFDeUUsS0FBRDtBQUFBLFNBQVk7QUFDekJ4RSxVQUFNO0FBQ0pNLGVBQVM7QUFETCxLQURtQjtBQUl6QnlCLFVBQU07QUFDSjFCLG9CQUFjO0FBRFYsS0FKbUI7QUFPekIyQixZQUFRO0FBQ04zQixvQkFBYztBQURSLEtBUGlCO0FBVXpCVCxXQUFPO0FBQ0w7QUFDQThCLGNBQVEsU0FGSDtBQUdMQyxzQkFBZ0I7QUFIWCxLQVZrQjtBQWV6QjhDLGVBQVc7QUFDVDtBQUNBL0MsY0FBUSxTQUZDO0FBR1RDLHNCQUFnQjtBQUhQLEtBZmM7QUFvQnpCTyxXQUFPO0FBQ0xDLGNBQVE7QUFESCxLQXBCa0I7QUF1QnpCdUMsd0JBQW9CO0FBQ2xCQyxhQUFPO0FBRFcsS0F2Qks7QUEwQnpCeEUsY0FBVTtBQUNScUMsWUFBTTtBQURFLEtBMUJlO0FBNkJ6Qm9DLFdBQU87QUFDTHRDLHFCQUFha0MsTUFBTUssT0FBTixDQUFjQyxJQUFkLEdBQXFCLENBQWxDLE9BREs7QUFFTEgsYUFBTztBQUZGLEtBN0JrQjtBQWlDekJyQyxZQUFRO0FBQ05sQyxjQUFRLGlDQURGO0FBRU5tQyxvQkFBYztBQUZSLEtBakNpQjtBQXFDekJ3QyxhQUFTO0FBQ1BDLGlCQUFXO0FBREo7QUFyQ2dCLEdBQVo7QUFBQSxDQUFmOztBQU5BO0FBekNBOzs7Ozs7O0lBeUZNQyxJOzs7QUFDSixnQkFBWXZDLEtBQVosRUFBbUI7QUFBQTs7QUFBQTs7QUFBQSxrSUFDWEEsS0FEVzs7QUFBQSxVQXVCbkJ3QyxlQXZCbUIsNEVBdUJEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNSQyxzQkFEUSxHQUNLLE1BQUt6QixLQURWLENBQ1J5QixRQURROzs7QUFHaEIsb0JBQUt2QyxRQUFMLENBQWMsRUFBRXVDLFVBQVUsQ0FBQ0EsUUFBYixFQUFkOztBQUhnQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxLQXZCQzs7QUFBQSxVQTZCbkJ4QyxZQTdCbUIsR0E2Qko7QUFBQSxhQUFNLE1BQUtDLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0E3Qkk7O0FBQUEsVUE4Qm5CQyxZQTlCbUIsR0E4Qko7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0E5Qkk7O0FBQUEsVUFnQ25CdUMsV0FoQ21CLEdBZ0NMO0FBQUEsYUFBTSxNQUFLMUMsS0FBTCxDQUFXTSxPQUFYLENBQW1CQyxJQUFuQixhQUFrQyxNQUFLUCxLQUFMLENBQVc1QyxNQUE3QyxDQUFOO0FBQUEsS0FoQ0s7O0FBQUEsVUFrQ25Cb0QsZ0JBbENtQixHQWtDQSxZQUFNO0FBQ3ZCLFlBQUtOLFFBQUwsQ0FBYyxFQUFFTyxjQUFjLElBQWhCLEVBQWQ7QUFDRCxLQXBDa0I7O0FBQUEsVUFzQ25CQyxpQkF0Q21CLEdBc0NDLFlBQU07QUFDeEIsWUFBS1IsUUFBTCxDQUFjLEVBQUVPLGNBQWMsS0FBaEIsRUFBZDtBQUNELEtBeENrQjs7QUFBQSxVQTBDbkJrQyxlQTFDbUIsNEVBMENEO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxzQ0FDTSxNQUFLM0MsS0FBTCxDQUFXWSxRQUFYLENBQW9CQyxJQUQxQixFQUNSbkYsS0FEUSx5QkFDUkEsS0FEUSxFQUNEQyxFQURDLHlCQUNEQSxFQURDO0FBRVJ5QixvQkFGUSxHQUVHLE1BQUs0QyxLQUZSLENBRVI1QyxNQUZRO0FBQUE7QUFBQSxxQkFJb0Isc0JBQWM7QUFDaEQxQiw0QkFEZ0Q7QUFFaERTLHdCQUFRUixFQUZ3QztBQUdoRHlCO0FBSGdELGVBQWQsQ0FKcEI7O0FBQUE7QUFBQTtBQUlSdEIsd0JBSlEsU0FJUkEsVUFKUTtBQUlJQyxtQkFKSixTQUlJQSxLQUpKOztBQUFBLG9CQVVaRCxlQUFlLEdBVkg7QUFBQTtBQUFBO0FBQUE7O0FBV2Q7QUFDQWdCLHNCQUFRZ0UsR0FBUixDQUFZL0UsS0FBWjtBQVpjOztBQUFBOztBQWdCaEIsb0JBQUtpRSxLQUFMLENBQVc0QyxnQkFBWCxDQUE0QixFQUFFeEYsY0FBRixFQUE1QjtBQUNBLG9CQUFLOEMsUUFBTCxDQUFjLEVBQUVPLGNBQWMsS0FBaEIsRUFBZDs7QUFqQmdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEtBMUNDOztBQUVqQixVQUFLTyxLQUFMLEdBQWE7QUFDWGIsYUFBTyxLQURJO0FBRVhzQyxnQkFBVSxLQUZDO0FBR1hoQyxvQkFBYztBQUhILEtBQWI7QUFGaUI7QUFPbEI7O0FBRUQ7Ozs7O3lDQUNxQjtBQUFBLG1CQUNpQixLQUFLVCxLQUR0QjtBQUFBLFVBQ1hZLFFBRFcsVUFDWEEsUUFEVztBQUFBLFVBQ0RpQixLQURDLFVBQ0RBLEtBREM7QUFBQSxVQUNNekUsTUFETixVQUNNQSxNQUROO0FBQUEsMkJBRVd3RCxTQUFTQyxJQUZwQjtBQUFBLFVBRVhuRixLQUZXLGtCQUVYQSxLQUZXO0FBQUEsVUFFQVMsTUFGQSxrQkFFSlIsRUFGSTtBQUFBLFVBR1h1RixRQUhXLEdBR0VXLE1BQU16RSxNQUFOLENBSEYsQ0FHWDhELFFBSFc7OztBQUtuQixVQUFNQyxTQUFTUCxTQUFTTSxRQUFULENBQWY7O0FBRUE7QUFDQSxVQUFJL0UsV0FBVytFLFFBQVgsSUFBdUIsQ0FBQ0MsTUFBNUIsRUFBb0M7QUFDbEMsYUFBS25CLEtBQUwsQ0FBVzZDLGNBQVgsQ0FBMEIsRUFBRWxILElBQUl1RixRQUFOLEVBQWdCeEYsWUFBaEIsRUFBMUI7QUFDRDtBQUNGOzs7NkJBeUNRO0FBQUEsb0JBQ3NDLEtBQUtzRSxLQUQzQztBQUFBLFVBQ0MvQixPQURELFdBQ0NBLE9BREQ7QUFBQSxVQUNVNEQsS0FEVixXQUNVQSxLQURWO0FBQUEsVUFDaUJqQixRQURqQixXQUNpQkEsUUFEakI7QUFBQSxVQUMyQnhELE1BRDNCLFdBQzJCQSxNQUQzQjtBQUFBLFVBRUN5RCxJQUZELEdBRVVELFFBRlYsQ0FFQ0MsSUFGRDtBQUFBLFVBR0NRLFVBSEQsR0FHNEJSLElBSDVCLENBR0NRLFVBSEQ7QUFBQSxVQUdpQmxGLE1BSGpCLEdBRzRCMEUsSUFINUIsQ0FHYWxGLEVBSGI7OztBQUtQLFVBQU13RCxPQUFPMEMsTUFBTXpFLE1BQU4sQ0FBYjs7QUFMTyxVQU9DcUYsUUFQRCxHQU9jLEtBQUt6QixLQVBuQixDQU9DeUIsUUFQRDs7O0FBU1AsVUFBSSxPQUFPdEQsSUFBUCxLQUFnQixXQUFwQixFQUFpQztBQUMvQixlQUFPLDBDQUFQO0FBQ0Q7O0FBWE0sVUFhQ3FDLEtBYkQsR0FheUNyQyxJQWJ6QyxDQWFDcUMsS0FiRDtBQUFBLFVBYVF0RSxLQWJSLEdBYXlDaUMsSUFiekMsQ0FhUWpDLEtBYlI7QUFBQSxVQWFlcUUsV0FiZixHQWF5Q3BDLElBYnpDLENBYWVvQyxXQWJmO0FBQUEsVUFhNEJMLFFBYjVCLEdBYXlDL0IsSUFiekMsQ0FhNEIrQixRQWI1Qjs7QUFjUCxVQUFNQyxTQUFTRCxhQUFhL0UsTUFBYixHQUFzQjBFLElBQXRCLEdBQTZCRCxTQUFTTSxRQUFULENBQTVDOztBQUVBLGFBQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQVdqRCxRQUFRWCxJQURyQjtBQUVFLHdCQUFjLEtBQUsyQyxZQUZyQjtBQUdFLHdCQUFjLEtBQUtHO0FBSHJCO0FBS0U7QUFBQTtBQUFBLFlBQU0sUUFBUSxLQUFLWSxLQUFMLENBQVdiLEtBQXpCLEVBQWdDLFdBQVdsQyxRQUFRb0IsSUFBbkQ7QUFDRTtBQUNFLG9CQUNFO0FBQUE7QUFBQSxnQkFBTyxXQUFXcEIsUUFBUXFCLE1BQTFCLEVBQWtDLFdBQVcsQ0FBN0M7QUFDRTtBQUFBO0FBQUEsa0JBQU0sV0FBUzZCLE9BQU9qQyxRQUF0QjtBQUNFO0FBQ0UsdUJBQUtpQyxPQUFPcEMsSUFEZDtBQUVFLHVCQUFLO0FBRlA7QUFERjtBQURGLGFBRko7QUFXRSxvQkFDRTtBQUFBO0FBQUEsZ0JBQVMsT0FBTSxhQUFmLEVBQTZCLFdBQVUsUUFBdkM7QUFDRTtBQUFBO0FBQUEsa0JBQVksY0FBWjtBQUNFO0FBREY7QUFERixhQVpKO0FBa0JFLG1CQUNFO0FBQUE7QUFBQSxnQkFBTSxXQUFTb0MsT0FBT2pDLFFBQXRCLEVBQWtDLFdBQVdqQixRQUFRZixLQUFyRDtBQUNHaUUscUJBQU9wQztBQURWLGFBbkJKO0FBdUJFLHVCQUNFO0FBQUE7QUFBQSxnQkFBTSxXQUFTb0MsT0FBT2pDLFFBQXRCLEVBQWtDLFdBQVdqQixRQUFROEQsU0FBckQ7QUFBQTtBQUNJWixxQkFBT2pDO0FBRFg7QUF4QkosWUFERjtBQThCRTtBQUNFLHVCQUFXakIsUUFBUXVCLEtBRHJCO0FBRUUsbUJBQ0VnQyxTQUNBLDJFQUpKO0FBTUUsbUJBQU90RTtBQU5ULFlBOUJGO0FBc0NFO0FBQUE7QUFBQSxjQUFhLFNBQVMsS0FBS3dGLFdBQTNCO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLE1BQUssT0FBakI7QUFBMEJ4RjtBQUExQixhQURGO0FBRUcsbUJBQU9xRSxXQUFQLEtBQXVCLFFBQXZCLEdBQ0M7QUFBQTtBQUFBLGdCQUFZLE1BQUssT0FBakIsRUFBeUIsa0JBQXpCLEVBQXNDLFdBQVd0RCxRQUFRb0UsT0FBekQ7QUFDR2Q7QUFESCxhQURELEdBSUc7QUFOTixXQXRDRjtBQStDRTtBQUFBO0FBQUE7QUFDRTtBQUFBO0FBQUEsZ0JBQVksU0FBUyxLQUFLaUIsZUFBMUI7QUFDRTtBQUFBO0FBQUEsa0JBQU8sY0FBYyxDQUFyQjtBQUNFO0FBQ0UsNkJBQVcsaUVBQ0x2RSxRQUFRK0Qsa0JBREgsRUFDMEJTLFFBRDFCO0FBRGI7QUFERjtBQURGLGFBREY7QUFVRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxTQUF2QjtBQUNFO0FBQUE7QUFBQSxrQkFBTyxjQUFjLENBQXJCO0FBQ0U7QUFERjtBQURGLGFBVkY7QUFlRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxZQUF2QjtBQUNFO0FBREYsYUFmRjtBQWtCR3BCLDBCQUFjbEYsV0FBVytFLFFBQXpCLEdBQ0M7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLGtCQUFZLGNBQVcsUUFBdkIsRUFBZ0MsU0FBUyxLQUFLVixnQkFBOUM7QUFDRTtBQURGLGVBREY7QUFJRTtBQUFBO0FBQUE7QUFDRSx3QkFBTSxLQUFLUSxLQUFMLENBQVdQLFlBRG5CO0FBRUUsMkJBQVMsS0FBS0MsaUJBRmhCO0FBR0UsK0NBQXlCdEQsTUFBekI7QUFIRjtBQUtFO0FBQUE7QUFBQSxvQkFBYSxjQUFZQSxNQUFaLG1CQUFiO0FBQUE7QUFBQSxpQkFMRjtBQVFFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxzQkFBUSxTQUFTLEtBQUtzRCxpQkFBdEIsRUFBeUMsT0FBTSxTQUEvQztBQUFBO0FBQUEsbUJBREY7QUFJRTtBQUFBO0FBQUE7QUFDRSwrQkFBUyxLQUFLaUMsZUFEaEI7QUFFRSw2QkFBTSxTQUZSO0FBR0U7QUFIRjtBQUFBO0FBQUE7QUFKRjtBQVJGO0FBSkYsYUFERCxHQTJCRyxJQTdDTjtBQThDRSxtREFBSyxXQUFXMUUsUUFBUVIsUUFBeEIsR0E5Q0Y7QUErQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsT0FBdkI7QUFDRTtBQUFBO0FBQUEsa0JBQU8sY0FBYyxDQUFyQjtBQUNFO0FBREY7QUFERixhQS9DRjtBQW9ERTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QixFQUErQixjQUEvQjtBQUNFO0FBREY7QUFwREY7QUEvQ0Y7QUFMRixPQURGO0FBZ0hEOzs7OztBQUdIOEUsS0FBS2pFLFNBQUwsR0FBaUI7QUFDZmdDLFdBQVMsb0JBQVUvQixNQUFWLENBQWlCQyxVQURYOztBQUdmUCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQUhYOztBQUtmb0MsWUFBVSxvQkFBVXJDLE1BQVYsQ0FBaUJDLFVBTFo7QUFNZnFELFNBQU8sb0JBQVV0RCxNQUFWLENBQWlCQyxVQU5UOztBQVFmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQXBCLFVBQVEsb0JBQVV3QixNQUFWLENBQWlCSixVQWZWOztBQWlCZnFFLGtCQUFnQixvQkFBVXBFLElBQVYsQ0FBZUQsVUFqQmhCO0FBa0Jmb0Usb0JBQWtCLG9CQUFVbkUsSUFBVixDQUFlRDtBQWxCbEIsQ0FBakI7O0FBcUJBLElBQU1pRCxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR2IsUUFBSCxTQUFHQSxRQUFIO0FBQUEsTUFBYWlCLEtBQWIsU0FBYUEsS0FBYjtBQUFBLFNBQTBCLEVBQUVqQixrQkFBRixFQUFZaUIsWUFBWixFQUExQjtBQUFBLENBQXhCOztBQUVBLElBQU1ILHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUM5RixRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRWlILHFDQURGO0FBRUVEO0FBRkYsR0FERixFQUtFaEgsUUFMRixDQUR5QjtBQUFBLENBQTNCOztrQkFTZSx5QkFBUTZGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXckUsTUFBWCxFQUFtQiwwQkFBV2tGLElBQVgsQ0FBbkIsQ0FEYSxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoVGY7Ozs7QUFDQTs7OztBQUNBOztBQUNBOztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUEzQkE7Ozs7Ozs7Ozs7O0FBNkJBLElBQU1sRixTQUFTO0FBQ2JDLFFBQU07QUFDSk0sYUFBUztBQURMLEdBRE87QUFJYnlCLFFBQU07QUFDSjFCLGtCQUFjO0FBRFYsR0FKTztBQU9iMkIsVUFBUTtBQUNOM0Isa0JBQWM7QUFEUixHQVBLO0FBVWJtQixpQkFBZSxFQVZGO0FBV2JLLFFBQU07QUFDSnZCLGFBQVM7QUFETCxHQVhPO0FBY2J3QixjQUFZO0FBQ1Y7QUFEVSxHQWRDO0FBaUJiRyxhQUFXLEVBakJFO0FBa0JiRyxrQkFBZ0I7QUFDZEMsV0FBTztBQURPLEdBbEJIO0FBcUJibEMsWUFBVTtBQUNScUMsVUFBTTtBQURFO0FBckJHLENBQWY7O0FBMEJBOzs7Ozs7SUFLTWdELFU7OztBQUNKLHNCQUFZOUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLDhJQUNYQSxLQURXOztBQUFBLFVBNkVuQkMsWUE3RW1CLEdBNkVKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBN0VJOztBQUFBLFVBOEVuQkMsWUE5RW1CLEdBOEVKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBOUVJOztBQUFBLFVBK0VuQjRDLE9BL0VtQixHQStFVDtBQUFBLGFBQU0sTUFBSzdDLFFBQUwsQ0FBYyxFQUFFOEMsUUFBUSxJQUFWLEVBQWQsQ0FBTjtBQUFBLEtBL0VTOztBQUVqQixVQUFLaEMsS0FBTCxHQUFhO0FBQ1hnQyxjQUFRLEtBREc7QUFFWDlGLGFBQU8sRUFGSTtBQUdYK0YsbUJBQWEsQ0FIRjtBQUlYN0UsbUJBQWEsVUFKRjtBQUtYbkIsZ0JBQVUsU0FMQzs7QUFPWGlHLGFBQU8sS0FQSTtBQVFYcEgsa0JBQVksQ0FSRDtBQVNYQyxhQUFPLEVBVEk7QUFVWG9ILGVBQVMsRUFWRTtBQVdYaEQsYUFBTztBQVhJLEtBQWI7O0FBY0EsVUFBS2pDLFFBQUwsR0FBZ0IsTUFBS0EsUUFBTCxDQUFja0YsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLFFBQUwsR0FBZ0IsTUFBS0EsUUFBTCxDQUFjRCxJQUFkLE9BQWhCO0FBakJpQjtBQWtCbEI7Ozs7NkJBRVFFLEMsRUFBRztBQUNWLFVBQU1wRyxRQUFRb0csRUFBRUMsTUFBRixDQUFTcEYsS0FBdkI7QUFDQSxVQUFNOEUsY0FBYy9GLE1BQU1zRyxNQUExQjtBQUNBLFVBQU1OLFFBQVFELGNBQWMsQ0FBZCxJQUFtQkEsY0FBYyxHQUEvQzs7QUFFQSxXQUFLL0MsUUFBTCxDQUFjO0FBQ1poRCxvQkFEWTtBQUVaK0YsZ0NBRlk7QUFHWkMsb0JBSFk7QUFJWm5ILGVBQU8sQ0FBQ21ILEtBQUQsR0FBUyw2Q0FBVCxHQUF5RDtBQUpwRCxPQUFkO0FBTUQ7Ozs7MkdBRWNJLEM7Ozs7Ozs7OztBQUNiQSxrQkFBRUcsY0FBRjt1Q0FDOEIsS0FBS3pELEtBQUwsQ0FBV1ksUUFBWCxDQUFvQkMsSSxFQUExQ25GLEssd0JBQUFBLEssRUFBV1MsTSx3QkFBSlIsRTt5QkFDYSxLQUFLcUYsSyxFQUF6Qi9ELFEsVUFBQUEsUSxFQUFVQyxLLFVBQUFBLEs7OztBQUVsQixxQkFBS2dELFFBQUwsQ0FBYyxFQUFFaUQsU0FBUyxrQkFBWCxFQUFkOzs7dUJBRTZDLHNCQUFjO0FBQ3pEekgsOEJBRHlEO0FBRXpEUyxnQ0FGeUQ7QUFHekRjLG9DQUh5RDtBQUl6REM7QUFKeUQsaUJBQWQsQzs7OztBQUFyQ3BCLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPWCx1QixTQUFBQSxPOztzQkFPdkJVLGNBQWMsRzs7Ozs7QUFDaEI7QUFDQSxxQkFBS29FLFFBQUwsQ0FBYyxFQUFFcEUsc0JBQUYsRUFBY0MsWUFBZCxFQUFkOzs7OztBQUlGLHFCQUFLbUUsUUFBTCxDQUFjLEVBQUVpRCxTQUFTLHVCQUFYLEVBQWQ7O0FBRUFPLDJCQUFXLFlBQU07QUFDZix5QkFBS3hELFFBQUwsQ0FBYztBQUNaZ0QsMkJBQU8sS0FESztBQUVacEgsZ0NBQVksQ0FGQTtBQUdaQywyQkFBTyxFQUhLO0FBSVpvSCw2QkFBUyxFQUpHOztBQU1abEcsOEJBQVUsU0FORTs7QUFRWkMsMkJBQU8sRUFSSztBQVNaK0YsaUNBQWE7QUFURCxtQkFBZDtBQVdELGlCQVpELEVBWUcsSUFaSDs7QUFjQSxxQkFBS2pELEtBQUwsQ0FBVzJELGdCQUFYO0FBQ0U3SDtBQURGLG1CQUVLVixPQUZMO0FBR0U4Qiw4QkFIRjtBQUlFRDtBQUpGOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBWU87QUFBQSxvQkFDZ0QsS0FBSytELEtBRHJEO0FBQUEsVUFDQ2IsS0FERCxXQUNDQSxLQUREO0FBQUEsVUFDUTZDLE1BRFIsV0FDUUEsTUFEUjtBQUFBLFVBQ2dCNUUsV0FEaEIsV0FDZ0JBLFdBRGhCO0FBQUEsVUFDNkJsQixLQUQ3QixXQUM2QkEsS0FEN0I7QUFBQSxVQUNvQ2lHLE9BRHBDLFdBQ29DQSxPQURwQztBQUFBLG1CQUV1QixLQUFLbkQsS0FGNUI7QUFBQSxVQUVDL0IsT0FGRCxVQUVDQSxPQUZEO0FBQUEsVUFFVTJDLFFBRlYsVUFFVUEsUUFGVjtBQUFBLDJCQUc0QkEsU0FBU0MsSUFIckM7QUFBQSxVQUdDOUIsSUFIRCxrQkFHQ0EsSUFIRDtBQUFBLFVBR09PLE1BSFAsa0JBR09BLE1BSFA7QUFBQSxVQUdlSixRQUhmLGtCQUdlQSxRQUhmOzs7QUFLUCxhQUNFO0FBQUE7QUFBQTtBQUNFLHFCQUFXakIsUUFBUVgsSUFEckI7QUFFRSx3QkFBYyxLQUFLMkMsWUFGckI7QUFHRSx3QkFBYyxLQUFLRztBQUhyQjtBQUtFO0FBQUE7QUFBQSxZQUFNLFFBQVFELEtBQWQsRUFBcUIsV0FBV2xDLFFBQVFvQixJQUF4QztBQUNFO0FBQ0Usb0JBQ0U7QUFBQTtBQUFBLGdCQUFPLFdBQVdwQixRQUFRcUIsTUFBMUIsRUFBa0MsV0FBVyxDQUE3QztBQUNFO0FBQUE7QUFBQSxrQkFBTSxXQUFTSixRQUFmO0FBQ0U7QUFDRSx1QkFBS0gsSUFEUDtBQUVFLHVCQUFLTyxVQUFVLDRDQUZqQjtBQUdFLDZCQUFXckIsUUFBUXFCO0FBSHJCO0FBREY7QUFERixhQUZKO0FBWUUsbUJBQ0U7QUFDRSxxQkFBT3BDLEtBRFQ7QUFFRSx3QkFBVSxLQUFLZ0IsUUFGakI7QUFHRSwyQkFBYUU7QUFIZixjQWJKO0FBbUJFLHVCQUFXSCxRQUFRbUIsVUFuQnJCO0FBb0JFLHFCQUFTLEtBQUsyRDtBQXBCaEIsWUFERjtBQXVCR0Msb0JBQVVHLFFBQVFLLE1BQWxCLEdBQ0M7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQWFMO0FBQWI7QUFERixXQURELEdBSUcsSUEzQk47QUE0QkdILG1CQUNDO0FBQUE7QUFBQTtBQUNFLG1EQUFLLFdBQVcvRSxRQUFRUixRQUF4QixHQURGO0FBRUU7QUFBQTtBQUFBLGdCQUFRLFlBQVIsRUFBZSxPQUFNLFFBQXJCLEVBQThCLFNBQVMsS0FBSzRGLFFBQTVDO0FBQUE7QUFBQTtBQUZGLFdBREQsR0FPRztBQW5DTjtBQUxGLE9BREY7QUE2Q0Q7Ozs7O0FBR0hQLFdBQVd4RSxTQUFYLEdBQXVCO0FBQ3JCTCxXQUFTLG9CQUFVTSxNQUFWLENBQWlCQyxVQURMOztBQUdyQm9DLFlBQVUsb0JBQVVyQyxNQUFWLENBQWlCQyxVQUhOOztBQUtyQm1GLG9CQUFrQixvQkFBVWxGLElBQVYsQ0FBZUQ7QUFMWixDQUF2Qjs7QUFRQSxJQUFNaUQsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdiLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7QUFDQSxJQUFNYyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDOUYsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0UrSDtBQURGLEdBREYsRUFJRS9ILFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVE2RixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYiwwQkFBV3JFLE1BQVgsRUFBbUJ5RixVQUFuQixDQURhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdNZjs7OztBQUNBOzs7O0FBRUE7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTXpGLFNBQVM7QUFDYkMsUUFBTTtBQUNKRyxjQUFVO0FBRE47QUFETyxDQUFmLEMsQ0FmQTs7Ozs7OztJQXFCTW1HLFE7OztBQUNKLG9CQUFZNUQsS0FBWixFQUFtQjtBQUFBOztBQUFBLDBJQUNYQSxLQURXOztBQUVqQixVQUFLZ0IsS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBQ1E7QUFBQSxtQkFDNEMsS0FBS2hCLEtBRGpEO0FBQUEsVUFDQy9CLE9BREQsVUFDQ0EsT0FERDtBQUFBLFVBQ1U1QyxJQURWLFVBQ1VBLElBRFY7QUFBQSxVQUNnQndHLEtBRGhCLFVBQ2dCQSxLQURoQjtBQUFBLFVBQ3VCWixPQUR2QixVQUN1QkEsT0FEdkI7QUFBQSxVQUNnQ1csT0FEaEMsVUFDZ0NBLE9BRGhDOzs7QUFHUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVczRCxRQUFRWCxJQUF4QjtBQUNHakMsaUJBQVMsT0FBVCxHQUNHdUcsUUFBUUMsS0FBUixDQUFjZ0MsR0FBZCxDQUFrQixVQUFDQyxDQUFEO0FBQUEsaUJBQU8sZ0RBQU0sS0FBS0EsRUFBRTFHLE1BQWIsRUFBcUIsUUFBUTBHLEVBQUUxRyxNQUEvQixHQUFQO0FBQUEsU0FBbEIsQ0FESCxHQUVHd0UsUUFBUVgsT0FBUixDQUFnQjRDLEdBQWhCLENBQW9CLFVBQUNFLENBQUQ7QUFBQSxpQkFDcEIsa0RBQVEsS0FBS0EsRUFBRTNILFFBQWYsRUFBeUIsVUFBVTJILEVBQUUzSCxRQUFyQyxHQURvQjtBQUFBLFNBQXBCO0FBSE4sT0FERjtBQVNEOzs7OztBQUdId0gsU0FBU3RGLFNBQVQsR0FBcUI7QUFDbkJMLFdBQVMsb0JBQVVNLE1BQVYsQ0FBaUJDLFVBRFA7QUFFbkJuRCxRQUFNLG9CQUFVc0QsTUFBVixDQUFpQkgsVUFGSixFQUVnQjtBQUNuQ29ELFdBQVMsb0JBQVVyRCxNQUFWLENBQWlCQyxVQUhQO0FBSW5CcUQsU0FBTyxvQkFBVXRELE1BSkU7QUFLbkIwQyxXQUFTLG9CQUFVMUM7QUFMQSxDQUFyQjs7a0JBUWUsd0JBQVdsQixNQUFYLEVBQW1CdUcsUUFBbkIsQyIsImZpbGUiOiI0MS5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSBhY3RpdmVFbGVtZW50O1xuXG52YXIgX293bmVyRG9jdW1lbnQgPSByZXF1aXJlKCcuL293bmVyRG9jdW1lbnQnKTtcblxudmFyIF9vd25lckRvY3VtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX293bmVyRG9jdW1lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBhY3RpdmVFbGVtZW50KCkge1xuICB2YXIgZG9jID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiAoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKCk7XG5cbiAgdHJ5IHtcbiAgICByZXR1cm4gZG9jLmFjdGl2ZUVsZW1lbnQ7XG4gIH0gY2F0Y2ggKGUpIHsvKiBpZSB0aHJvd3MgaWYgbm8gYWN0aXZlIGVsZW1lbnQgKi99XG59XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9hY3RpdmVFbGVtZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy9hY3RpdmVFbGVtZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIlwidXNlIHN0cmljdFwiO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5kZWZhdWx0ID0gb3duZXJEb2N1bWVudDtcbmZ1bmN0aW9uIG93bmVyRG9jdW1lbnQobm9kZSkge1xuICByZXR1cm4gbm9kZSAmJiBub2RlLm93bmVyRG9jdW1lbnQgfHwgZG9jdW1lbnQ7XG59XG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbXCJkZWZhdWx0XCJdO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDI3IDI4IDM0IDM1IDM2IDM3IDQwIDQxIDQyIDQzIDQ2IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmRlZmF1bHQgPSBnZXRXaW5kb3c7XG5mdW5jdGlvbiBnZXRXaW5kb3cobm9kZSkge1xuICByZXR1cm4gbm9kZSA9PT0gbm9kZS53aW5kb3cgPyBub2RlIDogbm9kZS5ub2RlVHlwZSA9PT0gOSA/IG5vZGUuZGVmYXVsdFZpZXcgfHwgbm9kZS5wYXJlbnRXaW5kb3cgOiBmYWxzZTtcbn1cbm1vZHVsZS5leHBvcnRzID0gZXhwb3J0c1tcImRlZmF1bHRcIl07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3F1ZXJ5L2lzV2luZG93LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZnVuY3Rpb24gKHJlY2FsYykge1xuICBpZiAoIXNpemUgJiYgc2l6ZSAhPT0gMCB8fCByZWNhbGMpIHtcbiAgICBpZiAoX2luRE9NMi5kZWZhdWx0KSB7XG4gICAgICB2YXIgc2Nyb2xsRGl2ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cbiAgICAgIHNjcm9sbERpdi5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XG4gICAgICBzY3JvbGxEaXYuc3R5bGUudG9wID0gJy05OTk5cHgnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLndpZHRoID0gJzUwcHgnO1xuICAgICAgc2Nyb2xsRGl2LnN0eWxlLmhlaWdodCA9ICc1MHB4JztcbiAgICAgIHNjcm9sbERpdi5zdHlsZS5vdmVyZmxvdyA9ICdzY3JvbGwnO1xuXG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHNjcm9sbERpdik7XG4gICAgICBzaXplID0gc2Nyb2xsRGl2Lm9mZnNldFdpZHRoIC0gc2Nyb2xsRGl2LmNsaWVudFdpZHRoO1xuICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChzY3JvbGxEaXYpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBzaXplO1xufTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJy4vaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzaXplID0gdm9pZCAwO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGV4cG9ydHNbJ2RlZmF1bHQnXTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9kb20taGVscGVycy91dGlsL3Njcm9sbGJhclNpemUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2RvbS1oZWxwZXJzL3V0aWwvc2Nyb2xsYmFyU2l6ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMS45OSA0YzAtMS4xLS44OS0yLTEuOTktMkg0Yy0xLjEgMC0yIC45LTIgMnYxMmMwIDEuMS45IDIgMiAyaDE0bDQgNC0uMDEtMTh6TTE4IDE0SDZ2LTJoMTJ2MnptMC0zSDZWOWgxMnYyem0wLTNINlY2aDEydjJ6JyB9KTtcblxudmFyIENvbW1lbnQgPSBmdW5jdGlvbiBDb21tZW50KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Db21tZW50ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShDb21tZW50KTtcbkNvbW1lbnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29tbWVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db21tZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db21tZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ002IDE5YzAgMS4xLjkgMiAyIDJoOGMxLjEgMCAyLS45IDItMlY3SDZ2MTJ6TTE5IDRoLTMuNWwtMS0xaC01bC0xIDFINXYyaDE0VjR6JyB9KTtcblxudmFyIERlbGV0ZSA9IGZ1bmN0aW9uIERlbGV0ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRGVsZXRlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShEZWxldGUpO1xuRGVsZXRlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IERlbGV0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9EZWxldGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0RlbGV0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI2IDI3IDI4IDMyIDM1IDM3IDM4IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTIgMjEuMzVsLTEuNDUtMS4zMkM1LjQgMTUuMzYgMiAxMi4yOCAyIDguNSAyIDUuNDIgNC40MiAzIDcuNSAzYzEuNzQgMCAzLjQxLjgxIDQuNSAyLjA5QzEzLjA5IDMuODEgMTQuNzYgMyAxNi41IDMgMTkuNTggMyAyMiA1LjQyIDIyIDguNWMwIDMuNzgtMy40IDYuODYtOC41NSAxMS41NEwxMiAyMS4zNXonIH0pO1xuXG52YXIgRmF2b3JpdGUgPSBmdW5jdGlvbiBGYXZvcml0ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRmF2b3JpdGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZhdm9yaXRlKTtcbkZhdm9yaXRlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZhdm9yaXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zhdm9yaXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQwIDQxIDQyIDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTIgOGMxLjEgMCAyLS45IDItMnMtLjktMi0yLTItMiAuOS0yIDIgLjkgMiAyIDJ6bTAgMmMtMS4xIDAtMiAuOS0yIDJzLjkgMiAyIDIgMi0uOSAyLTItLjktMi0yLTJ6bTAgNmMtMS4xIDAtMiAuOS0yIDJzLjkgMiAyIDIgMi0uOSAyLTItLjktMi0yLTJ6JyB9KTtcblxudmFyIE1vcmVWZXJ0ID0gZnVuY3Rpb24gTW9yZVZlcnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbk1vcmVWZXJ0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShNb3JlVmVydCk7XG5Nb3JlVmVydC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBNb3JlVmVydDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Nb3JlVmVydC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxIDI3IDI4IDMxIDM0IDM1IDM3IDQwIDQxIDQyIDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTIgNC41QzcgNC41IDIuNzMgNy42MSAxIDEyYzEuNzMgNC4zOSA2IDcuNSAxMSA3LjVzOS4yNy0zLjExIDExLTcuNWMtMS43My00LjM5LTYtNy41LTExLTcuNXpNMTIgMTdjLTIuNzYgMC01LTIuMjQtNS01czIuMjQtNSA1LTUgNSAyLjI0IDUgNS0yLjI0IDUtNSA1em0wLThjLTEuNjYgMC0zIDEuMzQtMyAzczEuMzQgMyAzIDMgMy0xLjM0IDMtMy0xLjM0LTMtMy0zeicgfSk7XG5cbnZhciBSZW1vdmVSZWRFeWUgPSBmdW5jdGlvbiBSZW1vdmVSZWRFeWUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblJlbW92ZVJlZEV5ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoUmVtb3ZlUmVkRXllKTtcblJlbW92ZVJlZEV5ZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBSZW1vdmVSZWRFeWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvUmVtb3ZlUmVkRXllLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9SZW1vdmVSZWRFeWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNSAzNyA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE4IDE2LjA4Yy0uNzYgMC0xLjQ0LjMtMS45Ni43N0w4LjkxIDEyLjdjLjA1LS4yMy4wOS0uNDYuMDktLjdzLS4wNC0uNDctLjA5LS43bDcuMDUtNC4xMWMuNTQuNSAxLjI1LjgxIDIuMDQuODEgMS42NiAwIDMtMS4zNCAzLTNzLTEuMzQtMy0zLTMtMyAxLjM0LTMgM2MwIC4yNC4wNC40Ny4wOS43TDguMDQgOS44MUM3LjUgOS4zMSA2Ljc5IDkgNiA5Yy0xLjY2IDAtMyAxLjM0LTMgM3MxLjM0IDMgMyAzYy43OSAwIDEuNS0uMzEgMi4wNC0uODFsNy4xMiA0LjE2Yy0uMDUuMjEtLjA4LjQzLS4wOC42NSAwIDEuNjEgMS4zMSAyLjkyIDIuOTIgMi45MiAxLjYxIDAgMi45Mi0xLjMxIDIuOTItMi45MnMtMS4zMS0yLjkyLTIuOTItMi45MnonIH0pO1xuXG52YXIgU2hhcmUgPSBmdW5jdGlvbiBTaGFyZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuU2hhcmUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFNoYXJlKTtcblNoYXJlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFNoYXJlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NoYXJlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TaGFyZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQwIDQxIDQyIDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTcgM0g3Yy0xLjEgMC0xLjk5LjktMS45OSAyTDUgMjFsNy0zIDcgM1Y1YzAtMS4xLS45LTItMi0yeicgfSk7XG5cbnZhciBUdXJuZWRJbiA9IGZ1bmN0aW9uIFR1cm5lZEluKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5UdXJuZWRJbiA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoVHVybmVkSW4pO1xuVHVybmVkSW4ubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gVHVybmVkSW47XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVHVybmVkSW4uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzQgMzUgMzcgNDAgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7IC8vICB3ZWFrXG5cbnZhciBSQURJVVMgPSAxMjtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWZsZXgnXG4gICAgfSxcbiAgICBiYWRnZToge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgZmxleERpcmVjdGlvbjogJ3JvdycsXG4gICAgICBmbGV4V3JhcDogJ3dyYXAnLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgYWxpZ25Db250ZW50OiAnY2VudGVyJyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICB0b3A6IC1SQURJVVMsXG4gICAgICByaWdodDogLVJBRElVUyxcbiAgICAgIGZvbnRGYW1pbHk6IHRoZW1lLnR5cG9ncmFwaHkuZm9udEZhbWlseSxcbiAgICAgIGZvbnRXZWlnaHQ6IHRoZW1lLnR5cG9ncmFwaHkuZm9udFdlaWdodCxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oUkFESVVTKSxcbiAgICAgIHdpZHRoOiBSQURJVVMgKiAyLFxuICAgICAgaGVpZ2h0OiBSQURJVVMgKiAyLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb2xvcixcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnRleHRDb2xvcixcbiAgICAgIHpJbmRleDogMSAvLyBSZW5kZXIgdGhlIGJhZGdlIG9uIHRvcCBvZiBwb3RlbnRpYWwgcmlwcGxlcy5cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDAsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMClcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCByZW5kZXJlZCB3aXRoaW4gdGhlIGJhZGdlLlxuICAgKi9cbiAgYmFkZ2VDb250ZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGJhZGdlIHdpbGwgYmUgYWRkZWQgcmVsYXRpdmUgdG8gdGhpcyBub2RlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydkZWZhdWx0JywgJ3ByaW1hcnknLCAnYWNjZW50J10pLmlzUmVxdWlyZWRcbn07XG5cbnZhciBCYWRnZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEJhZGdlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBCYWRnZSgpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBCYWRnZSk7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEJhZGdlLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShCYWRnZSkpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQmFkZ2UsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYmFkZ2VDb250ZW50ID0gX3Byb3BzLmJhZGdlQ29udGVudCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYmFkZ2VDb250ZW50JywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2NoaWxkcmVuJ10pO1xuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZVByb3ApO1xuICAgICAgdmFyIGJhZGdlQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmJhZGdlLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdkZWZhdWx0JykpO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlbixcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBiYWRnZUNsYXNzTmFtZSB9LFxuICAgICAgICAgIGJhZGdlQ29udGVudFxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQmFkZ2U7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5CYWRnZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnZGVmYXVsdCdcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQmFkZ2UnIH0pKEJhZGdlKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9CYWRnZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQmFkZ2UvQmFkZ2UuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNCAzNSAzNyA0MSA0MiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9CYWRnZSA9IHJlcXVpcmUoJy4vQmFkZ2UnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQmFkZ2UpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0JhZGdlL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9CYWRnZS9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM0IDM1IDM3IDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX01vZGFsID0gcmVxdWlyZSgnLi4vTW9kYWwnKTtcblxudmFyIF9Nb2RhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9Nb2RhbCk7XG5cbnZhciBfRmFkZSA9IHJlcXVpcmUoJy4uL3RyYW5zaXRpb25zL0ZhZGUnKTtcblxudmFyIF9GYWRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ZhZGUpO1xuXG52YXIgX3RyYW5zaXRpb25zID0gcmVxdWlyZSgnLi4vc3R5bGVzL3RyYW5zaXRpb25zJyk7XG5cbnZhciBfUGFwZXIgPSByZXF1aXJlKCcuLi9QYXBlcicpO1xuXG52YXIgX1BhcGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BhcGVyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgTW9kYWxcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgYWxpZ25JdGVtczogJ2NlbnRlcidcbiAgICB9LFxuICAgIHBhcGVyOiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBtYXJnaW46IHRoZW1lLnNwYWNpbmcudW5pdCAqIDQsXG4gICAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJyxcbiAgICAgIGZsZXg6ICcwIDEgYXV0bycsXG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIG1heEhlaWdodDogJzkwdmgnLFxuICAgICAgb3ZlcmZsb3dZOiAnYXV0bycsIC8vIEZpeCBJRTExIGlzc3VlLCB0byByZW1vdmUgYXQgc29tZSBwb2ludC5cbiAgICAgICcmOmZvY3VzJzoge1xuICAgICAgICBvdXRsaW5lOiAnbm9uZSdcbiAgICAgIH1cbiAgICB9LFxuICAgIHBhcGVyV2lkdGhYczoge1xuICAgICAgbWF4V2lkdGg6IE1hdGgubWF4KHRoZW1lLmJyZWFrcG9pbnRzLnZhbHVlcy54cywgMzYwKVxuICAgIH0sXG4gICAgcGFwZXJXaWR0aFNtOiB7XG4gICAgICBtYXhXaWR0aDogdGhlbWUuYnJlYWtwb2ludHMudmFsdWVzLnNtXG4gICAgfSxcbiAgICBwYXBlcldpZHRoTWQ6IHtcbiAgICAgIG1heFdpZHRoOiB0aGVtZS5icmVha3BvaW50cy52YWx1ZXMubWRcbiAgICB9LFxuICAgIGZ1bGxXaWR0aDoge1xuICAgICAgd2lkdGg6ICcxMDAlJ1xuICAgIH0sXG4gICAgZnVsbFNjcmVlbjoge1xuICAgICAgbWFyZ2luOiAwLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIG1heFdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIG1heEhlaWdodDogJzEwMCUnLFxuICAgICAgYm9yZGVyUmFkaXVzOiAwXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRGlhbG9nIGNoaWxkcmVuLCB1c3VhbGx5IHRoZSBpbmNsdWRlZCBzdWItY29tcG9uZW50cy5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBpdCB3aWxsIGJlIGZ1bGwtc2NyZWVuXG4gICAqL1xuICBmdWxsU2NyZWVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIGNsaWNraW5nIHRoZSBiYWNrZHJvcCB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlQmFja2Ryb3BDbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBoaXR0aW5nIGVzY2FwZSB3aWxsIG5vdCBmaXJlIHRoZSBgb25SZXF1ZXN0Q2xvc2VgIGNhbGxiYWNrLlxuICAgKi9cbiAgaWdub3JlRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBkdXJhdGlvbiBmb3IgdGhlIHRyYW5zaXRpb24sIGluIG1pbGxpc2Vjb25kcy5cbiAgICogWW91IG1heSBzcGVjaWZ5IGEgc2luZ2xlIHRpbWVvdXQgZm9yIGFsbCB0cmFuc2l0aW9ucywgb3IgaW5kaXZpZHVhbGx5IHdpdGggYW4gb2JqZWN0LlxuICAgKi9cbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIERldGVybWluZSB0aGUgbWF4IHdpZHRoIG9mIHRoZSBkaWFsb2cuXG4gICAqIFRoZSBkaWFsb2cgd2lkdGggZ3Jvd3Mgd2l0aCB0aGUgc2l6ZSBvZiB0aGUgc2NyZWVuLCB0aGlzIHByb3BlcnR5IGlzIHVzZWZ1bFxuICAgKiBvbiB0aGUgZGVza3RvcCB3aGVyZSB5b3UgbWlnaHQgbmVlZCBzb21lIGNvaGVyZW50IGRpZmZlcmVudCB3aWR0aCBzaXplIGFjcm9zcyB5b3VyXG4gICAqIGFwcGxpY2F0aW9uLlxuICAgKi9cbiAgbWF4V2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3hzJywgJ3NtJywgJ21kJ10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIHNwZWNpZmllZCwgc3RyZXRjaGVzIGRpYWxvZyB0byBtYXggd2lkdGguXG4gICAqL1xuICBmdWxsV2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGJhY2tkcm9wIGlzIGNsaWNrZWQuXG4gICAqL1xuICBvbkJhY2tkcm9wQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIGRpYWxvZyBlbnRlcnMuXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGRpYWxvZyBpcyBlbnRlcmluZy5cbiAgICovXG4gIG9uRW50ZXJpbmc6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgZGlhbG9nIGhhcyBlbnRlcmVkLlxuICAgKi9cbiAgb25FbnRlcmVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVzIHdoZW4gdGhlIGVzY2FwZSBrZXkgaXMgcmVsZWFzZWQgYW5kIHRoZSBtb2RhbCBpcyBpbiBmb2N1cy5cbiAgICovXG4gIG9uRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIGRpYWxvZyBleGl0cy5cbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBkaWFsb2cgaXMgZXhpdGluZy5cbiAgICovXG4gIG9uRXhpdGluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBkaWFsb2cgaGFzIGV4aXRlZC5cbiAgICovXG4gIG9uRXhpdGVkOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIGNvbXBvbmVudCByZXF1ZXN0cyB0byBiZSBjbG9zZWQuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKi9cbiAgb25SZXF1ZXN0Q2xvc2U6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBEaWFsb2cgaXMgb3Blbi5cbiAgICovXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRyYW5zaXRpb24gY29tcG9uZW50LlxuICAgKi9cbiAgdHJhbnNpdGlvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbXBvbmVudFR5cGUpLmlzUmVxdWlyZWRcbn07XG5cbi8qKlxuICogRGlhbG9ncyBhcmUgb3ZlcmxhaWQgbW9kYWwgcGFwZXIgYmFzZWQgY29tcG9uZW50cyB3aXRoIGEgYmFja2Ryb3AuXG4gKi9cbnZhciBEaWFsb2cgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShEaWFsb2csIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERpYWxvZygpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBEaWFsb2cpO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChEaWFsb2cuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKERpYWxvZykpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoRGlhbG9nLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZnVsbFNjcmVlbiA9IF9wcm9wcy5mdWxsU2NyZWVuLFxuICAgICAgICAgIGlnbm9yZUJhY2tkcm9wQ2xpY2sgPSBfcHJvcHMuaWdub3JlQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcCA9IF9wcm9wcy5pZ25vcmVFc2NhcGVLZXlVcCxcbiAgICAgICAgICB0cmFuc2l0aW9uRHVyYXRpb24gPSBfcHJvcHMudHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgIG1heFdpZHRoID0gX3Byb3BzLm1heFdpZHRoLFxuICAgICAgICAgIGZ1bGxXaWR0aCA9IF9wcm9wcy5mdWxsV2lkdGgsXG4gICAgICAgICAgb3BlbiA9IF9wcm9wcy5vcGVuLFxuICAgICAgICAgIG9uQmFja2Ryb3BDbGljayA9IF9wcm9wcy5vbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgb25Fc2NhcGVLZXlVcCA9IF9wcm9wcy5vbkVzY2FwZUtleVVwLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMub25FbnRlcixcbiAgICAgICAgICBvbkVudGVyaW5nID0gX3Byb3BzLm9uRW50ZXJpbmcsXG4gICAgICAgICAgb25FbnRlcmVkID0gX3Byb3BzLm9uRW50ZXJlZCxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMub25FeGl0LFxuICAgICAgICAgIG9uRXhpdGluZyA9IF9wcm9wcy5vbkV4aXRpbmcsXG4gICAgICAgICAgb25FeGl0ZWQgPSBfcHJvcHMub25FeGl0ZWQsXG4gICAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHMub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgICAgVHJhbnNpdGlvblByb3AgPSBfcHJvcHMudHJhbnNpdGlvbixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdmdWxsU2NyZWVuJywgJ2lnbm9yZUJhY2tkcm9wQ2xpY2snLCAnaWdub3JlRXNjYXBlS2V5VXAnLCAndHJhbnNpdGlvbkR1cmF0aW9uJywgJ21heFdpZHRoJywgJ2Z1bGxXaWR0aCcsICdvcGVuJywgJ29uQmFja2Ryb3BDbGljaycsICdvbkVzY2FwZUtleVVwJywgJ29uRW50ZXInLCAnb25FbnRlcmluZycsICdvbkVudGVyZWQnLCAnb25FeGl0JywgJ29uRXhpdGluZycsICdvbkV4aXRlZCcsICdvblJlcXVlc3RDbG9zZScsICd0cmFuc2l0aW9uJ10pO1xuXG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX01vZGFsMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWUpLFxuICAgICAgICAgIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uOiB0cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgaWdub3JlQmFja2Ryb3BDbGljazogaWdub3JlQmFja2Ryb3BDbGljayxcbiAgICAgICAgICBpZ25vcmVFc2NhcGVLZXlVcDogaWdub3JlRXNjYXBlS2V5VXAsXG4gICAgICAgICAgb25CYWNrZHJvcENsaWNrOiBvbkJhY2tkcm9wQ2xpY2ssXG4gICAgICAgICAgb25Fc2NhcGVLZXlVcDogb25Fc2NhcGVLZXlVcCxcbiAgICAgICAgICBvblJlcXVlc3RDbG9zZTogb25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgICAgc2hvdzogb3BlblxuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgIFRyYW5zaXRpb25Qcm9wLFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIGFwcGVhcjogdHJ1ZSxcbiAgICAgICAgICAgICdpbic6IG9wZW4sXG4gICAgICAgICAgICB0aW1lb3V0OiB0cmFuc2l0aW9uRHVyYXRpb24sXG4gICAgICAgICAgICBvbkVudGVyOiBvbkVudGVyLFxuICAgICAgICAgICAgb25FbnRlcmluZzogb25FbnRlcmluZyxcbiAgICAgICAgICAgIG9uRW50ZXJlZDogb25FbnRlcmVkLFxuICAgICAgICAgICAgb25FeGl0OiBvbkV4aXQsXG4gICAgICAgICAgICBvbkV4aXRpbmc6IG9uRXhpdGluZyxcbiAgICAgICAgICAgIG9uRXhpdGVkOiBvbkV4aXRlZFxuICAgICAgICAgIH0sXG4gICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBfUGFwZXIyLmRlZmF1bHQsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGVsZXZhdGlvbjogMjQsXG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnBhcGVyLCBjbGFzc2VzWydwYXBlcldpZHRoJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKG1heFdpZHRoKV0sIChfY2xhc3NOYW1lcyA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5mdWxsU2NyZWVuLCBmdWxsU2NyZWVuKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZnVsbFdpZHRoLCBmdWxsV2lkdGgpLCBfY2xhc3NOYW1lcykpXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApXG4gICAgICAgIClcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBEaWFsb2c7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5EaWFsb2cuZGVmYXVsdFByb3BzID0ge1xuICBmdWxsU2NyZWVuOiBmYWxzZSxcbiAgaWdub3JlQmFja2Ryb3BDbGljazogZmFsc2UsXG4gIGlnbm9yZUVzY2FwZUtleVVwOiBmYWxzZSxcbiAgdHJhbnNpdGlvbkR1cmF0aW9uOiB7XG4gICAgZW50ZXI6IF90cmFuc2l0aW9ucy5kdXJhdGlvbi5lbnRlcmluZ1NjcmVlbixcbiAgICBleGl0OiBfdHJhbnNpdGlvbnMuZHVyYXRpb24ubGVhdmluZ1NjcmVlblxuICB9LFxuICBtYXhXaWR0aDogJ3NtJyxcbiAgZnVsbFdpZHRoOiBmYWxzZSxcbiAgb3BlbjogZmFsc2UsXG4gIHRyYW5zaXRpb246IF9GYWRlMi5kZWZhdWx0XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aURpYWxvZycgfSkoRGlhbG9nKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvRGlhbG9nLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWY7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxucmVxdWlyZSgnLi4vQnV0dG9uJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbi8vIFNvIHdlIGRvbid0IGhhdmUgYW55IG92ZXJyaWRlIHByaW9yaXR5IGlzc3VlLlxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2ZsZXgtZW5kJyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAgbWFyZ2luOiB0aGVtZS5zcGFjaW5nLnVuaXQgKyAncHggJyArIHRoZW1lLnNwYWNpbmcudW5pdCAvIDIgKyAncHgnLFxuICAgICAgZmxleDogJzAgMCBhdXRvJ1xuICAgIH0sXG4gICAgYWN0aW9uOiB7XG4gICAgICBtYXJnaW46ICcwICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgLyAyICsgJ3B4J1xuICAgIH0sXG4gICAgYnV0dG9uOiB7XG4gICAgICBtaW5XaWR0aDogNjRcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIERpYWxvZ0FjdGlvbnMocHJvcHMpIHtcbiAgdmFyIGNoaWxkcmVuID0gcHJvcHMuY2hpbGRyZW4sXG4gICAgICBjbGFzc2VzID0gcHJvcHMuY2xhc3NlcyxcbiAgICAgIGNsYXNzTmFtZSA9IHByb3BzLmNsYXNzTmFtZSxcbiAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkocHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnXSk7XG5cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgJ2RpdicsXG4gICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsIGNsYXNzTmFtZSkgfSwgb3RoZXIpLFxuICAgIF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5tYXAoY2hpbGRyZW4sIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgaWYgKCFfcmVhY3QyLmRlZmF1bHQuaXNWYWxpZEVsZW1lbnQoY2hpbGQpKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5hY3Rpb24gfSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmJ1dHRvbiwgY2hpbGQucHJvcHMuY2xhc3NOYW1lKVxuICAgICAgICB9KVxuICAgICAgKTtcbiAgICB9KVxuICApO1xufVxuXG5EaWFsb2dBY3Rpb25zLnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IChfcmVmID0ge1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHRoZW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpRGlhbG9nQWN0aW9ucycgfSkoRGlhbG9nQWN0aW9ucyk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0FjdGlvbnMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dBY3Rpb25zLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9yZWY7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHZhciBzcGFjaW5nID0gdGhlbWUuc3BhY2luZy51bml0ICogMztcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICAgICAgb3ZlcmZsb3dZOiAnYXV0bycsXG4gICAgICBwYWRkaW5nOiAnMCAnICsgc3BhY2luZyArICdweCAnICsgc3BhY2luZyArICdweCAnICsgc3BhY2luZyArICdweCcsXG4gICAgICAnJjpmaXJzdC1jaGlsZCc6IHtcbiAgICAgICAgcGFkZGluZ1RvcDogc3BhY2luZ1xuICAgICAgfVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmdcbn07XG5cblxuZnVuY3Rpb24gRGlhbG9nQ29udGVudChwcm9wcykge1xuICB2YXIgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NOYW1lID0gcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShwcm9wcywgWydjbGFzc2VzJywgJ2NoaWxkcmVuJywgJ2NsYXNzTmFtZSddKTtcblxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAnZGl2JyxcbiAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSB9LCBvdGhlciksXG4gICAgY2hpbGRyZW5cbiAgKTtcbn1cblxuRGlhbG9nQ29udGVudC5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoX3JlZiA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkLFxuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpRGlhbG9nQ29udGVudCcgfSkoRGlhbG9nQ29udGVudCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0NvbnRlbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9yZWY7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDogKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgdGhlbWUudHlwb2dyYXBoeS5zdWJoZWFkaW5nLCB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LnNlY29uZGFyeSxcbiAgICAgIG1hcmdpbjogMFxuICAgIH0pXG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgY29tcG9uZW50LlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG5cbmZ1bmN0aW9uIERpYWxvZ0NvbnRlbnRUZXh0KHByb3BzKSB7XG4gIHZhciBjaGlsZHJlbiA9IHByb3BzLmNoaWxkcmVuLFxuICAgICAgY2xhc3NlcyA9IHByb3BzLmNsYXNzZXMsXG4gICAgICBjbGFzc05hbWUgPSBwcm9wcy5jbGFzc05hbWUsXG4gICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKHByb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJ10pO1xuXG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICdwJyxcbiAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSB9LCBvdGhlciksXG4gICAgY2hpbGRyZW5cbiAgKTtcbn1cblxuRGlhbG9nQ29udGVudFRleHQucHJvcFR5cGVzID0gcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKF9yZWYgPSB7XG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgdGhlbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSlcbn0sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjbGFzc2VzJywgcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjbGFzc05hbWUnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nKSwgX3JlZikgOiB7fTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlEaWFsb2dDb250ZW50VGV4dCcgfSkoRGlhbG9nQ29udGVudFRleHQpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dDb250ZW50VGV4dC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ0NvbnRlbnRUZXh0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfVHlwb2dyYXBoeSA9IHJlcXVpcmUoJy4uL1R5cG9ncmFwaHknKTtcblxudmFyIF9UeXBvZ3JhcGh5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1R5cG9ncmFwaHkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBtYXJnaW46IDAsXG4gICAgICBwYWRkaW5nOiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzICsgJ3B4ICcgKyB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzICsgJ3B4ICAgICAgIDIwcHggJyArIHRoZW1lLnNwYWNpbmcudW5pdCAqIDMgKyAncHgnLFxuICAgICAgZmxleDogJzAgMCBhdXRvJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNoaWxkcmVuIHdvbid0IGJlIHdyYXBwZWQgYnkgYSB0eXBvZ3JhcGh5IGNvbXBvbmVudC5cbiAgICogRm9yIGluc3RhbmNlLCB0aGlzIGNhbiBiZSB1c2VmdWwgdG8gcmVuZGVyIGFuIGg0IGluc3RlYWQgb2YgdGhlIGRlZmF1bHQgaDIuXG4gICAqL1xuICBkaXNhYmxlVHlwb2dyYXBoeTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxudmFyIERpYWxvZ1RpdGxlID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoRGlhbG9nVGl0bGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIERpYWxvZ1RpdGxlKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIERpYWxvZ1RpdGxlKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoRGlhbG9nVGl0bGUuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKERpYWxvZ1RpdGxlKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShEaWFsb2dUaXRsZSwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBkaXNhYmxlVHlwb2dyYXBoeSA9IF9wcm9wcy5kaXNhYmxlVHlwb2dyYXBoeSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdkaXNhYmxlVHlwb2dyYXBoeSddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgY2xhc3NOYW1lKSB9LCBvdGhlciksXG4gICAgICAgIGRpc2FibGVUeXBvZ3JhcGh5ID8gY2hpbGRyZW4gOiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICBfVHlwb2dyYXBoeTIuZGVmYXVsdCxcbiAgICAgICAgICB7IHR5cGU6ICd0aXRsZScgfSxcbiAgICAgICAgICBjaGlsZHJlblxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gRGlhbG9nVGl0bGU7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5EaWFsb2dUaXRsZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGRpc2FibGVUeXBvZ3JhcGh5OiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlEaWFsb2dUaXRsZScgfSkoRGlhbG9nVGl0bGUpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy9EaWFsb2dUaXRsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL0RpYWxvZ1RpdGxlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0RpYWxvZyA9IHJlcXVpcmUoJy4vRGlhbG9nJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZykuZGVmYXVsdDtcbiAgfVxufSk7XG5cbnZhciBfRGlhbG9nQWN0aW9ucyA9IHJlcXVpcmUoJy4vRGlhbG9nQWN0aW9ucycpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0RpYWxvZ0FjdGlvbnMnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9EaWFsb2dBY3Rpb25zKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9EaWFsb2dUaXRsZSA9IHJlcXVpcmUoJy4vRGlhbG9nVGl0bGUnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdEaWFsb2dUaXRsZScsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZ1RpdGxlKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9EaWFsb2dDb250ZW50ID0gcmVxdWlyZSgnLi9EaWFsb2dDb250ZW50Jyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnRGlhbG9nQ29udGVudCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0RpYWxvZ0NvbnRlbnQpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX0RpYWxvZ0NvbnRlbnRUZXh0ID0gcmVxdWlyZSgnLi9EaWFsb2dDb250ZW50VGV4dCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ0RpYWxvZ0NvbnRlbnRUZXh0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfRGlhbG9nQ29udGVudFRleHQpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG52YXIgX3dpdGhNb2JpbGVEaWFsb2cgPSByZXF1aXJlKCcuL3dpdGhNb2JpbGVEaWFsb2cnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICd3aXRoTW9iaWxlRGlhbG9nJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aE1vYmlsZURpYWxvZykuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9EaWFsb2cvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDUgMjcgMjggMzUgMzcgNDAgNDEgNDIgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgncmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG52YXIgX3dpdGhXaWR0aCA9IHJlcXVpcmUoJy4uL3V0aWxzL3dpdGhXaWR0aCcpO1xuXG52YXIgX3dpdGhXaWR0aDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoV2lkdGgpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSGlnaGVyT3JkZXJDb21wb25lbnQgPSByZXF1aXJlKCdyZWFjdC1mbG93LXR5cGVzJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSGlnaGVyT3JkZXJDb21wb25lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0JyZWFrcG9pbnQgPSByZXF1aXJlKCcuLi9zdHlsZXMvY3JlYXRlQnJlYWtwb2ludHMnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9JbmplY3RlZFByb3BzID0ge1xuICAvKipcbiAgICogSWYgaXNXaWR0aERvd24ob3B0aW9ucy5icmVha3BvaW50KSwgcmV0dXJuIHRydWUuXG4gICAqL1xuICBmdWxsU2NyZWVuOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxuXG4vKipcbiAqIERpYWxvZyB3aWxsIHJlc3BvbnNpdmVseSBiZSBmdWxsIHNjcmVlbiAqYXQgb3IgYmVsb3cqIHRoZSBnaXZlbiBicmVha3BvaW50XG4gKiAoZGVmYXVsdHMgdG8gJ3NtJyBmb3IgbW9iaWxlIGRldmljZXMpLlxuICogTm90aWNlIHRoYXQgdGhpcyBIaWdoZXItb3JkZXIgQ29tcG9uZW50IGlzIGluY29tcGF0aWJsZSB3aXRoIHNlcnZlciBzaWRlIHJlbmRlcmluZy5cbiAqL1xudmFyIHdpdGhNb2JpbGVEaWFsb2cgPSBmdW5jdGlvbiB3aXRoTW9iaWxlRGlhbG9nKCkge1xuICB2YXIgb3B0aW9ucyA9IGFyZ3VtZW50cy5sZW5ndGggPiAwICYmIGFyZ3VtZW50c1swXSAhPT0gdW5kZWZpbmVkID8gYXJndW1lbnRzWzBdIDogeyBicmVha3BvaW50OiAnc20nIH07XG4gIHJldHVybiBmdW5jdGlvbiAoQ29tcG9uZW50KSB7XG4gICAgdmFyIGJyZWFrcG9pbnQgPSBvcHRpb25zLmJyZWFrcG9pbnQ7XG5cblxuICAgIGZ1bmN0aW9uIFdpdGhNb2JpbGVEaWFsb2cocHJvcHMpIHtcbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChDb21wb25lbnQsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBmdWxsU2NyZWVuOiAoMCwgX3dpdGhXaWR0aC5pc1dpZHRoRG93bikoYnJlYWtwb2ludCwgcHJvcHMud2lkdGgpIH0sIHByb3BzKSk7XG4gICAgfVxuXG4gICAgV2l0aE1vYmlsZURpYWxvZy5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyB7XG4gICAgICB3aWR0aDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0JyZWFrcG9pbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50LmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50LmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9CcmVha3BvaW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0JyZWFrcG9pbnQpLmlzUmVxdWlyZWRcbiAgICB9IDoge307XG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIFdpdGhNb2JpbGVEaWFsb2cuZGlzcGxheU5hbWUgPSAoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQ29tcG9uZW50LCAnd2l0aE1vYmlsZURpYWxvZycpO1xuICAgIH1cblxuICAgIHJldHVybiAoMCwgX3dpdGhXaWR0aDIuZGVmYXVsdCkoKShXaXRoTW9iaWxlRGlhbG9nKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHdpdGhNb2JpbGVEaWFsb2c7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvRGlhbG9nL3dpdGhNb2JpbGVEaWFsb2cuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0RpYWxvZy93aXRoTW9iaWxlRGlhbG9nLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA1IDI3IDI4IDM1IDM3IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgekluZGV4OiAtMSxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICcxMDAlJyxcbiAgICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgICAgdG9wOiAwLFxuICAgICAgbGVmdDogMCxcbiAgICAgIC8vIFJlbW92ZSBncmV5IGhpZ2hsaWdodFxuICAgICAgV2Via2l0VGFwSGlnaGxpZ2h0Q29sb3I6IHRoZW1lLnBhbGV0dGUuY29tbW9uLnRyYW5zcGFyZW50LFxuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLmNvbW1vbi5saWdodEJsYWNrLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5JyksXG4gICAgICB3aWxsQ2hhbmdlOiAnb3BhY2l0eScsXG4gICAgICBvcGFjaXR5OiAwXG4gICAgfSxcbiAgICBpbnZpc2libGU6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5jb21tb24udHJhbnNwYXJlbnRcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBDYW4gYmUgdXNlZCwgZm9yIGluc3RhbmNlLCB0byByZW5kZXIgYSBsZXR0ZXIgaW5zaWRlIHRoZSBhdmF0YXIuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJhY2tkcm9wIGlzIGludmlzaWJsZS5cbiAgICovXG4gIGludmlzaWJsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZFxufTtcblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgQmFja2Ryb3AgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShCYWNrZHJvcCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQmFja2Ryb3AoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQmFja2Ryb3ApO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChCYWNrZHJvcC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQmFja2Ryb3ApKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEJhY2tkcm9wLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGludmlzaWJsZSA9IF9wcm9wcy5pbnZpc2libGUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnaW52aXNpYmxlJ10pO1xuXG5cbiAgICAgIHZhciBiYWNrZHJvcENsYXNzID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmludmlzaWJsZSwgaW52aXNpYmxlKSwgY2xhc3NOYW1lKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogYmFja2Ryb3BDbGFzcywgJ2FyaWEtaGlkZGVuJzogJ3RydWUnIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBCYWNrZHJvcDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkJhY2tkcm9wLmRlZmF1bHRQcm9wcyA9IHtcbiAgaW52aXNpYmxlOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlCYWNrZHJvcCcgfSkoQmFja2Ryb3ApO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL0JhY2tkcm9wLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9CYWNrZHJvcC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX2tleXMgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2tleXMnKTtcblxudmFyIF9rZXlzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2tleXMpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3JlYWN0RG9tID0gcmVxdWlyZSgncmVhY3QtZG9tJyk7XG5cbnZhciBfcmVhY3REb20yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3REb20pO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93YXJuaW5nID0gcmVxdWlyZSgnd2FybmluZycpO1xuXG52YXIgX3dhcm5pbmcyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2FybmluZyk7XG5cbnZhciBfa2V5Y29kZSA9IHJlcXVpcmUoJ2tleWNvZGUnKTtcblxudmFyIF9rZXljb2RlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2tleWNvZGUpO1xuXG52YXIgX2luRE9NID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvdXRpbC9pbkRPTScpO1xuXG52YXIgX2luRE9NMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luRE9NKTtcblxudmFyIF9jb250YWlucyA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3F1ZXJ5L2NvbnRhaW5zJyk7XG5cbnZhciBfY29udGFpbnMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY29udGFpbnMpO1xuXG52YXIgX2FjdGl2ZUVsZW1lbnQgPSByZXF1aXJlKCdkb20taGVscGVycy9hY3RpdmVFbGVtZW50Jyk7XG5cbnZhciBfYWN0aXZlRWxlbWVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9hY3RpdmVFbGVtZW50KTtcblxudmFyIF9vd25lckRvY3VtZW50ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvb3duZXJEb2N1bWVudCcpO1xuXG52YXIgX293bmVyRG9jdW1lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb3duZXJEb2N1bWVudCk7XG5cbnZhciBfYWRkRXZlbnRMaXN0ZW5lciA9IHJlcXVpcmUoJy4uL3V0aWxzL2FkZEV2ZW50TGlzdGVuZXInKTtcblxudmFyIF9hZGRFdmVudExpc3RlbmVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2FkZEV2ZW50TGlzdGVuZXIpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfRmFkZSA9IHJlcXVpcmUoJy4uL3RyYW5zaXRpb25zL0ZhZGUnKTtcblxudmFyIF9GYWRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ZhZGUpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfbW9kYWxNYW5hZ2VyID0gcmVxdWlyZSgnLi9tb2RhbE1hbmFnZXInKTtcblxudmFyIF9tb2RhbE1hbmFnZXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfbW9kYWxNYW5hZ2VyKTtcblxudmFyIF9CYWNrZHJvcCA9IHJlcXVpcmUoJy4vQmFja2Ryb3AnKTtcblxudmFyIF9CYWNrZHJvcDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CYWNrZHJvcCk7XG5cbnZhciBfUG9ydGFsID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvUG9ydGFsJyk7XG5cbnZhciBfUG9ydGFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BvcnRhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxuLy8gTW9kYWxzIGRvbid0IG9wZW4gb24gdGhlIHNlcnZlciBzbyB0aGlzIHdvbid0IGJyZWFrIGNvbmN1cnJlbmN5LlxuLy8gQ291bGQgYWxzbyBwdXQgdGhpcyBvbiBjb250ZXh0LlxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIG1vZGFsTWFuYWdlciA9ICgwLCBfbW9kYWxNYW5hZ2VyMi5kZWZhdWx0KSgpO1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgaGVpZ2h0OiAnMTAwJScsXG4gICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICAgIHpJbmRleDogdGhlbWUuekluZGV4LmRpYWxvZyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6IDBcbiAgICB9LFxuICAgIGhpZGRlbjoge1xuICAgICAgdmlzaWJpbGl0eTogJ2hpZGRlbidcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBUaGUgQ1NTIGNsYXNzIG5hbWUgb2YgdGhlIGJhY2tkcm9wIGVsZW1lbnQuXG4gICAqL1xuICBCYWNrZHJvcENsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogUGFzcyBhIGNvbXBvbmVudCBjbGFzcyB0byB1c2UgYXMgdGhlIGJhY2tkcm9wLlxuICAgKi9cbiAgQmFja2Ryb3BDb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJhY2tkcm9wIGlzIGludmlzaWJsZS5cbiAgICovXG4gIEJhY2tkcm9wSW52aXNpYmxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgZHVyYXRpb24gZm9yIHRoZSBiYWNrZHJvcCB0cmFuc2l0aW9uLCBpbiBtaWxsaXNlY29uZHMuXG4gICAqIFlvdSBtYXkgc3BlY2lmeSBhIHNpbmdsZSB0aW1lb3V0IGZvciBhbGwgdHJhbnNpdGlvbnMsIG9yIGluZGl2aWR1YWxseSB3aXRoIGFuIG9iamVjdC5cbiAgICovXG4gIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEEgc2luZ2xlIGNoaWxkIGNvbnRlbnQgZWxlbWVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQWx3YXlzIGtlZXAgdGhlIGNoaWxkcmVuIGluIHRoZSBET00uXG4gICAqIFRoaXMgcHJvcGVydHkgY2FuIGJlIHVzZWZ1bCBpbiBTRU8gc2l0dWF0aW9uIG9yXG4gICAqIHdoZW4geW91IHdhbnQgdG8gbWF4aW1pemUgdGhlIHJlc3BvbnNpdmVuZXNzIG9mIHRoZSBNb2RhbC5cbiAgICovXG4gIGtlZXBNb3VudGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBiYWNrZHJvcCBpcyBkaXNhYmxlZC5cbiAgICovXG4gIGRpc2FibGVCYWNrZHJvcDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCBjbGlja2luZyB0aGUgYmFja2Ryb3Agd2lsbCBub3QgZmlyZSB0aGUgYG9uUmVxdWVzdENsb3NlYCBjYWxsYmFjay5cbiAgICovXG4gIGlnbm9yZUJhY2tkcm9wQ2xpY2s6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgaGl0dGluZyBlc2NhcGUgd2lsbCBub3QgZmlyZSB0aGUgYG9uUmVxdWVzdENsb3NlYCBjYWxsYmFjay5cbiAgICovXG4gIGlnbm9yZUVzY2FwZUtleVVwOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBtb2RhbE1hbmFnZXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZXMgd2hlbiB0aGUgYmFja2Ryb3AgaXMgY2xpY2tlZCBvbi5cbiAgICovXG4gIG9uQmFja2Ryb3BDbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIGJlZm9yZSB0aGUgbW9kYWwgaXMgZW50ZXJpbmcuXG4gICAqL1xuICBvbkVudGVyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIG1vZGFsIGlzIGVudGVyaW5nLlxuICAgKi9cbiAgb25FbnRlcmluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBtb2RhbCBoYXMgZW50ZXJlZC5cbiAgICovXG4gIG9uRW50ZXJlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlcyB3aGVuIHRoZSBlc2NhcGUga2V5IGlzIHByZXNzZWQgYW5kIHRoZSBtb2RhbCBpcyBpbiBmb2N1cy5cbiAgICovXG4gIG9uRXNjYXBlS2V5VXA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCBiZWZvcmUgdGhlIG1vZGFsIGlzIGV4aXRpbmcuXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgbW9kYWwgaXMgZXhpdGluZy5cbiAgICovXG4gIG9uRXhpdGluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayBmaXJlZCB3aGVuIHRoZSBtb2RhbCBoYXMgZXhpdGVkLlxuICAgKi9cbiAgb25FeGl0ZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZmlyZWQgd2hlbiB0aGUgY29tcG9uZW50IHJlcXVlc3RzIHRvIGJlIGNsb3NlZC5cbiAgICpcbiAgICogQHBhcmFtIHtvYmplY3R9IGV2ZW50IFRoZSBldmVudCBzb3VyY2Ugb2YgdGhlIGNhbGxiYWNrXG4gICAqL1xuICBvblJlcXVlc3RDbG9zZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIE1vZGFsIGlzIHZpc2libGUuXG4gICAqL1xuICBzaG93OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkXG59O1xuXG4vKipcbiAqIFRoZSBtb2RhbCBjb21wb25lbnQgcHJvdmlkZXMgYSBzb2xpZCBmb3VuZGF0aW9uIGZvciBjcmVhdGluZyBkaWFsb2dzLFxuICogcG9wb3ZlcnMsIG9yIHdoYXRldmVyIGVsc2UuXG4gKiBUaGUgY29tcG9uZW50IHJlbmRlcnMgaXRzIGBjaGlsZHJlbmAgbm9kZSBpbiBmcm9udCBvZiBhIGJhY2tkcm9wIGNvbXBvbmVudC5cbiAqXG4gKiBUaGUgYE1vZGFsYCBvZmZlcnMgYSBmZXcgaGVscGZ1bCBmZWF0dXJlcyBvdmVyIHVzaW5nIGp1c3QgYSBgUG9ydGFsYCBjb21wb25lbnQgYW5kIHNvbWUgc3R5bGVzOlxuICogLSBNYW5hZ2VzIGRpYWxvZyBzdGFja2luZyB3aGVuIG9uZS1hdC1hLXRpbWUganVzdCBpc24ndCBlbm91Z2guXG4gKiAtIENyZWF0ZXMgYSBiYWNrZHJvcCwgZm9yIGRpc2FibGluZyBpbnRlcmFjdGlvbiBiZWxvdyB0aGUgbW9kYWwuXG4gKiAtIEl0IHByb3Blcmx5IG1hbmFnZXMgZm9jdXM7IG1vdmluZyB0byB0aGUgbW9kYWwgY29udGVudCxcbiAqICAgYW5kIGtlZXBpbmcgaXQgdGhlcmUgdW50aWwgdGhlIG1vZGFsIGlzIGNsb3NlZC5cbiAqIC0gSXQgZGlzYWJsZXMgc2Nyb2xsaW5nIG9mIHRoZSBwYWdlIGNvbnRlbnQgd2hpbGUgb3Blbi5cbiAqIC0gQWRkcyB0aGUgYXBwcm9wcmlhdGUgQVJJQSByb2xlcyBhcmUgYXV0b21hdGljYWxseS5cbiAqXG4gKiBUaGlzIGNvbXBvbmVudCBzaGFyZXMgbWFueSBjb25jZXB0cyB3aXRoIFtyZWFjdC1vdmVybGF5c10oaHR0cHM6Ly9yZWFjdC1ib290c3RyYXAuZ2l0aHViLmlvL3JlYWN0LW92ZXJsYXlzLyNtb2RhbHMpLlxuICovXG52YXIgTW9kYWwgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShNb2RhbCwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTW9kYWwoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgTW9kYWwpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gTW9kYWwuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKE1vZGFsKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX2luaXRpYWxpc2VQcm9wcy5jYWxsKF90aGlzKSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKE1vZGFsLCBbe1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxNb3VudCgpIHtcbiAgICAgIGlmICghdGhpcy5wcm9wcy5zaG93KSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBleGl0ZWQ6IHRydWUgfSk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIHRoaXMubW91bnRlZCA9IHRydWU7XG4gICAgICBpZiAodGhpcy5wcm9wcy5zaG93KSB7XG4gICAgICAgIHRoaXMuaGFuZGxlU2hvdygpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxSZWNlaXZlUHJvcHMnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzKG5leHRQcm9wcykge1xuICAgICAgaWYgKG5leHRQcm9wcy5zaG93ICYmIHRoaXMuc3RhdGUuZXhpdGVkKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBleGl0ZWQ6IGZhbHNlIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ2NvbXBvbmVudFdpbGxVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnRXaWxsVXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgaWYgKCF0aGlzLnByb3BzLnNob3cgJiYgbmV4dFByb3BzLnNob3cpIHtcbiAgICAgICAgdGhpcy5jaGVja0ZvckZvY3VzKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcykge1xuICAgICAgaWYgKCFwcmV2UHJvcHMuc2hvdyAmJiB0aGlzLnByb3BzLnNob3cpIHtcbiAgICAgICAgdGhpcy5oYW5kbGVTaG93KCk7XG4gICAgICB9XG4gICAgICAvLyBXZSBhcmUgd2FpdGluZyBmb3IgdGhlIG9uRXhpdGVkIGNhbGxiYWNrIHRvIGNhbGwgaGFuZGxlSGlkZS5cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsVW5tb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgaWYgKHRoaXMucHJvcHMuc2hvdyB8fCAhdGhpcy5zdGF0ZS5leGl0ZWQpIHtcbiAgICAgICAgdGhpcy5oYW5kbGVIaWRlKCk7XG4gICAgICB9XG4gICAgICB0aGlzLm1vdW50ZWQgPSBmYWxzZTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjaGVja0ZvckZvY3VzJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY2hlY2tGb3JGb2N1cygpIHtcbiAgICAgIGlmIChfaW5ET00yLmRlZmF1bHQpIHtcbiAgICAgICAgdGhpcy5sYXN0Rm9jdXMgPSAoMCwgX2FjdGl2ZUVsZW1lbnQyLmRlZmF1bHQpKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAncmVzdG9yZUxhc3RGb2N1cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlc3RvcmVMYXN0Rm9jdXMoKSB7XG4gICAgICBpZiAodGhpcy5sYXN0Rm9jdXMgJiYgdGhpcy5sYXN0Rm9jdXMuZm9jdXMpIHtcbiAgICAgICAgdGhpcy5sYXN0Rm9jdXMuZm9jdXMoKTtcbiAgICAgICAgdGhpcy5sYXN0Rm9jdXMgPSB1bmRlZmluZWQ7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaGFuZGxlU2hvdycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhbmRsZVNob3coKSB7XG4gICAgICB2YXIgZG9jID0gKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShfcmVhY3REb20yLmRlZmF1bHQuZmluZERPTU5vZGUodGhpcykpO1xuICAgICAgdGhpcy5wcm9wcy5tb2RhbE1hbmFnZXIuYWRkKHRoaXMpO1xuICAgICAgdGhpcy5vbkRvY3VtZW50S2V5VXBMaXN0ZW5lciA9ICgwLCBfYWRkRXZlbnRMaXN0ZW5lcjIuZGVmYXVsdCkoZG9jLCAna2V5dXAnLCB0aGlzLmhhbmRsZURvY3VtZW50S2V5VXApO1xuICAgICAgdGhpcy5vbkZvY3VzTGlzdGVuZXIgPSAoMCwgX2FkZEV2ZW50TGlzdGVuZXIyLmRlZmF1bHQpKGRvYywgJ2ZvY3VzJywgdGhpcy5oYW5kbGVGb2N1c0xpc3RlbmVyLCB0cnVlKTtcbiAgICAgIHRoaXMuZm9jdXMoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdmb2N1cycsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGZvY3VzKCkge1xuICAgICAgdmFyIGN1cnJlbnRGb2N1cyA9ICgwLCBfYWN0aXZlRWxlbWVudDIuZGVmYXVsdCkoKDAsIF9vd25lckRvY3VtZW50Mi5kZWZhdWx0KShfcmVhY3REb20yLmRlZmF1bHQuZmluZERPTU5vZGUodGhpcykpKTtcbiAgICAgIHZhciBtb2RhbENvbnRlbnQgPSB0aGlzLm1vZGFsICYmIHRoaXMubW9kYWwubGFzdENoaWxkO1xuICAgICAgdmFyIGZvY3VzSW5Nb2RhbCA9IGN1cnJlbnRGb2N1cyAmJiAoMCwgX2NvbnRhaW5zMi5kZWZhdWx0KShtb2RhbENvbnRlbnQsIGN1cnJlbnRGb2N1cyk7XG5cbiAgICAgIGlmIChtb2RhbENvbnRlbnQgJiYgIWZvY3VzSW5Nb2RhbCkge1xuICAgICAgICBpZiAoIW1vZGFsQ29udGVudC5oYXNBdHRyaWJ1dGUoJ3RhYkluZGV4JykpIHtcbiAgICAgICAgICBtb2RhbENvbnRlbnQuc2V0QXR0cmlidXRlKCd0YWJJbmRleCcsIC0xKTtcbiAgICAgICAgICBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKGZhbHNlLCAnTWF0ZXJpYWwtVUk6IHRoZSBtb2RhbCBjb250ZW50IG5vZGUgZG9lcyBub3QgYWNjZXB0IGZvY3VzLiAnICsgJ0ZvciB0aGUgYmVuZWZpdCBvZiBhc3Npc3RpdmUgdGVjaG5vbG9naWVzLCAnICsgJ3RoZSB0YWJJbmRleCBvZiB0aGUgbm9kZSBpcyBiZWluZyBzZXQgdG8gXCItMVwiLicpIDogdm9pZCAwO1xuICAgICAgICB9XG5cbiAgICAgICAgbW9kYWxDb250ZW50LmZvY3VzKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnaGFuZGxlSGlkZScsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGhhbmRsZUhpZGUoKSB7XG4gICAgICB0aGlzLnByb3BzLm1vZGFsTWFuYWdlci5yZW1vdmUodGhpcyk7XG4gICAgICBpZiAodGhpcy5vbkRvY3VtZW50S2V5VXBMaXN0ZW5lcikgdGhpcy5vbkRvY3VtZW50S2V5VXBMaXN0ZW5lci5yZW1vdmUoKTtcbiAgICAgIGlmICh0aGlzLm9uRm9jdXNMaXN0ZW5lcikgdGhpcy5vbkZvY3VzTGlzdGVuZXIucmVtb3ZlKCk7XG4gICAgICB0aGlzLnJlc3RvcmVMYXN0Rm9jdXMoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXJCYWNrZHJvcCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlckJhY2tkcm9wKCkge1xuICAgICAgdmFyIG90aGVyID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiB7fTtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIEJhY2tkcm9wQ29tcG9uZW50ID0gX3Byb3BzLkJhY2tkcm9wQ29tcG9uZW50LFxuICAgICAgICAgIEJhY2tkcm9wQ2xhc3NOYW1lID0gX3Byb3BzLkJhY2tkcm9wQ2xhc3NOYW1lLFxuICAgICAgICAgIEJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uID0gX3Byb3BzLkJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgIEJhY2tkcm9wSW52aXNpYmxlID0gX3Byb3BzLkJhY2tkcm9wSW52aXNpYmxlLFxuICAgICAgICAgIHNob3cgPSBfcHJvcHMuc2hvdztcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9GYWRlMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgYXBwZWFyOiB0cnVlLCAnaW4nOiBzaG93LCB0aW1lb3V0OiBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbiB9LCBvdGhlciksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KEJhY2tkcm9wQ29tcG9uZW50LCB7XG4gICAgICAgICAgaW52aXNpYmxlOiBCYWNrZHJvcEludmlzaWJsZSxcbiAgICAgICAgICBjbGFzc05hbWU6IEJhY2tkcm9wQ2xhc3NOYW1lLFxuICAgICAgICAgIG9uQ2xpY2s6IHRoaXMuaGFuZGxlQmFja2Ryb3BDbGlja1xuICAgICAgICB9KVxuICAgICAgKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3RoaXMyID0gdGhpcztcblxuICAgICAgdmFyIF9wcm9wczIgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGRpc2FibGVCYWNrZHJvcCA9IF9wcm9wczIuZGlzYWJsZUJhY2tkcm9wLFxuICAgICAgICAgIEJhY2tkcm9wQ29tcG9uZW50ID0gX3Byb3BzMi5CYWNrZHJvcENvbXBvbmVudCxcbiAgICAgICAgICBCYWNrZHJvcENsYXNzTmFtZSA9IF9wcm9wczIuQmFja2Ryb3BDbGFzc05hbWUsXG4gICAgICAgICAgQmFja2Ryb3BUcmFuc2l0aW9uRHVyYXRpb24gPSBfcHJvcHMyLkJhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uLFxuICAgICAgICAgIEJhY2tkcm9wSW52aXNpYmxlID0gX3Byb3BzMi5CYWNrZHJvcEludmlzaWJsZSxcbiAgICAgICAgICBpZ25vcmVCYWNrZHJvcENsaWNrID0gX3Byb3BzMi5pZ25vcmVCYWNrZHJvcENsaWNrLFxuICAgICAgICAgIGlnbm9yZUVzY2FwZUtleVVwID0gX3Byb3BzMi5pZ25vcmVFc2NhcGVLZXlVcCxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wczIuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wczIuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMyLmNsYXNzTmFtZSxcbiAgICAgICAgICBrZWVwTW91bnRlZCA9IF9wcm9wczIua2VlcE1vdW50ZWQsXG4gICAgICAgICAgbW9kYWxNYW5hZ2VyUHJvcCA9IF9wcm9wczIubW9kYWxNYW5hZ2VyLFxuICAgICAgICAgIG9uQmFja2Ryb3BDbGljayA9IF9wcm9wczIub25CYWNrZHJvcENsaWNrLFxuICAgICAgICAgIG9uRXNjYXBlS2V5VXAgPSBfcHJvcHMyLm9uRXNjYXBlS2V5VXAsXG4gICAgICAgICAgb25SZXF1ZXN0Q2xvc2UgPSBfcHJvcHMyLm9uUmVxdWVzdENsb3NlLFxuICAgICAgICAgIG9uRW50ZXIgPSBfcHJvcHMyLm9uRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZyA9IF9wcm9wczIub25FbnRlcmluZyxcbiAgICAgICAgICBvbkVudGVyZWQgPSBfcHJvcHMyLm9uRW50ZXJlZCxcbiAgICAgICAgICBvbkV4aXQgPSBfcHJvcHMyLm9uRXhpdCxcbiAgICAgICAgICBvbkV4aXRpbmcgPSBfcHJvcHMyLm9uRXhpdGluZyxcbiAgICAgICAgICBvbkV4aXRlZCA9IF9wcm9wczIub25FeGl0ZWQsXG4gICAgICAgICAgc2hvdyA9IF9wcm9wczIuc2hvdyxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wczIsIFsnZGlzYWJsZUJhY2tkcm9wJywgJ0JhY2tkcm9wQ29tcG9uZW50JywgJ0JhY2tkcm9wQ2xhc3NOYW1lJywgJ0JhY2tkcm9wVHJhbnNpdGlvbkR1cmF0aW9uJywgJ0JhY2tkcm9wSW52aXNpYmxlJywgJ2lnbm9yZUJhY2tkcm9wQ2xpY2snLCAnaWdub3JlRXNjYXBlS2V5VXAnLCAnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAna2VlcE1vdW50ZWQnLCAnbW9kYWxNYW5hZ2VyJywgJ29uQmFja2Ryb3BDbGljaycsICdvbkVzY2FwZUtleVVwJywgJ29uUmVxdWVzdENsb3NlJywgJ29uRW50ZXInLCAnb25FbnRlcmluZycsICdvbkVudGVyZWQnLCAnb25FeGl0JywgJ29uRXhpdGluZycsICdvbkV4aXRlZCcsICdzaG93J10pO1xuXG5cbiAgICAgIGlmICgha2VlcE1vdW50ZWQgJiYgIXNob3cgJiYgdGhpcy5zdGF0ZS5leGl0ZWQpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIHZhciB0cmFuc2l0aW9uQ2FsbGJhY2tzID0ge1xuICAgICAgICBvbkVudGVyOiBvbkVudGVyLFxuICAgICAgICBvbkVudGVyaW5nOiBvbkVudGVyaW5nLFxuICAgICAgICBvbkVudGVyZWQ6IG9uRW50ZXJlZCxcbiAgICAgICAgb25FeGl0OiBvbkV4aXQsXG4gICAgICAgIG9uRXhpdGluZzogb25FeGl0aW5nLFxuICAgICAgICBvbkV4aXRlZDogdGhpcy5oYW5kbGVUcmFuc2l0aW9uRXhpdGVkXG4gICAgICB9O1xuXG4gICAgICB2YXIgbW9kYWxDaGlsZCA9IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5vbmx5KGNoaWxkcmVuKTtcbiAgICAgIHZhciBfbW9kYWxDaGlsZCRwcm9wcyA9IG1vZGFsQ2hpbGQucHJvcHMsXG4gICAgICAgICAgcm9sZSA9IF9tb2RhbENoaWxkJHByb3BzLnJvbGUsXG4gICAgICAgICAgdGFiSW5kZXggPSBfbW9kYWxDaGlsZCRwcm9wcy50YWJJbmRleDtcblxuICAgICAgdmFyIGNoaWxkUHJvcHMgPSB7fTtcblxuICAgICAgaWYgKHJvbGUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBjaGlsZFByb3BzLnJvbGUgPSByb2xlID09PSB1bmRlZmluZWQgPyAnZG9jdW1lbnQnIDogcm9sZTtcbiAgICAgIH1cblxuICAgICAgaWYgKHRhYkluZGV4ID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgY2hpbGRQcm9wcy50YWJJbmRleCA9IHRhYkluZGV4ID09IG51bGwgPyAtMSA6IHRhYkluZGV4O1xuICAgICAgfVxuXG4gICAgICB2YXIgYmFja2Ryb3BQcm9wcyA9IHZvaWQgMDtcblxuICAgICAgLy8gSXQncyBhIFRyYW5zaXRpb24gbGlrZSBjb21wb25lbnRcbiAgICAgIGlmIChtb2RhbENoaWxkLnByb3BzLmhhc093blByb3BlcnR5KCdpbicpKSB7XG4gICAgICAgICgwLCBfa2V5czIuZGVmYXVsdCkodHJhbnNpdGlvbkNhbGxiYWNrcykuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgY2hpbGRQcm9wc1trZXldID0gKDAsIF9oZWxwZXJzLmNyZWF0ZUNoYWluZWRGdW5jdGlvbikodHJhbnNpdGlvbkNhbGxiYWNrc1trZXldLCBtb2RhbENoaWxkLnByb3BzW2tleV0pO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGJhY2tkcm9wUHJvcHMgPSB0cmFuc2l0aW9uQ2FsbGJhY2tzO1xuICAgICAgfVxuXG4gICAgICBpZiAoKDAsIF9rZXlzMi5kZWZhdWx0KShjaGlsZFByb3BzKS5sZW5ndGgpIHtcbiAgICAgICAgbW9kYWxDaGlsZCA9IF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQobW9kYWxDaGlsZCwgY2hpbGRQcm9wcyk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX1BvcnRhbDIuZGVmYXVsdCxcbiAgICAgICAge1xuICAgICAgICAgIG9wZW46IHRydWUsXG4gICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgX3RoaXMyLm1vdW50Tm9kZSA9IG5vZGUgPyBub2RlLmdldExheWVyKCkgOiBudWxsO1xuICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWUsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmhpZGRlbiwgdGhpcy5zdGF0ZS5leGl0ZWQpKVxuICAgICAgICAgIH0sIG90aGVyLCB7XG4gICAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICAgIF90aGlzMi5tb2RhbCA9IG5vZGU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfSksXG4gICAgICAgICAgIWRpc2FibGVCYWNrZHJvcCAmJiAoIWtlZXBNb3VudGVkIHx8IHNob3cgfHwgIXRoaXMuc3RhdGUuZXhpdGVkKSAmJiB0aGlzLnJlbmRlckJhY2tkcm9wKGJhY2tkcm9wUHJvcHMpLFxuICAgICAgICAgIG1vZGFsQ2hpbGRcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIE1vZGFsO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTW9kYWwuZGVmYXVsdFByb3BzID0ge1xuICBCYWNrZHJvcENvbXBvbmVudDogX0JhY2tkcm9wMi5kZWZhdWx0LFxuICBCYWNrZHJvcFRyYW5zaXRpb25EdXJhdGlvbjogMzAwLFxuICBCYWNrZHJvcEludmlzaWJsZTogZmFsc2UsXG4gIGtlZXBNb3VudGVkOiBmYWxzZSxcbiAgZGlzYWJsZUJhY2tkcm9wOiBmYWxzZSxcbiAgaWdub3JlQmFja2Ryb3BDbGljazogZmFsc2UsXG4gIGlnbm9yZUVzY2FwZUtleVVwOiBmYWxzZSxcbiAgbW9kYWxNYW5hZ2VyOiBtb2RhbE1hbmFnZXJcbn07XG5cbnZhciBfaW5pdGlhbGlzZVByb3BzID0gZnVuY3Rpb24gX2luaXRpYWxpc2VQcm9wcygpIHtcbiAgdmFyIF90aGlzMyA9IHRoaXM7XG5cbiAgdGhpcy5zdGF0ZSA9IHtcbiAgICBleGl0ZWQ6IGZhbHNlXG4gIH07XG4gIHRoaXMub25Eb2N1bWVudEtleVVwTGlzdGVuZXIgPSBudWxsO1xuICB0aGlzLm9uRm9jdXNMaXN0ZW5lciA9IG51bGw7XG4gIHRoaXMubW91bnRlZCA9IGZhbHNlO1xuICB0aGlzLmxhc3RGb2N1cyA9IHVuZGVmaW5lZDtcbiAgdGhpcy5tb2RhbCA9IG51bGw7XG4gIHRoaXMubW91bnROb2RlID0gbnVsbDtcblxuICB0aGlzLmhhbmRsZUZvY3VzTGlzdGVuZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKCFfdGhpczMubW91bnRlZCB8fCAhX3RoaXMzLnByb3BzLm1vZGFsTWFuYWdlci5pc1RvcE1vZGFsKF90aGlzMykpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB2YXIgY3VycmVudEZvY3VzID0gKDAsIF9hY3RpdmVFbGVtZW50Mi5kZWZhdWx0KSgoMCwgX293bmVyRG9jdW1lbnQyLmRlZmF1bHQpKF9yZWFjdERvbTIuZGVmYXVsdC5maW5kRE9NTm9kZShfdGhpczMpKSk7XG4gICAgdmFyIG1vZGFsQ29udGVudCA9IF90aGlzMy5tb2RhbCAmJiBfdGhpczMubW9kYWwubGFzdENoaWxkO1xuXG4gICAgaWYgKG1vZGFsQ29udGVudCAmJiBtb2RhbENvbnRlbnQgIT09IGN1cnJlbnRGb2N1cyAmJiAhKDAsIF9jb250YWluczIuZGVmYXVsdCkobW9kYWxDb250ZW50LCBjdXJyZW50Rm9jdXMpKSB7XG4gICAgICBtb2RhbENvbnRlbnQuZm9jdXMoKTtcbiAgICB9XG4gIH07XG5cbiAgdGhpcy5oYW5kbGVEb2N1bWVudEtleVVwID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgaWYgKCFfdGhpczMubW91bnRlZCB8fCAhX3RoaXMzLnByb3BzLm1vZGFsTWFuYWdlci5pc1RvcE1vZGFsKF90aGlzMykpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAoKDAsIF9rZXljb2RlMi5kZWZhdWx0KShldmVudCkgIT09ICdlc2MnKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIF9wcm9wczMgPSBfdGhpczMucHJvcHMsXG4gICAgICAgIG9uRXNjYXBlS2V5VXAgPSBfcHJvcHMzLm9uRXNjYXBlS2V5VXAsXG4gICAgICAgIG9uUmVxdWVzdENsb3NlID0gX3Byb3BzMy5vblJlcXVlc3RDbG9zZSxcbiAgICAgICAgaWdub3JlRXNjYXBlS2V5VXAgPSBfcHJvcHMzLmlnbm9yZUVzY2FwZUtleVVwO1xuXG5cbiAgICBpZiAob25Fc2NhcGVLZXlVcCkge1xuICAgICAgb25Fc2NhcGVLZXlVcChldmVudCk7XG4gICAgfVxuXG4gICAgaWYgKG9uUmVxdWVzdENsb3NlICYmICFpZ25vcmVFc2NhcGVLZXlVcCkge1xuICAgICAgb25SZXF1ZXN0Q2xvc2UoZXZlbnQpO1xuICAgIH1cbiAgfTtcblxuICB0aGlzLmhhbmRsZUJhY2tkcm9wQ2xpY2sgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICBpZiAoZXZlbnQudGFyZ2V0ICE9PSBldmVudC5jdXJyZW50VGFyZ2V0KSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdmFyIF9wcm9wczQgPSBfdGhpczMucHJvcHMsXG4gICAgICAgIG9uQmFja2Ryb3BDbGljayA9IF9wcm9wczQub25CYWNrZHJvcENsaWNrLFxuICAgICAgICBvblJlcXVlc3RDbG9zZSA9IF9wcm9wczQub25SZXF1ZXN0Q2xvc2UsXG4gICAgICAgIGlnbm9yZUJhY2tkcm9wQ2xpY2sgPSBfcHJvcHM0Lmlnbm9yZUJhY2tkcm9wQ2xpY2s7XG5cblxuICAgIGlmIChvbkJhY2tkcm9wQ2xpY2spIHtcbiAgICAgIG9uQmFja2Ryb3BDbGljayhldmVudCk7XG4gICAgfVxuXG4gICAgaWYgKG9uUmVxdWVzdENsb3NlICYmICFpZ25vcmVCYWNrZHJvcENsaWNrKSB7XG4gICAgICBvblJlcXVlc3RDbG9zZShldmVudCk7XG4gICAgfVxuICB9O1xuXG4gIHRoaXMuaGFuZGxlVHJhbnNpdGlvbkV4aXRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoX3RoaXMzLnByb3BzLm9uRXhpdGVkKSB7XG4gICAgICB2YXIgX3Byb3BzNTtcblxuICAgICAgKF9wcm9wczUgPSBfdGhpczMucHJvcHMpLm9uRXhpdGVkLmFwcGx5KF9wcm9wczUsIGFyZ3VtZW50cyk7XG4gICAgfVxuXG4gICAgX3RoaXMzLnNldFN0YXRlKHsgZXhpdGVkOiB0cnVlIH0pO1xuICAgIF90aGlzMy5oYW5kbGVIaWRlKCk7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBmbGlwOiBmYWxzZSwgbmFtZTogJ011aU1vZGFsJyB9KShNb2RhbCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvTW9kYWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL01vZGFsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9Nb2RhbCA9IHJlcXVpcmUoJy4vTW9kYWwnKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfTW9kYWwpLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9Nb2RhbC9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX2lzV2luZG93ID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvcXVlcnkvaXNXaW5kb3cnKTtcblxudmFyIF9pc1dpbmRvdzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1dpbmRvdyk7XG5cbnZhciBfb3duZXJEb2N1bWVudCA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL293bmVyRG9jdW1lbnQnKTtcblxudmFyIF9vd25lckRvY3VtZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX293bmVyRG9jdW1lbnQpO1xuXG52YXIgX2luRE9NID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvdXRpbC9pbkRPTScpO1xuXG52YXIgX2luRE9NMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luRE9NKTtcblxudmFyIF9zY3JvbGxiYXJTaXplID0gcmVxdWlyZSgnZG9tLWhlbHBlcnMvdXRpbC9zY3JvbGxiYXJTaXplJyk7XG5cbnZhciBfc2Nyb2xsYmFyU2l6ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zY3JvbGxiYXJTaXplKTtcblxudmFyIF9tYW5hZ2VBcmlhSGlkZGVuID0gcmVxdWlyZSgnLi4vdXRpbHMvbWFuYWdlQXJpYUhpZGRlbicpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG4vLyBUYWtlbiBmcm9tIGh0dHBzOi8vZ2l0aHViLmNvbS9yZWFjdC1ib290c3RyYXAvcmVhY3Qtb3ZlcmxheXMvYmxvYi9tYXN0ZXIvc3JjL01vZGFsTWFuYWdlci5qc1xuXG5mdW5jdGlvbiBnZXRQYWRkaW5nUmlnaHQobm9kZSkge1xuICByZXR1cm4gcGFyc2VJbnQobm9kZS5zdHlsZS5wYWRkaW5nUmlnaHQgfHwgMCwgMTApO1xufVxuXG4vLyBEbyB3ZSBoYXZlIGEgc2Nyb2xsIGJhcj9cbmZ1bmN0aW9uIGJvZHlJc092ZXJmbG93aW5nKG5vZGUpIHtcbiAgdmFyIGRvYyA9ICgwLCBfb3duZXJEb2N1bWVudDIuZGVmYXVsdCkobm9kZSk7XG4gIHZhciB3aW4gPSAoMCwgX2lzV2luZG93Mi5kZWZhdWx0KShkb2MpO1xuXG4gIC8vIFRha2VzIGluIGFjY291bnQgcG90ZW50aWFsIG5vbiB6ZXJvIG1hcmdpbiBvbiB0aGUgYm9keS5cbiAgdmFyIHN0eWxlID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZG9jLmJvZHkpO1xuICB2YXIgbWFyZ2luTGVmdCA9IHBhcnNlSW50KHN0eWxlLmdldFByb3BlcnR5VmFsdWUoJ21hcmdpbi1sZWZ0JyksIDEwKTtcbiAgdmFyIG1hcmdpblJpZ2h0ID0gcGFyc2VJbnQoc3R5bGUuZ2V0UHJvcGVydHlWYWx1ZSgnbWFyZ2luLXJpZ2h0JyksIDEwKTtcblxuICByZXR1cm4gbWFyZ2luTGVmdCArIGRvYy5ib2R5LmNsaWVudFdpZHRoICsgbWFyZ2luUmlnaHQgPCB3aW4uaW5uZXJXaWR0aDtcbn1cblxuZnVuY3Rpb24gZ2V0Q29udGFpbmVyKCkge1xuICB2YXIgY29udGFpbmVyID0gX2luRE9NMi5kZWZhdWx0ID8gd2luZG93LmRvY3VtZW50LmJvZHkgOiB7fTtcbiAgcHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09IFwicHJvZHVjdGlvblwiID8gKDAsIF93YXJuaW5nMi5kZWZhdWx0KShjb250YWluZXIgIT09IG51bGwsICdcXG5NYXRlcmlhbC1VSTogeW91IGFyZSBtb3N0IGxpa2VseSBldmFsdWF0aW5nIHRoZSBjb2RlIGJlZm9yZSB0aGVcXG5icm93c2VyIGhhcyBhIGNoYW5jZSB0byByZWFjaCB0aGUgPGJvZHk+LlxcblBsZWFzZSBtb3ZlIHRoZSBpbXBvcnQgYXQgdGhlIGVuZCBvZiB0aGUgPGJvZHk+LlxcbiAgJykgOiB2b2lkIDA7XG4gIHJldHVybiBjb250YWluZXI7XG59XG4vKipcbiAqIFN0YXRlIG1hbmFnZW1lbnQgaGVscGVyIGZvciBtb2RhbHMvbGF5ZXJzLlxuICogU2ltcGxpZmllZCwgYnV0IGluc3BpcmVkIGJ5IHJlYWN0LW92ZXJsYXkncyBNb2RhbE1hbmFnZXIgY2xhc3NcbiAqXG4gKiBAaW50ZXJuYWwgVXNlZCBieSB0aGUgTW9kYWwgdG8gZW5zdXJlIHByb3BlciBmb2N1cyBtYW5hZ2VtZW50LlxuICovXG5mdW5jdGlvbiBjcmVhdGVNb2RhbE1hbmFnZXIoKSB7XG4gIHZhciBfcmVmID0gYXJndW1lbnRzLmxlbmd0aCA+IDAgJiYgYXJndW1lbnRzWzBdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMF0gOiB7fSxcbiAgICAgIF9yZWYkaGlkZVNpYmxpbmdOb2RlcyA9IF9yZWYuaGlkZVNpYmxpbmdOb2RlcyxcbiAgICAgIGhpZGVTaWJsaW5nTm9kZXMgPSBfcmVmJGhpZGVTaWJsaW5nTm9kZXMgPT09IHVuZGVmaW5lZCA/IHRydWUgOiBfcmVmJGhpZGVTaWJsaW5nTm9kZXM7XG5cbiAgdmFyIG1vZGFscyA9IFtdO1xuXG4gIHZhciBwcmV2T3ZlcmZsb3cgPSB2b2lkIDA7XG4gIHZhciBwcmV2UGFkZGluZ3MgPSBbXTtcblxuICBmdW5jdGlvbiBhZGQobW9kYWwpIHtcbiAgICB2YXIgY29udGFpbmVyID0gZ2V0Q29udGFpbmVyKCk7XG4gICAgdmFyIG1vZGFsSWR4ID0gbW9kYWxzLmluZGV4T2YobW9kYWwpO1xuXG4gICAgaWYgKG1vZGFsSWR4ICE9PSAtMSkge1xuICAgICAgcmV0dXJuIG1vZGFsSWR4O1xuICAgIH1cblxuICAgIG1vZGFsSWR4ID0gbW9kYWxzLmxlbmd0aDtcbiAgICBtb2RhbHMucHVzaChtb2RhbCk7XG5cbiAgICBpZiAoaGlkZVNpYmxpbmdOb2Rlcykge1xuICAgICAgKDAsIF9tYW5hZ2VBcmlhSGlkZGVuLmhpZGVTaWJsaW5ncykoY29udGFpbmVyLCBtb2RhbC5tb3VudE5vZGUpO1xuICAgIH1cblxuICAgIGlmIChtb2RhbHMubGVuZ3RoID09PSAxKSB7XG4gICAgICAvLyBTYXZlIG91ciBjdXJyZW50IG92ZXJmbG93IHNvIHdlIGNhbiByZXZlcnRcbiAgICAgIC8vIGJhY2sgdG8gaXQgd2hlbiBhbGwgbW9kYWxzIGFyZSBjbG9zZWQhXG4gICAgICBwcmV2T3ZlcmZsb3cgPSBjb250YWluZXIuc3R5bGUub3ZlcmZsb3c7XG5cbiAgICAgIGlmIChib2R5SXNPdmVyZmxvd2luZyhjb250YWluZXIpKSB7XG4gICAgICAgIHByZXZQYWRkaW5ncyA9IFtnZXRQYWRkaW5nUmlnaHQoY29udGFpbmVyKV07XG4gICAgICAgIHZhciBzY3JvbGxiYXJTaXplID0gKDAsIF9zY3JvbGxiYXJTaXplMi5kZWZhdWx0KSgpO1xuICAgICAgICBjb250YWluZXIuc3R5bGUucGFkZGluZ1JpZ2h0ID0gcHJldlBhZGRpbmdzWzBdICsgc2Nyb2xsYmFyU2l6ZSArICdweCc7XG5cbiAgICAgICAgdmFyIGZpeGVkTm9kZXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubXVpLWZpeGVkJyk7XG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgZml4ZWROb2Rlcy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICAgIHZhciBwYWRkaW5nUmlnaHQgPSBnZXRQYWRkaW5nUmlnaHQoZml4ZWROb2Rlc1tpXSk7XG4gICAgICAgICAgcHJldlBhZGRpbmdzLnB1c2gocGFkZGluZ1JpZ2h0KTtcbiAgICAgICAgICBmaXhlZE5vZGVzW2ldLnN0eWxlLnBhZGRpbmdSaWdodCA9IHBhZGRpbmdSaWdodCArIHNjcm9sbGJhclNpemUgKyAncHgnO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGNvbnRhaW5lci5zdHlsZS5vdmVyZmxvdyA9ICdoaWRkZW4nO1xuICAgIH1cblxuICAgIHJldHVybiBtb2RhbElkeDtcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlbW92ZShtb2RhbCkge1xuICAgIHZhciBjb250YWluZXIgPSBnZXRDb250YWluZXIoKTtcbiAgICB2YXIgbW9kYWxJZHggPSBtb2RhbHMuaW5kZXhPZihtb2RhbCk7XG5cbiAgICBpZiAobW9kYWxJZHggPT09IC0xKSB7XG4gICAgICByZXR1cm4gbW9kYWxJZHg7XG4gICAgfVxuXG4gICAgbW9kYWxzLnNwbGljZShtb2RhbElkeCwgMSk7XG5cbiAgICBpZiAobW9kYWxzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgY29udGFpbmVyLnN0eWxlLm92ZXJmbG93ID0gcHJldk92ZXJmbG93O1xuICAgICAgY29udGFpbmVyLnN0eWxlLnBhZGRpbmdSaWdodCA9IHByZXZQYWRkaW5nc1swXTtcblxuICAgICAgdmFyIGZpeGVkTm9kZXMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcubXVpLWZpeGVkJyk7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZpeGVkTm9kZXMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgZml4ZWROb2Rlc1tpXS5zdHlsZS5wYWRkaW5nUmlnaHQgPSBwcmV2UGFkZGluZ3NbaSArIDFdICsgJ3B4JztcbiAgICAgIH1cblxuICAgICAgcHJldk92ZXJmbG93ID0gdW5kZWZpbmVkO1xuICAgICAgcHJldlBhZGRpbmdzID0gW107XG4gICAgICBpZiAoaGlkZVNpYmxpbmdOb2Rlcykge1xuICAgICAgICAoMCwgX21hbmFnZUFyaWFIaWRkZW4uc2hvd1NpYmxpbmdzKShjb250YWluZXIsIG1vZGFsLm1vdW50Tm9kZSk7XG4gICAgICB9XG4gICAgfSBlbHNlIGlmIChoaWRlU2libGluZ05vZGVzKSB7XG4gICAgICAvLyBvdGhlcndpc2UgbWFrZSBzdXJlIHRoZSBuZXh0IHRvcCBtb2RhbCBpcyB2aXNpYmxlIHRvIGEgU1JcbiAgICAgICgwLCBfbWFuYWdlQXJpYUhpZGRlbi5hcmlhSGlkZGVuKShmYWxzZSwgbW9kYWxzW21vZGFscy5sZW5ndGggLSAxXS5tb3VudE5vZGUpO1xuICAgIH1cblxuICAgIHJldHVybiBtb2RhbElkeDtcbiAgfVxuXG4gIGZ1bmN0aW9uIGlzVG9wTW9kYWwobW9kYWwpIHtcbiAgICByZXR1cm4gISFtb2RhbHMubGVuZ3RoICYmIG1vZGFsc1ttb2RhbHMubGVuZ3RoIC0gMV0gPT09IG1vZGFsO1xuICB9XG5cbiAgdmFyIG1vZGFsTWFuYWdlciA9IHsgYWRkOiBhZGQsIHJlbW92ZTogcmVtb3ZlLCBpc1RvcE1vZGFsOiBpc1RvcE1vZGFsIH07XG5cbiAgcmV0dXJuIG1vZGFsTWFuYWdlcjtcbn1cblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlTW9kYWxNYW5hZ2VyO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL01vZGFsL21vZGFsTWFuYWdlci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvTW9kYWwvbW9kYWxNYW5hZ2VyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9yZWFjdERvbSA9IHJlcXVpcmUoJ3JlYWN0LWRvbScpO1xuXG52YXIgX3JlYWN0RG9tMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0RG9tKTtcblxudmFyIF9pbkRPTSA9IHJlcXVpcmUoJ2RvbS1oZWxwZXJzL3V0aWwvaW5ET00nKTtcblxudmFyIF9pbkRPTTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbkRPTSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IHRvIHBvcnRhbCBpbiBvcmRlciB0byBlc2NhcGUgdGhlIHBhcmVudCBET00gbm9kZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCB0aGUgY2hpbGRyZW4gd2lsbCBiZSBtb3VudGVkIGludG8gdGhlIERPTS5cbiAgICovXG4gIG9wZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59O1xuXG4vKipcbiAqIEBpZ25vcmUgLSBpbnRlcm5hbCBjb21wb25lbnQuXG4gKi9cbnZhciBQb3J0YWwgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShQb3J0YWwsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFBvcnRhbCgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBQb3J0YWwpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gUG9ydGFsLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShQb3J0YWwpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5sYXllciA9IG51bGwsIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShQb3J0YWwsIFt7XG4gICAga2V5OiAnY29tcG9uZW50RGlkTW91bnQnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcbiAgICAgIGlmICghX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbCkge1xuICAgICAgICB0aGlzLnJlbmRlckxheWVyKCk7XG4gICAgICB9XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKCkge1xuICAgICAgLy8gU3VwcG9ydCByZWFjdEAxNS54LCB3aWxsIGJlIHJlbW92ZWQgYXQgc29tZSBwb2ludFxuICAgICAgaWYgKCFfcmVhY3REb20yLmRlZmF1bHQuY3JlYXRlUG9ydGFsKSB7XG4gICAgICAgIHRoaXMucmVuZGVyTGF5ZXIoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsVW5tb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgdGhpcy51bnJlbmRlckxheWVyKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnZ2V0TGF5ZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBnZXRMYXllcigpIHtcbiAgICAgIGlmICghdGhpcy5sYXllcikge1xuICAgICAgICB0aGlzLmxheWVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgIHRoaXMubGF5ZXIuc2V0QXR0cmlidXRlKCdkYXRhLW11aS1wb3J0YWwnLCAndHJ1ZScpO1xuICAgICAgICBpZiAoZG9jdW1lbnQuYm9keSAmJiB0aGlzLmxheWVyKSB7XG4gICAgICAgICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZCh0aGlzLmxheWVyKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5sYXllcjtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICd1bnJlbmRlckxheWVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gdW5yZW5kZXJMYXllcigpIHtcbiAgICAgIGlmICghdGhpcy5sYXllcikge1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIC8vIFN1cHBvcnQgcmVhY3RAMTUueCwgd2lsbCBiZSByZW1vdmVkIGF0IHNvbWUgcG9pbnRcbiAgICAgIGlmICghX3JlYWN0RG9tMi5kZWZhdWx0LmNyZWF0ZVBvcnRhbCkge1xuICAgICAgICBfcmVhY3REb20yLmRlZmF1bHQudW5tb3VudENvbXBvbmVudEF0Tm9kZSh0aGlzLmxheWVyKTtcbiAgICAgIH1cblxuICAgICAgaWYgKGRvY3VtZW50LmJvZHkpIHtcbiAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZCh0aGlzLmxheWVyKTtcbiAgICAgIH1cbiAgICAgIHRoaXMubGF5ZXIgPSBudWxsO1xuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlckxheWVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyTGF5ZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBvcGVuID0gX3Byb3BzLm9wZW47XG5cblxuICAgICAgaWYgKG9wZW4pIHtcbiAgICAgICAgLy8gQnkgY2FsbGluZyB0aGlzIG1ldGhvZCBpbiBjb21wb25lbnREaWRNb3VudCgpIGFuZFxuICAgICAgICAvLyBjb21wb25lbnREaWRVcGRhdGUoKSwgeW91J3JlIGVmZmVjdGl2ZWx5IGNyZWF0aW5nIGEgXCJ3b3JtaG9sZVwiIHRoYXRcbiAgICAgICAgLy8gZnVubmVscyBSZWFjdCdzIGhpZXJhcmNoaWNhbCB1cGRhdGVzIHRocm91Z2ggdG8gYSBET00gbm9kZSBvbiBhblxuICAgICAgICAvLyBlbnRpcmVseSBkaWZmZXJlbnQgcGFydCBvZiB0aGUgcGFnZS5cbiAgICAgICAgdmFyIGxheWVyRWxlbWVudCA9IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5vbmx5KGNoaWxkcmVuKTtcbiAgICAgICAgX3JlYWN0RG9tMi5kZWZhdWx0LnVuc3RhYmxlX3JlbmRlclN1YnRyZWVJbnRvQ29udGFpbmVyKHRoaXMsIGxheWVyRWxlbWVudCwgdGhpcy5nZXRMYXllcigpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMudW5yZW5kZXJMYXllcigpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMyID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wczIuY2hpbGRyZW4sXG4gICAgICAgICAgb3BlbiA9IF9wcm9wczIub3BlbjtcblxuICAgICAgLy8gU3VwcG9ydCByZWFjdEAxNS54LCB3aWxsIGJlIHJlbW92ZWQgYXQgc29tZSBwb2ludFxuXG4gICAgICBpZiAoIV9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwpIHtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIC8vIENhbid0IGJlIHJlbmRlcmVkIHNlcnZlci1zaWRlLlxuICAgICAgaWYgKF9pbkRPTTIuZGVmYXVsdCkge1xuICAgICAgICBpZiAob3Blbikge1xuICAgICAgICAgIHZhciBsYXllciA9IHRoaXMuZ2V0TGF5ZXIoKTtcbiAgICAgICAgICAvLyAkRmxvd0ZpeE1lIGxheWVyIGlzIG5vbi1udWxsXG4gICAgICAgICAgcmV0dXJuIF9yZWFjdERvbTIuZGVmYXVsdC5jcmVhdGVQb3J0YWwoY2hpbGRyZW4sIGxheWVyKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMudW5yZW5kZXJMYXllcigpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFBvcnRhbDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblBvcnRhbC5kZWZhdWx0UHJvcHMgPSB7XG4gIG9wZW46IGZhbHNlXG59O1xuUG9ydGFsLnByb3BUeXBlcyA9IHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/IHtcbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcbiAgb3BlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2xcbn0gOiB7fTtcbmV4cG9ydHMuZGVmYXVsdCA9IFBvcnRhbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC9Qb3J0YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL1BvcnRhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCJcInVzZSBzdHJpY3RcIjtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm9uZU9mVHlwZShbcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLCByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zaGFwZSh7XG4gIGVudGVyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgZXhpdDogcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLmlzUmVxdWlyZWRcbn0pXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5mdW5jO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNsYXNzZXMgPSB7XG4gIGFwcGVhcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBhcHBlYXJBY3RpdmU6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZW50ZXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZW50ZXJBY3RpdmU6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZXhpdDogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBleGl0QWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmdcbn07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMTEgMjcgMjggMzQgMzUgMzYgMzcgMzggNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfVHJhbnNpdGlvbiA9IHJlcXVpcmUoJ3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvVHJhbnNpdGlvbicpO1xuXG52YXIgX1RyYW5zaXRpb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVHJhbnNpdGlvbik7XG5cbnZhciBfdHJhbnNpdGlvbnMgPSByZXF1aXJlKCcuLi9zdHlsZXMvdHJhbnNpdGlvbnMnKTtcblxudmFyIF93aXRoVGhlbWUgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFRoZW1lJyk7XG5cbnZhciBfd2l0aFRoZW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhUaGVtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IFRyYW5zaXRpb25cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJy4uL2ludGVybmFsL3RyYW5zaXRpb24nKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgYXBwZWFyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBBIHNpbmdsZSBjaGlsZCBjb250ZW50IGVsZW1lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50LmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50LmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNvbXBvbmVudCB3aWxsIHRyYW5zaXRpb24gaW4uXG4gICAqL1xuICBpbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRXhpdDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBzdHlsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGR1cmF0aW9uIGZvciB0aGUgdHJhbnNpdGlvbiwgaW4gbWlsbGlzZWNvbmRzLlxuICAgKiBZb3UgbWF5IHNwZWNpZnkgYSBzaW5nbGUgdGltZW91dCBmb3IgYWxsIHRyYW5zaXRpb25zLCBvciBpbmRpdmlkdWFsbHkgd2l0aCBhbiBvYmplY3QuXG4gICAqL1xuICB0aW1lb3V0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkR1cmF0aW9uLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24uaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24pLmlzUmVxdWlyZWRcbn07XG5cblxudmFyIHJlZmxvdyA9IGZ1bmN0aW9uIHJlZmxvdyhub2RlKSB7XG4gIHJldHVybiBub2RlLnNjcm9sbFRvcDtcbn07XG5cbi8qKlxuICogVGhlIEZhZGUgdHJhbnNpdGlvbiBpcyB1c2VkIGJ5IHRoZSBNb2RhbCBjb21wb25lbnQuXG4gKiBJdCdzIHVzaW5nIFtyZWFjdC10cmFuc2l0aW9uLWdyb3VwXShodHRwczovL2dpdGh1Yi5jb20vcmVhY3Rqcy9yZWFjdC10cmFuc2l0aW9uLWdyb3VwKSBpbnRlcm5hbGx5LlxuICovXG5cbnZhciBGYWRlID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoRmFkZSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gRmFkZSgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBGYWRlKTtcblxuICAgIGZvciAodmFyIF9sZW4gPSBhcmd1bWVudHMubGVuZ3RoLCBhcmdzID0gQXJyYXkoX2xlbiksIF9rZXkgPSAwOyBfa2V5IDwgX2xlbjsgX2tleSsrKSB7XG4gICAgICBhcmdzW19rZXldID0gYXJndW1lbnRzW19rZXldO1xuICAgIH1cblxuICAgIHJldHVybiBfcmV0ID0gKF90ZW1wID0gKF90aGlzID0gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoX3JlZiA9IEZhZGUuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEZhZGUpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy5oYW5kbGVFbnRlciA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICBub2RlLnN0eWxlLm9wYWNpdHkgPSAnMCc7XG4gICAgICByZWZsb3cobm9kZSk7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXIobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlRW50ZXJpbmcgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wcy50aGVtZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMudGltZW91dDtcblxuICAgICAgbm9kZS5zdHlsZS50cmFuc2l0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5Jywge1xuICAgICAgICBkdXJhdGlvbjogdHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInID8gdGltZW91dCA6IHRpbWVvdXQuZW50ZXJcbiAgICAgIH0pO1xuICAgICAgLy8gJEZsb3dGaXhNZSAtIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9mbG93L3B1bGwvNTE2MVxuICAgICAgbm9kZS5zdHlsZS53ZWJraXRUcmFuc2l0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5Jywge1xuICAgICAgICBkdXJhdGlvbjogdHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInID8gdGltZW91dCA6IHRpbWVvdXQuZW50ZXJcbiAgICAgIH0pO1xuICAgICAgbm9kZS5zdHlsZS5vcGFjaXR5ID0gJzEnO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcmluZykge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyaW5nKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUV4aXQgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMyLnRoZW1lLFxuICAgICAgICAgIHRpbWVvdXQgPSBfdGhpcyRwcm9wczIudGltZW91dDtcblxuICAgICAgbm9kZS5zdHlsZS50cmFuc2l0aW9uID0gdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdvcGFjaXR5Jywge1xuICAgICAgICBkdXJhdGlvbjogdHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInID8gdGltZW91dCA6IHRpbWVvdXQuZXhpdFxuICAgICAgfSk7XG4gICAgICAvLyAkRmxvd0ZpeE1lIC0gaHR0cHM6Ly9naXRodWIuY29tL2ZhY2Vib29rL2Zsb3cvcHVsbC81MTYxXG4gICAgICBub2RlLnN0eWxlLndlYmtpdFRyYW5zaXRpb24gPSB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ29wYWNpdHknLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0eXBlb2YgdGltZW91dCA9PT0gJ251bWJlcicgPyB0aW1lb3V0IDogdGltZW91dC5leGl0XG4gICAgICB9KTtcbiAgICAgIG5vZGUuc3R5bGUub3BhY2l0eSA9ICcwJztcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRXhpdCkge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkV4aXQobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEZhZGUsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYXBwZWFyID0gX3Byb3BzLmFwcGVhcixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBvbkVudGVyID0gX3Byb3BzLm9uRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZyA9IF9wcm9wcy5vbkVudGVyaW5nLFxuICAgICAgICAgIG9uRXhpdCA9IF9wcm9wcy5vbkV4aXQsXG4gICAgICAgICAgc3R5bGVQcm9wID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICAgIHRoZW1lID0gX3Byb3BzLnRoZW1lLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2FwcGVhcicsICdjaGlsZHJlbicsICdvbkVudGVyJywgJ29uRW50ZXJpbmcnLCAnb25FeGl0JywgJ3N0eWxlJywgJ3RoZW1lJ10pO1xuXG5cbiAgICAgIHZhciBzdHlsZSA9ICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe30sIHN0eWxlUHJvcCk7XG5cbiAgICAgIC8vIEZvciBzZXJ2ZXIgc2lkZSByZW5kZXJpbmcuXG4gICAgICBpZiAoIXRoaXMucHJvcHMuaW4gfHwgYXBwZWFyKSB7XG4gICAgICAgIHN0eWxlLm9wYWNpdHkgPSAnMCc7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgX1RyYW5zaXRpb24yLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGFwcGVhcjogYXBwZWFyLFxuICAgICAgICAgIHN0eWxlOiBzdHlsZSxcbiAgICAgICAgICBvbkVudGVyOiB0aGlzLmhhbmRsZUVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmc6IHRoaXMuaGFuZGxlRW50ZXJpbmcsXG4gICAgICAgICAgb25FeGl0OiB0aGlzLmhhbmRsZUV4aXRcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEZhZGU7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5GYWRlLmRlZmF1bHRQcm9wcyA9IHtcbiAgYXBwZWFyOiB0cnVlLFxuICB0aW1lb3V0OiB7XG4gICAgZW50ZXI6IF90cmFuc2l0aW9ucy5kdXJhdGlvbi5lbnRlcmluZ1NjcmVlbixcbiAgICBleGl0OiBfdHJhbnNpdGlvbnMuZHVyYXRpb24ubGVhdmluZ1NjcmVlblxuICB9XG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoVGhlbWUyLmRlZmF1bHQpKCkoRmFkZSk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvRmFkZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvRmFkZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMjcgMjggMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLmFyaWFIaWRkZW4gPSBhcmlhSGlkZGVuO1xuZXhwb3J0cy5oaWRlU2libGluZ3MgPSBoaWRlU2libGluZ3M7XG5leHBvcnRzLnNob3dTaWJsaW5ncyA9IHNob3dTaWJsaW5ncztcbi8vICB3ZWFrXG5cbnZhciBCTEFDS0xJU1QgPSBbJ3RlbXBsYXRlJywgJ3NjcmlwdCcsICdzdHlsZSddO1xuXG52YXIgaXNIaWRhYmxlID0gZnVuY3Rpb24gaXNIaWRhYmxlKF9yZWYpIHtcbiAgdmFyIG5vZGVUeXBlID0gX3JlZi5ub2RlVHlwZSxcbiAgICAgIHRhZ05hbWUgPSBfcmVmLnRhZ05hbWU7XG4gIHJldHVybiBub2RlVHlwZSA9PT0gMSAmJiBCTEFDS0xJU1QuaW5kZXhPZih0YWdOYW1lLnRvTG93ZXJDYXNlKCkpID09PSAtMTtcbn07XG5cbnZhciBzaWJsaW5ncyA9IGZ1bmN0aW9uIHNpYmxpbmdzKGNvbnRhaW5lciwgbW91bnQsIGNiKSB7XG4gIG1vdW50ID0gW10uY29uY2F0KG1vdW50KTsgLy8gZXNsaW50LWRpc2FibGUtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICBbXS5mb3JFYWNoLmNhbGwoY29udGFpbmVyLmNoaWxkcmVuLCBmdW5jdGlvbiAobm9kZSkge1xuICAgIGlmIChtb3VudC5pbmRleE9mKG5vZGUpID09PSAtMSAmJiBpc0hpZGFibGUobm9kZSkpIHtcbiAgICAgIGNiKG5vZGUpO1xuICAgIH1cbiAgfSk7XG59O1xuXG5mdW5jdGlvbiBhcmlhSGlkZGVuKHNob3csIG5vZGUpIHtcbiAgaWYgKCFub2RlKSB7XG4gICAgcmV0dXJuO1xuICB9XG4gIGlmIChzaG93KSB7XG4gICAgbm9kZS5zZXRBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJywgJ3RydWUnKTtcbiAgfSBlbHNlIHtcbiAgICBub2RlLnJlbW92ZUF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nKTtcbiAgfVxufVxuXG5mdW5jdGlvbiBoaWRlU2libGluZ3MoY29udGFpbmVyLCBtb3VudE5vZGUpIHtcbiAgc2libGluZ3MoY29udGFpbmVyLCBtb3VudE5vZGUsIGZ1bmN0aW9uIChub2RlKSB7XG4gICAgcmV0dXJuIGFyaWFIaWRkZW4odHJ1ZSwgbm9kZSk7XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBzaG93U2libGluZ3MoY29udGFpbmVyLCBtb3VudE5vZGUpIHtcbiAgc2libGluZ3MoY29udGFpbmVyLCBtb3VudE5vZGUsIGZ1bmN0aW9uIChub2RlKSB7XG4gICAgcmV0dXJuIGFyaWFIaWRkZW4oZmFsc2UsIG5vZGUpO1xuICB9KTtcbn1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS91dGlscy9tYW5hZ2VBcmlhSGlkZGVuLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS91dGlscy9tYW5hZ2VBcmlhSGlkZGVuLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgNSAyNyAyOCAzNCAzNSAzNiAzNyA0MCA0MSA0MiA0MyA0NiIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9pbnZhcmlhbnQgPSByZXF1aXJlKCdpbnZhcmlhbnQnKTtcblxudmFyIF9pbnZhcmlhbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW52YXJpYW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKG9iaiwga2V5cykgeyB2YXIgdGFyZ2V0ID0ge307IGZvciAodmFyIGkgaW4gb2JqKSB7IGlmIChrZXlzLmluZGV4T2YoaSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgaSkpIGNvbnRpbnVlOyB0YXJnZXRbaV0gPSBvYmpbaV07IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBpc01vZGlmaWVkRXZlbnQgPSBmdW5jdGlvbiBpc01vZGlmaWVkRXZlbnQoZXZlbnQpIHtcbiAgcmV0dXJuICEhKGV2ZW50Lm1ldGFLZXkgfHwgZXZlbnQuYWx0S2V5IHx8IGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQuc2hpZnRLZXkpO1xufTtcblxuLyoqXG4gKiBUaGUgcHVibGljIEFQSSBmb3IgcmVuZGVyaW5nIGEgaGlzdG9yeS1hd2FyZSA8YT4uXG4gKi9cblxudmFyIExpbmsgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoTGluaywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGluaygpIHtcbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIExpbmspO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwuYXBwbHkoX1JlYWN0JENvbXBvbmVudCwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmhhbmRsZUNsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25DbGljaykgX3RoaXMucHJvcHMub25DbGljayhldmVudCk7XG5cbiAgICAgIGlmICghZXZlbnQuZGVmYXVsdFByZXZlbnRlZCAmJiAvLyBvbkNsaWNrIHByZXZlbnRlZCBkZWZhdWx0XG4gICAgICBldmVudC5idXR0b24gPT09IDAgJiYgLy8gaWdub3JlIHJpZ2h0IGNsaWNrc1xuICAgICAgIV90aGlzLnByb3BzLnRhcmdldCAmJiAvLyBsZXQgYnJvd3NlciBoYW5kbGUgXCJ0YXJnZXQ9X2JsYW5rXCIgZXRjLlxuICAgICAgIWlzTW9kaWZpZWRFdmVudChldmVudCkgLy8gaWdub3JlIGNsaWNrcyB3aXRoIG1vZGlmaWVyIGtleXNcbiAgICAgICkge1xuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICB2YXIgaGlzdG9yeSA9IF90aGlzLmNvbnRleHQucm91dGVyLmhpc3Rvcnk7XG4gICAgICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgICAgIHJlcGxhY2UgPSBfdGhpcyRwcm9wcy5yZXBsYWNlLFxuICAgICAgICAgICAgICB0byA9IF90aGlzJHByb3BzLnRvO1xuXG5cbiAgICAgICAgICBpZiAocmVwbGFjZSkge1xuICAgICAgICAgICAgaGlzdG9yeS5yZXBsYWNlKHRvKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaGlzdG9yeS5wdXNoKHRvKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gIExpbmsucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgcmVwbGFjZSA9IF9wcm9wcy5yZXBsYWNlLFxuICAgICAgICB0byA9IF9wcm9wcy50byxcbiAgICAgICAgaW5uZXJSZWYgPSBfcHJvcHMuaW5uZXJSZWYsXG4gICAgICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9wcm9wcywgWydyZXBsYWNlJywgJ3RvJywgJ2lubmVyUmVmJ10pOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVudXNlZC12YXJzXG5cbiAgICAoMCwgX2ludmFyaWFudDIuZGVmYXVsdCkodGhpcy5jb250ZXh0LnJvdXRlciwgJ1lvdSBzaG91bGQgbm90IHVzZSA8TGluaz4gb3V0c2lkZSBhIDxSb3V0ZXI+Jyk7XG5cbiAgICB2YXIgaHJlZiA9IHRoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeS5jcmVhdGVIcmVmKHR5cGVvZiB0byA9PT0gJ3N0cmluZycgPyB7IHBhdGhuYW1lOiB0byB9IDogdG8pO1xuXG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdhJywgX2V4dGVuZHMoe30sIHByb3BzLCB7IG9uQ2xpY2s6IHRoaXMuaGFuZGxlQ2xpY2ssIGhyZWY6IGhyZWYsIHJlZjogaW5uZXJSZWYgfSkpO1xuICB9O1xuXG4gIHJldHVybiBMaW5rO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGluay5wcm9wVHlwZXMgPSB7XG4gIG9uQ2xpY2s6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcbiAgdGFyZ2V0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcbiAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICB0bzogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdF0pLmlzUmVxdWlyZWQsXG4gIGlubmVyUmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuY10pXG59O1xuTGluay5kZWZhdWx0UHJvcHMgPSB7XG4gIHJlcGxhY2U6IGZhbHNlXG59O1xuTGluay5jb250ZXh0VHlwZXMgPSB7XG4gIHJvdXRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgaGlzdG9yeTogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgICBwdXNoOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgY3JlYXRlSHJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWRcbiAgICB9KS5pc1JlcXVpcmVkXG4gIH0pLmlzUmVxdWlyZWRcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjYgMjcgMjggMzAgMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNTcgNTggNTkgNjEiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX2ludmFyaWFudCA9IHJlcXVpcmUoJ2ludmFyaWFudCcpO1xuXG52YXIgX2ludmFyaWFudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbnZhcmlhbnQpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX21hdGNoUGF0aCA9IHJlcXVpcmUoJy4vbWF0Y2hQYXRoJyk7XG5cbnZhciBfbWF0Y2hQYXRoMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX21hdGNoUGF0aCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIGlzRW1wdHlDaGlsZHJlbiA9IGZ1bmN0aW9uIGlzRW1wdHlDaGlsZHJlbihjaGlsZHJlbikge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLmNvdW50KGNoaWxkcmVuKSA9PT0gMDtcbn07XG5cbi8qKlxuICogVGhlIHB1YmxpYyBBUEkgZm9yIG1hdGNoaW5nIGEgc2luZ2xlIHBhdGggYW5kIHJlbmRlcmluZy5cbiAqL1xuXG52YXIgUm91dGUgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoUm91dGUsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFJvdXRlKCkge1xuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgUm91dGUpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwuYXBwbHkoX1JlYWN0JENvbXBvbmVudCwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0ge1xuICAgICAgbWF0Y2g6IF90aGlzLmNvbXB1dGVNYXRjaChfdGhpcy5wcm9wcywgX3RoaXMuY29udGV4dC5yb3V0ZXIpXG4gICAgfSwgX3RlbXApLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihfdGhpcywgX3JldCk7XG4gIH1cblxuICBSb3V0ZS5wcm90b3R5cGUuZ2V0Q2hpbGRDb250ZXh0ID0gZnVuY3Rpb24gZ2V0Q2hpbGRDb250ZXh0KCkge1xuICAgIHJldHVybiB7XG4gICAgICByb3V0ZXI6IF9leHRlbmRzKHt9LCB0aGlzLmNvbnRleHQucm91dGVyLCB7XG4gICAgICAgIHJvdXRlOiB7XG4gICAgICAgICAgbG9jYXRpb246IHRoaXMucHJvcHMubG9jYXRpb24gfHwgdGhpcy5jb250ZXh0LnJvdXRlci5yb3V0ZS5sb2NhdGlvbixcbiAgICAgICAgICBtYXRjaDogdGhpcy5zdGF0ZS5tYXRjaFxuICAgICAgICB9XG4gICAgICB9KVxuICAgIH07XG4gIH07XG5cbiAgUm91dGUucHJvdG90eXBlLmNvbXB1dGVNYXRjaCA9IGZ1bmN0aW9uIGNvbXB1dGVNYXRjaChfcmVmLCByb3V0ZXIpIHtcbiAgICB2YXIgY29tcHV0ZWRNYXRjaCA9IF9yZWYuY29tcHV0ZWRNYXRjaCxcbiAgICAgICAgbG9jYXRpb24gPSBfcmVmLmxvY2F0aW9uLFxuICAgICAgICBwYXRoID0gX3JlZi5wYXRoLFxuICAgICAgICBzdHJpY3QgPSBfcmVmLnN0cmljdCxcbiAgICAgICAgZXhhY3QgPSBfcmVmLmV4YWN0LFxuICAgICAgICBzZW5zaXRpdmUgPSBfcmVmLnNlbnNpdGl2ZTtcblxuICAgIGlmIChjb21wdXRlZE1hdGNoKSByZXR1cm4gY29tcHV0ZWRNYXRjaDsgLy8gPFN3aXRjaD4gYWxyZWFkeSBjb21wdXRlZCB0aGUgbWF0Y2ggZm9yIHVzXG5cbiAgICAoMCwgX2ludmFyaWFudDIuZGVmYXVsdCkocm91dGVyLCAnWW91IHNob3VsZCBub3QgdXNlIDxSb3V0ZT4gb3Igd2l0aFJvdXRlcigpIG91dHNpZGUgYSA8Um91dGVyPicpO1xuXG4gICAgdmFyIHJvdXRlID0gcm91dGVyLnJvdXRlO1xuXG4gICAgdmFyIHBhdGhuYW1lID0gKGxvY2F0aW9uIHx8IHJvdXRlLmxvY2F0aW9uKS5wYXRobmFtZTtcblxuICAgIHJldHVybiBwYXRoID8gKDAsIF9tYXRjaFBhdGgyLmRlZmF1bHQpKHBhdGhuYW1lLCB7IHBhdGg6IHBhdGgsIHN0cmljdDogc3RyaWN0LCBleGFjdDogZXhhY3QsIHNlbnNpdGl2ZTogc2Vuc2l0aXZlIH0pIDogcm91dGUubWF0Y2g7XG4gIH07XG5cbiAgUm91dGUucHJvdG90eXBlLmNvbXBvbmVudFdpbGxNb3VudCA9IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxNb3VudCgpIHtcbiAgICAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKCEodGhpcy5wcm9wcy5jb21wb25lbnQgJiYgdGhpcy5wcm9wcy5yZW5kZXIpLCAnWW91IHNob3VsZCBub3QgdXNlIDxSb3V0ZSBjb21wb25lbnQ+IGFuZCA8Um91dGUgcmVuZGVyPiBpbiB0aGUgc2FtZSByb3V0ZTsgPFJvdXRlIHJlbmRlcj4gd2lsbCBiZSBpZ25vcmVkJyk7XG5cbiAgICAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKCEodGhpcy5wcm9wcy5jb21wb25lbnQgJiYgdGhpcy5wcm9wcy5jaGlsZHJlbiAmJiAhaXNFbXB0eUNoaWxkcmVuKHRoaXMucHJvcHMuY2hpbGRyZW4pKSwgJ1lvdSBzaG91bGQgbm90IHVzZSA8Um91dGUgY29tcG9uZW50PiBhbmQgPFJvdXRlIGNoaWxkcmVuPiBpbiB0aGUgc2FtZSByb3V0ZTsgPFJvdXRlIGNoaWxkcmVuPiB3aWxsIGJlIGlnbm9yZWQnKTtcblxuICAgICgwLCBfd2FybmluZzIuZGVmYXVsdCkoISh0aGlzLnByb3BzLnJlbmRlciAmJiB0aGlzLnByb3BzLmNoaWxkcmVuICYmICFpc0VtcHR5Q2hpbGRyZW4odGhpcy5wcm9wcy5jaGlsZHJlbikpLCAnWW91IHNob3VsZCBub3QgdXNlIDxSb3V0ZSByZW5kZXI+IGFuZCA8Um91dGUgY2hpbGRyZW4+IGluIHRoZSBzYW1lIHJvdXRlOyA8Um91dGUgY2hpbGRyZW4+IHdpbGwgYmUgaWdub3JlZCcpO1xuICB9O1xuXG4gIFJvdXRlLnByb3RvdHlwZS5jb21wb25lbnRXaWxsUmVjZWl2ZVByb3BzID0gZnVuY3Rpb24gY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyhuZXh0UHJvcHMsIG5leHRDb250ZXh0KSB7XG4gICAgKDAsIF93YXJuaW5nMi5kZWZhdWx0KSghKG5leHRQcm9wcy5sb2NhdGlvbiAmJiAhdGhpcy5wcm9wcy5sb2NhdGlvbiksICc8Um91dGU+IGVsZW1lbnRzIHNob3VsZCBub3QgY2hhbmdlIGZyb20gdW5jb250cm9sbGVkIHRvIGNvbnRyb2xsZWQgKG9yIHZpY2UgdmVyc2EpLiBZb3UgaW5pdGlhbGx5IHVzZWQgbm8gXCJsb2NhdGlvblwiIHByb3AgYW5kIHRoZW4gcHJvdmlkZWQgb25lIG9uIGEgc3Vic2VxdWVudCByZW5kZXIuJyk7XG5cbiAgICAoMCwgX3dhcm5pbmcyLmRlZmF1bHQpKCEoIW5leHRQcm9wcy5sb2NhdGlvbiAmJiB0aGlzLnByb3BzLmxvY2F0aW9uKSwgJzxSb3V0ZT4gZWxlbWVudHMgc2hvdWxkIG5vdCBjaGFuZ2UgZnJvbSBjb250cm9sbGVkIHRvIHVuY29udHJvbGxlZCAob3IgdmljZSB2ZXJzYSkuIFlvdSBwcm92aWRlZCBhIFwibG9jYXRpb25cIiBwcm9wIGluaXRpYWxseSBidXQgb21pdHRlZCBpdCBvbiBhIHN1YnNlcXVlbnQgcmVuZGVyLicpO1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICBtYXRjaDogdGhpcy5jb21wdXRlTWF0Y2gobmV4dFByb3BzLCBuZXh0Q29udGV4dC5yb3V0ZXIpXG4gICAgfSk7XG4gIH07XG5cbiAgUm91dGUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgbWF0Y2ggPSB0aGlzLnN0YXRlLm1hdGNoO1xuICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgY29tcG9uZW50ID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgcmVuZGVyID0gX3Byb3BzLnJlbmRlcjtcbiAgICB2YXIgX2NvbnRleHQkcm91dGVyID0gdGhpcy5jb250ZXh0LnJvdXRlcixcbiAgICAgICAgaGlzdG9yeSA9IF9jb250ZXh0JHJvdXRlci5oaXN0b3J5LFxuICAgICAgICByb3V0ZSA9IF9jb250ZXh0JHJvdXRlci5yb3V0ZSxcbiAgICAgICAgc3RhdGljQ29udGV4dCA9IF9jb250ZXh0JHJvdXRlci5zdGF0aWNDb250ZXh0O1xuXG4gICAgdmFyIGxvY2F0aW9uID0gdGhpcy5wcm9wcy5sb2NhdGlvbiB8fCByb3V0ZS5sb2NhdGlvbjtcbiAgICB2YXIgcHJvcHMgPSB7IG1hdGNoOiBtYXRjaCwgbG9jYXRpb246IGxvY2F0aW9uLCBoaXN0b3J5OiBoaXN0b3J5LCBzdGF0aWNDb250ZXh0OiBzdGF0aWNDb250ZXh0IH07XG5cbiAgICByZXR1cm4gY29tcG9uZW50ID8gLy8gY29tcG9uZW50IHByb3AgZ2V0cyBmaXJzdCBwcmlvcml0eSwgb25seSBjYWxsZWQgaWYgdGhlcmUncyBhIG1hdGNoXG4gICAgbWF0Y2ggPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChjb21wb25lbnQsIHByb3BzKSA6IG51bGwgOiByZW5kZXIgPyAvLyByZW5kZXIgcHJvcCBpcyBuZXh0LCBvbmx5IGNhbGxlZCBpZiB0aGVyZSdzIGEgbWF0Y2hcbiAgICBtYXRjaCA/IHJlbmRlcihwcm9wcykgOiBudWxsIDogY2hpbGRyZW4gPyAvLyBjaGlsZHJlbiBjb21lIGxhc3QsIGFsd2F5cyBjYWxsZWRcbiAgICB0eXBlb2YgY2hpbGRyZW4gPT09ICdmdW5jdGlvbicgPyBjaGlsZHJlbihwcm9wcykgOiAhaXNFbXB0eUNoaWxkcmVuKGNoaWxkcmVuKSA/IF9yZWFjdDIuZGVmYXVsdC5DaGlsZHJlbi5vbmx5KGNoaWxkcmVuKSA6IG51bGwgOiBudWxsO1xuICB9O1xuXG4gIHJldHVybiBSb3V0ZTtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblJvdXRlLnByb3BUeXBlcyA9IHtcbiAgY29tcHV0ZWRNYXRjaDogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QsIC8vIHByaXZhdGUsIGZyb20gPFN3aXRjaD5cbiAgcGF0aDogX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsXG4gIGV4YWN0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmJvb2wsXG4gIHN0cmljdDogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICBzZW5zaXRpdmU6IF9wcm9wVHlwZXMyLmRlZmF1bHQuYm9vbCxcbiAgY29tcG9uZW50OiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMsXG4gIHJlbmRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLFxuICBjaGlsZHJlbjogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYywgX3Byb3BUeXBlczIuZGVmYXVsdC5ub2RlXSksXG4gIGxvY2F0aW9uOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdFxufTtcblJvdXRlLmNvbnRleHRUeXBlcyA9IHtcbiAgcm91dGVyOiBfcHJvcFR5cGVzMi5kZWZhdWx0LnNoYXBlKHtcbiAgICBoaXN0b3J5OiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIHJvdXRlOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIHN0YXRpY0NvbnRleHQ6IF9wcm9wVHlwZXMyLmRlZmF1bHQub2JqZWN0XG4gIH0pXG59O1xuUm91dGUuY2hpbGRDb250ZXh0VHlwZXMgPSB7XG4gIHJvdXRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5vYmplY3QuaXNSZXF1aXJlZFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9IFJvdXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci9Sb3V0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL1JvdXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMjcgMjggMzUgMzcgNDAgNDEgNDIiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfcGF0aFRvUmVnZXhwID0gcmVxdWlyZSgncGF0aC10by1yZWdleHAnKTtcblxudmFyIF9wYXRoVG9SZWdleHAyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcGF0aFRvUmVnZXhwKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHBhdHRlcm5DYWNoZSA9IHt9O1xudmFyIGNhY2hlTGltaXQgPSAxMDAwMDtcbnZhciBjYWNoZUNvdW50ID0gMDtcblxudmFyIGNvbXBpbGVQYXRoID0gZnVuY3Rpb24gY29tcGlsZVBhdGgocGF0dGVybiwgb3B0aW9ucykge1xuICB2YXIgY2FjaGVLZXkgPSAnJyArIG9wdGlvbnMuZW5kICsgb3B0aW9ucy5zdHJpY3QgKyBvcHRpb25zLnNlbnNpdGl2ZTtcbiAgdmFyIGNhY2hlID0gcGF0dGVybkNhY2hlW2NhY2hlS2V5XSB8fCAocGF0dGVybkNhY2hlW2NhY2hlS2V5XSA9IHt9KTtcblxuICBpZiAoY2FjaGVbcGF0dGVybl0pIHJldHVybiBjYWNoZVtwYXR0ZXJuXTtcblxuICB2YXIga2V5cyA9IFtdO1xuICB2YXIgcmUgPSAoMCwgX3BhdGhUb1JlZ2V4cDIuZGVmYXVsdCkocGF0dGVybiwga2V5cywgb3B0aW9ucyk7XG4gIHZhciBjb21waWxlZFBhdHRlcm4gPSB7IHJlOiByZSwga2V5czoga2V5cyB9O1xuXG4gIGlmIChjYWNoZUNvdW50IDwgY2FjaGVMaW1pdCkge1xuICAgIGNhY2hlW3BhdHRlcm5dID0gY29tcGlsZWRQYXR0ZXJuO1xuICAgIGNhY2hlQ291bnQrKztcbiAgfVxuXG4gIHJldHVybiBjb21waWxlZFBhdHRlcm47XG59O1xuXG4vKipcbiAqIFB1YmxpYyBBUEkgZm9yIG1hdGNoaW5nIGEgVVJMIHBhdGhuYW1lIHRvIGEgcGF0aCBwYXR0ZXJuLlxuICovXG52YXIgbWF0Y2hQYXRoID0gZnVuY3Rpb24gbWF0Y2hQYXRoKHBhdGhuYW1lKSB7XG4gIHZhciBvcHRpb25zID0gYXJndW1lbnRzLmxlbmd0aCA+IDEgJiYgYXJndW1lbnRzWzFdICE9PSB1bmRlZmluZWQgPyBhcmd1bWVudHNbMV0gOiB7fTtcblxuICBpZiAodHlwZW9mIG9wdGlvbnMgPT09ICdzdHJpbmcnKSBvcHRpb25zID0geyBwYXRoOiBvcHRpb25zIH07XG5cbiAgdmFyIF9vcHRpb25zID0gb3B0aW9ucyxcbiAgICAgIF9vcHRpb25zJHBhdGggPSBfb3B0aW9ucy5wYXRoLFxuICAgICAgcGF0aCA9IF9vcHRpb25zJHBhdGggPT09IHVuZGVmaW5lZCA/ICcvJyA6IF9vcHRpb25zJHBhdGgsXG4gICAgICBfb3B0aW9ucyRleGFjdCA9IF9vcHRpb25zLmV4YWN0LFxuICAgICAgZXhhY3QgPSBfb3B0aW9ucyRleGFjdCA9PT0gdW5kZWZpbmVkID8gZmFsc2UgOiBfb3B0aW9ucyRleGFjdCxcbiAgICAgIF9vcHRpb25zJHN0cmljdCA9IF9vcHRpb25zLnN0cmljdCxcbiAgICAgIHN0cmljdCA9IF9vcHRpb25zJHN0cmljdCA9PT0gdW5kZWZpbmVkID8gZmFsc2UgOiBfb3B0aW9ucyRzdHJpY3QsXG4gICAgICBfb3B0aW9ucyRzZW5zaXRpdmUgPSBfb3B0aW9ucy5zZW5zaXRpdmUsXG4gICAgICBzZW5zaXRpdmUgPSBfb3B0aW9ucyRzZW5zaXRpdmUgPT09IHVuZGVmaW5lZCA/IGZhbHNlIDogX29wdGlvbnMkc2Vuc2l0aXZlO1xuXG4gIHZhciBfY29tcGlsZVBhdGggPSBjb21waWxlUGF0aChwYXRoLCB7IGVuZDogZXhhY3QsIHN0cmljdDogc3RyaWN0LCBzZW5zaXRpdmU6IHNlbnNpdGl2ZSB9KSxcbiAgICAgIHJlID0gX2NvbXBpbGVQYXRoLnJlLFxuICAgICAga2V5cyA9IF9jb21waWxlUGF0aC5rZXlzO1xuXG4gIHZhciBtYXRjaCA9IHJlLmV4ZWMocGF0aG5hbWUpO1xuXG4gIGlmICghbWF0Y2gpIHJldHVybiBudWxsO1xuXG4gIHZhciB1cmwgPSBtYXRjaFswXSxcbiAgICAgIHZhbHVlcyA9IG1hdGNoLnNsaWNlKDEpO1xuXG4gIHZhciBpc0V4YWN0ID0gcGF0aG5hbWUgPT09IHVybDtcblxuICBpZiAoZXhhY3QgJiYgIWlzRXhhY3QpIHJldHVybiBudWxsO1xuXG4gIHJldHVybiB7XG4gICAgcGF0aDogcGF0aCwgLy8gdGhlIHBhdGggcGF0dGVybiB1c2VkIHRvIG1hdGNoXG4gICAgdXJsOiBwYXRoID09PSAnLycgJiYgdXJsID09PSAnJyA/ICcvJyA6IHVybCwgLy8gdGhlIG1hdGNoZWQgcG9ydGlvbiBvZiB0aGUgVVJMXG4gICAgaXNFeGFjdDogaXNFeGFjdCwgLy8gd2hldGhlciBvciBub3Qgd2UgbWF0Y2hlZCBleGFjdGx5XG4gICAgcGFyYW1zOiBrZXlzLnJlZHVjZShmdW5jdGlvbiAobWVtbywga2V5LCBpbmRleCkge1xuICAgICAgbWVtb1trZXkubmFtZV0gPSB2YWx1ZXNbaW5kZXhdO1xuICAgICAgcmV0dXJuIG1lbW87XG4gICAgfSwge30pXG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBtYXRjaFBhdGg7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL21hdGNoUGF0aC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL21hdGNoUGF0aC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI3IDI4IDM1IDM3IDQwIDQxIDQyIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHJvcFR5cGVzID0gcmVxdWlyZSgncHJvcC10eXBlcycpO1xuXG52YXIgX3Byb3BUeXBlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wcm9wVHlwZXMpO1xuXG52YXIgX2hvaXN0Tm9uUmVhY3RTdGF0aWNzID0gcmVxdWlyZSgnaG9pc3Qtbm9uLXJlYWN0LXN0YXRpY3MnKTtcblxudmFyIF9ob2lzdE5vblJlYWN0U3RhdGljczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9ob2lzdE5vblJlYWN0U3RhdGljcyk7XG5cbnZhciBfUm91dGUgPSByZXF1aXJlKCcuL1JvdXRlJyk7XG5cbnZhciBfUm91dGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfUm91dGUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMob2JqLCBrZXlzKSB7IHZhciB0YXJnZXQgPSB7fTsgZm9yICh2YXIgaSBpbiBvYmopIHsgaWYgKGtleXMuaW5kZXhPZihpKSA+PSAwKSBjb250aW51ZTsgaWYgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBpKSkgY29udGludWU7IHRhcmdldFtpXSA9IG9ialtpXTsgfSByZXR1cm4gdGFyZ2V0OyB9XG5cbi8qKlxuICogQSBwdWJsaWMgaGlnaGVyLW9yZGVyIGNvbXBvbmVudCB0byBhY2Nlc3MgdGhlIGltcGVyYXRpdmUgQVBJXG4gKi9cbnZhciB3aXRoUm91dGVyID0gZnVuY3Rpb24gd2l0aFJvdXRlcihDb21wb25lbnQpIHtcbiAgdmFyIEMgPSBmdW5jdGlvbiBDKHByb3BzKSB7XG4gICAgdmFyIHdyYXBwZWRDb21wb25lbnRSZWYgPSBwcm9wcy53cmFwcGVkQ29tcG9uZW50UmVmLFxuICAgICAgICByZW1haW5pbmdQcm9wcyA9IF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhwcm9wcywgWyd3cmFwcGVkQ29tcG9uZW50UmVmJ10pO1xuXG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9Sb3V0ZTIuZGVmYXVsdCwgeyByZW5kZXI6IGZ1bmN0aW9uIHJlbmRlcihyb3V0ZUNvbXBvbmVudFByb3BzKSB7XG4gICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChDb21wb25lbnQsIF9leHRlbmRzKHt9LCByZW1haW5pbmdQcm9wcywgcm91dGVDb21wb25lbnRQcm9wcywgeyByZWY6IHdyYXBwZWRDb21wb25lbnRSZWYgfSkpO1xuICAgICAgfSB9KTtcbiAgfTtcblxuICBDLmRpc3BsYXlOYW1lID0gJ3dpdGhSb3V0ZXIoJyArIChDb21wb25lbnQuZGlzcGxheU5hbWUgfHwgQ29tcG9uZW50Lm5hbWUpICsgJyknO1xuICBDLldyYXBwZWRDb21wb25lbnQgPSBDb21wb25lbnQ7XG4gIEMucHJvcFR5cGVzID0ge1xuICAgIHdyYXBwZWRDb21wb25lbnRSZWY6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuY1xuICB9O1xuXG4gIHJldHVybiAoMCwgX2hvaXN0Tm9uUmVhY3RTdGF0aWNzMi5kZWZhdWx0KShDLCBDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd2l0aFJvdXRlcjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXIvd2l0aFJvdXRlci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyL3dpdGhSb3V0ZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNyAyOCAzNSAzNyA0MCA0MSA0MiIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBDT1VSU0VfREVMRVRFIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL2NvdXJzZXMnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uIGRlbGV0ZUNvdXJzZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYXV0aG9ySWQgLVxuICogQHJldHVybiB7T2JqZWN0fSAtXG4gKi9cbmV4cG9ydCBkZWZhdWx0IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7IHR5cGU6IENPVVJTRV9ERUxFVEUsIHBheWxvYWQgfSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy9kZWxldGUuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQge1xuICBFTEVNRU5UX0dFVF9TVEFSVCxcbiAgRUxFTUVOVF9HRVRfRVJST1IsXG4gIEVMRU1FTlRfR0VUX1NVQ0NFU1MsXG59IGZyb20gJy4uLy4uL2NvbnN0YW50cy9lbGVtZW50cyc7XG5cbmltcG9ydCBhcGlHZXRCeUlkIGZyb20gJy4uLy4uL2FwaS9lbGVtZW50cy9nZXRCeUlkJztcblxuLyoqXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEVsZW1lbnRTdGFydCA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UX0dFVF9TVEFSVCwgcGF5bG9hZCB9KTtcblxuLyoqXG4gKlxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnN0YXR1c0NvZGVcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC51c2VybmFtZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZWxlbWVudFR5cGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNpcmNsZVR5cGUgLSBvbmx5IGlmIGVsZW1lbnRUeXBlIGlzICdjaXJjbGUnXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5uYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmF2YXRhciAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jb3ZlciAtXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGdldEVsZW1lbnRTdWNjZXNzID0gKHBheWxvYWQpID0+ICh7IHR5cGU6IEVMRU1FTlRfR0VUX1NVQ0NFU1MsIHBheWxvYWQgfSk7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuZXJyb3JcbiAqIEByZXR1cm4ge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0RWxlbWVudEVycm9yID0gKHBheWxvYWQpID0+ICh7IHR5cGU6IEVMRU1FTlRfR0VUX0VSUk9SLCBwYXlsb2FkIH0pO1xuXG4vKipcbiAqXG4gKiBAZnVuY3Rpb24gZ2V0XG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW5cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmlkXG4gKi9cbmNvbnN0IGdldCA9ICh7IHRva2VuLCBpZCB9KSA9PiAoZGlzcGF0Y2gpID0+IHtcbiAgZGlzcGF0Y2goZ2V0RWxlbWVudFN0YXJ0KHsgaWQgfSkpO1xuXG4gIGFwaUdldEJ5SWQoeyB0b2tlbiwgaWQgfSlcbiAgICAudGhlbigoeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9KSA9PiB7XG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgZGlzcGF0Y2goZ2V0RWxlbWVudEVycm9yKHsgaWQsIHN0YXR1c0NvZGUsIGVycm9yIH0pKTtcblxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGRpc3BhdGNoKGdldEVsZW1lbnRTdWNjZXNzKHsgaWQsIHN0YXR1c0NvZGUsIC4uLnBheWxvYWQgfSkpO1xuICAgIH0pXG4gICAgLmNhdGNoKCh7IHN0YXR1c0NvZGUsIGVycm9yIH0pID0+IHtcbiAgICAgIGRpc3BhdGNoKGdldEVsZW1lbnRFcnJvcih7IGlkLCBzdGF0dXNDb2RlLCBlcnJvciB9KSk7XG4gICAgfSk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBnZXQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZC5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBQT1NUX0RFTEVURSB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9wb3N0cyc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgcG9zdElkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uIGRlbGV0ZVBvc3RcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWRcbiAqIEByZXR1cm5zIHtPYmplY3R9XG4gKi9cbmNvbnN0IGRlbGV0ZVBvc3QgPSAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoe1xuICB0eXBlOiBQT1NUX0RFTEVURSxcbiAgcGF5bG9hZCxcbn0pO1xuXG5leHBvcnQgZGVmYXVsdCBkZWxldGVQb3N0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL3Bvc3RzL2RlbGV0ZS5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBQT1NUX0NSRUFURSB9IGZyb20gJy4uLy4uLy4uL2NvbnN0YW50cy9wb3N0cyc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgcG9zdElkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG4gIHRpbWVzdGFtcDogc3RyaW5nLFxuXG4gIHN0YXR1c0NvZGU6IG51bWJlcixcbiAgbWVzc2FnZTogc3RyaW5nLFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICogVGhpcyBhY3Rpb24gd2lsbCBjcmVhdGUgcG9zdCBmb3IgdXNlciBpbiBzdG9yZS5cbiAqIEBmdW5jdGlvbiBjcmVhdGVcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmF1dGhvcklkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50aXRsZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGltZXN0YW1wXG4gKlxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubWVzc2FnZVxuICpcbiAqIEByZXR1cm5zIHtPYmplY3R9XG4gKi9cbmNvbnN0IGNyZWF0ZSA9IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7IHR5cGU6IFBPU1RfQ1JFQVRFLCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvdXNlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7fFxuICB0b2tlbjogc3RyaW5nLFxuICB1c2VySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbnx9O1xuXG4vKipcbiAqIGRlbGV0ZSBjb3Vyc2VcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGRlbGV0ZUNvdXJzZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWRcbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZUlkXG4gKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBkZWxldGVDb3Vyc2UoeyB0b2tlbiwgdXNlcklkLCBjb3Vyc2VJZCB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzLyR7Y291cnNlSWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnREVMRVRFJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBkZWxldGVDb3Vyc2U7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL2RlbGV0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqXG4gKiBAZnVuY3Rpb24gZ2V0QnlJZFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZFxuICovXG5hc3luYyBmdW5jdGlvbiBnZXRCeUlkKHsgdG9rZW4sIGlkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvZWxlbWVudHMvJHtpZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdldEJ5SWQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9lbGVtZW50cy9nZXRCeUlkLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmlsZSBjcmVhdGUgcG9zdCBmb3IgdXNlclxuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gd2lsbCBjcmVhdGUgcG9zdCBieSB1c2VyLlxuICogQGFzeW5jXG4gKiBAZnVuY3Rpb24gY3JlYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW5cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZFxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQucG9zdFR5cGVcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRpdGxlXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqIGNyZWF0ZSh7XG4gKiAgIHVzZXJJZDogMTIzMTQxMjMxMzQsXG4gKiAgIHRva2VuOiAnaGVhZC5ib2R5LnRhaWwnLFxuICogICBwb3N0VHlwZTogJ2FydGljbGUnLFxuICogICB0aXRsZTogJ1RoaXMgaXMgYXdlc29tZSBhcnRpY2xlJyxcbiAqIH0pXG4gKiAgIC50aGVuKHJlcyA9PiBjb25zb2xlLmxvZyhyZXMpKVxuICogICAuY2F0Y2goZXJyID0+IGNvbnNvbGUuZXJyb3IoZXJyKSk7XG4gKi9cbmFzeW5jIGZ1bmN0aW9uIGNyZWF0ZSh7IHRva2VuLCB1c2VySWQsIHBvc3RUeXBlLCB0aXRsZSB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9wb3N0c2A7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoeyBwb3N0VHlwZSwgdGl0bGUgfSksXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvcG9zdHMvdXNlcnMvY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmlsZSBkZWxldGUgcG9zdCBvZiB1c2VyLlxuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIGRlbGV0ZSBwb3N0IG9mIHVzZXJcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGRlbGV0ZVBvc3RcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0SWQgLVxuICogQHJldHVybiB7UHJvbWlzZX0gLVxuICpcbiAqIEBleGFtcGxlXG4gKi9cbmFzeW5jIGZ1bmN0aW9uIGRlbGV0ZVBvc3QoeyB1c2VySWQsIHRva2VuLCBwb3N0SWQgfSkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS91c2Vycy8ke3VzZXJJZH0vcG9zdHMvJHtwb3N0SWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnREVMRVRFJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBkZWxldGVQb3N0O1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvcG9zdHMvdXNlcnMvZGVsZXRlLmpzIiwiLyoqXG4gKiBUaGlzIGlucHV0IGNvbXBvbmVudCBpcyBtYWlubHkgZGV2ZWxvcGVkIGZvciBjcmVhdGUgcG9zdCwgY3JlYXRlXG4gKiBjb3Vyc2UgY29tcG9uZW50LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBkaXNwbGF5OiAnZmxleCcsXG4gIH0sXG4gIGlucHV0OiB7XG4gICAgZmxleEdyb3c6ICcxJyxcbiAgICBib3JkZXI6ICcycHggc29saWQgI2RhZGFkYScsXG4gICAgYm9yZGVyUmFkaXVzOiAnN3B4JyxcbiAgICBwYWRkaW5nOiAnOHB4JyxcbiAgICAnJjpmb2N1cyc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJyxcbiAgICAgIGJvcmRlckNvbG9yOiAnIzllY2FlZCcsXG4gICAgICBib3hTaGFkb3c6ICcwIDAgMTBweCAjOWVjYWVkJyxcbiAgICB9LFxuICB9LFxufTtcblxuY29uc3QgSW5wdXQgPSAoeyBjbGFzc2VzLCBvbkNoYW5nZSwgdmFsdWUsIHBsYWNlaG9sZGVyLCBkaXNhYmxlZCwgdHlwZSB9KSA9PiAoXG4gIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgIDxpbnB1dFxuICAgICAgdHlwZT17dHlwZSB8fCAndGV4dCd9XG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXIgfHwgJ2lucHV0J31cbiAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgIG9uQ2hhbmdlPXtvbkNoYW5nZX1cbiAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5pbnB1dH1cbiAgICAgIGRpc2FibGVkPXshIWRpc2FibGVkfVxuICAgIC8+XG4gIDwvZGl2PlxuKTtcblxuSW5wdXQucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLm9uZU9mVHlwZShbUHJvcFR5cGVzLnN0cmluZywgUHJvcFR5cGVzLm51bWJlcl0pLmlzUmVxdWlyZWQsXG4gIHBsYWNlaG9sZGVyOiBQcm9wVHlwZXMuc3RyaW5nLFxuICB0eXBlOiBQcm9wVHlwZXMuc3RyaW5nLFxuICBkaXNhYmxlZDogUHJvcFR5cGVzLmJvb2wsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoSW5wdXQpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb21wb25lbnRzL0lucHV0LmpzIiwiLyoqXG4gKiBjb3Vyc2UgcGFnZSBjb21wb25lbnRcbiAqIFRoaXMgY29tcG9uZW50IHdpbGwgZGlzcGxheSBvbmx1IG9uZSBjb3Vyc2UgaW5mb3JtYXRpb25cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgd2l0aFJvdXRlciBmcm9tICdyZWFjdC1yb3V0ZXIvd2l0aFJvdXRlcic7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgQ2FyZCwge1xuICBDYXJkTWVkaWEsXG4gIENhcmRIZWFkZXIsXG4gIENhcmRDb250ZW50LFxuICBDYXJkQWN0aW9ucyxcbn0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgRGlhbG9nLCB7IERpYWxvZ0FjdGlvbnMsIERpYWxvZ1RpdGxlIH0gZnJvbSAnbWF0ZXJpYWwtdWkvRGlhbG9nJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcbmltcG9ydCBQYXBlciBmcm9tICdtYXRlcmlhbC11aS9QYXBlcic7XG5cbmltcG9ydCBNb3JlVmVydEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvTW9yZVZlcnQnO1xuaW1wb3J0IEZhdm9yaXRlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9GYXZvcml0ZSc7XG5pbXBvcnQgVHVybmVkSW5JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1R1cm5lZEluJztcbmltcG9ydCBEZWxldGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0RlbGV0ZSc7XG5pbXBvcnQgU2hhcmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NoYXJlJztcblxuaW1wb3J0IGFwaUNvdXJzZURlbGV0ZSBmcm9tICcuLi8uLi9hcGkvY291cnNlcy91c2Vycy9kZWxldGUnO1xuaW1wb3J0IGFjdGlvbkdldEVsZW1lbnRCeUlkIGZyb20gJy4uLy4uL2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZCc7XG5pbXBvcnQgYWN0aW9uQ291cnNlRGVsZXRlIGZyb20gJy4uLy4uL2FjdGlvbnMvY291cnNlcy9kZWxldGUnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBncmlkQ29udGFpbmVyOiB7fSxcbiAgbmFtZToge1xuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIHVzZXJuYW1lOiB7XG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgcG9zdDoge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGNhcmRIZWFkZXI6IHtcbiAgICAvLyB0ZXh0QWxpZ246ICdjZW50ZXInXG4gIH0sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBjYXJkTWVkaWE6IHt9LFxuICBtZWRpYToge1xuICAgIGhlaWdodDogMTk0LFxuICB9LFxuICBjYXJkTWVkaWFJbWFnZToge1xuICAgIHdpZHRoOiAnMTAwJScsXG4gIH0sXG4gIG1hcmdpbjoge1xuICAgIGJvcmRlcjogJzAuNXB4IHNvbGlkIHJnYmEoMjA5LDIwOSwyMDksMSknLFxuICAgIG1hcmdpbkJvdHRvbTogJy0xMHB4JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuY2xhc3MgQ291cnNlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGhvdmVyOiBmYWxzZSxcbiAgICAgIGRlbGV0ZURpYWxvZzogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIGNvbXBvbmVudFdpbGxNb3VudCgpIHtcbiAgICBjb25zdCB7IGVsZW1lbnRzLCBjb3Vyc2VzLCBjb3Vyc2VJZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHRva2VuLCBpZDogdXNlcklkIH0gPSBlbGVtZW50cy51c2VyO1xuXG4gICAgY29uc3QgeyBhdXRob3JJZCB9ID0gY291cnNlc1tjb3Vyc2VJZF07XG4gICAgY29uc3QgYXV0aG9yID0gZWxlbWVudHNbYXV0aG9ySWRdO1xuXG4gICAgLy8gaWYgYXV0aG9yIGlzIG5vdCBmb3VuZFxuICAgIGlmICh1c2VySWQgIT09IGF1dGhvcklkICYmICFhdXRob3IpIHtcbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uR2V0RWxlbWVudEJ5SWQoeyBpZDogYXV0aG9ySWQsIHRva2VuIH0pO1xuICAgIH1cbiAgfVxuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcblxuICBvbkNsaWNrQ29udGVudCA9ICgpID0+IHtcbiAgICBjb25zdCB7IGNvdXJzZUlkIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goYC9jb3Vyc2VzLyR7Y291cnNlSWR9YCk7XG4gIH07XG5cbiAgZGVsZXRlRGlhbG9nT3BlbiA9ICgpID0+IHtcbiAgICB0aGlzLnNldFN0YXRlKHsgZGVsZXRlRGlhbG9nOiB0cnVlIH0pO1xuICB9O1xuXG4gIGRlbGV0ZURpYWxvZ0Nsb3NlID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IGZhbHNlIH0pO1xuICB9O1xuXG4gIGRlbGV0ZUNvdXJzZUNsaWNrID0gYXN5bmMgKCkgPT4ge1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBjb3Vyc2VJZCB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IgfSA9IGF3YWl0IGFwaUNvdXJzZURlbGV0ZSh7XG4gICAgICB0b2tlbixcbiAgICAgIHVzZXJJZDogaWQsXG4gICAgICBjb3Vyc2VJZCxcbiAgICB9KTtcbiAgICBpZiAoc3RhdHVzQ29kZSAhPT0gMjAwKSB7XG4gICAgICAvLyBzaG93IHNvbWUgZXJyb3IgbWVzc2FnZS5cbiAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICB0aGlzLnByb3BzLmFjdGlvbkNvdXJzZURlbGV0ZSh7IGNvdXJzZUlkIH0pO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IGZhbHNlIH0pO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGNvdXJzZXMsIGVsZW1lbnRzLCBjb3Vyc2VJZCB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IHsgdXNlciB9ID0gZWxlbWVudHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluLCBpZDogdXNlcklkIH0gPSB1c2VyO1xuXG4gICAgY29uc3QgY291cnNlID0gY291cnNlc1tjb3Vyc2VJZF07XG5cbiAgICBpZiAodHlwZW9mIGNvdXJzZSA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiA8ZGl2IC8+O1xuICAgIH1cblxuICAgIGNvbnN0IHsgdGl0bGUsIGRlc2NyaXB0aW9uLCBhdXRob3JJZCwgY292ZXIgfSA9IGNvdXJzZTtcbiAgICBjb25zdCBhdXRob3IgPSBhdXRob3JJZCA9PT0gdXNlcklkID8gdXNlciA6IGVsZW1lbnRzW2F1dGhvcklkXTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICBvbk1vdXNlTGVhdmU9e3RoaXMub25Nb3VzZUxlYXZlfVxuICAgICAgPlxuICAgICAgICA8Q2FyZCByYWlzZWQ9e3RoaXMuc3RhdGUuaG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgPFBhcGVyIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9IGVsZXZhdGlvbj17MX0+XG4gICAgICAgICAgICAgICAgPEF2YXRhclxuICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAgIGF1dGhvci5hdmF0YXIgfHxcbiAgICAgICAgICAgICAgICAgICAgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZydcbiAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L1BhcGVyPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYWN0aW9uPXtcbiAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvbWluZyBzb29uXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICAgICAgPE1vcmVWZXJ0SWNvbiAvPlxuICAgICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfSBjbGFzc05hbWU9e2NsYXNzZXMubmFtZX0+XG4gICAgICAgICAgICAgICAge2F1dGhvci5uYW1lfVxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBzdWJoZWFkZXI9e1xuICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnVzZXJuYW1lfT5cbiAgICAgICAgICAgICAgICB7YEAke2F1dGhvci51c2VybmFtZX1gfVxuICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgLz5cbiAgICAgICAgICA8Q2FyZE1lZGlhXG4gICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMubWVkaWF9XG4gICAgICAgICAgICBpbWFnZT17XG4gICAgICAgICAgICAgIGNvdmVyIHx8XG4gICAgICAgICAgICAgICdodHRwOi8vZ2ludmEuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzA4L2dpbnZhXzIwMTYtMDgtMDJfMDItMzAtNTQuanBnJ1xuICAgICAgICAgICAgfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPENhcmRDb250ZW50IG9uQ2xpY2s9e3RoaXMub25DbGlja0NvbnRlbnR9PlxuICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cInRpdGxlXCIgZ3V0dGVyQm90dG9tPlxuICAgICAgICAgICAgICB7dGl0bGV9XG4gICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICB7dHlwZW9mIGRlc2NyaXB0aW9uID09PSAnc3RyaW5nJyA/IChcbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkxXCIgZ3V0dGVyQm90dG9tPlxuICAgICAgICAgICAgICAgIHtkZXNjcmlwdGlvbn1cbiAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkFkZCB0byBmYXZvcml0ZXNcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFkZCB0byBmYXZvcml0ZXNcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8RmF2b3JpdGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQm9va21hcmtcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvb2sgTWFya3NcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8VHVybmVkSW5JY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIHtpc1NpZ25lZEluICYmIHVzZXJJZCA9PT0gYXV0aG9yLmlkID8gKFxuICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJEZWxldGVcIiBvbkNsaWNrPXt0aGlzLmRlbGV0ZURpYWxvZ09wZW59PlxuICAgICAgICAgICAgICAgICAgPERlbGV0ZUljb24gLz5cbiAgICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICAgICAgPERpYWxvZ1xuICAgICAgICAgICAgICAgICAgb3Blbj17dGhpcy5zdGF0ZS5kZWxldGVEaWFsb2d9XG4gICAgICAgICAgICAgICAgICBvbkNsb3NlPXt0aGlzLmRlbGV0ZURpYWxvZ0Nsb3NlfVxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PXtgY291cnNlLSR7Y291cnNlSWR9LWRlbGV0ZS1kaWFsb2dgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxEaWFsb2dUaXRsZSBpZD17YGNvdXJzZS0ke2NvdXJzZUlkfS1kZWxldGUtZGlhbG9nYH0+XG4gICAgICAgICAgICAgICAgICAgIEFyZSB5b3Ugc3VyZT9cbiAgICAgICAgICAgICAgICAgIDwvRGlhbG9nVGl0bGU+XG4gICAgICAgICAgICAgICAgICA8RGlhbG9nQWN0aW9ucz5cbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBvbkNsaWNrPXt0aGlzLmRlbGV0ZURpYWxvZ0Nsb3NlfSBjb2xvcj1cInByaW1hcnlcIj5cbiAgICAgICAgICAgICAgICAgICAgICBDYW5jZWxcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLmRlbGV0ZUNvdXJzZUNsaWNrfVxuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBEZWxldGVcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICA8L0RpYWxvZ0FjdGlvbnM+XG4gICAgICAgICAgICAgICAgPC9EaWFsb2c+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJTaGFyZVwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICA8U2hhcmVJY29uIC8+XG4gICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5Db3Vyc2UucHJvcFR5cGVzID0ge1xuICBoaXN0b3J5OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGNvdXJzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgY291cnNlSWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcblxuICBhY3Rpb25HZXRFbGVtZW50QnlJZDogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgYWN0aW9uQ291cnNlRGVsZXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMsIGNvdXJzZXMgfSkgPT4gKHsgZWxlbWVudHMsIGNvdXJzZXMgfSk7XG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25HZXRFbGVtZW50QnlJZCxcbiAgICAgIGFjdGlvbkNvdXJzZURlbGV0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKHdpdGhSb3V0ZXIoQ291cnNlKSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZS9pbmRleC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuXG5pbXBvcnQgUG9zdENyZWF0ZSBmcm9tICcuLi8uLi9Qb3N0Q3JlYXRlJztcbmltcG9ydCBUaW1lbGluZSBmcm9tICcuLi8uLi9UaW1lbGluZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge30sXG59O1xuXG5jbGFzcyBUYWJQb3N0cyBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHVzZXIsIGVsZW1lbnQsIHBvc3RzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaXNTaWduZWRJbiwgaWQgfSA9IHVzZXI7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0+XG4gICAgICAgICAgPEdyaWQgaXRlbSB4cz17MTJ9IHNtPXs5fSBtZD17N30gbGc9ezZ9IHhsPXs2fT5cbiAgICAgICAgICAgIHtpc1NpZ25lZEluICYmIGlkID09PSBlbGVtZW50LmlkID8gPFBvc3RDcmVhdGUgLz4gOiBudWxsfVxuICAgICAgICAgICAgPFRpbWVsaW5lIHR5cGU9eydwb3N0cyd9IHBvc3RzPXtwb3N0c30gZWxlbWVudD17ZWxlbWVudH0gLz5cbiAgICAgICAgICA8L0dyaWQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuVGFiUG9zdHMucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgdXNlcjogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHBvc3RzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoc3RhdGUpID0+IHN0YXRlO1xuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PiBiaW5kQWN0aW9uQ3JlYXRvcnMoe30sIGRpc3BhdGNoKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShUYWJQb3N0cyksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0VsZW1lbnRQYWdlL1RhYnMvVGFiUG9zdHMuanMiLCIvKipcbiAqIFBvc3QgY29udGFpbmVyIGNvbXBvbmVudFxuICogSXQgd2lsbCBkaXNwbGF5IG9uZSBwb3N0IHdpdGggc29tZSBvZiBhY3Rpb25zIChlZy4gQm9va21hcmtzLCBTaGFyZSwgRGVsZXRlKVxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB3aXRoUm91dGVyIGZyb20gJ3JlYWN0LXJvdXRlci93aXRoUm91dGVyJztcbmltcG9ydCBMaW5rIGZyb20gJ3JlYWN0LXJvdXRlci1kb20vTGluayc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgY2xhc3NuYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5pbXBvcnQgQ2FyZCwge1xuICBDYXJkSGVhZGVyLFxuICBDYXJkTWVkaWEsXG4gIENhcmRDb250ZW50LFxuICBDYXJkQWN0aW9ucyxcbn0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgRGlhbG9nLCB7IERpYWxvZ0FjdGlvbnMsIERpYWxvZ1RpdGxlIH0gZnJvbSAnbWF0ZXJpYWwtdWkvRGlhbG9nJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJhZGdlIGZyb20gJ21hdGVyaWFsLXVpL0JhZGdlJztcbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IFBhcGVyIGZyb20gJ21hdGVyaWFsLXVpL1BhcGVyJztcbmltcG9ydCBUb29sdGlwIGZyb20gJ21hdGVyaWFsLXVpL1Rvb2x0aXAnO1xuXG5pbXBvcnQgTW9yZVZlcnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL01vcmVWZXJ0JztcbmltcG9ydCBGYXZvcml0ZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRmF2b3JpdGUnO1xuaW1wb3J0IFR1cm5lZEluSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9UdXJuZWRJbic7XG5pbXBvcnQgQ29tbWVudEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQ29tbWVudCc7XG5pbXBvcnQgRGVsZXRlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9EZWxldGUnO1xuaW1wb3J0IFJlbW92ZVJlZEV5ZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvUmVtb3ZlUmVkRXllJztcbmltcG9ydCBTaGFyZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvU2hhcmUnO1xuXG4vLyBBbGwgYWN0aW9ucyBhcmUgZGVmaW5lZCBoZXJlLlxuaW1wb3J0IGdldEVsZW1lbnRCeUlkIGZyb20gJy4uLy4uL2FjdGlvbnMvZWxlbWVudHMvZ2V0QnlJZCc7XG5cbmltcG9ydCBhcGlQb3N0RGVsZXRlIGZyb20gJy4uLy4uL2FwaS9wb3N0cy91c2Vycy9kZWxldGUnO1xuaW1wb3J0IGFjdGlvblBvc3REZWxldGUgZnJvbSAnLi4vLi4vYWN0aW9ucy9wb3N0cy9kZWxldGUnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAnMSUnLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgYXZhdGFyOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgfSxcbiAgdGl0bGU6IHtcbiAgICAvLyBjb2xvcjogJyMzNjU4OTknLFxuICAgIGN1cnNvcjogJ3BvaW50ZXInLFxuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gIH0sXG4gIHN1YmhlYWRlcjoge1xuICAgIC8vIGNvbG9yOiAnIzM2NTg5OScsXG4gICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgfSxcbiAgbWVkaWE6IHtcbiAgICBoZWlnaHQ6IDE5NCxcbiAgfSxcbiAgZmF2b3JpdGVJY29uQWN0aXZlOiB7XG4gICAgY29sb3I6ICcjRjUwMDU3JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBiYWRnZToge1xuICAgIG1hcmdpbjogYDAgJHt0aGVtZS5zcGFjaW5nLnVuaXQgKiAyfXB4YCxcbiAgICBjb2xvcjogJ2JsdWUnLFxuICB9LFxuICBtYXJnaW46IHtcbiAgICBib3JkZXI6ICcwLjVweCBzb2xpZCByZ2JhKDIwOSwyMDksMjA5LDEpJyxcbiAgICBtYXJnaW5Cb3R0b206ICctMTBweCcsXG4gIH0sXG4gIGNvbnRlbnQ6IHtcbiAgICB0ZXh0QWxpZ246ICdqdXN0aWZ5JyxcbiAgfSxcbn0pO1xuXG5jbGFzcyBQb3N0IGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGhvdmVyOiBmYWxzZSxcbiAgICAgIGZhdm9yaXRlOiBmYWxzZSxcbiAgICAgIGRlbGV0ZURpYWxvZzogZmFsc2UsXG4gICAgfTtcbiAgfVxuXG4gIC8vIGNvbXBvbmVudFdpbGxNb3VudCBpcyBleGVjdXRlZCBiZWZvcmUgcmVuZGVyaW5nXG4gIGNvbXBvbmVudFdpbGxNb3VudCgpIHtcbiAgICBjb25zdCB7IGVsZW1lbnRzLCBwb3N0cywgcG9zdElkIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkOiB1c2VySWQgfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBhdXRob3JJZCB9ID0gcG9zdHNbcG9zdElkXTtcblxuICAgIGNvbnN0IGF1dGhvciA9IGVsZW1lbnRzW2F1dGhvcklkXTtcblxuICAgIC8vIGlmIGF1dGhvciBpcyBub3QgZm91bmRcbiAgICBpZiAodXNlcklkICE9PSBhdXRob3JJZCAmJiAhYXV0aG9yKSB7XG4gICAgICB0aGlzLnByb3BzLmdldEVsZW1lbnRCeUlkKHsgaWQ6IGF1dGhvcklkLCB0b2tlbiB9KTtcbiAgICB9XG4gIH1cblxuICBvbkNsaWNrRmF2b3JpdGUgPSBhc3luYyAoKSA9PiB7XG4gICAgY29uc3QgeyBmYXZvcml0ZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHRoaXMuc2V0U3RhdGUoeyBmYXZvcml0ZTogIWZhdm9yaXRlIH0pO1xuICB9O1xuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcblxuICBvbkNsaWNrUG9zdCA9ICgpID0+IHRoaXMucHJvcHMuaGlzdG9yeS5wdXNoKGAvcG9zdHMvJHt0aGlzLnByb3BzLnBvc3RJZH1gKTtcblxuICBkZWxldGVEaWFsb2dPcGVuID0gKCkgPT4ge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkZWxldGVEaWFsb2c6IHRydWUgfSk7XG4gIH07XG5cbiAgZGVsZXRlRGlhbG9nQ2xvc2UgPSAoKSA9PiB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGRlbGV0ZURpYWxvZzogZmFsc2UgfSk7XG4gIH07XG5cbiAgZGVsZXRlUG9zdENsaWNrID0gYXN5bmMgKCkgPT4ge1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBwb3N0SWQgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yIH0gPSBhd2FpdCBhcGlQb3N0RGVsZXRlKHtcbiAgICAgIHRva2VuLFxuICAgICAgdXNlcklkOiBpZCxcbiAgICAgIHBvc3RJZCxcbiAgICB9KTtcblxuICAgIGlmIChzdGF0dXNDb2RlICE9PSAyMDApIHtcbiAgICAgIC8vIHNob3cgc29tZSBlcnJvciBtZXNzYWdlLlxuICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIHRoaXMucHJvcHMuYWN0aW9uUG9zdERlbGV0ZSh7IHBvc3RJZCB9KTtcbiAgICB0aGlzLnNldFN0YXRlKHsgZGVsZXRlRGlhbG9nOiBmYWxzZSB9KTtcbiAgfTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBwb3N0cywgZWxlbWVudHMsIHBvc3RJZCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IHVzZXIgfSA9IGVsZW1lbnRzO1xuICAgIGNvbnN0IHsgaXNTaWduZWRJbiwgaWQ6IHVzZXJJZCB9ID0gdXNlcjtcblxuICAgIGNvbnN0IHBvc3QgPSBwb3N0c1twb3N0SWRdO1xuXG4gICAgY29uc3QgeyBmYXZvcml0ZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGlmICh0eXBlb2YgcG9zdCA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgIHJldHVybiA8ZGl2IC8+O1xuICAgIH1cblxuICAgIGNvbnN0IHsgY292ZXIsIHRpdGxlLCBkZXNjcmlwdGlvbiwgYXV0aG9ySWQgfSA9IHBvc3Q7XG4gICAgY29uc3QgYXV0aG9yID0gYXV0aG9ySWQgPT09IHVzZXJJZCA/IHVzZXIgOiBlbGVtZW50c1thdXRob3JJZF07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH1cbiAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLm9uTW91c2VFbnRlcn1cbiAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgID5cbiAgICAgICAgPENhcmQgcmFpc2VkPXt0aGlzLnN0YXRlLmhvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgIGF2YXRhcj17XG4gICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfT5cbiAgICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgc3JjPXsnL3B1YmxpYy9waG90b3MvbWF5YXNoLWxvZ28tdHJhbnNwYXJlbnQucG5nJ31cbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICA8L1BhcGVyPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYWN0aW9uPXtcbiAgICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJDb21pbmcgc29vblwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICAgIDxJY29uQnV0dG9uIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgICAgPE1vcmVWZXJ0SWNvbiAvPlxuICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICA8TGluayB0bz17YC9AJHthdXRob3IudXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlfT5cbiAgICAgICAgICAgICAgICB7YXV0aG9yLm5hbWV9XG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHN1YmhlYWRlcj17XG4gICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfSBjbGFzc05hbWU9e2NsYXNzZXMuc3ViaGVhZGVyfT5cbiAgICAgICAgICAgICAgICBAe2F1dGhvci51c2VybmFtZX1cbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgfVxuICAgICAgICAgIC8+XG4gICAgICAgICAgPENhcmRNZWRpYVxuICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLm1lZGlhfVxuICAgICAgICAgICAgaW1hZ2U9e1xuICAgICAgICAgICAgICBjb3ZlciB8fFxuICAgICAgICAgICAgICAnaHR0cDovL2dpbnZhLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNi8wOC9naW52YV8yMDE2LTA4LTAyXzAyLTMwLTU0LmpwZydcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRpdGxlPXt0aXRsZX1cbiAgICAgICAgICAvPlxuICAgICAgICAgIDxDYXJkQ29udGVudCBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tQb3N0fT5cbiAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHR5cGU9XCJ0aXRsZVwiPnt0aXRsZX08L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICB7dHlwZW9mIGRlc2NyaXB0aW9uID09PSAnc3RyaW5nJyA/IChcbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkxXCIgZ3V0dGVyQm90dG9tIGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fT5cbiAgICAgICAgICAgICAgICB7ZGVzY3JpcHRpb259XG4gICAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgey8qIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLm1hcmdpbn0gLz4gKi99XG4gICAgICAgICAgPENhcmRBY3Rpb25zPlxuICAgICAgICAgICAgPEljb25CdXR0b24gb25DbGljaz17dGhpcy5vbkNsaWNrRmF2b3JpdGV9PlxuICAgICAgICAgICAgICA8QmFkZ2UgYmFkZ2VDb250ZW50PXswfT5cbiAgICAgICAgICAgICAgICA8RmF2b3JpdGVJY29uXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzbmFtZXMoe1xuICAgICAgICAgICAgICAgICAgICBbYCR7Y2xhc3Nlcy5mYXZvcml0ZUljb25BY3RpdmV9YF06IGZhdm9yaXRlLFxuICAgICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9CYWRnZT5cbiAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJDb21tZW50XCI+XG4gICAgICAgICAgICAgIDxCYWRnZSBiYWRnZUNvbnRlbnQ9ezB9PlxuICAgICAgICAgICAgICAgIDxDb21tZW50SWNvbiAvPlxuICAgICAgICAgICAgICA8L0JhZGdlPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvb2sgTWFya3NcIj5cbiAgICAgICAgICAgICAgPFR1cm5lZEluSWNvbiAvPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAge2lzU2lnbmVkSW4gJiYgdXNlcklkID09PSBhdXRob3JJZCA/IChcbiAgICAgICAgICAgICAgPGRpdj5cbiAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiRGVsZXRlXCIgb25DbGljaz17dGhpcy5kZWxldGVEaWFsb2dPcGVufT5cbiAgICAgICAgICAgICAgICAgIDxEZWxldGVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICAgIDxEaWFsb2dcbiAgICAgICAgICAgICAgICAgIG9wZW49e3RoaXMuc3RhdGUuZGVsZXRlRGlhbG9nfVxuICAgICAgICAgICAgICAgICAgb25DbG9zZT17dGhpcy5kZWxldGVEaWFsb2dDbG9zZX1cbiAgICAgICAgICAgICAgICAgIGFyaWEtbGFiZWxsZWRieT17YHBvc3QtJHtwb3N0SWR9LWRlbGV0ZS1kaWFsb2dgfVxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxEaWFsb2dUaXRsZSBpZD17YHBvc3QtJHtwb3N0SWR9LWRlbGV0ZS1kaWFsb2dgfT5cbiAgICAgICAgICAgICAgICAgICAgQXJlIHlvdSBzdXJlP1xuICAgICAgICAgICAgICAgICAgPC9EaWFsb2dUaXRsZT5cbiAgICAgICAgICAgICAgICAgIDxEaWFsb2dBY3Rpb25zPlxuICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIG9uQ2xpY2s9e3RoaXMuZGVsZXRlRGlhbG9nQ2xvc2V9IGNvbG9yPVwicHJpbWFyeVwiPlxuICAgICAgICAgICAgICAgICAgICAgIENhbmNlbFxuICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuZGVsZXRlUG9zdENsaWNrfVxuICAgICAgICAgICAgICAgICAgICAgIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICAgICAgYXV0b0ZvY3VzXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBEZWxldGVcbiAgICAgICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgICAgICA8L0RpYWxvZ0FjdGlvbnM+XG4gICAgICAgICAgICAgICAgPC9EaWFsb2c+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJTaGFyZVwiPlxuICAgICAgICAgICAgICA8QmFkZ2UgYmFkZ2VDb250ZW50PXswfT5cbiAgICAgICAgICAgICAgICA8UmVtb3ZlUmVkRXllSWNvbiAvPlxuICAgICAgICAgICAgICA8L0JhZGdlPlxuICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlNoYXJlXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgIDxTaGFyZUljb24gLz5cbiAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICA8L0NhcmQ+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblBvc3QucHJvcFR5cGVzID0ge1xuICBoaXN0b3J5OiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHBvc3RzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgLy8gcG9zdDogUHJvcFR5cGVzLnNoYXBlKHtcbiAgLy8gICBhdXRob3JJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAvLyAgIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAvLyAgIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIC8vICAgZGVzY3JpcHRpb246IFByb3BUeXBlcy5zdHJpbmcsXG4gIC8vIH0pLmlzUmVxdWlyZWQsXG5cbiAgcG9zdElkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG5cbiAgZ2V0RWxlbWVudEJ5SWQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGFjdGlvblBvc3REZWxldGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cywgcG9zdHMgfSkgPT4gKHsgZWxlbWVudHMsIHBvc3RzIH0pO1xuXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBnZXRFbGVtZW50QnlJZCxcbiAgICAgIGFjdGlvblBvc3REZWxldGUsXG4gICAgfSxcbiAgICBkaXNwYXRjaCxcbiAgKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKSh3aXRoUm91dGVyKFBvc3QpKSxcbik7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdC9pbmRleC5qcyIsIi8qKlxuICogUG9zdENyZWF0ZSBjb21wb25lbnQgd2lsbCBiZSB1c2VkIHRvIGNyZWF0ZSBuZXcgcG9zdC5cbiAqXG4gKiBOb3RlOiBiZWZvcmUgYWRkaW5nIHRoaXMgY29tcG9uZW50IGluIGFueSBwbGFjZSwgbWFrZSBzdXJlXG4gKiB0aGUgcGVyc29uIHdobyBpcyBhY2Nlc3NpbmcgdGhpcyBjb21wb25lbnQgc2hvdWxkIGhhdmUgcmlnaHQgdG8gY3JlYXRlIHBvc3QuXG4gKiBpLmUuIHBlcnNvbiBzaG91bGQgYmUgbG9nZ2VkIGluIGFuZCB1c2VyIGNhbiBvbmx5IGNyZWF0ZSBwb3N0IGZvciB0aGVpclxuICogcHJvZmlsZSBvbmx5LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgQ2FyZCwgeyBDYXJkSGVhZGVyLCBDYXJkQ29udGVudCwgQ2FyZEFjdGlvbnMgfSBmcm9tICdtYXRlcmlhbC11aS9DYXJkJztcbmltcG9ydCBBdmF0YXIgZnJvbSAnbWF0ZXJpYWwtdWkvQXZhdGFyJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IFBhcGVyIGZyb20gJ21hdGVyaWFsLXVpL1BhcGVyJztcblxuaW1wb3J0IElucHV0IGZyb20gJy4uLy4uL2NvbXBvbmVudHMvSW5wdXQnO1xuXG5pbXBvcnQgYXBpUG9zdENyZWF0ZSBmcm9tICcuLi8uLi9hcGkvcG9zdHMvdXNlcnMvY3JlYXRlJztcbmltcG9ydCBhY3Rpb25Qb3N0Q3JlYXRlIGZyb20gJy4uLy4uL2FjdGlvbnMvcG9zdHMvdXNlcnMvY3JlYXRlJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgcGFkZGluZzogJzElJyxcbiAgfSxcbiAgY2FyZDoge1xuICAgIGJvcmRlclJhZGl1czogJzhweCcsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gIH0sXG4gIGdyaWRDb250YWluZXI6IHt9LFxuICBwb3N0OiB7XG4gICAgcGFkZGluZzogJzElJyxcbiAgfSxcbiAgY2FyZEhlYWRlcjoge1xuICAgIC8vIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgfSxcbiAgY2FyZE1lZGlhOiB7fSxcbiAgY2FyZE1lZGlhSW1hZ2U6IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59O1xuXG4vKipcbiAqIFBvc3RDcmVhdGUgY29tcG9uZW50IHdpbGwgY3JlYXRlIGEgbmV3IHBvc3QgZm9yIHNpZ25lZCBpbiB1c2VyLlxuICogQGNsYXNzIFBvc3RDcmVhdGVcbiAqIEB2ZXJzaW9uIDEuMC4wXG4gKi9cbmNsYXNzIFBvc3RDcmVhdGUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgYWN0aXZlOiBmYWxzZSxcbiAgICAgIHRpdGxlOiAnJyxcbiAgICAgIHRpdGxlTGVuZ3RoOiAwLFxuICAgICAgcGxhY2Vob2xkZXI6ICdUaXRsZS4uLicsXG4gICAgICBwb3N0VHlwZTogJ2FydGljbGUnLFxuXG4gICAgICB2YWxpZDogZmFsc2UsXG4gICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgZXJyb3I6ICcnLFxuICAgICAgbWVzc2FnZTogJycsXG4gICAgICBob3ZlcjogZmFsc2UsXG4gICAgfTtcblxuICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblN1Ym1pdCA9IHRoaXMub25TdWJtaXQuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2hhbmdlKGUpIHtcbiAgICBjb25zdCB0aXRsZSA9IGUudGFyZ2V0LnZhbHVlO1xuICAgIGNvbnN0IHRpdGxlTGVuZ3RoID0gdGl0bGUubGVuZ3RoO1xuICAgIGNvbnN0IHZhbGlkID0gdGl0bGVMZW5ndGggPiAwICYmIHRpdGxlTGVuZ3RoIDwgMTQ4O1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICB0aXRsZSxcbiAgICAgIHRpdGxlTGVuZ3RoLFxuICAgICAgdmFsaWQsXG4gICAgICBlcnJvcjogIXZhbGlkID8gJ1RpdGxlIGxlbmd0aCBzaG91bGQgYmUgaW4gYmV0d2VlbiAxIHRvIDE0OC4nIDogJycsXG4gICAgfSk7XG4gIH1cblxuICBhc3luYyBvblN1Ym1pdChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGNvbnN0IHsgdG9rZW4sIGlkOiB1c2VySWQgfSA9IHRoaXMucHJvcHMuZWxlbWVudHMudXNlcjtcbiAgICBjb25zdCB7IHBvc3RUeXBlLCB0aXRsZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlOiAnQ3JlYXRpbmcgcG9zdC4uLicgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCBhcGlQb3N0Q3JlYXRlKHtcbiAgICAgIHRva2VuLFxuICAgICAgdXNlcklkLFxuICAgICAgcG9zdFR5cGUsXG4gICAgICB0aXRsZSxcbiAgICB9KTtcblxuICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgLy8gaGFuZGxlIGVycm9yXG4gICAgICB0aGlzLnNldFN0YXRlKHsgc3RhdHVzQ29kZSwgZXJyb3IgfSk7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2U6ICdTdWNjZXNzZnVsbHkgQ3JlYXRlZC4nIH0pO1xuXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgdmFsaWQ6IGZhbHNlLFxuICAgICAgICBzdGF0dXNDb2RlOiAwLFxuICAgICAgICBlcnJvcjogJycsXG4gICAgICAgIG1lc3NhZ2U6ICcnLFxuXG4gICAgICAgIHBvc3RUeXBlOiAnYXJ0aWNsZScsXG5cbiAgICAgICAgdGl0bGU6ICcnLFxuICAgICAgICB0aXRsZUxlbmd0aDogMCxcbiAgICAgIH0pO1xuICAgIH0sIDE1MDApO1xuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25Qb3N0Q3JlYXRlKHtcbiAgICAgIHN0YXR1c0NvZGUsXG4gICAgICAuLi5wYXlsb2FkLFxuICAgICAgdGl0bGUsXG4gICAgICBwb3N0VHlwZSxcbiAgICB9KTtcbiAgfVxuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcbiAgb25Gb2N1cyA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBhY3RpdmU6IHRydWUgfSk7XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgaG92ZXIsIGFjdGl2ZSwgcGxhY2Vob2xkZXIsIHRpdGxlLCBtZXNzYWdlIH0gPSB0aGlzLnN0YXRlO1xuICAgIGNvbnN0IHsgY2xhc3NlcywgZWxlbWVudHMgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBuYW1lLCBhdmF0YXIsIHVzZXJuYW1lIH0gPSBlbGVtZW50cy51c2VyO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9XG4gICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5vbk1vdXNlRW50ZXJ9XG4gICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbk1vdXNlTGVhdmV9XG4gICAgICA+XG4gICAgICAgIDxDYXJkIHJhaXNlZD17aG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICA8Q2FyZEhlYWRlclxuICAgICAgICAgICAgYXZhdGFyPXtcbiAgICAgICAgICAgICAgPFBhcGVyIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9IGVsZXZhdGlvbj17MX0+XG4gICAgICAgICAgICAgICAgPExpbmsgdG89e2AvQCR7dXNlcm5hbWV9YH0+XG4gICAgICAgICAgICAgICAgICA8QXZhdGFyXG4gICAgICAgICAgICAgICAgICAgIGFsdD17bmFtZX1cbiAgICAgICAgICAgICAgICAgICAgc3JjPXthdmF0YXIgfHwgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZyd9XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgICAgPC9QYXBlcj5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRpdGxlPXtcbiAgICAgICAgICAgICAgPElucHV0XG4gICAgICAgICAgICAgICAgdmFsdWU9e3RpdGxlfVxuICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkSGVhZGVyfVxuICAgICAgICAgICAgb25Gb2N1cz17dGhpcy5vbkZvY3VzfVxuICAgICAgICAgIC8+XG4gICAgICAgICAge2FjdGl2ZSAmJiBtZXNzYWdlLmxlbmd0aCA/IChcbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPFR5cG9ncmFwaHk+e21lc3NhZ2V9PC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICB7YWN0aXZlID8gKFxuICAgICAgICAgICAgPENhcmRBY3Rpb25zPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgICAgPEJ1dHRvbiByYWlzZWQgY29sb3I9XCJhY2NlbnRcIiBvbkNsaWNrPXt0aGlzLm9uU3VibWl0fT5cbiAgICAgICAgICAgICAgICBDcmVhdGVcbiAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICA8L0NhcmQ+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblBvc3RDcmVhdGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBhY3Rpb25Qb3N0Q3JlYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25Qb3N0Q3JlYXRlLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoUG9zdENyZWF0ZSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Bvc3RDcmVhdGUvaW5kZXguanMiLCIvKipcbiAqIFRpbWVsaW5lIERpc3BsYXkgY29tcG9uZW50XG4gKiBUaGlzIGNvbXBvbmVudCB3aWxsIGRpc3BsYXkgZWl0aGVyIGNvdXJzZSBvciBQb3N0IHRpbWVsaW5lXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcblxuaW1wb3J0IFBvc3QgZnJvbSAnLi4vUG9zdCc7XG5pbXBvcnQgQ291cnNlIGZyb20gJy4uL0NvdXJzZSc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIGZsZXhHcm93OiAnMScsXG4gIH0sXG59O1xuXG5jbGFzcyBUaW1lbGluZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCB0eXBlLCBwb3N0cywgY291cnNlcywgZWxlbWVudCB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAge3R5cGUgPT09ICdwb3N0cydcbiAgICAgICAgICA/IGVsZW1lbnQucG9zdHMubWFwKChwKSA9PiA8UG9zdCBrZXk9e3AucG9zdElkfSBwb3N0SWQ9e3AucG9zdElkfSAvPilcbiAgICAgICAgICA6IGVsZW1lbnQuY291cnNlcy5tYXAoKGMpID0+IChcbiAgICAgICAgICAgIDxDb3Vyc2Uga2V5PXtjLmNvdXJzZUlkfSBjb3Vyc2VJZD17Yy5jb3Vyc2VJZH0gLz5cbiAgICAgICAgICApKX1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuVGltZWxpbmUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHR5cGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCwgLy8gJ3Bvc3RzJyBvciAnY291cnNlcydcbiAgZWxlbWVudDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBwb3N0czogUHJvcFR5cGVzLm9iamVjdCxcbiAgY291cnNlczogUHJvcFR5cGVzLm9iamVjdCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShUaW1lbGluZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvVGltZWxpbmUvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9