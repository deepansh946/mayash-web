webpackJsonp([19],{

/***/ "./src/client/containers/AppDrawer/index.js":
/*!**************************************************!*\
  !*** ./src/client/containers/AppDrawer/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _styles = __webpack_require__(/*! ./styles */ "./src/client/containers/AppDrawer/styles.js");

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AsyncDrawerNotSignedIn = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(36).then(__webpack_require__.bind(null, /*! ./DrawerNotSignedIn */ "./src/client/containers/AppDrawer/DrawerNotSignedIn.js"));
  },
  modules: ['./DrawerNotSignedIn'],
  loading: _Loading2.default
});
// import { bindActionCreators } from 'redux';
/**
 * This is a Drawer Container Component.
 * This will include all the routes when users are not signed in.
 * When users are signed in, it will display their personal informations.
 *
 * @format
 */

var AsyncDrawerSignedIn = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(43).then(__webpack_require__.bind(null, /*! ./DrawerSignedIn */ "./src/client/containers/AppDrawer/DrawerSignedIn.js"));
  },
  modules: ['./DrawerSignedIn'],
  loading: _Loading2.default
});

function AppDrawer(props) {
  var isSignedIn = props.elements.user.isSignedIn;


  return isSignedIn ? _react2.default.createElement(AsyncDrawerSignedIn, props) : _react2.default.createElement(AsyncDrawerNotSignedIn, props);
}

/**
 * This will validate all the incoming props from parent component
 * to AppDrawer component.
 * @type {Object}
 */
AppDrawer.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  open: _propTypes2.default.bool.isRequired,
  closeDrawer: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref) {
  var elements = _ref.elements;
  return { elements: elements };
};

/**
 * @param {object} dispatch : dipatch meesege (action type)
 * @returns {object} : action creators
 */
/*
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
*/

/**
 * 'connect' method connect state to action creators
 * When we call connect, immediately we call AppDrawer component
 * and all states and action creators are passed to AppDrawer component
 */
exports.default = (0, _reactRedux.connect)(mapStateToProps
// mapDispatchToProps,
)((0, _withStyles2.default)(_styles2.default)(AppDrawer));

/***/ }),

/***/ "./src/client/containers/AppDrawer/styles.js":
/*!***************************************************!*\
  !*** ./src/client/containers/AppDrawer/styles.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * /*
 *   This Style file is for index.js file.
 *
 * @format
 */

var styles = function styles(theme) {
  return {
    list: {
      width: 250,
      flex: 'initial'
    },
    listFull: {
      width: 'auto',
      flex: 'initial'
    },
    user: {
      minHeight: '100px',
      paddingTop: '12px',
      paddingRight: '16px',
      paddingBottom: '12px',
      paddingLeft: '16px',
      backgroundColor: theme.palette.primary[700],
      backgroundImage: 'url("https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg")',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center'
    },
    avatar: {
      width: 60,
      height: 60,
      paddingTop: 10,
      paddingBottom: 10
    },

    avatarUnsignedPage: {
      float: 'left',
      paddingTop: 10
    },
    button: {
      transitionDuration: '0.4s',
      '&:hover': {
        background: theme.palette.primary[100],
        transform: 'scale(1.09)'
      }
    },
    info: {
      color: theme.palette.getContrastText(theme.palette.primary[700]),
      paddingTop: 2,
      paddingBottom: 2
    }
  };
};

exports.default = styles;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQXBwRHJhd2VyL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9BcHBEcmF3ZXIvc3R5bGVzLmpzIl0sIm5hbWVzIjpbIkFzeW5jRHJhd2VyTm90U2lnbmVkSW4iLCJsb2FkZXIiLCJtb2R1bGVzIiwibG9hZGluZyIsIkFzeW5jRHJhd2VyU2lnbmVkSW4iLCJBcHBEcmF3ZXIiLCJwcm9wcyIsImlzU2lnbmVkSW4iLCJlbGVtZW50cyIsInVzZXIiLCJwcm9wVHlwZXMiLCJjbGFzc2VzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm9wZW4iLCJib29sIiwiY2xvc2VEcmF3ZXIiLCJmdW5jIiwibWFwU3RhdGVUb1Byb3BzIiwic3R5bGVzIiwidGhlbWUiLCJsaXN0Iiwid2lkdGgiLCJmbGV4IiwibGlzdEZ1bGwiLCJtaW5IZWlnaHQiLCJwYWRkaW5nVG9wIiwicGFkZGluZ1JpZ2h0IiwicGFkZGluZ0JvdHRvbSIsInBhZGRpbmdMZWZ0IiwiYmFja2dyb3VuZENvbG9yIiwicGFsZXR0ZSIsInByaW1hcnkiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJiYWNrZ3JvdW5kUmVwZWF0IiwiYmFja2dyb3VuZFBvc2l0aW9uIiwiYXZhdGFyIiwiaGVpZ2h0IiwiYXZhdGFyVW5zaWduZWRQYWdlIiwiZmxvYXQiLCJidXR0b24iLCJ0cmFuc2l0aW9uRHVyYXRpb24iLCJiYWNrZ3JvdW5kIiwidHJhbnNmb3JtIiwiaW5mbyIsImNvbG9yIiwiZ2V0Q29udHJhc3RUZXh0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQVFBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUEseUJBQXlCLDZCQUFTO0FBQ3RDQyxVQUFRO0FBQUEsV0FBTSxpS0FBTjtBQUFBLEdBRDhCO0FBRXRDQyxXQUFTLENBQUMscUJBQUQsQ0FGNkI7QUFHdENDO0FBSHNDLENBQVQsQ0FBL0I7QUFWQTtBQVZBOzs7Ozs7OztBQTBCQSxJQUFNQyxzQkFBc0IsNkJBQVM7QUFDbkNILFVBQVE7QUFBQSxXQUFNLDJKQUFOO0FBQUEsR0FEMkI7QUFFbkNDLFdBQVMsQ0FBQyxrQkFBRCxDQUYwQjtBQUduQ0M7QUFIbUMsQ0FBVCxDQUE1Qjs7QUFNQSxTQUFTRSxTQUFULENBQW1CQyxLQUFuQixFQUEwQjtBQUFBLE1BQ2hCQyxVQURnQixHQUNERCxNQUFNRSxRQUFOLENBQWVDLElBRGQsQ0FDaEJGLFVBRGdCOzs7QUFHeEIsU0FBT0EsYUFDTCw4QkFBQyxtQkFBRCxFQUF5QkQsS0FBekIsQ0FESyxHQUdMLDhCQUFDLHNCQUFELEVBQTRCQSxLQUE1QixDQUhGO0FBS0Q7O0FBRUQ7Ozs7O0FBS0FELFVBQVVLLFNBQVYsR0FBc0I7QUFDcEJDLFdBQVMsb0JBQVVDLE1BQVYsQ0FBaUJDLFVBRE47O0FBR3BCTCxZQUFVLG9CQUFVSSxNQUFWLENBQWlCQyxVQUhQOztBQUtwQkMsUUFBTSxvQkFBVUMsSUFBVixDQUFlRixVQUxEO0FBTXBCRyxlQUFhLG9CQUFVQyxJQUFWLENBQWVKO0FBTlIsQ0FBdEI7O0FBU0EsSUFBTUssa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdWLFFBQUgsUUFBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7O0FBRUE7Ozs7QUFJQTs7OztBQUlBOzs7OztrQkFLZSx5QkFDYlU7QUFDQTtBQUZhLEVBR2IsNENBQW1CYixTQUFuQixDQUhhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3ZFZjs7Ozs7OztBQU9BLElBQU1jLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxLQUFEO0FBQUEsU0FBWTtBQUN6QkMsVUFBTTtBQUNKQyxhQUFPLEdBREg7QUFFSkMsWUFBTTtBQUZGLEtBRG1CO0FBS3pCQyxjQUFVO0FBQ1JGLGFBQU8sTUFEQztBQUVSQyxZQUFNO0FBRkUsS0FMZTtBQVN6QmQsVUFBTTtBQUNKZ0IsaUJBQVcsT0FEUDtBQUVKQyxrQkFBWSxNQUZSO0FBR0pDLG9CQUFjLE1BSFY7QUFJSkMscUJBQWUsTUFKWDtBQUtKQyxtQkFBYSxNQUxUO0FBTUpDLHVCQUFpQlYsTUFBTVcsT0FBTixDQUFjQyxPQUFkLENBQXNCLEdBQXRCLENBTmI7QUFPSkMsdUJBQ0UsK0VBUkU7QUFTSkMsd0JBQWtCLFdBVGQ7QUFVSkMsMEJBQW9CO0FBVmhCLEtBVG1CO0FBcUJ6QkMsWUFBUTtBQUNOZCxhQUFPLEVBREQ7QUFFTmUsY0FBUSxFQUZGO0FBR05YLGtCQUFZLEVBSE47QUFJTkUscUJBQWU7QUFKVCxLQXJCaUI7O0FBNEJ6QlUsd0JBQW9CO0FBQ2xCQyxhQUFPLE1BRFc7QUFFbEJiLGtCQUFZO0FBRk0sS0E1Qks7QUFnQ3pCYyxZQUFRO0FBQ05DLDBCQUFvQixNQURkO0FBRU4saUJBQVc7QUFDVEMsb0JBQVl0QixNQUFNVyxPQUFOLENBQWNDLE9BQWQsQ0FBc0IsR0FBdEIsQ0FESDtBQUVUVyxtQkFBVztBQUZGO0FBRkwsS0FoQ2lCO0FBdUN6QkMsVUFBTTtBQUNKQyxhQUFPekIsTUFBTVcsT0FBTixDQUFjZSxlQUFkLENBQThCMUIsTUFBTVcsT0FBTixDQUFjQyxPQUFkLENBQXNCLEdBQXRCLENBQTlCLENBREg7QUFFSk4sa0JBQVksQ0FGUjtBQUdKRSxxQkFBZTtBQUhYO0FBdkNtQixHQUFaO0FBQUEsQ0FBZjs7a0JBOENlVCxNIiwiZmlsZSI6IjE5LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogVGhpcyBpcyBhIERyYXdlciBDb250YWluZXIgQ29tcG9uZW50LlxuICogVGhpcyB3aWxsIGluY2x1ZGUgYWxsIHRoZSByb3V0ZXMgd2hlbiB1c2VycyBhcmUgbm90IHNpZ25lZCBpbi5cbiAqIFdoZW4gdXNlcnMgYXJlIHNpZ25lZCBpbiwgaXQgd2lsbCBkaXNwbGF5IHRoZWlyIHBlcnNvbmFsIGluZm9ybWF0aW9ucy5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG4vLyBpbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuaW1wb3J0IExvYWRhYmxlIGZyb20gJ3JlYWN0LWxvYWRhYmxlJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG5pbXBvcnQgTG9hZGluZyBmcm9tICcuLi8uLi9jb21wb25lbnRzL0xvYWRpbmcnO1xuXG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGVzJztcblxuY29uc3QgQXN5bmNEcmF3ZXJOb3RTaWduZWRJbiA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vRHJhd2VyTm90U2lnbmVkSW4nKSxcbiAgbW9kdWxlczogWycuL0RyYXdlck5vdFNpZ25lZEluJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNEcmF3ZXJTaWduZWRJbiA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vRHJhd2VyU2lnbmVkSW4nKSxcbiAgbW9kdWxlczogWycuL0RyYXdlclNpZ25lZEluJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuZnVuY3Rpb24gQXBwRHJhd2VyKHByb3BzKSB7XG4gIGNvbnN0IHsgaXNTaWduZWRJbiB9ID0gcHJvcHMuZWxlbWVudHMudXNlcjtcblxuICByZXR1cm4gaXNTaWduZWRJbiA/IChcbiAgICA8QXN5bmNEcmF3ZXJTaWduZWRJbiB7Li4ucHJvcHN9IC8+XG4gICkgOiAoXG4gICAgPEFzeW5jRHJhd2VyTm90U2lnbmVkSW4gey4uLnByb3BzfSAvPlxuICApO1xufVxuXG4vKipcbiAqIFRoaXMgd2lsbCB2YWxpZGF0ZSBhbGwgdGhlIGluY29taW5nIHByb3BzIGZyb20gcGFyZW50IGNvbXBvbmVudFxuICogdG8gQXBwRHJhd2VyIGNvbXBvbmVudC5cbiAqIEB0eXBlIHtPYmplY3R9XG4gKi9cbkFwcERyYXdlci5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBlbGVtZW50czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIG9wZW46IFByb3BUeXBlcy5ib29sLmlzUmVxdWlyZWQsXG4gIGNsb3NlRHJhd2VyOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5cbi8qKlxuICogQHBhcmFtIHtvYmplY3R9IGRpc3BhdGNoIDogZGlwYXRjaCBtZWVzZWdlIChhY3Rpb24gdHlwZSlcbiAqIEByZXR1cm5zIHtvYmplY3R9IDogYWN0aW9uIGNyZWF0b3JzXG4gKi9cbi8qXG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSBkaXNwYXRjaCA9PiBiaW5kQWN0aW9uQ3JlYXRvcnMoYWN0aW9ucywgZGlzcGF0Y2gpO1xuKi9cblxuLyoqXG4gKiAnY29ubmVjdCcgbWV0aG9kIGNvbm5lY3Qgc3RhdGUgdG8gYWN0aW9uIGNyZWF0b3JzXG4gKiBXaGVuIHdlIGNhbGwgY29ubmVjdCwgaW1tZWRpYXRlbHkgd2UgY2FsbCBBcHBEcmF3ZXIgY29tcG9uZW50XG4gKiBhbmQgYWxsIHN0YXRlcyBhbmQgYWN0aW9uIGNyZWF0b3JzIGFyZSBwYXNzZWQgdG8gQXBwRHJhd2VyIGNvbXBvbmVudFxuICovXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KFxuICBtYXBTdGF0ZVRvUHJvcHMsXG4gIC8vIG1hcERpc3BhdGNoVG9Qcm9wcyxcbikod2l0aFN0eWxlcyhzdHlsZXMpKEFwcERyYXdlcikpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0FwcERyYXdlci9pbmRleC5qcyIsIi8qKlxuICogLypcbiAqICAgVGhpcyBTdHlsZSBmaWxlIGlzIGZvciBpbmRleC5qcyBmaWxlLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIGxpc3Q6IHtcbiAgICB3aWR0aDogMjUwLFxuICAgIGZsZXg6ICdpbml0aWFsJyxcbiAgfSxcbiAgbGlzdEZ1bGw6IHtcbiAgICB3aWR0aDogJ2F1dG8nLFxuICAgIGZsZXg6ICdpbml0aWFsJyxcbiAgfSxcbiAgdXNlcjoge1xuICAgIG1pbkhlaWdodDogJzEwMHB4JyxcbiAgICBwYWRkaW5nVG9wOiAnMTJweCcsXG4gICAgcGFkZGluZ1JpZ2h0OiAnMTZweCcsXG4gICAgcGFkZGluZ0JvdHRvbTogJzEycHgnLFxuICAgIHBhZGRpbmdMZWZ0OiAnMTZweCcsXG4gICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNzAwXSxcbiAgICBiYWNrZ3JvdW5kSW1hZ2U6XG4gICAgICAndXJsKFwiaHR0cHM6Ly9kcml2ZW5sb2NhbC5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTUvMTAvTWF0ZXJpYWwtZGVzaWduLmpwZ1wiKScsXG4gICAgYmFja2dyb3VuZFJlcGVhdDogJ25vLXJlcGVhdCcsXG4gICAgYmFja2dyb3VuZFBvc2l0aW9uOiAnY2VudGVyIGNlbnRlcicsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIHdpZHRoOiA2MCxcbiAgICBoZWlnaHQ6IDYwLFxuICAgIHBhZGRpbmdUb3A6IDEwLFxuICAgIHBhZGRpbmdCb3R0b206IDEwLFxuICB9LFxuXG4gIGF2YXRhclVuc2lnbmVkUGFnZToge1xuICAgIGZsb2F0OiAnbGVmdCcsXG4gICAgcGFkZGluZ1RvcDogMTAsXG4gIH0sXG4gIGJ1dHRvbjoge1xuICAgIHRyYW5zaXRpb25EdXJhdGlvbjogJzAuNHMnLFxuICAgICcmOmhvdmVyJzoge1xuICAgICAgYmFja2dyb3VuZDogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzEwMF0sXG4gICAgICB0cmFuc2Zvcm06ICdzY2FsZSgxLjA5KScsXG4gICAgfSxcbiAgfSxcbiAgaW5mbzoge1xuICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNzAwXSksXG4gICAgcGFkZGluZ1RvcDogMixcbiAgICBwYWRkaW5nQm90dG9tOiAyLFxuICB9LFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IHN0eWxlcztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9BcHBEcmF3ZXIvc3R5bGVzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==