webpackJsonp([11],{

/***/ "./node_modules/material-ui-icons/ExpandMore.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/ExpandMore.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z' });

var ExpandMore = function ExpandMore(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

ExpandMore = (0, _pure2.default)(ExpandMore);
ExpandMore.muiName = 'SvgIcon';

exports.default = ExpandMore;

/***/ }),

/***/ "./node_modules/material-ui-icons/NavigateNext.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/NavigateNext.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z' });

var NavigateNext = function NavigateNext(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

NavigateNext = (0, _pure2.default)(NavigateNext);
NavigateNext.muiName = 'SvgIcon';

exports.default = NavigateNext;

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Collapse.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/transitions/Collapse.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    container: {
      height: 0,
      overflow: 'hidden',
      transition: theme.transitions.create('height')
    },
    entered: {
      height: 'auto'
    },
    wrapper: {
      // Hack to get children with a negative margin to not falsify the height computation.
      display: 'flex'
    },
    wrapperInner: {
      width: '100%'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content node to be collapsed.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: typeof String === 'function' ? __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").instanceOf(String) : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   * The default value is a `button`.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * The height of the container when collapsed.
   */
  collapsedHeight: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired,

  /**
   * Properties applied to the `Collapse` container.
   */
  containerProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   *
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  timeout: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]).isRequired,

  /**
   * @ignore
   */
  unmountOnExit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var Collapse = function (_React$Component) {
  (0, _inherits3.default)(Collapse, _React$Component);

  function Collapse() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Collapse);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Collapse.__proto__ || (0, _getPrototypeOf2.default)(Collapse)).call.apply(_ref, [this].concat(args))), _this), _this.wrapper = null, _this.autoTransitionDuration = undefined, _this.handleEnter = function (node) {
      node.style.height = _this.props.collapsedHeight;

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          timeout = _this$props.timeout,
          theme = _this$props.theme;

      var wrapperHeight = _this.wrapper ? _this.wrapper.clientHeight : 0;

      if (timeout === 'auto') {
        var duration2 = theme.transitions.getAutoHeightDuration(wrapperHeight);
        node.style.transitionDuration = duration2 + 'ms';
        _this.autoTransitionDuration = duration2;
      } else if (typeof timeout === 'number') {
        node.style.transitionDuration = timeout + 'ms';
      } else if (timeout && typeof timeout.enter === 'number') {
        node.style.transitionDuration = timeout.enter + 'ms';
      } else {
        // The propType will warn in this case.
      }

      node.style.height = wrapperHeight + 'px';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleEntered = function (node) {
      node.style.height = 'auto';

      if (_this.props.onEntered) {
        _this.props.onEntered(node);
      }
    }, _this.handleExit = function (node) {
      var wrapperHeight = _this.wrapper ? _this.wrapper.clientHeight : 0;
      node.style.height = wrapperHeight + 'px';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _this.handleExiting = function (node) {
      var _this$props2 = _this.props,
          timeout = _this$props2.timeout,
          theme = _this$props2.theme;

      var wrapperHeight = _this.wrapper ? _this.wrapper.clientHeight : 0;

      if (timeout === 'auto') {
        var duration2 = theme.transitions.getAutoHeightDuration(wrapperHeight);
        node.style.transitionDuration = duration2 + 'ms';
        _this.autoTransitionDuration = duration2;
      } else if (typeof timeout === 'number') {
        node.style.transitionDuration = timeout + 'ms';
      } else if (timeout && typeof timeout.exit === 'number') {
        node.style.transitionDuration = timeout.exit + 'ms';
      } else {
        // The propType will warn in this case.
      }

      node.style.height = _this.props.collapsedHeight;

      if (_this.props.onExiting) {
        _this.props.onExiting(node);
      }
    }, _this.addEndListener = function (node, next) {
      if (_this.props.timeout === 'auto') {
        setTimeout(next, _this.autoTransitionDuration || 0);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Collapse, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          ComponentProp = _props.component,
          collapsedHeight = _props.collapsedHeight,
          containerProps = _props.containerProps,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onEntered = _props.onEntered,
          onExit = _props.onExit,
          onExiting = _props.onExiting,
          style = _props.style,
          timeout = _props.timeout,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'classes', 'className', 'component', 'collapsedHeight', 'containerProps', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'style', 'timeout', 'theme']);


      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          onEntering: this.handleEntering,
          onEnter: this.handleEnter,
          onEntered: this.handleEntered,
          onExiting: this.handleExiting,
          onExit: this.handleExit,
          addEndListener: this.addEndListener,
          timeout: timeout === 'auto' ? null : timeout
        }, other),
        function (state, otherInner) {
          return _react2.default.createElement(
            ComponentProp,
            (0, _extends3.default)({
              className: (0, _classnames2.default)(classes.container, (0, _defineProperty3.default)({}, classes.entered, state === 'entered'), className),
              style: (0, _extends3.default)({}, style, {
                minHeight: collapsedHeight
              })
            }, otherInner),
            _react2.default.createElement(
              'div',
              {
                className: classes.wrapper,
                ref: function ref(node) {
                  _this2.wrapper = node;
                }
              },
              _react2.default.createElement(
                'div',
                { className: classes.wrapperInner },
                children
              )
            )
          );
        }
      );
    }
  }]);
  return Collapse;
}(_react2.default.Component);

Collapse.defaultProps = {
  appear: false,
  component: 'div',
  collapsedHeight: '0px',
  timeout: _transitions.duration.standard
};
exports.default = (0, _withStyles2.default)(styles, {
  withTheme: true,
  name: 'MuiCollapse'
})(Collapse);

/***/ }),

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/components/NavigationButtonNext.js":
/*!*******************************************************!*\
  !*** ./src/client/components/NavigationButtonNext.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _NavigateNext = __webpack_require__(/*! material-ui-icons/NavigateNext */ "./node_modules/material-ui-icons/NavigateNext.js");

var _NavigateNext2 = _interopRequireDefault(_NavigateNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This component is create to show naviagtion button to move next page
 *
 * @format
 */

var styles = function styles(theme) {
  var _root;

  return {
    root: (_root = {
      position: 'fixed',
      right: '2%',
      bottom: '4%'
    }, (0, _defineProperty3.default)(_root, theme.breakpoints.down('md'), {
      right: '2%',
      bottom: '2%'
    }), (0, _defineProperty3.default)(_root, theme.breakpoints.down('sm'), {
      right: '2%',
      bottom: '1%'
    }), _root)
  };
};

/**
 *
 * @param {object} classes
 * @param {path} to
 */
var NavigateButtonNext = function NavigateButtonNext(_ref) {
  var classes = _ref.classes,
      _ref$to = _ref.to,
      to = _ref$to === undefined ? '/' : _ref$to;
  return _react2.default.createElement(
    'div',
    { className: classes.root },
    _react2.default.createElement(
      _Button2.default,
      {
        fab: true,
        color: 'accent',
        component: _Link2.default,
        className: classes.button,
        raised: true,
        to: to
      },
      _react2.default.createElement(_NavigateNext2.default, null)
    )
  );
};

NavigateButtonNext.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  to: _propTypes2.default.string.isRequired
};

NavigateButtonNext.defaultProps = {
  to: '/'
};

exports.default = (0, _styles.withStyles)(styles)(NavigateButtonNext);

/***/ }),

/***/ "./src/client/pages/Services.js":
/*!**************************************!*\
  !*** ./src/client/pages/Services.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames5 = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames6 = _interopRequireDefault(_classnames5);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Collapse = __webpack_require__(/*! material-ui/transitions/Collapse */ "./node_modules/material-ui/transitions/Collapse.js");

var _Collapse2 = _interopRequireDefault(_Collapse);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _ExpandMore = __webpack_require__(/*! material-ui-icons/ExpandMore */ "./node_modules/material-ui-icons/ExpandMore.js");

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _NavigationButtonNext = __webpack_require__(/*! ../components/NavigationButtonNext */ "./src/client/components/NavigationButtonNext.js");

var _NavigationButtonNext2 = _interopRequireDefault(_NavigationButtonNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  var _title, _subheader, _media, _back;

  return {
    root: {},
    cardHeader: {
      textAlign: 'center'
    },
    card: {
      marginTop: '2%',
      backgroundColor: theme.palette.background.default
    },
    title: (_title = {
      fontWeight: theme.typography.fontWeightMedium,
      color: theme.palette.getContrastText(theme.palette.primary[700])
    }, (0, _defineProperty3.default)(_title, theme.breakpoints.down('sm'), {
      fontSize: 30
    }), (0, _defineProperty3.default)(_title, theme.breakpoints.down('xs'), {
      fontSize: 30
    }), _title),
    subheader: (_subheader = {
      color: theme.palette.getContrastText(theme.palette.primary[700])
    }, (0, _defineProperty3.default)(_subheader, theme.breakpoints.down('md'), {
      fontSize: 16
    }), (0, _defineProperty3.default)(_subheader, theme.breakpoints.down('sm'), {
      fontSize: 16
    }), (0, _defineProperty3.default)(_subheader, theme.breakpoints.down('xs'), {
      fontSize: 16
    }), _subheader),

    servicesTitle: {
      textAlign: 'center',
      height: '20vh',
      backgroundImage: 'url("https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg")',
      backgroundPosition: 'center',
      backgroundRepeat: 'no-repeat'
    },

    paragraph: {
      padding: '2%'
    },
    gridItem: {
      padding: '2%'
    },

    media: (_media = {
      width: '100%',
      position: 'relative',
      top: '-28px',
      opacity: 1,
      cursor: 'crosshair',
      transitionDuration: '0.3s',
      borderRadius: '8px 8px 8px 8px',
      boxShadow: '0 8px 11px 8px rgba(0, 0, 0, 0.2),' + ' 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
      '&:hover': {
        transform: 'scale(1.02)',
        opacity: 0.9
      }
    }, (0, _defineProperty3.default)(_media, theme.breakpoints.up('xl'), {
      marginBottom: '-85px',
      height: '170px'
    }), (0, _defineProperty3.default)(_media, theme.breakpoints.down('xl'), {
      marginBottom: '-85px',
      height: '170px'
    }), (0, _defineProperty3.default)(_media, theme.breakpoints.down('lg'), {
      marginBottom: '-85px',
      height: '170px'
    }), (0, _defineProperty3.default)(_media, theme.breakpoints.down('md'), {
      marginBottom: '-85px',
      height: '170px'
    }), (0, _defineProperty3.default)(_media, theme.breakpoints.down('sm'), {
      marginBottom: '-85px',
      height: '150px'
    }), (0, _defineProperty3.default)(_media, theme.breakpoints.down('xs'), {
      marginBottom: '-85px',
      height: '150px'
    }), _media),

    expand: {
      transform: 'rotate(0deg)',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })
    },
    transform: 'rotate(180deg)',
    expandOpen: {
      // height: '2px',
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    back: (_back = {
      backgroundColor: theme.palette.primary[400],
      borderRadius: '50%',
      position: 'relative'
    }, (0, _defineProperty3.default)(_back, theme.breakpoints.up('xl'), {
      marginTop: '-10%'
    }), (0, _defineProperty3.default)(_back, theme.breakpoints.down('xl'), {
      marginTop: '-5%'
    }), (0, _defineProperty3.default)(_back, theme.breakpoints.down('lg'), {
      marginTop: '-3%'
    }), (0, _defineProperty3.default)(_back, theme.breakpoints.down('md'), {
      marginTop: '-5%'
    }), (0, _defineProperty3.default)(_back, theme.breakpoints.down('sm'), {
      marginTop: '1%'
    }), (0, _defineProperty3.default)(_back, theme.breakpoints.down('xs'), {
      marginTop: '-10%'
    }), _back)
  };
}; /**
    * This component contains discription of services of mayash
    *
    * @format
    */

var Services = function (_React$Component) {
  (0, _inherits3.default)(Services, _React$Component);

  function Services(props) {
    (0, _classCallCheck3.default)(this, Services);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Services.__proto__ || (0, _getPrototypeOf2.default)(Services)).call(this, props));

    _this.handleExpandClick = function () {
      return _this.setState({ expanded: !_this.state.expanded });
    };

    _this.secondHandleExpandClick = function () {
      return _this.setState({ expand: !_this.state.expand });
    };

    _this.thirdHandleExpandClick = function () {
      return _this.setState({ onexpand: !_this.state.onexpand });
    };

    _this.fourthHandleExpandClick = function () {
      return _this.setState({ onexpanded: !_this.state.onexpanded });
    };

    _this.state = {
      expanded: false,
      expand: false,
      onexpand: false,
      onexpanded: false
    };
    return _this;
  }

  (0, _createClass3.default)(Services, [{
    key: 'render',
    value: function render() {
      var classes = this.props.classes;


      return _react2.default.createElement(
        _Grid2.default,
        {
          container: true,
          spacing: 0,
          justify: 'space-around',
          className: classes.root
        },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 12, md: 12, lg: 12 },
          _react2.default.createElement(
            _Card2.default,
            { raised: true },
            _react2.default.createElement(_Card.CardHeader, {
              title: _react2.default.createElement(
                _Typography2.default,
                { className: classes.title, type: 'display3' },
                'SERVICES'
              ),
              subheader: _react2.default.createElement(
                _Typography2.default,
                { className: classes.subheader, type: 'headline' },
                'Our products and services will help students as well as teachers to improve their knowledge and skills.'
              ),
              className: (0, _classnames6.default)(classes.cardHeader, classes.servicesTitle)
            })
          )
        ),
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 6, md: 6, lg: 3, className: classes.gridItem },
          _react2.default.createElement(
            _Card2.default,
            { raised: true, className: classes.card },
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement('img', {
                className: classes.media,
                src: 'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg',
                alt: 'hia'
              })
            ),
            _react2.default.createElement(
              _Card.CardActions,
              { disableActionSpacing: true },
              _react2.default.createElement('div', { className: classes.flexGrow }),
              _react2.default.createElement(
                'div',
                { className: classes.back },
                _react2.default.createElement(
                  _Button2.default,
                  {
                    className: (0, _classnames6.default)(classes.expand, (0, _defineProperty3.default)({}, classes.expandOpen, this.state.expanded)),
                    onClick: this.handleExpandClick,
                    'aria-expanded': this.state.expanded,
                    'aria-label': 'Show more',
                    fab: true,
                    color: 'accent',
                    raised: true
                  },
                  _react2.default.createElement(_ExpandMore2.default, null)
                )
              ),
              _react2.default.createElement('div', { className: classes.flexGrow })
            ),
            _react2.default.createElement(
              _Collapse2.default,
              { 'in': this.state.expanded, timeout: 'auto', unmountOnExit: true },
              _react2.default.createElement(
                _Card.CardContent,
                { className: classes.content },
                _react2.default.createElement(
                  _Typography2.default,
                  { type: 'body2' },
                  _react2.default.createElement(
                    'ol',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'Get organised Notes.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Doubts discussion Channel'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Connect with your alumni'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Get latest updates in your respective field'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Test yourself.'
                    )
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 6, md: 6, lg: 3, className: classes.gridItem },
          _react2.default.createElement(
            _Card2.default,
            { raised: true, className: classes.card },
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement('img', {
                className: classes.media,
                src: 'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg',
                alt: 'hia'
              })
            ),
            _react2.default.createElement(
              _Card.CardActions,
              { disableActionSpacing: true },
              _react2.default.createElement('div', { className: classes.flexGrow }),
              _react2.default.createElement(
                'div',
                { className: classes.back },
                _react2.default.createElement(
                  _Button2.default,
                  {
                    className: (0, _classnames6.default)(classes.expand, (0, _defineProperty3.default)({}, classes.expandOpen, this.state.expand)),
                    onClick: this.secondHandleExpandClick,
                    'aria-expanded': this.state.expand,
                    'aria-label': 'Show more',
                    fab: true,
                    color: 'accent',
                    raised: true
                  },
                  _react2.default.createElement(_ExpandMore2.default, null)
                )
              ),
              _react2.default.createElement('div', { className: classes.flexGrow })
            ),
            _react2.default.createElement(
              _Collapse2.default,
              { 'in': this.state.expand, timeout: 'auto', unmountOnExit: true },
              _react2.default.createElement(
                _Card.CardContent,
                null,
                _react2.default.createElement(
                  _Typography2.default,
                  { type: 'body2' },
                  _react2.default.createElement(
                    'ol',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'Connect with your students.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Doubts discussion Channel to clear Doubts of your students.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Share your knowledge with not only your students but with whole world.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Get latest updates in your respective field.'
                    )
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 6, md: 6, lg: 3, className: classes.gridItem },
          _react2.default.createElement(
            _Card2.default,
            { raised: true, className: classes.card },
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement('img', {
                className: classes.media,
                src: 'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg',
                alt: 'hia'
              })
            ),
            _react2.default.createElement(
              _Card.CardActions,
              { disableActionSpacing: true },
              _react2.default.createElement('div', { className: classes.flexGrow }),
              _react2.default.createElement(
                'div',
                { className: classes.back },
                _react2.default.createElement(
                  _Button2.default,
                  {
                    className: (0, _classnames6.default)(classes.expand, (0, _defineProperty3.default)({}, classes.expandOpen, this.state.onexpand)),
                    onClick: this.thirdHandleExpandClick,
                    'aria-expanded': this.state.onexpand,
                    'aria-label': 'Show more',
                    fab: true,
                    color: 'accent',
                    raised: true
                  },
                  _react2.default.createElement(_ExpandMore2.default, null)
                )
              ),
              _react2.default.createElement('div', { className: classes.flexGrow })
            ),
            _react2.default.createElement(
              _Collapse2.default,
              { 'in': this.state.onexpand, timeout: 'auto', unmountOnExit: true },
              _react2.default.createElement(
                _Card.CardContent,
                null,
                _react2.default.createElement(
                  _Typography2.default,
                  { type: 'body2' },
                  _react2.default.createElement(
                    'ol',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'Connect with your students.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Doubts discussion Channel to clear Doubts of your students.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Share your knowledge with not only your students but with whole world.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Get latest updates in your respective field.'
                    )
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 6, md: 6, lg: 3, className: classes.gridItem },
          _react2.default.createElement(
            _Card2.default,
            { raised: true, className: classes.card },
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement('img', {
                className: classes.media,
                src: 'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg',
                alt: 'hia'
              })
            ),
            _react2.default.createElement(
              _Card.CardActions,
              { disableActionSpacing: true },
              _react2.default.createElement('div', { className: classes.flexGrow }),
              _react2.default.createElement(
                'div',
                { className: classes.back },
                _react2.default.createElement(
                  _Button2.default,
                  {
                    className: (0, _classnames6.default)(classes.expand, (0, _defineProperty3.default)({}, classes.expandOpen, this.state.onexpanded)),
                    onClick: this.fourthHandleExpandClick,
                    'aria-expanded': this.state.onexpanded,
                    'aria-label': 'Show more',
                    fab: true,
                    color: 'accent',
                    raised: true
                  },
                  _react2.default.createElement(_ExpandMore2.default, null)
                )
              ),
              _react2.default.createElement('div', { className: classes.flexGrow })
            ),
            _react2.default.createElement(
              _Collapse2.default,
              { 'in': this.state.onexpanded, timeout: 'auto', unmountOnExit: true },
              _react2.default.createElement(
                _Card.CardContent,
                null,
                _react2.default.createElement(
                  _Typography2.default,
                  { type: 'body2' },
                  _react2.default.createElement(
                    'ol',
                    null,
                    _react2.default.createElement(
                      'li',
                      null,
                      'Get updated knowledge on latest technologies.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'Stay updated with industries trends.'
                    ),
                    _react2.default.createElement(
                      'li',
                      null,
                      'See growth of your employes.'
                    )
                  )
                )
              )
            )
          )
        ),
        _react2.default.createElement(_NavigationButtonNext2.default, { to: '/pricing' })
      );
    }
  }]);
  return Services;
}(_react2.default.Component);

Services.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(Services);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRXhwYW5kTW9yZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvTmF2aWdhdGVOZXh0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0NvbGxhcHNlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWFjdC1yb3V0ZXItZG9tL0xpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvTmF2aWdhdGlvbkJ1dHRvbk5leHQuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9wYWdlcy9TZXJ2aWNlcy5qcyJdLCJuYW1lcyI6WyJzdHlsZXMiLCJ0aGVtZSIsInJvb3QiLCJwb3NpdGlvbiIsInJpZ2h0IiwiYm90dG9tIiwiYnJlYWtwb2ludHMiLCJkb3duIiwiTmF2aWdhdGVCdXR0b25OZXh0IiwiY2xhc3NlcyIsInRvIiwiYnV0dG9uIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsInN0cmluZyIsImRlZmF1bHRQcm9wcyIsImNhcmRIZWFkZXIiLCJ0ZXh0QWxpZ24iLCJjYXJkIiwibWFyZ2luVG9wIiwiYmFja2dyb3VuZENvbG9yIiwicGFsZXR0ZSIsImJhY2tncm91bmQiLCJkZWZhdWx0IiwidGl0bGUiLCJmb250V2VpZ2h0IiwidHlwb2dyYXBoeSIsImZvbnRXZWlnaHRNZWRpdW0iLCJjb2xvciIsImdldENvbnRyYXN0VGV4dCIsInByaW1hcnkiLCJmb250U2l6ZSIsInN1YmhlYWRlciIsInNlcnZpY2VzVGl0bGUiLCJoZWlnaHQiLCJiYWNrZ3JvdW5kSW1hZ2UiLCJiYWNrZ3JvdW5kUG9zaXRpb24iLCJiYWNrZ3JvdW5kUmVwZWF0IiwicGFyYWdyYXBoIiwicGFkZGluZyIsImdyaWRJdGVtIiwibWVkaWEiLCJ3aWR0aCIsInRvcCIsIm9wYWNpdHkiLCJjdXJzb3IiLCJ0cmFuc2l0aW9uRHVyYXRpb24iLCJib3JkZXJSYWRpdXMiLCJib3hTaGFkb3ciLCJ0cmFuc2Zvcm0iLCJ1cCIsIm1hcmdpbkJvdHRvbSIsImV4cGFuZCIsInRyYW5zaXRpb24iLCJ0cmFuc2l0aW9ucyIsImNyZWF0ZSIsImR1cmF0aW9uIiwic2hvcnRlc3QiLCJleHBhbmRPcGVuIiwiZmxleEdyb3ciLCJmbGV4IiwiYmFjayIsIlNlcnZpY2VzIiwicHJvcHMiLCJoYW5kbGVFeHBhbmRDbGljayIsInNldFN0YXRlIiwiZXhwYW5kZWQiLCJzdGF0ZSIsInNlY29uZEhhbmRsZUV4cGFuZENsaWNrIiwidGhpcmRIYW5kbGVFeHBhbmRDbGljayIsIm9uZXhwYW5kIiwiZm91cnRoSGFuZGxlRXhwYW5kQ2xpY2siLCJvbmV4cGFuZGVkIiwiY29udGVudCIsIkNvbXBvbmVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9EQUFvRDs7QUFFdEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNEQUFzRDs7QUFFeEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGOztBQUU5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHFCQUFxQixXOzs7Ozs7Ozs7Ozs7O0FDbEwxRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7O0FDaEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0dBQXNHO0FBQ3RHLDhDQUE4QztBQUM5QztBQUNBLGVBQWU7QUFDZixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGlCQUFpQixrQ0FBa0M7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsWTs7Ozs7Ozs7Ozs7OztBQzFWRDs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsOENBQThDLGlCQUFpQixxQkFBcUIsb0NBQW9DLDZEQUE2RCxvQkFBb0IsRUFBRSxlQUFlOztBQUUxTixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnRkFBZ0Y7O0FBRWhGOztBQUVBLGdGQUFnRixlQUFlOztBQUUvRix5REFBeUQsVUFBVSx1REFBdUQ7QUFDMUg7O0FBRUE7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBLHVCOzs7Ozs7Ozs7Ozs7O0FDN0dBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7Ozs7QUNyQkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2ZBOztBQUVBOztBQUVBLG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUTtBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLDhEOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ1pBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix5Qzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUN6REE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixVQUFVLHFCQUFxQjtBQUM1RDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1JBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQUNBOzs7O0FBRUE7Ozs7OztBQWJBOzs7Ozs7QUFlQSxJQUFNQSxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRDtBQUFBOztBQUFBLFNBQVk7QUFDekJDO0FBQ0VDLGdCQUFVLE9BRFo7QUFFRUMsYUFBTyxJQUZUO0FBR0VDLGNBQVE7QUFIViw0Q0FJR0osTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FKSCxFQUlrQztBQUM5QkgsYUFBTyxJQUR1QjtBQUU5QkMsY0FBUTtBQUZzQixLQUpsQyx3Q0FRR0osTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FSSCxFQVFrQztBQUM5QkgsYUFBTyxJQUR1QjtBQUU5QkMsY0FBUTtBQUZzQixLQVJsQztBQUR5QixHQUFaO0FBQUEsQ0FBZjs7QUFnQkE7Ozs7O0FBS0EsSUFBTUcscUJBQXFCLFNBQXJCQSxrQkFBcUI7QUFBQSxNQUFHQyxPQUFILFFBQUdBLE9BQUg7QUFBQSxxQkFBWUMsRUFBWjtBQUFBLE1BQVlBLEVBQVosMkJBQWlCLEdBQWpCO0FBQUEsU0FDekI7QUFBQTtBQUFBLE1BQUssV0FBV0QsUUFBUVAsSUFBeEI7QUFDRTtBQUFBO0FBQUE7QUFDRSxpQkFERjtBQUVFLGVBQU0sUUFGUjtBQUdFLGlDQUhGO0FBSUUsbUJBQVdPLFFBQVFFLE1BSnJCO0FBS0Usb0JBTEY7QUFNRSxZQUFJRDtBQU5OO0FBUUU7QUFSRjtBQURGLEdBRHlCO0FBQUEsQ0FBM0I7O0FBZUFGLG1CQUFtQkksU0FBbkIsR0FBK0I7QUFDN0JILFdBQVMsb0JBQVVJLE1BQVYsQ0FBaUJDLFVBREc7QUFFN0JKLE1BQUksb0JBQVVLLE1BQVYsQ0FBaUJEO0FBRlEsQ0FBL0I7O0FBS0FOLG1CQUFtQlEsWUFBbkIsR0FBa0M7QUFDaENOLE1BQUk7QUFENEIsQ0FBbEM7O2tCQUllLHdCQUFXVixNQUFYLEVBQW1CUSxrQkFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3REZjs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNUixTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRDtBQUFBOztBQUFBLFNBQVk7QUFDekJDLFVBQU0sRUFEbUI7QUFFekJlLGdCQUFZO0FBQ1ZDLGlCQUFXO0FBREQsS0FGYTtBQUt6QkMsVUFBTTtBQUNKQyxpQkFBVyxJQURQO0FBRUpDLHVCQUFpQnBCLE1BQU1xQixPQUFOLENBQWNDLFVBQWQsQ0FBeUJDO0FBRnRDLEtBTG1CO0FBU3pCQztBQUNFQyxrQkFBWXpCLE1BQU0wQixVQUFOLENBQWlCQyxnQkFEL0I7QUFFRUMsYUFBTzVCLE1BQU1xQixPQUFOLENBQWNRLGVBQWQsQ0FBOEI3QixNQUFNcUIsT0FBTixDQUFjUyxPQUFkLENBQXNCLEdBQXRCLENBQTlCO0FBRlQsNkNBR0c5QixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQUhILEVBR2tDO0FBQzlCeUIsZ0JBQVU7QUFEb0IsS0FIbEMseUNBTUcvQixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQU5ILEVBTWtDO0FBQzlCeUIsZ0JBQVU7QUFEb0IsS0FObEMsVUFUeUI7QUFtQnpCQztBQUNFSixhQUFPNUIsTUFBTXFCLE9BQU4sQ0FBY1EsZUFBZCxDQUE4QjdCLE1BQU1xQixPQUFOLENBQWNTLE9BQWQsQ0FBc0IsR0FBdEIsQ0FBOUI7QUFEVCxpREFFRzlCLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBRkgsRUFFa0M7QUFDOUJ5QixnQkFBVTtBQURvQixLQUZsQyw2Q0FLRy9CLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBTEgsRUFLa0M7QUFDOUJ5QixnQkFBVTtBQURvQixLQUxsQyw2Q0FRRy9CLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBUkgsRUFRa0M7QUFDOUJ5QixnQkFBVTtBQURvQixLQVJsQyxjQW5CeUI7O0FBZ0N6QkUsbUJBQWU7QUFDYmhCLGlCQUFXLFFBREU7QUFFYmlCLGNBQVEsTUFGSztBQUdiQyx1QkFDRSwrRUFKVztBQUtiQywwQkFBb0IsUUFMUDtBQU1iQyx3QkFBa0I7QUFOTCxLQWhDVTs7QUF5Q3pCQyxlQUFXO0FBQ1RDLGVBQVM7QUFEQSxLQXpDYztBQTRDekJDLGNBQVU7QUFDUkQsZUFBUztBQURELEtBNUNlOztBQWdEekJFO0FBQ0VDLGFBQU8sTUFEVDtBQUVFeEMsZ0JBQVUsVUFGWjtBQUdFeUMsV0FBSyxPQUhQO0FBSUVDLGVBQVMsQ0FKWDtBQUtFQyxjQUFRLFdBTFY7QUFNRUMsMEJBQW9CLE1BTnRCO0FBT0VDLG9CQUFjLGlCQVBoQjtBQVFFQyxpQkFDRSx1Q0FDQSxtQ0FWSjtBQVdFLGlCQUFXO0FBQ1RDLG1CQUFXLGFBREY7QUFFVEwsaUJBQVM7QUFGQTtBQVhiLDZDQWVHNUMsTUFBTUssV0FBTixDQUFrQjZDLEVBQWxCLENBQXFCLElBQXJCLENBZkgsRUFlZ0M7QUFDNUJDLG9CQUFjLE9BRGM7QUFFNUJqQixjQUFRO0FBRm9CLEtBZmhDLHlDQW1CR2xDLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBbkJILEVBbUJrQztBQUM5QjZDLG9CQUFjLE9BRGdCO0FBRTlCakIsY0FBUTtBQUZzQixLQW5CbEMseUNBdUJHbEMsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0F2QkgsRUF1QmtDO0FBQzlCNkMsb0JBQWMsT0FEZ0I7QUFFOUJqQixjQUFRO0FBRnNCLEtBdkJsQyx5Q0EyQkdsQyxNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQTNCSCxFQTJCa0M7QUFDOUI2QyxvQkFBYyxPQURnQjtBQUU5QmpCLGNBQVE7QUFGc0IsS0EzQmxDLHlDQStCR2xDLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBL0JILEVBK0JrQztBQUM5QjZDLG9CQUFjLE9BRGdCO0FBRTlCakIsY0FBUTtBQUZzQixLQS9CbEMseUNBbUNHbEMsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FuQ0gsRUFtQ2tDO0FBQzlCNkMsb0JBQWMsT0FEZ0I7QUFFOUJqQixjQUFRO0FBRnNCLEtBbkNsQyxVQWhEeUI7O0FBeUZ6QmtCLFlBQVE7QUFDTkgsaUJBQVcsY0FETDtBQUVOSSxrQkFBWXJELE1BQU1zRCxXQUFOLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QixFQUFzQztBQUNoREMsa0JBQVV4RCxNQUFNc0QsV0FBTixDQUFrQkUsUUFBbEIsQ0FBMkJDO0FBRFcsT0FBdEM7QUFGTixLQXpGaUI7QUErRnpCUixlQUFXLGdCQS9GYztBQWdHekJTLGdCQUFZO0FBQ1Y7QUFEVSxLQWhHYTtBQW1HekJDLGNBQVU7QUFDUkMsWUFBTTtBQURFLEtBbkdlO0FBc0d6QkM7QUFDRXpDLHVCQUFpQnBCLE1BQU1xQixPQUFOLENBQWNTLE9BQWQsQ0FBc0IsR0FBdEIsQ0FEbkI7QUFFRWlCLG9CQUFjLEtBRmhCO0FBR0U3QyxnQkFBVTtBQUhaLDRDQUlHRixNQUFNSyxXQUFOLENBQWtCNkMsRUFBbEIsQ0FBcUIsSUFBckIsQ0FKSCxFQUlnQztBQUM1Qi9CLGlCQUFXO0FBRGlCLEtBSmhDLHdDQU9HbkIsTUFBTUssV0FBTixDQUFrQkMsSUFBbEIsQ0FBdUIsSUFBdkIsQ0FQSCxFQU9rQztBQUM5QmEsaUJBQVc7QUFEbUIsS0FQbEMsd0NBVUduQixNQUFNSyxXQUFOLENBQWtCQyxJQUFsQixDQUF1QixJQUF2QixDQVZILEVBVWtDO0FBQzlCYSxpQkFBVztBQURtQixLQVZsQyx3Q0FhR25CLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBYkgsRUFha0M7QUFDOUJhLGlCQUFXO0FBRG1CLEtBYmxDLHdDQWdCR25CLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBaEJILEVBZ0JrQztBQUM5QmEsaUJBQVc7QUFEbUIsS0FoQmxDLHdDQW1CR25CLE1BQU1LLFdBQU4sQ0FBa0JDLElBQWxCLENBQXVCLElBQXZCLENBbkJILEVBbUJrQztBQUM5QmEsaUJBQVc7QUFEbUIsS0FuQmxDO0FBdEd5QixHQUFaO0FBQUEsQ0FBZixDLENBdEJBOzs7Ozs7SUFxSk0yQyxROzs7QUFDSixvQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLDBJQUNYQSxLQURXOztBQUFBLFVBVW5CQyxpQkFWbUIsR0FVQztBQUFBLGFBQU0sTUFBS0MsUUFBTCxDQUFjLEVBQUVDLFVBQVUsQ0FBQyxNQUFLQyxLQUFMLENBQVdELFFBQXhCLEVBQWQsQ0FBTjtBQUFBLEtBVkQ7O0FBQUEsVUFZbkJFLHVCQVptQixHQVlPO0FBQUEsYUFBTSxNQUFLSCxRQUFMLENBQWMsRUFBRWIsUUFBUSxDQUFDLE1BQUtlLEtBQUwsQ0FBV2YsTUFBdEIsRUFBZCxDQUFOO0FBQUEsS0FaUDs7QUFBQSxVQWNuQmlCLHNCQWRtQixHQWNNO0FBQUEsYUFDdkIsTUFBS0osUUFBTCxDQUFjLEVBQUVLLFVBQVUsQ0FBQyxNQUFLSCxLQUFMLENBQVdHLFFBQXhCLEVBQWQsQ0FEdUI7QUFBQSxLQWROOztBQUFBLFVBaUJuQkMsdUJBakJtQixHQWlCTztBQUFBLGFBQ3hCLE1BQUtOLFFBQUwsQ0FBYyxFQUFFTyxZQUFZLENBQUMsTUFBS0wsS0FBTCxDQUFXSyxVQUExQixFQUFkLENBRHdCO0FBQUEsS0FqQlA7O0FBRWpCLFVBQUtMLEtBQUwsR0FBYTtBQUNYRCxnQkFBVSxLQURDO0FBRVhkLGNBQVEsS0FGRztBQUdYa0IsZ0JBQVUsS0FIQztBQUlYRSxrQkFBWTtBQUpELEtBQWI7QUFGaUI7QUFRbEI7Ozs7NkJBWVE7QUFBQSxVQUNDaEUsT0FERCxHQUNhLEtBQUt1RCxLQURsQixDQUNDdkQsT0FERDs7O0FBR1AsYUFDRTtBQUFBO0FBQUE7QUFDRSx5QkFERjtBQUVFLG1CQUFTLENBRlg7QUFHRSxtQkFBUSxjQUhWO0FBSUUscUJBQVdBLFFBQVFQO0FBSnJCO0FBTUU7QUFBQTtBQUFBLFlBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLEVBQXZCLEVBQTJCLElBQUksRUFBL0IsRUFBbUMsSUFBSSxFQUF2QztBQUNFO0FBQUE7QUFBQSxjQUFNLFlBQU47QUFDRTtBQUNFLHFCQUNFO0FBQUE7QUFBQSxrQkFBWSxXQUFXTyxRQUFRZ0IsS0FBL0IsRUFBc0MsTUFBSyxVQUEzQztBQUFBO0FBQUEsZUFGSjtBQU1FLHlCQUNFO0FBQUE7QUFBQSxrQkFBWSxXQUFXaEIsUUFBUXdCLFNBQS9CLEVBQTBDLE1BQUssVUFBL0M7QUFBQTtBQUFBLGVBUEo7QUFZRSx5QkFBVywwQkFBV3hCLFFBQVFRLFVBQW5CLEVBQStCUixRQUFReUIsYUFBdkM7QUFaYjtBQURGO0FBREYsU0FORjtBQXdCRTtBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLElBQUksQ0FBdkIsRUFBMEIsSUFBSSxDQUE5QixFQUFpQyxJQUFJLENBQXJDLEVBQXdDLFdBQVd6QixRQUFRZ0MsUUFBM0Q7QUFDRTtBQUFBO0FBQUEsY0FBTSxZQUFOLEVBQWEsV0FBV2hDLFFBQVFVLElBQWhDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRSwyQkFBV1YsUUFBUWlDLEtBRHJCO0FBRUUscUJBQ0Usd0VBSEo7QUFLRSxxQkFBSztBQUxQO0FBREYsYUFERjtBQVVFO0FBQUE7QUFBQSxnQkFBYSwwQkFBYjtBQUNFLHFEQUFLLFdBQVdqQyxRQUFRbUQsUUFBeEIsR0FERjtBQUVFO0FBQUE7QUFBQSxrQkFBSyxXQUFXbkQsUUFBUXFELElBQXhCO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsK0JBQVcsMEJBQVdyRCxRQUFRNEMsTUFBbkIsb0NBQ1I1QyxRQUFRa0QsVUFEQSxFQUNhLEtBQUtTLEtBQUwsQ0FBV0QsUUFEeEIsRUFEYjtBQUlFLDZCQUFTLEtBQUtGLGlCQUpoQjtBQUtFLHFDQUFlLEtBQUtHLEtBQUwsQ0FBV0QsUUFMNUI7QUFNRSxrQ0FBVyxXQU5iO0FBT0UsNkJBUEY7QUFRRSwyQkFBTSxRQVJSO0FBU0U7QUFURjtBQVdFO0FBWEY7QUFERixlQUZGO0FBaUJFLHFEQUFLLFdBQVcxRCxRQUFRbUQsUUFBeEI7QUFqQkYsYUFWRjtBQTZCRTtBQUFBO0FBQUEsZ0JBQVUsTUFBSSxLQUFLUSxLQUFMLENBQVdELFFBQXpCLEVBQW1DLFNBQVEsTUFBM0MsRUFBa0QsbUJBQWxEO0FBQ0U7QUFBQTtBQUFBLGtCQUFhLFdBQVcxRCxRQUFRaUUsT0FBaEM7QUFDRTtBQUFBO0FBQUEsb0JBQVksTUFBSyxPQUFqQjtBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREY7QUFFRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZGO0FBR0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFIRjtBQUlFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSkY7QUFLRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEY7QUFERjtBQURGO0FBREY7QUE3QkY7QUFERixTQXhCRjtBQXFFRTtBQUFBO0FBQUEsWUFBTSxVQUFOLEVBQVcsSUFBSSxFQUFmLEVBQW1CLElBQUksQ0FBdkIsRUFBMEIsSUFBSSxDQUE5QixFQUFpQyxJQUFJLENBQXJDLEVBQXdDLFdBQVdqRSxRQUFRZ0MsUUFBM0Q7QUFDRTtBQUFBO0FBQUEsY0FBTSxZQUFOLEVBQWEsV0FBV2hDLFFBQVFVLElBQWhDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRSwyQkFBV1YsUUFBUWlDLEtBRHJCO0FBRUUscUJBQ0Usd0VBSEo7QUFLRSxxQkFBSztBQUxQO0FBREYsYUFERjtBQVVFO0FBQUE7QUFBQSxnQkFBYSwwQkFBYjtBQUNFLHFEQUFLLFdBQVdqQyxRQUFRbUQsUUFBeEIsR0FERjtBQUVFO0FBQUE7QUFBQSxrQkFBSyxXQUFXbkQsUUFBUXFELElBQXhCO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsK0JBQVcsMEJBQVdyRCxRQUFRNEMsTUFBbkIsb0NBQ1I1QyxRQUFRa0QsVUFEQSxFQUNhLEtBQUtTLEtBQUwsQ0FBV2YsTUFEeEIsRUFEYjtBQUlFLDZCQUFTLEtBQUtnQix1QkFKaEI7QUFLRSxxQ0FBZSxLQUFLRCxLQUFMLENBQVdmLE1BTDVCO0FBTUUsa0NBQVcsV0FOYjtBQU9FLDZCQVBGO0FBUUUsMkJBQU0sUUFSUjtBQVNFO0FBVEY7QUFXRTtBQVhGO0FBREYsZUFGRjtBQWlCRSxxREFBSyxXQUFXNUMsUUFBUW1ELFFBQXhCO0FBakJGLGFBVkY7QUE2QkU7QUFBQTtBQUFBLGdCQUFVLE1BQUksS0FBS1EsS0FBTCxDQUFXZixNQUF6QixFQUFpQyxTQUFRLE1BQXpDLEVBQWdELG1CQUFoRDtBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxvQkFBWSxNQUFLLE9BQWpCO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRkY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQU5GO0FBVUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVZGO0FBREY7QUFERjtBQURGO0FBN0JGO0FBREYsU0FyRUY7QUF1SEU7QUFBQTtBQUFBLFlBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLENBQXZCLEVBQTBCLElBQUksQ0FBOUIsRUFBaUMsSUFBSSxDQUFyQyxFQUF3QyxXQUFXNUMsUUFBUWdDLFFBQTNEO0FBQ0U7QUFBQTtBQUFBLGNBQU0sWUFBTixFQUFhLFdBQVdoQyxRQUFRVSxJQUFoQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQ0UsMkJBQVdWLFFBQVFpQyxLQURyQjtBQUVFLHFCQUNFLHdFQUhKO0FBS0UscUJBQUs7QUFMUDtBQURGLGFBREY7QUFVRTtBQUFBO0FBQUEsZ0JBQWEsMEJBQWI7QUFDRSxxREFBSyxXQUFXakMsUUFBUW1ELFFBQXhCLEdBREY7QUFFRTtBQUFBO0FBQUEsa0JBQUssV0FBV25ELFFBQVFxRCxJQUF4QjtBQUNFO0FBQUE7QUFBQTtBQUNFLCtCQUFXLDBCQUFXckQsUUFBUTRDLE1BQW5CLG9DQUNSNUMsUUFBUWtELFVBREEsRUFDYSxLQUFLUyxLQUFMLENBQVdHLFFBRHhCLEVBRGI7QUFJRSw2QkFBUyxLQUFLRCxzQkFKaEI7QUFLRSxxQ0FBZSxLQUFLRixLQUFMLENBQVdHLFFBTDVCO0FBTUUsa0NBQVcsV0FOYjtBQU9FLDZCQVBGO0FBUUUsMkJBQU0sUUFSUjtBQVNFO0FBVEY7QUFXRTtBQVhGO0FBREYsZUFGRjtBQWlCRSxxREFBSyxXQUFXOUQsUUFBUW1ELFFBQXhCO0FBakJGLGFBVkY7QUE2QkU7QUFBQTtBQUFBLGdCQUFVLE1BQUksS0FBS1EsS0FBTCxDQUFXRyxRQUF6QixFQUFtQyxTQUFRLE1BQTNDLEVBQWtELG1CQUFsRDtBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxvQkFBWSxNQUFLLE9BQWpCO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRkY7QUFNRTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQU5GO0FBVUU7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVZGO0FBREY7QUFERjtBQURGO0FBN0JGO0FBREYsU0F2SEY7QUF5S0U7QUFBQTtBQUFBLFlBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLENBQXZCLEVBQTBCLElBQUksQ0FBOUIsRUFBaUMsSUFBSSxDQUFyQyxFQUF3QyxXQUFXOUQsUUFBUWdDLFFBQTNEO0FBQ0U7QUFBQTtBQUFBLGNBQU0sWUFBTixFQUFhLFdBQVdoQyxRQUFRVSxJQUFoQztBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQ0UsMkJBQVdWLFFBQVFpQyxLQURyQjtBQUVFLHFCQUNFLHdFQUhKO0FBS0UscUJBQUs7QUFMUDtBQURGLGFBREY7QUFVRTtBQUFBO0FBQUEsZ0JBQWEsMEJBQWI7QUFDRSxxREFBSyxXQUFXakMsUUFBUW1ELFFBQXhCLEdBREY7QUFFRTtBQUFBO0FBQUEsa0JBQUssV0FBV25ELFFBQVFxRCxJQUF4QjtBQUNFO0FBQUE7QUFBQTtBQUNFLCtCQUFXLDBCQUFXckQsUUFBUTRDLE1BQW5CLG9DQUNSNUMsUUFBUWtELFVBREEsRUFDYSxLQUFLUyxLQUFMLENBQVdLLFVBRHhCLEVBRGI7QUFJRSw2QkFBUyxLQUFLRCx1QkFKaEI7QUFLRSxxQ0FBZSxLQUFLSixLQUFMLENBQVdLLFVBTDVCO0FBTUUsa0NBQVcsV0FOYjtBQU9FLDZCQVBGO0FBUUUsMkJBQU0sUUFSUjtBQVNFO0FBVEY7QUFXRTtBQVhGO0FBREYsZUFGRjtBQWlCRSxxREFBSyxXQUFXaEUsUUFBUW1ELFFBQXhCO0FBakJGLGFBVkY7QUE2QkU7QUFBQTtBQUFBLGdCQUFVLE1BQUksS0FBS1EsS0FBTCxDQUFXSyxVQUF6QixFQUFxQyxTQUFRLE1BQTdDLEVBQW9ELG1CQUFwRDtBQUNFO0FBQUE7QUFBQTtBQUNFO0FBQUE7QUFBQSxvQkFBWSxNQUFLLE9BQWpCO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERjtBQUVFO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRkY7QUFHRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSEY7QUFERjtBQURGO0FBREY7QUE3QkY7QUFERixTQXpLRjtBQW9ORSx3RUFBc0IsSUFBSSxVQUExQjtBQXBORixPQURGO0FBd05EOzs7RUFoUG9CLGdCQUFNRSxTOztBQW1QN0JaLFNBQVNuRCxTQUFULEdBQXFCO0FBQ25CSCxXQUFTLG9CQUFVSSxNQUFWLENBQWlCQztBQURQLENBQXJCOztrQkFJZSwwQkFBV2QsTUFBWCxFQUFtQitELFFBQW5CLEMiLCJmaWxlIjoiMTEuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTYuNTkgOC41OUwxMiAxMy4xNyA3LjQxIDguNTkgNiAxMGw2IDYgNi02eicgfSk7XG5cbnZhciBFeHBhbmRNb3JlID0gZnVuY3Rpb24gRXhwYW5kTW9yZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRXhwYW5kTW9yZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRXhwYW5kTW9yZSk7XG5FeHBhbmRNb3JlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEV4cGFuZE1vcmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRXhwYW5kTW9yZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRXhwYW5kTW9yZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIDM4IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTAgNkw4LjU5IDcuNDEgMTMuMTcgMTJsLTQuNTggNC41OUwxMCAxOGw2LTZ6JyB9KTtcblxudmFyIE5hdmlnYXRlTmV4dCA9IGZ1bmN0aW9uIE5hdmlnYXRlTmV4dChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuTmF2aWdhdGVOZXh0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShOYXZpZ2F0ZU5leHQpO1xuTmF2aWdhdGVOZXh0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IE5hdmlnYXRlTmV4dDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9OYXZpZ2F0ZU5leHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL05hdmlnYXRlTmV4dC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2lubGluZS1ibG9jaycsXG4gICAgICBmaWxsOiAnY3VycmVudENvbG9yJyxcbiAgICAgIGhlaWdodDogMjQsXG4gICAgICB3aWR0aDogMjQsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZScsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdmaWxsJywge1xuICAgICAgICBkdXJhdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuZHVyYXRpb24uc2hvcnRlclxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogRWxlbWVudHMgcGFzc2VkIGludG8gdGhlIFNWRyBJY29uLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm92aWRlcyBhIGh1bWFuLXJlYWRhYmxlIHRpdGxlIGZvciB0aGUgZWxlbWVudCB0aGF0IGNvbnRhaW5zIGl0LlxuICAgKiBodHRwczovL3d3dy53My5vcmcvVFIvU1ZHLWFjY2Vzcy8jRXF1aXZhbGVudFxuICAgKi9cbiAgdGl0bGVBY2Nlc3M6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIEFsbG93cyB5b3UgdG8gcmVkZWZpbmUgd2hhdCB0aGUgY29vcmRpbmF0ZXMgd2l0aG91dCB1bml0cyBtZWFuIGluc2lkZSBhbiBzdmcgZWxlbWVudC5cbiAgICogRm9yIGV4YW1wbGUsIGlmIHRoZSBTVkcgZWxlbWVudCBpcyA1MDAgKHdpZHRoKSBieSAyMDAgKGhlaWdodCksXG4gICAqIGFuZCB5b3UgcGFzcyB2aWV3Qm94PVwiMCAwIDUwIDIwXCIsXG4gICAqIHRoaXMgbWVhbnMgdGhhdCB0aGUgY29vcmRpbmF0ZXMgaW5zaWRlIHRoZSBzdmcgd2lsbCBnbyBmcm9tIHRoZSB0b3AgbGVmdCBjb3JuZXIgKDAsMClcbiAgICogdG8gYm90dG9tIHJpZ2h0ICg1MCwyMCkgYW5kIGVhY2ggdW5pdCB3aWxsIGJlIHdvcnRoIDEwcHguXG4gICAqL1xuICB2aWV3Qm94OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLmlzUmVxdWlyZWRcbn07XG5cbnZhciBTdmdJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoU3ZnSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gU3ZnSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBTdmdJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoU3ZnSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoU3ZnSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoU3ZnSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgdGl0bGVBY2Nlc3MgPSBfcHJvcHMudGl0bGVBY2Nlc3MsXG4gICAgICAgICAgdmlld0JveCA9IF9wcm9wcy52aWV3Qm94LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ3RpdGxlQWNjZXNzJywgJ3ZpZXdCb3gnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzdmcnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzTmFtZSxcbiAgICAgICAgICBmb2N1c2FibGU6ICdmYWxzZScsXG4gICAgICAgICAgdmlld0JveDogdmlld0JveCxcbiAgICAgICAgICAnYXJpYS1oaWRkZW4nOiB0aXRsZUFjY2VzcyA/ICdmYWxzZScgOiAndHJ1ZSdcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICB0aXRsZUFjY2VzcyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICd0aXRsZScsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICB0aXRsZUFjY2Vzc1xuICAgICAgICApIDogbnVsbCxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBTdmdJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuU3ZnSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpZXdCb3g6ICcwIDAgMjQgMjQnLFxuICBjb2xvcjogJ2luaGVyaXQnXG59O1xuU3ZnSWNvbi5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aVN2Z0ljb24nIH0pKFN2Z0ljb24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi9TdmdJY29uJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pLmRlZmF1bHQ7XG4gIH1cbn0pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5vbmVPZlR5cGUoW3JlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlciwgcmVxdWlyZShcInByb3AtdHlwZXNcIikuc2hhcGUoe1xuICBlbnRlcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLmlzUmVxdWlyZWQsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm51bWJlci5pc1JlcXVpcmVkXG59KV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZShcInByb3AtdHlwZXNcIikuZnVuYztcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DbGFzc2VzID0ge1xuICBhcHBlYXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgYXBwZWFyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGVudGVyQWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmcsXG4gIGV4aXQ6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZXhpdEFjdGl2ZTogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nXG59O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL2ludGVybmFsL3RyYW5zaXRpb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiA1IDExIDI3IDI4IDM0IDM1IDM2IDM3IDM4IDQwIDQxIDQyIDQzIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfVHJhbnNpdGlvbiA9IHJlcXVpcmUoJ3JlYWN0LXRyYW5zaXRpb24tZ3JvdXAvVHJhbnNpdGlvbicpO1xuXG52YXIgX1RyYW5zaXRpb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVHJhbnNpdGlvbik7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF90cmFuc2l0aW9ucyA9IHJlcXVpcmUoJy4uL3N0eWxlcy90cmFuc2l0aW9ucycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IFRyYW5zaXRpb25cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID0gcmVxdWlyZSgnLi4vaW50ZXJuYWwvdHJhbnNpdGlvbicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICBjb250YWluZXI6IHtcbiAgICAgIGhlaWdodDogMCxcbiAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnaGVpZ2h0JylcbiAgICB9LFxuICAgIGVudGVyZWQ6IHtcbiAgICAgIGhlaWdodDogJ2F1dG8nXG4gICAgfSxcbiAgICB3cmFwcGVyOiB7XG4gICAgICAvLyBIYWNrIHRvIGdldCBjaGlsZHJlbiB3aXRoIGEgbmVnYXRpdmUgbWFyZ2luIHRvIG5vdCBmYWxzaWZ5IHRoZSBoZWlnaHQgY29tcHV0YXRpb24uXG4gICAgICBkaXNwbGF5OiAnZmxleCdcbiAgICB9LFxuICAgIHdyYXBwZXJJbm5lcjoge1xuICAgICAgd2lkdGg6ICcxMDAlJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uRHVyYXRpb24gPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoe1xuICBlbnRlcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcbiAgZXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxufSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2F1dG8nXSldKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgYXBwZWFyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBub2RlIHRvIGJlIGNvbGxhcHNlZC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiB0eXBlb2YgU3RyaW5nID09PSAnZnVuY3Rpb24nID8gcmVxdWlyZSgncHJvcC10eXBlcycpLmluc3RhbmNlT2YoU3RyaW5nKSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnksXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKiBUaGUgZGVmYXVsdCB2YWx1ZSBpcyBhIGBidXR0b25gLlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgaGVpZ2h0IG9mIHRoZSBjb250YWluZXIgd2hlbiBjb2xsYXBzZWQuXG4gICAqL1xuICBjb2xsYXBzZWRIZWlnaHQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvcGVydGllcyBhcHBsaWVkIHRvIHRoZSBgQ29sbGFwc2VgIGNvbnRhaW5lci5cbiAgICovXG4gIGNvbnRhaW5lclByb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjb21wb25lbnQgd2lsbCB0cmFuc2l0aW9uIGluLlxuICAgKi9cbiAgaW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXI6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcmluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkVudGVyZWQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FeGl0OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRXhpdGluZzogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBzdHlsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGR1cmF0aW9uIGZvciB0aGUgdHJhbnNpdGlvbiwgaW4gbWlsbGlzZWNvbmRzLlxuICAgKiBZb3UgbWF5IHNwZWNpZnkgYSBzaW5nbGUgdGltZW91dCBmb3IgYWxsIHRyYW5zaXRpb25zLCBvciBpbmRpdmlkdWFsbHkgd2l0aCBhbiBvYmplY3QuXG4gICAqXG4gICAqIFNldCB0byAnYXV0bycgdG8gYXV0b21hdGljYWxseSBjYWxjdWxhdGUgdHJhbnNpdGlvbiB0aW1lIGJhc2VkIG9uIGhlaWdodC5cbiAgICovXG4gIHRpbWVvdXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgZW50ZXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsXG4gICAgZXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxuICB9KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHVubW91bnRPbkV4aXQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sXG59O1xuXG52YXIgQ29sbGFwc2UgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShDb2xsYXBzZSwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQ29sbGFwc2UoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQ29sbGFwc2UpO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gQ29sbGFwc2UuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKENvbGxhcHNlKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMud3JhcHBlciA9IG51bGwsIF90aGlzLmF1dG9UcmFuc2l0aW9uRHVyYXRpb24gPSB1bmRlZmluZWQsIF90aGlzLmhhbmRsZUVudGVyID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gX3RoaXMucHJvcHMuY29sbGFwc2VkSGVpZ2h0O1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcikge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyKG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUVudGVyaW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wcyA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRpbWVvdXQgPSBfdGhpcyRwcm9wcy50aW1lb3V0LFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMudGhlbWU7XG5cbiAgICAgIHZhciB3cmFwcGVySGVpZ2h0ID0gX3RoaXMud3JhcHBlciA/IF90aGlzLndyYXBwZXIuY2xpZW50SGVpZ2h0IDogMDtcblxuICAgICAgaWYgKHRpbWVvdXQgPT09ICdhdXRvJykge1xuICAgICAgICB2YXIgZHVyYXRpb24yID0gdGhlbWUudHJhbnNpdGlvbnMuZ2V0QXV0b0hlaWdodER1cmF0aW9uKHdyYXBwZXJIZWlnaHQpO1xuICAgICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IGR1cmF0aW9uMiArICdtcyc7XG4gICAgICAgIF90aGlzLmF1dG9UcmFuc2l0aW9uRHVyYXRpb24gPSBkdXJhdGlvbjI7XG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJykge1xuICAgICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IHRpbWVvdXQgKyAnbXMnO1xuICAgICAgfSBlbHNlIGlmICh0aW1lb3V0ICYmIHR5cGVvZiB0aW1lb3V0LmVudGVyID09PSAnbnVtYmVyJykge1xuICAgICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IHRpbWVvdXQuZW50ZXIgKyAnbXMnO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gVGhlIHByb3BUeXBlIHdpbGwgd2FybiBpbiB0aGlzIGNhc2UuXG4gICAgICB9XG5cbiAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gd3JhcHBlckhlaWdodCArICdweCc7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyaW5nKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJpbmcobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlRW50ZXJlZCA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICBub2RlLnN0eWxlLmhlaWdodCA9ICdhdXRvJztcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJlZCkge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkVudGVyZWQobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlRXhpdCA9IGZ1bmN0aW9uIChub2RlKSB7XG4gICAgICB2YXIgd3JhcHBlckhlaWdodCA9IF90aGlzLndyYXBwZXIgPyBfdGhpcy53cmFwcGVyLmNsaWVudEhlaWdodCA6IDA7XG4gICAgICBub2RlLnN0eWxlLmhlaWdodCA9IHdyYXBwZXJIZWlnaHQgKyAncHgnO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FeGl0KSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRXhpdChub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0aW5nID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczIgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICB0aW1lb3V0ID0gX3RoaXMkcHJvcHMyLnRpbWVvdXQsXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wczIudGhlbWU7XG5cbiAgICAgIHZhciB3cmFwcGVySGVpZ2h0ID0gX3RoaXMud3JhcHBlciA/IF90aGlzLndyYXBwZXIuY2xpZW50SGVpZ2h0IDogMDtcblxuICAgICAgaWYgKHRpbWVvdXQgPT09ICdhdXRvJykge1xuICAgICAgICB2YXIgZHVyYXRpb24yID0gdGhlbWUudHJhbnNpdGlvbnMuZ2V0QXV0b0hlaWdodER1cmF0aW9uKHdyYXBwZXJIZWlnaHQpO1xuICAgICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IGR1cmF0aW9uMiArICdtcyc7XG4gICAgICAgIF90aGlzLmF1dG9UcmFuc2l0aW9uRHVyYXRpb24gPSBkdXJhdGlvbjI7XG4gICAgICB9IGVsc2UgaWYgKHR5cGVvZiB0aW1lb3V0ID09PSAnbnVtYmVyJykge1xuICAgICAgICBub2RlLnN0eWxlLnRyYW5zaXRpb25EdXJhdGlvbiA9IHRpbWVvdXQgKyAnbXMnO1xuICAgICAgfSBlbHNlIGlmICh0aW1lb3V0ICYmIHR5cGVvZiB0aW1lb3V0LmV4aXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gdGltZW91dC5leGl0ICsgJ21zJztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIFRoZSBwcm9wVHlwZSB3aWxsIHdhcm4gaW4gdGhpcyBjYXNlLlxuICAgICAgfVxuXG4gICAgICBub2RlLnN0eWxlLmhlaWdodCA9IF90aGlzLnByb3BzLmNvbGxhcHNlZEhlaWdodDtcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRXhpdGluZykge1xuICAgICAgICBfdGhpcy5wcm9wcy5vbkV4aXRpbmcobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuYWRkRW5kTGlzdGVuZXIgPSBmdW5jdGlvbiAobm9kZSwgbmV4dCkge1xuICAgICAgaWYgKF90aGlzLnByb3BzLnRpbWVvdXQgPT09ICdhdXRvJykge1xuICAgICAgICBzZXRUaW1lb3V0KG5leHQsIF90aGlzLmF1dG9UcmFuc2l0aW9uRHVyYXRpb24gfHwgMCk7XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKENvbGxhcHNlLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhcHBlYXIgPSBfcHJvcHMuYXBwZWFyLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIENvbXBvbmVudFByb3AgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICAgIGNvbGxhcHNlZEhlaWdodCA9IF9wcm9wcy5jb2xsYXBzZWRIZWlnaHQsXG4gICAgICAgICAgY29udGFpbmVyUHJvcHMgPSBfcHJvcHMuY29udGFpbmVyUHJvcHMsXG4gICAgICAgICAgb25FbnRlciA9IF9wcm9wcy5vbkVudGVyLFxuICAgICAgICAgIG9uRW50ZXJpbmcgPSBfcHJvcHMub25FbnRlcmluZyxcbiAgICAgICAgICBvbkVudGVyZWQgPSBfcHJvcHMub25FbnRlcmVkLFxuICAgICAgICAgIG9uRXhpdCA9IF9wcm9wcy5vbkV4aXQsXG4gICAgICAgICAgb25FeGl0aW5nID0gX3Byb3BzLm9uRXhpdGluZyxcbiAgICAgICAgICBzdHlsZSA9IF9wcm9wcy5zdHlsZSxcbiAgICAgICAgICB0aW1lb3V0ID0gX3Byb3BzLnRpbWVvdXQsXG4gICAgICAgICAgdGhlbWUgPSBfcHJvcHMudGhlbWUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYXBwZWFyJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdjb2xsYXBzZWRIZWlnaHQnLCAnY29udGFpbmVyUHJvcHMnLCAnb25FbnRlcicsICdvbkVudGVyaW5nJywgJ29uRW50ZXJlZCcsICdvbkV4aXQnLCAnb25FeGl0aW5nJywgJ3N0eWxlJywgJ3RpbWVvdXQnLCAndGhlbWUnXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfVHJhbnNpdGlvbjIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgYXBwZWFyOiBhcHBlYXIsXG4gICAgICAgICAgb25FbnRlcmluZzogdGhpcy5oYW5kbGVFbnRlcmluZyxcbiAgICAgICAgICBvbkVudGVyOiB0aGlzLmhhbmRsZUVudGVyLFxuICAgICAgICAgIG9uRW50ZXJlZDogdGhpcy5oYW5kbGVFbnRlcmVkLFxuICAgICAgICAgIG9uRXhpdGluZzogdGhpcy5oYW5kbGVFeGl0aW5nLFxuICAgICAgICAgIG9uRXhpdDogdGhpcy5oYW5kbGVFeGl0LFxuICAgICAgICAgIGFkZEVuZExpc3RlbmVyOiB0aGlzLmFkZEVuZExpc3RlbmVyLFxuICAgICAgICAgIHRpbWVvdXQ6IHRpbWVvdXQgPT09ICdhdXRvJyA/IG51bGwgOiB0aW1lb3V0XG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgZnVuY3Rpb24gKHN0YXRlLCBvdGhlcklubmVyKSB7XG4gICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgQ29tcG9uZW50UHJvcCxcbiAgICAgICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5jb250YWluZXIsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmVudGVyZWQsIHN0YXRlID09PSAnZW50ZXJlZCcpLCBjbGFzc05hbWUpLFxuICAgICAgICAgICAgICBzdHlsZTogKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7fSwgc3R5bGUsIHtcbiAgICAgICAgICAgICAgICBtaW5IZWlnaHQ6IGNvbGxhcHNlZEhlaWdodFxuICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgfSwgb3RoZXJJbm5lciksXG4gICAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICAgJ2RpdicsXG4gICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzZXMud3JhcHBlcixcbiAgICAgICAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICAgICAgICBfdGhpczIud3JhcHBlciA9IG5vZGU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy53cmFwcGVySW5uZXIgfSxcbiAgICAgICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICAgICApXG4gICAgICAgICAgICApXG4gICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIENvbGxhcHNlO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQ29sbGFwc2UuZGVmYXVsdFByb3BzID0ge1xuICBhcHBlYXI6IGZhbHNlLFxuICBjb21wb25lbnQ6ICdkaXYnLFxuICBjb2xsYXBzZWRIZWlnaHQ6ICcwcHgnLFxuICB0aW1lb3V0OiBfdHJhbnNpdGlvbnMuZHVyYXRpb24uc3RhbmRhcmRcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywge1xuICB3aXRoVGhlbWU6IHRydWUsXG4gIG5hbWU6ICdNdWlDb2xsYXBzZSdcbn0pKENvbGxhcHNlKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9Db2xsYXBzZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvdHJhbnNpdGlvbnMvQ29sbGFwc2UuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAxMSAzOCIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9pbnZhcmlhbnQgPSByZXF1aXJlKCdpbnZhcmlhbnQnKTtcblxudmFyIF9pbnZhcmlhbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW52YXJpYW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKG9iaiwga2V5cykgeyB2YXIgdGFyZ2V0ID0ge307IGZvciAodmFyIGkgaW4gb2JqKSB7IGlmIChrZXlzLmluZGV4T2YoaSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgaSkpIGNvbnRpbnVlOyB0YXJnZXRbaV0gPSBvYmpbaV07IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBpc01vZGlmaWVkRXZlbnQgPSBmdW5jdGlvbiBpc01vZGlmaWVkRXZlbnQoZXZlbnQpIHtcbiAgcmV0dXJuICEhKGV2ZW50Lm1ldGFLZXkgfHwgZXZlbnQuYWx0S2V5IHx8IGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQuc2hpZnRLZXkpO1xufTtcblxuLyoqXG4gKiBUaGUgcHVibGljIEFQSSBmb3IgcmVuZGVyaW5nIGEgaGlzdG9yeS1hd2FyZSA8YT4uXG4gKi9cblxudmFyIExpbmsgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoTGluaywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGluaygpIHtcbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIExpbmspO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwuYXBwbHkoX1JlYWN0JENvbXBvbmVudCwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmhhbmRsZUNsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25DbGljaykgX3RoaXMucHJvcHMub25DbGljayhldmVudCk7XG5cbiAgICAgIGlmICghZXZlbnQuZGVmYXVsdFByZXZlbnRlZCAmJiAvLyBvbkNsaWNrIHByZXZlbnRlZCBkZWZhdWx0XG4gICAgICBldmVudC5idXR0b24gPT09IDAgJiYgLy8gaWdub3JlIHJpZ2h0IGNsaWNrc1xuICAgICAgIV90aGlzLnByb3BzLnRhcmdldCAmJiAvLyBsZXQgYnJvd3NlciBoYW5kbGUgXCJ0YXJnZXQ9X2JsYW5rXCIgZXRjLlxuICAgICAgIWlzTW9kaWZpZWRFdmVudChldmVudCkgLy8gaWdub3JlIGNsaWNrcyB3aXRoIG1vZGlmaWVyIGtleXNcbiAgICAgICkge1xuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICB2YXIgaGlzdG9yeSA9IF90aGlzLmNvbnRleHQucm91dGVyLmhpc3Rvcnk7XG4gICAgICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgICAgIHJlcGxhY2UgPSBfdGhpcyRwcm9wcy5yZXBsYWNlLFxuICAgICAgICAgICAgICB0byA9IF90aGlzJHByb3BzLnRvO1xuXG5cbiAgICAgICAgICBpZiAocmVwbGFjZSkge1xuICAgICAgICAgICAgaGlzdG9yeS5yZXBsYWNlKHRvKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaGlzdG9yeS5wdXNoKHRvKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gIExpbmsucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgcmVwbGFjZSA9IF9wcm9wcy5yZXBsYWNlLFxuICAgICAgICB0byA9IF9wcm9wcy50byxcbiAgICAgICAgaW5uZXJSZWYgPSBfcHJvcHMuaW5uZXJSZWYsXG4gICAgICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9wcm9wcywgWydyZXBsYWNlJywgJ3RvJywgJ2lubmVyUmVmJ10pOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVudXNlZC12YXJzXG5cbiAgICAoMCwgX2ludmFyaWFudDIuZGVmYXVsdCkodGhpcy5jb250ZXh0LnJvdXRlciwgJ1lvdSBzaG91bGQgbm90IHVzZSA8TGluaz4gb3V0c2lkZSBhIDxSb3V0ZXI+Jyk7XG5cbiAgICB2YXIgaHJlZiA9IHRoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeS5jcmVhdGVIcmVmKHR5cGVvZiB0byA9PT0gJ3N0cmluZycgPyB7IHBhdGhuYW1lOiB0byB9IDogdG8pO1xuXG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdhJywgX2V4dGVuZHMoe30sIHByb3BzLCB7IG9uQ2xpY2s6IHRoaXMuaGFuZGxlQ2xpY2ssIGhyZWY6IGhyZWYsIHJlZjogaW5uZXJSZWYgfSkpO1xuICB9O1xuXG4gIHJldHVybiBMaW5rO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGluay5wcm9wVHlwZXMgPSB7XG4gIG9uQ2xpY2s6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcbiAgdGFyZ2V0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcbiAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICB0bzogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdF0pLmlzUmVxdWlyZWQsXG4gIGlubmVyUmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuY10pXG59O1xuTGluay5kZWZhdWx0UHJvcHMgPSB7XG4gIHJlcGxhY2U6IGZhbHNlXG59O1xuTGluay5jb250ZXh0VHlwZXMgPSB7XG4gIHJvdXRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgaGlzdG9yeTogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgICBwdXNoOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgY3JlYXRlSHJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWRcbiAgICB9KS5pc1JlcXVpcmVkXG4gIH0pLmlzUmVxdWlyZWRcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjYgMjcgMjggMzAgMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNTcgNTggNTkgNjEiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IHJlcXVpcmUoJy4vdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbCcpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJFbGVtZW50VXRpbCk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IHJlcXVpcmUoJy4vaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCcpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBjcmVhdGVGYWN0b3J5ID0gZnVuY3Rpb24gY3JlYXRlRmFjdG9yeSh0eXBlKSB7XG4gIHZhciBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCA9ICgwLCBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIuZGVmYXVsdCkodHlwZSk7XG4gIHJldHVybiBmdW5jdGlvbiAocCwgYykge1xuICAgIHJldHVybiAoMCwgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwyLmRlZmF1bHQpKGZhbHNlLCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcCwgYyk7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVGYWN0b3J5O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBnZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIGdldERpc3BsYXlOYW1lKENvbXBvbmVudCkge1xuICBpZiAodHlwZW9mIENvbXBvbmVudCA9PT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gQ29tcG9uZW50O1xuICB9XG5cbiAgaWYgKCFDb21wb25lbnQpIHtcbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9XG5cbiAgcmV0dXJuIENvbXBvbmVudC5kaXNwbGF5TmFtZSB8fCBDb21wb25lbnQubmFtZSB8fCAnQ29tcG9uZW50Jztcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGdldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxudmFyIGlzQ2xhc3NDb21wb25lbnQgPSBmdW5jdGlvbiBpc0NsYXNzQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbihDb21wb25lbnQgJiYgQ29tcG9uZW50LnByb3RvdHlwZSAmJiBfdHlwZW9mKENvbXBvbmVudC5wcm90b3R5cGUuaXNSZWFjdENvbXBvbmVudCkgPT09ICdvYmplY3QnKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzQ2xhc3NDb21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudCA9IHJlcXVpcmUoJy4vaXNDbGFzc0NvbXBvbmVudCcpO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNDbGFzc0NvbXBvbmVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4odHlwZW9mIENvbXBvbmVudCA9PT0gJ2Z1bmN0aW9uJyAmJiAhKDAsIF9pc0NsYXNzQ29tcG9uZW50Mi5kZWZhdWx0KShDb21wb25lbnQpICYmICFDb21wb25lbnQuZGVmYXVsdFByb3BzICYmICFDb21wb25lbnQuY29udGV4dFR5cGVzICYmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ3Byb2R1Y3Rpb24nIHx8ICFDb21wb25lbnQucHJvcFR5cGVzKSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3Nob3VsZFVwZGF0ZSA9IHJlcXVpcmUoJy4vc2hvdWxkVXBkYXRlJyk7XG5cbnZhciBfc2hvdWxkVXBkYXRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Nob3VsZFVwZGF0ZSk7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnLi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHB1cmUgPSBmdW5jdGlvbiBwdXJlKEJhc2VDb21wb25lbnQpIHtcbiAgdmFyIGhvYyA9ICgwLCBfc2hvdWxkVXBkYXRlMi5kZWZhdWx0KShmdW5jdGlvbiAocHJvcHMsIG5leHRQcm9wcykge1xuICAgIHJldHVybiAhKDAsIF9zaGFsbG93RXF1YWwyLmRlZmF1bHQpKHByb3BzLCBuZXh0UHJvcHMpO1xuICB9KTtcblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3B1cmUnKSkoaG9jKEJhc2VDb21wb25lbnQpKTtcbiAgfVxuXG4gIHJldHVybiBob2MoQmFzZUNvbXBvbmVudCk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBwdXJlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NldFN0YXRpYyA9IHJlcXVpcmUoJy4vc2V0U3RhdGljJyk7XG5cbnZhciBfc2V0U3RhdGljMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldFN0YXRpYyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHNldERpc3BsYXlOYW1lKGRpc3BsYXlOYW1lKSB7XG4gIHJldHVybiAoMCwgX3NldFN0YXRpYzIuZGVmYXVsdCkoJ2Rpc3BsYXlOYW1lJywgZGlzcGxheU5hbWUpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIHNldFN0YXRpYyA9IGZ1bmN0aW9uIHNldFN0YXRpYyhrZXksIHZhbHVlKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgQmFzZUNvbXBvbmVudFtrZXldID0gdmFsdWU7XG4gICAgLyogZXNsaW50LWVuYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIHJldHVybiBCYXNlQ29tcG9uZW50O1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0U3RhdGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCdmYmpzL2xpYi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gX3NoYWxsb3dFcXVhbDIuZGVmYXVsdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeSA9IHJlcXVpcmUoJy4vY3JlYXRlRWFnZXJGYWN0b3J5Jyk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRmFjdG9yeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIHNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZFVwZGF0ZSh0ZXN0KSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIHZhciBmYWN0b3J5ID0gKDAsIF9jcmVhdGVFYWdlckZhY3RvcnkyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpO1xuXG4gICAgdmFyIFNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gICAgICBfaW5oZXJpdHMoU2hvdWxkVXBkYXRlLCBfQ29tcG9uZW50KTtcblxuICAgICAgZnVuY3Rpb24gU2hvdWxkVXBkYXRlKCkge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2hvdWxkVXBkYXRlKTtcblxuICAgICAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NvbXBvbmVudC5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgICAgIH1cblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5zaG91bGRDb21wb25lbnRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgICAgIHJldHVybiB0ZXN0KHRoaXMucHJvcHMsIG5leHRQcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIGZhY3RvcnkodGhpcy5wcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICAgIH0oX3JlYWN0LkNvbXBvbmVudCk7XG5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAnc2hvdWxkVXBkYXRlJykpKFNob3VsZFVwZGF0ZSk7XG4gICAgfVxuICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzaG91bGRVcGRhdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBjcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gZnVuY3Rpb24gY3JlYXRlRWFnZXJFbGVtZW50VXRpbChoYXNLZXksIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwcm9wcywgY2hpbGRyZW4pIHtcbiAgaWYgKCFoYXNLZXkgJiYgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQpIHtcbiAgICBpZiAoY2hpbGRyZW4pIHtcbiAgICAgIHJldHVybiB0eXBlKF9leHRlbmRzKHt9LCBwcm9wcywgeyBjaGlsZHJlbjogY2hpbGRyZW4gfSkpO1xuICAgIH1cbiAgICByZXR1cm4gdHlwZShwcm9wcyk7XG4gIH1cblxuICB2YXIgQ29tcG9uZW50ID0gdHlwZTtcblxuICBpZiAoY2hpbGRyZW4pIHtcbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICBDb21wb25lbnQsXG4gICAgICBwcm9wcyxcbiAgICAgIGNoaWxkcmVuXG4gICAgKTtcbiAgfVxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChDb21wb25lbnQsIHByb3BzKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWw7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZ2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL2dldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfZ2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0RGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgd3JhcERpc3BsYXlOYW1lID0gZnVuY3Rpb24gd3JhcERpc3BsYXlOYW1lKEJhc2VDb21wb25lbnQsIGhvY05hbWUpIHtcbiAgcmV0dXJuIGhvY05hbWUgKyAnKCcgKyAoMCwgX2dldERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KSArICcpJztcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHdyYXBEaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIvKipcbiAqIFRoaXMgY29tcG9uZW50IGlzIGNyZWF0ZSB0byBzaG93IG5hdmlhZ3Rpb24gYnV0dG9uIHRvIG1vdmUgbmV4dCBwYWdlXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgTGluayBmcm9tICdyZWFjdC1yb3V0ZXItZG9tL0xpbmsnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcblxuaW1wb3J0IHsgd2l0aFN0eWxlcyB9IGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcyc7XG5pbXBvcnQgQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0J1dHRvbic7XG5cbmltcG9ydCBOYXZpZ2F0ZU5leHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL05hdmlnYXRlTmV4dCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgIHJpZ2h0OiAnMiUnLFxuICAgIGJvdHRvbTogJzQlJyxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHtcbiAgICAgIHJpZ2h0OiAnMiUnLFxuICAgICAgYm90dG9tOiAnMiUnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyldOiB7XG4gICAgICByaWdodDogJzIlJyxcbiAgICAgIGJvdHRvbTogJzElJyxcbiAgICB9LFxuICB9LFxufSk7XG5cbi8qKlxuICpcbiAqIEBwYXJhbSB7b2JqZWN0fSBjbGFzc2VzXG4gKiBAcGFyYW0ge3BhdGh9IHRvXG4gKi9cbmNvbnN0IE5hdmlnYXRlQnV0dG9uTmV4dCA9ICh7IGNsYXNzZXMsIHRvID0gJy8nIH0pID0+IChcbiAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgPEJ1dHRvblxuICAgICAgZmFiXG4gICAgICBjb2xvcj1cImFjY2VudFwiXG4gICAgICBjb21wb25lbnQ9e0xpbmt9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMuYnV0dG9ufVxuICAgICAgcmFpc2VkXG4gICAgICB0bz17dG99XG4gICAgPlxuICAgICAgPE5hdmlnYXRlTmV4dEljb24gLz5cbiAgICA8L0J1dHRvbj5cbiAgPC9kaXY+XG4pO1xuXG5OYXZpZ2F0ZUJ1dHRvbk5leHQucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHRvOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG59O1xuXG5OYXZpZ2F0ZUJ1dHRvbk5leHQuZGVmYXVsdFByb3BzID0ge1xuICB0bzogJy8nLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKE5hdmlnYXRlQnV0dG9uTmV4dCk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvTmF2aWdhdGlvbkJ1dHRvbk5leHQuanMiLCIvKipcbiAqIFRoaXMgY29tcG9uZW50IGNvbnRhaW5zIGRpc2NyaXB0aW9uIG9mIHNlcnZpY2VzIG9mIG1heWFzaFxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBjbGFzc25hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuXG5pbXBvcnQgVHlwb2dyYXBoeSBmcm9tICdtYXRlcmlhbC11aS9UeXBvZ3JhcGh5JztcbmltcG9ydCBDb2xsYXBzZSBmcm9tICdtYXRlcmlhbC11aS90cmFuc2l0aW9ucy9Db2xsYXBzZSc7XG5cbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcbmltcG9ydCBFeHBhbmRNb3JlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9FeHBhbmRNb3JlJztcblxuaW1wb3J0IE5hdmlnYXRpb25CdXR0b25OZXh0IGZyb20gJy4uL2NvbXBvbmVudHMvTmF2aWdhdGlvbkJ1dHRvbk5leHQnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHt9LFxuICBjYXJkSGVhZGVyOiB7XG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgfSxcbiAgY2FyZDoge1xuICAgIG1hcmdpblRvcDogJzIlJyxcbiAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LFxuICB9LFxuICB0aXRsZToge1xuICAgIGZvbnRXZWlnaHQ6IHRoZW1lLnR5cG9ncmFwaHkuZm9udFdlaWdodE1lZGl1bSxcbiAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzcwMF0pLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgZm9udFNpemU6IDMwLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7XG4gICAgICBmb250U2l6ZTogMzAsXG4gICAgfSxcbiAgfSxcbiAgc3ViaGVhZGVyOiB7XG4gICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs3MDBdKSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignbWQnKV06IHtcbiAgICAgIGZvbnRTaXplOiAxNixcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpXToge1xuICAgICAgZm9udFNpemU6IDE2LFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7XG4gICAgICBmb250U2l6ZTogMTYsXG4gICAgfSxcbiAgfSxcblxuICBzZXJ2aWNlc1RpdGxlOiB7XG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgICBoZWlnaHQ6ICcyMHZoJyxcbiAgICBiYWNrZ3JvdW5kSW1hZ2U6XG4gICAgICAndXJsKFwiaHR0cHM6Ly9kcml2ZW5sb2NhbC5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTUvMTAvTWF0ZXJpYWwtZGVzaWduLmpwZ1wiKScsXG4gICAgYmFja2dyb3VuZFBvc2l0aW9uOiAnY2VudGVyJyxcbiAgICBiYWNrZ3JvdW5kUmVwZWF0OiAnbm8tcmVwZWF0JyxcbiAgfSxcblxuICBwYXJhZ3JhcGg6IHtcbiAgICBwYWRkaW5nOiAnMiUnLFxuICB9LFxuICBncmlkSXRlbToge1xuICAgIHBhZGRpbmc6ICcyJScsXG4gIH0sXG5cbiAgbWVkaWE6IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgIHRvcDogJy0yOHB4JyxcbiAgICBvcGFjaXR5OiAxLFxuICAgIGN1cnNvcjogJ2Nyb3NzaGFpcicsXG4gICAgdHJhbnNpdGlvbkR1cmF0aW9uOiAnMC4zcycsXG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4IDhweCA4cHggOHB4JyxcbiAgICBib3hTaGFkb3c6XG4gICAgICAnMCA4cHggMTFweCA4cHggcmdiYSgwLCAwLCAwLCAwLjIpLCcgK1xuICAgICAgJyAwIDZweCAyMHB4IDAgcmdiYSgwLCAwLCAwLCAwLjE5KScsXG4gICAgJyY6aG92ZXInOiB7XG4gICAgICB0cmFuc2Zvcm06ICdzY2FsZSgxLjAyKScsXG4gICAgICBvcGFjaXR5OiAwLjksXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMudXAoJ3hsJyldOiB7XG4gICAgICBtYXJnaW5Cb3R0b206ICctODVweCcsXG4gICAgICBoZWlnaHQ6ICcxNzBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneGwnKV06IHtcbiAgICAgIG1hcmdpbkJvdHRvbTogJy04NXB4JyxcbiAgICAgIGhlaWdodDogJzE3MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdsZycpXToge1xuICAgICAgbWFyZ2luQm90dG9tOiAnLTg1cHgnLFxuICAgICAgaGVpZ2h0OiAnMTcwcHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBtYXJnaW5Cb3R0b206ICctODVweCcsXG4gICAgICBoZWlnaHQ6ICcxNzBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIG1hcmdpbkJvdHRvbTogJy04NXB4JyxcbiAgICAgIGhlaWdodDogJzE1MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4cycpXToge1xuICAgICAgbWFyZ2luQm90dG9tOiAnLTg1cHgnLFxuICAgICAgaGVpZ2h0OiAnMTUwcHgnLFxuICAgIH0sXG4gIH0sXG5cbiAgZXhwYW5kOiB7XG4gICAgdHJhbnNmb3JtOiAncm90YXRlKDBkZWcpJyxcbiAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ3RyYW5zZm9ybScsIHtcbiAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdCxcbiAgICB9KSxcbiAgfSxcbiAgdHJhbnNmb3JtOiAncm90YXRlKDE4MGRlZyknLFxuICBleHBhbmRPcGVuOiB7XG4gICAgLy8gaGVpZ2h0OiAnMnB4JyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBiYWNrOiB7XG4gICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNDAwXSxcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHtcbiAgICAgIG1hcmdpblRvcDogJy0xMCUnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hsJyldOiB7XG4gICAgICBtYXJnaW5Ub3A6ICctNSUnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ2xnJyldOiB7XG4gICAgICBtYXJnaW5Ub3A6ICctMyUnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBtYXJnaW5Ub3A6ICctNSUnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3NtJyldOiB7XG4gICAgICBtYXJnaW5Ub3A6ICcxJScsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bigneHMnKV06IHtcbiAgICAgIG1hcmdpblRvcDogJy0xMCUnLFxuICAgIH0sXG4gIH0sXG59KTtcblxuY2xhc3MgU2VydmljZXMgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgZXhwYW5kZWQ6IGZhbHNlLFxuICAgICAgZXhwYW5kOiBmYWxzZSxcbiAgICAgIG9uZXhwYW5kOiBmYWxzZSxcbiAgICAgIG9uZXhwYW5kZWQ6IGZhbHNlLFxuICAgIH07XG4gIH1cblxuICBoYW5kbGVFeHBhbmRDbGljayA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBleHBhbmRlZDogIXRoaXMuc3RhdGUuZXhwYW5kZWQgfSk7XG5cbiAgc2Vjb25kSGFuZGxlRXhwYW5kQ2xpY2sgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgZXhwYW5kOiAhdGhpcy5zdGF0ZS5leHBhbmQgfSk7XG5cbiAgdGhpcmRIYW5kbGVFeHBhbmRDbGljayA9ICgpID0+XG4gICAgdGhpcy5zZXRTdGF0ZSh7IG9uZXhwYW5kOiAhdGhpcy5zdGF0ZS5vbmV4cGFuZCB9KTtcblxuICBmb3VydGhIYW5kbGVFeHBhbmRDbGljayA9ICgpID0+XG4gICAgdGhpcy5zZXRTdGF0ZSh7IG9uZXhwYW5kZWQ6ICF0aGlzLnN0YXRlLm9uZXhwYW5kZWQgfSk7XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcyB9ID0gdGhpcy5wcm9wcztcblxuICAgIHJldHVybiAoXG4gICAgICA8R3JpZFxuICAgICAgICBjb250YWluZXJcbiAgICAgICAgc3BhY2luZz17MH1cbiAgICAgICAganVzdGlmeT1cInNwYWNlLWFyb3VuZFwiXG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgPlxuICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0gc209ezEyfSBtZD17MTJ9IGxnPXsxMn0+XG4gICAgICAgICAgPENhcmQgcmFpc2VkPlxuICAgICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgICAgdGl0bGU9e1xuICAgICAgICAgICAgICAgIDxUeXBvZ3JhcGh5IGNsYXNzTmFtZT17Y2xhc3Nlcy50aXRsZX0gdHlwZT1cImRpc3BsYXkzXCI+XG4gICAgICAgICAgICAgICAgICBTRVJWSUNFU1xuICAgICAgICAgICAgICAgIDwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICBzdWJoZWFkZXI9e1xuICAgICAgICAgICAgICAgIDxUeXBvZ3JhcGh5IGNsYXNzTmFtZT17Y2xhc3Nlcy5zdWJoZWFkZXJ9IHR5cGU9XCJoZWFkbGluZVwiPlxuICAgICAgICAgICAgICAgICAgT3VyIHByb2R1Y3RzIGFuZCBzZXJ2aWNlcyB3aWxsIGhlbHAgc3R1ZGVudHMgYXMgd2VsbCBhc1xuICAgICAgICAgICAgICAgICAgdGVhY2hlcnMgdG8gaW1wcm92ZSB0aGVpciBrbm93bGVkZ2UgYW5kIHNraWxscy5cbiAgICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc25hbWVzKGNsYXNzZXMuY2FyZEhlYWRlciwgY2xhc3Nlcy5zZXJ2aWNlc1RpdGxlKX1cbiAgICAgICAgICAgIC8+XG4gICAgICAgICAgPC9DYXJkPlxuICAgICAgICA8L0dyaWQ+XG4gICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBzbT17Nn0gbWQ9ezZ9IGxnPXszfSBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19PlxuICAgICAgICAgIDxDYXJkIHJhaXNlZCBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIDxpbWdcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMubWVkaWF9XG4gICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICdodHRwczovL2RyaXZlbmxvY2FsLmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAxNS8xMC9NYXRlcmlhbC1kZXNpZ24uanBnJ1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBhbHQ9eydoaWEnfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAgIDxDYXJkQWN0aW9ucyBkaXNhYmxlQWN0aW9uU3BhY2luZz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmJhY2t9PlxuICAgICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3NuYW1lcyhjbGFzc2VzLmV4cGFuZCwge1xuICAgICAgICAgICAgICAgICAgICBbY2xhc3Nlcy5leHBhbmRPcGVuXTogdGhpcy5zdGF0ZS5leHBhbmRlZCxcbiAgICAgICAgICAgICAgICAgIH0pfVxuICAgICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVFeHBhbmRDbGlja31cbiAgICAgICAgICAgICAgICAgIGFyaWEtZXhwYW5kZWQ9e3RoaXMuc3RhdGUuZXhwYW5kZWR9XG4gICAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiU2hvdyBtb3JlXCJcbiAgICAgICAgICAgICAgICAgIGZhYlxuICAgICAgICAgICAgICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgICAgICAgICAgICAgcmFpc2VkXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPEV4cGFuZE1vcmVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICAgICA8Q29sbGFwc2UgaW49e3RoaXMuc3RhdGUuZXhwYW5kZWR9IHRpbWVvdXQ9XCJhdXRvXCIgdW5tb3VudE9uRXhpdD5cbiAgICAgICAgICAgICAgPENhcmRDb250ZW50IGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fT5cbiAgICAgICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwiYm9keTJcIj5cbiAgICAgICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkdldCBvcmdhbmlzZWQgTm90ZXMuPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkRvdWJ0cyBkaXNjdXNzaW9uIENoYW5uZWw8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+Q29ubmVjdCB3aXRoIHlvdXIgYWx1bW5pPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkdldCBsYXRlc3QgdXBkYXRlcyBpbiB5b3VyIHJlc3BlY3RpdmUgZmllbGQ8L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+VGVzdCB5b3Vyc2VsZi48L2xpPlxuICAgICAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8L0NvbGxhcHNlPlxuICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgPC9HcmlkPlxuICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0gc209ezZ9IG1kPXs2fSBsZz17M30gY2xhc3NOYW1lPXtjbGFzc2VzLmdyaWRJdGVtfT5cbiAgICAgICAgICA8Q2FyZCByYWlzZWQgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICA8aW1nXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLm1lZGlhfVxuICAgICAgICAgICAgICAgIHNyYz17XG4gICAgICAgICAgICAgICAgICAnaHR0cHM6Ly9kcml2ZW5sb2NhbC5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTUvMTAvTWF0ZXJpYWwtZGVzaWduLmpwZydcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYWx0PXsnaGlhJ31cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8Q2FyZEFjdGlvbnMgZGlzYWJsZUFjdGlvblNwYWNpbmc+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLmZsZXhHcm93fSAvPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5iYWNrfT5cbiAgICAgICAgICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzbmFtZXMoY2xhc3Nlcy5leHBhbmQsIHtcbiAgICAgICAgICAgICAgICAgICAgW2NsYXNzZXMuZXhwYW5kT3Blbl06IHRoaXMuc3RhdGUuZXhwYW5kLFxuICAgICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLnNlY29uZEhhbmRsZUV4cGFuZENsaWNrfVxuICAgICAgICAgICAgICAgICAgYXJpYS1leHBhbmRlZD17dGhpcy5zdGF0ZS5leHBhbmR9XG4gICAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiU2hvdyBtb3JlXCJcbiAgICAgICAgICAgICAgICAgIGZhYlxuICAgICAgICAgICAgICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgICAgICAgICAgICAgcmFpc2VkXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPEV4cGFuZE1vcmVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICAgICA8Q29sbGFwc2UgaW49e3RoaXMuc3RhdGUuZXhwYW5kfSB0aW1lb3V0PVwiYXV0b1wiIHVubW91bnRPbkV4aXQ+XG4gICAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwiYm9keTJcIj5cbiAgICAgICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkNvbm5lY3Qgd2l0aCB5b3VyIHN0dWRlbnRzLjwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBEb3VidHMgZGlzY3Vzc2lvbiBDaGFubmVsIHRvIGNsZWFyIERvdWJ0cyBvZiB5b3VyXG4gICAgICAgICAgICAgICAgICAgICAgc3R1ZGVudHMuXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBTaGFyZSB5b3VyIGtub3dsZWRnZSB3aXRoIG5vdCBvbmx5IHlvdXIgc3R1ZGVudHMgYnV0IHdpdGhcbiAgICAgICAgICAgICAgICAgICAgICB3aG9sZSB3b3JsZC5cbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkdldCBsYXRlc3QgdXBkYXRlcyBpbiB5b3VyIHJlc3BlY3RpdmUgZmllbGQuPC9saT5cbiAgICAgICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgPC9Db2xsYXBzZT5cbiAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgICAgPEdyaWQgaXRlbSB4cz17MTJ9IHNtPXs2fSBtZD17Nn0gbGc9ezN9IGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX0+XG4gICAgICAgICAgPENhcmQgcmFpc2VkIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5tZWRpYX1cbiAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgJ2h0dHBzOi8vZHJpdmVubG9jYWwuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE1LzEwL01hdGVyaWFsLWRlc2lnbi5qcGcnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGFsdD17J2hpYSd9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuYmFja30+XG4gICAgICAgICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc25hbWVzKGNsYXNzZXMuZXhwYW5kLCB7XG4gICAgICAgICAgICAgICAgICAgIFtjbGFzc2VzLmV4cGFuZE9wZW5dOiB0aGlzLnN0YXRlLm9uZXhwYW5kLFxuICAgICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLnRoaXJkSGFuZGxlRXhwYW5kQ2xpY2t9XG4gICAgICAgICAgICAgICAgICBhcmlhLWV4cGFuZGVkPXt0aGlzLnN0YXRlLm9uZXhwYW5kfVxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIlNob3cgbW9yZVwiXG4gICAgICAgICAgICAgICAgICBmYWJcbiAgICAgICAgICAgICAgICAgIGNvbG9yPVwiYWNjZW50XCJcbiAgICAgICAgICAgICAgICAgIHJhaXNlZFxuICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgIDxFeHBhbmRNb3JlSWNvbiAvPlxuICAgICAgICAgICAgICAgIDwvQnV0dG9uPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICAgICAgPENvbGxhcHNlIGluPXt0aGlzLnN0YXRlLm9uZXhwYW5kfSB0aW1lb3V0PVwiYXV0b1wiIHVubW91bnRPbkV4aXQ+XG4gICAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwiYm9keTJcIj5cbiAgICAgICAgICAgICAgICAgIDxvbD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkNvbm5lY3Qgd2l0aCB5b3VyIHN0dWRlbnRzLjwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBEb3VidHMgZGlzY3Vzc2lvbiBDaGFubmVsIHRvIGNsZWFyIERvdWJ0cyBvZiB5b3VyXG4gICAgICAgICAgICAgICAgICAgICAgc3R1ZGVudHMuXG4gICAgICAgICAgICAgICAgICAgIDwvbGk+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICAgICAgICBTaGFyZSB5b3VyIGtub3dsZWRnZSB3aXRoIG5vdCBvbmx5IHlvdXIgc3R1ZGVudHMgYnV0IHdpdGhcbiAgICAgICAgICAgICAgICAgICAgICB3aG9sZSB3b3JsZC5cbiAgICAgICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPkdldCBsYXRlc3QgdXBkYXRlcyBpbiB5b3VyIHJlc3BlY3RpdmUgZmllbGQuPC9saT5cbiAgICAgICAgICAgICAgICAgIDwvb2w+XG4gICAgICAgICAgICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgPC9Db2xsYXBzZT5cbiAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgICAgPEdyaWQgaXRlbSB4cz17MTJ9IHNtPXs2fSBtZD17Nn0gbGc9ezN9IGNsYXNzTmFtZT17Y2xhc3Nlcy5ncmlkSXRlbX0+XG4gICAgICAgICAgPENhcmQgcmFpc2VkIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgPGltZ1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5tZWRpYX1cbiAgICAgICAgICAgICAgICBzcmM9e1xuICAgICAgICAgICAgICAgICAgJ2h0dHBzOi8vZHJpdmVubG9jYWwuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE1LzEwL01hdGVyaWFsLWRlc2lnbi5qcGcnXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGFsdD17J2hpYSd9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L0NhcmRDb250ZW50PlxuICAgICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuYmFja30+XG4gICAgICAgICAgICAgICAgPEJ1dHRvblxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc25hbWVzKGNsYXNzZXMuZXhwYW5kLCB7XG4gICAgICAgICAgICAgICAgICAgIFtjbGFzc2VzLmV4cGFuZE9wZW5dOiB0aGlzLnN0YXRlLm9uZXhwYW5kZWQsXG4gICAgICAgICAgICAgICAgICB9KX1cbiAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMuZm91cnRoSGFuZGxlRXhwYW5kQ2xpY2t9XG4gICAgICAgICAgICAgICAgICBhcmlhLWV4cGFuZGVkPXt0aGlzLnN0YXRlLm9uZXhwYW5kZWR9XG4gICAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiU2hvdyBtb3JlXCJcbiAgICAgICAgICAgICAgICAgIGZhYlxuICAgICAgICAgICAgICAgICAgY29sb3I9XCJhY2NlbnRcIlxuICAgICAgICAgICAgICAgICAgcmFpc2VkXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPEV4cGFuZE1vcmVJY29uIC8+XG4gICAgICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgIDwvQ2FyZEFjdGlvbnM+XG4gICAgICAgICAgICA8Q29sbGFwc2UgaW49e3RoaXMuc3RhdGUub25leHBhbmRlZH0gdGltZW91dD1cImF1dG9cIiB1bm1vdW50T25FeGl0PlxuICAgICAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgdHlwZT1cImJvZHkyXCI+XG4gICAgICAgICAgICAgICAgICA8b2w+XG4gICAgICAgICAgICAgICAgICAgIDxsaT5HZXQgdXBkYXRlZCBrbm93bGVkZ2Ugb24gbGF0ZXN0IHRlY2hub2xvZ2llcy48L2xpPlxuICAgICAgICAgICAgICAgICAgICA8bGk+U3RheSB1cGRhdGVkIHdpdGggaW5kdXN0cmllcyB0cmVuZHMuPC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPlNlZSBncm93dGggb2YgeW91ciBlbXBsb3llcy48L2xpPlxuICAgICAgICAgICAgICAgICAgPC9vbD5cbiAgICAgICAgICAgICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8L0NvbGxhcHNlPlxuICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgPC9HcmlkPlxuICAgICAgICA8TmF2aWdhdGlvbkJ1dHRvbk5leHQgdG89eycvcHJpY2luZyd9IC8+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5TZXJ2aWNlcy5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShTZXJ2aWNlcyk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L3BhZ2VzL1NlcnZpY2VzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==