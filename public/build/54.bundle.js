webpackJsonp([54],{

/***/ "./node_modules/lodash/_baseIntersection.js":
/*!**************************************************!*\
  !*** ./node_modules/lodash/_baseIntersection.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var SetCache = __webpack_require__(/*! ./_SetCache */ "./node_modules/lodash/_SetCache.js"),
    arrayIncludes = __webpack_require__(/*! ./_arrayIncludes */ "./node_modules/lodash/_arrayIncludes.js"),
    arrayIncludesWith = __webpack_require__(/*! ./_arrayIncludesWith */ "./node_modules/lodash/_arrayIncludesWith.js"),
    arrayMap = __webpack_require__(/*! ./_arrayMap */ "./node_modules/lodash/_arrayMap.js"),
    baseUnary = __webpack_require__(/*! ./_baseUnary */ "./node_modules/lodash/_baseUnary.js"),
    cacheHas = __webpack_require__(/*! ./_cacheHas */ "./node_modules/lodash/_cacheHas.js");

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeMin = Math.min;

/**
 * The base implementation of methods like `_.intersection`, without support
 * for iteratee shorthands, that accepts an array of arrays to inspect.
 *
 * @private
 * @param {Array} arrays The arrays to inspect.
 * @param {Function} [iteratee] The iteratee invoked per element.
 * @param {Function} [comparator] The comparator invoked per element.
 * @returns {Array} Returns the new array of shared values.
 */
function baseIntersection(arrays, iteratee, comparator) {
  var includes = comparator ? arrayIncludesWith : arrayIncludes,
      length = arrays[0].length,
      othLength = arrays.length,
      othIndex = othLength,
      caches = Array(othLength),
      maxLength = Infinity,
      result = [];

  while (othIndex--) {
    var array = arrays[othIndex];
    if (othIndex && iteratee) {
      array = arrayMap(array, baseUnary(iteratee));
    }
    maxLength = nativeMin(array.length, maxLength);
    caches[othIndex] = !comparator && (iteratee || (length >= 120 && array.length >= 120))
      ? new SetCache(othIndex && array)
      : undefined;
  }
  array = arrays[0];

  var index = -1,
      seen = caches[0];

  outer:
  while (++index < length && result.length < maxLength) {
    var value = array[index],
        computed = iteratee ? iteratee(value) : value;

    value = (comparator || value !== 0) ? value : 0;
    if (!(seen
          ? cacheHas(seen, computed)
          : includes(result, computed, comparator)
        )) {
      othIndex = othLength;
      while (--othIndex) {
        var cache = caches[othIndex];
        if (!(cache
              ? cacheHas(cache, computed)
              : includes(arrays[othIndex], computed, comparator))
            ) {
          continue outer;
        }
      }
      if (seen) {
        seen.push(computed);
      }
      result.push(value);
    }
  }
  return result;
}

module.exports = baseIntersection;


/***/ }),

/***/ "./node_modules/lodash/_castArrayLikeObject.js":
/*!*****************************************************!*\
  !*** ./node_modules/lodash/_castArrayLikeObject.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var isArrayLikeObject = __webpack_require__(/*! ./isArrayLikeObject */ "./node_modules/lodash/isArrayLikeObject.js");

/**
 * Casts `value` to an empty array if it's not an array like object.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {Array|Object} Returns the cast array-like object.
 */
function castArrayLikeObject(value) {
  return isArrayLikeObject(value) ? value : [];
}

module.exports = castArrayLikeObject;


/***/ }),

/***/ "./node_modules/lodash/intersectionBy.js":
/*!***********************************************!*\
  !*** ./node_modules/lodash/intersectionBy.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var arrayMap = __webpack_require__(/*! ./_arrayMap */ "./node_modules/lodash/_arrayMap.js"),
    baseIntersection = __webpack_require__(/*! ./_baseIntersection */ "./node_modules/lodash/_baseIntersection.js"),
    baseIteratee = __webpack_require__(/*! ./_baseIteratee */ "./node_modules/lodash/_baseIteratee.js"),
    baseRest = __webpack_require__(/*! ./_baseRest */ "./node_modules/lodash/_baseRest.js"),
    castArrayLikeObject = __webpack_require__(/*! ./_castArrayLikeObject */ "./node_modules/lodash/_castArrayLikeObject.js"),
    last = __webpack_require__(/*! ./last */ "./node_modules/lodash/last.js");

/**
 * This method is like `_.intersection` except that it accepts `iteratee`
 * which is invoked for each element of each `arrays` to generate the criterion
 * by which they're compared. The order and references of result values are
 * determined by the first array. The iteratee is invoked with one argument:
 * (value).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Array
 * @param {...Array} [arrays] The arrays to inspect.
 * @param {Function} [iteratee=_.identity] The iteratee invoked per element.
 * @returns {Array} Returns the new array of intersecting values.
 * @example
 *
 * _.intersectionBy([2.1, 1.2], [2.3, 3.4], Math.floor);
 * // => [2.1]
 *
 * // The `_.property` iteratee shorthand.
 * _.intersectionBy([{ 'x': 1 }], [{ 'x': 2 }, { 'x': 1 }], 'x');
 * // => [{ 'x': 1 }]
 */
var intersectionBy = baseRest(function(arrays) {
  var iteratee = last(arrays),
      mapped = arrayMap(arrays, castArrayLikeObject);

  if (iteratee === last(mapped)) {
    iteratee = undefined;
  } else {
    mapped.pop();
  }
  return (mapped.length && mapped[0] === arrays[0])
    ? baseIntersection(mapped, baseIteratee(iteratee, 2))
    : [];
});

module.exports = intersectionBy;


/***/ }),

/***/ "./node_modules/lodash/last.js":
/*!*************************************!*\
  !*** ./node_modules/lodash/last.js ***!
  \*************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

/**
 * Gets the last element of `array`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Array
 * @param {Array} array The array to query.
 * @returns {*} Returns the last element of `array`.
 * @example
 *
 * _.last([1, 2, 3]);
 * // => 3
 */
function last(array) {
  var length = array == null ? 0 : array.length;
  return length ? array[length - 1] : undefined;
}

module.exports = last;


/***/ }),

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Chip/Chip.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Chip/Chip.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _keycode = __webpack_require__(/*! keycode */ "./node_modules/keycode/index.js");

var _keycode2 = _interopRequireDefault(_keycode);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Cancel = __webpack_require__(/*! ../svg-icons/Cancel */ "./node_modules/material-ui/svg-icons/Cancel.js");

var _Cancel2 = _interopRequireDefault(_Cancel);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

var _Avatar = __webpack_require__(/*! ../Avatar/Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  var height = 32;
  var backgroundColor = (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.12);
  var deleteIconColor = (0, _colorManipulator.fade)(theme.palette.text.primary, 0.26);

  return {
    root: {
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(13),
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      height: height,
      color: theme.palette.getContrastText(backgroundColor),
      backgroundColor: backgroundColor,
      borderRadius: height / 2,
      whiteSpace: 'nowrap',
      width: 'fit-content',
      transition: theme.transitions.create(),
      // label will inherit this from root, then `clickable` class overrides this for both
      cursor: 'default',
      outline: 'none', // No outline on focused element in Chrome (as triggered by tabIndex prop)
      border: 'none', // Remove `button` border
      padding: 0 // Remove `button` padding
    },
    clickable: {
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      cursor: 'pointer',
      '&:hover, &:focus': {
        backgroundColor: (0, _colorManipulator.emphasize)(backgroundColor, 0.08)
      },
      '&:active': {
        boxShadow: theme.shadows[1],
        backgroundColor: (0, _colorManipulator.emphasize)(backgroundColor, 0.12)
      }
    },
    deletable: {
      '&:focus': {
        backgroundColor: (0, _colorManipulator.emphasize)(backgroundColor, 0.08)
      }
    },
    avatar: {
      marginRight: -4,
      width: 32,
      height: 32,
      fontSize: theme.typography.pxToRem(16)
    },
    avatarChildren: {
      width: 19,
      height: 19
    },
    label: {
      display: 'flex',
      alignItems: 'center',
      paddingLeft: 12,
      paddingRight: 12,
      userSelect: 'none',
      whiteSpace: 'nowrap',
      cursor: 'inherit'
    },
    deleteIcon: {
      // Remove grey highlight
      WebkitTapHighlightColor: theme.palette.common.transparent,
      color: deleteIconColor,
      cursor: 'pointer',
      height: 'auto',
      margin: '0 4px 0 -8px',
      '&:hover': {
        color: (0, _colorManipulator.fade)(deleteIconColor, 0.4)
      }
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Avatar element.
   */
  avatar: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * Custom delete icon. Will be shown only if `onRequestDelete` is set.
   */
  deleteIcon: typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element),

  /**
   * The content of the label.
   */
  label: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  onKeyDown: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Callback function fired when the delete icon is clicked.
   * If set, the delete icon will be shown.
   */
  onRequestDelete: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  tabIndex: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string])
};

/**
 * Chips represent complex entities in small blocks, such as a contact.
 */
var Chip = function (_React$Component) {
  (0, _inherits3.default)(Chip, _React$Component);

  function Chip() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Chip);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Chip.__proto__ || (0, _getPrototypeOf2.default)(Chip)).call.apply(_ref, [this].concat(args))), _this), _this.chipRef = null, _this.handleDeleteIconClick = function (event) {
      // Stop the event from bubbling up to the `Chip`
      event.stopPropagation();
      var onRequestDelete = _this.props.onRequestDelete;

      if (onRequestDelete) {
        onRequestDelete(event);
      }
    }, _this.handleKeyDown = function (event) {
      var _this$props = _this.props,
          onClick = _this$props.onClick,
          onRequestDelete = _this$props.onRequestDelete,
          onKeyDown = _this$props.onKeyDown;

      var key = (0, _keycode2.default)(event);

      if (onClick && (key === 'space' || key === 'enter')) {
        event.preventDefault();
        onClick(event);
      } else if (onRequestDelete && key === 'backspace') {
        event.preventDefault();
        onRequestDelete(event);
      } else if (key === 'esc') {
        event.preventDefault();
        if (_this.chipRef) {
          _this.chipRef.blur();
        }
      }

      if (onKeyDown) {
        onKeyDown(event);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Chip, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          avatarProp = _props.avatar,
          classes = _props.classes,
          classNameProp = _props.className,
          label = _props.label,
          onClick = _props.onClick,
          onKeyDown = _props.onKeyDown,
          onRequestDelete = _props.onRequestDelete,
          deleteIconProp = _props.deleteIcon,
          tabIndexProp = _props.tabIndex,
          other = (0, _objectWithoutProperties3.default)(_props, ['avatar', 'classes', 'className', 'label', 'onClick', 'onKeyDown', 'onRequestDelete', 'deleteIcon', 'tabIndex']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.clickable, onClick), (0, _defineProperty3.default)({}, classes.deletable, onRequestDelete), classNameProp);

      var deleteIcon = null;
      if (onRequestDelete && deleteIconProp && _react2.default.isValidElement(deleteIconProp)) {
        deleteIcon = _react2.default.cloneElement(deleteIconProp, {
          onClick: this.handleDeleteIconClick,
          className: (0, _classnames2.default)(classes.deleteIcon, deleteIconProp.props.className)
        });
      } else if (onRequestDelete) {
        deleteIcon = _react2.default.createElement(_Cancel2.default, { className: classes.deleteIcon, onClick: this.handleDeleteIconClick });
      }

      var avatar = null;
      if (avatarProp && _react2.default.isValidElement(avatarProp)) {
        avatar = _react2.default.cloneElement(avatarProp, {
          className: (0, _classnames2.default)(classes.avatar, avatarProp.props.className),
          childrenClassName: (0, _classnames2.default)(classes.avatarChildren, avatarProp.props.childrenClassName)
        });
      }

      var tabIndex = tabIndexProp;

      if (!tabIndex) {
        tabIndex = onClick || onRequestDelete ? 0 : -1;
      }

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({
          role: 'button',
          className: className,
          tabIndex: tabIndex,
          onClick: onClick,
          onKeyDown: this.handleKeyDown
        }, other, {
          ref: function ref(node) {
            _this2.chipRef = node;
          }
        }),
        avatar,
        _react2.default.createElement(
          'span',
          { className: classes.label },
          label
        ),
        deleteIcon
      );
    }
  }]);
  return Chip;
}(_react2.default.Component);

Chip.defaultProps = {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiChip' })(Chip);

/***/ }),

/***/ "./node_modules/material-ui/Chip/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Chip/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Chip = __webpack_require__(/*! ./Chip */ "./node_modules/material-ui/Chip/Chip.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Chip).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/Cancel.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/Cancel.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2zm5 13.59L15.59 17 12 13.41 8.41 17 7 15.59 10.59 12 7 8.41 8.41 7 12 10.59 15.59 7 17 8.41 13.41 12 17 15.59z' });

var Cancel = function Cancel(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
Cancel = (0, _pure2.default)(Cancel);
Cancel.muiName = 'SvgIcon';

exports.default = Cancel;

/***/ }),

/***/ "./src/client/actions/elements/getAll.js":
/*!***********************************************!*\
  !*** ./src/client/actions/elements/getAll.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _elements = __webpack_require__(/*! ../../constants/elements */ "./src/client/constants/elements.js");

/**
 *
 * @function
 * @param {Object[]} payload -
 * @param {Object} payload[] -
 * @param {number} payload[].id -
 * @param {string} payload[].username -
 * @param {string} payload[].elementType - 'user' or 'circle'
 * @param {string} payload[].circleType - if elementType is 'circle' then
 * 'edu', 'org', 'field', 'location', 'social' else this key not exists.
 * @param {string} payload[].name -
 * @param {string} payload[].avatar -
 * @returns {Object} - redux action type.
 */
var getAll = function getAll(payload) {
  return { type: _elements.ELEMENTS_GET, payload: payload };
}; /** @format */

exports.default = getAll;

/***/ }),

/***/ "./src/client/actions/users/circles/getAll.js":
/*!****************************************************!*\
  !*** ./src/client/actions/users/circles/getAll.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _circles = __webpack_require__(/*! ../../../constants/users/circles */ "./src/client/constants/users/circles.js");

/**
 * get all user's circles action
 * @function getAll
 * @param {Object} payload -
 * @param {Object} payload.id -
 * @param {Object[]} payload.circles -
 * @param {Object} payload[].circles -
 * @param {number} payload[].circles.id -
 * @returns {Object} -
 */
var getAll = function getAll(payload) {
  return {
    type: _circles.USER_CIRCLES_GET,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/users/circles/getAll.js":
/*!************************************************!*\
  !*** ./src/client/api/users/circles/getAll.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Get all user's circles by userId
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @returns {Promise}
 *
 * @example
 */
/** @format */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/circles';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/constants/users/circles.js":
/*!***********************************************!*\
  !*** ./src/client/constants/users/circles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains action constants for User's circles
 *
 * @format
 */

var USER_CIRCLES_GET = exports.USER_CIRCLES_GET = 'USER_CIRCLES_GET';

/***/ }),

/***/ "./src/client/containers/Circle/index.js":
/*!***********************************************!*\
  !*** ./src/client/containers/Circle/index.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouterDom = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Chip = __webpack_require__(/*! material-ui/Chip */ "./node_modules/material-ui/Chip/index.js");

var _Chip2 = _interopRequireDefault(_Chip);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Post container component
 * It will display one post with some of actions (eg. Bookmarks, Share, Delete)
 *
 * @format
 */

var styles = function styles(theme) {
  return {
    root: {},
    card: {
      borderRadius: '8px',
      display: 'flex'
    },
    details: {
      display: 'flex',
      flexDirection: 'column'
    },
    cover: {
      width: theme.spacing.unit * 10,
      height: theme.spacing.unit * 10,
      margin: theme.spacing.unit * 2,
      borderRadius: '50%',
      flexShrink: 0,
      backgroundColor: theme.palette.background.default
    },
    chip: {
      // margin: theme.spacing.unit,
    },
    link: {
      textDecoration: 'none',
      '&:link': {
        textDecoration: 'none',
        color: 'accent'
      },
      '&:visited': {
        textDecoration: 'none',
        color: 'black'
      },
      '&:hover': {
        color: 'none'
      },
      '&:active': {
        color: 'none'
      }
    },
    flexGrow: {
      flex: '1 1 auto'
    }
  };
};

var Circle = function (_Component) {
  (0, _inherits3.default)(Circle, _Component);

  function Circle(props) {
    (0, _classCallCheck3.default)(this, Circle);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Circle.__proto__ || (0, _getPrototypeOf2.default)(Circle)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.state = {
      hover: false
    };
    return _this;
  }

  (0, _createClass3.default)(Circle, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextState, nextProp) {
      return this.state !== nextState || this.props !== nextProp;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          id = _props.id,
          username = _props.username,
          name = _props.name,
          avatar = _props.avatar,
          cover = _props.cover,
          circleType = _props.circleType;


      var circle = void 0;

      if (circleType === 'field') {
        circle = 'FIELD';
      }

      if (circleType === 'edu') {
        circle = 'EDUCATIONAL INSTITUTE';
      }

      if (circleType === 'org') {
        circle = 'ORGNIZATION';
      }

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { className: classes.card },
          _react2.default.createElement(_Card.CardMedia, { className: classes.cover, image: avatar, title: 'Field' }),
          _react2.default.createElement(
            'div',
            { className: classes.details },
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(
                _reactRouterDom.Link,
                { to: '@' + username, className: classes.link },
                _react2.default.createElement(
                  _Typography2.default,
                  { type: 'headline' },
                  name
                )
              ),
              _react2.default.createElement(
                _reactRouterDom.Link,
                { to: '@' + username, className: classes.link },
                _react2.default.createElement(
                  _Typography2.default,
                  { type: 'subheading' },
                  username
                )
              )
            ),
            _react2.default.createElement(
              _Card.CardActions,
              null,
              _react2.default.createElement(_Chip2.default, { label: circle, className: classes.chip })
            )
          )
        )
      );
    }
  }]);
  return Circle;
}(_react.Component);

Circle.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  id: _propTypes2.default.number.isRequired,
  username: _propTypes2.default.string.isRequired,
  circleType: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  cover: _propTypes2.default.string,
  avatar: _propTypes2.default.string
};

exports.default = (0, _withStyles2.default)(styles)(Circle);

/***/ }),

/***/ "./src/client/containers/CircleTimeline/index.js":
/*!*******************************************************!*\
  !*** ./src/client/containers/CircleTimeline/index.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _intersectionBy = __webpack_require__(/*! lodash/intersectionBy */ "./node_modules/lodash/intersectionBy.js");

var _intersectionBy2 = _interopRequireDefault(_intersectionBy);

var _Circle = __webpack_require__(/*! ../Circle */ "./src/client/containers/Circle/index.js");

var _Circle2 = _interopRequireDefault(_Circle);

var _getAll = __webpack_require__(/*! ../../api/users/circles/getAll */ "./src/client/api/users/circles/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../actions/elements/getAll */ "./src/client/actions/elements/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

var _getAll5 = __webpack_require__(/*! ../../actions/users/circles/getAll */ "./src/client/actions/users/circles/getAll.js");

var _getAll6 = _interopRequireDefault(_getAll5);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      display: 'flex-start',
      paddingLeft: '2px',
      paddingRight: '2px'
      // [theme.breakpoints.down('md')]: {
      //   paddingLeft: '2%',
      //   paddingRight: '2%',
      // },
      // [theme.breakpoints.down('sm')]: {
      //   paddingLeft: '2%',
      //   paddingRight: '2%',
      // },
      // [theme.breakpoints.down('xs')]: {
      //   paddingLeft: '2%',
      //   paddingRight: '2%',
      // },
    },
    gridItem: {
      padding: '4px 2px 2px 2px'
    }
  };
}; /** @format */

var CircleTimeline = function (_Component) {
  (0, _inherits3.default)(CircleTimeline, _Component);

  function CircleTimeline(props) {
    (0, _classCallCheck3.default)(this, CircleTimeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CircleTimeline.__proto__ || (0, _getPrototypeOf2.default)(CircleTimeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(CircleTimeline, [{
    key: 'componentWillMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props$elements$, id, token, _ref2, statusCode, error, payload;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _props$elements$ = this.props.elements[0], id = _props$elements$.id, token = _props$elements$.token;
                _context.next = 3;
                return (0, _getAll2.default)({
                  token: token,
                  userId: id
                });

              case 3:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode !== 200)) {
                  _context.next = 10;
                  break;
                }

                // handle error
                console.log(statusCode, error);
                return _context.abrupt('return');

              case 10:

                this.props.actionGetAllElements(payload);
                this.props.actionGetAllUserCircles({
                  id: id,
                  circles: payload.map(function (c) {
                    return { id: c.id };
                  })
                });

              case 12:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentWillMount() {
        return _ref.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements;

      var user = elements[0];

      var circles = (0, _intersectionBy2.default)(elements, user.circles, 'id');

      return _react2.default.createElement(
        _Grid2.default,
        {
          container: true
          // justify="center"
          // alignContent="center"
          , spacing: 0
          // className={classes.root}
          // alignItems="center"
        },
        typeof circles !== 'undefined' && circles.map(function (c) {
          return _react2.default.createElement(
            _Grid2.default,
            {
              key: c.id,
              item: true,
              xs: 6,
              sm: 6,
              md: 4,
              lg: 4,
              xl: 4,
              className: classes.gridItem
            },
            _react2.default.createElement(_Circle2.default, c)
          );
        })
      );
    }
  }]);
  return CircleTimeline;
}(_react.Component);

CircleTimeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,
  actionGetAllElements: _propTypes2.default.func.isRequired,
  actionGetAllUserCircles: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref3) {
  var elements = _ref3.elements;
  return { elements: elements };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionGetAllElements: _getAll4.default,
    actionGetAllUserCircles: _getAll6.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(CircleTimeline));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlSW50ZXJzZWN0aW9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9sb2Rhc2gvX2Nhc3RBcnJheUxpa2VPYmplY3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL2xvZGFzaC9pbnRlcnNlY3Rpb25CeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbG9kYXNoL2xhc3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9BdmF0YXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0NoaXAvQ2hpcC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQ2hpcC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0NhbmNlbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvZWxlbWVudHMvZ2V0QWxsLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy91c2Vycy9jaXJjbGVzL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS91c2Vycy9jaXJjbGVzL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnN0YW50cy91c2Vycy9jaXJjbGVzLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9DaXJjbGUvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NpcmNsZVRpbWVsaW5lL2luZGV4LmpzIl0sIm5hbWVzIjpbImdldEFsbCIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJ1c2VySWQiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsIlVTRVJfQ0lSQ0xFU19HRVQiLCJzdHlsZXMiLCJ0aGVtZSIsInJvb3QiLCJjYXJkIiwiYm9yZGVyUmFkaXVzIiwiZGlzcGxheSIsImRldGFpbHMiLCJmbGV4RGlyZWN0aW9uIiwiY292ZXIiLCJ3aWR0aCIsInNwYWNpbmciLCJ1bml0IiwiaGVpZ2h0IiwibWFyZ2luIiwiZmxleFNocmluayIsImJhY2tncm91bmRDb2xvciIsInBhbGV0dGUiLCJiYWNrZ3JvdW5kIiwiZGVmYXVsdCIsImNoaXAiLCJsaW5rIiwidGV4dERlY29yYXRpb24iLCJjb2xvciIsImZsZXhHcm93IiwiZmxleCIsIkNpcmNsZSIsInByb3BzIiwib25Nb3VzZUVudGVyIiwic2V0U3RhdGUiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsInN0YXRlIiwibmV4dFN0YXRlIiwibmV4dFByb3AiLCJjbGFzc2VzIiwiaWQiLCJ1c2VybmFtZSIsIm5hbWUiLCJhdmF0YXIiLCJjaXJjbGVUeXBlIiwiY2lyY2xlIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm51bWJlciIsInN0cmluZyIsInBhZGRpbmdMZWZ0IiwicGFkZGluZ1JpZ2h0IiwiZ3JpZEl0ZW0iLCJwYWRkaW5nIiwiQ2lyY2xlVGltZWxpbmUiLCJlbGVtZW50cyIsImxvZyIsImFjdGlvbkdldEFsbEVsZW1lbnRzIiwiYWN0aW9uR2V0QWxsVXNlckNpcmNsZXMiLCJjaXJjbGVzIiwibWFwIiwiYyIsInVzZXIiLCJmdW5jIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxNQUFNO0FBQ2pCLFdBQVcsU0FBUztBQUNwQixXQUFXLFNBQVM7QUFDcEIsYUFBYSxNQUFNO0FBQ25CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQ3pFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsRUFBRTtBQUNiLGFBQWEsYUFBYTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQ2JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxTQUFTO0FBQ3BCLFdBQVcsU0FBUztBQUNwQixhQUFhLE1BQU07QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLFNBQVMsS0FBSyxTQUFTLEdBQUcsU0FBUztBQUN6RCxXQUFXLFNBQVM7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7Ozs7Ozs7Ozs7OztBQzVDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsTUFBTTtBQUNqQixhQUFhLEVBQUU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7O0FDbkJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4RjtBQUM5Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUUsZ0NBQWdDO0FBQ2pHLFNBQVM7QUFDVDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0EsZ0NBQWdDLHVCQUF1QjtBQUN2RDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxvQkFBb0IsVTs7Ozs7Ozs7Ozs7OztBQy9NekU7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4RiwrREFBK0Q7O0FBRTdKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTztBQUNQLHNFQUFzRSxxRUFBcUU7QUFDM0k7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQSxxREFBcUQsa0JBQWtCLFE7Ozs7Ozs7Ozs7Ozs7QUM3VHZFOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVELHNDQUFzQyx1Q0FBdUMsZ0JBQWdCLEU7Ozs7Ozs7Ozs7Ozs7QUNmN0Y7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCx1TEFBdUw7O0FBRXpPO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSx5Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTs7Ozs7Ozs7Ozs7Ozs7QUFjQSxJQUFNQSxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsT0FBRDtBQUFBLFNBQWMsRUFBRUMsNEJBQUYsRUFBc0JELGdCQUF0QixFQUFkO0FBQUEsQ0FBZixDLENBbEJBOztrQkFvQmVELE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmZjs7QUFjQTs7Ozs7Ozs7OztBQVVBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0I7QUFDNUNDLG1DQUQ0QztBQUU1Q0Q7QUFGNEMsR0FBL0I7QUFBQSxDQUFmLEMsQ0E3QkE7Ozs7O2tCQWtDZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzdCZjs7Ozs7Ozs7Ozs7QUFMQTs7O3NGQWdCQTtBQUFBLFFBQXdCRyxLQUF4QixTQUF3QkEsS0FBeEI7QUFBQSxRQUErQkMsTUFBL0IsU0FBK0JBLE1BQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVDLGVBRlYsa0NBRXFDRCxNQUZyQztBQUFBO0FBQUEsbUJBSXNCLCtCQUFNQyxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZU47QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVPLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBZVFELFVBQVUsR0FmbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZ0JhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBaEJiOztBQUFBO0FBQUE7QUFBQSxtQkFzQnVCRixJQUFJSyxJQUFKLEVBdEJ2Qjs7QUFBQTtBQXNCVUEsZ0JBdEJWO0FBQUEsd0VBd0JnQkEsSUF4QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEwQklDLG9CQUFRRixLQUFSOztBQTFCSiw2Q0E0Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBNUJYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlZCxNOzs7OztBQWRmOzs7O0FBQ0E7Ozs7a0JBZ0RlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNuRGY7Ozs7OztBQU1PLElBQU1pQiw4Q0FBbUIsa0JBQXpCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0NQOzs7O0FBQ0E7Ozs7QUFDQTs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFoQkE7Ozs7Ozs7QUFrQkEsSUFBTUMsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNLEVBRG1CO0FBRXpCQyxVQUFNO0FBQ0pDLG9CQUFjLEtBRFY7QUFFSkMsZUFBUztBQUZMLEtBRm1CO0FBTXpCQyxhQUFTO0FBQ1BELGVBQVMsTUFERjtBQUVQRSxxQkFBZTtBQUZSLEtBTmdCO0FBVXpCQyxXQUFPO0FBQ0xDLGFBQU9SLE1BQU1TLE9BQU4sQ0FBY0MsSUFBZCxHQUFxQixFQUR2QjtBQUVMQyxjQUFRWCxNQUFNUyxPQUFOLENBQWNDLElBQWQsR0FBcUIsRUFGeEI7QUFHTEUsY0FBUVosTUFBTVMsT0FBTixDQUFjQyxJQUFkLEdBQXFCLENBSHhCO0FBSUxQLG9CQUFjLEtBSlQ7QUFLTFUsa0JBQVksQ0FMUDtBQU1MQyx1QkFBaUJkLE1BQU1lLE9BQU4sQ0FBY0MsVUFBZCxDQUF5QkM7QUFOckMsS0FWa0I7QUFrQnpCQyxVQUFNO0FBQ0o7QUFESSxLQWxCbUI7QUFxQnpCQyxVQUFNO0FBQ0pDLHNCQUFnQixNQURaO0FBRUosZ0JBQVU7QUFDUkEsd0JBQWdCLE1BRFI7QUFFUkMsZUFBTztBQUZDLE9BRk47QUFNSixtQkFBYTtBQUNYRCx3QkFBZ0IsTUFETDtBQUVYQyxlQUFPO0FBRkksT0FOVDtBQVVKLGlCQUFXO0FBQ1RBLGVBQU87QUFERSxPQVZQO0FBYUosa0JBQVk7QUFDVkEsZUFBTztBQURHO0FBYlIsS0FyQm1CO0FBc0N6QkMsY0FBVTtBQUNSQyxZQUFNO0FBREU7QUF0Q2UsR0FBWjtBQUFBLENBQWY7O0lBMkNNQyxNOzs7QUFDSixrQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLHNJQUNYQSxLQURXOztBQUFBLFVBV25CQyxZQVhtQixHQVdKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBWEk7O0FBQUEsVUFZbkJDLFlBWm1CLEdBWUo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0FaSTs7QUFFakIsVUFBS0UsS0FBTCxHQUFhO0FBQ1hGLGFBQU87QUFESSxLQUFiO0FBRmlCO0FBS2xCOzs7OzBDQUVxQkcsUyxFQUFXQyxRLEVBQVU7QUFDekMsYUFBTyxLQUFLRixLQUFMLEtBQWVDLFNBQWYsSUFBNEIsS0FBS04sS0FBTCxLQUFlTyxRQUFsRDtBQUNEOzs7NkJBS1E7QUFBQSxtQkFTSCxLQUFLUCxLQVRGO0FBQUEsVUFFTFEsT0FGSyxVQUVMQSxPQUZLO0FBQUEsVUFHTEMsRUFISyxVQUdMQSxFQUhLO0FBQUEsVUFJTEMsUUFKSyxVQUlMQSxRQUpLO0FBQUEsVUFLTEMsSUFMSyxVQUtMQSxJQUxLO0FBQUEsVUFNTEMsTUFOSyxVQU1MQSxNQU5LO0FBQUEsVUFPTDlCLEtBUEssVUFPTEEsS0FQSztBQUFBLFVBUUwrQixVQVJLLFVBUUxBLFVBUks7OztBQVdQLFVBQUlDLGVBQUo7O0FBRUEsVUFBSUQsZUFBZSxPQUFuQixFQUE0QjtBQUMxQkMsaUJBQVMsT0FBVDtBQUNEOztBQUVELFVBQUlELGVBQWUsS0FBbkIsRUFBMEI7QUFDeEJDLGlCQUFTLHVCQUFUO0FBQ0Q7O0FBRUQsVUFBSUQsZUFBZSxLQUFuQixFQUEwQjtBQUN4QkMsaUJBQVMsYUFBVDtBQUNEOztBQUVELGFBQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQVdOLFFBQVFoQyxJQURyQjtBQUVFLHdCQUFjLEtBQUt5QixZQUZyQjtBQUdFLHdCQUFjLEtBQUtHO0FBSHJCO0FBS0U7QUFBQTtBQUFBLFlBQU0sV0FBV0ksUUFBUS9CLElBQXpCO0FBQ0UsMkRBQVcsV0FBVytCLFFBQVExQixLQUE5QixFQUFxQyxPQUFPOEIsTUFBNUMsRUFBb0QsT0FBTSxPQUExRCxHQURGO0FBRUU7QUFBQTtBQUFBLGNBQUssV0FBV0osUUFBUTVCLE9BQXhCO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFBQTtBQUFBLGtCQUFNLFVBQVE4QixRQUFkLEVBQTBCLFdBQVdGLFFBQVFkLElBQTdDO0FBQ0U7QUFBQTtBQUFBLG9CQUFZLE1BQUssVUFBakI7QUFBNkJpQjtBQUE3QjtBQURGLGVBREY7QUFJRTtBQUFBO0FBQUEsa0JBQU0sVUFBUUQsUUFBZCxFQUEwQixXQUFXRixRQUFRZCxJQUE3QztBQUNFO0FBQUE7QUFBQSxvQkFBWSxNQUFLLFlBQWpCO0FBQStCZ0I7QUFBL0I7QUFERjtBQUpGLGFBREY7QUFTRTtBQUFBO0FBQUE7QUFDRSw4REFBTSxPQUFPSSxNQUFiLEVBQXFCLFdBQVdOLFFBQVFmLElBQXhDO0FBREY7QUFURjtBQUZGO0FBTEYsT0FERjtBQXdCRDs7Ozs7QUFHSE0sT0FBT2dCLFNBQVAsR0FBbUI7QUFDakJQLFdBQVMsb0JBQVVRLE1BQVYsQ0FBaUJDLFVBRFQ7QUFFakJSLE1BQUksb0JBQVVTLE1BQVYsQ0FBaUJELFVBRko7QUFHakJQLFlBQVUsb0JBQVVTLE1BQVYsQ0FBaUJGLFVBSFY7QUFJakJKLGNBQVksb0JBQVVNLE1BQVYsQ0FBaUJGLFVBSlo7QUFLakJOLFFBQU0sb0JBQVVRLE1BQVYsQ0FBaUJGLFVBTE47QUFNakJuQyxTQUFPLG9CQUFVcUMsTUFOQTtBQU9qQlAsVUFBUSxvQkFBVU87QUFQRCxDQUFuQjs7a0JBVWUsMEJBQVc3QyxNQUFYLEVBQW1CeUIsTUFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4SWY7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFNekIsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pHLGVBQVMsWUFETDtBQUVKeUMsbUJBQWEsS0FGVDtBQUdKQyxvQkFBYztBQUNkO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWZJLEtBRG1CO0FBa0J6QkMsY0FBVTtBQUNSQyxlQUFTO0FBREQ7QUFsQmUsR0FBWjtBQUFBLENBQWYsQyxDQXBCQTs7SUEyQ01DLGM7OztBQUNKLDBCQUFZeEIsS0FBWixFQUFtQjtBQUFBOztBQUFBLHNKQUNYQSxLQURXOztBQUVqQixVQUFLSyxLQUFMLEdBQWEsRUFBYjtBQUZpQjtBQUdsQjs7Ozs7Ozs7Ozs7O21DQUd1QixLQUFLTCxLQUFMLENBQVd5QixRQUFYLENBQW9CLENBQXBCLEMsRUFBZGhCLEUsb0JBQUFBLEUsRUFBSWxELEssb0JBQUFBLEs7O3VCQUVpQyxzQkFBa0I7QUFDN0RBLDhCQUQ2RDtBQUU3REMsMEJBQVFpRDtBQUZxRCxpQkFBbEIsQzs7OztBQUFyQ3hDLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPYix1QixTQUFBQSxPOztzQkFLdkJZLGVBQWUsRzs7Ozs7QUFDakI7QUFDQUcsd0JBQVFzRCxHQUFSLENBQVl6RCxVQUFaLEVBQXdCQyxLQUF4Qjs7Ozs7QUFJRixxQkFBSzhCLEtBQUwsQ0FBVzJCLG9CQUFYLENBQWdDdEUsT0FBaEM7QUFDQSxxQkFBSzJDLEtBQUwsQ0FBVzRCLHVCQUFYLENBQW1DO0FBQ2pDbkIsd0JBRGlDO0FBRWpDb0IsMkJBQVN4RSxRQUFReUUsR0FBUixDQUFZLFVBQUNDLENBQUQ7QUFBQSwyQkFBUSxFQUFFdEIsSUFBSXNCLEVBQUV0QixFQUFSLEVBQVI7QUFBQSxtQkFBWjtBQUZ3QixpQkFBbkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFNTztBQUFBLG1CQUN1QixLQUFLVCxLQUQ1QjtBQUFBLFVBQ0NRLE9BREQsVUFDQ0EsT0FERDtBQUFBLFVBQ1VpQixRQURWLFVBQ1VBLFFBRFY7O0FBRVAsVUFBTU8sT0FBT1AsU0FBUyxDQUFULENBQWI7O0FBRUEsVUFBTUksVUFBVSw4QkFBZUosUUFBZixFQUF5Qk8sS0FBS0gsT0FBOUIsRUFBdUMsSUFBdkMsQ0FBaEI7O0FBRUEsYUFDRTtBQUFBO0FBQUE7QUFDRTtBQUNBO0FBQ0E7QUFIRixZQUlFLFNBQVM7QUFDVDtBQUNBO0FBTkY7QUFRRyxlQUFPQSxPQUFQLEtBQW1CLFdBQW5CLElBQ0NBLFFBQVFDLEdBQVIsQ0FBWSxVQUFDQyxDQUFEO0FBQUEsaUJBQ1Y7QUFBQTtBQUFBO0FBQ0UsbUJBQUtBLEVBQUV0QixFQURUO0FBRUUsd0JBRkY7QUFHRSxrQkFBSSxDQUhOO0FBSUUsa0JBQUksQ0FKTjtBQUtFLGtCQUFJLENBTE47QUFNRSxrQkFBSSxDQU5OO0FBT0Usa0JBQUksQ0FQTjtBQVFFLHlCQUFXRCxRQUFRYztBQVJyQjtBQVVFLDREQUFZUyxDQUFaO0FBVkYsV0FEVTtBQUFBLFNBQVo7QUFUSixPQURGO0FBMEJEOzs7OztBQUdIUCxlQUFlVCxTQUFmLEdBQTJCO0FBQ3pCUCxXQUFTLG9CQUFVUSxNQUFWLENBQWlCQyxVQUREO0FBRXpCUSxZQUFVLG9CQUFVVCxNQUFWLENBQWlCQyxVQUZGO0FBR3pCVSx3QkFBc0Isb0JBQVVNLElBQVYsQ0FBZWhCLFVBSFo7QUFJekJXLDJCQUF5QixvQkFBVUssSUFBVixDQUFlaEI7QUFKZixDQUEzQjs7QUFPQSxJQUFNaUIsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdULFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7QUFDQSxJQUFNVSxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDQyxRQUFEO0FBQUEsU0FDekIsK0JBQ0U7QUFDRVQsMENBREY7QUFFRUM7QUFGRixHQURGLEVBS0VRLFFBTEYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBU2UseUJBQVFGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXN0QsTUFBWCxFQUFtQmtELGNBQW5CLENBRGEsQyIsImZpbGUiOiI1NC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgU2V0Q2FjaGUgPSByZXF1aXJlKCcuL19TZXRDYWNoZScpLFxuICAgIGFycmF5SW5jbHVkZXMgPSByZXF1aXJlKCcuL19hcnJheUluY2x1ZGVzJyksXG4gICAgYXJyYXlJbmNsdWRlc1dpdGggPSByZXF1aXJlKCcuL19hcnJheUluY2x1ZGVzV2l0aCcpLFxuICAgIGFycmF5TWFwID0gcmVxdWlyZSgnLi9fYXJyYXlNYXAnKSxcbiAgICBiYXNlVW5hcnkgPSByZXF1aXJlKCcuL19iYXNlVW5hcnknKSxcbiAgICBjYWNoZUhhcyA9IHJlcXVpcmUoJy4vX2NhY2hlSGFzJyk7XG5cbi8qIEJ1aWx0LWluIG1ldGhvZCByZWZlcmVuY2VzIGZvciB0aG9zZSB3aXRoIHRoZSBzYW1lIG5hbWUgYXMgb3RoZXIgYGxvZGFzaGAgbWV0aG9kcy4gKi9cbnZhciBuYXRpdmVNaW4gPSBNYXRoLm1pbjtcblxuLyoqXG4gKiBUaGUgYmFzZSBpbXBsZW1lbnRhdGlvbiBvZiBtZXRob2RzIGxpa2UgYF8uaW50ZXJzZWN0aW9uYCwgd2l0aG91dCBzdXBwb3J0XG4gKiBmb3IgaXRlcmF0ZWUgc2hvcnRoYW5kcywgdGhhdCBhY2NlcHRzIGFuIGFycmF5IG9mIGFycmF5cyB0byBpbnNwZWN0LlxuICpcbiAqIEBwcml2YXRlXG4gKiBAcGFyYW0ge0FycmF5fSBhcnJheXMgVGhlIGFycmF5cyB0byBpbnNwZWN0LlxuICogQHBhcmFtIHtGdW5jdGlvbn0gW2l0ZXJhdGVlXSBUaGUgaXRlcmF0ZWUgaW52b2tlZCBwZXIgZWxlbWVudC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtjb21wYXJhdG9yXSBUaGUgY29tcGFyYXRvciBpbnZva2VkIHBlciBlbGVtZW50LlxuICogQHJldHVybnMge0FycmF5fSBSZXR1cm5zIHRoZSBuZXcgYXJyYXkgb2Ygc2hhcmVkIHZhbHVlcy5cbiAqL1xuZnVuY3Rpb24gYmFzZUludGVyc2VjdGlvbihhcnJheXMsIGl0ZXJhdGVlLCBjb21wYXJhdG9yKSB7XG4gIHZhciBpbmNsdWRlcyA9IGNvbXBhcmF0b3IgPyBhcnJheUluY2x1ZGVzV2l0aCA6IGFycmF5SW5jbHVkZXMsXG4gICAgICBsZW5ndGggPSBhcnJheXNbMF0ubGVuZ3RoLFxuICAgICAgb3RoTGVuZ3RoID0gYXJyYXlzLmxlbmd0aCxcbiAgICAgIG90aEluZGV4ID0gb3RoTGVuZ3RoLFxuICAgICAgY2FjaGVzID0gQXJyYXkob3RoTGVuZ3RoKSxcbiAgICAgIG1heExlbmd0aCA9IEluZmluaXR5LFxuICAgICAgcmVzdWx0ID0gW107XG5cbiAgd2hpbGUgKG90aEluZGV4LS0pIHtcbiAgICB2YXIgYXJyYXkgPSBhcnJheXNbb3RoSW5kZXhdO1xuICAgIGlmIChvdGhJbmRleCAmJiBpdGVyYXRlZSkge1xuICAgICAgYXJyYXkgPSBhcnJheU1hcChhcnJheSwgYmFzZVVuYXJ5KGl0ZXJhdGVlKSk7XG4gICAgfVxuICAgIG1heExlbmd0aCA9IG5hdGl2ZU1pbihhcnJheS5sZW5ndGgsIG1heExlbmd0aCk7XG4gICAgY2FjaGVzW290aEluZGV4XSA9ICFjb21wYXJhdG9yICYmIChpdGVyYXRlZSB8fCAobGVuZ3RoID49IDEyMCAmJiBhcnJheS5sZW5ndGggPj0gMTIwKSlcbiAgICAgID8gbmV3IFNldENhY2hlKG90aEluZGV4ICYmIGFycmF5KVxuICAgICAgOiB1bmRlZmluZWQ7XG4gIH1cbiAgYXJyYXkgPSBhcnJheXNbMF07XG5cbiAgdmFyIGluZGV4ID0gLTEsXG4gICAgICBzZWVuID0gY2FjaGVzWzBdO1xuXG4gIG91dGVyOlxuICB3aGlsZSAoKytpbmRleCA8IGxlbmd0aCAmJiByZXN1bHQubGVuZ3RoIDwgbWF4TGVuZ3RoKSB7XG4gICAgdmFyIHZhbHVlID0gYXJyYXlbaW5kZXhdLFxuICAgICAgICBjb21wdXRlZCA9IGl0ZXJhdGVlID8gaXRlcmF0ZWUodmFsdWUpIDogdmFsdWU7XG5cbiAgICB2YWx1ZSA9IChjb21wYXJhdG9yIHx8IHZhbHVlICE9PSAwKSA/IHZhbHVlIDogMDtcbiAgICBpZiAoIShzZWVuXG4gICAgICAgICAgPyBjYWNoZUhhcyhzZWVuLCBjb21wdXRlZClcbiAgICAgICAgICA6IGluY2x1ZGVzKHJlc3VsdCwgY29tcHV0ZWQsIGNvbXBhcmF0b3IpXG4gICAgICAgICkpIHtcbiAgICAgIG90aEluZGV4ID0gb3RoTGVuZ3RoO1xuICAgICAgd2hpbGUgKC0tb3RoSW5kZXgpIHtcbiAgICAgICAgdmFyIGNhY2hlID0gY2FjaGVzW290aEluZGV4XTtcbiAgICAgICAgaWYgKCEoY2FjaGVcbiAgICAgICAgICAgICAgPyBjYWNoZUhhcyhjYWNoZSwgY29tcHV0ZWQpXG4gICAgICAgICAgICAgIDogaW5jbHVkZXMoYXJyYXlzW290aEluZGV4XSwgY29tcHV0ZWQsIGNvbXBhcmF0b3IpKVxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgY29udGludWUgb3V0ZXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIGlmIChzZWVuKSB7XG4gICAgICAgIHNlZW4ucHVzaChjb21wdXRlZCk7XG4gICAgICB9XG4gICAgICByZXN1bHQucHVzaCh2YWx1ZSk7XG4gICAgfVxuICB9XG4gIHJldHVybiByZXN1bHQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gYmFzZUludGVyc2VjdGlvbjtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9fYmFzZUludGVyc2VjdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbG9kYXNoL19iYXNlSW50ZXJzZWN0aW9uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNTQiLCJ2YXIgaXNBcnJheUxpa2VPYmplY3QgPSByZXF1aXJlKCcuL2lzQXJyYXlMaWtlT2JqZWN0Jyk7XG5cbi8qKlxuICogQ2FzdHMgYHZhbHVlYCB0byBhbiBlbXB0eSBhcnJheSBpZiBpdCdzIG5vdCBhbiBhcnJheSBsaWtlIG9iamVjdC5cbiAqXG4gKiBAcHJpdmF0ZVxuICogQHBhcmFtIHsqfSB2YWx1ZSBUaGUgdmFsdWUgdG8gaW5zcGVjdC5cbiAqIEByZXR1cm5zIHtBcnJheXxPYmplY3R9IFJldHVybnMgdGhlIGNhc3QgYXJyYXktbGlrZSBvYmplY3QuXG4gKi9cbmZ1bmN0aW9uIGNhc3RBcnJheUxpa2VPYmplY3QodmFsdWUpIHtcbiAgcmV0dXJuIGlzQXJyYXlMaWtlT2JqZWN0KHZhbHVlKSA/IHZhbHVlIDogW107XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY2FzdEFycmF5TGlrZU9iamVjdDtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9fY2FzdEFycmF5TGlrZU9iamVjdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbG9kYXNoL19jYXN0QXJyYXlMaWtlT2JqZWN0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNTQiLCJ2YXIgYXJyYXlNYXAgPSByZXF1aXJlKCcuL19hcnJheU1hcCcpLFxuICAgIGJhc2VJbnRlcnNlY3Rpb24gPSByZXF1aXJlKCcuL19iYXNlSW50ZXJzZWN0aW9uJyksXG4gICAgYmFzZUl0ZXJhdGVlID0gcmVxdWlyZSgnLi9fYmFzZUl0ZXJhdGVlJyksXG4gICAgYmFzZVJlc3QgPSByZXF1aXJlKCcuL19iYXNlUmVzdCcpLFxuICAgIGNhc3RBcnJheUxpa2VPYmplY3QgPSByZXF1aXJlKCcuL19jYXN0QXJyYXlMaWtlT2JqZWN0JyksXG4gICAgbGFzdCA9IHJlcXVpcmUoJy4vbGFzdCcpO1xuXG4vKipcbiAqIFRoaXMgbWV0aG9kIGlzIGxpa2UgYF8uaW50ZXJzZWN0aW9uYCBleGNlcHQgdGhhdCBpdCBhY2NlcHRzIGBpdGVyYXRlZWBcbiAqIHdoaWNoIGlzIGludm9rZWQgZm9yIGVhY2ggZWxlbWVudCBvZiBlYWNoIGBhcnJheXNgIHRvIGdlbmVyYXRlIHRoZSBjcml0ZXJpb25cbiAqIGJ5IHdoaWNoIHRoZXkncmUgY29tcGFyZWQuIFRoZSBvcmRlciBhbmQgcmVmZXJlbmNlcyBvZiByZXN1bHQgdmFsdWVzIGFyZVxuICogZGV0ZXJtaW5lZCBieSB0aGUgZmlyc3QgYXJyYXkuIFRoZSBpdGVyYXRlZSBpcyBpbnZva2VkIHdpdGggb25lIGFyZ3VtZW50OlxuICogKHZhbHVlKS5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDQuMC4wXG4gKiBAY2F0ZWdvcnkgQXJyYXlcbiAqIEBwYXJhbSB7Li4uQXJyYXl9IFthcnJheXNdIFRoZSBhcnJheXMgdG8gaW5zcGVjdC5cbiAqIEBwYXJhbSB7RnVuY3Rpb259IFtpdGVyYXRlZT1fLmlkZW50aXR5XSBUaGUgaXRlcmF0ZWUgaW52b2tlZCBwZXIgZWxlbWVudC5cbiAqIEByZXR1cm5zIHtBcnJheX0gUmV0dXJucyB0aGUgbmV3IGFycmF5IG9mIGludGVyc2VjdGluZyB2YWx1ZXMuXG4gKiBAZXhhbXBsZVxuICpcbiAqIF8uaW50ZXJzZWN0aW9uQnkoWzIuMSwgMS4yXSwgWzIuMywgMy40XSwgTWF0aC5mbG9vcik7XG4gKiAvLyA9PiBbMi4xXVxuICpcbiAqIC8vIFRoZSBgXy5wcm9wZXJ0eWAgaXRlcmF0ZWUgc2hvcnRoYW5kLlxuICogXy5pbnRlcnNlY3Rpb25CeShbeyAneCc6IDEgfV0sIFt7ICd4JzogMiB9LCB7ICd4JzogMSB9XSwgJ3gnKTtcbiAqIC8vID0+IFt7ICd4JzogMSB9XVxuICovXG52YXIgaW50ZXJzZWN0aW9uQnkgPSBiYXNlUmVzdChmdW5jdGlvbihhcnJheXMpIHtcbiAgdmFyIGl0ZXJhdGVlID0gbGFzdChhcnJheXMpLFxuICAgICAgbWFwcGVkID0gYXJyYXlNYXAoYXJyYXlzLCBjYXN0QXJyYXlMaWtlT2JqZWN0KTtcblxuICBpZiAoaXRlcmF0ZWUgPT09IGxhc3QobWFwcGVkKSkge1xuICAgIGl0ZXJhdGVlID0gdW5kZWZpbmVkO1xuICB9IGVsc2Uge1xuICAgIG1hcHBlZC5wb3AoKTtcbiAgfVxuICByZXR1cm4gKG1hcHBlZC5sZW5ndGggJiYgbWFwcGVkWzBdID09PSBhcnJheXNbMF0pXG4gICAgPyBiYXNlSW50ZXJzZWN0aW9uKG1hcHBlZCwgYmFzZUl0ZXJhdGVlKGl0ZXJhdGVlLCAyKSlcbiAgICA6IFtdO1xufSk7XG5cbm1vZHVsZS5leHBvcnRzID0gaW50ZXJzZWN0aW9uQnk7XG5cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9sb2Rhc2gvaW50ZXJzZWN0aW9uQnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9pbnRlcnNlY3Rpb25CeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDU0IiwiLyoqXG4gKiBHZXRzIHRoZSBsYXN0IGVsZW1lbnQgb2YgYGFycmF5YC5cbiAqXG4gKiBAc3RhdGljXG4gKiBAbWVtYmVyT2YgX1xuICogQHNpbmNlIDAuMS4wXG4gKiBAY2F0ZWdvcnkgQXJyYXlcbiAqIEBwYXJhbSB7QXJyYXl9IGFycmF5IFRoZSBhcnJheSB0byBxdWVyeS5cbiAqIEByZXR1cm5zIHsqfSBSZXR1cm5zIHRoZSBsYXN0IGVsZW1lbnQgb2YgYGFycmF5YC5cbiAqIEBleGFtcGxlXG4gKlxuICogXy5sYXN0KFsxLCAyLCAzXSk7XG4gKiAvLyA9PiAzXG4gKi9cbmZ1bmN0aW9uIGxhc3QoYXJyYXkpIHtcbiAgdmFyIGxlbmd0aCA9IGFycmF5ID09IG51bGwgPyAwIDogYXJyYXkubGVuZ3RoO1xuICByZXR1cm4gbGVuZ3RoID8gYXJyYXlbbGVuZ3RoIC0gMV0gOiB1bmRlZmluZWQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGFzdDtcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL2xvZGFzaC9sYXN0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9sb2Rhc2gvbGFzdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDU0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9jb2xvck1hbmlwdWxhdG9yID0gcmVxdWlyZSgnLi4vc3R5bGVzL2NvbG9yTWFuaXB1bGF0b3InKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgZmxleFNocmluazogMCxcbiAgICAgIHdpZHRoOiA0MCxcbiAgICAgIGhlaWdodDogNDAsXG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDIwKSxcbiAgICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbicsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZSdcbiAgICB9LFxuICAgIGNvbG9yRGVmYXVsdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LFxuICAgICAgYmFja2dyb3VuZENvbG9yOiAoMCwgX2NvbG9yTWFuaXB1bGF0b3IuZW1waGFzaXplKSh0aGVtZS5wYWxldHRlLmJhY2tncm91bmQuZGVmYXVsdCwgMC4yNilcbiAgICB9LFxuICAgIGltZzoge1xuICAgICAgbWF4V2lkdGg6ICcxMDAlJyxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcidcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VkIGluIGNvbWJpbmF0aW9uIHdpdGggYHNyY2Agb3IgYHNyY1NldGAgdG9cbiAgICogcHJvdmlkZSBhbiBhbHQgYXR0cmlidXRlIGZvciB0aGUgcmVuZGVyZWQgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIGFsdDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZCB0byByZW5kZXIgaWNvbiBvciB0ZXh0IGVsZW1lbnRzIGluc2lkZSB0aGUgQXZhdGFyLlxuICAgKiBgc3JjYCBhbmQgYGFsdGAgcHJvcHMgd2lsbCBub3QgYmUgdXNlZCBhbmQgbm8gYGltZ2Agd2lsbFxuICAgKiBiZSByZW5kZXJlZCBieSBkZWZhdWx0LlxuICAgKlxuICAgKiBUaGlzIGNhbiBiZSBhbiBlbGVtZW50LCBvciBqdXN0IGEgc3RyaW5nLlxuICAgKi9cbiAgY2hpbGRyZW46IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsIHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50ID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50KV0pLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIFRoZSBjbGFzc05hbWUgb2YgdGhlIGNoaWxkIGVsZW1lbnQuXG4gICAqIFVzZWQgYnkgQ2hpcCBhbmQgTGlzdEl0ZW1JY29uIHRvIHN0eWxlIHRoZSBBdmF0YXIgaWNvbi5cbiAgICovXG4gIGNoaWxkcmVuQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb21wb25lbnQgdXNlZCBmb3IgdGhlIHJvb3Qgbm9kZS5cbiAgICogRWl0aGVyIGEgc3RyaW5nIHRvIHVzZSBhIERPTSBlbGVtZW50IG9yIGEgY29tcG9uZW50LlxuICAgKi9cbiAgY29tcG9uZW50OiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBpbWdgIGVsZW1lbnQgd2hlbiB0aGUgY29tcG9uZW50XG4gICAqIGlzIHVzZWQgdG8gZGlzcGxheSBhbiBpbWFnZS5cbiAgICovXG4gIGltZ1Byb3BzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgYHNpemVzYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc2l6ZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBgc3JjYCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc3JjOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgYHNyY1NldGAgYXR0cmlidXRlIGZvciB0aGUgYGltZ2AgZWxlbWVudC5cbiAgICovXG4gIHNyY1NldDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ1xufTtcblxudmFyIEF2YXRhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEF2YXRhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQXZhdGFyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEF2YXRhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEF2YXRhci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQXZhdGFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShBdmF0YXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYWx0ID0gX3Byb3BzLmFsdCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jaGlsZHJlbkNsYXNzTmFtZSxcbiAgICAgICAgICBDb21wb25lbnRQcm9wID0gX3Byb3BzLmNvbXBvbmVudCxcbiAgICAgICAgICBpbWdQcm9wcyA9IF9wcm9wcy5pbWdQcm9wcyxcbiAgICAgICAgICBzaXplcyA9IF9wcm9wcy5zaXplcyxcbiAgICAgICAgICBzcmMgPSBfcHJvcHMuc3JjLFxuICAgICAgICAgIHNyY1NldCA9IF9wcm9wcy5zcmNTZXQsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYWx0JywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NoaWxkcmVuJywgJ2NoaWxkcmVuQ2xhc3NOYW1lJywgJ2NvbXBvbmVudCcsICdpbWdQcm9wcycsICdzaXplcycsICdzcmMnLCAnc3JjU2V0J10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuY29sb3JEZWZhdWx0LCBjaGlsZHJlblByb3AgJiYgIXNyYyAmJiAhc3JjU2V0KSwgY2xhc3NOYW1lUHJvcCk7XG4gICAgICB2YXIgY2hpbGRyZW4gPSBudWxsO1xuXG4gICAgICBpZiAoY2hpbGRyZW5Qcm9wKSB7XG4gICAgICAgIGlmIChjaGlsZHJlbkNsYXNzTmFtZVByb3AgJiYgdHlwZW9mIGNoaWxkcmVuUHJvcCAhPT0gJ3N0cmluZycgJiYgX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGNoaWxkcmVuUHJvcCkpIHtcbiAgICAgICAgICB2YXIgX2NoaWxkcmVuQ2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjaGlsZHJlbkNsYXNzTmFtZVByb3AsIGNoaWxkcmVuUHJvcC5wcm9wcy5jbGFzc05hbWUpO1xuICAgICAgICAgIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZHJlblByb3AsIHsgY2xhc3NOYW1lOiBfY2hpbGRyZW5DbGFzc05hbWUgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgY2hpbGRyZW4gPSBjaGlsZHJlblByb3A7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSBpZiAoc3JjIHx8IHNyY1NldCkge1xuICAgICAgICBjaGlsZHJlbiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdpbWcnLCAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhbHQ6IGFsdCxcbiAgICAgICAgICBzcmM6IHNyYyxcbiAgICAgICAgICBzcmNTZXQ6IHNyY1NldCxcbiAgICAgICAgICBzaXplczogc2l6ZXMsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc2VzLmltZ1xuICAgICAgICB9LCBpbWdQcm9wcykpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIENvbXBvbmVudFByb3AsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9LCBvdGhlciksXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQXZhdGFyO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuQXZhdGFyLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29tcG9uZW50OiAnZGl2J1xufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlBdmF0YXInIH0pKEF2YXRhcik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNCA2IDI0IDI2IDI3IDI4IDMxIDMzIDM0IDM1IDM2IDM3IDM5IDQwIDQzIDQ5IDU0IDU3IDU4IDU5IDYxIDYzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfa2V5Y29kZSA9IHJlcXVpcmUoJ2tleWNvZGUnKTtcblxudmFyIF9rZXljb2RlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2tleWNvZGUpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQ2FuY2VsID0gcmVxdWlyZSgnLi4vc3ZnLWljb25zL0NhbmNlbCcpO1xuXG52YXIgX0NhbmNlbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9DYW5jZWwpO1xuXG52YXIgX2NvbG9yTWFuaXB1bGF0b3IgPSByZXF1aXJlKCcuLi9zdHlsZXMvY29sb3JNYW5pcHVsYXRvcicpO1xuXG52YXIgX0F2YXRhciA9IHJlcXVpcmUoJy4uL0F2YXRhci9BdmF0YXInKTtcblxudmFyIF9BdmF0YXIyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQXZhdGFyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHZhciBoZWlnaHQgPSAzMjtcbiAgdmFyIGJhY2tncm91bmRDb2xvciA9ICgwLCBfY29sb3JNYW5pcHVsYXRvci5lbXBoYXNpemUpKHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LCAwLjEyKTtcbiAgdmFyIGRlbGV0ZUljb25Db2xvciA9ICgwLCBfY29sb3JNYW5pcHVsYXRvci5mYWRlKSh0aGVtZS5wYWxldHRlLnRleHQucHJpbWFyeSwgMC4yNik7XG5cbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBmb250RmFtaWx5OiB0aGVtZS50eXBvZ3JhcGh5LmZvbnRGYW1pbHksXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDEzKSxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgaGVpZ2h0OiBoZWlnaHQsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQoYmFja2dyb3VuZENvbG9yKSxcbiAgICAgIGJhY2tncm91bmRDb2xvcjogYmFja2dyb3VuZENvbG9yLFxuICAgICAgYm9yZGVyUmFkaXVzOiBoZWlnaHQgLyAyLFxuICAgICAgd2hpdGVTcGFjZTogJ25vd3JhcCcsXG4gICAgICB3aWR0aDogJ2ZpdC1jb250ZW50JyxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgpLFxuICAgICAgLy8gbGFiZWwgd2lsbCBpbmhlcml0IHRoaXMgZnJvbSByb290LCB0aGVuIGBjbGlja2FibGVgIGNsYXNzIG92ZXJyaWRlcyB0aGlzIGZvciBib3RoXG4gICAgICBjdXJzb3I6ICdkZWZhdWx0JyxcbiAgICAgIG91dGxpbmU6ICdub25lJywgLy8gTm8gb3V0bGluZSBvbiBmb2N1c2VkIGVsZW1lbnQgaW4gQ2hyb21lIChhcyB0cmlnZ2VyZWQgYnkgdGFiSW5kZXggcHJvcClcbiAgICAgIGJvcmRlcjogJ25vbmUnLCAvLyBSZW1vdmUgYGJ1dHRvbmAgYm9yZGVyXG4gICAgICBwYWRkaW5nOiAwIC8vIFJlbW92ZSBgYnV0dG9uYCBwYWRkaW5nXG4gICAgfSxcbiAgICBjbGlja2FibGU6IHtcbiAgICAgIC8vIFJlbW92ZSBncmV5IGhpZ2hsaWdodFxuICAgICAgV2Via2l0VGFwSGlnaGxpZ2h0Q29sb3I6IHRoZW1lLnBhbGV0dGUuY29tbW9uLnRyYW5zcGFyZW50LFxuICAgICAgY3Vyc29yOiAncG9pbnRlcicsXG4gICAgICAnJjpob3ZlciwgJjpmb2N1cyc6IHtcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiAoMCwgX2NvbG9yTWFuaXB1bGF0b3IuZW1waGFzaXplKShiYWNrZ3JvdW5kQ29sb3IsIDAuMDgpXG4gICAgICB9LFxuICAgICAgJyY6YWN0aXZlJzoge1xuICAgICAgICBib3hTaGFkb3c6IHRoZW1lLnNoYWRvd3NbMV0sXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogKDAsIF9jb2xvck1hbmlwdWxhdG9yLmVtcGhhc2l6ZSkoYmFja2dyb3VuZENvbG9yLCAwLjEyKVxuICAgICAgfVxuICAgIH0sXG4gICAgZGVsZXRhYmxlOiB7XG4gICAgICAnJjpmb2N1cyc6IHtcbiAgICAgICAgYmFja2dyb3VuZENvbG9yOiAoMCwgX2NvbG9yTWFuaXB1bGF0b3IuZW1waGFzaXplKShiYWNrZ3JvdW5kQ29sb3IsIDAuMDgpXG4gICAgICB9XG4gICAgfSxcbiAgICBhdmF0YXI6IHtcbiAgICAgIG1hcmdpblJpZ2h0OiAtNCxcbiAgICAgIHdpZHRoOiAzMixcbiAgICAgIGhlaWdodDogMzIsXG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKDE2KVxuICAgIH0sXG4gICAgYXZhdGFyQ2hpbGRyZW46IHtcbiAgICAgIHdpZHRoOiAxOSxcbiAgICAgIGhlaWdodDogMTlcbiAgICB9LFxuICAgIGxhYmVsOiB7XG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnY2VudGVyJyxcbiAgICAgIHBhZGRpbmdMZWZ0OiAxMixcbiAgICAgIHBhZGRpbmdSaWdodDogMTIsXG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZScsXG4gICAgICB3aGl0ZVNwYWNlOiAnbm93cmFwJyxcbiAgICAgIGN1cnNvcjogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBkZWxldGVJY29uOiB7XG4gICAgICAvLyBSZW1vdmUgZ3JleSBoaWdobGlnaHRcbiAgICAgIFdlYmtpdFRhcEhpZ2hsaWdodENvbG9yOiB0aGVtZS5wYWxldHRlLmNvbW1vbi50cmFuc3BhcmVudCxcbiAgICAgIGNvbG9yOiBkZWxldGVJY29uQ29sb3IsXG4gICAgICBjdXJzb3I6ICdwb2ludGVyJyxcbiAgICAgIGhlaWdodDogJ2F1dG8nLFxuICAgICAgbWFyZ2luOiAnMCA0cHggMCAtOHB4JyxcbiAgICAgICcmOmhvdmVyJzoge1xuICAgICAgICBjb2xvcjogKDAsIF9jb2xvck1hbmlwdWxhdG9yLmZhZGUpKGRlbGV0ZUljb25Db2xvciwgMC40KVxuICAgICAgfVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIEF2YXRhciBlbGVtZW50LlxuICAgKi9cbiAgYXZhdGFyOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQ3VzdG9tIGRlbGV0ZSBpY29uLiBXaWxsIGJlIHNob3duIG9ubHkgaWYgYG9uUmVxdWVzdERlbGV0ZWAgaXMgc2V0LlxuICAgKi9cbiAgZGVsZXRlSWNvbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpLFxuXG4gIC8qKlxuICAgKiBUaGUgY29udGVudCBvZiB0aGUgbGFiZWwuXG4gICAqL1xuICBsYWJlbDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkNsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25LZXlEb3duOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQ2FsbGJhY2sgZnVuY3Rpb24gZmlyZWQgd2hlbiB0aGUgZGVsZXRlIGljb24gaXMgY2xpY2tlZC5cbiAgICogSWYgc2V0LCB0aGUgZGVsZXRlIGljb24gd2lsbCBiZSBzaG93bi5cbiAgICovXG4gIG9uUmVxdWVzdERlbGV0ZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHRhYkluZGV4OiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXSlcbn07XG5cbi8qKlxuICogQ2hpcHMgcmVwcmVzZW50IGNvbXBsZXggZW50aXRpZXMgaW4gc21hbGwgYmxvY2tzLCBzdWNoIGFzIGEgY29udGFjdC5cbiAqL1xudmFyIENoaXAgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShDaGlwLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBDaGlwKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIENoaXApO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChfcmVmID0gQ2hpcC5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQ2hpcCkpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmNoaXBSZWYgPSBudWxsLCBfdGhpcy5oYW5kbGVEZWxldGVJY29uQ2xpY2sgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgIC8vIFN0b3AgdGhlIGV2ZW50IGZyb20gYnViYmxpbmcgdXAgdG8gdGhlIGBDaGlwYFxuICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICB2YXIgb25SZXF1ZXN0RGVsZXRlID0gX3RoaXMucHJvcHMub25SZXF1ZXN0RGVsZXRlO1xuXG4gICAgICBpZiAob25SZXF1ZXN0RGVsZXRlKSB7XG4gICAgICAgIG9uUmVxdWVzdERlbGV0ZShldmVudCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlS2V5RG93biA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgb25DbGljayA9IF90aGlzJHByb3BzLm9uQ2xpY2ssXG4gICAgICAgICAgb25SZXF1ZXN0RGVsZXRlID0gX3RoaXMkcHJvcHMub25SZXF1ZXN0RGVsZXRlLFxuICAgICAgICAgIG9uS2V5RG93biA9IF90aGlzJHByb3BzLm9uS2V5RG93bjtcblxuICAgICAgdmFyIGtleSA9ICgwLCBfa2V5Y29kZTIuZGVmYXVsdCkoZXZlbnQpO1xuXG4gICAgICBpZiAob25DbGljayAmJiAoa2V5ID09PSAnc3BhY2UnIHx8IGtleSA9PT0gJ2VudGVyJykpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgb25DbGljayhldmVudCk7XG4gICAgICB9IGVsc2UgaWYgKG9uUmVxdWVzdERlbGV0ZSAmJiBrZXkgPT09ICdiYWNrc3BhY2UnKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIG9uUmVxdWVzdERlbGV0ZShldmVudCk7XG4gICAgICB9IGVsc2UgaWYgKGtleSA9PT0gJ2VzYycpIHtcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgaWYgKF90aGlzLmNoaXBSZWYpIHtcbiAgICAgICAgICBfdGhpcy5jaGlwUmVmLmJsdXIoKTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBpZiAob25LZXlEb3duKSB7XG4gICAgICAgIG9uS2V5RG93bihldmVudCk7XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKENoaXAsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGF2YXRhclByb3AgPSBfcHJvcHMuYXZhdGFyLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBsYWJlbCA9IF9wcm9wcy5sYWJlbCxcbiAgICAgICAgICBvbkNsaWNrID0gX3Byb3BzLm9uQ2xpY2ssXG4gICAgICAgICAgb25LZXlEb3duID0gX3Byb3BzLm9uS2V5RG93bixcbiAgICAgICAgICBvblJlcXVlc3REZWxldGUgPSBfcHJvcHMub25SZXF1ZXN0RGVsZXRlLFxuICAgICAgICAgIGRlbGV0ZUljb25Qcm9wID0gX3Byb3BzLmRlbGV0ZUljb24sXG4gICAgICAgICAgdGFiSW5kZXhQcm9wID0gX3Byb3BzLnRhYkluZGV4LFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2F2YXRhcicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdsYWJlbCcsICdvbkNsaWNrJywgJ29uS2V5RG93bicsICdvblJlcXVlc3REZWxldGUnLCAnZGVsZXRlSWNvbicsICd0YWJJbmRleCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmNsaWNrYWJsZSwgb25DbGljayksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzLmRlbGV0YWJsZSwgb25SZXF1ZXN0RGVsZXRlKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHZhciBkZWxldGVJY29uID0gbnVsbDtcbiAgICAgIGlmIChvblJlcXVlc3REZWxldGUgJiYgZGVsZXRlSWNvblByb3AgJiYgX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGRlbGV0ZUljb25Qcm9wKSkge1xuICAgICAgICBkZWxldGVJY29uID0gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChkZWxldGVJY29uUHJvcCwge1xuICAgICAgICAgIG9uQ2xpY2s6IHRoaXMuaGFuZGxlRGVsZXRlSWNvbkNsaWNrLFxuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmRlbGV0ZUljb24sIGRlbGV0ZUljb25Qcm9wLnByb3BzLmNsYXNzTmFtZSlcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKG9uUmVxdWVzdERlbGV0ZSkge1xuICAgICAgICBkZWxldGVJY29uID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX0NhbmNlbDIuZGVmYXVsdCwgeyBjbGFzc05hbWU6IGNsYXNzZXMuZGVsZXRlSWNvbiwgb25DbGljazogdGhpcy5oYW5kbGVEZWxldGVJY29uQ2xpY2sgfSk7XG4gICAgICB9XG5cbiAgICAgIHZhciBhdmF0YXIgPSBudWxsO1xuICAgICAgaWYgKGF2YXRhclByb3AgJiYgX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGF2YXRhclByb3ApKSB7XG4gICAgICAgIGF2YXRhciA9IF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoYXZhdGFyUHJvcCwge1xuICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmF2YXRhciwgYXZhdGFyUHJvcC5wcm9wcy5jbGFzc05hbWUpLFxuICAgICAgICAgIGNoaWxkcmVuQ2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMuYXZhdGFyQ2hpbGRyZW4sIGF2YXRhclByb3AucHJvcHMuY2hpbGRyZW5DbGFzc05hbWUpXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICB2YXIgdGFiSW5kZXggPSB0YWJJbmRleFByb3A7XG5cbiAgICAgIGlmICghdGFiSW5kZXgpIHtcbiAgICAgICAgdGFiSW5kZXggPSBvbkNsaWNrIHx8IG9uUmVxdWVzdERlbGV0ZSA/IDAgOiAtMTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnZGl2JyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgcm9sZTogJ2J1dHRvbicsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgICAgdGFiSW5kZXg6IHRhYkluZGV4LFxuICAgICAgICAgIG9uQ2xpY2s6IG9uQ2xpY2ssXG4gICAgICAgICAgb25LZXlEb3duOiB0aGlzLmhhbmRsZUtleURvd25cbiAgICAgICAgfSwgb3RoZXIsIHtcbiAgICAgICAgICByZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG4gICAgICAgICAgICBfdGhpczIuY2hpcFJlZiA9IG5vZGU7XG4gICAgICAgICAgfVxuICAgICAgICB9KSxcbiAgICAgICAgYXZhdGFyLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAnc3BhbicsXG4gICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMubGFiZWwgfSxcbiAgICAgICAgICBsYWJlbFxuICAgICAgICApLFxuICAgICAgICBkZWxldGVJY29uXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQ2hpcDtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkNoaXAuZGVmYXVsdFByb3BzID0ge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpQ2hpcCcgfSkoQ2hpcCk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQ2hpcC9DaGlwLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9DaGlwL0NoaXAuanNcbi8vIG1vZHVsZSBjaHVua3MgPSA0IDYgNTQiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfQ2hpcCA9IHJlcXVpcmUoJy4vQ2hpcCcpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9DaGlwKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9DaGlwL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9DaGlwL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNCA2IDU0IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMiAyQzYuNDcgMiAyIDYuNDcgMiAxMnM0LjQ3IDEwIDEwIDEwIDEwLTQuNDcgMTAtMTBTMTcuNTMgMiAxMiAyem01IDEzLjU5TDE1LjU5IDE3IDEyIDEzLjQxIDguNDEgMTcgNyAxNS41OSAxMC41OSAxMiA3IDguNDEgOC40MSA3IDEyIDEwLjU5IDE1LjU5IDcgMTcgOC40MSAxMy40MSAxMiAxNyAxNS41OXonIH0pO1xuXG52YXIgQ2FuY2VsID0gZnVuY3Rpb24gQ2FuY2VsKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuQ2FuY2VsID0gKDAsIF9wdXJlMi5kZWZhdWx0KShDYW5jZWwpO1xuQ2FuY2VsLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENhbmNlbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvQ2FuY2VsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvQ2FuY2VsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gNCA2IDU0IiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IHsgRUxFTUVOVFNfR0VUIH0gZnJvbSAnLi4vLi4vY29uc3RhbnRzL2VsZW1lbnRzJztcblxuLyoqXG4gKlxuICogQGZ1bmN0aW9uXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkW10gLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWRbXS5pZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZFtdLnVzZXJuYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkW10uZWxlbWVudFR5cGUgLSAndXNlcicgb3IgJ2NpcmNsZSdcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkW10uY2lyY2xlVHlwZSAtIGlmIGVsZW1lbnRUeXBlIGlzICdjaXJjbGUnIHRoZW5cbiAqICdlZHUnLCAnb3JnJywgJ2ZpZWxkJywgJ2xvY2F0aW9uJywgJ3NvY2lhbCcgZWxzZSB0aGlzIGtleSBub3QgZXhpc3RzLlxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWRbXS5uYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkW10uYXZhdGFyIC1cbiAqIEByZXR1cm5zIHtPYmplY3R9IC0gcmVkdXggYWN0aW9uIHR5cGUuXG4gKi9cbmNvbnN0IGdldEFsbCA9IChwYXlsb2FkKSA9PiAoeyB0eXBlOiBFTEVNRU5UU19HRVQsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9lbGVtZW50cy9nZXRBbGwuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgVVNFUl9DSVJDTEVTX0dFVCB9IGZyb20gJy4uLy4uLy4uL2NvbnN0YW50cy91c2Vycy9jaXJjbGVzJztcblxudHlwZSBQYXlsb2FkID0ge1xuICBpZDogbnVtYmVyLFxuICBjaXJjbGVzOiBBcnJheTx7XG4gICAgaWQ6IG51bWJlcixcbiAgfT4sXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBnZXQgYWxsIHVzZXIncyBjaXJjbGVzIGFjdGlvblxuICogQGZ1bmN0aW9uIGdldEFsbFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuaWQgLVxuICogQHBhcmFtIHtPYmplY3RbXX0gcGF5bG9hZC5jaXJjbGVzIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkW10uY2lyY2xlcyAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZFtdLmNpcmNsZXMuaWQgLVxuICogQHJldHVybnMge09iamVjdH0gLVxuICovXG5jb25zdCBnZXRBbGwgPSAocGF5bG9hZDogUGF5bG9hZCk6IEFjdGlvbiA9PiAoe1xuICB0eXBlOiBVU0VSX0NJUkNMRVNfR0VULFxuICBwYXlsb2FkLFxufSk7XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy91c2Vycy9jaXJjbGVzL2dldEFsbC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIEdldCBhbGwgdXNlcidzIGNpcmNsZXMgYnkgdXNlcklkXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBnZXRBbGxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkXG4gKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiBnZXRBbGwoeyB0b2tlbiwgdXNlcklkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L2NpcmNsZXNgO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGdldEFsbDtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3VzZXJzL2NpcmNsZXMvZ2V0QWxsLmpzIiwiLyoqXG4gKiBUaGlzIGZpbGUgY29udGFpbnMgYWN0aW9uIGNvbnN0YW50cyBmb3IgVXNlcidzIGNpcmNsZXNcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuZXhwb3J0IGNvbnN0IFVTRVJfQ0lSQ0xFU19HRVQgPSAnVVNFUl9DSVJDTEVTX0dFVCc7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnN0YW50cy91c2Vycy9jaXJjbGVzLmpzIiwiLyoqXG4gKiBQb3N0IGNvbnRhaW5lciBjb21wb25lbnRcbiAqIEl0IHdpbGwgZGlzcGxheSBvbmUgcG9zdCB3aXRoIHNvbWUgb2YgYWN0aW9ucyAoZWcuIEJvb2ttYXJrcywgU2hhcmUsIERlbGV0ZSlcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgeyBMaW5rIH0gZnJvbSAncmVhY3Qtcm91dGVyLWRvbSc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmltcG9ydCBDYXJkLCB7IENhcmRNZWRpYSwgQ2FyZENvbnRlbnQsIENhcmRBY3Rpb25zIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5cbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IENoaXAgZnJvbSAnbWF0ZXJpYWwtdWkvQ2hpcCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge30sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgfSxcbiAgZGV0YWlsczoge1xuICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICBmbGV4RGlyZWN0aW9uOiAnY29sdW1uJyxcbiAgfSxcbiAgY292ZXI6IHtcbiAgICB3aWR0aDogdGhlbWUuc3BhY2luZy51bml0ICogMTAsXG4gICAgaGVpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAxMCxcbiAgICBtYXJnaW46IHRoZW1lLnNwYWNpbmcudW5pdCAqIDIsXG4gICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICBmbGV4U2hyaW5rOiAwLFxuICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsXG4gIH0sXG4gIGNoaXA6IHtcbiAgICAvLyBtYXJnaW46IHRoZW1lLnNwYWNpbmcudW5pdCxcbiAgfSxcbiAgbGluazoge1xuICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gICAgJyY6bGluayc6IHtcbiAgICAgIHRleHREZWNvcmF0aW9uOiAnbm9uZScsXG4gICAgICBjb2xvcjogJ2FjY2VudCcsXG4gICAgfSxcbiAgICAnJjp2aXNpdGVkJzoge1xuICAgICAgdGV4dERlY29yYXRpb246ICdub25lJyxcbiAgICAgIGNvbG9yOiAnYmxhY2snLFxuICAgIH0sXG4gICAgJyY6aG92ZXInOiB7XG4gICAgICBjb2xvcjogJ25vbmUnLFxuICAgIH0sXG4gICAgJyY6YWN0aXZlJzoge1xuICAgICAgY29sb3I6ICdub25lJyxcbiAgICB9LFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59KTtcblxuY2xhc3MgQ2lyY2xlIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGhvdmVyOiBmYWxzZSxcbiAgICB9O1xuICB9XG5cbiAgc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRTdGF0ZSwgbmV4dFByb3ApIHtcbiAgICByZXR1cm4gdGhpcy5zdGF0ZSAhPT0gbmV4dFN0YXRlIHx8IHRoaXMucHJvcHMgIT09IG5leHRQcm9wO1xuICB9XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7XG4gICAgICBjbGFzc2VzLFxuICAgICAgaWQsXG4gICAgICB1c2VybmFtZSxcbiAgICAgIG5hbWUsXG4gICAgICBhdmF0YXIsXG4gICAgICBjb3ZlcixcbiAgICAgIGNpcmNsZVR5cGUsXG4gICAgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBsZXQgY2lyY2xlO1xuXG4gICAgaWYgKGNpcmNsZVR5cGUgPT09ICdmaWVsZCcpIHtcbiAgICAgIGNpcmNsZSA9ICdGSUVMRCc7XG4gICAgfVxuXG4gICAgaWYgKGNpcmNsZVR5cGUgPT09ICdlZHUnKSB7XG4gICAgICBjaXJjbGUgPSAnRURVQ0FUSU9OQUwgSU5TVElUVVRFJztcbiAgICB9XG5cbiAgICBpZiAoY2lyY2xlVHlwZSA9PT0gJ29yZycpIHtcbiAgICAgIGNpcmNsZSA9ICdPUkdOSVpBVElPTic7XG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9XG4gICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5vbk1vdXNlRW50ZXJ9XG4gICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbk1vdXNlTGVhdmV9XG4gICAgICA+XG4gICAgICAgIDxDYXJkIGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICA8Q2FyZE1lZGlhIGNsYXNzTmFtZT17Y2xhc3Nlcy5jb3Zlcn0gaW1hZ2U9e2F2YXRhcn0gdGl0bGU9XCJGaWVsZFwiIC8+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZGV0YWlsc30+XG4gICAgICAgICAgICA8Q2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIDxMaW5rIHRvPXtgQCR7dXNlcm5hbWV9YH0gY2xhc3NOYW1lPXtjbGFzc2VzLmxpbmt9PlxuICAgICAgICAgICAgICAgIDxUeXBvZ3JhcGh5IHR5cGU9XCJoZWFkbGluZVwiPntuYW1lfTwvVHlwb2dyYXBoeT5cbiAgICAgICAgICAgICAgPC9MaW5rPlxuICAgICAgICAgICAgICA8TGluayB0bz17YEAke3VzZXJuYW1lfWB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5saW5rfT5cbiAgICAgICAgICAgICAgICA8VHlwb2dyYXBoeSB0eXBlPVwic3ViaGVhZGluZ1wiPnt1c2VybmFtZX08L1R5cG9ncmFwaHk+XG4gICAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICAgIDxDaGlwIGxhYmVsPXtjaXJjbGV9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jaGlwfSAvPlxuICAgICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5DaXJjbGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGlkOiBQcm9wVHlwZXMubnVtYmVyLmlzUmVxdWlyZWQsXG4gIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIGNpcmNsZVR5cGU6IFByb3BUeXBlcy5zdHJpbmcuaXNSZXF1aXJlZCxcbiAgbmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBjb3ZlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgYXZhdGFyOiBQcm9wVHlwZXMuc3RyaW5nLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKENpcmNsZSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ2lyY2xlL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuXG5pbXBvcnQgaW50ZXJzZWN0aW9uQnkgZnJvbSAnbG9kYXNoL2ludGVyc2VjdGlvbkJ5JztcblxuaW1wb3J0IENpcmNsZSBmcm9tICcuLi9DaXJjbGUnO1xuXG5pbXBvcnQgYXBpR2V0VXNlckNpcmNsZXMgZnJvbSAnLi4vLi4vYXBpL3VzZXJzL2NpcmNsZXMvZ2V0QWxsJztcblxuaW1wb3J0IGFjdGlvbkdldEFsbEVsZW1lbnRzIGZyb20gJy4uLy4uL2FjdGlvbnMvZWxlbWVudHMvZ2V0QWxsJztcbmltcG9ydCBhY3Rpb25HZXRBbGxVc2VyQ2lyY2xlcyBmcm9tICcuLi8uLi9hY3Rpb25zL3VzZXJzL2NpcmNsZXMvZ2V0QWxsJztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7XG4gICAgZGlzcGxheTogJ2ZsZXgtc3RhcnQnLFxuICAgIHBhZGRpbmdMZWZ0OiAnMnB4JyxcbiAgICBwYWRkaW5nUmlnaHQ6ICcycHgnLFxuICAgIC8vIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCdtZCcpXToge1xuICAgIC8vICAgcGFkZGluZ0xlZnQ6ICcyJScsXG4gICAgLy8gICBwYWRkaW5nUmlnaHQ6ICcyJScsXG4gICAgLy8gfSxcbiAgICAvLyBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAvLyAgIHBhZGRpbmdMZWZ0OiAnMiUnLFxuICAgIC8vICAgcGFkZGluZ1JpZ2h0OiAnMiUnLFxuICAgIC8vIH0sXG4gICAgLy8gW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ3hzJyldOiB7XG4gICAgLy8gICBwYWRkaW5nTGVmdDogJzIlJyxcbiAgICAvLyAgIHBhZGRpbmdSaWdodDogJzIlJyxcbiAgICAvLyB9LFxuICB9LFxuICBncmlkSXRlbToge1xuICAgIHBhZGRpbmc6ICc0cHggMnB4IDJweCAycHgnLFxuICB9LFxufSk7XG5cbmNsYXNzIENpcmNsZVRpbWVsaW5lIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50V2lsbE1vdW50KCkge1xuICAgIGNvbnN0IHsgaWQsIHRva2VuIH0gPSB0aGlzLnByb3BzLmVsZW1lbnRzWzBdO1xuXG4gICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgYXBpR2V0VXNlckNpcmNsZXMoe1xuICAgICAgdG9rZW4sXG4gICAgICB1c2VySWQ6IGlkLFxuICAgIH0pO1xuXG4gICAgaWYgKHN0YXR1c0NvZGUgIT09IDIwMCkge1xuICAgICAgLy8gaGFuZGxlIGVycm9yXG4gICAgICBjb25zb2xlLmxvZyhzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25HZXRBbGxFbGVtZW50cyhwYXlsb2FkKTtcbiAgICB0aGlzLnByb3BzLmFjdGlvbkdldEFsbFVzZXJDaXJjbGVzKHtcbiAgICAgIGlkLFxuICAgICAgY2lyY2xlczogcGF5bG9hZC5tYXAoKGMpID0+ICh7IGlkOiBjLmlkIH0pKSxcbiAgICB9KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHVzZXIgPSBlbGVtZW50c1swXTtcblxuICAgIGNvbnN0IGNpcmNsZXMgPSBpbnRlcnNlY3Rpb25CeShlbGVtZW50cywgdXNlci5jaXJjbGVzLCAnaWQnKTtcblxuICAgIHJldHVybiAoXG4gICAgICA8R3JpZFxuICAgICAgICBjb250YWluZXJcbiAgICAgICAgLy8ganVzdGlmeT1cImNlbnRlclwiXG4gICAgICAgIC8vIGFsaWduQ29udGVudD1cImNlbnRlclwiXG4gICAgICAgIHNwYWNpbmc9ezB9XG4gICAgICAgIC8vIGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fVxuICAgICAgICAvLyBhbGlnbkl0ZW1zPVwiY2VudGVyXCJcbiAgICAgID5cbiAgICAgICAge3R5cGVvZiBjaXJjbGVzICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICAgIGNpcmNsZXMubWFwKChjKSA9PiAoXG4gICAgICAgICAgICA8R3JpZFxuICAgICAgICAgICAgICBrZXk9e2MuaWR9XG4gICAgICAgICAgICAgIGl0ZW1cbiAgICAgICAgICAgICAgeHM9ezZ9XG4gICAgICAgICAgICAgIHNtPXs2fVxuICAgICAgICAgICAgICBtZD17NH1cbiAgICAgICAgICAgICAgbGc9ezR9XG4gICAgICAgICAgICAgIHhsPXs0fVxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxDaXJjbGUgey4uLmN9IC8+XG4gICAgICAgICAgICA8L0dyaWQ+XG4gICAgICAgICAgKSl9XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5DaXJjbGVUaW1lbGluZS5wcm9wVHlwZXMgPSB7XG4gIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgYWN0aW9uR2V0QWxsRWxlbWVudHM6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGFjdGlvbkdldEFsbFVzZXJDaXJjbGVzOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5jb25zdCBtYXBEaXNwYXRjaFRvUHJvcHMgPSAoZGlzcGF0Y2gpID0+XG4gIGJpbmRBY3Rpb25DcmVhdG9ycyhcbiAgICB7XG4gICAgICBhY3Rpb25HZXRBbGxFbGVtZW50cyxcbiAgICAgIGFjdGlvbkdldEFsbFVzZXJDaXJjbGVzLFxuICAgIH0sXG4gICAgZGlzcGF0Y2gsXG4gICk7XG5cbmV4cG9ydCBkZWZhdWx0IGNvbm5lY3QobWFwU3RhdGVUb1Byb3BzLCBtYXBEaXNwYXRjaFRvUHJvcHMpKFxuICB3aXRoU3R5bGVzKHN0eWxlcykoQ2lyY2xlVGltZWxpbmUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9DaXJjbGVUaW1lbGluZS9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=