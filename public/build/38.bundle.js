webpackJsonp([38],{

/***/ "./node_modules/material-ui-icons/AttachFile.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/AttachFile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z' });

var AttachFile = function AttachFile(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

AttachFile = (0, _pure2.default)(AttachFile);
AttachFile.muiName = 'SvgIcon';

exports.default = AttachFile;

/***/ }),

/***/ "./node_modules/material-ui-icons/Code.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Code.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z' });

var Code = function Code(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Code = (0, _pure2.default)(Code);
Code.muiName = 'SvgIcon';

exports.default = Code;

/***/ }),

/***/ "./node_modules/material-ui-icons/Delete.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui-icons/Delete.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zM19 4h-3.5l-1-1h-5l-1 1H5v2h14V4z' });

var Delete = function Delete(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Delete = (0, _pure2.default)(Delete);
Delete.muiName = 'SvgIcon';

exports.default = Delete;

/***/ }),

/***/ "./node_modules/material-ui-icons/Edit.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Edit.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z' });

var Edit = function Edit(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Edit = (0, _pure2.default)(Edit);
Edit.muiName = 'SvgIcon';

exports.default = Edit;

/***/ }),

/***/ "./node_modules/material-ui-icons/ExpandMore.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/ExpandMore.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.59 8.59L12 13.17 7.41 8.59 6 10l6 6 6-6z' });

var ExpandMore = function ExpandMore(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

ExpandMore = (0, _pure2.default)(ExpandMore);
ExpandMore.muiName = 'SvgIcon';

exports.default = ExpandMore;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignCenter.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignCenter.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M7 15v2h10v-2H7zm-4 6h18v-2H3v2zm0-8h18v-2H3v2zm4-6v2h10V7H7zM3 3v2h18V3H3z' });

var FormatAlignCenter = function FormatAlignCenter(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignCenter = (0, _pure2.default)(FormatAlignCenter);
FormatAlignCenter.muiName = 'SvgIcon';

exports.default = FormatAlignCenter;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignJustify.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignJustify.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18V7H3v2zm0-6v2h18V3H3z' });

var FormatAlignJustify = function FormatAlignJustify(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignJustify = (0, _pure2.default)(FormatAlignJustify);
FormatAlignJustify.muiName = 'SvgIcon';

exports.default = FormatAlignJustify;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignLeft.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignLeft.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15 15H3v2h12v-2zm0-8H3v2h12V7zM3 13h18v-2H3v2zm0 8h18v-2H3v2zM3 3v2h18V3H3z' });

var FormatAlignLeft = function FormatAlignLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignLeft = (0, _pure2.default)(FormatAlignLeft);
FormatAlignLeft.muiName = 'SvgIcon';

exports.default = FormatAlignLeft;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignRight.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignRight.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm6-4h12v-2H9v2zm-6-4h18v-2H3v2zm6-4h12V7H9v2zM3 3v2h18V3H3z' });

var FormatAlignRight = function FormatAlignRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignRight = (0, _pure2.default)(FormatAlignRight);
FormatAlignRight.muiName = 'SvgIcon';

exports.default = FormatAlignRight;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatBold.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatBold.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.6 10.79c.97-.67 1.65-1.77 1.65-2.79 0-2.26-1.75-4-4-4H7v14h7.04c2.09 0 3.71-1.7 3.71-3.79 0-1.52-.86-2.82-2.15-3.42zM10 6.5h3c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-3v-3zm3.5 9H10v-3h3.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5z' });

var FormatBold = function FormatBold(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatBold = (0, _pure2.default)(FormatBold);
FormatBold.muiName = 'SvgIcon';

exports.default = FormatBold;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatItalic.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatItalic.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4z' });

var FormatItalic = function FormatItalic(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatItalic = (0, _pure2.default)(FormatItalic);
FormatItalic.muiName = 'SvgIcon';

exports.default = FormatItalic;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListBulleted.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListBulleted.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M4 10.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5-.67-1.5-1.5-1.5zm0-6c-.83 0-1.5.67-1.5 1.5S3.17 7.5 4 7.5 5.5 6.83 5.5 6 4.83 4.5 4 4.5zm0 12c-.83 0-1.5.68-1.5 1.5s.68 1.5 1.5 1.5 1.5-.68 1.5-1.5-.67-1.5-1.5-1.5zM7 19h14v-2H7v2zm0-6h14v-2H7v2zm0-8v2h14V5H7z' });

var FormatListBulleted = function FormatListBulleted(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListBulleted = (0, _pure2.default)(FormatListBulleted);
FormatListBulleted.muiName = 'SvgIcon';

exports.default = FormatListBulleted;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListNumbered.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListNumbered.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z' });

var FormatListNumbered = function FormatListNumbered(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListNumbered = (0, _pure2.default)(FormatListNumbered);
FormatListNumbered.muiName = 'SvgIcon';

exports.default = FormatListNumbered;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatQuote.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatQuote.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' });

var FormatQuote = function FormatQuote(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatQuote = (0, _pure2.default)(FormatQuote);
FormatQuote.muiName = 'SvgIcon';

exports.default = FormatQuote;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatUnderlined.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatUnderlined.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z' });

var FormatUnderlined = function FormatUnderlined(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatUnderlined = (0, _pure2.default)(FormatUnderlined);
FormatUnderlined.muiName = 'SvgIcon';

exports.default = FormatUnderlined;

/***/ }),

/***/ "./node_modules/material-ui-icons/Functions.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Functions.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 4H6v2l6.5 6L6 18v2h12v-3h-7l5-5-5-5h7z' });

var Functions = function Functions(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Functions = (0, _pure2.default)(Functions);
Functions.muiName = 'SvgIcon';

exports.default = Functions;

/***/ }),

/***/ "./node_modules/material-ui-icons/Highlight.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Highlight.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 14l3 3v5h6v-5l3-3V9H6zm5-12h2v3h-2zM3.5 5.875L4.914 4.46l2.12 2.122L5.62 7.997zm13.46.71l2.123-2.12 1.414 1.414L18.375 8z' });

var Highlight = function Highlight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Highlight = (0, _pure2.default)(Highlight);
Highlight.muiName = 'SvgIcon';

exports.default = Highlight;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertComment.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertComment.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var InsertComment = function InsertComment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertComment = (0, _pure2.default)(InsertComment);
InsertComment.muiName = 'SvgIcon';

exports.default = InsertComment;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertEmoticon.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertEmoticon.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });

var InsertEmoticon = function InsertEmoticon(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertEmoticon = (0, _pure2.default)(InsertEmoticon);
InsertEmoticon.muiName = 'SvgIcon';

exports.default = InsertEmoticon;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertLink.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertLink.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z' });

var InsertLink = function InsertLink(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertLink = (0, _pure2.default)(InsertLink);
InsertLink.muiName = 'SvgIcon';

exports.default = InsertLink;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui-icons/Save.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Save.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z' });

var Save = function Save(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Save = (0, _pure2.default)(Save);
Save.muiName = 'SvgIcon';

exports.default = Save;

/***/ }),

/***/ "./node_modules/material-ui-icons/Title.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Title.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M5 4v3h5.5v12h3V7H19V4z' });

var Title = function Title(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Title = (0, _pure2.default)(Title);
Title.muiName = 'SvgIcon';

exports.default = Title;

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/internal/transition.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui/internal/transition.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
})]);

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_TransitionClasses = {
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  appearActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  enterActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,
  exitActive: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

/***/ }),

/***/ "./node_modules/material-ui/transitions/Collapse.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/transitions/Collapse.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _Transition = __webpack_require__(/*! react-transition-group/Transition */ "./node_modules/react-transition-group/Transition.js");

var _Transition2 = _interopRequireDefault(_Transition);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _transitions = __webpack_require__(/*! ../styles/transitions */ "./node_modules/material-ui/styles/transitions.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// @inheritedComponent Transition

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_TransitionCallback = __webpack_require__(/*! ../internal/transition */ "./node_modules/material-ui/internal/transition.js").babelPluginFlowReactPropTypes_proptype_TransitionCallback || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    container: {
      height: 0,
      overflow: 'hidden',
      transition: theme.transitions.create('height')
    },
    entered: {
      height: 'auto'
    },
    wrapper: {
      // Hack to get children with a negative margin to not falsify the height computation.
      display: 'flex'
    },
    wrapperInner: {
      width: '100%'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_TransitionDuration = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * @ignore
   */
  appear: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content node to be collapsed.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: typeof String === 'function' ? __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").instanceOf(String) : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   * The default value is a `button`.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * The height of the container when collapsed.
   */
  collapsedHeight: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired,

  /**
   * Properties applied to the `Collapse` container.
   */
  containerProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * If `true`, the component will transition in.
   */
  in: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  onEnter: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntering: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onEntered: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExit: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  onExiting: typeof babelPluginFlowReactPropTypes_proptype_TransitionCallback === 'function' ? babelPluginFlowReactPropTypes_proptype_TransitionCallback : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_TransitionCallback),

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The duration for the transition, in milliseconds.
   * You may specify a single timeout for all transitions, or individually with an object.
   *
   * Set to 'auto' to automatically calculate transition time based on height.
   */
  timeout: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number, __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    enter: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    exit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto'])]).isRequired,

  /**
   * @ignore
   */
  unmountOnExit: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var Collapse = function (_React$Component) {
  (0, _inherits3.default)(Collapse, _React$Component);

  function Collapse() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Collapse);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Collapse.__proto__ || (0, _getPrototypeOf2.default)(Collapse)).call.apply(_ref, [this].concat(args))), _this), _this.wrapper = null, _this.autoTransitionDuration = undefined, _this.handleEnter = function (node) {
      node.style.height = _this.props.collapsedHeight;

      if (_this.props.onEnter) {
        _this.props.onEnter(node);
      }
    }, _this.handleEntering = function (node) {
      var _this$props = _this.props,
          timeout = _this$props.timeout,
          theme = _this$props.theme;

      var wrapperHeight = _this.wrapper ? _this.wrapper.clientHeight : 0;

      if (timeout === 'auto') {
        var duration2 = theme.transitions.getAutoHeightDuration(wrapperHeight);
        node.style.transitionDuration = duration2 + 'ms';
        _this.autoTransitionDuration = duration2;
      } else if (typeof timeout === 'number') {
        node.style.transitionDuration = timeout + 'ms';
      } else if (timeout && typeof timeout.enter === 'number') {
        node.style.transitionDuration = timeout.enter + 'ms';
      } else {
        // The propType will warn in this case.
      }

      node.style.height = wrapperHeight + 'px';

      if (_this.props.onEntering) {
        _this.props.onEntering(node);
      }
    }, _this.handleEntered = function (node) {
      node.style.height = 'auto';

      if (_this.props.onEntered) {
        _this.props.onEntered(node);
      }
    }, _this.handleExit = function (node) {
      var wrapperHeight = _this.wrapper ? _this.wrapper.clientHeight : 0;
      node.style.height = wrapperHeight + 'px';

      if (_this.props.onExit) {
        _this.props.onExit(node);
      }
    }, _this.handleExiting = function (node) {
      var _this$props2 = _this.props,
          timeout = _this$props2.timeout,
          theme = _this$props2.theme;

      var wrapperHeight = _this.wrapper ? _this.wrapper.clientHeight : 0;

      if (timeout === 'auto') {
        var duration2 = theme.transitions.getAutoHeightDuration(wrapperHeight);
        node.style.transitionDuration = duration2 + 'ms';
        _this.autoTransitionDuration = duration2;
      } else if (typeof timeout === 'number') {
        node.style.transitionDuration = timeout + 'ms';
      } else if (timeout && typeof timeout.exit === 'number') {
        node.style.transitionDuration = timeout.exit + 'ms';
      } else {
        // The propType will warn in this case.
      }

      node.style.height = _this.props.collapsedHeight;

      if (_this.props.onExiting) {
        _this.props.onExiting(node);
      }
    }, _this.addEndListener = function (node, next) {
      if (_this.props.timeout === 'auto') {
        setTimeout(next, _this.autoTransitionDuration || 0);
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Collapse, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          appear = _props.appear,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          ComponentProp = _props.component,
          collapsedHeight = _props.collapsedHeight,
          containerProps = _props.containerProps,
          onEnter = _props.onEnter,
          onEntering = _props.onEntering,
          onEntered = _props.onEntered,
          onExit = _props.onExit,
          onExiting = _props.onExiting,
          style = _props.style,
          timeout = _props.timeout,
          theme = _props.theme,
          other = (0, _objectWithoutProperties3.default)(_props, ['appear', 'children', 'classes', 'className', 'component', 'collapsedHeight', 'containerProps', 'onEnter', 'onEntering', 'onEntered', 'onExit', 'onExiting', 'style', 'timeout', 'theme']);


      return _react2.default.createElement(
        _Transition2.default,
        (0, _extends3.default)({
          appear: appear,
          onEntering: this.handleEntering,
          onEnter: this.handleEnter,
          onEntered: this.handleEntered,
          onExiting: this.handleExiting,
          onExit: this.handleExit,
          addEndListener: this.addEndListener,
          timeout: timeout === 'auto' ? null : timeout
        }, other),
        function (state, otherInner) {
          return _react2.default.createElement(
            ComponentProp,
            (0, _extends3.default)({
              className: (0, _classnames2.default)(classes.container, (0, _defineProperty3.default)({}, classes.entered, state === 'entered'), className),
              style: (0, _extends3.default)({}, style, {
                minHeight: collapsedHeight
              })
            }, otherInner),
            _react2.default.createElement(
              'div',
              {
                className: classes.wrapper,
                ref: function ref(node) {
                  _this2.wrapper = node;
                }
              },
              _react2.default.createElement(
                'div',
                { className: classes.wrapperInner },
                children
              )
            )
          );
        }
      );
    }
  }]);
  return Collapse;
}(_react2.default.Component);

Collapse.defaultProps = {
  appear: false,
  component: 'div',
  collapsedHeight: '0px',
  timeout: _transitions.duration.standard
};
exports.default = (0, _withStyles2.default)(styles, {
  withTheme: true,
  name: 'MuiCollapse'
})(Collapse);

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/actions/courses/users/modules/delete.js":
/*!************************************************************!*\
  !*** ./src/client/actions/courses/users/modules/delete.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../../constants/courses */ "./src/client/constants/courses.js");

/**
 *
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @return {Object}
 */
var deleteModule = function deleteModule(payload) {
  return {
    type: _courses.MODULE_DELETE,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = deleteModule;

/***/ }),

/***/ "./src/client/actions/courses/users/modules/update.js":
/*!************************************************************!*\
  !*** ./src/client/actions/courses/users/modules/update.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _courses = __webpack_require__(/*! ../../../../constants/courses */ "./src/client/constants/courses.js");

/**
 * This action will update course module in store
 * @function update
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.title -
 * @param {Object} payload.data -
 * @return {Object}
 */
var update = function update(payload) {
  return { type: _courses.MODULE_UPDATE, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = update;

/***/ }),

/***/ "./src/client/api/courses/users/modules/delete.js":
/*!********************************************************!*\
  !*** ./src/client/api/courses/users/modules/delete.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * For deleting course module
 * @async
 * @function deleteModule
 * @param {Object} payload
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @file
 * @format
 * 
 */

var deleteModule = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId,
        moduleId = _ref2.moduleId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId + '/modules/' + moduleId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deleteModule(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deleteModule;

/***/ }),

/***/ "./src/client/api/courses/users/modules/update.js":
/*!********************************************************!*\
  !*** ./src/client/api/courses/users/modules/update.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * Update Module
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.title -
 * @param {Object} payload.data -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @fileOverview
 * @format
 * 
 */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        courseId = _ref2.courseId,
        moduleId = _ref2.moduleId,
        title = _ref2.title,
        data = _ref2.data;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses/' + courseId + '/modules/' + moduleId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ title: title, data: data })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/components/Title.js":
/*!****************************************!*\
  !*** ./src/client/components/Title.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit'
  }
}; /**
    * Title component is for editable component to edit title with textarea.
    * This component should be used with CardHeader title place.
    * TODO: make textarea autoresizable.
    *
    * @format
    */

var Title = function Title(_ref) {
  var classes = _ref.classes,
      readOnly = _ref.readOnly,
      value = _ref.value,
      placeholder = _ref.placeholder,
      onChange = _ref.onChange,
      minLength = _ref.minLength,
      maxLength = _ref.maxLength;

  if (readOnly) {
    return _react2.default.createElement(
      'div',
      { className: classes.value },
      value
    );
  }

  return _react2.default.createElement('textarea', {
    placeholder: placeholder,
    value: value,
    onChange: onChange,
    className: classes.textarea,
    minLength: minLength || 1,
    maxLength: maxLength || 148,
    rows: 1,
    readOnly: readOnly
  });
};

Title.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  readOnly: _propTypes2.default.bool.isRequired,
  placeholder: _propTypes2.default.string.isRequired,
  value: _propTypes2.default.string.isRequired,
  onChange: _propTypes2.default.func.isRequired,
  minLength: _propTypes2.default.number,
  maxLength: _propTypes2.default.number
};

Title.defaultProps = {
  value: 'Untitled',
  placeholder: 'Title goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 148
};

exports.default = (0, _styles.withStyles)(styles)(Title);

/***/ }),

/***/ "./src/client/containers/CoursePage/Module.js":
/*!****************************************************!*\
  !*** ./src/client/containers/CoursePage/Module.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames2 = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames3 = _interopRequireDefault(_classnames2);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Collapse = __webpack_require__(/*! material-ui/transitions/Collapse */ "./node_modules/material-ui/transitions/Collapse.js");

var _Collapse2 = _interopRequireDefault(_Collapse);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Edit = __webpack_require__(/*! material-ui-icons/Edit */ "./node_modules/material-ui-icons/Edit.js");

var _Edit2 = _interopRequireDefault(_Edit);

var _Save = __webpack_require__(/*! material-ui-icons/Save */ "./node_modules/material-ui-icons/Save.js");

var _Save2 = _interopRequireDefault(_Save);

var _Delete = __webpack_require__(/*! material-ui-icons/Delete */ "./node_modules/material-ui-icons/Delete.js");

var _Delete2 = _interopRequireDefault(_Delete);

var _ExpandMore = __webpack_require__(/*! material-ui-icons/ExpandMore */ "./node_modules/material-ui-icons/ExpandMore.js");

var _ExpandMore2 = _interopRequireDefault(_ExpandMore);

var _Title = __webpack_require__(/*! ../../components/Title */ "./src/client/components/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _mayashEditor = __webpack_require__(/*! ../../../lib/mayash-editor */ "./src/lib/mayash-editor/index.js");

var _mayashEditor2 = _interopRequireDefault(_mayashEditor);

var _update = __webpack_require__(/*! ../../api/courses/users/modules/update */ "./src/client/api/courses/users/modules/update.js");

var _update2 = _interopRequireDefault(_update);

var _delete = __webpack_require__(/*! ../../api/courses/users/modules/delete */ "./src/client/api/courses/users/modules/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _update3 = __webpack_require__(/*! ../../actions/courses/users/modules/update */ "./src/client/actions/courses/users/modules/update.js");

var _update4 = _interopRequireDefault(_update3);

var _delete3 = __webpack_require__(/*! ../../actions/courses/users/modules/delete */ "./src/client/actions/courses/users/modules/delete.js");

var _delete4 = _interopRequireDefault(_delete3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      paddingBottom: '1%'
    },
    card: {
      borderRadius: '8px'
    },
    expand: {
      transform: 'rotate(0deg)',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: 'rotate(180deg)'
    },
    flexGrow: {
      flex: '1 1 auto'
    }
  };
}; /** @format */

var Module = function (_Component) {
  (0, _inherits3.default)(Module, _Component);

  function Module(props) {
    (0, _classCallCheck3.default)(this, Module);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Module.__proto__ || (0, _getPrototypeOf2.default)(Module)).call(this, props));

    _this.onChangeTitle = function (e) {
      return _this.setState({ title: e.target.value });
    };

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    var title = props.title,
        data = props.data;

    _this.state = {
      hover: false,
      expanded: false,
      edit: false,

      title: title,
      data: (0, _mayashEditor.createEditorState)(data),

      message: ''
    };

    _this.handleExpandClick = _this.handleExpandClick.bind(_this);
    _this.onChange = _this.onChange.bind(_this);
    _this.onEdit = _this.onEdit.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    _this.onClickDelete = _this.onClickDelete.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Module, [{
    key: 'onChange',
    value: function onChange(data) {
      this.setState({ data: data });
    }
  }, {
    key: 'onEdit',
    value: function onEdit() {
      this.setState({
        edit: !this.state.edit,
        expanded: true
      });
    }
  }, {
    key: 'onSave',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, courseId, moduleId, titleProp, elements, _elements$user, userId, token, _state, edit, title, data, body, _ref2, statusCode, error, message;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, courseId = _props.courseId, moduleId = _props.moduleId, titleProp = _props.title, elements = _props.elements;
                _elements$user = elements.user, userId = _elements$user.userId, token = _elements$user.token;
                _state = this.state, edit = _state.edit, title = _state.title, data = _state.data;
                body = {};


                if (titleProp !== title) {
                  body = (0, _extends3.default)({}, body, { title: title });
                }

                body = (0, _extends3.default)({}, body, {
                  data: (0, _draftJs.convertToRaw)(data.getCurrentContent())
                });

                _context.next = 9;
                return (0, _update2.default)((0, _extends3.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  moduleId: moduleId
                }, body));

              case 9:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                message = _ref2.message;

                if (!(statusCode >= 300)) {
                  _context.next = 16;
                  break;
                }

                this.setState({ message: error });
                return _context.abrupt('return');

              case 16:

                this.setState({ message: message });

                this.props.actionUpdate((0, _extends3.default)({
                  authorId: userId,
                  courseId: courseId,
                  moduleId: moduleId
                }, body, {
                  statusCode: statusCode
                }));

                this.setState({ edit: !edit });
                _context.next = 24;
                break;

              case 21:
                _context.prev = 21;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 24:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 21]]);
      }));

      function onSave() {
        return _ref.apply(this, arguments);
      }

      return onSave;
    }()
  }, {
    key: 'onClickDelete',
    value: function () {
      var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _props2, moduleId, user, course, authorId, courseId, token, userId, _ref4, statusCode, message, error;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _props2 = this.props, moduleId = _props2.moduleId, user = _props2.user, course = _props2.course;
                authorId = course.authorId, courseId = course.courseId;
                token = user.token, userId = user.id;
                _context2.next = 6;
                return (0, _delete2.default)({
                  token: token,
                  userId: userId,
                  courseId: courseId,
                  moduleId: moduleId
                });

              case 6:
                _ref4 = _context2.sent;
                statusCode = _ref4.statusCode;
                message = _ref4.message;
                error = _ref4.error;

                if (!(statusCode >= 300)) {
                  _context2.next = 13;
                  break;
                }

                this.setState({ message: error });
                return _context2.abrupt('return');

              case 13:

                this.setState({ message: message });

                this.props.actionDelete({
                  statusCode: statusCode,
                  moduleId: moduleId,
                  courseId: courseId,
                  authorId: authorId
                });
                _context2.next = 20;
                break;

              case 17:
                _context2.prev = 17;
                _context2.t0 = _context2['catch'](0);

                console.error(_context2.t0);

              case 20:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 17]]);
      }));

      function onClickDelete() {
        return _ref3.apply(this, arguments);
      }

      return onClickDelete;
    }()
  }, {
    key: 'handleExpandClick',
    value: function handleExpandClick() {
      this.setState({ expanded: !this.state.expanded });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          classes = _props3.classes,
          elements = _props3.elements,
          authorId = _props3.authorId,
          index = _props3.index,
          dataProp = _props3.data;
      var _elements$user2 = elements.user,
          userId = _elements$user2.id,
          isSignedIn = _elements$user2.isSignedIn;
      var _state2 = this.state,
          edit = _state2.edit,
          expanded = _state2.expanded,
          title = _state2.title,
          data = _state2.data;


      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: this.state.hover, className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            title: _react2.default.createElement(_Title2.default, {
              readOnly: !edit,
              value: edit ? title : index + '. ' + title,
              onChange: this.onChangeTitle
            })
          }),
          edit || typeof dataProp !== 'undefined' ? _react2.default.createElement(
            _Collapse2.default,
            { 'in': expanded, timeout: 'auto', unmountOnExit: true },
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(_mayashEditor2.default, {
                editorState: data,
                onChange: this.onChange,
                readOnly: !edit,
                placeholder: 'Module Content'
              })
            )
          ) : null,
          _react2.default.createElement(
            _Card.CardActions,
            { disableActionSpacing: true },
            _react2.default.createElement('div', { className: classes.flexGrow }),
            isSignedIn && authorId === userId && _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Edit',
                onClick: edit ? this.onSave : this.onEdit
              },
              edit ? _react2.default.createElement(_Save2.default, null) : _react2.default.createElement(_Edit2.default, null)
            ),
            isSignedIn && authorId === userId && _react2.default.createElement(
              _IconButton2.default,
              { onClick: this.onClickDelete, 'aria-label': 'Delete' },
              _react2.default.createElement(_Delete2.default, null)
            ),
            _react2.default.createElement(
              _Tooltip2.default,
              { title: 'Expand', placement: 'bottom' },
              _react2.default.createElement(
                _IconButton2.default,
                {
                  className: (0, _classnames3.default)(classes.expand, (0, _defineProperty3.default)({}, classes.expandOpen, expanded)),
                  onClick: this.handleExpandClick,
                  'aria-expanded': expanded,
                  'aria-label': 'Show more'
                },
                _react2.default.createElement(_ExpandMore2.default, null)
              )
            )
          )
        )
      );
    }
  }]);
  return Module;
}(_react.Component);

Module.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  authorId: _propTypes2.default.number.isRequired,
  courseId: _propTypes2.default.number.isRequired,
  moduleId: _propTypes2.default.number.isRequired,

  // index is module number in course.
  index: _propTypes2.default.number.isRequired,

  title: _propTypes2.default.string.isRequired,
  // description: PropTypes.string, // no using it for now.
  data: _propTypes2.default.object,

  actionUpdate: _propTypes2.default.func.isRequired,
  actionDelete: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref5) {
  var elements = _ref5.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionUpdate: _update4.default,
    actionDelete: _delete4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(Module));

/***/ }),

/***/ "./src/lib/mayash-editor/Editor.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-editor/Editor.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! material-ui-icons/Title */ "./node_modules/material-ui-icons/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _FormatBold = __webpack_require__(/*! material-ui-icons/FormatBold */ "./node_modules/material-ui-icons/FormatBold.js");

var _FormatBold2 = _interopRequireDefault(_FormatBold);

var _FormatItalic = __webpack_require__(/*! material-ui-icons/FormatItalic */ "./node_modules/material-ui-icons/FormatItalic.js");

var _FormatItalic2 = _interopRequireDefault(_FormatItalic);

var _FormatUnderlined = __webpack_require__(/*! material-ui-icons/FormatUnderlined */ "./node_modules/material-ui-icons/FormatUnderlined.js");

var _FormatUnderlined2 = _interopRequireDefault(_FormatUnderlined);

var _FormatQuote = __webpack_require__(/*! material-ui-icons/FormatQuote */ "./node_modules/material-ui-icons/FormatQuote.js");

var _FormatQuote2 = _interopRequireDefault(_FormatQuote);

var _Code = __webpack_require__(/*! material-ui-icons/Code */ "./node_modules/material-ui-icons/Code.js");

var _Code2 = _interopRequireDefault(_Code);

var _FormatListNumbered = __webpack_require__(/*! material-ui-icons/FormatListNumbered */ "./node_modules/material-ui-icons/FormatListNumbered.js");

var _FormatListNumbered2 = _interopRequireDefault(_FormatListNumbered);

var _FormatListBulleted = __webpack_require__(/*! material-ui-icons/FormatListBulleted */ "./node_modules/material-ui-icons/FormatListBulleted.js");

var _FormatListBulleted2 = _interopRequireDefault(_FormatListBulleted);

var _FormatAlignCenter = __webpack_require__(/*! material-ui-icons/FormatAlignCenter */ "./node_modules/material-ui-icons/FormatAlignCenter.js");

var _FormatAlignCenter2 = _interopRequireDefault(_FormatAlignCenter);

var _FormatAlignLeft = __webpack_require__(/*! material-ui-icons/FormatAlignLeft */ "./node_modules/material-ui-icons/FormatAlignLeft.js");

var _FormatAlignLeft2 = _interopRequireDefault(_FormatAlignLeft);

var _FormatAlignRight = __webpack_require__(/*! material-ui-icons/FormatAlignRight */ "./node_modules/material-ui-icons/FormatAlignRight.js");

var _FormatAlignRight2 = _interopRequireDefault(_FormatAlignRight);

var _FormatAlignJustify = __webpack_require__(/*! material-ui-icons/FormatAlignJustify */ "./node_modules/material-ui-icons/FormatAlignJustify.js");

var _FormatAlignJustify2 = _interopRequireDefault(_FormatAlignJustify);

var _AttachFile = __webpack_require__(/*! material-ui-icons/AttachFile */ "./node_modules/material-ui-icons/AttachFile.js");

var _AttachFile2 = _interopRequireDefault(_AttachFile);

var _InsertLink = __webpack_require__(/*! material-ui-icons/InsertLink */ "./node_modules/material-ui-icons/InsertLink.js");

var _InsertLink2 = _interopRequireDefault(_InsertLink);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _InsertEmoticon = __webpack_require__(/*! material-ui-icons/InsertEmoticon */ "./node_modules/material-ui-icons/InsertEmoticon.js");

var _InsertEmoticon2 = _interopRequireDefault(_InsertEmoticon);

var _InsertComment = __webpack_require__(/*! material-ui-icons/InsertComment */ "./node_modules/material-ui-icons/InsertComment.js");

var _InsertComment2 = _interopRequireDefault(_InsertComment);

var _Highlight = __webpack_require__(/*! material-ui-icons/Highlight */ "./node_modules/material-ui-icons/Highlight.js");

var _Highlight2 = _interopRequireDefault(_Highlight);

var _Functions = __webpack_require__(/*! material-ui-icons/Functions */ "./node_modules/material-ui-icons/Functions.js");

var _Functions2 = _interopRequireDefault(_Functions);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Atomic = __webpack_require__(/*! ./components/Atomic */ "./src/lib/mayash-editor/components/Atomic.js");

var _Atomic2 = _interopRequireDefault(_Atomic);

var _EditorStyles = __webpack_require__(/*! ./EditorStyles */ "./src/lib/mayash-editor/EditorStyles.js");

var _EditorStyles2 = _interopRequireDefault(_EditorStyles);

var _constants = __webpack_require__(/*! ./constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * MayashEditor
 */


// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

var MayashEditor = function (_Component) {
  (0, _inherits3.default)(MayashEditor, _Component);

  /**
   * Mayash Editor's proptypes are defined here.
   */
  function MayashEditor() {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, MayashEditor);

    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    var _this = (0, _possibleConstructorReturn3.default)(this, (MayashEditor.__proto__ || (0, _getPrototypeOf2.default)(MayashEditor)).call(this));

    _this.onChangeInsertPhoto = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _ref2, statusCode, error, payload, src, editorState, contentState, contentStateWithEntity, entityKey, middleEditorState, newEditorState;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // e.preventDefault();
                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 21;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _context.next = 6;
                return _this.props.apiPhotoUpload({
                  formData: formData
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle Error
                console.error(statusCode, error);
                return _context.abrupt('return');

              case 13:
                src = payload.photoUrl;
                editorState = _this.props.editorState;
                contentState = editorState.getCurrentContent();
                contentStateWithEntity = contentState.createEntity(_constants.Blocks.PHOTO, 'IMMUTABLE', { src: src });
                entityKey = contentStateWithEntity.getLastCreatedEntityKey();
                middleEditorState = _draftJs.EditorState.set(editorState, {
                  currentContent: contentStateWithEntity
                });
                newEditorState = _draftJs.AtomicBlockUtils.insertAtomicBlock(middleEditorState, entityKey, ' ');


                _this.onChange(newEditorState);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.onChange = function (editorState) {
      _this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    _this.focus = function () {
      return _this.editorNode.focus();
    };

    _this.onTab = _this.onTab.bind(_this);
    _this.onTitleClick = _this.onTitleClick.bind(_this);
    _this.onCodeClick = _this.onCodeClick.bind(_this);
    _this.onQuoteClick = _this.onQuoteClick.bind(_this);
    _this.onListBulletedClick = _this.onListBulletedClick.bind(_this);
    _this.onListNumberedClick = _this.onListNumberedClick.bind(_this);
    _this.onBoldClick = _this.onBoldClick.bind(_this);
    _this.onItalicClick = _this.onItalicClick.bind(_this);
    _this.onUnderLineClick = _this.onUnderLineClick.bind(_this);
    _this.onHighlightClick = _this.onHighlightClick.bind(_this);
    _this.handleKeyCommand = _this.handleKeyCommand.bind(_this);
    _this.onClickInsertPhoto = _this.onClickInsertPhoto.bind(_this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
    return _this;
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */


  (0, _createClass3.default)(MayashEditor, [{
    key: 'onTab',
    value: function onTab(e) {
      e.preventDefault();
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.onTab(e, editorState, 4);

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Title Button.
     * @function onTitleClick
     */

  }, {
    key: 'onTitleClick',
    value: function onTitleClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'header-one');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Code Button.
     * @function onCodeClick
     */

  }, {
    key: 'onCodeClick',
    value: function onCodeClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'code-block');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Quote Button.
     * @function onQuoteClick
     */

  }, {
    key: 'onQuoteClick',
    value: function onQuoteClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'blockquote');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with unordered list items
     * @function onListBulletedClick
     */

  }, {
    key: 'onListBulletedClick',
    value: function onListBulletedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'unordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with ordered list item.
     * @function onListNumberedClick
     */

  }, {
    key: 'onListNumberedClick',
    value: function onListNumberedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'ordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will give selected content Bold styling.
     * @function onBoldClick
     */

  }, {
    key: 'onBoldClick',
    value: function onBoldClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'BOLD');

      this.onChange(newEditorState);
    }

    /**
     * This function will give italic styling to selected content.
     * @function onItalicClick
     */

  }, {
    key: 'onItalicClick',
    value: function onItalicClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'ITALIC');

      this.onChange(newEditorState);
    }

    /**
     * This function will give underline styling to selected content.
     * @function onUnderLineClick
     */

  }, {
    key: 'onUnderLineClick',
    value: function onUnderLineClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');

      this.onChange(newEditorState);
    }

    /**
     * This function will highlight selected content.
     * @function onHighlightClick
     */

  }, {
    key: 'onHighlightClick',
    value: function onHighlightClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'CODE');

      this.onChange(newEditorState);
    }

    /**
     *
     */

  }, {
    key: 'onClickInsertPhoto',
    value: function onClickInsertPhoto() {
      this.photo.value = null;
      this.photo.click();
    }

    /**
     *
     */

  }, {
    key: 'handleKeyCommand',


    /**
     * This function will give custom handle commands to Editor.
     * @function handleKeyCommand
     * @param {string} command
     * @return {string}
     */
    value: function handleKeyCommand(command) {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.handleKeyCommand(editorState, command);

      if (newEditorState) {
        this.onChange(newEditorState);
        return _constants.HANDLED;
      }

      return _constants.NOT_HANDLED;
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockRendererFn',
    value: function blockRendererFn(block) {
      switch (block.getType()) {
        case _constants.Blocks.ATOMIC:
          return {
            component: _Atomic2.default,
            editable: false
          };

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockStyleFn',
    value: function blockStyleFn(block) {
      switch (block.getType()) {
        case 'blockquote':
          return 'RichEditor-blockquote';

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          readOnly = _props.readOnly,
          onChange = _props.onChange,
          editorState = _props.editorState,
          placeholder = _props.placeholder;

      // Custom overrides for "code" style.

      var styleMap = {
        CODE: {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
          fontSize: 16,
          padding: 2
        }
      };

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        !readOnly ? _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-controls': '' }) },
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Title', id: 'title', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Title', onClick: this.onTitleClick },
              _react2.default.createElement(_Title2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Bold', id: 'bold', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Bold', onClick: this.onBoldClick },
              _react2.default.createElement(_FormatBold2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Italic', id: 'italic', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Italic', onClick: this.onItalicClick },
              _react2.default.createElement(_FormatItalic2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Underline', id: 'underline', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Underline',
                onClick: this.onUnderLineClick
              },
              _react2.default.createElement(_FormatUnderlined2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Code', id: 'code', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Code', onClick: this.onCodeClick },
              _react2.default.createElement(_Code2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Quote', id: 'quote', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Quote', onClick: this.onQuoteClick },
              _react2.default.createElement(_FormatQuote2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Unordered List',
              id: 'unorderd-list',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Unordered List',
                onClick: this.onListBulletedClick
              },
              _react2.default.createElement(_FormatListBulleted2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Ordered List', id: 'ordered-list', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Ordered List',
                onClick: this.onListNumberedClick
              },
              _react2.default.createElement(_FormatListNumbered2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Left', id: 'align-left', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Left', disabled: true },
              _react2.default.createElement(_FormatAlignLeft2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Center', id: 'align-center', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Center', disabled: true },
              _react2.default.createElement(_FormatAlignCenter2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignRight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignJustify2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Attach File', id: 'attach-file', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Attach File', disabled: true },
              _react2.default.createElement(_AttachFile2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Link', id: 'insert-link', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Link', disabled: true },
              _react2.default.createElement(_InsertLink2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Photo', id: 'insert-photo', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Insert Photo',
                disabled: typeof this.props.apiPhotoUpload !== 'function',
                onClick: this.onClickInsertPhoto
              },
              _react2.default.createElement(_InsertPhoto2.default, null),
              _react2.default.createElement('input', {
                type: 'file',
                accept: 'image/jpeg|png|gif',
                onChange: this.onChangeInsertPhoto,
                ref: function ref(photo) {
                  _this3.photo = photo;
                },
                style: { display: 'none' }
              })
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Emoticon',
              id: 'insertE-emoticon',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Emoticon', disabled: true },
              _react2.default.createElement(_InsertEmoticon2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Comment',
              id: 'insert-comment',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Comment', disabled: true },
              _react2.default.createElement(_InsertComment2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Highlight Text',
              id: 'highlight-text',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Highlight Text',
                onClick: this.onHighlightClick
              },
              _react2.default.createElement(_Highlight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Add Functions',
              id: 'add-functions',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Add Functions', disabled: true },
              _react2.default.createElement(_Functions2.default, null)
            )
          )
        ) : null,
        _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-area': '' }) },
          _react2.default.createElement(_draftJs.Editor
          /* Basics */

          , { editorState: editorState,
            onChange: onChange
            /* Presentation */

            , placeholder: placeholder
            // textAlignment="center"

            // textDirectionality="LTR"

            , blockRendererFn: this.blockRendererFn,
            blockStyleFn: this.blockStyleFn,
            customStyleMap: styleMap
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            , readOnly: readOnly,
            spellCheck: true
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            , handleKeyCommand: this.handleKeyCommand
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            , onTab: this.onTab
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            , ref: function ref(node) {
              _this3.editorNode = node;
            }
          })
        )
      );
    }
  }]);
  return MayashEditor;
}(_react.Component);

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

// import Icon from 'material-ui/Icon';
/** @format */

MayashEditor.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  editorState: _ReactPropTypes2.default.object.isRequired,
  onChange: _ReactPropTypes2.default.func.isRequired,

  placeholder: _ReactPropTypes2.default.string,

  readOnly: _ReactPropTypes2.default.bool.isRequired,

  /**
   * This api function should be applied for enabling photo adding
   * feature in Mayash Editor.
   */
  apiPhotoUpload: _ReactPropTypes2.default.func
};
MayashEditor.defaultProps = {
  readOnly: true,
  placeholder: 'Write Here...'
};
exports.default = (0, _withStyles2.default)(_EditorStyles2.default)(MayashEditor);

/***/ }),

/***/ "./src/lib/mayash-editor/EditorState.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/EditorState.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = undefined;

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Decorators = __webpack_require__(/*! ./components/Decorators */ "./src/lib/mayash-editor/components/Decorators.js");

/** @format */

var defaultDecorators = new _draftJs.CompositeDecorator([{
  strategy: _Decorators.handleStrategy,
  component: _Decorators.HandleSpan
}, {
  strategy: _Decorators.hashtagStrategy,
  component: _Decorators.HashtagSpan
}]);

var createEditorState = exports.createEditorState = function createEditorState() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var decorators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDecorators;

  if (content === null) {
    return _draftJs.EditorState.createEmpty(decorators);
  }
  return _draftJs.EditorState.createWithContent((0, _draftJs.convertFromRaw)(content), decorators);
};

exports.default = createEditorState;

/***/ }),

/***/ "./src/lib/mayash-editor/EditorStyles.js":
/*!***********************************************!*\
  !*** ./src/lib/mayash-editor/EditorStyles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the CSS-in-JS styles of Editor component.
 *
 * @format
 */

exports.default = {
  '@global': {
    '.RichEditor-root': {
      background: '#fff',
      border: '1px solid #ddd',
      fontFamily: "'Georgia', serif",
      fontSize: '14px',
      padding: '15px'
    },
    '.RichEditor-editor': {
      borderTop: '1px solid #ddd',
      cursor: 'text',
      fontSize: '16px',
      marginTop: '10px'
    },
    '.public-DraftEditorPlaceholder-root': {
      margin: '0 -15px -15px',
      padding: '15px'
    },
    '.public-DraftEditor-content': {
      margin: '0 -15px -15px',
      padding: '15px'
      // minHeight: '100px',
    },
    '.RichEditor-blockquote': {
      backgroundColor: '5px solid #eee',
      borderLeft: '5px solid #eee',
      color: '#666',
      fontFamily: "'Hoefler Text', 'Georgia', serif",
      fontStyle: 'italic',
      margin: '16px 0',
      padding: '10px 20px'
    },
    '.public-DraftStyleDefault-pre': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: "'Inconsolata', 'Menlo', 'Consolas', monospace",
      fontSize: '16px',
      padding: '20px'
    }
  },
  root: {
    // padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/***/ }),

/***/ "./src/lib/mayash-editor/components/Atomic.js":
/*!****************************************************!*\
  !*** ./src/lib/mayash-editor/components/Atomic.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = __webpack_require__(/*! ../constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial'
  }
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
/**
 *
 * @format
 */

var Atomic = function (_Component) {
  (0, _inherits3.default)(Atomic, _Component);

  function Atomic(props) {
    (0, _classCallCheck3.default)(this, Atomic);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Atomic.__proto__ || (0, _getPrototypeOf2.default)(Atomic)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Atomic, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          contentState = _props.contentState,
          block = _props.block;


      var entity = contentState.getEntity(block.getEntityAt(0));

      var _entity$getData = entity.getData(),
          src = _entity$getData.src;

      var type = entity.getType();

      if (type === _constants.Blocks.PHOTO) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('img', { alt: 'alt', src: src, style: styles.photo })
        );
      }

      return _react2.default.createElement('div', null);
    }
  }]);
  return Atomic;
}(_react.Component);

Atomic.propTypes = {
  contentState: _propTypes2.default.object.isRequired,
  block: _propTypes2.default.any.isRequired
};
exports.default = Atomic;

/***/ }),

/***/ "./src/lib/mayash-editor/components/Decorators.js":
/*!********************************************************!*\
  !*** ./src/lib/mayash-editor/components/Decorators.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashtagSpan = exports.HandleSpan = undefined;
exports.handleStrategy = handleStrategy;
exports.hashtagStrategy = hashtagStrategy;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _style = __webpack_require__(/*! ../style */ "./src/lib/mayash-editor/style/index.js");

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
// eslint is disabled here for now.
var HANDLE_REGEX = /\@[\w]+/g;
var HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function findWithRegex(regex, contentBlock, callback) {
  var text = contentBlock.getText();
  var matchArr = void 0,
      start = void 0;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

var HandleSpan = exports.HandleSpan = function HandleSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.handle },
    props.children
  );
};

var HashtagSpan = exports.HashtagSpan = function HashtagSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.hashtag },
    props.children
  );
};

exports.default = {
  handleStrategy: handleStrategy,
  HandleSpan: HandleSpan,

  hashtagStrategy: hashtagStrategy,
  HashtagSpan: HashtagSpan
};

/***/ }),

/***/ "./src/lib/mayash-editor/constants.js":
/*!********************************************!*\
  !*** ./src/lib/mayash-editor/constants.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
var Blocks = exports.Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video'
};

/**
 * @constant Inline
 */
var Inline = exports.Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT'
};

/**
 * @constant Entity
 */
var Entity = exports.Entity = {
  LINK: 'LINK'
};

/**
 * @constant HYPERLINK
 */
var HYPERLINK = exports.HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
var HANDLED = exports.HANDLED = 'handled';
var NOT_HANDLED = exports.NOT_HANDLED = 'not_handled';

exports.default = {
  Blocks: Blocks,
  Inline: Inline,
  Entity: Entity
};

/***/ }),

/***/ "./src/lib/mayash-editor/index.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-editor/index.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = exports.MayashEditor = undefined;

var _Editor = __webpack_require__(/*! ./Editor */ "./src/lib/mayash-editor/Editor.js");

Object.defineProperty(exports, 'MayashEditor', {
  enumerable: true,
  get: function get() {
    return _Editor.MayashEditor;
  }
});

var _EditorState = __webpack_require__(/*! ./EditorState */ "./src/lib/mayash-editor/EditorState.js");

Object.defineProperty(exports, 'createEditorState', {
  enumerable: true,
  get: function get() {
    return _EditorState.createEditorState;
  }
});

var _Editor2 = _interopRequireDefault(_Editor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Editor2.default;

/***/ }),

/***/ "./src/lib/mayash-editor/style/index.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/style/index.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @format */

var style = exports.style = {
  root: {
    padding: 20,
    width: 600
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10
  },
  button: {
    marginTop: 10,
    textAlign: 'center'
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override'
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)'
  }
};

exports.default = style;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FZGl0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9FeHBhbmRNb3JlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRVbmRlcmxpbmVkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GdW5jdGlvbnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0hpZ2hsaWdodC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0Q29tbWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9TYXZlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9pbnRlcm5hbC90cmFuc2l0aW9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9Db2xsYXBzZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VzZXJzL21vZHVsZXMvZGVsZXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VzZXJzL21vZHVsZXMvdXBkYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy9kZWxldGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvY291cnNlcy91c2Vycy9tb2R1bGVzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbXBvbmVudHMvVGl0bGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvTW9kdWxlLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3IuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvclN0YXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdHlsZXMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2NvbXBvbmVudHMvQXRvbWljLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0RlY29yYXRvcnMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2NvbnN0YW50cy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL3N0eWxlL2luZGV4LmpzIl0sIm5hbWVzIjpbImRlbGV0ZU1vZHVsZSIsInBheWxvYWQiLCJ0eXBlIiwidXBkYXRlIiwidG9rZW4iLCJ1c2VySWQiLCJjb3Vyc2VJZCIsIm1vZHVsZUlkIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJ0aXRsZSIsImRhdGEiLCJib2R5Iiwic3R5bGVzIiwidmFsdWUiLCJ0ZXh0YXJlYSIsIndpZHRoIiwiaGVpZ2h0IiwiYm9yZGVyIiwicmVzaXplIiwiZm9udCIsIlRpdGxlIiwiY2xhc3NlcyIsInJlYWRPbmx5IiwicGxhY2Vob2xkZXIiLCJvbkNoYW5nZSIsIm1pbkxlbmd0aCIsIm1heExlbmd0aCIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJib29sIiwic3RyaW5nIiwiZnVuYyIsIm51bWJlciIsImRlZmF1bHRQcm9wcyIsInRoZW1lIiwicm9vdCIsInBhZGRpbmdCb3R0b20iLCJjYXJkIiwiYm9yZGVyUmFkaXVzIiwiZXhwYW5kIiwidHJhbnNmb3JtIiwidHJhbnNpdGlvbiIsInRyYW5zaXRpb25zIiwiY3JlYXRlIiwiZHVyYXRpb24iLCJzaG9ydGVzdCIsImV4cGFuZE9wZW4iLCJmbGV4R3JvdyIsImZsZXgiLCJNb2R1bGUiLCJwcm9wcyIsIm9uQ2hhbmdlVGl0bGUiLCJlIiwic2V0U3RhdGUiLCJ0YXJnZXQiLCJvbk1vdXNlRW50ZXIiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsInN0YXRlIiwiZXhwYW5kZWQiLCJlZGl0IiwibWVzc2FnZSIsImhhbmRsZUV4cGFuZENsaWNrIiwiYmluZCIsIm9uRWRpdCIsIm9uU2F2ZSIsIm9uQ2xpY2tEZWxldGUiLCJ0aXRsZVByb3AiLCJlbGVtZW50cyIsInVzZXIiLCJnZXRDdXJyZW50Q29udGVudCIsImFjdGlvblVwZGF0ZSIsImF1dGhvcklkIiwiY291cnNlIiwiaWQiLCJhY3Rpb25EZWxldGUiLCJpbmRleCIsImRhdGFQcm9wIiwiaXNTaWduZWRJbiIsIm1hcFN0YXRlVG9Qcm9wcyIsIm1hcERpc3BhdGNoVG9Qcm9wcyIsImRpc3BhdGNoIiwiTWF5YXNoRWRpdG9yIiwib25DaGFuZ2VJbnNlcnRQaG90byIsImZpbGUiLCJmaWxlcyIsImluZGV4T2YiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiYXBpUGhvdG9VcGxvYWQiLCJzcmMiLCJwaG90b1VybCIsImVkaXRvclN0YXRlIiwiY29udGVudFN0YXRlIiwiY29udGVudFN0YXRlV2l0aEVudGl0eSIsImNyZWF0ZUVudGl0eSIsIlBIT1RPIiwiZW50aXR5S2V5IiwiZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkiLCJtaWRkbGVFZGl0b3JTdGF0ZSIsInNldCIsImN1cnJlbnRDb250ZW50IiwibmV3RWRpdG9yU3RhdGUiLCJpbnNlcnRBdG9taWNCbG9jayIsImZvY3VzIiwiZWRpdG9yTm9kZSIsIm9uVGFiIiwib25UaXRsZUNsaWNrIiwib25Db2RlQ2xpY2siLCJvblF1b3RlQ2xpY2siLCJvbkxpc3RCdWxsZXRlZENsaWNrIiwib25MaXN0TnVtYmVyZWRDbGljayIsIm9uQm9sZENsaWNrIiwib25JdGFsaWNDbGljayIsIm9uVW5kZXJMaW5lQ2xpY2siLCJvbkhpZ2hsaWdodENsaWNrIiwiaGFuZGxlS2V5Q29tbWFuZCIsIm9uQ2xpY2tJbnNlcnRQaG90byIsInByZXZlbnREZWZhdWx0IiwidG9nZ2xlQmxvY2tUeXBlIiwidG9nZ2xlSW5saW5lU3R5bGUiLCJwaG90byIsImNsaWNrIiwiY29tbWFuZCIsImJsb2NrIiwiZ2V0VHlwZSIsIkFUT01JQyIsImNvbXBvbmVudCIsImVkaXRhYmxlIiwic3R5bGVNYXAiLCJDT0RFIiwiYmFja2dyb3VuZENvbG9yIiwiZm9udEZhbWlseSIsImZvbnRTaXplIiwicGFkZGluZyIsImRpc3BsYXkiLCJibG9ja1JlbmRlcmVyRm4iLCJibG9ja1N0eWxlRm4iLCJub2RlIiwiZGVmYXVsdERlY29yYXRvcnMiLCJzdHJhdGVneSIsImNyZWF0ZUVkaXRvclN0YXRlIiwiY29udGVudCIsImRlY29yYXRvcnMiLCJjcmVhdGVFbXB0eSIsImNyZWF0ZVdpdGhDb250ZW50IiwiYmFja2dyb3VuZCIsImJvcmRlclRvcCIsImN1cnNvciIsIm1hcmdpblRvcCIsIm1hcmdpbiIsImJvcmRlckxlZnQiLCJjb2xvciIsImZvbnRTdHlsZSIsIndoaXRlU3BhY2UiLCJBdG9taWMiLCJlbnRpdHkiLCJnZXRFbnRpdHkiLCJnZXRFbnRpdHlBdCIsImdldERhdGEiLCJhbnkiLCJoYW5kbGVTdHJhdGVneSIsImhhc2h0YWdTdHJhdGVneSIsIkhBTkRMRV9SRUdFWCIsIkhBU0hUQUdfUkVHRVgiLCJmaW5kV2l0aFJlZ2V4IiwicmVnZXgiLCJjb250ZW50QmxvY2siLCJjYWxsYmFjayIsInRleHQiLCJnZXRUZXh0IiwibWF0Y2hBcnIiLCJzdGFydCIsImV4ZWMiLCJsZW5ndGgiLCJIYW5kbGVTcGFuIiwiaGFuZGxlIiwiY2hpbGRyZW4iLCJIYXNodGFnU3BhbiIsImhhc2h0YWciLCJCbG9ja3MiLCJVTlNUWUxFRCIsIlBBUkFHUkFQSCIsIkgxIiwiSDIiLCJIMyIsIkg0IiwiSDUiLCJINiIsIk9MIiwiVUwiLCJCTE9DS1FVT1RFIiwiVklERU8iLCJJbmxpbmUiLCJCT0xEIiwiSVRBTElDIiwiU1RSSUtFVEhST1VHSCIsIlVOREVSTElORSIsIkhJR0hMSUdIVCIsIkVudGl0eSIsIkxJTksiLCJIWVBFUkxJTksiLCJIQU5ETEVEIiwiTk9UX0hBTkRMRUQiLCJzdHlsZSIsImVkaXRvciIsIm1pbkhlaWdodCIsImJ1dHRvbiIsInRleHRBbGlnbiIsImRpcmVjdGlvbiIsInVuaWNvZGVCaWRpIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb1FBQW9ROztBQUV0VDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0dBQWtHOztBQUVwSjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QscUZBQXFGOztBQUV2STtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHlCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsNkpBQTZKOztBQUUvTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb0RBQW9EOztBQUV0RztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsbUZBQW1GOztBQUVySTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG9DOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0ZBQWtGOztBQUVwSTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb0ZBQW9GOztBQUV0STtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsbUZBQW1GOztBQUVySTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsNk9BQTZPOztBQUUvUjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsd0RBQXdEOztBQUUxRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsdVJBQXVSOztBQUV6VTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0pBQWtKOztBQUVwTTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsZ0RBQWdEOztBQUVsRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDhCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsMkhBQTJIOztBQUU3SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsa0RBQWtEOztBQUVwRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsb0lBQW9JOztBQUV0TDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsc0hBQXNIOztBQUV4SztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QseVdBQXlXOztBQUUzWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsMk5BQTJOOztBQUU3UTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDZCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsZ0lBQWdJOztBQUVsTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLDhCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0Qsc0pBQXNKOztBQUV4TTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDOztBQUVEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixrREFBa0QsK0JBQStCOztBQUVqRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHdCOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLG1QQUE0STtBQUM1STs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1AsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RTtBQUM5RTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsV0FBVywyQkFBMkI7QUFDdEM7QUFDQTtBQUNBLGFBQWEsMEJBQTBCO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7O0FBRUE7QUFDQSxXQUFXO0FBQ1g7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHdCQUF3QixjOzs7Ozs7Ozs7Ozs7O0FDck83RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxFOzs7Ozs7Ozs7Ozs7O0FDaEJBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0EsT0FBTztBQUNQO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0dBQXNHO0FBQ3RHLDhDQUE4QztBQUM5QztBQUNBLGVBQWU7QUFDZixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGlCQUFpQixrQ0FBa0M7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUMsWTs7Ozs7Ozs7Ozs7OztBQzFWRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNmQTs7QUFFQTs7QUFFQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSw4RDs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDekRBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsVUFBVSxxQkFBcUI7QUFDNUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1RBOztBQWFBOzs7Ozs7OztBQVFBLElBQU1BLGVBQWUsU0FBZkEsWUFBZSxDQUFDQyxPQUFEO0FBQUEsU0FBK0I7QUFDbERDLGdDQURrRDtBQUVsREQ7QUFGa0QsR0FBL0I7QUFBQSxDQUFyQixDLENBMUJBOzs7OztrQkErQmVELFk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxQmY7O0FBZUE7Ozs7Ozs7Ozs7O0FBV0EsSUFBTUcsU0FBUyxTQUFUQSxNQUFTLENBQUNGLE9BQUQ7QUFBQSxTQUErQixFQUFFQyw0QkFBRixFQUF1QkQsZ0JBQXZCLEVBQS9CO0FBQUEsQ0FBZixDLENBL0JBOzs7OztrQkFpQ2VFLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqQmY7Ozs7Ozs7Ozs7Ozs7QUFoQkE7Ozs7Ozs7c0ZBNkJBO0FBQUEsUUFBOEJDLEtBQTlCLFNBQThCQSxLQUE5QjtBQUFBLFFBQXFDQyxNQUFyQyxTQUFxQ0EsTUFBckM7QUFBQSxRQUE2Q0MsUUFBN0MsU0FBNkNBLFFBQTdDO0FBQUEsUUFBdURDLFFBQXZELFNBQXVEQSxRQUF2RDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ0gsTUFGckMsaUJBRXVEQyxRQUZ2RCxpQkFFMkVDLFFBRjNFO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsUUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlUjtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVVMsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlLLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFGLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVqQixZOzs7OztBQXZCZjs7OztBQUNBOzs7O2tCQXlEZUEsWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5Q2Y7Ozs7Ozs7Ozs7Ozs7OztBQWxCQTs7Ozs7OztzRkFpQ0E7QUFBQSxRQUNFSSxLQURGLFNBQ0VBLEtBREY7QUFBQSxRQUVFQyxNQUZGLFNBRUVBLE1BRkY7QUFBQSxRQUdFQyxRQUhGLFNBR0VBLFFBSEY7QUFBQSxRQUlFQyxRQUpGLFNBSUVBLFFBSkY7QUFBQSxRQUtFYSxLQUxGLFNBS0VBLEtBTEY7QUFBQSxRQU1FQyxJQU5GLFNBTUVBLElBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFTVWIsZUFUVixrQ0FTcUNILE1BVHJDLGlCQVN1REMsUUFUdkQsaUJBUzJFQyxRQVQzRTtBQUFBO0FBQUEsbUJBV3NCLCtCQUFNQyxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVI7QUFIUixlQUZrQjtBQU8zQmtCLG9CQUFNLHlCQUFlLEVBQUVGLFlBQUYsRUFBU0MsVUFBVCxFQUFmO0FBUHFCLGFBQVgsQ0FYdEI7O0FBQUE7QUFXVVIsZUFYVjtBQXFCWUMsa0JBckJaLEdBcUJtQ0QsR0FyQm5DLENBcUJZQyxNQXJCWixFQXFCb0JDLFVBckJwQixHQXFCbUNGLEdBckJuQyxDQXFCb0JFLFVBckJwQjs7QUFBQSxrQkF1QlFELFVBQVUsR0F2QmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQXdCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQXhCYjs7QUFBQTtBQUFBO0FBQUEsbUJBOEJ1QkYsSUFBSUssSUFBSixFQTlCdkI7O0FBQUE7QUE4QlVBLGdCQTlCVjtBQUFBLHdFQWdDZ0JBLElBaENoQjs7QUFBQTtBQUFBO0FBQUE7O0FBa0NJQyxvQkFBUUYsS0FBUjs7QUFsQ0osNkNBb0NXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQXBDWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWQsTTs7Ozs7QUEzQmY7Ozs7QUFDQTs7OztrQkFxRWVBLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNwRWY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUEsSUFBTW9CLFNBQVM7QUFDYkMsU0FBTyxFQURNO0FBRWJDLFlBQVU7QUFDUkMsV0FBTyxNQURDO0FBRVJDLFlBQVEsTUFGQTtBQUdSQyxZQUFRLE1BSEE7QUFJUjtBQUNBQyxZQUFRLE1BTEE7QUFNUkMsVUFBTTtBQU5FO0FBRkcsQ0FBZixDLENBYkE7Ozs7Ozs7O0FBeUJBLElBQU1DLFFBQVEsU0FBUkEsS0FBUSxPQVFSO0FBQUEsTUFQSkMsT0FPSSxRQVBKQSxPQU9JO0FBQUEsTUFOSkMsUUFNSSxRQU5KQSxRQU1JO0FBQUEsTUFMSlQsS0FLSSxRQUxKQSxLQUtJO0FBQUEsTUFKSlUsV0FJSSxRQUpKQSxXQUlJO0FBQUEsTUFISkMsUUFHSSxRQUhKQSxRQUdJO0FBQUEsTUFGSkMsU0FFSSxRQUZKQSxTQUVJO0FBQUEsTUFESkMsU0FDSSxRQURKQSxTQUNJOztBQUNKLE1BQUlKLFFBQUosRUFBYztBQUNaLFdBQU87QUFBQTtBQUFBLFFBQUssV0FBV0QsUUFBUVIsS0FBeEI7QUFBZ0NBO0FBQWhDLEtBQVA7QUFDRDs7QUFFRCxTQUNFO0FBQ0UsaUJBQWFVLFdBRGY7QUFFRSxXQUFPVixLQUZUO0FBR0UsY0FBVVcsUUFIWjtBQUlFLGVBQVdILFFBQVFQLFFBSnJCO0FBS0UsZUFBV1csYUFBYSxDQUwxQjtBQU1FLGVBQVdDLGFBQWEsR0FOMUI7QUFPRSxVQUFNLENBUFI7QUFRRSxjQUFVSjtBQVJaLElBREY7QUFZRCxDQXpCRDs7QUEyQkFGLE1BQU1PLFNBQU4sR0FBa0I7QUFDaEJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFY7QUFFaEJQLFlBQVUsb0JBQVVRLElBQVYsQ0FBZUQsVUFGVDtBQUdoQk4sZUFBYSxvQkFBVVEsTUFBVixDQUFpQkYsVUFIZDtBQUloQmhCLFNBQU8sb0JBQVVrQixNQUFWLENBQWlCRixVQUpSO0FBS2hCTCxZQUFVLG9CQUFVUSxJQUFWLENBQWVILFVBTFQ7QUFNaEJKLGFBQVcsb0JBQVVRLE1BTkw7QUFPaEJQLGFBQVcsb0JBQVVPO0FBUEwsQ0FBbEI7O0FBVUFiLE1BQU1jLFlBQU4sR0FBcUI7QUFDbkJyQixTQUFPLFVBRFk7QUFFbkJVLGVBQWEsb0JBRk07QUFHbkJELFlBQVUsSUFIUztBQUluQkcsYUFBVyxDQUpRO0FBS25CQyxhQUFXO0FBTFEsQ0FBckI7O2tCQVFlLHdCQUFXZCxNQUFYLEVBQW1CUSxLQUFuQixDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEVmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTVIsU0FBUyxTQUFUQSxNQUFTLENBQUN1QixLQUFEO0FBQUEsU0FBWTtBQUN6QkMsVUFBTTtBQUNKQyxxQkFBZTtBQURYLEtBRG1CO0FBSXpCQyxVQUFNO0FBQ0pDLG9CQUFjO0FBRFYsS0FKbUI7QUFPekJDLFlBQVE7QUFDTkMsaUJBQVcsY0FETDtBQUVOQyxrQkFBWVAsTUFBTVEsV0FBTixDQUFrQkMsTUFBbEIsQ0FBeUIsV0FBekIsRUFBc0M7QUFDaERDLGtCQUFVVixNQUFNUSxXQUFOLENBQWtCRSxRQUFsQixDQUEyQkM7QUFEVyxPQUF0QztBQUZOLEtBUGlCO0FBYXpCQyxnQkFBWTtBQUNWTixpQkFBVztBQURELEtBYmE7QUFnQnpCTyxjQUFVO0FBQ1JDLFlBQU07QUFERTtBQWhCZSxHQUFaO0FBQUEsQ0FBZixDLENBL0JBOztJQW9ETUMsTTs7O0FBQ0osa0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxzSUFDWEEsS0FEVzs7QUFBQSxVQXlCbkJDLGFBekJtQixHQXlCSCxVQUFDQyxDQUFEO0FBQUEsYUFBTyxNQUFLQyxRQUFMLENBQWMsRUFBRTdDLE9BQU80QyxFQUFFRSxNQUFGLENBQVMxQyxLQUFsQixFQUFkLENBQVA7QUFBQSxLQXpCRzs7QUFBQSxVQWdIbkIyQyxZQWhIbUIsR0FnSEo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFRyxPQUFPLElBQVQsRUFBZCxDQUFOO0FBQUEsS0FoSEk7O0FBQUEsVUFpSG5CQyxZQWpIbUIsR0FpSEo7QUFBQSxhQUFNLE1BQUtKLFFBQUwsQ0FBYyxFQUFFRyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0FqSEk7O0FBQUEsUUFFVGhELEtBRlMsR0FFTzBDLEtBRlAsQ0FFVDFDLEtBRlM7QUFBQSxRQUVGQyxJQUZFLEdBRU95QyxLQUZQLENBRUZ6QyxJQUZFOztBQUdqQixVQUFLaUQsS0FBTCxHQUFhO0FBQ1hGLGFBQU8sS0FESTtBQUVYRyxnQkFBVSxLQUZDO0FBR1hDLFlBQU0sS0FISzs7QUFLWHBELGtCQUxXO0FBTVhDLFlBQU0scUNBQWtCQSxJQUFsQixDQU5LOztBQVFYb0QsZUFBUztBQVJFLEtBQWI7O0FBV0EsVUFBS0MsaUJBQUwsR0FBeUIsTUFBS0EsaUJBQUwsQ0FBdUJDLElBQXZCLE9BQXpCO0FBQ0EsVUFBS3hDLFFBQUwsR0FBZ0IsTUFBS0EsUUFBTCxDQUFjd0MsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlELElBQVosT0FBZDtBQUNBLFVBQUtFLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlGLElBQVosT0FBZDtBQUNBLFVBQUtHLGFBQUwsR0FBcUIsTUFBS0EsYUFBTCxDQUFtQkgsSUFBbkIsT0FBckI7QUFsQmlCO0FBbUJsQjs7Ozs2QkFFUXRELEksRUFBTTtBQUNiLFdBQUs0QyxRQUFMLENBQWMsRUFBRTVDLFVBQUYsRUFBZDtBQUNEOzs7NkJBSVE7QUFDUCxXQUFLNEMsUUFBTCxDQUFjO0FBQ1pPLGNBQU0sQ0FBQyxLQUFLRixLQUFMLENBQVdFLElBRE47QUFFWkQsa0JBQVU7QUFGRSxPQUFkO0FBSUQ7Ozs7Ozs7Ozs7Ozt5QkFJOEQsS0FBS1QsSyxFQUF4RHhELFEsVUFBQUEsUSxFQUFVQyxRLFVBQUFBLFEsRUFBaUJ3RSxTLFVBQVAzRCxLLEVBQWtCNEQsUSxVQUFBQSxRO2lDQUNwQkEsU0FBU0MsSSxFQUEzQjVFLE0sa0JBQUFBLE0sRUFBUUQsSyxrQkFBQUEsSzt5QkFFYyxLQUFLa0UsSyxFQUEzQkUsSSxVQUFBQSxJLEVBQU1wRCxLLFVBQUFBLEssRUFBT0MsSSxVQUFBQSxJO0FBRWpCQyxvQixHQUFPLEU7OztBQUVYLG9CQUFJeUQsY0FBYzNELEtBQWxCLEVBQXlCO0FBQ3ZCRSxvREFBWUEsSUFBWixJQUFrQkYsWUFBbEI7QUFDRDs7QUFFREUsa0RBQ0tBLElBREw7QUFFRUQsd0JBQU0sMkJBQWFBLEtBQUs2RCxpQkFBTCxFQUFiO0FBRlI7Ozt1QkFLNkM7QUFDM0M5RSw4QkFEMkM7QUFFM0NDLGdDQUYyQztBQUczQ0Msb0NBSDJDO0FBSTNDQztBQUoyQyxtQkFLeENlLElBTHdDLEU7Ozs7QUFBckNOLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPd0QsdUIsU0FBQUEsTzs7c0JBUXZCekQsY0FBYyxHOzs7OztBQUNoQixxQkFBS2lELFFBQUwsQ0FBYyxFQUFFUSxTQUFTeEQsS0FBWCxFQUFkOzs7OztBQUlGLHFCQUFLZ0QsUUFBTCxDQUFjLEVBQUVRLGdCQUFGLEVBQWQ7O0FBRUEscUJBQUtYLEtBQUwsQ0FBV3FCLFlBQVg7QUFDRUMsNEJBQVUvRSxNQURaO0FBRUVDLG9DQUZGO0FBR0VDO0FBSEYsbUJBSUtlLElBSkw7QUFLRU47QUFMRjs7QUFRQSxxQkFBS2lELFFBQUwsQ0FBYyxFQUFFTyxNQUFNLENBQUNBLElBQVQsRUFBZDs7Ozs7Ozs7QUFFQXJELHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7MEJBTW1DLEtBQUs2QyxLLEVBQWhDdkQsUSxXQUFBQSxRLEVBQVUwRSxJLFdBQUFBLEksRUFBTUksTSxXQUFBQSxNO0FBQ2hCRCx3QixHQUF1QkMsTSxDQUF2QkQsUSxFQUFVOUUsUSxHQUFhK0UsTSxDQUFiL0UsUTtBQUNWRixxQixHQUFzQjZFLEksQ0FBdEI3RSxLLEVBQVdDLE0sR0FBVzRFLEksQ0FBZkssRTs7dUJBRThCLHNCQUFnQjtBQUMzRGxGLDhCQUQyRDtBQUUzREMsZ0NBRjJEO0FBRzNEQyxvQ0FIMkQ7QUFJM0RDO0FBSjJELGlCQUFoQixDOzs7O0FBQXJDUywwQixTQUFBQSxVO0FBQVl5RCx1QixTQUFBQSxPO0FBQVN4RCxxQixTQUFBQSxLOztzQkFPekJELGNBQWMsRzs7Ozs7QUFDaEIscUJBQUtpRCxRQUFMLENBQWMsRUFBRVEsU0FBU3hELEtBQVgsRUFBZDs7Ozs7QUFJRixxQkFBS2dELFFBQUwsQ0FBYyxFQUFFUSxnQkFBRixFQUFkOztBQUVBLHFCQUFLWCxLQUFMLENBQVd5QixZQUFYLENBQXdCO0FBQ3RCdkUsd0NBRHNCO0FBRXRCVCxvQ0FGc0I7QUFHdEJELG9DQUhzQjtBQUl0QjhFO0FBSnNCLGlCQUF4Qjs7Ozs7Ozs7QUFPQWpFLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7d0NBT2dCO0FBQ2xCLFdBQUtnRCxRQUFMLENBQWMsRUFBRU0sVUFBVSxDQUFDLEtBQUtELEtBQUwsQ0FBV0MsUUFBeEIsRUFBZDtBQUNEOzs7NkJBRVE7QUFBQSxvQkFDd0QsS0FBS1QsS0FEN0Q7QUFBQSxVQUNDOUIsT0FERCxXQUNDQSxPQUREO0FBQUEsVUFDVWdELFFBRFYsV0FDVUEsUUFEVjtBQUFBLFVBQ29CSSxRQURwQixXQUNvQkEsUUFEcEI7QUFBQSxVQUM4QkksS0FEOUIsV0FDOEJBLEtBRDlCO0FBQUEsVUFDMkNDLFFBRDNDLFdBQ3FDcEUsSUFEckM7QUFBQSw0QkFFNEIyRCxTQUFTQyxJQUZyQztBQUFBLFVBRUs1RSxNQUZMLG1CQUVDaUYsRUFGRDtBQUFBLFVBRWFJLFVBRmIsbUJBRWFBLFVBRmI7QUFBQSxvQkFJaUMsS0FBS3BCLEtBSnRDO0FBQUEsVUFJQ0UsSUFKRCxXQUlDQSxJQUpEO0FBQUEsVUFJT0QsUUFKUCxXQUlPQSxRQUpQO0FBQUEsVUFJaUJuRCxLQUpqQixXQUlpQkEsS0FKakI7QUFBQSxVQUl3QkMsSUFKeEIsV0FJd0JBLElBSnhCOzs7QUFNUCxhQUNFO0FBQUE7QUFBQTtBQUNFLHFCQUFXVyxRQUFRZSxJQURyQjtBQUVFLHdCQUFjLEtBQUtvQixZQUZyQjtBQUdFLHdCQUFjLEtBQUtFO0FBSHJCO0FBS0U7QUFBQTtBQUFBLFlBQU0sUUFBUSxLQUFLQyxLQUFMLENBQVdGLEtBQXpCLEVBQWdDLFdBQVdwQyxRQUFRaUIsSUFBbkQ7QUFDRTtBQUNFLG1CQUNFO0FBQ0Usd0JBQVUsQ0FBQ3VCLElBRGI7QUFFRSxxQkFBT0EsT0FBT3BELEtBQVAsR0FBa0JvRSxLQUFsQixVQUE0QnBFLEtBRnJDO0FBR0Usd0JBQVUsS0FBSzJDO0FBSGpCO0FBRkosWUFERjtBQVVHUyxrQkFBUSxPQUFPaUIsUUFBUCxLQUFvQixXQUE1QixHQUNDO0FBQUE7QUFBQSxjQUFVLE1BQUlsQixRQUFkLEVBQXdCLFNBQVMsTUFBakMsRUFBeUMsbUJBQXpDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRSw2QkFBYWxELElBRGY7QUFFRSwwQkFBVSxLQUFLYyxRQUZqQjtBQUdFLDBCQUFVLENBQUNxQyxJQUhiO0FBSUUsNkJBQWE7QUFKZjtBQURGO0FBREYsV0FERCxHQVdHLElBckJOO0FBc0JFO0FBQUE7QUFBQSxjQUFhLDBCQUFiO0FBQ0UsbURBQUssV0FBV3hDLFFBQVEyQixRQUF4QixHQURGO0FBRUcrQiwwQkFDQ04sYUFBYS9FLE1BRGQsSUFFRztBQUFBO0FBQUE7QUFDRSw4QkFBVyxNQURiO0FBRUUseUJBQVNtRSxPQUFPLEtBQUtLLE1BQVosR0FBcUIsS0FBS0Q7QUFGckM7QUFJR0oscUJBQU8sbURBQVAsR0FBc0I7QUFKekIsYUFKTjtBQVdHa0IsMEJBQ0NOLGFBQWEvRSxNQURkLElBRUc7QUFBQTtBQUFBLGdCQUFZLFNBQVMsS0FBS3lFLGFBQTFCLEVBQXlDLGNBQVcsUUFBcEQ7QUFDRTtBQURGLGFBYk47QUFpQkU7QUFBQTtBQUFBLGdCQUFTLE9BQU0sUUFBZixFQUF3QixXQUFVLFFBQWxDO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsNkJBQVcsMEJBQVc5QyxRQUFRbUIsTUFBbkIsb0NBQ1JuQixRQUFRMEIsVUFEQSxFQUNhYSxRQURiLEVBRGI7QUFJRSwyQkFBUyxLQUFLRyxpQkFKaEI7QUFLRSxtQ0FBZUgsUUFMakI7QUFNRSxnQ0FBVztBQU5iO0FBUUU7QUFSRjtBQURGO0FBakJGO0FBdEJGO0FBTEYsT0FERjtBQTZERDs7Ozs7QUFHSFYsT0FBT3ZCLFNBQVAsR0FBbUI7QUFDakJOLFdBQVMsb0JBQVVPLE1BQVYsQ0FBaUJDLFVBRFQ7O0FBR2pCd0MsWUFBVSxvQkFBVXpDLE1BQVYsQ0FBaUJDLFVBSFY7O0FBS2pCNEMsWUFBVSxvQkFBVXhDLE1BQVYsQ0FBaUJKLFVBTFY7QUFNakJsQyxZQUFVLG9CQUFVc0MsTUFBVixDQUFpQkosVUFOVjtBQU9qQmpDLFlBQVUsb0JBQVVxQyxNQUFWLENBQWlCSixVQVBWOztBQVNqQjtBQUNBZ0QsU0FBTyxvQkFBVTVDLE1BQVYsQ0FBaUJKLFVBVlA7O0FBWWpCcEIsU0FBTyxvQkFBVXNCLE1BQVYsQ0FBaUJGLFVBWlA7QUFhakI7QUFDQW5CLFFBQU0sb0JBQVVrQixNQWRDOztBQWdCakI0QyxnQkFBYyxvQkFBVXhDLElBQVYsQ0FBZUgsVUFoQlo7QUFpQmpCK0MsZ0JBQWMsb0JBQVU1QyxJQUFWLENBQWVIO0FBakJaLENBQW5COztBQW9CQSxJQUFNbUQsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdYLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVBLGtCQUFGLEVBQW5CO0FBQUEsQ0FBeEI7O0FBRUEsSUFBTVkscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VWLGtDQURGO0FBRUVJO0FBRkYsR0FERixFQUtFTSxRQUxGLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVNlLHlCQUFRRixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYiwwQkFBV3JFLE1BQVgsRUFBbUJzQyxNQUFuQixDQURhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL1FmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBR0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFVQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFRQTs7OztBQUNBOzs7O0FBRUE7O0FBa0JBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7OztBQWhDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBcURNaUMsWTs7O0FBQ0o7OztBQXlCQSwwQkFBYztBQUFBOztBQUFBOztBQUVaOztBQUVBOzs7Ozs7QUFKWTs7QUFBQSxVQTZMZEMsbUJBN0xjO0FBQUEsMEZBNkxRLGlCQUFPL0IsQ0FBUDtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ3BCO0FBQ01nQyxvQkFGYyxHQUVQaEMsRUFBRUUsTUFBRixDQUFTK0IsS0FBVCxDQUFlLENBQWYsQ0FGTzs7QUFBQSxzQkFJaEJELEtBQUs5RixJQUFMLENBQVVnRyxPQUFWLENBQWtCLFFBQWxCLE1BQWdDLENBSmhCO0FBQUE7QUFBQTtBQUFBOztBQUtaQyx3QkFMWSxHQUtELElBQUlDLFFBQUosRUFMQzs7QUFNbEJELHlCQUFTRSxNQUFULENBQWdCLE9BQWhCLEVBQXlCTCxJQUF6Qjs7QUFOa0I7QUFBQSx1QkFRMkIsTUFBS2xDLEtBQUwsQ0FBV3dDLGNBQVgsQ0FBMEI7QUFDckVIO0FBRHFFLGlCQUExQixDQVIzQjs7QUFBQTtBQUFBO0FBUVZuRiwwQkFSVSxTQVFWQSxVQVJVO0FBUUVDLHFCQVJGLFNBUUVBLEtBUkY7QUFRU2hCLHVCQVJULFNBUVNBLE9BUlQ7O0FBQUEsc0JBWWRlLGNBQWMsR0FaQTtBQUFBO0FBQUE7QUFBQTs7QUFhaEI7QUFDQUcsd0JBQVFGLEtBQVIsQ0FBY0QsVUFBZCxFQUEwQkMsS0FBMUI7QUFkZ0I7O0FBQUE7QUFrQkFzRixtQkFsQkEsR0FrQlF0RyxPQWxCUixDQWtCVnVHLFFBbEJVO0FBb0JWQywyQkFwQlUsR0FvQk0sTUFBSzNDLEtBcEJYLENBb0JWMkMsV0FwQlU7QUFzQlpDLDRCQXRCWSxHQXNCR0QsWUFBWXZCLGlCQUFaLEVBdEJIO0FBdUJaeUIsc0NBdkJZLEdBdUJhRCxhQUFhRSxZQUFiLENBQzdCLGtCQUFPQyxLQURzQixFQUU3QixXQUY2QixFQUc3QixFQUFFTixRQUFGLEVBSDZCLENBdkJiO0FBNkJaTyx5QkE3QlksR0E2QkFILHVCQUF1QkksdUJBQXZCLEVBN0JBO0FBK0JaQyxpQ0EvQlksR0ErQlEscUJBQVlDLEdBQVosQ0FBZ0JSLFdBQWhCLEVBQTZCO0FBQ3JEUyxrQ0FBZ0JQO0FBRHFDLGlCQUE3QixDQS9CUjtBQW1DWlEsOEJBbkNZLEdBbUNLLDBCQUFpQkMsaUJBQWpCLENBQ3JCSixpQkFEcUIsRUFFckJGLFNBRnFCLEVBR3JCLEdBSHFCLENBbkNMOzs7QUF5Q2xCLHNCQUFLM0UsUUFBTCxDQUFjZ0YsY0FBZDs7QUF6Q2tCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BN0xSOztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQVVaLFVBQUtoRixRQUFMLEdBQWdCLFVBQUNzRSxXQUFELEVBQWlCO0FBQy9CLFlBQUszQyxLQUFMLENBQVczQixRQUFYLENBQW9Cc0UsV0FBcEI7QUFDRCxLQUZEOztBQUlBOzs7O0FBSUEsVUFBS1ksS0FBTCxHQUFhO0FBQUEsYUFBTSxNQUFLQyxVQUFMLENBQWdCRCxLQUFoQixFQUFOO0FBQUEsS0FBYjs7QUFFQSxVQUFLRSxLQUFMLEdBQWEsTUFBS0EsS0FBTCxDQUFXNUMsSUFBWCxPQUFiO0FBQ0EsVUFBSzZDLFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQjdDLElBQWxCLE9BQXBCO0FBQ0EsVUFBSzhDLFdBQUwsR0FBbUIsTUFBS0EsV0FBTCxDQUFpQjlDLElBQWpCLE9BQW5CO0FBQ0EsVUFBSytDLFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQi9DLElBQWxCLE9BQXBCO0FBQ0EsVUFBS2dELG1CQUFMLEdBQTJCLE1BQUtBLG1CQUFMLENBQXlCaEQsSUFBekIsT0FBM0I7QUFDQSxVQUFLaUQsbUJBQUwsR0FBMkIsTUFBS0EsbUJBQUwsQ0FBeUJqRCxJQUF6QixPQUEzQjtBQUNBLFVBQUtrRCxXQUFMLEdBQW1CLE1BQUtBLFdBQUwsQ0FBaUJsRCxJQUFqQixPQUFuQjtBQUNBLFVBQUttRCxhQUFMLEdBQXFCLE1BQUtBLGFBQUwsQ0FBbUJuRCxJQUFuQixPQUFyQjtBQUNBLFVBQUtvRCxnQkFBTCxHQUF3QixNQUFLQSxnQkFBTCxDQUFzQnBELElBQXRCLE9BQXhCO0FBQ0EsVUFBS3FELGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCckQsSUFBdEIsT0FBeEI7QUFDQSxVQUFLc0QsZ0JBQUwsR0FBd0IsTUFBS0EsZ0JBQUwsQ0FBc0J0RCxJQUF0QixPQUF4QjtBQUNBLFVBQUt1RCxrQkFBTCxHQUEwQixNQUFLQSxrQkFBTCxDQUF3QnZELElBQXhCLE9BQTFCOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBNUNZO0FBNkNiOztBQUVEOzs7Ozs7Ozs7MEJBS01YLEMsRUFBRztBQUNQQSxRQUFFbUUsY0FBRjtBQURPLFVBRUMxQixXQUZELEdBRWlCLEtBQUszQyxLQUZ0QixDQUVDMkMsV0FGRDs7O0FBSVAsVUFBTVUsaUJBQWlCLG1CQUFVSSxLQUFWLENBQWdCdkQsQ0FBaEIsRUFBbUJ5QyxXQUFuQixFQUFnQyxDQUFoQyxDQUF2Qjs7QUFFQSxXQUFLdEUsUUFBTCxDQUFjZ0YsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O21DQUllO0FBQUEsVUFDTFYsV0FESyxHQUNXLEtBQUszQyxLQURoQixDQUNMMkMsV0FESzs7O0FBR2IsVUFBTVUsaUJBQWlCLG1CQUFVaUIsZUFBVixDQUEwQjNCLFdBQTFCLEVBQXVDLFlBQXZDLENBQXZCOztBQUVBLFdBQUt0RSxRQUFMLENBQWNnRixjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBSzNDLEtBRGpCLENBQ0oyQyxXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQTBCM0IsV0FBMUIsRUFBdUMsWUFBdkMsQ0FBdkI7O0FBRUEsV0FBS3RFLFFBQUwsQ0FBY2dGLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzttQ0FJZTtBQUFBLFVBQ0xWLFdBREssR0FDVyxLQUFLM0MsS0FEaEIsQ0FDTDJDLFdBREs7OztBQUdiLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FBMEIzQixXQUExQixFQUF1QyxZQUF2QyxDQUF2Qjs7QUFFQSxXQUFLdEUsUUFBTCxDQUFjZ0YsY0FBZDtBQUNEOztBQUVEOzs7Ozs7OzBDQUlzQjtBQUFBLFVBQ1pWLFdBRFksR0FDSSxLQUFLM0MsS0FEVCxDQUNaMkMsV0FEWTs7O0FBR3BCLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FDckIzQixXQURxQixFQUVyQixxQkFGcUIsQ0FBdkI7O0FBS0EsV0FBS3RFLFFBQUwsQ0FBY2dGLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzswQ0FJc0I7QUFBQSxVQUNaVixXQURZLEdBQ0ksS0FBSzNDLEtBRFQsQ0FDWjJDLFdBRFk7OztBQUdwQixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQ3JCM0IsV0FEcUIsRUFFckIsbUJBRnFCLENBQXZCOztBQUtBLFdBQUt0RSxRQUFMLENBQWNnRixjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBSzNDLEtBRGpCLENBQ0oyQyxXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLE1BQXpDLENBQXZCOztBQUVBLFdBQUt0RSxRQUFMLENBQWNnRixjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7b0NBSWdCO0FBQUEsVUFDTlYsV0FETSxHQUNVLEtBQUszQyxLQURmLENBQ04yQyxXQURNOzs7QUFHZCxVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLFFBQXpDLENBQXZCOztBQUVBLFdBQUt0RSxRQUFMLENBQWNnRixjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7dUNBSW1CO0FBQUEsVUFDVFYsV0FEUyxHQUNPLEtBQUszQyxLQURaLENBQ1QyQyxXQURTOzs7QUFHakIsVUFBTVUsaUJBQWlCLG1CQUFVa0IsaUJBQVYsQ0FDckI1QixXQURxQixFQUVyQixXQUZxQixDQUF2Qjs7QUFLQSxXQUFLdEUsUUFBTCxDQUFjZ0YsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O3VDQUltQjtBQUFBLFVBQ1RWLFdBRFMsR0FDTyxLQUFLM0MsS0FEWixDQUNUMkMsV0FEUzs7O0FBR2pCLFVBQU1VLGlCQUFpQixtQkFBVWtCLGlCQUFWLENBQTRCNUIsV0FBNUIsRUFBeUMsTUFBekMsQ0FBdkI7O0FBRUEsV0FBS3RFLFFBQUwsQ0FBY2dGLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7O3lDQUdxQjtBQUNuQixXQUFLbUIsS0FBTCxDQUFXOUcsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUs4RyxLQUFMLENBQVdDLEtBQVg7QUFDRDs7QUFFRDs7Ozs7Ozs7QUFnREE7Ozs7OztxQ0FNaUJDLE8sRUFBUztBQUFBLFVBQ2hCL0IsV0FEZ0IsR0FDQSxLQUFLM0MsS0FETCxDQUNoQjJDLFdBRGdCOzs7QUFHeEIsVUFBTVUsaUJBQWlCLG1CQUFVYyxnQkFBVixDQUEyQnhCLFdBQTNCLEVBQXdDK0IsT0FBeEMsQ0FBdkI7O0FBRUEsVUFBSXJCLGNBQUosRUFBb0I7QUFDbEIsYUFBS2hGLFFBQUwsQ0FBY2dGLGNBQWQ7QUFDQTtBQUNEOztBQUVEO0FBQ0Q7O0FBRUQ7Ozs7b0NBQ2dCc0IsSyxFQUFPO0FBQ3JCLGNBQVFBLE1BQU1DLE9BQU4sRUFBUjtBQUNFLGFBQUssa0JBQU9DLE1BQVo7QUFDRSxpQkFBTztBQUNMQyx1Q0FESztBQUVMQyxzQkFBVTtBQUZMLFdBQVA7O0FBS0Y7QUFDRSxpQkFBTyxJQUFQO0FBUko7QUFVRDtBQUNEOztBQUVBOzs7O2lDQUNhSixLLEVBQU87QUFDbEIsY0FBUUEsTUFBTUMsT0FBTixFQUFSO0FBQ0UsYUFBSyxZQUFMO0FBQ0UsaUJBQU8sdUJBQVA7O0FBRUY7QUFDRSxpQkFBTyxJQUFQO0FBTEo7QUFPRDtBQUNEOzs7OzZCQUVTO0FBQUE7O0FBQUEsbUJBT0gsS0FBSzVFLEtBUEY7QUFBQSxVQUVMOUIsT0FGSyxVQUVMQSxPQUZLO0FBQUEsVUFHTEMsUUFISyxVQUdMQSxRQUhLO0FBQUEsVUFJTEUsUUFKSyxVQUlMQSxRQUpLO0FBQUEsVUFLTHNFLFdBTEssVUFLTEEsV0FMSztBQUFBLFVBTUx2RSxXQU5LLFVBTUxBLFdBTks7O0FBU1A7O0FBQ0EsVUFBTTRHLFdBQVc7QUFDZkMsY0FBTTtBQUNKQywyQkFBaUIscUJBRGI7QUFFSkMsc0JBQVksK0NBRlI7QUFHSkMsb0JBQVUsRUFITjtBQUlKQyxtQkFBUztBQUpMO0FBRFMsT0FBakI7O0FBU0EsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFXbkgsUUFBUWUsSUFBeEI7QUFDRyxTQUFDZCxRQUFELEdBQ0M7QUFBQTtBQUFBLFlBQUssV0FBVywwQkFBVyxFQUFFLG1CQUFtQixFQUFyQixFQUFYLENBQWhCO0FBQ0U7QUFBQTtBQUFBLGNBQVMsT0FBTSxPQUFmLEVBQXVCLElBQUcsT0FBMUIsRUFBa0MsV0FBVSxRQUE1QztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE9BQXZCLEVBQStCLFNBQVMsS0FBS3VGLFlBQTdDO0FBQ0U7QUFERjtBQURGLFdBREY7QUFNRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE1BQWYsRUFBc0IsSUFBRyxNQUF6QixFQUFnQyxXQUFVLFFBQTFDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsTUFBdkIsRUFBOEIsU0FBUyxLQUFLSyxXQUE1QztBQUNFO0FBREY7QUFERixXQU5GO0FBV0U7QUFBQTtBQUFBLGNBQVMsT0FBTSxRQUFmLEVBQXdCLElBQUcsUUFBM0IsRUFBb0MsV0FBVSxRQUE5QztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLFFBQXZCLEVBQWdDLFNBQVMsS0FBS0MsYUFBOUM7QUFDRTtBQURGO0FBREYsV0FYRjtBQWdCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLFdBQWYsRUFBMkIsSUFBRyxXQUE5QixFQUEwQyxXQUFVLFFBQXBEO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsV0FEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQURGLFdBaEJGO0FBd0JFO0FBQUE7QUFBQSxjQUFTLE9BQU0sTUFBZixFQUFzQixJQUFHLE1BQXpCLEVBQWdDLFdBQVUsUUFBMUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxNQUF2QixFQUE4QixTQUFTLEtBQUtOLFdBQTVDO0FBQ0U7QUFERjtBQURGLFdBeEJGO0FBNkJFO0FBQUE7QUFBQSxjQUFTLE9BQU0sT0FBZixFQUF1QixJQUFHLE9BQTFCLEVBQWtDLFdBQVUsUUFBNUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QixFQUErQixTQUFTLEtBQUtDLFlBQTdDO0FBQ0U7QUFERjtBQURGLFdBN0JGO0FBa0NFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGdCQURSO0FBRUUsa0JBQUcsZUFGTDtBQUdFLHlCQUFVO0FBSFo7QUFLRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxnQkFEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQUxGLFdBbENGO0FBOENFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxjQURiO0FBRUUseUJBQVMsS0FBS0M7QUFGaEI7QUFJRTtBQUpGO0FBREYsV0E5Q0Y7QUFzREU7QUFBQTtBQUFBLGNBQVMsT0FBTSxZQUFmLEVBQTRCLElBQUcsWUFBL0IsRUFBNEMsV0FBVSxRQUF0RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLFlBQXZCLEVBQW9DLGNBQXBDO0FBQ0U7QUFERjtBQURGLFdBdERGO0FBMkRFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxjQUF2QixFQUFzQyxjQUF0QztBQUNFO0FBREY7QUFERixXQTNERjtBQWdFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0FoRUY7QUFxRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBckVGO0FBMEVFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQTFFRjtBQStFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0EvRUY7QUFvRkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxjQUFmLEVBQThCLElBQUcsY0FBakMsRUFBZ0QsV0FBVSxRQUExRDtBQUNFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGNBRGI7QUFFRSwwQkFBVSxPQUFPLEtBQUs5RCxLQUFMLENBQVd3QyxjQUFsQixLQUFxQyxVQUZqRDtBQUdFLHlCQUFTLEtBQUs0QjtBQUhoQjtBQUtFLHdFQUxGO0FBTUU7QUFDRSxzQkFBSyxNQURQO0FBRUUsd0JBQU8sb0JBRlQ7QUFHRSwwQkFBVSxLQUFLbkMsbUJBSGpCO0FBSUUscUJBQUssYUFBQ3VDLEtBQUQsRUFBVztBQUNkLHlCQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDRCxpQkFOSDtBQU9FLHVCQUFPLEVBQUVjLFNBQVMsTUFBWDtBQVBUO0FBTkY7QUFERixXQXBGRjtBQXNHRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxpQkFEUjtBQUVFLGtCQUFHLGtCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGlCQUF2QixFQUF5QyxjQUF6QztBQUNFO0FBREY7QUFMRixXQXRHRjtBQStHRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGdCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGdCQUF2QixFQUF3QyxjQUF4QztBQUNFO0FBREY7QUFMRixXQS9HRjtBQXdIRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGdCQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLGdCQURiO0FBRUUseUJBQVMsS0FBS3BCO0FBRmhCO0FBSUU7QUFKRjtBQUxGLFdBeEhGO0FBb0lFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGVBRFI7QUFFRSxrQkFBRyxlQUZMO0FBR0UseUJBQVU7QUFIWjtBQUtFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGVBQXZCLEVBQXVDLGNBQXZDO0FBQ0U7QUFERjtBQUxGO0FBcElGLFNBREQsR0ErSUcsSUFoSk47QUFpSkU7QUFBQTtBQUFBLFlBQUssV0FBVywwQkFBVyxFQUFFLGVBQWUsRUFBakIsRUFBWCxDQUFoQjtBQUNFO0FBQ0U7O0FBREYsY0FHRSxhQUFhdkIsV0FIZjtBQUlFLHNCQUFVdEU7QUFDVjs7QUFMRixjQU9FLGFBQWFEO0FBQ2I7O0FBRUE7O0FBVkYsY0FZRSxpQkFBaUIsS0FBS21ILGVBWnhCO0FBYUUsMEJBQWMsS0FBS0MsWUFickI7QUFjRSw0QkFBZ0JSO0FBQ2hCOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQXZCRixjQXlCRSxVQUFVN0csUUF6Qlo7QUEwQkU7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFuQ0YsY0FxQ0Usa0JBQWtCLEtBQUtnRztBQUN2Qjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFsREYsY0FvREUsT0FBTyxLQUFLVjtBQUNaOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQTNFRixjQTZFRSxLQUFLLGFBQUNnQyxJQUFELEVBQVU7QUFDYixxQkFBS2pDLFVBQUwsR0FBa0JpQyxJQUFsQjtBQUNEO0FBL0VIO0FBREY7QUFqSkYsT0FERjtBQXVPRDs7Ozs7QUE1akJIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBM0RBO0FBUkE7O0FBNkVNekQsWSxDQUlHeEQsUyxHQUFZO0FBQ2pCTixXQUFTLHlCQUFVTyxNQUFWLENBQWlCQyxVQURUOztBQUdqQmlFLGVBQWEseUJBQVVsRSxNQUFWLENBQWlCQyxVQUhiO0FBSWpCTCxZQUFVLHlCQUFVUSxJQUFWLENBQWVILFVBSlI7O0FBTWpCTixlQUFhLHlCQUFVUSxNQU5OOztBQVFqQlQsWUFBVSx5QkFBVVEsSUFBVixDQUFlRCxVQVJSOztBQVVqQjs7OztBQUlBOEQsa0JBQWdCLHlCQUFVM0Q7QUFkVCxDO0FBSmZtRCxZLENBcUJHakQsWSxHQUFlO0FBQ3BCWixZQUFVLElBRFU7QUFFcEJDLGVBQWE7QUFGTyxDO2tCQTBoQlQsa0RBQW1CNEQsWUFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxbkJmOztBQUVBOztBQUpBOztBQVdBLElBQU0wRCxvQkFBb0IsZ0NBQXVCLENBQy9DO0FBQ0VDLHNDQURGO0FBRUViO0FBRkYsQ0FEK0MsRUFLL0M7QUFDRWEsdUNBREY7QUFFRWI7QUFGRixDQUwrQyxDQUF2QixDQUExQjs7QUFXTyxJQUFNYyxnREFBb0IsU0FBcEJBLGlCQUFvQixHQUc1QjtBQUFBLE1BRkhDLE9BRUcsdUVBRk8sSUFFUDtBQUFBLE1BREhDLFVBQ0csdUVBRFVKLGlCQUNWOztBQUNILE1BQUlHLFlBQVksSUFBaEIsRUFBc0I7QUFDcEIsV0FBTyxxQkFBWUUsV0FBWixDQUF3QkQsVUFBeEIsQ0FBUDtBQUNEO0FBQ0QsU0FBTyxxQkFBWUUsaUJBQVosQ0FBOEIsNkJBQWVILE9BQWYsQ0FBOUIsRUFBdURDLFVBQXZELENBQVA7QUFDRCxDQVJNOztrQkFVUUYsaUI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hDZjs7Ozs7O2tCQU1lO0FBQ2IsYUFBVztBQUNULHdCQUFvQjtBQUNsQkssa0JBQVksTUFETTtBQUVsQm5JLGNBQVEsZ0JBRlU7QUFHbEJxSCxrQkFBWSxrQkFITTtBQUlsQkMsZ0JBQVUsTUFKUTtBQUtsQkMsZUFBUztBQUxTLEtBRFg7QUFRVCwwQkFBc0I7QUFDcEJhLGlCQUFXLGdCQURTO0FBRXBCQyxjQUFRLE1BRlk7QUFHcEJmLGdCQUFVLE1BSFU7QUFJcEJnQixpQkFBVztBQUpTLEtBUmI7QUFjVCwyQ0FBdUM7QUFDckNDLGNBQVEsZUFENkI7QUFFckNoQixlQUFTO0FBRjRCLEtBZDlCO0FBa0JULG1DQUErQjtBQUM3QmdCLGNBQVEsZUFEcUI7QUFFN0JoQixlQUFTO0FBQ1Q7QUFINkIsS0FsQnRCO0FBdUJULDhCQUEwQjtBQUN4QkgsdUJBQWlCLGdCQURPO0FBRXhCb0Isa0JBQVksZ0JBRlk7QUFHeEJDLGFBQU8sTUFIaUI7QUFJeEJwQixrQkFBWSxrQ0FKWTtBQUt4QnFCLGlCQUFXLFFBTGE7QUFNeEJILGNBQVEsUUFOZ0I7QUFPeEJoQixlQUFTO0FBUGUsS0F2QmpCO0FBZ0NULHFDQUFpQztBQUMvQkgsdUJBQWlCLHFCQURjO0FBRS9CQyxrQkFBWSwrQ0FGbUI7QUFHL0JDLGdCQUFVLE1BSHFCO0FBSS9CQyxlQUFTO0FBSnNCO0FBaEN4QixHQURFO0FBd0NicEcsUUFBTTtBQUNKO0FBREksR0F4Q087QUEyQ2JZLFlBQVU7QUFDUkMsVUFBTTtBQURFO0FBM0NHLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0RmOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBLElBQU1yQyxTQUFTO0FBQ2IrRyxTQUFPO0FBQ0w1RyxXQUFPLE1BREY7QUFFTDtBQUNBO0FBQ0E2SSxnQkFBWTtBQUpQO0FBRE0sQ0FBZjs7QUFTQTs7Ozs7Ozs7QUFuQkE7Ozs7O0lBMkJNQyxNOzs7QUFNSixrQkFBWTFHLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxzSUFDWEEsS0FEVzs7QUFFakIsVUFBS1EsS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQSxtQkFDeUIsS0FBS1IsS0FEOUI7QUFBQSxVQUNDNEMsWUFERCxVQUNDQSxZQUREO0FBQUEsVUFDZStCLEtBRGYsVUFDZUEsS0FEZjs7O0FBR1AsVUFBTWdDLFNBQVMvRCxhQUFhZ0UsU0FBYixDQUF1QmpDLE1BQU1rQyxXQUFOLENBQWtCLENBQWxCLENBQXZCLENBQWY7O0FBSE8sNEJBSVNGLE9BQU9HLE9BQVAsRUFKVDtBQUFBLFVBSUNyRSxHQUpELG1CQUlDQSxHQUpEOztBQUtQLFVBQU1yRyxPQUFPdUssT0FBTy9CLE9BQVAsRUFBYjs7QUFFQSxVQUFJeEksU0FBUyxrQkFBTzJHLEtBQXBCLEVBQTJCO0FBQ3pCLGVBQ0U7QUFBQTtBQUFBO0FBQ0UsaURBQUssS0FBSyxLQUFWLEVBQWlCLEtBQUtOLEdBQXRCLEVBQTJCLE9BQU9oRixPQUFPK0csS0FBekM7QUFERixTQURGO0FBS0Q7O0FBRUQsYUFBTywwQ0FBUDtBQUNEOzs7OztBQTNCR2tDLE0sQ0FDR2xJLFMsR0FBWTtBQUNqQm9FLGdCQUFjLG9CQUFVbkUsTUFBVixDQUFpQkMsVUFEZDtBQUVqQmlHLFNBQU8sb0JBQVVvQyxHQUFWLENBQWNySTtBQUZKLEM7a0JBNkJOZ0ksTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztRQ3ZDQ00sYyxHQUFBQSxjO1FBSUFDLGUsR0FBQUEsZTs7QUFwQmhCOzs7O0FBQ0E7Ozs7OztBQUhBO0FBQ0E7QUFJQSxJQUFNQyxlQUFlLFVBQXJCO0FBQ0EsSUFBTUMsZ0JBQWdCLHVCQUF0Qjs7QUFFQSxTQUFTQyxhQUFULENBQXVCQyxLQUF2QixFQUE4QkMsWUFBOUIsRUFBNENDLFFBQTVDLEVBQXNEO0FBQ3BELE1BQU1DLE9BQU9GLGFBQWFHLE9BQWIsRUFBYjtBQUNBLE1BQUlDLGlCQUFKO0FBQUEsTUFDRUMsY0FERjtBQUVBLFNBQU8sQ0FBQ0QsV0FBV0wsTUFBTU8sSUFBTixDQUFXSixJQUFYLENBQVosTUFBa0MsSUFBekMsRUFBK0M7QUFDN0NHLFlBQVFELFNBQVNoRyxLQUFqQjtBQUNBNkYsYUFBU0ksS0FBVCxFQUFnQkEsUUFBUUQsU0FBUyxDQUFULEVBQVlHLE1BQXBDO0FBQ0Q7QUFDRjs7QUFFTSxTQUFTYixjQUFULENBQXdCTSxZQUF4QixFQUFzQ0MsUUFBdEMsRUFBZ0QzRSxZQUFoRCxFQUE4RDtBQUNuRXdFLGdCQUFjRixZQUFkLEVBQTRCSSxZQUE1QixFQUEwQ0MsUUFBMUM7QUFDRDs7QUFFTSxTQUFTTixlQUFULENBQXlCSyxZQUF6QixFQUF1Q0MsUUFBdkMsRUFBaUQzRSxZQUFqRCxFQUErRDtBQUNwRXdFLGdCQUFjRCxhQUFkLEVBQTZCRyxZQUE3QixFQUEyQ0MsUUFBM0M7QUFDRDs7QUFFTSxJQUFNTyxrQ0FBYSxTQUFiQSxVQUFhO0FBQUEsU0FBUztBQUFBO0FBQUEsTUFBTSxPQUFPLGdCQUFNQyxNQUFuQjtBQUE0Qi9ILFVBQU1nSTtBQUFsQyxHQUFUO0FBQUEsQ0FBbkI7O0FBRUEsSUFBTUMsb0NBQWMsU0FBZEEsV0FBYztBQUFBLFNBQVM7QUFBQTtBQUFBLE1BQU0sT0FBTyxnQkFBTUMsT0FBbkI7QUFBNkJsSSxVQUFNZ0k7QUFBbkMsR0FBVDtBQUFBLENBQXBCOztrQkFHUTtBQUNiaEIsZ0NBRGE7QUFFYmMsd0JBRmE7O0FBSWJiLGtDQUphO0FBS2JnQjtBQUxhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CZjs7Ozs7OztBQU9BOzs7QUFHTyxJQUFNRSwwQkFBUztBQUNwQkMsWUFBVSxVQURVO0FBRXBCQyxhQUFXLFVBRlM7O0FBSXBCQyxNQUFJLFlBSmdCO0FBS3BCQyxNQUFJLFlBTGdCO0FBTXBCQyxNQUFJLGNBTmdCO0FBT3BCQyxNQUFJLGFBUGdCO0FBUXBCQyxNQUFJLGFBUmdCO0FBU3BCQyxNQUFJLFlBVGdCOztBQVdwQkMsTUFBSSxtQkFYZ0I7QUFZcEJDLE1BQUkscUJBWmdCOztBQWNwQjVELFFBQU0sWUFkYzs7QUFnQnBCNkQsY0FBWSxZQWhCUTs7QUFrQnBCakUsVUFBUSxRQWxCWTtBQW1CcEI5QixTQUFPLGNBbkJhO0FBb0JwQmdHLFNBQU87QUFwQmEsQ0FBZjs7QUF1QlA7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNLE1BRGM7QUFFcEJoRSxRQUFNLE1BRmM7QUFHcEJpRSxVQUFRLFFBSFk7QUFJcEJDLGlCQUFlLGVBSks7QUFLcEJDLGFBQVcsV0FMUztBQU1wQkMsYUFBVztBQU5TLENBQWY7O0FBU1A7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNO0FBRGMsQ0FBZjs7QUFJUDs7O0FBR08sSUFBTUMsZ0NBQVksV0FBbEI7O0FBRVA7OztBQUdPLElBQU1DLDRCQUFVLFNBQWhCO0FBQ0EsSUFBTUMsb0NBQWMsYUFBcEI7O2tCQUVRO0FBQ2J2QixnQkFEYTtBQUViYSxnQkFGYTtBQUdiTTtBQUhhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0RmOzs7OzttQkFFU3RILFk7Ozs7Ozs7Ozt3QkFFQTRELGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05UOztBQUVPLElBQU0rRCx3QkFBUTtBQUNuQjFLLFFBQU07QUFDSm9HLGFBQVMsRUFETDtBQUVKekgsV0FBTztBQUZILEdBRGE7QUFLbkJnTSxVQUFRO0FBQ045TCxZQUFRLGdCQURGO0FBRU5xSSxZQUFRLE1BRkY7QUFHTmYsY0FBVSxFQUhKO0FBSU55RSxlQUFXLEVBSkw7QUFLTnhFLGFBQVM7QUFMSCxHQUxXO0FBWW5CeUUsVUFBUTtBQUNOMUQsZUFBVyxFQURMO0FBRU4yRCxlQUFXO0FBRkwsR0FaVztBQWdCbkJoQyxVQUFRO0FBQ054QixXQUFPLHlCQUREO0FBRU55RCxlQUFXLEtBRkw7QUFHTkMsaUJBQWE7QUFIUCxHQWhCVztBQXFCbkIvQixXQUFTO0FBQ1AzQixXQUFPO0FBREE7QUFyQlUsQ0FBZDs7a0JBMEJRb0QsSyIsImZpbGUiOiIzOC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNi41IDZ2MTEuNWMwIDIuMjEtMS43OSA0LTQgNHMtNC0xLjc5LTQtNFY1YzAtMS4zOCAxLjEyLTIuNSAyLjUtMi41czIuNSAxLjEyIDIuNSAyLjV2MTAuNWMwIC41NS0uNDUgMS0xIDFzLTEtLjQ1LTEtMVY2SDEwdjkuNWMwIDEuMzggMS4xMiAyLjUgMi41IDIuNXMyLjUtMS4xMiAyLjUtMi41VjVjMC0yLjIxLTEuNzktNC00LTRTNyAyLjc5IDcgNXYxMi41YzAgMy4wNCAyLjQ2IDUuNSA1LjUgNS41czUuNS0yLjQ2IDUuNS01LjVWNmgtMS41eicgfSk7XG5cbnZhciBBdHRhY2hGaWxlID0gZnVuY3Rpb24gQXR0YWNoRmlsZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQXR0YWNoRmlsZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQXR0YWNoRmlsZSk7XG5BdHRhY2hGaWxlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEF0dGFjaEZpbGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ005LjQgMTYuNkw0LjggMTJsNC42LTQuNkw4IDZsLTYgNiA2IDYgMS40LTEuNHptNS4yIDBsNC42LTQuNi00LjYtNC42TDE2IDZsNiA2LTYgNi0xLjQtMS40eicgfSk7XG5cbnZhciBDb2RlID0gZnVuY3Rpb24gQ29kZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuQ29kZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoQ29kZSk7XG5Db2RlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IENvZGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ002IDE5YzAgMS4xLjkgMiAyIDJoOGMxLjEgMCAyLS45IDItMlY3SDZ2MTJ6TTE5IDRoLTMuNWwtMS0xaC01bC0xIDFINXYyaDE0VjR6JyB9KTtcblxudmFyIERlbGV0ZSA9IGZ1bmN0aW9uIERlbGV0ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRGVsZXRlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShEZWxldGUpO1xuRGVsZXRlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IERlbGV0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9EZWxldGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0RlbGV0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI2IDI3IDI4IDMyIDM1IDM3IDM4IDQwIDQxIDQyIDQ2IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAxNy4yNVYyMWgzLjc1TDE3LjgxIDkuOTRsLTMuNzUtMy43NUwzIDE3LjI1ek0yMC43MSA3LjA0Yy4zOS0uMzkuMzktMS4wMiAwLTEuNDFsLTIuMzQtMi4zNGMtLjM5LS4zOS0xLjAyLS4zOS0xLjQxIDBsLTEuODMgMS44MyAzLjc1IDMuNzUgMS44My0xLjgzeicgfSk7XG5cbnZhciBFZGl0ID0gZnVuY3Rpb24gRWRpdChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRWRpdCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRWRpdCk7XG5FZGl0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEVkaXQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI2IDMxIDM0IDM4IDQ0IDQ1IDQ3IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTYuNTkgOC41OUwxMiAxMy4xNyA3LjQxIDguNTkgNiAxMGw2IDYgNi02eicgfSk7XG5cbnZhciBFeHBhbmRNb3JlID0gZnVuY3Rpb24gRXhwYW5kTW9yZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRXhwYW5kTW9yZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRXhwYW5kTW9yZSk7XG5FeHBhbmRNb3JlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEV4cGFuZE1vcmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRXhwYW5kTW9yZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRXhwYW5kTW9yZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIDM4IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNyAxNXYyaDEwdi0ySDd6bS00IDZoMTh2LTJIM3Yyem0wLThoMTh2LTJIM3Yyem00LTZ2MmgxMFY3SDd6TTMgM3YyaDE4VjNIM3onIH0pO1xuXG52YXIgRm9ybWF0QWxpZ25DZW50ZXIgPSBmdW5jdGlvbiBGb3JtYXRBbGlnbkNlbnRlcihwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25DZW50ZXIgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEFsaWduQ2VudGVyKTtcbkZvcm1hdEFsaWduQ2VudGVyLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduQ2VudGVyO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zIDIxaDE4di0ySDN2MnptMC00aDE4di0ySDN2MnptMC00aDE4di0ySDN2MnptMC00aDE4VjdIM3Yyem0wLTZ2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduSnVzdGlmeSA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduSnVzdGlmeShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25KdXN0aWZ5ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkp1c3RpZnkpO1xuRm9ybWF0QWxpZ25KdXN0aWZ5Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduSnVzdGlmeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNSAxNUgzdjJoMTJ2LTJ6bTAtOEgzdjJoMTJWN3pNMyAxM2gxOHYtMkgzdjJ6bTAgOGgxOHYtMkgzdjJ6TTMgM3YyaDE4VjNIM3onIH0pO1xuXG52YXIgRm9ybWF0QWxpZ25MZWZ0ID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25MZWZ0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRBbGlnbkxlZnQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEFsaWduTGVmdCk7XG5Gb3JtYXRBbGlnbkxlZnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0QWxpZ25MZWZ0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMgMjFoMTh2LTJIM3Yyem02LTRoMTJ2LTJIOXYyem0tNi00aDE4di0ySDN2MnptNi00aDEyVjdIOXYyek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduUmlnaHQgPSBmdW5jdGlvbiBGb3JtYXRBbGlnblJpZ2h0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRBbGlnblJpZ2h0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnblJpZ2h0KTtcbkZvcm1hdEFsaWduUmlnaHQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0QWxpZ25SaWdodDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnblJpZ2h0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnblJpZ2h0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE1LjYgMTAuNzljLjk3LS42NyAxLjY1LTEuNzcgMS42NS0yLjc5IDAtMi4yNi0xLjc1LTQtNC00SDd2MTRoNy4wNGMyLjA5IDAgMy43MS0xLjcgMy43MS0zLjc5IDAtMS41Mi0uODYtMi44Mi0yLjE1LTMuNDJ6TTEwIDYuNWgzYy44MyAwIDEuNS42NyAxLjUgMS41cy0uNjcgMS41LTEuNSAxLjVoLTN2LTN6bTMuNSA5SDEwdi0zaDMuNWMuODMgMCAxLjUuNjcgMS41IDEuNXMtLjY3IDEuNS0xLjUgMS41eicgfSk7XG5cbnZhciBGb3JtYXRCb2xkID0gZnVuY3Rpb24gRm9ybWF0Qm9sZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0Qm9sZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0Qm9sZCk7XG5Gb3JtYXRCb2xkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEJvbGQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Qm9sZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Qm9sZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xMCA0djNoMi4yMWwtMy40MiA4SDZ2M2g4di0zaC0yLjIxbDMuNDItOEgxOFY0eicgfSk7XG5cbnZhciBGb3JtYXRJdGFsaWMgPSBmdW5jdGlvbiBGb3JtYXRJdGFsaWMocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEl0YWxpYyA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0SXRhbGljKTtcbkZvcm1hdEl0YWxpYy5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRJdGFsaWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SXRhbGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNCAxMC41Yy0uODMgMC0xLjUuNjctMS41IDEuNXMuNjcgMS41IDEuNSAxLjUgMS41LS42NyAxLjUtMS41LS42Ny0xLjUtMS41LTEuNXptMC02Yy0uODMgMC0xLjUuNjctMS41IDEuNVMzLjE3IDcuNSA0IDcuNSA1LjUgNi44MyA1LjUgNiA0LjgzIDQuNSA0IDQuNXptMCAxMmMtLjgzIDAtMS41LjY4LTEuNSAxLjVzLjY4IDEuNSAxLjUgMS41IDEuNS0uNjggMS41LTEuNS0uNjctMS41LTEuNS0xLjV6TTcgMTloMTR2LTJIN3Yyem0wLTZoMTR2LTJIN3Yyem0wLTh2MmgxNFY1SDd6JyB9KTtcblxudmFyIEZvcm1hdExpc3RCdWxsZXRlZCA9IGZ1bmN0aW9uIEZvcm1hdExpc3RCdWxsZXRlZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0TGlzdEJ1bGxldGVkID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRMaXN0QnVsbGV0ZWQpO1xuRm9ybWF0TGlzdEJ1bGxldGVkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdExpc3RCdWxsZXRlZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yIDE3aDJ2LjVIM3YxaDF2LjVIMnYxaDN2LTRIMnYxem0xLTloMVY0SDJ2MWgxdjN6bS0xIDNoMS44TDIgMTMuMXYuOWgzdi0xSDMuMkw1IDEwLjlWMTBIMnYxem01LTZ2MmgxNFY1SDd6bTAgMTRoMTR2LTJIN3Yyem0wLTZoMTR2LTJIN3YyeicgfSk7XG5cbnZhciBGb3JtYXRMaXN0TnVtYmVyZWQgPSBmdW5jdGlvbiBGb3JtYXRMaXN0TnVtYmVyZWQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdExpc3ROdW1iZXJlZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0TGlzdE51bWJlcmVkKTtcbkZvcm1hdExpc3ROdW1iZXJlZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRMaXN0TnVtYmVyZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNiAxN2gzbDItNFY3SDV2Nmgzem04IDBoM2wyLTRWN2gtNnY2aDN6JyB9KTtcblxudmFyIEZvcm1hdFF1b3RlID0gZnVuY3Rpb24gRm9ybWF0UXVvdGUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdFF1b3RlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRRdW90ZSk7XG5Gb3JtYXRRdW90ZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRRdW90ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0UXVvdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTIgMTdjMy4zMSAwIDYtMi42OSA2LTZWM2gtMi41djhjMCAxLjkzLTEuNTcgMy41LTMuNSAzLjVTOC41IDEyLjkzIDguNSAxMVYzSDZ2OGMwIDMuMzEgMi42OSA2IDYgNnptLTcgMnYyaDE0di0ySDV6JyB9KTtcblxudmFyIEZvcm1hdFVuZGVybGluZWQgPSBmdW5jdGlvbiBGb3JtYXRVbmRlcmxpbmVkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRVbmRlcmxpbmVkID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRVbmRlcmxpbmVkKTtcbkZvcm1hdFVuZGVybGluZWQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0VW5kZXJsaW5lZDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRVbmRlcmxpbmVkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRVbmRlcmxpbmVkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE4IDRINnYybDYuNSA2TDYgMTh2MmgxMnYtM2gtN2w1LTUtNS01aDd6JyB9KTtcblxudmFyIEZ1bmN0aW9ucyA9IGZ1bmN0aW9uIEZ1bmN0aW9ucyhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRnVuY3Rpb25zID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGdW5jdGlvbnMpO1xuRnVuY3Rpb25zLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZ1bmN0aW9ucztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GdW5jdGlvbnMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ002IDE0bDMgM3Y1aDZ2LTVsMy0zVjlINnptNS0xMmgydjNoLTJ6TTMuNSA1Ljg3NUw0LjkxNCA0LjQ2bDIuMTIgMi4xMjJMNS42MiA3Ljk5N3ptMTMuNDYuNzFsMi4xMjMtMi4xMiAxLjQxNCAxLjQxNEwxOC4zNzUgOHonIH0pO1xuXG52YXIgSGlnaGxpZ2h0ID0gZnVuY3Rpb24gSGlnaGxpZ2h0KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5IaWdobGlnaHQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEhpZ2hsaWdodCk7XG5IaWdobGlnaHQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSGlnaGxpZ2h0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0hpZ2hsaWdodC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTIwIDJINGMtMS4xIDAtMiAuOS0yIDJ2MTJjMCAxLjEuOSAyIDIgMmgxNGw0IDRWNGMwLTEuMS0uOS0yLTItMnptLTIgMTJINnYtMmgxMnYyem0wLTNINlY5aDEydjJ6bTAtM0g2VjZoMTJ2MnonIH0pO1xuXG52YXIgSW5zZXJ0Q29tbWVudCA9IGZ1bmN0aW9uIEluc2VydENvbW1lbnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydENvbW1lbnQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydENvbW1lbnQpO1xuSW5zZXJ0Q29tbWVudC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRDb21tZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTEuOTkgMkM2LjQ3IDIgMiA2LjQ4IDIgMTJzNC40NyAxMCA5Ljk5IDEwQzE3LjUyIDIyIDIyIDE3LjUyIDIyIDEyUzE3LjUyIDIgMTEuOTkgMnpNMTIgMjBjLTQuNDIgMC04LTMuNTgtOC04czMuNTgtOCA4LTggOCAzLjU4IDggOC0zLjU4IDgtOCA4em0zLjUtOWMuODMgMCAxLjUtLjY3IDEuNS0xLjVTMTYuMzMgOCAxNS41IDggMTQgOC42NyAxNCA5LjVzLjY3IDEuNSAxLjUgMS41em0tNyAwYy44MyAwIDEuNS0uNjcgMS41LTEuNVM5LjMzIDggOC41IDggNyA4LjY3IDcgOS41IDcuNjcgMTEgOC41IDExem0zLjUgNi41YzIuMzMgMCA0LjMxLTEuNDYgNS4xMS0zLjVINi44OWMuOCAyLjA0IDIuNzggMy41IDUuMTEgMy41eicgfSk7XG5cbnZhciBJbnNlcnRFbW90aWNvbiA9IGZ1bmN0aW9uIEluc2VydEVtb3RpY29uKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRFbW90aWNvbiA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSW5zZXJ0RW1vdGljb24pO1xuSW5zZXJ0RW1vdGljb24ubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0RW1vdGljb247XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMuOSAxMmMwLTEuNzEgMS4zOS0zLjEgMy4xLTMuMWg0VjdIN2MtMi43NiAwLTUgMi4yNC01IDVzMi4yNCA1IDUgNWg0di0xLjlIN2MtMS43MSAwLTMuMS0xLjM5LTMuMS0zLjF6TTggMTNoOHYtMkg4djJ6bTktNmgtNHYxLjloNGMxLjcxIDAgMy4xIDEuMzkgMy4xIDMuMXMtMS4zOSAzLjEtMy4xIDMuMWgtNFYxN2g0YzIuNzYgMCA1LTIuMjQgNS01cy0yLjI0LTUtNS01eicgfSk7XG5cbnZhciBJbnNlcnRMaW5rID0gZnVuY3Rpb24gSW5zZXJ0TGluayhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0TGluayA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSW5zZXJ0TGluayk7XG5JbnNlcnRMaW5rLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydExpbms7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0TGluay5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0TGluay5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMSAxOVY1YzAtMS4xLS45LTItMi0ySDVjLTEuMSAwLTIgLjktMiAydjE0YzAgMS4xLjkgMiAyIDJoMTRjMS4xIDAgMi0uOSAyLTJ6TTguNSAxMy41bDIuNSAzLjAxTDE0LjUgMTJsNC41IDZINWwzLjUtNC41eicgfSk7XG5cbnZhciBJbnNlcnRQaG90byA9IGZ1bmN0aW9uIEluc2VydFBob3RvKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRQaG90byA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoSW5zZXJ0UGhvdG8pO1xuSW5zZXJ0UGhvdG8ubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0UGhvdG87XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAyNCAzMSAzNCAzOCAzOSA0NCA0NSA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE3IDNINWMtMS4xMSAwLTIgLjktMiAydjE0YzAgMS4xLjg5IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yVjdsLTQtNHptLTUgMTZjLTEuNjYgMC0zLTEuMzQtMy0zczEuMzQtMyAzLTMgMyAxLjM0IDMgMy0xLjM0IDMtMyAzem0zLTEwSDVWNWgxMHY0eicgfSk7XG5cbnZhciBTYXZlID0gZnVuY3Rpb24gU2F2ZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuU2F2ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoU2F2ZSk7XG5TYXZlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFNhdmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2F2ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2F2ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDI2IDMxIDM0IDM4IDQ0IDQ1IDQ2IDQ3IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNSA0djNoNS41djEyaDNWN0gxOVY0eicgfSk7XG5cbnZhciBUaXRsZSA9IGZ1bmN0aW9uIFRpdGxlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5UaXRsZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoVGl0bGUpO1xuVGl0bGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gVGl0bGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1RpdGxlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQnV0dG9uQmFzZSA9IHJlcXVpcmUoJy4uL0J1dHRvbkJhc2UnKTtcblxudmFyIF9CdXR0b25CYXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0J1dHRvbkJhc2UpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4uL0ljb24nKTtcblxudmFyIF9JY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pO1xuXG5yZXF1aXJlKCcuLi9TdmdJY29uJyk7XG5cbnZhciBfcmVhY3RIZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvcmVhY3RIZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7IC8vICB3ZWFrXG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IEJ1dHRvbkJhc2VcblxuLy8gRW5zdXJlIENTUyBzcGVjaWZpY2l0eVxuXG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gICAgICBmbGV4OiAnMCAwIGF1dG8nLFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyNCksXG4gICAgICB3aWR0aDogdGhlbWUuc3BhY2luZy51bml0ICogNixcbiAgICAgIGhlaWdodDogdGhlbWUuc3BhY2luZy51bml0ICogNixcbiAgICAgIHBhZGRpbmc6IDAsXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZSxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnYmFja2dyb3VuZC1jb2xvcicsIHtcbiAgICAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXN0XG4gICAgICB9KVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH0sXG4gICAgY29sb3JJbmhlcml0OiB7XG4gICAgICBjb2xvcjogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBkaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBsYWJlbDoge1xuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdpbmhlcml0JyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnaW5oZXJpdCdcbiAgICB9LFxuICAgIGljb246IHtcbiAgICAgIHdpZHRoOiAnMWVtJyxcbiAgICAgIGhlaWdodDogJzFlbSdcbiAgICB9LFxuICAgIGtleWJvYXJkRm9jdXNlZDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlclxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIG5hdGl2ZSBidXR0b24gY29tcG9uZW50LlxuICAgKi9cbiAgYnV0dG9uUmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogVGhlIGljb24gZWxlbWVudC5cbiAgICogSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhbiBpY29uIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydkZWZhdWx0JywgJ2luaGVyaXQnLCAncHJpbWFyeScsICdjb250cmFzdCcsICdhY2NlbnQnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYnV0dG9uIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgcmlwcGxlIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlUmlwcGxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSByb290IGNvbXBvbmVudC5cbiAgICovXG4gIHJvb3RSZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jXG59O1xuXG4vKipcbiAqIFJlZmVyIHRvIHRoZSBbSWNvbnNdKC9zdHlsZS9pY29ucykgc2VjdGlvbiBvZiB0aGUgZG9jdW1lbnRhdGlvblxuICogcmVnYXJkaW5nIHRoZSBhdmFpbGFibGUgaWNvbiBvcHRpb25zLlxuICovXG52YXIgSWNvbkJ1dHRvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEljb25CdXR0b24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEljb25CdXR0b24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgSWNvbkJ1dHRvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEljb25CdXR0b24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEljb25CdXR0b24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEljb25CdXR0b24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBidXR0b25SZWYgPSBfcHJvcHMuYnV0dG9uUmVmLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIGRpc2FibGVkID0gX3Byb3BzLmRpc2FibGVkLFxuICAgICAgICAgIHJvb3RSZWYgPSBfcHJvcHMucm9vdFJlZixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydidXR0b25SZWYnLCAnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAnZGlzYWJsZWQnLCAncm9vdFJlZiddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9CdXR0b25CYXNlMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnZGVmYXVsdCcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5kaXNhYmxlZCwgZGlzYWJsZWQpLCBfY2xhc3NOYW1lcyksIGNsYXNzTmFtZSksXG4gICAgICAgICAgY2VudGVyUmlwcGxlOiB0cnVlLFxuICAgICAgICAgIGtleWJvYXJkRm9jdXNlZENsYXNzTmFtZTogY2xhc3Nlcy5rZXlib2FyZEZvY3VzZWQsXG4gICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVkXG4gICAgICAgIH0sIG90aGVyLCB7XG4gICAgICAgICAgcm9vdFJlZjogYnV0dG9uUmVmLFxuICAgICAgICAgIHJlZjogcm9vdFJlZlxuICAgICAgICB9KSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmxhYmVsIH0sXG4gICAgICAgICAgdHlwZW9mIGNoaWxkcmVuID09PSAnc3RyaW5nJyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgX0ljb24yLmRlZmF1bHQsXG4gICAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5pY29uIH0sXG4gICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICkgOiBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ubWFwKGNoaWxkcmVuLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICAgIGlmICgoMCwgX3JlYWN0SGVscGVycy5pc011aUVsZW1lbnQpKGNoaWxkLCBbJ0ljb24nLCAnU3ZnSWNvbiddKSkge1xuICAgICAgICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmljb24sIGNoaWxkLnByb3BzLmNsYXNzTmFtZSlcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBjaGlsZDtcbiAgICAgICAgICB9KVxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gSWNvbkJ1dHRvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkljb25CdXR0b24uZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ2RlZmF1bHQnLFxuICBkaXNhYmxlZDogZmFsc2UsXG4gIGRpc2FibGVSaXBwbGU6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUljb25CdXR0b24nIH0pKEljb25CdXR0b24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAzIDYgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzcgMzggMzkgNDAgNDQgNDUgNDYgNDkgNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfSWNvbkJ1dHRvbiA9IHJlcXVpcmUoJy4vSWNvbkJ1dHRvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uQnV0dG9uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAzIDYgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzcgMzggMzkgNDAgNDQgNDUgNDYgNDkgNTUiLCJcInVzZSBzdHJpY3RcIjtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLm9uZU9mVHlwZShbcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLCByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zaGFwZSh7XG4gIGVudGVyOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgZXhpdDogcmVxdWlyZShcInByb3AtdHlwZXNcIikubnVtYmVyLmlzUmVxdWlyZWRcbn0pXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5mdW5jO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNsYXNzZXMgPSB7XG4gIGFwcGVhcjogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBhcHBlYXJBY3RpdmU6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZW50ZXI6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZW50ZXJBY3RpdmU6IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpLnN0cmluZyxcbiAgZXhpdDogcmVxdWlyZShcInByb3AtdHlwZXNcIikuc3RyaW5nLFxuICBleGl0QWN0aXZlOiByZXF1aXJlKFwicHJvcC10eXBlc1wiKS5zdHJpbmdcbn07XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvaW50ZXJuYWwvdHJhbnNpdGlvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDUgMTEgMjcgMjggMzQgMzUgMzYgMzcgMzggNDAgNDEgNDIgNDMgNDYiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF9UcmFuc2l0aW9uID0gcmVxdWlyZSgncmVhY3QtdHJhbnNpdGlvbi1ncm91cC9UcmFuc2l0aW9uJyk7XG5cbnZhciBfVHJhbnNpdGlvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UcmFuc2l0aW9uKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX3RyYW5zaXRpb25zID0gcmVxdWlyZSgnLi4vc3R5bGVzL3RyYW5zaXRpb25zJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgVHJhbnNpdGlvblxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPSByZXF1aXJlKCcuLi9pbnRlcm5hbC90cmFuc2l0aW9uJykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIGNvbnRhaW5lcjoge1xuICAgICAgaGVpZ2h0OiAwLFxuICAgICAgb3ZlcmZsb3c6ICdoaWRkZW4nLFxuICAgICAgdHJhbnNpdGlvbjogdGhlbWUudHJhbnNpdGlvbnMuY3JlYXRlKCdoZWlnaHQnKVxuICAgIH0sXG4gICAgZW50ZXJlZDoge1xuICAgICAgaGVpZ2h0OiAnYXV0bydcbiAgICB9LFxuICAgIHdyYXBwZXI6IHtcbiAgICAgIC8vIEhhY2sgdG8gZ2V0IGNoaWxkcmVuIHdpdGggYSBuZWdhdGl2ZSBtYXJnaW4gdG8gbm90IGZhbHNpZnkgdGhlIGhlaWdodCBjb21wdXRhdGlvbi5cbiAgICAgIGRpc3BsYXk6ICdmbGV4J1xuICAgIH0sXG4gICAgd3JhcHBlcklubmVyOiB7XG4gICAgICB3aWR0aDogJzEwMCUnXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25EdXJhdGlvbiA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gIGVudGVyOiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICBleGl0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG59KSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0byddKV0pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBhcHBlYXI6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG5vZGUgdG8gYmUgY29sbGFwc2VkLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHR5cGVvZiBTdHJpbmcgPT09ICdmdW5jdGlvbicgPyByZXF1aXJlKCdwcm9wLXR5cGVzJykuaW5zdGFuY2VPZihTdHJpbmcpIDogcmVxdWlyZSgncHJvcC10eXBlcycpLmFueSxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIGZvciB0aGUgcm9vdCBub2RlLlxuICAgKiBFaXRoZXIgYSBzdHJpbmcgdG8gdXNlIGEgRE9NIGVsZW1lbnQgb3IgYSBjb21wb25lbnQuXG4gICAqIFRoZSBkZWZhdWx0IHZhbHVlIGlzIGEgYGJ1dHRvbmAuXG4gICAqL1xuICBjb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBoZWlnaHQgb2YgdGhlIGNvbnRhaW5lciB3aGVuIGNvbGxhcHNlZC5cbiAgICovXG4gIGNvbGxhcHNlZEhlaWdodDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZy5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBQcm9wZXJ0aWVzIGFwcGxpZWQgdG8gdGhlIGBDb2xsYXBzZWAgY29udGFpbmVyLlxuICAgKi9cbiAgY29udGFpbmVyUHJvcHM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNvbXBvbmVudCB3aWxsIHRyYW5zaXRpb24gaW4uXG4gICAqL1xuICBpbjogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FbnRlcjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkVudGVyaW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uRW50ZXJlZDogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2spLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBvbkV4aXQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UcmFuc2l0aW9uQ2FsbGJhY2sgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25FeGl0aW5nOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfVHJhbnNpdGlvbkNhbGxiYWNrIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1RyYW5zaXRpb25DYWxsYmFjayksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHN0eWxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBUaGUgZHVyYXRpb24gZm9yIHRoZSB0cmFuc2l0aW9uLCBpbiBtaWxsaXNlY29uZHMuXG4gICAqIFlvdSBtYXkgc3BlY2lmeSBhIHNpbmdsZSB0aW1lb3V0IGZvciBhbGwgdHJhbnNpdGlvbnMsIG9yIGluZGl2aWR1YWxseSB3aXRoIGFuIG9iamVjdC5cbiAgICpcbiAgICogU2V0IHRvICdhdXRvJyB0byBhdXRvbWF0aWNhbGx5IGNhbGN1bGF0ZSB0cmFuc2l0aW9uIHRpbWUgYmFzZWQgb24gaGVpZ2h0LlxuICAgKi9cbiAgdGltZW91dDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlciwgcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKHtcbiAgICBlbnRlcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcbiAgICBleGl0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyXG4gIH0pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhdXRvJ10pXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgdW5tb3VudE9uRXhpdDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2xcbn07XG5cbnZhciBDb2xsYXBzZSA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKENvbGxhcHNlLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBDb2xsYXBzZSgpIHtcbiAgICB2YXIgX3JlZjtcblxuICAgIHZhciBfdGVtcCwgX3RoaXMsIF9yZXQ7XG5cbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBDb2xsYXBzZSk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBDb2xsYXBzZS5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQ29sbGFwc2UpKS5jYWxsLmFwcGx5KF9yZWYsIFt0aGlzXS5jb25jYXQoYXJncykpKSwgX3RoaXMpLCBfdGhpcy53cmFwcGVyID0gbnVsbCwgX3RoaXMuYXV0b1RyYW5zaXRpb25EdXJhdGlvbiA9IHVuZGVmaW5lZCwgX3RoaXMuaGFuZGxlRW50ZXIgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSBfdGhpcy5wcm9wcy5jb2xsYXBzZWRIZWlnaHQ7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkVudGVyKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXIobm9kZSk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlRW50ZXJpbmcgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgdGltZW91dCA9IF90aGlzJHByb3BzLnRpbWVvdXQsXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wcy50aGVtZTtcblxuICAgICAgdmFyIHdyYXBwZXJIZWlnaHQgPSBfdGhpcy53cmFwcGVyID8gX3RoaXMud3JhcHBlci5jbGllbnRIZWlnaHQgOiAwO1xuXG4gICAgICBpZiAodGltZW91dCA9PT0gJ2F1dG8nKSB7XG4gICAgICAgIHZhciBkdXJhdGlvbjIgPSB0aGVtZS50cmFuc2l0aW9ucy5nZXRBdXRvSGVpZ2h0RHVyYXRpb24od3JhcHBlckhlaWdodCk7XG4gICAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gZHVyYXRpb24yICsgJ21zJztcbiAgICAgICAgX3RoaXMuYXV0b1RyYW5zaXRpb25EdXJhdGlvbiA9IGR1cmF0aW9uMjtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gdGltZW91dCArICdtcyc7XG4gICAgICB9IGVsc2UgaWYgKHRpbWVvdXQgJiYgdHlwZW9mIHRpbWVvdXQuZW50ZXIgPT09ICdudW1iZXInKSB7XG4gICAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gdGltZW91dC5lbnRlciArICdtcyc7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICAvLyBUaGUgcHJvcFR5cGUgd2lsbCB3YXJuIGluIHRoaXMgY2FzZS5cbiAgICAgIH1cblxuICAgICAgbm9kZS5zdHlsZS5oZWlnaHQgPSB3cmFwcGVySGVpZ2h0ICsgJ3B4JztcblxuICAgICAgaWYgKF90aGlzLnByb3BzLm9uRW50ZXJpbmcpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FbnRlcmluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFbnRlcmVkID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gJ2F1dG8nO1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FbnRlcmVkKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRW50ZXJlZChub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5oYW5kbGVFeGl0ID0gZnVuY3Rpb24gKG5vZGUpIHtcbiAgICAgIHZhciB3cmFwcGVySGVpZ2h0ID0gX3RoaXMud3JhcHBlciA/IF90aGlzLndyYXBwZXIuY2xpZW50SGVpZ2h0IDogMDtcbiAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gd3JhcHBlckhlaWdodCArICdweCc7XG5cbiAgICAgIGlmIChfdGhpcy5wcm9wcy5vbkV4aXQpIHtcbiAgICAgICAgX3RoaXMucHJvcHMub25FeGl0KG5vZGUpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZUV4aXRpbmcgPSBmdW5jdGlvbiAobm9kZSkge1xuICAgICAgdmFyIF90aGlzJHByb3BzMiA9IF90aGlzLnByb3BzLFxuICAgICAgICAgIHRpbWVvdXQgPSBfdGhpcyRwcm9wczIudGltZW91dCxcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzMi50aGVtZTtcblxuICAgICAgdmFyIHdyYXBwZXJIZWlnaHQgPSBfdGhpcy53cmFwcGVyID8gX3RoaXMud3JhcHBlci5jbGllbnRIZWlnaHQgOiAwO1xuXG4gICAgICBpZiAodGltZW91dCA9PT0gJ2F1dG8nKSB7XG4gICAgICAgIHZhciBkdXJhdGlvbjIgPSB0aGVtZS50cmFuc2l0aW9ucy5nZXRBdXRvSGVpZ2h0RHVyYXRpb24od3JhcHBlckhlaWdodCk7XG4gICAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gZHVyYXRpb24yICsgJ21zJztcbiAgICAgICAgX3RoaXMuYXV0b1RyYW5zaXRpb25EdXJhdGlvbiA9IGR1cmF0aW9uMjtcbiAgICAgIH0gZWxzZSBpZiAodHlwZW9mIHRpbWVvdXQgPT09ICdudW1iZXInKSB7XG4gICAgICAgIG5vZGUuc3R5bGUudHJhbnNpdGlvbkR1cmF0aW9uID0gdGltZW91dCArICdtcyc7XG4gICAgICB9IGVsc2UgaWYgKHRpbWVvdXQgJiYgdHlwZW9mIHRpbWVvdXQuZXhpdCA9PT0gJ251bWJlcicpIHtcbiAgICAgICAgbm9kZS5zdHlsZS50cmFuc2l0aW9uRHVyYXRpb24gPSB0aW1lb3V0LmV4aXQgKyAnbXMnO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgLy8gVGhlIHByb3BUeXBlIHdpbGwgd2FybiBpbiB0aGlzIGNhc2UuXG4gICAgICB9XG5cbiAgICAgIG5vZGUuc3R5bGUuaGVpZ2h0ID0gX3RoaXMucHJvcHMuY29sbGFwc2VkSGVpZ2h0O1xuXG4gICAgICBpZiAoX3RoaXMucHJvcHMub25FeGl0aW5nKSB7XG4gICAgICAgIF90aGlzLnByb3BzLm9uRXhpdGluZyhub2RlKTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy5hZGRFbmRMaXN0ZW5lciA9IGZ1bmN0aW9uIChub2RlLCBuZXh0KSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMudGltZW91dCA9PT0gJ2F1dG8nKSB7XG4gICAgICAgIHNldFRpbWVvdXQobmV4dCwgX3RoaXMuYXV0b1RyYW5zaXRpb25EdXJhdGlvbiB8fCAwKTtcbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoQ29sbGFwc2UsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF90aGlzMiA9IHRoaXM7XG5cbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGFwcGVhciA9IF9wcm9wcy5hcHBlYXIsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZSA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgQ29tcG9uZW50UHJvcCA9IF9wcm9wcy5jb21wb25lbnQsXG4gICAgICAgICAgY29sbGFwc2VkSGVpZ2h0ID0gX3Byb3BzLmNvbGxhcHNlZEhlaWdodCxcbiAgICAgICAgICBjb250YWluZXJQcm9wcyA9IF9wcm9wcy5jb250YWluZXJQcm9wcyxcbiAgICAgICAgICBvbkVudGVyID0gX3Byb3BzLm9uRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmluZyA9IF9wcm9wcy5vbkVudGVyaW5nLFxuICAgICAgICAgIG9uRW50ZXJlZCA9IF9wcm9wcy5vbkVudGVyZWQsXG4gICAgICAgICAgb25FeGl0ID0gX3Byb3BzLm9uRXhpdCxcbiAgICAgICAgICBvbkV4aXRpbmcgPSBfcHJvcHMub25FeGl0aW5nLFxuICAgICAgICAgIHN0eWxlID0gX3Byb3BzLnN0eWxlLFxuICAgICAgICAgIHRpbWVvdXQgPSBfcHJvcHMudGltZW91dCxcbiAgICAgICAgICB0aGVtZSA9IF9wcm9wcy50aGVtZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhcHBlYXInLCAnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29tcG9uZW50JywgJ2NvbGxhcHNlZEhlaWdodCcsICdjb250YWluZXJQcm9wcycsICdvbkVudGVyJywgJ29uRW50ZXJpbmcnLCAnb25FbnRlcmVkJywgJ29uRXhpdCcsICdvbkV4aXRpbmcnLCAnc3R5bGUnLCAndGltZW91dCcsICd0aGVtZSddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9UcmFuc2l0aW9uMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBhcHBlYXI6IGFwcGVhcixcbiAgICAgICAgICBvbkVudGVyaW5nOiB0aGlzLmhhbmRsZUVudGVyaW5nLFxuICAgICAgICAgIG9uRW50ZXI6IHRoaXMuaGFuZGxlRW50ZXIsXG4gICAgICAgICAgb25FbnRlcmVkOiB0aGlzLmhhbmRsZUVudGVyZWQsXG4gICAgICAgICAgb25FeGl0aW5nOiB0aGlzLmhhbmRsZUV4aXRpbmcsXG4gICAgICAgICAgb25FeGl0OiB0aGlzLmhhbmRsZUV4aXQsXG4gICAgICAgICAgYWRkRW5kTGlzdGVuZXI6IHRoaXMuYWRkRW5kTGlzdGVuZXIsXG4gICAgICAgICAgdGltZW91dDogdGltZW91dCA9PT0gJ2F1dG8nID8gbnVsbCA6IHRpbWVvdXRcbiAgICAgICAgfSwgb3RoZXIpLFxuICAgICAgICBmdW5jdGlvbiAoc3RhdGUsIG90aGVySW5uZXIpIHtcbiAgICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICBDb21wb25lbnRQcm9wLFxuICAgICAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmNvbnRhaW5lciwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuZW50ZXJlZCwgc3RhdGUgPT09ICdlbnRlcmVkJyksIGNsYXNzTmFtZSksXG4gICAgICAgICAgICAgIHN0eWxlOiAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHt9LCBzdHlsZSwge1xuICAgICAgICAgICAgICAgIG1pbkhlaWdodDogY29sbGFwc2VkSGVpZ2h0XG4gICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICB9LCBvdGhlcklubmVyKSxcbiAgICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3Nlcy53cmFwcGVyLFxuICAgICAgICAgICAgICAgIHJlZjogZnVuY3Rpb24gcmVmKG5vZGUpIHtcbiAgICAgICAgICAgICAgICAgIF90aGlzMi53cmFwcGVyID0gbm9kZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgICAgICdkaXYnLFxuICAgICAgICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLndyYXBwZXJJbm5lciB9LFxuICAgICAgICAgICAgICAgIGNoaWxkcmVuXG4gICAgICAgICAgICAgIClcbiAgICAgICAgICAgIClcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gQ29sbGFwc2U7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5Db2xsYXBzZS5kZWZhdWx0UHJvcHMgPSB7XG4gIGFwcGVhcjogZmFsc2UsXG4gIGNvbXBvbmVudDogJ2RpdicsXG4gIGNvbGxhcHNlZEhlaWdodDogJzBweCcsXG4gIHRpbWVvdXQ6IF90cmFuc2l0aW9ucy5kdXJhdGlvbi5zdGFuZGFyZFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7XG4gIHdpdGhUaGVtZTogdHJ1ZSxcbiAgbmFtZTogJ011aUNvbGxhcHNlJ1xufSkoQ29sbGFwc2UpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3RyYW5zaXRpb25zL0NvbGxhcHNlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS90cmFuc2l0aW9ucy9Db2xsYXBzZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDExIDM4IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSByZXF1aXJlKCcuL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwnKTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSByZXF1aXJlKCcuL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQnKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRmFjdG9yeSA9IGZ1bmN0aW9uIGNyZWF0ZUZhY3RvcnkodHlwZSkge1xuICB2YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQgPSAoMCwgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyLmRlZmF1bHQpKHR5cGUpO1xuICByZXR1cm4gZnVuY3Rpb24gKHAsIGMpIHtcbiAgICByZXR1cm4gKDAsIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMi5kZWZhdWx0KShmYWxzZSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHAsIGMpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRmFjdG9yeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgZ2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBnZXREaXNwbGF5TmFtZShDb21wb25lbnQpIHtcbiAgaWYgKHR5cGVvZiBDb21wb25lbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfVxuXG4gIGlmICghQ29tcG9uZW50KSB7XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuXG4gIHJldHVybiBDb21wb25lbnQuZGlzcGxheU5hbWUgfHwgQ29tcG9uZW50Lm5hbWUgfHwgJ0NvbXBvbmVudCc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBnZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XG5cbnZhciBpc0NsYXNzQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNDbGFzc0NvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4oQ29tcG9uZW50ICYmIENvbXBvbmVudC5wcm90b3R5cGUgJiYgX3R5cGVvZihDb21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQpID09PSAnb2JqZWN0Jyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc0NsYXNzQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQgPSByZXF1aXJlKCcuL2lzQ2xhc3NDb21wb25lbnQnKTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzQ2xhc3NDb21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IGZ1bmN0aW9uIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKHR5cGVvZiBDb21wb25lbnQgPT09ICdmdW5jdGlvbicgJiYgISgwLCBfaXNDbGFzc0NvbXBvbmVudDIuZGVmYXVsdCkoQ29tcG9uZW50KSAmJiAhQ29tcG9uZW50LmRlZmF1bHRQcm9wcyAmJiAhQ29tcG9uZW50LmNvbnRleHRUeXBlcyAmJiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdwcm9kdWN0aW9uJyB8fCAhQ29tcG9uZW50LnByb3BUeXBlcykpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaG91bGRVcGRhdGUgPSByZXF1aXJlKCcuL3Nob3VsZFVwZGF0ZScpO1xuXG52YXIgX3Nob3VsZFVwZGF0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaG91bGRVcGRhdGUpO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJy4vc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwdXJlID0gZnVuY3Rpb24gcHVyZShCYXNlQ29tcG9uZW50KSB7XG4gIHZhciBob2MgPSAoMCwgX3Nob3VsZFVwZGF0ZTIuZGVmYXVsdCkoZnVuY3Rpb24gKHByb3BzLCBuZXh0UHJvcHMpIHtcbiAgICByZXR1cm4gISgwLCBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0KShwcm9wcywgbmV4dFByb3BzKTtcbiAgfSk7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdwdXJlJykpKGhvYyhCYXNlQ29tcG9uZW50KSk7XG4gIH1cblxuICByZXR1cm4gaG9jKEJhc2VDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gcHVyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zZXRTdGF0aWMgPSByZXF1aXJlKCcuL3NldFN0YXRpYycpO1xuXG52YXIgX3NldFN0YXRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRTdGF0aWMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBzZXREaXNwbGF5TmFtZShkaXNwbGF5TmFtZSkge1xuICByZXR1cm4gKDAsIF9zZXRTdGF0aWMyLmRlZmF1bHQpKCdkaXNwbGF5TmFtZScsIGRpc3BsYXlOYW1lKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBzZXRTdGF0aWMgPSBmdW5jdGlvbiBzZXRTdGF0aWMoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIEJhc2VDb21wb25lbnRba2V5XSA9IHZhbHVlO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICByZXR1cm4gQmFzZUNvbXBvbmVudDtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldFN0YXRpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnZmJqcy9saWIvc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9zaGFsbG93RXF1YWwyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkgPSByZXF1aXJlKCcuL2NyZWF0ZUVhZ2VyRmFjdG9yeScpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckZhY3RvcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBzaG91bGRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRVcGRhdGUodGVzdCkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICB2YXIgZmFjdG9yeSA9ICgwLCBfY3JlYXRlRWFnZXJGYWN0b3J5Mi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KTtcblxuICAgIHZhciBTaG91bGRVcGRhdGUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgICAgX2luaGVyaXRzKFNob3VsZFVwZGF0ZSwgX0NvbXBvbmVudCk7XG5cbiAgICAgIGZ1bmN0aW9uIFNob3VsZFVwZGF0ZSgpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNob3VsZFVwZGF0ZSk7XG5cbiAgICAgICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgICB9XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgICByZXR1cm4gdGVzdCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiBmYWN0b3J5KHRoaXMucHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgICB9KF9yZWFjdC5Db21wb25lbnQpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3Nob3VsZFVwZGF0ZScpKShTaG91bGRVcGRhdGUpO1xuICAgIH1cbiAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2hvdWxkVXBkYXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IGZ1bmN0aW9uIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwoaGFzS2V5LCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcHJvcHMsIGNoaWxkcmVuKSB7XG4gIGlmICghaGFzS2V5ICYmIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50KSB7XG4gICAgaWYgKGNoaWxkcmVuKSB7XG4gICAgICByZXR1cm4gdHlwZShfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgY2hpbGRyZW46IGNoaWxkcmVuIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHR5cGUocHJvcHMpO1xuICB9XG5cbiAgdmFyIENvbXBvbmVudCA9IHR5cGU7XG5cbiAgaWYgKGNoaWxkcmVuKSB7XG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgQ29tcG9uZW50LFxuICAgICAgcHJvcHMsXG4gICAgICBjaGlsZHJlblxuICAgICk7XG4gIH1cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBwcm9wcyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVFYWdlckVsZW1lbnRVdGlsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2dldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9nZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX2dldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHdyYXBEaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHdyYXBEaXNwbGF5TmFtZShCYXNlQ29tcG9uZW50LCBob2NOYW1lKSB7XG4gIHJldHVybiBob2NOYW1lICsgJygnICsgKDAsIF9nZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCkgKyAnKSc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3cmFwRGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IE1PRFVMRV9ERUxFVEUgfSBmcm9tICcuLi8uLi8uLi8uLi9jb25zdGFudHMvY291cnNlcyc7XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgYXV0aG9ySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgbW9kdWxlSWQ6IG51bWJlcixcbn07XG5cbnR5cGUgQWN0aW9uID0ge1xuICB0eXBlOiBzdHJpbmcsXG4gIHBheWxvYWQ6IFBheWxvYWQsXG59O1xuXG4vKipcbiAqXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5hdXRob3JJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5tb2R1bGVJZCAtXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IGRlbGV0ZU1vZHVsZSA9IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7XG4gIHR5cGU6IE1PRFVMRV9ERUxFVEUsXG4gIHBheWxvYWQsXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgZGVsZXRlTW9kdWxlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy9kZWxldGUuanMiLCIvKipcbiAqIEBmb3JtYXRcbiAqIEBmbG93XG4gKi9cblxuaW1wb3J0IHsgTU9EVUxFX1VQREFURSB9IGZyb20gJy4uLy4uLy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBQYXlsb2FkID0ge1xuICBhdXRob3JJZDogbnVtYmVyLFxuICBjb3Vyc2VJZDogbnVtYmVyLFxuICBtb2R1bGVJZDogbnVtYmVyLFxuICB0aXRsZT86IHN0cmluZyxcbiAgZGF0YT86IG1peGVkLFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICogVGhpcyBhY3Rpb24gd2lsbCB1cGRhdGUgY291cnNlIG1vZHVsZSBpbiBzdG9yZVxuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuYXV0aG9ySWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlSWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQubW9kdWxlSWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudGl0bGUgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuZGF0YSAtXG4gKiBAcmV0dXJuIHtPYmplY3R9XG4gKi9cbmNvbnN0IHVwZGF0ZSA9IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7IHR5cGU6IE1PRFVMRV9VUERBVEUsIHBheWxvYWQgfSk7XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL3VzZXJzL21vZHVsZXMvdXBkYXRlLmpzIiwiLyoqXG4gKiBAZmlsZVxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vLi4vY29uZmlnJztcblxudHlwZSBQYXlsb2FkID0ge1xuICB0b2tlbjogc3RyaW5nLFxuICB1c2VySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgbW9kdWxlSWQ6IG51bWJlcixcbn07XG5cbi8qKlxuICogRm9yIGRlbGV0aW5nIGNvdXJzZSBtb2R1bGVcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIGRlbGV0ZU1vZHVsZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VJZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5tb2R1bGVJZCAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZGVsZXRlTW9kdWxlKHsgdG9rZW4sIHVzZXJJZCwgY291cnNlSWQsIG1vZHVsZUlkIH06IFBheWxvYWQpIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvdXNlcnMvJHt1c2VySWR9L2NvdXJzZXMvJHtjb3Vyc2VJZH0vbW9kdWxlcy8ke21vZHVsZUlkfWA7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0RFTEVURScsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZGVsZXRlTW9kdWxlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvY291cnNlcy91c2Vycy9tb2R1bGVzL2RlbGV0ZS5qcyIsIi8qKlxuICogQGZpbGVPdmVydmlld1xuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5pbXBvcnQgeyBIT1NUIH0gZnJvbSAnLi4vLi4vLi4vY29uZmlnJztcblxudHlwZSBQYXlsb2FkID0ge1xuICB0b2tlbjogc3RyaW5nLFxuICB1c2VySWQ6IG51bWJlcixcbiAgY291cnNlSWQ6IG51bWJlcixcbiAgbW9kdWxlSWQ6IG51bWJlcixcbiAgdGl0bGU/OiBzdHJpbmcsXG4gIGRhdGE/OiBhbnksXG59O1xuXG4vKipcbiAqIFVwZGF0ZSBNb2R1bGVcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQudXNlcklkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZUlkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLm1vZHVsZUlkIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRpdGxlIC1cbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmRhdGEgLVxuICogQHJldHVybiB7UHJvbWlzZX0gLVxuICpcbiAqIEBleGFtcGxlXG4gKi9cbmFzeW5jIGZ1bmN0aW9uIHVwZGF0ZSh7XG4gIHRva2VuLFxuICB1c2VySWQsXG4gIGNvdXJzZUlkLFxuICBtb2R1bGVJZCxcbiAgdGl0bGUsXG4gIGRhdGEsXG59OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzLyR7Y291cnNlSWR9L21vZHVsZXMvJHttb2R1bGVJZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdQVVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoeyB0aXRsZSwgZGF0YSB9KSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB1cGRhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL21vZHVsZXMvdXBkYXRlLmpzIiwiLyoqXG4gKiBUaXRsZSBjb21wb25lbnQgaXMgZm9yIGVkaXRhYmxlIGNvbXBvbmVudCB0byBlZGl0IHRpdGxlIHdpdGggdGV4dGFyZWEuXG4gKiBUaGlzIGNvbXBvbmVudCBzaG91bGQgYmUgdXNlZCB3aXRoIENhcmRIZWFkZXIgdGl0bGUgcGxhY2UuXG4gKiBUT0RPOiBtYWtlIHRleHRhcmVhIGF1dG9yZXNpemFibGUuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyB3aXRoU3R5bGVzIH0gZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICB2YWx1ZToge30sXG4gIHRleHRhcmVhOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICBoZWlnaHQ6ICdhdXRvJyxcbiAgICBib3JkZXI6ICdub25lJyxcbiAgICAvLyBvdXRsaW5lOiAnbm9uZScsXG4gICAgcmVzaXplOiAnbm9uZScsXG4gICAgZm9udDogJ2luaGVyaXQnLFxuICB9LFxufTtcblxuY29uc3QgVGl0bGUgPSAoe1xuICBjbGFzc2VzLFxuICByZWFkT25seSxcbiAgdmFsdWUsXG4gIHBsYWNlaG9sZGVyLFxuICBvbkNoYW5nZSxcbiAgbWluTGVuZ3RoLFxuICBtYXhMZW5ndGgsXG59KSA9PiB7XG4gIGlmIChyZWFkT25seSkge1xuICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy52YWx1ZX0+e3ZhbHVlfTwvZGl2PjtcbiAgfVxuXG4gIHJldHVybiAoXG4gICAgPHRleHRhcmVhXG4gICAgICBwbGFjZWhvbGRlcj17cGxhY2Vob2xkZXJ9XG4gICAgICB2YWx1ZT17dmFsdWV9XG4gICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICBjbGFzc05hbWU9e2NsYXNzZXMudGV4dGFyZWF9XG4gICAgICBtaW5MZW5ndGg9e21pbkxlbmd0aCB8fCAxfVxuICAgICAgbWF4TGVuZ3RoPXttYXhMZW5ndGggfHwgMTQ4fVxuICAgICAgcm93cz17MX1cbiAgICAgIHJlYWRPbmx5PXtyZWFkT25seX1cbiAgICAvPlxuICApO1xufTtcblxuVGl0bGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICB2YWx1ZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgbWluTGVuZ3RoOiBQcm9wVHlwZXMubnVtYmVyLFxuICBtYXhMZW5ndGg6IFByb3BUeXBlcy5udW1iZXIsXG59O1xuXG5UaXRsZS5kZWZhdWx0UHJvcHMgPSB7XG4gIHZhbHVlOiAnVW50aXRsZWQnLFxuICBwbGFjZWhvbGRlcjogJ1RpdGxlIGdvZXMgaGVyZS4uLicsXG4gIHJlYWRPbmx5OiB0cnVlLFxuICBtaW5MZW5ndGg6IDEsXG4gIG1heExlbmd0aDogMTQ4LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFRpdGxlKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29tcG9uZW50cy9UaXRsZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IGNsYXNzbmFtZXMgZnJvbSAnY2xhc3NuYW1lcyc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBDYXJkLCB7IENhcmRIZWFkZXIsIENhcmRDb250ZW50LCBDYXJkQWN0aW9ucyB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcbmltcG9ydCBDb2xsYXBzZSBmcm9tICdtYXRlcmlhbC11aS90cmFuc2l0aW9ucy9Db2xsYXBzZSc7XG5cbmltcG9ydCB7IGNvbnZlcnRUb1JhdyB9IGZyb20gJ2RyYWZ0LWpzJztcblxuaW1wb3J0IEVkaXRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0VkaXQnO1xuaW1wb3J0IFNhdmVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1NhdmUnO1xuaW1wb3J0IERlbGV0ZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRGVsZXRlJztcbmltcG9ydCBFeHBhbmRNb3JlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9FeHBhbmRNb3JlJztcblxuaW1wb3J0IFRpdGxlIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvVGl0bGUnO1xuaW1wb3J0IE1heWFzaEVkaXRvciwgeyBjcmVhdGVFZGl0b3JTdGF0ZSB9IGZyb20gJy4uLy4uLy4uL2xpYi9tYXlhc2gtZWRpdG9yJztcblxuaW1wb3J0IGFwaU1vZHVsZVVwZGF0ZSBmcm9tICcuLi8uLi9hcGkvY291cnNlcy91c2Vycy9tb2R1bGVzL3VwZGF0ZSc7XG5pbXBvcnQgYXBpTW9kdWxlRGVsZXRlIGZyb20gJy4uLy4uL2FwaS9jb3Vyc2VzL3VzZXJzL21vZHVsZXMvZGVsZXRlJztcblxuaW1wb3J0IGFjdGlvblVwZGF0ZSBmcm9tICcuLi8uLi9hY3Rpb25zL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy91cGRhdGUnO1xuaW1wb3J0IGFjdGlvbkRlbGV0ZSBmcm9tICcuLi8uLi9hY3Rpb25zL2NvdXJzZXMvdXNlcnMvbW9kdWxlcy9kZWxldGUnO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nQm90dG9tOiAnMSUnLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgZXhwYW5kOiB7XG4gICAgdHJhbnNmb3JtOiAncm90YXRlKDBkZWcpJyxcbiAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ3RyYW5zZm9ybScsIHtcbiAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdCxcbiAgICB9KSxcbiAgfSxcbiAgZXhwYW5kT3Blbjoge1xuICAgIHRyYW5zZm9ybTogJ3JvdGF0ZSgxODBkZWcpJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufSk7XG5cbmNsYXNzIE1vZHVsZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIGNvbnN0IHsgdGl0bGUsIGRhdGEgfSA9IHByb3BzO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBob3ZlcjogZmFsc2UsXG4gICAgICBleHBhbmRlZDogZmFsc2UsXG4gICAgICBlZGl0OiBmYWxzZSxcblxuICAgICAgdGl0bGUsXG4gICAgICBkYXRhOiBjcmVhdGVFZGl0b3JTdGF0ZShkYXRhKSxcblxuICAgICAgbWVzc2FnZTogJycsXG4gICAgfTtcblxuICAgIHRoaXMuaGFuZGxlRXhwYW5kQ2xpY2sgPSB0aGlzLmhhbmRsZUV4cGFuZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2UuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uRWRpdCA9IHRoaXMub25FZGl0LmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblNhdmUgPSB0aGlzLm9uU2F2ZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25DbGlja0RlbGV0ZSA9IHRoaXMub25DbGlja0RlbGV0ZS5iaW5kKHRoaXMpO1xuICB9XG5cbiAgb25DaGFuZ2UoZGF0YSkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyBkYXRhIH0pO1xuICB9XG5cbiAgb25DaGFuZ2VUaXRsZSA9IChlKSA9PiB0aGlzLnNldFN0YXRlKHsgdGl0bGU6IGUudGFyZ2V0LnZhbHVlIH0pO1xuXG4gIG9uRWRpdCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIGVkaXQ6ICF0aGlzLnN0YXRlLmVkaXQsXG4gICAgICBleHBhbmRlZDogdHJ1ZSxcbiAgICB9KTtcbiAgfVxuXG4gIGFzeW5jIG9uU2F2ZSgpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyBjb3Vyc2VJZCwgbW9kdWxlSWQsIHRpdGxlOiB0aXRsZVByb3AsIGVsZW1lbnRzIH0gPSB0aGlzLnByb3BzO1xuICAgICAgY29uc3QgeyB1c2VySWQsIHRva2VuIH0gPSBlbGVtZW50cy51c2VyO1xuXG4gICAgICBjb25zdCB7IGVkaXQsIHRpdGxlLCBkYXRhIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgICBsZXQgYm9keSA9IHt9O1xuXG4gICAgICBpZiAodGl0bGVQcm9wICE9PSB0aXRsZSkge1xuICAgICAgICBib2R5ID0geyAuLi5ib2R5LCB0aXRsZSB9O1xuICAgICAgfVxuXG4gICAgICBib2R5ID0ge1xuICAgICAgICAuLi5ib2R5LFxuICAgICAgICBkYXRhOiBjb252ZXJ0VG9SYXcoZGF0YS5nZXRDdXJyZW50Q29udGVudCgpKSxcbiAgICAgIH07XG5cbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IsIG1lc3NhZ2UgfSA9IGF3YWl0IGFwaU1vZHVsZVVwZGF0ZSh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIGNvdXJzZUlkLFxuICAgICAgICBtb2R1bGVJZCxcbiAgICAgICAgLi4uYm9keSxcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2U6IGVycm9yIH0pO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtZXNzYWdlIH0pO1xuXG4gICAgICB0aGlzLnByb3BzLmFjdGlvblVwZGF0ZSh7XG4gICAgICAgIGF1dGhvcklkOiB1c2VySWQsXG4gICAgICAgIGNvdXJzZUlkLFxuICAgICAgICBtb2R1bGVJZCxcbiAgICAgICAgLi4uYm9keSxcbiAgICAgICAgc3RhdHVzQ29kZSxcbiAgICAgIH0pO1xuXG4gICAgICB0aGlzLnNldFN0YXRlKHsgZWRpdDogIWVkaXQgfSk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgY29uc29sZS5lcnJvcihlKTtcbiAgICB9XG4gIH1cblxuICBhc3luYyBvbkNsaWNrRGVsZXRlKCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IG1vZHVsZUlkLCB1c2VyLCBjb3Vyc2UgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IGF1dGhvcklkLCBjb3Vyc2VJZCB9ID0gY291cnNlO1xuICAgICAgY29uc3QgeyB0b2tlbiwgaWQ6IHVzZXJJZCB9ID0gdXNlcjtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBtZXNzYWdlLCBlcnJvciB9ID0gYXdhaXQgYXBpTW9kdWxlRGVsZXRlKHtcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIHVzZXJJZCxcbiAgICAgICAgY291cnNlSWQsXG4gICAgICAgIG1vZHVsZUlkLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgbWVzc2FnZTogZXJyb3IgfSk7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zZXRTdGF0ZSh7IG1lc3NhZ2UgfSk7XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uRGVsZXRlKHtcbiAgICAgICAgc3RhdHVzQ29kZSxcbiAgICAgICAgbW9kdWxlSWQsXG4gICAgICAgIGNvdXJzZUlkLFxuICAgICAgICBhdXRob3JJZCxcbiAgICAgIH0pO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICB9XG4gIH1cblxuICBvbk1vdXNlRW50ZXIgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IHRydWUgfSk7XG4gIG9uTW91c2VMZWF2ZSA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogZmFsc2UgfSk7XG5cbiAgaGFuZGxlRXhwYW5kQ2xpY2soKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGV4cGFuZGVkOiAhdGhpcy5zdGF0ZS5leHBhbmRlZCB9KTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIGVsZW1lbnRzLCBhdXRob3JJZCwgaW5kZXgsIGRhdGE6IGRhdGFQcm9wIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgaXNTaWduZWRJbiB9ID0gZWxlbWVudHMudXNlcjtcblxuICAgIGNvbnN0IHsgZWRpdCwgZXhwYW5kZWQsIHRpdGxlLCBkYXRhIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXZcbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9XG4gICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5vbk1vdXNlRW50ZXJ9XG4gICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbk1vdXNlTGVhdmV9XG4gICAgICA+XG4gICAgICAgIDxDYXJkIHJhaXNlZD17dGhpcy5zdGF0ZS5ob3Zlcn0gY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICB0aXRsZT17XG4gICAgICAgICAgICAgIDxUaXRsZVxuICAgICAgICAgICAgICAgIHJlYWRPbmx5PXshZWRpdH1cbiAgICAgICAgICAgICAgICB2YWx1ZT17ZWRpdCA/IHRpdGxlIDogYCR7aW5kZXh9LiAke3RpdGxlfWB9XG4gICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2VUaXRsZX1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAvPlxuICAgICAgICAgIHtlZGl0IHx8IHR5cGVvZiBkYXRhUHJvcCAhPT0gJ3VuZGVmaW5lZCcgPyAoXG4gICAgICAgICAgICA8Q29sbGFwc2UgaW49e2V4cGFuZGVkfSB0aW1lb3V0PXsnYXV0byd9IHVubW91bnRPbkV4aXQ+XG4gICAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgICA8TWF5YXNoRWRpdG9yXG4gICAgICAgICAgICAgICAgICBlZGl0b3JTdGF0ZT17ZGF0YX1cbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgcmVhZE9ubHk9eyFlZGl0fVxuICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eydNb2R1bGUgQ29udGVudCd9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAgIDwvQ29sbGFwc2U+XG4gICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgPENhcmRBY3Rpb25zIGRpc2FibGVBY3Rpb25TcGFjaW5nPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9e2NsYXNzZXMuZmxleEdyb3d9IC8+XG4gICAgICAgICAgICB7aXNTaWduZWRJbiAmJlxuICAgICAgICAgICAgICBhdXRob3JJZCA9PT0gdXNlcklkICYmIChcbiAgICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkVkaXRcIlxuICAgICAgICAgICAgICAgICAgb25DbGljaz17ZWRpdCA/IHRoaXMub25TYXZlIDogdGhpcy5vbkVkaXR9XG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAge2VkaXQgPyA8U2F2ZUljb24gLz4gOiA8RWRpdEljb24gLz59XG4gICAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgICApfVxuICAgICAgICAgICAge2lzU2lnbmVkSW4gJiZcbiAgICAgICAgICAgICAgYXV0aG9ySWQgPT09IHVzZXJJZCAmJiAoXG4gICAgICAgICAgICAgICAgPEljb25CdXR0b24gb25DbGljaz17dGhpcy5vbkNsaWNrRGVsZXRlfSBhcmlhLWxhYmVsPVwiRGVsZXRlXCI+XG4gICAgICAgICAgICAgICAgICA8RGVsZXRlSWNvbiAvPlxuICAgICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgICAgKX1cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiRXhwYW5kXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc25hbWVzKGNsYXNzZXMuZXhwYW5kLCB7XG4gICAgICAgICAgICAgICAgICBbY2xhc3Nlcy5leHBhbmRPcGVuXTogZXhwYW5kZWQsXG4gICAgICAgICAgICAgICAgfSl9XG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5oYW5kbGVFeHBhbmRDbGlja31cbiAgICAgICAgICAgICAgICBhcmlhLWV4cGFuZGVkPXtleHBhbmRlZH1cbiAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiU2hvdyBtb3JlXCJcbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxFeHBhbmRNb3JlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgPC9DYXJkQWN0aW9ucz5cbiAgICAgICAgPC9DYXJkPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5Nb2R1bGUucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBhdXRob3JJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICBjb3Vyc2VJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICBtb2R1bGVJZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuXG4gIC8vIGluZGV4IGlzIG1vZHVsZSBudW1iZXIgaW4gY291cnNlLlxuICBpbmRleDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuXG4gIHRpdGxlOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gIC8vIGRlc2NyaXB0aW9uOiBQcm9wVHlwZXMuc3RyaW5nLCAvLyBubyB1c2luZyBpdCBmb3Igbm93LlxuICBkYXRhOiBQcm9wVHlwZXMub2JqZWN0LFxuXG4gIGFjdGlvblVwZGF0ZTogUHJvcFR5cGVzLmZ1bmMuaXNSZXF1aXJlZCxcbiAgYWN0aW9uRGVsZXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgZWxlbWVudHMgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvblVwZGF0ZSxcbiAgICAgIGFjdGlvbkRlbGV0ZSxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKE1vZHVsZSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVBhZ2UvTW9kdWxlLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncmVhY3QvbGliL1JlYWN0UHJvcFR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbi8vIGltcG9ydCBJY29uIGZyb20gJ21hdGVyaWFsLXVpL0ljb24nO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcblxuaW1wb3J0IFRpdGxlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9UaXRsZSc7XG5pbXBvcnQgRm9ybWF0Qm9sZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Qm9sZCc7XG5pbXBvcnQgRm9ybWF0SXRhbGljIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYyc7XG5pbXBvcnQgRm9ybWF0VW5kZXJsaW5lZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZCc7XG5pbXBvcnQgRm9ybWF0UXVvdGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlJztcbi8vIGltcG9ydCBGb3JtYXRDbGVhckljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q2xlYXInO1xuLy8gaW1wb3J0IEZvcm1hdENvbG9yRmlsbEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q29sb3JGaWxsJztcbi8vIGltcG9ydCBGb3JtYXRDb2xvclJlc2V0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRDb2xvclJlc2V0Jztcbi8vIGltcG9ydCBGb3JtYXRDb2xvclRleHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENvbG9yVGV4dCc7XG4vLyBpbXBvcnQgRm9ybWF0SW5kZW50RGVjcmVhc2VJY29uXG4vLyAgIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEluZGVudERlY3JlYXNlJztcbi8vIGltcG9ydCBGb3JtYXRJbmRlbnRJbmNyZWFzZUljb25cbi8vICAgZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SW5kZW50SW5jcmVhc2UnO1xuXG5pbXBvcnQgQ29kZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQ29kZSc7XG5cbmltcG9ydCBGb3JtYXRMaXN0TnVtYmVyZWRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZCc7XG5pbXBvcnQgRm9ybWF0TGlzdEJ1bGxldGVkSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQnO1xuXG5pbXBvcnQgRm9ybWF0QWxpZ25DZW50ZXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyJztcbmltcG9ydCBGb3JtYXRBbGlnbkxlZnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdCc7XG5pbXBvcnQgRm9ybWF0QWxpZ25SaWdodEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodCc7XG5pbXBvcnQgRm9ybWF0QWxpZ25KdXN0aWZ5SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnknO1xuXG5pbXBvcnQgQXR0YWNoRmlsZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZSc7XG5pbXBvcnQgSW5zZXJ0TGlua0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0TGluayc7XG5pbXBvcnQgSW5zZXJ0UGhvdG9JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvJztcbmltcG9ydCBJbnNlcnRFbW90aWNvbkljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24nO1xuaW1wb3J0IEluc2VydENvbW1lbnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQnO1xuXG4vLyBpbXBvcnQgVmVydGljYWxBbGlnblRvcEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnblRvcCc7XG4vLyBpbXBvcnQgVmVydGljYWxBbGlnbkJvdHRvbUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnbkJvdHRvbSc7XG4vLyBpbXBvcnQgVmVydGljYWxBbGlnbkNlbnRlckljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnbkNlbnRlcic7XG5cbi8vIGltcG9ydCBXcmFwVGV4dEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvV3JhcFRleHQnO1xuXG5pbXBvcnQgSGlnaGxpZ2h0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQnO1xuaW1wb3J0IEZ1bmN0aW9uc0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zJztcblxuaW1wb3J0IHtcbiAgRWRpdG9yLFxuICBSaWNoVXRpbHMsXG4gIC8vIEVudGl0eSxcbiAgLy8gQ29tcG9zaXRlRGVjb3JhdG9yLFxuICBBdG9taWNCbG9ja1V0aWxzLFxuICBFZGl0b3JTdGF0ZSxcbiAgLy8gICBjb252ZXJ0VG9SYXcsXG59IGZyb20gJ2RyYWZ0LWpzJztcblxuLy8gaW1wb3J0IHtcbi8vICAgaGFzaHRhZ1N0cmF0ZWd5LFxuLy8gICBIYXNodGFnU3BhbixcblxuLy8gICBoYW5kbGVTdHJhdGVneSxcbi8vICAgSGFuZGxlU3Bhbixcbi8vIH0gZnJvbSAnLi9jb21wb25lbnRzL0RlY29yYXRvcnMnO1xuXG5pbXBvcnQgQXRvbWljIGZyb20gJy4vY29tcG9uZW50cy9BdG9taWMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL0VkaXRvclN0eWxlcyc7XG5cbmltcG9ydCB7IEJsb2NrcywgSEFORExFRCwgTk9UX0hBTkRMRUQgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5cbi8qKlxuICogTWF5YXNoRWRpdG9yXG4gKi9cbmNsYXNzIE1heWFzaEVkaXRvciBleHRlbmRzIENvbXBvbmVudCB7XG4gIC8qKlxuICAgKiBNYXlhc2ggRWRpdG9yJ3MgcHJvcHR5cGVzIGFyZSBkZWZpbmVkIGhlcmUuXG4gICAqL1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAgIGVkaXRvclN0YXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG5cbiAgICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcblxuICAgIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuXG4gICAgLyoqXG4gICAgICogVGhpcyBhcGkgZnVuY3Rpb24gc2hvdWxkIGJlIGFwcGxpZWQgZm9yIGVuYWJsaW5nIHBob3RvIGFkZGluZ1xuICAgICAqIGZlYXR1cmUgaW4gTWF5YXNoIEVkaXRvci5cbiAgICAgKi9cbiAgICBhcGlQaG90b1VwbG9hZDogUHJvcFR5cGVzLmZ1bmMsXG4gIH07XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICByZWFkT25seTogdHJ1ZSxcbiAgICBwbGFjZWhvbGRlcjogJ1dyaXRlIEhlcmUuLi4nLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgLy8gdGhpcy5zdGF0ZSA9IHt9O1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGJlIHN1cHBsaWVkIHRvIERyYWZ0LmpzIEVkaXRvciB0byBoYW5kbGVcbiAgICAgKiBvbkNoYW5nZSBldmVudC5cbiAgICAgKiBAZnVuY3Rpb24gb25DaGFuZ2VcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZWRpdG9yU3RhdGVcbiAgICAgKi9cbiAgICB0aGlzLm9uQ2hhbmdlID0gKGVkaXRvclN0YXRlKSA9PiB7XG4gICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlKGVkaXRvclN0YXRlKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBmb2N1cyBldmVudCBpbiBEcmFmdC5qcyBFZGl0b3IuXG4gICAgICogQGZ1bmN0aW9uIGZvY3VzXG4gICAgICovXG4gICAgdGhpcy5mb2N1cyA9ICgpID0+IHRoaXMuZWRpdG9yTm9kZS5mb2N1cygpO1xuXG4gICAgdGhpcy5vblRhYiA9IHRoaXMub25UYWIuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uVGl0bGVDbGljayA9IHRoaXMub25UaXRsZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNvZGVDbGljayA9IHRoaXMub25Db2RlQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uUXVvdGVDbGljayA9IHRoaXMub25RdW90ZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrID0gdGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrID0gdGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkJvbGRDbGljayA9IHRoaXMub25Cb2xkQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uSXRhbGljQ2xpY2sgPSB0aGlzLm9uSXRhbGljQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uVW5kZXJMaW5lQ2xpY2sgPSB0aGlzLm9uVW5kZXJMaW5lQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uSGlnaGxpZ2h0Q2xpY2sgPSB0aGlzLm9uSGlnaGxpZ2h0Q2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUtleUNvbW1hbmQgPSB0aGlzLmhhbmRsZUtleUNvbW1hbmQuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uQ2xpY2tJbnNlcnRQaG90byA9IHRoaXMub25DbGlja0luc2VydFBob3RvLmJpbmQodGhpcyk7XG5cbiAgICAvLyB0aGlzLmJsb2NrUmVuZGVyZXJGbiA9IHRoaXMuYmxvY2tSZW5kZXJlckZuLmJpbmQodGhpcyk7XG5cbiAgICAvLyBjb25zdCBjb21wb3NpdGVEZWNvcmF0b3IgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKFtcbiAgICAvLyAgIHtcbiAgICAvLyAgICAgc3RyYXRlZ3k6IGhhbmRsZVN0cmF0ZWd5LFxuICAgIC8vICAgICBjb21wb25lbnQ6IEhhbmRsZVNwYW4sXG4gICAgLy8gICB9LFxuICAgIC8vICAge1xuICAgIC8vICAgICBzdHJhdGVneTogaGFzaHRhZ1N0cmF0ZWd5LFxuICAgIC8vICAgICBjb21wb25lbnQ6IEhhc2h0YWdTcGFuLFxuICAgIC8vICAgfSxcbiAgICAvLyBdKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBvblRhYigpIHdpbGwgaGFuZGxlIFRhYiBidXR0b24gcHJlc3MgZXZlbnQgaW4gRWRpdG9yLlxuICAgKiBAZnVuY3Rpb24gb25UYWJcbiAgICogQHBhcmFtIHtFdmVudH0gZVxuICAgKi9cbiAgb25UYWIoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMub25UYWIoZSwgZWRpdG9yU3RhdGUsIDQpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBUaXRsZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvblRpdGxlQ2xpY2tcbiAgICovXG4gIG9uVGl0bGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnaGVhZGVyLW9uZScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBDb2RlIEJ1dHRvbi5cbiAgICogQGZ1bmN0aW9uIG9uQ29kZUNsaWNrXG4gICAqL1xuICBvbkNvZGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnY29kZS1ibG9jaycpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBRdW90ZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvblF1b3RlQ2xpY2tcbiAgICovXG4gIG9uUXVvdGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnYmxvY2txdW90ZScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIHVwZGF0ZSBibG9jayB3aXRoIHVub3JkZXJlZCBsaXN0IGl0ZW1zXG4gICAqIEBmdW5jdGlvbiBvbkxpc3RCdWxsZXRlZENsaWNrXG4gICAqL1xuICBvbkxpc3RCdWxsZXRlZENsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVCbG9ja1R5cGUoXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICApO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIHVwZGF0ZSBibG9jayB3aXRoIG9yZGVyZWQgbGlzdCBpdGVtLlxuICAgKiBAZnVuY3Rpb24gb25MaXN0TnVtYmVyZWRDbGlja1xuICAgKi9cbiAgb25MaXN0TnVtYmVyZWRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICAnb3JkZXJlZC1saXN0LWl0ZW0nLFxuICAgICk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBzZWxlY3RlZCBjb250ZW50IEJvbGQgc3R5bGluZy5cbiAgICogQGZ1bmN0aW9uIG9uQm9sZENsaWNrXG4gICAqL1xuICBvbkJvbGRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdCT0xEJyk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBpdGFsaWMgc3R5bGluZyB0byBzZWxlY3RlZCBjb250ZW50LlxuICAgKiBAZnVuY3Rpb24gb25JdGFsaWNDbGlja1xuICAgKi9cbiAgb25JdGFsaWNDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdJVEFMSUMnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBnaXZlIHVuZGVybGluZSBzdHlsaW5nIHRvIHNlbGVjdGVkIGNvbnRlbnQuXG4gICAqIEBmdW5jdGlvbiBvblVuZGVyTGluZUNsaWNrXG4gICAqL1xuICBvblVuZGVyTGluZUNsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVJbmxpbmVTdHlsZShcbiAgICAgIGVkaXRvclN0YXRlLFxuICAgICAgJ1VOREVSTElORScsXG4gICAgKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoaWdobGlnaHQgc2VsZWN0ZWQgY29udGVudC5cbiAgICogQGZ1bmN0aW9uIG9uSGlnaGxpZ2h0Q2xpY2tcbiAgICovXG4gIG9uSGlnaGxpZ2h0Q2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnQ09ERScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICpcbiAgICovXG4gIG9uQ2xpY2tJbnNlcnRQaG90bygpIHtcbiAgICB0aGlzLnBob3RvLnZhbHVlID0gbnVsbDtcbiAgICB0aGlzLnBob3RvLmNsaWNrKCk7XG4gIH1cblxuICAvKipcbiAgICpcbiAgICovXG4gIG9uQ2hhbmdlSW5zZXJ0UGhvdG8gPSBhc3luYyAoZSkgPT4ge1xuICAgIC8vIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCBmaWxlID0gZS50YXJnZXQuZmlsZXNbMF07XG5cbiAgICBpZiAoZmlsZS50eXBlLmluZGV4T2YoJ2ltYWdlLycpID09PSAwKSB7XG4gICAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgZm9ybURhdGEuYXBwZW5kKCdwaG90bycsIGZpbGUpO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCB0aGlzLnByb3BzLmFwaVBob3RvVXBsb2FkKHtcbiAgICAgICAgZm9ybURhdGEsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIC8vIGhhbmRsZSBFcnJvclxuICAgICAgICBjb25zb2xlLmVycm9yKHN0YXR1c0NvZGUsIGVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCB7IHBob3RvVXJsOiBzcmMgfSA9IHBheWxvYWQ7XG5cbiAgICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICAgIGNvbnN0IGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgICBjb25zdCBjb250ZW50U3RhdGVXaXRoRW50aXR5ID0gY29udGVudFN0YXRlLmNyZWF0ZUVudGl0eShcbiAgICAgICAgQmxvY2tzLlBIT1RPLFxuICAgICAgICAnSU1NVVRBQkxFJyxcbiAgICAgICAgeyBzcmMgfSxcbiAgICAgICk7XG5cbiAgICAgIGNvbnN0IGVudGl0eUtleSA9IGNvbnRlbnRTdGF0ZVdpdGhFbnRpdHkuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKTtcblxuICAgICAgY29uc3QgbWlkZGxlRWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5zZXQoZWRpdG9yU3RhdGUsIHtcbiAgICAgICAgY3VycmVudENvbnRlbnQ6IGNvbnRlbnRTdGF0ZVdpdGhFbnRpdHksXG4gICAgICB9KTtcblxuICAgICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBBdG9taWNCbG9ja1V0aWxzLmluc2VydEF0b21pY0Jsb2NrKFxuICAgICAgICBtaWRkbGVFZGl0b3JTdGF0ZSxcbiAgICAgICAgZW50aXR5S2V5LFxuICAgICAgICAnICcsXG4gICAgICApO1xuXG4gICAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBnaXZlIGN1c3RvbSBoYW5kbGUgY29tbWFuZHMgdG8gRWRpdG9yLlxuICAgKiBAZnVuY3Rpb24gaGFuZGxlS2V5Q29tbWFuZFxuICAgKiBAcGFyYW0ge3N0cmluZ30gY29tbWFuZFxuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqL1xuICBoYW5kbGVLZXlDb21tYW5kKGNvbW1hbmQpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMuaGFuZGxlS2V5Q29tbWFuZChlZGl0b3JTdGF0ZSwgY29tbWFuZCk7XG5cbiAgICBpZiAobmV3RWRpdG9yU3RhdGUpIHtcbiAgICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICAgICAgcmV0dXJuIEhBTkRMRUQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIE5PVF9IQU5ETEVEO1xuICB9XG5cbiAgLyogZXNsaW50LWRpc2FibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuICBibG9ja1JlbmRlcmVyRm4oYmxvY2spIHtcbiAgICBzd2l0Y2ggKGJsb2NrLmdldFR5cGUoKSkge1xuICAgICAgY2FzZSBCbG9ja3MuQVRPTUlDOlxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNvbXBvbmVudDogQXRvbWljLFxuICAgICAgICAgIGVkaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgfTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG4gIC8qIGVzbGludC1lbmFibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuXG4gIC8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbiAgYmxvY2tTdHlsZUZuKGJsb2NrKSB7XG4gICAgc3dpdGNoIChibG9jay5nZXRUeXBlKCkpIHtcbiAgICAgIGNhc2UgJ2Jsb2NrcXVvdGUnOlxuICAgICAgICByZXR1cm4gJ1JpY2hFZGl0b3ItYmxvY2txdW90ZSc7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3Qge1xuICAgICAgY2xhc3NlcyxcbiAgICAgIHJlYWRPbmx5LFxuICAgICAgb25DaGFuZ2UsXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgIHBsYWNlaG9sZGVyLFxuICAgIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgLy8gQ3VzdG9tIG92ZXJyaWRlcyBmb3IgXCJjb2RlXCIgc3R5bGUuXG4gICAgY29uc3Qgc3R5bGVNYXAgPSB7XG4gICAgICBDT0RFOiB7XG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwgMCwgMCwgMC4wNSknLFxuICAgICAgICBmb250RmFtaWx5OiAnXCJJbmNvbnNvbGF0YVwiLCBcIk1lbmxvXCIsIFwiQ29uc29sYXNcIiwgbW9ub3NwYWNlJyxcbiAgICAgICAgZm9udFNpemU6IDE2LFxuICAgICAgICBwYWRkaW5nOiAyLFxuICAgICAgfSxcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7IXJlYWRPbmx5ID8gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHsgJ2VkaXRvci1jb250cm9scyc6ICcnIH0pfT5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiVGl0bGVcIiBpZD1cInRpdGxlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJUaXRsZVwiIG9uQ2xpY2s9e3RoaXMub25UaXRsZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8VGl0bGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQm9sZFwiIGlkPVwiYm9sZFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQm9sZFwiIG9uQ2xpY2s9e3RoaXMub25Cb2xkQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRCb2xkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkl0YWxpY1wiIGlkPVwiaXRhbGljXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJdGFsaWNcIiBvbkNsaWNrPXt0aGlzLm9uSXRhbGljQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRJdGFsaWMgLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJVbmRlcmxpbmVcIiBpZD1cInVuZGVybGluZVwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVbmRlcmxpbmVcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25VbmRlckxpbmVDbGlja31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRVbmRlcmxpbmVkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvZGVcIiBpZD1cImNvZGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkNvZGVcIiBvbkNsaWNrPXt0aGlzLm9uQ29kZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8Q29kZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJRdW90ZVwiIGlkPVwicXVvdGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlF1b3RlXCIgb25DbGljaz17dGhpcy5vblF1b3RlQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRRdW90ZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJVbm9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgIGlkPVwidW5vcmRlcmQtbGlzdFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIlVub3JkZXJlZCBMaXN0XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uTGlzdEJ1bGxldGVkQ2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0TGlzdEJ1bGxldGVkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIk9yZGVyZWQgTGlzdFwiIGlkPVwib3JkZXJlZC1saXN0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIk9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZvcm1hdExpc3ROdW1iZXJlZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBMZWZ0XCIgaWQ9XCJhbGlnbi1sZWZ0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBbGlnbiBMZWZ0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduTGVmdEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBDZW50ZXJcIiBpZD1cImFsaWduLWNlbnRlclwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gQ2VudGVyXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduQ2VudGVySWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkFsaWduIFJpZ2h0XCIgaWQ9XCJhbGlnbi1yaWdodFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gUmlnaHRcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0QWxpZ25SaWdodEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBSaWdodFwiIGlkPVwiYWxpZ24tcmlnaHRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIFJpZ2h0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduSnVzdGlmeUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBdHRhY2ggRmlsZVwiIGlkPVwiYXR0YWNoLWZpbGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkF0dGFjaCBGaWxlXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEF0dGFjaEZpbGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiSW5zZXJ0IExpbmtcIiBpZD1cImluc2VydC1saW5rXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgTGlua1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRMaW5rSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkluc2VydCBQaG90b1wiIGlkPVwiaW5zZXJ0LXBob3RvXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkluc2VydCBQaG90b1wiXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3R5cGVvZiB0aGlzLnByb3BzLmFwaVBob3RvVXBsb2FkICE9PSAnZnVuY3Rpb24nfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25DbGlja0luc2VydFBob3RvfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEluc2VydFBob3RvSWNvbiAvPlxuICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgdHlwZT1cImZpbGVcIlxuICAgICAgICAgICAgICAgICAgYWNjZXB0PVwiaW1hZ2UvanBlZ3xwbmd8Z2lmXCJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlSW5zZXJ0UGhvdG99XG4gICAgICAgICAgICAgICAgICByZWY9eyhwaG90bykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnBob3RvID0gcGhvdG87XG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgZGlzcGxheTogJ25vbmUnIH19XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJJbnNlcnQgRW1vdGljb25cIlxuICAgICAgICAgICAgICBpZD1cImluc2VydEUtZW1vdGljb25cIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IEVtb3RpY29uXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEluc2VydEVtb3RpY29uSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkluc2VydCBDb21tZW50XCJcbiAgICAgICAgICAgICAgaWQ9XCJpbnNlcnQtY29tbWVudFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgQ29tbWVudFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRDb21tZW50SWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkhpZ2hsaWdodCBUZXh0XCJcbiAgICAgICAgICAgICAgaWQ9XCJoaWdobGlnaHQtdGV4dFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkhpZ2hsaWdodCBUZXh0XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uSGlnaGxpZ2h0Q2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8SGlnaGxpZ2h0SWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkFkZCBGdW5jdGlvbnNcIlxuICAgICAgICAgICAgICBpZD1cImFkZC1mdW5jdGlvbnNcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWRkIEZ1bmN0aW9uc1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGdW5jdGlvbnNJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHsgJ2VkaXRvci1hcmVhJzogJycgfSl9PlxuICAgICAgICAgIDxFZGl0b3JcbiAgICAgICAgICAgIC8qIEJhc2ljcyAqL1xuXG4gICAgICAgICAgICBlZGl0b3JTdGF0ZT17ZWRpdG9yU3RhdGV9XG4gICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICAgICAgICAvKiBQcmVzZW50YXRpb24gKi9cblxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgLy8gdGV4dEFsaWdubWVudD1cImNlbnRlclwiXG5cbiAgICAgICAgICAgIC8vIHRleHREaXJlY3Rpb25hbGl0eT1cIkxUUlwiXG5cbiAgICAgICAgICAgIGJsb2NrUmVuZGVyZXJGbj17dGhpcy5ibG9ja1JlbmRlcmVyRm59XG4gICAgICAgICAgICBibG9ja1N0eWxlRm49e3RoaXMuYmxvY2tTdHlsZUZufVxuICAgICAgICAgICAgY3VzdG9tU3R5bGVNYXA9e3N0eWxlTWFwfVxuICAgICAgICAgICAgLy8gY3VzdG9tU3R5bGVGbj17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8qIEJlaGF2aW9yICovXG5cbiAgICAgICAgICAgIC8vIGF1dG9DYXBpdGFsaXplPVwic2VudGVuY2VzXCJcblxuICAgICAgICAgICAgLy8gYXV0b0NvbXBsZXRlPVwib2ZmXCJcblxuICAgICAgICAgICAgLy8gYXV0b0NvcnJlY3Q9XCJvZmZcIlxuXG4gICAgICAgICAgICByZWFkT25seT17cmVhZE9ubHl9XG4gICAgICAgICAgICBzcGVsbENoZWNrXG4gICAgICAgICAgICAvLyBzdHJpcFBhc3RlZFN0eWxlcz17ZmFsc2V9XG5cbiAgICAgICAgICAgIC8qIERPTSBhbmQgQWNjZXNzaWJpbGl0eSAqL1xuXG4gICAgICAgICAgICAvLyBlZGl0b3JLZXlcblxuICAgICAgICAgICAgLyogQ2FuY2VsYWJsZSBIYW5kbGVycyAqL1xuXG4gICAgICAgICAgICAvLyBoYW5kbGVSZXR1cm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICBoYW5kbGVLZXlDb21tYW5kPXt0aGlzLmhhbmRsZUtleUNvbW1hbmR9XG4gICAgICAgICAgICAvLyBoYW5kbGVCZWZvcmVJbnB1dD17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZVBhc3RlZFRleHQ9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVQYXN0ZWRGaWxlcz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZURyb3BwZWRGaWxlcz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZURyb3A9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBLZXkgSGFuZGxlcnMgKi9cblxuICAgICAgICAgICAgLy8gb25Fc2NhcGU9eygpID0+IHt9fVxuXG4gICAgICAgICAgICBvblRhYj17dGhpcy5vblRhYn1cbiAgICAgICAgICAgIC8vIG9uVXBBcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uUmlnaHRBcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uRG93bkFycm93PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gb25MZWZ0QXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBrZXlCaW5kaW5nRm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBNb3VzZSBFdmVudCAqL1xuXG4gICAgICAgICAgICAvLyBvbkZvY3VzPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gb25CbHVyPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogTWV0aG9kcyAqL1xuXG4gICAgICAgICAgICAvLyBmb2N1cz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGJsdXI9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBGb3IgUmVmZXJlbmNlICovXG5cbiAgICAgICAgICAgIHJlZj17KG5vZGUpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5lZGl0b3JOb2RlID0gbm9kZTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShNYXlhc2hFZGl0b3IpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvci5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCB7IEVkaXRvclN0YXRlLCBjb252ZXJ0RnJvbVJhdywgQ29tcG9zaXRlRGVjb3JhdG9yIH0gZnJvbSAnZHJhZnQtanMnO1xuXG5pbXBvcnQge1xuICBoYXNodGFnU3RyYXRlZ3ksXG4gIEhhc2h0YWdTcGFuLFxuICBoYW5kbGVTdHJhdGVneSxcbiAgSGFuZGxlU3Bhbixcbn0gZnJvbSAnLi9jb21wb25lbnRzL0RlY29yYXRvcnMnO1xuXG5jb25zdCBkZWZhdWx0RGVjb3JhdG9ycyA9IG5ldyBDb21wb3NpdGVEZWNvcmF0b3IoW1xuICB7XG4gICAgc3RyYXRlZ3k6IGhhbmRsZVN0cmF0ZWd5LFxuICAgIGNvbXBvbmVudDogSGFuZGxlU3BhbixcbiAgfSxcbiAge1xuICAgIHN0cmF0ZWd5OiBoYXNodGFnU3RyYXRlZ3ksXG4gICAgY29tcG9uZW50OiBIYXNodGFnU3BhbixcbiAgfSxcbl0pO1xuXG5leHBvcnQgY29uc3QgY3JlYXRlRWRpdG9yU3RhdGUgPSAoXG4gIGNvbnRlbnQgPSBudWxsLFxuICBkZWNvcmF0b3JzID0gZGVmYXVsdERlY29yYXRvcnMsXG4pID0+IHtcbiAgaWYgKGNvbnRlbnQgPT09IG51bGwpIHtcbiAgICByZXR1cm4gRWRpdG9yU3RhdGUuY3JlYXRlRW1wdHkoZGVjb3JhdG9ycyk7XG4gIH1cbiAgcmV0dXJuIEVkaXRvclN0YXRlLmNyZWF0ZVdpdGhDb250ZW50KGNvbnZlcnRGcm9tUmF3KGNvbnRlbnQpLCBkZWNvcmF0b3JzKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUVkaXRvclN0YXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvclN0YXRlLmpzIiwiLyoqXG4gKiBUaGlzIGZpbGUgY29udGFpbnMgYWxsIHRoZSBDU1MtaW4tSlMgc3R5bGVzIG9mIEVkaXRvciBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgJ0BnbG9iYWwnOiB7XG4gICAgJy5SaWNoRWRpdG9yLXJvb3QnOiB7XG4gICAgICBiYWNrZ3JvdW5kOiAnI2ZmZicsXG4gICAgICBib3JkZXI6ICcxcHggc29saWQgI2RkZCcsXG4gICAgICBmb250RmFtaWx5OiBcIidHZW9yZ2lhJywgc2VyaWZcIixcbiAgICAgIGZvbnRTaXplOiAnMTRweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgfSxcbiAgICAnLlJpY2hFZGl0b3ItZWRpdG9yJzoge1xuICAgICAgYm9yZGVyVG9wOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgICAgY3Vyc29yOiAndGV4dCcsXG4gICAgICBmb250U2l6ZTogJzE2cHgnLFxuICAgICAgbWFyZ2luVG9wOiAnMTBweCcsXG4gICAgfSxcbiAgICAnLnB1YmxpYy1EcmFmdEVkaXRvclBsYWNlaG9sZGVyLXJvb3QnOiB7XG4gICAgICBtYXJnaW46ICcwIC0xNXB4IC0xNXB4JyxcbiAgICAgIHBhZGRpbmc6ICcxNXB4JyxcbiAgICB9LFxuICAgICcucHVibGljLURyYWZ0RWRpdG9yLWNvbnRlbnQnOiB7XG4gICAgICBtYXJnaW46ICcwIC0xNXB4IC0xNXB4JyxcbiAgICAgIHBhZGRpbmc6ICcxNXB4JyxcbiAgICAgIC8vIG1pbkhlaWdodDogJzEwMHB4JyxcbiAgICB9LFxuICAgICcuUmljaEVkaXRvci1ibG9ja3F1b3RlJzoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiAnNXB4IHNvbGlkICNlZWUnLFxuICAgICAgYm9yZGVyTGVmdDogJzVweCBzb2xpZCAjZWVlJyxcbiAgICAgIGNvbG9yOiAnIzY2NicsXG4gICAgICBmb250RmFtaWx5OiBcIidIb2VmbGVyIFRleHQnLCAnR2VvcmdpYScsIHNlcmlmXCIsXG4gICAgICBmb250U3R5bGU6ICdpdGFsaWMnLFxuICAgICAgbWFyZ2luOiAnMTZweCAwJyxcbiAgICAgIHBhZGRpbmc6ICcxMHB4IDIwcHgnLFxuICAgIH0sXG4gICAgJy5wdWJsaWMtRHJhZnRTdHlsZURlZmF1bHQtcHJlJzoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSgwLCAwLCAwLCAwLjA1KScsXG4gICAgICBmb250RmFtaWx5OiBcIidJbmNvbnNvbGF0YScsICdNZW5sbycsICdDb25zb2xhcycsIG1vbm9zcGFjZVwiLFxuICAgICAgZm9udFNpemU6ICcxNnB4JyxcbiAgICAgIHBhZGRpbmc6ICcyMHB4JyxcbiAgICB9LFxuICB9LFxuICByb290OiB7XG4gICAgLy8gcGFkZGluZzogJzElJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdHlsZXMuanMiLCIvKipcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IEJsb2NrcyB9IGZyb20gJy4uL2NvbnN0YW50cyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcGhvdG86IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIC8vIEZpeCBhbiBpc3N1ZSB3aXRoIEZpcmVmb3ggcmVuZGVyaW5nIHZpZGVvIGNvbnRyb2xzXG4gICAgLy8gd2l0aCAncHJlLXdyYXAnIHdoaXRlLXNwYWNlXG4gICAgd2hpdGVTcGFjZTogJ2luaXRpYWwnLFxuICB9LFxufTtcblxuLyoqXG4gKlxuICogQGNsYXNzIEF0b21pYyAtIHRoaXMgUmVhY3QgY29tcG9uZW50IHdpbGwgYmUgdXNlZCB0byByZW5kZXIgQXRvbWljXG4gKiBjb21wb25lbnRzIG9mIERyYWZ0LmpzXG4gKlxuICogQHRvZG8gLSBjb25maWd1cmUgdGhpcyBmb3IgYXVkaW8uXG4gKiBAdG9kbyAtIGNvbmZpZ3VyZSB0aGlzIGZvciB2aWRlby5cbiAqL1xuY2xhc3MgQXRvbWljIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjb250ZW50U3RhdGU6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBibG9jazogUHJvcFR5cGVzLmFueS5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNvbnRlbnRTdGF0ZSwgYmxvY2sgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBlbnRpdHkgPSBjb250ZW50U3RhdGUuZ2V0RW50aXR5KGJsb2NrLmdldEVudGl0eUF0KDApKTtcbiAgICBjb25zdCB7IHNyYyB9ID0gZW50aXR5LmdldERhdGEoKTtcbiAgICBjb25zdCB0eXBlID0gZW50aXR5LmdldFR5cGUoKTtcblxuICAgIGlmICh0eXBlID09PSBCbG9ja3MuUEhPVE8pIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGltZyBhbHQ9eydhbHQnfSBzcmM9e3NyY30gc3R5bGU9e3N0eWxlcy5waG90b30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiA8ZGl2IC8+O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF0b21pYztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0F0b21pYy5qcyIsIi8qIGVzbGludC1kaXNhYmxlICovXG4vLyBlc2xpbnQgaXMgZGlzYWJsZWQgaGVyZSBmb3Igbm93LlxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBzdHlsZSBmcm9tICcuLi9zdHlsZSc7XG5cbmNvbnN0IEhBTkRMRV9SRUdFWCA9IC9cXEBbXFx3XSsvZztcbmNvbnN0IEhBU0hUQUdfUkVHRVggPSAvXFwjW1xcd1xcdTA1OTAtXFx1MDVmZl0rL2c7XG5cbmZ1bmN0aW9uIGZpbmRXaXRoUmVnZXgocmVnZXgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spIHtcbiAgY29uc3QgdGV4dCA9IGNvbnRlbnRCbG9jay5nZXRUZXh0KCk7XG4gIGxldCBtYXRjaEFycixcbiAgICBzdGFydDtcbiAgd2hpbGUgKChtYXRjaEFyciA9IHJlZ2V4LmV4ZWModGV4dCkpICE9PSBudWxsKSB7XG4gICAgc3RhcnQgPSBtYXRjaEFyci5pbmRleDtcbiAgICBjYWxsYmFjayhzdGFydCwgc3RhcnQgKyBtYXRjaEFyclswXS5sZW5ndGgpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBoYW5kbGVTdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrLCBjb250ZW50U3RhdGUpIHtcbiAgZmluZFdpdGhSZWdleChIQU5ETEVfUkVHRVgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFzaHRhZ1N0cmF0ZWd5KGNvbnRlbnRCbG9jaywgY2FsbGJhY2ssIGNvbnRlbnRTdGF0ZSkge1xuICBmaW5kV2l0aFJlZ2V4KEhBU0hUQUdfUkVHRVgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spO1xufVxuXG5leHBvcnQgY29uc3QgSGFuZGxlU3BhbiA9IHByb3BzID0+IDxzcGFuIHN0eWxlPXtzdHlsZS5oYW5kbGV9Pntwcm9wcy5jaGlsZHJlbn08L3NwYW4+O1xuXG5leHBvcnQgY29uc3QgSGFzaHRhZ1NwYW4gPSBwcm9wcyA9PiA8c3BhbiBzdHlsZT17c3R5bGUuaGFzaHRhZ30+e3Byb3BzLmNoaWxkcmVufTwvc3Bhbj47XG5cblxuZXhwb3J0IGRlZmF1bHQge1xuICBoYW5kbGVTdHJhdGVneSxcbiAgSGFuZGxlU3BhbixcblxuICBoYXNodGFnU3RyYXRlZ3ksXG4gIEhhc2h0YWdTcGFuLFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0RlY29yYXRvcnMuanMiLCIvKipcbiAqIFNvbWUgb2YgdGhlIGNvbnN0YW50cyB3aGljaCBhcmUgdXNlZCB0aHJvdWdob3V0IHRoaXMgcHJvamVjdCBpbnN0ZWFkIG9mXG4gKiBkaXJlY3RseSB1c2luZyBzdHJpbmcuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbi8qKlxuICogQGNvbnN0YW50IEJsb2Nrc1xuICovXG5leHBvcnQgY29uc3QgQmxvY2tzID0ge1xuICBVTlNUWUxFRDogJ3Vuc3R5bGVkJyxcbiAgUEFSQUdSQVBIOiAndW5zdHlsZWQnLFxuXG4gIEgxOiAnaGVhZGVyLW9uZScsXG4gIEgyOiAnaGVhZGVyLXR3bycsXG4gIEgzOiAnaGVhZGVyLXRocmVlJyxcbiAgSDQ6ICdoZWFkZXItZm91cicsXG4gIEg1OiAnaGVhZGVyLWZpdmUnLFxuICBINjogJ2hlYWRlci1zaXgnLFxuXG4gIE9MOiAnb3JkZXJlZC1saXN0LWl0ZW0nLFxuICBVTDogJ3Vub3JkZXJlZC1saXN0LWl0ZW0nLFxuXG4gIENPREU6ICdjb2RlLWJsb2NrJyxcblxuICBCTE9DS1FVT1RFOiAnYmxvY2txdW90ZScsXG5cbiAgQVRPTUlDOiAnYXRvbWljJyxcbiAgUEhPVE86ICdhdG9taWM6cGhvdG8nLFxuICBWSURFTzogJ2F0b21pYzp2aWRlbycsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBJbmxpbmVcbiAqL1xuZXhwb3J0IGNvbnN0IElubGluZSA9IHtcbiAgQk9MRDogJ0JPTEQnLFxuICBDT0RFOiAnQ09ERScsXG4gIElUQUxJQzogJ0lUQUxJQycsXG4gIFNUUklLRVRIUk9VR0g6ICdTVFJJS0VUSFJPVUdIJyxcbiAgVU5ERVJMSU5FOiAnVU5ERVJMSU5FJyxcbiAgSElHSExJR0hUOiAnSElHSExJR0hUJyxcbn07XG5cbi8qKlxuICogQGNvbnN0YW50IEVudGl0eVxuICovXG5leHBvcnQgY29uc3QgRW50aXR5ID0ge1xuICBMSU5LOiAnTElOSycsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBIWVBFUkxJTktcbiAqL1xuZXhwb3J0IGNvbnN0IEhZUEVSTElOSyA9ICdoeXBlcmxpbmsnO1xuXG4vKipcbiAqIENvbnN0YW50cyB0byBoYW5kbGUga2V5IGNvbW1hbmRzXG4gKi9cbmV4cG9ydCBjb25zdCBIQU5ETEVEID0gJ2hhbmRsZWQnO1xuZXhwb3J0IGNvbnN0IE5PVF9IQU5ETEVEID0gJ25vdF9oYW5kbGVkJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBCbG9ja3MsXG4gIElubGluZSxcbiAgRW50aXR5LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb25zdGFudHMuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgRWRpdG9yIGZyb20gJy4vRWRpdG9yJztcblxuZXhwb3J0IHsgTWF5YXNoRWRpdG9yIH0gZnJvbSAnLi9FZGl0b3InO1xuXG5leHBvcnQgeyBjcmVhdGVFZGl0b3JTdGF0ZSB9IGZyb20gJy4vRWRpdG9yU3RhdGUnO1xuXG5leHBvcnQgZGVmYXVsdCBFZGl0b3I7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvaW5kZXguanMiLCIvKiogQGZvcm1hdCAqL1xuXG5leHBvcnQgY29uc3Qgc3R5bGUgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAyMCxcbiAgICB3aWR0aDogNjAwLFxuICB9LFxuICBlZGl0b3I6IHtcbiAgICBib3JkZXI6ICcxcHggc29saWQgI2RkZCcsXG4gICAgY3Vyc29yOiAndGV4dCcsXG4gICAgZm9udFNpemU6IDE2LFxuICAgIG1pbkhlaWdodDogNDAsXG4gICAgcGFkZGluZzogMTAsXG4gIH0sXG4gIGJ1dHRvbjoge1xuICAgIG1hcmdpblRvcDogMTAsXG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgfSxcbiAgaGFuZGxlOiB7XG4gICAgY29sb3I6ICdyZ2JhKDk4LCAxNzcsIDI1NCwgMS4wKScsXG4gICAgZGlyZWN0aW9uOiAnbHRyJyxcbiAgICB1bmljb2RlQmlkaTogJ2JpZGktb3ZlcnJpZGUnLFxuICB9LFxuICBoYXNodGFnOiB7XG4gICAgY29sb3I6ICdyZ2JhKDk1LCAxODQsIDEzOCwgMS4wKScsXG4gIH0sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBzdHlsZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9zdHlsZS9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=