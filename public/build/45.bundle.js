webpackJsonp([45],{

/***/ "./node_modules/material-ui-icons/AttachFile.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/AttachFile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z' });

var AttachFile = function AttachFile(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

AttachFile = (0, _pure2.default)(AttachFile);
AttachFile.muiName = 'SvgIcon';

exports.default = AttachFile;

/***/ }),

/***/ "./node_modules/material-ui-icons/Code.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Code.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z' });

var Code = function Code(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Code = (0, _pure2.default)(Code);
Code.muiName = 'SvgIcon';

exports.default = Code;

/***/ }),

/***/ "./node_modules/material-ui-icons/Edit.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Edit.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z' });

var Edit = function Edit(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Edit = (0, _pure2.default)(Edit);
Edit.muiName = 'SvgIcon';

exports.default = Edit;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignCenter.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignCenter.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M7 15v2h10v-2H7zm-4 6h18v-2H3v2zm0-8h18v-2H3v2zm4-6v2h10V7H7zM3 3v2h18V3H3z' });

var FormatAlignCenter = function FormatAlignCenter(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignCenter = (0, _pure2.default)(FormatAlignCenter);
FormatAlignCenter.muiName = 'SvgIcon';

exports.default = FormatAlignCenter;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignJustify.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignJustify.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18V7H3v2zm0-6v2h18V3H3z' });

var FormatAlignJustify = function FormatAlignJustify(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignJustify = (0, _pure2.default)(FormatAlignJustify);
FormatAlignJustify.muiName = 'SvgIcon';

exports.default = FormatAlignJustify;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignLeft.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignLeft.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15 15H3v2h12v-2zm0-8H3v2h12V7zM3 13h18v-2H3v2zm0 8h18v-2H3v2zM3 3v2h18V3H3z' });

var FormatAlignLeft = function FormatAlignLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignLeft = (0, _pure2.default)(FormatAlignLeft);
FormatAlignLeft.muiName = 'SvgIcon';

exports.default = FormatAlignLeft;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignRight.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignRight.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm6-4h12v-2H9v2zm-6-4h18v-2H3v2zm6-4h12V7H9v2zM3 3v2h18V3H3z' });

var FormatAlignRight = function FormatAlignRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignRight = (0, _pure2.default)(FormatAlignRight);
FormatAlignRight.muiName = 'SvgIcon';

exports.default = FormatAlignRight;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatBold.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatBold.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.6 10.79c.97-.67 1.65-1.77 1.65-2.79 0-2.26-1.75-4-4-4H7v14h7.04c2.09 0 3.71-1.7 3.71-3.79 0-1.52-.86-2.82-2.15-3.42zM10 6.5h3c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-3v-3zm3.5 9H10v-3h3.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5z' });

var FormatBold = function FormatBold(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatBold = (0, _pure2.default)(FormatBold);
FormatBold.muiName = 'SvgIcon';

exports.default = FormatBold;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatItalic.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatItalic.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4z' });

var FormatItalic = function FormatItalic(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatItalic = (0, _pure2.default)(FormatItalic);
FormatItalic.muiName = 'SvgIcon';

exports.default = FormatItalic;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListBulleted.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListBulleted.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M4 10.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5-.67-1.5-1.5-1.5zm0-6c-.83 0-1.5.67-1.5 1.5S3.17 7.5 4 7.5 5.5 6.83 5.5 6 4.83 4.5 4 4.5zm0 12c-.83 0-1.5.68-1.5 1.5s.68 1.5 1.5 1.5 1.5-.68 1.5-1.5-.67-1.5-1.5-1.5zM7 19h14v-2H7v2zm0-6h14v-2H7v2zm0-8v2h14V5H7z' });

var FormatListBulleted = function FormatListBulleted(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListBulleted = (0, _pure2.default)(FormatListBulleted);
FormatListBulleted.muiName = 'SvgIcon';

exports.default = FormatListBulleted;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListNumbered.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListNumbered.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z' });

var FormatListNumbered = function FormatListNumbered(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListNumbered = (0, _pure2.default)(FormatListNumbered);
FormatListNumbered.muiName = 'SvgIcon';

exports.default = FormatListNumbered;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatQuote.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatQuote.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' });

var FormatQuote = function FormatQuote(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatQuote = (0, _pure2.default)(FormatQuote);
FormatQuote.muiName = 'SvgIcon';

exports.default = FormatQuote;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatUnderlined.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatUnderlined.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z' });

var FormatUnderlined = function FormatUnderlined(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatUnderlined = (0, _pure2.default)(FormatUnderlined);
FormatUnderlined.muiName = 'SvgIcon';

exports.default = FormatUnderlined;

/***/ }),

/***/ "./node_modules/material-ui-icons/Functions.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Functions.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 4H6v2l6.5 6L6 18v2h12v-3h-7l5-5-5-5h7z' });

var Functions = function Functions(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Functions = (0, _pure2.default)(Functions);
Functions.muiName = 'SvgIcon';

exports.default = Functions;

/***/ }),

/***/ "./node_modules/material-ui-icons/Highlight.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Highlight.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 14l3 3v5h6v-5l3-3V9H6zm5-12h2v3h-2zM3.5 5.875L4.914 4.46l2.12 2.122L5.62 7.997zm13.46.71l2.123-2.12 1.414 1.414L18.375 8z' });

var Highlight = function Highlight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Highlight = (0, _pure2.default)(Highlight);
Highlight.muiName = 'SvgIcon';

exports.default = Highlight;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertComment.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertComment.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var InsertComment = function InsertComment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertComment = (0, _pure2.default)(InsertComment);
InsertComment.muiName = 'SvgIcon';

exports.default = InsertComment;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertEmoticon.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertEmoticon.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });

var InsertEmoticon = function InsertEmoticon(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertEmoticon = (0, _pure2.default)(InsertEmoticon);
InsertEmoticon.muiName = 'SvgIcon';

exports.default = InsertEmoticon;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertLink.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertLink.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z' });

var InsertLink = function InsertLink(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertLink = (0, _pure2.default)(InsertLink);
InsertLink.muiName = 'SvgIcon';

exports.default = InsertLink;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui-icons/Save.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Save.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M17 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V7l-4-4zm-5 16c-1.66 0-3-1.34-3-3s1.34-3 3-3 3 1.34 3 3-1.34 3-3 3zm3-10H5V5h10v4z' });

var Save = function Save(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Save = (0, _pure2.default)(Save);
Save.muiName = 'SvgIcon';

exports.default = Save;

/***/ }),

/***/ "./node_modules/material-ui-icons/Title.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Title.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M5 4v3h5.5v12h3V7H19V4z' });

var Title = function Title(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Title = (0, _pure2.default)(Title);
Title.muiName = 'SvgIcon';

exports.default = Title;

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/api/users/update.js":
/*!****************************************!*\
  !*** ./src/client/api/users/update.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.token -
 * @param {string} payload.name -
 * @param {Object} payload.avatar -
 * @param {Object} payload.cover -
 * @param {Object} payload.resume -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var id = _ref2.id,
        token = _ref2.token,
        name = _ref2.name,
        avatar = _ref2.avatar,
        cover = _ref2.cover,
        resume = _ref2.resume;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ name: name, avatar: avatar, cover: cover, resume: resume })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/containers/Home/TabMyResume.js":
/*!***************************************************!*\
  !*** ./src/client/containers/Home/TabMyResume.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _Edit = __webpack_require__(/*! material-ui-icons/Edit */ "./node_modules/material-ui-icons/Edit.js");

var _Edit2 = _interopRequireDefault(_Edit);

var _Save = __webpack_require__(/*! material-ui-icons/Save */ "./node_modules/material-ui-icons/Save.js");

var _Save2 = _interopRequireDefault(_Save);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _mayashEditor = __webpack_require__(/*! ../../../lib/mayash-editor */ "./src/lib/mayash-editor/index.js");

var _mayashEditor2 = _interopRequireDefault(_mayashEditor);

var _update = __webpack_require__(/*! ../../api/users/update */ "./src/client/api/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  var _editButton;

  return {
    root: {
      padding: '1%'
    },
    flexGrow: {
      flex: '1 1 auto'
    },
    card: {
      borderRadius: '8px'
    },
    editButton: (_editButton = {
      position: 'fixed'
    }, (0, _defineProperty3.default)(_editButton, theme.breakpoints.up('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('xl'), {
      bottom: '40px',
      right: '40px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('md'), {
      bottom: '30px',
      right: '30px'
    }), (0, _defineProperty3.default)(_editButton, theme.breakpoints.down('sm'), {
      display: 'none',
      bottom: '25px',
      right: '25px'
    }), _editButton)
  };
}; /** @format */

var TabMyResume = function (_Component) {
  (0, _inherits3.default)(TabMyResume, _Component);

  function TabMyResume(props) {
    (0, _classCallCheck3.default)(this, TabMyResume);

    var _this = (0, _possibleConstructorReturn3.default)(this, (TabMyResume.__proto__ || (0, _getPrototypeOf2.default)(TabMyResume)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.state = {
      resume: (0, _mayashEditor.createEditorState)(props.user.resume)
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onEdit = _this.onEdit.bind(_this);
    _this.onSave = _this.onSave.bind(_this);
    return _this;
  }

  // This on change function is for MayashEditor


  (0, _createClass3.default)(TabMyResume, [{
    key: 'onChange',
    value: function onChange(resume) {
      this.setState({ resume: resume });
    }
  }, {
    key: 'onEdit',
    value: function onEdit() {
      this.setState({ edit: !this.state.edit });
    }
  }, {
    key: 'onSave',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var resume, _props$user, id, token, _ref2, statusCode, error;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                resume = this.state.resume;
                _props$user = this.props.user, id = _props$user.id, token = _props$user.token;
                _context.next = 4;
                return (0, _update2.default)({
                  id: id,
                  token: token,
                  resume: (0, _draftJs.convertToRaw)(resume.getCurrentContent())
                });

              case 4:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;

                if (!(statusCode !== 200)) {
                  _context.next = 10;
                  break;
                }

                // error handle
                console.error(error);
                return _context.abrupt('return');

              case 10:

                this.props.actionUserUpdate({
                  id: id,
                  resume: (0, _draftJs.convertToRaw)(resume.getCurrentContent())
                });

                this.setState({ edit: false });

              case 12:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function onSave() {
        return _ref.apply(this, arguments);
      }

      return onSave;
    }()
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var _state = this.state,
          hover = _state.hover,
          edit = _state.edit,
          resume = _state.resume;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 10, md: 8, lg: 6, xl: 6 },
          _react2.default.createElement(
            'div',
            {
              onMouseEnter: this.onMouseEnter,
              onMouseLeave: this.onMouseLeave
            },
            _react2.default.createElement(
              _Card2.default,
              { raised: hover, className: classes.card },
              _react2.default.createElement(
                _Card.CardContent,
                null,
                _react2.default.createElement(_mayashEditor2.default, {
                  editorState: resume,
                  onChange: this.onChange,
                  readOnly: !edit
                })
              )
            ),
            _react2.default.createElement(
              _Button2.default,
              {
                fab: true,
                color: 'accent',
                'aria-label': 'Edit',
                className: classes.editButton,
                onClick: edit ? this.onSave : this.onEdit
              },
              edit ? _react2.default.createElement(_Save2.default, null) : _react2.default.createElement(_Edit2.default, null)
            )
          )
        )
      );
    }
  }]);
  return TabMyResume;
}(_react.Component);

TabMyResume.propTypes = {
  /**
   * TabMyResume style object
   */
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.object.isRequired,

  actionUserUpdate: _propTypes2.default.func.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(TabMyResume);

/***/ }),

/***/ "./src/lib/mayash-editor/Editor.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-editor/Editor.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! material-ui-icons/Title */ "./node_modules/material-ui-icons/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _FormatBold = __webpack_require__(/*! material-ui-icons/FormatBold */ "./node_modules/material-ui-icons/FormatBold.js");

var _FormatBold2 = _interopRequireDefault(_FormatBold);

var _FormatItalic = __webpack_require__(/*! material-ui-icons/FormatItalic */ "./node_modules/material-ui-icons/FormatItalic.js");

var _FormatItalic2 = _interopRequireDefault(_FormatItalic);

var _FormatUnderlined = __webpack_require__(/*! material-ui-icons/FormatUnderlined */ "./node_modules/material-ui-icons/FormatUnderlined.js");

var _FormatUnderlined2 = _interopRequireDefault(_FormatUnderlined);

var _FormatQuote = __webpack_require__(/*! material-ui-icons/FormatQuote */ "./node_modules/material-ui-icons/FormatQuote.js");

var _FormatQuote2 = _interopRequireDefault(_FormatQuote);

var _Code = __webpack_require__(/*! material-ui-icons/Code */ "./node_modules/material-ui-icons/Code.js");

var _Code2 = _interopRequireDefault(_Code);

var _FormatListNumbered = __webpack_require__(/*! material-ui-icons/FormatListNumbered */ "./node_modules/material-ui-icons/FormatListNumbered.js");

var _FormatListNumbered2 = _interopRequireDefault(_FormatListNumbered);

var _FormatListBulleted = __webpack_require__(/*! material-ui-icons/FormatListBulleted */ "./node_modules/material-ui-icons/FormatListBulleted.js");

var _FormatListBulleted2 = _interopRequireDefault(_FormatListBulleted);

var _FormatAlignCenter = __webpack_require__(/*! material-ui-icons/FormatAlignCenter */ "./node_modules/material-ui-icons/FormatAlignCenter.js");

var _FormatAlignCenter2 = _interopRequireDefault(_FormatAlignCenter);

var _FormatAlignLeft = __webpack_require__(/*! material-ui-icons/FormatAlignLeft */ "./node_modules/material-ui-icons/FormatAlignLeft.js");

var _FormatAlignLeft2 = _interopRequireDefault(_FormatAlignLeft);

var _FormatAlignRight = __webpack_require__(/*! material-ui-icons/FormatAlignRight */ "./node_modules/material-ui-icons/FormatAlignRight.js");

var _FormatAlignRight2 = _interopRequireDefault(_FormatAlignRight);

var _FormatAlignJustify = __webpack_require__(/*! material-ui-icons/FormatAlignJustify */ "./node_modules/material-ui-icons/FormatAlignJustify.js");

var _FormatAlignJustify2 = _interopRequireDefault(_FormatAlignJustify);

var _AttachFile = __webpack_require__(/*! material-ui-icons/AttachFile */ "./node_modules/material-ui-icons/AttachFile.js");

var _AttachFile2 = _interopRequireDefault(_AttachFile);

var _InsertLink = __webpack_require__(/*! material-ui-icons/InsertLink */ "./node_modules/material-ui-icons/InsertLink.js");

var _InsertLink2 = _interopRequireDefault(_InsertLink);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _InsertEmoticon = __webpack_require__(/*! material-ui-icons/InsertEmoticon */ "./node_modules/material-ui-icons/InsertEmoticon.js");

var _InsertEmoticon2 = _interopRequireDefault(_InsertEmoticon);

var _InsertComment = __webpack_require__(/*! material-ui-icons/InsertComment */ "./node_modules/material-ui-icons/InsertComment.js");

var _InsertComment2 = _interopRequireDefault(_InsertComment);

var _Highlight = __webpack_require__(/*! material-ui-icons/Highlight */ "./node_modules/material-ui-icons/Highlight.js");

var _Highlight2 = _interopRequireDefault(_Highlight);

var _Functions = __webpack_require__(/*! material-ui-icons/Functions */ "./node_modules/material-ui-icons/Functions.js");

var _Functions2 = _interopRequireDefault(_Functions);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Atomic = __webpack_require__(/*! ./components/Atomic */ "./src/lib/mayash-editor/components/Atomic.js");

var _Atomic2 = _interopRequireDefault(_Atomic);

var _EditorStyles = __webpack_require__(/*! ./EditorStyles */ "./src/lib/mayash-editor/EditorStyles.js");

var _EditorStyles2 = _interopRequireDefault(_EditorStyles);

var _constants = __webpack_require__(/*! ./constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * MayashEditor
 */


// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

var MayashEditor = function (_Component) {
  (0, _inherits3.default)(MayashEditor, _Component);

  /**
   * Mayash Editor's proptypes are defined here.
   */
  function MayashEditor() {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, MayashEditor);

    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    var _this = (0, _possibleConstructorReturn3.default)(this, (MayashEditor.__proto__ || (0, _getPrototypeOf2.default)(MayashEditor)).call(this));

    _this.onChangeInsertPhoto = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _ref2, statusCode, error, payload, src, editorState, contentState, contentStateWithEntity, entityKey, middleEditorState, newEditorState;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // e.preventDefault();
                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 21;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _context.next = 6;
                return _this.props.apiPhotoUpload({
                  formData: formData
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle Error
                console.error(statusCode, error);
                return _context.abrupt('return');

              case 13:
                src = payload.photoUrl;
                editorState = _this.props.editorState;
                contentState = editorState.getCurrentContent();
                contentStateWithEntity = contentState.createEntity(_constants.Blocks.PHOTO, 'IMMUTABLE', { src: src });
                entityKey = contentStateWithEntity.getLastCreatedEntityKey();
                middleEditorState = _draftJs.EditorState.set(editorState, {
                  currentContent: contentStateWithEntity
                });
                newEditorState = _draftJs.AtomicBlockUtils.insertAtomicBlock(middleEditorState, entityKey, ' ');


                _this.onChange(newEditorState);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.onChange = function (editorState) {
      _this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    _this.focus = function () {
      return _this.editorNode.focus();
    };

    _this.onTab = _this.onTab.bind(_this);
    _this.onTitleClick = _this.onTitleClick.bind(_this);
    _this.onCodeClick = _this.onCodeClick.bind(_this);
    _this.onQuoteClick = _this.onQuoteClick.bind(_this);
    _this.onListBulletedClick = _this.onListBulletedClick.bind(_this);
    _this.onListNumberedClick = _this.onListNumberedClick.bind(_this);
    _this.onBoldClick = _this.onBoldClick.bind(_this);
    _this.onItalicClick = _this.onItalicClick.bind(_this);
    _this.onUnderLineClick = _this.onUnderLineClick.bind(_this);
    _this.onHighlightClick = _this.onHighlightClick.bind(_this);
    _this.handleKeyCommand = _this.handleKeyCommand.bind(_this);
    _this.onClickInsertPhoto = _this.onClickInsertPhoto.bind(_this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
    return _this;
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */


  (0, _createClass3.default)(MayashEditor, [{
    key: 'onTab',
    value: function onTab(e) {
      e.preventDefault();
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.onTab(e, editorState, 4);

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Title Button.
     * @function onTitleClick
     */

  }, {
    key: 'onTitleClick',
    value: function onTitleClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'header-one');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Code Button.
     * @function onCodeClick
     */

  }, {
    key: 'onCodeClick',
    value: function onCodeClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'code-block');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Quote Button.
     * @function onQuoteClick
     */

  }, {
    key: 'onQuoteClick',
    value: function onQuoteClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'blockquote');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with unordered list items
     * @function onListBulletedClick
     */

  }, {
    key: 'onListBulletedClick',
    value: function onListBulletedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'unordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with ordered list item.
     * @function onListNumberedClick
     */

  }, {
    key: 'onListNumberedClick',
    value: function onListNumberedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'ordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will give selected content Bold styling.
     * @function onBoldClick
     */

  }, {
    key: 'onBoldClick',
    value: function onBoldClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'BOLD');

      this.onChange(newEditorState);
    }

    /**
     * This function will give italic styling to selected content.
     * @function onItalicClick
     */

  }, {
    key: 'onItalicClick',
    value: function onItalicClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'ITALIC');

      this.onChange(newEditorState);
    }

    /**
     * This function will give underline styling to selected content.
     * @function onUnderLineClick
     */

  }, {
    key: 'onUnderLineClick',
    value: function onUnderLineClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');

      this.onChange(newEditorState);
    }

    /**
     * This function will highlight selected content.
     * @function onHighlightClick
     */

  }, {
    key: 'onHighlightClick',
    value: function onHighlightClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'CODE');

      this.onChange(newEditorState);
    }

    /**
     *
     */

  }, {
    key: 'onClickInsertPhoto',
    value: function onClickInsertPhoto() {
      this.photo.value = null;
      this.photo.click();
    }

    /**
     *
     */

  }, {
    key: 'handleKeyCommand',


    /**
     * This function will give custom handle commands to Editor.
     * @function handleKeyCommand
     * @param {string} command
     * @return {string}
     */
    value: function handleKeyCommand(command) {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.handleKeyCommand(editorState, command);

      if (newEditorState) {
        this.onChange(newEditorState);
        return _constants.HANDLED;
      }

      return _constants.NOT_HANDLED;
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockRendererFn',
    value: function blockRendererFn(block) {
      switch (block.getType()) {
        case _constants.Blocks.ATOMIC:
          return {
            component: _Atomic2.default,
            editable: false
          };

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockStyleFn',
    value: function blockStyleFn(block) {
      switch (block.getType()) {
        case 'blockquote':
          return 'RichEditor-blockquote';

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          readOnly = _props.readOnly,
          onChange = _props.onChange,
          editorState = _props.editorState,
          placeholder = _props.placeholder;

      // Custom overrides for "code" style.

      var styleMap = {
        CODE: {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
          fontSize: 16,
          padding: 2
        }
      };

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        !readOnly ? _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-controls': '' }) },
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Title', id: 'title', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Title', onClick: this.onTitleClick },
              _react2.default.createElement(_Title2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Bold', id: 'bold', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Bold', onClick: this.onBoldClick },
              _react2.default.createElement(_FormatBold2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Italic', id: 'italic', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Italic', onClick: this.onItalicClick },
              _react2.default.createElement(_FormatItalic2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Underline', id: 'underline', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Underline',
                onClick: this.onUnderLineClick
              },
              _react2.default.createElement(_FormatUnderlined2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Code', id: 'code', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Code', onClick: this.onCodeClick },
              _react2.default.createElement(_Code2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Quote', id: 'quote', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Quote', onClick: this.onQuoteClick },
              _react2.default.createElement(_FormatQuote2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Unordered List',
              id: 'unorderd-list',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Unordered List',
                onClick: this.onListBulletedClick
              },
              _react2.default.createElement(_FormatListBulleted2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Ordered List', id: 'ordered-list', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Ordered List',
                onClick: this.onListNumberedClick
              },
              _react2.default.createElement(_FormatListNumbered2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Left', id: 'align-left', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Left', disabled: true },
              _react2.default.createElement(_FormatAlignLeft2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Center', id: 'align-center', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Center', disabled: true },
              _react2.default.createElement(_FormatAlignCenter2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignRight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignJustify2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Attach File', id: 'attach-file', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Attach File', disabled: true },
              _react2.default.createElement(_AttachFile2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Link', id: 'insert-link', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Link', disabled: true },
              _react2.default.createElement(_InsertLink2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Photo', id: 'insert-photo', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Insert Photo',
                disabled: typeof this.props.apiPhotoUpload !== 'function',
                onClick: this.onClickInsertPhoto
              },
              _react2.default.createElement(_InsertPhoto2.default, null),
              _react2.default.createElement('input', {
                type: 'file',
                accept: 'image/jpeg|png|gif',
                onChange: this.onChangeInsertPhoto,
                ref: function ref(photo) {
                  _this3.photo = photo;
                },
                style: { display: 'none' }
              })
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Emoticon',
              id: 'insertE-emoticon',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Emoticon', disabled: true },
              _react2.default.createElement(_InsertEmoticon2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Comment',
              id: 'insert-comment',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Comment', disabled: true },
              _react2.default.createElement(_InsertComment2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Highlight Text',
              id: 'highlight-text',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Highlight Text',
                onClick: this.onHighlightClick
              },
              _react2.default.createElement(_Highlight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Add Functions',
              id: 'add-functions',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Add Functions', disabled: true },
              _react2.default.createElement(_Functions2.default, null)
            )
          )
        ) : null,
        _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-area': '' }) },
          _react2.default.createElement(_draftJs.Editor
          /* Basics */

          , { editorState: editorState,
            onChange: onChange
            /* Presentation */

            , placeholder: placeholder
            // textAlignment="center"

            // textDirectionality="LTR"

            , blockRendererFn: this.blockRendererFn,
            blockStyleFn: this.blockStyleFn,
            customStyleMap: styleMap
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            , readOnly: readOnly,
            spellCheck: true
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            , handleKeyCommand: this.handleKeyCommand
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            , onTab: this.onTab
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            , ref: function ref(node) {
              _this3.editorNode = node;
            }
          })
        )
      );
    }
  }]);
  return MayashEditor;
}(_react.Component);

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

// import Icon from 'material-ui/Icon';
/** @format */

MayashEditor.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  editorState: _ReactPropTypes2.default.object.isRequired,
  onChange: _ReactPropTypes2.default.func.isRequired,

  placeholder: _ReactPropTypes2.default.string,

  readOnly: _ReactPropTypes2.default.bool.isRequired,

  /**
   * This api function should be applied for enabling photo adding
   * feature in Mayash Editor.
   */
  apiPhotoUpload: _ReactPropTypes2.default.func
};
MayashEditor.defaultProps = {
  readOnly: true,
  placeholder: 'Write Here...'
};
exports.default = (0, _withStyles2.default)(_EditorStyles2.default)(MayashEditor);

/***/ }),

/***/ "./src/lib/mayash-editor/EditorState.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/EditorState.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = undefined;

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Decorators = __webpack_require__(/*! ./components/Decorators */ "./src/lib/mayash-editor/components/Decorators.js");

/** @format */

var defaultDecorators = new _draftJs.CompositeDecorator([{
  strategy: _Decorators.handleStrategy,
  component: _Decorators.HandleSpan
}, {
  strategy: _Decorators.hashtagStrategy,
  component: _Decorators.HashtagSpan
}]);

var createEditorState = exports.createEditorState = function createEditorState() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var decorators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDecorators;

  if (content === null) {
    return _draftJs.EditorState.createEmpty(decorators);
  }
  return _draftJs.EditorState.createWithContent((0, _draftJs.convertFromRaw)(content), decorators);
};

exports.default = createEditorState;

/***/ }),

/***/ "./src/lib/mayash-editor/EditorStyles.js":
/*!***********************************************!*\
  !*** ./src/lib/mayash-editor/EditorStyles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the CSS-in-JS styles of Editor component.
 *
 * @format
 */

exports.default = {
  '@global': {
    '.RichEditor-root': {
      background: '#fff',
      border: '1px solid #ddd',
      fontFamily: "'Georgia', serif",
      fontSize: '14px',
      padding: '15px'
    },
    '.RichEditor-editor': {
      borderTop: '1px solid #ddd',
      cursor: 'text',
      fontSize: '16px',
      marginTop: '10px'
    },
    '.public-DraftEditorPlaceholder-root': {
      margin: '0 -15px -15px',
      padding: '15px'
    },
    '.public-DraftEditor-content': {
      margin: '0 -15px -15px',
      padding: '15px'
      // minHeight: '100px',
    },
    '.RichEditor-blockquote': {
      backgroundColor: '5px solid #eee',
      borderLeft: '5px solid #eee',
      color: '#666',
      fontFamily: "'Hoefler Text', 'Georgia', serif",
      fontStyle: 'italic',
      margin: '16px 0',
      padding: '10px 20px'
    },
    '.public-DraftStyleDefault-pre': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: "'Inconsolata', 'Menlo', 'Consolas', monospace",
      fontSize: '16px',
      padding: '20px'
    }
  },
  root: {
    // padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/***/ }),

/***/ "./src/lib/mayash-editor/components/Atomic.js":
/*!****************************************************!*\
  !*** ./src/lib/mayash-editor/components/Atomic.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = __webpack_require__(/*! ../constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial'
  }
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
/**
 *
 * @format
 */

var Atomic = function (_Component) {
  (0, _inherits3.default)(Atomic, _Component);

  function Atomic(props) {
    (0, _classCallCheck3.default)(this, Atomic);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Atomic.__proto__ || (0, _getPrototypeOf2.default)(Atomic)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Atomic, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          contentState = _props.contentState,
          block = _props.block;


      var entity = contentState.getEntity(block.getEntityAt(0));

      var _entity$getData = entity.getData(),
          src = _entity$getData.src;

      var type = entity.getType();

      if (type === _constants.Blocks.PHOTO) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('img', { alt: 'alt', src: src, style: styles.photo })
        );
      }

      return _react2.default.createElement('div', null);
    }
  }]);
  return Atomic;
}(_react.Component);

Atomic.propTypes = {
  contentState: _propTypes2.default.object.isRequired,
  block: _propTypes2.default.any.isRequired
};
exports.default = Atomic;

/***/ }),

/***/ "./src/lib/mayash-editor/components/Decorators.js":
/*!********************************************************!*\
  !*** ./src/lib/mayash-editor/components/Decorators.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashtagSpan = exports.HandleSpan = undefined;
exports.handleStrategy = handleStrategy;
exports.hashtagStrategy = hashtagStrategy;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _style = __webpack_require__(/*! ../style */ "./src/lib/mayash-editor/style/index.js");

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
// eslint is disabled here for now.
var HANDLE_REGEX = /\@[\w]+/g;
var HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function findWithRegex(regex, contentBlock, callback) {
  var text = contentBlock.getText();
  var matchArr = void 0,
      start = void 0;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

var HandleSpan = exports.HandleSpan = function HandleSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.handle },
    props.children
  );
};

var HashtagSpan = exports.HashtagSpan = function HashtagSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.hashtag },
    props.children
  );
};

exports.default = {
  handleStrategy: handleStrategy,
  HandleSpan: HandleSpan,

  hashtagStrategy: hashtagStrategy,
  HashtagSpan: HashtagSpan
};

/***/ }),

/***/ "./src/lib/mayash-editor/constants.js":
/*!********************************************!*\
  !*** ./src/lib/mayash-editor/constants.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
var Blocks = exports.Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video'
};

/**
 * @constant Inline
 */
var Inline = exports.Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT'
};

/**
 * @constant Entity
 */
var Entity = exports.Entity = {
  LINK: 'LINK'
};

/**
 * @constant HYPERLINK
 */
var HYPERLINK = exports.HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
var HANDLED = exports.HANDLED = 'handled';
var NOT_HANDLED = exports.NOT_HANDLED = 'not_handled';

exports.default = {
  Blocks: Blocks,
  Inline: Inline,
  Entity: Entity
};

/***/ }),

/***/ "./src/lib/mayash-editor/index.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-editor/index.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = exports.MayashEditor = undefined;

var _Editor = __webpack_require__(/*! ./Editor */ "./src/lib/mayash-editor/Editor.js");

Object.defineProperty(exports, 'MayashEditor', {
  enumerable: true,
  get: function get() {
    return _Editor.MayashEditor;
  }
});

var _EditorState = __webpack_require__(/*! ./EditorState */ "./src/lib/mayash-editor/EditorState.js");

Object.defineProperty(exports, 'createEditorState', {
  enumerable: true,
  get: function get() {
    return _EditorState.createEditorState;
  }
});

var _Editor2 = _interopRequireDefault(_Editor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Editor2.default;

/***/ }),

/***/ "./src/lib/mayash-editor/style/index.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/style/index.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @format */

var style = exports.style = {
  root: {
    padding: 20,
    width: 600
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10
  },
  button: {
    marginTop: 10,
    textAlign: 'center'
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override'
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)'
  }
};

exports.default = style;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRWRpdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnblJpZ2h0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvU2F2ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3VzZXJzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvSG9tZS9UYWJNeVJlc3VtZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3R5bGVzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0F0b21pYy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9EZWNvcmF0b3JzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9zdHlsZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJpZCIsInRva2VuIiwibmFtZSIsImF2YXRhciIsImNvdmVyIiwicmVzdW1lIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJib2R5IiwicmVzIiwic3RhdHVzIiwic3RhdHVzVGV4dCIsInN0YXR1c0NvZGUiLCJlcnJvciIsImpzb24iLCJjb25zb2xlIiwidXBkYXRlIiwic3R5bGVzIiwidGhlbWUiLCJyb290IiwicGFkZGluZyIsImZsZXhHcm93IiwiZmxleCIsImNhcmQiLCJib3JkZXJSYWRpdXMiLCJlZGl0QnV0dG9uIiwicG9zaXRpb24iLCJicmVha3BvaW50cyIsInVwIiwiYm90dG9tIiwicmlnaHQiLCJkb3duIiwiZGlzcGxheSIsIlRhYk15UmVzdW1lIiwicHJvcHMiLCJvbk1vdXNlRW50ZXIiLCJzZXRTdGF0ZSIsImhvdmVyIiwib25Nb3VzZUxlYXZlIiwic3RhdGUiLCJ1c2VyIiwib25DaGFuZ2UiLCJiaW5kIiwib25FZGl0Iiwib25TYXZlIiwiZWRpdCIsImdldEN1cnJlbnRDb250ZW50IiwiYWN0aW9uVXNlclVwZGF0ZSIsImNsYXNzZXMiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwiZnVuYyIsIk1heWFzaEVkaXRvciIsIm9uQ2hhbmdlSW5zZXJ0UGhvdG8iLCJlIiwiZmlsZSIsInRhcmdldCIsImZpbGVzIiwidHlwZSIsImluZGV4T2YiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiYXBpUGhvdG9VcGxvYWQiLCJwYXlsb2FkIiwic3JjIiwicGhvdG9VcmwiLCJlZGl0b3JTdGF0ZSIsImNvbnRlbnRTdGF0ZSIsImNvbnRlbnRTdGF0ZVdpdGhFbnRpdHkiLCJjcmVhdGVFbnRpdHkiLCJQSE9UTyIsImVudGl0eUtleSIsImdldExhc3RDcmVhdGVkRW50aXR5S2V5IiwibWlkZGxlRWRpdG9yU3RhdGUiLCJzZXQiLCJjdXJyZW50Q29udGVudCIsIm5ld0VkaXRvclN0YXRlIiwiaW5zZXJ0QXRvbWljQmxvY2siLCJmb2N1cyIsImVkaXRvck5vZGUiLCJvblRhYiIsIm9uVGl0bGVDbGljayIsIm9uQ29kZUNsaWNrIiwib25RdW90ZUNsaWNrIiwib25MaXN0QnVsbGV0ZWRDbGljayIsIm9uTGlzdE51bWJlcmVkQ2xpY2siLCJvbkJvbGRDbGljayIsIm9uSXRhbGljQ2xpY2siLCJvblVuZGVyTGluZUNsaWNrIiwib25IaWdobGlnaHRDbGljayIsImhhbmRsZUtleUNvbW1hbmQiLCJvbkNsaWNrSW5zZXJ0UGhvdG8iLCJwcmV2ZW50RGVmYXVsdCIsInRvZ2dsZUJsb2NrVHlwZSIsInRvZ2dsZUlubGluZVN0eWxlIiwicGhvdG8iLCJ2YWx1ZSIsImNsaWNrIiwiY29tbWFuZCIsImJsb2NrIiwiZ2V0VHlwZSIsIkFUT01JQyIsImNvbXBvbmVudCIsImVkaXRhYmxlIiwicmVhZE9ubHkiLCJwbGFjZWhvbGRlciIsInN0eWxlTWFwIiwiQ09ERSIsImJhY2tncm91bmRDb2xvciIsImZvbnRGYW1pbHkiLCJmb250U2l6ZSIsImJsb2NrUmVuZGVyZXJGbiIsImJsb2NrU3R5bGVGbiIsIm5vZGUiLCJzdHJpbmciLCJib29sIiwiZGVmYXVsdFByb3BzIiwiZGVmYXVsdERlY29yYXRvcnMiLCJzdHJhdGVneSIsImNyZWF0ZUVkaXRvclN0YXRlIiwiY29udGVudCIsImRlY29yYXRvcnMiLCJjcmVhdGVFbXB0eSIsImNyZWF0ZVdpdGhDb250ZW50IiwiYmFja2dyb3VuZCIsImJvcmRlciIsImJvcmRlclRvcCIsImN1cnNvciIsIm1hcmdpblRvcCIsIm1hcmdpbiIsImJvcmRlckxlZnQiLCJjb2xvciIsImZvbnRTdHlsZSIsIndpZHRoIiwid2hpdGVTcGFjZSIsIkF0b21pYyIsImVudGl0eSIsImdldEVudGl0eSIsImdldEVudGl0eUF0IiwiZ2V0RGF0YSIsImFueSIsImhhbmRsZVN0cmF0ZWd5IiwiaGFzaHRhZ1N0cmF0ZWd5IiwiSEFORExFX1JFR0VYIiwiSEFTSFRBR19SRUdFWCIsImZpbmRXaXRoUmVnZXgiLCJyZWdleCIsImNvbnRlbnRCbG9jayIsImNhbGxiYWNrIiwidGV4dCIsImdldFRleHQiLCJtYXRjaEFyciIsInN0YXJ0IiwiZXhlYyIsImluZGV4IiwibGVuZ3RoIiwiSGFuZGxlU3BhbiIsImhhbmRsZSIsImNoaWxkcmVuIiwiSGFzaHRhZ1NwYW4iLCJoYXNodGFnIiwiQmxvY2tzIiwiVU5TVFlMRUQiLCJQQVJBR1JBUEgiLCJIMSIsIkgyIiwiSDMiLCJINCIsIkg1IiwiSDYiLCJPTCIsIlVMIiwiQkxPQ0tRVU9URSIsIlZJREVPIiwiSW5saW5lIiwiQk9MRCIsIklUQUxJQyIsIlNUUklLRVRIUk9VR0giLCJVTkRFUkxJTkUiLCJISUdITElHSFQiLCJFbnRpdHkiLCJMSU5LIiwiSFlQRVJMSU5LIiwiSEFORExFRCIsIk5PVF9IQU5ETEVEIiwic3R5bGUiLCJlZGl0b3IiLCJtaW5IZWlnaHQiLCJidXR0b24iLCJ0ZXh0QWxpZ24iLCJkaXJlY3Rpb24iLCJ1bmljb2RlQmlkaSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9RQUFvUTs7QUFFdFQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtHQUFrRzs7QUFFcEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDZKQUE2Sjs7QUFFL007QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1GQUFtRjs7QUFFckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtGQUFrRjs7QUFFcEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9GQUFvRjs7QUFFdEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1GQUFtRjs7QUFFckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDZPQUE2Tzs7QUFFL1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHdEQUF3RDs7QUFFMUc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHVSQUF1Ujs7QUFFelU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtKQUFrSjs7QUFFcE07QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGdEQUFnRDs7QUFFbEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDJIQUEySDs7QUFFN0s7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtEQUFrRDs7QUFFcEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9JQUFvSTs7QUFFdEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNIQUFzSDs7QUFFeEs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlXQUF5Vzs7QUFFM1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDJOQUEyTjs7QUFFN1E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGdJQUFnSTs7QUFFbEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNKQUFzSjs7QUFFeE07QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELCtCQUErQjs7QUFFakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx3Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixtUEFBNEk7QUFDNUk7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSw4RUFBOEU7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFdBQVcsMkJBQTJCO0FBQ3RDO0FBQ0E7QUFDQSxhQUFhLDBCQUEwQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmOztBQUVBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCx3QkFBd0IsYzs7Ozs7Ozs7Ozs7OztBQ3JPN0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdDOzs7Ozs7Ozs7Ozs7O0FDckJBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNmQTs7QUFFQTs7QUFFQSxvR0FBb0csbUJBQW1CLEVBQUUsbUJBQW1CLDhIQUE4SDs7QUFFMVE7QUFDQTtBQUNBOztBQUVBLG1DOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSw4RDs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSxHQUFHOztBQUVIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVCOzs7Ozs7Ozs7Ozs7O0FDbENBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNEI7Ozs7Ozs7Ozs7Ozs7QUNaQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YseUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0YsaURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLCtCOzs7Ozs7Ozs7Ozs7O0FDekRBOztBQUVBOztBQUVBLG1EQUFtRCxnQkFBZ0Isc0JBQXNCLE9BQU8sMkJBQTJCLDBCQUEwQix5REFBeUQsMkJBQTJCLEVBQUUsRUFBRSxFQUFFLGVBQWU7O0FBRTlQOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQSw2QkFBNkIsVUFBVSxxQkFBcUI7QUFDNUQ7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEseUM7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNUQTs7Ozs7Ozs7Ozs7Ozs7O0FBTEE7OztzRkFvQkE7QUFBQSxRQUF3QkEsRUFBeEIsU0FBd0JBLEVBQXhCO0FBQUEsUUFBNEJDLEtBQTVCLFNBQTRCQSxLQUE1QjtBQUFBLFFBQW1DQyxJQUFuQyxTQUFtQ0EsSUFBbkM7QUFBQSxRQUF5Q0MsTUFBekMsU0FBeUNBLE1BQXpDO0FBQUEsUUFBaURDLEtBQWpELFNBQWlEQSxLQUFqRDtBQUFBLFFBQXdEQyxNQUF4RCxTQUF3REEsTUFBeEQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNOLEVBRnJDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1NLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlVDtBQUhSLGVBRmtCO0FBTzNCVSxvQkFBTSx5QkFBZSxFQUFFVCxVQUFGLEVBQVFDLGNBQVIsRUFBZ0JDLFlBQWhCLEVBQXVCQyxjQUF2QixFQUFmO0FBUHFCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVU8sZUFKVjtBQWNZQyxrQkFkWixHQWNtQ0QsR0FkbkMsQ0FjWUMsTUFkWixFQWNvQkMsVUFkcEIsR0FjbUNGLEdBZG5DLENBY29CRSxVQWRwQjs7QUFBQSxrQkFnQlFELFVBQVUsR0FoQmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWlCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWpCYjs7QUFBQTtBQUFBO0FBQUEsbUJBdUJ1QkYsSUFBSUssSUFBSixFQXZCdkI7O0FBQUE7QUF1QlVBLGdCQXZCVjtBQUFBLHdFQXlCZ0JBLElBekJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMkJJQyxvQkFBUUYsS0FBUjs7QUEzQkosNkNBNkJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTdCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZUcsTTs7Ozs7QUFsQmY7Ozs7QUFDQTs7OztrQkFxRGVBLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3REZjs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7O0FBRUE7Ozs7QUFFQTs7Ozs7O0FBRUEsSUFBTUMsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQTs7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pDLGVBQVM7QUFETCxLQURtQjtBQUl6QkMsY0FBVTtBQUNSQyxZQUFNO0FBREUsS0FKZTtBQU96QkMsVUFBTTtBQUNKQyxvQkFBYztBQURWLEtBUG1CO0FBVXpCQztBQUNFQyxnQkFBVTtBQURaLGtEQUVHUixNQUFNUyxXQUFOLENBQWtCQyxFQUFsQixDQUFxQixJQUFyQixDQUZILEVBRWdDO0FBQzVCQyxjQUFRLE1BRG9CO0FBRTVCQyxhQUFPO0FBRnFCLEtBRmhDLDhDQU1HWixNQUFNUyxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQU5ILEVBTWtDO0FBQzlCRixjQUFRLE1BRHNCO0FBRTlCQyxhQUFPO0FBRnVCLEtBTmxDLDhDQVVHWixNQUFNUyxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQVZILEVBVWtDO0FBQzlCRixjQUFRLE1BRHNCO0FBRTlCQyxhQUFPO0FBRnVCLEtBVmxDLDhDQWNHWixNQUFNUyxXQUFOLENBQWtCSSxJQUFsQixDQUF1QixJQUF2QixDQWRILEVBY2tDO0FBQzlCQyxlQUFTLE1BRHFCO0FBRTlCSCxjQUFRLE1BRnNCO0FBRzlCQyxhQUFPO0FBSHVCLEtBZGxDO0FBVnlCLEdBQVo7QUFBQSxDQUFmLEMsQ0FuQkE7O0lBbURNRyxXOzs7QUFDSix1QkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGdKQUNYQSxLQURXOztBQUFBLFVBNENuQkMsWUE1Q21CLEdBNENKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBNUNJOztBQUFBLFVBNkNuQkMsWUE3Q21CLEdBNkNKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBN0NJOztBQUVqQixVQUFLRSxLQUFMLEdBQWE7QUFDWHJDLGNBQVEscUNBQWtCZ0MsTUFBTU0sSUFBTixDQUFXdEMsTUFBN0I7QUFERyxLQUFiOztBQUlBLFVBQUt1QyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0MsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlELElBQVosT0FBZDtBQUNBLFVBQUtFLE1BQUwsR0FBYyxNQUFLQSxNQUFMLENBQVlGLElBQVosT0FBZDtBQVJpQjtBQVNsQjs7QUFFRDs7Ozs7NkJBQ1N4QyxNLEVBQVE7QUFDZixXQUFLa0MsUUFBTCxDQUFjLEVBQUVsQyxjQUFGLEVBQWQ7QUFDRDs7OzZCQUVRO0FBQ1AsV0FBS2tDLFFBQUwsQ0FBYyxFQUFFUyxNQUFNLENBQUMsS0FBS04sS0FBTCxDQUFXTSxJQUFwQixFQUFkO0FBQ0Q7Ozs7Ozs7Ozs7O0FBR1MzQyxzQixHQUFXLEtBQUtxQyxLLENBQWhCckMsTTs4QkFDYyxLQUFLZ0MsS0FBTCxDQUFXTSxJLEVBQXpCM0MsRSxlQUFBQSxFLEVBQUlDLEssZUFBQUEsSzs7dUJBRXdCLHNCQUFjO0FBQ2hERCx3QkFEZ0Q7QUFFaERDLDhCQUZnRDtBQUdoREksMEJBQVEsMkJBQWFBLE9BQU80QyxpQkFBUCxFQUFiO0FBSHdDLGlCQUFkLEM7Ozs7QUFBNUJsQywwQixTQUFBQSxVO0FBQVlDLHFCLFNBQUFBLEs7O3NCQU1oQkQsZUFBZSxHOzs7OztBQUNqQjtBQUNBRyx3QkFBUUYsS0FBUixDQUFjQSxLQUFkOzs7OztBQUlGLHFCQUFLcUIsS0FBTCxDQUFXYSxnQkFBWCxDQUE0QjtBQUMxQmxELHdCQUQwQjtBQUUxQkssMEJBQVEsMkJBQWFBLE9BQU80QyxpQkFBUCxFQUFiO0FBRmtCLGlCQUE1Qjs7QUFLQSxxQkFBS1YsUUFBTCxDQUFjLEVBQUVTLE1BQU0sS0FBUixFQUFkOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBTU87QUFBQSxVQUNDRyxPQURELEdBQ2EsS0FBS2QsS0FEbEIsQ0FDQ2MsT0FERDtBQUFBLG1CQUV5QixLQUFLVCxLQUY5QjtBQUFBLFVBRUNGLEtBRkQsVUFFQ0EsS0FGRDtBQUFBLFVBRVFRLElBRlIsVUFFUUEsSUFGUjtBQUFBLFVBRWMzQyxNQUZkLFVBRWNBLE1BRmQ7OztBQUlQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBVzhDLFFBQVE3QixJQUFoRTtBQUNFO0FBQUE7QUFBQSxZQUFNLFVBQU4sRUFBVyxJQUFJLEVBQWYsRUFBbUIsSUFBSSxFQUF2QixFQUEyQixJQUFJLENBQS9CLEVBQWtDLElBQUksQ0FBdEMsRUFBeUMsSUFBSSxDQUE3QztBQUNFO0FBQUE7QUFBQTtBQUNFLDRCQUFjLEtBQUtnQixZQURyQjtBQUVFLDRCQUFjLEtBQUtHO0FBRnJCO0FBSUU7QUFBQTtBQUFBLGdCQUFNLFFBQVFELEtBQWQsRUFBcUIsV0FBV1csUUFBUXpCLElBQXhDO0FBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRSwrQkFBYXJCLE1BRGY7QUFFRSw0QkFBVSxLQUFLdUMsUUFGakI7QUFHRSw0QkFBVSxDQUFDSTtBQUhiO0FBREY7QUFERixhQUpGO0FBYUU7QUFBQTtBQUFBO0FBQ0UseUJBREY7QUFFRSx1QkFBTSxRQUZSO0FBR0UsOEJBQVcsTUFIYjtBQUlFLDJCQUFXRyxRQUFRdkIsVUFKckI7QUFLRSx5QkFBU29CLE9BQU8sS0FBS0QsTUFBWixHQUFxQixLQUFLRDtBQUxyQztBQU9HRSxxQkFBTyxtREFBUCxHQUFzQjtBQVB6QjtBQWJGO0FBREY7QUFERixPQURGO0FBNkJEOzs7OztBQUdIWixZQUFZZ0IsU0FBWixHQUF3QjtBQUN0Qjs7O0FBR0FELFdBQVMsb0JBQVVFLE1BQVYsQ0FBaUJDLFVBSko7O0FBTXRCWCxRQUFNLG9CQUFVVSxNQUFWLENBQWlCQyxVQU5EOztBQVF0Qkosb0JBQWtCLG9CQUFVSyxJQUFWLENBQWVEO0FBUlgsQ0FBeEI7O2tCQVdlLDBCQUFXbEMsTUFBWCxFQUFtQmdCLFdBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaEpmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBR0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFVQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFRQTs7OztBQUNBOzs7O0FBRUE7O0FBa0JBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7OztBQWhDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBcURNb0IsWTs7O0FBQ0o7OztBQXlCQSwwQkFBYztBQUFBOztBQUFBOztBQUVaOztBQUVBOzs7Ozs7QUFKWTs7QUFBQSxVQTZMZEMsbUJBN0xjO0FBQUEsMEZBNkxRLGlCQUFPQyxDQUFQO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDcEI7QUFDTUMsb0JBRmMsR0FFUEQsRUFBRUUsTUFBRixDQUFTQyxLQUFULENBQWUsQ0FBZixDQUZPOztBQUFBLHNCQUloQkYsS0FBS0csSUFBTCxDQUFVQyxPQUFWLENBQWtCLFFBQWxCLE1BQWdDLENBSmhCO0FBQUE7QUFBQTtBQUFBOztBQUtaQyx3QkFMWSxHQUtELElBQUlDLFFBQUosRUFMQzs7QUFNbEJELHlCQUFTRSxNQUFULENBQWdCLE9BQWhCLEVBQXlCUCxJQUF6Qjs7QUFOa0I7QUFBQSx1QkFRMkIsTUFBS3RCLEtBQUwsQ0FBVzhCLGNBQVgsQ0FBMEI7QUFDckVIO0FBRHFFLGlCQUExQixDQVIzQjs7QUFBQTtBQUFBO0FBUVZqRCwwQkFSVSxTQVFWQSxVQVJVO0FBUUVDLHFCQVJGLFNBUUVBLEtBUkY7QUFRU29ELHVCQVJULFNBUVNBLE9BUlQ7O0FBQUEsc0JBWWRyRCxjQUFjLEdBWkE7QUFBQTtBQUFBO0FBQUE7O0FBYWhCO0FBQ0FHLHdCQUFRRixLQUFSLENBQWNELFVBQWQsRUFBMEJDLEtBQTFCO0FBZGdCOztBQUFBO0FBa0JBcUQsbUJBbEJBLEdBa0JRRCxPQWxCUixDQWtCVkUsUUFsQlU7QUFvQlZDLDJCQXBCVSxHQW9CTSxNQUFLbEMsS0FwQlgsQ0FvQlZrQyxXQXBCVTtBQXNCWkMsNEJBdEJZLEdBc0JHRCxZQUFZdEIsaUJBQVosRUF0Qkg7QUF1Qlp3QixzQ0F2QlksR0F1QmFELGFBQWFFLFlBQWIsQ0FDN0Isa0JBQU9DLEtBRHNCLEVBRTdCLFdBRjZCLEVBRzdCLEVBQUVOLFFBQUYsRUFINkIsQ0F2QmI7QUE2QlpPLHlCQTdCWSxHQTZCQUgsdUJBQXVCSSx1QkFBdkIsRUE3QkE7QUErQlpDLGlDQS9CWSxHQStCUSxxQkFBWUMsR0FBWixDQUFnQlIsV0FBaEIsRUFBNkI7QUFDckRTLGtDQUFnQlA7QUFEcUMsaUJBQTdCLENBL0JSO0FBbUNaUSw4QkFuQ1ksR0FtQ0ssMEJBQWlCQyxpQkFBakIsQ0FDckJKLGlCQURxQixFQUVyQkYsU0FGcUIsRUFHckIsR0FIcUIsQ0FuQ0w7OztBQXlDbEIsc0JBQUtoQyxRQUFMLENBQWNxQyxjQUFkOztBQXpDa0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsT0E3TFI7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBVVosVUFBS3JDLFFBQUwsR0FBZ0IsVUFBQzJCLFdBQUQsRUFBaUI7QUFDL0IsWUFBS2xDLEtBQUwsQ0FBV08sUUFBWCxDQUFvQjJCLFdBQXBCO0FBQ0QsS0FGRDs7QUFJQTs7OztBQUlBLFVBQUtZLEtBQUwsR0FBYTtBQUFBLGFBQU0sTUFBS0MsVUFBTCxDQUFnQkQsS0FBaEIsRUFBTjtBQUFBLEtBQWI7O0FBRUEsVUFBS0UsS0FBTCxHQUFhLE1BQUtBLEtBQUwsQ0FBV3hDLElBQVgsT0FBYjtBQUNBLFVBQUt5QyxZQUFMLEdBQW9CLE1BQUtBLFlBQUwsQ0FBa0J6QyxJQUFsQixPQUFwQjtBQUNBLFVBQUswQyxXQUFMLEdBQW1CLE1BQUtBLFdBQUwsQ0FBaUIxQyxJQUFqQixPQUFuQjtBQUNBLFVBQUsyQyxZQUFMLEdBQW9CLE1BQUtBLFlBQUwsQ0FBa0IzQyxJQUFsQixPQUFwQjtBQUNBLFVBQUs0QyxtQkFBTCxHQUEyQixNQUFLQSxtQkFBTCxDQUF5QjVDLElBQXpCLE9BQTNCO0FBQ0EsVUFBSzZDLG1CQUFMLEdBQTJCLE1BQUtBLG1CQUFMLENBQXlCN0MsSUFBekIsT0FBM0I7QUFDQSxVQUFLOEMsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCOUMsSUFBakIsT0FBbkI7QUFDQSxVQUFLK0MsYUFBTCxHQUFxQixNQUFLQSxhQUFMLENBQW1CL0MsSUFBbkIsT0FBckI7QUFDQSxVQUFLZ0QsZ0JBQUwsR0FBd0IsTUFBS0EsZ0JBQUwsQ0FBc0JoRCxJQUF0QixPQUF4QjtBQUNBLFVBQUtpRCxnQkFBTCxHQUF3QixNQUFLQSxnQkFBTCxDQUFzQmpELElBQXRCLE9BQXhCO0FBQ0EsVUFBS2tELGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCbEQsSUFBdEIsT0FBeEI7QUFDQSxVQUFLbUQsa0JBQUwsR0FBMEIsTUFBS0Esa0JBQUwsQ0FBd0JuRCxJQUF4QixPQUExQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVDWTtBQTZDYjs7QUFFRDs7Ozs7Ozs7OzBCQUtNYSxDLEVBQUc7QUFDUEEsUUFBRXVDLGNBQUY7QUFETyxVQUVDMUIsV0FGRCxHQUVpQixLQUFLbEMsS0FGdEIsQ0FFQ2tDLFdBRkQ7OztBQUlQLFVBQU1VLGlCQUFpQixtQkFBVUksS0FBVixDQUFnQjNCLENBQWhCLEVBQW1CYSxXQUFuQixFQUFnQyxDQUFoQyxDQUF2Qjs7QUFFQSxXQUFLM0IsUUFBTCxDQUFjcUMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O21DQUllO0FBQUEsVUFDTFYsV0FESyxHQUNXLEtBQUtsQyxLQURoQixDQUNMa0MsV0FESzs7O0FBR2IsVUFBTVUsaUJBQWlCLG1CQUFVaUIsZUFBVixDQUEwQjNCLFdBQTFCLEVBQXVDLFlBQXZDLENBQXZCOztBQUVBLFdBQUszQixRQUFMLENBQWNxQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBS2xDLEtBRGpCLENBQ0prQyxXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQTBCM0IsV0FBMUIsRUFBdUMsWUFBdkMsQ0FBdkI7O0FBRUEsV0FBSzNCLFFBQUwsQ0FBY3FDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzttQ0FJZTtBQUFBLFVBQ0xWLFdBREssR0FDVyxLQUFLbEMsS0FEaEIsQ0FDTGtDLFdBREs7OztBQUdiLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FBMEIzQixXQUExQixFQUF1QyxZQUF2QyxDQUF2Qjs7QUFFQSxXQUFLM0IsUUFBTCxDQUFjcUMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7OzBDQUlzQjtBQUFBLFVBQ1pWLFdBRFksR0FDSSxLQUFLbEMsS0FEVCxDQUNaa0MsV0FEWTs7O0FBR3BCLFVBQU1VLGlCQUFpQixtQkFBVWlCLGVBQVYsQ0FDckIzQixXQURxQixFQUVyQixxQkFGcUIsQ0FBdkI7O0FBS0EsV0FBSzNCLFFBQUwsQ0FBY3FDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzswQ0FJc0I7QUFBQSxVQUNaVixXQURZLEdBQ0ksS0FBS2xDLEtBRFQsQ0FDWmtDLFdBRFk7OztBQUdwQixVQUFNVSxpQkFBaUIsbUJBQVVpQixlQUFWLENBQ3JCM0IsV0FEcUIsRUFFckIsbUJBRnFCLENBQXZCOztBQUtBLFdBQUszQixRQUFMLENBQWNxQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7a0NBSWM7QUFBQSxVQUNKVixXQURJLEdBQ1ksS0FBS2xDLEtBRGpCLENBQ0prQyxXQURJOzs7QUFHWixVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLE1BQXpDLENBQXZCOztBQUVBLFdBQUszQixRQUFMLENBQWNxQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7b0NBSWdCO0FBQUEsVUFDTlYsV0FETSxHQUNVLEtBQUtsQyxLQURmLENBQ05rQyxXQURNOzs7QUFHZCxVQUFNVSxpQkFBaUIsbUJBQVVrQixpQkFBVixDQUE0QjVCLFdBQTVCLEVBQXlDLFFBQXpDLENBQXZCOztBQUVBLFdBQUszQixRQUFMLENBQWNxQyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7dUNBSW1CO0FBQUEsVUFDVFYsV0FEUyxHQUNPLEtBQUtsQyxLQURaLENBQ1RrQyxXQURTOzs7QUFHakIsVUFBTVUsaUJBQWlCLG1CQUFVa0IsaUJBQVYsQ0FDckI1QixXQURxQixFQUVyQixXQUZxQixDQUF2Qjs7QUFLQSxXQUFLM0IsUUFBTCxDQUFjcUMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O3VDQUltQjtBQUFBLFVBQ1RWLFdBRFMsR0FDTyxLQUFLbEMsS0FEWixDQUNUa0MsV0FEUzs7O0FBR2pCLFVBQU1VLGlCQUFpQixtQkFBVWtCLGlCQUFWLENBQTRCNUIsV0FBNUIsRUFBeUMsTUFBekMsQ0FBdkI7O0FBRUEsV0FBSzNCLFFBQUwsQ0FBY3FDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7O3lDQUdxQjtBQUNuQixXQUFLbUIsS0FBTCxDQUFXQyxLQUFYLEdBQW1CLElBQW5CO0FBQ0EsV0FBS0QsS0FBTCxDQUFXRSxLQUFYO0FBQ0Q7O0FBRUQ7Ozs7Ozs7O0FBZ0RBOzs7Ozs7cUNBTWlCQyxPLEVBQVM7QUFBQSxVQUNoQmhDLFdBRGdCLEdBQ0EsS0FBS2xDLEtBREwsQ0FDaEJrQyxXQURnQjs7O0FBR3hCLFVBQU1VLGlCQUFpQixtQkFBVWMsZ0JBQVYsQ0FBMkJ4QixXQUEzQixFQUF3Q2dDLE9BQXhDLENBQXZCOztBQUVBLFVBQUl0QixjQUFKLEVBQW9CO0FBQ2xCLGFBQUtyQyxRQUFMLENBQWNxQyxjQUFkO0FBQ0E7QUFDRDs7QUFFRDtBQUNEOztBQUVEOzs7O29DQUNnQnVCLEssRUFBTztBQUNyQixjQUFRQSxNQUFNQyxPQUFOLEVBQVI7QUFDRSxhQUFLLGtCQUFPQyxNQUFaO0FBQ0UsaUJBQU87QUFDTEMsdUNBREs7QUFFTEMsc0JBQVU7QUFGTCxXQUFQOztBQUtGO0FBQ0UsaUJBQU8sSUFBUDtBQVJKO0FBVUQ7QUFDRDs7QUFFQTs7OztpQ0FDYUosSyxFQUFPO0FBQ2xCLGNBQVFBLE1BQU1DLE9BQU4sRUFBUjtBQUNFLGFBQUssWUFBTDtBQUNFLGlCQUFPLHVCQUFQOztBQUVGO0FBQ0UsaUJBQU8sSUFBUDtBQUxKO0FBT0Q7QUFDRDs7Ozs2QkFFUztBQUFBOztBQUFBLG1CQU9ILEtBQUtwRSxLQVBGO0FBQUEsVUFFTGMsT0FGSyxVQUVMQSxPQUZLO0FBQUEsVUFHTDBELFFBSEssVUFHTEEsUUFISztBQUFBLFVBSUxqRSxRQUpLLFVBSUxBLFFBSks7QUFBQSxVQUtMMkIsV0FMSyxVQUtMQSxXQUxLO0FBQUEsVUFNTHVDLFdBTkssVUFNTEEsV0FOSzs7QUFTUDs7QUFDQSxVQUFNQyxXQUFXO0FBQ2ZDLGNBQU07QUFDSkMsMkJBQWlCLHFCQURiO0FBRUpDLHNCQUFZLCtDQUZSO0FBR0pDLG9CQUFVLEVBSE47QUFJSjVGLG1CQUFTO0FBSkw7QUFEUyxPQUFqQjs7QUFTQSxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVc0QixRQUFRN0IsSUFBeEI7QUFDRyxTQUFDdUYsUUFBRCxHQUNDO0FBQUE7QUFBQSxZQUFLLFdBQVcsMEJBQVcsRUFBRSxtQkFBbUIsRUFBckIsRUFBWCxDQUFoQjtBQUNFO0FBQUE7QUFBQSxjQUFTLE9BQU0sT0FBZixFQUF1QixJQUFHLE9BQTFCLEVBQWtDLFdBQVUsUUFBNUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QixFQUErQixTQUFTLEtBQUt2QixZQUE3QztBQUNFO0FBREY7QUFERixXQURGO0FBTUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxNQUFmLEVBQXNCLElBQUcsTUFBekIsRUFBZ0MsV0FBVSxRQUExQztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE1BQXZCLEVBQThCLFNBQVMsS0FBS0ssV0FBNUM7QUFDRTtBQURGO0FBREYsV0FORjtBQVdFO0FBQUE7QUFBQSxjQUFTLE9BQU0sUUFBZixFQUF3QixJQUFHLFFBQTNCLEVBQW9DLFdBQVUsUUFBOUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxRQUF2QixFQUFnQyxTQUFTLEtBQUtDLGFBQTlDO0FBQ0U7QUFERjtBQURGLFdBWEY7QUFnQkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxXQUFmLEVBQTJCLElBQUcsV0FBOUIsRUFBMEMsV0FBVSxRQUFwRDtBQUNFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLFdBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFERixXQWhCRjtBQXdCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE1BQWYsRUFBc0IsSUFBRyxNQUF6QixFQUFnQyxXQUFVLFFBQTFDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsTUFBdkIsRUFBOEIsU0FBUyxLQUFLTixXQUE1QztBQUNFO0FBREY7QUFERixXQXhCRjtBQTZCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE9BQWYsRUFBdUIsSUFBRyxPQUExQixFQUFrQyxXQUFVLFFBQTVDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsT0FBdkIsRUFBK0IsU0FBUyxLQUFLQyxZQUE3QztBQUNFO0FBREY7QUFERixXQTdCRjtBQWtDRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGVBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsZ0JBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFMRixXQWxDRjtBQThDRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsY0FEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQURGLFdBOUNGO0FBc0RFO0FBQUE7QUFBQSxjQUFTLE9BQU0sWUFBZixFQUE0QixJQUFHLFlBQS9CLEVBQTRDLFdBQVUsUUFBdEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxZQUF2QixFQUFvQyxjQUFwQztBQUNFO0FBREY7QUFERixXQXRERjtBQTJERTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsY0FBdkIsRUFBc0MsY0FBdEM7QUFDRTtBQURGO0FBREYsV0EzREY7QUFnRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBaEVGO0FBcUVFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQXJFRjtBQTBFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0ExRUY7QUErRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBL0VGO0FBb0ZFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxjQURiO0FBRUUsMEJBQVUsT0FBTyxLQUFLckQsS0FBTCxDQUFXOEIsY0FBbEIsS0FBcUMsVUFGakQ7QUFHRSx5QkFBUyxLQUFLNkI7QUFIaEI7QUFLRSx3RUFMRjtBQU1FO0FBQ0Usc0JBQUssTUFEUDtBQUVFLHdCQUFPLG9CQUZUO0FBR0UsMEJBQVUsS0FBS3ZDLG1CQUhqQjtBQUlFLHFCQUFLLGFBQUMyQyxLQUFELEVBQVc7QUFDZCx5QkFBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0QsaUJBTkg7QUFPRSx1QkFBTyxFQUFFakUsU0FBUyxNQUFYO0FBUFQ7QUFORjtBQURGLFdBcEZGO0FBc0dFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGlCQURSO0FBRUUsa0JBQUcsa0JBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsaUJBQXZCLEVBQXlDLGNBQXpDO0FBQ0U7QUFERjtBQUxGLFdBdEdGO0FBK0dFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGdCQURSO0FBRUUsa0JBQUcsZ0JBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsZ0JBQXZCLEVBQXdDLGNBQXhDO0FBQ0U7QUFERjtBQUxGLFdBL0dGO0FBd0hFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGdCQURSO0FBRUUsa0JBQUcsZ0JBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsZ0JBRGI7QUFFRSx5QkFBUyxLQUFLMkQ7QUFGaEI7QUFJRTtBQUpGO0FBTEYsV0F4SEY7QUFvSUU7QUFBQTtBQUFBO0FBQ0UscUJBQU0sZUFEUjtBQUVFLGtCQUFHLGVBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsZUFBdkIsRUFBdUMsY0FBdkM7QUFDRTtBQURGO0FBTEY7QUFwSUYsU0FERCxHQStJRyxJQWhKTjtBQWlKRTtBQUFBO0FBQUEsWUFBSyxXQUFXLDBCQUFXLEVBQUUsZUFBZSxFQUFqQixFQUFYLENBQWhCO0FBQ0U7QUFDRTs7QUFERixjQUdFLGFBQWF2QixXQUhmO0FBSUUsc0JBQVUzQjtBQUNWOztBQUxGLGNBT0UsYUFBYWtFO0FBQ2I7O0FBRUE7O0FBVkYsY0FZRSxpQkFBaUIsS0FBS00sZUFaeEI7QUFhRSwwQkFBYyxLQUFLQyxZQWJyQjtBQWNFLDRCQUFnQk47QUFDaEI7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBdkJGLGNBeUJFLFVBQVVGLFFBekJaO0FBMEJFO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBbkNGLGNBcUNFLGtCQUFrQixLQUFLZDtBQUN2Qjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFsREYsY0FvREUsT0FBTyxLQUFLVjtBQUNaOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQTNFRixjQTZFRSxLQUFLLGFBQUNpQyxJQUFELEVBQVU7QUFDYixxQkFBS2xDLFVBQUwsR0FBa0JrQyxJQUFsQjtBQUNEO0FBL0VIO0FBREY7QUFqSkYsT0FERjtBQXVPRDs7Ozs7QUE1akJIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBM0RBO0FBUkE7O0FBNkVNOUQsWSxDQUlHSixTLEdBQVk7QUFDakJELFdBQVMseUJBQVVFLE1BQVYsQ0FBaUJDLFVBRFQ7O0FBR2pCaUIsZUFBYSx5QkFBVWxCLE1BQVYsQ0FBaUJDLFVBSGI7QUFJakJWLFlBQVUseUJBQVVXLElBQVYsQ0FBZUQsVUFKUjs7QUFNakJ3RCxlQUFhLHlCQUFVUyxNQU5OOztBQVFqQlYsWUFBVSx5QkFBVVcsSUFBVixDQUFlbEUsVUFSUjs7QUFVakI7Ozs7QUFJQWEsa0JBQWdCLHlCQUFVWjtBQWRULEM7QUFKZkMsWSxDQXFCR2lFLFksR0FBZTtBQUNwQlosWUFBVSxJQURVO0FBRXBCQyxlQUFhO0FBRk8sQztrQkEwaEJULGtEQUFtQnRELFlBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDMW5CZjs7QUFFQTs7QUFKQTs7QUFXQSxJQUFNa0Usb0JBQW9CLGdDQUF1QixDQUMvQztBQUNFQyxzQ0FERjtBQUVFaEI7QUFGRixDQUQrQyxFQUsvQztBQUNFZ0IsdUNBREY7QUFFRWhCO0FBRkYsQ0FMK0MsQ0FBdkIsQ0FBMUI7O0FBV08sSUFBTWlCLGdEQUFvQixTQUFwQkEsaUJBQW9CLEdBRzVCO0FBQUEsTUFGSEMsT0FFRyx1RUFGTyxJQUVQO0FBQUEsTUFESEMsVUFDRyx1RUFEVUosaUJBQ1Y7O0FBQ0gsTUFBSUcsWUFBWSxJQUFoQixFQUFzQjtBQUNwQixXQUFPLHFCQUFZRSxXQUFaLENBQXdCRCxVQUF4QixDQUFQO0FBQ0Q7QUFDRCxTQUFPLHFCQUFZRSxpQkFBWixDQUE4Qiw2QkFBZUgsT0FBZixDQUE5QixFQUF1REMsVUFBdkQsQ0FBUDtBQUNELENBUk07O2tCQVVRRixpQjs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaENmOzs7Ozs7a0JBTWU7QUFDYixhQUFXO0FBQ1Qsd0JBQW9CO0FBQ2xCSyxrQkFBWSxNQURNO0FBRWxCQyxjQUFRLGdCQUZVO0FBR2xCaEIsa0JBQVksa0JBSE07QUFJbEJDLGdCQUFVLE1BSlE7QUFLbEI1RixlQUFTO0FBTFMsS0FEWDtBQVFULDBCQUFzQjtBQUNwQjRHLGlCQUFXLGdCQURTO0FBRXBCQyxjQUFRLE1BRlk7QUFHcEJqQixnQkFBVSxNQUhVO0FBSXBCa0IsaUJBQVc7QUFKUyxLQVJiO0FBY1QsMkNBQXVDO0FBQ3JDQyxjQUFRLGVBRDZCO0FBRXJDL0csZUFBUztBQUY0QixLQWQ5QjtBQWtCVCxtQ0FBK0I7QUFDN0IrRyxjQUFRLGVBRHFCO0FBRTdCL0csZUFBUztBQUNUO0FBSDZCLEtBbEJ0QjtBQXVCVCw4QkFBMEI7QUFDeEIwRix1QkFBaUIsZ0JBRE87QUFFeEJzQixrQkFBWSxnQkFGWTtBQUd4QkMsYUFBTyxNQUhpQjtBQUl4QnRCLGtCQUFZLGtDQUpZO0FBS3hCdUIsaUJBQVcsUUFMYTtBQU14QkgsY0FBUSxRQU5nQjtBQU94Qi9HLGVBQVM7QUFQZSxLQXZCakI7QUFnQ1QscUNBQWlDO0FBQy9CMEYsdUJBQWlCLHFCQURjO0FBRS9CQyxrQkFBWSwrQ0FGbUI7QUFHL0JDLGdCQUFVLE1BSHFCO0FBSS9CNUYsZUFBUztBQUpzQjtBQWhDeEIsR0FERTtBQXdDYkQsUUFBTTtBQUNKO0FBREksR0F4Q087QUEyQ2JFLFlBQVU7QUFDUkMsVUFBTTtBQURFO0FBM0NHLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0RmOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBLElBQU1MLFNBQVM7QUFDYmdGLFNBQU87QUFDTHNDLFdBQU8sTUFERjtBQUVMO0FBQ0E7QUFDQUMsZ0JBQVk7QUFKUDtBQURNLENBQWY7O0FBU0E7Ozs7Ozs7O0FBbkJBOzs7OztJQTJCTUMsTTs7O0FBTUosa0JBQVl2RyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtLLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7OzZCQUVRO0FBQUEsbUJBQ3lCLEtBQUtMLEtBRDlCO0FBQUEsVUFDQ21DLFlBREQsVUFDQ0EsWUFERDtBQUFBLFVBQ2VnQyxLQURmLFVBQ2VBLEtBRGY7OztBQUdQLFVBQU1xQyxTQUFTckUsYUFBYXNFLFNBQWIsQ0FBdUJ0QyxNQUFNdUMsV0FBTixDQUFrQixDQUFsQixDQUF2QixDQUFmOztBQUhPLDRCQUlTRixPQUFPRyxPQUFQLEVBSlQ7QUFBQSxVQUlDM0UsR0FKRCxtQkFJQ0EsR0FKRDs7QUFLUCxVQUFNUCxPQUFPK0UsT0FBT3BDLE9BQVAsRUFBYjs7QUFFQSxVQUFJM0MsU0FBUyxrQkFBT2EsS0FBcEIsRUFBMkI7QUFDekIsZUFDRTtBQUFBO0FBQUE7QUFDRSxpREFBSyxLQUFLLEtBQVYsRUFBaUIsS0FBS04sR0FBdEIsRUFBMkIsT0FBT2pELE9BQU9nRixLQUF6QztBQURGLFNBREY7QUFLRDs7QUFFRCxhQUFPLDBDQUFQO0FBQ0Q7Ozs7O0FBM0JHd0MsTSxDQUNHeEYsUyxHQUFZO0FBQ2pCb0IsZ0JBQWMsb0JBQVVuQixNQUFWLENBQWlCQyxVQURkO0FBRWpCa0QsU0FBTyxvQkFBVXlDLEdBQVYsQ0FBYzNGO0FBRkosQztrQkE2Qk5zRixNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDdkNDTSxjLEdBQUFBLGM7UUFJQUMsZSxHQUFBQSxlOztBQXBCaEI7Ozs7QUFDQTs7Ozs7O0FBSEE7QUFDQTtBQUlBLElBQU1DLGVBQWUsVUFBckI7QUFDQSxJQUFNQyxnQkFBZ0IsdUJBQXRCOztBQUVBLFNBQVNDLGFBQVQsQ0FBdUJDLEtBQXZCLEVBQThCQyxZQUE5QixFQUE0Q0MsUUFBNUMsRUFBc0Q7QUFDcEQsTUFBTUMsT0FBT0YsYUFBYUcsT0FBYixFQUFiO0FBQ0EsTUFBSUMsaUJBQUo7QUFBQSxNQUNFQyxjQURGO0FBRUEsU0FBTyxDQUFDRCxXQUFXTCxNQUFNTyxJQUFOLENBQVdKLElBQVgsQ0FBWixNQUFrQyxJQUF6QyxFQUErQztBQUM3Q0csWUFBUUQsU0FBU0csS0FBakI7QUFDQU4sYUFBU0ksS0FBVCxFQUFnQkEsUUFBUUQsU0FBUyxDQUFULEVBQVlJLE1BQXBDO0FBQ0Q7QUFDRjs7QUFFTSxTQUFTZCxjQUFULENBQXdCTSxZQUF4QixFQUFzQ0MsUUFBdEMsRUFBZ0RqRixZQUFoRCxFQUE4RDtBQUNuRThFLGdCQUFjRixZQUFkLEVBQTRCSSxZQUE1QixFQUEwQ0MsUUFBMUM7QUFDRDs7QUFFTSxTQUFTTixlQUFULENBQXlCSyxZQUF6QixFQUF1Q0MsUUFBdkMsRUFBaURqRixZQUFqRCxFQUErRDtBQUNwRThFLGdCQUFjRCxhQUFkLEVBQTZCRyxZQUE3QixFQUEyQ0MsUUFBM0M7QUFDRDs7QUFFTSxJQUFNUSxrQ0FBYSxTQUFiQSxVQUFhO0FBQUEsU0FBUztBQUFBO0FBQUEsTUFBTSxPQUFPLGdCQUFNQyxNQUFuQjtBQUE0QjdILFVBQU04SDtBQUFsQyxHQUFUO0FBQUEsQ0FBbkI7O0FBRUEsSUFBTUMsb0NBQWMsU0FBZEEsV0FBYztBQUFBLFNBQVM7QUFBQTtBQUFBLE1BQU0sT0FBTyxnQkFBTUMsT0FBbkI7QUFBNkJoSSxVQUFNOEg7QUFBbkMsR0FBVDtBQUFBLENBQXBCOztrQkFHUTtBQUNiakIsZ0NBRGE7QUFFYmUsd0JBRmE7O0FBSWJkLGtDQUphO0FBS2JpQjtBQUxhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CZjs7Ozs7OztBQU9BOzs7QUFHTyxJQUFNRSwwQkFBUztBQUNwQkMsWUFBVSxVQURVO0FBRXBCQyxhQUFXLFVBRlM7O0FBSXBCQyxNQUFJLFlBSmdCO0FBS3BCQyxNQUFJLFlBTGdCO0FBTXBCQyxNQUFJLGNBTmdCO0FBT3BCQyxNQUFJLGFBUGdCO0FBUXBCQyxNQUFJLGFBUmdCO0FBU3BCQyxNQUFJLFlBVGdCOztBQVdwQkMsTUFBSSxtQkFYZ0I7QUFZcEJDLE1BQUkscUJBWmdCOztBQWNwQmhFLFFBQU0sWUFkYzs7QUFnQnBCaUUsY0FBWSxZQWhCUTs7QUFrQnBCdkUsVUFBUSxRQWxCWTtBQW1CcEIvQixTQUFPLGNBbkJhO0FBb0JwQnVHLFNBQU87QUFwQmEsQ0FBZjs7QUF1QlA7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNLE1BRGM7QUFFcEJwRSxRQUFNLE1BRmM7QUFHcEJxRSxVQUFRLFFBSFk7QUFJcEJDLGlCQUFlLGVBSks7QUFLcEJDLGFBQVcsV0FMUztBQU1wQkMsYUFBVztBQU5TLENBQWY7O0FBU1A7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNO0FBRGMsQ0FBZjs7QUFJUDs7O0FBR08sSUFBTUMsZ0NBQVksV0FBbEI7O0FBRVA7OztBQUdPLElBQU1DLDRCQUFVLFNBQWhCO0FBQ0EsSUFBTUMsb0NBQWMsYUFBcEI7O2tCQUVRO0FBQ2J2QixnQkFEYTtBQUViYSxnQkFGYTtBQUdiTTtBQUhhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0RmOzs7OzttQkFFU2pJLFk7Ozs7Ozs7Ozt3QkFFQW9FLGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05UOztBQUVPLElBQU1rRSx3QkFBUTtBQUNuQnhLLFFBQU07QUFDSkMsYUFBUyxFQURMO0FBRUptSCxXQUFPO0FBRkgsR0FEYTtBQUtuQnFELFVBQVE7QUFDTjdELFlBQVEsZ0JBREY7QUFFTkUsWUFBUSxNQUZGO0FBR05qQixjQUFVLEVBSEo7QUFJTjZFLGVBQVcsRUFKTDtBQUtOekssYUFBUztBQUxILEdBTFc7QUFZbkIwSyxVQUFRO0FBQ041RCxlQUFXLEVBREw7QUFFTjZELGVBQVc7QUFGTCxHQVpXO0FBZ0JuQmhDLFVBQVE7QUFDTjFCLFdBQU8seUJBREQ7QUFFTjJELGVBQVcsS0FGTDtBQUdOQyxpQkFBYTtBQUhQLEdBaEJXO0FBcUJuQi9CLFdBQVM7QUFDUDdCLFdBQU87QUFEQTtBQXJCVSxDQUFkOztrQkEwQlFzRCxLIiwiZmlsZSI6IjQ1LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE2LjUgNnYxMS41YzAgMi4yMS0xLjc5IDQtNCA0cy00LTEuNzktNC00VjVjMC0xLjM4IDEuMTItMi41IDIuNS0yLjVzMi41IDEuMTIgMi41IDIuNXYxMC41YzAgLjU1LS40NSAxLTEgMXMtMS0uNDUtMS0xVjZIMTB2OS41YzAgMS4zOCAxLjEyIDIuNSAyLjUgMi41czIuNS0xLjEyIDIuNS0yLjVWNWMwLTIuMjEtMS43OS00LTQtNFM3IDIuNzkgNyA1djEyLjVjMCAzLjA0IDIuNDYgNS41IDUuNSA1LjVzNS41LTIuNDYgNS41LTUuNVY2aC0xLjV6JyB9KTtcblxudmFyIEF0dGFjaEZpbGUgPSBmdW5jdGlvbiBBdHRhY2hGaWxlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5BdHRhY2hGaWxlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShBdHRhY2hGaWxlKTtcbkF0dGFjaEZpbGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQXR0YWNoRmlsZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9BdHRhY2hGaWxlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9BdHRhY2hGaWxlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTkuNCAxNi42TDQuOCAxMmw0LjYtNC42TDggNmwtNiA2IDYgNiAxLjQtMS40em01LjIgMGw0LjYtNC42LTQuNi00LjZMMTYgNmw2IDYtNiA2LTEuNC0xLjR6JyB9KTtcblxudmFyIENvZGUgPSBmdW5jdGlvbiBDb2RlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Db2RlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShDb2RlKTtcbkNvZGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29kZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db2RlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db2RlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTMgMTcuMjVWMjFoMy43NUwxNy44MSA5Ljk0bC0zLjc1LTMuNzVMMyAxNy4yNXpNMjAuNzEgNy4wNGMuMzktLjM5LjM5LTEuMDIgMC0xLjQxbC0yLjM0LTIuMzRjLS4zOS0uMzktMS4wMi0uMzktMS40MSAwbC0xLjgzIDEuODMgMy43NSAzLjc1IDEuODMtMS44M3onIH0pO1xuXG52YXIgRWRpdCA9IGZ1bmN0aW9uIEVkaXQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkVkaXQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEVkaXQpO1xuRWRpdC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBFZGl0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0VkaXQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0VkaXQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNiAzMSAzNCAzOCA0NCA0NSA0NyA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTcgMTV2MmgxMHYtMkg3em0tNCA2aDE4di0ySDN2MnptMC04aDE4di0ySDN2MnptNC02djJoMTBWN0g3ek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduQ2VudGVyID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25DZW50ZXIocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduQ2VudGVyID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkNlbnRlcik7XG5Gb3JtYXRBbGlnbkNlbnRlci5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkNlbnRlcjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAyMWgxOHYtMkgzdjJ6bTAtNGgxOHYtMkgzdjJ6bTAtNGgxOHYtMkgzdjJ6bTAtNGgxOFY3SDN2MnptMC02djJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkp1c3RpZnkgPSBmdW5jdGlvbiBGb3JtYXRBbGlnbkp1c3RpZnkocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduSnVzdGlmeSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25KdXN0aWZ5KTtcbkZvcm1hdEFsaWduSnVzdGlmeS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkp1c3RpZnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTUgMTVIM3YyaDEydi0yem0wLThIM3YyaDEyVjd6TTMgMTNoMTh2LTJIM3Yyem0wIDhoMTh2LTJIM3Yyek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduTGVmdCA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduTGVmdChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25MZWZ0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkxlZnQpO1xuRm9ybWF0QWxpZ25MZWZ0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduTGVmdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zIDIxaDE4di0ySDN2MnptNi00aDEydi0ySDl2MnptLTYtNGgxOHYtMkgzdjJ6bTYtNGgxMlY3SDl2MnpNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnblJpZ2h0ID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25SaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25SaWdodCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25SaWdodCk7XG5Gb3JtYXRBbGlnblJpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduUmlnaHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNS42IDEwLjc5Yy45Ny0uNjcgMS42NS0xLjc3IDEuNjUtMi43OSAwLTIuMjYtMS43NS00LTQtNEg3djE0aDcuMDRjMi4wOSAwIDMuNzEtMS43IDMuNzEtMy43OSAwLTEuNTItLjg2LTIuODItMi4xNS0zLjQyek0xMCA2LjVoM2MuODMgMCAxLjUuNjcgMS41IDEuNXMtLjY3IDEuNS0xLjUgMS41aC0zdi0zem0zLjUgOUgxMHYtM2gzLjVjLjgzIDAgMS41LjY3IDEuNSAxLjVzLS42NyAxLjUtMS41IDEuNXonIH0pO1xuXG52YXIgRm9ybWF0Qm9sZCA9IGZ1bmN0aW9uIEZvcm1hdEJvbGQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEJvbGQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEJvbGQpO1xuRm9ybWF0Qm9sZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRCb2xkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTAgNHYzaDIuMjFsLTMuNDIgOEg2djNoOHYtM2gtMi4yMWwzLjQyLThIMThWNHonIH0pO1xuXG52YXIgRm9ybWF0SXRhbGljID0gZnVuY3Rpb24gRm9ybWF0SXRhbGljKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRJdGFsaWMgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEl0YWxpYyk7XG5Gb3JtYXRJdGFsaWMubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0SXRhbGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SXRhbGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTQgMTAuNWMtLjgzIDAtMS41LjY3LTEuNSAxLjVzLjY3IDEuNSAxLjUgMS41IDEuNS0uNjcgMS41LTEuNS0uNjctMS41LTEuNS0xLjV6bTAtNmMtLjgzIDAtMS41LjY3LTEuNSAxLjVTMy4xNyA3LjUgNCA3LjUgNS41IDYuODMgNS41IDYgNC44MyA0LjUgNCA0LjV6bTAgMTJjLS44MyAwLTEuNS42OC0xLjUgMS41cy42OCAxLjUgMS41IDEuNSAxLjUtLjY4IDEuNS0xLjUtLjY3LTEuNS0xLjUtMS41ek03IDE5aDE0di0ySDd2MnptMC02aDE0di0ySDd2MnptMC04djJoMTRWNUg3eicgfSk7XG5cbnZhciBGb3JtYXRMaXN0QnVsbGV0ZWQgPSBmdW5jdGlvbiBGb3JtYXRMaXN0QnVsbGV0ZWQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdExpc3RCdWxsZXRlZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0TGlzdEJ1bGxldGVkKTtcbkZvcm1hdExpc3RCdWxsZXRlZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRMaXN0QnVsbGV0ZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMiAxN2gydi41SDN2MWgxdi41SDJ2MWgzdi00SDJ2MXptMS05aDFWNEgydjFoMXYzem0tMSAzaDEuOEwyIDEzLjF2LjloM3YtMUgzLjJMNSAxMC45VjEwSDJ2MXptNS02djJoMTRWNUg3em0wIDE0aDE0di0ySDd2MnptMC02aDE0di0ySDd2MnonIH0pO1xuXG52YXIgRm9ybWF0TGlzdE51bWJlcmVkID0gZnVuY3Rpb24gRm9ybWF0TGlzdE51bWJlcmVkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRMaXN0TnVtYmVyZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdExpc3ROdW1iZXJlZCk7XG5Gb3JtYXRMaXN0TnVtYmVyZWQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0TGlzdE51bWJlcmVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYgMTdoM2wyLTRWN0g1djZoM3ptOCAwaDNsMi00VjdoLTZ2NmgzeicgfSk7XG5cbnZhciBGb3JtYXRRdW90ZSA9IGZ1bmN0aW9uIEZvcm1hdFF1b3RlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRRdW90ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0UXVvdGUpO1xuRm9ybWF0UXVvdGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0UXVvdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0UXVvdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDE3YzMuMzEgMCA2LTIuNjkgNi02VjNoLTIuNXY4YzAgMS45My0xLjU3IDMuNS0zLjUgMy41UzguNSAxMi45MyA4LjUgMTFWM0g2djhjMCAzLjMxIDIuNjkgNiA2IDZ6bS03IDJ2MmgxNHYtMkg1eicgfSk7XG5cbnZhciBGb3JtYXRVbmRlcmxpbmVkID0gZnVuY3Rpb24gRm9ybWF0VW5kZXJsaW5lZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0VW5kZXJsaW5lZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0VW5kZXJsaW5lZCk7XG5Gb3JtYXRVbmRlcmxpbmVkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdFVuZGVybGluZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xOCA0SDZ2Mmw2LjUgNkw2IDE4djJoMTJ2LTNoLTdsNS01LTUtNWg3eicgfSk7XG5cbnZhciBGdW5jdGlvbnMgPSBmdW5jdGlvbiBGdW5jdGlvbnMocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZ1bmN0aW9ucyA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRnVuY3Rpb25zKTtcbkZ1bmN0aW9ucy5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGdW5jdGlvbnM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GdW5jdGlvbnMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNiAxNGwzIDN2NWg2di01bDMtM1Y5SDZ6bTUtMTJoMnYzaC0yek0zLjUgNS44NzVMNC45MTQgNC40NmwyLjEyIDIuMTIyTDUuNjIgNy45OTd6bTEzLjQ2LjcxbDIuMTIzLTIuMTIgMS40MTQgMS40MTRMMTguMzc1IDh6JyB9KTtcblxudmFyIEhpZ2hsaWdodCA9IGZ1bmN0aW9uIEhpZ2hsaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSGlnaGxpZ2h0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShIaWdobGlnaHQpO1xuSGlnaGxpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEhpZ2hsaWdodDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0hpZ2hsaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMCAySDRjLTEuMSAwLTIgLjktMiAydjEyYzAgMS4xLjkgMiAyIDJoMTRsNCA0VjRjMC0xLjEtLjktMi0yLTJ6bS0yIDEySDZ2LTJoMTJ2MnptMC0zSDZWOWgxMnYyem0wLTNINlY2aDEydjJ6JyB9KTtcblxudmFyIEluc2VydENvbW1lbnQgPSBmdW5jdGlvbiBJbnNlcnRDb21tZW50KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRDb21tZW50ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRDb21tZW50KTtcbkluc2VydENvbW1lbnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0Q29tbWVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTExLjk5IDJDNi40NyAyIDIgNi40OCAyIDEyczQuNDcgMTAgOS45OSAxMEMxNy41MiAyMiAyMiAxNy41MiAyMiAxMlMxNy41MiAyIDExLjk5IDJ6TTEyIDIwYy00LjQyIDAtOC0zLjU4LTgtOHMzLjU4LTggOC04IDggMy41OCA4IDgtMy41OCA4LTggOHptMy41LTljLjgzIDAgMS41LS42NyAxLjUtMS41UzE2LjMzIDggMTUuNSA4IDE0IDguNjcgMTQgOS41cy42NyAxLjUgMS41IDEuNXptLTcgMGMuODMgMCAxLjUtLjY3IDEuNS0xLjVTOS4zMyA4IDguNSA4IDcgOC42NyA3IDkuNSA3LjY3IDExIDguNSAxMXptMy41IDYuNWMyLjMzIDAgNC4zMS0xLjQ2IDUuMTEtMy41SDYuODljLjggMi4wNCAyLjc4IDMuNSA1LjExIDMuNXonIH0pO1xuXG52YXIgSW5zZXJ0RW1vdGljb24gPSBmdW5jdGlvbiBJbnNlcnRFbW90aWNvbihwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0RW1vdGljb24gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydEVtb3RpY29uKTtcbkluc2VydEVtb3RpY29uLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydEVtb3RpY29uO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRFbW90aWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zLjkgMTJjMC0xLjcxIDEuMzktMy4xIDMuMS0zLjFoNFY3SDdjLTIuNzYgMC01IDIuMjQtNSA1czIuMjQgNSA1IDVoNHYtMS45SDdjLTEuNzEgMC0zLjEtMS4zOS0zLjEtMy4xek04IDEzaDh2LTJIOHYyem05LTZoLTR2MS45aDRjMS43MSAwIDMuMSAxLjM5IDMuMSAzLjFzLTEuMzkgMy4xLTMuMSAzLjFoLTRWMTdoNGMyLjc2IDAgNS0yLjI0IDUtNXMtMi4yNC01LTUtNXonIH0pO1xuXG52YXIgSW5zZXJ0TGluayA9IGZ1bmN0aW9uIEluc2VydExpbmsocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydExpbmsgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydExpbmspO1xuSW5zZXJ0TGluay5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjEgMTlWNWMwLTEuMS0uOS0yLTItMkg1Yy0xLjEgMC0yIC45LTIgMnYxNGMwIDEuMS45IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yek04LjUgMTMuNWwyLjUgMy4wMUwxNC41IDEybDQuNSA2SDVsMy41LTQuNXonIH0pO1xuXG52YXIgSW5zZXJ0UGhvdG8gPSBmdW5jdGlvbiBJbnNlcnRQaG90byhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0UGhvdG8gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydFBob3RvKTtcbkluc2VydFBob3RvLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydFBob3RvO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMjQgMzEgMzQgMzggMzkgNDQgNDUgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNyAzSDVjLTEuMTEgMC0yIC45LTIgMnYxNGMwIDEuMS44OSAyIDIgMmgxNGMxLjEgMCAyLS45IDItMlY3bC00LTR6bS01IDE2Yy0xLjY2IDAtMy0xLjM0LTMtM3MxLjM0LTMgMy0zIDMgMS4zNCAzIDMtMS4zNCAzLTMgM3ptMy0xMEg1VjVoMTB2NHonIH0pO1xuXG52YXIgU2F2ZSA9IGZ1bmN0aW9uIFNhdmUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblNhdmUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFNhdmUpO1xuU2F2ZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBTYXZlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NhdmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1NhdmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAyNiAzMSAzNCAzOCA0NCA0NSA0NiA0NyA0OSIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTUgNHYzaDUuNXYxMmgzVjdIMTlWNHonIH0pO1xuXG52YXIgVGl0bGUgPSBmdW5jdGlvbiBUaXRsZShwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuVGl0bGUgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKFRpdGxlKTtcblRpdGxlLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IFRpdGxlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL1RpdGxlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuLi9JY29uJyk7XG5cbnZhciBfSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKTtcblxucmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX3JlYWN0SGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL3JlYWN0SGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBCdXR0b25CYXNlXG5cbi8vIEVuc3VyZSBDU1Mgc3BlY2lmaWNpdHlcblxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgICAgZmxleDogJzAgMCBhdXRvJyxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjQpLFxuICAgICAgd2lkdGg6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBoZWlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBwYWRkaW5nOiAwLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmUsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2JhY2tncm91bmQtY29sb3InLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdFxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIGNvbG9ySW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgZGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgbGFiZWw6IHtcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnaW5oZXJpdCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICB3aWR0aDogJzFlbScsXG4gICAgICBoZWlnaHQ6ICcxZW0nXG4gICAgfSxcbiAgICBrZXlib2FyZEZvY3VzZWQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSBuYXRpdmUgYnV0dG9uIGNvbXBvbmVudC5cbiAgICovXG4gIGJ1dHRvblJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIGVsZW1lbnQuXG4gICAqIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYW4gaWNvbiBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdpbmhlcml0JywgJ3ByaW1hcnknLCAnY29udHJhc3QnLCAnYWNjZW50J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJ1dHRvbiB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHJpcHBsZSB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufTtcblxuLyoqXG4gKiBSZWZlciB0byB0aGUgW0ljb25zXSgvc3R5bGUvaWNvbnMpIHNlY3Rpb24gb2YgdGhlIGRvY3VtZW50YXRpb25cbiAqIHJlZ2FyZGluZyB0aGUgYXZhaWxhYmxlIGljb24gb3B0aW9ucy5cbiAqL1xudmFyIEljb25CdXR0b24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJY29uQnV0dG9uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEljb25CdXR0b24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChJY29uQnV0dG9uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShJY29uQnV0dG9uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYnV0dG9uUmVmID0gX3Byb3BzLmJ1dHRvblJlZixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uUmVmJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2Rpc2FibGVkJywgJ3Jvb3RSZWYnXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2RlZmF1bHQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGlzYWJsZWQsIGRpc2FibGVkKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWUpLFxuICAgICAgICAgIGNlbnRlclJpcHBsZTogdHJ1ZSxcbiAgICAgICAgICBrZXlib2FyZEZvY3VzZWRDbGFzc05hbWU6IGNsYXNzZXMua2V5Ym9hcmRGb2N1c2VkLFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZFxuICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgIHJvb3RSZWY6IGJ1dHRvblJlZixcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5sYWJlbCB9LFxuICAgICAgICAgIHR5cGVvZiBjaGlsZHJlbiA9PT0gJ3N0cmluZycgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuaWNvbiB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApIDogX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgICBpZiAoKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KShjaGlsZCwgWydJY29uJywgJ1N2Z0ljb24nXSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5pY29uLCBjaGlsZC5wcm9wcy5jbGFzc05hbWUpXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgfSlcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb25CdXR0b247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdkZWZhdWx0JyxcbiAgZGlzYWJsZWQ6IGZhbHNlLFxuICBkaXNhYmxlUmlwcGxlOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uQnV0dG9uJyB9KShJY29uQnV0dG9uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb25CdXR0b24gPSByZXF1aXJlKCcuL0ljb25CdXR0b24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbkJ1dHRvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSByZXF1aXJlKCcuL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwnKTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSByZXF1aXJlKCcuL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQnKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRmFjdG9yeSA9IGZ1bmN0aW9uIGNyZWF0ZUZhY3RvcnkodHlwZSkge1xuICB2YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQgPSAoMCwgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyLmRlZmF1bHQpKHR5cGUpO1xuICByZXR1cm4gZnVuY3Rpb24gKHAsIGMpIHtcbiAgICByZXR1cm4gKDAsIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsMi5kZWZhdWx0KShmYWxzZSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHAsIGMpO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRmFjdG9yeTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgZ2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBnZXREaXNwbGF5TmFtZShDb21wb25lbnQpIHtcbiAgaWYgKHR5cGVvZiBDb21wb25lbnQgPT09ICdzdHJpbmcnKSB7XG4gICAgcmV0dXJuIENvbXBvbmVudDtcbiAgfVxuXG4gIGlmICghQ29tcG9uZW50KSB7XG4gICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgfVxuXG4gIHJldHVybiBDb21wb25lbnQuZGlzcGxheU5hbWUgfHwgQ29tcG9uZW50Lm5hbWUgfHwgJ0NvbXBvbmVudCc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBnZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3R5cGVvZiA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiB0eXBlb2YgU3ltYm9sLml0ZXJhdG9yID09PSBcInN5bWJvbFwiID8gZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gdHlwZW9mIG9iajsgfSA6IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7IH07XG5cbnZhciBpc0NsYXNzQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNDbGFzc0NvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4oQ29tcG9uZW50ICYmIENvbXBvbmVudC5wcm90b3R5cGUgJiYgX3R5cGVvZihDb21wb25lbnQucHJvdG90eXBlLmlzUmVhY3RDb21wb25lbnQpID09PSAnb2JqZWN0Jyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc0NsYXNzQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQgPSByZXF1aXJlKCcuL2lzQ2xhc3NDb21wb25lbnQnKTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2lzQ2xhc3NDb21wb25lbnQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IGZ1bmN0aW9uIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKHR5cGVvZiBDb21wb25lbnQgPT09ICdmdW5jdGlvbicgJiYgISgwLCBfaXNDbGFzc0NvbXBvbmVudDIuZGVmYXVsdCkoQ29tcG9uZW50KSAmJiAhQ29tcG9uZW50LmRlZmF1bHRQcm9wcyAmJiAhQ29tcG9uZW50LmNvbnRleHRUeXBlcyAmJiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgPT09ICdwcm9kdWN0aW9uJyB8fCAhQ29tcG9uZW50LnByb3BUeXBlcykpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaG91bGRVcGRhdGUgPSByZXF1aXJlKCcuL3Nob3VsZFVwZGF0ZScpO1xuXG52YXIgX3Nob3VsZFVwZGF0ZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaG91bGRVcGRhdGUpO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJy4vc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBwdXJlID0gZnVuY3Rpb24gcHVyZShCYXNlQ29tcG9uZW50KSB7XG4gIHZhciBob2MgPSAoMCwgX3Nob3VsZFVwZGF0ZTIuZGVmYXVsdCkoZnVuY3Rpb24gKHByb3BzLCBuZXh0UHJvcHMpIHtcbiAgICByZXR1cm4gISgwLCBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0KShwcm9wcywgbmV4dFByb3BzKTtcbiAgfSk7XG5cbiAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdwdXJlJykpKGhvYyhCYXNlQ29tcG9uZW50KSk7XG4gIH1cblxuICByZXR1cm4gaG9jKEJhc2VDb21wb25lbnQpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gcHVyZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zZXRTdGF0aWMgPSByZXF1aXJlKCcuL3NldFN0YXRpYycpO1xuXG52YXIgX3NldFN0YXRpYzIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXRTdGF0aWMpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc2V0RGlzcGxheU5hbWUgPSBmdW5jdGlvbiBzZXREaXNwbGF5TmFtZShkaXNwbGF5TmFtZSkge1xuICByZXR1cm4gKDAsIF9zZXRTdGF0aWMyLmRlZmF1bHQpKCdkaXNwbGF5TmFtZScsIGRpc3BsYXlOYW1lKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBzZXRTdGF0aWMgPSBmdW5jdGlvbiBzZXRTdGF0aWMoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIEJhc2VDb21wb25lbnRba2V5XSA9IHZhbHVlO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICByZXR1cm4gQmFzZUNvbXBvbmVudDtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldFN0YXRpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnZmJqcy9saWIvc2hhbGxvd0VxdWFsJyk7XG5cbnZhciBfc2hhbGxvd0VxdWFsMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NoYWxsb3dFcXVhbCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9zaGFsbG93RXF1YWwyLmRlZmF1bHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkgPSByZXF1aXJlKCcuL2NyZWF0ZUVhZ2VyRmFjdG9yeScpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckZhY3RvcnkpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBzaG91bGRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRVcGRhdGUodGVzdCkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICB2YXIgZmFjdG9yeSA9ICgwLCBfY3JlYXRlRWFnZXJGYWN0b3J5Mi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KTtcblxuICAgIHZhciBTaG91bGRVcGRhdGUgPSBmdW5jdGlvbiAoX0NvbXBvbmVudCkge1xuICAgICAgX2luaGVyaXRzKFNob3VsZFVwZGF0ZSwgX0NvbXBvbmVudCk7XG5cbiAgICAgIGZ1bmN0aW9uIFNob3VsZFVwZGF0ZSgpIHtcbiAgICAgICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIFNob3VsZFVwZGF0ZSk7XG5cbiAgICAgICAgcmV0dXJuIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHRoaXMsIF9Db21wb25lbnQuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gICAgICB9XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUuc2hvdWxkQ29tcG9uZW50VXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcykge1xuICAgICAgICByZXR1cm4gdGVzdCh0aGlzLnByb3BzLCBuZXh0UHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5yZW5kZXIgPSBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICAgIHJldHVybiBmYWN0b3J5KHRoaXMucHJvcHMpO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgICB9KF9yZWFjdC5Db21wb25lbnQpO1xuXG4gICAgaWYgKHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSAncHJvZHVjdGlvbicpIHtcbiAgICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3Nob3VsZFVwZGF0ZScpKShTaG91bGRVcGRhdGUpO1xuICAgIH1cbiAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2hvdWxkVXBkYXRlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IGZ1bmN0aW9uIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwoaGFzS2V5LCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcHJvcHMsIGNoaWxkcmVuKSB7XG4gIGlmICghaGFzS2V5ICYmIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50KSB7XG4gICAgaWYgKGNoaWxkcmVuKSB7XG4gICAgICByZXR1cm4gdHlwZShfZXh0ZW5kcyh7fSwgcHJvcHMsIHsgY2hpbGRyZW46IGNoaWxkcmVuIH0pKTtcbiAgICB9XG4gICAgcmV0dXJuIHR5cGUocHJvcHMpO1xuICB9XG5cbiAgdmFyIENvbXBvbmVudCA9IHR5cGU7XG5cbiAgaWYgKGNoaWxkcmVuKSB7XG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgQ29tcG9uZW50LFxuICAgICAgcHJvcHMsXG4gICAgICBjaGlsZHJlblxuICAgICk7XG4gIH1cblxuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoQ29tcG9uZW50LCBwcm9wcyk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVFYWdlckVsZW1lbnRVdGlsO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2dldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9nZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX2dldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHdyYXBEaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHdyYXBEaXNwbGF5TmFtZShCYXNlQ29tcG9uZW50LCBob2NOYW1lKSB7XG4gIHJldHVybiBob2NOYW1lICsgJygnICsgKDAsIF9nZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCkgKyAnKSc7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSB3cmFwRGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmFtZSAtXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5hdmF0YXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuY292ZXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQucmVzdW1lIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiB1cGRhdGUoeyBpZCwgdG9rZW4sIG5hbWUsIGF2YXRhciwgY292ZXIsIHJlc3VtZSB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7aWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUFVUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgbmFtZSwgYXZhdGFyLCBjb3ZlciwgcmVzdW1lIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3VzZXJzL3VwZGF0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBDYXJkLCB7IENhcmRDb250ZW50IH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5cbmltcG9ydCBCdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvQnV0dG9uJztcbmltcG9ydCBFZGl0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9FZGl0JztcbmltcG9ydCBTYXZlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9TYXZlJztcblxuaW1wb3J0IHsgY29udmVydFRvUmF3IH0gZnJvbSAnZHJhZnQtanMnO1xuXG5pbXBvcnQgTWF5YXNoRWRpdG9yLCB7IGNyZWF0ZUVkaXRvclN0YXRlIH0gZnJvbSAnLi4vLi4vLi4vbGliL21heWFzaC1lZGl0b3InO1xuXG5pbXBvcnQgYXBpVXNlclVwZGF0ZSBmcm9tICcuLi8uLi9hcGkvdXNlcnMvdXBkYXRlJztcblxuY29uc3Qgc3R5bGVzID0gKHRoZW1lKSA9PiAoe1xuICByb290OiB7XG4gICAgcGFkZGluZzogJzElJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbiAgZWRpdEJ1dHRvbjoge1xuICAgIHBvc2l0aW9uOiAnZml4ZWQnLFxuICAgIFt0aGVtZS5icmVha3BvaW50cy51cCgneGwnKV06IHtcbiAgICAgIGJvdHRvbTogJzQwcHgnLFxuICAgICAgcmlnaHQ6ICc0MHB4JyxcbiAgICB9LFxuICAgIFt0aGVtZS5icmVha3BvaW50cy5kb3duKCd4bCcpXToge1xuICAgICAgYm90dG9tOiAnNDBweCcsXG4gICAgICByaWdodDogJzQwcHgnLFxuICAgIH0sXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyldOiB7XG4gICAgICBib3R0b206ICczMHB4JyxcbiAgICAgIHJpZ2h0OiAnMzBweCcsXG4gICAgfSxcbiAgICBbdGhlbWUuYnJlYWtwb2ludHMuZG93bignc20nKV06IHtcbiAgICAgIGRpc3BsYXk6ICdub25lJyxcbiAgICAgIGJvdHRvbTogJzI1cHgnLFxuICAgICAgcmlnaHQ6ICcyNXB4JyxcbiAgICB9LFxuICB9LFxufSk7XG5cbmNsYXNzIFRhYk15UmVzdW1lIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHJlc3VtZTogY3JlYXRlRWRpdG9yU3RhdGUocHJvcHMudXNlci5yZXN1bWUpLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2hhbmdlID0gdGhpcy5vbkNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25FZGl0ID0gdGhpcy5vbkVkaXQuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uU2F2ZSA9IHRoaXMub25TYXZlLmJpbmQodGhpcyk7XG4gIH1cblxuICAvLyBUaGlzIG9uIGNoYW5nZSBmdW5jdGlvbiBpcyBmb3IgTWF5YXNoRWRpdG9yXG4gIG9uQ2hhbmdlKHJlc3VtZSkge1xuICAgIHRoaXMuc2V0U3RhdGUoeyByZXN1bWUgfSk7XG4gIH1cblxuICBvbkVkaXQoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXQ6ICF0aGlzLnN0YXRlLmVkaXQgfSk7XG4gIH1cblxuICBhc3luYyBvblNhdmUoKSB7XG4gICAgY29uc3QgeyByZXN1bWUgfSA9IHRoaXMuc3RhdGU7XG4gICAgY29uc3QgeyBpZCwgdG9rZW4gfSA9IHRoaXMucHJvcHMudXNlcjtcblxuICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IgfSA9IGF3YWl0IGFwaVVzZXJVcGRhdGUoe1xuICAgICAgaWQsXG4gICAgICB0b2tlbixcbiAgICAgIHJlc3VtZTogY29udmVydFRvUmF3KHJlc3VtZS5nZXRDdXJyZW50Q29udGVudCgpKSxcbiAgICB9KTtcblxuICAgIGlmIChzdGF0dXNDb2RlICE9PSAyMDApIHtcbiAgICAgIC8vIGVycm9yIGhhbmRsZVxuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgdGhpcy5wcm9wcy5hY3Rpb25Vc2VyVXBkYXRlKHtcbiAgICAgIGlkLFxuICAgICAgcmVzdW1lOiBjb252ZXJ0VG9SYXcocmVzdW1lLmdldEN1cnJlbnRDb250ZW50KCkpLFxuICAgIH0pO1xuXG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVkaXQ6IGZhbHNlIH0pO1xuICB9XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBob3ZlciwgZWRpdCwgcmVzdW1lIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkIGNvbnRhaW5lciBqdXN0aWZ5PVwiY2VudGVyXCIgc3BhY2luZz17MH0gY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0gc209ezEwfSBtZD17OH0gbGc9ezZ9IHhsPXs2fT5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI9e3RoaXMub25Nb3VzZUVudGVyfVxuICAgICAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Q2FyZCByYWlzZWQ9e2hvdmVyfSBjbGFzc05hbWU9e2NsYXNzZXMuY2FyZH0+XG4gICAgICAgICAgICAgIDxDYXJkQ29udGVudD5cbiAgICAgICAgICAgICAgICA8TWF5YXNoRWRpdG9yXG4gICAgICAgICAgICAgICAgICBlZGl0b3JTdGF0ZT17cmVzdW1lfVxuICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2V9XG4gICAgICAgICAgICAgICAgICByZWFkT25seT17IWVkaXR9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgZmFiXG4gICAgICAgICAgICAgIGNvbG9yPVwiYWNjZW50XCJcbiAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkVkaXRcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZWRpdEJ1dHRvbn1cbiAgICAgICAgICAgICAgb25DbGljaz17ZWRpdCA/IHRoaXMub25TYXZlIDogdGhpcy5vbkVkaXR9XG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIHtlZGl0ID8gPFNhdmVJY29uIC8+IDogPEVkaXRJY29uIC8+fVxuICAgICAgICAgICAgPC9CdXR0b24+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cblRhYk15UmVzdW1lLnByb3BUeXBlcyA9IHtcbiAgLyoqXG4gICAqIFRhYk15UmVzdW1lIHN0eWxlIG9iamVjdFxuICAgKi9cbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBhY3Rpb25Vc2VyVXBkYXRlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuZXhwb3J0IGRlZmF1bHQgd2l0aFN0eWxlcyhzdHlsZXMpKFRhYk15UmVzdW1lKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Ib21lL1RhYk15UmVzdW1lLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncmVhY3QvbGliL1JlYWN0UHJvcFR5cGVzJztcbmltcG9ydCBjbGFzc05hbWVzIGZyb20gJ2NsYXNzbmFtZXMnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbi8vIGltcG9ydCBJY29uIGZyb20gJ21hdGVyaWFsLXVpL0ljb24nO1xuaW1wb3J0IEljb25CdXR0b24gZnJvbSAnbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbic7XG5pbXBvcnQgVG9vbHRpcCBmcm9tICdtYXRlcmlhbC11aS9Ub29sdGlwJztcblxuaW1wb3J0IFRpdGxlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9UaXRsZSc7XG5pbXBvcnQgRm9ybWF0Qm9sZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Qm9sZCc7XG5pbXBvcnQgRm9ybWF0SXRhbGljIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYyc7XG5pbXBvcnQgRm9ybWF0VW5kZXJsaW5lZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZCc7XG5pbXBvcnQgRm9ybWF0UXVvdGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlJztcbi8vIGltcG9ydCBGb3JtYXRDbGVhckljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q2xlYXInO1xuLy8gaW1wb3J0IEZvcm1hdENvbG9yRmlsbEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q29sb3JGaWxsJztcbi8vIGltcG9ydCBGb3JtYXRDb2xvclJlc2V0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRDb2xvclJlc2V0Jztcbi8vIGltcG9ydCBGb3JtYXRDb2xvclRleHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENvbG9yVGV4dCc7XG4vLyBpbXBvcnQgRm9ybWF0SW5kZW50RGVjcmVhc2VJY29uXG4vLyAgIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEluZGVudERlY3JlYXNlJztcbi8vIGltcG9ydCBGb3JtYXRJbmRlbnRJbmNyZWFzZUljb25cbi8vICAgZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SW5kZW50SW5jcmVhc2UnO1xuXG5pbXBvcnQgQ29kZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQ29kZSc7XG5cbmltcG9ydCBGb3JtYXRMaXN0TnVtYmVyZWRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZCc7XG5pbXBvcnQgRm9ybWF0TGlzdEJ1bGxldGVkSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQnO1xuXG5pbXBvcnQgRm9ybWF0QWxpZ25DZW50ZXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduQ2VudGVyJztcbmltcG9ydCBGb3JtYXRBbGlnbkxlZnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdCc7XG5pbXBvcnQgRm9ybWF0QWxpZ25SaWdodEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodCc7XG5pbXBvcnQgRm9ybWF0QWxpZ25KdXN0aWZ5SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnknO1xuXG5pbXBvcnQgQXR0YWNoRmlsZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZSc7XG5pbXBvcnQgSW5zZXJ0TGlua0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0TGluayc7XG5pbXBvcnQgSW5zZXJ0UGhvdG9JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvJztcbmltcG9ydCBJbnNlcnRFbW90aWNvbkljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0RW1vdGljb24nO1xuaW1wb3J0IEluc2VydENvbW1lbnRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQnO1xuXG4vLyBpbXBvcnQgVmVydGljYWxBbGlnblRvcEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnblRvcCc7XG4vLyBpbXBvcnQgVmVydGljYWxBbGlnbkJvdHRvbUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnbkJvdHRvbSc7XG4vLyBpbXBvcnQgVmVydGljYWxBbGlnbkNlbnRlckljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVmVydGljYWxBbGlnbkNlbnRlcic7XG5cbi8vIGltcG9ydCBXcmFwVGV4dEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvV3JhcFRleHQnO1xuXG5pbXBvcnQgSGlnaGxpZ2h0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQnO1xuaW1wb3J0IEZ1bmN0aW9uc0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zJztcblxuaW1wb3J0IHtcbiAgRWRpdG9yLFxuICBSaWNoVXRpbHMsXG4gIC8vIEVudGl0eSxcbiAgLy8gQ29tcG9zaXRlRGVjb3JhdG9yLFxuICBBdG9taWNCbG9ja1V0aWxzLFxuICBFZGl0b3JTdGF0ZSxcbiAgLy8gICBjb252ZXJ0VG9SYXcsXG59IGZyb20gJ2RyYWZ0LWpzJztcblxuLy8gaW1wb3J0IHtcbi8vICAgaGFzaHRhZ1N0cmF0ZWd5LFxuLy8gICBIYXNodGFnU3BhbixcblxuLy8gICBoYW5kbGVTdHJhdGVneSxcbi8vICAgSGFuZGxlU3Bhbixcbi8vIH0gZnJvbSAnLi9jb21wb25lbnRzL0RlY29yYXRvcnMnO1xuXG5pbXBvcnQgQXRvbWljIGZyb20gJy4vY29tcG9uZW50cy9BdG9taWMnO1xuaW1wb3J0IHN0eWxlcyBmcm9tICcuL0VkaXRvclN0eWxlcyc7XG5cbmltcG9ydCB7IEJsb2NrcywgSEFORExFRCwgTk9UX0hBTkRMRUQgfSBmcm9tICcuL2NvbnN0YW50cyc7XG5cbi8qKlxuICogTWF5YXNoRWRpdG9yXG4gKi9cbmNsYXNzIE1heWFzaEVkaXRvciBleHRlbmRzIENvbXBvbmVudCB7XG4gIC8qKlxuICAgKiBNYXlhc2ggRWRpdG9yJ3MgcHJvcHR5cGVzIGFyZSBkZWZpbmVkIGhlcmUuXG4gICAqL1xuICBzdGF0aWMgcHJvcFR5cGVzID0ge1xuICAgIGNsYXNzZXM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAgIGVkaXRvclN0YXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG5cbiAgICBwbGFjZWhvbGRlcjogUHJvcFR5cGVzLnN0cmluZyxcblxuICAgIHJlYWRPbmx5OiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxuXG4gICAgLyoqXG4gICAgICogVGhpcyBhcGkgZnVuY3Rpb24gc2hvdWxkIGJlIGFwcGxpZWQgZm9yIGVuYWJsaW5nIHBob3RvIGFkZGluZ1xuICAgICAqIGZlYXR1cmUgaW4gTWF5YXNoIEVkaXRvci5cbiAgICAgKi9cbiAgICBhcGlQaG90b1VwbG9hZDogUHJvcFR5cGVzLmZ1bmMsXG4gIH07XG5cbiAgc3RhdGljIGRlZmF1bHRQcm9wcyA9IHtcbiAgICByZWFkT25seTogdHJ1ZSxcbiAgICBwbGFjZWhvbGRlcjogJ1dyaXRlIEhlcmUuLi4nLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKCk7XG4gICAgLy8gdGhpcy5zdGF0ZSA9IHt9O1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGJlIHN1cHBsaWVkIHRvIERyYWZ0LmpzIEVkaXRvciB0byBoYW5kbGVcbiAgICAgKiBvbkNoYW5nZSBldmVudC5cbiAgICAgKiBAZnVuY3Rpb24gb25DaGFuZ2VcbiAgICAgKiBAcGFyYW0ge09iamVjdH0gZWRpdG9yU3RhdGVcbiAgICAgKi9cbiAgICB0aGlzLm9uQ2hhbmdlID0gKGVkaXRvclN0YXRlKSA9PiB7XG4gICAgICB0aGlzLnByb3BzLm9uQ2hhbmdlKGVkaXRvclN0YXRlKTtcbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBmb2N1cyBldmVudCBpbiBEcmFmdC5qcyBFZGl0b3IuXG4gICAgICogQGZ1bmN0aW9uIGZvY3VzXG4gICAgICovXG4gICAgdGhpcy5mb2N1cyA9ICgpID0+IHRoaXMuZWRpdG9yTm9kZS5mb2N1cygpO1xuXG4gICAgdGhpcy5vblRhYiA9IHRoaXMub25UYWIuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uVGl0bGVDbGljayA9IHRoaXMub25UaXRsZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNvZGVDbGljayA9IHRoaXMub25Db2RlQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uUXVvdGVDbGljayA9IHRoaXMub25RdW90ZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrID0gdGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrID0gdGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkJvbGRDbGljayA9IHRoaXMub25Cb2xkQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uSXRhbGljQ2xpY2sgPSB0aGlzLm9uSXRhbGljQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uVW5kZXJMaW5lQ2xpY2sgPSB0aGlzLm9uVW5kZXJMaW5lQ2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uSGlnaGxpZ2h0Q2xpY2sgPSB0aGlzLm9uSGlnaGxpZ2h0Q2xpY2suYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUtleUNvbW1hbmQgPSB0aGlzLmhhbmRsZUtleUNvbW1hbmQuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uQ2xpY2tJbnNlcnRQaG90byA9IHRoaXMub25DbGlja0luc2VydFBob3RvLmJpbmQodGhpcyk7XG5cbiAgICAvLyB0aGlzLmJsb2NrUmVuZGVyZXJGbiA9IHRoaXMuYmxvY2tSZW5kZXJlckZuLmJpbmQodGhpcyk7XG5cbiAgICAvLyBjb25zdCBjb21wb3NpdGVEZWNvcmF0b3IgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKFtcbiAgICAvLyAgIHtcbiAgICAvLyAgICAgc3RyYXRlZ3k6IGhhbmRsZVN0cmF0ZWd5LFxuICAgIC8vICAgICBjb21wb25lbnQ6IEhhbmRsZVNwYW4sXG4gICAgLy8gICB9LFxuICAgIC8vICAge1xuICAgIC8vICAgICBzdHJhdGVneTogaGFzaHRhZ1N0cmF0ZWd5LFxuICAgIC8vICAgICBjb21wb25lbnQ6IEhhc2h0YWdTcGFuLFxuICAgIC8vICAgfSxcbiAgICAvLyBdKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBvblRhYigpIHdpbGwgaGFuZGxlIFRhYiBidXR0b24gcHJlc3MgZXZlbnQgaW4gRWRpdG9yLlxuICAgKiBAZnVuY3Rpb24gb25UYWJcbiAgICogQHBhcmFtIHtFdmVudH0gZVxuICAgKi9cbiAgb25UYWIoZSkge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMub25UYWIoZSwgZWRpdG9yU3RhdGUsIDQpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBUaXRsZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvblRpdGxlQ2xpY2tcbiAgICovXG4gIG9uVGl0bGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnaGVhZGVyLW9uZScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBDb2RlIEJ1dHRvbi5cbiAgICogQGZ1bmN0aW9uIG9uQ29kZUNsaWNrXG4gICAqL1xuICBvbkNvZGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnY29kZS1ibG9jaycpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGhhbmRsZSBldmVudCBvbiBRdW90ZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvblF1b3RlQ2xpY2tcbiAgICovXG4gIG9uUXVvdGVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKGVkaXRvclN0YXRlLCAnYmxvY2txdW90ZScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIHVwZGF0ZSBibG9jayB3aXRoIHVub3JkZXJlZCBsaXN0IGl0ZW1zXG4gICAqIEBmdW5jdGlvbiBvbkxpc3RCdWxsZXRlZENsaWNrXG4gICAqL1xuICBvbkxpc3RCdWxsZXRlZENsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVCbG9ja1R5cGUoXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICApO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIHVwZGF0ZSBibG9jayB3aXRoIG9yZGVyZWQgbGlzdCBpdGVtLlxuICAgKiBAZnVuY3Rpb24gb25MaXN0TnVtYmVyZWRDbGlja1xuICAgKi9cbiAgb25MaXN0TnVtYmVyZWRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICAnb3JkZXJlZC1saXN0LWl0ZW0nLFxuICAgICk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBzZWxlY3RlZCBjb250ZW50IEJvbGQgc3R5bGluZy5cbiAgICogQGZ1bmN0aW9uIG9uQm9sZENsaWNrXG4gICAqL1xuICBvbkJvbGRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdCT0xEJyk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBpdGFsaWMgc3R5bGluZyB0byBzZWxlY3RlZCBjb250ZW50LlxuICAgKiBAZnVuY3Rpb24gb25JdGFsaWNDbGlja1xuICAgKi9cbiAgb25JdGFsaWNDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoZWRpdG9yU3RhdGUsICdJVEFMSUMnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBnaXZlIHVuZGVybGluZSBzdHlsaW5nIHRvIHNlbGVjdGVkIGNvbnRlbnQuXG4gICAqIEBmdW5jdGlvbiBvblVuZGVyTGluZUNsaWNrXG4gICAqL1xuICBvblVuZGVyTGluZUNsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVJbmxpbmVTdHlsZShcbiAgICAgIGVkaXRvclN0YXRlLFxuICAgICAgJ1VOREVSTElORScsXG4gICAgKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoaWdobGlnaHQgc2VsZWN0ZWQgY29udGVudC5cbiAgICogQGZ1bmN0aW9uIG9uSGlnaGxpZ2h0Q2xpY2tcbiAgICovXG4gIG9uSGlnaGxpZ2h0Q2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnQ09ERScpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICpcbiAgICovXG4gIG9uQ2xpY2tJbnNlcnRQaG90bygpIHtcbiAgICB0aGlzLnBob3RvLnZhbHVlID0gbnVsbDtcbiAgICB0aGlzLnBob3RvLmNsaWNrKCk7XG4gIH1cblxuICAvKipcbiAgICpcbiAgICovXG4gIG9uQ2hhbmdlSW5zZXJ0UGhvdG8gPSBhc3luYyAoZSkgPT4ge1xuICAgIC8vIGUucHJldmVudERlZmF1bHQoKTtcbiAgICBjb25zdCBmaWxlID0gZS50YXJnZXQuZmlsZXNbMF07XG5cbiAgICBpZiAoZmlsZS50eXBlLmluZGV4T2YoJ2ltYWdlLycpID09PSAwKSB7XG4gICAgICBjb25zdCBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xuICAgICAgZm9ybURhdGEuYXBwZW5kKCdwaG90bycsIGZpbGUpO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCB0aGlzLnByb3BzLmFwaVBob3RvVXBsb2FkKHtcbiAgICAgICAgZm9ybURhdGEsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIC8vIGhhbmRsZSBFcnJvclxuICAgICAgICBjb25zb2xlLmVycm9yKHN0YXR1c0NvZGUsIGVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBjb25zdCB7IHBob3RvVXJsOiBzcmMgfSA9IHBheWxvYWQ7XG5cbiAgICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICAgIGNvbnN0IGNvbnRlbnRTdGF0ZSA9IGVkaXRvclN0YXRlLmdldEN1cnJlbnRDb250ZW50KCk7XG4gICAgICBjb25zdCBjb250ZW50U3RhdGVXaXRoRW50aXR5ID0gY29udGVudFN0YXRlLmNyZWF0ZUVudGl0eShcbiAgICAgICAgQmxvY2tzLlBIT1RPLFxuICAgICAgICAnSU1NVVRBQkxFJyxcbiAgICAgICAgeyBzcmMgfSxcbiAgICAgICk7XG5cbiAgICAgIGNvbnN0IGVudGl0eUtleSA9IGNvbnRlbnRTdGF0ZVdpdGhFbnRpdHkuZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkoKTtcblxuICAgICAgY29uc3QgbWlkZGxlRWRpdG9yU3RhdGUgPSBFZGl0b3JTdGF0ZS5zZXQoZWRpdG9yU3RhdGUsIHtcbiAgICAgICAgY3VycmVudENvbnRlbnQ6IGNvbnRlbnRTdGF0ZVdpdGhFbnRpdHksXG4gICAgICB9KTtcblxuICAgICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBBdG9taWNCbG9ja1V0aWxzLmluc2VydEF0b21pY0Jsb2NrKFxuICAgICAgICBtaWRkbGVFZGl0b3JTdGF0ZSxcbiAgICAgICAgZW50aXR5S2V5LFxuICAgICAgICAnICcsXG4gICAgICApO1xuXG4gICAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICB9XG4gIH07XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBnaXZlIGN1c3RvbSBoYW5kbGUgY29tbWFuZHMgdG8gRWRpdG9yLlxuICAgKiBAZnVuY3Rpb24gaGFuZGxlS2V5Q29tbWFuZFxuICAgKiBAcGFyYW0ge3N0cmluZ30gY29tbWFuZFxuICAgKiBAcmV0dXJuIHtzdHJpbmd9XG4gICAqL1xuICBoYW5kbGVLZXlDb21tYW5kKGNvbW1hbmQpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMuaGFuZGxlS2V5Q29tbWFuZChlZGl0b3JTdGF0ZSwgY29tbWFuZCk7XG5cbiAgICBpZiAobmV3RWRpdG9yU3RhdGUpIHtcbiAgICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICAgICAgcmV0dXJuIEhBTkRMRUQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIE5PVF9IQU5ETEVEO1xuICB9XG5cbiAgLyogZXNsaW50LWRpc2FibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuICBibG9ja1JlbmRlcmVyRm4oYmxvY2spIHtcbiAgICBzd2l0Y2ggKGJsb2NrLmdldFR5cGUoKSkge1xuICAgICAgY2FzZSBCbG9ja3MuQVRPTUlDOlxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGNvbXBvbmVudDogQXRvbWljLFxuICAgICAgICAgIGVkaXRhYmxlOiBmYWxzZSxcbiAgICAgICAgfTtcblxuICAgICAgZGVmYXVsdDpcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG4gIC8qIGVzbGludC1lbmFibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuXG4gIC8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbiAgYmxvY2tTdHlsZUZuKGJsb2NrKSB7XG4gICAgc3dpdGNoIChibG9jay5nZXRUeXBlKCkpIHtcbiAgICAgIGNhc2UgJ2Jsb2NrcXVvdGUnOlxuICAgICAgICByZXR1cm4gJ1JpY2hFZGl0b3ItYmxvY2txdW90ZSc7XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3Qge1xuICAgICAgY2xhc3NlcyxcbiAgICAgIHJlYWRPbmx5LFxuICAgICAgb25DaGFuZ2UsXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgIHBsYWNlaG9sZGVyLFxuICAgIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgLy8gQ3VzdG9tIG92ZXJyaWRlcyBmb3IgXCJjb2RlXCIgc3R5bGUuXG4gICAgY29uc3Qgc3R5bGVNYXAgPSB7XG4gICAgICBDT0RFOiB7XG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwgMCwgMCwgMC4wNSknLFxuICAgICAgICBmb250RmFtaWx5OiAnXCJJbmNvbnNvbGF0YVwiLCBcIk1lbmxvXCIsIFwiQ29uc29sYXNcIiwgbW9ub3NwYWNlJyxcbiAgICAgICAgZm9udFNpemU6IDE2LFxuICAgICAgICBwYWRkaW5nOiAyLFxuICAgICAgfSxcbiAgICB9O1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7IXJlYWRPbmx5ID8gKFxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHsgJ2VkaXRvci1jb250cm9scyc6ICcnIH0pfT5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiVGl0bGVcIiBpZD1cInRpdGxlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJUaXRsZVwiIG9uQ2xpY2s9e3RoaXMub25UaXRsZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8VGl0bGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQm9sZFwiIGlkPVwiYm9sZFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQm9sZFwiIG9uQ2xpY2s9e3RoaXMub25Cb2xkQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRCb2xkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkl0YWxpY1wiIGlkPVwiaXRhbGljXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJdGFsaWNcIiBvbkNsaWNrPXt0aGlzLm9uSXRhbGljQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRJdGFsaWMgLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJVbmRlcmxpbmVcIiBpZD1cInVuZGVybGluZVwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVbmRlcmxpbmVcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25VbmRlckxpbmVDbGlja31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRVbmRlcmxpbmVkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkNvZGVcIiBpZD1cImNvZGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkNvZGVcIiBvbkNsaWNrPXt0aGlzLm9uQ29kZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8Q29kZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJRdW90ZVwiIGlkPVwicXVvdGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIlF1b3RlXCIgb25DbGljaz17dGhpcy5vblF1b3RlQ2xpY2t9PlxuICAgICAgICAgICAgICAgIDxGb3JtYXRRdW90ZUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJVbm9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgIGlkPVwidW5vcmRlcmQtbGlzdFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIlVub3JkZXJlZCBMaXN0XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uTGlzdEJ1bGxldGVkQ2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0TGlzdEJ1bGxldGVkSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIk9yZGVyZWQgTGlzdFwiIGlkPVwib3JkZXJlZC1saXN0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIk9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkxpc3ROdW1iZXJlZENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZvcm1hdExpc3ROdW1iZXJlZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBMZWZ0XCIgaWQ9XCJhbGlnbi1sZWZ0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBbGlnbiBMZWZ0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduTGVmdEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBDZW50ZXJcIiBpZD1cImFsaWduLWNlbnRlclwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gQ2VudGVyXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduQ2VudGVySWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkFsaWduIFJpZ2h0XCIgaWQ9XCJhbGlnbi1yaWdodFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gUmlnaHRcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0QWxpZ25SaWdodEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBSaWdodFwiIGlkPVwiYWxpZ24tcmlnaHRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIFJpZ2h0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduSnVzdGlmeUljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBdHRhY2ggRmlsZVwiIGlkPVwiYXR0YWNoLWZpbGVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkF0dGFjaCBGaWxlXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEF0dGFjaEZpbGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiSW5zZXJ0IExpbmtcIiBpZD1cImluc2VydC1saW5rXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgTGlua1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRMaW5rSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkluc2VydCBQaG90b1wiIGlkPVwiaW5zZXJ0LXBob3RvXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkluc2VydCBQaG90b1wiXG4gICAgICAgICAgICAgICAgZGlzYWJsZWQ9e3R5cGVvZiB0aGlzLnByb3BzLmFwaVBob3RvVXBsb2FkICE9PSAnZnVuY3Rpb24nfVxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25DbGlja0luc2VydFBob3RvfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEluc2VydFBob3RvSWNvbiAvPlxuICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgdHlwZT1cImZpbGVcIlxuICAgICAgICAgICAgICAgICAgYWNjZXB0PVwiaW1hZ2UvanBlZ3xwbmd8Z2lmXCJcbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlSW5zZXJ0UGhvdG99XG4gICAgICAgICAgICAgICAgICByZWY9eyhwaG90bykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnBob3RvID0gcGhvdG87XG4gICAgICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICAgICAgc3R5bGU9e3sgZGlzcGxheTogJ25vbmUnIH19XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJJbnNlcnQgRW1vdGljb25cIlxuICAgICAgICAgICAgICBpZD1cImluc2VydEUtZW1vdGljb25cIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IEVtb3RpY29uXCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEluc2VydEVtb3RpY29uSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkluc2VydCBDb21tZW50XCJcbiAgICAgICAgICAgICAgaWQ9XCJpbnNlcnQtY29tbWVudFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJJbnNlcnQgQ29tbWVudFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRDb21tZW50SWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkhpZ2hsaWdodCBUZXh0XCJcbiAgICAgICAgICAgICAgaWQ9XCJoaWdobGlnaHQtdGV4dFwiXG4gICAgICAgICAgICAgIHBsYWNlbWVudD1cImJvdHRvbVwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uXG4gICAgICAgICAgICAgICAgYXJpYS1sYWJlbD1cIkhpZ2hsaWdodCBUZXh0XCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uSGlnaGxpZ2h0Q2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8SGlnaGxpZ2h0SWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcFxuICAgICAgICAgICAgICB0aXRsZT1cIkFkZCBGdW5jdGlvbnNcIlxuICAgICAgICAgICAgICBpZD1cImFkZC1mdW5jdGlvbnNcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWRkIEZ1bmN0aW9uc1wiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGdW5jdGlvbnNJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgKSA6IG51bGx9XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc05hbWVzKHsgJ2VkaXRvci1hcmVhJzogJycgfSl9PlxuICAgICAgICAgIDxFZGl0b3JcbiAgICAgICAgICAgIC8qIEJhc2ljcyAqL1xuXG4gICAgICAgICAgICBlZGl0b3JTdGF0ZT17ZWRpdG9yU3RhdGV9XG4gICAgICAgICAgICBvbkNoYW5nZT17b25DaGFuZ2V9XG4gICAgICAgICAgICAvKiBQcmVzZW50YXRpb24gKi9cblxuICAgICAgICAgICAgcGxhY2Vob2xkZXI9e3BsYWNlaG9sZGVyfVxuICAgICAgICAgICAgLy8gdGV4dEFsaWdubWVudD1cImNlbnRlclwiXG5cbiAgICAgICAgICAgIC8vIHRleHREaXJlY3Rpb25hbGl0eT1cIkxUUlwiXG5cbiAgICAgICAgICAgIGJsb2NrUmVuZGVyZXJGbj17dGhpcy5ibG9ja1JlbmRlcmVyRm59XG4gICAgICAgICAgICBibG9ja1N0eWxlRm49e3RoaXMuYmxvY2tTdHlsZUZufVxuICAgICAgICAgICAgY3VzdG9tU3R5bGVNYXA9e3N0eWxlTWFwfVxuICAgICAgICAgICAgLy8gY3VzdG9tU3R5bGVGbj17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8qIEJlaGF2aW9yICovXG5cbiAgICAgICAgICAgIC8vIGF1dG9DYXBpdGFsaXplPVwic2VudGVuY2VzXCJcblxuICAgICAgICAgICAgLy8gYXV0b0NvbXBsZXRlPVwib2ZmXCJcblxuICAgICAgICAgICAgLy8gYXV0b0NvcnJlY3Q9XCJvZmZcIlxuXG4gICAgICAgICAgICByZWFkT25seT17cmVhZE9ubHl9XG4gICAgICAgICAgICBzcGVsbENoZWNrXG4gICAgICAgICAgICAvLyBzdHJpcFBhc3RlZFN0eWxlcz17ZmFsc2V9XG5cbiAgICAgICAgICAgIC8qIERPTSBhbmQgQWNjZXNzaWJpbGl0eSAqL1xuXG4gICAgICAgICAgICAvLyBlZGl0b3JLZXlcblxuICAgICAgICAgICAgLyogQ2FuY2VsYWJsZSBIYW5kbGVycyAqL1xuXG4gICAgICAgICAgICAvLyBoYW5kbGVSZXR1cm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICBoYW5kbGVLZXlDb21tYW5kPXt0aGlzLmhhbmRsZUtleUNvbW1hbmR9XG4gICAgICAgICAgICAvLyBoYW5kbGVCZWZvcmVJbnB1dD17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZVBhc3RlZFRleHQ9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVQYXN0ZWRGaWxlcz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZURyb3BwZWRGaWxlcz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZURyb3A9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBLZXkgSGFuZGxlcnMgKi9cblxuICAgICAgICAgICAgLy8gb25Fc2NhcGU9eygpID0+IHt9fVxuXG4gICAgICAgICAgICBvblRhYj17dGhpcy5vblRhYn1cbiAgICAgICAgICAgIC8vIG9uVXBBcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uUmlnaHRBcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uRG93bkFycm93PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gb25MZWZ0QXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBrZXlCaW5kaW5nRm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBNb3VzZSBFdmVudCAqL1xuXG4gICAgICAgICAgICAvLyBvbkZvY3VzPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gb25CbHVyPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogTWV0aG9kcyAqL1xuXG4gICAgICAgICAgICAvLyBmb2N1cz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIGJsdXI9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBGb3IgUmVmZXJlbmNlICovXG5cbiAgICAgICAgICAgIHJlZj17KG5vZGUpID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5lZGl0b3JOb2RlID0gbm9kZTtcbiAgICAgICAgICAgIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHdpdGhTdHlsZXMoc3R5bGVzKShNYXlhc2hFZGl0b3IpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvci5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCB7IEVkaXRvclN0YXRlLCBjb252ZXJ0RnJvbVJhdywgQ29tcG9zaXRlRGVjb3JhdG9yIH0gZnJvbSAnZHJhZnQtanMnO1xuXG5pbXBvcnQge1xuICBoYXNodGFnU3RyYXRlZ3ksXG4gIEhhc2h0YWdTcGFuLFxuICBoYW5kbGVTdHJhdGVneSxcbiAgSGFuZGxlU3Bhbixcbn0gZnJvbSAnLi9jb21wb25lbnRzL0RlY29yYXRvcnMnO1xuXG5jb25zdCBkZWZhdWx0RGVjb3JhdG9ycyA9IG5ldyBDb21wb3NpdGVEZWNvcmF0b3IoW1xuICB7XG4gICAgc3RyYXRlZ3k6IGhhbmRsZVN0cmF0ZWd5LFxuICAgIGNvbXBvbmVudDogSGFuZGxlU3BhbixcbiAgfSxcbiAge1xuICAgIHN0cmF0ZWd5OiBoYXNodGFnU3RyYXRlZ3ksXG4gICAgY29tcG9uZW50OiBIYXNodGFnU3BhbixcbiAgfSxcbl0pO1xuXG5leHBvcnQgY29uc3QgY3JlYXRlRWRpdG9yU3RhdGUgPSAoXG4gIGNvbnRlbnQgPSBudWxsLFxuICBkZWNvcmF0b3JzID0gZGVmYXVsdERlY29yYXRvcnMsXG4pID0+IHtcbiAgaWYgKGNvbnRlbnQgPT09IG51bGwpIHtcbiAgICByZXR1cm4gRWRpdG9yU3RhdGUuY3JlYXRlRW1wdHkoZGVjb3JhdG9ycyk7XG4gIH1cbiAgcmV0dXJuIEVkaXRvclN0YXRlLmNyZWF0ZVdpdGhDb250ZW50KGNvbnZlcnRGcm9tUmF3KGNvbnRlbnQpLCBkZWNvcmF0b3JzKTtcbn07XG5cbmV4cG9ydCBkZWZhdWx0IGNyZWF0ZUVkaXRvclN0YXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL0VkaXRvclN0YXRlLmpzIiwiLyoqXG4gKiBUaGlzIGZpbGUgY29udGFpbnMgYWxsIHRoZSBDU1MtaW4tSlMgc3R5bGVzIG9mIEVkaXRvciBjb21wb25lbnQuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgJ0BnbG9iYWwnOiB7XG4gICAgJy5SaWNoRWRpdG9yLXJvb3QnOiB7XG4gICAgICBiYWNrZ3JvdW5kOiAnI2ZmZicsXG4gICAgICBib3JkZXI6ICcxcHggc29saWQgI2RkZCcsXG4gICAgICBmb250RmFtaWx5OiBcIidHZW9yZ2lhJywgc2VyaWZcIixcbiAgICAgIGZvbnRTaXplOiAnMTRweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgfSxcbiAgICAnLlJpY2hFZGl0b3ItZWRpdG9yJzoge1xuICAgICAgYm9yZGVyVG9wOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgICAgY3Vyc29yOiAndGV4dCcsXG4gICAgICBmb250U2l6ZTogJzE2cHgnLFxuICAgICAgbWFyZ2luVG9wOiAnMTBweCcsXG4gICAgfSxcbiAgICAnLnB1YmxpYy1EcmFmdEVkaXRvclBsYWNlaG9sZGVyLXJvb3QnOiB7XG4gICAgICBtYXJnaW46ICcwIC0xNXB4IC0xNXB4JyxcbiAgICAgIHBhZGRpbmc6ICcxNXB4JyxcbiAgICB9LFxuICAgICcucHVibGljLURyYWZ0RWRpdG9yLWNvbnRlbnQnOiB7XG4gICAgICBtYXJnaW46ICcwIC0xNXB4IC0xNXB4JyxcbiAgICAgIHBhZGRpbmc6ICcxNXB4JyxcbiAgICAgIC8vIG1pbkhlaWdodDogJzEwMHB4JyxcbiAgICB9LFxuICAgICcuUmljaEVkaXRvci1ibG9ja3F1b3RlJzoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiAnNXB4IHNvbGlkICNlZWUnLFxuICAgICAgYm9yZGVyTGVmdDogJzVweCBzb2xpZCAjZWVlJyxcbiAgICAgIGNvbG9yOiAnIzY2NicsXG4gICAgICBmb250RmFtaWx5OiBcIidIb2VmbGVyIFRleHQnLCAnR2VvcmdpYScsIHNlcmlmXCIsXG4gICAgICBmb250U3R5bGU6ICdpdGFsaWMnLFxuICAgICAgbWFyZ2luOiAnMTZweCAwJyxcbiAgICAgIHBhZGRpbmc6ICcxMHB4IDIwcHgnLFxuICAgIH0sXG4gICAgJy5wdWJsaWMtRHJhZnRTdHlsZURlZmF1bHQtcHJlJzoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSgwLCAwLCAwLCAwLjA1KScsXG4gICAgICBmb250RmFtaWx5OiBcIidJbmNvbnNvbGF0YScsICdNZW5sbycsICdDb25zb2xhcycsIG1vbm9zcGFjZVwiLFxuICAgICAgZm9udFNpemU6ICcxNnB4JyxcbiAgICAgIHBhZGRpbmc6ICcyMHB4JyxcbiAgICB9LFxuICB9LFxuICByb290OiB7XG4gICAgLy8gcGFkZGluZzogJzElJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdHlsZXMuanMiLCIvKipcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IEJsb2NrcyB9IGZyb20gJy4uL2NvbnN0YW50cyc7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcGhvdG86IHtcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIC8vIEZpeCBhbiBpc3N1ZSB3aXRoIEZpcmVmb3ggcmVuZGVyaW5nIHZpZGVvIGNvbnRyb2xzXG4gICAgLy8gd2l0aCAncHJlLXdyYXAnIHdoaXRlLXNwYWNlXG4gICAgd2hpdGVTcGFjZTogJ2luaXRpYWwnLFxuICB9LFxufTtcblxuLyoqXG4gKlxuICogQGNsYXNzIEF0b21pYyAtIHRoaXMgUmVhY3QgY29tcG9uZW50IHdpbGwgYmUgdXNlZCB0byByZW5kZXIgQXRvbWljXG4gKiBjb21wb25lbnRzIG9mIERyYWZ0LmpzXG4gKlxuICogQHRvZG8gLSBjb25maWd1cmUgdGhpcyBmb3IgYXVkaW8uXG4gKiBAdG9kbyAtIGNvbmZpZ3VyZSB0aGlzIGZvciB2aWRlby5cbiAqL1xuY2xhc3MgQXRvbWljIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjb250ZW50U3RhdGU6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgICBibG9jazogUHJvcFR5cGVzLmFueS5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7fTtcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNvbnRlbnRTdGF0ZSwgYmxvY2sgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBlbnRpdHkgPSBjb250ZW50U3RhdGUuZ2V0RW50aXR5KGJsb2NrLmdldEVudGl0eUF0KDApKTtcbiAgICBjb25zdCB7IHNyYyB9ID0gZW50aXR5LmdldERhdGEoKTtcbiAgICBjb25zdCB0eXBlID0gZW50aXR5LmdldFR5cGUoKTtcblxuICAgIGlmICh0eXBlID09PSBCbG9ja3MuUEhPVE8pIHtcbiAgICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgPGltZyBhbHQ9eydhbHQnfSBzcmM9e3NyY30gc3R5bGU9e3N0eWxlcy5waG90b30gLz5cbiAgICAgICAgPC9kaXY+XG4gICAgICApO1xuICAgIH1cblxuICAgIHJldHVybiA8ZGl2IC8+O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IEF0b21pYztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0F0b21pYy5qcyIsIi8qIGVzbGludC1kaXNhYmxlICovXG4vLyBlc2xpbnQgaXMgZGlzYWJsZWQgaGVyZSBmb3Igbm93LlxuaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcbmltcG9ydCBzdHlsZSBmcm9tICcuLi9zdHlsZSc7XG5cbmNvbnN0IEhBTkRMRV9SRUdFWCA9IC9cXEBbXFx3XSsvZztcbmNvbnN0IEhBU0hUQUdfUkVHRVggPSAvXFwjW1xcd1xcdTA1OTAtXFx1MDVmZl0rL2c7XG5cbmZ1bmN0aW9uIGZpbmRXaXRoUmVnZXgocmVnZXgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spIHtcbiAgY29uc3QgdGV4dCA9IGNvbnRlbnRCbG9jay5nZXRUZXh0KCk7XG4gIGxldCBtYXRjaEFycixcbiAgICBzdGFydDtcbiAgd2hpbGUgKChtYXRjaEFyciA9IHJlZ2V4LmV4ZWModGV4dCkpICE9PSBudWxsKSB7XG4gICAgc3RhcnQgPSBtYXRjaEFyci5pbmRleDtcbiAgICBjYWxsYmFjayhzdGFydCwgc3RhcnQgKyBtYXRjaEFyclswXS5sZW5ndGgpO1xuICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBoYW5kbGVTdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrLCBjb250ZW50U3RhdGUpIHtcbiAgZmluZFdpdGhSZWdleChIQU5ETEVfUkVHRVgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFzaHRhZ1N0cmF0ZWd5KGNvbnRlbnRCbG9jaywgY2FsbGJhY2ssIGNvbnRlbnRTdGF0ZSkge1xuICBmaW5kV2l0aFJlZ2V4KEhBU0hUQUdfUkVHRVgsIGNvbnRlbnRCbG9jaywgY2FsbGJhY2spO1xufVxuXG5leHBvcnQgY29uc3QgSGFuZGxlU3BhbiA9IHByb3BzID0+IDxzcGFuIHN0eWxlPXtzdHlsZS5oYW5kbGV9Pntwcm9wcy5jaGlsZHJlbn08L3NwYW4+O1xuXG5leHBvcnQgY29uc3QgSGFzaHRhZ1NwYW4gPSBwcm9wcyA9PiA8c3BhbiBzdHlsZT17c3R5bGUuaGFzaHRhZ30+e3Byb3BzLmNoaWxkcmVufTwvc3Bhbj47XG5cblxuZXhwb3J0IGRlZmF1bHQge1xuICBoYW5kbGVTdHJhdGVneSxcbiAgSGFuZGxlU3BhbixcblxuICBoYXNodGFnU3RyYXRlZ3ksXG4gIEhhc2h0YWdTcGFuLFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0RlY29yYXRvcnMuanMiLCIvKipcbiAqIFNvbWUgb2YgdGhlIGNvbnN0YW50cyB3aGljaCBhcmUgdXNlZCB0aHJvdWdob3V0IHRoaXMgcHJvamVjdCBpbnN0ZWFkIG9mXG4gKiBkaXJlY3RseSB1c2luZyBzdHJpbmcuXG4gKlxuICogQGZvcm1hdFxuICovXG5cbi8qKlxuICogQGNvbnN0YW50IEJsb2Nrc1xuICovXG5leHBvcnQgY29uc3QgQmxvY2tzID0ge1xuICBVTlNUWUxFRDogJ3Vuc3R5bGVkJyxcbiAgUEFSQUdSQVBIOiAndW5zdHlsZWQnLFxuXG4gIEgxOiAnaGVhZGVyLW9uZScsXG4gIEgyOiAnaGVhZGVyLXR3bycsXG4gIEgzOiAnaGVhZGVyLXRocmVlJyxcbiAgSDQ6ICdoZWFkZXItZm91cicsXG4gIEg1OiAnaGVhZGVyLWZpdmUnLFxuICBINjogJ2hlYWRlci1zaXgnLFxuXG4gIE9MOiAnb3JkZXJlZC1saXN0LWl0ZW0nLFxuICBVTDogJ3Vub3JkZXJlZC1saXN0LWl0ZW0nLFxuXG4gIENPREU6ICdjb2RlLWJsb2NrJyxcblxuICBCTE9DS1FVT1RFOiAnYmxvY2txdW90ZScsXG5cbiAgQVRPTUlDOiAnYXRvbWljJyxcbiAgUEhPVE86ICdhdG9taWM6cGhvdG8nLFxuICBWSURFTzogJ2F0b21pYzp2aWRlbycsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBJbmxpbmVcbiAqL1xuZXhwb3J0IGNvbnN0IElubGluZSA9IHtcbiAgQk9MRDogJ0JPTEQnLFxuICBDT0RFOiAnQ09ERScsXG4gIElUQUxJQzogJ0lUQUxJQycsXG4gIFNUUklLRVRIUk9VR0g6ICdTVFJJS0VUSFJPVUdIJyxcbiAgVU5ERVJMSU5FOiAnVU5ERVJMSU5FJyxcbiAgSElHSExJR0hUOiAnSElHSExJR0hUJyxcbn07XG5cbi8qKlxuICogQGNvbnN0YW50IEVudGl0eVxuICovXG5leHBvcnQgY29uc3QgRW50aXR5ID0ge1xuICBMSU5LOiAnTElOSycsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBIWVBFUkxJTktcbiAqL1xuZXhwb3J0IGNvbnN0IEhZUEVSTElOSyA9ICdoeXBlcmxpbmsnO1xuXG4vKipcbiAqIENvbnN0YW50cyB0byBoYW5kbGUga2V5IGNvbW1hbmRzXG4gKi9cbmV4cG9ydCBjb25zdCBIQU5ETEVEID0gJ2hhbmRsZWQnO1xuZXhwb3J0IGNvbnN0IE5PVF9IQU5ETEVEID0gJ25vdF9oYW5kbGVkJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBCbG9ja3MsXG4gIElubGluZSxcbiAgRW50aXR5LFxufTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb25zdGFudHMuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgRWRpdG9yIGZyb20gJy4vRWRpdG9yJztcblxuZXhwb3J0IHsgTWF5YXNoRWRpdG9yIH0gZnJvbSAnLi9FZGl0b3InO1xuXG5leHBvcnQgeyBjcmVhdGVFZGl0b3JTdGF0ZSB9IGZyb20gJy4vRWRpdG9yU3RhdGUnO1xuXG5leHBvcnQgZGVmYXVsdCBFZGl0b3I7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvaW5kZXguanMiLCIvKiogQGZvcm1hdCAqL1xuXG5leHBvcnQgY29uc3Qgc3R5bGUgPSB7XG4gIHJvb3Q6IHtcbiAgICBwYWRkaW5nOiAyMCxcbiAgICB3aWR0aDogNjAwLFxuICB9LFxuICBlZGl0b3I6IHtcbiAgICBib3JkZXI6ICcxcHggc29saWQgI2RkZCcsXG4gICAgY3Vyc29yOiAndGV4dCcsXG4gICAgZm9udFNpemU6IDE2LFxuICAgIG1pbkhlaWdodDogNDAsXG4gICAgcGFkZGluZzogMTAsXG4gIH0sXG4gIGJ1dHRvbjoge1xuICAgIG1hcmdpblRvcDogMTAsXG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgfSxcbiAgaGFuZGxlOiB7XG4gICAgY29sb3I6ICdyZ2JhKDk4LCAxNzcsIDI1NCwgMS4wKScsXG4gICAgZGlyZWN0aW9uOiAnbHRyJyxcbiAgICB1bmljb2RlQmlkaTogJ2JpZGktb3ZlcnJpZGUnLFxuICB9LFxuICBoYXNodGFnOiB7XG4gICAgY29sb3I6ICdyZ2JhKDk1LCAxODQsIDEzOCwgMS4wKScsXG4gIH0sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBzdHlsZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9zdHlsZS9pbmRleC5qcyJdLCJzb3VyY2VSb290IjoiIn0=