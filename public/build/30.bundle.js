webpackJsonp([30],{

/***/ "./node_modules/react-router-dom/Link.js":
/*!***********************************************!*\
  !*** ./node_modules/react-router-dom/Link.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _invariant = __webpack_require__(/*! invariant */ "./node_modules/invariant/browser.js");

var _invariant2 = _interopRequireDefault(_invariant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var isModifiedEvent = function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
};

/**
 * The public API for rendering a history-aware <a>.
 */

var Link = function (_React$Component) {
  _inherits(Link, _React$Component);

  function Link() {
    var _temp, _this, _ret;

    _classCallCheck(this, Link);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, _React$Component.call.apply(_React$Component, [this].concat(args))), _this), _this.handleClick = function (event) {
      if (_this.props.onClick) _this.props.onClick(event);

      if (!event.defaultPrevented && // onClick prevented default
      event.button === 0 && // ignore right clicks
      !_this.props.target && // let browser handle "target=_blank" etc.
      !isModifiedEvent(event) // ignore clicks with modifier keys
      ) {
          event.preventDefault();

          var history = _this.context.router.history;
          var _this$props = _this.props,
              replace = _this$props.replace,
              to = _this$props.to;


          if (replace) {
            history.replace(to);
          } else {
            history.push(to);
          }
        }
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  Link.prototype.render = function render() {
    var _props = this.props,
        replace = _props.replace,
        to = _props.to,
        innerRef = _props.innerRef,
        props = _objectWithoutProperties(_props, ['replace', 'to', 'innerRef']); // eslint-disable-line no-unused-vars

    (0, _invariant2.default)(this.context.router, 'You should not use <Link> outside a <Router>');

    var href = this.context.router.history.createHref(typeof to === 'string' ? { pathname: to } : to);

    return _react2.default.createElement('a', _extends({}, props, { onClick: this.handleClick, href: href, ref: innerRef }));
  };

  return Link;
}(_react2.default.Component);

Link.propTypes = {
  onClick: _propTypes2.default.func,
  target: _propTypes2.default.string,
  replace: _propTypes2.default.bool,
  to: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.object]).isRequired,
  innerRef: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.func])
};
Link.defaultProps = {
  replace: false
};
Link.contextTypes = {
  router: _propTypes2.default.shape({
    history: _propTypes2.default.shape({
      push: _propTypes2.default.func.isRequired,
      replace: _propTypes2.default.func.isRequired,
      createHref: _propTypes2.default.func.isRequired
    }).isRequired
  }).isRequired
};
exports.default = Link;

/***/ }),

/***/ "./src/client/containers/Home/LandingPage.js":
/*!***************************************************!*\
  !*** ./src/client/containers/Home/LandingPage.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _Link = __webpack_require__(/*! react-router-dom/Link */ "./node_modules/react-router-dom/Link.js");

var _Link2 = _interopRequireDefault(_Link);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Typography = __webpack_require__(/*! material-ui/Typography */ "./node_modules/material-ui/Typography/index.js");

var _Typography2 = _interopRequireDefault(_Typography);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = function styles(theme) {
  return {
    hero: {
      // Makes the hero full height until we get some more content.
      minHeight: '100vh',
      flex: '0 0 auto',
      display: 'flex',
      backgroundColor: 'rgba(0,0,0,.4)',
      justifyContent: 'center',
      alignItems: 'center'
    },
    content: (0, _defineProperty3.default)({
      padding: '10% 10px',
      textAlign: 'center'
    }, theme.breakpoints.up('sm'), {
      padding: '10px 10px'
    }),
    title: {
      color: theme.palette.getContrastText(theme.palette.primary[700])
    },
    subheading: {
      color: theme.palette.getContrastText(theme.palette.primary[700])
    },
    button: {
      marginTop: 20,
      backgroundColor: theme.palette.secondary[300]
    },
    logo: {
      marginBottom: '10px',
      width: '100%',
      height: '100%',
      animation: 'spin infinite 20s linear'
    },
    '@keyframes spin': {
      from: {
        transform: 'rotate(0deg)'
      },
      to: {
        transform: 'rotate(360deg)'
      }
    }
  };
};

var LandingPage = function LandingPage(_ref) {
  var classes = _ref.classes;
  return _react2.default.createElement(
    'div',
    { className: classes.hero },
    _react2.default.createElement(
      'div',
      { className: classes.content },
      _react2.default.createElement('img', {
        src: '/public/photos/mayash-logo-transparent.png',
        alt: 'Mayash Logo',
        className: classes.logo
      }),
      _react2.default.createElement(
        _Typography2.default,
        { type: 'display2', component: 'h1', className: classes.title },
        'Mayash'
      ),
      _react2.default.createElement(
        _Typography2.default,
        {
          type: 'subheading',
          component: 'h2',
          className: classes.subheading
        },
        'Transforming Education...'
      ),
      _react2.default.createElement(
        _Button2.default,
        {
          component: _Link2.default,
          className: classes.button,
          raised: true,
          to: '/introduction'
        },
        'Introduction'
      )
    )
  );
};

LandingPage.propTypes = {
  classes: _propTypes2.default.object.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(LandingPage);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Ib21lL0xhbmRpbmdQYWdlLmpzIl0sIm5hbWVzIjpbInN0eWxlcyIsInRoZW1lIiwiaGVybyIsIm1pbkhlaWdodCIsImZsZXgiLCJkaXNwbGF5IiwiYmFja2dyb3VuZENvbG9yIiwianVzdGlmeUNvbnRlbnQiLCJhbGlnbkl0ZW1zIiwiY29udGVudCIsInBhZGRpbmciLCJ0ZXh0QWxpZ24iLCJicmVha3BvaW50cyIsInVwIiwidGl0bGUiLCJjb2xvciIsInBhbGV0dGUiLCJnZXRDb250cmFzdFRleHQiLCJwcmltYXJ5Iiwic3ViaGVhZGluZyIsImJ1dHRvbiIsIm1hcmdpblRvcCIsInNlY29uZGFyeSIsImxvZ28iLCJtYXJnaW5Cb3R0b20iLCJ3aWR0aCIsImhlaWdodCIsImFuaW1hdGlvbiIsImZyb20iLCJ0cmFuc2Zvcm0iLCJ0byIsIkxhbmRpbmdQYWdlIiwiY2xhc3NlcyIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDhDQUE4QyxpQkFBaUIscUJBQXFCLG9DQUFvQyw2REFBNkQsb0JBQW9CLEVBQUUsZUFBZTs7QUFFMU4saURBQWlELDBDQUEwQywwREFBMEQsRUFBRTs7QUFFdkosaURBQWlELGFBQWEsdUZBQXVGLEVBQUUsdUZBQXVGOztBQUU5TywwQ0FBMEMsK0RBQStELHFHQUFxRyxFQUFFLHlFQUF5RSxlQUFlLHlFQUF5RSxFQUFFLEVBQUUsdUhBQXVIOztBQUU1ZTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0ZBQWdGOztBQUVoRjs7QUFFQSxnRkFBZ0YsZUFBZTs7QUFFL0YseURBQXlELFVBQVUsdURBQXVEO0FBQzFIOztBQUVBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxHQUFHO0FBQ0g7QUFDQSx1Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMzR0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFSQTs7QUFVQSxJQUFNQSxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0MsS0FBRDtBQUFBLFNBQVk7QUFDekJDLFVBQU07QUFDSjtBQUNBQyxpQkFBVyxPQUZQO0FBR0pDLFlBQU0sVUFIRjtBQUlKQyxlQUFTLE1BSkw7QUFLSkMsdUJBQWlCLGdCQUxiO0FBTUpDLHNCQUFnQixRQU5aO0FBT0pDLGtCQUFZO0FBUFIsS0FEbUI7QUFVekJDO0FBQ0VDLGVBQVMsVUFEWDtBQUVFQyxpQkFBVztBQUZiLE9BR0dWLE1BQU1XLFdBQU4sQ0FBa0JDLEVBQWxCLENBQXFCLElBQXJCLENBSEgsRUFHZ0M7QUFDNUJILGVBQVM7QUFEbUIsS0FIaEMsQ0FWeUI7QUFpQnpCSSxXQUFPO0FBQ0xDLGFBQU9kLE1BQU1lLE9BQU4sQ0FBY0MsZUFBZCxDQUE4QmhCLE1BQU1lLE9BQU4sQ0FBY0UsT0FBZCxDQUFzQixHQUF0QixDQUE5QjtBQURGLEtBakJrQjtBQW9CekJDLGdCQUFZO0FBQ1ZKLGFBQU9kLE1BQU1lLE9BQU4sQ0FBY0MsZUFBZCxDQUE4QmhCLE1BQU1lLE9BQU4sQ0FBY0UsT0FBZCxDQUFzQixHQUF0QixDQUE5QjtBQURHLEtBcEJhO0FBdUJ6QkUsWUFBUTtBQUNOQyxpQkFBVyxFQURMO0FBRU5mLHVCQUFpQkwsTUFBTWUsT0FBTixDQUFjTSxTQUFkLENBQXdCLEdBQXhCO0FBRlgsS0F2QmlCO0FBMkJ6QkMsVUFBTTtBQUNKQyxvQkFBYyxNQURWO0FBRUpDLGFBQU8sTUFGSDtBQUdKQyxjQUFRLE1BSEo7QUFJSkMsaUJBQVc7QUFKUCxLQTNCbUI7QUFpQ3pCLHVCQUFtQjtBQUNqQkMsWUFBTTtBQUNKQyxtQkFBVztBQURQLE9BRFc7QUFJakJDLFVBQUk7QUFDRkQsbUJBQVc7QUFEVDtBQUphO0FBakNNLEdBQVo7QUFBQSxDQUFmOztBQTJDQSxJQUFNRSxjQUFjLFNBQWRBLFdBQWM7QUFBQSxNQUFHQyxPQUFILFFBQUdBLE9BQUg7QUFBQSxTQUNsQjtBQUFBO0FBQUEsTUFBSyxXQUFXQSxRQUFROUIsSUFBeEI7QUFDRTtBQUFBO0FBQUEsUUFBSyxXQUFXOEIsUUFBUXZCLE9BQXhCO0FBQ0U7QUFDRSxhQUFLLDRDQURQO0FBRUUsYUFBSyxhQUZQO0FBR0UsbUJBQVd1QixRQUFRVDtBQUhyQixRQURGO0FBTUU7QUFBQTtBQUFBLFVBQVksTUFBSyxVQUFqQixFQUE0QixXQUFVLElBQXRDLEVBQTJDLFdBQVdTLFFBQVFsQixLQUE5RDtBQUFBO0FBQUEsT0FORjtBQVNFO0FBQUE7QUFBQTtBQUNFLGdCQUFLLFlBRFA7QUFFRSxxQkFBVSxJQUZaO0FBR0UscUJBQVdrQixRQUFRYjtBQUhyQjtBQUFBO0FBQUEsT0FURjtBQWdCRTtBQUFBO0FBQUE7QUFDRSxtQ0FERjtBQUVFLHFCQUFXYSxRQUFRWixNQUZyQjtBQUdFLHNCQUhGO0FBSUUsY0FBRztBQUpMO0FBQUE7QUFBQTtBQWhCRjtBQURGLEdBRGtCO0FBQUEsQ0FBcEI7O0FBOEJBVyxZQUFZRSxTQUFaLEdBQXdCO0FBQ3RCRCxXQUFTLG9CQUFVRSxNQUFWLENBQWlCQztBQURKLENBQXhCOztrQkFJZSwwQkFBV25DLE1BQVgsRUFBbUIrQixXQUFuQixDIiwiZmlsZSI6IjMwLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9leHRlbmRzID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbiAodGFyZ2V0KSB7IGZvciAodmFyIGkgPSAxOyBpIDwgYXJndW1lbnRzLmxlbmd0aDsgaSsrKSB7IHZhciBzb3VyY2UgPSBhcmd1bWVudHNbaV07IGZvciAodmFyIGtleSBpbiBzb3VyY2UpIHsgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGtleSkpIHsgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTsgfSB9IH0gcmV0dXJuIHRhcmdldDsgfTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9pbnZhcmlhbnQgPSByZXF1aXJlKCdpbnZhcmlhbnQnKTtcblxudmFyIF9pbnZhcmlhbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW52YXJpYW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKG9iaiwga2V5cykgeyB2YXIgdGFyZ2V0ID0ge307IGZvciAodmFyIGkgaW4gb2JqKSB7IGlmIChrZXlzLmluZGV4T2YoaSkgPj0gMCkgY29udGludWU7IGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iaiwgaSkpIGNvbnRpbnVlOyB0YXJnZXRbaV0gPSBvYmpbaV07IH0gcmV0dXJuIHRhcmdldDsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBpc01vZGlmaWVkRXZlbnQgPSBmdW5jdGlvbiBpc01vZGlmaWVkRXZlbnQoZXZlbnQpIHtcbiAgcmV0dXJuICEhKGV2ZW50Lm1ldGFLZXkgfHwgZXZlbnQuYWx0S2V5IHx8IGV2ZW50LmN0cmxLZXkgfHwgZXZlbnQuc2hpZnRLZXkpO1xufTtcblxuLyoqXG4gKiBUaGUgcHVibGljIEFQSSBmb3IgcmVuZGVyaW5nIGEgaGlzdG9yeS1hd2FyZSA8YT4uXG4gKi9cblxudmFyIExpbmsgPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICBfaW5oZXJpdHMoTGluaywgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gTGluaygpIHtcbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgX2NsYXNzQ2FsbENoZWNrKHRoaXMsIExpbmspO1xuXG4gICAgZm9yICh2YXIgX2xlbiA9IGFyZ3VtZW50cy5sZW5ndGgsIGFyZ3MgPSBBcnJheShfbGVuKSwgX2tleSA9IDA7IF9rZXkgPCBfbGVuOyBfa2V5KyspIHtcbiAgICAgIGFyZ3NbX2tleV0gPSBhcmd1bWVudHNbX2tleV07XG4gICAgfVxuXG4gICAgcmV0dXJuIF9yZXQgPSAoX3RlbXAgPSAoX3RoaXMgPSBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfUmVhY3QkQ29tcG9uZW50LmNhbGwuYXBwbHkoX1JlYWN0JENvbXBvbmVudCwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLmhhbmRsZUNsaWNrID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICBpZiAoX3RoaXMucHJvcHMub25DbGljaykgX3RoaXMucHJvcHMub25DbGljayhldmVudCk7XG5cbiAgICAgIGlmICghZXZlbnQuZGVmYXVsdFByZXZlbnRlZCAmJiAvLyBvbkNsaWNrIHByZXZlbnRlZCBkZWZhdWx0XG4gICAgICBldmVudC5idXR0b24gPT09IDAgJiYgLy8gaWdub3JlIHJpZ2h0IGNsaWNrc1xuICAgICAgIV90aGlzLnByb3BzLnRhcmdldCAmJiAvLyBsZXQgYnJvd3NlciBoYW5kbGUgXCJ0YXJnZXQ9X2JsYW5rXCIgZXRjLlxuICAgICAgIWlzTW9kaWZpZWRFdmVudChldmVudCkgLy8gaWdub3JlIGNsaWNrcyB3aXRoIG1vZGlmaWVyIGtleXNcbiAgICAgICkge1xuICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgICB2YXIgaGlzdG9yeSA9IF90aGlzLmNvbnRleHQucm91dGVyLmhpc3Rvcnk7XG4gICAgICAgICAgdmFyIF90aGlzJHByb3BzID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgICAgIHJlcGxhY2UgPSBfdGhpcyRwcm9wcy5yZXBsYWNlLFxuICAgICAgICAgICAgICB0byA9IF90aGlzJHByb3BzLnRvO1xuXG5cbiAgICAgICAgICBpZiAocmVwbGFjZSkge1xuICAgICAgICAgICAgaGlzdG9yeS5yZXBsYWNlKHRvKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaGlzdG9yeS5wdXNoKHRvKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LCBfdGVtcCksIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gIExpbmsucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgcmVwbGFjZSA9IF9wcm9wcy5yZXBsYWNlLFxuICAgICAgICB0byA9IF9wcm9wcy50byxcbiAgICAgICAgaW5uZXJSZWYgPSBfcHJvcHMuaW5uZXJSZWYsXG4gICAgICAgIHByb3BzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzKF9wcm9wcywgWydyZXBsYWNlJywgJ3RvJywgJ2lubmVyUmVmJ10pOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVudXNlZC12YXJzXG5cbiAgICAoMCwgX2ludmFyaWFudDIuZGVmYXVsdCkodGhpcy5jb250ZXh0LnJvdXRlciwgJ1lvdSBzaG91bGQgbm90IHVzZSA8TGluaz4gb3V0c2lkZSBhIDxSb3V0ZXI+Jyk7XG5cbiAgICB2YXIgaHJlZiA9IHRoaXMuY29udGV4dC5yb3V0ZXIuaGlzdG9yeS5jcmVhdGVIcmVmKHR5cGVvZiB0byA9PT0gJ3N0cmluZycgPyB7IHBhdGhuYW1lOiB0byB9IDogdG8pO1xuXG4gICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdhJywgX2V4dGVuZHMoe30sIHByb3BzLCB7IG9uQ2xpY2s6IHRoaXMuaGFuZGxlQ2xpY2ssIGhyZWY6IGhyZWYsIHJlZjogaW5uZXJSZWYgfSkpO1xuICB9O1xuXG4gIHJldHVybiBMaW5rO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuTGluay5wcm9wVHlwZXMgPSB7XG4gIG9uQ2xpY2s6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYyxcbiAgdGFyZ2V0OiBfcHJvcFR5cGVzMi5kZWZhdWx0LnN0cmluZyxcbiAgcmVwbGFjZTogX3Byb3BUeXBlczIuZGVmYXVsdC5ib29sLFxuICB0bzogX3Byb3BUeXBlczIuZGVmYXVsdC5vbmVPZlR5cGUoW19wcm9wVHlwZXMyLmRlZmF1bHQuc3RyaW5nLCBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9iamVjdF0pLmlzUmVxdWlyZWQsXG4gIGlubmVyUmVmOiBfcHJvcFR5cGVzMi5kZWZhdWx0Lm9uZU9mVHlwZShbX3Byb3BUeXBlczIuZGVmYXVsdC5zdHJpbmcsIF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuY10pXG59O1xuTGluay5kZWZhdWx0UHJvcHMgPSB7XG4gIHJlcGxhY2U6IGZhbHNlXG59O1xuTGluay5jb250ZXh0VHlwZXMgPSB7XG4gIHJvdXRlcjogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgaGlzdG9yeTogX3Byb3BUeXBlczIuZGVmYXVsdC5zaGFwZSh7XG4gICAgICBwdXNoOiBfcHJvcFR5cGVzMi5kZWZhdWx0LmZ1bmMuaXNSZXF1aXJlZCxcbiAgICAgIHJlcGxhY2U6IF9wcm9wVHlwZXMyLmRlZmF1bHQuZnVuYy5pc1JlcXVpcmVkLFxuICAgICAgY3JlYXRlSHJlZjogX3Byb3BUeXBlczIuZGVmYXVsdC5mdW5jLmlzUmVxdWlyZWRcbiAgICB9KS5pc1JlcXVpcmVkXG4gIH0pLmlzUmVxdWlyZWRcbn07XG5leHBvcnRzLmRlZmF1bHQgPSBMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXJvdXRlci1kb20vTGluay5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtcm91dGVyLWRvbS9MaW5rLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjYgMjcgMjggMzAgMzQgMzUgMzYgMzcgNDAgNDEgNDIgNDMgNTcgNTggNTkgNjEiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCBMaW5rIGZyb20gJ3JlYWN0LXJvdXRlci1kb20vTGluayc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ21hdGVyaWFsLXVpL1R5cG9ncmFwaHknO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIGhlcm86IHtcbiAgICAvLyBNYWtlcyB0aGUgaGVybyBmdWxsIGhlaWdodCB1bnRpbCB3ZSBnZXQgc29tZSBtb3JlIGNvbnRlbnQuXG4gICAgbWluSGVpZ2h0OiAnMTAwdmgnLFxuICAgIGZsZXg6ICcwIDAgYXV0bycsXG4gICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwwLDAsLjQpJyxcbiAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcicsXG4gICAgYWxpZ25JdGVtczogJ2NlbnRlcicsXG4gIH0sXG4gIGNvbnRlbnQ6IHtcbiAgICBwYWRkaW5nOiAnMTAlIDEwcHgnLFxuICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gICAgW3RoZW1lLmJyZWFrcG9pbnRzLnVwKCdzbScpXToge1xuICAgICAgcGFkZGluZzogJzEwcHggMTBweCcsXG4gICAgfSxcbiAgfSxcbiAgdGl0bGU6IHtcbiAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzcwMF0pLFxuICB9LFxuICBzdWJoZWFkaW5nOiB7XG4gICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs3MDBdKSxcbiAgfSxcbiAgYnV0dG9uOiB7XG4gICAgbWFyZ2luVG9wOiAyMCxcbiAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5WzMwMF0sXG4gIH0sXG4gIGxvZ286IHtcbiAgICBtYXJnaW5Cb3R0b206ICcxMHB4JyxcbiAgICB3aWR0aDogJzEwMCUnLFxuICAgIGhlaWdodDogJzEwMCUnLFxuICAgIGFuaW1hdGlvbjogJ3NwaW4gaW5maW5pdGUgMjBzIGxpbmVhcicsXG4gIH0sXG4gICdAa2V5ZnJhbWVzIHNwaW4nOiB7XG4gICAgZnJvbToge1xuICAgICAgdHJhbnNmb3JtOiAncm90YXRlKDBkZWcpJyxcbiAgICB9LFxuICAgIHRvOiB7XG4gICAgICB0cmFuc2Zvcm06ICdyb3RhdGUoMzYwZGVnKScsXG4gICAgfSxcbiAgfSxcbn0pO1xuXG5jb25zdCBMYW5kaW5nUGFnZSA9ICh7IGNsYXNzZXMgfSkgPT4gKFxuICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5oZXJvfT5cbiAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5jb250ZW50fT5cbiAgICAgIDxpbWdcbiAgICAgICAgc3JjPXsnL3B1YmxpYy9waG90b3MvbWF5YXNoLWxvZ28tdHJhbnNwYXJlbnQucG5nJ31cbiAgICAgICAgYWx0PXsnTWF5YXNoIExvZ28nfVxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMubG9nb31cbiAgICAgIC8+XG4gICAgICA8VHlwb2dyYXBoeSB0eXBlPVwiZGlzcGxheTJcIiBjb21wb25lbnQ9XCJoMVwiIGNsYXNzTmFtZT17Y2xhc3Nlcy50aXRsZX0+XG4gICAgICAgIE1heWFzaFxuICAgICAgPC9UeXBvZ3JhcGh5PlxuICAgICAgPFR5cG9ncmFwaHlcbiAgICAgICAgdHlwZT1cInN1YmhlYWRpbmdcIlxuICAgICAgICBjb21wb25lbnQ9XCJoMlwiXG4gICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5zdWJoZWFkaW5nfVxuICAgICAgPlxuICAgICAgICBUcmFuc2Zvcm1pbmcgRWR1Y2F0aW9uLi4uXG4gICAgICA8L1R5cG9ncmFwaHk+XG4gICAgICA8QnV0dG9uXG4gICAgICAgIGNvbXBvbmVudD17TGlua31cbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmJ1dHRvbn1cbiAgICAgICAgcmFpc2VkXG4gICAgICAgIHRvPVwiL2ludHJvZHVjdGlvblwiXG4gICAgICA+XG4gICAgICAgIEludHJvZHVjdGlvblxuICAgICAgPC9CdXR0b24+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuKTtcblxuTGFuZGluZ1BhZ2UucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTGFuZGluZ1BhZ2UpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0hvbWUvTGFuZGluZ1BhZ2UuanMiXSwic291cmNlUm9vdCI6IiJ9