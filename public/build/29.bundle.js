webpackJsonp([29,71],{

/***/ "./src/client/actions/posts/getAll.js":
/*!********************************************!*\
  !*** ./src/client/actions/posts/getAll.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../constants/posts */ "./src/client/constants/posts.js");

/**
 * This action will update 'posts' store
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id - element id of which all these posts belong
 * @param {Object[]} payload.posts
 * @param {Object} payload.posts[]
 * @param {number} payload.posts[].postId
 * @param {number} payload.posts[].authorId
 * @param {string} payload.posts[].title
 * @param {string} payload.posts[].description
 * @param {Object} payload.posts[].data
 * @param {string} payload.posts[].timestamp
 *
 * @returns {Object}
 */
var getAll = function getAll(payload) {
  return { type: _posts.POSTS_GET, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/posts/users/getAll.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/getAll.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * This function fetch all posts of requested user
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/containers/PostTimeline/index.js":
/*!*****************************************************!*\
  !*** ./src/client/containers/PostTimeline/index.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../api/posts/users/getAll */ "./src/client/api/posts/users/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../actions/posts/getAll */ "./src/client/actions/posts/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

var _styles = __webpack_require__(/*! ./styles */ "./src/client/containers/PostTimeline/styles.js");

var _styles2 = _interopRequireDefault(_styles);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AsyncPostCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(57).then(__webpack_require__.bind(null, /*! ../PostCreate */ "./src/client/containers/PostCreate/index.js"));
  },
  modules: ['../PostCreate'],
  loading: _Loading2.default
});

// import actions from '../../actions';

/**
 * PostTimeline component will display only posts
 *
 * @format
 */

var AsyncPost = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(37).then(__webpack_require__.bind(null, /*! ../Post */ "./src/client/containers/Post/index.js"));
  },
  modules: ['../Post'],
  loading: _Loading2.default
});

var PostTimeline = function (_Component) {
  (0, _inherits3.default)(PostTimeline, _Component);

  function PostTimeline(props) {
    (0, _classCallCheck3.default)(this, PostTimeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (PostTimeline.__proto__ || (0, _getPrototypeOf2.default)(PostTimeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(PostTimeline, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, elements, element, _elements$user, userId, token, id, elementType, _ref2, statusCode, error, payload, next;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, elements = _props.elements, element = _props.element;
                _elements$user = elements.user, userId = _elements$user.id, token = _elements$user.token;
                id = element.id, elementType = element.elementType;

                if (!(elementType !== 'user' || element.posts.length !== 0)) {
                  _context.next = 6;
                  break;
                }

                return _context.abrupt('return');

              case 6:
                _context.next = 8;
                return (0, _getAll2.default)({
                  token: token,
                  userId: userId
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;
                next = _ref2.next;

                if (!(statusCode >= 300)) {
                  _context.next = 16;
                  break;
                }

                // handle error
                console.error(error);
                return _context.abrupt('return');

              case 16:

                this.props.actionPostsGet({
                  id: userId,
                  statusCode: statusCode,
                  posts: payload
                });
                _context.next = 22;
                break;

              case 19:
                _context.prev = 19;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 22:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 19]]);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()

    // shouldComponentUpdate(nextProps, nextState) {
    //   return this.state !== nextState && this.props !== nextProps;
    // }

  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          elements = _props2.elements,
          element = _props2.element;
      var _elements$user2 = elements.user,
          isSignedIn = _elements$user2.isSignedIn,
          userId = _elements$user2.id;
      var id = element.id,
          posts = element.posts;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 9, md: 7, lg: 6, xl: 6 },
          isSignedIn && userId === id ? _react2.default.createElement(AsyncPostCreate, null) : null,
          typeof posts !== 'undefined' && posts.map(function (_ref3) {
            var postId = _ref3.postId;
            return _react2.default.createElement(AsyncPost, { key: postId, postId: postId });
          })
        )
      );
    }
  }]);
  return PostTimeline;
}(_react.Component);

PostTimeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  elements: _propTypes2.default.object.isRequired,

  element: _propTypes2.default.shape({
    id: _propTypes2.default.number,
    username: _propTypes2.default.string,
    posts: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      postId: _propTypes2.default.number
    }))
  }),

  actionPostsGet: _propTypes2.default.func.isRequired
};


var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements;
  return { elements: elements };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionPostsGet: _getAll4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(_styles2.default)(PostTimeline));

/***/ }),

/***/ "./src/client/containers/PostTimeline/styles.js":
/*!******************************************************!*\
  !*** ./src/client/containers/PostTimeline/styles.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the styles for PostTimeline component.
 *
 * @format
 */

var styles = {
  root: {}
};

exports.default = styles;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvZ2V0QWxsLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL3Bvc3RzL3VzZXJzL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUG9zdFRpbWVsaW5lL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0VGltZWxpbmUvc3R5bGVzLmpzIl0sIm5hbWVzIjpbImdldEFsbCIsInBheWxvYWQiLCJ0eXBlIiwidXNlcklkIiwidG9rZW4iLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsIkFzeW5jUG9zdENyZWF0ZSIsImxvYWRlciIsIm1vZHVsZXMiLCJsb2FkaW5nIiwiQXN5bmNQb3N0IiwiUG9zdFRpbWVsaW5lIiwicHJvcHMiLCJzdGF0ZSIsImVsZW1lbnRzIiwiZWxlbWVudCIsInVzZXIiLCJpZCIsImVsZW1lbnRUeXBlIiwicG9zdHMiLCJsZW5ndGgiLCJuZXh0IiwiYWN0aW9uUG9zdHNHZXQiLCJjbGFzc2VzIiwiaXNTaWduZWRJbiIsInJvb3QiLCJtYXAiLCJwb3N0SWQiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic2hhcGUiLCJudW1iZXIiLCJ1c2VybmFtZSIsInN0cmluZyIsImFycmF5T2YiLCJmdW5jIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giLCJzdHlsZXMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBS0E7O0FBc0JBOzs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBLElBQU1BLFNBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0IsRUFBRUMsc0JBQUYsRUFBbUJELGdCQUFuQixFQUEvQjtBQUFBLENBQWYsQyxDQTNDQTs7Ozs7a0JBNkNlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckNmOzs7Ozs7Ozs7OztBQVJBOzs7Ozs7c0ZBbUJBO0FBQUEsUUFBd0JHLE1BQXhCLFNBQXdCQSxNQUF4QjtBQUFBLFFBQWdDQyxLQUFoQyxTQUFnQ0EsS0FBaEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNGLE1BRnJDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1FLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlTDtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVU0sZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlLLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFGLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVkLE07Ozs7O0FBZGY7Ozs7QUFDQTs7OztrQkFnRGVBLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaERmOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFJQTs7Ozs7O0FBRUEsSUFBTWlCLGtCQUFrQiw2QkFBUztBQUMvQkMsVUFBUTtBQUFBLFdBQU0sZ0pBQU47QUFBQSxHQUR1QjtBQUUvQkMsV0FBUyxDQUFDLGVBQUQsQ0FGc0I7QUFHL0JDO0FBSCtCLENBQVQsQ0FBeEI7O0FBSkE7O0FBdEJBOzs7Ozs7QUFnQ0EsSUFBTUMsWUFBWSw2QkFBUztBQUN6QkgsVUFBUTtBQUFBLFdBQU0sb0lBQU47QUFBQSxHQURpQjtBQUV6QkMsV0FBUyxDQUFDLFNBQUQsQ0FGZ0I7QUFHekJDO0FBSHlCLENBQVQsQ0FBbEI7O0lBTU1FLFk7OztBQW1CSix3QkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLGtKQUNYQSxLQURXOztBQUVqQixVQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUZpQjtBQUdsQjs7Ozs7Ozs7Ozs7Ozt5QkFJaUMsS0FBS0QsSyxFQUEzQkUsUSxVQUFBQSxRLEVBQVVDLE8sVUFBQUEsTztpQ0FDWUQsU0FBU0UsSSxFQUEzQnhCLE0sa0JBQUp5QixFLEVBQVl4QixLLGtCQUFBQSxLO0FBQ1p3QixrQixHQUFvQkYsTyxDQUFwQkUsRSxFQUFJQyxXLEdBQWdCSCxPLENBQWhCRyxXOztzQkFFUkEsZ0JBQWdCLE1BQWhCLElBQTBCSCxRQUFRSSxLQUFSLENBQWNDLE1BQWQsS0FBeUIsQzs7Ozs7Ozs7O3VCQUtKLHNCQUFnQjtBQUNqRTNCLDhCQURpRTtBQUVqRUQ7QUFGaUUsaUJBQWhCLEM7Ozs7QUFBM0NVLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPYix1QixTQUFBQSxPO0FBQVMrQixvQixTQUFBQSxJOztzQkFLaENuQixjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0FHLHdCQUFRRixLQUFSLENBQWNBLEtBQWQ7Ozs7O0FBSUYscUJBQUtTLEtBQUwsQ0FBV1UsY0FBWCxDQUEwQjtBQUN4Qkwsc0JBQUl6QixNQURvQjtBQUV4QlUsd0NBRndCO0FBR3hCaUIseUJBQU83QjtBQUhpQixpQkFBMUI7Ozs7Ozs7O0FBTUFlLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7OztBQUlKO0FBQ0E7QUFDQTs7Ozs2QkFFUztBQUFBLG9CQUNnQyxLQUFLUyxLQURyQztBQUFBLFVBQ0NXLE9BREQsV0FDQ0EsT0FERDtBQUFBLFVBQ1VULFFBRFYsV0FDVUEsUUFEVjtBQUFBLFVBQ29CQyxPQURwQixXQUNvQkEsT0FEcEI7QUFBQSw0QkFFNEJELFNBQVNFLElBRnJDO0FBQUEsVUFFQ1EsVUFGRCxtQkFFQ0EsVUFGRDtBQUFBLFVBRWlCaEMsTUFGakIsbUJBRWF5QixFQUZiO0FBQUEsVUFHQ0EsRUFIRCxHQUdlRixPQUhmLENBR0NFLEVBSEQ7QUFBQSxVQUdLRSxLQUhMLEdBR2VKLE9BSGYsQ0FHS0ksS0FITDs7O0FBS1AsYUFDRTtBQUFBO0FBQUEsVUFBTSxlQUFOLEVBQWdCLFNBQVEsUUFBeEIsRUFBaUMsU0FBUyxDQUExQyxFQUE2QyxXQUFXSSxRQUFRRSxJQUFoRTtBQUNFO0FBQUE7QUFBQSxZQUFNLFVBQU4sRUFBVyxJQUFJLEVBQWYsRUFBbUIsSUFBSSxDQUF2QixFQUEwQixJQUFJLENBQTlCLEVBQWlDLElBQUksQ0FBckMsRUFBd0MsSUFBSSxDQUE1QztBQUVHRCx3QkFBY2hDLFdBQVd5QixFQUF6QixHQUE4Qiw4QkFBQyxlQUFELE9BQTlCLEdBQW9ELElBRnZEO0FBSUcsaUJBQU9FLEtBQVAsS0FBaUIsV0FBakIsSUFDQ0EsTUFBTU8sR0FBTixDQUFVO0FBQUEsZ0JBQUdDLE1BQUgsU0FBR0EsTUFBSDtBQUFBLG1CQUNSLDhCQUFDLFNBQUQsSUFBVyxLQUFLQSxNQUFoQixFQUF3QixRQUFRQSxNQUFoQyxHQURRO0FBQUEsV0FBVjtBQUxKO0FBREYsT0FERjtBQWFEOzs7OztBQTlFR2hCLFksQ0FDR2lCLFMsR0FBWTtBQUNqQkwsV0FBUyxvQkFBVU0sTUFBVixDQUFpQkMsVUFEVDs7QUFHakJoQixZQUFVLG9CQUFVZSxNQUFWLENBQWlCQyxVQUhWOztBQUtqQmYsV0FBUyxvQkFBVWdCLEtBQVYsQ0FBZ0I7QUFDdkJkLFFBQUksb0JBQVVlLE1BRFM7QUFFdkJDLGNBQVUsb0JBQVVDLE1BRkc7QUFHdkJmLFdBQU8sb0JBQVVnQixPQUFWLENBQ0wsb0JBQVVKLEtBQVYsQ0FBZ0I7QUFDZEosY0FBUSxvQkFBVUs7QUFESixLQUFoQixDQURLO0FBSGdCLEdBQWhCLENBTFE7O0FBZWpCVixrQkFBZ0Isb0JBQVVjLElBQVYsQ0FBZU47QUFmZCxDOzs7QUFnRnJCLElBQU1PLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHdkIsUUFBSCxTQUFHQSxRQUFIO0FBQUEsU0FBbUIsRUFBRUEsa0JBQUYsRUFBbkI7QUFBQSxDQUF4Qjs7QUFFQSxJQUFNd0IscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VqQjtBQURGLEdBREYsRUFJRWlCLFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVFGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDRDQUFtQjNCLFlBQW5CLENBRGEsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDaklmOzs7Ozs7QUFNQSxJQUFNNkIsU0FBUztBQUNiZixRQUFNO0FBRE8sQ0FBZjs7a0JBSWVlLE0iLCJmaWxlIjoiMjkuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IFBPU1RTX0dFVCB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9wb3N0cyc7XG5cbnR5cGUgUG9zdCA9IHtcbiAgcG9zdElkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG4gIGRlc2NyaXB0aW9uPzogc3RyaW5nLFxuICBkYXRhPzogYW55LFxuICB0aW1lc3RhbXA6IHN0cmluZyxcbn07XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBpZDogbnVtYmVyLFxuICBwb3N0czogQXJyYXk8UG9zdD4sXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBUaGlzIGFjdGlvbiB3aWxsIHVwZGF0ZSAncG9zdHMnIHN0b3JlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWQgLSBlbGVtZW50IGlkIG9mIHdoaWNoIGFsbCB0aGVzZSBwb3N0cyBiZWxvbmdcbiAqIEBwYXJhbSB7T2JqZWN0W119IHBheWxvYWQucG9zdHNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLnBvc3RzW11cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnBvc3RzW10ucG9zdElkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0c1tdLmF1dGhvcklkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5wb3N0c1tdLnRpdGxlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5wb3N0c1tdLmRlc2NyaXB0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5wb3N0c1tdLmRhdGFcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnBvc3RzW10udGltZXN0YW1wXG4gKlxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0QWxsID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHsgdHlwZTogUE9TVFNfR0VULCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvZ2V0QWxsLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmlsZVxuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gZmV0Y2ggYWxsIHBvc3RzIG9mIHJlcXVlc3RlZCB1c2VyXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBnZXRBbGxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdXNlcklkLCB0b2tlbiB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9wb3N0c2A7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZ2V0QWxsO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvcG9zdHMvdXNlcnMvZ2V0QWxsLmpzIiwiLyoqXG4gKiBQb3N0VGltZWxpbmUgY29tcG9uZW50IHdpbGwgZGlzcGxheSBvbmx5IHBvc3RzXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBMb2FkYWJsZSBmcm9tICdyZWFjdC1sb2FkYWJsZSc7XG5cbmltcG9ydCBMb2FkaW5nIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvTG9hZGluZyc7XG5cbmltcG9ydCBhcGlVc2VyUG9zdHNHZXQgZnJvbSAnLi4vLi4vYXBpL3Bvc3RzL3VzZXJzL2dldEFsbCc7XG5cbmltcG9ydCBhY3Rpb25Qb3N0c0dldCBmcm9tICcuLi8uLi9hY3Rpb25zL3Bvc3RzL2dldEFsbCc7XG5cbi8vIGltcG9ydCBhY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMnO1xuXG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGVzJztcblxuY29uc3QgQXN5bmNQb3N0Q3JlYXRlID0gTG9hZGFibGUoe1xuICBsb2FkZXI6ICgpID0+IGltcG9ydCgnLi4vUG9zdENyZWF0ZScpLFxuICBtb2R1bGVzOiBbJy4uL1Bvc3RDcmVhdGUnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY1Bvc3QgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuLi9Qb3N0JyksXG4gIG1vZHVsZXM6IFsnLi4vUG9zdCddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNsYXNzIFBvc3RUaW1lbGluZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gICAgZWxlbWVudHM6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAgIGVsZW1lbnQ6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgICBpZDogUHJvcFR5cGVzLm51bWJlcixcbiAgICAgIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgICAgcG9zdHM6IFByb3BUeXBlcy5hcnJheU9mKFxuICAgICAgICBQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgICAgIHBvc3RJZDogUHJvcFR5cGVzLm51bWJlcixcbiAgICAgICAgfSksXG4gICAgICApLFxuICAgIH0pLFxuXG4gICAgYWN0aW9uUG9zdHNHZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgZWxlbWVudHMsIGVsZW1lbnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IGlkOiB1c2VySWQsIHRva2VuIH0gPSBlbGVtZW50cy51c2VyO1xuICAgICAgY29uc3QgeyBpZCwgZWxlbWVudFR5cGUgfSA9IGVsZW1lbnQ7XG5cbiAgICAgIGlmIChlbGVtZW50VHlwZSAhPT0gJ3VzZXInIHx8IGVsZW1lbnQucG9zdHMubGVuZ3RoICE9PSAwKSB7XG4gICAgICAgIC8vIHVwZGF0ZSBUaGlzIHRpbWVsaW5lIGNvbXBvbmVudCBmb3IgY2lyY2xlcyBhbHNvLlxuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IsIHBheWxvYWQsIG5leHQgfSA9IGF3YWl0IGFwaVVzZXJQb3N0c0dldCh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICAgIC8vIGhhbmRsZSBlcnJvclxuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICB0aGlzLnByb3BzLmFjdGlvblBvc3RzR2V0KHtcbiAgICAgICAgaWQ6IHVzZXJJZCxcbiAgICAgICAgc3RhdHVzQ29kZSxcbiAgICAgICAgcG9zdHM6IHBheWxvYWQsXG4gICAgICB9KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgLy8gc2hvdWxkQ29tcG9uZW50VXBkYXRlKG5leHRQcm9wcywgbmV4dFN0YXRlKSB7XG4gIC8vICAgcmV0dXJuIHRoaXMuc3RhdGUgIT09IG5leHRTdGF0ZSAmJiB0aGlzLnByb3BzICE9PSBuZXh0UHJvcHM7XG4gIC8vIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBlbGVtZW50cywgZWxlbWVudCB9ID0gdGhpcy5wcm9wcztcbiAgICBjb25zdCB7IGlzU2lnbmVkSW4sIGlkOiB1c2VySWQgfSA9IGVsZW1lbnRzLnVzZXI7XG4gICAgY29uc3QgeyBpZCwgcG9zdHMgfSA9IGVsZW1lbnQ7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEdyaWQgY29udGFpbmVyIGp1c3RpZnk9XCJjZW50ZXJcIiBzcGFjaW5nPXswfSBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBzbT17OX0gbWQ9ezd9IGxnPXs2fSB4bD17Nn0+XG4gICAgICAgICAgey8qIGlmIHVzZXIgaGFzIHNpZ25lZCBpbiB0aGVuIGFsc28gc2hvdyBjcmVhdGUgcG9zdCBjb21wb25lbnQgKi99XG4gICAgICAgICAge2lzU2lnbmVkSW4gJiYgdXNlcklkID09PSBpZCA/IDxBc3luY1Bvc3RDcmVhdGUgLz4gOiBudWxsfVxuXG4gICAgICAgICAge3R5cGVvZiBwb3N0cyAhPT0gJ3VuZGVmaW5lZCcgJiZcbiAgICAgICAgICAgIHBvc3RzLm1hcCgoeyBwb3N0SWQgfSkgPT4gKFxuICAgICAgICAgICAgICA8QXN5bmNQb3N0IGtleT17cG9zdElkfSBwb3N0SWQ9e3Bvc3RJZH0gLz5cbiAgICAgICAgICAgICkpfVxuICAgICAgICA8L0dyaWQ+XG4gICAgICA8L0dyaWQ+XG4gICAgKTtcbiAgfVxufVxuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cyB9KSA9PiAoeyBlbGVtZW50cyB9KTtcblxuY29uc3QgbWFwRGlzcGF0Y2hUb1Byb3BzID0gKGRpc3BhdGNoKSA9PlxuICBiaW5kQWN0aW9uQ3JlYXRvcnMoXG4gICAge1xuICAgICAgYWN0aW9uUG9zdHNHZXQsXG4gICAgfSxcbiAgICBkaXNwYXRjaCxcbiAgKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShQb3N0VGltZWxpbmUpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0VGltZWxpbmUvaW5kZXguanMiLCIvKipcbiAqIFRoaXMgZmlsZSBjb250YWlucyBhbGwgdGhlIHN0eWxlcyBmb3IgUG9zdFRpbWVsaW5lIGNvbXBvbmVudC5cbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7fSxcbn07XG5cbmV4cG9ydCBkZWZhdWx0IHN0eWxlcztcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9Qb3N0VGltZWxpbmUvc3R5bGVzLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==