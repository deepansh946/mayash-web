webpackJsonp([21],{

/***/ "./src/client/containers/Home/index.js":
/*!*********************************************!*\
  !*** ./src/client/containers/Home/index.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * This is Home page component
 * Home page will be different for SignedIn and unSignedIn user
 *
 * @format
 */

var AsyncLandingPage = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(30/* duplicate */).then(__webpack_require__.bind(null, /*! ./LandingPage */ "./src/client/containers/Home/LandingPage.js"));
  },
  modules: ['./LandingPage'],
  loading: _Loading2.default
});

var AsyncHome = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(48).then(__webpack_require__.bind(null, /*! ./Home */ "./src/client/containers/Home/Home.js"));
  },
  modules: ['./Home'],
  loading: _Loading2.default
});

var styles = {
  root: {
    flexGrow: 1,
    width: '100%'
  }
};

/**
 *
 */

var Index = function (_Component) {
  (0, _inherits3.default)(Index, _Component);

  function Index() {
    (0, _classCallCheck3.default)(this, Index);
    return (0, _possibleConstructorReturn3.default)(this, (Index.__proto__ || (0, _getPrototypeOf2.default)(Index)).apply(this, arguments));
  }

  (0, _createClass3.default)(Index, [{
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return this.state !== nextState && this.props !== nextProps;
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          user = _props.user;
      var isSignedIn = user.isSignedIn;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        isSignedIn ? _react2.default.createElement(AsyncHome, null) : _react2.default.createElement(AsyncLandingPage, null)
      );
    }
  }]);
  return Index;
}(_react.Component);

Index.propTypes = {
  /**
   * Classes object contains the styling.
   */
  classes: _propTypes2.default.object.isRequired,

  /**
   * This user object will come from redux store's elements part.
   */
  user: _propTypes2.default.shape({
    isSignedIn: _propTypes2.default.bool.isRequired
  })
};


var mapStateToProps = function mapStateToProps(_ref) {
  var elements = _ref.elements;
  return { user: elements.user };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)((0, _withStyles2.default)(styles)(Index));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvSG9tZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJBc3luY0xhbmRpbmdQYWdlIiwibG9hZGVyIiwibW9kdWxlcyIsImxvYWRpbmciLCJBc3luY0hvbWUiLCJzdHlsZXMiLCJyb290IiwiZmxleEdyb3ciLCJ3aWR0aCIsIkluZGV4IiwibmV4dFByb3BzIiwibmV4dFN0YXRlIiwic3RhdGUiLCJwcm9wcyIsImNsYXNzZXMiLCJ1c2VyIiwiaXNTaWduZWRJbiIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJzaGFwZSIsImJvb2wiLCJtYXBTdGF0ZVRvUHJvcHMiLCJlbGVtZW50cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQU9BOzs7O0FBQ0E7Ozs7QUFFQTs7QUFFQTs7OztBQUVBOzs7O0FBRUE7Ozs7OztBQWhCQTs7Ozs7OztBQWtCQSxJQUFNQSxtQkFBbUIsNkJBQVM7QUFDaENDLFVBQVE7QUFBQSxXQUFNLCtKQUFOO0FBQUEsR0FEd0I7QUFFaENDLFdBQVMsQ0FBQyxlQUFELENBRnVCO0FBR2hDQztBQUhnQyxDQUFULENBQXpCOztBQU1BLElBQU1DLFlBQVksNkJBQVM7QUFDekJILFVBQVE7QUFBQSxXQUFNLGtJQUFOO0FBQUEsR0FEaUI7QUFFekJDLFdBQVMsQ0FBQyxRQUFELENBRmdCO0FBR3pCQztBQUh5QixDQUFULENBQWxCOztBQU1BLElBQU1FLFNBQVM7QUFDYkMsUUFBTTtBQUNKQyxjQUFVLENBRE47QUFFSkMsV0FBTztBQUZIO0FBRE8sQ0FBZjs7QUFPQTs7OztJQUdNQyxLOzs7Ozs7Ozs7OzBDQWVrQkMsUyxFQUFXQyxTLEVBQVc7QUFDMUMsYUFBTyxLQUFLQyxLQUFMLEtBQWVELFNBQWYsSUFBNEIsS0FBS0UsS0FBTCxLQUFlSCxTQUFsRDtBQUNEOzs7NkJBRVE7QUFBQSxtQkFDbUIsS0FBS0csS0FEeEI7QUFBQSxVQUNDQyxPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVQyxJQURWLFVBQ1VBLElBRFY7QUFBQSxVQUVDQyxVQUZELEdBRWdCRCxJQUZoQixDQUVDQyxVQUZEOzs7QUFJUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdGLFFBQVFSLElBQXhCO0FBQ0dVLHFCQUFhLDhCQUFDLFNBQUQsT0FBYixHQUE2Qiw4QkFBQyxnQkFBRDtBQURoQyxPQURGO0FBS0Q7Ozs7O0FBNUJHUCxLLENBQ0dRLFMsR0FBWTtBQUNqQjs7O0FBR0FILFdBQVMsb0JBQVVJLE1BQVYsQ0FBaUJDLFVBSlQ7O0FBTWpCOzs7QUFHQUosUUFBTSxvQkFBVUssS0FBVixDQUFnQjtBQUNwQkosZ0JBQVksb0JBQVVLLElBQVYsQ0FBZUY7QUFEUCxHQUFoQjtBQVRXLEM7OztBQThCckIsSUFBTUcsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdDLFFBQUgsUUFBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVSLE1BQU1RLFNBQVNSLElBQWpCLEVBQW5CO0FBQUEsQ0FBeEI7O2tCQUVlLHlCQUFRTyxlQUFSLEVBQXlCLDBCQUFXakIsTUFBWCxFQUFtQkksS0FBbkIsQ0FBekIsQyIsImZpbGUiOiIyMS5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIFRoaXMgaXMgSG9tZSBwYWdlIGNvbXBvbmVudFxuICogSG9tZSBwYWdlIHdpbGwgYmUgZGlmZmVyZW50IGZvciBTaWduZWRJbiBhbmQgdW5TaWduZWRJbiB1c2VyXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgTG9hZGFibGUgZnJvbSAncmVhY3QtbG9hZGFibGUnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5cbmltcG9ydCBMb2FkaW5nIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvTG9hZGluZyc7XG5cbmNvbnN0IEFzeW5jTGFuZGluZ1BhZ2UgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuL0xhbmRpbmdQYWdlJyksXG4gIG1vZHVsZXM6IFsnLi9MYW5kaW5nUGFnZSddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNvbnN0IEFzeW5jSG9tZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vSG9tZScpLFxuICBtb2R1bGVzOiBbJy4vSG9tZSddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge1xuICAgIGZsZXhHcm93OiAxLFxuICAgIHdpZHRoOiAnMTAwJScsXG4gIH0sXG59O1xuXG4vKipcbiAqXG4gKi9cbmNsYXNzIEluZGV4IGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICAvKipcbiAgICAgKiBDbGFzc2VzIG9iamVjdCBjb250YWlucyB0aGUgc3R5bGluZy5cbiAgICAgKi9cbiAgICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgICAvKipcbiAgICAgKiBUaGlzIHVzZXIgb2JqZWN0IHdpbGwgY29tZSBmcm9tIHJlZHV4IHN0b3JlJ3MgZWxlbWVudHMgcGFydC5cbiAgICAgKi9cbiAgICB1c2VyOiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgaXNTaWduZWRJbjogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcbiAgICB9KSxcbiAgfTtcblxuICBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzLCBuZXh0U3RhdGUpIHtcbiAgICByZXR1cm4gdGhpcy5zdGF0ZSAhPT0gbmV4dFN0YXRlICYmIHRoaXMucHJvcHMgIT09IG5leHRQcm9wcztcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7IGNsYXNzZXMsIHVzZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluIH0gPSB1c2VyO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICB7aXNTaWduZWRJbiA/IDxBc3luY0hvbWUgLz4gOiA8QXN5bmNMYW5kaW5nUGFnZSAvPn1cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgZWxlbWVudHMgfSkgPT4gKHsgdXNlcjogZWxlbWVudHMudXNlciB9KTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMpKHdpdGhTdHlsZXMoc3R5bGVzKShJbmRleCkpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0hvbWUvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9