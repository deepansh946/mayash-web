webpackJsonp([25],{

/***/ "./src/client/api/follow/create.js":
/*!*****************************************!*\
  !*** ./src/client/api/follow/create.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.followerId
 * @param {number} payload.followingId
 * @return {Promise} -
 */
/** @format */

var create = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        followerId = _ref2.followerId,
        followingId = _ref2.followingId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/follow/' + followerId + '/' + followingId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function create(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = create;

/***/ }),

/***/ "./src/client/api/follow/delete.js":
/*!*****************************************!*\
  !*** ./src/client/api/follow/delete.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.followerId
 * @param {number} payload.followingId
 * @return {Promise} -
 */
/** @format */

var deleteFollower = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        followerId = _ref2.followerId,
        followingId = _ref2.followingId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/follow/' + followerId + '/' + followingId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'DELETE',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function deleteFollower(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = deleteFollower;

/***/ }),

/***/ "./src/client/api/follow/get.js":
/*!**************************************!*\
  !*** ./src/client/api/follow/get.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.followerId
 * @param {number} payload.followingId
 * @return {Promise} -
 */
/** @format */

var get = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        followerId = _ref2.followerId,
        followingId = _ref2.followingId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/follow/' + followerId + '/' + followingId;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function get(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = get;

/***/ }),

/***/ "./src/client/containers/Follow/index.js":
/*!***********************************************!*\
  !*** ./src/client/containers/Follow/index.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _styles = __webpack_require__(/*! material-ui/styles */ "./node_modules/material-ui/styles/index.js");

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _get = __webpack_require__(/*! ../../api/follow/get */ "./src/client/api/follow/get.js");

var _get2 = _interopRequireDefault(_get);

var _create = __webpack_require__(/*! ../../api/follow/create */ "./src/client/api/follow/create.js");

var _create2 = _interopRequireDefault(_create);

var _delete = __webpack_require__(/*! ../../api/follow/delete */ "./src/client/api/follow/delete.js");

var _delete2 = _interopRequireDefault(_delete);

var _styles2 = __webpack_require__(/*! ./styles */ "./src/client/containers/Follow/styles.js");

var _styles3 = _interopRequireDefault(_styles2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Follow component is designed for displaying a user
 * is following any element or not.
 *
 */
var Follow = function (_Component) {
  (0, _inherits3.default)(Follow, _Component);

  function Follow(props) {
    (0, _classCallCheck3.default)(this, Follow);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Follow.__proto__ || (0, _getPrototypeOf2.default)(Follow)).call(this, props));

    _this.state = {
      follow: false
    };

    _this.onClickFollow = _this.onClickFollow.bind(_this);
    _this.onClickUnfollow = _this.onClickUnfollow.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Follow, [{
    key: 'componentWillMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, user, element, followerId, token, followingId, res;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, user = _props.user, element = _props.element;
                followerId = user.id, token = user.token;
                followingId = element.id;
                _context.next = 6;
                return (0, _get2.default)({
                  token: token,
                  followerId: followerId,
                  followingId: followingId
                });

              case 6:
                res = _context.sent;


                if (res.statusCode < 300) {
                  this.setState({ follow: true });
                }
                _context.next = 13;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 13:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 10]]);
      }));

      function componentWillMount() {
        return _ref.apply(this, arguments);
      }

      return componentWillMount;
    }()
  }, {
    key: 'onClickFollow',
    value: function () {
      var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
        var _props2, user, element, followerId, token, followingId, _ref3, statusCode, error;

        return _regenerator2.default.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _props2 = this.props, user = _props2.user, element = _props2.element;
                followerId = user.id, token = user.token;
                followingId = element.id;
                _context2.next = 6;
                return (0, _create2.default)({
                  token: token,
                  followerId: followerId,
                  followingId: followingId
                });

              case 6:
                _ref3 = _context2.sent;
                statusCode = _ref3.statusCode;
                error = _ref3.error;

                if (!(statusCode >= 300)) {
                  _context2.next = 12;
                  break;
                }

                console.log(error);
                return _context2.abrupt('return');

              case 12:

                this.setState({ follow: true });
                _context2.next = 18;
                break;

              case 15:
                _context2.prev = 15;
                _context2.t0 = _context2['catch'](0);

                console.error(_context2.t0);

              case 18:
              case 'end':
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 15]]);
      }));

      function onClickFollow() {
        return _ref2.apply(this, arguments);
      }

      return onClickFollow;
    }()
  }, {
    key: 'onClickUnfollow',
    value: function () {
      var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
        var _props3, user, element, followerId, token, followingId, res;

        return _regenerator2.default.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _props3 = this.props, user = _props3.user, element = _props3.element;
                followerId = user.id, token = user.token;
                followingId = element.id;
                _context3.next = 6;
                return (0, _delete2.default)({
                  token: token,
                  followerId: followerId,
                  followingId: followingId
                });

              case 6:
                res = _context3.sent;

                if (!(res.statusCode >= 300)) {
                  _context3.next = 10;
                  break;
                }

                console.log(res.error);
                return _context3.abrupt('return');

              case 10:

                this.setState({ follow: false });
                _context3.next = 16;
                break;

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3['catch'](0);

                console.error(_context3.t0);

              case 16:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 13]]);
      }));

      function onClickUnfollow() {
        return _ref4.apply(this, arguments);
      }

      return onClickUnfollow;
    }()
  }, {
    key: 'render',
    value: function render() {
      var classes = this.props.classes;
      var follow = this.state.follow;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Button2.default,
          {
            raised: true,
            color: follow ? 'accent' : null,
            onClick: !follow ? this.onClickFollow : this.onClickUnfollow,
            className: classes.button
          },
          !follow ? 'Follow' : 'Following'
        )
      );
    }
  }]);
  return Follow;
}(_react.Component); /**
                      * This component is create to follow or follower
                      * @format
                      */

Follow.propTypes = {
  /**
   * classes object contains the styles for Follow component.
   */
  classes: _propTypes2.default.object.isRequired,

  /**
   * user object contains user's info. this comes from redux store.
   */
  user: _propTypes2.default.object.isRequired,

  /**
   * element object contains all the info of element of which user is
   * going to follow or unfollow.
   */
  element: _propTypes2.default.object.isRequired
};

var mapStateToProps = function mapStateToProps(_ref5) {
  var elements = _ref5.elements;
  return { user: elements.user };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)((0, _styles.withStyles)(_styles3.default)(Follow));

/***/ }),

/***/ "./src/client/containers/Follow/styles.js":
/*!************************************************!*\
  !*** ./src/client/containers/Follow/styles.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This component is create to follow or follower
 * @format
 */

var styles = function styles(theme) {
  return {
    root: {
      margin: 'auto'
    },
    button: {
      borderRadius: '15px',
      fontWeight: 'bold'
    }
  };
};

exports.default = styles;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9mb2xsb3cvY3JlYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYXBpL2ZvbGxvdy9kZWxldGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvZm9sbG93L2dldC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRm9sbG93L2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9Gb2xsb3cvc3R5bGVzLmpzIl0sIm5hbWVzIjpbInRva2VuIiwiZm9sbG93ZXJJZCIsImZvbGxvd2luZ0lkIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJjcmVhdGUiLCJkZWxldGVGb2xsb3dlciIsImdldCIsIkZvbGxvdyIsInByb3BzIiwic3RhdGUiLCJmb2xsb3ciLCJvbkNsaWNrRm9sbG93IiwiYmluZCIsIm9uQ2xpY2tVbmZvbGxvdyIsInVzZXIiLCJlbGVtZW50IiwiaWQiLCJzZXRTdGF0ZSIsImxvZyIsImNsYXNzZXMiLCJyb290IiwiYnV0dG9uIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm1hcFN0YXRlVG9Qcm9wcyIsImVsZW1lbnRzIiwic3R5bGVzIiwidGhlbWUiLCJtYXJnaW4iLCJib3JkZXJSYWRpdXMiLCJmb250V2VpZ2h0Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtBOzs7Ozs7O0FBTEE7OztzRkFZQTtBQUFBLFFBQXdCQSxLQUF4QixTQUF3QkEsS0FBeEI7QUFBQSxRQUErQkMsVUFBL0IsU0FBK0JBLFVBQS9CO0FBQUEsUUFBMkNDLFdBQTNDLFNBQTJDQSxXQUEzQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLG1DQUVzQ0YsVUFGdEMsU0FFb0RDLFdBRnBEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsTUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlUDtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVVEsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFjUUQsVUFBVSxHQWRsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FlYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWZiOztBQUFBO0FBQUE7QUFBQSxtQkFxQnVCRixJQUFJSyxJQUFKLEVBckJ2Qjs7QUFBQTtBQXFCVUEsZ0JBckJWO0FBQUEsd0VBdUJnQkEsSUF2QmhCOztBQUFBO0FBQUE7QUFBQTs7QUF5QklDLG9CQUFRRixLQUFSOztBQXpCSiw2Q0EyQlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBM0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlRyxNOzs7OztBQVZmOzs7O0FBQ0E7Ozs7a0JBMkNlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekNmOzs7Ozs7O0FBTEE7OztzRkFZQTtBQUFBLFFBQWdDZixLQUFoQyxTQUFnQ0EsS0FBaEM7QUFBQSxRQUF1Q0MsVUFBdkMsU0FBdUNBLFVBQXZDO0FBQUEsUUFBbURDLFdBQW5ELFNBQW1EQSxXQUFuRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLG1DQUVzQ0YsVUFGdEMsU0FFb0RDLFdBRnBEO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsUUFEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlUDtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVVEsZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFjUUQsVUFBVSxHQWRsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FlYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWZiOztBQUFBO0FBQUE7QUFBQSxtQkFxQnVCRixJQUFJSyxJQUFKLEVBckJ2Qjs7QUFBQTtBQXFCVUEsZ0JBckJWO0FBQUEsd0VBdUJnQkEsSUF2QmhCOztBQUFBO0FBQUE7QUFBQTs7QUF5QklDLG9CQUFRRixLQUFSOztBQXpCSiw2Q0EyQlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBM0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlSSxjOzs7OztBQVZmOzs7O0FBQ0E7Ozs7a0JBMkNlQSxjOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDekNmOzs7Ozs7O0FBTEE7OztzRkFZQTtBQUFBLFFBQXFCaEIsS0FBckIsU0FBcUJBLEtBQXJCO0FBQUEsUUFBNEJDLFVBQTVCLFNBQTRCQSxVQUE1QjtBQUFBLFFBQXdDQyxXQUF4QyxTQUF3Q0EsV0FBeEM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixtQ0FFc0NGLFVBRnRDLFNBRW9EQyxXQUZwRDtBQUFBO0FBQUEsbUJBSXNCLCtCQUFNQyxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLEtBRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQLGdDQUFnQixrQkFGVDtBQUdQQywrQkFBZVA7QUFIUjtBQUZrQixhQUFYLENBSnRCOztBQUFBO0FBSVVRLGVBSlY7QUFhWUMsa0JBYlosR0FhbUNELEdBYm5DLENBYVlDLE1BYlosRUFhb0JDLFVBYnBCLEdBYW1DRixHQWJuQyxDQWFvQkUsVUFicEI7O0FBQUEsa0JBY1FELFVBQVUsR0FkbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBZWE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFmYjs7QUFBQTtBQUFBO0FBQUEsbUJBcUJ1QkYsSUFBSUssSUFBSixFQXJCdkI7O0FBQUE7QUFxQlVBLGdCQXJCVjtBQUFBLHdFQXVCZ0JBLElBdkJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBeUJJQyxvQkFBUUYsS0FBUjs7QUF6QkosNkNBMkJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTNCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZUssRzs7Ozs7QUFWZjs7OztBQUNBOzs7O2tCQTJDZUEsRzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN6Q2Y7Ozs7QUFDQTs7OztBQUVBOztBQUNBOzs7O0FBQ0E7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7O0FBRUE7Ozs7OztBQUVBOzs7OztJQUtNQyxNOzs7QUFtQkosa0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxzSUFDWEEsS0FEVzs7QUFFakIsVUFBS0MsS0FBTCxHQUFhO0FBQ1hDLGNBQVE7QUFERyxLQUFiOztBQUlBLFVBQUtDLGFBQUwsR0FBcUIsTUFBS0EsYUFBTCxDQUFtQkMsSUFBbkIsT0FBckI7QUFDQSxVQUFLQyxlQUFMLEdBQXVCLE1BQUtBLGVBQUwsQ0FBcUJELElBQXJCLE9BQXZCO0FBUGlCO0FBUWxCOzs7Ozs7Ozs7Ozs7O3lCQUk2QixLQUFLSixLLEVBQXZCTSxJLFVBQUFBLEksRUFBTUMsTyxVQUFBQSxPO0FBQ0Z6QiwwQixHQUFzQndCLEksQ0FBMUJFLEUsRUFBZ0IzQixLLEdBQVV5QixJLENBQVZ6QixLO0FBQ1pFLDJCLEdBQWdCd0IsTyxDQUFwQkMsRTs7dUJBRVUsbUJBQWU7QUFDL0IzQiw4QkFEK0I7QUFFL0JDLHdDQUYrQjtBQUcvQkM7QUFIK0IsaUJBQWYsQzs7O0FBQVpNLG1COzs7QUFNTixvQkFBSUEsSUFBSUcsVUFBSixHQUFpQixHQUFyQixFQUEwQjtBQUN4Qix1QkFBS2lCLFFBQUwsQ0FBYyxFQUFFUCxRQUFRLElBQVYsRUFBZDtBQUNEOzs7Ozs7OztBQUVEUCx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzBCQU0wQixLQUFLTyxLLEVBQXZCTSxJLFdBQUFBLEksRUFBTUMsTyxXQUFBQSxPO0FBQ0Z6QiwwQixHQUFzQndCLEksQ0FBMUJFLEUsRUFBZ0IzQixLLEdBQVV5QixJLENBQVZ6QixLO0FBQ1pFLDJCLEdBQWdCd0IsTyxDQUFwQkMsRTs7dUJBRTRCLHNCQUFrQjtBQUNwRDNCLDhCQURvRDtBQUVwREMsd0NBRm9EO0FBR3BEQztBQUhvRCxpQkFBbEIsQzs7OztBQUE1QlMsMEIsU0FBQUEsVTtBQUFZQyxxQixTQUFBQSxLOztzQkFNaEJELGNBQWMsRzs7Ozs7QUFDaEJHLHdCQUFRZSxHQUFSLENBQVlqQixLQUFaOzs7OztBQUlGLHFCQUFLZ0IsUUFBTCxDQUFjLEVBQUVQLFFBQVEsSUFBVixFQUFkOzs7Ozs7OztBQUVBUCx3QkFBUUYsS0FBUjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzBCQU0wQixLQUFLTyxLLEVBQXZCTSxJLFdBQUFBLEksRUFBTUMsTyxXQUFBQSxPO0FBQ0Z6QiwwQixHQUFzQndCLEksQ0FBMUJFLEUsRUFBZ0IzQixLLEdBQVV5QixJLENBQVZ6QixLO0FBQ1pFLDJCLEdBQWdCd0IsTyxDQUFwQkMsRTs7dUJBRVUsc0JBQWtCO0FBQ2xDM0IsOEJBRGtDO0FBRWxDQyx3Q0FGa0M7QUFHbENDO0FBSGtDLGlCQUFsQixDOzs7QUFBWk0sbUI7O3NCQU1GQSxJQUFJRyxVQUFKLElBQWtCLEc7Ozs7O0FBQ3BCRyx3QkFBUWUsR0FBUixDQUFZckIsSUFBSUksS0FBaEI7Ozs7O0FBSUYscUJBQUtnQixRQUFMLENBQWMsRUFBRVAsUUFBUSxLQUFWLEVBQWQ7Ozs7Ozs7O0FBRUFQLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7NkJBSUs7QUFBQSxVQUNDa0IsT0FERCxHQUNhLEtBQUtYLEtBRGxCLENBQ0NXLE9BREQ7QUFBQSxVQUVDVCxNQUZELEdBRVksS0FBS0QsS0FGakIsQ0FFQ0MsTUFGRDs7O0FBSVAsYUFDRTtBQUFBO0FBQUEsVUFBSyxXQUFXUyxRQUFRQyxJQUF4QjtBQUNFO0FBQUE7QUFBQTtBQUNFLHdCQURGO0FBRUUsbUJBQU9WLFNBQVMsUUFBVCxHQUFvQixJQUY3QjtBQUdFLHFCQUFTLENBQUNBLE1BQUQsR0FBVSxLQUFLQyxhQUFmLEdBQStCLEtBQUtFLGVBSC9DO0FBSUUsdUJBQVdNLFFBQVFFO0FBSnJCO0FBTUcsV0FBQ1gsTUFBRCxHQUFVLFFBQVYsR0FBcUI7QUFOeEI7QUFERixPQURGO0FBWUQ7OztxQkF0SUg7Ozs7O0FBdUJNSCxNLENBQ0dlLFMsR0FBWTtBQUNqQjs7O0FBR0FILFdBQVMsb0JBQVVJLE1BQVYsQ0FBaUJDLFVBSlQ7O0FBTWpCOzs7QUFHQVYsUUFBTSxvQkFBVVMsTUFBVixDQUFpQkMsVUFUTjs7QUFXakI7Ozs7QUFJQVQsV0FBUyxvQkFBVVEsTUFBVixDQUFpQkM7QUFmVCxDOztBQWdIckIsSUFBTUMsa0JBQWtCLFNBQWxCQSxlQUFrQjtBQUFBLE1BQUdDLFFBQUgsU0FBR0EsUUFBSDtBQUFBLFNBQW1CLEVBQUVaLE1BQU1ZLFNBQVNaLElBQWpCLEVBQW5CO0FBQUEsQ0FBeEI7O2tCQUVlLHlCQUFRVyxlQUFSLEVBQXlCLDBDQUFtQmxCLE1BQW5CLENBQXpCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFJZjs7Ozs7QUFLQSxJQUFNb0IsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCUixVQUFNO0FBQ0pTLGNBQVE7QUFESixLQURtQjtBQUl6QlIsWUFBUTtBQUNOUyxvQkFBYyxNQURSO0FBRU5DLGtCQUFZO0FBRk47QUFKaUIsR0FBWjtBQUFBLENBQWY7O2tCQVVlSixNIiwiZmlsZSI6IjI1LmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqIEBwYXJhbSB7b2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZm9sbG93ZXJJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZm9sbG93aW5nSWRcbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqL1xuYXN5bmMgZnVuY3Rpb24gY3JlYXRlKHsgdG9rZW4sIGZvbGxvd2VySWQsIGZvbGxvd2luZ0lkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvZm9sbG93LyR7Zm9sbG93ZXJJZH0vJHtmb2xsb3dpbmdJZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgY3JlYXRlO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvZm9sbG93L2NyZWF0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqIEBwYXJhbSB7b2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZm9sbG93ZXJJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZm9sbG93aW5nSWRcbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqL1xuYXN5bmMgZnVuY3Rpb24gZGVsZXRlRm9sbG93ZXIoeyB0b2tlbiwgZm9sbG93ZXJJZCwgZm9sbG93aW5nSWQgfSkge1xuICB0cnkge1xuICAgIGNvbnN0IHVybCA9IGAke0hPU1R9L2FwaS9mb2xsb3cvJHtmb2xsb3dlcklkfS8ke2ZvbGxvd2luZ0lkfWA7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0RFTEVURScsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGRlbGV0ZUZvbGxvd2VyO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvZm9sbG93L2RlbGV0ZS5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi9jb25maWcnO1xuXG4vKipcbiAqIEBwYXJhbSB7b2JqZWN0fSBwYXlsb2FkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlblxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZm9sbG93ZXJJZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuZm9sbG93aW5nSWRcbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0KHsgdG9rZW4sIGZvbGxvd2VySWQsIGZvbGxvd2luZ0lkIH0pIHtcbiAgdHJ5IHtcbiAgICBjb25zdCB1cmwgPSBgJHtIT1NUfS9hcGkvZm9sbG93LyR7Zm9sbG93ZXJJZH0vJHtmb2xsb3dpbmdJZH1gO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdHRVQnLFxuICAgICAgaGVhZGVyczoge1xuICAgICAgICBBY2NlcHQ6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgQXV0aG9yaXphdGlvbjogdG9rZW4sXG4gICAgICB9LFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9mb2xsb3cvZ2V0LmpzIiwiLyoqXG4gKiBUaGlzIGNvbXBvbmVudCBpcyBjcmVhdGUgdG8gZm9sbG93IG9yIGZvbGxvd2VyXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IHdpdGhTdHlsZXMgfSBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMnO1xuaW1wb3J0IEJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9CdXR0b24nO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcblxuaW1wb3J0IGFwaUdldEZvbGxvd2VyIGZyb20gJy4uLy4uL2FwaS9mb2xsb3cvZ2V0JztcbmltcG9ydCBhcGlDcmVhdGVGb2xsb3dlciBmcm9tICcuLi8uLi9hcGkvZm9sbG93L2NyZWF0ZSc7XG5pbXBvcnQgYXBpRGVsZXRlRm9sbG93ZXIgZnJvbSAnLi4vLi4vYXBpL2ZvbGxvdy9kZWxldGUnO1xuXG5pbXBvcnQgc3R5bGVzIGZyb20gJy4vc3R5bGVzJztcblxuLyoqXG4gKiBGb2xsb3cgY29tcG9uZW50IGlzIGRlc2lnbmVkIGZvciBkaXNwbGF5aW5nIGEgdXNlclxuICogaXMgZm9sbG93aW5nIGFueSBlbGVtZW50IG9yIG5vdC5cbiAqXG4gKi9cbmNsYXNzIEZvbGxvdyBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgLyoqXG4gICAgICogY2xhc3NlcyBvYmplY3QgY29udGFpbnMgdGhlIHN0eWxlcyBmb3IgRm9sbG93IGNvbXBvbmVudC5cbiAgICAgKi9cbiAgICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgICAvKipcbiAgICAgKiB1c2VyIG9iamVjdCBjb250YWlucyB1c2VyJ3MgaW5mby4gdGhpcyBjb21lcyBmcm9tIHJlZHV4IHN0b3JlLlxuICAgICAqL1xuICAgIHVzZXI6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICAgIC8qKlxuICAgICAqIGVsZW1lbnQgb2JqZWN0IGNvbnRhaW5zIGFsbCB0aGUgaW5mbyBvZiBlbGVtZW50IG9mIHdoaWNoIHVzZXIgaXNcbiAgICAgKiBnb2luZyB0byBmb2xsb3cgb3IgdW5mb2xsb3cuXG4gICAgICovXG4gICAgZWxlbWVudDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICB9O1xuXG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICBmb2xsb3c6IGZhbHNlLFxuICAgIH07XG5cbiAgICB0aGlzLm9uQ2xpY2tGb2xsb3cgPSB0aGlzLm9uQ2xpY2tGb2xsb3cuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uQ2xpY2tVbmZvbGxvdyA9IHRoaXMub25DbGlja1VuZm9sbG93LmJpbmQodGhpcyk7XG4gIH1cblxuICBhc3luYyBjb21wb25lbnRXaWxsTW91bnQoKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgdXNlciwgZWxlbWVudCB9ID0gdGhpcy5wcm9wcztcbiAgICAgIGNvbnN0IHsgaWQ6IGZvbGxvd2VySWQsIHRva2VuIH0gPSB1c2VyO1xuICAgICAgY29uc3QgeyBpZDogZm9sbG93aW5nSWQgfSA9IGVsZW1lbnQ7XG5cbiAgICAgIGNvbnN0IHJlcyA9IGF3YWl0IGFwaUdldEZvbGxvd2VyKHtcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIGZvbGxvd2VySWQsXG4gICAgICAgIGZvbGxvd2luZ0lkLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSA8IDMwMCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZm9sbG93OiB0cnVlIH0pO1xuICAgICAgfVxuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICB9XG4gIH1cblxuICBhc3luYyBvbkNsaWNrRm9sbG93KCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IHVzZXIsIGVsZW1lbnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IGlkOiBmb2xsb3dlcklkLCB0b2tlbiB9ID0gdXNlcjtcbiAgICAgIGNvbnN0IHsgaWQ6IGZvbGxvd2luZ0lkIH0gPSBlbGVtZW50O1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yIH0gPSBhd2FpdCBhcGlDcmVhdGVGb2xsb3dlcih7XG4gICAgICAgIHRva2VuLFxuICAgICAgICBmb2xsb3dlcklkLFxuICAgICAgICBmb2xsb3dpbmdJZCxcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgY29uc29sZS5sb2coZXJyb3IpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBmb2xsb3c6IHRydWUgfSk7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH1cbiAgfVxuXG4gIGFzeW5jIG9uQ2xpY2tVbmZvbGxvdygpIHtcbiAgICB0cnkge1xuICAgICAgY29uc3QgeyB1c2VyLCBlbGVtZW50IH0gPSB0aGlzLnByb3BzO1xuICAgICAgY29uc3QgeyBpZDogZm9sbG93ZXJJZCwgdG9rZW4gfSA9IHVzZXI7XG4gICAgICBjb25zdCB7IGlkOiBmb2xsb3dpbmdJZCB9ID0gZWxlbWVudDtcblxuICAgICAgY29uc3QgcmVzID0gYXdhaXQgYXBpRGVsZXRlRm9sbG93ZXIoe1xuICAgICAgICB0b2tlbixcbiAgICAgICAgZm9sbG93ZXJJZCxcbiAgICAgICAgZm9sbG93aW5nSWQsXG4gICAgICB9KTtcblxuICAgICAgaWYgKHJlcy5zdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICBjb25zb2xlLmxvZyhyZXMuZXJyb3IpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBmb2xsb3c6IGZhbHNlIH0pO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICB9XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzIH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgZm9sbG93IH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8QnV0dG9uXG4gICAgICAgICAgcmFpc2VkXG4gICAgICAgICAgY29sb3I9e2ZvbGxvdyA/ICdhY2NlbnQnIDogbnVsbH1cbiAgICAgICAgICBvbkNsaWNrPXshZm9sbG93ID8gdGhpcy5vbkNsaWNrRm9sbG93IDogdGhpcy5vbkNsaWNrVW5mb2xsb3d9XG4gICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmJ1dHRvbn1cbiAgICAgICAgPlxuICAgICAgICAgIHshZm9sbG93ID8gJ0ZvbGxvdycgOiAnRm9sbG93aW5nJ31cbiAgICAgICAgPC9CdXR0b24+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cyB9KSA9PiAoeyB1c2VyOiBlbGVtZW50cy51c2VyIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcykod2l0aFN0eWxlcyhzdHlsZXMpKEZvbGxvdykpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0ZvbGxvdy9pbmRleC5qcyIsIi8qKlxuICogVGhpcyBjb21wb25lbnQgaXMgY3JlYXRlIHRvIGZvbGxvdyBvciBmb2xsb3dlclxuICogQGZvcm1hdFxuICovXG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIG1hcmdpbjogJ2F1dG8nLFxuICB9LFxuICBidXR0b246IHtcbiAgICBib3JkZXJSYWRpdXM6ICcxNXB4JyxcbiAgICBmb250V2VpZ2h0OiAnYm9sZCcsXG4gIH0sXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGVzO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0ZvbGxvdy9zdHlsZXMuanMiXSwic291cmNlUm9vdCI6IiJ9