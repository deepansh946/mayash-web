webpackJsonp([39,24],{

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui/AppBar/AppBar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/AppBar/AppBar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Paper = __webpack_require__(/*! ../Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent Paper

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      boxSizing: 'border-box', // Prevent padding issue with the Modal and fixed positioned AppBar.
      zIndex: theme.zIndex.appBar,
      flexShrink: 0
    },
    positionFixed: {
      position: 'fixed',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionAbsolute: {
      position: 'absolute',
      top: 0,
      left: 'auto',
      right: 0
    },
    positionStatic: {
      position: 'static',
      flexShrink: 0
    },
    colorDefault: {
      backgroundColor: theme.palette.background.appBar,
      color: theme.palette.getContrastText(theme.palette.background.appBar)
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500],
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200,
      color: theme.palette.getContrastText(theme.palette.secondary.A200)
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']);

var babelPluginFlowReactPropTypes_proptype_Position = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'primary', 'accent', 'default']).isRequired,

  /**
   * The positioning type.
   */
  position: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['static', 'fixed', 'absolute']).isRequired
};

var AppBar = function (_React$Component) {
  (0, _inherits3.default)(AppBar, _React$Component);

  function AppBar() {
    (0, _classCallCheck3.default)(this, AppBar);
    return (0, _possibleConstructorReturn3.default)(this, (AppBar.__proto__ || (0, _getPrototypeOf2.default)(AppBar)).apply(this, arguments));
  }

  (0, _createClass3.default)(AppBar, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          position = _props.position,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'position']);


      var className = (0, _classnames2.default)(classes.root, classes['position' + (0, _helpers.capitalizeFirstLetter)(position)], (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), (0, _defineProperty3.default)(_classNames, 'mui-fixed', position === 'fixed'), _classNames), classNameProp);

      return _react2.default.createElement(
        _Paper2.default,
        (0, _extends3.default)({ square: true, component: 'header', elevation: 4, className: className }, other),
        children
      );
    }
  }]);
  return AppBar;
}(_react2.default.Component);

AppBar.defaultProps = {
  color: 'primary',
  position: 'fixed'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAppBar' })(AppBar);

/***/ }),

/***/ "./node_modules/material-ui/AppBar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/AppBar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _AppBar = __webpack_require__(/*! ./AppBar */ "./node_modules/material-ui/AppBar/AppBar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_AppBar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Avatar/Avatar.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/Avatar/Avatar.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _colorManipulator = __webpack_require__(/*! ../styles/colorManipulator */ "./node_modules/material-ui/styles/colorManipulator.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_ElementType = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_ElementType || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'relative',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexShrink: 0,
      width: 40,
      height: 40,
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.pxToRem(20),
      borderRadius: '50%',
      overflow: 'hidden',
      userSelect: 'none'
    },
    colorDefault: {
      color: theme.palette.background.default,
      backgroundColor: (0, _colorManipulator.emphasize)(theme.palette.background.default, 0.26)
    },
    img: {
      maxWidth: '100%',
      width: '100%',
      height: 'auto',
      textAlign: 'center'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Used in combination with `src` or `srcSet` to
   * provide an alt attribute for the rendered `img` element.
   */
  alt: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Used to render icon or text elements inside the Avatar.
   * `src` and `alt` props will not be used and no `img` will
   * be rendered by default.
   *
   * This can be an element, or just a string.
   */
  children: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * The className of the child element.
   * Used by Chip and ListItemIcon to style the Avatar icon.
   */
  childrenClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The component used for the root node.
   * Either a string to use a DOM element or a component.
   */
  component: typeof babelPluginFlowReactPropTypes_proptype_ElementType === 'function' ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired ? babelPluginFlowReactPropTypes_proptype_ElementType.isRequired : babelPluginFlowReactPropTypes_proptype_ElementType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ElementType).isRequired,

  /**
   * Properties applied to the `img` element when the component
   * is used to display an image.
   */
  imgProps: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The `sizes` attribute for the `img` element.
   */
  sizes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `src` attribute for the `img` element.
   */
  src: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The `srcSet` attribute for the `img` element.
   */
  srcSet: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string
};

var Avatar = function (_React$Component) {
  (0, _inherits3.default)(Avatar, _React$Component);

  function Avatar() {
    (0, _classCallCheck3.default)(this, Avatar);
    return (0, _possibleConstructorReturn3.default)(this, (Avatar.__proto__ || (0, _getPrototypeOf2.default)(Avatar)).apply(this, arguments));
  }

  (0, _createClass3.default)(Avatar, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          alt = _props.alt,
          classes = _props.classes,
          classNameProp = _props.className,
          childrenProp = _props.children,
          childrenClassNameProp = _props.childrenClassName,
          ComponentProp = _props.component,
          imgProps = _props.imgProps,
          sizes = _props.sizes,
          src = _props.src,
          srcSet = _props.srcSet,
          other = (0, _objectWithoutProperties3.default)(_props, ['alt', 'classes', 'className', 'children', 'childrenClassName', 'component', 'imgProps', 'sizes', 'src', 'srcSet']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes.colorDefault, childrenProp && !src && !srcSet), classNameProp);
      var children = null;

      if (childrenProp) {
        if (childrenClassNameProp && typeof childrenProp !== 'string' && _react2.default.isValidElement(childrenProp)) {
          var _childrenClassName = (0, _classnames2.default)(childrenClassNameProp, childrenProp.props.className);
          children = _react2.default.cloneElement(childrenProp, { className: _childrenClassName });
        } else {
          children = childrenProp;
        }
      } else if (src || srcSet) {
        children = _react2.default.createElement('img', (0, _extends3.default)({
          alt: alt,
          src: src,
          srcSet: srcSet,
          sizes: sizes,
          className: classes.img
        }, imgProps));
      }

      return _react2.default.createElement(
        ComponentProp,
        (0, _extends3.default)({ className: className }, other),
        children
      );
    }
  }]);
  return Avatar;
}(_react2.default.Component);

Avatar.defaultProps = {
  component: 'div'
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiAvatar' })(Avatar);

/***/ }),

/***/ "./node_modules/material-ui/Avatar/index.js":
/*!**************************************************!*\
  !*** ./node_modules/material-ui/Avatar/index.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Avatar = __webpack_require__(/*! ./Avatar */ "./node_modules/material-ui/Avatar/Avatar.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Avatar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Icon/Icon.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Icon/Icon.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      userSelect: 'none'
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The name of the icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired
};

var Icon = function (_React$Component) {
  (0, _inherits3.default)(Icon, _React$Component);

  function Icon() {
    (0, _classCallCheck3.default)(this, Icon);
    return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
  }

  (0, _createClass3.default)(Icon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color']);


      var className = (0, _classnames2.default)('material-icons', classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'span',
        (0, _extends3.default)({ className: className, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Icon;
}(_react2.default.Component);

Icon.defaultProps = {
  color: 'inherit'
};
Icon.muiName = 'Icon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIcon' })(Icon);

/***/ }),

/***/ "./node_modules/material-ui/Icon/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Icon/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/material-ui/Icon/Icon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Icon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/Tabs/Tab.js":
/*!**********************************************!*\
  !*** ./node_modules/material-ui/Tabs/Tab.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _extends3 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends4 = _interopRequireDefault(_extends3);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Element = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Element || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;
// @inheritedComponent ButtonBase

var styles = exports.styles = function styles(theme) {
  return {
    root: (0, _extends4.default)({}, theme.typography.button, (0, _defineProperty3.default)({
      maxWidth: 264,
      position: 'relative',
      minWidth: 72,
      padding: 0,
      height: 48,
      flex: 'none',
      overflow: 'hidden'
    }, theme.breakpoints.up('md'), {
      minWidth: 160
    })),
    rootLabelIcon: {
      height: 72
    },
    rootAccent: {
      color: theme.palette.text.secondary
    },
    rootAccentSelected: {
      color: theme.palette.secondary.A200
    },
    rootAccentDisabled: {
      color: theme.palette.text.disabled
    },
    rootPrimary: {
      color: theme.palette.text.secondary
    },
    rootPrimarySelected: {
      color: theme.palette.primary[500]
    },
    rootPrimaryDisabled: {
      color: theme.palette.text.disabled
    },
    rootInherit: {
      color: 'inherit',
      opacity: 0.7
    },
    rootInheritSelected: {
      opacity: 1
    },
    rootInheritDisabled: {
      opacity: 0.4
    },
    fullWidth: {
      flexGrow: 1
    },
    wrapper: {
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      flexDirection: 'column'
    },
    labelContainer: (0, _defineProperty3.default)({
      paddingTop: 6,
      paddingBottom: 6,
      paddingLeft: 12,
      paddingRight: 12
    }, theme.breakpoints.up('md'), {
      paddingLeft: theme.spacing.unit * 3,
      paddingRight: theme.spacing.unit * 3
    }),
    label: (0, _defineProperty3.default)({
      fontSize: theme.typography.pxToRem(theme.typography.fontSize),
      whiteSpace: 'normal'
    }, theme.breakpoints.up('md'), {
      fontSize: theme.typography.pxToRem(theme.typography.fontSize - 1)
    }),
    labelWrapped: (0, _defineProperty3.default)({}, theme.breakpoints.down('md'), {
      fontSize: theme.typography.pxToRem(theme.typography.fontSize - 2)
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tab will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * @ignore
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * The icon element. If a string is provided, it will be used as a font ligature.
   */
  icon: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   * For server side rendering consideration, we let the selected tab
   * render the indicator.
   */
  indicator: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * The label element.
   */
  label: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string, typeof babelPluginFlowReactPropTypes_proptype_Element === 'function' ? babelPluginFlowReactPropTypes_proptype_Element : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Element)]),

  /**
   * @ignore
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * @ignore
   */
  selected: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool,

  /**
   * @ignore
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  textColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]),

  /**
   * You can provide your own value. Otherwise, we fallback to the child position index.
   */
  value: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any
};

var Tab = function (_React$Component) {
  (0, _inherits3.default)(Tab, _React$Component);

  function Tab() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Tab);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Tab.__proto__ || (0, _getPrototypeOf2.default)(Tab)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      wrappedText: false
    }, _this.handleChange = function (event) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          value = _this$props.value,
          onClick = _this$props.onClick;


      if (onChange) {
        onChange(event, value);
      }

      if (onClick) {
        onClick(event);
      }
    }, _this.label = undefined, _this.checkTextWrap = function () {
      if (_this.label) {
        var _wrappedText = _this.label.getClientRects().length > 1;
        if (_this.state.wrappedText !== _wrappedText) {
          _this.setState({ wrappedText: _wrappedText });
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Tab, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.checkTextWrap();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      if (this.state.wrappedText === prevState.wrappedText) {
        /**
         * At certain text and tab lengths, a larger font size may wrap to two lines while the smaller
         * font size still only requires one line.  This check will prevent an infinite render loop
         * fron occurring in that scenario.
         */
        this.checkTextWrap();
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this,
          _classNames2;

      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          disabled = _props.disabled,
          fullWidth = _props.fullWidth,
          iconProp = _props.icon,
          indicator = _props.indicator,
          labelProp = _props.label,
          onChange = _props.onChange,
          selected = _props.selected,
          styleProp = _props.style,
          textColor = _props.textColor,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'disabled', 'fullWidth', 'icon', 'indicator', 'label', 'onChange', 'selected', 'style', 'textColor', 'value']);


      var icon = void 0;

      if (iconProp !== undefined) {
        icon = _react2.default.isValidElement(iconProp) ? iconProp : _react2.default.createElement(
          _Icon2.default,
          null,
          iconProp
        );
      }

      var label = void 0;

      if (labelProp !== undefined) {
        label = _react2.default.createElement(
          'div',
          { className: classes.labelContainer },
          _react2.default.createElement(
            'span',
            {
              className: (0, _classnames2.default)(classes.label, (0, _defineProperty3.default)({}, classes.labelWrapped, this.state.wrappedText)),
              ref: function ref(node) {
                _this2.label = node;
              }
            },
            labelProp
          )
        );
      }

      var className = (0, _classnames2.default)(classes.root, (_classNames2 = {}, (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor)], true), (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor) + 'Disabled'], disabled), (0, _defineProperty3.default)(_classNames2, classes['root' + (0, _helpers.capitalizeFirstLetter)(textColor) + 'Selected'], selected), (0, _defineProperty3.default)(_classNames2, classes.rootLabelIcon, icon && label), (0, _defineProperty3.default)(_classNames2, classes.fullWidth, fullWidth), _classNames2), classNameProp);

      var style = {};

      if (textColor !== 'accent' && textColor !== 'inherit') {
        style.color = textColor;
      }

      style = (0, _keys2.default)(style).length > 0 ? (0, _extends4.default)({}, style, styleProp) : styleProp;

      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends4.default)({
          focusRipple: true,
          className: className,
          style: style,
          role: 'tab',
          'aria-selected': selected,
          disabled: disabled
        }, other, {
          onClick: this.handleChange
        }),
        _react2.default.createElement(
          'span',
          { className: classes.wrapper },
          icon,
          label
        ),
        indicator
      );
    }
  }]);
  return Tab;
}(_react2.default.Component);

Tab.defaultProps = {
  disabled: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTab' })(Tab);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/TabIndicator.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui/Tabs/TabIndicator.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _ref; //  weak

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      position: 'absolute',
      height: 2,
      bottom: 0,
      width: '100%',
      transition: theme.transitions.create(),
      willChange: 'left, width'
    },
    colorAccent: {
      backgroundColor: theme.palette.secondary.A200
    },
    colorPrimary: {
      backgroundColor: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_IndicatorStyle = {
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
};
var babelPluginFlowReactPropTypes_proptype_ProvidedProps = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired
};
var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * @ignore
   * The color of the tab indicator.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired,

  /**
   * @ignore
   * The style of the root element.
   */
  style: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
    left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
    width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
  }).isRequired
};


/**
 * @ignore - internal component.
 */
function TabIndicator(props) {
  var classes = props.classes,
      classNameProp = props.className,
      color = props.color,
      styleProp = props.style;

  var colorPredefined = ['primary', 'accent'].indexOf(color) !== -1;
  var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], colorPredefined), classNameProp);

  var style = colorPredefined ? styleProp : (0, _extends3.default)({}, styleProp, {
    backgroundColor: color
  });

  return _react2.default.createElement('div', { className: className, style: style });
}

TabIndicator.propTypes =  true ? (_ref = {
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object.isRequired
}, (0, _defineProperty3.default)(_ref, 'classes', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object), (0, _defineProperty3.default)(_ref, 'className', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string), (0, _defineProperty3.default)(_ref, 'color', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired), (0, _defineProperty3.default)(_ref, 'style', __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape({
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number,
  width: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number
}).isRequired), _ref) : {};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTabIndicator' })(TabIndicator);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/TabScrollButton.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui/Tabs/TabScrollButton.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _KeyboardArrowLeft = __webpack_require__(/*! ../svg-icons/KeyboardArrowLeft */ "./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js");

var _KeyboardArrowLeft2 = _interopRequireDefault(_KeyboardArrowLeft);

var _KeyboardArrowRight = __webpack_require__(/*! ../svg-icons/KeyboardArrowRight */ "./node_modules/material-ui/svg-icons/KeyboardArrowRight.js");

var _KeyboardArrowRight2 = _interopRequireDefault(_KeyboardArrowRight);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//  weak

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      color: 'inherit',
      flex: '0 0 ' + theme.spacing.unit * 7 + 'px'
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Which direction should the button indicate?
   */
  direction: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['left', 'right']).isRequired,

  /**
   * Callback to execute for button press.
   */
  onClick: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * Should the button be present or just consume space.
   */
  visible: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool
};

var _ref = _react2.default.createElement(_KeyboardArrowLeft2.default, null);

var _ref2 = _react2.default.createElement(_KeyboardArrowRight2.default, null);

/**
 * @ignore - internal component.
 */
var TabScrollButton = function (_React$Component) {
  (0, _inherits3.default)(TabScrollButton, _React$Component);

  function TabScrollButton() {
    (0, _classCallCheck3.default)(this, TabScrollButton);
    return (0, _possibleConstructorReturn3.default)(this, (TabScrollButton.__proto__ || (0, _getPrototypeOf2.default)(TabScrollButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(TabScrollButton, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          classNameProp = _props.className,
          direction = _props.direction,
          onClick = _props.onClick,
          visible = _props.visible,
          other = (0, _objectWithoutProperties3.default)(_props, ['classes', 'className', 'direction', 'onClick', 'visible']);


      var className = (0, _classnames2.default)(classes.root, classNameProp);

      if (!visible) {
        return _react2.default.createElement('div', { className: className });
      }

      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({ className: className, onClick: onClick, tabIndex: -1 }, other),
        direction === 'left' ? _ref : _ref2
      );
    }
  }]);
  return TabScrollButton;
}(_react2.default.Component);

TabScrollButton.defaultProps = {
  visible: true
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiTabScrollButton' })(TabScrollButton);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/Tabs.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Tabs/Tabs.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _isNan = __webpack_require__(/*! babel-runtime/core-js/number/is-nan */ "./node_modules/babel-runtime/core-js/number/is-nan.js");

var _isNan2 = _interopRequireDefault(_isNan);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _warning = __webpack_require__(/*! warning */ "./node_modules/warning/browser.js");

var _warning2 = _interopRequireDefault(_warning);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _debounce = __webpack_require__(/*! lodash/debounce */ "./node_modules/lodash/debounce.js");

var _debounce2 = _interopRequireDefault(_debounce);

var _reactScrollbarSize = __webpack_require__(/*! react-scrollbar-size */ "./node_modules/react-scrollbar-size/index.js");

var _reactScrollbarSize2 = _interopRequireDefault(_reactScrollbarSize);

var _normalizeScrollLeft = __webpack_require__(/*! normalize-scroll-left */ "./node_modules/normalize-scroll-left/lib/main.js");

var _scroll = __webpack_require__(/*! scroll */ "./node_modules/scroll/index.js");

var _scroll2 = _interopRequireDefault(_scroll);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _TabIndicator = __webpack_require__(/*! ./TabIndicator */ "./node_modules/material-ui/Tabs/TabIndicator.js");

var _TabIndicator2 = _interopRequireDefault(_TabIndicator);

var _TabScrollButton = __webpack_require__(/*! ./TabScrollButton */ "./node_modules/material-ui/Tabs/TabScrollButton.js");

var _TabScrollButton2 = _interopRequireDefault(_TabScrollButton);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_ComponentType = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func;

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var babelPluginFlowReactPropTypes_proptype_IndicatorStyle = __webpack_require__(/*! ./TabIndicator */ "./node_modules/material-ui/Tabs/TabIndicator.js").babelPluginFlowReactPropTypes_proptype_IndicatorStyle || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      overflow: 'hidden',
      minHeight: 48,
      WebkitOverflowScrolling: 'touch' // Add iOS momentum scrolling.
    },
    flexContainer: {
      display: 'flex'
    },
    scrollingContainer: {
      position: 'relative',
      display: 'inline-block',
      flex: '1 1 auto',
      whiteSpace: 'nowrap'
    },
    fixed: {
      overflowX: 'hidden',
      width: '100%'
    },
    scrollable: {
      overflowX: 'scroll'
    },
    centered: {
      justifyContent: 'center'
    },
    buttonAuto: (0, _defineProperty3.default)({}, theme.breakpoints.down('sm'), {
      display: 'none'
    })
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The CSS class name of the scroll button elements.
   */
  buttonClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tabs will be centered.
   * This property is intended for large views.
   */
  centered: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The content of the component.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * If `true`, the tabs will grow to use all the available space.
   * This property is intended for small views, like on mobile.
   */
  fullWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * The CSS class name of the indicator element.
   */
  indicatorClassName: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Determines the color of the indicator.
   */
  indicatorColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOfType([__webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['primary']), __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string]).isRequired,

  /**
   * Callback fired when the value changes.
   *
   * @param {object} event The event source of the callback
   * @param {number} value We default to the index of the child
   */
  onChange: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func.isRequired,

  /**
   * True invokes scrolling properties and allow for horizontally scrolling
   * (or swiping) the tab bar.
   */
  scrollable: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Determine behavior of scroll buttons when tabs are set to scroll
   * `auto` will only present them on medium and larger viewports
   * `on` will always present them
   * `off` will never present them
   */
  scrollButtons: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['auto', 'on', 'off']).isRequired,

  /**
   * The component used to render the scroll buttons.
   */
  TabScrollButton: typeof babelPluginFlowReactPropTypes_proptype_ComponentType === 'function' ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired ? babelPluginFlowReactPropTypes_proptype_ComponentType.isRequired : babelPluginFlowReactPropTypes_proptype_ComponentType : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_ComponentType).isRequired,

  /**
   * Determines the color of the `Tab`.
   */
  textColor: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['accent', 'primary', 'inherit']).isRequired,

  /**
   * The value of the currently selected `Tab`.
   * If you don't want any selected `Tab`, you can set this property to `false`.
   */
  value: function value(props, propName, componentName) {
    if (!Object.prototype.hasOwnProperty.call(props, propName)) {
      throw new Error('Prop `' + propName + '` has type \'any\' or \'mixed\', but was not provided to `' + componentName + '`. Pass undefined or any other value.');
    }
  }
};
var babelPluginFlowReactPropTypes_proptype_TabsMeta = {
  clientWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollLeft: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollLeftNormalized: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  scrollWidth: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,

  // ClientRect
  left: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired,
  right: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").number.isRequired
};

var Tabs = function (_React$Component) {
  (0, _inherits3.default)(Tabs, _React$Component);

  function Tabs() {
    var _ref;

    var _temp, _this, _ret;

    (0, _classCallCheck3.default)(this, Tabs);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = Tabs.__proto__ || (0, _getPrototypeOf2.default)(Tabs)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      indicatorStyle: {},
      scrollerStyle: {
        marginBottom: 0
      },
      showLeftScroll: false,
      showRightScroll: false,
      mounted: false
    }, _this.getConditionalElements = function () {
      var _this$props = _this.props,
          classes = _this$props.classes,
          buttonClassName = _this$props.buttonClassName,
          scrollable = _this$props.scrollable,
          scrollButtons = _this$props.scrollButtons,
          TabScrollButtonProp = _this$props.TabScrollButton,
          theme = _this$props.theme;

      var conditionalElements = {};
      conditionalElements.scrollbarSizeListener = scrollable ? _react2.default.createElement(_reactScrollbarSize2.default, {
        onLoad: _this.handleScrollbarSizeChange,
        onChange: _this.handleScrollbarSizeChange
      }) : null;

      var showScrollButtons = scrollable && (scrollButtons === 'auto' || scrollButtons === 'on');

      conditionalElements.scrollButtonLeft = showScrollButtons ? _react2.default.createElement(TabScrollButtonProp, {
        direction: theme && theme.direction === 'rtl' ? 'right' : 'left',
        onClick: _this.handleLeftScrollClick,
        visible: _this.state.showLeftScroll,
        className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.buttonAuto, scrollButtons === 'auto'), buttonClassName)
      }) : null;

      conditionalElements.scrollButtonRight = showScrollButtons ? _react2.default.createElement(TabScrollButtonProp, {
        direction: theme && theme.direction === 'rtl' ? 'left' : 'right',
        onClick: _this.handleRightScrollClick,
        visible: _this.state.showRightScroll,
        className: (0, _classnames2.default)((0, _defineProperty3.default)({}, classes.buttonAuto, scrollButtons === 'auto'), buttonClassName)
      }) : null;

      return conditionalElements;
    }, _this.getTabsMeta = function (value, direction) {
      var tabsMeta = void 0;
      if (_this.tabs) {
        var rect = _this.tabs.getBoundingClientRect();
        // create a new object with ClientRect class props + scrollLeft
        tabsMeta = {
          clientWidth: _this.tabs ? _this.tabs.clientWidth : 0,
          scrollLeft: _this.tabs ? _this.tabs.scrollLeft : 0,
          scrollLeftNormalized: _this.tabs ? (0, _normalizeScrollLeft.getNormalizedScrollLeft)(_this.tabs, direction) : 0,
          scrollWidth: _this.tabs ? _this.tabs.scrollWidth : 0,
          left: rect.left,
          right: rect.right
        };
      }

      var tabMeta = void 0;
      if (_this.tabs && value !== false) {
        var _children = _this.tabs.children[0].children;

        if (_children.length > 0) {
          var tab = _children[_this.valueToIndex[value]];
           true ? (0, _warning2.default)(Boolean(tab), 'Material-UI: the value provided `' + value + '` is invalid') : void 0;
          tabMeta = tab ? tab.getBoundingClientRect() : null;
        }
      }
      return { tabsMeta: tabsMeta, tabMeta: tabMeta };
    }, _this.tabs = undefined, _this.valueToIndex = {}, _this.handleResize = (0, _debounce2.default)(function () {
      _this.updateIndicatorState(_this.props);
      _this.updateScrollButtonState();
    }, 166), _this.handleLeftScrollClick = function () {
      if (_this.tabs) {
        _this.moveTabsScroll(-_this.tabs.clientWidth);
      }
    }, _this.handleRightScrollClick = function () {
      if (_this.tabs) {
        _this.moveTabsScroll(_this.tabs.clientWidth);
      }
    }, _this.handleScrollbarSizeChange = function (_ref2) {
      var scrollbarHeight = _ref2.scrollbarHeight;

      _this.setState({
        scrollerStyle: {
          marginBottom: -scrollbarHeight
        }
      });
    }, _this.handleTabsScroll = (0, _debounce2.default)(function () {
      _this.updateScrollButtonState();
    }, 166), _this.moveTabsScroll = function (delta) {
      var theme = _this.props.theme;


      if (_this.tabs) {
        var themeDirection = theme && theme.direction;
        var multiplier = themeDirection === 'rtl' ? -1 : 1;
        var nextScrollLeft = _this.tabs.scrollLeft + delta * multiplier;
        // Fix for Edge
        var invert = themeDirection === 'rtl' && (0, _normalizeScrollLeft.detectScrollType)() === 'reverse' ? -1 : 1;
        _scroll2.default.left(_this.tabs, invert * nextScrollLeft);
      }
    }, _this.scrollSelectedIntoView = function () {
      var _this$props2 = _this.props,
          theme = _this$props2.theme,
          value = _this$props2.value;


      var themeDirection = theme && theme.direction;

      var _this$getTabsMeta = _this.getTabsMeta(value, themeDirection),
          tabsMeta = _this$getTabsMeta.tabsMeta,
          tabMeta = _this$getTabsMeta.tabMeta;

      if (!tabMeta || !tabsMeta) {
        return;
      }

      if (tabMeta.left < tabsMeta.left) {
        // left side of button is out of view
        var nextScrollLeft = tabsMeta.scrollLeft + (tabMeta.left - tabsMeta.left);
        _scroll2.default.left(_this.tabs, nextScrollLeft);
      } else if (tabMeta.right > tabsMeta.right) {
        // right side of button is out of view
        var _nextScrollLeft = tabsMeta.scrollLeft + (tabMeta.right - tabsMeta.right);
        _scroll2.default.left(_this.tabs, _nextScrollLeft);
      }
    }, _this.updateScrollButtonState = function () {
      var _this$props3 = _this.props,
          scrollable = _this$props3.scrollable,
          scrollButtons = _this$props3.scrollButtons,
          theme = _this$props3.theme;

      var themeDirection = theme && theme.direction;

      if (_this.tabs && scrollable && scrollButtons !== 'off') {
        var _this$tabs = _this.tabs,
            _scrollWidth = _this$tabs.scrollWidth,
            _clientWidth = _this$tabs.clientWidth;

        var _scrollLeft = (0, _normalizeScrollLeft.getNormalizedScrollLeft)(_this.tabs, themeDirection);

        var _showLeftScroll = themeDirection === 'rtl' ? _scrollWidth > _clientWidth + _scrollLeft : _scrollLeft > 0;

        var _showRightScroll = themeDirection === 'rtl' ? _scrollLeft > 0 : _scrollWidth > _clientWidth + _scrollLeft;

        if (_showLeftScroll !== _this.state.showLeftScroll || _showRightScroll !== _this.state.showRightScroll) {
          _this.setState({ showLeftScroll: _showLeftScroll, showRightScroll: _showRightScroll });
        }
      }
    }, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
  }

  (0, _createClass3.default)(Tabs, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState({ mounted: true });
      this.updateIndicatorState(this.props);
      this.updateScrollButtonState();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      this.updateScrollButtonState();

      // The index might have changed at the same time.
      // We need to check again the right indicator position.
      this.updateIndicatorState(this.props);

      if (this.state.indicatorStyle !== prevState.indicatorStyle) {
        this.scrollSelectedIntoView();
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.handleResize.cancel();
      this.handleTabsScroll.cancel();
    }
  }, {
    key: 'updateIndicatorState',
    value: function updateIndicatorState(props) {
      var theme = props.theme,
          value = props.value;


      var themeDirection = theme && theme.direction;

      var _getTabsMeta = this.getTabsMeta(value, themeDirection),
          tabsMeta = _getTabsMeta.tabsMeta,
          tabMeta = _getTabsMeta.tabMeta;

      var left = 0;

      if (tabMeta && tabsMeta) {
        var correction = themeDirection === 'rtl' ? tabsMeta.scrollLeftNormalized + tabsMeta.clientWidth - tabsMeta.scrollWidth : tabsMeta.scrollLeft;
        left = tabMeta.left - tabsMeta.left + correction;
      }

      var indicatorStyle = {
        left: left,
        // May be wrong until the font is loaded.
        width: tabMeta ? tabMeta.width : 0
      };

      if ((indicatorStyle.left !== this.state.indicatorStyle.left || indicatorStyle.width !== this.state.indicatorStyle.width) && !(0, _isNan2.default)(indicatorStyle.left) && !(0, _isNan2.default)(indicatorStyle.width)) {
        this.setState({ indicatorStyle: indicatorStyle });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _classNames3,
          _this2 = this;

      var _props = this.props,
          buttonClassName = _props.buttonClassName,
          centered = _props.centered,
          classes = _props.classes,
          childrenProp = _props.children,
          classNameProp = _props.className,
          fullWidth = _props.fullWidth,
          indicatorClassName = _props.indicatorClassName,
          indicatorColor = _props.indicatorColor,
          onChange = _props.onChange,
          scrollable = _props.scrollable,
          scrollButtons = _props.scrollButtons,
          TabScrollButtonProp = _props.TabScrollButton,
          textColor = _props.textColor,
          theme = _props.theme,
          value = _props.value,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonClassName', 'centered', 'classes', 'children', 'className', 'fullWidth', 'indicatorClassName', 'indicatorColor', 'onChange', 'scrollable', 'scrollButtons', 'TabScrollButton', 'textColor', 'theme', 'value']);


      var className = (0, _classnames2.default)(classes.root, classNameProp);
      var scrollerClassName = (0, _classnames2.default)(classes.scrollingContainer, (_classNames3 = {}, (0, _defineProperty3.default)(_classNames3, classes.fixed, !scrollable), (0, _defineProperty3.default)(_classNames3, classes.scrollable, scrollable), _classNames3));
      var tabItemContainerClassName = (0, _classnames2.default)(classes.flexContainer, (0, _defineProperty3.default)({}, classes.centered, centered && !scrollable));

      var indicator = _react2.default.createElement(_TabIndicator2.default, {
        style: this.state.indicatorStyle,
        className: indicatorClassName,
        color: indicatorColor
      });

      this.valueToIndex = {};
      var childIndex = 0;
      var children = _react2.default.Children.map(childrenProp, function (child) {
        if (!_react2.default.isValidElement(child)) {
          return null;
        }

        var childValue = child.props.value || childIndex;
        _this2.valueToIndex[childValue] = childIndex;
        var selected = childValue === value;

        childIndex += 1;
        return _react2.default.cloneElement(child, {
          fullWidth: fullWidth,
          indicator: selected && !_this2.state.mounted && indicator,
          selected: selected,
          onChange: onChange,
          textColor: textColor,
          value: childValue
        });
      });

      var conditionalElements = this.getConditionalElements();

      return _react2.default.createElement(
        'div',
        (0, _extends3.default)({ className: className }, other),
        _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }),
        conditionalElements.scrollbarSizeListener,
        _react2.default.createElement(
          'div',
          { className: classes.flexContainer },
          conditionalElements.scrollButtonLeft,
          _react2.default.createElement(
            'div',
            {
              className: scrollerClassName,
              style: this.state.scrollerStyle,
              ref: function ref(node) {
                _this2.tabs = node;
              },
              role: 'tablist',
              onScroll: this.handleTabsScroll
            },
            _react2.default.createElement(
              'div',
              { className: tabItemContainerClassName },
              children
            ),
            this.state.mounted && indicator
          ),
          conditionalElements.scrollButtonRight
        )
      );
    }
  }]);
  return Tabs;
}(_react2.default.Component);

Tabs.defaultProps = {
  centered: false,
  fullWidth: false,
  indicatorColor: 'accent',
  scrollable: false,
  scrollButtons: 'auto',
  TabScrollButton: _TabScrollButton2.default,
  textColor: 'inherit'
};
exports.default = (0, _withStyles2.default)(styles, { withTheme: true, name: 'MuiTabs' })(Tabs);

/***/ }),

/***/ "./node_modules/material-ui/Tabs/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Tabs/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Tabs = __webpack_require__(/*! ./Tabs */ "./node_modules/material-ui/Tabs/Tabs.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Tabs).default;
  }
});

var _Tab = __webpack_require__(/*! ./Tab */ "./node_modules/material-ui/Tabs/Tab.js");

Object.defineProperty(exports, 'Tab', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Tab).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/pure.js":
/*!*****************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/pure.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/material-ui/node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/material-ui/node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js":
/*!***************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/setDisplayName.js ***!
  \***************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/material-ui/node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/setStatic.js":
/*!**********************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/setStatic.js ***!
  \**********************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/shallowEqual.js":
/*!*************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/shallowEqual.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/material-ui/node_modules/recompose/shouldUpdate.js":
/*!*************************************************************************!*\
  !*** ./node_modules/material-ui/node_modules/recompose/shouldUpdate.js ***!
  \*************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/material-ui/node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/material-ui/node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _react.createFactory)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js":
/*!*****************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/KeyboardArrowLeft.js ***!
  \*****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M15.41 16.09l-4.58-4.59 4.58-4.59L14 5.5l-6 6 6 6z' });

var KeyboardArrowLeft = function KeyboardArrowLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
KeyboardArrowLeft = (0, _pure2.default)(KeyboardArrowLeft);
KeyboardArrowLeft.muiName = 'SvgIcon';

exports.default = KeyboardArrowLeft;

/***/ }),

/***/ "./node_modules/material-ui/svg-icons/KeyboardArrowRight.js":
/*!******************************************************************!*\
  !*** ./node_modules/material-ui/svg-icons/KeyboardArrowRight.js ***!
  \******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/material-ui/node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @ignore - internal component.
 */
var _ref = _react2.default.createElement('path', { d: 'M8.59 16.34l4.58-4.59-4.58-4.59L10 5.75l6 6-6 6z' });

var KeyboardArrowRight = function KeyboardArrowRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};
KeyboardArrowRight = (0, _pure2.default)(KeyboardArrowRight);
KeyboardArrowRight.muiName = 'SvgIcon';

exports.default = KeyboardArrowRight;

/***/ }),

/***/ "./node_modules/normalize-scroll-left/lib/main.js":
/*!********************************************************!*\
  !*** ./node_modules/normalize-scroll-left/lib/main.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// Based on https://github.com/react-bootstrap/dom-helpers/blob/master/src/util/inDOM.js
var inDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);
var cachedType;
function _setScrollType(type) {
    cachedType = type;
}
exports._setScrollType = _setScrollType;
// Based on the jquery plugin https://github.com/othree/jquery.rtl-scroll-type
function detectScrollType() {
    if (cachedType) {
        return cachedType;
    }
    if (!inDOM || !window.document.body) {
        return 'indeterminate';
    }
    var dummy = window.document.createElement('div');
    dummy.appendChild(document.createTextNode('ABCD'));
    dummy.dir = 'rtl';
    dummy.style.fontSize = '14px';
    dummy.style.width = '4px';
    dummy.style.height = '1px';
    dummy.style.position = 'absolute';
    dummy.style.top = '-1000px';
    dummy.style.overflow = 'scroll';
    document.body.appendChild(dummy);
    cachedType = 'reverse';
    if (dummy.scrollLeft > 0) {
        cachedType = 'default';
    }
    else {
        dummy.scrollLeft = 1;
        if (dummy.scrollLeft === 0) {
            cachedType = 'negative';
        }
    }
    document.body.removeChild(dummy);
    return cachedType;
}
exports.detectScrollType = detectScrollType;
// Based on https://stackoverflow.com/a/24394376
function getNormalizedScrollLeft(element, direction) {
    var scrollLeft = element.scrollLeft;
    // Perform the calculations only when direction is rtl to avoid messing up the ltr bahavior
    if (direction !== 'rtl') {
        return scrollLeft;
    }
    var type = detectScrollType();
    if (type === 'indeterminate') {
        return Number.NaN;
    }
    switch (type) {
        case 'negative':
            return element.scrollWidth - element.clientWidth + scrollLeft;
        case 'reverse':
            return element.scrollWidth - element.clientWidth - scrollLeft;
    }
    return scrollLeft;
}
exports.getNormalizedScrollLeft = getNormalizedScrollLeft;
function setNormalizedScrollLeft(element, scrollLeft, direction) {
    // Perform the calculations only when direction is rtl to avoid messing up the ltr bahavior
    if (direction !== 'rtl') {
        element.scrollLeft = scrollLeft;
        return;
    }
    var type = detectScrollType();
    if (type === 'indeterminate') {
        return;
    }
    switch (type) {
        case 'negative':
            element.scrollLeft = element.clientWidth - element.scrollWidth + scrollLeft;
            break;
        case 'reverse':
            element.scrollLeft = element.scrollWidth - element.clientWidth - scrollLeft;
            break;
        default:
            element.scrollLeft = scrollLeft;
            break;
    }
}
exports.setNormalizedScrollLeft = setNormalizedScrollLeft;


/***/ }),

/***/ "./node_modules/rafl/index.js":
/*!************************************!*\
  !*** ./node_modules/rafl/index.js ***!
  \************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var global = __webpack_require__(/*! global */ "./node_modules/global/window.js")

/**
 * `requestAnimationFrame()`
 */

var request = global.requestAnimationFrame
  || global.webkitRequestAnimationFrame
  || global.mozRequestAnimationFrame
  || fallback

var prev = +new Date
function fallback (fn) {
  var curr = +new Date
  var ms = Math.max(0, 16 - (curr - prev))
  var req = setTimeout(fn, ms)
  return prev = curr, req
}

/**
 * `cancelAnimationFrame()`
 */

var cancel = global.cancelAnimationFrame
  || global.webkitCancelAnimationFrame
  || global.mozCancelAnimationFrame
  || clearTimeout

if (Function.prototype.bind) {
  request = request.bind(global)
  cancel = cancel.bind(global)
}

exports = module.exports = request
exports.cancel = cancel


/***/ }),

/***/ "./node_modules/react-scrollbar-size/ScrollbarSize.js":
/*!************************************************************!*\
  !*** ./node_modules/react-scrollbar-size/ScrollbarSize.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactEventListener = __webpack_require__(/*! react-event-listener */ "./node_modules/react-event-listener/lib/index.js");

var _reactEventListener2 = _interopRequireDefault(_reactEventListener);

var _stifle = __webpack_require__(/*! stifle */ "./node_modules/stifle/index.js");

var _stifle2 = _interopRequireDefault(_stifle);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
	width: '100px',
	height: '100px',
	position: 'absolute',
	top: '-100000px',
	overflow: 'scroll',
	msOverflowStyle: 'scrollbar'
};

var ScrollbarSize = function (_Component) {
	(0, _inherits3.default)(ScrollbarSize, _Component);

	function ScrollbarSize() {
		var _ref;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, ScrollbarSize);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = ScrollbarSize.__proto__ || (0, _getPrototypeOf2.default)(ScrollbarSize)).call.apply(_ref, [this].concat(args))), _this), _this.setMeasurements = function () {
			_this.scrollbarHeight = _this.node.offsetHeight - _this.node.clientHeight;
			_this.scrollbarWidth = _this.node.offsetWidth - _this.node.clientWidth;
		}, _this.handleResize = (0, _stifle2.default)(function () {
			var onChange = _this.props.onChange;


			var prevHeight = _this.scrollbarHeight;
			var prevWidth = _this.scrollbarWidth;
			_this.setMeasurements();
			if (prevHeight !== _this.scrollbarHeight || prevWidth !== _this.scrollbarWidth) {
				onChange({ scrollbarHeight: _this.scrollbarHeight, scrollbarWidth: _this.scrollbarWidth });
			}
		}, 166), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	(0, _createClass3.default)(ScrollbarSize, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			var onLoad = this.props.onLoad;


			if (onLoad) {
				this.setMeasurements();
				onLoad({ scrollbarHeight: this.scrollbarHeight, scrollbarWidth: this.scrollbarWidth });
			}
		}
	}, {
		key: 'componentWillUnmount',
		value: function componentWillUnmount() {
			this.handleResize.cancel();
		}
	}, {
		key: 'render',
		// Corresponds to 10 frames at 60 Hz.

		value: function render() {
			var _this2 = this;

			var onChange = this.props.onChange;


			return _react2.default.createElement(
				'div',
				null,
				onChange ? _react2.default.createElement(_reactEventListener2.default, { target: 'window', onResize: this.handleResize }) : null,
				_react2.default.createElement('div', {
					style: styles,
					ref: function ref(node) {
						_this2.node = node;
					}
				})
			);
		}
	}]);
	return ScrollbarSize;
}(_react.Component);

ScrollbarSize.defaultProps = {
	onLoad: null,
	onChange: null
};
exports.default = ScrollbarSize;

/***/ }),

/***/ "./node_modules/react-scrollbar-size/index.js":
/*!****************************************************!*\
  !*** ./node_modules/react-scrollbar-size/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ScrollbarSize = __webpack_require__(/*! ./ScrollbarSize */ "./node_modules/react-scrollbar-size/ScrollbarSize.js");

var _ScrollbarSize2 = _interopRequireDefault(_ScrollbarSize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _ScrollbarSize2.default;

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./node_modules/scroll/index.js":
/*!**************************************!*\
  !*** ./node_modules/scroll/index.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

var raf = __webpack_require__(/*! rafl */ "./node_modules/rafl/index.js")

function scroll (prop, element, to, options, callback) {
  var start = +new Date
  var from = element[prop]
  var cancelled = false

  var ease = inOutSine
  var duration = 350

  if (typeof options === 'function') {
    callback = options
  }
  else {
    options = options || {}
    ease = options.ease || ease
    duration = options.duration || duration
    callback = callback || function () {}
  }

  if (from === to) {
    return callback(
      new Error('Element already at target scroll position'),
      element[prop]
    )
  }

  function cancel () {
    cancelled = true
  }

  function animate (timestamp) {
    if (cancelled) {
      return callback(
        new Error('Scroll cancelled'),
        element[prop]
      )
    }

    var now = +new Date
    var time = Math.min(1, ((now - start) / duration))
    var eased = ease(time)

    element[prop] = (eased * (to - from)) + from

    time < 1 ? raf(animate) : raf(function () {
      callback(null, element[prop])
    })
  }

  raf(animate)

  return cancel
}

function inOutSine (n) {
  return .5 * (1 - Math.cos(Math.PI * n))
}

module.exports = {
  top: function (element, to, options, callback) {
    return scroll('scrollTop', element, to, options, callback)
  },
  left: function (element, to, options, callback) {
    return scroll('scrollLeft', element, to, options, callback)
  }
}


/***/ }),

/***/ "./node_modules/stifle/index.js":
/*!**************************************!*\
  !*** ./node_modules/stifle/index.js ***!
  \**************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports) {

module.exports = stifle;


function stifle (fn, wait) {
  if (typeof fn !== 'function' || typeof wait !== 'number') {
    throw new Error('stifle(fn, wait) -- expected a function and number of milliseconds, got (' + typeof fn + ', ' + typeof wait + ')');
  }

  var timer;    // Timer to fire after `wait` has elapsed
  var called;   // Keep track if it gets called during the `wait`

  var wrapper = function () {

    // Check if still "cooling down" from a previous call
    if (timer) {
      called = true;
    } else {
      // Start a timer to fire after the `wait` is over
      timer = setTimeout(afterWait, wait);
      // And call the wrapped function
      fn();
    }
  }

  // Add a cancel method, to kill and pending calls
  wrapper.cancel = function () {
    // Clear the called flag, or it would fire twice when called again later
    called = false;

    // Turn off the timer, so it won't fire after the wait expires
    if (timer) {
      clearTimeout(timer);
      timer = undefined;
    }
  }

  function afterWait() {
    // Empty out the timer
    timer = undefined;

    // If it was called during the `wait`, fire it again
    if (called) {
      called = false;
      wrapper();
    }
  }

  return wrapper;
}


/***/ }),

/***/ "./src/client/actions/courses/getAll.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/getAll.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAll = undefined;

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 * This action will update courses reducer
 * @function getAll
 * @param {Object} payload
 * @param {Object} payload.statusCode -
 * @param {Object[]} payload.courses
 * @param {Object} payload.courses[]
 * @param {number} payload.courses[].courseId
 * @param {number} payload.courses[].authorId
 * @param {string} payload.courses[].title
 * @param {string} payload.courses[].description
 * @param {Object} payload.courses[].syllabus
 * @param {Object[]} payload.courses[].courseModules
 * @param {Object} payload.courses[].courseModules[]
 * @param {number} payload.courses[].courseModules[].moduleId
 * @param {string} payload.courses[].timestamp
 *
 * @returns {Object}
 */
var getAll = exports.getAll = function getAll(payload) {
  return {
    type: _courses.COURSES_GET,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/actions/posts/getAll.js":
/*!********************************************!*\
  !*** ./src/client/actions/posts/getAll.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _posts = __webpack_require__(/*! ../../constants/posts */ "./src/client/constants/posts.js");

/**
 * This action will update 'posts' store
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id - element id of which all these posts belong
 * @param {Object[]} payload.posts
 * @param {Object} payload.posts[]
 * @param {number} payload.posts[].postId
 * @param {number} payload.posts[].authorId
 * @param {string} payload.posts[].title
 * @param {string} payload.posts[].description
 * @param {Object} payload.posts[].data
 * @param {string} payload.posts[].timestamp
 *
 * @returns {Object}
 */
var getAll = function getAll(payload) {
  return { type: _posts.POSTS_GET, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/actions/users/update.js":
/*!********************************************!*\
  !*** ./src/client/actions/users/update.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _update = __webpack_require__(/*! ../../constants/users/update */ "./src/client/constants/users/update.js");

var _update2 = _interopRequireDefault(_update);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * User update action.
 * User can update their info in redux store by it's 'id' only.
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @param {Object} payload.resume -
 *
 * @returns {Object}
 */
var update = function update(payload) {
  return { type: _update2.default, payload: payload };
}; /**
    * @format
    * 
    */

exports.default = update;

/***/ }),

/***/ "./src/client/api/courses/users/getAll.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/getAll.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * 
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/photos/users/cover/create.js":
/*!*****************************************************!*\
  !*** ./src/client/api/photos/users/cover/create.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *
 * @async
 * @function
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {Object} payload.formData
 * @returns {Promise}
 *
 * @example
 */
/**
 * @format
 * 
 */

exports.default = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId,
        formData = _ref2.formData;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/photos';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                // 'Content-Type': 'application/json',
                Authorization: token
              },
              body: formData
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  function create(_x) {
    return _ref.apply(this, arguments);
  }

  return create;
}();

/***/ }),

/***/ "./src/client/api/posts/users/getAll.js":
/*!**********************************************!*\
  !*** ./src/client/api/posts/users/getAll.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * This function fetch all posts of requested user
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * @file
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var userId = _ref2.userId,
        token = _ref2.token;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/posts';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/users/update.js":
/*!****************************************!*\
  !*** ./src/client/api/users/update.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _stringify = __webpack_require__(/*! babel-runtime/core-js/json/stringify */ "./node_modules/babel-runtime/core-js/json/stringify.js");

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.token -
 * @param {string} payload.name -
 * @param {Object} payload.avatar -
 * @param {Object} payload.cover -
 * @param {Object} payload.resume -
 * @return {Promise} -
 *
 * @example
 */
/** @format */

var update = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var id = _ref2.id,
        token = _ref2.token,
        name = _ref2.name,
        avatar = _ref2.avatar,
        cover = _ref2.cover,
        resume = _ref2.resume;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + id;
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'PUT',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              },
              body: (0, _stringify2.default)({ name: name, avatar: avatar, cover: cover, resume: resume })
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function update(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = update;

/***/ }),

/***/ "./src/client/constants/users/update.js":
/*!**********************************************!*\
  !*** ./src/client/constants/users/update.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains action constants for User's update
 *
 * @format
 */

var USER_UPDATE = 'USER_UPDATE';

exports.default = USER_UPDATE;

/***/ }),

/***/ "./src/client/containers/ElementInfo/AvatarComponent.js":
/*!**************************************************************!*\
  !*** ./src/client/containers/ElementInfo/AvatarComponent.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _update = __webpack_require__(/*! ../../api/users/update */ "./src/client/api/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _create = __webpack_require__(/*! ../../api/photos/users/cover/create */ "./src/client/api/photos/users/cover/create.js");

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @format
 */

var AvatarComponent = function (_Component) {
  (0, _inherits3.default)(AvatarComponent, _Component);

  function AvatarComponent(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, AvatarComponent);

    var _this = (0, _possibleConstructorReturn3.default)(this, (AvatarComponent.__proto__ || (0, _getPrototypeOf2.default)(AvatarComponent)).call(this, props));

    _this.onChange = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _this$props, userId, token, _ref2, statusCode, error, payload, avatar;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (e) {
                  e.preventDefault();
                }

                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 16;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _this$props = _this.props, userId = _this$props.userId, token = _this$props.token;
                _context.next = 8;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  formData: formData
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;


                if (statusCode >= 300) {
                  // handle Error
                  console.error(statusCode, error);
                }

                avatar = payload.photoUrl;


                (0, _update2.default)({ id: userId, token: token, avatar: avatar });

                _this.props.actionUserUpdate({ id: userId, avatar: avatar });

              case 16:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.state = {};

    _this.onClick = _this.onClick.bind(_this);
    // this.onChange = this.onChange.bind(this);
    return _this;
  }

  (0, _createClass3.default)(AvatarComponent, [{
    key: 'onClick',
    value: function onClick() {
      this.photo.value = null;
      this.photo.click();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      return _react2.default.createElement(
        'div',
        {
          style: (0, _extends3.default)({
            position: 'relative'
          }, this.props.style),
          className: this.props.className
        },
        _react2.default.createElement('img', {
          alt: this.props.children.props.alt,
          src: this.props.children.props.src,
          className: this.props.children.props.className
        }),
        _react2.default.createElement(
          _IconButton2.default,
          {
            'aria-label': 'Insert Photo',
            onClick: this.onClick,
            style: {
              position: 'absolute',
              right: '0px',
              top: '0px'
            }
          },
          _react2.default.createElement(_InsertPhoto2.default, null),
          _react2.default.createElement('input', {
            type: 'file',
            accept: 'image/jpeg|png|gif',
            onChange: this.onChange,
            ref: function ref(photo) {
              _this3.photo = photo;
            },
            style: { display: 'none' }
          })
        )
      );
    }
  }]);
  return AvatarComponent;
}(_react.Component);

AvatarComponent.propTypes = {
  style: _propTypes2.default.object,
  className: _propTypes2.default.string,
  actionUserUpdate: _propTypes2.default.func.isRequired
};


AvatarComponent.propTypes = {
  children: _propTypes2.default.shape({
    props: _propTypes2.default.shape({
      src: _propTypes2.default.string,
      alt: _propTypes2.default.string,
      className: _propTypes2.default.string
    })
  }).isRequired,
  style: _propTypes2.default.object
};

exports.default = AvatarComponent;

/***/ }),

/***/ "./src/client/containers/ElementInfo/CoverImage.js":
/*!*********************************************************!*\
  !*** ./src/client/containers/ElementInfo/CoverImage.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _update = __webpack_require__(/*! ../../api/users/update */ "./src/client/api/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _create = __webpack_require__(/*! ../../api/photos/users/cover/create */ "./src/client/api/photos/users/cover/create.js");

var _create2 = _interopRequireDefault(_create);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * @format
 */

var CoverImage = function (_Component) {
  (0, _inherits3.default)(CoverImage, _Component);

  function CoverImage(props) {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, CoverImage);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CoverImage.__proto__ || (0, _getPrototypeOf2.default)(CoverImage)).call(this, props));

    _this.onChange = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _this$props, userId, token, _ref2, statusCode, error, payload, cover;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (e) {
                  e.preventDefault();
                }

                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 16;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _this$props = _this.props, userId = _this$props.userId, token = _this$props.token;
                _context.next = 8;
                return (0, _create2.default)({
                  token: token,
                  userId: userId,
                  formData: formData
                });

              case 8:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;


                if (statusCode >= 300) {
                  // handle Error
                  console.error(statusCode, error);
                }

                cover = payload.photoUrl;


                (0, _update2.default)({ id: userId, token: token, cover: cover });

                _this.props.actionUserUpdate({ id: userId, cover: cover });

              case 16:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.state = {};

    _this.onClick = _this.onClick.bind(_this);
    // this.onChange = this.onChange.bind(this);
    return _this;
  }

  (0, _createClass3.default)(CoverImage, [{
    key: 'onClick',
    value: function onClick() {
      this.photo.value = null;
      this.photo.click();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      return _react2.default.createElement(
        'div',
        {
          style: (0, _extends3.default)({
            position: 'relative'
          }, this.props.style),
          className: this.props.className
        },
        _react2.default.createElement(
          _IconButton2.default,
          { 'aria-label': 'Insert Photo', onClick: this.onClick },
          _react2.default.createElement(_InsertPhoto2.default, null),
          _react2.default.createElement('input', {
            type: 'file',
            accept: 'image/jpeg|png|gif',
            onChange: this.onChange,
            ref: function ref(photo) {
              _this3.photo = photo;
            },
            style: { display: 'none' }
          })
        )
      );
    }
  }]);
  return CoverImage;
}(_react.Component);

CoverImage.propTypes = {
  style: _propTypes2.default.object,
  className: _propTypes2.default.string,
  actionUserUpdate: _propTypes2.default.func.isRequired
};
exports.default = CoverImage;

/***/ }),

/***/ "./src/client/containers/ElementInfo/index.js":
/*!****************************************************!*\
  !*** ./src/client/containers/ElementInfo/index.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _CoverImage = __webpack_require__(/*! ./CoverImage */ "./src/client/containers/ElementInfo/CoverImage.js");

var _CoverImage2 = _interopRequireDefault(_CoverImage);

var _AvatarComponent = __webpack_require__(/*! ./AvatarComponent */ "./src/client/containers/ElementInfo/AvatarComponent.js");

var _AvatarComponent2 = _interopRequireDefault(_AvatarComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = function styles(theme) {
  return {
    root: {
      flexGrow: 1
    },
    media: {
      height: 250
    },
    username: {},
    card: {},
    cardHeaderRoot: {
      minHeight: '100px',
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center'
    },
    cardHeaderAvatar: {},
    cardHeaderContent: {},
    avatar: {
      width: '8rem',
      position: 'relative',
      height: '8rem',
      border: '1px solid grey'
    },
    avatarIcon: {
      position: 'relative',
      top: '-40px',
      marginBottom: '-48px'
    }
  };
};

var ElementInfo = function (_Component) {
  (0, _inherits3.default)(ElementInfo, _Component);

  function ElementInfo(props) {
    (0, _classCallCheck3.default)(this, ElementInfo);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ElementInfo.__proto__ || (0, _getPrototypeOf2.default)(ElementInfo)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(ElementInfo, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          classes = _props.classes,
          user = _props.user,
          element = _props.element;
      var userId = user.id,
          token = user.token;
      var id = element.id,
          username = element.username,
          name = element.name,
          avatar = element.avatar,
          cover = element.cover;

      var urlAvatar = 'https://storage.googleapis.com/mayash-web/drive/avatar/user_m.svg';
      var urlCover = 'http://ginva.com/wp-content/uploads/2016/08/img_57a000bf9846c.png';

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(
          _Card2.default,
          { className: classes.card },
          _react2.default.createElement(_Card.CardHeader, {
            avatar: userId === id ? _react2.default.createElement(_Avatar2.default, {
              alt: name,
              src: avatar || urlAvatar,
              image: avatar || urlAvatar,
              component: function component(thisProps) {
                return _react2.default.createElement(_AvatarComponent2.default, (0, _extends3.default)({}, thisProps, {
                  token: token,
                  userId: userId,
                  actionUserUpdate: _this2.props.actionUserUpdate
                }));
              },
              className: classes.avatar
            }) : _react2.default.createElement(_Avatar2.default, {
              alt: name,
              src: avatar || urlAvatar,
              className: classes.avatar
            }),
            title: name,
            subheader: '@' + username,
            action: userId === id ? _react2.default.createElement(_CoverImage2.default, {
              token: token,
              userId: userId,
              actionUserUpdate: this.props.actionUserUpdate
            }) : null,
            classes: {
              root: classes.cardHeaderRoot,
              avatar: classes.cardHeaderAvatar,
              content: classes.cardHeaderContent
            },
            style: { backgroundImage: 'url(' + (cover || urlCover) + ')' }
          })
        )
      );
    }
  }]);
  return ElementInfo;
}(_react.Component);

ElementInfo.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  user: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired
  }),

  element: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    username: _propTypes2.default.string.isRequired,
    name: _propTypes2.default.string.isRequired,
    avatar: _propTypes2.default.string,
    cover: _propTypes2.default.string
  }),
  actionUserUpdate: _propTypes2.default.func.isRequired
};

exports.default = (0, _withStyles2.default)(styles)(ElementInfo);

/***/ }),

/***/ "./src/client/containers/ElementPage/User.js":
/*!***************************************************!*\
  !*** ./src/client/containers/ElementPage/User.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _AppBar = __webpack_require__(/*! material-ui/AppBar */ "./node_modules/material-ui/AppBar/index.js");

var _AppBar2 = _interopRequireDefault(_AppBar);

var _Tabs = __webpack_require__(/*! material-ui/Tabs */ "./node_modules/material-ui/Tabs/index.js");

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../api/courses/users/getAll */ "./src/client/api/courses/users/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../api/posts/users/getAll */ "./src/client/api/posts/users/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

var _update = __webpack_require__(/*! ../../actions/users/update */ "./src/client/actions/users/update.js");

var _update2 = _interopRequireDefault(_update);

var _getAll5 = __webpack_require__(/*! ../../actions/posts/getAll */ "./src/client/actions/posts/getAll.js");

var _getAll6 = _interopRequireDefault(_getAll5);

var _getAll7 = __webpack_require__(/*! ../../actions/courses/getAll */ "./src/client/actions/courses/getAll.js");

var _getAll8 = _interopRequireDefault(_getAll7);

var _index = __webpack_require__(/*! ../ElementInfo/index */ "./src/client/containers/ElementInfo/index.js");

var _index2 = _interopRequireDefault(_index);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AsyncFollow = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(25/* duplicate */).then(__webpack_require__.bind(null, /*! ../Follow */ "./src/client/containers/Follow/index.js"));
  },
  modules: ['../Follow'],
  loading: _Loading2.default
}); /** @format */

var AsyncTabResume = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(47).then(__webpack_require__.bind(null, /*! ./Tabs/TabResume */ "./src/client/containers/ElementPage/Tabs/TabResume.js"));
  },
  modules: ['./Tabs/TabResume'],
  loading: _Loading2.default
});

var AsyncTabPosts = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(41).then(__webpack_require__.bind(null, /*! ./Tabs/TabPosts */ "./src/client/containers/ElementPage/Tabs/TabPosts.js"));
  },
  modules: ['./Tabs/TabPosts'],
  loading: _Loading2.default
});

var AsyncTabCourses = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(42).then(__webpack_require__.bind(null, /*! ./Tabs/TabCourses */ "./src/client/containers/ElementPage/Tabs/TabCourses.js"));
  },
  modules: ['./Tabs/TabCourses'],
  loading: _Loading2.default
});

var styles = function styles(theme) {
  return {
    root: {
      flexGrow: 1,
      width: '100%'
    },
    media: {
      height: 250
    },
    follow: {
      margin: 'auto'
    },
    button: {
      borderRadius: '15px',
      fontWeight: 'bold'
    },
    flexGrow: {
      flex: '1 1 auto'
    }
  };
};

var User = function (_Component) {
  (0, _inherits3.default)(User, _Component);

  function User(props) {
    (0, _classCallCheck3.default)(this, User);

    var _this = (0, _possibleConstructorReturn3.default)(this, (User.__proto__ || (0, _getPrototypeOf2.default)(User)).call(this, props));

    _this.state = {
      value: 0,
      follow: false
    };

    _this.handleChange = _this.handleChange.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(User, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var element, id, token, courses, posts, _ref2, statusCode, error, payload, next, _ref3, _statusCode, _error, _payload, _next;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                element = this.props.element;
                id = element.id, token = element.token, courses = element.courses, posts = element.posts;

                if (!(typeof posts === 'undefined' || (0, _keys2.default)(posts).length === 0)) {
                  _context.next = 15;
                  break;
                }

                _context.next = 6;
                return (0, _getAll4.default)({
                  token: token,
                  userId: id
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;
                next = _ref2.next;

                if (!(statusCode >= 300)) {
                  _context.next = 14;
                  break;
                }

                // handle error
                console.error(error);
                return _context.abrupt('return');

              case 14:

                this.props.actionUserPostsGet({
                  id: id,
                  statusCode: statusCode,
                  posts: payload
                });

              case 15:
                if (!(typeof courses === 'undefined')) {
                  _context.next = 27;
                  break;
                }

                _context.next = 18;
                return (0, _getAll2.default)({
                  token: token,
                  userId: id
                });

              case 18:
                _ref3 = _context.sent;
                _statusCode = _ref3.statusCode;
                _error = _ref3.error;
                _payload = _ref3.payload;
                _next = _ref3.next;

                if (!(_statusCode >= 300)) {
                  _context.next = 26;
                  break;
                }

                // handle error
                console.error(_error);
                return _context.abrupt('return');

              case 26:

                this.props.actionUserCoursesGet({
                  id: id,
                  statusCode: _statusCode,
                  courses: _payload
                });

              case 27:
                _context.next = 32;
                break;

              case 29:
                _context.prev = 29;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 32:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 29]]);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: 'handleChange',
    value: function handleChange(event, value) {
      this.setState({ value: value });
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements,
          element = _props.element,
          posts = _props.posts,
          courses = _props.courses;
      var user = elements.user;
      var isSignedIn = user.isSignedIn,
          userId = user.id;
      var _element$guru = element.guru,
          guru = _element$guru === undefined ? false : _element$guru,
          id = element.id;
      var value = this.state.value;


      return _react2.default.createElement(
        'div',
        { className: classes.root },
        _react2.default.createElement(_index2.default, {
          user: user,
          element: element,
          actionUserUpdate: this.props.actionUserUpdate
        }),
        _react2.default.createElement(
          _AppBar2.default,
          { position: 'static', color: 'default' },
          guru ? _react2.default.createElement(
            _Tabs2.default,
            {
              value: value,
              onChange: this.handleChange,
              indicatorColor: 'primary',
              textColor: 'primary',
              scrollable: true,
              scrollButtons: 'auto'
            },
            _react2.default.createElement(_Tabs.Tab, { label: 'Resume' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Posts' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Courses' }),
            isSignedIn && userId !== id ? _react2.default.createElement(AsyncFollow, { element: element }) : null
          ) : _react2.default.createElement(
            _Tabs2.default,
            {
              value: value,
              onChange: this.handleChange,
              indicatorColor: 'primary',
              textColor: 'primary',
              scrollable: true,
              scrollButtons: 'auto'
            },
            _react2.default.createElement(_Tabs.Tab, { label: 'Resume' }),
            _react2.default.createElement(_Tabs.Tab, { label: 'Posts' }),
            isSignedIn && userId !== id ? _react2.default.createElement(AsyncFollow, { element: element }) : null
          )
        ),
        value === 0 && _react2.default.createElement(AsyncTabResume, {
          user: user,
          element: element,
          actionUserUpdate: this.props.actionUserUpdate
        }),
        value === 1 && _react2.default.createElement(AsyncTabPosts, { user: user, element: element, posts: posts }),
        guru && value === 2 && _react2.default.createElement(AsyncTabCourses, { user: user, element: element, courses: courses })
      );
    }
  }]);
  return User;
}(_react.Component);

User.propTypes = {
  classes: _propTypes2.default.object.isRequired,

  element: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,
  posts: _propTypes2.default.object.isRequired,
  courses: _propTypes2.default.object.isRequired,
  actionUserUpdate: _propTypes2.default.func,
  actionUserPostsGet: _propTypes2.default.func.isRequired,
  actionUserCoursesGet: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var posts = _ref4.posts,
      courses = _ref4.courses;
  return { posts: posts, courses: courses };
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionUserUpdate: _update2.default,
    actionUserPostsGet: _getAll6.default,
    actionUserCoursesGet: _getAll8.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(User));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0FwcEJhci9BcHBCYXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0FwcEJhci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL0F2YXRhci5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXZhdGFyL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9UYWJJbmRpY2F0b3IuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiU2Nyb2xsQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dMZWZ0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd1JpZ2h0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9ub3JtYWxpemUtc2Nyb2xsLWxlZnQvbGliL21haW4uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JhZmwvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXNjcm9sbGJhci1zaXplL1Njcm9sbGJhclNpemUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlYWN0LXNjcm9sbGJhci1zaXplL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvY3JlYXRlRWFnZXJGYWN0b3J5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3Njcm9sbC9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvc3RpZmxlL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy9jb3Vyc2VzL2dldEFsbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvZ2V0QWxsLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvYWN0aW9ucy91c2Vycy91cGRhdGUuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvY291cnNlcy91c2Vycy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvcGhvdG9zL3VzZXJzL2NvdmVyL2NyZWF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FwaS9wb3N0cy91c2Vycy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvdXNlcnMvdXBkYXRlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29uc3RhbnRzL3VzZXJzL3VwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRWxlbWVudEluZm8vQXZhdGFyQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9FbGVtZW50SW5mby9Db3ZlckltYWdlLmpzIiwid2VicGFjazovLy8uL3NyYy9jbGllbnQvY29udGFpbmVycy9FbGVtZW50SW5mby9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRWxlbWVudFBhZ2UvVXNlci5qcyJdLCJuYW1lcyI6WyJnZXRBbGwiLCJwYXlsb2FkIiwidHlwZSIsInVwZGF0ZSIsInRva2VuIiwidXNlcklkIiwidXJsIiwibWV0aG9kIiwiaGVhZGVycyIsIkFjY2VwdCIsIkF1dGhvcml6YXRpb24iLCJyZXMiLCJzdGF0dXMiLCJzdGF0dXNUZXh0Iiwic3RhdHVzQ29kZSIsImVycm9yIiwianNvbiIsImNvbnNvbGUiLCJmb3JtRGF0YSIsImJvZHkiLCJjcmVhdGUiLCJpZCIsIm5hbWUiLCJhdmF0YXIiLCJjb3ZlciIsInJlc3VtZSIsIlVTRVJfVVBEQVRFIiwiQXZhdGFyQ29tcG9uZW50IiwicHJvcHMiLCJvbkNoYW5nZSIsImUiLCJwcmV2ZW50RGVmYXVsdCIsImZpbGUiLCJ0YXJnZXQiLCJmaWxlcyIsImluZGV4T2YiLCJGb3JtRGF0YSIsImFwcGVuZCIsInBob3RvVXJsIiwiYWN0aW9uVXNlclVwZGF0ZSIsInN0YXRlIiwib25DbGljayIsImJpbmQiLCJwaG90byIsInZhbHVlIiwiY2xpY2siLCJwb3NpdGlvbiIsInN0eWxlIiwiY2xhc3NOYW1lIiwiY2hpbGRyZW4iLCJhbHQiLCJzcmMiLCJyaWdodCIsInRvcCIsImRpc3BsYXkiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJzdHJpbmciLCJmdW5jIiwiaXNSZXF1aXJlZCIsInNoYXBlIiwiQ292ZXJJbWFnZSIsInN0eWxlcyIsInRoZW1lIiwicm9vdCIsImZsZXhHcm93IiwibWVkaWEiLCJoZWlnaHQiLCJ1c2VybmFtZSIsImNhcmQiLCJjYXJkSGVhZGVyUm9vdCIsIm1pbkhlaWdodCIsImJhY2tncm91bmRTaXplIiwiYmFja2dyb3VuZFJlcGVhdCIsImJhY2tncm91bmRQb3NpdGlvbiIsImNhcmRIZWFkZXJBdmF0YXIiLCJjYXJkSGVhZGVyQ29udGVudCIsIndpZHRoIiwiYm9yZGVyIiwiYXZhdGFySWNvbiIsIm1hcmdpbkJvdHRvbSIsIkVsZW1lbnRJbmZvIiwiY2xhc3NlcyIsInVzZXIiLCJlbGVtZW50IiwidXJsQXZhdGFyIiwidXJsQ292ZXIiLCJ0aGlzUHJvcHMiLCJjb250ZW50IiwiYmFja2dyb3VuZEltYWdlIiwibnVtYmVyIiwiQXN5bmNGb2xsb3ciLCJsb2FkZXIiLCJtb2R1bGVzIiwibG9hZGluZyIsIkFzeW5jVGFiUmVzdW1lIiwiQXN5bmNUYWJQb3N0cyIsIkFzeW5jVGFiQ291cnNlcyIsImZvbGxvdyIsIm1hcmdpbiIsImJ1dHRvbiIsImJvcmRlclJhZGl1cyIsImZvbnRXZWlnaHQiLCJmbGV4IiwiVXNlciIsImhhbmRsZUNoYW5nZSIsImNvdXJzZXMiLCJwb3N0cyIsImxlbmd0aCIsIm5leHQiLCJhY3Rpb25Vc2VyUG9zdHNHZXQiLCJhY3Rpb25Vc2VyQ291cnNlc0dldCIsImV2ZW50Iiwic2V0U3RhdGUiLCJlbGVtZW50cyIsImlzU2lnbmVkSW4iLCJndXJ1IiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGtEQUFrRCxnSUFBZ0k7O0FBRWxMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsOEI7Ozs7Ozs7Ozs7Ozs7QUNqQ0E7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0Esb0pBQW9KOztBQUVwSjtBQUNBO0FBQ0EsZ0NBQWdDLHdFQUF3RTtBQUN4RztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELG9CQUFvQixVOzs7Ozs7Ozs7Ozs7O0FDNUt6RTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLDhGQUE4RjtBQUM5Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUUsZ0NBQWdDO0FBQ2pHLFNBQVM7QUFDVDtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0EsZ0NBQWdDLHVCQUF1QjtBQUN2RDtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxvQkFBb0IsVTs7Ozs7Ozs7Ozs7OztBQy9NekU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLGdIQUFnSDs7QUFFaEg7QUFDQTtBQUNBLGdDQUFnQyw4Q0FBOEM7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxrQkFBa0IsUTs7Ozs7Ozs7Ozs7OztBQzlJdkU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixtUEFBNEk7QUFDNUk7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSw4RUFBOEU7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFdBQVcsMkJBQTJCO0FBQ3RDO0FBQ0E7QUFDQSxhQUFhLDBCQUEwQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmOztBQUVBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCx3QkFBd0IsYzs7Ozs7Ozs7Ozs7OztBQ3JPN0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGOztBQUU5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHFCQUFxQixXOzs7Ozs7Ozs7Ozs7O0FDbEwxRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLG1DQUFtQztBQUNuQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLEtBQUs7QUFDTCxrREFBa0Q7QUFDbEQ7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQSxtRUFBbUUsYUFBYTtBQUNoRjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLDRCQUE0QjtBQUN0RDtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLG9DQUFvQztBQUMvQztBQUNBO0FBQ0E7QUFDQSxrR0FBa0c7QUFDbEc7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGdGQUFnRjs7QUFFaEY7O0FBRUE7QUFDQTtBQUNBOztBQUVBLCtFQUErRTs7QUFFL0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxXQUFXLDZCQUE2QjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxpQkFBaUIsTzs7Ozs7Ozs7Ozs7OztBQ3hXdEU7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxTQUFTOztBQUVUOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0EsMEZBQTBGOztBQUUxRixxRUFBcUU7QUFDckU7QUFDQSxHQUFHOztBQUVILCtDQUErQyxxQ0FBcUM7QUFDcEY7O0FBRUE7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0EsQ0FBQztBQUNELHFEQUFxRCwwQkFBMEIsZ0I7Ozs7Ozs7Ozs7Ozs7QUNoSC9FOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBOztBQUVBO0FBQ0EscURBQXFELHVCQUF1QjtBQUM1RTs7QUFFQTtBQUNBO0FBQ0EsZ0NBQWdDLHVEQUF1RDtBQUN2RjtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCw2QkFBNkIsbUI7Ozs7Ozs7Ozs7Ozs7QUNqSmxGOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGOztBQUVBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTCxnREFBZ0Q7QUFDaEQ7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxhQUFhLE9BQU87QUFDcEIsYUFBYSxPQUFPO0FBQ3BCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBOztBQUVBLG1FQUFtRSxhQUFhO0FBQ2hGO0FBQ0E7O0FBRUE7QUFDQSx3QkFBd0I7QUFDeEI7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPOztBQUVQOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkVBQTZFO0FBQzdFLE9BQU87O0FBRVA7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2RUFBNkU7QUFDN0UsT0FBTzs7QUFFUDtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYztBQUNkLEtBQUssaURBQWlEO0FBQ3REO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUCxLQUFLO0FBQ0w7QUFDQSxLQUFLO0FBQ0w7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBO0FBQ0EsMEJBQTBCLHFFQUFxRTtBQUMvRjtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLGdCQUFnQjtBQUNyQztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHVCQUF1QixpQ0FBaUM7QUFDeEQ7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBLHNHQUFzRztBQUN0Ryx1SEFBdUg7O0FBRXZIO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTzs7QUFFUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsT0FBTzs7QUFFUDs7QUFFQTtBQUNBO0FBQ0EsZ0NBQWdDLHVCQUF1QjtBQUN2RCxxRUFBcUUsZ0RBQWdEO0FBQ3JIO0FBQ0E7QUFDQTtBQUNBLFdBQVcsbUNBQW1DO0FBQzlDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsZUFBZSx1Q0FBdUM7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELG1DQUFtQyxROzs7Ozs7Ozs7Ozs7O0FDamlCeEY7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ3hCN0Y7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ1pBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix5Qzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUNyREE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRCwwREFBMEQ7O0FBRTVHO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ25DQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0Esa0RBQWtELHdEQUF3RDs7QUFFMUc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHFDOzs7Ozs7Ozs7Ozs7O0FDbkNBO0FBQ0EsOENBQThDLGNBQWM7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNuRkE7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUEsaUVBQWlFLGFBQWE7QUFDOUU7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsY0FBYywrRUFBK0U7QUFDN0Y7QUFDQSxHQUFHO0FBQ0g7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxZQUFZLDZFQUE2RTtBQUN6RjtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRTtBQUNGO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0EsNEVBQTRFLGdEQUFnRDtBQUM1SDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxFQUFFO0FBQ0Y7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0M7Ozs7Ozs7Ozs7Ozs7QUNqSUE7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLDBDOzs7Ozs7Ozs7Ozs7O0FDWkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7OztBQ3JCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDZkE7O0FBRUE7O0FBRUEsb0dBQW9HLG1CQUFtQixFQUFFLG1CQUFtQiw4SEFBOEg7O0FBRTFRO0FBQ0E7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsOEQ7Ozs7Ozs7Ozs7Ozs7QUNkQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0EsR0FBRzs7QUFFSDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2xDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDRCOzs7Ozs7Ozs7Ozs7O0FDWkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLHlDOzs7Ozs7Ozs7Ozs7O0FDVkE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGLGlEQUFpRCwwQ0FBMEMsMERBQTBELEVBQUU7O0FBRXZKLGlEQUFpRCxhQUFhLHVGQUF1RixFQUFFLHVGQUF1Rjs7QUFFOU8sMENBQTBDLCtEQUErRCxxR0FBcUcsRUFBRSx5RUFBeUUsZUFBZSx5RUFBeUUsRUFBRSxFQUFFLHVIQUF1SDs7QUFFNWU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxLQUFLOztBQUVMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ3pEQTs7QUFFQTs7QUFFQSxtREFBbUQsZ0JBQWdCLHNCQUFzQixPQUFPLDJCQUEyQiwwQkFBMEIseURBQXlELDJCQUEyQixFQUFFLEVBQUUsRUFBRSxlQUFlOztBQUU5UDs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLFVBQVUscUJBQXFCO0FBQzVEO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHlDOzs7Ozs7Ozs7Ozs7O0FDakNBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7O0FDbEVBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxZQUFZO0FBQ1osYUFBYTs7QUFFYjs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNDQTs7QUF3QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQk8sSUFBTUEsMEJBQVMsU0FBVEEsTUFBUyxDQUFDQyxPQUFEO0FBQUEsU0FBK0I7QUFDbkRDLDhCQURtRDtBQUVuREQ7QUFGbUQsR0FBL0I7QUFBQSxDQUFmLEMsQ0FoRFA7Ozs7O2tCQXFEZUQsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hEZjs7QUFzQkE7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkEsSUFBTUEsU0FBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQixFQUFFQyxzQkFBRixFQUFtQkQsZ0JBQW5CLEVBQS9CO0FBQUEsQ0FBZixDLENBM0NBOzs7OztrQkE2Q2VELE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4Q2Y7Ozs7OztBQWdCQTs7Ozs7Ozs7Ozs7Ozs7QUFjQSxJQUFNRyxTQUFTLFNBQVRBLE1BQVMsQ0FBQ0YsT0FBRDtBQUFBLFNBQStCLEVBQUVDLHNCQUFGLEVBQXFCRCxnQkFBckIsRUFBL0I7QUFBQSxDQUFmLEMsQ0FuQ0E7Ozs7O2tCQXFDZUUsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hCZjs7Ozs7Ozs7OztBQWJBOzs7Ozs7c0ZBdUJBO0FBQUEsUUFBd0JDLEtBQXhCLFNBQXdCQSxLQUF4QjtBQUFBLFFBQStCQyxNQUEvQixTQUErQkEsTUFBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFFVUMsZUFGVixrQ0FFcUNELE1BRnJDO0FBQUE7QUFBQSxtQkFJc0IsK0JBQU1DLEdBQU4sRUFBVztBQUMzQkMsc0JBQVEsS0FEbUI7QUFFM0JDLHVCQUFTO0FBQ1BDLHdCQUFRLGtCQUREO0FBRVAsZ0NBQWdCLGtCQUZUO0FBR1BDLCtCQUFlTjtBQUhSO0FBRmtCLGFBQVgsQ0FKdEI7O0FBQUE7QUFJVU8sZUFKVjtBQWFZQyxrQkFiWixHQWFtQ0QsR0FibkMsQ0FhWUMsTUFiWixFQWFvQkMsVUFicEIsR0FhbUNGLEdBYm5DLENBYW9CRSxVQWJwQjs7QUFBQSxrQkFlUUQsVUFBVSxHQWZsQjtBQUFBO0FBQUE7QUFBQTs7QUFBQSw2Q0FnQmE7QUFDTEUsMEJBQVlGLE1BRFA7QUFFTEcscUJBQU9GO0FBRkYsYUFoQmI7O0FBQUE7QUFBQTtBQUFBLG1CQXNCdUJGLElBQUlLLElBQUosRUF0QnZCOztBQUFBO0FBc0JVQSxnQkF0QlY7QUFBQSx3RUF3QmdCQSxJQXhCaEI7O0FBQUE7QUFBQTtBQUFBOztBQTBCSUMsb0JBQVFGLEtBQVI7O0FBMUJKLDZDQTRCVztBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUE1Qlg7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7a0JBQWVmLE07Ozs7O0FBbEJmOzs7O0FBQ0E7Ozs7a0JBb0RlQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckRmOzs7O0FBRUE7Ozs7QUFtQkE7Ozs7Ozs7Ozs7OztBQTFCQTs7Ozs7O3NGQXNDZTtBQUFBLFFBQ2JJLEtBRGEsU0FDYkEsS0FEYTtBQUFBLFFBRWJDLE1BRmEsU0FFYkEsTUFGYTtBQUFBLFFBR2JhLFFBSGEsU0FHYkEsUUFIYTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU1MWixlQU5LLGtDQU1zQkQsTUFOdEI7QUFBQTtBQUFBLG1CQVFPLCtCQUFNQyxHQUFOLEVBQVc7QUFDM0JDLHNCQUFRLE1BRG1CO0FBRTNCQyx1QkFBUztBQUNQQyx3QkFBUSxrQkFERDtBQUVQO0FBQ0FDLCtCQUFlTjtBQUhSLGVBRmtCO0FBTzNCZSxvQkFBTUQ7QUFQcUIsYUFBWCxDQVJQOztBQUFBO0FBUUxQLGVBUks7QUFrQkhDLGtCQWxCRyxHQWtCb0JELEdBbEJwQixDQWtCSEMsTUFsQkcsRUFrQktDLFVBbEJMLEdBa0JvQkYsR0FsQnBCLENBa0JLRSxVQWxCTDs7QUFBQSxrQkFvQlBELFVBQVUsR0FwQkg7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBcUJGO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBckJFOztBQUFBO0FBQUE7QUFBQSxtQkEyQlFGLElBQUlLLElBQUosRUEzQlI7O0FBQUE7QUEyQkxBLGdCQTNCSztBQUFBLHdFQTZCQ0EsSUE3QkQ7O0FBQUE7QUFBQTtBQUFBOztBQStCWEMsb0JBQVFGLEtBQVI7O0FBL0JXLDZDQWlDSjtBQUNMRCwwQkFBWSxHQURQO0FBRUxDLHFCQUFPO0FBRkYsYUFqQ0k7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsRzs7V0FBZUssTTs7OztTQUFBQSxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlCOUI7Ozs7Ozs7Ozs7O0FBUkE7Ozs7OztzRkFtQkE7QUFBQSxRQUF3QmYsTUFBeEIsU0FBd0JBLE1BQXhCO0FBQUEsUUFBZ0NELEtBQWhDLFNBQWdDQSxLQUFoQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVRSxlQUZWLGtDQUVxQ0QsTUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUMsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVOO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBYVlDLGtCQWJaLEdBYW1DRCxHQWJuQyxDQWFZQyxNQWJaLEVBYW9CQyxVQWJwQixHQWFtQ0YsR0FibkMsQ0Fhb0JFLFVBYnBCOztBQUFBLGtCQWVRRCxVQUFVLEdBZmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWhCYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0J1QkYsSUFBSUssSUFBSixFQXRCdkI7O0FBQUE7QUFzQlVBLGdCQXRCVjtBQUFBLHdFQXdCZ0JBLElBeEJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMEJJQyxvQkFBUUYsS0FBUjs7QUExQkosNkNBNEJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWYsTTs7Ozs7QUFkZjs7OztBQUNBOzs7O2tCQWdEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNqRGY7Ozs7Ozs7Ozs7Ozs7OztBQUxBOzs7c0ZBb0JBO0FBQUEsUUFBd0JxQixFQUF4QixTQUF3QkEsRUFBeEI7QUFBQSxRQUE0QmpCLEtBQTVCLFNBQTRCQSxLQUE1QjtBQUFBLFFBQW1Da0IsSUFBbkMsU0FBbUNBLElBQW5DO0FBQUEsUUFBeUNDLE1BQXpDLFNBQXlDQSxNQUF6QztBQUFBLFFBQWlEQyxLQUFqRCxTQUFpREEsS0FBakQ7QUFBQSxRQUF3REMsTUFBeEQsU0FBd0RBLE1BQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRVVuQixlQUZWLGtDQUVxQ2UsRUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTWYsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVOO0FBSFIsZUFGa0I7QUFPM0JlLG9CQUFNLHlCQUFlLEVBQUVHLFVBQUYsRUFBUUMsY0FBUixFQUFnQkMsWUFBaEIsRUFBdUJDLGNBQXZCLEVBQWY7QUFQcUIsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVZCxlQUpWO0FBY1lDLGtCQWRaLEdBY21DRCxHQWRuQyxDQWNZQyxNQWRaLEVBY29CQyxVQWRwQixHQWNtQ0YsR0FkbkMsQ0Fjb0JFLFVBZHBCOztBQUFBLGtCQWdCUUQsVUFBVSxHQWhCbEI7QUFBQTtBQUFBO0FBQUE7O0FBQUEsNkNBaUJhO0FBQ0xFLDBCQUFZRixNQURQO0FBRUxHLHFCQUFPRjtBQUZGLGFBakJiOztBQUFBO0FBQUE7QUFBQSxtQkF1QnVCRixJQUFJSyxJQUFKLEVBdkJ2Qjs7QUFBQTtBQXVCVUEsZ0JBdkJWO0FBQUEsd0VBeUJnQkEsSUF6QmhCOztBQUFBO0FBQUE7QUFBQTs7QUEyQklDLG9CQUFRRixLQUFSOztBQTNCSiw2Q0E2Qlc7QUFDTEQsMEJBQVksR0FEUDtBQUVMQyxxQkFBTztBQUZGLGFBN0JYOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEc7O2tCQUFlWixNOzs7OztBQWxCZjs7OztBQUNBOzs7O2tCQXFEZUEsTTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeERmOzs7Ozs7QUFNQSxJQUFNdUIsY0FBYyxhQUFwQjs7a0JBRWVBLFc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0pmOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBWEE7Ozs7SUFhTUMsZTs7O0FBT0osMkJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQTs7QUFBQSx3SkFDWEEsS0FEVzs7QUFBQSxVQWFuQkMsUUFibUI7QUFBQSwwRkFhUixpQkFBT0MsQ0FBUDtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ1Qsb0JBQUlBLENBQUosRUFBTztBQUNMQSxvQkFBRUMsY0FBRjtBQUNEOztBQUVLQyxvQkFMRyxHQUtJRixFQUFFRyxNQUFGLENBQVNDLEtBQVQsQ0FBZSxDQUFmLENBTEo7O0FBQUEsc0JBT0xGLEtBQUs5QixJQUFMLENBQVVpQyxPQUFWLENBQWtCLFFBQWxCLE1BQWdDLENBUDNCO0FBQUE7QUFBQTtBQUFBOztBQVFEakIsd0JBUkMsR0FRVSxJQUFJa0IsUUFBSixFQVJWOztBQVNQbEIseUJBQVNtQixNQUFULENBQWdCLE9BQWhCLEVBQXlCTCxJQUF6Qjs7QUFUTyw4QkFXbUIsTUFBS0osS0FYeEIsRUFXQ3ZCLE1BWEQsZUFXQ0EsTUFYRCxFQVdTRCxLQVhULGVBV1NBLEtBWFQ7QUFBQTtBQUFBLHVCQWFzQyxzQkFBZTtBQUMxREEsOEJBRDBEO0FBRTFEQyxnQ0FGMEQ7QUFHMURhO0FBSDBELGlCQUFmLENBYnRDOztBQUFBO0FBQUE7QUFhQ0osMEJBYkQsU0FhQ0EsVUFiRDtBQWFhQyxxQkFiYixTQWFhQSxLQWJiO0FBYW9CZCx1QkFicEIsU0Fhb0JBLE9BYnBCOzs7QUFtQlAsb0JBQUlhLGNBQWMsR0FBbEIsRUFBdUI7QUFDckI7QUFDQUcsMEJBQVFGLEtBQVIsQ0FBY0QsVUFBZCxFQUEwQkMsS0FBMUI7QUFDRDs7QUFFaUJRLHNCQXhCWCxHQXdCc0J0QixPQXhCdEIsQ0F3QkNxQyxRQXhCRDs7O0FBMEJQLHNDQUFjLEVBQUVqQixJQUFJaEIsTUFBTixFQUFjRCxZQUFkLEVBQXFCbUIsY0FBckIsRUFBZDs7QUFFQSxzQkFBS0ssS0FBTCxDQUFXVyxnQkFBWCxDQUE0QixFQUFFbEIsSUFBSWhCLE1BQU4sRUFBY2tCLGNBQWQsRUFBNUI7O0FBNUJPO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLE9BYlE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7O0FBRWpCLFVBQUtpQixLQUFMLEdBQWEsRUFBYjs7QUFFQSxVQUFLQyxPQUFMLEdBQWUsTUFBS0EsT0FBTCxDQUFhQyxJQUFiLE9BQWY7QUFDQTtBQUxpQjtBQU1sQjs7Ozs4QkFFUztBQUNSLFdBQUtDLEtBQUwsQ0FBV0MsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUtELEtBQUwsQ0FBV0UsS0FBWDtBQUNEOzs7NkJBa0NRO0FBQUE7O0FBQ1AsYUFDRTtBQUFBO0FBQUE7QUFDRTtBQUNFQyxzQkFBVTtBQURaLGFBRUssS0FBS2xCLEtBQUwsQ0FBV21CLEtBRmhCLENBREY7QUFLRSxxQkFBVyxLQUFLbkIsS0FBTCxDQUFXb0I7QUFMeEI7QUFPRTtBQUNFLGVBQUssS0FBS3BCLEtBQUwsQ0FBV3FCLFFBQVgsQ0FBb0JyQixLQUFwQixDQUEwQnNCLEdBRGpDO0FBRUUsZUFBSyxLQUFLdEIsS0FBTCxDQUFXcUIsUUFBWCxDQUFvQnJCLEtBQXBCLENBQTBCdUIsR0FGakM7QUFHRSxxQkFBVyxLQUFLdkIsS0FBTCxDQUFXcUIsUUFBWCxDQUFvQnJCLEtBQXBCLENBQTBCb0I7QUFIdkMsVUFQRjtBQVlFO0FBQUE7QUFBQTtBQUNFLDBCQUFXLGNBRGI7QUFFRSxxQkFBUyxLQUFLUCxPQUZoQjtBQUdFLG1CQUFPO0FBQ0xLLHdCQUFVLFVBREw7QUFFTE0scUJBQU8sS0FGRjtBQUdMQyxtQkFBSztBQUhBO0FBSFQ7QUFTRSxvRUFURjtBQVVFO0FBQ0Usa0JBQUssTUFEUDtBQUVFLG9CQUFPLG9CQUZUO0FBR0Usc0JBQVUsS0FBS3hCLFFBSGpCO0FBSUUsaUJBQUssYUFBQ2MsS0FBRCxFQUFXO0FBQ2QscUJBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNELGFBTkg7QUFPRSxtQkFBTyxFQUFFVyxTQUFTLE1BQVg7QUFQVDtBQVZGO0FBWkYsT0FERjtBQW1DRDs7Ozs7QUF4RkczQixlLENBQ0c0QixTLEdBQVk7QUFDakJSLFNBQU8sb0JBQVVTLE1BREE7QUFFakJSLGFBQVcsb0JBQVVTLE1BRko7QUFHakJsQixvQkFBa0Isb0JBQVVtQixJQUFWLENBQWVDO0FBSGhCLEM7OztBQTBGckJoQyxnQkFBZ0I0QixTQUFoQixHQUE0QjtBQUMxQk4sWUFBVSxvQkFBVVcsS0FBVixDQUFnQjtBQUN4QmhDLFdBQU8sb0JBQVVnQyxLQUFWLENBQWdCO0FBQ3JCVCxXQUFLLG9CQUFVTSxNQURNO0FBRXJCUCxXQUFLLG9CQUFVTyxNQUZNO0FBR3JCVCxpQkFBVyxvQkFBVVM7QUFIQSxLQUFoQjtBQURpQixHQUFoQixFQU1QRSxVQVB1QjtBQVExQlosU0FBTyxvQkFBVVM7QUFSUyxDQUE1Qjs7a0JBV2U3QixlOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMvR2Y7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7Ozs7QUFYQTs7OztJQWFNa0MsVTs7O0FBT0osc0JBQVlqQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUE7O0FBQUEsOElBQ1hBLEtBRFc7O0FBQUEsVUFhbkJDLFFBYm1CO0FBQUEsMEZBYVIsaUJBQU9DLENBQVA7QUFBQTs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNULG9CQUFJQSxDQUFKLEVBQU87QUFDTEEsb0JBQUVDLGNBQUY7QUFDRDs7QUFFS0Msb0JBTEcsR0FLSUYsRUFBRUcsTUFBRixDQUFTQyxLQUFULENBQWUsQ0FBZixDQUxKOztBQUFBLHNCQU9MRixLQUFLOUIsSUFBTCxDQUFVaUMsT0FBVixDQUFrQixRQUFsQixNQUFnQyxDQVAzQjtBQUFBO0FBQUE7QUFBQTs7QUFRRGpCLHdCQVJDLEdBUVUsSUFBSWtCLFFBQUosRUFSVjs7QUFTUGxCLHlCQUFTbUIsTUFBVCxDQUFnQixPQUFoQixFQUF5QkwsSUFBekI7O0FBVE8sOEJBV21CLE1BQUtKLEtBWHhCLEVBV0N2QixNQVhELGVBV0NBLE1BWEQsRUFXU0QsS0FYVCxlQVdTQSxLQVhUO0FBQUE7QUFBQSx1QkFhc0Msc0JBQWU7QUFDMURBLDhCQUQwRDtBQUUxREMsZ0NBRjBEO0FBRzFEYTtBQUgwRCxpQkFBZixDQWJ0Qzs7QUFBQTtBQUFBO0FBYUNKLDBCQWJELFNBYUNBLFVBYkQ7QUFhYUMscUJBYmIsU0FhYUEsS0FiYjtBQWFvQmQsdUJBYnBCLFNBYW9CQSxPQWJwQjs7O0FBbUJQLG9CQUFJYSxjQUFjLEdBQWxCLEVBQXVCO0FBQ3JCO0FBQ0FHLDBCQUFRRixLQUFSLENBQWNELFVBQWQsRUFBMEJDLEtBQTFCO0FBQ0Q7O0FBRWlCUyxxQkF4QlgsR0F3QnFCdkIsT0F4QnJCLENBd0JDcUMsUUF4QkQ7OztBQTBCUCxzQ0FBYyxFQUFFakIsSUFBSWhCLE1BQU4sRUFBY0QsWUFBZCxFQUFxQm9CLFlBQXJCLEVBQWQ7O0FBRUEsc0JBQUtJLEtBQUwsQ0FBV1csZ0JBQVgsQ0FBNEIsRUFBRWxCLElBQUloQixNQUFOLEVBQWNtQixZQUFkLEVBQTVCOztBQTVCTztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQWJROztBQUFBO0FBQUE7QUFBQTtBQUFBOztBQUVqQixVQUFLZ0IsS0FBTCxHQUFhLEVBQWI7O0FBRUEsVUFBS0MsT0FBTCxHQUFlLE1BQUtBLE9BQUwsQ0FBYUMsSUFBYixPQUFmO0FBQ0E7QUFMaUI7QUFNbEI7Ozs7OEJBRVM7QUFDUixXQUFLQyxLQUFMLENBQVdDLEtBQVgsR0FBbUIsSUFBbkI7QUFDQSxXQUFLRCxLQUFMLENBQVdFLEtBQVg7QUFDRDs7OzZCQWtDUTtBQUFBOztBQUNQLGFBQ0U7QUFBQTtBQUFBO0FBQ0U7QUFDRUMsc0JBQVU7QUFEWixhQUVLLEtBQUtsQixLQUFMLENBQVdtQixLQUZoQixDQURGO0FBS0UscUJBQVcsS0FBS25CLEtBQUwsQ0FBV29CO0FBTHhCO0FBT0U7QUFBQTtBQUFBLFlBQVksY0FBVyxjQUF2QixFQUFzQyxTQUFTLEtBQUtQLE9BQXBEO0FBQ0Usb0VBREY7QUFFRTtBQUNFLGtCQUFLLE1BRFA7QUFFRSxvQkFBTyxvQkFGVDtBQUdFLHNCQUFVLEtBQUtaLFFBSGpCO0FBSUUsaUJBQUssYUFBQ2MsS0FBRCxFQUFXO0FBQ2QscUJBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNELGFBTkg7QUFPRSxtQkFBTyxFQUFFVyxTQUFTLE1BQVg7QUFQVDtBQUZGO0FBUEYsT0FERjtBQXNCRDs7Ozs7QUEzRUdPLFUsQ0FDR04sUyxHQUFZO0FBQ2pCUixTQUFPLG9CQUFVUyxNQURBO0FBRWpCUixhQUFXLG9CQUFVUyxNQUZKO0FBR2pCbEIsb0JBQWtCLG9CQUFVbUIsSUFBVixDQUFlQztBQUhoQixDO2tCQTZFTkUsVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNGZjs7OztBQUNBOzs7O0FBRUE7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7Ozs7O0FBRUEsSUFBTUMsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pDLGdCQUFVO0FBRE4sS0FEbUI7QUFJekJDLFdBQU87QUFDTEMsY0FBUTtBQURILEtBSmtCO0FBT3pCQyxjQUFVLEVBUGU7QUFRekJDLFVBQU0sRUFSbUI7QUFTekJDLG9CQUFnQjtBQUNkQyxpQkFBVyxPQURHO0FBRWRDLHNCQUFnQixPQUZGO0FBR2RDLHdCQUFrQixXQUhKO0FBSWRDLDBCQUFvQjtBQUpOLEtBVFM7QUFlekJDLHNCQUFrQixFQWZPO0FBZ0J6QkMsdUJBQW1CLEVBaEJNO0FBaUJ6QnJELFlBQVE7QUFDTnNELGFBQU8sTUFERDtBQUVOL0IsZ0JBQVUsVUFGSjtBQUdOcUIsY0FBUSxNQUhGO0FBSU5XLGNBQVE7QUFKRixLQWpCaUI7QUF1QnpCQyxnQkFBWTtBQUNWakMsZ0JBQVUsVUFEQTtBQUVWTyxXQUFLLE9BRks7QUFHVjJCLG9CQUFjO0FBSEo7QUF2QmEsR0FBWjtBQUFBLENBQWY7O0lBOEJNQyxXOzs7QUFDSix1QkFBWXJELEtBQVosRUFBbUI7QUFBQTs7QUFBQSxnSkFDWEEsS0FEVzs7QUFFakIsVUFBS1ksS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQTs7QUFBQSxtQkFDNEIsS0FBS1osS0FEakM7QUFBQSxVQUNDc0QsT0FERCxVQUNDQSxPQUREO0FBQUEsVUFDVUMsSUFEVixVQUNVQSxJQURWO0FBQUEsVUFDZ0JDLE9BRGhCLFVBQ2dCQSxPQURoQjtBQUFBLFVBRUsvRSxNQUZMLEdBRXVCOEUsSUFGdkIsQ0FFQzlELEVBRkQ7QUFBQSxVQUVhakIsS0FGYixHQUV1QitFLElBRnZCLENBRWEvRSxLQUZiO0FBQUEsVUFHQ2lCLEVBSEQsR0FHdUMrRCxPQUh2QyxDQUdDL0QsRUFIRDtBQUFBLFVBR0srQyxRQUhMLEdBR3VDZ0IsT0FIdkMsQ0FHS2hCLFFBSEw7QUFBQSxVQUdlOUMsSUFIZixHQUd1QzhELE9BSHZDLENBR2U5RCxJQUhmO0FBQUEsVUFHcUJDLE1BSHJCLEdBR3VDNkQsT0FIdkMsQ0FHcUI3RCxNQUhyQjtBQUFBLFVBRzZCQyxLQUg3QixHQUd1QzRELE9BSHZDLENBRzZCNUQsS0FIN0I7O0FBSVAsVUFBTTZELFlBQ0osbUVBREY7QUFFQSxVQUFNQyxXQUNKLG1FQURGOztBQUdBLGFBQ0U7QUFBQTtBQUFBLFVBQUssV0FBV0osUUFBUWxCLElBQXhCO0FBQ0U7QUFBQTtBQUFBLFlBQU0sV0FBV2tCLFFBQVFiLElBQXpCO0FBQ0U7QUFDRSxvQkFDRWhFLFdBQVdnQixFQUFYLEdBQ0U7QUFDRSxtQkFBS0MsSUFEUDtBQUVFLG1CQUFLQyxVQUFVOEQsU0FGakI7QUFHRSxxQkFBTzlELFVBQVU4RCxTQUhuQjtBQUlFLHlCQUFXLG1CQUFDRSxTQUFEO0FBQUEsdUJBQ1Qsb0ZBQ01BLFNBRE47QUFFRSx5QkFBT25GLEtBRlQ7QUFHRSwwQkFBUUMsTUFIVjtBQUlFLG9DQUFrQixPQUFLdUIsS0FBTCxDQUFXVztBQUovQixtQkFEUztBQUFBLGVBSmI7QUFZRSx5QkFBVzJDLFFBQVEzRDtBQVpyQixjQURGLEdBZ0JFO0FBQ0UsbUJBQUtELElBRFA7QUFFRSxtQkFBS0MsVUFBVThELFNBRmpCO0FBR0UseUJBQVdILFFBQVEzRDtBQUhyQixjQWxCTjtBQXlCRSxtQkFBT0QsSUF6QlQ7QUEwQkUsNkJBQWU4QyxRQTFCakI7QUEyQkUsb0JBQ0UvRCxXQUFXZ0IsRUFBWCxHQUNFO0FBQ0UscUJBQU9qQixLQURUO0FBRUUsc0JBQVFDLE1BRlY7QUFHRSxnQ0FBa0IsS0FBS3VCLEtBQUwsQ0FBV1c7QUFIL0IsY0FERixHQU1JLElBbENSO0FBb0NFLHFCQUFTO0FBQ1B5QixvQkFBTWtCLFFBQVFaLGNBRFA7QUFFUC9DLHNCQUFRMkQsUUFBUVAsZ0JBRlQ7QUFHUGEsdUJBQVNOLFFBQVFOO0FBSFYsYUFwQ1g7QUF5Q0UsbUJBQU8sRUFBRWEsMkJBQXdCakUsU0FBUzhELFFBQWpDLE9BQUY7QUF6Q1Q7QUFERjtBQURGLE9BREY7QUFpREQ7Ozs7O0FBR0hMLFlBQVkxQixTQUFaLEdBQXdCO0FBQ3RCMkIsV0FBUyxvQkFBVTFCLE1BQVYsQ0FBaUJHLFVBREo7O0FBR3RCd0IsUUFBTSxvQkFBVXZCLEtBQVYsQ0FBZ0I7QUFDcEJ2QyxRQUFJLG9CQUFVcUUsTUFBVixDQUFpQi9CO0FBREQsR0FBaEIsQ0FIZ0I7O0FBT3RCeUIsV0FBUyxvQkFBVXhCLEtBQVYsQ0FBZ0I7QUFDdkJ2QyxRQUFJLG9CQUFVcUUsTUFBVixDQUFpQi9CLFVBREU7QUFFdkJTLGNBQVUsb0JBQVVYLE1BQVYsQ0FBaUJFLFVBRko7QUFHdkJyQyxVQUFNLG9CQUFVbUMsTUFBVixDQUFpQkUsVUFIQTtBQUl2QnBDLFlBQVEsb0JBQVVrQyxNQUpLO0FBS3ZCakMsV0FBTyxvQkFBVWlDO0FBTE0sR0FBaEIsQ0FQYTtBQWN0QmxCLG9CQUFrQixvQkFBVW1CLElBQVYsQ0FBZUM7QUFkWCxDQUF4Qjs7a0JBaUJlLDBCQUFXRyxNQUFYLEVBQW1CbUIsV0FBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0hmOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7Ozs7QUFFQSxJQUFNVSxjQUFjLDZCQUFTO0FBQzNCQyxVQUFRO0FBQUEsV0FBTSx1SkFBTjtBQUFBLEdBRG1CO0FBRTNCQyxXQUFTLENBQUMsV0FBRCxDQUZrQjtBQUczQkM7QUFIMkIsQ0FBVCxDQUFwQixDLENBeEJBOztBQThCQSxJQUFNQyxpQkFBaUIsNkJBQVM7QUFDOUJILFVBQVE7QUFBQSxXQUFNLDZKQUFOO0FBQUEsR0FEc0I7QUFFOUJDLFdBQVMsQ0FBQyxrQkFBRCxDQUZxQjtBQUc5QkM7QUFIOEIsQ0FBVCxDQUF2Qjs7QUFNQSxJQUFNRSxnQkFBZ0IsNkJBQVM7QUFDN0JKLFVBQVE7QUFBQSxXQUFNLDJKQUFOO0FBQUEsR0FEcUI7QUFFN0JDLFdBQVMsQ0FBQyxpQkFBRCxDQUZvQjtBQUc3QkM7QUFINkIsQ0FBVCxDQUF0Qjs7QUFNQSxJQUFNRyxrQkFBa0IsNkJBQVM7QUFDL0JMLFVBQVE7QUFBQSxXQUFNLCtKQUFOO0FBQUEsR0FEdUI7QUFFL0JDLFdBQVMsQ0FBQyxtQkFBRCxDQUZzQjtBQUcvQkM7QUFIK0IsQ0FBVCxDQUF4Qjs7QUFNQSxJQUFNaEMsU0FBUyxTQUFUQSxNQUFTLENBQUNDLEtBQUQ7QUFBQSxTQUFZO0FBQ3pCQyxVQUFNO0FBQ0pDLGdCQUFVLENBRE47QUFFSlksYUFBTztBQUZILEtBRG1CO0FBS3pCWCxXQUFPO0FBQ0xDLGNBQVE7QUFESCxLQUxrQjtBQVF6QitCLFlBQVE7QUFDTkMsY0FBUTtBQURGLEtBUmlCO0FBV3pCQyxZQUFRO0FBQ05DLG9CQUFjLE1BRFI7QUFFTkMsa0JBQVk7QUFGTixLQVhpQjtBQWV6QnJDLGNBQVU7QUFDUnNDLFlBQU07QUFERTtBQWZlLEdBQVo7QUFBQSxDQUFmOztJQW9CTUMsSTs7O0FBQ0osZ0JBQVk1RSxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsa0lBQ1hBLEtBRFc7O0FBRWpCLFVBQUtZLEtBQUwsR0FBYTtBQUNYSSxhQUFPLENBREk7QUFFWHNELGNBQVE7QUFGRyxLQUFiOztBQUtBLFVBQUtPLFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQi9ELElBQWxCLE9BQXBCO0FBUGlCO0FBUWxCOzs7Ozs7Ozs7Ozs7O0FBSVcwQyx1QixHQUFZLEtBQUt4RCxLLENBQWpCd0QsTztBQUNBL0Qsa0IsR0FBOEIrRCxPLENBQTlCL0QsRSxFQUFJakIsSyxHQUEwQmdGLE8sQ0FBMUJoRixLLEVBQU9zRyxPLEdBQW1CdEIsTyxDQUFuQnNCLE8sRUFBU0MsSyxHQUFVdkIsTyxDQUFWdUIsSzs7c0JBRXhCLE9BQU9BLEtBQVAsS0FBaUIsV0FBakIsSUFBZ0Msb0JBQVlBLEtBQVosRUFBbUJDLE1BQW5CLEtBQThCLEM7Ozs7Ozt1QkFDYixzQkFBZ0I7QUFDakV4Ryw4QkFEaUU7QUFFakVDLDBCQUFRZ0I7QUFGeUQsaUJBQWhCLEM7Ozs7QUFBM0NQLDBCLFNBQUFBLFU7QUFBWUMscUIsU0FBQUEsSztBQUFPZCx1QixTQUFBQSxPO0FBQVM0RyxvQixTQUFBQSxJOztzQkFLaEMvRixjQUFjLEc7Ozs7O0FBQ2hCO0FBQ0FHLHdCQUFRRixLQUFSLENBQWNBLEtBQWQ7Ozs7O0FBSUYscUJBQUthLEtBQUwsQ0FBV2tGLGtCQUFYLENBQThCO0FBQzVCekYsd0JBRDRCO0FBRTVCUCx3Q0FGNEI7QUFHNUI2Rix5QkFBTzFHO0FBSHFCLGlCQUE5Qjs7O3NCQU9FLE9BQU95RyxPQUFQLEtBQW1CLFc7Ozs7Ozt1QkFDOEIsc0JBQWtCO0FBQ25FdEcsOEJBRG1FO0FBRW5FQywwQkFBUWdCO0FBRjJELGlCQUFsQixDOzs7O0FBQTNDUCwyQixTQUFBQSxVO0FBQVlDLHNCLFNBQUFBLEs7QUFBT2Qsd0IsU0FBQUEsTztBQUFTNEcscUIsU0FBQUEsSTs7c0JBS2hDL0YsZUFBYyxHOzs7OztBQUNoQjtBQUNBRyx3QkFBUUYsS0FBUixDQUFjQSxNQUFkOzs7OztBQUlGLHFCQUFLYSxLQUFMLENBQVdtRixvQkFBWCxDQUFnQztBQUM5QjFGLHdCQUQ4QjtBQUU5QlAseUNBRjhCO0FBRzlCNEYsMkJBQVN6RztBQUhxQixpQkFBaEM7Ozs7Ozs7Ozs7QUFPRmdCLHdCQUFRRixLQUFSOzs7Ozs7Ozs7Ozs7Ozs7Ozs7aUNBSVNpRyxLLEVBQU9wRSxLLEVBQU87QUFDekIsV0FBS3FFLFFBQUwsQ0FBYyxFQUFFckUsWUFBRixFQUFkO0FBQ0Q7Ozs2QkFFUTtBQUFBLG1CQUNnRCxLQUFLaEIsS0FEckQ7QUFBQSxVQUNDc0QsT0FERCxVQUNDQSxPQUREO0FBQUEsVUFDVWdDLFFBRFYsVUFDVUEsUUFEVjtBQUFBLFVBQ29COUIsT0FEcEIsVUFDb0JBLE9BRHBCO0FBQUEsVUFDNkJ1QixLQUQ3QixVQUM2QkEsS0FEN0I7QUFBQSxVQUNvQ0QsT0FEcEMsVUFDb0NBLE9BRHBDO0FBQUEsVUFFQ3ZCLElBRkQsR0FFVStCLFFBRlYsQ0FFQy9CLElBRkQ7QUFBQSxVQUdDZ0MsVUFIRCxHQUc0QmhDLElBSDVCLENBR0NnQyxVQUhEO0FBQUEsVUFHaUI5RyxNQUhqQixHQUc0QjhFLElBSDVCLENBR2E5RCxFQUhiO0FBQUEsMEJBS3NCK0QsT0FMdEIsQ0FLQ2dDLElBTEQ7QUFBQSxVQUtDQSxJQUxELGlDQUtRLEtBTFI7QUFBQSxVQUtlL0YsRUFMZixHQUtzQitELE9BTHRCLENBS2UvRCxFQUxmO0FBQUEsVUFPQ3VCLEtBUEQsR0FPVyxLQUFLSixLQVBoQixDQU9DSSxLQVBEOzs7QUFTUCxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdzQyxRQUFRbEIsSUFBeEI7QUFDRTtBQUNFLGdCQUFNbUIsSUFEUjtBQUVFLG1CQUFTQyxPQUZYO0FBR0UsNEJBQWtCLEtBQUt4RCxLQUFMLENBQVdXO0FBSC9CLFVBREY7QUFPRTtBQUFBO0FBQUEsWUFBUSxVQUFTLFFBQWpCLEVBQTBCLE9BQU0sU0FBaEM7QUFDRzZFLGlCQUNDO0FBQUE7QUFBQTtBQUNFLHFCQUFPeEUsS0FEVDtBQUVFLHdCQUFVLEtBQUs2RCxZQUZqQjtBQUdFLDhCQUFlLFNBSGpCO0FBSUUseUJBQVUsU0FKWjtBQUtFLDhCQUxGO0FBTUUsNkJBQWM7QUFOaEI7QUFRRSx1REFBSyxPQUFNLFFBQVgsR0FSRjtBQVNFLHVEQUFLLE9BQU0sT0FBWCxHQVRGO0FBVUUsdURBQUssT0FBTSxTQUFYLEdBVkY7QUFZR1UsMEJBQWM5RyxXQUFXZ0IsRUFBekIsR0FDQyw4QkFBQyxXQUFELElBQWEsU0FBUytELE9BQXRCLEdBREQsR0FFRztBQWROLFdBREQsR0FrQkM7QUFBQTtBQUFBO0FBQ0UscUJBQU94QyxLQURUO0FBRUUsd0JBQVUsS0FBSzZELFlBRmpCO0FBR0UsOEJBQWUsU0FIakI7QUFJRSx5QkFBVSxTQUpaO0FBS0UsOEJBTEY7QUFNRSw2QkFBYztBQU5oQjtBQVFFLHVEQUFLLE9BQU0sUUFBWCxHQVJGO0FBU0UsdURBQUssT0FBTSxPQUFYLEdBVEY7QUFXR1UsMEJBQWM5RyxXQUFXZ0IsRUFBekIsR0FDQyw4QkFBQyxXQUFELElBQWEsU0FBUytELE9BQXRCLEdBREQsR0FFRztBQWJOO0FBbkJKLFNBUEY7QUEyQ0d4QyxrQkFBVSxDQUFWLElBQ0MsOEJBQUMsY0FBRDtBQUNFLGdCQUFNdUMsSUFEUjtBQUVFLG1CQUFTQyxPQUZYO0FBR0UsNEJBQWtCLEtBQUt4RCxLQUFMLENBQVdXO0FBSC9CLFVBNUNKO0FBa0RHSyxrQkFBVSxDQUFWLElBQ0MsOEJBQUMsYUFBRCxJQUFlLE1BQU11QyxJQUFyQixFQUEyQixTQUFTQyxPQUFwQyxFQUE2QyxPQUFPdUIsS0FBcEQsR0FuREo7QUFxREdTLGdCQUNDeEUsVUFBVSxDQURYLElBRUcsOEJBQUMsZUFBRCxJQUFpQixNQUFNdUMsSUFBdkIsRUFBNkIsU0FBU0MsT0FBdEMsRUFBK0MsU0FBU3NCLE9BQXhEO0FBdkROLE9BREY7QUE0REQ7Ozs7O0FBR0hGLEtBQUtqRCxTQUFMLEdBQWlCO0FBQ2YyQixXQUFTLG9CQUFVMUIsTUFBVixDQUFpQkcsVUFEWDs7QUFHZnlCLFdBQVMsb0JBQVU1QixNQUFWLENBQWlCRyxVQUhYO0FBSWZ1RCxZQUFVLG9CQUFVMUQsTUFBVixDQUFpQkcsVUFKWjtBQUtmZ0QsU0FBTyxvQkFBVW5ELE1BQVYsQ0FBaUJHLFVBTFQ7QUFNZitDLFdBQVMsb0JBQVVsRCxNQUFWLENBQWlCRyxVQU5YO0FBT2ZwQixvQkFBa0Isb0JBQVVtQixJQVBiO0FBUWZvRCxzQkFBb0Isb0JBQVVwRCxJQUFWLENBQWVDLFVBUnBCO0FBU2ZvRCx3QkFBc0Isb0JBQVVyRCxJQUFWLENBQWVDO0FBVHRCLENBQWpCOztBQVlBLElBQU0wRCxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR1YsS0FBSCxTQUFHQSxLQUFIO0FBQUEsTUFBVUQsT0FBVixTQUFVQSxPQUFWO0FBQUEsU0FBeUIsRUFBRUMsWUFBRixFQUFTRCxnQkFBVCxFQUF6QjtBQUFBLENBQXhCOztBQUVBLElBQU1ZLHFCQUFxQixTQUFyQkEsa0JBQXFCLENBQUNDLFFBQUQ7QUFBQSxTQUN6QiwrQkFDRTtBQUNFaEYsc0NBREY7QUFFRXVFLHdDQUZGO0FBR0VDO0FBSEYsR0FERixFQU1FUSxRQU5GLENBRHlCO0FBQUEsQ0FBM0I7O2tCQVVlLHlCQUFRRixlQUFSLEVBQXlCQyxrQkFBekIsRUFDYiwwQkFBV3hELE1BQVgsRUFBbUIwQyxJQUFuQixDQURhLEMiLCJmaWxlIjoiMzkuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjEgMTlWNWMwLTEuMS0uOS0yLTItMkg1Yy0xLjEgMC0yIC45LTIgMnYxNGMwIDEuMS45IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yek04LjUgMTMuNWwyLjUgMy4wMUwxNC41IDEybDQuNSA2SDVsMy41LTQuNXonIH0pO1xuXG52YXIgSW5zZXJ0UGhvdG8gPSBmdW5jdGlvbiBJbnNlcnRQaG90byhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0UGhvdG8gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydFBob3RvKTtcbkluc2VydFBob3RvLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydFBob3RvO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMjQgMzEgMzQgMzggMzkgNDQgNDUgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfUGFwZXIgPSByZXF1aXJlKCcuLi9QYXBlcicpO1xuXG52YXIgX1BhcGVyMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1BhcGVyKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgUGFwZXJcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgZmxleERpcmVjdGlvbjogJ2NvbHVtbicsXG4gICAgICB3aWR0aDogJzEwMCUnLFxuICAgICAgYm94U2l6aW5nOiAnYm9yZGVyLWJveCcsIC8vIFByZXZlbnQgcGFkZGluZyBpc3N1ZSB3aXRoIHRoZSBNb2RhbCBhbmQgZml4ZWQgcG9zaXRpb25lZCBBcHBCYXIuXG4gICAgICB6SW5kZXg6IHRoZW1lLnpJbmRleC5hcHBCYXIsXG4gICAgICBmbGV4U2hyaW5rOiAwXG4gICAgfSxcbiAgICBwb3NpdGlvbkZpeGVkOiB7XG4gICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6ICdhdXRvJyxcbiAgICAgIHJpZ2h0OiAwXG4gICAgfSxcbiAgICBwb3NpdGlvbkFic29sdXRlOiB7XG4gICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgICAgIHRvcDogMCxcbiAgICAgIGxlZnQ6ICdhdXRvJyxcbiAgICAgIHJpZ2h0OiAwXG4gICAgfSxcbiAgICBwb3NpdGlvblN0YXRpYzoge1xuICAgICAgcG9zaXRpb246ICdzdGF0aWMnLFxuICAgICAgZmxleFNocmluazogMFxuICAgIH0sXG4gICAgY29sb3JEZWZhdWx0OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5hcHBCYXIsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmFwcEJhcilcbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDAsXG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMClcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ3ByaW1hcnknLCAnYWNjZW50JywgJ2RlZmF1bHQnXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qb3NpdGlvbiA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3N0YXRpYycsICdmaXhlZCcsICdhYnNvbHV0ZSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIGNvbnRlbnQgb2YgdGhlIGNvbXBvbmVudC5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAncHJpbWFyeScsICdhY2NlbnQnLCAnZGVmYXVsdCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgcG9zaXRpb25pbmcgdHlwZS5cbiAgICovXG4gIHBvc2l0aW9uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydzdGF0aWMnLCAnZml4ZWQnLCAnYWJzb2x1dGUnXSkuaXNSZXF1aXJlZFxufTtcblxudmFyIEFwcEJhciA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEFwcEJhciwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gQXBwQmFyKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEFwcEJhcik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEFwcEJhci5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoQXBwQmFyKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShBcHBCYXIsIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgcG9zaXRpb24gPSBfcHJvcHMucG9zaXRpb24sXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAncG9zaXRpb24nXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc2VzWydwb3NpdGlvbicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShwb3NpdGlvbildLCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgJ211aS1maXhlZCcsIHBvc2l0aW9uID09PSAnZml4ZWQnKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfUGFwZXIyLmRlZmF1bHQsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBzcXVhcmU6IHRydWUsIGNvbXBvbmVudDogJ2hlYWRlcicsIGVsZXZhdGlvbjogNCwgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEFwcEJhcjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkFwcEJhci5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAncHJpbWFyeScsXG4gIHBvc2l0aW9uOiAnZml4ZWQnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUFwcEJhcicgfSkoQXBwQmFyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvQXBwQmFyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvQXBwQmFyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0FwcEJhciA9IHJlcXVpcmUoJy4vQXBwQmFyJyk7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnZGVmYXVsdCcsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0FwcEJhcikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvQXBwQmFyL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BcHBCYXIvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2NvbG9yTWFuaXB1bGF0b3IgPSByZXF1aXJlKCcuLi9zdHlsZXMvY29sb3JNYW5pcHVsYXRvcicpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxuICAgICAgYWxpZ25JdGVtczogJ2NlbnRlcicsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcicsXG4gICAgICBmbGV4U2hyaW5rOiAwLFxuICAgICAgd2lkdGg6IDQwLFxuICAgICAgaGVpZ2h0OiA0MCxcbiAgICAgIGZvbnRGYW1pbHk6IHRoZW1lLnR5cG9ncmFwaHkuZm9udEZhbWlseSxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjApLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcbiAgICAgIHVzZXJTZWxlY3Q6ICdub25lJ1xuICAgIH0sXG4gICAgY29sb3JEZWZhdWx0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5iYWNrZ3JvdW5kLmRlZmF1bHQsXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6ICgwLCBfY29sb3JNYW5pcHVsYXRvci5lbXBoYXNpemUpKHRoZW1lLnBhbGV0dGUuYmFja2dyb3VuZC5kZWZhdWx0LCAwLjI2KVxuICAgIH0sXG4gICAgaW1nOiB7XG4gICAgICBtYXhXaWR0aDogJzEwMCUnLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGhlaWdodDogJ2F1dG8nLFxuICAgICAgdGV4dEFsaWduOiAnY2VudGVyJ1xuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZWQgaW4gY29tYmluYXRpb24gd2l0aCBgc3JjYCBvciBgc3JjU2V0YCB0b1xuICAgKiBwcm92aWRlIGFuIGFsdCBhdHRyaWJ1dGUgZm9yIHRoZSByZW5kZXJlZCBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgYWx0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBVc2VkIHRvIHJlbmRlciBpY29uIG9yIHRleHQgZWxlbWVudHMgaW5zaWRlIHRoZSBBdmF0YXIuXG4gICAqIGBzcmNgIGFuZCBgYWx0YCBwcm9wcyB3aWxsIG5vdCBiZSB1c2VkIGFuZCBubyBgaW1nYCB3aWxsXG4gICAqIGJlIHJlbmRlcmVkIGJ5IGRlZmF1bHQuXG4gICAqXG4gICAqIFRoaXMgY2FuIGJlIGFuIGVsZW1lbnQsIG9yIGp1c3QgYSBzdHJpbmcuXG4gICAqL1xuICBjaGlsZHJlbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZywgdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpXSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICogVGhlIGNsYXNzTmFtZSBvZiB0aGUgY2hpbGQgZWxlbWVudC5cbiAgICogVXNlZCBieSBDaGlwIGFuZCBMaXN0SXRlbUljb24gdG8gc3R5bGUgdGhlIEF2YXRhciBpY29uLlxuICAgKi9cbiAgY2hpbGRyZW5DbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIGZvciB0aGUgcm9vdCBub2RlLlxuICAgKiBFaXRoZXIgYSBzdHJpbmcgdG8gdXNlIGEgRE9NIGVsZW1lbnQgb3IgYSBjb21wb25lbnQuXG4gICAqL1xuICBjb21wb25lbnQ6IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnRUeXBlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudFR5cGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFByb3BlcnRpZXMgYXBwbGllZCB0byB0aGUgYGltZ2AgZWxlbWVudCB3aGVuIHRoZSBjb21wb25lbnRcbiAgICogaXMgdXNlZCB0byBkaXNwbGF5IGFuIGltYWdlLlxuICAgKi9cbiAgaW1nUHJvcHM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBgc2l6ZXNgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzaXplczogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGBzcmNgIGF0dHJpYnV0ZSBmb3IgdGhlIGBpbWdgIGVsZW1lbnQuXG4gICAqL1xuICBzcmM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBgc3JjU2V0YCBhdHRyaWJ1dGUgZm9yIHRoZSBgaW1nYCBlbGVtZW50LlxuICAgKi9cbiAgc3JjU2V0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXG59O1xuXG52YXIgQXZhdGFyID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoQXZhdGFyLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBBdmF0YXIoKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgQXZhdGFyKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoQXZhdGFyLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShBdmF0YXIpKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEF2YXRhciwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBhbHQgPSBfcHJvcHMuYWx0LFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjaGlsZHJlblByb3AgPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2hpbGRyZW5DbGFzc05hbWVQcm9wID0gX3Byb3BzLmNoaWxkcmVuQ2xhc3NOYW1lLFxuICAgICAgICAgIENvbXBvbmVudFByb3AgPSBfcHJvcHMuY29tcG9uZW50LFxuICAgICAgICAgIGltZ1Byb3BzID0gX3Byb3BzLmltZ1Byb3BzLFxuICAgICAgICAgIHNpemVzID0gX3Byb3BzLnNpemVzLFxuICAgICAgICAgIHNyYyA9IF9wcm9wcy5zcmMsXG4gICAgICAgICAgc3JjU2V0ID0gX3Byb3BzLnNyY1NldCxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydhbHQnLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY2hpbGRyZW4nLCAnY2hpbGRyZW5DbGFzc05hbWUnLCAnY29tcG9uZW50JywgJ2ltZ1Byb3BzJywgJ3NpemVzJywgJ3NyYycsICdzcmNTZXQnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5jb2xvckRlZmF1bHQsIGNoaWxkcmVuUHJvcCAmJiAhc3JjICYmICFzcmNTZXQpLCBjbGFzc05hbWVQcm9wKTtcbiAgICAgIHZhciBjaGlsZHJlbiA9IG51bGw7XG5cbiAgICAgIGlmIChjaGlsZHJlblByb3ApIHtcbiAgICAgICAgaWYgKGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCAmJiB0eXBlb2YgY2hpbGRyZW5Qcm9wICE9PSAnc3RyaW5nJyAmJiBfcmVhY3QyLmRlZmF1bHQuaXNWYWxpZEVsZW1lbnQoY2hpbGRyZW5Qcm9wKSkge1xuICAgICAgICAgIHZhciBfY2hpbGRyZW5DbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNoaWxkcmVuQ2xhc3NOYW1lUHJvcCwgY2hpbGRyZW5Qcm9wLnByb3BzLmNsYXNzTmFtZSk7XG4gICAgICAgICAgY2hpbGRyZW4gPSBfcmVhY3QyLmRlZmF1bHQuY2xvbmVFbGVtZW50KGNoaWxkcmVuUHJvcCwgeyBjbGFzc05hbWU6IF9jaGlsZHJlbkNsYXNzTmFtZSB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBjaGlsZHJlbiA9IGNoaWxkcmVuUHJvcDtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmIChzcmMgfHwgc3JjU2V0KSB7XG4gICAgICAgIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ2ltZycsICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGFsdDogYWx0LFxuICAgICAgICAgIHNyYzogc3JjLFxuICAgICAgICAgIHNyY1NldDogc3JjU2V0LFxuICAgICAgICAgIHNpemVzOiBzaXplcyxcbiAgICAgICAgICBjbGFzc05hbWU6IGNsYXNzZXMuaW1nXG4gICAgICAgIH0sIGltZ1Byb3BzKSk7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgQ29tcG9uZW50UHJvcCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBBdmF0YXI7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5BdmF0YXIuZGVmYXVsdFByb3BzID0ge1xuICBjb21wb25lbnQ6ICdkaXYnXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUF2YXRhcicgfSkoQXZhdGFyKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvQXZhdGFyLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA0IDYgMjQgMjYgMjcgMjggMzEgMzMgMzQgMzUgMzYgMzcgMzkgNDAgNDMgNDkgNTQgNTcgNTggNTkgNjEgNjMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfQXZhdGFyID0gcmVxdWlyZSgnLi9BdmF0YXInKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQXZhdGFyKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9BdmF0YXIvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0F2YXRhci9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNCAyNCAyNiAyNyAyOCAzMSAzMyAzNCAzNSAzNiAzNyAzOSA0MCA0MyA0OSA1NyA1OCA1OSA2MSA2MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfaGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL2hlbHBlcnMnKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcblxudmFyIHN0eWxlcyA9IGV4cG9ydHMuc3R5bGVzID0gZnVuY3Rpb24gc3R5bGVzKHRoZW1lKSB7XG4gIHJldHVybiB7XG4gICAgcm9vdDoge1xuICAgICAgdXNlclNlbGVjdDogJ25vbmUnXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQWN0aW9uOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGNvbG9yRXJyb3I6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmVycm9yWzUwMF1cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbG9yID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFRoZSBuYW1lIG9mIHRoZSBpY29uIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKS5pc1JlcXVpcmVkXG59O1xuXG52YXIgSWNvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEljb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEljb24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgSWNvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEljb24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEljb24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEljb24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJ10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKCdtYXRlcmlhbC1pY29ucycsIGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnc3BhbicsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoeyBjbGFzc05hbWU6IGNsYXNzTmFtZSwgJ2FyaWEtaGlkZGVuJzogJ3RydWUnIH0sIG90aGVyKSxcbiAgICAgICAgY2hpbGRyZW5cbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBJY29uO1xufShfcmVhY3QyLmRlZmF1bHQuQ29tcG9uZW50KTtcblxuSWNvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIGNvbG9yOiAnaW5oZXJpdCdcbn07XG5JY29uLm11aU5hbWUgPSAnSWNvbic7XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpSWNvbicgfSkoSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9JY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA2IDcgOCA5IDM0IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuL0ljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDYgNyA4IDkgMzQgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuLi9JY29uJyk7XG5cbnZhciBfSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKTtcblxucmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX3JlYWN0SGVscGVycyA9IHJlcXVpcmUoJy4uL3V0aWxzL3JlYWN0SGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55OyAvLyAgd2Vha1xuLy8gQGluaGVyaXRlZENvbXBvbmVudCBCdXR0b25CYXNlXG5cbi8vIEVuc3VyZSBDU1Mgc3BlY2lmaWNpdHlcblxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB0ZXh0QWxpZ246ICdjZW50ZXInLFxuICAgICAgZmxleDogJzAgMCBhdXRvJyxcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0oMjQpLFxuICAgICAgd2lkdGg6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBoZWlnaHQ6IHRoZW1lLnNwYWNpbmcudW5pdCAqIDYsXG4gICAgICBwYWRkaW5nOiAwLFxuICAgICAgYm9yZGVyUmFkaXVzOiAnNTAlJyxcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmUsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2JhY2tncm91bmQtY29sb3InLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVzdFxuICAgICAgfSlcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9LFxuICAgIGNvbG9ySW5oZXJpdDoge1xuICAgICAgY29sb3I6ICdpbmhlcml0J1xuICAgIH0sXG4gICAgZGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgbGFiZWw6IHtcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICBkaXNwbGF5OiAnZmxleCcsXG4gICAgICBhbGlnbkl0ZW1zOiAnaW5oZXJpdCcsXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBpY29uOiB7XG4gICAgICB3aWR0aDogJzFlbScsXG4gICAgICBoZWlnaHQ6ICcxZW0nXG4gICAgfSxcbiAgICBrZXlib2FyZEZvY3VzZWQ6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpdmlkZXJcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSBuYXRpdmUgYnV0dG9uIGNvbXBvbmVudC5cbiAgICovXG4gIGJ1dHRvblJlZjogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIFRoZSBpY29uIGVsZW1lbnQuXG4gICAqIElmIGEgc3RyaW5nIGlzIHByb3ZpZGVkLCBpdCB3aWxsIGJlIHVzZWQgYXMgYW4gaWNvbiBmb250IGxpZ2F0dXJlLlxuICAgKi9cbiAgY2hpbGRyZW46IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlKSxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnZGVmYXVsdCcsICdpbmhlcml0JywgJ3ByaW1hcnknLCAnY29udHJhc3QnLCAnYWNjZW50J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGJ1dHRvbiB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHJpcHBsZSB3aWxsIGJlIGRpc2FibGVkLlxuICAgKi9cbiAgZGlzYWJsZVJpcHBsZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlIHRoYXQgcHJvcGVydHkgdG8gcGFzcyBhIHJlZiBjYWxsYmFjayB0byB0aGUgcm9vdCBjb21wb25lbnQuXG4gICAqL1xuICByb290UmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuY1xufTtcblxuLyoqXG4gKiBSZWZlciB0byB0aGUgW0ljb25zXSgvc3R5bGUvaWNvbnMpIHNlY3Rpb24gb2YgdGhlIGRvY3VtZW50YXRpb25cbiAqIHJlZ2FyZGluZyB0aGUgYXZhaWxhYmxlIGljb24gb3B0aW9ucy5cbiAqL1xudmFyIEljb25CdXR0b24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBJY29uQnV0dG9uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIEljb25CdXR0b24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChJY29uQnV0dG9uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShJY29uQnV0dG9uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShJY29uQnV0dG9uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYnV0dG9uUmVmID0gX3Byb3BzLmJ1dHRvblJlZixcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICBkaXNhYmxlZCA9IF9wcm9wcy5kaXNhYmxlZCxcbiAgICAgICAgICByb290UmVmID0gX3Byb3BzLnJvb3RSZWYsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uUmVmJywgJ2NoaWxkcmVuJywgJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2NvbG9yJywgJ2Rpc2FibGVkJywgJ3Jvb3RSZWYnXSk7XG5cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzID0ge30sICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzLCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2RlZmF1bHQnKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXMuZGlzYWJsZWQsIGRpc2FibGVkKSwgX2NsYXNzTmFtZXMpLCBjbGFzc05hbWUpLFxuICAgICAgICAgIGNlbnRlclJpcHBsZTogdHJ1ZSxcbiAgICAgICAgICBrZXlib2FyZEZvY3VzZWRDbGFzc05hbWU6IGNsYXNzZXMua2V5Ym9hcmRGb2N1c2VkLFxuICAgICAgICAgIGRpc2FibGVkOiBkaXNhYmxlZFxuICAgICAgICB9LCBvdGhlciwge1xuICAgICAgICAgIHJvb3RSZWY6IGJ1dHRvblJlZixcbiAgICAgICAgICByZWY6IHJvb3RSZWZcbiAgICAgICAgfSksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5sYWJlbCB9LFxuICAgICAgICAgIHR5cGVvZiBjaGlsZHJlbiA9PT0gJ3N0cmluZycgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAgIF9JY29uMi5kZWZhdWx0LFxuICAgICAgICAgICAgeyBjbGFzc05hbWU6IGNsYXNzZXMuaWNvbiB9LFxuICAgICAgICAgICAgY2hpbGRyZW5cbiAgICAgICAgICApIDogX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlbiwgZnVuY3Rpb24gKGNoaWxkKSB7XG4gICAgICAgICAgICBpZiAoKDAsIF9yZWFjdEhlbHBlcnMuaXNNdWlFbGVtZW50KShjaGlsZCwgWydJY29uJywgJ1N2Z0ljb24nXSkpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5pY29uLCBjaGlsZC5wcm9wcy5jbGFzc05hbWUpXG4gICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICByZXR1cm4gY2hpbGQ7XG4gICAgICAgICAgfSlcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb25CdXR0b247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uQnV0dG9uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdkZWZhdWx0JyxcbiAgZGlzYWJsZWQ6IGZhbHNlLFxuICBkaXNhYmxlUmlwcGxlOiBmYWxzZVxufTtcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uQnV0dG9uJyB9KShJY29uQnV0dG9uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL0ljb25CdXR0b24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX0ljb25CdXR0b24gPSByZXF1aXJlKCcuL0ljb25CdXR0b24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfSWNvbkJ1dHRvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDEgMyA2IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM3IDM4IDM5IDQwIDQ0IDQ1IDQ2IDQ5IDU1IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBkaXNwbGF5OiAnaW5saW5lLWJsb2NrJyxcbiAgICAgIGZpbGw6ICdjdXJyZW50Q29sb3InLFxuICAgICAgaGVpZ2h0OiAyNCxcbiAgICAgIHdpZHRoOiAyNCxcbiAgICAgIHVzZXJTZWxlY3Q6ICdub25lJyxcbiAgICAgIGZsZXhTaHJpbms6IDAsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoJ2ZpbGwnLCB7XG4gICAgICAgIGR1cmF0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5kdXJhdGlvbi5zaG9ydGVyXG4gICAgICB9KVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckFjdGlvbjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZVxuICAgIH0sXG4gICAgY29sb3JDb250cmFzdDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZ2V0Q29udHJhc3RUZXh0KHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdKVxuICAgIH0sXG4gICAgY29sb3JEaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBjb2xvckVycm9yOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5lcnJvcls1MDBdXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db2xvciA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBFbGVtZW50cyBwYXNzZWQgaW50byB0aGUgU1ZHIEljb24uXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlLmlzUmVxdWlyZWQgOiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFByb3ZpZGVzIGEgaHVtYW4tcmVhZGFibGUgdGl0bGUgZm9yIHRoZSBlbGVtZW50IHRoYXQgY29udGFpbnMgaXQuXG4gICAqIGh0dHBzOi8vd3d3LnczLm9yZy9UUi9TVkctYWNjZXNzLyNFcXVpdmFsZW50XG4gICAqL1xuICB0aXRsZUFjY2VzczogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogQWxsb3dzIHlvdSB0byByZWRlZmluZSB3aGF0IHRoZSBjb29yZGluYXRlcyB3aXRob3V0IHVuaXRzIG1lYW4gaW5zaWRlIGFuIHN2ZyBlbGVtZW50LlxuICAgKiBGb3IgZXhhbXBsZSwgaWYgdGhlIFNWRyBlbGVtZW50IGlzIDUwMCAod2lkdGgpIGJ5IDIwMCAoaGVpZ2h0KSxcbiAgICogYW5kIHlvdSBwYXNzIHZpZXdCb3g9XCIwIDAgNTAgMjBcIixcbiAgICogdGhpcyBtZWFucyB0aGF0IHRoZSBjb29yZGluYXRlcyBpbnNpZGUgdGhlIHN2ZyB3aWxsIGdvIGZyb20gdGhlIHRvcCBsZWZ0IGNvcm5lciAoMCwwKVxuICAgKiB0byBib3R0b20gcmlnaHQgKDUwLDIwKSBhbmQgZWFjaCB1bml0IHdpbGwgYmUgd29ydGggMTBweC5cbiAgICovXG4gIHZpZXdCb3g6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcuaXNSZXF1aXJlZFxufTtcblxudmFyIFN2Z0ljb24gPSBmdW5jdGlvbiAoX1JlYWN0JENvbXBvbmVudCkge1xuICAoMCwgX2luaGVyaXRzMy5kZWZhdWx0KShTdmdJY29uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBTdmdJY29uKCkge1xuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFN2Z0ljb24pO1xuICAgIHJldHVybiAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKHRoaXMsIChTdmdJY29uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShTdmdJY29uKSkuYXBwbHkodGhpcywgYXJndW1lbnRzKSk7XG4gIH1cblxuICAoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShTdmdJY29uLCBbe1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfcHJvcHMgPSB0aGlzLnByb3BzLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWVQcm9wID0gX3Byb3BzLmNsYXNzTmFtZSxcbiAgICAgICAgICBjb2xvciA9IF9wcm9wcy5jb2xvcixcbiAgICAgICAgICB0aXRsZUFjY2VzcyA9IF9wcm9wcy50aXRsZUFjY2VzcyxcbiAgICAgICAgICB2aWV3Qm94ID0gX3Byb3BzLnZpZXdCb3gsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAndGl0bGVBY2Nlc3MnLCAndmlld0JveCddKTtcblxuXG4gICAgICB2YXIgY2xhc3NOYW1lID0gKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLnJvb3QsICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCBjbGFzc2VzWydjb2xvcicgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKShjb2xvcildLCBjb2xvciAhPT0gJ2luaGVyaXQnKSwgY2xhc3NOYW1lUHJvcCk7XG5cbiAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgJ3N2ZycsXG4gICAgICAgICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe1xuICAgICAgICAgIGNsYXNzTmFtZTogY2xhc3NOYW1lLFxuICAgICAgICAgIGZvY3VzYWJsZTogJ2ZhbHNlJyxcbiAgICAgICAgICB2aWV3Qm94OiB2aWV3Qm94LFxuICAgICAgICAgICdhcmlhLWhpZGRlbic6IHRpdGxlQWNjZXNzID8gJ2ZhbHNlJyA6ICd0cnVlJ1xuICAgICAgICB9LCBvdGhlciksXG4gICAgICAgIHRpdGxlQWNjZXNzID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3RpdGxlJyxcbiAgICAgICAgICBudWxsLFxuICAgICAgICAgIHRpdGxlQWNjZXNzXG4gICAgICAgICkgOiBudWxsLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFN2Z0ljb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5TdmdJY29uLmRlZmF1bHRQcm9wcyA9IHtcbiAgdmlld0JveDogJzAgMCAyNCAyNCcsXG4gIGNvbG9yOiAnaW5oZXJpdCdcbn07XG5TdmdJY29uLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpU3ZnSWNvbicgfSkoU3ZnSWNvbik7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9TdmdJY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMiAzIDQgNSA2IDcgOCA5IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDM0IDM2IDM5IDQzIDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCcuL1N2Z0ljb24nKTtcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdkZWZhdWx0Jywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uIGdldCgpIHtcbiAgICByZXR1cm4gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvU3ZnSWNvbi9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMzQgMzYgMzkgNDMgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9rZXlzID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9rZXlzJyk7XG5cbnZhciBfa2V5czIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9rZXlzKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX2V4dGVuZHMzID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzNCA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMzKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX0J1dHRvbkJhc2UgPSByZXF1aXJlKCcuLi9CdXR0b25CYXNlJyk7XG5cbnZhciBfQnV0dG9uQmFzZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9CdXR0b25CYXNlKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG52YXIgX0ljb24gPSByZXF1aXJlKCcuLi9JY29uJyk7XG5cbnZhciBfSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPSByZXF1aXJlKCdyZWFjdCcpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgfHwgcmVxdWlyZSgncHJvcC10eXBlcycpLmFueTtcbi8vIEBpbmhlcml0ZWRDb21wb25lbnQgQnV0dG9uQmFzZVxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiAoMCwgX2V4dGVuZHM0LmRlZmF1bHQpKHt9LCB0aGVtZS50eXBvZ3JhcGh5LmJ1dHRvbiwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe1xuICAgICAgbWF4V2lkdGg6IDI2NCxcbiAgICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgICAgbWluV2lkdGg6IDcyLFxuICAgICAgcGFkZGluZzogMCxcbiAgICAgIGhlaWdodDogNDgsXG4gICAgICBmbGV4OiAnbm9uZScsXG4gICAgICBvdmVyZmxvdzogJ2hpZGRlbidcbiAgICB9LCB0aGVtZS5icmVha3BvaW50cy51cCgnbWQnKSwge1xuICAgICAgbWluV2lkdGg6IDE2MFxuICAgIH0pKSxcbiAgICByb290TGFiZWxJY29uOiB7XG4gICAgICBoZWlnaHQ6IDcyXG4gICAgfSxcbiAgICByb290QWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LnNlY29uZGFyeVxuICAgIH0sXG4gICAgcm9vdEFjY2VudFNlbGVjdGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgcm9vdEFjY2VudERpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS50ZXh0LmRpc2FibGVkXG4gICAgfSxcbiAgICByb290UHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5zZWNvbmRhcnlcbiAgICB9LFxuICAgIHJvb3RQcmltYXJ5U2VsZWN0ZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH0sXG4gICAgcm9vdFByaW1hcnlEaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUudGV4dC5kaXNhYmxlZFxuICAgIH0sXG4gICAgcm9vdEluaGVyaXQ6IHtcbiAgICAgIGNvbG9yOiAnaW5oZXJpdCcsXG4gICAgICBvcGFjaXR5OiAwLjdcbiAgICB9LFxuICAgIHJvb3RJbmhlcml0U2VsZWN0ZWQ6IHtcbiAgICAgIG9wYWNpdHk6IDFcbiAgICB9LFxuICAgIHJvb3RJbmhlcml0RGlzYWJsZWQ6IHtcbiAgICAgIG9wYWNpdHk6IDAuNFxuICAgIH0sXG4gICAgZnVsbFdpZHRoOiB7XG4gICAgICBmbGV4R3JvdzogMVxuICAgIH0sXG4gICAgd3JhcHBlcjoge1xuICAgICAgZGlzcGxheTogJ2lubGluZS1mbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdjZW50ZXInLFxuICAgICAganVzdGlmeUNvbnRlbnQ6ICdjZW50ZXInLFxuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGZsZXhEaXJlY3Rpb246ICdjb2x1bW4nXG4gICAgfSxcbiAgICBsYWJlbENvbnRhaW5lcjogKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe1xuICAgICAgcGFkZGluZ1RvcDogNixcbiAgICAgIHBhZGRpbmdCb3R0b206IDYsXG4gICAgICBwYWRkaW5nTGVmdDogMTIsXG4gICAgICBwYWRkaW5nUmlnaHQ6IDEyXG4gICAgfSwgdGhlbWUuYnJlYWtwb2ludHMudXAoJ21kJyksIHtcbiAgICAgIHBhZGRpbmdMZWZ0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzLFxuICAgICAgcGFkZGluZ1JpZ2h0OiB0aGVtZS5zcGFjaW5nLnVuaXQgKiAzXG4gICAgfSksXG4gICAgbGFiZWw6ICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHtcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0odGhlbWUudHlwb2dyYXBoeS5mb250U2l6ZSksXG4gICAgICB3aGl0ZVNwYWNlOiAnbm9ybWFsJ1xuICAgIH0sIHRoZW1lLmJyZWFrcG9pbnRzLnVwKCdtZCcpLCB7XG4gICAgICBmb250U2l6ZTogdGhlbWUudHlwb2dyYXBoeS5weFRvUmVtKHRoZW1lLnR5cG9ncmFwaHkuZm9udFNpemUgLSAxKVxuICAgIH0pLFxuICAgIGxhYmVsV3JhcHBlZDogKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIHRoZW1lLmJyZWFrcG9pbnRzLmRvd24oJ21kJyksIHtcbiAgICAgIGZvbnRTaXplOiB0aGVtZS50eXBvZ3JhcGh5LnB4VG9SZW0odGhlbWUudHlwb2dyYXBoeS5mb250U2l6ZSAtIDIpXG4gICAgfSlcbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZWZ1bCB0byBleHRlbmQgdGhlIHN0eWxlIGFwcGxpZWQgdG8gY29tcG9uZW50cy5cbiAgICovXG4gIGNsYXNzZXM6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgdGFiIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgZnVsbFdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCxcblxuICAvKipcbiAgICogVGhlIGljb24gZWxlbWVudC4gSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBpY29uOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKiBGb3Igc2VydmVyIHNpZGUgcmVuZGVyaW5nIGNvbnNpZGVyYXRpb24sIHdlIGxldCB0aGUgc2VsZWN0ZWQgdGFiXG4gICAqIHJlbmRlciB0aGUgaW5kaWNhdG9yLlxuICAgKi9cbiAgaW5kaWNhdG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLCB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfRWxlbWVudCldKSxcblxuICAvKipcbiAgICogVGhlIGxhYmVsIGVsZW1lbnQuXG4gICAqL1xuICBsYWJlbDogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZywgdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9FbGVtZW50IDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0VsZW1lbnQpXSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIG9uQ2hhbmdlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgb25DbGljazogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHNlbGVjdGVkOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QsXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIHRleHRDb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mVHlwZShbcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYWNjZW50J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydwcmltYXJ5J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0J10pLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nXSksXG5cbiAgLyoqXG4gICAqIFlvdSBjYW4gcHJvdmlkZSB5b3VyIG93biB2YWx1ZS4gT3RoZXJ3aXNlLCB3ZSBmYWxsYmFjayB0byB0aGUgY2hpbGQgcG9zaXRpb24gaW5kZXguXG4gICAqL1xuICB2YWx1ZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmFueVxufTtcblxudmFyIFRhYiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFRhYiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gVGFiKCkge1xuICAgIHZhciBfcmVmO1xuXG4gICAgdmFyIF90ZW1wLCBfdGhpcywgX3JldDtcblxuICAgICgwLCBfY2xhc3NDYWxsQ2hlY2szLmRlZmF1bHQpKHRoaXMsIFRhYik7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBUYWIuX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFRhYikpLmNhbGwuYXBwbHkoX3JlZiwgW3RoaXNdLmNvbmNhdChhcmdzKSkpLCBfdGhpcyksIF90aGlzLnN0YXRlID0ge1xuICAgICAgd3JhcHBlZFRleHQ6IGZhbHNlXG4gICAgfSwgX3RoaXMuaGFuZGxlQ2hhbmdlID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBvbkNoYW5nZSA9IF90aGlzJHByb3BzLm9uQ2hhbmdlLFxuICAgICAgICAgIHZhbHVlID0gX3RoaXMkcHJvcHMudmFsdWUsXG4gICAgICAgICAgb25DbGljayA9IF90aGlzJHByb3BzLm9uQ2xpY2s7XG5cblxuICAgICAgaWYgKG9uQ2hhbmdlKSB7XG4gICAgICAgIG9uQ2hhbmdlKGV2ZW50LCB2YWx1ZSk7XG4gICAgICB9XG5cbiAgICAgIGlmIChvbkNsaWNrKSB7XG4gICAgICAgIG9uQ2xpY2soZXZlbnQpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmxhYmVsID0gdW5kZWZpbmVkLCBfdGhpcy5jaGVja1RleHRXcmFwID0gZnVuY3Rpb24gKCkge1xuICAgICAgaWYgKF90aGlzLmxhYmVsKSB7XG4gICAgICAgIHZhciBfd3JhcHBlZFRleHQgPSBfdGhpcy5sYWJlbC5nZXRDbGllbnRSZWN0cygpLmxlbmd0aCA+IDE7XG4gICAgICAgIGlmIChfdGhpcy5zdGF0ZS53cmFwcGVkVGV4dCAhPT0gX3dyYXBwZWRUZXh0KSB7XG4gICAgICAgICAgX3RoaXMuc2V0U3RhdGUoeyB3cmFwcGVkVGV4dDogX3dyYXBwZWRUZXh0IH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfSwgX3RlbXApLCAoMCwgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zLmRlZmF1bHQpKF90aGlzLCBfcmV0KTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFRhYiwgW3tcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgdGhpcy5jaGVja1RleHRXcmFwKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAnY29tcG9uZW50RGlkVXBkYXRlJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkVXBkYXRlKHByZXZQcm9wcywgcHJldlN0YXRlKSB7XG4gICAgICBpZiAodGhpcy5zdGF0ZS53cmFwcGVkVGV4dCA9PT0gcHJldlN0YXRlLndyYXBwZWRUZXh0KSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBBdCBjZXJ0YWluIHRleHQgYW5kIHRhYiBsZW5ndGhzLCBhIGxhcmdlciBmb250IHNpemUgbWF5IHdyYXAgdG8gdHdvIGxpbmVzIHdoaWxlIHRoZSBzbWFsbGVyXG4gICAgICAgICAqIGZvbnQgc2l6ZSBzdGlsbCBvbmx5IHJlcXVpcmVzIG9uZSBsaW5lLiAgVGhpcyBjaGVjayB3aWxsIHByZXZlbnQgYW4gaW5maW5pdGUgcmVuZGVyIGxvb3BcbiAgICAgICAgICogZnJvbiBvY2N1cnJpbmcgaW4gdGhhdCBzY2VuYXJpby5cbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuY2hlY2tUZXh0V3JhcCgpO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfdGhpczIgPSB0aGlzLFxuICAgICAgICAgIF9jbGFzc05hbWVzMjtcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGRpc2FibGVkID0gX3Byb3BzLmRpc2FibGVkLFxuICAgICAgICAgIGZ1bGxXaWR0aCA9IF9wcm9wcy5mdWxsV2lkdGgsXG4gICAgICAgICAgaWNvblByb3AgPSBfcHJvcHMuaWNvbixcbiAgICAgICAgICBpbmRpY2F0b3IgPSBfcHJvcHMuaW5kaWNhdG9yLFxuICAgICAgICAgIGxhYmVsUHJvcCA9IF9wcm9wcy5sYWJlbCxcbiAgICAgICAgICBvbkNoYW5nZSA9IF9wcm9wcy5vbkNoYW5nZSxcbiAgICAgICAgICBzZWxlY3RlZCA9IF9wcm9wcy5zZWxlY3RlZCxcbiAgICAgICAgICBzdHlsZVByb3AgPSBfcHJvcHMuc3R5bGUsXG4gICAgICAgICAgdGV4dENvbG9yID0gX3Byb3BzLnRleHRDb2xvcixcbiAgICAgICAgICB2YWx1ZSA9IF9wcm9wcy52YWx1ZSxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdkaXNhYmxlZCcsICdmdWxsV2lkdGgnLCAnaWNvbicsICdpbmRpY2F0b3InLCAnbGFiZWwnLCAnb25DaGFuZ2UnLCAnc2VsZWN0ZWQnLCAnc3R5bGUnLCAndGV4dENvbG9yJywgJ3ZhbHVlJ10pO1xuXG5cbiAgICAgIHZhciBpY29uID0gdm9pZCAwO1xuXG4gICAgICBpZiAoaWNvblByb3AgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBpY29uID0gX3JlYWN0Mi5kZWZhdWx0LmlzVmFsaWRFbGVtZW50KGljb25Qcm9wKSA/IGljb25Qcm9wIDogX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgX0ljb24yLmRlZmF1bHQsXG4gICAgICAgICAgbnVsbCxcbiAgICAgICAgICBpY29uUHJvcFxuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICB2YXIgbGFiZWwgPSB2b2lkIDA7XG5cbiAgICAgIGlmIChsYWJlbFByb3AgIT09IHVuZGVmaW5lZCkge1xuICAgICAgICBsYWJlbCA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmxhYmVsQ29udGFpbmVyIH0sXG4gICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAnc3BhbicsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmxhYmVsLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5sYWJlbFdyYXBwZWQsIHRoaXMuc3RhdGUud3JhcHBlZFRleHQpKSxcbiAgICAgICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgICAgIF90aGlzMi5sYWJlbCA9IG5vZGU7XG4gICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBsYWJlbFByb3BcbiAgICAgICAgICApXG4gICAgICAgICk7XG4gICAgICB9XG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKF9jbGFzc05hbWVzMiA9IHt9LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczIsIGNsYXNzZXNbJ3Jvb3QnICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikodGV4dENvbG9yKV0sIHRydWUpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczIsIGNsYXNzZXNbJ3Jvb3QnICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikodGV4dENvbG9yKSArICdEaXNhYmxlZCddLCBkaXNhYmxlZCksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMiwgY2xhc3Nlc1sncm9vdCcgKyAoMCwgX2hlbHBlcnMuY2FwaXRhbGl6ZUZpcnN0TGV0dGVyKSh0ZXh0Q29sb3IpICsgJ1NlbGVjdGVkJ10sIHNlbGVjdGVkKSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMyLCBjbGFzc2VzLnJvb3RMYWJlbEljb24sIGljb24gJiYgbGFiZWwpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lczIsIGNsYXNzZXMuZnVsbFdpZHRoLCBmdWxsV2lkdGgpLCBfY2xhc3NOYW1lczIpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgdmFyIHN0eWxlID0ge307XG5cbiAgICAgIGlmICh0ZXh0Q29sb3IgIT09ICdhY2NlbnQnICYmIHRleHRDb2xvciAhPT0gJ2luaGVyaXQnKSB7XG4gICAgICAgIHN0eWxlLmNvbG9yID0gdGV4dENvbG9yO1xuICAgICAgfVxuXG4gICAgICBzdHlsZSA9ICgwLCBfa2V5czIuZGVmYXVsdCkoc3R5bGUpLmxlbmd0aCA+IDAgPyAoMCwgX2V4dGVuZHM0LmRlZmF1bHQpKHt9LCBzdHlsZSwgc3R5bGVQcm9wKSA6IHN0eWxlUHJvcDtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzNC5kZWZhdWx0KSh7XG4gICAgICAgICAgZm9jdXNSaXBwbGU6IHRydWUsXG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgICAgc3R5bGU6IHN0eWxlLFxuICAgICAgICAgIHJvbGU6ICd0YWInLFxuICAgICAgICAgICdhcmlhLXNlbGVjdGVkJzogc2VsZWN0ZWQsXG4gICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVkXG4gICAgICAgIH0sIG90aGVyLCB7XG4gICAgICAgICAgb25DbGljazogdGhpcy5oYW5kbGVDaGFuZ2VcbiAgICAgICAgfSksXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdzcGFuJyxcbiAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy53cmFwcGVyIH0sXG4gICAgICAgICAgaWNvbixcbiAgICAgICAgICBsYWJlbFxuICAgICAgICApLFxuICAgICAgICBpbmRpY2F0b3JcbiAgICAgICk7XG4gICAgfVxuICB9XSk7XG4gIHJldHVybiBUYWI7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5UYWIuZGVmYXVsdFByb3BzID0ge1xuICBkaXNhYmxlZDogZmFsc2Vcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpVGFiJyB9KShUYWIpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9yZWY7IC8vICB3ZWFrXG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcbiAgICAgIGhlaWdodDogMixcbiAgICAgIGJvdHRvbTogMCxcbiAgICAgIHdpZHRoOiAnMTAwJScsXG4gICAgICB0cmFuc2l0aW9uOiB0aGVtZS50cmFuc2l0aW9ucy5jcmVhdGUoKSxcbiAgICAgIHdpbGxDaGFuZ2U6ICdsZWZ0LCB3aWR0aCdcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9JbmRpY2F0b3JTdHlsZSA9IHtcbiAgbGVmdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcbiAgd2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXJcbn07XG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvdmlkZWRQcm9wcyA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkXG59O1xudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgdGFiIGluZGljYXRvci5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2ZUeXBlKFtyZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydhY2NlbnQnXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ3ByaW1hcnknXSksIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqIFRoZSBzdHlsZSBvZiB0aGUgcm9vdCBlbGVtZW50LlxuICAgKi9cbiAgc3R5bGU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZSh7XG4gICAgbGVmdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlcixcbiAgICB3aWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxuICB9KS5pc1JlcXVpcmVkXG59O1xuXG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xuZnVuY3Rpb24gVGFiSW5kaWNhdG9yKHByb3BzKSB7XG4gIHZhciBjbGFzc2VzID0gcHJvcHMuY2xhc3NlcyxcbiAgICAgIGNsYXNzTmFtZVByb3AgPSBwcm9wcy5jbGFzc05hbWUsXG4gICAgICBjb2xvciA9IHByb3BzLmNvbG9yLFxuICAgICAgc3R5bGVQcm9wID0gcHJvcHMuc3R5bGU7XG5cbiAgdmFyIGNvbG9yUHJlZGVmaW5lZCA9IFsncHJpbWFyeScsICdhY2NlbnQnXS5pbmRleE9mKGNvbG9yKSAhPT0gLTE7XG4gIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yUHJlZGVmaW5lZCksIGNsYXNzTmFtZVByb3ApO1xuXG4gIHZhciBzdHlsZSA9IGNvbG9yUHJlZGVmaW5lZCA/IHN0eWxlUHJvcCA6ICgwLCBfZXh0ZW5kczMuZGVmYXVsdCkoe30sIHN0eWxlUHJvcCwge1xuICAgIGJhY2tncm91bmRDb2xvcjogY29sb3JcbiAgfSk7XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdkaXYnLCB7IGNsYXNzTmFtZTogY2xhc3NOYW1lLCBzdHlsZTogc3R5bGUgfSk7XG59XG5cblRhYkluZGljYXRvci5wcm9wVHlwZXMgPSBwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gXCJwcm9kdWN0aW9uXCIgPyAoX3JlZiA9IHtcbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdC5pc1JlcXVpcmVkXG59LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NlcycsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vYmplY3QpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnY2xhc3NOYW1lJywgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9yZWYsICdjb2xvcicsIHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2FjY2VudCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncHJpbWFyeSddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLmlzUmVxdWlyZWQpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfcmVmLCAnc3R5bGUnLCByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoe1xuICBsZWZ0OiByZXF1aXJlKCdwcm9wLXR5cGVzJykubnVtYmVyLFxuICB3aWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlclxufSkuaXNSZXF1aXJlZCksIF9yZWYpIDoge307XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpVGFiSW5kaWNhdG9yJyB9KShUYWJJbmRpY2F0b3IpO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiSW5kaWNhdG9yLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYkluZGljYXRvci5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9CdXR0b25CYXNlID0gcmVxdWlyZSgnLi4vQnV0dG9uQmFzZScpO1xuXG52YXIgX0J1dHRvbkJhc2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfQnV0dG9uQmFzZSk7XG5cbnZhciBfS2V5Ym9hcmRBcnJvd0xlZnQgPSByZXF1aXJlKCcuLi9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd0xlZnQnKTtcblxudmFyIF9LZXlib2FyZEFycm93TGVmdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9LZXlib2FyZEFycm93TGVmdCk7XG5cbnZhciBfS2V5Ym9hcmRBcnJvd1JpZ2h0ID0gcmVxdWlyZSgnLi4vc3ZnLWljb25zL0tleWJvYXJkQXJyb3dSaWdodCcpO1xuXG52YXIgX0tleWJvYXJkQXJyb3dSaWdodDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9LZXlib2FyZEFycm93UmlnaHQpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG4vLyAgd2Vha1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICBjb2xvcjogJ2luaGVyaXQnLFxuICAgICAgZmxleDogJzAgMCAnICsgdGhlbWUuc3BhY2luZy51bml0ICogNyArICdweCdcbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfUHJvcHMgPSB7XG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFdoaWNoIGRpcmVjdGlvbiBzaG91bGQgdGhlIGJ1dHRvbiBpbmRpY2F0ZT9cbiAgICovXG4gIGRpcmVjdGlvbjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnbGVmdCcsICdyaWdodCddKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBDYWxsYmFjayB0byBleGVjdXRlIGZvciBidXR0b24gcHJlc3MuXG4gICAqL1xuICBvbkNsaWNrOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogU2hvdWxkIHRoZSBidXR0b24gYmUgcHJlc2VudCBvciBqdXN0IGNvbnN1bWUgc3BhY2UuXG4gICAqL1xuICB2aXNpYmxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbFxufTtcblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfS2V5Ym9hcmRBcnJvd0xlZnQyLmRlZmF1bHQsIG51bGwpO1xuXG52YXIgX3JlZjIgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfS2V5Ym9hcmRBcnJvd1JpZ2h0Mi5kZWZhdWx0LCBudWxsKTtcblxuLyoqXG4gKiBAaWdub3JlIC0gaW50ZXJuYWwgY29tcG9uZW50LlxuICovXG52YXIgVGFiU2Nyb2xsQnV0dG9uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoVGFiU2Nyb2xsQnV0dG9uLCBfUmVhY3QkQ29tcG9uZW50KTtcblxuICBmdW5jdGlvbiBUYWJTY3JvbGxCdXR0b24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgVGFiU2Nyb2xsQnV0dG9uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoVGFiU2Nyb2xsQnV0dG9uLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShUYWJTY3JvbGxCdXR0b24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFRhYlNjcm9sbEJ1dHRvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgZGlyZWN0aW9uID0gX3Byb3BzLmRpcmVjdGlvbixcbiAgICAgICAgICBvbkNsaWNrID0gX3Byb3BzLm9uQ2xpY2ssXG4gICAgICAgICAgdmlzaWJsZSA9IF9wcm9wcy52aXNpYmxlLFxuICAgICAgICAgIG90aGVyID0gKDAsIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMuZGVmYXVsdCkoX3Byb3BzLCBbJ2NsYXNzZXMnLCAnY2xhc3NOYW1lJywgJ2RpcmVjdGlvbicsICdvbkNsaWNrJywgJ3Zpc2libGUnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgaWYgKCF2aXNpYmxlKSB7XG4gICAgICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgnZGl2JywgeyBjbGFzc05hbWU6IGNsYXNzTmFtZSB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICBfQnV0dG9uQmFzZTIuZGVmYXVsdCxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lLCBvbkNsaWNrOiBvbkNsaWNrLCB0YWJJbmRleDogLTEgfSwgb3RoZXIpLFxuICAgICAgICBkaXJlY3Rpb24gPT09ICdsZWZ0JyA/IF9yZWYgOiBfcmVmMlxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFRhYlNjcm9sbEJ1dHRvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblRhYlNjcm9sbEJ1dHRvbi5kZWZhdWx0UHJvcHMgPSB7XG4gIHZpc2libGU6IHRydWVcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyBuYW1lOiAnTXVpVGFiU2Nyb2xsQnV0dG9uJyB9KShUYWJTY3JvbGxCdXR0b24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFiU2Nyb2xsQnV0dG9uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYlNjcm9sbEJ1dHRvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9pc05hbiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9udW1iZXIvaXMtbmFuJyk7XG5cbnZhciBfaXNOYW4yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNOYW4pO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfd2FybmluZyA9IHJlcXVpcmUoJ3dhcm5pbmcnKTtcblxudmFyIF93YXJuaW5nMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dhcm5pbmcpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF9yZWFjdEV2ZW50TGlzdGVuZXIgPSByZXF1aXJlKCdyZWFjdC1ldmVudC1saXN0ZW5lcicpO1xuXG52YXIgX3JlYWN0RXZlbnRMaXN0ZW5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdEV2ZW50TGlzdGVuZXIpO1xuXG52YXIgX2RlYm91bmNlID0gcmVxdWlyZSgnbG9kYXNoL2RlYm91bmNlJyk7XG5cbnZhciBfZGVib3VuY2UyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVib3VuY2UpO1xuXG52YXIgX3JlYWN0U2Nyb2xsYmFyU2l6ZSA9IHJlcXVpcmUoJ3JlYWN0LXNjcm9sbGJhci1zaXplJyk7XG5cbnZhciBfcmVhY3RTY3JvbGxiYXJTaXplMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0U2Nyb2xsYmFyU2l6ZSk7XG5cbnZhciBfbm9ybWFsaXplU2Nyb2xsTGVmdCA9IHJlcXVpcmUoJ25vcm1hbGl6ZS1zY3JvbGwtbGVmdCcpO1xuXG52YXIgX3Njcm9sbCA9IHJlcXVpcmUoJ3Njcm9sbCcpO1xuXG52YXIgX3Njcm9sbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zY3JvbGwpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfVGFiSW5kaWNhdG9yID0gcmVxdWlyZSgnLi9UYWJJbmRpY2F0b3InKTtcblxudmFyIF9UYWJJbmRpY2F0b3IyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfVGFiSW5kaWNhdG9yKTtcblxudmFyIF9UYWJTY3JvbGxCdXR0b24gPSByZXF1aXJlKCcuL1RhYlNjcm9sbEJ1dHRvbicpO1xuXG52YXIgX1RhYlNjcm9sbEJ1dHRvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UYWJTY3JvbGxCdXR0b24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZSA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jO1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfSW5kaWNhdG9yU3R5bGUgPSByZXF1aXJlKCcuL1RhYkluZGljYXRvcicpLmJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0luZGljYXRvclN0eWxlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIG92ZXJmbG93OiAnaGlkZGVuJyxcbiAgICAgIG1pbkhlaWdodDogNDgsXG4gICAgICBXZWJraXRPdmVyZmxvd1Njcm9sbGluZzogJ3RvdWNoJyAvLyBBZGQgaU9TIG1vbWVudHVtIHNjcm9sbGluZy5cbiAgICB9LFxuICAgIGZsZXhDb250YWluZXI6IHtcbiAgICAgIGRpc3BsYXk6ICdmbGV4J1xuICAgIH0sXG4gICAgc2Nyb2xsaW5nQ29udGFpbmVyOiB7XG4gICAgICBwb3NpdGlvbjogJ3JlbGF0aXZlJyxcbiAgICAgIGRpc3BsYXk6ICdpbmxpbmUtYmxvY2snLFxuICAgICAgZmxleDogJzEgMSBhdXRvJyxcbiAgICAgIHdoaXRlU3BhY2U6ICdub3dyYXAnXG4gICAgfSxcbiAgICBmaXhlZDoge1xuICAgICAgb3ZlcmZsb3dYOiAnaGlkZGVuJyxcbiAgICAgIHdpZHRoOiAnMTAwJSdcbiAgICB9LFxuICAgIHNjcm9sbGFibGU6IHtcbiAgICAgIG92ZXJmbG93WDogJ3Njcm9sbCdcbiAgICB9LFxuICAgIGNlbnRlcmVkOiB7XG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcidcbiAgICB9LFxuICAgIGJ1dHRvbkF1dG86ICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKHt9LCB0aGVtZS5icmVha3BvaW50cy5kb3duKCdzbScpLCB7XG4gICAgICBkaXNwbGF5OiAnbm9uZSdcbiAgICB9KVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIENTUyBjbGFzcyBuYW1lIG9mIHRoZSBzY3JvbGwgYnV0dG9uIGVsZW1lbnRzLlxuICAgKi9cbiAgYnV0dG9uQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSB0YWJzIHdpbGwgYmUgY2VudGVyZWQuXG4gICAqIFRoaXMgcHJvcGVydHkgaXMgaW50ZW5kZWQgZm9yIGxhcmdlIHZpZXdzLlxuICAgKi9cbiAgY2VudGVyZWQ6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5ib29sLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIFRoZSBjb250ZW50IG9mIHRoZSBjb21wb25lbnQuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIHRhYnMgd2lsbCBncm93IHRvIHVzZSBhbGwgdGhlIGF2YWlsYWJsZSBzcGFjZS5cbiAgICogVGhpcyBwcm9wZXJ0eSBpcyBpbnRlbmRlZCBmb3Igc21hbGwgdmlld3MsIGxpa2Ugb24gbW9iaWxlLlxuICAgKi9cbiAgZnVsbFdpZHRoOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBUaGUgQ1NTIGNsYXNzIG5hbWUgb2YgdGhlIGluZGljYXRvciBlbGVtZW50LlxuICAgKi9cbiAgaW5kaWNhdG9yQ2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHRoZSBjb2xvciBvZiB0aGUgaW5kaWNhdG9yLlxuICAgKi9cbiAgaW5kaWNhdG9yQ29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZlR5cGUoW3JlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2FjY2VudCddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsncHJpbWFyeSddKSwgcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZ10pLmlzUmVxdWlyZWQsXG5cbiAgLyoqXG4gICAqIENhbGxiYWNrIGZpcmVkIHdoZW4gdGhlIHZhbHVlIGNoYW5nZXMuXG4gICAqXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBldmVudCBUaGUgZXZlbnQgc291cmNlIG9mIHRoZSBjYWxsYmFja1xuICAgKiBAcGFyYW0ge251bWJlcn0gdmFsdWUgV2UgZGVmYXVsdCB0byB0aGUgaW5kZXggb2YgdGhlIGNoaWxkXG4gICAqL1xuICBvbkNoYW5nZTogcmVxdWlyZSgncHJvcC10eXBlcycpLmZ1bmMuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVHJ1ZSBpbnZva2VzIHNjcm9sbGluZyBwcm9wZXJ0aWVzIGFuZCBhbGxvdyBmb3IgaG9yaXpvbnRhbGx5IHNjcm9sbGluZ1xuICAgKiAob3Igc3dpcGluZykgdGhlIHRhYiBiYXIuXG4gICAqL1xuICBzY3JvbGxhYmxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmUgYmVoYXZpb3Igb2Ygc2Nyb2xsIGJ1dHRvbnMgd2hlbiB0YWJzIGFyZSBzZXQgdG8gc2Nyb2xsXG4gICAqIGBhdXRvYCB3aWxsIG9ubHkgcHJlc2VudCB0aGVtIG9uIG1lZGl1bSBhbmQgbGFyZ2VyIHZpZXdwb3J0c1xuICAgKiBgb25gIHdpbGwgYWx3YXlzIHByZXNlbnQgdGhlbVxuICAgKiBgb2ZmYCB3aWxsIG5ldmVyIHByZXNlbnQgdGhlbVxuICAgKi9cbiAgc2Nyb2xsQnV0dG9uczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnYXV0bycsICdvbicsICdvZmYnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIGNvbXBvbmVudCB1c2VkIHRvIHJlbmRlciB0aGUgc2Nyb2xsIGJ1dHRvbnMuXG4gICAqL1xuICBUYWJTY3JvbGxCdXR0b246IHR5cGVvZiBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlID09PSAnZnVuY3Rpb24nID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZS5pc1JlcXVpcmVkID8gYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZS5pc1JlcXVpcmVkIDogYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29tcG9uZW50VHlwZSA6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zaGFwZShiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Db21wb25lbnRUeXBlKS5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBEZXRlcm1pbmVzIHRoZSBjb2xvciBvZiB0aGUgYFRhYmAuXG4gICAqL1xuICB0ZXh0Q29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2FjY2VudCcsICdwcmltYXJ5JywgJ2luaGVyaXQnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVGhlIHZhbHVlIG9mIHRoZSBjdXJyZW50bHkgc2VsZWN0ZWQgYFRhYmAuXG4gICAqIElmIHlvdSBkb24ndCB3YW50IGFueSBzZWxlY3RlZCBgVGFiYCwgeW91IGNhbiBzZXQgdGhpcyBwcm9wZXJ0eSB0byBgZmFsc2VgLlxuICAgKi9cbiAgdmFsdWU6IGZ1bmN0aW9uIHZhbHVlKHByb3BzLCBwcm9wTmFtZSwgY29tcG9uZW50TmFtZSkge1xuICAgIGlmICghT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHByb3BzLCBwcm9wTmFtZSkpIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignUHJvcCBgJyArIHByb3BOYW1lICsgJ2AgaGFzIHR5cGUgXFwnYW55XFwnIG9yIFxcJ21peGVkXFwnLCBidXQgd2FzIG5vdCBwcm92aWRlZCB0byBgJyArIGNvbXBvbmVudE5hbWUgKyAnYC4gUGFzcyB1bmRlZmluZWQgb3IgYW55IG90aGVyIHZhbHVlLicpO1xuICAgIH1cbiAgfVxufTtcbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9UYWJzTWV0YSA9IHtcbiAgY2xpZW50V2lkdGg6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5udW1iZXIuaXNSZXF1aXJlZCxcbiAgc2Nyb2xsTGVmdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkLFxuICBzY3JvbGxMZWZ0Tm9ybWFsaXplZDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkLFxuICBzY3JvbGxXaWR0aDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkLFxuXG4gIC8vIENsaWVudFJlY3RcbiAgbGVmdDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkLFxuICByaWdodDogcmVxdWlyZSgncHJvcC10eXBlcycpLm51bWJlci5pc1JlcXVpcmVkXG59O1xuXG52YXIgVGFicyA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFRhYnMsIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFRhYnMoKSB7XG4gICAgdmFyIF9yZWY7XG5cbiAgICB2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgVGFicyk7XG5cbiAgICBmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuICAgICAgYXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcbiAgICB9XG5cbiAgICByZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBUYWJzLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShUYWJzKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuc3RhdGUgPSB7XG4gICAgICBpbmRpY2F0b3JTdHlsZToge30sXG4gICAgICBzY3JvbGxlclN0eWxlOiB7XG4gICAgICAgIG1hcmdpbkJvdHRvbTogMFxuICAgICAgfSxcbiAgICAgIHNob3dMZWZ0U2Nyb2xsOiBmYWxzZSxcbiAgICAgIHNob3dSaWdodFNjcm9sbDogZmFsc2UsXG4gICAgICBtb3VudGVkOiBmYWxzZVxuICAgIH0sIF90aGlzLmdldENvbmRpdGlvbmFsRWxlbWVudHMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBjbGFzc2VzID0gX3RoaXMkcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBidXR0b25DbGFzc05hbWUgPSBfdGhpcyRwcm9wcy5idXR0b25DbGFzc05hbWUsXG4gICAgICAgICAgc2Nyb2xsYWJsZSA9IF90aGlzJHByb3BzLnNjcm9sbGFibGUsXG4gICAgICAgICAgc2Nyb2xsQnV0dG9ucyA9IF90aGlzJHByb3BzLnNjcm9sbEJ1dHRvbnMsXG4gICAgICAgICAgVGFiU2Nyb2xsQnV0dG9uUHJvcCA9IF90aGlzJHByb3BzLlRhYlNjcm9sbEJ1dHRvbixcbiAgICAgICAgICB0aGVtZSA9IF90aGlzJHByb3BzLnRoZW1lO1xuXG4gICAgICB2YXIgY29uZGl0aW9uYWxFbGVtZW50cyA9IHt9O1xuICAgICAgY29uZGl0aW9uYWxFbGVtZW50cy5zY3JvbGxiYXJTaXplTGlzdGVuZXIgPSBzY3JvbGxhYmxlID8gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoX3JlYWN0U2Nyb2xsYmFyU2l6ZTIuZGVmYXVsdCwge1xuICAgICAgICBvbkxvYWQ6IF90aGlzLmhhbmRsZVNjcm9sbGJhclNpemVDaGFuZ2UsXG4gICAgICAgIG9uQ2hhbmdlOiBfdGhpcy5oYW5kbGVTY3JvbGxiYXJTaXplQ2hhbmdlXG4gICAgICB9KSA6IG51bGw7XG5cbiAgICAgIHZhciBzaG93U2Nyb2xsQnV0dG9ucyA9IHNjcm9sbGFibGUgJiYgKHNjcm9sbEJ1dHRvbnMgPT09ICdhdXRvJyB8fCBzY3JvbGxCdXR0b25zID09PSAnb24nKTtcblxuICAgICAgY29uZGl0aW9uYWxFbGVtZW50cy5zY3JvbGxCdXR0b25MZWZ0ID0gc2hvd1Njcm9sbEJ1dHRvbnMgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChUYWJTY3JvbGxCdXR0b25Qcm9wLCB7XG4gICAgICAgIGRpcmVjdGlvbjogdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uID09PSAncnRsJyA/ICdyaWdodCcgOiAnbGVmdCcsXG4gICAgICAgIG9uQ2xpY2s6IF90aGlzLmhhbmRsZUxlZnRTY3JvbGxDbGljayxcbiAgICAgICAgdmlzaWJsZTogX3RoaXMuc3RhdGUuc2hvd0xlZnRTY3JvbGwsXG4gICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KSgoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5idXR0b25BdXRvLCBzY3JvbGxCdXR0b25zID09PSAnYXV0bycpLCBidXR0b25DbGFzc05hbWUpXG4gICAgICB9KSA6IG51bGw7XG5cbiAgICAgIGNvbmRpdGlvbmFsRWxlbWVudHMuc2Nyb2xsQnV0dG9uUmlnaHQgPSBzaG93U2Nyb2xsQnV0dG9ucyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFRhYlNjcm9sbEJ1dHRvblByb3AsIHtcbiAgICAgICAgZGlyZWN0aW9uOiB0aGVtZSAmJiB0aGVtZS5kaXJlY3Rpb24gPT09ICdydGwnID8gJ2xlZnQnIDogJ3JpZ2h0JyxcbiAgICAgICAgb25DbGljazogX3RoaXMuaGFuZGxlUmlnaHRTY3JvbGxDbGljayxcbiAgICAgICAgdmlzaWJsZTogX3RoaXMuc3RhdGUuc2hvd1JpZ2h0U2Nyb2xsLFxuICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXMuYnV0dG9uQXV0bywgc2Nyb2xsQnV0dG9ucyA9PT0gJ2F1dG8nKSwgYnV0dG9uQ2xhc3NOYW1lKVxuICAgICAgfSkgOiBudWxsO1xuXG4gICAgICByZXR1cm4gY29uZGl0aW9uYWxFbGVtZW50cztcbiAgICB9LCBfdGhpcy5nZXRUYWJzTWV0YSA9IGZ1bmN0aW9uICh2YWx1ZSwgZGlyZWN0aW9uKSB7XG4gICAgICB2YXIgdGFic01ldGEgPSB2b2lkIDA7XG4gICAgICBpZiAoX3RoaXMudGFicykge1xuICAgICAgICB2YXIgcmVjdCA9IF90aGlzLnRhYnMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgIC8vIGNyZWF0ZSBhIG5ldyBvYmplY3Qgd2l0aCBDbGllbnRSZWN0IGNsYXNzIHByb3BzICsgc2Nyb2xsTGVmdFxuICAgICAgICB0YWJzTWV0YSA9IHtcbiAgICAgICAgICBjbGllbnRXaWR0aDogX3RoaXMudGFicyA/IF90aGlzLnRhYnMuY2xpZW50V2lkdGggOiAwLFxuICAgICAgICAgIHNjcm9sbExlZnQ6IF90aGlzLnRhYnMgPyBfdGhpcy50YWJzLnNjcm9sbExlZnQgOiAwLFxuICAgICAgICAgIHNjcm9sbExlZnROb3JtYWxpemVkOiBfdGhpcy50YWJzID8gKDAsIF9ub3JtYWxpemVTY3JvbGxMZWZ0LmdldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0KShfdGhpcy50YWJzLCBkaXJlY3Rpb24pIDogMCxcbiAgICAgICAgICBzY3JvbGxXaWR0aDogX3RoaXMudGFicyA/IF90aGlzLnRhYnMuc2Nyb2xsV2lkdGggOiAwLFxuICAgICAgICAgIGxlZnQ6IHJlY3QubGVmdCxcbiAgICAgICAgICByaWdodDogcmVjdC5yaWdodFxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICB2YXIgdGFiTWV0YSA9IHZvaWQgMDtcbiAgICAgIGlmIChfdGhpcy50YWJzICYmIHZhbHVlICE9PSBmYWxzZSkge1xuICAgICAgICB2YXIgX2NoaWxkcmVuID0gX3RoaXMudGFicy5jaGlsZHJlblswXS5jaGlsZHJlbjtcblxuICAgICAgICBpZiAoX2NoaWxkcmVuLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICB2YXIgdGFiID0gX2NoaWxkcmVuW190aGlzLnZhbHVlVG9JbmRleFt2YWx1ZV1dO1xuICAgICAgICAgIHByb2Nlc3MuZW52Lk5PREVfRU5WICE9PSBcInByb2R1Y3Rpb25cIiA/ICgwLCBfd2FybmluZzIuZGVmYXVsdCkoQm9vbGVhbih0YWIpLCAnTWF0ZXJpYWwtVUk6IHRoZSB2YWx1ZSBwcm92aWRlZCBgJyArIHZhbHVlICsgJ2AgaXMgaW52YWxpZCcpIDogdm9pZCAwO1xuICAgICAgICAgIHRhYk1ldGEgPSB0YWIgPyB0YWIuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkgOiBudWxsO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICByZXR1cm4geyB0YWJzTWV0YTogdGFic01ldGEsIHRhYk1ldGE6IHRhYk1ldGEgfTtcbiAgICB9LCBfdGhpcy50YWJzID0gdW5kZWZpbmVkLCBfdGhpcy52YWx1ZVRvSW5kZXggPSB7fSwgX3RoaXMuaGFuZGxlUmVzaXplID0gKDAsIF9kZWJvdW5jZTIuZGVmYXVsdCkoZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMudXBkYXRlSW5kaWNhdG9yU3RhdGUoX3RoaXMucHJvcHMpO1xuICAgICAgX3RoaXMudXBkYXRlU2Nyb2xsQnV0dG9uU3RhdGUoKTtcbiAgICB9LCAxNjYpLCBfdGhpcy5oYW5kbGVMZWZ0U2Nyb2xsQ2xpY2sgPSBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoX3RoaXMudGFicykge1xuICAgICAgICBfdGhpcy5tb3ZlVGFic1Njcm9sbCgtX3RoaXMudGFicy5jbGllbnRXaWR0aCk7XG4gICAgICB9XG4gICAgfSwgX3RoaXMuaGFuZGxlUmlnaHRTY3JvbGxDbGljayA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmIChfdGhpcy50YWJzKSB7XG4gICAgICAgIF90aGlzLm1vdmVUYWJzU2Nyb2xsKF90aGlzLnRhYnMuY2xpZW50V2lkdGgpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLmhhbmRsZVNjcm9sbGJhclNpemVDaGFuZ2UgPSBmdW5jdGlvbiAoX3JlZjIpIHtcbiAgICAgIHZhciBzY3JvbGxiYXJIZWlnaHQgPSBfcmVmMi5zY3JvbGxiYXJIZWlnaHQ7XG5cbiAgICAgIF90aGlzLnNldFN0YXRlKHtcbiAgICAgICAgc2Nyb2xsZXJTdHlsZToge1xuICAgICAgICAgIG1hcmdpbkJvdHRvbTogLXNjcm9sbGJhckhlaWdodFxuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9LCBfdGhpcy5oYW5kbGVUYWJzU2Nyb2xsID0gKDAsIF9kZWJvdW5jZTIuZGVmYXVsdCkoZnVuY3Rpb24gKCkge1xuICAgICAgX3RoaXMudXBkYXRlU2Nyb2xsQnV0dG9uU3RhdGUoKTtcbiAgICB9LCAxNjYpLCBfdGhpcy5tb3ZlVGFic1Njcm9sbCA9IGZ1bmN0aW9uIChkZWx0YSkge1xuICAgICAgdmFyIHRoZW1lID0gX3RoaXMucHJvcHMudGhlbWU7XG5cblxuICAgICAgaWYgKF90aGlzLnRhYnMpIHtcbiAgICAgICAgdmFyIHRoZW1lRGlyZWN0aW9uID0gdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uO1xuICAgICAgICB2YXIgbXVsdGlwbGllciA9IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyA/IC0xIDogMTtcbiAgICAgICAgdmFyIG5leHRTY3JvbGxMZWZ0ID0gX3RoaXMudGFicy5zY3JvbGxMZWZ0ICsgZGVsdGEgKiBtdWx0aXBsaWVyO1xuICAgICAgICAvLyBGaXggZm9yIEVkZ2VcbiAgICAgICAgdmFyIGludmVydCA9IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyAmJiAoMCwgX25vcm1hbGl6ZVNjcm9sbExlZnQuZGV0ZWN0U2Nyb2xsVHlwZSkoKSA9PT0gJ3JldmVyc2UnID8gLTEgOiAxO1xuICAgICAgICBfc2Nyb2xsMi5kZWZhdWx0LmxlZnQoX3RoaXMudGFicywgaW52ZXJ0ICogbmV4dFNjcm9sbExlZnQpO1xuICAgICAgfVxuICAgIH0sIF90aGlzLnNjcm9sbFNlbGVjdGVkSW50b1ZpZXcgPSBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgX3RoaXMkcHJvcHMyID0gX3RoaXMucHJvcHMsXG4gICAgICAgICAgdGhlbWUgPSBfdGhpcyRwcm9wczIudGhlbWUsXG4gICAgICAgICAgdmFsdWUgPSBfdGhpcyRwcm9wczIudmFsdWU7XG5cblxuICAgICAgdmFyIHRoZW1lRGlyZWN0aW9uID0gdGhlbWUgJiYgdGhlbWUuZGlyZWN0aW9uO1xuXG4gICAgICB2YXIgX3RoaXMkZ2V0VGFic01ldGEgPSBfdGhpcy5nZXRUYWJzTWV0YSh2YWx1ZSwgdGhlbWVEaXJlY3Rpb24pLFxuICAgICAgICAgIHRhYnNNZXRhID0gX3RoaXMkZ2V0VGFic01ldGEudGFic01ldGEsXG4gICAgICAgICAgdGFiTWV0YSA9IF90aGlzJGdldFRhYnNNZXRhLnRhYk1ldGE7XG5cbiAgICAgIGlmICghdGFiTWV0YSB8fCAhdGFic01ldGEpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgICAgfVxuXG4gICAgICBpZiAodGFiTWV0YS5sZWZ0IDwgdGFic01ldGEubGVmdCkge1xuICAgICAgICAvLyBsZWZ0IHNpZGUgb2YgYnV0dG9uIGlzIG91dCBvZiB2aWV3XG4gICAgICAgIHZhciBuZXh0U2Nyb2xsTGVmdCA9IHRhYnNNZXRhLnNjcm9sbExlZnQgKyAodGFiTWV0YS5sZWZ0IC0gdGFic01ldGEubGVmdCk7XG4gICAgICAgIF9zY3JvbGwyLmRlZmF1bHQubGVmdChfdGhpcy50YWJzLCBuZXh0U2Nyb2xsTGVmdCk7XG4gICAgICB9IGVsc2UgaWYgKHRhYk1ldGEucmlnaHQgPiB0YWJzTWV0YS5yaWdodCkge1xuICAgICAgICAvLyByaWdodCBzaWRlIG9mIGJ1dHRvbiBpcyBvdXQgb2Ygdmlld1xuICAgICAgICB2YXIgX25leHRTY3JvbGxMZWZ0ID0gdGFic01ldGEuc2Nyb2xsTGVmdCArICh0YWJNZXRhLnJpZ2h0IC0gdGFic01ldGEucmlnaHQpO1xuICAgICAgICBfc2Nyb2xsMi5kZWZhdWx0LmxlZnQoX3RoaXMudGFicywgX25leHRTY3JvbGxMZWZ0KTtcbiAgICAgIH1cbiAgICB9LCBfdGhpcy51cGRhdGVTY3JvbGxCdXR0b25TdGF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBfdGhpcyRwcm9wczMgPSBfdGhpcy5wcm9wcyxcbiAgICAgICAgICBzY3JvbGxhYmxlID0gX3RoaXMkcHJvcHMzLnNjcm9sbGFibGUsXG4gICAgICAgICAgc2Nyb2xsQnV0dG9ucyA9IF90aGlzJHByb3BzMy5zY3JvbGxCdXR0b25zLFxuICAgICAgICAgIHRoZW1lID0gX3RoaXMkcHJvcHMzLnRoZW1lO1xuXG4gICAgICB2YXIgdGhlbWVEaXJlY3Rpb24gPSB0aGVtZSAmJiB0aGVtZS5kaXJlY3Rpb247XG5cbiAgICAgIGlmIChfdGhpcy50YWJzICYmIHNjcm9sbGFibGUgJiYgc2Nyb2xsQnV0dG9ucyAhPT0gJ29mZicpIHtcbiAgICAgICAgdmFyIF90aGlzJHRhYnMgPSBfdGhpcy50YWJzLFxuICAgICAgICAgICAgX3Njcm9sbFdpZHRoID0gX3RoaXMkdGFicy5zY3JvbGxXaWR0aCxcbiAgICAgICAgICAgIF9jbGllbnRXaWR0aCA9IF90aGlzJHRhYnMuY2xpZW50V2lkdGg7XG5cbiAgICAgICAgdmFyIF9zY3JvbGxMZWZ0ID0gKDAsIF9ub3JtYWxpemVTY3JvbGxMZWZ0LmdldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0KShfdGhpcy50YWJzLCB0aGVtZURpcmVjdGlvbik7XG5cbiAgICAgICAgdmFyIF9zaG93TGVmdFNjcm9sbCA9IHRoZW1lRGlyZWN0aW9uID09PSAncnRsJyA/IF9zY3JvbGxXaWR0aCA+IF9jbGllbnRXaWR0aCArIF9zY3JvbGxMZWZ0IDogX3Njcm9sbExlZnQgPiAwO1xuXG4gICAgICAgIHZhciBfc2hvd1JpZ2h0U2Nyb2xsID0gdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnID8gX3Njcm9sbExlZnQgPiAwIDogX3Njcm9sbFdpZHRoID4gX2NsaWVudFdpZHRoICsgX3Njcm9sbExlZnQ7XG5cbiAgICAgICAgaWYgKF9zaG93TGVmdFNjcm9sbCAhPT0gX3RoaXMuc3RhdGUuc2hvd0xlZnRTY3JvbGwgfHwgX3Nob3dSaWdodFNjcm9sbCAhPT0gX3RoaXMuc3RhdGUuc2hvd1JpZ2h0U2Nyb2xsKSB7XG4gICAgICAgICAgX3RoaXMuc2V0U3RhdGUoeyBzaG93TGVmdFNjcm9sbDogX3Nob3dMZWZ0U2Nyb2xsLCBzaG93UmlnaHRTY3JvbGw6IF9zaG93UmlnaHRTY3JvbGwgfSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LCBfdGVtcCksICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkoX3RoaXMsIF9yZXQpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoVGFicywgW3tcbiAgICBrZXk6ICdjb21wb25lbnREaWRNb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIHJlYWN0L25vLWRpZC1tb3VudC1zZXQtc3RhdGVcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBtb3VudGVkOiB0cnVlIH0pO1xuICAgICAgdGhpcy51cGRhdGVJbmRpY2F0b3JTdGF0ZSh0aGlzLnByb3BzKTtcbiAgICAgIHRoaXMudXBkYXRlU2Nyb2xsQnV0dG9uU3RhdGUoKTtcbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnREaWRVcGRhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiBjb21wb25lbnREaWRVcGRhdGUocHJldlByb3BzLCBwcmV2U3RhdGUpIHtcbiAgICAgIHRoaXMudXBkYXRlU2Nyb2xsQnV0dG9uU3RhdGUoKTtcblxuICAgICAgLy8gVGhlIGluZGV4IG1pZ2h0IGhhdmUgY2hhbmdlZCBhdCB0aGUgc2FtZSB0aW1lLlxuICAgICAgLy8gV2UgbmVlZCB0byBjaGVjayBhZ2FpbiB0aGUgcmlnaHQgaW5kaWNhdG9yIHBvc2l0aW9uLlxuICAgICAgdGhpcy51cGRhdGVJbmRpY2F0b3JTdGF0ZSh0aGlzLnByb3BzKTtcblxuICAgICAgaWYgKHRoaXMuc3RhdGUuaW5kaWNhdG9yU3R5bGUgIT09IHByZXZTdGF0ZS5pbmRpY2F0b3JTdHlsZSkge1xuICAgICAgICB0aGlzLnNjcm9sbFNlbGVjdGVkSW50b1ZpZXcoKTtcbiAgICAgIH1cbiAgICB9XG4gIH0sIHtcbiAgICBrZXk6ICdjb21wb25lbnRXaWxsVW5tb3VudCcsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuICAgICAgdGhpcy5oYW5kbGVSZXNpemUuY2FuY2VsKCk7XG4gICAgICB0aGlzLmhhbmRsZVRhYnNTY3JvbGwuY2FuY2VsKCk7XG4gICAgfVxuICB9LCB7XG4gICAga2V5OiAndXBkYXRlSW5kaWNhdG9yU3RhdGUnLFxuICAgIHZhbHVlOiBmdW5jdGlvbiB1cGRhdGVJbmRpY2F0b3JTdGF0ZShwcm9wcykge1xuICAgICAgdmFyIHRoZW1lID0gcHJvcHMudGhlbWUsXG4gICAgICAgICAgdmFsdWUgPSBwcm9wcy52YWx1ZTtcblxuXG4gICAgICB2YXIgdGhlbWVEaXJlY3Rpb24gPSB0aGVtZSAmJiB0aGVtZS5kaXJlY3Rpb247XG5cbiAgICAgIHZhciBfZ2V0VGFic01ldGEgPSB0aGlzLmdldFRhYnNNZXRhKHZhbHVlLCB0aGVtZURpcmVjdGlvbiksXG4gICAgICAgICAgdGFic01ldGEgPSBfZ2V0VGFic01ldGEudGFic01ldGEsXG4gICAgICAgICAgdGFiTWV0YSA9IF9nZXRUYWJzTWV0YS50YWJNZXRhO1xuXG4gICAgICB2YXIgbGVmdCA9IDA7XG5cbiAgICAgIGlmICh0YWJNZXRhICYmIHRhYnNNZXRhKSB7XG4gICAgICAgIHZhciBjb3JyZWN0aW9uID0gdGhlbWVEaXJlY3Rpb24gPT09ICdydGwnID8gdGFic01ldGEuc2Nyb2xsTGVmdE5vcm1hbGl6ZWQgKyB0YWJzTWV0YS5jbGllbnRXaWR0aCAtIHRhYnNNZXRhLnNjcm9sbFdpZHRoIDogdGFic01ldGEuc2Nyb2xsTGVmdDtcbiAgICAgICAgbGVmdCA9IHRhYk1ldGEubGVmdCAtIHRhYnNNZXRhLmxlZnQgKyBjb3JyZWN0aW9uO1xuICAgICAgfVxuXG4gICAgICB2YXIgaW5kaWNhdG9yU3R5bGUgPSB7XG4gICAgICAgIGxlZnQ6IGxlZnQsXG4gICAgICAgIC8vIE1heSBiZSB3cm9uZyB1bnRpbCB0aGUgZm9udCBpcyBsb2FkZWQuXG4gICAgICAgIHdpZHRoOiB0YWJNZXRhID8gdGFiTWV0YS53aWR0aCA6IDBcbiAgICAgIH07XG5cbiAgICAgIGlmICgoaW5kaWNhdG9yU3R5bGUubGVmdCAhPT0gdGhpcy5zdGF0ZS5pbmRpY2F0b3JTdHlsZS5sZWZ0IHx8IGluZGljYXRvclN0eWxlLndpZHRoICE9PSB0aGlzLnN0YXRlLmluZGljYXRvclN0eWxlLndpZHRoKSAmJiAhKDAsIF9pc05hbjIuZGVmYXVsdCkoaW5kaWNhdG9yU3R5bGUubGVmdCkgJiYgISgwLCBfaXNOYW4yLmRlZmF1bHQpKGluZGljYXRvclN0eWxlLndpZHRoKSkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgaW5kaWNhdG9yU3R5bGU6IGluZGljYXRvclN0eWxlIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfSwge1xuICAgIGtleTogJ3JlbmRlcicsXG4gICAgdmFsdWU6IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgIHZhciBfY2xhc3NOYW1lczMsXG4gICAgICAgICAgX3RoaXMyID0gdGhpcztcblxuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgYnV0dG9uQ2xhc3NOYW1lID0gX3Byb3BzLmJ1dHRvbkNsYXNzTmFtZSxcbiAgICAgICAgICBjZW50ZXJlZCA9IF9wcm9wcy5jZW50ZXJlZCxcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2hpbGRyZW5Qcm9wID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGZ1bGxXaWR0aCA9IF9wcm9wcy5mdWxsV2lkdGgsXG4gICAgICAgICAgaW5kaWNhdG9yQ2xhc3NOYW1lID0gX3Byb3BzLmluZGljYXRvckNsYXNzTmFtZSxcbiAgICAgICAgICBpbmRpY2F0b3JDb2xvciA9IF9wcm9wcy5pbmRpY2F0b3JDb2xvcixcbiAgICAgICAgICBvbkNoYW5nZSA9IF9wcm9wcy5vbkNoYW5nZSxcbiAgICAgICAgICBzY3JvbGxhYmxlID0gX3Byb3BzLnNjcm9sbGFibGUsXG4gICAgICAgICAgc2Nyb2xsQnV0dG9ucyA9IF9wcm9wcy5zY3JvbGxCdXR0b25zLFxuICAgICAgICAgIFRhYlNjcm9sbEJ1dHRvblByb3AgPSBfcHJvcHMuVGFiU2Nyb2xsQnV0dG9uLFxuICAgICAgICAgIHRleHRDb2xvciA9IF9wcm9wcy50ZXh0Q29sb3IsXG4gICAgICAgICAgdGhlbWUgPSBfcHJvcHMudGhlbWUsXG4gICAgICAgICAgdmFsdWUgPSBfcHJvcHMudmFsdWUsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnYnV0dG9uQ2xhc3NOYW1lJywgJ2NlbnRlcmVkJywgJ2NsYXNzZXMnLCAnY2hpbGRyZW4nLCAnY2xhc3NOYW1lJywgJ2Z1bGxXaWR0aCcsICdpbmRpY2F0b3JDbGFzc05hbWUnLCAnaW5kaWNhdG9yQ29sb3InLCAnb25DaGFuZ2UnLCAnc2Nyb2xsYWJsZScsICdzY3JvbGxCdXR0b25zJywgJ1RhYlNjcm9sbEJ1dHRvbicsICd0ZXh0Q29sb3InLCAndGhlbWUnLCAndmFsdWUnXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCBjbGFzc05hbWVQcm9wKTtcbiAgICAgIHZhciBzY3JvbGxlckNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5zY3JvbGxpbmdDb250YWluZXIsIChfY2xhc3NOYW1lczMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMzLCBjbGFzc2VzLmZpeGVkLCAhc2Nyb2xsYWJsZSksICgwLCBfZGVmaW5lUHJvcGVydHkzLmRlZmF1bHQpKF9jbGFzc05hbWVzMywgY2xhc3Nlcy5zY3JvbGxhYmxlLCBzY3JvbGxhYmxlKSwgX2NsYXNzTmFtZXMzKSk7XG4gICAgICB2YXIgdGFiSXRlbUNvbnRhaW5lckNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5mbGV4Q29udGFpbmVyLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlcy5jZW50ZXJlZCwgY2VudGVyZWQgJiYgIXNjcm9sbGFibGUpKTtcblxuICAgICAgdmFyIGluZGljYXRvciA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KF9UYWJJbmRpY2F0b3IyLmRlZmF1bHQsIHtcbiAgICAgICAgc3R5bGU6IHRoaXMuc3RhdGUuaW5kaWNhdG9yU3R5bGUsXG4gICAgICAgIGNsYXNzTmFtZTogaW5kaWNhdG9yQ2xhc3NOYW1lLFxuICAgICAgICBjb2xvcjogaW5kaWNhdG9yQ29sb3JcbiAgICAgIH0pO1xuXG4gICAgICB0aGlzLnZhbHVlVG9JbmRleCA9IHt9O1xuICAgICAgdmFyIGNoaWxkSW5kZXggPSAwO1xuICAgICAgdmFyIGNoaWxkcmVuID0gX3JlYWN0Mi5kZWZhdWx0LkNoaWxkcmVuLm1hcChjaGlsZHJlblByb3AsIGZ1bmN0aW9uIChjaGlsZCkge1xuICAgICAgICBpZiAoIV9yZWFjdDIuZGVmYXVsdC5pc1ZhbGlkRWxlbWVudChjaGlsZCkpIHtcbiAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuXG4gICAgICAgIHZhciBjaGlsZFZhbHVlID0gY2hpbGQucHJvcHMudmFsdWUgfHwgY2hpbGRJbmRleDtcbiAgICAgICAgX3RoaXMyLnZhbHVlVG9JbmRleFtjaGlsZFZhbHVlXSA9IGNoaWxkSW5kZXg7XG4gICAgICAgIHZhciBzZWxlY3RlZCA9IGNoaWxkVmFsdWUgPT09IHZhbHVlO1xuXG4gICAgICAgIGNoaWxkSW5kZXggKz0gMTtcbiAgICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jbG9uZUVsZW1lbnQoY2hpbGQsIHtcbiAgICAgICAgICBmdWxsV2lkdGg6IGZ1bGxXaWR0aCxcbiAgICAgICAgICBpbmRpY2F0b3I6IHNlbGVjdGVkICYmICFfdGhpczIuc3RhdGUubW91bnRlZCAmJiBpbmRpY2F0b3IsXG4gICAgICAgICAgc2VsZWN0ZWQ6IHNlbGVjdGVkLFxuICAgICAgICAgIG9uQ2hhbmdlOiBvbkNoYW5nZSxcbiAgICAgICAgICB0ZXh0Q29sb3I6IHRleHRDb2xvcixcbiAgICAgICAgICB2YWx1ZTogY2hpbGRWYWx1ZVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuXG4gICAgICB2YXIgY29uZGl0aW9uYWxFbGVtZW50cyA9IHRoaXMuZ2V0Q29uZGl0aW9uYWxFbGVtZW50cygpO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdkaXYnLFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHsgY2xhc3NOYW1lOiBjbGFzc05hbWUgfSwgb3RoZXIpLFxuICAgICAgICBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfcmVhY3RFdmVudExpc3RlbmVyMi5kZWZhdWx0LCB7IHRhcmdldDogJ3dpbmRvdycsIG9uUmVzaXplOiB0aGlzLmhhbmRsZVJlc2l6ZSB9KSxcbiAgICAgICAgY29uZGl0aW9uYWxFbGVtZW50cy5zY3JvbGxiYXJTaXplTGlzdGVuZXIsXG4gICAgICAgIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICdkaXYnLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmZsZXhDb250YWluZXIgfSxcbiAgICAgICAgICBjb25kaXRpb25hbEVsZW1lbnRzLnNjcm9sbEJ1dHRvbkxlZnQsXG4gICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAnZGl2JyxcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgY2xhc3NOYW1lOiBzY3JvbGxlckNsYXNzTmFtZSxcbiAgICAgICAgICAgICAgc3R5bGU6IHRoaXMuc3RhdGUuc2Nyb2xsZXJTdHlsZSxcbiAgICAgICAgICAgICAgcmVmOiBmdW5jdGlvbiByZWYobm9kZSkge1xuICAgICAgICAgICAgICAgIF90aGlzMi50YWJzID0gbm9kZTtcbiAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgcm9sZTogJ3RhYmxpc3QnLFxuICAgICAgICAgICAgICBvblNjcm9sbDogdGhpcy5oYW5kbGVUYWJzU2Nyb2xsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgICAgICdkaXYnLFxuICAgICAgICAgICAgICB7IGNsYXNzTmFtZTogdGFiSXRlbUNvbnRhaW5lckNsYXNzTmFtZSB9LFxuICAgICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICAgKSxcbiAgICAgICAgICAgIHRoaXMuc3RhdGUubW91bnRlZCAmJiBpbmRpY2F0b3JcbiAgICAgICAgICApLFxuICAgICAgICAgIGNvbmRpdGlvbmFsRWxlbWVudHMuc2Nyb2xsQnV0dG9uUmlnaHRcbiAgICAgICAgKVxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIFRhYnM7XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5UYWJzLmRlZmF1bHRQcm9wcyA9IHtcbiAgY2VudGVyZWQ6IGZhbHNlLFxuICBmdWxsV2lkdGg6IGZhbHNlLFxuICBpbmRpY2F0b3JDb2xvcjogJ2FjY2VudCcsXG4gIHNjcm9sbGFibGU6IGZhbHNlLFxuICBzY3JvbGxCdXR0b25zOiAnYXV0bycsXG4gIFRhYlNjcm9sbEJ1dHRvbjogX1RhYlNjcm9sbEJ1dHRvbjIuZGVmYXVsdCxcbiAgdGV4dENvbG9yOiAnaW5oZXJpdCdcbn07XG5leHBvcnRzLmRlZmF1bHQgPSAoMCwgX3dpdGhTdHlsZXMyLmRlZmF1bHQpKHN0eWxlcywgeyB3aXRoVGhlbWU6IHRydWUsIG5hbWU6ICdNdWlUYWJzJyB9KShUYWJzKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9UYWJzL1RhYnMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1RhYnMvVGFicy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfVGFicyA9IHJlcXVpcmUoJy4vVGFicycpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9UYWJzKS5kZWZhdWx0O1xuICB9XG59KTtcblxudmFyIF9UYWIgPSByZXF1aXJlKCcuL1RhYicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ1RhYicsIHtcbiAgZW51bWVyYWJsZTogdHJ1ZSxcbiAgZ2V0OiBmdW5jdGlvbiBnZXQoKSB7XG4gICAgcmV0dXJuIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1RhYikuZGVmYXVsdDtcbiAgfVxufSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvVGFicy9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgNCA2IDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NldFN0YXRpYyA9IHJlcXVpcmUoJy4vc2V0U3RhdGljJyk7XG5cbnZhciBfc2V0U3RhdGljMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldFN0YXRpYyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHNldERpc3BsYXlOYW1lKGRpc3BsYXlOYW1lKSB7XG4gIHJldHVybiAoMCwgX3NldFN0YXRpYzIuZGVmYXVsdCkoJ2Rpc3BsYXlOYW1lJywgZGlzcGxheU5hbWUpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiA0IDYgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCJcInVzZSBzdHJpY3RcIjtcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBzZXRTdGF0aWMgPSBmdW5jdGlvbiBzZXRTdGF0aWMoa2V5LCB2YWx1ZSkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICAvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIEJhc2VDb21wb25lbnRba2V5XSA9IHZhbHVlO1xuICAgIC8qIGVzbGludC1lbmFibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICByZXR1cm4gQmFzZUNvbXBvbmVudDtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNldFN0YXRpYztcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgNCA2IDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NoYWxsb3dFcXVhbC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMiA0IDYgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5mdW5jdGlvbiBfY2xhc3NDYWxsQ2hlY2soaW5zdGFuY2UsIENvbnN0cnVjdG9yKSB7IGlmICghKGluc3RhbmNlIGluc3RhbmNlb2YgQ29uc3RydWN0b3IpKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7IH0gfVxuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7IGlmICghc2VsZikgeyB0aHJvdyBuZXcgUmVmZXJlbmNlRXJyb3IoXCJ0aGlzIGhhc24ndCBiZWVuIGluaXRpYWxpc2VkIC0gc3VwZXIoKSBoYXNuJ3QgYmVlbiBjYWxsZWRcIik7IH0gcmV0dXJuIGNhbGwgJiYgKHR5cGVvZiBjYWxsID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpID8gY2FsbCA6IHNlbGY7IH1cblxuZnVuY3Rpb24gX2luaGVyaXRzKHN1YkNsYXNzLCBzdXBlckNsYXNzKSB7IGlmICh0eXBlb2Ygc3VwZXJDbGFzcyAhPT0gXCJmdW5jdGlvblwiICYmIHN1cGVyQ2xhc3MgIT09IG51bGwpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN1cGVyIGV4cHJlc3Npb24gbXVzdCBlaXRoZXIgYmUgbnVsbCBvciBhIGZ1bmN0aW9uLCBub3QgXCIgKyB0eXBlb2Ygc3VwZXJDbGFzcyk7IH0gc3ViQ2xhc3MucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShzdXBlckNsYXNzICYmIHN1cGVyQ2xhc3MucHJvdG90eXBlLCB7IGNvbnN0cnVjdG9yOiB7IHZhbHVlOiBzdWJDbGFzcywgZW51bWVyYWJsZTogZmFsc2UsIHdyaXRhYmxlOiB0cnVlLCBjb25maWd1cmFibGU6IHRydWUgfSB9KTsgaWYgKHN1cGVyQ2xhc3MpIE9iamVjdC5zZXRQcm90b3R5cGVPZiA/IE9iamVjdC5zZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcykgOiBzdWJDbGFzcy5fX3Byb3RvX18gPSBzdXBlckNsYXNzOyB9XG5cbnZhciBzaG91bGRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRVcGRhdGUodGVzdCkge1xuICByZXR1cm4gZnVuY3Rpb24gKEJhc2VDb21wb25lbnQpIHtcbiAgICB2YXIgZmFjdG9yeSA9ICgwLCBfcmVhY3QuY3JlYXRlRmFjdG9yeSkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaG91bGRVcGRhdGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDIgNCA2IDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNS40MSAxNi4wOWwtNC41OC00LjU5IDQuNTgtNC41OUwxNCA1LjVsLTYgNiA2IDZ6JyB9KTtcblxudmFyIEtleWJvYXJkQXJyb3dMZWZ0ID0gZnVuY3Rpb24gS2V5Ym9hcmRBcnJvd0xlZnQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5LZXlib2FyZEFycm93TGVmdCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoS2V5Ym9hcmRBcnJvd0xlZnQpO1xuS2V5Ym9hcmRBcnJvd0xlZnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gS2V5Ym9hcmRBcnJvd0xlZnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvc3ZnLWljb25zL0tleWJvYXJkQXJyb3dMZWZ0LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9zdmctaWNvbnMvS2V5Ym9hcmRBcnJvd0xlZnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnLi4vU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbi8qKlxuICogQGlnbm9yZSAtIGludGVybmFsIGNvbXBvbmVudC5cbiAqL1xudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ004LjU5IDE2LjM0bDQuNTgtNC41OS00LjU4LTQuNTlMMTAgNS43NWw2IDYtNiA2eicgfSk7XG5cbnZhciBLZXlib2FyZEFycm93UmlnaHQgPSBmdW5jdGlvbiBLZXlib2FyZEFycm93UmlnaHQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5LZXlib2FyZEFycm93UmlnaHQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEtleWJvYXJkQXJyb3dSaWdodCk7XG5LZXlib2FyZEFycm93UmlnaHQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gS2V5Ym9hcmRBcnJvd1JpZ2h0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9LZXlib2FyZEFycm93UmlnaHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL3N2Zy1pY29ucy9LZXlib2FyZEFycm93UmlnaHQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiXCJ1c2Ugc3RyaWN0XCI7XHJcbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwgeyB2YWx1ZTogdHJ1ZSB9KTtcclxuLy8gQmFzZWQgb24gaHR0cHM6Ly9naXRodWIuY29tL3JlYWN0LWJvb3RzdHJhcC9kb20taGVscGVycy9ibG9iL21hc3Rlci9zcmMvdXRpbC9pbkRPTS5qc1xyXG52YXIgaW5ET00gPSAhISh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJiB3aW5kb3cuZG9jdW1lbnQgJiYgd2luZG93LmRvY3VtZW50LmNyZWF0ZUVsZW1lbnQpO1xyXG52YXIgY2FjaGVkVHlwZTtcclxuZnVuY3Rpb24gX3NldFNjcm9sbFR5cGUodHlwZSkge1xyXG4gICAgY2FjaGVkVHlwZSA9IHR5cGU7XHJcbn1cclxuZXhwb3J0cy5fc2V0U2Nyb2xsVHlwZSA9IF9zZXRTY3JvbGxUeXBlO1xyXG4vLyBCYXNlZCBvbiB0aGUganF1ZXJ5IHBsdWdpbiBodHRwczovL2dpdGh1Yi5jb20vb3RocmVlL2pxdWVyeS5ydGwtc2Nyb2xsLXR5cGVcclxuZnVuY3Rpb24gZGV0ZWN0U2Nyb2xsVHlwZSgpIHtcclxuICAgIGlmIChjYWNoZWRUeXBlKSB7XHJcbiAgICAgICAgcmV0dXJuIGNhY2hlZFR5cGU7XHJcbiAgICB9XHJcbiAgICBpZiAoIWluRE9NIHx8ICF3aW5kb3cuZG9jdW1lbnQuYm9keSkge1xyXG4gICAgICAgIHJldHVybiAnaW5kZXRlcm1pbmF0ZSc7XHJcbiAgICB9XHJcbiAgICB2YXIgZHVtbXkgPSB3aW5kb3cuZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICBkdW1teS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSgnQUJDRCcpKTtcclxuICAgIGR1bW15LmRpciA9ICdydGwnO1xyXG4gICAgZHVtbXkuc3R5bGUuZm9udFNpemUgPSAnMTRweCc7XHJcbiAgICBkdW1teS5zdHlsZS53aWR0aCA9ICc0cHgnO1xyXG4gICAgZHVtbXkuc3R5bGUuaGVpZ2h0ID0gJzFweCc7XHJcbiAgICBkdW1teS5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XHJcbiAgICBkdW1teS5zdHlsZS50b3AgPSAnLTEwMDBweCc7XHJcbiAgICBkdW1teS5zdHlsZS5vdmVyZmxvdyA9ICdzY3JvbGwnO1xyXG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChkdW1teSk7XHJcbiAgICBjYWNoZWRUeXBlID0gJ3JldmVyc2UnO1xyXG4gICAgaWYgKGR1bW15LnNjcm9sbExlZnQgPiAwKSB7XHJcbiAgICAgICAgY2FjaGVkVHlwZSA9ICdkZWZhdWx0JztcclxuICAgIH1cclxuICAgIGVsc2Uge1xyXG4gICAgICAgIGR1bW15LnNjcm9sbExlZnQgPSAxO1xyXG4gICAgICAgIGlmIChkdW1teS5zY3JvbGxMZWZ0ID09PSAwKSB7XHJcbiAgICAgICAgICAgIGNhY2hlZFR5cGUgPSAnbmVnYXRpdmUnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQoZHVtbXkpO1xyXG4gICAgcmV0dXJuIGNhY2hlZFR5cGU7XHJcbn1cclxuZXhwb3J0cy5kZXRlY3RTY3JvbGxUeXBlID0gZGV0ZWN0U2Nyb2xsVHlwZTtcclxuLy8gQmFzZWQgb24gaHR0cHM6Ly9zdGFja292ZXJmbG93LmNvbS9hLzI0Mzk0Mzc2XHJcbmZ1bmN0aW9uIGdldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0KGVsZW1lbnQsIGRpcmVjdGlvbikge1xyXG4gICAgdmFyIHNjcm9sbExlZnQgPSBlbGVtZW50LnNjcm9sbExlZnQ7XHJcbiAgICAvLyBQZXJmb3JtIHRoZSBjYWxjdWxhdGlvbnMgb25seSB3aGVuIGRpcmVjdGlvbiBpcyBydGwgdG8gYXZvaWQgbWVzc2luZyB1cCB0aGUgbHRyIGJhaGF2aW9yXHJcbiAgICBpZiAoZGlyZWN0aW9uICE9PSAncnRsJykge1xyXG4gICAgICAgIHJldHVybiBzY3JvbGxMZWZ0O1xyXG4gICAgfVxyXG4gICAgdmFyIHR5cGUgPSBkZXRlY3RTY3JvbGxUeXBlKCk7XHJcbiAgICBpZiAodHlwZSA9PT0gJ2luZGV0ZXJtaW5hdGUnKSB7XHJcbiAgICAgICAgcmV0dXJuIE51bWJlci5OYU47XHJcbiAgICB9XHJcbiAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICBjYXNlICduZWdhdGl2ZSc6XHJcbiAgICAgICAgICAgIHJldHVybiBlbGVtZW50LnNjcm9sbFdpZHRoIC0gZWxlbWVudC5jbGllbnRXaWR0aCArIHNjcm9sbExlZnQ7XHJcbiAgICAgICAgY2FzZSAncmV2ZXJzZSc6XHJcbiAgICAgICAgICAgIHJldHVybiBlbGVtZW50LnNjcm9sbFdpZHRoIC0gZWxlbWVudC5jbGllbnRXaWR0aCAtIHNjcm9sbExlZnQ7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gc2Nyb2xsTGVmdDtcclxufVxyXG5leHBvcnRzLmdldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0ID0gZ2V0Tm9ybWFsaXplZFNjcm9sbExlZnQ7XHJcbmZ1bmN0aW9uIHNldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0KGVsZW1lbnQsIHNjcm9sbExlZnQsIGRpcmVjdGlvbikge1xyXG4gICAgLy8gUGVyZm9ybSB0aGUgY2FsY3VsYXRpb25zIG9ubHkgd2hlbiBkaXJlY3Rpb24gaXMgcnRsIHRvIGF2b2lkIG1lc3NpbmcgdXAgdGhlIGx0ciBiYWhhdmlvclxyXG4gICAgaWYgKGRpcmVjdGlvbiAhPT0gJ3J0bCcpIHtcclxuICAgICAgICBlbGVtZW50LnNjcm9sbExlZnQgPSBzY3JvbGxMZWZ0O1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHZhciB0eXBlID0gZGV0ZWN0U2Nyb2xsVHlwZSgpO1xyXG4gICAgaWYgKHR5cGUgPT09ICdpbmRldGVybWluYXRlJykge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICAgIGNhc2UgJ25lZ2F0aXZlJzpcclxuICAgICAgICAgICAgZWxlbWVudC5zY3JvbGxMZWZ0ID0gZWxlbWVudC5jbGllbnRXaWR0aCAtIGVsZW1lbnQuc2Nyb2xsV2lkdGggKyBzY3JvbGxMZWZ0O1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlICdyZXZlcnNlJzpcclxuICAgICAgICAgICAgZWxlbWVudC5zY3JvbGxMZWZ0ID0gZWxlbWVudC5zY3JvbGxXaWR0aCAtIGVsZW1lbnQuY2xpZW50V2lkdGggLSBzY3JvbGxMZWZ0O1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICBlbGVtZW50LnNjcm9sbExlZnQgPSBzY3JvbGxMZWZ0O1xyXG4gICAgICAgICAgICBicmVhaztcclxuICAgIH1cclxufVxyXG5leHBvcnRzLnNldE5vcm1hbGl6ZWRTY3JvbGxMZWZ0ID0gc2V0Tm9ybWFsaXplZFNjcm9sbExlZnQ7XHJcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL25vcm1hbGl6ZS1zY3JvbGwtbGVmdC9saWIvbWFpbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbm9ybWFsaXplLXNjcm9sbC1sZWZ0L2xpYi9tYWluLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsInZhciBnbG9iYWwgPSByZXF1aXJlKCdnbG9iYWwnKVxuXG4vKipcbiAqIGByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoKWBcbiAqL1xuXG52YXIgcmVxdWVzdCA9IGdsb2JhbC5yZXF1ZXN0QW5pbWF0aW9uRnJhbWVcbiAgfHwgZ2xvYmFsLndlYmtpdFJlcXVlc3RBbmltYXRpb25GcmFtZVxuICB8fCBnbG9iYWwubW96UmVxdWVzdEFuaW1hdGlvbkZyYW1lXG4gIHx8IGZhbGxiYWNrXG5cbnZhciBwcmV2ID0gK25ldyBEYXRlXG5mdW5jdGlvbiBmYWxsYmFjayAoZm4pIHtcbiAgdmFyIGN1cnIgPSArbmV3IERhdGVcbiAgdmFyIG1zID0gTWF0aC5tYXgoMCwgMTYgLSAoY3VyciAtIHByZXYpKVxuICB2YXIgcmVxID0gc2V0VGltZW91dChmbiwgbXMpXG4gIHJldHVybiBwcmV2ID0gY3VyciwgcmVxXG59XG5cbi8qKlxuICogYGNhbmNlbEFuaW1hdGlvbkZyYW1lKClgXG4gKi9cblxudmFyIGNhbmNlbCA9IGdsb2JhbC5jYW5jZWxBbmltYXRpb25GcmFtZVxuICB8fCBnbG9iYWwud2Via2l0Q2FuY2VsQW5pbWF0aW9uRnJhbWVcbiAgfHwgZ2xvYmFsLm1vekNhbmNlbEFuaW1hdGlvbkZyYW1lXG4gIHx8IGNsZWFyVGltZW91dFxuXG5pZiAoRnVuY3Rpb24ucHJvdG90eXBlLmJpbmQpIHtcbiAgcmVxdWVzdCA9IHJlcXVlc3QuYmluZChnbG9iYWwpXG4gIGNhbmNlbCA9IGNhbmNlbC5iaW5kKGdsb2JhbClcbn1cblxuZXhwb3J0cyA9IG1vZHVsZS5leHBvcnRzID0gcmVxdWVzdFxuZXhwb3J0cy5jYW5jZWwgPSBjYW5jZWxcblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JhZmwvaW5kZXguanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JhZmwvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcblx0dmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3Byb3BUeXBlcyA9IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKTtcblxudmFyIF9wcm9wVHlwZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHJvcFR5cGVzKTtcblxudmFyIF9yZWFjdEV2ZW50TGlzdGVuZXIgPSByZXF1aXJlKCdyZWFjdC1ldmVudC1saXN0ZW5lcicpO1xuXG52YXIgX3JlYWN0RXZlbnRMaXN0ZW5lcjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdEV2ZW50TGlzdGVuZXIpO1xuXG52YXIgX3N0aWZsZSA9IHJlcXVpcmUoJ3N0aWZsZScpO1xuXG52YXIgX3N0aWZsZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zdGlmbGUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgc3R5bGVzID0ge1xuXHR3aWR0aDogJzEwMHB4Jyxcblx0aGVpZ2h0OiAnMTAwcHgnLFxuXHRwb3NpdGlvbjogJ2Fic29sdXRlJyxcblx0dG9wOiAnLTEwMDAwMHB4Jyxcblx0b3ZlcmZsb3c6ICdzY3JvbGwnLFxuXHRtc092ZXJmbG93U3R5bGU6ICdzY3JvbGxiYXInXG59O1xuXG52YXIgU2Nyb2xsYmFyU2l6ZSA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG5cdCgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFNjcm9sbGJhclNpemUsIF9Db21wb25lbnQpO1xuXG5cdGZ1bmN0aW9uIFNjcm9sbGJhclNpemUoKSB7XG5cdFx0dmFyIF9yZWY7XG5cblx0XHR2YXIgX3RlbXAsIF90aGlzLCBfcmV0O1xuXG5cdFx0KDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgU2Nyb2xsYmFyU2l6ZSk7XG5cblx0XHRmb3IgKHZhciBfbGVuID0gYXJndW1lbnRzLmxlbmd0aCwgYXJncyA9IEFycmF5KF9sZW4pLCBfa2V5ID0gMDsgX2tleSA8IF9sZW47IF9rZXkrKykge1xuXHRcdFx0YXJnc1tfa2V5XSA9IGFyZ3VtZW50c1tfa2V5XTtcblx0XHR9XG5cblx0XHRyZXR1cm4gX3JldCA9IChfdGVtcCA9IChfdGhpcyA9ICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKF9yZWYgPSBTY3JvbGxiYXJTaXplLl9fcHJvdG9fXyB8fCAoMCwgX2dldFByb3RvdHlwZU9mMi5kZWZhdWx0KShTY3JvbGxiYXJTaXplKSkuY2FsbC5hcHBseShfcmVmLCBbdGhpc10uY29uY2F0KGFyZ3MpKSksIF90aGlzKSwgX3RoaXMuc2V0TWVhc3VyZW1lbnRzID0gZnVuY3Rpb24gKCkge1xuXHRcdFx0X3RoaXMuc2Nyb2xsYmFySGVpZ2h0ID0gX3RoaXMubm9kZS5vZmZzZXRIZWlnaHQgLSBfdGhpcy5ub2RlLmNsaWVudEhlaWdodDtcblx0XHRcdF90aGlzLnNjcm9sbGJhcldpZHRoID0gX3RoaXMubm9kZS5vZmZzZXRXaWR0aCAtIF90aGlzLm5vZGUuY2xpZW50V2lkdGg7XG5cdFx0fSwgX3RoaXMuaGFuZGxlUmVzaXplID0gKDAsIF9zdGlmbGUyLmRlZmF1bHQpKGZ1bmN0aW9uICgpIHtcblx0XHRcdHZhciBvbkNoYW5nZSA9IF90aGlzLnByb3BzLm9uQ2hhbmdlO1xuXG5cblx0XHRcdHZhciBwcmV2SGVpZ2h0ID0gX3RoaXMuc2Nyb2xsYmFySGVpZ2h0O1xuXHRcdFx0dmFyIHByZXZXaWR0aCA9IF90aGlzLnNjcm9sbGJhcldpZHRoO1xuXHRcdFx0X3RoaXMuc2V0TWVhc3VyZW1lbnRzKCk7XG5cdFx0XHRpZiAocHJldkhlaWdodCAhPT0gX3RoaXMuc2Nyb2xsYmFySGVpZ2h0IHx8IHByZXZXaWR0aCAhPT0gX3RoaXMuc2Nyb2xsYmFyV2lkdGgpIHtcblx0XHRcdFx0b25DaGFuZ2UoeyBzY3JvbGxiYXJIZWlnaHQ6IF90aGlzLnNjcm9sbGJhckhlaWdodCwgc2Nyb2xsYmFyV2lkdGg6IF90aGlzLnNjcm9sbGJhcldpZHRoIH0pO1xuXHRcdFx0fVxuXHRcdH0sIDE2NiksIF90ZW1wKSwgKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KShfdGhpcywgX3JldCk7XG5cdH1cblxuXHQoMCwgX2NyZWF0ZUNsYXNzMy5kZWZhdWx0KShTY3JvbGxiYXJTaXplLCBbe1xuXHRcdGtleTogJ2NvbXBvbmVudERpZE1vdW50Jyxcblx0XHR2YWx1ZTogZnVuY3Rpb24gY29tcG9uZW50RGlkTW91bnQoKSB7XG5cdFx0XHR2YXIgb25Mb2FkID0gdGhpcy5wcm9wcy5vbkxvYWQ7XG5cblxuXHRcdFx0aWYgKG9uTG9hZCkge1xuXHRcdFx0XHR0aGlzLnNldE1lYXN1cmVtZW50cygpO1xuXHRcdFx0XHRvbkxvYWQoeyBzY3JvbGxiYXJIZWlnaHQ6IHRoaXMuc2Nyb2xsYmFySGVpZ2h0LCBzY3JvbGxiYXJXaWR0aDogdGhpcy5zY3JvbGxiYXJXaWR0aCB9KTtcblx0XHRcdH1cblx0XHR9XG5cdH0sIHtcblx0XHRrZXk6ICdjb21wb25lbnRXaWxsVW5tb3VudCcsXG5cdFx0dmFsdWU6IGZ1bmN0aW9uIGNvbXBvbmVudFdpbGxVbm1vdW50KCkge1xuXHRcdFx0dGhpcy5oYW5kbGVSZXNpemUuY2FuY2VsKCk7XG5cdFx0fVxuXHR9LCB7XG5cdFx0a2V5OiAncmVuZGVyJyxcblx0XHQvLyBDb3JyZXNwb25kcyB0byAxMCBmcmFtZXMgYXQgNjAgSHouXG5cblx0XHR2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuXHRcdFx0dmFyIF90aGlzMiA9IHRoaXM7XG5cblx0XHRcdHZhciBvbkNoYW5nZSA9IHRoaXMucHJvcHMub25DaGFuZ2U7XG5cblxuXHRcdFx0cmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuXHRcdFx0XHQnZGl2Jyxcblx0XHRcdFx0bnVsbCxcblx0XHRcdFx0b25DaGFuZ2UgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChfcmVhY3RFdmVudExpc3RlbmVyMi5kZWZhdWx0LCB7IHRhcmdldDogJ3dpbmRvdycsIG9uUmVzaXplOiB0aGlzLmhhbmRsZVJlc2l6ZSB9KSA6IG51bGwsXG5cdFx0XHRcdF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdkaXYnLCB7XG5cdFx0XHRcdFx0c3R5bGU6IHN0eWxlcyxcblx0XHRcdFx0XHRyZWY6IGZ1bmN0aW9uIHJlZihub2RlKSB7XG5cdFx0XHRcdFx0XHRfdGhpczIubm9kZSA9IG5vZGU7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KVxuXHRcdFx0KTtcblx0XHR9XG5cdH1dKTtcblx0cmV0dXJuIFNjcm9sbGJhclNpemU7XG59KF9yZWFjdC5Db21wb25lbnQpO1xuXG5TY3JvbGxiYXJTaXplLmRlZmF1bHRQcm9wcyA9IHtcblx0b25Mb2FkOiBudWxsLFxuXHRvbkNoYW5nZTogbnVsbFxufTtcbmV4cG9ydHMuZGVmYXVsdCA9IFNjcm9sbGJhclNpemU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2Nyb2xsYmFyLXNpemUvU2Nyb2xsYmFyU2l6ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVhY3Qtc2Nyb2xsYmFyLXNpemUvU2Nyb2xsYmFyU2l6ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfU2Nyb2xsYmFyU2l6ZSA9IHJlcXVpcmUoJy4vU2Nyb2xsYmFyU2l6ZScpO1xuXG52YXIgX1Njcm9sbGJhclNpemUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU2Nyb2xsYmFyU2l6ZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmV4cG9ydHMuZGVmYXVsdCA9IF9TY3JvbGxiYXJTaXplMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlYWN0LXNjcm9sbGJhci1zaXplL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWFjdC1zY3JvbGxiYXItc2l6ZS9pbmRleC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgNyA4IDkgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbCA9IHJlcXVpcmUoJy4vdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbCcpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJFbGVtZW50VXRpbCk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCA9IHJlcXVpcmUoJy4vaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCcpO1xuXG52YXIgX2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBjcmVhdGVGYWN0b3J5ID0gZnVuY3Rpb24gY3JlYXRlRmFjdG9yeSh0eXBlKSB7XG4gIHZhciBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCA9ICgwLCBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIuZGVmYXVsdCkodHlwZSk7XG4gIHJldHVybiBmdW5jdGlvbiAocCwgYykge1xuICAgIHJldHVybiAoMCwgX2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwyLmRlZmF1bHQpKGZhbHNlLCBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCwgdHlwZSwgcCwgYyk7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBjcmVhdGVGYWN0b3J5O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBnZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIGdldERpc3BsYXlOYW1lKENvbXBvbmVudCkge1xuICBpZiAodHlwZW9mIENvbXBvbmVudCA9PT0gJ3N0cmluZycpIHtcbiAgICByZXR1cm4gQ29tcG9uZW50O1xuICB9XG5cbiAgaWYgKCFDb21wb25lbnQpIHtcbiAgICByZXR1cm4gdW5kZWZpbmVkO1xuICB9XG5cbiAgcmV0dXJuIENvbXBvbmVudC5kaXNwbGF5TmFtZSB8fCBDb21wb25lbnQubmFtZSB8fCAnQ29tcG9uZW50Jztcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGdldERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfdHlwZW9mID0gdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIgPyBmdW5jdGlvbiAob2JqKSB7IHJldHVybiB0eXBlb2Ygb2JqOyB9IDogZnVuY3Rpb24gKG9iaikgeyByZXR1cm4gb2JqICYmIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiBvYmouY29uc3RydWN0b3IgPT09IFN5bWJvbCAmJiBvYmogIT09IFN5bWJvbC5wcm90b3R5cGUgPyBcInN5bWJvbFwiIDogdHlwZW9mIG9iajsgfTtcblxudmFyIGlzQ2xhc3NDb21wb25lbnQgPSBmdW5jdGlvbiBpc0NsYXNzQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbihDb21wb25lbnQgJiYgQ29tcG9uZW50LnByb3RvdHlwZSAmJiBfdHlwZW9mKENvbXBvbmVudC5wcm90b3R5cGUuaXNSZWFjdENvbXBvbmVudCkgPT09ICdvYmplY3QnKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzQ2xhc3NDb21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc0NsYXNzQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudCA9IHJlcXVpcmUoJy4vaXNDbGFzc0NvbXBvbmVudCcpO1xuXG52YXIgX2lzQ2xhc3NDb21wb25lbnQyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaXNDbGFzc0NvbXBvbmVudCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gZnVuY3Rpb24gaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudChDb21wb25lbnQpIHtcbiAgcmV0dXJuIEJvb2xlYW4odHlwZW9mIENvbXBvbmVudCA9PT0gJ2Z1bmN0aW9uJyAmJiAhKDAsIF9pc0NsYXNzQ29tcG9uZW50Mi5kZWZhdWx0KShDb21wb25lbnQpICYmICFDb21wb25lbnQuZGVmYXVsdFByb3BzICYmICFDb21wb25lbnQuY29udGV4dFR5cGVzICYmIChwcm9jZXNzLmVudi5OT0RFX0VOViA9PT0gJ3Byb2R1Y3Rpb24nIHx8ICFDb21wb25lbnQucHJvcFR5cGVzKSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3Nob3VsZFVwZGF0ZSA9IHJlcXVpcmUoJy4vc2hvdWxkVXBkYXRlJyk7XG5cbnZhciBfc2hvdWxkVXBkYXRlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Nob3VsZFVwZGF0ZSk7XG5cbnZhciBfc2hhbGxvd0VxdWFsID0gcmVxdWlyZSgnLi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vc2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9zZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zZXREaXNwbGF5TmFtZSk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi93cmFwRGlzcGxheU5hbWUnKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd3JhcERpc3BsYXlOYW1lKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHB1cmUgPSBmdW5jdGlvbiBwdXJlKEJhc2VDb21wb25lbnQpIHtcbiAgdmFyIGhvYyA9ICgwLCBfc2hvdWxkVXBkYXRlMi5kZWZhdWx0KShmdW5jdGlvbiAocHJvcHMsIG5leHRQcm9wcykge1xuICAgIHJldHVybiAhKDAsIF9zaGFsbG93RXF1YWwyLmRlZmF1bHQpKHByb3BzLCBuZXh0UHJvcHMpO1xuICB9KTtcblxuICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgIHJldHVybiAoMCwgX3NldERpc3BsYXlOYW1lMi5kZWZhdWx0KSgoMCwgX3dyYXBEaXNwbGF5TmFtZTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCwgJ3B1cmUnKSkoaG9jKEJhc2VDb21wb25lbnQpKTtcbiAgfVxuXG4gIHJldHVybiBob2MoQmFzZUNvbXBvbmVudCk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBwdXJlO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NldFN0YXRpYyA9IHJlcXVpcmUoJy4vc2V0U3RhdGljJyk7XG5cbnZhciBfc2V0U3RhdGljMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldFN0YXRpYyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBzZXREaXNwbGF5TmFtZSA9IGZ1bmN0aW9uIHNldERpc3BsYXlOYW1lKGRpc3BsYXlOYW1lKSB7XG4gIHJldHVybiAoMCwgX3NldFN0YXRpYzIuZGVmYXVsdCkoJ2Rpc3BsYXlOYW1lJywgZGlzcGxheU5hbWUpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIlwidXNlIHN0cmljdFwiO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIHNldFN0YXRpYyA9IGZ1bmN0aW9uIHNldFN0YXRpYyhrZXksIHZhbHVlKSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIC8qIGVzbGludC1kaXNhYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgQmFzZUNvbXBvbmVudFtrZXldID0gdmFsdWU7XG4gICAgLyogZXNsaW50LWVuYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuICAgIHJldHVybiBCYXNlQ29tcG9uZW50O1xuICB9O1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gc2V0U3RhdGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXRTdGF0aWMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCdmYmpzL2xpYi9zaGFsbG93RXF1YWwnKTtcblxudmFyIF9zaGFsbG93RXF1YWwyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hhbGxvd0VxdWFsKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZXhwb3J0cy5kZWZhdWx0ID0gX3NoYWxsb3dFcXVhbDIuZGVmYXVsdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG52YXIgX2NyZWF0ZUVhZ2VyRmFjdG9yeSA9IHJlcXVpcmUoJy4vY3JlYXRlRWFnZXJGYWN0b3J5Jyk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUVhZ2VyRmFjdG9yeSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbmZ1bmN0aW9uIF9jbGFzc0NhbGxDaGVjayhpbnN0YW5jZSwgQ29uc3RydWN0b3IpIHsgaWYgKCEoaW5zdGFuY2UgaW5zdGFuY2VvZiBDb25zdHJ1Y3RvcikpIHsgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkNhbm5vdCBjYWxsIGEgY2xhc3MgYXMgYSBmdW5jdGlvblwiKTsgfSB9XG5cbmZ1bmN0aW9uIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuKHNlbGYsIGNhbGwpIHsgaWYgKCFzZWxmKSB7IHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTsgfSByZXR1cm4gY2FsbCAmJiAodHlwZW9mIGNhbGwgPT09IFwib2JqZWN0XCIgfHwgdHlwZW9mIGNhbGwgPT09IFwiZnVuY3Rpb25cIikgPyBjYWxsIDogc2VsZjsgfVxuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHsgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIiArIHR5cGVvZiBzdXBlckNsYXNzKTsgfSBzdWJDbGFzcy5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKHN1cGVyQ2xhc3MgJiYgc3VwZXJDbGFzcy5wcm90b3R5cGUsIHsgY29uc3RydWN0b3I6IHsgdmFsdWU6IHN1YkNsYXNzLCBlbnVtZXJhYmxlOiBmYWxzZSwgd3JpdGFibGU6IHRydWUsIGNvbmZpZ3VyYWJsZTogdHJ1ZSB9IH0pOyBpZiAoc3VwZXJDbGFzcykgT2JqZWN0LnNldFByb3RvdHlwZU9mID8gT2JqZWN0LnNldFByb3RvdHlwZU9mKHN1YkNsYXNzLCBzdXBlckNsYXNzKSA6IHN1YkNsYXNzLl9fcHJvdG9fXyA9IHN1cGVyQ2xhc3M7IH1cblxudmFyIHNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZFVwZGF0ZSh0ZXN0KSB7XG4gIHJldHVybiBmdW5jdGlvbiAoQmFzZUNvbXBvbmVudCkge1xuICAgIHZhciBmYWN0b3J5ID0gKDAsIF9jcmVhdGVFYWdlckZhY3RvcnkyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpO1xuXG4gICAgdmFyIFNob3VsZFVwZGF0ZSA9IGZ1bmN0aW9uIChfQ29tcG9uZW50KSB7XG4gICAgICBfaW5oZXJpdHMoU2hvdWxkVXBkYXRlLCBfQ29tcG9uZW50KTtcblxuICAgICAgZnVuY3Rpb24gU2hvdWxkVXBkYXRlKCkge1xuICAgICAgICBfY2xhc3NDYWxsQ2hlY2sodGhpcywgU2hvdWxkVXBkYXRlKTtcblxuICAgICAgICByZXR1cm4gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4odGhpcywgX0NvbXBvbmVudC5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgICAgIH1cblxuICAgICAgU2hvdWxkVXBkYXRlLnByb3RvdHlwZS5zaG91bGRDb21wb25lbnRVcGRhdGUgPSBmdW5jdGlvbiBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFByb3BzKSB7XG4gICAgICAgIHJldHVybiB0ZXN0KHRoaXMucHJvcHMsIG5leHRQcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcigpIHtcbiAgICAgICAgcmV0dXJuIGZhY3RvcnkodGhpcy5wcm9wcyk7XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gU2hvdWxkVXBkYXRlO1xuICAgIH0oX3JlYWN0LkNvbXBvbmVudCk7XG5cbiAgICBpZiAocHJvY2Vzcy5lbnYuTk9ERV9FTlYgIT09ICdwcm9kdWN0aW9uJykge1xuICAgICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAnc2hvdWxkVXBkYXRlJykpKFNob3VsZFVwZGF0ZSk7XG4gICAgfVxuICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzaG91bGRVcGRhdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3Nob3VsZFVwZGF0ZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX2V4dGVuZHMgPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uICh0YXJnZXQpIHsgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIHsgdmFyIHNvdXJjZSA9IGFyZ3VtZW50c1tpXTsgZm9yICh2YXIga2V5IGluIHNvdXJjZSkgeyBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHNvdXJjZSwga2V5KSkgeyB0YXJnZXRba2V5XSA9IHNvdXJjZVtrZXldOyB9IH0gfSByZXR1cm4gdGFyZ2V0OyB9O1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBjcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gZnVuY3Rpb24gY3JlYXRlRWFnZXJFbGVtZW50VXRpbChoYXNLZXksIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwcm9wcywgY2hpbGRyZW4pIHtcbiAgaWYgKCFoYXNLZXkgJiYgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQpIHtcbiAgICBpZiAoY2hpbGRyZW4pIHtcbiAgICAgIHJldHVybiB0eXBlKF9leHRlbmRzKHt9LCBwcm9wcywgeyBjaGlsZHJlbjogY2hpbGRyZW4gfSkpO1xuICAgIH1cbiAgICByZXR1cm4gdHlwZShwcm9wcyk7XG4gIH1cblxuICB2YXIgQ29tcG9uZW50ID0gdHlwZTtcblxuICBpZiAoY2hpbGRyZW4pIHtcbiAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICBDb21wb25lbnQsXG4gICAgICBwcm9wcyxcbiAgICAgIGNoaWxkcmVuXG4gICAgKTtcbiAgfVxuXG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChDb21wb25lbnQsIHByb3BzKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWw7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZ2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL2dldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfZ2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0RGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgd3JhcERpc3BsYXlOYW1lID0gZnVuY3Rpb24gd3JhcERpc3BsYXlOYW1lKEJhc2VDb21wb25lbnQsIGhvY05hbWUpIHtcbiAgcmV0dXJuIGhvY05hbWUgKyAnKCcgKyAoMCwgX2dldERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50KSArICcpJztcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHdyYXBEaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvd3JhcERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCJ2YXIgcmFmID0gcmVxdWlyZSgncmFmbCcpXG5cbmZ1bmN0aW9uIHNjcm9sbCAocHJvcCwgZWxlbWVudCwgdG8sIG9wdGlvbnMsIGNhbGxiYWNrKSB7XG4gIHZhciBzdGFydCA9ICtuZXcgRGF0ZVxuICB2YXIgZnJvbSA9IGVsZW1lbnRbcHJvcF1cbiAgdmFyIGNhbmNlbGxlZCA9IGZhbHNlXG5cbiAgdmFyIGVhc2UgPSBpbk91dFNpbmVcbiAgdmFyIGR1cmF0aW9uID0gMzUwXG5cbiAgaWYgKHR5cGVvZiBvcHRpb25zID09PSAnZnVuY3Rpb24nKSB7XG4gICAgY2FsbGJhY2sgPSBvcHRpb25zXG4gIH1cbiAgZWxzZSB7XG4gICAgb3B0aW9ucyA9IG9wdGlvbnMgfHwge31cbiAgICBlYXNlID0gb3B0aW9ucy5lYXNlIHx8IGVhc2VcbiAgICBkdXJhdGlvbiA9IG9wdGlvbnMuZHVyYXRpb24gfHwgZHVyYXRpb25cbiAgICBjYWxsYmFjayA9IGNhbGxiYWNrIHx8IGZ1bmN0aW9uICgpIHt9XG4gIH1cblxuICBpZiAoZnJvbSA9PT0gdG8pIHtcbiAgICByZXR1cm4gY2FsbGJhY2soXG4gICAgICBuZXcgRXJyb3IoJ0VsZW1lbnQgYWxyZWFkeSBhdCB0YXJnZXQgc2Nyb2xsIHBvc2l0aW9uJyksXG4gICAgICBlbGVtZW50W3Byb3BdXG4gICAgKVxuICB9XG5cbiAgZnVuY3Rpb24gY2FuY2VsICgpIHtcbiAgICBjYW5jZWxsZWQgPSB0cnVlXG4gIH1cblxuICBmdW5jdGlvbiBhbmltYXRlICh0aW1lc3RhbXApIHtcbiAgICBpZiAoY2FuY2VsbGVkKSB7XG4gICAgICByZXR1cm4gY2FsbGJhY2soXG4gICAgICAgIG5ldyBFcnJvcignU2Nyb2xsIGNhbmNlbGxlZCcpLFxuICAgICAgICBlbGVtZW50W3Byb3BdXG4gICAgICApXG4gICAgfVxuXG4gICAgdmFyIG5vdyA9ICtuZXcgRGF0ZVxuICAgIHZhciB0aW1lID0gTWF0aC5taW4oMSwgKChub3cgLSBzdGFydCkgLyBkdXJhdGlvbikpXG4gICAgdmFyIGVhc2VkID0gZWFzZSh0aW1lKVxuXG4gICAgZWxlbWVudFtwcm9wXSA9IChlYXNlZCAqICh0byAtIGZyb20pKSArIGZyb21cblxuICAgIHRpbWUgPCAxID8gcmFmKGFuaW1hdGUpIDogcmFmKGZ1bmN0aW9uICgpIHtcbiAgICAgIGNhbGxiYWNrKG51bGwsIGVsZW1lbnRbcHJvcF0pXG4gICAgfSlcbiAgfVxuXG4gIHJhZihhbmltYXRlKVxuXG4gIHJldHVybiBjYW5jZWxcbn1cblxuZnVuY3Rpb24gaW5PdXRTaW5lIChuKSB7XG4gIHJldHVybiAuNSAqICgxIC0gTWF0aC5jb3MoTWF0aC5QSSAqIG4pKVxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgdG9wOiBmdW5jdGlvbiAoZWxlbWVudCwgdG8sIG9wdGlvbnMsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIHNjcm9sbCgnc2Nyb2xsVG9wJywgZWxlbWVudCwgdG8sIG9wdGlvbnMsIGNhbGxiYWNrKVxuICB9LFxuICBsZWZ0OiBmdW5jdGlvbiAoZWxlbWVudCwgdG8sIG9wdGlvbnMsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIHNjcm9sbCgnc2Nyb2xsTGVmdCcsIGVsZW1lbnQsIHRvLCBvcHRpb25zLCBjYWxsYmFjaylcbiAgfVxufVxuXG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvc2Nyb2xsL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9zY3JvbGwvaW5kZXguanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDcgOCA5IDM5IDQ4IDUwIDUxIDUyIDUzIiwibW9kdWxlLmV4cG9ydHMgPSBzdGlmbGU7XG5cblxuZnVuY3Rpb24gc3RpZmxlIChmbiwgd2FpdCkge1xuICBpZiAodHlwZW9mIGZuICE9PSAnZnVuY3Rpb24nIHx8IHR5cGVvZiB3YWl0ICE9PSAnbnVtYmVyJykge1xuICAgIHRocm93IG5ldyBFcnJvcignc3RpZmxlKGZuLCB3YWl0KSAtLSBleHBlY3RlZCBhIGZ1bmN0aW9uIGFuZCBudW1iZXIgb2YgbWlsbGlzZWNvbmRzLCBnb3QgKCcgKyB0eXBlb2YgZm4gKyAnLCAnICsgdHlwZW9mIHdhaXQgKyAnKScpO1xuICB9XG5cbiAgdmFyIHRpbWVyOyAgICAvLyBUaW1lciB0byBmaXJlIGFmdGVyIGB3YWl0YCBoYXMgZWxhcHNlZFxuICB2YXIgY2FsbGVkOyAgIC8vIEtlZXAgdHJhY2sgaWYgaXQgZ2V0cyBjYWxsZWQgZHVyaW5nIHRoZSBgd2FpdGBcblxuICB2YXIgd3JhcHBlciA9IGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIENoZWNrIGlmIHN0aWxsIFwiY29vbGluZyBkb3duXCIgZnJvbSBhIHByZXZpb3VzIGNhbGxcbiAgICBpZiAodGltZXIpIHtcbiAgICAgIGNhbGxlZCA9IHRydWU7XG4gICAgfSBlbHNlIHtcbiAgICAgIC8vIFN0YXJ0IGEgdGltZXIgdG8gZmlyZSBhZnRlciB0aGUgYHdhaXRgIGlzIG92ZXJcbiAgICAgIHRpbWVyID0gc2V0VGltZW91dChhZnRlcldhaXQsIHdhaXQpO1xuICAgICAgLy8gQW5kIGNhbGwgdGhlIHdyYXBwZWQgZnVuY3Rpb25cbiAgICAgIGZuKCk7XG4gICAgfVxuICB9XG5cbiAgLy8gQWRkIGEgY2FuY2VsIG1ldGhvZCwgdG8ga2lsbCBhbmQgcGVuZGluZyBjYWxsc1xuICB3cmFwcGVyLmNhbmNlbCA9IGZ1bmN0aW9uICgpIHtcbiAgICAvLyBDbGVhciB0aGUgY2FsbGVkIGZsYWcsIG9yIGl0IHdvdWxkIGZpcmUgdHdpY2Ugd2hlbiBjYWxsZWQgYWdhaW4gbGF0ZXJcbiAgICBjYWxsZWQgPSBmYWxzZTtcblxuICAgIC8vIFR1cm4gb2ZmIHRoZSB0aW1lciwgc28gaXQgd29uJ3QgZmlyZSBhZnRlciB0aGUgd2FpdCBleHBpcmVzXG4gICAgaWYgKHRpbWVyKSB7XG4gICAgICBjbGVhclRpbWVvdXQodGltZXIpO1xuICAgICAgdGltZXIgPSB1bmRlZmluZWQ7XG4gICAgfVxuICB9XG5cbiAgZnVuY3Rpb24gYWZ0ZXJXYWl0KCkge1xuICAgIC8vIEVtcHR5IG91dCB0aGUgdGltZXJcbiAgICB0aW1lciA9IHVuZGVmaW5lZDtcblxuICAgIC8vIElmIGl0IHdhcyBjYWxsZWQgZHVyaW5nIHRoZSBgd2FpdGAsIGZpcmUgaXQgYWdhaW5cbiAgICBpZiAoY2FsbGVkKSB7XG4gICAgICBjYWxsZWQgPSBmYWxzZTtcbiAgICAgIHdyYXBwZXIoKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gd3JhcHBlcjtcbn1cblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3N0aWZsZS9pbmRleC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvc3RpZmxlL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCA3IDggOSAzOSA0OCA1MCA1MSA1MiA1MyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBDT1VSU0VTX0dFVCB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBDb3Vyc2UgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG4gIGRlc2NyaXB0aW9uPzogc3RyaW5nLFxuICBzeWxsYWJ1cz86IG1peGVkLFxuICBjb3Vyc2VNb2R1bGVzPzogQXJyYXk8e1xuICAgIG1vZHVsZUlkOiBudW1iZXIsXG4gIH0+LFxuICB0aW1lc3RhbXA6IHN0cmluZyxcbn07XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBjb3Vyc2VzOiBBcnJheTxDb3Vyc2U+LFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICogVGhpcyBhY3Rpb24gd2lsbCB1cGRhdGUgY291cnNlcyByZWR1Y2VyXG4gKiBAZnVuY3Rpb24gZ2V0QWxsXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuc3RhdHVzQ29kZSAtXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkLmNvdXJzZXNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmNvdXJzZXNbXVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlc1tdLmNvdXJzZUlkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VzW10uYXV0aG9ySWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNvdXJzZXNbXS50aXRsZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY291cnNlc1tdLmRlc2NyaXB0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5jb3Vyc2VzW10uc3lsbGFidXNcbiAqIEBwYXJhbSB7T2JqZWN0W119IHBheWxvYWQuY291cnNlc1tdLmNvdXJzZU1vZHVsZXNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmNvdXJzZXNbXS5jb3Vyc2VNb2R1bGVzW11cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZXNbXS5jb3Vyc2VNb2R1bGVzW10ubW9kdWxlSWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNvdXJzZXNbXS50aW1lc3RhbXBcbiAqXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICovXG5leHBvcnQgY29uc3QgZ2V0QWxsID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHtcbiAgdHlwZTogQ09VUlNFU19HRVQsXG4gIHBheWxvYWQsXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgZ2V0QWxsO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvZ2V0QWxsLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCB7IFBPU1RTX0dFVCB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9wb3N0cyc7XG5cbnR5cGUgUG9zdCA9IHtcbiAgcG9zdElkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG4gIGRlc2NyaXB0aW9uPzogc3RyaW5nLFxuICBkYXRhPzogYW55LFxuICB0aW1lc3RhbXA6IHN0cmluZyxcbn07XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBpZDogbnVtYmVyLFxuICBwb3N0czogQXJyYXk8UG9zdD4sXG59O1xuXG50eXBlIEFjdGlvbiA9IHtcbiAgdHlwZTogc3RyaW5nLFxuICBwYXlsb2FkOiBQYXlsb2FkLFxufTtcblxuLyoqXG4gKiBUaGlzIGFjdGlvbiB3aWxsIHVwZGF0ZSAncG9zdHMnIHN0b3JlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuc3RhdHVzQ29kZVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWQgLSBlbGVtZW50IGlkIG9mIHdoaWNoIGFsbCB0aGVzZSBwb3N0cyBiZWxvbmdcbiAqIEBwYXJhbSB7T2JqZWN0W119IHBheWxvYWQucG9zdHNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLnBvc3RzW11cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnBvc3RzW10ucG9zdElkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5wb3N0c1tdLmF1dGhvcklkXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5wb3N0c1tdLnRpdGxlXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5wb3N0c1tdLmRlc2NyaXB0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5wb3N0c1tdLmRhdGFcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnBvc3RzW10udGltZXN0YW1wXG4gKlxuICogQHJldHVybnMge09iamVjdH1cbiAqL1xuY29uc3QgZ2V0QWxsID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHsgdHlwZTogUE9TVFNfR0VULCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvcG9zdHMvZ2V0QWxsLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBVU0VSX1VQREFURSBmcm9tICcuLi8uLi9jb25zdGFudHMvdXNlcnMvdXBkYXRlJztcblxudHlwZSBQYXlsb2FkID0ge1xuICBpZDogbnVtYmVyLFxuICB1c2VybmFtZT86IHN0cmluZyxcbiAgbmFtZT86IHN0cmluZyxcbiAgYXZhdGFyPzogc3RyaW5nLFxuICBjb3Zlcj86IHN0cmluZyxcbiAgcmVzdW1lPzogYW55LFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICogVXNlciB1cGRhdGUgYWN0aW9uLlxuICogVXNlciBjYW4gdXBkYXRlIHRoZWlyIGluZm8gaW4gcmVkdXggc3RvcmUgYnkgaXQncyAnaWQnIG9ubHkuXG4gKiBAZnVuY3Rpb24gdXBkYXRlXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZCAtXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5pZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC51c2VybmFtZSAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5uYW1lIC1cbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmF2YXRhciAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC5jb3ZlciAtXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5yZXN1bWUgLVxuICpcbiAqIEByZXR1cm5zIHtPYmplY3R9XG4gKi9cbmNvbnN0IHVwZGF0ZSA9IChwYXlsb2FkOiBQYXlsb2FkKTogQWN0aW9uID0+ICh7IHR5cGU6IFVTRVJfVVBEQVRFLCBwYXlsb2FkIH0pO1xuXG5leHBvcnQgZGVmYXVsdCB1cGRhdGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FjdGlvbnMvdXNlcnMvdXBkYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7fFxuICB0b2tlbj86IHN0cmluZyxcbiAgdXNlcklkOiBudW1iZXIsXG58fTtcblxuLyoqXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBnZXRBbGxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdG9rZW4sIHVzZXJJZCB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL2dldEFsbC5qcyIsIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5cbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7XG4gIHRva2VuOiBzdHJpbmcsXG4gIHVzZXJJZDogbnVtYmVyLFxuICBmb3JtRGF0YTogYW55LFxufTtcblxudHlwZSBTdWNjZXNzID0ge1xuICBzdGF0dXNDb2RlOiBudW1iZXIsXG4gIG1lc3NhZ2U6IHN0cmluZyxcbiAgcGF5bG9hZDogYW55LFxufTtcblxudHlwZSBFcnJvclJlc3BvbnNlID0ge1xuICBzdGF0dXNDb2RlOiBudW1iZXIsXG4gIGVycm9yOiBzdHJpbmcsXG59O1xuXG4vKipcbiAqXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvblxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLnRva2VuXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC51c2VySWRcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmZvcm1EYXRhXG4gKiBAcmV0dXJucyB7UHJvbWlzZX1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5leHBvcnQgZGVmYXVsdCBhc3luYyBmdW5jdGlvbiBjcmVhdGUoe1xuICB0b2tlbixcbiAgdXNlcklkLFxuICBmb3JtRGF0YSxcbn06IFBheWxvYWQpOiBQcm9taXNlPFN1Y2Nlc3MgfCBFcnJvclJlc3BvbnNlPiB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9waG90b3NgO1xuXG4gICAgY29uc3QgcmVzID0gYXdhaXQgZmV0Y2godXJsLCB7XG4gICAgICBtZXRob2Q6ICdQT1NUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIC8vICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IGZvcm1EYXRhLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9waG90b3MvdXNlcnMvY292ZXIvY3JlYXRlLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmlsZVxuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gZmV0Y2ggYWxsIHBvc3RzIG9mIHJlcXVlc3RlZCB1c2VyXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBnZXRBbGxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdXNlcklkLCB0b2tlbiB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9wb3N0c2A7XG5cbiAgICBjb25zdCByZXMgPSBhd2FpdCBmZXRjaCh1cmwsIHtcbiAgICAgIG1ldGhvZDogJ0dFVCcsXG4gICAgICBoZWFkZXJzOiB7XG4gICAgICAgIEFjY2VwdDogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICAnQ29udGVudC1UeXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nLFxuICAgICAgICBBdXRob3JpemF0aW9uOiB0b2tlbixcbiAgICAgIH0sXG4gICAgfSk7XG5cbiAgICBjb25zdCB7IHN0YXR1cywgc3RhdHVzVGV4dCB9ID0gcmVzO1xuXG4gICAgaWYgKHN0YXR1cyA+PSAzMDApIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0YXR1c0NvZGU6IHN0YXR1cyxcbiAgICAgICAgZXJyb3I6IHN0YXR1c1RleHQsXG4gICAgICB9O1xuICAgIH1cblxuICAgIGNvbnN0IGpzb24gPSBhd2FpdCByZXMuanNvbigpO1xuXG4gICAgcmV0dXJuIHsgLi4uanNvbiB9O1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIHN0YXR1c0NvZGU6IDQwMCxcbiAgICAgIGVycm9yOiAnU29tZXRoaW5nIHdlbnQgd3JvbmcsIHBsZWFzZSB0cnkgYWdhaW4uLi4nLFxuICAgIH07XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgZ2V0QWxsO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hcGkvcG9zdHMvdXNlcnMvZ2V0QWxsLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IGZldGNoIGZyb20gJ2lzb21vcnBoaWMtZmV0Y2gnO1xuaW1wb3J0IHsgSE9TVCB9IGZyb20gJy4uL2NvbmZpZyc7XG5cbi8qKlxuICpcbiAqIEBhc3luY1xuICogQGZ1bmN0aW9uIHVwZGF0ZVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQgLVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuaWQgLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQudG9rZW4gLVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQubmFtZSAtXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5hdmF0YXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuY292ZXIgLVxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQucmVzdW1lIC1cbiAqIEByZXR1cm4ge1Byb21pc2V9IC1cbiAqXG4gKiBAZXhhbXBsZVxuICovXG5hc3luYyBmdW5jdGlvbiB1cGRhdGUoeyBpZCwgdG9rZW4sIG5hbWUsIGF2YXRhciwgY292ZXIsIHJlc3VtZSB9KSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7aWR9YDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnUFVUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KHsgbmFtZSwgYXZhdGFyLCBjb3ZlciwgcmVzdW1lIH0pLFxuICAgIH0pO1xuXG4gICAgY29uc3QgeyBzdGF0dXMsIHN0YXR1c1RleHQgfSA9IHJlcztcblxuICAgIGlmIChzdGF0dXMgPj0gMzAwKSB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgIGVycm9yOiBzdGF0dXNUZXh0LFxuICAgICAgfTtcbiAgICB9XG5cbiAgICBjb25zdCBqc29uID0gYXdhaXQgcmVzLmpzb24oKTtcblxuICAgIHJldHVybiB7IC4uLmpzb24gfTtcbiAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcblxuICAgIHJldHVybiB7XG4gICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICBlcnJvcjogJ1NvbWV0aGluZyB3ZW50IHdyb25nLCBwbGVhc2UgdHJ5IGFnYWluLi4uJyxcbiAgICB9O1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHVwZGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvYXBpL3VzZXJzL3VwZGF0ZS5qcyIsIi8qKlxuICogVGhpcyBmaWxlIGNvbnRhaW5zIGFjdGlvbiBjb25zdGFudHMgZm9yIFVzZXIncyB1cGRhdGVcbiAqXG4gKiBAZm9ybWF0XG4gKi9cblxuY29uc3QgVVNFUl9VUERBVEUgPSAnVVNFUl9VUERBVEUnO1xuXG5leHBvcnQgZGVmYXVsdCBVU0VSX1VQREFURTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29uc3RhbnRzL3VzZXJzL3VwZGF0ZS5qcyIsIi8qKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgSWNvbkJ1dHRvbiBmcm9tICdtYXRlcmlhbC11aS9JY29uQnV0dG9uJztcbmltcG9ydCBJbnNlcnRQaG90b0ljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSW5zZXJ0UGhvdG8nO1xuXG5pbXBvcnQgYXBpVXNlclVwZGF0ZSBmcm9tICcuLi8uLi9hcGkvdXNlcnMvdXBkYXRlJztcbmltcG9ydCBhcGlQaG90b1VwbG9hZCBmcm9tICcuLi8uLi9hcGkvcGhvdG9zL3VzZXJzL2NvdmVyL2NyZWF0ZSc7XG5cbmNsYXNzIEF2YXRhckNvbXBvbmVudCBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGFjdGlvblVzZXJVcGRhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuXG4gICAgdGhpcy5vbkNsaWNrID0gdGhpcy5vbkNsaWNrLmJpbmQodGhpcyk7XG4gICAgLy8gdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2UuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2xpY2soKSB7XG4gICAgdGhpcy5waG90by52YWx1ZSA9IG51bGw7XG4gICAgdGhpcy5waG90by5jbGljaygpO1xuICB9XG5cbiAgb25DaGFuZ2UgPSBhc3luYyAoZSkgPT4ge1xuICAgIGlmIChlKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgfVxuXG4gICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xuXG4gICAgaWYgKGZpbGUudHlwZS5pbmRleE9mKCdpbWFnZS8nKSA9PT0gMCkge1xuICAgICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICAgIGZvcm1EYXRhLmFwcGVuZCgncGhvdG8nLCBmaWxlKTtcblxuICAgICAgY29uc3QgeyB1c2VySWQsIHRva2VuIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCBhcGlQaG90b1VwbG9hZCh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgRXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHsgcGhvdG9Vcmw6IGF2YXRhciB9ID0gcGF5bG9hZDtcblxuICAgICAgYXBpVXNlclVwZGF0ZSh7IGlkOiB1c2VySWQsIHRva2VuLCBhdmF0YXIgfSk7XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uVXNlclVwZGF0ZSh7IGlkOiB1c2VySWQsIGF2YXRhciB9KTtcbiAgICB9XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICAgICAgLi4udGhpcy5wcm9wcy5zdHlsZSxcbiAgICAgICAgfX1cbiAgICAgICAgY2xhc3NOYW1lPXt0aGlzLnByb3BzLmNsYXNzTmFtZX1cbiAgICAgID5cbiAgICAgICAgPGltZ1xuICAgICAgICAgIGFsdD17dGhpcy5wcm9wcy5jaGlsZHJlbi5wcm9wcy5hbHR9XG4gICAgICAgICAgc3JjPXt0aGlzLnByb3BzLmNoaWxkcmVuLnByb3BzLnNyY31cbiAgICAgICAgICBjbGFzc05hbWU9e3RoaXMucHJvcHMuY2hpbGRyZW4ucHJvcHMuY2xhc3NOYW1lfVxuICAgICAgICAvPlxuICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgIGFyaWEtbGFiZWw9XCJJbnNlcnQgUGhvdG9cIlxuICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25DbGlja31cbiAgICAgICAgICBzdHlsZT17e1xuICAgICAgICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXG4gICAgICAgICAgICByaWdodDogJzBweCcsXG4gICAgICAgICAgICB0b3A6ICcwcHgnLFxuICAgICAgICAgIH19XG4gICAgICAgID5cbiAgICAgICAgICA8SW5zZXJ0UGhvdG9JY29uIC8+XG4gICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICB0eXBlPVwiZmlsZVwiXG4gICAgICAgICAgICBhY2NlcHQ9XCJpbWFnZS9qcGVnfHBuZ3xnaWZcIlxuICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMub25DaGFuZ2V9XG4gICAgICAgICAgICByZWY9eyhwaG90bykgPT4ge1xuICAgICAgICAgICAgICB0aGlzLnBob3RvID0gcGhvdG87XG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgc3R5bGU9e3sgZGlzcGxheTogJ25vbmUnIH19XG4gICAgICAgICAgLz5cbiAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5BdmF0YXJDb21wb25lbnQucHJvcFR5cGVzID0ge1xuICBjaGlsZHJlbjogUHJvcFR5cGVzLnNoYXBlKHtcbiAgICBwcm9wczogUHJvcFR5cGVzLnNoYXBlKHtcbiAgICAgIHNyYzogUHJvcFR5cGVzLnN0cmluZyxcbiAgICAgIGFsdDogUHJvcFR5cGVzLnN0cmluZyxcbiAgICAgIGNsYXNzTmFtZTogUHJvcFR5cGVzLnN0cmluZyxcbiAgICB9KSxcbiAgfSkuaXNSZXF1aXJlZCxcbiAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBBdmF0YXJDb21wb25lbnQ7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvRWxlbWVudEluZm8vQXZhdGFyQ29tcG9uZW50LmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IEluc2VydFBob3RvSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90byc7XG5cbmltcG9ydCBhcGlVc2VyVXBkYXRlIGZyb20gJy4uLy4uL2FwaS91c2Vycy91cGRhdGUnO1xuaW1wb3J0IGFwaVBob3RvVXBsb2FkIGZyb20gJy4uLy4uL2FwaS9waG90b3MvdXNlcnMvY292ZXIvY3JlYXRlJztcblxuY2xhc3MgQ292ZXJJbWFnZSBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgc3R5bGU6IFByb3BUeXBlcy5vYmplY3QsXG4gICAgY2xhc3NOYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGFjdGlvblVzZXJVcGRhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIH07XG5cbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuXG4gICAgdGhpcy5vbkNsaWNrID0gdGhpcy5vbkNsaWNrLmJpbmQodGhpcyk7XG4gICAgLy8gdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2UuYmluZCh0aGlzKTtcbiAgfVxuXG4gIG9uQ2xpY2soKSB7XG4gICAgdGhpcy5waG90by52YWx1ZSA9IG51bGw7XG4gICAgdGhpcy5waG90by5jbGljaygpO1xuICB9XG5cbiAgb25DaGFuZ2UgPSBhc3luYyAoZSkgPT4ge1xuICAgIGlmIChlKSB7XG4gICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgfVxuXG4gICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xuXG4gICAgaWYgKGZpbGUudHlwZS5pbmRleE9mKCdpbWFnZS8nKSA9PT0gMCkge1xuICAgICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICAgIGZvcm1EYXRhLmFwcGVuZCgncGhvdG8nLCBmaWxlKTtcblxuICAgICAgY29uc3QgeyB1c2VySWQsIHRva2VuIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCB7IHN0YXR1c0NvZGUsIGVycm9yLCBwYXlsb2FkIH0gPSBhd2FpdCBhcGlQaG90b1VwbG9hZCh7XG4gICAgICAgIHRva2VuLFxuICAgICAgICB1c2VySWQsXG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgRXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHsgcGhvdG9Vcmw6IGNvdmVyIH0gPSBwYXlsb2FkO1xuXG4gICAgICBhcGlVc2VyVXBkYXRlKHsgaWQ6IHVzZXJJZCwgdG9rZW4sIGNvdmVyIH0pO1xuXG4gICAgICB0aGlzLnByb3BzLmFjdGlvblVzZXJVcGRhdGUoeyBpZDogdXNlcklkLCBjb3ZlciB9KTtcbiAgICB9XG4gIH07XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybiAoXG4gICAgICA8ZGl2XG4gICAgICAgIHN0eWxlPXt7XG4gICAgICAgICAgcG9zaXRpb246ICdyZWxhdGl2ZScsXG4gICAgICAgICAgLi4udGhpcy5wcm9wcy5zdHlsZSxcbiAgICAgICAgfX1cbiAgICAgICAgY2xhc3NOYW1lPXt0aGlzLnByb3BzLmNsYXNzTmFtZX1cbiAgICAgID5cbiAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkluc2VydCBQaG90b1wiIG9uQ2xpY2s9e3RoaXMub25DbGlja30+XG4gICAgICAgICAgPEluc2VydFBob3RvSWNvbiAvPlxuICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgdHlwZT1cImZpbGVcIlxuICAgICAgICAgICAgYWNjZXB0PVwiaW1hZ2UvanBlZ3xwbmd8Z2lmXCJcbiAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgcmVmPXsocGhvdG8pID0+IHtcbiAgICAgICAgICAgICAgdGhpcy5waG90byA9IHBob3RvO1xuICAgICAgICAgICAgfX1cbiAgICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6ICdub25lJyB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgIDwvZGl2PlxuICAgICk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQ292ZXJJbWFnZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9FbGVtZW50SW5mby9Db3ZlckltYWdlLmpzIiwiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcblxuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IEF2YXRhciBmcm9tICdtYXRlcmlhbC11aS9BdmF0YXInO1xuXG5pbXBvcnQgQ292ZXJJbWFnZSBmcm9tICcuL0NvdmVySW1hZ2UnO1xuaW1wb3J0IEF2YXRhckNvbXBvbmVudCBmcm9tICcuL0F2YXRhckNvbXBvbmVudCc7XG5cbmNvbnN0IHN0eWxlcyA9ICh0aGVtZSkgPT4gKHtcbiAgcm9vdDoge1xuICAgIGZsZXhHcm93OiAxLFxuICB9LFxuICBtZWRpYToge1xuICAgIGhlaWdodDogMjUwLFxuICB9LFxuICB1c2VybmFtZToge30sXG4gIGNhcmQ6IHt9LFxuICBjYXJkSGVhZGVyUm9vdDoge1xuICAgIG1pbkhlaWdodDogJzEwMHB4JyxcbiAgICBiYWNrZ3JvdW5kU2l6ZTogJ2NvdmVyJyxcbiAgICBiYWNrZ3JvdW5kUmVwZWF0OiAnbm8tcmVwZWF0JyxcbiAgICBiYWNrZ3JvdW5kUG9zaXRpb246ICdjZW50ZXInLFxuICB9LFxuICBjYXJkSGVhZGVyQXZhdGFyOiB7fSxcbiAgY2FyZEhlYWRlckNvbnRlbnQ6IHt9LFxuICBhdmF0YXI6IHtcbiAgICB3aWR0aDogJzhyZW0nLFxuICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgIGhlaWdodDogJzhyZW0nLFxuICAgIGJvcmRlcjogJzFweCBzb2xpZCBncmV5JyxcbiAgfSxcbiAgYXZhdGFySWNvbjoge1xuICAgIHBvc2l0aW9uOiAncmVsYXRpdmUnLFxuICAgIHRvcDogJy00MHB4JyxcbiAgICBtYXJnaW5Cb3R0b206ICctNDhweCcsXG4gIH0sXG59KTtcblxuY2xhc3MgRWxlbWVudEluZm8gZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCB1c2VyLCBlbGVtZW50IH0gPSB0aGlzLnByb3BzO1xuICAgIGNvbnN0IHsgaWQ6IHVzZXJJZCwgdG9rZW4gfSA9IHVzZXI7XG4gICAgY29uc3QgeyBpZCwgdXNlcm5hbWUsIG5hbWUsIGF2YXRhciwgY292ZXIgfSA9IGVsZW1lbnQ7XG4gICAgY29uc3QgdXJsQXZhdGFyID1cbiAgICAgICdodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vbWF5YXNoLXdlYi9kcml2ZS9hdmF0YXIvdXNlcl9tLnN2Zyc7XG4gICAgY29uc3QgdXJsQ292ZXIgPVxuICAgICAgJ2h0dHA6Ly9naW52YS5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMTYvMDgvaW1nXzU3YTAwMGJmOTg0NmMucG5nJztcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPENhcmQgY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9PlxuICAgICAgICAgIDxDYXJkSGVhZGVyXG4gICAgICAgICAgICBhdmF0YXI9e1xuICAgICAgICAgICAgICB1c2VySWQgPT09IGlkID8gKFxuICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgIGFsdD17bmFtZX1cbiAgICAgICAgICAgICAgICAgIHNyYz17YXZhdGFyIHx8IHVybEF2YXRhcn1cbiAgICAgICAgICAgICAgICAgIGltYWdlPXthdmF0YXIgfHwgdXJsQXZhdGFyfVxuICAgICAgICAgICAgICAgICAgY29tcG9uZW50PXsodGhpc1Byb3BzKSA9PiAoXG4gICAgICAgICAgICAgICAgICAgIDxBdmF0YXJDb21wb25lbnRcbiAgICAgICAgICAgICAgICAgICAgICB7Li4udGhpc1Byb3BzfVxuICAgICAgICAgICAgICAgICAgICAgIHRva2VuPXt0b2tlbn1cbiAgICAgICAgICAgICAgICAgICAgICB1c2VySWQ9e3VzZXJJZH1cbiAgICAgICAgICAgICAgICAgICAgICBhY3Rpb25Vc2VyVXBkYXRlPXt0aGlzLnByb3BzLmFjdGlvblVzZXJVcGRhdGV9XG4gICAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgICApfVxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmF2YXRhcn1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICApIDogKFxuICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgIGFsdD17bmFtZX1cbiAgICAgICAgICAgICAgICAgIHNyYz17YXZhdGFyIHx8IHVybEF2YXRhcn1cbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5hdmF0YXJ9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGl0bGU9e25hbWV9XG4gICAgICAgICAgICBzdWJoZWFkZXI9e2BAJHt1c2VybmFtZX1gfVxuICAgICAgICAgICAgYWN0aW9uPXtcbiAgICAgICAgICAgICAgdXNlcklkID09PSBpZCA/IChcbiAgICAgICAgICAgICAgICA8Q292ZXJJbWFnZVxuICAgICAgICAgICAgICAgICAgdG9rZW49e3Rva2VufVxuICAgICAgICAgICAgICAgICAgdXNlcklkPXt1c2VySWR9XG4gICAgICAgICAgICAgICAgICBhY3Rpb25Vc2VyVXBkYXRlPXt0aGlzLnByb3BzLmFjdGlvblVzZXJVcGRhdGV9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgKSA6IG51bGxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNsYXNzZXM9e3tcbiAgICAgICAgICAgICAgcm9vdDogY2xhc3Nlcy5jYXJkSGVhZGVyUm9vdCxcbiAgICAgICAgICAgICAgYXZhdGFyOiBjbGFzc2VzLmNhcmRIZWFkZXJBdmF0YXIsXG4gICAgICAgICAgICAgIGNvbnRlbnQ6IGNsYXNzZXMuY2FyZEhlYWRlckNvbnRlbnQsXG4gICAgICAgICAgICB9fVxuICAgICAgICAgICAgc3R5bGU9e3sgYmFja2dyb3VuZEltYWdlOiBgdXJsKCR7Y292ZXIgfHwgdXJsQ292ZXJ9KWAgfX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0NhcmQ+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbkVsZW1lbnRJbmZvLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuXG4gIHVzZXI6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgaWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgfSksXG5cbiAgZWxlbWVudDogUHJvcFR5cGVzLnNoYXBlKHtcbiAgICBpZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICAgIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLmlzUmVxdWlyZWQsXG4gICAgbmFtZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIGF2YXRhcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgICBjb3ZlcjogUHJvcFR5cGVzLnN0cmluZyxcbiAgfSksXG4gIGFjdGlvblVzZXJVcGRhdGU6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoRWxlbWVudEluZm8pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0VsZW1lbnRJbmZvL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5cbmltcG9ydCB7IGJpbmRBY3Rpb25DcmVhdG9ycyB9IGZyb20gJ3JlZHV4JztcbmltcG9ydCB7IGNvbm5lY3QgfSBmcm9tICdyZWFjdC1yZWR1eCc7XG5cbmltcG9ydCBMb2FkYWJsZSBmcm9tICdyZWFjdC1sb2FkYWJsZSc7XG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgQXBwQmFyIGZyb20gJ21hdGVyaWFsLXVpL0FwcEJhcic7XG5pbXBvcnQgVGFicywgeyBUYWIgfSBmcm9tICdtYXRlcmlhbC11aS9UYWJzJztcblxuaW1wb3J0IExvYWRpbmcgZnJvbSAnLi4vLi4vY29tcG9uZW50cy9Mb2FkaW5nJztcblxuaW1wb3J0IGFwaVVzZXJDb3Vyc2VzR2V0IGZyb20gJy4uLy4uL2FwaS9jb3Vyc2VzL3VzZXJzL2dldEFsbCc7XG5pbXBvcnQgYXBpVXNlclBvc3RzR2V0IGZyb20gJy4uLy4uL2FwaS9wb3N0cy91c2Vycy9nZXRBbGwnO1xuXG5pbXBvcnQgYWN0aW9uVXNlclVwZGF0ZSBmcm9tICcuLi8uLi9hY3Rpb25zL3VzZXJzL3VwZGF0ZSc7XG5pbXBvcnQgYWN0aW9uVXNlclBvc3RzR2V0IGZyb20gJy4uLy4uL2FjdGlvbnMvcG9zdHMvZ2V0QWxsJztcbmltcG9ydCBhY3Rpb25Vc2VyQ291cnNlc0dldCBmcm9tICcuLi8uLi9hY3Rpb25zL2NvdXJzZXMvZ2V0QWxsJztcblxuaW1wb3J0IEVsZW1lbnRJbmZvIGZyb20gJy4uL0VsZW1lbnRJbmZvL2luZGV4JztcblxuY29uc3QgQXN5bmNGb2xsb3cgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuLi9Gb2xsb3cnKSxcbiAgbW9kdWxlczogWycuLi9Gb2xsb3cnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY1RhYlJlc3VtZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vVGFicy9UYWJSZXN1bWUnKSxcbiAgbW9kdWxlczogWycuL1RhYnMvVGFiUmVzdW1lJ10sXG4gIGxvYWRpbmc6IExvYWRpbmcsXG59KTtcblxuY29uc3QgQXN5bmNUYWJQb3N0cyA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vVGFicy9UYWJQb3N0cycpLFxuICBtb2R1bGVzOiBbJy4vVGFicy9UYWJQb3N0cyddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNvbnN0IEFzeW5jVGFiQ291cnNlcyA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4vVGFicy9UYWJDb3Vyc2VzJyksXG4gIG1vZHVsZXM6IFsnLi9UYWJzL1RhYkNvdXJzZXMnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBzdHlsZXMgPSAodGhlbWUpID0+ICh7XG4gIHJvb3Q6IHtcbiAgICBmbGV4R3JvdzogMSxcbiAgICB3aWR0aDogJzEwMCUnLFxuICB9LFxuICBtZWRpYToge1xuICAgIGhlaWdodDogMjUwLFxuICB9LFxuICBmb2xsb3c6IHtcbiAgICBtYXJnaW46ICdhdXRvJyxcbiAgfSxcbiAgYnV0dG9uOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnMTVweCcsXG4gICAgZm9udFdlaWdodDogJ2JvbGQnLFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59KTtcblxuY2xhc3MgVXNlciBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKHByb3BzKSB7XG4gICAgc3VwZXIocHJvcHMpO1xuICAgIHRoaXMuc3RhdGUgPSB7XG4gICAgICB2YWx1ZTogMCxcbiAgICAgIGZvbGxvdzogZmFsc2UsXG4gICAgfTtcblxuICAgIHRoaXMuaGFuZGxlQ2hhbmdlID0gdGhpcy5oYW5kbGVDaGFuZ2UuYmluZCh0aGlzKTtcbiAgfVxuXG4gIGFzeW5jIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCB7IGVsZW1lbnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IGlkLCB0b2tlbiwgY291cnNlcywgcG9zdHMgfSA9IGVsZW1lbnQ7XG5cbiAgICAgIGlmICh0eXBlb2YgcG9zdHMgPT09ICd1bmRlZmluZWQnIHx8IE9iamVjdC5rZXlzKHBvc3RzKS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCwgbmV4dCB9ID0gYXdhaXQgYXBpVXNlclBvc3RzR2V0KHtcbiAgICAgICAgICB0b2tlbixcbiAgICAgICAgICB1c2VySWQ6IGlkLFxuICAgICAgICB9KTtcblxuICAgICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JcbiAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcbiAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnByb3BzLmFjdGlvblVzZXJQb3N0c0dldCh7XG4gICAgICAgICAgaWQsXG4gICAgICAgICAgc3RhdHVzQ29kZSxcbiAgICAgICAgICBwb3N0czogcGF5bG9hZCxcbiAgICAgICAgfSk7XG4gICAgICB9XG5cbiAgICAgIGlmICh0eXBlb2YgY291cnNlcyA9PT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCwgbmV4dCB9ID0gYXdhaXQgYXBpVXNlckNvdXJzZXNHZXQoe1xuICAgICAgICAgIHRva2VuLFxuICAgICAgICAgIHVzZXJJZDogaWQsXG4gICAgICAgIH0pO1xuXG4gICAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAgIC8vIGhhbmRsZSBlcnJvclxuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMucHJvcHMuYWN0aW9uVXNlckNvdXJzZXNHZXQoe1xuICAgICAgICAgIGlkLFxuICAgICAgICAgIHN0YXR1c0NvZGUsXG4gICAgICAgICAgY291cnNlczogcGF5bG9hZCxcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgIH1cbiAgfVxuXG4gIGhhbmRsZUNoYW5nZShldmVudCwgdmFsdWUpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgdmFsdWUgfSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBlbGVtZW50cywgZWxlbWVudCwgcG9zdHMsIGNvdXJzZXMgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyB1c2VyIH0gPSBlbGVtZW50cztcbiAgICBjb25zdCB7IGlzU2lnbmVkSW4sIGlkOiB1c2VySWQgfSA9IHVzZXI7XG5cbiAgICBjb25zdCB7IGd1cnUgPSBmYWxzZSwgaWQgfSA9IGVsZW1lbnQ7XG5cbiAgICBjb25zdCB7IHZhbHVlIH0gPSB0aGlzLnN0YXRlO1xuXG4gICAgcmV0dXJuIChcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9PlxuICAgICAgICA8RWxlbWVudEluZm9cbiAgICAgICAgICB1c2VyPXt1c2VyfVxuICAgICAgICAgIGVsZW1lbnQ9e2VsZW1lbnR9XG4gICAgICAgICAgYWN0aW9uVXNlclVwZGF0ZT17dGhpcy5wcm9wcy5hY3Rpb25Vc2VyVXBkYXRlfVxuICAgICAgICAvPlxuXG4gICAgICAgIDxBcHBCYXIgcG9zaXRpb249XCJzdGF0aWNcIiBjb2xvcj1cImRlZmF1bHRcIj5cbiAgICAgICAgICB7Z3VydSA/IChcbiAgICAgICAgICAgIDxUYWJzXG4gICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuICAgICAgICAgICAgICBpbmRpY2F0b3JDb2xvcj1cInByaW1hcnlcIlxuICAgICAgICAgICAgICB0ZXh0Q29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgc2Nyb2xsYWJsZVxuICAgICAgICAgICAgICBzY3JvbGxCdXR0b25zPVwiYXV0b1wiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxUYWIgbGFiZWw9XCJSZXN1bWVcIiAvPlxuICAgICAgICAgICAgICA8VGFiIGxhYmVsPVwiUG9zdHNcIiAvPlxuICAgICAgICAgICAgICA8VGFiIGxhYmVsPVwiQ291cnNlc1wiIC8+XG5cbiAgICAgICAgICAgICAge2lzU2lnbmVkSW4gJiYgdXNlcklkICE9PSBpZCA/IChcbiAgICAgICAgICAgICAgICA8QXN5bmNGb2xsb3cgZWxlbWVudD17ZWxlbWVudH0gLz5cbiAgICAgICAgICAgICAgKSA6IG51bGx9XG4gICAgICAgICAgICA8L1RhYnM+XG4gICAgICAgICAgKSA6IChcbiAgICAgICAgICAgIDxUYWJzXG4gICAgICAgICAgICAgIHZhbHVlPXt2YWx1ZX1cbiAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuICAgICAgICAgICAgICBpbmRpY2F0b3JDb2xvcj1cInByaW1hcnlcIlxuICAgICAgICAgICAgICB0ZXh0Q29sb3I9XCJwcmltYXJ5XCJcbiAgICAgICAgICAgICAgc2Nyb2xsYWJsZVxuICAgICAgICAgICAgICBzY3JvbGxCdXR0b25zPVwiYXV0b1wiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIDxUYWIgbGFiZWw9XCJSZXN1bWVcIiAvPlxuICAgICAgICAgICAgICA8VGFiIGxhYmVsPVwiUG9zdHNcIiAvPlxuXG4gICAgICAgICAgICAgIHtpc1NpZ25lZEluICYmIHVzZXJJZCAhPT0gaWQgPyAoXG4gICAgICAgICAgICAgICAgPEFzeW5jRm9sbG93IGVsZW1lbnQ9e2VsZW1lbnR9IC8+XG4gICAgICAgICAgICAgICkgOiBudWxsfVxuICAgICAgICAgICAgPC9UYWJzPlxuICAgICAgICAgICl9XG4gICAgICAgIDwvQXBwQmFyPlxuICAgICAgICB7dmFsdWUgPT09IDAgJiYgKFxuICAgICAgICAgIDxBc3luY1RhYlJlc3VtZVxuICAgICAgICAgICAgdXNlcj17dXNlcn1cbiAgICAgICAgICAgIGVsZW1lbnQ9e2VsZW1lbnR9XG4gICAgICAgICAgICBhY3Rpb25Vc2VyVXBkYXRlPXt0aGlzLnByb3BzLmFjdGlvblVzZXJVcGRhdGV9XG4gICAgICAgICAgLz5cbiAgICAgICAgKX1cbiAgICAgICAge3ZhbHVlID09PSAxICYmIChcbiAgICAgICAgICA8QXN5bmNUYWJQb3N0cyB1c2VyPXt1c2VyfSBlbGVtZW50PXtlbGVtZW50fSBwb3N0cz17cG9zdHN9IC8+XG4gICAgICAgICl9XG4gICAgICAgIHtndXJ1ICYmXG4gICAgICAgICAgdmFsdWUgPT09IDIgJiYgKFxuICAgICAgICAgICAgPEFzeW5jVGFiQ291cnNlcyB1c2VyPXt1c2VyfSBlbGVtZW50PXtlbGVtZW50fSBjb3Vyc2VzPXtjb3Vyc2VzfSAvPlxuICAgICAgICAgICl9XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cblVzZXIucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgZWxlbWVudDogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBwb3N0czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBjb3Vyc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGFjdGlvblVzZXJVcGRhdGU6IFByb3BUeXBlcy5mdW5jLFxuICBhY3Rpb25Vc2VyUG9zdHNHZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG4gIGFjdGlvblVzZXJDb3Vyc2VzR2V0OiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxufTtcblxuY29uc3QgbWFwU3RhdGVUb1Byb3BzID0gKHsgcG9zdHMsIGNvdXJzZXMgfSkgPT4gKHsgcG9zdHMsIGNvdXJzZXMgfSk7XG5cbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvblVzZXJVcGRhdGUsXG4gICAgICBhY3Rpb25Vc2VyUG9zdHNHZXQsXG4gICAgICBhY3Rpb25Vc2VyQ291cnNlc0dldCxcbiAgICB9LFxuICAgIGRpc3BhdGNoLFxuICApO1xuXG5leHBvcnQgZGVmYXVsdCBjb25uZWN0KG1hcFN0YXRlVG9Qcm9wcywgbWFwRGlzcGF0Y2hUb1Byb3BzKShcbiAgd2l0aFN0eWxlcyhzdHlsZXMpKFVzZXIpLFxuKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9jbGllbnQvY29udGFpbmVycy9FbGVtZW50UGFnZS9Vc2VyLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==