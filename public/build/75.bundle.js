webpackJsonp([75],{

/***/ "./src/client/containers/ProfilePage/Email.js":
/*!****************************************************!*\
  !*** ./src/client/containers/ProfilePage/Email.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _Button = __webpack_require__(/*! material-ui/Button */ "./node_modules/material-ui/Button/index.js");

var _Button2 = _interopRequireDefault(_Button);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  root: {},
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  },
  gridItem: {
    padding: '1%'
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed'
    },
    '&:valid': {
      outline: 'none',
      borderColor: 'green',
      boxShadow: '0 0 10px #9ecaed'
    },
    '&:invalid': {
      outline: 'none',
      borderColor: 'red',
      boxShadow: '0 0 10px #9ecaed'
    }
  },
  flexGrow: {
    flex: '1 1 auto'
  }
}; /** @format */

var PATTERN = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';

var Email = function (_Component) {
  (0, _inherits3.default)(Email, _Component);

  function Email(props) {
    (0, _classCallCheck3.default)(this, Email);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Email.__proto__ || (0, _getPrototypeOf2.default)(Email)).call(this, props));

    var email = props.user.email;


    _this.state = {
      email: email,
      focus: false,
      isValid: false,
      error: '',
      message: ''
    };

    _this.onChange = _this.onChange.bind(_this);
    _this.onSubmit = _this.onSubmit.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Email, [{
    key: 'onChange',
    value: function onChange(e) {
      var email = e.target.value;
      var user = this.props.user;


      var regex = new RegExp(PATTERN);

      if (!regex.test(email)) {
        this.setState({ email: email, isValid: false });
        return;
      }
      this.setState({ email: email, isValid: true });

      if (user.email === email) {
        return;
      }
      console.log(email);
    }
  }, {
    key: 'onSubmit',
    value: function onSubmit() {
      var email = this.state.email;
      var _props$user = this.props.user,
          id = _props$user.id,
          token = _props$user.token;
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props = this.props,
          classes = _props.classes,
          disabled = _props.disabled,
          user = _props.user;
      var _state = this.state,
          focus = _state.focus,
          isValid = _state.isValid,
          message = _state.message;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          {
            item: true,
            xs: 12,
            sm: 10,
            md: 8,
            lg: 6,
            xl: 6,
            className: classes.gridItem
          },
          _react2.default.createElement(
            'div',
            {
              onMouseEnter: function onMouseEnter() {
                return _this2.setState({ hover: true });
              },
              onMouseLeave: function onMouseLeave() {
                return _this2.setState({ hover: false });
              }
            },
            _react2.default.createElement(
              _Card2.default,
              { raised: this.state.hover, className: classes.card },
              _react2.default.createElement(_Card.CardHeader, { title: 'Email' }),
              _react2.default.createElement(
                _Card.CardContent,
                {
                  onFocus: function onFocus() {
                    return _this2.setState({ focus: true });
                  },
                  onBlur: function onBlur() {
                    return _this2.setState({
                      focus: !(user.email === _this2.state.email)
                    });
                  },
                  style: { display: 'flex' }
                },
                _react2.default.createElement('input', {
                  placeholder: 'Username',
                  value: this.state.email,
                  onChange: this.onChange,
                  pattern: PATTERN,
                  required: true,
                  disabled: true,
                  className: classes.input
                })
              ),
              focus && !(disabled === true) ? _react2.default.createElement(
                _Card.CardActions,
                null,
                _react2.default.createElement('div', { className: classes.flexGrow }),
                _react2.default.createElement(
                  _Button2.default,
                  {
                    raised: true,
                    color: 'primary',
                    onClick: this.onSubmit,
                    disabled: !isValid
                  },
                  'Submit'
                )
              ) : null
            )
          )
        )
      );
    }
  }]);
  return Email;
}(_react.Component);

Email.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  user: _propTypes2.default.shape({
    id: _propTypes2.default.number.isRequired,
    token: _propTypes2.default.string.isRequired,
    name: _propTypes2.default.string,
    email: _propTypes2.default.string
  }).isRequired,
  disabled: _propTypes2.default.bool.isRequired
};

Email.defaultProps = {
  disabled: false
};

exports.default = (0, _withStyles2.default)(styles)(Email);

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUHJvZmlsZVBhZ2UvRW1haWwuanMiXSwibmFtZXMiOlsic3R5bGVzIiwicm9vdCIsImNhcmQiLCJib3JkZXJSYWRpdXMiLCJhdmF0YXIiLCJncmlkSXRlbSIsInBhZGRpbmciLCJpbnB1dCIsImZsZXhHcm93IiwiYm9yZGVyIiwib3V0bGluZSIsImJvcmRlckNvbG9yIiwiYm94U2hhZG93IiwiZmxleCIsIlBBVFRFUk4iLCJFbWFpbCIsInByb3BzIiwiZW1haWwiLCJ1c2VyIiwic3RhdGUiLCJmb2N1cyIsImlzVmFsaWQiLCJlcnJvciIsIm1lc3NhZ2UiLCJvbkNoYW5nZSIsImJpbmQiLCJvblN1Ym1pdCIsImUiLCJ0YXJnZXQiLCJ2YWx1ZSIsInJlZ2V4IiwiUmVnRXhwIiwidGVzdCIsInNldFN0YXRlIiwiY29uc29sZSIsImxvZyIsImlkIiwidG9rZW4iLCJjbGFzc2VzIiwiZGlzYWJsZWQiLCJob3ZlciIsImRpc3BsYXkiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic2hhcGUiLCJudW1iZXIiLCJzdHJpbmciLCJuYW1lIiwiYm9vbCIsImRlZmF1bHRQcm9wcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFFQSxJQUFNQSxTQUFTO0FBQ2JDLFFBQU0sRUFETztBQUViQyxRQUFNO0FBQ0pDLGtCQUFjO0FBRFYsR0FGTztBQUtiQyxVQUFRO0FBQ05ELGtCQUFjO0FBRFIsR0FMSztBQVFiRSxZQUFVO0FBQ1JDLGFBQVM7QUFERCxHQVJHO0FBV2JDLFNBQU87QUFDTEMsY0FBVSxHQURMO0FBRUxDLFlBQVEsbUJBRkg7QUFHTE4sa0JBQWMsS0FIVDtBQUlMRyxhQUFTLEtBSko7QUFLTCxlQUFXO0FBQ1RJLGVBQVMsTUFEQTtBQUVUQyxtQkFBYSxTQUZKO0FBR1RDLGlCQUFXO0FBSEYsS0FMTjtBQVVMLGVBQVc7QUFDVEYsZUFBUyxNQURBO0FBRVRDLG1CQUFhLE9BRko7QUFHVEMsaUJBQVc7QUFIRixLQVZOO0FBZUwsaUJBQWE7QUFDWEYsZUFBUyxNQURFO0FBRVhDLG1CQUFhLEtBRkY7QUFHWEMsaUJBQVc7QUFIQTtBQWZSLEdBWE07QUFnQ2JKLFlBQVU7QUFDUkssVUFBTTtBQURFO0FBaENHLENBQWYsQyxDQVhBOztBQWdEQSxJQUFNQyxVQUFVLGlEQUFoQjs7SUFFTUMsSzs7O0FBQ0osaUJBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSxvSUFDWEEsS0FEVzs7QUFBQSxRQUVUQyxLQUZTLEdBRUNELE1BQU1FLElBRlAsQ0FFVEQsS0FGUzs7O0FBSWpCLFVBQUtFLEtBQUwsR0FBYTtBQUNYRixrQkFEVztBQUVYRyxhQUFPLEtBRkk7QUFHWEMsZUFBUyxLQUhFO0FBSVhDLGFBQU8sRUFKSTtBQUtYQyxlQUFTO0FBTEUsS0FBYjs7QUFRQSxVQUFLQyxRQUFMLEdBQWdCLE1BQUtBLFFBQUwsQ0FBY0MsSUFBZCxPQUFoQjtBQUNBLFVBQUtDLFFBQUwsR0FBZ0IsTUFBS0EsUUFBTCxDQUFjRCxJQUFkLE9BQWhCO0FBYmlCO0FBY2xCOzs7OzZCQUVRRSxDLEVBQUc7QUFBQSxVQUNLVixLQURMLEdBQ2VVLEVBQUVDLE1BRGpCLENBQ0ZDLEtBREU7QUFBQSxVQUVGWCxJQUZFLEdBRU8sS0FBS0YsS0FGWixDQUVGRSxJQUZFOzs7QUFJVixVQUFNWSxRQUFRLElBQUlDLE1BQUosQ0FBV2pCLE9BQVgsQ0FBZDs7QUFFQSxVQUFJLENBQUNnQixNQUFNRSxJQUFOLENBQVdmLEtBQVgsQ0FBTCxFQUF3QjtBQUN0QixhQUFLZ0IsUUFBTCxDQUFjLEVBQUVoQixZQUFGLEVBQVNJLFNBQVMsS0FBbEIsRUFBZDtBQUNBO0FBQ0Q7QUFDRCxXQUFLWSxRQUFMLENBQWMsRUFBRWhCLFlBQUYsRUFBU0ksU0FBUyxJQUFsQixFQUFkOztBQUVBLFVBQUlILEtBQUtELEtBQUwsS0FBZUEsS0FBbkIsRUFBMEI7QUFDeEI7QUFDRDtBQUNEaUIsY0FBUUMsR0FBUixDQUFZbEIsS0FBWjtBQUNEOzs7K0JBRVU7QUFBQSxVQUNEQSxLQURDLEdBQ1MsS0FBS0UsS0FEZCxDQUNERixLQURDO0FBQUEsd0JBRWEsS0FBS0QsS0FBTCxDQUFXRSxJQUZ4QjtBQUFBLFVBRURrQixFQUZDLGVBRURBLEVBRkM7QUFBQSxVQUVHQyxLQUZILGVBRUdBLEtBRkg7QUFHVjs7OzZCQUVRO0FBQUE7O0FBQUEsbUJBQzZCLEtBQUtyQixLQURsQztBQUFBLFVBQ0NzQixPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVQyxRQURWLFVBQ1VBLFFBRFY7QUFBQSxVQUNvQnJCLElBRHBCLFVBQ29CQSxJQURwQjtBQUFBLG1CQUU2QixLQUFLQyxLQUZsQztBQUFBLFVBRUNDLEtBRkQsVUFFQ0EsS0FGRDtBQUFBLFVBRVFDLE9BRlIsVUFFUUEsT0FGUjtBQUFBLFVBRWlCRSxPQUZqQixVQUVpQkEsT0FGakI7OztBQUlQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBV2UsUUFBUXJDLElBQWhFO0FBQ0U7QUFBQTtBQUFBO0FBQ0Usc0JBREY7QUFFRSxnQkFBSSxFQUZOO0FBR0UsZ0JBQUksRUFITjtBQUlFLGdCQUFJLENBSk47QUFLRSxnQkFBSSxDQUxOO0FBTUUsZ0JBQUksQ0FOTjtBQU9FLHVCQUFXcUMsUUFBUWpDO0FBUHJCO0FBU0U7QUFBQTtBQUFBO0FBQ0UsNEJBQWM7QUFBQSx1QkFBTSxPQUFLNEIsUUFBTCxDQUFjLEVBQUVPLE9BQU8sSUFBVCxFQUFkLENBQU47QUFBQSxlQURoQjtBQUVFLDRCQUFjO0FBQUEsdUJBQU0sT0FBS1AsUUFBTCxDQUFjLEVBQUVPLE9BQU8sS0FBVCxFQUFkLENBQU47QUFBQTtBQUZoQjtBQUlFO0FBQUE7QUFBQSxnQkFBTSxRQUFRLEtBQUtyQixLQUFMLENBQVdxQixLQUF6QixFQUFnQyxXQUFXRixRQUFRcEMsSUFBbkQ7QUFDRSxnRUFBWSxPQUFPLE9BQW5CLEdBREY7QUFFRTtBQUFBO0FBQUE7QUFDRSwyQkFBUztBQUFBLDJCQUFNLE9BQUsrQixRQUFMLENBQWMsRUFBRWIsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLG1CQURYO0FBRUUsMEJBQVE7QUFBQSwyQkFDTixPQUFLYSxRQUFMLENBQWM7QUFDWmIsNkJBQU8sRUFBRUYsS0FBS0QsS0FBTCxLQUFlLE9BQUtFLEtBQUwsQ0FBV0YsS0FBNUI7QUFESyxxQkFBZCxDQURNO0FBQUEsbUJBRlY7QUFPRSx5QkFBTyxFQUFFd0IsU0FBUyxNQUFYO0FBUFQ7QUFTRTtBQUNFLCtCQUFhLFVBRGY7QUFFRSx5QkFBTyxLQUFLdEIsS0FBTCxDQUFXRixLQUZwQjtBQUdFLDRCQUFVLEtBQUtPLFFBSGpCO0FBSUUsMkJBQVNWLE9BSlg7QUFLRSxnQ0FMRjtBQU1FLGdDQU5GO0FBT0UsNkJBQVd3QixRQUFRL0I7QUFQckI7QUFURixlQUZGO0FBcUJHYSx1QkFBUyxFQUFFbUIsYUFBYSxJQUFmLENBQVQsR0FDQztBQUFBO0FBQUE7QUFDRSx1REFBSyxXQUFXRCxRQUFROUIsUUFBeEIsR0FERjtBQUVFO0FBQUE7QUFBQTtBQUNFLGdDQURGO0FBRUUsMkJBQU0sU0FGUjtBQUdFLDZCQUFTLEtBQUtrQixRQUhoQjtBQUlFLDhCQUFVLENBQUNMO0FBSmI7QUFBQTtBQUFBO0FBRkYsZUFERCxHQVlHO0FBakNOO0FBSkY7QUFURjtBQURGLE9BREY7QUFzREQ7Ozs7O0FBR0hOLE1BQU0yQixTQUFOLEdBQWtCO0FBQ2hCSixXQUFTLG9CQUFVSyxNQUFWLENBQWlCQyxVQURWO0FBRWhCMUIsUUFBTSxvQkFBVTJCLEtBQVYsQ0FBZ0I7QUFDcEJULFFBQUksb0JBQVVVLE1BQVYsQ0FBaUJGLFVBREQ7QUFFcEJQLFdBQU8sb0JBQVVVLE1BQVYsQ0FBaUJILFVBRko7QUFHcEJJLFVBQU0sb0JBQVVELE1BSEk7QUFJcEI5QixXQUFPLG9CQUFVOEI7QUFKRyxHQUFoQixFQUtISCxVQVBhO0FBUWhCTCxZQUFVLG9CQUFVVSxJQUFWLENBQWVMO0FBUlQsQ0FBbEI7O0FBV0E3QixNQUFNbUMsWUFBTixHQUFxQjtBQUNuQlgsWUFBVTtBQURTLENBQXJCOztrQkFJZSwwQkFBV3ZDLE1BQVgsRUFBbUJlLEtBQW5CLEMiLCJmaWxlIjoiNzUuYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcyc7XG5pbXBvcnQgZmV0Y2ggZnJvbSAnaXNvbW9ycGhpYy1mZXRjaCc7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBHcmlkIGZyb20gJ21hdGVyaWFsLXVpL0dyaWQnO1xuaW1wb3J0IENhcmQsIHsgQ2FyZEhlYWRlciwgQ2FyZENvbnRlbnQsIENhcmRBY3Rpb25zIH0gZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZCc7XG5pbXBvcnQgQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0J1dHRvbic7XG5cbmNvbnN0IHN0eWxlcyA9IHtcbiAgcm9vdDoge30sXG4gIGNhcmQ6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc4cHgnLFxuICB9LFxuICBhdmF0YXI6IHtcbiAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICB9LFxuICBncmlkSXRlbToge1xuICAgIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGlucHV0OiB7XG4gICAgZmxleEdyb3c6ICcxJyxcbiAgICBib3JkZXI6ICcycHggc29saWQgI2RhZGFkYScsXG4gICAgYm9yZGVyUmFkaXVzOiAnN3B4JyxcbiAgICBwYWRkaW5nOiAnOHB4JyxcbiAgICAnJjpmb2N1cyc6IHtcbiAgICAgIG91dGxpbmU6ICdub25lJyxcbiAgICAgIGJvcmRlckNvbG9yOiAnIzllY2FlZCcsXG4gICAgICBib3hTaGFkb3c6ICcwIDAgMTBweCAjOWVjYWVkJyxcbiAgICB9LFxuICAgICcmOnZhbGlkJzoge1xuICAgICAgb3V0bGluZTogJ25vbmUnLFxuICAgICAgYm9yZGVyQ29sb3I6ICdncmVlbicsXG4gICAgICBib3hTaGFkb3c6ICcwIDAgMTBweCAjOWVjYWVkJyxcbiAgICB9LFxuICAgICcmOmludmFsaWQnOiB7XG4gICAgICBvdXRsaW5lOiAnbm9uZScsXG4gICAgICBib3JkZXJDb2xvcjogJ3JlZCcsXG4gICAgICBib3hTaGFkb3c6ICcwIDAgMTBweCAjOWVjYWVkJyxcbiAgICB9LFxuICB9LFxuICBmbGV4R3Jvdzoge1xuICAgIGZsZXg6ICcxIDEgYXV0bycsXG4gIH0sXG59O1xuXG5jb25zdCBQQVRURVJOID0gJ15bYS16QS1aMC05Xy4rLV0rQFthLXpBLVowLTktXSsuW2EtekEtWjAtOS0uXSskJztcblxuY2xhc3MgRW1haWwgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICBjb25zdCB7IGVtYWlsIH0gPSBwcm9wcy51c2VyO1xuXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGVtYWlsLFxuICAgICAgZm9jdXM6IGZhbHNlLFxuICAgICAgaXNWYWxpZDogZmFsc2UsXG4gICAgICBlcnJvcjogJycsXG4gICAgICBtZXNzYWdlOiAnJyxcbiAgICB9O1xuXG4gICAgdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2UuYmluZCh0aGlzKTtcbiAgICB0aGlzLm9uU3VibWl0ID0gdGhpcy5vblN1Ym1pdC5iaW5kKHRoaXMpO1xuICB9XG5cbiAgb25DaGFuZ2UoZSkge1xuICAgIGNvbnN0IHsgdmFsdWU6IGVtYWlsIH0gPSBlLnRhcmdldDtcbiAgICBjb25zdCB7IHVzZXIgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCByZWdleCA9IG5ldyBSZWdFeHAoUEFUVEVSTik7XG5cbiAgICBpZiAoIXJlZ2V4LnRlc3QoZW1haWwpKSB7XG4gICAgICB0aGlzLnNldFN0YXRlKHsgZW1haWwsIGlzVmFsaWQ6IGZhbHNlIH0pO1xuICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB0aGlzLnNldFN0YXRlKHsgZW1haWwsIGlzVmFsaWQ6IHRydWUgfSk7XG5cbiAgICBpZiAodXNlci5lbWFpbCA9PT0gZW1haWwpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgY29uc29sZS5sb2coZW1haWwpO1xuICB9XG5cbiAgb25TdWJtaXQoKSB7XG4gICAgY29uc3QgeyBlbWFpbCB9ID0gdGhpcy5zdGF0ZTtcbiAgICBjb25zdCB7IGlkLCB0b2tlbiB9ID0gdGhpcy5wcm9wcy51c2VyO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgZGlzYWJsZWQsIHVzZXIgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBmb2N1cywgaXNWYWxpZCwgbWVzc2FnZSB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIHJldHVybiAoXG4gICAgICA8R3JpZCBjb250YWluZXIganVzdGlmeT1cImNlbnRlclwiIHNwYWNpbmc9ezB9IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgPEdyaWRcbiAgICAgICAgICBpdGVtXG4gICAgICAgICAgeHM9ezEyfVxuICAgICAgICAgIHNtPXsxMH1cbiAgICAgICAgICBtZD17OH1cbiAgICAgICAgICBsZz17Nn1cbiAgICAgICAgICB4bD17Nn1cbiAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZ3JpZEl0ZW19XG4gICAgICAgID5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBvbk1vdXNlRW50ZXI9eygpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KX1cbiAgICAgICAgICAgIG9uTW91c2VMZWF2ZT17KCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KX1cbiAgICAgICAgICA+XG4gICAgICAgICAgICA8Q2FyZCByYWlzZWQ9e3RoaXMuc3RhdGUuaG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICAgICAgPENhcmRIZWFkZXIgdGl0bGU9eydFbWFpbCd9IC8+XG4gICAgICAgICAgICAgIDxDYXJkQ29udGVudFxuICAgICAgICAgICAgICAgIG9uRm9jdXM9eygpID0+IHRoaXMuc2V0U3RhdGUoeyBmb2N1czogdHJ1ZSB9KX1cbiAgICAgICAgICAgICAgICBvbkJsdXI9eygpID0+XG4gICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgICAgICAgICAgZm9jdXM6ICEodXNlci5lbWFpbCA9PT0gdGhpcy5zdGF0ZS5lbWFpbCksXG4gICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzdHlsZT17eyBkaXNwbGF5OiAnZmxleCcgfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9eydVc2VybmFtZSd9XG4gICAgICAgICAgICAgICAgICB2YWx1ZT17dGhpcy5zdGF0ZS5lbWFpbH1cbiAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXt0aGlzLm9uQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgcGF0dGVybj17UEFUVEVSTn1cbiAgICAgICAgICAgICAgICAgIHJlcXVpcmVkXG4gICAgICAgICAgICAgICAgICBkaXNhYmxlZFxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLmlucHV0fVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvQ2FyZENvbnRlbnQ+XG4gICAgICAgICAgICAgIHtmb2N1cyAmJiAhKGRpc2FibGVkID09PSB0cnVlKSA/IChcbiAgICAgICAgICAgICAgICA8Q2FyZEFjdGlvbnM+XG4gICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5mbGV4R3Jvd30gLz5cbiAgICAgICAgICAgICAgICAgIDxCdXR0b25cbiAgICAgICAgICAgICAgICAgICAgcmFpc2VkXG4gICAgICAgICAgICAgICAgICAgIGNvbG9yPVwicHJpbWFyeVwiXG4gICAgICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25TdWJtaXR9XG4gICAgICAgICAgICAgICAgICAgIGRpc2FibGVkPXshaXNWYWxpZH1cbiAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgU3VibWl0XG4gICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cbiAgICAgICAgICAgICAgICA8L0NhcmRBY3Rpb25zPlxuICAgICAgICAgICAgICApIDogbnVsbH1cbiAgICAgICAgICAgIDwvQ2FyZD5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9HcmlkPlxuICAgICAgPC9HcmlkPlxuICAgICk7XG4gIH1cbn1cblxuRW1haWwucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIHVzZXI6IFByb3BUeXBlcy5zaGFwZSh7XG4gICAgaWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgICB0b2tlbjogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICAgIG5hbWU6IFByb3BUeXBlcy5zdHJpbmcsXG4gICAgZW1haWw6IFByb3BUeXBlcy5zdHJpbmcsXG4gIH0pLmlzUmVxdWlyZWQsXG4gIGRpc2FibGVkOiBQcm9wVHlwZXMuYm9vbC5pc1JlcXVpcmVkLFxufTtcblxuRW1haWwuZGVmYXVsdFByb3BzID0ge1xuICBkaXNhYmxlZDogZmFsc2UsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoRW1haWwpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL1Byb2ZpbGVQYWdlL0VtYWlsLmpzIl0sInNvdXJjZVJvb3QiOiIifQ==