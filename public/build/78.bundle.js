webpackJsonp([78],{

/***/ "./src/client/containers/CoursePage/Discussion/AnswerCard.js":
/*!*******************************************************************!*\
  !*** ./src/client/containers/CoursePage/Discussion/AnswerCard.js ***!
  \*******************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRouter = __webpack_require__(/*! react-router */ "./node_modules/react-router/es/index.js");

var _reactRouterDom = __webpack_require__(/*! react-router-dom */ "./node_modules/react-router-dom/es/index.js");

var _moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");

var _moment2 = _interopRequireDefault(_moment);

var _Paper = __webpack_require__(/*! material-ui/Paper */ "./node_modules/material-ui/Paper/index.js");

var _Paper2 = _interopRequireDefault(_Paper);

var _Avatar = __webpack_require__(/*! material-ui/Avatar */ "./node_modules/material-ui/Avatar/index.js");

var _Avatar2 = _interopRequireDefault(_Avatar);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _CardHeader = __webpack_require__(/*! material-ui/Card/CardHeader */ "./node_modules/material-ui/Card/CardHeader.js");

var _CardHeader2 = _interopRequireDefault(_CardHeader);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {
    paddingBottom: '1%'
  },
  card: {
    borderRadius: '8px'
  },
  avatar: {
    borderRadius: '50%'
  }
};

var AnswerCard = function (_Component) {
  (0, _inherits3.default)(AnswerCard, _Component);

  function AnswerCard(props) {
    (0, _classCallCheck3.default)(this, AnswerCard);

    var _this = (0, _possibleConstructorReturn3.default)(this, (AnswerCard.__proto__ || (0, _getPrototypeOf2.default)(AnswerCard)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onClick = function () {
      var _this$props = _this.props,
          courseId = _this$props.courseId,
          questionId = _this$props.questionId;

      _this.props.history.push('/courses/' + courseId + '/discussion/' + questionId);
    };

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(AnswerCard, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          courseId = _props.courseId,
          authorId = _props.authorId,
          questionId = _props.questionId,
          title = _props.title,
          data = _props.data,
          timestamp = _props.timestamp;
      var hover = this.state.hover;


      var author = { authorId: authorId, username: 'hbarve1', name: 'Himank' };

      return _react2.default.createElement(
        'div',
        {
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Card2.default,
          { raised: hover, className: classes.card, onClick: this.onClick },
          _react2.default.createElement(_CardHeader2.default, {
            avatar: _react2.default.createElement(
              _Paper2.default,
              { className: classes.avatar, elevation: 1 },
              _react2.default.createElement(
                _reactRouterDom.Link,
                { to: '/@' + author.username },
                _react2.default.createElement(_Avatar2.default, {
                  alt: author.name,
                  src: author.avatar || '/public/photos/mayash-logo-transparent.png'
                })
              )
            ),
            title: title,
            subheader: (0, _moment2.default)(timestamp).fromNow()
          })
        )
      );
    }
  }]);
  return AnswerCard;
}(_react.Component);

AnswerCard.propTypes = {
  // match: PropTypes.shape({
  //   params: PropTypes.shape({
  //     courseId: PropTypes.string, // this should be number.
  //     questionId: PropTypes.string, // this should be number.
  //   }).isRequired,
  // }).isRequired,
  history: _propTypes2.default.object.isRequired,
  // location: PropTypes.object.isRequired,

  classes: _propTypes2.default.object.isRequired,

  courseId: _propTypes2.default.number.isRequired,
  authorId: _propTypes2.default.number.isRequired,
  questionId: _propTypes2.default.number.isRequired,
  title: _propTypes2.default.string.isRequired,
  data: _propTypes2.default.object,
  timestamp: _propTypes2.default.string
};

exports.default = (0, _reactRouter.withRouter)((0, _withStyles2.default)(styles)(AnswerCard));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9EaXNjdXNzaW9uL0Fuc3dlckNhcmQuanMiXSwibmFtZXMiOlsic3R5bGVzIiwicm9vdCIsInBhZGRpbmdCb3R0b20iLCJjYXJkIiwiYm9yZGVyUmFkaXVzIiwiYXZhdGFyIiwiQW5zd2VyQ2FyZCIsInByb3BzIiwib25Nb3VzZUVudGVyIiwic2V0U3RhdGUiLCJob3ZlciIsIm9uTW91c2VMZWF2ZSIsIm9uQ2xpY2siLCJjb3Vyc2VJZCIsInF1ZXN0aW9uSWQiLCJoaXN0b3J5IiwicHVzaCIsInN0YXRlIiwiY2xhc3NlcyIsImF1dGhvcklkIiwidGl0bGUiLCJkYXRhIiwidGltZXN0YW1wIiwiYXV0aG9yIiwidXNlcm5hbWUiLCJuYW1lIiwiZnJvbU5vdyIsInByb3BUeXBlcyIsIm9iamVjdCIsImlzUmVxdWlyZWQiLCJudW1iZXIiLCJzdHJpbmciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7O0FBQ0E7O0FBRUE7Ozs7QUFFQTs7OztBQUNBOzs7O0FBRUE7Ozs7QUFDQTs7OztBQUNBOzs7Ozs7QUFkQTs7QUFnQkEsSUFBTUEsU0FBUztBQUNiQyxRQUFNO0FBQ0pDLG1CQUFlO0FBRFgsR0FETztBQUliQyxRQUFNO0FBQ0pDLGtCQUFjO0FBRFYsR0FKTztBQU9iQyxVQUFRO0FBQ05ELGtCQUFjO0FBRFI7QUFQSyxDQUFmOztJQVlNRSxVOzs7QUFDSixzQkFBWUMsS0FBWixFQUFtQjtBQUFBOztBQUFBLDhJQUNYQSxLQURXOztBQUFBLFVBS25CQyxZQUxtQixHQUtKO0FBQUEsYUFBTSxNQUFLQyxRQUFMLENBQWMsRUFBRUMsT0FBTyxJQUFULEVBQWQsQ0FBTjtBQUFBLEtBTEk7O0FBQUEsVUFNbkJDLFlBTm1CLEdBTUo7QUFBQSxhQUFNLE1BQUtGLFFBQUwsQ0FBYyxFQUFFQyxPQUFPLEtBQVQsRUFBZCxDQUFOO0FBQUEsS0FOSTs7QUFBQSxVQU9uQkUsT0FQbUIsR0FPVCxZQUFNO0FBQUEsd0JBQ21CLE1BQUtMLEtBRHhCO0FBQUEsVUFDTk0sUUFETSxlQUNOQSxRQURNO0FBQUEsVUFDSUMsVUFESixlQUNJQSxVQURKOztBQUVkLFlBQUtQLEtBQUwsQ0FBV1EsT0FBWCxDQUFtQkMsSUFBbkIsZUFBb0NILFFBQXBDLG9CQUEyREMsVUFBM0Q7QUFDRCxLQVZrQjs7QUFFakIsVUFBS0csS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBU1E7QUFBQSxtQkFTSCxLQUFLVixLQVRGO0FBQUEsVUFFTFcsT0FGSyxVQUVMQSxPQUZLO0FBQUEsVUFHTEwsUUFISyxVQUdMQSxRQUhLO0FBQUEsVUFJTE0sUUFKSyxVQUlMQSxRQUpLO0FBQUEsVUFLTEwsVUFMSyxVQUtMQSxVQUxLO0FBQUEsVUFNTE0sS0FOSyxVQU1MQSxLQU5LO0FBQUEsVUFPTEMsSUFQSyxVQU9MQSxJQVBLO0FBQUEsVUFRTEMsU0FSSyxVQVFMQSxTQVJLO0FBQUEsVUFVQ1osS0FWRCxHQVVXLEtBQUtPLEtBVmhCLENBVUNQLEtBVkQ7OztBQVlQLFVBQU1hLFNBQVMsRUFBRUosa0JBQUYsRUFBWUssVUFBVSxTQUF0QixFQUFpQ0MsTUFBTSxRQUF2QyxFQUFmOztBQUVBLGFBQ0U7QUFBQTtBQUFBO0FBQ0UscUJBQVdQLFFBQVFqQixJQURyQjtBQUVFLHdCQUFjLEtBQUtPLFlBRnJCO0FBR0Usd0JBQWMsS0FBS0c7QUFIckI7QUFLRTtBQUFBO0FBQUEsWUFBTSxRQUFRRCxLQUFkLEVBQXFCLFdBQVdRLFFBQVFmLElBQXhDLEVBQThDLFNBQVMsS0FBS1MsT0FBNUQ7QUFDRTtBQUNFLG9CQUNFO0FBQUE7QUFBQSxnQkFBTyxXQUFXTSxRQUFRYixNQUExQixFQUFrQyxXQUFXLENBQTdDO0FBQ0U7QUFBQTtBQUFBLGtCQUFNLFdBQVNrQixPQUFPQyxRQUF0QjtBQUNFO0FBQ0UsdUJBQUtELE9BQU9FLElBRGQ7QUFFRSx1QkFDRUYsT0FBT2xCLE1BQVAsSUFDQTtBQUpKO0FBREY7QUFERixhQUZKO0FBY0UsbUJBQU9lLEtBZFQ7QUFlRSx1QkFBVyxzQkFBT0UsU0FBUCxFQUFrQkksT0FBbEI7QUFmYjtBQURGO0FBTEYsT0FERjtBQTJCRDs7Ozs7QUFHSHBCLFdBQVdxQixTQUFYLEdBQXVCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBWixXQUFTLG9CQUFVYSxNQUFWLENBQWlCQyxVQVBMO0FBUXJCOztBQUVBWCxXQUFTLG9CQUFVVSxNQUFWLENBQWlCQyxVQVZMOztBQVlyQmhCLFlBQVUsb0JBQVVpQixNQUFWLENBQWlCRCxVQVpOO0FBYXJCVixZQUFVLG9CQUFVVyxNQUFWLENBQWlCRCxVQWJOO0FBY3JCZixjQUFZLG9CQUFVZ0IsTUFBVixDQUFpQkQsVUFkUjtBQWVyQlQsU0FBTyxvQkFBVVcsTUFBVixDQUFpQkYsVUFmSDtBQWdCckJSLFFBQU0sb0JBQVVPLE1BaEJLO0FBaUJyQk4sYUFBVyxvQkFBVVM7QUFqQkEsQ0FBdkI7O2tCQW9CZSw2QkFBVywwQkFBVy9CLE1BQVgsRUFBbUJNLFVBQW5CLENBQVgsQyIsImZpbGUiOiI3OC5idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJztcbmltcG9ydCB7IHdpdGhSb3V0ZXIgfSBmcm9tICdyZWFjdC1yb3V0ZXInO1xuaW1wb3J0IHsgTGluayB9IGZyb20gJ3JlYWN0LXJvdXRlci1kb20nO1xuXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XG5cbmltcG9ydCBQYXBlciBmcm9tICdtYXRlcmlhbC11aS9QYXBlcic7XG5pbXBvcnQgQXZhdGFyIGZyb20gJ21hdGVyaWFsLXVpL0F2YXRhcic7XG5cbmltcG9ydCB3aXRoU3R5bGVzIGZyb20gJ21hdGVyaWFsLXVpL3N0eWxlcy93aXRoU3R5bGVzJztcbmltcG9ydCBDYXJkIGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuaW1wb3J0IENhcmRIZWFkZXIgZnJvbSAnbWF0ZXJpYWwtdWkvQ2FyZC9DYXJkSGVhZGVyJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgcGFkZGluZ0JvdHRvbTogJzElJyxcbiAgfSxcbiAgY2FyZDoge1xuICAgIGJvcmRlclJhZGl1czogJzhweCcsXG4gIH0sXG4gIGF2YXRhcjoge1xuICAgIGJvcmRlclJhZGl1czogJzUwJScsXG4gIH0sXG59O1xuXG5jbGFzcyBBbnN3ZXJDYXJkIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgb25Nb3VzZUVudGVyID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiB0cnVlIH0pO1xuICBvbk1vdXNlTGVhdmUgPSAoKSA9PiB0aGlzLnNldFN0YXRlKHsgaG92ZXI6IGZhbHNlIH0pO1xuICBvbkNsaWNrID0gKCkgPT4ge1xuICAgIGNvbnN0IHsgY291cnNlSWQsIHF1ZXN0aW9uSWQgfSA9IHRoaXMucHJvcHM7XG4gICAgdGhpcy5wcm9wcy5oaXN0b3J5LnB1c2goYC9jb3Vyc2VzLyR7Y291cnNlSWR9L2Rpc2N1c3Npb24vJHtxdWVzdGlvbklkfWApO1xuICB9O1xuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zdCB7XG4gICAgICBjbGFzc2VzLFxuICAgICAgY291cnNlSWQsXG4gICAgICBhdXRob3JJZCxcbiAgICAgIHF1ZXN0aW9uSWQsXG4gICAgICB0aXRsZSxcbiAgICAgIGRhdGEsXG4gICAgICB0aW1lc3RhbXAsXG4gICAgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBob3ZlciB9ID0gdGhpcy5zdGF0ZTtcblxuICAgIGNvbnN0IGF1dGhvciA9IHsgYXV0aG9ySWQsIHVzZXJuYW1lOiAnaGJhcnZlMScsIG5hbWU6ICdIaW1hbmsnIH07XG5cbiAgICByZXR1cm4gKFxuICAgICAgPGRpdlxuICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH1cbiAgICAgICAgb25Nb3VzZUVudGVyPXt0aGlzLm9uTW91c2VFbnRlcn1cbiAgICAgICAgb25Nb3VzZUxlYXZlPXt0aGlzLm9uTW91c2VMZWF2ZX1cbiAgICAgID5cbiAgICAgICAgPENhcmQgcmFpc2VkPXtob3Zlcn0gY2xhc3NOYW1lPXtjbGFzc2VzLmNhcmR9IG9uQ2xpY2s9e3RoaXMub25DbGlja30+XG4gICAgICAgICAgPENhcmRIZWFkZXJcbiAgICAgICAgICAgIGF2YXRhcj17XG4gICAgICAgICAgICAgIDxQYXBlciBjbGFzc05hbWU9e2NsYXNzZXMuYXZhdGFyfSBlbGV2YXRpb249ezF9PlxuICAgICAgICAgICAgICAgIDxMaW5rIHRvPXtgL0Ake2F1dGhvci51c2VybmFtZX1gfT5cbiAgICAgICAgICAgICAgICAgIDxBdmF0YXJcbiAgICAgICAgICAgICAgICAgICAgYWx0PXthdXRob3IubmFtZX1cbiAgICAgICAgICAgICAgICAgICAgc3JjPXtcbiAgICAgICAgICAgICAgICAgICAgICBhdXRob3IuYXZhdGFyIHx8XG4gICAgICAgICAgICAgICAgICAgICAgJy9wdWJsaWMvcGhvdG9zL21heWFzaC1sb2dvLXRyYW5zcGFyZW50LnBuZydcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgICAgIDwvUGFwZXI+XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aXRsZT17dGl0bGV9XG4gICAgICAgICAgICBzdWJoZWFkZXI9e21vbWVudCh0aW1lc3RhbXApLmZyb21Ob3coKX1cbiAgICAgICAgICAvPlxuICAgICAgICA8L0NhcmQ+XG4gICAgICA8L2Rpdj5cbiAgICApO1xuICB9XG59XG5cbkFuc3dlckNhcmQucHJvcFR5cGVzID0ge1xuICAvLyBtYXRjaDogUHJvcFR5cGVzLnNoYXBlKHtcbiAgLy8gICBwYXJhbXM6IFByb3BUeXBlcy5zaGFwZSh7XG4gIC8vICAgICBjb3Vyc2VJZDogUHJvcFR5cGVzLnN0cmluZywgLy8gdGhpcyBzaG91bGQgYmUgbnVtYmVyLlxuICAvLyAgICAgcXVlc3Rpb25JZDogUHJvcFR5cGVzLnN0cmluZywgLy8gdGhpcyBzaG91bGQgYmUgbnVtYmVyLlxuICAvLyAgIH0pLmlzUmVxdWlyZWQsXG4gIC8vIH0pLmlzUmVxdWlyZWQsXG4gIGhpc3Rvcnk6IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcbiAgLy8gbG9jYXRpb246IFByb3BUeXBlcy5vYmplY3QuaXNSZXF1aXJlZCxcblxuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgY291cnNlSWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgYXV0aG9ySWQ6IFByb3BUeXBlcy5udW1iZXIuaXNSZXF1aXJlZCxcbiAgcXVlc3Rpb25JZDogUHJvcFR5cGVzLm51bWJlci5pc1JlcXVpcmVkLFxuICB0aXRsZTogUHJvcFR5cGVzLnN0cmluZy5pc1JlcXVpcmVkLFxuICBkYXRhOiBQcm9wVHlwZXMub2JqZWN0LFxuICB0aW1lc3RhbXA6IFByb3BUeXBlcy5zdHJpbmcsXG59O1xuXG5leHBvcnQgZGVmYXVsdCB3aXRoUm91dGVyKHdpdGhTdHlsZXMoc3R5bGVzKShBbnN3ZXJDYXJkKSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvQ291cnNlUGFnZS9EaXNjdXNzaW9uL0Fuc3dlckNhcmQuanMiXSwic291cmNlUm9vdCI6IiJ9