webpackJsonp([3],{

/***/ "./node_modules/material-ui-icons/AttachFile.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/AttachFile.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z' });

var AttachFile = function AttachFile(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

AttachFile = (0, _pure2.default)(AttachFile);
AttachFile.muiName = 'SvgIcon';

exports.default = AttachFile;

/***/ }),

/***/ "./node_modules/material-ui-icons/Code.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui-icons/Code.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M9.4 16.6L4.8 12l4.6-4.6L8 6l-6 6 6 6 1.4-1.4zm5.2 0l4.6-4.6-4.6-4.6L16 6l6 6-6 6-1.4-1.4z' });

var Code = function Code(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Code = (0, _pure2.default)(Code);
Code.muiName = 'SvgIcon';

exports.default = Code;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignCenter.js":
/*!*************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignCenter.js ***!
  \*************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M7 15v2h10v-2H7zm-4 6h18v-2H3v2zm0-8h18v-2H3v2zm4-6v2h10V7H7zM3 3v2h18V3H3z' });

var FormatAlignCenter = function FormatAlignCenter(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignCenter = (0, _pure2.default)(FormatAlignCenter);
FormatAlignCenter.muiName = 'SvgIcon';

exports.default = FormatAlignCenter;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignJustify.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignJustify.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18v-2H3v2zm0-4h18V7H3v2zm0-6v2h18V3H3z' });

var FormatAlignJustify = function FormatAlignJustify(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignJustify = (0, _pure2.default)(FormatAlignJustify);
FormatAlignJustify.muiName = 'SvgIcon';

exports.default = FormatAlignJustify;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignLeft.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignLeft.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15 15H3v2h12v-2zm0-8H3v2h12V7zM3 13h18v-2H3v2zm0 8h18v-2H3v2zM3 3v2h18V3H3z' });

var FormatAlignLeft = function FormatAlignLeft(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignLeft = (0, _pure2.default)(FormatAlignLeft);
FormatAlignLeft.muiName = 'SvgIcon';

exports.default = FormatAlignLeft;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatAlignRight.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatAlignRight.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3 21h18v-2H3v2zm6-4h12v-2H9v2zm-6-4h18v-2H3v2zm6-4h12V7H9v2zM3 3v2h18V3H3z' });

var FormatAlignRight = function FormatAlignRight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatAlignRight = (0, _pure2.default)(FormatAlignRight);
FormatAlignRight.muiName = 'SvgIcon';

exports.default = FormatAlignRight;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatBold.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatBold.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M15.6 10.79c.97-.67 1.65-1.77 1.65-2.79 0-2.26-1.75-4-4-4H7v14h7.04c2.09 0 3.71-1.7 3.71-3.79 0-1.52-.86-2.82-2.15-3.42zM10 6.5h3c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5h-3v-3zm3.5 9H10v-3h3.5c.83 0 1.5.67 1.5 1.5s-.67 1.5-1.5 1.5z' });

var FormatBold = function FormatBold(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatBold = (0, _pure2.default)(FormatBold);
FormatBold.muiName = 'SvgIcon';

exports.default = FormatBold;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatItalic.js":
/*!********************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatItalic.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M10 4v3h2.21l-3.42 8H6v3h8v-3h-2.21l3.42-8H18V4z' });

var FormatItalic = function FormatItalic(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatItalic = (0, _pure2.default)(FormatItalic);
FormatItalic.muiName = 'SvgIcon';

exports.default = FormatItalic;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListBulleted.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListBulleted.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M4 10.5c-.83 0-1.5.67-1.5 1.5s.67 1.5 1.5 1.5 1.5-.67 1.5-1.5-.67-1.5-1.5-1.5zm0-6c-.83 0-1.5.67-1.5 1.5S3.17 7.5 4 7.5 5.5 6.83 5.5 6 4.83 4.5 4 4.5zm0 12c-.83 0-1.5.68-1.5 1.5s.68 1.5 1.5 1.5 1.5-.68 1.5-1.5-.67-1.5-1.5-1.5zM7 19h14v-2H7v2zm0-6h14v-2H7v2zm0-8v2h14V5H7z' });

var FormatListBulleted = function FormatListBulleted(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListBulleted = (0, _pure2.default)(FormatListBulleted);
FormatListBulleted.muiName = 'SvgIcon';

exports.default = FormatListBulleted;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatListNumbered.js":
/*!**************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatListNumbered.js ***!
  \**************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M2 17h2v.5H3v1h1v.5H2v1h3v-4H2v1zm1-9h1V4H2v1h1v3zm-1 3h1.8L2 13.1v.9h3v-1H3.2L5 10.9V10H2v1zm5-6v2h14V5H7zm0 14h14v-2H7v2zm0-6h14v-2H7v2z' });

var FormatListNumbered = function FormatListNumbered(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatListNumbered = (0, _pure2.default)(FormatListNumbered);
FormatListNumbered.muiName = 'SvgIcon';

exports.default = FormatListNumbered;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatQuote.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatQuote.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 17h3l2-4V7H5v6h3zm8 0h3l2-4V7h-6v6h3z' });

var FormatQuote = function FormatQuote(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatQuote = (0, _pure2.default)(FormatQuote);
FormatQuote.muiName = 'SvgIcon';

exports.default = FormatQuote;

/***/ }),

/***/ "./node_modules/material-ui-icons/FormatUnderlined.js":
/*!************************************************************!*\
  !*** ./node_modules/material-ui-icons/FormatUnderlined.js ***!
  \************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M12 17c3.31 0 6-2.69 6-6V3h-2.5v8c0 1.93-1.57 3.5-3.5 3.5S8.5 12.93 8.5 11V3H6v8c0 3.31 2.69 6 6 6zm-7 2v2h14v-2H5z' });

var FormatUnderlined = function FormatUnderlined(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

FormatUnderlined = (0, _pure2.default)(FormatUnderlined);
FormatUnderlined.muiName = 'SvgIcon';

exports.default = FormatUnderlined;

/***/ }),

/***/ "./node_modules/material-ui-icons/Functions.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Functions.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M18 4H6v2l6.5 6L6 18v2h12v-3h-7l5-5-5-5h7z' });

var Functions = function Functions(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Functions = (0, _pure2.default)(Functions);
Functions.muiName = 'SvgIcon';

exports.default = Functions;

/***/ }),

/***/ "./node_modules/material-ui-icons/Highlight.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui-icons/Highlight.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M6 14l3 3v5h6v-5l3-3V9H6zm5-12h2v3h-2zM3.5 5.875L4.914 4.46l2.12 2.122L5.62 7.997zm13.46.71l2.123-2.12 1.414 1.414L18.375 8z' });

var Highlight = function Highlight(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Highlight = (0, _pure2.default)(Highlight);
Highlight.muiName = 'SvgIcon';

exports.default = Highlight;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertComment.js":
/*!*********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertComment.js ***!
  \*********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M20 2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h14l4 4V4c0-1.1-.9-2-2-2zm-2 12H6v-2h12v2zm0-3H6V9h12v2zm0-3H6V6h12v2z' });

var InsertComment = function InsertComment(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertComment = (0, _pure2.default)(InsertComment);
InsertComment.muiName = 'SvgIcon';

exports.default = InsertComment;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertEmoticon.js":
/*!**********************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertEmoticon.js ***!
  \**********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8zm3.5-9c.83 0 1.5-.67 1.5-1.5S16.33 8 15.5 8 14 8.67 14 9.5s.67 1.5 1.5 1.5zm-7 0c.83 0 1.5-.67 1.5-1.5S9.33 8 8.5 8 7 8.67 7 9.5 7.67 11 8.5 11zm3.5 6.5c2.33 0 4.31-1.46 5.11-3.5H6.89c.8 2.04 2.78 3.5 5.11 3.5z' });

var InsertEmoticon = function InsertEmoticon(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertEmoticon = (0, _pure2.default)(InsertEmoticon);
InsertEmoticon.muiName = 'SvgIcon';

exports.default = InsertEmoticon;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertLink.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertLink.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M3.9 12c0-1.71 1.39-3.1 3.1-3.1h4V7H7c-2.76 0-5 2.24-5 5s2.24 5 5 5h4v-1.9H7c-1.71 0-3.1-1.39-3.1-3.1zM8 13h8v-2H8v2zm9-6h-4v1.9h4c1.71 0 3.1 1.39 3.1 3.1s-1.39 3.1-3.1 3.1h-4V17h4c2.76 0 5-2.24 5-5s-2.24-5-5-5z' });

var InsertLink = function InsertLink(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertLink = (0, _pure2.default)(InsertLink);
InsertLink.muiName = 'SvgIcon';

exports.default = InsertLink;

/***/ }),

/***/ "./node_modules/material-ui-icons/InsertPhoto.js":
/*!*******************************************************!*\
  !*** ./node_modules/material-ui-icons/InsertPhoto.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M21 19V5c0-1.1-.9-2-2-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2zM8.5 13.5l2.5 3.01L14.5 12l4.5 6H5l3.5-4.5z' });

var InsertPhoto = function InsertPhoto(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

InsertPhoto = (0, _pure2.default)(InsertPhoto);
InsertPhoto.muiName = 'SvgIcon';

exports.default = InsertPhoto;

/***/ }),

/***/ "./node_modules/material-ui-icons/Title.js":
/*!*************************************************!*\
  !*** ./node_modules/material-ui-icons/Title.js ***!
  \*************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _pure = __webpack_require__(/*! recompose/pure */ "./node_modules/recompose/pure.js");

var _pure2 = _interopRequireDefault(_pure);

var _SvgIcon = __webpack_require__(/*! material-ui/SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _SvgIcon2 = _interopRequireDefault(_SvgIcon);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _ref = _react2.default.createElement('path', { d: 'M5 4v3h5.5v12h3V7H19V4z' });

var Title = function Title(props) {
  return _react2.default.createElement(
    _SvgIcon2.default,
    props,
    _ref
  );
};

Title = (0, _pure2.default)(Title);
Title.muiName = 'SvgIcon';

exports.default = Title;

/***/ }),

/***/ "./node_modules/material-ui/Icon/Icon.js":
/*!***********************************************!*\
  !*** ./node_modules/material-ui/Icon/Icon.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      userSelect: 'none'
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * The name of the icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired
};

var Icon = function (_React$Component) {
  (0, _inherits3.default)(Icon, _React$Component);

  function Icon() {
    (0, _classCallCheck3.default)(this, Icon);
    return (0, _possibleConstructorReturn3.default)(this, (Icon.__proto__ || (0, _getPrototypeOf2.default)(Icon)).apply(this, arguments));
  }

  (0, _createClass3.default)(Icon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color']);


      var className = (0, _classnames2.default)('material-icons', classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'span',
        (0, _extends3.default)({ className: className, 'aria-hidden': 'true' }, other),
        children
      );
    }
  }]);
  return Icon;
}(_react2.default.Component);

Icon.defaultProps = {
  color: 'inherit'
};
Icon.muiName = 'Icon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIcon' })(Icon);

/***/ }),

/***/ "./node_modules/material-ui/Icon/index.js":
/*!************************************************!*\
  !*** ./node_modules/material-ui/Icon/index.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Icon = __webpack_require__(/*! ./Icon */ "./node_modules/material-ui/Icon/Icon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Icon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/IconButton/IconButton.js":
/*!***********************************************************!*\
  !*** ./node_modules/material-ui/IconButton/IconButton.js ***!
  \***********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _ButtonBase = __webpack_require__(/*! ../ButtonBase */ "./node_modules/material-ui/ButtonBase/index.js");

var _ButtonBase2 = _interopRequireDefault(_ButtonBase);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

var _Icon = __webpack_require__(/*! ../Icon */ "./node_modules/material-ui/Icon/index.js");

var _Icon2 = _interopRequireDefault(_Icon);

__webpack_require__(/*! ../SvgIcon */ "./node_modules/material-ui/SvgIcon/index.js");

var _reactHelpers = __webpack_require__(/*! ../utils/reactHelpers */ "./node_modules/material-ui/utils/reactHelpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any; //  weak
// @inheritedComponent ButtonBase

// Ensure CSS specificity


var styles = exports.styles = function styles(theme) {
  return {
    root: {
      textAlign: 'center',
      flex: '0 0 auto',
      fontSize: theme.typography.pxToRem(24),
      width: theme.spacing.unit * 6,
      height: theme.spacing.unit * 6,
      padding: 0,
      borderRadius: '50%',
      color: theme.palette.action.active,
      transition: theme.transitions.create('background-color', {
        duration: theme.transitions.duration.shortest
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    },
    colorInherit: {
      color: 'inherit'
    },
    disabled: {
      color: theme.palette.action.disabled
    },
    label: {
      width: '100%',
      display: 'flex',
      alignItems: 'inherit',
      justifyContent: 'inherit'
    },
    icon: {
      width: '1em',
      height: '1em'
    },
    keyboardFocused: {
      backgroundColor: theme.palette.text.divider
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Use that property to pass a ref callback to the native button component.
   */
  buttonRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func,

  /**
   * The icon element.
   * If a string is provided, it will be used as an icon font ligature.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node),

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['default', 'inherit', 'primary', 'contrast', 'accent']).isRequired,

  /**
   * If `true`, the button will be disabled.
   */
  disabled: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * If `true`, the ripple will be disabled.
   */
  disableRipple: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").bool.isRequired,

  /**
   * Use that property to pass a ref callback to the root component.
   */
  rootRef: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").func
};

/**
 * Refer to the [Icons](/style/icons) section of the documentation
 * regarding the available icon options.
 */
var IconButton = function (_React$Component) {
  (0, _inherits3.default)(IconButton, _React$Component);

  function IconButton() {
    (0, _classCallCheck3.default)(this, IconButton);
    return (0, _possibleConstructorReturn3.default)(this, (IconButton.__proto__ || (0, _getPrototypeOf2.default)(IconButton)).apply(this, arguments));
  }

  (0, _createClass3.default)(IconButton, [{
    key: 'render',
    value: function render() {
      var _classNames;

      var _props = this.props,
          buttonRef = _props.buttonRef,
          children = _props.children,
          classes = _props.classes,
          className = _props.className,
          color = _props.color,
          disabled = _props.disabled,
          rootRef = _props.rootRef,
          other = (0, _objectWithoutProperties3.default)(_props, ['buttonRef', 'children', 'classes', 'className', 'color', 'disabled', 'rootRef']);


      return _react2.default.createElement(
        _ButtonBase2.default,
        (0, _extends3.default)({
          className: (0, _classnames2.default)(classes.root, (_classNames = {}, (0, _defineProperty3.default)(_classNames, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'default'), (0, _defineProperty3.default)(_classNames, classes.disabled, disabled), _classNames), className),
          centerRipple: true,
          keyboardFocusedClassName: classes.keyboardFocused,
          disabled: disabled
        }, other, {
          rootRef: buttonRef,
          ref: rootRef
        }),
        _react2.default.createElement(
          'span',
          { className: classes.label },
          typeof children === 'string' ? _react2.default.createElement(
            _Icon2.default,
            { className: classes.icon },
            children
          ) : _react2.default.Children.map(children, function (child) {
            if ((0, _reactHelpers.isMuiElement)(child, ['Icon', 'SvgIcon'])) {
              return _react2.default.cloneElement(child, {
                className: (0, _classnames2.default)(classes.icon, child.props.className)
              });
            }

            return child;
          })
        )
      );
    }
  }]);
  return IconButton;
}(_react2.default.Component);

IconButton.defaultProps = {
  color: 'default',
  disabled: false,
  disableRipple: false
};
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiIconButton' })(IconButton);

/***/ }),

/***/ "./node_modules/material-ui/IconButton/index.js":
/*!******************************************************!*\
  !*** ./node_modules/material-ui/IconButton/index.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _IconButton = __webpack_require__(/*! ./IconButton */ "./node_modules/material-ui/IconButton/IconButton.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_IconButton).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/SvgIcon.js":
/*!*****************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/SvgIcon.js ***!
  \*****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.styles = undefined;

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ "./node_modules/babel-runtime/helpers/defineProperty.js");

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _objectWithoutProperties2 = __webpack_require__(/*! babel-runtime/helpers/objectWithoutProperties */ "./node_modules/babel-runtime/helpers/objectWithoutProperties.js");

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! ../styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _helpers = __webpack_require__(/*! ../utils/helpers */ "./node_modules/material-ui/utils/helpers.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var babelPluginFlowReactPropTypes_proptype_Node = __webpack_require__(/*! react */ "./node_modules/react/react.js").babelPluginFlowReactPropTypes_proptype_Node || __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").any;

var styles = exports.styles = function styles(theme) {
  return {
    root: {
      display: 'inline-block',
      fill: 'currentColor',
      height: 24,
      width: 24,
      userSelect: 'none',
      flexShrink: 0,
      transition: theme.transitions.create('fill', {
        duration: theme.transitions.duration.shorter
      })
    },
    colorAccent: {
      color: theme.palette.secondary.A200
    },
    colorAction: {
      color: theme.palette.action.active
    },
    colorContrast: {
      color: theme.palette.getContrastText(theme.palette.primary[500])
    },
    colorDisabled: {
      color: theme.palette.action.disabled
    },
    colorError: {
      color: theme.palette.error[500]
    },
    colorPrimary: {
      color: theme.palette.primary[500]
    }
  };
};

var babelPluginFlowReactPropTypes_proptype_Color = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']);

var babelPluginFlowReactPropTypes_proptype_Props = {
  /**
   * Elements passed into the SVG Icon.
   */
  children: typeof babelPluginFlowReactPropTypes_proptype_Node === 'function' ? babelPluginFlowReactPropTypes_proptype_Node.isRequired ? babelPluginFlowReactPropTypes_proptype_Node.isRequired : babelPluginFlowReactPropTypes_proptype_Node : __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").shape(babelPluginFlowReactPropTypes_proptype_Node).isRequired,

  /**
   * Useful to extend the style applied to components.
   */
  classes: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").object,

  /**
   * @ignore
   */
  className: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * The color of the component. It's using the theme palette when that makes sense.
   */
  color: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").oneOf(['inherit', 'accent', 'action', 'contrast', 'disabled', 'error', 'primary']).isRequired,

  /**
   * Provides a human-readable title for the element that contains it.
   * https://www.w3.org/TR/SVG-access/#Equivalent
   */
  titleAccess: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string,

  /**
   * Allows you to redefine what the coordinates without units mean inside an svg element.
   * For example, if the SVG element is 500 (width) by 200 (height),
   * and you pass viewBox="0 0 50 20",
   * this means that the coordinates inside the svg will go from the top left corner (0,0)
   * to bottom right (50,20) and each unit will be worth 10px.
   */
  viewBox: __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js").string.isRequired
};

var SvgIcon = function (_React$Component) {
  (0, _inherits3.default)(SvgIcon, _React$Component);

  function SvgIcon() {
    (0, _classCallCheck3.default)(this, SvgIcon);
    return (0, _possibleConstructorReturn3.default)(this, (SvgIcon.__proto__ || (0, _getPrototypeOf2.default)(SvgIcon)).apply(this, arguments));
  }

  (0, _createClass3.default)(SvgIcon, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          classes = _props.classes,
          classNameProp = _props.className,
          color = _props.color,
          titleAccess = _props.titleAccess,
          viewBox = _props.viewBox,
          other = (0, _objectWithoutProperties3.default)(_props, ['children', 'classes', 'className', 'color', 'titleAccess', 'viewBox']);


      var className = (0, _classnames2.default)(classes.root, (0, _defineProperty3.default)({}, classes['color' + (0, _helpers.capitalizeFirstLetter)(color)], color !== 'inherit'), classNameProp);

      return _react2.default.createElement(
        'svg',
        (0, _extends3.default)({
          className: className,
          focusable: 'false',
          viewBox: viewBox,
          'aria-hidden': titleAccess ? 'false' : 'true'
        }, other),
        titleAccess ? _react2.default.createElement(
          'title',
          null,
          titleAccess
        ) : null,
        children
      );
    }
  }]);
  return SvgIcon;
}(_react2.default.Component);

SvgIcon.defaultProps = {
  viewBox: '0 0 24 24',
  color: 'inherit'
};
SvgIcon.muiName = 'SvgIcon';
exports.default = (0, _withStyles2.default)(styles, { name: 'MuiSvgIcon' })(SvgIcon);

/***/ }),

/***/ "./node_modules/material-ui/SvgIcon/index.js":
/*!***************************************************!*\
  !*** ./node_modules/material-ui/SvgIcon/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _SvgIcon = __webpack_require__(/*! ./SvgIcon */ "./node_modules/material-ui/SvgIcon/SvgIcon.js");

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_SvgIcon).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/***/ }),

/***/ "./node_modules/recompose/createEagerFactory.js":
/*!******************************************************!*\
  !*** ./node_modules/recompose/createEagerFactory.js ***!
  \******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _createEagerElementUtil = __webpack_require__(/*! ./utils/createEagerElementUtil */ "./node_modules/recompose/utils/createEagerElementUtil.js");

var _createEagerElementUtil2 = _interopRequireDefault(_createEagerElementUtil);

var _isReferentiallyTransparentFunctionComponent = __webpack_require__(/*! ./isReferentiallyTransparentFunctionComponent */ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js");

var _isReferentiallyTransparentFunctionComponent2 = _interopRequireDefault(_isReferentiallyTransparentFunctionComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createFactory = function createFactory(type) {
  var isReferentiallyTransparent = (0, _isReferentiallyTransparentFunctionComponent2.default)(type);
  return function (p, c) {
    return (0, _createEagerElementUtil2.default)(false, isReferentiallyTransparent, type, p, c);
  };
};

exports.default = createFactory;

/***/ }),

/***/ "./node_modules/recompose/getDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/getDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var getDisplayName = function getDisplayName(Component) {
  if (typeof Component === 'string') {
    return Component;
  }

  if (!Component) {
    return undefined;
  }

  return Component.displayName || Component.name || 'Component';
};

exports.default = getDisplayName;

/***/ }),

/***/ "./node_modules/recompose/isClassComponent.js":
/*!****************************************************!*\
  !*** ./node_modules/recompose/isClassComponent.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var isClassComponent = function isClassComponent(Component) {
  return Boolean(Component && Component.prototype && _typeof(Component.prototype.isReactComponent) === 'object');
};

exports.default = isClassComponent;

/***/ }),

/***/ "./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js":
/*!*******************************************************************************!*\
  !*** ./node_modules/recompose/isReferentiallyTransparentFunctionComponent.js ***!
  \*******************************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _isClassComponent = __webpack_require__(/*! ./isClassComponent */ "./node_modules/recompose/isClassComponent.js");

var _isClassComponent2 = _interopRequireDefault(_isClassComponent);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isReferentiallyTransparentFunctionComponent = function isReferentiallyTransparentFunctionComponent(Component) {
  return Boolean(typeof Component === 'function' && !(0, _isClassComponent2.default)(Component) && !Component.defaultProps && !Component.contextTypes && ("development" === 'production' || !Component.propTypes));
};

exports.default = isReferentiallyTransparentFunctionComponent;

/***/ }),

/***/ "./node_modules/recompose/pure.js":
/*!****************************************!*\
  !*** ./node_modules/recompose/pure.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shouldUpdate = __webpack_require__(/*! ./shouldUpdate */ "./node_modules/recompose/shouldUpdate.js");

var _shouldUpdate2 = _interopRequireDefault(_shouldUpdate);

var _shallowEqual = __webpack_require__(/*! ./shallowEqual */ "./node_modules/recompose/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var pure = function pure(BaseComponent) {
  var hoc = (0, _shouldUpdate2.default)(function (props, nextProps) {
    return !(0, _shallowEqual2.default)(props, nextProps);
  });

  if (true) {
    return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'pure'))(hoc(BaseComponent));
  }

  return hoc(BaseComponent);
};

exports.default = pure;

/***/ }),

/***/ "./node_modules/recompose/setDisplayName.js":
/*!**************************************************!*\
  !*** ./node_modules/recompose/setDisplayName.js ***!
  \**************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _setStatic = __webpack_require__(/*! ./setStatic */ "./node_modules/recompose/setStatic.js");

var _setStatic2 = _interopRequireDefault(_setStatic);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setDisplayName = function setDisplayName(displayName) {
  return (0, _setStatic2.default)('displayName', displayName);
};

exports.default = setDisplayName;

/***/ }),

/***/ "./node_modules/recompose/setStatic.js":
/*!*********************************************!*\
  !*** ./node_modules/recompose/setStatic.js ***!
  \*********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var setStatic = function setStatic(key, value) {
  return function (BaseComponent) {
    /* eslint-disable no-param-reassign */
    BaseComponent[key] = value;
    /* eslint-enable no-param-reassign */
    return BaseComponent;
  };
};

exports.default = setStatic;

/***/ }),

/***/ "./node_modules/recompose/shallowEqual.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shallowEqual.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _shallowEqual = __webpack_require__(/*! fbjs/lib/shallowEqual */ "./node_modules/fbjs/lib/shallowEqual.js");

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _shallowEqual2.default;

/***/ }),

/***/ "./node_modules/recompose/shouldUpdate.js":
/*!************************************************!*\
  !*** ./node_modules/recompose/shouldUpdate.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _setDisplayName = __webpack_require__(/*! ./setDisplayName */ "./node_modules/recompose/setDisplayName.js");

var _setDisplayName2 = _interopRequireDefault(_setDisplayName);

var _wrapDisplayName = __webpack_require__(/*! ./wrapDisplayName */ "./node_modules/recompose/wrapDisplayName.js");

var _wrapDisplayName2 = _interopRequireDefault(_wrapDisplayName);

var _createEagerFactory = __webpack_require__(/*! ./createEagerFactory */ "./node_modules/recompose/createEagerFactory.js");

var _createEagerFactory2 = _interopRequireDefault(_createEagerFactory);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var shouldUpdate = function shouldUpdate(test) {
  return function (BaseComponent) {
    var factory = (0, _createEagerFactory2.default)(BaseComponent);

    var ShouldUpdate = function (_Component) {
      _inherits(ShouldUpdate, _Component);

      function ShouldUpdate() {
        _classCallCheck(this, ShouldUpdate);

        return _possibleConstructorReturn(this, _Component.apply(this, arguments));
      }

      ShouldUpdate.prototype.shouldComponentUpdate = function shouldComponentUpdate(nextProps) {
        return test(this.props, nextProps);
      };

      ShouldUpdate.prototype.render = function render() {
        return factory(this.props);
      };

      return ShouldUpdate;
    }(_react.Component);

    if (true) {
      return (0, _setDisplayName2.default)((0, _wrapDisplayName2.default)(BaseComponent, 'shouldUpdate'))(ShouldUpdate);
    }
    return ShouldUpdate;
  };
};

exports.default = shouldUpdate;

/***/ }),

/***/ "./node_modules/recompose/utils/createEagerElementUtil.js":
/*!****************************************************************!*\
  !*** ./node_modules/recompose/utils/createEagerElementUtil.js ***!
  \****************************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createEagerElementUtil = function createEagerElementUtil(hasKey, isReferentiallyTransparent, type, props, children) {
  if (!hasKey && isReferentiallyTransparent) {
    if (children) {
      return type(_extends({}, props, { children: children }));
    }
    return type(props);
  }

  var Component = type;

  if (children) {
    return _react2.default.createElement(
      Component,
      props,
      children
    );
  }

  return _react2.default.createElement(Component, props);
};

exports.default = createEagerElementUtil;

/***/ }),

/***/ "./node_modules/recompose/wrapDisplayName.js":
/*!***************************************************!*\
  !*** ./node_modules/recompose/wrapDisplayName.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var _getDisplayName = __webpack_require__(/*! ./getDisplayName */ "./node_modules/recompose/getDisplayName.js");

var _getDisplayName2 = _interopRequireDefault(_getDisplayName);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var wrapDisplayName = function wrapDisplayName(BaseComponent, hocName) {
  return hocName + '(' + (0, _getDisplayName2.default)(BaseComponent) + ')';
};

exports.default = wrapDisplayName;

/***/ }),

/***/ "./src/client/containers/ResumePage/index.js":
/*!***************************************************!*\
  !*** ./src/client/containers/ResumePage/index.js ***!
  \***************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _keys = __webpack_require__(/*! babel-runtime/core-js/object/keys */ "./node_modules/babel-runtime/core-js/object/keys.js");

var _keys2 = _interopRequireDefault(_keys);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _Card = __webpack_require__(/*! material-ui/Card */ "./node_modules/material-ui/Card/index.js");

var _Card2 = _interopRequireDefault(_Card);

var _mayashEditor = __webpack_require__(/*! ../../../lib/mayash-editor */ "./src/lib/mayash-editor/index.js");

var _mayashEditor2 = _interopRequireDefault(_mayashEditor);

var _ErrorPage = __webpack_require__(/*! ../../components/ErrorPage */ "./src/client/components/ErrorPage.js");

var _ErrorPage2 = _interopRequireDefault(_ErrorPage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/** @format */

var styles = {
  root: {
    flexGrow: 1,
    paddingTop: '5px'
  },
  title: {
    textAlign: 'center'
  },
  flexGrow: {
    flex: '1 1 auto'
  },
  card: {
    borderRadius: '8px'
  }
};

var ResumePage = function (_Component) {
  (0, _inherits3.default)(ResumePage, _Component);

  function ResumePage(props) {
    (0, _classCallCheck3.default)(this, ResumePage);

    var _this = (0, _possibleConstructorReturn3.default)(this, (ResumePage.__proto__ || (0, _getPrototypeOf2.default)(ResumePage)).call(this, props));

    _this.onMouseEnter = function () {
      return _this.setState({ hover: true });
    };

    _this.onMouseLeave = function () {
      return _this.setState({ hover: false });
    };

    _this.onChange = function () {};

    _this.state = {
      hover: false
    };
    return _this;
  }

  // shouldComponentUpdate(nextState, nextProps) {
  //   return this.state !== nextState || this.props !== nextProps;
  // }

  (0, _createClass3.default)(ResumePage, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          classes = _props.classes,
          elements = _props.elements,
          match = _props.match;
      var username = match.params.username;
      var hover = this.state.hover;


      var u = (0, _keys2.default)(elements).find(function (id) {
        return elements[id].username === username;
      });

      var element = elements[u];

      if (!element || element.statusCode >= 300) {
        return _react2.default.createElement(_ErrorPage2.default, element);
      }

      if (element.elementType !== 'user') {
        return _react2.default.createElement(_ErrorPage2.default, null);
      }

      if (!element.resume) {
        return _react2.default.createElement(_ErrorPage2.default, null);
      }

      return _react2.default.createElement(
        _Grid2.default,
        {
          container: true,
          justify: 'center',
          spacing: 0,
          className: classes.root,
          onMouseEnter: this.onMouseEnter,
          onMouseLeave: this.onMouseLeave
        },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 11, sm: 11, md: 11, lg: 11, xl: 11 },
          _react2.default.createElement(
            _Card2.default,
            { raised: hover, className: classes.card },
            _react2.default.createElement(_Card.CardHeader, { title: 'Resume', className: classes.title }),
            _react2.default.createElement(
              _Card.CardContent,
              null,
              _react2.default.createElement(_mayashEditor2.default, {
                editorState: (0, _mayashEditor.createEditorState)(element.resume),
                onChange: this.onChange,
                readOnly: true
              })
            )
          )
        )
      );
    }
  }]);
  return ResumePage;
}(_react.Component);

ResumePage.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  match: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired
};

var mapStateToProps = function mapStateToProps(_ref) {
  var elements = _ref.elements;
  return { elements: elements };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)((0, _withStyles2.default)(styles)(ResumePage));

/***/ }),

/***/ "./src/lib/mayash-editor/Editor.js":
/*!*****************************************!*\
  !*** ./src/lib/mayash-editor/Editor.js ***!
  \*****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _ReactPropTypes = __webpack_require__(/*! react/lib/ReactPropTypes */ "./node_modules/react/lib/ReactPropTypes.js");

var _ReactPropTypes2 = _interopRequireDefault(_ReactPropTypes);

var _classnames = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");

var _classnames2 = _interopRequireDefault(_classnames);

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _IconButton = __webpack_require__(/*! material-ui/IconButton */ "./node_modules/material-ui/IconButton/index.js");

var _IconButton2 = _interopRequireDefault(_IconButton);

var _Tooltip = __webpack_require__(/*! material-ui/Tooltip */ "./node_modules/material-ui/Tooltip/index.js");

var _Tooltip2 = _interopRequireDefault(_Tooltip);

var _Title = __webpack_require__(/*! material-ui-icons/Title */ "./node_modules/material-ui-icons/Title.js");

var _Title2 = _interopRequireDefault(_Title);

var _FormatBold = __webpack_require__(/*! material-ui-icons/FormatBold */ "./node_modules/material-ui-icons/FormatBold.js");

var _FormatBold2 = _interopRequireDefault(_FormatBold);

var _FormatItalic = __webpack_require__(/*! material-ui-icons/FormatItalic */ "./node_modules/material-ui-icons/FormatItalic.js");

var _FormatItalic2 = _interopRequireDefault(_FormatItalic);

var _FormatUnderlined = __webpack_require__(/*! material-ui-icons/FormatUnderlined */ "./node_modules/material-ui-icons/FormatUnderlined.js");

var _FormatUnderlined2 = _interopRequireDefault(_FormatUnderlined);

var _FormatQuote = __webpack_require__(/*! material-ui-icons/FormatQuote */ "./node_modules/material-ui-icons/FormatQuote.js");

var _FormatQuote2 = _interopRequireDefault(_FormatQuote);

var _Code = __webpack_require__(/*! material-ui-icons/Code */ "./node_modules/material-ui-icons/Code.js");

var _Code2 = _interopRequireDefault(_Code);

var _FormatListNumbered = __webpack_require__(/*! material-ui-icons/FormatListNumbered */ "./node_modules/material-ui-icons/FormatListNumbered.js");

var _FormatListNumbered2 = _interopRequireDefault(_FormatListNumbered);

var _FormatListBulleted = __webpack_require__(/*! material-ui-icons/FormatListBulleted */ "./node_modules/material-ui-icons/FormatListBulleted.js");

var _FormatListBulleted2 = _interopRequireDefault(_FormatListBulleted);

var _FormatAlignCenter = __webpack_require__(/*! material-ui-icons/FormatAlignCenter */ "./node_modules/material-ui-icons/FormatAlignCenter.js");

var _FormatAlignCenter2 = _interopRequireDefault(_FormatAlignCenter);

var _FormatAlignLeft = __webpack_require__(/*! material-ui-icons/FormatAlignLeft */ "./node_modules/material-ui-icons/FormatAlignLeft.js");

var _FormatAlignLeft2 = _interopRequireDefault(_FormatAlignLeft);

var _FormatAlignRight = __webpack_require__(/*! material-ui-icons/FormatAlignRight */ "./node_modules/material-ui-icons/FormatAlignRight.js");

var _FormatAlignRight2 = _interopRequireDefault(_FormatAlignRight);

var _FormatAlignJustify = __webpack_require__(/*! material-ui-icons/FormatAlignJustify */ "./node_modules/material-ui-icons/FormatAlignJustify.js");

var _FormatAlignJustify2 = _interopRequireDefault(_FormatAlignJustify);

var _AttachFile = __webpack_require__(/*! material-ui-icons/AttachFile */ "./node_modules/material-ui-icons/AttachFile.js");

var _AttachFile2 = _interopRequireDefault(_AttachFile);

var _InsertLink = __webpack_require__(/*! material-ui-icons/InsertLink */ "./node_modules/material-ui-icons/InsertLink.js");

var _InsertLink2 = _interopRequireDefault(_InsertLink);

var _InsertPhoto = __webpack_require__(/*! material-ui-icons/InsertPhoto */ "./node_modules/material-ui-icons/InsertPhoto.js");

var _InsertPhoto2 = _interopRequireDefault(_InsertPhoto);

var _InsertEmoticon = __webpack_require__(/*! material-ui-icons/InsertEmoticon */ "./node_modules/material-ui-icons/InsertEmoticon.js");

var _InsertEmoticon2 = _interopRequireDefault(_InsertEmoticon);

var _InsertComment = __webpack_require__(/*! material-ui-icons/InsertComment */ "./node_modules/material-ui-icons/InsertComment.js");

var _InsertComment2 = _interopRequireDefault(_InsertComment);

var _Highlight = __webpack_require__(/*! material-ui-icons/Highlight */ "./node_modules/material-ui-icons/Highlight.js");

var _Highlight2 = _interopRequireDefault(_Highlight);

var _Functions = __webpack_require__(/*! material-ui-icons/Functions */ "./node_modules/material-ui-icons/Functions.js");

var _Functions2 = _interopRequireDefault(_Functions);

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Atomic = __webpack_require__(/*! ./components/Atomic */ "./src/lib/mayash-editor/components/Atomic.js");

var _Atomic2 = _interopRequireDefault(_Atomic);

var _EditorStyles = __webpack_require__(/*! ./EditorStyles */ "./src/lib/mayash-editor/EditorStyles.js");

var _EditorStyles2 = _interopRequireDefault(_EditorStyles);

var _constants = __webpack_require__(/*! ./constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * MayashEditor
 */


// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

var MayashEditor = function (_Component) {
  (0, _inherits3.default)(MayashEditor, _Component);

  /**
   * Mayash Editor's proptypes are defined here.
   */
  function MayashEditor() {
    var _this2 = this;

    (0, _classCallCheck3.default)(this, MayashEditor);

    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    var _this = (0, _possibleConstructorReturn3.default)(this, (MayashEditor.__proto__ || (0, _getPrototypeOf2.default)(MayashEditor)).call(this));

    _this.onChangeInsertPhoto = function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(e) {
        var file, formData, _ref2, statusCode, error, payload, src, editorState, contentState, contentStateWithEntity, entityKey, middleEditorState, newEditorState;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // e.preventDefault();
                file = e.target.files[0];

                if (!(file.type.indexOf('image/') === 0)) {
                  _context.next = 21;
                  break;
                }

                formData = new FormData();

                formData.append('photo', file);

                _context.next = 6;
                return _this.props.apiPhotoUpload({
                  formData: formData
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;

                if (!(statusCode >= 300)) {
                  _context.next = 13;
                  break;
                }

                // handle Error
                console.error(statusCode, error);
                return _context.abrupt('return');

              case 13:
                src = payload.photoUrl;
                editorState = _this.props.editorState;
                contentState = editorState.getCurrentContent();
                contentStateWithEntity = contentState.createEntity(_constants.Blocks.PHOTO, 'IMMUTABLE', { src: src });
                entityKey = contentStateWithEntity.getLastCreatedEntityKey();
                middleEditorState = _draftJs.EditorState.set(editorState, {
                  currentContent: contentStateWithEntity
                });
                newEditorState = _draftJs.AtomicBlockUtils.insertAtomicBlock(middleEditorState, entityKey, ' ');


                _this.onChange(newEditorState);

              case 21:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, _this2);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }();

    _this.onChange = function (editorState) {
      _this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    _this.focus = function () {
      return _this.editorNode.focus();
    };

    _this.onTab = _this.onTab.bind(_this);
    _this.onTitleClick = _this.onTitleClick.bind(_this);
    _this.onCodeClick = _this.onCodeClick.bind(_this);
    _this.onQuoteClick = _this.onQuoteClick.bind(_this);
    _this.onListBulletedClick = _this.onListBulletedClick.bind(_this);
    _this.onListNumberedClick = _this.onListNumberedClick.bind(_this);
    _this.onBoldClick = _this.onBoldClick.bind(_this);
    _this.onItalicClick = _this.onItalicClick.bind(_this);
    _this.onUnderLineClick = _this.onUnderLineClick.bind(_this);
    _this.onHighlightClick = _this.onHighlightClick.bind(_this);
    _this.handleKeyCommand = _this.handleKeyCommand.bind(_this);
    _this.onClickInsertPhoto = _this.onClickInsertPhoto.bind(_this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
    return _this;
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */


  (0, _createClass3.default)(MayashEditor, [{
    key: 'onTab',
    value: function onTab(e) {
      e.preventDefault();
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.onTab(e, editorState, 4);

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Title Button.
     * @function onTitleClick
     */

  }, {
    key: 'onTitleClick',
    value: function onTitleClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'header-one');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Code Button.
     * @function onCodeClick
     */

  }, {
    key: 'onCodeClick',
    value: function onCodeClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'code-block');

      this.onChange(newEditorState);
    }

    /**
     * This function will handle event on Quote Button.
     * @function onQuoteClick
     */

  }, {
    key: 'onQuoteClick',
    value: function onQuoteClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'blockquote');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with unordered list items
     * @function onListBulletedClick
     */

  }, {
    key: 'onListBulletedClick',
    value: function onListBulletedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'unordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will update block with ordered list item.
     * @function onListNumberedClick
     */

  }, {
    key: 'onListNumberedClick',
    value: function onListNumberedClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleBlockType(editorState, 'ordered-list-item');

      this.onChange(newEditorState);
    }

    /**
     * This function will give selected content Bold styling.
     * @function onBoldClick
     */

  }, {
    key: 'onBoldClick',
    value: function onBoldClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'BOLD');

      this.onChange(newEditorState);
    }

    /**
     * This function will give italic styling to selected content.
     * @function onItalicClick
     */

  }, {
    key: 'onItalicClick',
    value: function onItalicClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'ITALIC');

      this.onChange(newEditorState);
    }

    /**
     * This function will give underline styling to selected content.
     * @function onUnderLineClick
     */

  }, {
    key: 'onUnderLineClick',
    value: function onUnderLineClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'UNDERLINE');

      this.onChange(newEditorState);
    }

    /**
     * This function will highlight selected content.
     * @function onHighlightClick
     */

  }, {
    key: 'onHighlightClick',
    value: function onHighlightClick() {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.toggleInlineStyle(editorState, 'CODE');

      this.onChange(newEditorState);
    }

    /**
     *
     */

  }, {
    key: 'onClickInsertPhoto',
    value: function onClickInsertPhoto() {
      this.photo.value = null;
      this.photo.click();
    }

    /**
     *
     */

  }, {
    key: 'handleKeyCommand',


    /**
     * This function will give custom handle commands to Editor.
     * @function handleKeyCommand
     * @param {string} command
     * @return {string}
     */
    value: function handleKeyCommand(command) {
      var editorState = this.props.editorState;


      var newEditorState = _draftJs.RichUtils.handleKeyCommand(editorState, command);

      if (newEditorState) {
        this.onChange(newEditorState);
        return _constants.HANDLED;
      }

      return _constants.NOT_HANDLED;
    }

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockRendererFn',
    value: function blockRendererFn(block) {
      switch (block.getType()) {
        case _constants.Blocks.ATOMIC:
          return {
            component: _Atomic2.default,
            editable: false
          };

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

    /* eslint-disable class-methods-use-this */

  }, {
    key: 'blockStyleFn',
    value: function blockStyleFn(block) {
      switch (block.getType()) {
        case 'blockquote':
          return 'RichEditor-blockquote';

        default:
          return null;
      }
    }
    /* eslint-enable class-methods-use-this */

  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          classes = _props.classes,
          readOnly = _props.readOnly,
          onChange = _props.onChange,
          editorState = _props.editorState,
          placeholder = _props.placeholder;

      // Custom overrides for "code" style.

      var styleMap = {
        CODE: {
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
          fontSize: 16,
          padding: 2
        }
      };

      return _react2.default.createElement(
        'div',
        { className: classes.root },
        !readOnly ? _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-controls': '' }) },
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Title', id: 'title', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Title', onClick: this.onTitleClick },
              _react2.default.createElement(_Title2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Bold', id: 'bold', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Bold', onClick: this.onBoldClick },
              _react2.default.createElement(_FormatBold2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Italic', id: 'italic', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Italic', onClick: this.onItalicClick },
              _react2.default.createElement(_FormatItalic2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Underline', id: 'underline', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Underline',
                onClick: this.onUnderLineClick
              },
              _react2.default.createElement(_FormatUnderlined2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Code', id: 'code', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Code', onClick: this.onCodeClick },
              _react2.default.createElement(_Code2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Quote', id: 'quote', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Quote', onClick: this.onQuoteClick },
              _react2.default.createElement(_FormatQuote2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Unordered List',
              id: 'unorderd-list',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Unordered List',
                onClick: this.onListBulletedClick
              },
              _react2.default.createElement(_FormatListBulleted2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Ordered List', id: 'ordered-list', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Ordered List',
                onClick: this.onListNumberedClick
              },
              _react2.default.createElement(_FormatListNumbered2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Left', id: 'align-left', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Left', disabled: true },
              _react2.default.createElement(_FormatAlignLeft2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Center', id: 'align-center', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Center', disabled: true },
              _react2.default.createElement(_FormatAlignCenter2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignRight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Align Right', id: 'align-right', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Align Right', disabled: true },
              _react2.default.createElement(_FormatAlignJustify2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Attach File', id: 'attach-file', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Attach File', disabled: true },
              _react2.default.createElement(_AttachFile2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Link', id: 'insert-link', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Link', disabled: true },
              _react2.default.createElement(_InsertLink2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            { title: 'Insert Photo', id: 'insert-photo', placement: 'bottom' },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Insert Photo',
                disabled: typeof this.props.apiPhotoUpload !== 'function',
                onClick: this.onClickInsertPhoto
              },
              _react2.default.createElement(_InsertPhoto2.default, null),
              _react2.default.createElement('input', {
                type: 'file',
                accept: 'image/jpeg|png|gif',
                onChange: this.onChangeInsertPhoto,
                ref: function ref(photo) {
                  _this3.photo = photo;
                },
                style: { display: 'none' }
              })
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Emoticon',
              id: 'insertE-emoticon',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Emoticon', disabled: true },
              _react2.default.createElement(_InsertEmoticon2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Insert Comment',
              id: 'insert-comment',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Insert Comment', disabled: true },
              _react2.default.createElement(_InsertComment2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Highlight Text',
              id: 'highlight-text',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              {
                'aria-label': 'Highlight Text',
                onClick: this.onHighlightClick
              },
              _react2.default.createElement(_Highlight2.default, null)
            )
          ),
          _react2.default.createElement(
            _Tooltip2.default,
            {
              title: 'Add Functions',
              id: 'add-functions',
              placement: 'bottom'
            },
            _react2.default.createElement(
              _IconButton2.default,
              { 'aria-label': 'Add Functions', disabled: true },
              _react2.default.createElement(_Functions2.default, null)
            )
          )
        ) : null,
        _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)({ 'editor-area': '' }) },
          _react2.default.createElement(_draftJs.Editor
          /* Basics */

          , { editorState: editorState,
            onChange: onChange
            /* Presentation */

            , placeholder: placeholder
            // textAlignment="center"

            // textDirectionality="LTR"

            , blockRendererFn: this.blockRendererFn,
            blockStyleFn: this.blockStyleFn,
            customStyleMap: styleMap
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            , readOnly: readOnly,
            spellCheck: true
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            , handleKeyCommand: this.handleKeyCommand
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            , onTab: this.onTab
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            , ref: function ref(node) {
              _this3.editorNode = node;
            }
          })
        )
      );
    }
  }]);
  return MayashEditor;
}(_react.Component);

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

// import Icon from 'material-ui/Icon';
/** @format */

MayashEditor.propTypes = {
  classes: _ReactPropTypes2.default.object.isRequired,

  editorState: _ReactPropTypes2.default.object.isRequired,
  onChange: _ReactPropTypes2.default.func.isRequired,

  placeholder: _ReactPropTypes2.default.string,

  readOnly: _ReactPropTypes2.default.bool.isRequired,

  /**
   * This api function should be applied for enabling photo adding
   * feature in Mayash Editor.
   */
  apiPhotoUpload: _ReactPropTypes2.default.func
};
MayashEditor.defaultProps = {
  readOnly: true,
  placeholder: 'Write Here...'
};
exports.default = (0, _withStyles2.default)(_EditorStyles2.default)(MayashEditor);

/***/ }),

/***/ "./src/lib/mayash-editor/EditorState.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/EditorState.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = undefined;

var _draftJs = __webpack_require__(/*! draft-js */ "./node_modules/draft-js/lib/Draft.js");

var _Decorators = __webpack_require__(/*! ./components/Decorators */ "./src/lib/mayash-editor/components/Decorators.js");

/** @format */

var defaultDecorators = new _draftJs.CompositeDecorator([{
  strategy: _Decorators.handleStrategy,
  component: _Decorators.HandleSpan
}, {
  strategy: _Decorators.hashtagStrategy,
  component: _Decorators.HashtagSpan
}]);

var createEditorState = exports.createEditorState = function createEditorState() {
  var content = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
  var decorators = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultDecorators;

  if (content === null) {
    return _draftJs.EditorState.createEmpty(decorators);
  }
  return _draftJs.EditorState.createWithContent((0, _draftJs.convertFromRaw)(content), decorators);
};

exports.default = createEditorState;

/***/ }),

/***/ "./src/lib/mayash-editor/EditorStyles.js":
/*!***********************************************!*\
  !*** ./src/lib/mayash-editor/EditorStyles.js ***!
  \***********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * This file contains all the CSS-in-JS styles of Editor component.
 *
 * @format
 */

exports.default = {
  '@global': {
    '.RichEditor-root': {
      background: '#fff',
      border: '1px solid #ddd',
      fontFamily: "'Georgia', serif",
      fontSize: '14px',
      padding: '15px'
    },
    '.RichEditor-editor': {
      borderTop: '1px solid #ddd',
      cursor: 'text',
      fontSize: '16px',
      marginTop: '10px'
    },
    '.public-DraftEditorPlaceholder-root': {
      margin: '0 -15px -15px',
      padding: '15px'
    },
    '.public-DraftEditor-content': {
      margin: '0 -15px -15px',
      padding: '15px'
      // minHeight: '100px',
    },
    '.RichEditor-blockquote': {
      backgroundColor: '5px solid #eee',
      borderLeft: '5px solid #eee',
      color: '#666',
      fontFamily: "'Hoefler Text', 'Georgia', serif",
      fontStyle: 'italic',
      margin: '16px 0',
      padding: '10px 20px'
    },
    '.public-DraftStyleDefault-pre': {
      backgroundColor: 'rgba(0, 0, 0, 0.05)',
      fontFamily: "'Inconsolata', 'Menlo', 'Consolas', monospace",
      fontSize: '16px',
      padding: '20px'
    }
  },
  root: {
    // padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto'
  }
};

/***/ }),

/***/ "./src/lib/mayash-editor/components/Atomic.js":
/*!****************************************************!*\
  !*** ./src/lib/mayash-editor/components/Atomic.js ***!
  \****************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _constants = __webpack_require__(/*! ../constants */ "./src/lib/mayash-editor/constants.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial'
  }
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
/**
 *
 * @format
 */

var Atomic = function (_Component) {
  (0, _inherits3.default)(Atomic, _Component);

  function Atomic(props) {
    (0, _classCallCheck3.default)(this, Atomic);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Atomic.__proto__ || (0, _getPrototypeOf2.default)(Atomic)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(Atomic, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          contentState = _props.contentState,
          block = _props.block;


      var entity = contentState.getEntity(block.getEntityAt(0));

      var _entity$getData = entity.getData(),
          src = _entity$getData.src;

      var type = entity.getType();

      if (type === _constants.Blocks.PHOTO) {
        return _react2.default.createElement(
          'div',
          null,
          _react2.default.createElement('img', { alt: 'alt', src: src, style: styles.photo })
        );
      }

      return _react2.default.createElement('div', null);
    }
  }]);
  return Atomic;
}(_react.Component);

Atomic.propTypes = {
  contentState: _propTypes2.default.object.isRequired,
  block: _propTypes2.default.any.isRequired
};
exports.default = Atomic;

/***/ }),

/***/ "./src/lib/mayash-editor/components/Decorators.js":
/*!********************************************************!*\
  !*** ./src/lib/mayash-editor/components/Decorators.js ***!
  \********************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashtagSpan = exports.HandleSpan = undefined;
exports.handleStrategy = handleStrategy;
exports.hashtagStrategy = hashtagStrategy;

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _style = __webpack_require__(/*! ../style */ "./src/lib/mayash-editor/style/index.js");

var _style2 = _interopRequireDefault(_style);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/* eslint-disable */
// eslint is disabled here for now.
var HANDLE_REGEX = /\@[\w]+/g;
var HASHTAG_REGEX = /\#[\w\u0590-\u05ff]+/g;

function findWithRegex(regex, contentBlock, callback) {
  var text = contentBlock.getText();
  var matchArr = void 0,
      start = void 0;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

function handleStrategy(contentBlock, callback, contentState) {
  findWithRegex(HANDLE_REGEX, contentBlock, callback);
}

function hashtagStrategy(contentBlock, callback, contentState) {
  findWithRegex(HASHTAG_REGEX, contentBlock, callback);
}

var HandleSpan = exports.HandleSpan = function HandleSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.handle },
    props.children
  );
};

var HashtagSpan = exports.HashtagSpan = function HashtagSpan(props) {
  return _react2.default.createElement(
    'span',
    { style: _style2.default.hashtag },
    props.children
  );
};

exports.default = {
  handleStrategy: handleStrategy,
  HandleSpan: HandleSpan,

  hashtagStrategy: hashtagStrategy,
  HashtagSpan: HashtagSpan
};

/***/ }),

/***/ "./src/lib/mayash-editor/constants.js":
/*!********************************************!*\
  !*** ./src/lib/mayash-editor/constants.js ***!
  \********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
var Blocks = exports.Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video'
};

/**
 * @constant Inline
 */
var Inline = exports.Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT'
};

/**
 * @constant Entity
 */
var Entity = exports.Entity = {
  LINK: 'LINK'
};

/**
 * @constant HYPERLINK
 */
var HYPERLINK = exports.HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
var HANDLED = exports.HANDLED = 'handled';
var NOT_HANDLED = exports.NOT_HANDLED = 'not_handled';

exports.default = {
  Blocks: Blocks,
  Inline: Inline,
  Entity: Entity
};

/***/ }),

/***/ "./src/lib/mayash-editor/index.js":
/*!****************************************!*\
  !*** ./src/lib/mayash-editor/index.js ***!
  \****************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEditorState = exports.MayashEditor = undefined;

var _Editor = __webpack_require__(/*! ./Editor */ "./src/lib/mayash-editor/Editor.js");

Object.defineProperty(exports, 'MayashEditor', {
  enumerable: true,
  get: function get() {
    return _Editor.MayashEditor;
  }
});

var _EditorState = __webpack_require__(/*! ./EditorState */ "./src/lib/mayash-editor/EditorState.js");

Object.defineProperty(exports, 'createEditorState', {
  enumerable: true,
  get: function get() {
    return _EditorState.createEditorState;
  }
});

var _Editor2 = _interopRequireDefault(_Editor);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _Editor2.default;

/***/ }),

/***/ "./src/lib/mayash-editor/style/index.js":
/*!**********************************************!*\
  !*** ./src/lib/mayash-editor/style/index.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
/** @format */

var style = exports.style = {
  root: {
    padding: 20,
    width: 600
  },
  editor: {
    border: '1px solid #ddd',
    cursor: 'text',
    fontSize: 16,
    minHeight: 40,
    padding: 10
  },
  button: {
    marginTop: 10,
    textAlign: 'center'
  },
  handle: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override'
  },
  hashtag: {
    color: 'rgba(95, 184, 138, 1.0)'
  }
};

exports.default = style;

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQXR0YWNoRmlsZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvQ29kZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduSnVzdGlmeS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25MZWZ0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnblJpZ2h0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRCb2xkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3RCdWxsZXRlZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydENvbW1lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRMaW5rLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vSWNvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbi9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9jcmVhdGVFYWdlckZhY3RvcnkuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9nZXREaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvcHVyZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldERpc3BsYXlOYW1lLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0U3RhdGljLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hhbGxvd0VxdWFsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3dyYXBEaXNwbGF5TmFtZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUmVzdW1lUGFnZS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3R5bGVzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb21wb25lbnRzL0F0b21pYy5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9EZWNvcmF0b3JzLmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2luZGV4LmpzIiwid2VicGFjazovLy8uL3NyYy9saWIvbWF5YXNoLWVkaXRvci9zdHlsZS9pbmRleC5qcyJdLCJuYW1lcyI6WyJzdHlsZXMiLCJyb290IiwiZmxleEdyb3ciLCJwYWRkaW5nVG9wIiwidGl0bGUiLCJ0ZXh0QWxpZ24iLCJmbGV4IiwiY2FyZCIsImJvcmRlclJhZGl1cyIsIlJlc3VtZVBhZ2UiLCJwcm9wcyIsIm9uTW91c2VFbnRlciIsInNldFN0YXRlIiwiaG92ZXIiLCJvbk1vdXNlTGVhdmUiLCJvbkNoYW5nZSIsInN0YXRlIiwiY2xhc3NlcyIsImVsZW1lbnRzIiwibWF0Y2giLCJ1c2VybmFtZSIsInBhcmFtcyIsInUiLCJmaW5kIiwiaWQiLCJlbGVtZW50Iiwic3RhdHVzQ29kZSIsImVsZW1lbnRUeXBlIiwicmVzdW1lIiwicHJvcFR5cGVzIiwib2JqZWN0IiwiaXNSZXF1aXJlZCIsIm1hcFN0YXRlVG9Qcm9wcyIsIk1heWFzaEVkaXRvciIsIm9uQ2hhbmdlSW5zZXJ0UGhvdG8iLCJlIiwiZmlsZSIsInRhcmdldCIsImZpbGVzIiwidHlwZSIsImluZGV4T2YiLCJmb3JtRGF0YSIsIkZvcm1EYXRhIiwiYXBwZW5kIiwiYXBpUGhvdG9VcGxvYWQiLCJlcnJvciIsInBheWxvYWQiLCJjb25zb2xlIiwic3JjIiwicGhvdG9VcmwiLCJlZGl0b3JTdGF0ZSIsImNvbnRlbnRTdGF0ZSIsImdldEN1cnJlbnRDb250ZW50IiwiY29udGVudFN0YXRlV2l0aEVudGl0eSIsImNyZWF0ZUVudGl0eSIsIlBIT1RPIiwiZW50aXR5S2V5IiwiZ2V0TGFzdENyZWF0ZWRFbnRpdHlLZXkiLCJtaWRkbGVFZGl0b3JTdGF0ZSIsInNldCIsImN1cnJlbnRDb250ZW50IiwibmV3RWRpdG9yU3RhdGUiLCJpbnNlcnRBdG9taWNCbG9jayIsImZvY3VzIiwiZWRpdG9yTm9kZSIsIm9uVGFiIiwiYmluZCIsIm9uVGl0bGVDbGljayIsIm9uQ29kZUNsaWNrIiwib25RdW90ZUNsaWNrIiwib25MaXN0QnVsbGV0ZWRDbGljayIsIm9uTGlzdE51bWJlcmVkQ2xpY2siLCJvbkJvbGRDbGljayIsIm9uSXRhbGljQ2xpY2siLCJvblVuZGVyTGluZUNsaWNrIiwib25IaWdobGlnaHRDbGljayIsImhhbmRsZUtleUNvbW1hbmQiLCJvbkNsaWNrSW5zZXJ0UGhvdG8iLCJwcmV2ZW50RGVmYXVsdCIsInRvZ2dsZUJsb2NrVHlwZSIsInRvZ2dsZUlubGluZVN0eWxlIiwicGhvdG8iLCJ2YWx1ZSIsImNsaWNrIiwiY29tbWFuZCIsImJsb2NrIiwiZ2V0VHlwZSIsIkFUT01JQyIsImNvbXBvbmVudCIsImVkaXRhYmxlIiwicmVhZE9ubHkiLCJwbGFjZWhvbGRlciIsInN0eWxlTWFwIiwiQ09ERSIsImJhY2tncm91bmRDb2xvciIsImZvbnRGYW1pbHkiLCJmb250U2l6ZSIsInBhZGRpbmciLCJkaXNwbGF5IiwiYmxvY2tSZW5kZXJlckZuIiwiYmxvY2tTdHlsZUZuIiwibm9kZSIsImZ1bmMiLCJzdHJpbmciLCJib29sIiwiZGVmYXVsdFByb3BzIiwiZGVmYXVsdERlY29yYXRvcnMiLCJzdHJhdGVneSIsImNyZWF0ZUVkaXRvclN0YXRlIiwiY29udGVudCIsImRlY29yYXRvcnMiLCJjcmVhdGVFbXB0eSIsImNyZWF0ZVdpdGhDb250ZW50IiwiYmFja2dyb3VuZCIsImJvcmRlciIsImJvcmRlclRvcCIsImN1cnNvciIsIm1hcmdpblRvcCIsIm1hcmdpbiIsImJvcmRlckxlZnQiLCJjb2xvciIsImZvbnRTdHlsZSIsIndpZHRoIiwid2hpdGVTcGFjZSIsIkF0b21pYyIsImVudGl0eSIsImdldEVudGl0eSIsImdldEVudGl0eUF0IiwiZ2V0RGF0YSIsImFueSIsImhhbmRsZVN0cmF0ZWd5IiwiaGFzaHRhZ1N0cmF0ZWd5IiwiSEFORExFX1JFR0VYIiwiSEFTSFRBR19SRUdFWCIsImZpbmRXaXRoUmVnZXgiLCJyZWdleCIsImNvbnRlbnRCbG9jayIsImNhbGxiYWNrIiwidGV4dCIsImdldFRleHQiLCJtYXRjaEFyciIsInN0YXJ0IiwiZXhlYyIsImluZGV4IiwibGVuZ3RoIiwiSGFuZGxlU3BhbiIsImhhbmRsZSIsImNoaWxkcmVuIiwiSGFzaHRhZ1NwYW4iLCJoYXNodGFnIiwiQmxvY2tzIiwiVU5TVFlMRUQiLCJQQVJBR1JBUEgiLCJIMSIsIkgyIiwiSDMiLCJINCIsIkg1IiwiSDYiLCJPTCIsIlVMIiwiQkxPQ0tRVU9URSIsIlZJREVPIiwiSW5saW5lIiwiQk9MRCIsIklUQUxJQyIsIlNUUklLRVRIUk9VR0giLCJVTkRFUkxJTkUiLCJISUdITElHSFQiLCJFbnRpdHkiLCJMSU5LIiwiSFlQRVJMSU5LIiwiSEFORExFRCIsIk5PVF9IQU5ETEVEIiwic3R5bGUiLCJlZGl0b3IiLCJtaW5IZWlnaHQiLCJidXR0b24iLCJkaXJlY3Rpb24iLCJ1bmljb2RlQmlkaSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9RQUFvUTs7QUFFdFQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtHQUFrRzs7QUFFcEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx1Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1GQUFtRjs7QUFFckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxvQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtGQUFrRjs7QUFFcEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9GQUFvRjs7QUFFdEk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG1GQUFtRjs7QUFFckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDZPQUE2Tzs7QUFFL1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHdEQUF3RDs7QUFFMUc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHVSQUF1Ujs7QUFFelU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtKQUFrSjs7QUFFcE07QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxxQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGdEQUFnRDs7QUFFbEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDJIQUEySDs7QUFFN0s7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxtQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGtEQUFrRDs7QUFFcEc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELG9JQUFvSTs7QUFFdEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHNIQUFzSDs7QUFFeEs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELHlXQUF5Vzs7QUFFM1o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELDJOQUEyTjs7QUFFN1E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw2Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELGdJQUFnSTs7QUFFbEw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Ysa0RBQWtELCtCQUErQjs7QUFFakY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx3Qjs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBLGdIQUFnSDs7QUFFaEg7QUFDQTtBQUNBLGdDQUFnQyw4Q0FBOEM7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxrQkFBa0IsUTs7Ozs7Ozs7Ozs7OztBQzlJdkU7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixtUEFBNEk7QUFDNUk7O0FBRUE7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQSw4RUFBOEU7QUFDOUU7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFdBQVcsMkJBQTJCO0FBQ3RDO0FBQ0E7QUFDQSxhQUFhLDBCQUEwQjtBQUN2QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZTtBQUNmOztBQUVBO0FBQ0EsV0FBVztBQUNYO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCx3QkFBd0IsYzs7Ozs7Ozs7Ozs7OztBQ3JPN0U7O0FBRUE7QUFDQTtBQUNBLENBQUM7O0FBRUQ7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7O0FBRUQsc0NBQXNDLHVDQUF1QyxnQkFBZ0IsRTs7Ozs7Ozs7Ozs7OztBQ2Y3Rjs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0EsOEZBQThGOztBQUU5RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscURBQXFELHFCQUFxQixXOzs7Ozs7Ozs7Ozs7O0FDbEwxRTs7QUFFQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRCxzQ0FBc0MsdUNBQXVDLGdCQUFnQixFOzs7Ozs7Ozs7Ozs7O0FDZjdGOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7Ozs7QUNyQkE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2ZBOztBQUVBOztBQUVBLG9HQUFvRyxtQkFBbUIsRUFBRSxtQkFBbUIsOEhBQThIOztBQUUxUTtBQUNBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7Ozs7QUNWQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLDhEOzs7Ozs7Ozs7Ozs7O0FDZEE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLEdBQUc7O0FBRUg7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsdUI7Ozs7Ozs7Ozs7Ozs7QUNsQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTs7QUFFQSxpQzs7Ozs7Ozs7Ozs7OztBQ2RBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSw0Qjs7Ozs7Ozs7Ozs7OztBQ1pBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3Rix5Qzs7Ozs7Ozs7Ozs7OztBQ1ZBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBLHNDQUFzQyx1Q0FBdUMsZ0JBQWdCOztBQUU3RixpREFBaUQsMENBQTBDLDBEQUEwRCxFQUFFOztBQUV2SixpREFBaUQsYUFBYSx1RkFBdUYsRUFBRSx1RkFBdUY7O0FBRTlPLDBDQUEwQywrREFBK0QscUdBQXFHLEVBQUUseUVBQXlFLGVBQWUseUVBQXlFLEVBQUUsRUFBRSx1SEFBdUg7O0FBRTVlO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsK0I7Ozs7Ozs7Ozs7Ozs7QUN6REE7O0FBRUE7O0FBRUEsbURBQW1ELGdCQUFnQixzQkFBc0IsT0FBTywyQkFBMkIsMEJBQTBCLHlEQUF5RCwyQkFBMkIsRUFBRSxFQUFFLEVBQUUsZUFBZTs7QUFFOVA7O0FBRUE7O0FBRUEsc0NBQXNDLHVDQUF1QyxnQkFBZ0I7O0FBRTdGO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QixVQUFVLHFCQUFxQjtBQUM1RDtBQUNBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSx5Qzs7Ozs7Ozs7Ozs7OztBQ2pDQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQSxzQ0FBc0MsdUNBQXVDLGdCQUFnQjs7QUFFN0Y7QUFDQTtBQUNBOztBQUVBLGtDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDWkE7Ozs7QUFDQTs7OztBQUNBOztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUE7Ozs7OztBQVpBOztBQWNBLElBQU1BLFNBQVM7QUFDYkMsUUFBTTtBQUNKQyxjQUFVLENBRE47QUFFSkMsZ0JBQVk7QUFGUixHQURPO0FBS2JDLFNBQU87QUFDTEMsZUFBVztBQUROLEdBTE07QUFRYkgsWUFBVTtBQUNSSSxVQUFNO0FBREUsR0FSRztBQVdiQyxRQUFNO0FBQ0pDLGtCQUFjO0FBRFY7QUFYTyxDQUFmOztJQWdCTUMsVTs7O0FBQ0osc0JBQVlDLEtBQVosRUFBbUI7QUFBQTs7QUFBQSw4SUFDWEEsS0FEVzs7QUFBQSxVQVduQkMsWUFYbUIsR0FXSjtBQUFBLGFBQU0sTUFBS0MsUUFBTCxDQUFjLEVBQUVDLE9BQU8sSUFBVCxFQUFkLENBQU47QUFBQSxLQVhJOztBQUFBLFVBWW5CQyxZQVptQixHQVlKO0FBQUEsYUFBTSxNQUFLRixRQUFMLENBQWMsRUFBRUMsT0FBTyxLQUFULEVBQWQsQ0FBTjtBQUFBLEtBWkk7O0FBQUEsVUFhbkJFLFFBYm1CLEdBYVIsWUFBTSxDQUFFLENBYkE7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYTtBQUNYSCxhQUFPO0FBREksS0FBYjtBQUZpQjtBQUtsQjs7QUFFRDtBQUNBO0FBQ0E7Ozs7NkJBTVM7QUFBQSxtQkFDOEIsS0FBS0gsS0FEbkM7QUFBQSxVQUNDTyxPQURELFVBQ0NBLE9BREQ7QUFBQSxVQUNVQyxRQURWLFVBQ1VBLFFBRFY7QUFBQSxVQUNvQkMsS0FEcEIsVUFDb0JBLEtBRHBCO0FBQUEsVUFFQ0MsUUFGRCxHQUVjRCxNQUFNRSxNQUZwQixDQUVDRCxRQUZEO0FBQUEsVUFHQ1AsS0FIRCxHQUdXLEtBQUtHLEtBSGhCLENBR0NILEtBSEQ7OztBQUtQLFVBQU1TLElBQUksb0JBQVlKLFFBQVosRUFBc0JLLElBQXRCLENBQ1IsVUFBQ0MsRUFBRDtBQUFBLGVBQVFOLFNBQVNNLEVBQVQsRUFBYUosUUFBYixLQUEwQkEsUUFBbEM7QUFBQSxPQURRLENBQVY7O0FBSUEsVUFBTUssVUFBVVAsU0FBU0ksQ0FBVCxDQUFoQjs7QUFFQSxVQUFJLENBQUNHLE9BQUQsSUFBWUEsUUFBUUMsVUFBUixJQUFzQixHQUF0QyxFQUEyQztBQUN6QyxlQUFPLG1EQUFlRCxPQUFmLENBQVA7QUFDRDs7QUFFRCxVQUFJQSxRQUFRRSxXQUFSLEtBQXdCLE1BQTVCLEVBQW9DO0FBQ2xDLGVBQU8sd0RBQVA7QUFDRDs7QUFFRCxVQUFJLENBQUNGLFFBQVFHLE1BQWIsRUFBcUI7QUFDbkIsZUFBTyx3REFBUDtBQUNEOztBQUVELGFBQ0U7QUFBQTtBQUFBO0FBQ0UseUJBREY7QUFFRSxtQkFBUSxRQUZWO0FBR0UsbUJBQVMsQ0FIWDtBQUlFLHFCQUFXWCxRQUFRaEIsSUFKckI7QUFLRSx3QkFBYyxLQUFLVSxZQUxyQjtBQU1FLHdCQUFjLEtBQUtHO0FBTnJCO0FBUUU7QUFBQTtBQUFBLFlBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLEVBQXZCLEVBQTJCLElBQUksRUFBL0IsRUFBbUMsSUFBSSxFQUF2QyxFQUEyQyxJQUFJLEVBQS9DO0FBQ0U7QUFBQTtBQUFBLGNBQU0sUUFBUUQsS0FBZCxFQUFxQixXQUFXSSxRQUFRVixJQUF4QztBQUNFLDhEQUFZLE9BQU8sUUFBbkIsRUFBNkIsV0FBV1UsUUFBUWIsS0FBaEQsR0FERjtBQUVFO0FBQUE7QUFBQTtBQUNFO0FBQ0UsNkJBQWEscUNBQWtCcUIsUUFBUUcsTUFBMUIsQ0FEZjtBQUVFLDBCQUFVLEtBQUtiLFFBRmpCO0FBR0U7QUFIRjtBQURGO0FBRkY7QUFERjtBQVJGLE9BREY7QUF1QkQ7Ozs7O0FBR0hOLFdBQVdvQixTQUFYLEdBQXVCO0FBQ3JCWixXQUFTLG9CQUFVYSxNQUFWLENBQWlCQyxVQURMO0FBRXJCWixTQUFPLG9CQUFVVyxNQUFWLENBQWlCQyxVQUZIO0FBR3JCYixZQUFVLG9CQUFVWSxNQUFWLENBQWlCQztBQUhOLENBQXZCOztBQU1BLElBQU1DLGtCQUFrQixTQUFsQkEsZUFBa0I7QUFBQSxNQUFHZCxRQUFILFFBQUdBLFFBQUg7QUFBQSxTQUFtQixFQUFFQSxrQkFBRixFQUFuQjtBQUFBLENBQXhCOztrQkFFZSx5QkFBUWMsZUFBUixFQUF5QiwwQkFBV2hDLE1BQVgsRUFBbUJTLFVBQW5CLENBQXpCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDckdmOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBR0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFVQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUVBOzs7O0FBQ0E7Ozs7QUFDQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFRQTs7OztBQUNBOzs7O0FBRUE7O0FBa0JBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7OztBQWhDQTtBQUNBO0FBQ0E7O0FBRUE7O0FBN0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0lBcURNd0IsWTs7O0FBQ0o7OztBQXlCQSwwQkFBYztBQUFBOztBQUFBOztBQUVaOztBQUVBOzs7Ozs7QUFKWTs7QUFBQSxVQTZMZEMsbUJBN0xjO0FBQUEsMEZBNkxRLGlCQUFPQyxDQUFQO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDcEI7QUFDTUMsb0JBRmMsR0FFUEQsRUFBRUUsTUFBRixDQUFTQyxLQUFULENBQWUsQ0FBZixDQUZPOztBQUFBLHNCQUloQkYsS0FBS0csSUFBTCxDQUFVQyxPQUFWLENBQWtCLFFBQWxCLE1BQWdDLENBSmhCO0FBQUE7QUFBQTtBQUFBOztBQUtaQyx3QkFMWSxHQUtELElBQUlDLFFBQUosRUFMQzs7QUFNbEJELHlCQUFTRSxNQUFULENBQWdCLE9BQWhCLEVBQXlCUCxJQUF6Qjs7QUFOa0I7QUFBQSx1QkFRMkIsTUFBSzFCLEtBQUwsQ0FBV2tDLGNBQVgsQ0FBMEI7QUFDckVIO0FBRHFFLGlCQUExQixDQVIzQjs7QUFBQTtBQUFBO0FBUVZmLDBCQVJVLFNBUVZBLFVBUlU7QUFRRW1CLHFCQVJGLFNBUUVBLEtBUkY7QUFRU0MsdUJBUlQsU0FRU0EsT0FSVDs7QUFBQSxzQkFZZHBCLGNBQWMsR0FaQTtBQUFBO0FBQUE7QUFBQTs7QUFhaEI7QUFDQXFCLHdCQUFRRixLQUFSLENBQWNuQixVQUFkLEVBQTBCbUIsS0FBMUI7QUFkZ0I7O0FBQUE7QUFrQkFHLG1CQWxCQSxHQWtCUUYsT0FsQlIsQ0FrQlZHLFFBbEJVO0FBb0JWQywyQkFwQlUsR0FvQk0sTUFBS3hDLEtBcEJYLENBb0JWd0MsV0FwQlU7QUFzQlpDLDRCQXRCWSxHQXNCR0QsWUFBWUUsaUJBQVosRUF0Qkg7QUF1QlpDLHNDQXZCWSxHQXVCYUYsYUFBYUcsWUFBYixDQUM3QixrQkFBT0MsS0FEc0IsRUFFN0IsV0FGNkIsRUFHN0IsRUFBRVAsUUFBRixFQUg2QixDQXZCYjtBQTZCWlEseUJBN0JZLEdBNkJBSCx1QkFBdUJJLHVCQUF2QixFQTdCQTtBQStCWkMsaUNBL0JZLEdBK0JRLHFCQUFZQyxHQUFaLENBQWdCVCxXQUFoQixFQUE2QjtBQUNyRFUsa0NBQWdCUDtBQURxQyxpQkFBN0IsQ0EvQlI7QUFtQ1pRLDhCQW5DWSxHQW1DSywwQkFBaUJDLGlCQUFqQixDQUNyQkosaUJBRHFCLEVBRXJCRixTQUZxQixFQUdyQixHQUhxQixDQW5DTDs7O0FBeUNsQixzQkFBS3pDLFFBQUwsQ0FBYzhDLGNBQWQ7O0FBekNrQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxPQTdMUjs7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFVWixVQUFLOUMsUUFBTCxHQUFnQixVQUFDbUMsV0FBRCxFQUFpQjtBQUMvQixZQUFLeEMsS0FBTCxDQUFXSyxRQUFYLENBQW9CbUMsV0FBcEI7QUFDRCxLQUZEOztBQUlBOzs7O0FBSUEsVUFBS2EsS0FBTCxHQUFhO0FBQUEsYUFBTSxNQUFLQyxVQUFMLENBQWdCRCxLQUFoQixFQUFOO0FBQUEsS0FBYjs7QUFFQSxVQUFLRSxLQUFMLEdBQWEsTUFBS0EsS0FBTCxDQUFXQyxJQUFYLE9BQWI7QUFDQSxVQUFLQyxZQUFMLEdBQW9CLE1BQUtBLFlBQUwsQ0FBa0JELElBQWxCLE9BQXBCO0FBQ0EsVUFBS0UsV0FBTCxHQUFtQixNQUFLQSxXQUFMLENBQWlCRixJQUFqQixPQUFuQjtBQUNBLFVBQUtHLFlBQUwsR0FBb0IsTUFBS0EsWUFBTCxDQUFrQkgsSUFBbEIsT0FBcEI7QUFDQSxVQUFLSSxtQkFBTCxHQUEyQixNQUFLQSxtQkFBTCxDQUF5QkosSUFBekIsT0FBM0I7QUFDQSxVQUFLSyxtQkFBTCxHQUEyQixNQUFLQSxtQkFBTCxDQUF5QkwsSUFBekIsT0FBM0I7QUFDQSxVQUFLTSxXQUFMLEdBQW1CLE1BQUtBLFdBQUwsQ0FBaUJOLElBQWpCLE9BQW5CO0FBQ0EsVUFBS08sYUFBTCxHQUFxQixNQUFLQSxhQUFMLENBQW1CUCxJQUFuQixPQUFyQjtBQUNBLFVBQUtRLGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCUixJQUF0QixPQUF4QjtBQUNBLFVBQUtTLGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCVCxJQUF0QixPQUF4QjtBQUNBLFVBQUtVLGdCQUFMLEdBQXdCLE1BQUtBLGdCQUFMLENBQXNCVixJQUF0QixPQUF4QjtBQUNBLFVBQUtXLGtCQUFMLEdBQTBCLE1BQUtBLGtCQUFMLENBQXdCWCxJQUF4QixPQUExQjs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQTVDWTtBQTZDYjs7QUFFRDs7Ozs7Ozs7OzBCQUtNL0IsQyxFQUFHO0FBQ1BBLFFBQUUyQyxjQUFGO0FBRE8sVUFFQzVCLFdBRkQsR0FFaUIsS0FBS3hDLEtBRnRCLENBRUN3QyxXQUZEOzs7QUFJUCxVQUFNVyxpQkFBaUIsbUJBQVVJLEtBQVYsQ0FBZ0I5QixDQUFoQixFQUFtQmUsV0FBbkIsRUFBZ0MsQ0FBaEMsQ0FBdkI7O0FBRUEsV0FBS25DLFFBQUwsQ0FBYzhDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzttQ0FJZTtBQUFBLFVBQ0xYLFdBREssR0FDVyxLQUFLeEMsS0FEaEIsQ0FDTHdDLFdBREs7OztBQUdiLFVBQU1XLGlCQUFpQixtQkFBVWtCLGVBQVYsQ0FBMEI3QixXQUExQixFQUF1QyxZQUF2QyxDQUF2Qjs7QUFFQSxXQUFLbkMsUUFBTCxDQUFjOEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O2tDQUljO0FBQUEsVUFDSlgsV0FESSxHQUNZLEtBQUt4QyxLQURqQixDQUNKd0MsV0FESTs7O0FBR1osVUFBTVcsaUJBQWlCLG1CQUFVa0IsZUFBVixDQUEwQjdCLFdBQTFCLEVBQXVDLFlBQXZDLENBQXZCOztBQUVBLFdBQUtuQyxRQUFMLENBQWM4QyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7bUNBSWU7QUFBQSxVQUNMWCxXQURLLEdBQ1csS0FBS3hDLEtBRGhCLENBQ0x3QyxXQURLOzs7QUFHYixVQUFNVyxpQkFBaUIsbUJBQVVrQixlQUFWLENBQTBCN0IsV0FBMUIsRUFBdUMsWUFBdkMsQ0FBdkI7O0FBRUEsV0FBS25DLFFBQUwsQ0FBYzhDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7OzswQ0FJc0I7QUFBQSxVQUNaWCxXQURZLEdBQ0ksS0FBS3hDLEtBRFQsQ0FDWndDLFdBRFk7OztBQUdwQixVQUFNVyxpQkFBaUIsbUJBQVVrQixlQUFWLENBQ3JCN0IsV0FEcUIsRUFFckIscUJBRnFCLENBQXZCOztBQUtBLFdBQUtuQyxRQUFMLENBQWM4QyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozs7MENBSXNCO0FBQUEsVUFDWlgsV0FEWSxHQUNJLEtBQUt4QyxLQURULENBQ1p3QyxXQURZOzs7QUFHcEIsVUFBTVcsaUJBQWlCLG1CQUFVa0IsZUFBVixDQUNyQjdCLFdBRHFCLEVBRXJCLG1CQUZxQixDQUF2Qjs7QUFLQSxXQUFLbkMsUUFBTCxDQUFjOEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O2tDQUljO0FBQUEsVUFDSlgsV0FESSxHQUNZLEtBQUt4QyxLQURqQixDQUNKd0MsV0FESTs7O0FBR1osVUFBTVcsaUJBQWlCLG1CQUFVbUIsaUJBQVYsQ0FBNEI5QixXQUE1QixFQUF5QyxNQUF6QyxDQUF2Qjs7QUFFQSxXQUFLbkMsUUFBTCxDQUFjOEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O29DQUlnQjtBQUFBLFVBQ05YLFdBRE0sR0FDVSxLQUFLeEMsS0FEZixDQUNOd0MsV0FETTs7O0FBR2QsVUFBTVcsaUJBQWlCLG1CQUFVbUIsaUJBQVYsQ0FBNEI5QixXQUE1QixFQUF5QyxRQUF6QyxDQUF2Qjs7QUFFQSxXQUFLbkMsUUFBTCxDQUFjOEMsY0FBZDtBQUNEOztBQUVEOzs7Ozs7O3VDQUltQjtBQUFBLFVBQ1RYLFdBRFMsR0FDTyxLQUFLeEMsS0FEWixDQUNUd0MsV0FEUzs7O0FBR2pCLFVBQU1XLGlCQUFpQixtQkFBVW1CLGlCQUFWLENBQ3JCOUIsV0FEcUIsRUFFckIsV0FGcUIsQ0FBdkI7O0FBS0EsV0FBS25DLFFBQUwsQ0FBYzhDLGNBQWQ7QUFDRDs7QUFFRDs7Ozs7Ozt1Q0FJbUI7QUFBQSxVQUNUWCxXQURTLEdBQ08sS0FBS3hDLEtBRFosQ0FDVHdDLFdBRFM7OztBQUdqQixVQUFNVyxpQkFBaUIsbUJBQVVtQixpQkFBVixDQUE0QjlCLFdBQTVCLEVBQXlDLE1BQXpDLENBQXZCOztBQUVBLFdBQUtuQyxRQUFMLENBQWM4QyxjQUFkO0FBQ0Q7O0FBRUQ7Ozs7Ozt5Q0FHcUI7QUFDbkIsV0FBS29CLEtBQUwsQ0FBV0MsS0FBWCxHQUFtQixJQUFuQjtBQUNBLFdBQUtELEtBQUwsQ0FBV0UsS0FBWDtBQUNEOztBQUVEOzs7Ozs7OztBQWdEQTs7Ozs7O3FDQU1pQkMsTyxFQUFTO0FBQUEsVUFDaEJsQyxXQURnQixHQUNBLEtBQUt4QyxLQURMLENBQ2hCd0MsV0FEZ0I7OztBQUd4QixVQUFNVyxpQkFBaUIsbUJBQVVlLGdCQUFWLENBQTJCMUIsV0FBM0IsRUFBd0NrQyxPQUF4QyxDQUF2Qjs7QUFFQSxVQUFJdkIsY0FBSixFQUFvQjtBQUNsQixhQUFLOUMsUUFBTCxDQUFjOEMsY0FBZDtBQUNBO0FBQ0Q7O0FBRUQ7QUFDRDs7QUFFRDs7OztvQ0FDZ0J3QixLLEVBQU87QUFDckIsY0FBUUEsTUFBTUMsT0FBTixFQUFSO0FBQ0UsYUFBSyxrQkFBT0MsTUFBWjtBQUNFLGlCQUFPO0FBQ0xDLHVDQURLO0FBRUxDLHNCQUFVO0FBRkwsV0FBUDs7QUFLRjtBQUNFLGlCQUFPLElBQVA7QUFSSjtBQVVEO0FBQ0Q7O0FBRUE7Ozs7aUNBQ2FKLEssRUFBTztBQUNsQixjQUFRQSxNQUFNQyxPQUFOLEVBQVI7QUFDRSxhQUFLLFlBQUw7QUFDRSxpQkFBTyx1QkFBUDs7QUFFRjtBQUNFLGlCQUFPLElBQVA7QUFMSjtBQU9EO0FBQ0Q7Ozs7NkJBRVM7QUFBQTs7QUFBQSxtQkFPSCxLQUFLNUUsS0FQRjtBQUFBLFVBRUxPLE9BRkssVUFFTEEsT0FGSztBQUFBLFVBR0x5RSxRQUhLLFVBR0xBLFFBSEs7QUFBQSxVQUlMM0UsUUFKSyxVQUlMQSxRQUpLO0FBQUEsVUFLTG1DLFdBTEssVUFLTEEsV0FMSztBQUFBLFVBTUx5QyxXQU5LLFVBTUxBLFdBTks7O0FBU1A7O0FBQ0EsVUFBTUMsV0FBVztBQUNmQyxjQUFNO0FBQ0pDLDJCQUFpQixxQkFEYjtBQUVKQyxzQkFBWSwrQ0FGUjtBQUdKQyxvQkFBVSxFQUhOO0FBSUpDLG1CQUFTO0FBSkw7QUFEUyxPQUFqQjs7QUFTQSxhQUNFO0FBQUE7QUFBQSxVQUFLLFdBQVdoRixRQUFRaEIsSUFBeEI7QUFDRyxTQUFDeUYsUUFBRCxHQUNDO0FBQUE7QUFBQSxZQUFLLFdBQVcsMEJBQVcsRUFBRSxtQkFBbUIsRUFBckIsRUFBWCxDQUFoQjtBQUNFO0FBQUE7QUFBQSxjQUFTLE9BQU0sT0FBZixFQUF1QixJQUFHLE9BQTFCLEVBQWtDLFdBQVUsUUFBNUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxPQUF2QixFQUErQixTQUFTLEtBQUt2QixZQUE3QztBQUNFO0FBREY7QUFERixXQURGO0FBTUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxNQUFmLEVBQXNCLElBQUcsTUFBekIsRUFBZ0MsV0FBVSxRQUExQztBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLE1BQXZCLEVBQThCLFNBQVMsS0FBS0ssV0FBNUM7QUFDRTtBQURGO0FBREYsV0FORjtBQVdFO0FBQUE7QUFBQSxjQUFTLE9BQU0sUUFBZixFQUF3QixJQUFHLFFBQTNCLEVBQW9DLFdBQVUsUUFBOUM7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxRQUF2QixFQUFnQyxTQUFTLEtBQUtDLGFBQTlDO0FBQ0U7QUFERjtBQURGLFdBWEY7QUFnQkU7QUFBQTtBQUFBLGNBQVMsT0FBTSxXQUFmLEVBQTJCLElBQUcsV0FBOUIsRUFBMEMsV0FBVSxRQUFwRDtBQUNFO0FBQUE7QUFBQTtBQUNFLDhCQUFXLFdBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFERixXQWhCRjtBQXdCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE1BQWYsRUFBc0IsSUFBRyxNQUF6QixFQUFnQyxXQUFVLFFBQTFDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsTUFBdkIsRUFBOEIsU0FBUyxLQUFLTixXQUE1QztBQUNFO0FBREY7QUFERixXQXhCRjtBQTZCRTtBQUFBO0FBQUEsY0FBUyxPQUFNLE9BQWYsRUFBdUIsSUFBRyxPQUExQixFQUFrQyxXQUFVLFFBQTVDO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsT0FBdkIsRUFBK0IsU0FBUyxLQUFLQyxZQUE3QztBQUNFO0FBREY7QUFERixXQTdCRjtBQWtDRTtBQUFBO0FBQUE7QUFDRSxxQkFBTSxnQkFEUjtBQUVFLGtCQUFHLGVBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsZ0JBRGI7QUFFRSx5QkFBUyxLQUFLQztBQUZoQjtBQUlFO0FBSkY7QUFMRixXQWxDRjtBQThDRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsY0FEYjtBQUVFLHlCQUFTLEtBQUtDO0FBRmhCO0FBSUU7QUFKRjtBQURGLFdBOUNGO0FBc0RFO0FBQUE7QUFBQSxjQUFTLE9BQU0sWUFBZixFQUE0QixJQUFHLFlBQS9CLEVBQTRDLFdBQVUsUUFBdEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxZQUF2QixFQUFvQyxjQUFwQztBQUNFO0FBREY7QUFERixXQXRERjtBQTJERTtBQUFBO0FBQUEsY0FBUyxPQUFNLGNBQWYsRUFBOEIsSUFBRyxjQUFqQyxFQUFnRCxXQUFVLFFBQTFEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsY0FBdkIsRUFBc0MsY0FBdEM7QUFDRTtBQURGO0FBREYsV0EzREY7QUFnRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBaEVGO0FBcUVFO0FBQUE7QUFBQSxjQUFTLE9BQU0sYUFBZixFQUE2QixJQUFHLGFBQWhDLEVBQThDLFdBQVUsUUFBeEQ7QUFDRTtBQUFBO0FBQUEsZ0JBQVksY0FBVyxhQUF2QixFQUFxQyxjQUFyQztBQUNFO0FBREY7QUFERixXQXJFRjtBQTBFRTtBQUFBO0FBQUEsY0FBUyxPQUFNLGFBQWYsRUFBNkIsSUFBRyxhQUFoQyxFQUE4QyxXQUFVLFFBQXhEO0FBQ0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsYUFBdkIsRUFBcUMsY0FBckM7QUFDRTtBQURGO0FBREYsV0ExRUY7QUErRUU7QUFBQTtBQUFBLGNBQVMsT0FBTSxhQUFmLEVBQTZCLElBQUcsYUFBaEMsRUFBOEMsV0FBVSxRQUF4RDtBQUNFO0FBQUE7QUFBQSxnQkFBWSxjQUFXLGFBQXZCLEVBQXFDLGNBQXJDO0FBQ0U7QUFERjtBQURGLFdBL0VGO0FBb0ZFO0FBQUE7QUFBQSxjQUFTLE9BQU0sY0FBZixFQUE4QixJQUFHLGNBQWpDLEVBQWdELFdBQVUsUUFBMUQ7QUFDRTtBQUFBO0FBQUE7QUFDRSw4QkFBVyxjQURiO0FBRUUsMEJBQVUsT0FBTyxLQUFLN0QsS0FBTCxDQUFXa0MsY0FBbEIsS0FBcUMsVUFGakQ7QUFHRSx5QkFBUyxLQUFLaUM7QUFIaEI7QUFLRSx3RUFMRjtBQU1FO0FBQ0Usc0JBQUssTUFEUDtBQUVFLHdCQUFPLG9CQUZUO0FBR0UsMEJBQVUsS0FBSzNDLG1CQUhqQjtBQUlFLHFCQUFLLGFBQUMrQyxLQUFELEVBQVc7QUFDZCx5QkFBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0QsaUJBTkg7QUFPRSx1QkFBTyxFQUFFaUIsU0FBUyxNQUFYO0FBUFQ7QUFORjtBQURGLFdBcEZGO0FBc0dFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGlCQURSO0FBRUUsa0JBQUcsa0JBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsaUJBQXZCLEVBQXlDLGNBQXpDO0FBQ0U7QUFERjtBQUxGLFdBdEdGO0FBK0dFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGdCQURSO0FBRUUsa0JBQUcsZ0JBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsZ0JBQXZCLEVBQXdDLGNBQXhDO0FBQ0U7QUFERjtBQUxGLFdBL0dGO0FBd0hFO0FBQUE7QUFBQTtBQUNFLHFCQUFNLGdCQURSO0FBRUUsa0JBQUcsZ0JBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBO0FBQ0UsOEJBQVcsZ0JBRGI7QUFFRSx5QkFBUyxLQUFLdkI7QUFGaEI7QUFJRTtBQUpGO0FBTEYsV0F4SEY7QUFvSUU7QUFBQTtBQUFBO0FBQ0UscUJBQU0sZUFEUjtBQUVFLGtCQUFHLGVBRkw7QUFHRSx5QkFBVTtBQUhaO0FBS0U7QUFBQTtBQUFBLGdCQUFZLGNBQVcsZUFBdkIsRUFBdUMsY0FBdkM7QUFDRTtBQURGO0FBTEY7QUFwSUYsU0FERCxHQStJRyxJQWhKTjtBQWlKRTtBQUFBO0FBQUEsWUFBSyxXQUFXLDBCQUFXLEVBQUUsZUFBZSxFQUFqQixFQUFYLENBQWhCO0FBQ0U7QUFDRTs7QUFERixjQUdFLGFBQWF6QixXQUhmO0FBSUUsc0JBQVVuQztBQUNWOztBQUxGLGNBT0UsYUFBYTRFO0FBQ2I7O0FBRUE7O0FBVkYsY0FZRSxpQkFBaUIsS0FBS1EsZUFaeEI7QUFhRSwwQkFBYyxLQUFLQyxZQWJyQjtBQWNFLDRCQUFnQlI7QUFDaEI7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBdkJGLGNBeUJFLFVBQVVGLFFBekJaO0FBMEJFO0FBQ0E7O0FBRUE7O0FBRUE7O0FBRUE7O0FBRUE7O0FBbkNGLGNBcUNFLGtCQUFrQixLQUFLZDtBQUN2Qjs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFFQTs7QUFsREYsY0FvREUsT0FBTyxLQUFLWDtBQUNaOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOztBQTNFRixjQTZFRSxLQUFLLGFBQUNvQyxJQUFELEVBQVU7QUFDYixxQkFBS3JDLFVBQUwsR0FBa0JxQyxJQUFsQjtBQUNEO0FBL0VIO0FBREY7QUFqSkYsT0FERjtBQXVPRDs7Ozs7QUE1akJIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBM0RBO0FBUkE7O0FBNkVNcEUsWSxDQUlHSixTLEdBQVk7QUFDakJaLFdBQVMseUJBQVVhLE1BQVYsQ0FBaUJDLFVBRFQ7O0FBR2pCbUIsZUFBYSx5QkFBVXBCLE1BQVYsQ0FBaUJDLFVBSGI7QUFJakJoQixZQUFVLHlCQUFVdUYsSUFBVixDQUFldkUsVUFKUjs7QUFNakI0RCxlQUFhLHlCQUFVWSxNQU5OOztBQVFqQmIsWUFBVSx5QkFBVWMsSUFBVixDQUFlekUsVUFSUjs7QUFVakI7Ozs7QUFJQWEsa0JBQWdCLHlCQUFVMEQ7QUFkVCxDO0FBSmZyRSxZLENBcUJHd0UsWSxHQUFlO0FBQ3BCZixZQUFVLElBRFU7QUFFcEJDLGVBQWE7QUFGTyxDO2tCQTBoQlQsa0RBQW1CMUQsWUFBbkIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxbkJmOztBQUVBOztBQUpBOztBQVdBLElBQU15RSxvQkFBb0IsZ0NBQXVCLENBQy9DO0FBQ0VDLHNDQURGO0FBRUVuQjtBQUZGLENBRCtDLEVBSy9DO0FBQ0VtQix1Q0FERjtBQUVFbkI7QUFGRixDQUwrQyxDQUF2QixDQUExQjs7QUFXTyxJQUFNb0IsZ0RBQW9CLFNBQXBCQSxpQkFBb0IsR0FHNUI7QUFBQSxNQUZIQyxPQUVHLHVFQUZPLElBRVA7QUFBQSxNQURIQyxVQUNHLHVFQURVSixpQkFDVjs7QUFDSCxNQUFJRyxZQUFZLElBQWhCLEVBQXNCO0FBQ3BCLFdBQU8scUJBQVlFLFdBQVosQ0FBd0JELFVBQXhCLENBQVA7QUFDRDtBQUNELFNBQU8scUJBQVlFLGlCQUFaLENBQThCLDZCQUFlSCxPQUFmLENBQTlCLEVBQXVEQyxVQUF2RCxDQUFQO0FBQ0QsQ0FSTTs7a0JBVVFGLGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNoQ2Y7Ozs7OztrQkFNZTtBQUNiLGFBQVc7QUFDVCx3QkFBb0I7QUFDbEJLLGtCQUFZLE1BRE07QUFFbEJDLGNBQVEsZ0JBRlU7QUFHbEJuQixrQkFBWSxrQkFITTtBQUlsQkMsZ0JBQVUsTUFKUTtBQUtsQkMsZUFBUztBQUxTLEtBRFg7QUFRVCwwQkFBc0I7QUFDcEJrQixpQkFBVyxnQkFEUztBQUVwQkMsY0FBUSxNQUZZO0FBR3BCcEIsZ0JBQVUsTUFIVTtBQUlwQnFCLGlCQUFXO0FBSlMsS0FSYjtBQWNULDJDQUF1QztBQUNyQ0MsY0FBUSxlQUQ2QjtBQUVyQ3JCLGVBQVM7QUFGNEIsS0FkOUI7QUFrQlQsbUNBQStCO0FBQzdCcUIsY0FBUSxlQURxQjtBQUU3QnJCLGVBQVM7QUFDVDtBQUg2QixLQWxCdEI7QUF1QlQsOEJBQTBCO0FBQ3hCSCx1QkFBaUIsZ0JBRE87QUFFeEJ5QixrQkFBWSxnQkFGWTtBQUd4QkMsYUFBTyxNQUhpQjtBQUl4QnpCLGtCQUFZLGtDQUpZO0FBS3hCMEIsaUJBQVcsUUFMYTtBQU14QkgsY0FBUSxRQU5nQjtBQU94QnJCLGVBQVM7QUFQZSxLQXZCakI7QUFnQ1QscUNBQWlDO0FBQy9CSCx1QkFBaUIscUJBRGM7QUFFL0JDLGtCQUFZLCtDQUZtQjtBQUcvQkMsZ0JBQVUsTUFIcUI7QUFJL0JDLGVBQVM7QUFKc0I7QUFoQ3hCLEdBREU7QUF3Q2JoRyxRQUFNO0FBQ0o7QUFESSxHQXhDTztBQTJDYkMsWUFBVTtBQUNSSSxVQUFNO0FBREU7QUEzQ0csQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRGY7Ozs7QUFDQTs7OztBQUVBOzs7O0FBRUEsSUFBTU4sU0FBUztBQUNiaUYsU0FBTztBQUNMeUMsV0FBTyxNQURGO0FBRUw7QUFDQTtBQUNBQyxnQkFBWTtBQUpQO0FBRE0sQ0FBZjs7QUFTQTs7Ozs7Ozs7QUFuQkE7Ozs7O0lBMkJNQyxNOzs7QUFNSixrQkFBWWxILEtBQVosRUFBbUI7QUFBQTs7QUFBQSxzSUFDWEEsS0FEVzs7QUFFakIsVUFBS00sS0FBTCxHQUFhLEVBQWI7QUFGaUI7QUFHbEI7Ozs7NkJBRVE7QUFBQSxtQkFDeUIsS0FBS04sS0FEOUI7QUFBQSxVQUNDeUMsWUFERCxVQUNDQSxZQUREO0FBQUEsVUFDZWtDLEtBRGYsVUFDZUEsS0FEZjs7O0FBR1AsVUFBTXdDLFNBQVMxRSxhQUFhMkUsU0FBYixDQUF1QnpDLE1BQU0wQyxXQUFOLENBQWtCLENBQWxCLENBQXZCLENBQWY7O0FBSE8sNEJBSVNGLE9BQU9HLE9BQVAsRUFKVDtBQUFBLFVBSUNoRixHQUpELG1CQUlDQSxHQUpEOztBQUtQLFVBQU1ULE9BQU9zRixPQUFPdkMsT0FBUCxFQUFiOztBQUVBLFVBQUkvQyxTQUFTLGtCQUFPZ0IsS0FBcEIsRUFBMkI7QUFDekIsZUFDRTtBQUFBO0FBQUE7QUFDRSxpREFBSyxLQUFLLEtBQVYsRUFBaUIsS0FBS1AsR0FBdEIsRUFBMkIsT0FBT2hELE9BQU9pRixLQUF6QztBQURGLFNBREY7QUFLRDs7QUFFRCxhQUFPLDBDQUFQO0FBQ0Q7Ozs7O0FBM0JHMkMsTSxDQUNHL0YsUyxHQUFZO0FBQ2pCc0IsZ0JBQWMsb0JBQVVyQixNQUFWLENBQWlCQyxVQURkO0FBRWpCc0QsU0FBTyxvQkFBVTRDLEdBQVYsQ0FBY2xHO0FBRkosQztrQkE2Qk42RixNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1FDdkNDTSxjLEdBQUFBLGM7UUFJQUMsZSxHQUFBQSxlOztBQXBCaEI7Ozs7QUFDQTs7Ozs7O0FBSEE7QUFDQTtBQUlBLElBQU1DLGVBQWUsVUFBckI7QUFDQSxJQUFNQyxnQkFBZ0IsdUJBQXRCOztBQUVBLFNBQVNDLGFBQVQsQ0FBdUJDLEtBQXZCLEVBQThCQyxZQUE5QixFQUE0Q0MsUUFBNUMsRUFBc0Q7QUFDcEQsTUFBTUMsT0FBT0YsYUFBYUcsT0FBYixFQUFiO0FBQ0EsTUFBSUMsaUJBQUo7QUFBQSxNQUNFQyxjQURGO0FBRUEsU0FBTyxDQUFDRCxXQUFXTCxNQUFNTyxJQUFOLENBQVdKLElBQVgsQ0FBWixNQUFrQyxJQUF6QyxFQUErQztBQUM3Q0csWUFBUUQsU0FBU0csS0FBakI7QUFDQU4sYUFBU0ksS0FBVCxFQUFnQkEsUUFBUUQsU0FBUyxDQUFULEVBQVlJLE1BQXBDO0FBQ0Q7QUFDRjs7QUFFTSxTQUFTZCxjQUFULENBQXdCTSxZQUF4QixFQUFzQ0MsUUFBdEMsRUFBZ0R0RixZQUFoRCxFQUE4RDtBQUNuRW1GLGdCQUFjRixZQUFkLEVBQTRCSSxZQUE1QixFQUEwQ0MsUUFBMUM7QUFDRDs7QUFFTSxTQUFTTixlQUFULENBQXlCSyxZQUF6QixFQUF1Q0MsUUFBdkMsRUFBaUR0RixZQUFqRCxFQUErRDtBQUNwRW1GLGdCQUFjRCxhQUFkLEVBQTZCRyxZQUE3QixFQUEyQ0MsUUFBM0M7QUFDRDs7QUFFTSxJQUFNUSxrQ0FBYSxTQUFiQSxVQUFhO0FBQUEsU0FBUztBQUFBO0FBQUEsTUFBTSxPQUFPLGdCQUFNQyxNQUFuQjtBQUE0QnhJLFVBQU15STtBQUFsQyxHQUFUO0FBQUEsQ0FBbkI7O0FBRUEsSUFBTUMsb0NBQWMsU0FBZEEsV0FBYztBQUFBLFNBQVM7QUFBQTtBQUFBLE1BQU0sT0FBTyxnQkFBTUMsT0FBbkI7QUFBNkIzSSxVQUFNeUk7QUFBbkMsR0FBVDtBQUFBLENBQXBCOztrQkFHUTtBQUNiakIsZ0NBRGE7QUFFYmUsd0JBRmE7O0FBSWJkLGtDQUphO0FBS2JpQjtBQUxhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQy9CZjs7Ozs7OztBQU9BOzs7QUFHTyxJQUFNRSwwQkFBUztBQUNwQkMsWUFBVSxVQURVO0FBRXBCQyxhQUFXLFVBRlM7O0FBSXBCQyxNQUFJLFlBSmdCO0FBS3BCQyxNQUFJLFlBTGdCO0FBTXBCQyxNQUFJLGNBTmdCO0FBT3BCQyxNQUFJLGFBUGdCO0FBUXBCQyxNQUFJLGFBUmdCO0FBU3BCQyxNQUFJLFlBVGdCOztBQVdwQkMsTUFBSSxtQkFYZ0I7QUFZcEJDLE1BQUkscUJBWmdCOztBQWNwQm5FLFFBQU0sWUFkYzs7QUFnQnBCb0UsY0FBWSxZQWhCUTs7QUFrQnBCMUUsVUFBUSxRQWxCWTtBQW1CcEJoQyxTQUFPLGNBbkJhO0FBb0JwQjJHLFNBQU87QUFwQmEsQ0FBZjs7QUF1QlA7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNLE1BRGM7QUFFcEJ2RSxRQUFNLE1BRmM7QUFHcEJ3RSxVQUFRLFFBSFk7QUFJcEJDLGlCQUFlLGVBSks7QUFLcEJDLGFBQVcsV0FMUztBQU1wQkMsYUFBVztBQU5TLENBQWY7O0FBU1A7OztBQUdPLElBQU1DLDBCQUFTO0FBQ3BCQyxRQUFNO0FBRGMsQ0FBZjs7QUFJUDs7O0FBR08sSUFBTUMsZ0NBQVksV0FBbEI7O0FBRVA7OztBQUdPLElBQU1DLDRCQUFVLFNBQWhCO0FBQ0EsSUFBTUMsb0NBQWMsYUFBcEI7O2tCQUVRO0FBQ2J2QixnQkFEYTtBQUViYSxnQkFGYTtBQUdiTTtBQUhhLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDN0RmOzs7OzttQkFFU3hJLFk7Ozs7Ozs7Ozt3QkFFQTJFLGlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ05UOztBQUVPLElBQU1rRSx3QkFBUTtBQUNuQjdLLFFBQU07QUFDSmdHLGFBQVMsRUFETDtBQUVKeUIsV0FBTztBQUZILEdBRGE7QUFLbkJxRCxVQUFRO0FBQ043RCxZQUFRLGdCQURGO0FBRU5FLFlBQVEsTUFGRjtBQUdOcEIsY0FBVSxFQUhKO0FBSU5nRixlQUFXLEVBSkw7QUFLTi9FLGFBQVM7QUFMSCxHQUxXO0FBWW5CZ0YsVUFBUTtBQUNONUQsZUFBVyxFQURMO0FBRU5oSCxlQUFXO0FBRkwsR0FaVztBQWdCbkI2SSxVQUFRO0FBQ04xQixXQUFPLHlCQUREO0FBRU4wRCxlQUFXLEtBRkw7QUFHTkMsaUJBQWE7QUFIUCxHQWhCVztBQXFCbkI5QixXQUFTO0FBQ1A3QixXQUFPO0FBREE7QUFyQlUsQ0FBZDs7a0JBMEJRc0QsSyIsImZpbGUiOiIzLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTE2LjUgNnYxMS41YzAgMi4yMS0xLjc5IDQtNCA0cy00LTEuNzktNC00VjVjMC0xLjM4IDEuMTItMi41IDIuNS0yLjVzMi41IDEuMTIgMi41IDIuNXYxMC41YzAgLjU1LS40NSAxLTEgMXMtMS0uNDUtMS0xVjZIMTB2OS41YzAgMS4zOCAxLjEyIDIuNSAyLjUgMi41czIuNS0xLjEyIDIuNS0yLjVWNWMwLTIuMjEtMS43OS00LTQtNFM3IDIuNzkgNyA1djEyLjVjMCAzLjA0IDIuNDYgNS41IDUuNSA1LjVzNS41LTIuNDYgNS41LTUuNVY2aC0xLjV6JyB9KTtcblxudmFyIEF0dGFjaEZpbGUgPSBmdW5jdGlvbiBBdHRhY2hGaWxlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5BdHRhY2hGaWxlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShBdHRhY2hGaWxlKTtcbkF0dGFjaEZpbGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQXR0YWNoRmlsZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9BdHRhY2hGaWxlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9BdHRhY2hGaWxlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTkuNCAxNi42TDQuOCAxMmw0LjYtNC42TDggNmwtNiA2IDYgNiAxLjQtMS40em01LjIgMGw0LjYtNC42LTQuNi00LjZMMTYgNmw2IDYtNiA2LTEuNC0xLjR6JyB9KTtcblxudmFyIENvZGUgPSBmdW5jdGlvbiBDb2RlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Db2RlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShDb2RlKTtcbkNvZGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gQ29kZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db2RlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Db2RlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTcgMTV2MmgxMHYtMkg3em0tNCA2aDE4di0ySDN2MnptMC04aDE4di0ySDN2MnptNC02djJoMTBWN0g3ek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduQ2VudGVyID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25DZW50ZXIocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduQ2VudGVyID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkNlbnRlcik7XG5Gb3JtYXRBbGlnbkNlbnRlci5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkNlbnRlcjtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlci5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25DZW50ZXIuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMyAyMWgxOHYtMkgzdjJ6bTAtNGgxOHYtMkgzdjJ6bTAtNGgxOHYtMkgzdjJ6bTAtNGgxOFY3SDN2MnptMC02djJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnbkp1c3RpZnkgPSBmdW5jdGlvbiBGb3JtYXRBbGlnbkp1c3RpZnkocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEFsaWduSnVzdGlmeSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25KdXN0aWZ5KTtcbkZvcm1hdEFsaWduSnVzdGlmeS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRBbGlnbkp1c3RpZnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkp1c3RpZnkuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTUgMTVIM3YyaDEydi0yem0wLThIM3YyaDEyVjd6TTMgMTNoMTh2LTJIM3Yyem0wIDhoMTh2LTJIM3Yyek0zIDN2MmgxOFYzSDN6JyB9KTtcblxudmFyIEZvcm1hdEFsaWduTGVmdCA9IGZ1bmN0aW9uIEZvcm1hdEFsaWduTGVmdChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25MZWZ0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShGb3JtYXRBbGlnbkxlZnQpO1xuRm9ybWF0QWxpZ25MZWZ0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduTGVmdDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduTGVmdC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zIDIxaDE4di0ySDN2MnptNi00aDEydi0ySDl2MnptLTYtNGgxOHYtMkgzdjJ6bTYtNGgxMlY3SDl2MnpNMyAzdjJoMThWM0gzeicgfSk7XG5cbnZhciBGb3JtYXRBbGlnblJpZ2h0ID0gZnVuY3Rpb24gRm9ybWF0QWxpZ25SaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0QWxpZ25SaWdodCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0QWxpZ25SaWdodCk7XG5Gb3JtYXRBbGlnblJpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdEFsaWduUmlnaHQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25SaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xNS42IDEwLjc5Yy45Ny0uNjcgMS42NS0xLjc3IDEuNjUtMi43OSAwLTIuMjYtMS43NS00LTQtNEg3djE0aDcuMDRjMi4wOSAwIDMuNzEtMS43IDMuNzEtMy43OSAwLTEuNTItLjg2LTIuODItMi4xNS0zLjQyek0xMCA2LjVoM2MuODMgMCAxLjUuNjcgMS41IDEuNXMtLjY3IDEuNS0xLjUgMS41aC0zdi0zem0zLjUgOUgxMHYtM2gzLjVjLjgzIDAgMS41LjY3IDEuNSAxLjVzLS42NyAxLjUtMS41IDEuNXonIH0pO1xuXG52YXIgRm9ybWF0Qm9sZCA9IGZ1bmN0aW9uIEZvcm1hdEJvbGQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdEJvbGQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEJvbGQpO1xuRm9ybWF0Qm9sZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRCb2xkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMTAgNHYzaDIuMjFsLTMuNDIgOEg2djNoOHYtM2gtMi4yMWwzLjQyLThIMThWNHonIH0pO1xuXG52YXIgRm9ybWF0SXRhbGljID0gZnVuY3Rpb24gRm9ybWF0SXRhbGljKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRJdGFsaWMgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdEl0YWxpYyk7XG5Gb3JtYXRJdGFsaWMubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0SXRhbGljO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEl0YWxpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0SXRhbGljLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTQgMTAuNWMtLjgzIDAtMS41LjY3LTEuNSAxLjVzLjY3IDEuNSAxLjUgMS41IDEuNS0uNjcgMS41LTEuNS0uNjctMS41LTEuNS0xLjV6bTAtNmMtLjgzIDAtMS41LjY3LTEuNSAxLjVTMy4xNyA3LjUgNCA3LjUgNS41IDYuODMgNS41IDYgNC44MyA0LjUgNCA0LjV6bTAgMTJjLS44MyAwLTEuNS42OC0xLjUgMS41cy42OCAxLjUgMS41IDEuNSAxLjUtLjY4IDEuNS0xLjUtLjY3LTEuNS0xLjUtMS41ek03IDE5aDE0di0ySDd2MnptMC02aDE0di0ySDd2MnptMC04djJoMTRWNUg3eicgfSk7XG5cbnZhciBGb3JtYXRMaXN0QnVsbGV0ZWQgPSBmdW5jdGlvbiBGb3JtYXRMaXN0QnVsbGV0ZWQocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZvcm1hdExpc3RCdWxsZXRlZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0TGlzdEJ1bGxldGVkKTtcbkZvcm1hdExpc3RCdWxsZXRlZC5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGb3JtYXRMaXN0QnVsbGV0ZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0QnVsbGV0ZWQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMiAxN2gydi41SDN2MWgxdi41SDJ2MWgzdi00SDJ2MXptMS05aDFWNEgydjFoMXYzem0tMSAzaDEuOEwyIDEzLjF2LjloM3YtMUgzLjJMNSAxMC45VjEwSDJ2MXptNS02djJoMTRWNUg3em0wIDE0aDE0di0ySDd2MnptMC02aDE0di0ySDd2MnonIH0pO1xuXG52YXIgRm9ybWF0TGlzdE51bWJlcmVkID0gZnVuY3Rpb24gRm9ybWF0TGlzdE51bWJlcmVkKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRMaXN0TnVtYmVyZWQgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEZvcm1hdExpc3ROdW1iZXJlZCk7XG5Gb3JtYXRMaXN0TnVtYmVyZWQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0TGlzdE51bWJlcmVkO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdExpc3ROdW1iZXJlZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdE51bWJlcmVkLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTYgMTdoM2wyLTRWN0g1djZoM3ptOCAwaDNsMi00VjdoLTZ2NmgzeicgfSk7XG5cbnZhciBGb3JtYXRRdW90ZSA9IGZ1bmN0aW9uIEZvcm1hdFF1b3RlKHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5Gb3JtYXRRdW90ZSA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0UXVvdGUpO1xuRm9ybWF0UXVvdGUubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gRm9ybWF0UXVvdGU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0UXVvdGUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFF1b3RlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTEyIDE3YzMuMzEgMCA2LTIuNjkgNi02VjNoLTIuNXY4YzAgMS45My0xLjU3IDMuNS0zLjUgMy41UzguNSAxMi45MyA4LjUgMTFWM0g2djhjMCAzLjMxIDIuNjkgNiA2IDZ6bS03IDJ2MmgxNHYtMkg1eicgfSk7XG5cbnZhciBGb3JtYXRVbmRlcmxpbmVkID0gZnVuY3Rpb24gRm9ybWF0VW5kZXJsaW5lZChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuRm9ybWF0VW5kZXJsaW5lZCA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRm9ybWF0VW5kZXJsaW5lZCk7XG5Gb3JtYXRVbmRlcmxpbmVkLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEZvcm1hdFVuZGVybGluZWQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0VW5kZXJsaW5lZC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00xOCA0SDZ2Mmw2LjUgNkw2IDE4djJoMTJ2LTNoLTdsNS01LTUtNWg3eicgfSk7XG5cbnZhciBGdW5jdGlvbnMgPSBmdW5jdGlvbiBGdW5jdGlvbnMocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkZ1bmN0aW9ucyA9ICgwLCBfcHVyZTIuZGVmYXVsdCkoRnVuY3Rpb25zKTtcbkZ1bmN0aW9ucy5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBGdW5jdGlvbnM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvRnVuY3Rpb25zLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9GdW5jdGlvbnMuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNNiAxNGwzIDN2NWg2di01bDMtM1Y5SDZ6bTUtMTJoMnYzaC0yek0zLjUgNS44NzVMNC45MTQgNC40NmwyLjEyIDIuMTIyTDUuNjIgNy45OTd6bTEzLjQ2LjcxbDIuMTIzLTIuMTIgMS40MTQgMS40MTRMMTguMzc1IDh6JyB9KTtcblxudmFyIEhpZ2hsaWdodCA9IGZ1bmN0aW9uIEhpZ2hsaWdodChwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSGlnaGxpZ2h0ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShIaWdobGlnaHQpO1xuSGlnaGxpZ2h0Lm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEhpZ2hsaWdodDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9IaWdobGlnaHQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0hpZ2hsaWdodC5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00yMCAySDRjLTEuMSAwLTIgLjktMiAydjEyYzAgMS4xLjkgMiAyIDJoMTRsNCA0VjRjMC0xLjEtLjktMi0yLTJ6bS0yIDEySDZ2LTJoMTJ2MnptMC0zSDZWOWgxMnYyem0wLTNINlY2aDEydjJ6JyB9KTtcblxudmFyIEluc2VydENvbW1lbnQgPSBmdW5jdGlvbiBJbnNlcnRDb21tZW50KHByb3BzKSB7XG4gIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICBfU3ZnSWNvbjIuZGVmYXVsdCxcbiAgICBwcm9wcyxcbiAgICBfcmVmXG4gICk7XG59O1xuXG5JbnNlcnRDb21tZW50ID0gKDAsIF9wdXJlMi5kZWZhdWx0KShJbnNlcnRDb21tZW50KTtcbkluc2VydENvbW1lbnQubXVpTmFtZSA9ICdTdmdJY29uJztcblxuZXhwb3J0cy5kZWZhdWx0ID0gSW5zZXJ0Q29tbWVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMyAzMSAzNCAzOCA0NCA0NSA0NyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX3B1cmUgPSByZXF1aXJlKCdyZWNvbXBvc2UvcHVyZScpO1xuXG52YXIgX3B1cmUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcHVyZSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJ21hdGVyaWFsLXVpL1N2Z0ljb24nKTtcblxudmFyIF9TdmdJY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX1N2Z0ljb24pO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgX3JlZiA9IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KCdwYXRoJywgeyBkOiAnTTExLjk5IDJDNi40NyAyIDIgNi40OCAyIDEyczQuNDcgMTAgOS45OSAxMEMxNy41MiAyMiAyMiAxNy41MiAyMiAxMlMxNy41MiAyIDExLjk5IDJ6TTEyIDIwYy00LjQyIDAtOC0zLjU4LTgtOHMzLjU4LTggOC04IDggMy41OCA4IDgtMy41OCA4LTggOHptMy41LTljLjgzIDAgMS41LS42NyAxLjUtMS41UzE2LjMzIDggMTUuNSA4IDE0IDguNjcgMTQgOS41cy42NyAxLjUgMS41IDEuNXptLTcgMGMuODMgMCAxLjUtLjY3IDEuNS0xLjVTOS4zMyA4IDguNSA4IDcgOC42NyA3IDkuNSA3LjY3IDExIDguNSAxMXptMy41IDYuNWMyLjMzIDAgNC4zMS0xLjQ2IDUuMTEtMy41SDYuODljLjggMi4wNCAyLjc4IDMuNSA1LjExIDMuNXonIH0pO1xuXG52YXIgSW5zZXJ0RW1vdGljb24gPSBmdW5jdGlvbiBJbnNlcnRFbW90aWNvbihwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0RW1vdGljb24gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydEVtb3RpY29uKTtcbkluc2VydEVtb3RpY29uLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydEVtb3RpY29uO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRFbW90aWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMzEgMzQgMzggNDQgNDUgNDciLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ00zLjkgMTJjMC0xLjcxIDEuMzktMy4xIDMuMS0zLjFoNFY3SDdjLTIuNzYgMC01IDIuMjQtNSA1czIuMjQgNSA1IDVoNHYtMS45SDdjLTEuNzEgMC0zLjEtMS4zOS0zLjEtMy4xek04IDEzaDh2LTJIOHYyem05LTZoLTR2MS45aDRjMS43MSAwIDMuMSAxLjM5IDMuMSAzLjFzLTEuMzkgMy4xLTMuMSAzLjFoLTRWMTdoNGMyLjc2IDAgNS0yLjI0IDUtNXMtMi4yNC01LTUtNXonIH0pO1xuXG52YXIgSW5zZXJ0TGluayA9IGZ1bmN0aW9uIEluc2VydExpbmsocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cbkluc2VydExpbmsgPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydExpbmspO1xuSW5zZXJ0TGluay5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBJbnNlcnRMaW5rO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfcHVyZSA9IHJlcXVpcmUoJ3JlY29tcG9zZS9wdXJlJyk7XG5cbnZhciBfcHVyZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wdXJlKTtcblxudmFyIF9TdmdJY29uID0gcmVxdWlyZSgnbWF0ZXJpYWwtdWkvU3ZnSWNvbicpO1xuXG52YXIgX1N2Z0ljb24yID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfU3ZnSWNvbik7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBfcmVmID0gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoJ3BhdGgnLCB7IGQ6ICdNMjEgMTlWNWMwLTEuMS0uOS0yLTItMkg1Yy0xLjEgMC0yIC45LTIgMnYxNGMwIDEuMS45IDIgMiAyaDE0YzEuMSAwIDItLjkgMi0yek04LjUgMTMuNWwyLjUgMy4wMUwxNC41IDEybDQuNSA2SDVsMy41LTQuNXonIH0pO1xuXG52YXIgSW5zZXJ0UGhvdG8gPSBmdW5jdGlvbiBJbnNlcnRQaG90byhwcm9wcykge1xuICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgX1N2Z0ljb24yLmRlZmF1bHQsXG4gICAgcHJvcHMsXG4gICAgX3JlZlxuICApO1xufTtcblxuSW5zZXJ0UGhvdG8gPSAoMCwgX3B1cmUyLmRlZmF1bHQpKEluc2VydFBob3RvKTtcbkluc2VydFBob3RvLm11aU5hbWUgPSAnU3ZnSWNvbic7XG5cbmV4cG9ydHMuZGVmYXVsdCA9IEluc2VydFBob3RvO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpLWljb25zL0luc2VydFBob3RvLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90by5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDMgMjQgMzEgMzQgMzggMzkgNDQgNDUgNDkiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9wdXJlID0gcmVxdWlyZSgncmVjb21wb3NlL3B1cmUnKTtcblxudmFyIF9wdXJlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3B1cmUpO1xuXG52YXIgX1N2Z0ljb24gPSByZXF1aXJlKCdtYXRlcmlhbC11aS9TdmdJY29uJyk7XG5cbnZhciBfU3ZnSWNvbjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIF9yZWYgPSBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudCgncGF0aCcsIHsgZDogJ001IDR2M2g1LjV2MTJoM1Y3SDE5VjR6JyB9KTtcblxudmFyIFRpdGxlID0gZnVuY3Rpb24gVGl0bGUocHJvcHMpIHtcbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgIF9TdmdJY29uMi5kZWZhdWx0LFxuICAgIHByb3BzLFxuICAgIF9yZWZcbiAgKTtcbn07XG5cblRpdGxlID0gKDAsIF9wdXJlMi5kZWZhdWx0KShUaXRsZSk7XG5UaXRsZS5tdWlOYW1lID0gJ1N2Z0ljb24nO1xuXG5leHBvcnRzLmRlZmF1bHQgPSBUaXRsZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS1pY29ucy9UaXRsZS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAzIDMxIDM0IDM4IDQ0IDQ1IDQ3IiwiJ3VzZSBzdHJpY3QnO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgXCJfX2VzTW9kdWxlXCIsIHtcbiAgdmFsdWU6IHRydWVcbn0pO1xuZXhwb3J0cy5zdHlsZXMgPSB1bmRlZmluZWQ7XG5cbnZhciBfZXh0ZW5kczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZXh0ZW5kcycpO1xuXG52YXIgX2V4dGVuZHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZXh0ZW5kczIpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9kZWZpbmVQcm9wZXJ0eScpO1xuXG52YXIgX2RlZmluZVByb3BlcnR5MyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2RlZmluZVByb3BlcnR5Mik7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzJyk7XG5cbnZhciBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMyKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvY29yZS1qcy9vYmplY3QvZ2V0LXByb3RvdHlwZS1vZicpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2dldFByb3RvdHlwZU9mKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY2xhc3NDYWxsQ2hlY2snKTtcblxudmFyIF9jbGFzc0NhbGxDaGVjazMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc0NhbGxDaGVjazIpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jcmVhdGVDbGFzcycpO1xuXG52YXIgX2NyZWF0ZUNsYXNzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NyZWF0ZUNsYXNzMik7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybicpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4zID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjIpO1xuXG52YXIgX2luaGVyaXRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9pbmhlcml0cycpO1xuXG52YXIgX2luaGVyaXRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2luaGVyaXRzMik7XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxudmFyIF9jbGFzc25hbWVzID0gcmVxdWlyZSgnY2xhc3NuYW1lcycpO1xuXG52YXIgX2NsYXNzbmFtZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NuYW1lcyk7XG5cbnZhciBfd2l0aFN0eWxlcyA9IHJlcXVpcmUoJy4uL3N0eWxlcy93aXRoU3R5bGVzJyk7XG5cbnZhciBfd2l0aFN0eWxlczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93aXRoU3R5bGVzKTtcblxudmFyIF9oZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvaGVscGVycycpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9IHJlcXVpcmUoJ3JlYWN0JykuYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSB8fCByZXF1aXJlKCdwcm9wLXR5cGVzJykuYW55O1xuXG52YXIgc3R5bGVzID0gZXhwb3J0cy5zdHlsZXMgPSBmdW5jdGlvbiBzdHlsZXModGhlbWUpIHtcbiAgcmV0dXJuIHtcbiAgICByb290OiB7XG4gICAgICB1c2VyU2VsZWN0OiAnbm9uZSdcbiAgICB9LFxuICAgIGNvbG9yQWNjZW50OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkuQTIwMFxuICAgIH0sXG4gICAgY29sb3JBY3Rpb246IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5hY3RpdmVcbiAgICB9LFxuICAgIGNvbG9yQ29udHJhc3Q6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmdldENvbnRyYXN0VGV4dCh0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXSlcbiAgICB9LFxuICAgIGNvbG9yRGlzYWJsZWQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmFjdGlvbi5kaXNhYmxlZFxuICAgIH0sXG4gICAgY29sb3JFcnJvcjoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuZXJyb3JbNTAwXVxuICAgIH0sXG4gICAgY29sb3JQcmltYXJ5OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF1cbiAgICB9XG4gIH07XG59O1xuXG52YXIgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfQ29sb3IgPSByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydpbmhlcml0JywgJ2FjY2VudCcsICdhY3Rpb24nLCAnY29udHJhc3QnLCAnZGlzYWJsZWQnLCAnZXJyb3InLCAncHJpbWFyeSddKTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX1Byb3BzID0ge1xuICAvKipcbiAgICogVGhlIG5hbWUgb2YgdGhlIGljb24gZm9udCBsaWdhdHVyZS5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSksXG5cbiAgLyoqXG4gICAqIEBpZ25vcmVcbiAgICovXG4gIGNsYXNzTmFtZTogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZyxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogVGhlIGNvbG9yIG9mIHRoZSBjb21wb25lbnQuIEl0J3MgdXNpbmcgdGhlIHRoZW1lIHBhbGV0dGUgd2hlbiB0aGF0IG1ha2VzIHNlbnNlLlxuICAgKi9cbiAgY29sb3I6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5vbmVPZihbJ2luaGVyaXQnLCAnYWNjZW50JywgJ2FjdGlvbicsICdjb250cmFzdCcsICdkaXNhYmxlZCcsICdlcnJvcicsICdwcmltYXJ5J10pLmlzUmVxdWlyZWRcbn07XG5cbnZhciBJY29uID0gZnVuY3Rpb24gKF9SZWFjdCRDb21wb25lbnQpIHtcbiAgKDAsIF9pbmhlcml0czMuZGVmYXVsdCkoSWNvbiwgX1JlYWN0JENvbXBvbmVudCk7XG5cbiAgZnVuY3Rpb24gSWNvbigpIHtcbiAgICAoMCwgX2NsYXNzQ2FsbENoZWNrMy5kZWZhdWx0KSh0aGlzLCBJY29uKTtcbiAgICByZXR1cm4gKDAsIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMy5kZWZhdWx0KSh0aGlzLCAoSWNvbi5fX3Byb3RvX18gfHwgKDAsIF9nZXRQcm90b3R5cGVPZjIuZGVmYXVsdCkoSWNvbikpLmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICB9XG5cbiAgKDAsIF9jcmVhdGVDbGFzczMuZGVmYXVsdCkoSWNvbiwgW3tcbiAgICBrZXk6ICdyZW5kZXInLFxuICAgIHZhbHVlOiBmdW5jdGlvbiByZW5kZXIoKSB7XG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBjaGlsZHJlbiA9IF9wcm9wcy5jaGlsZHJlbixcbiAgICAgICAgICBjbGFzc2VzID0gX3Byb3BzLmNsYXNzZXMsXG4gICAgICAgICAgY2xhc3NOYW1lUHJvcCA9IF9wcm9wcy5jbGFzc05hbWUsXG4gICAgICAgICAgY29sb3IgPSBfcHJvcHMuY29sb3IsXG4gICAgICAgICAgb3RoZXIgPSAoMCwgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMy5kZWZhdWx0KShfcHJvcHMsIFsnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InXSk7XG5cblxuICAgICAgdmFyIGNsYXNzTmFtZSA9ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoJ21hdGVyaWFsLWljb25zJywgY2xhc3Nlcy5yb290LCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KSh7fSwgY2xhc3Nlc1snY29sb3InICsgKDAsIF9oZWxwZXJzLmNhcGl0YWxpemVGaXJzdExldHRlcikoY29sb3IpXSwgY29sb3IgIT09ICdpbmhlcml0JyksIGNsYXNzTmFtZVByb3ApO1xuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICdzcGFuJyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7IGNsYXNzTmFtZTogY2xhc3NOYW1lLCAnYXJpYS1oaWRkZW4nOiAndHJ1ZScgfSwgb3RoZXIpLFxuICAgICAgICBjaGlsZHJlblxuICAgICAgKTtcbiAgICB9XG4gIH1dKTtcbiAgcmV0dXJuIEljb247XG59KF9yZWFjdDIuZGVmYXVsdC5Db21wb25lbnQpO1xuXG5JY29uLmRlZmF1bHRQcm9wcyA9IHtcbiAgY29sb3I6ICdpbmhlcml0J1xufTtcbkljb24ubXVpTmFtZSA9ICdJY29uJztcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlJY29uJyB9KShJY29uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL0ljb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb24vSWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDYgNyA4IDkgMzQgMzkgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4vSWNvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNiA3IDggOSAzNCAzOSA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuT2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFwiX19lc01vZHVsZVwiLCB7XG4gIHZhbHVlOiB0cnVlXG59KTtcbmV4cG9ydHMuc3R5bGVzID0gdW5kZWZpbmVkO1xuXG52YXIgX2V4dGVuZHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2V4dGVuZHMnKTtcblxudmFyIF9leHRlbmRzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2V4dGVuZHMyKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvZGVmaW5lUHJvcGVydHknKTtcblxudmFyIF9kZWZpbmVQcm9wZXJ0eTMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9kZWZpbmVQcm9wZXJ0eTIpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9vYmplY3RXaXRob3V0UHJvcGVydGllcycpO1xuXG52YXIgX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzMik7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2NvcmUtanMvb2JqZWN0L2dldC1wcm90b3R5cGUtb2YnKTtcblxudmFyIF9nZXRQcm90b3R5cGVPZjIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXRQcm90b3R5cGVPZik7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2syID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrJyk7XG5cbnZhciBfY2xhc3NDYWxsQ2hlY2szID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY2xhc3NDYWxsQ2hlY2syKTtcblxudmFyIF9jcmVhdGVDbGFzczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvY3JlYXRlQ2xhc3MnKTtcblxudmFyIF9jcmVhdGVDbGFzczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVDbGFzczIpO1xuXG52YXIgX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4nKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4yKTtcblxudmFyIF9pbmhlcml0czIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvaW5oZXJpdHMnKTtcblxudmFyIF9pbmhlcml0czMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pbmhlcml0czIpO1xuXG52YXIgX3JlYWN0ID0gcmVxdWlyZSgncmVhY3QnKTtcblxudmFyIF9yZWFjdDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9yZWFjdCk7XG5cbnZhciBfY2xhc3NuYW1lcyA9IHJlcXVpcmUoJ2NsYXNzbmFtZXMnKTtcblxudmFyIF9jbGFzc25hbWVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzbmFtZXMpO1xuXG52YXIgX3dpdGhTdHlsZXMgPSByZXF1aXJlKCcuLi9zdHlsZXMvd2l0aFN0eWxlcycpO1xuXG52YXIgX3dpdGhTdHlsZXMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfd2l0aFN0eWxlcyk7XG5cbnZhciBfQnV0dG9uQmFzZSA9IHJlcXVpcmUoJy4uL0J1dHRvbkJhc2UnKTtcblxudmFyIF9CdXR0b25CYXNlMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0J1dHRvbkJhc2UpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbnZhciBfSWNvbiA9IHJlcXVpcmUoJy4uL0ljb24nKTtcblxudmFyIF9JY29uMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX0ljb24pO1xuXG5yZXF1aXJlKCcuLi9TdmdJY29uJyk7XG5cbnZhciBfcmVhY3RIZWxwZXJzID0gcmVxdWlyZSgnLi4vdXRpbHMvcmVhY3RIZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7IC8vICB3ZWFrXG4vLyBAaW5oZXJpdGVkQ29tcG9uZW50IEJ1dHRvbkJhc2VcblxuLy8gRW5zdXJlIENTUyBzcGVjaWZpY2l0eVxuXG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gICAgICBmbGV4OiAnMCAwIGF1dG8nLFxuICAgICAgZm9udFNpemU6IHRoZW1lLnR5cG9ncmFwaHkucHhUb1JlbSgyNCksXG4gICAgICB3aWR0aDogdGhlbWUuc3BhY2luZy51bml0ICogNixcbiAgICAgIGhlaWdodDogdGhlbWUuc3BhY2luZy51bml0ICogNixcbiAgICAgIHBhZGRpbmc6IDAsXG4gICAgICBib3JkZXJSYWRpdXM6ICc1MCUnLFxuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmFjdGl2ZSxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnYmFja2dyb3VuZC1jb2xvcicsIHtcbiAgICAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXN0XG4gICAgICB9KVxuICAgIH0sXG4gICAgY29sb3JBY2NlbnQ6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnNlY29uZGFyeS5BMjAwXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvclByaW1hcnk6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLnByaW1hcnlbNTAwXVxuICAgIH0sXG4gICAgY29sb3JJbmhlcml0OiB7XG4gICAgICBjb2xvcjogJ2luaGVyaXQnXG4gICAgfSxcbiAgICBkaXNhYmxlZDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuYWN0aW9uLmRpc2FibGVkXG4gICAgfSxcbiAgICBsYWJlbDoge1xuICAgICAgd2lkdGg6ICcxMDAlJyxcbiAgICAgIGRpc3BsYXk6ICdmbGV4JyxcbiAgICAgIGFsaWduSXRlbXM6ICdpbmhlcml0JyxcbiAgICAgIGp1c3RpZnlDb250ZW50OiAnaW5oZXJpdCdcbiAgICB9LFxuICAgIGljb246IHtcbiAgICAgIHdpZHRoOiAnMWVtJyxcbiAgICAgIGhlaWdodDogJzFlbSdcbiAgICB9LFxuICAgIGtleWJvYXJkRm9jdXNlZDoge1xuICAgICAgYmFja2dyb3VuZENvbG9yOiB0aGVtZS5wYWxldHRlLnRleHQuZGl2aWRlclxuICAgIH1cbiAgfTtcbn07XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIFVzZSB0aGF0IHByb3BlcnR5IHRvIHBhc3MgYSByZWYgY2FsbGJhY2sgdG8gdGhlIG5hdGl2ZSBidXR0b24gY29tcG9uZW50LlxuICAgKi9cbiAgYnV0dG9uUmVmOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuZnVuYyxcblxuICAvKipcbiAgICogVGhlIGljb24gZWxlbWVudC5cbiAgICogSWYgYSBzdHJpbmcgaXMgcHJvdmlkZWQsIGl0IHdpbGwgYmUgdXNlZCBhcyBhbiBpY29uIGZvbnQgbGlnYXR1cmUuXG4gICAqL1xuICBjaGlsZHJlbjogdHlwZW9mIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgPT09ICdmdW5jdGlvbicgPyBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIDogcmVxdWlyZSgncHJvcC10eXBlcycpLnNoYXBlKGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUpLFxuXG4gIC8qKlxuICAgKiBVc2VmdWwgdG8gZXh0ZW5kIHRoZSBzdHlsZSBhcHBsaWVkIHRvIGNvbXBvbmVudHMuXG4gICAqL1xuICBjbGFzc2VzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub2JqZWN0LFxuXG4gIC8qKlxuICAgKiBAaWdub3JlXG4gICAqL1xuICBjbGFzc05hbWU6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5zdHJpbmcsXG5cbiAgLyoqXG4gICAqIFRoZSBjb2xvciBvZiB0aGUgY29tcG9uZW50LiBJdCdzIHVzaW5nIHRoZSB0aGVtZSBwYWxldHRlIHdoZW4gdGhhdCBtYWtlcyBzZW5zZS5cbiAgICovXG4gIGNvbG9yOiByZXF1aXJlKCdwcm9wLXR5cGVzJykub25lT2YoWydkZWZhdWx0JywgJ2luaGVyaXQnLCAncHJpbWFyeScsICdjb250cmFzdCcsICdhY2NlbnQnXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgYnV0dG9uIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlZDogcmVxdWlyZSgncHJvcC10eXBlcycpLmJvb2wuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogSWYgYHRydWVgLCB0aGUgcmlwcGxlIHdpbGwgYmUgZGlzYWJsZWQuXG4gICAqL1xuICBkaXNhYmxlUmlwcGxlOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuYm9vbC5pc1JlcXVpcmVkLFxuXG4gIC8qKlxuICAgKiBVc2UgdGhhdCBwcm9wZXJ0eSB0byBwYXNzIGEgcmVmIGNhbGxiYWNrIHRvIHRoZSByb290IGNvbXBvbmVudC5cbiAgICovXG4gIHJvb3RSZWY6IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5mdW5jXG59O1xuXG4vKipcbiAqIFJlZmVyIHRvIHRoZSBbSWNvbnNdKC9zdHlsZS9pY29ucykgc2VjdGlvbiBvZiB0aGUgZG9jdW1lbnRhdGlvblxuICogcmVnYXJkaW5nIHRoZSBhdmFpbGFibGUgaWNvbiBvcHRpb25zLlxuICovXG52YXIgSWNvbkJ1dHRvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKEljb25CdXR0b24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIEljb25CdXR0b24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgSWNvbkJ1dHRvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKEljb25CdXR0b24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKEljb25CdXR0b24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKEljb25CdXR0b24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9jbGFzc05hbWVzO1xuXG4gICAgICB2YXIgX3Byb3BzID0gdGhpcy5wcm9wcyxcbiAgICAgICAgICBidXR0b25SZWYgPSBfcHJvcHMuYnV0dG9uUmVmLFxuICAgICAgICAgIGNoaWxkcmVuID0gX3Byb3BzLmNoaWxkcmVuLFxuICAgICAgICAgIGNsYXNzZXMgPSBfcHJvcHMuY2xhc3NlcyxcbiAgICAgICAgICBjbGFzc05hbWUgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIGRpc2FibGVkID0gX3Byb3BzLmRpc2FibGVkLFxuICAgICAgICAgIHJvb3RSZWYgPSBfcHJvcHMucm9vdFJlZixcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydidXR0b25SZWYnLCAnY2hpbGRyZW4nLCAnY2xhc3NlcycsICdjbGFzc05hbWUnLCAnY29sb3InLCAnZGlzYWJsZWQnLCAncm9vdFJlZiddKTtcblxuXG4gICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgIF9CdXR0b25CYXNlMi5kZWZhdWx0LFxuICAgICAgICAoMCwgX2V4dGVuZHMzLmRlZmF1bHQpKHtcbiAgICAgICAgICBjbGFzc05hbWU6ICgwLCBfY2xhc3NuYW1lczIuZGVmYXVsdCkoY2xhc3Nlcy5yb290LCAoX2NsYXNzTmFtZXMgPSB7fSwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoX2NsYXNzTmFtZXMsIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnZGVmYXVsdCcpLCAoMCwgX2RlZmluZVByb3BlcnR5My5kZWZhdWx0KShfY2xhc3NOYW1lcywgY2xhc3Nlcy5kaXNhYmxlZCwgZGlzYWJsZWQpLCBfY2xhc3NOYW1lcyksIGNsYXNzTmFtZSksXG4gICAgICAgICAgY2VudGVyUmlwcGxlOiB0cnVlLFxuICAgICAgICAgIGtleWJvYXJkRm9jdXNlZENsYXNzTmFtZTogY2xhc3Nlcy5rZXlib2FyZEZvY3VzZWQsXG4gICAgICAgICAgZGlzYWJsZWQ6IGRpc2FibGVkXG4gICAgICAgIH0sIG90aGVyLCB7XG4gICAgICAgICAgcm9vdFJlZjogYnV0dG9uUmVmLFxuICAgICAgICAgIHJlZjogcm9vdFJlZlxuICAgICAgICB9KSxcbiAgICAgICAgX3JlYWN0Mi5kZWZhdWx0LmNyZWF0ZUVsZW1lbnQoXG4gICAgICAgICAgJ3NwYW4nLFxuICAgICAgICAgIHsgY2xhc3NOYW1lOiBjbGFzc2VzLmxhYmVsIH0sXG4gICAgICAgICAgdHlwZW9mIGNoaWxkcmVuID09PSAnc3RyaW5nJyA/IF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAgICAgX0ljb24yLmRlZmF1bHQsXG4gICAgICAgICAgICB7IGNsYXNzTmFtZTogY2xhc3Nlcy5pY29uIH0sXG4gICAgICAgICAgICBjaGlsZHJlblxuICAgICAgICAgICkgOiBfcmVhY3QyLmRlZmF1bHQuQ2hpbGRyZW4ubWFwKGNoaWxkcmVuLCBmdW5jdGlvbiAoY2hpbGQpIHtcbiAgICAgICAgICAgIGlmICgoMCwgX3JlYWN0SGVscGVycy5pc011aUVsZW1lbnQpKGNoaWxkLCBbJ0ljb24nLCAnU3ZnSWNvbiddKSkge1xuICAgICAgICAgICAgICByZXR1cm4gX3JlYWN0Mi5kZWZhdWx0LmNsb25lRWxlbWVudChjaGlsZCwge1xuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogKDAsIF9jbGFzc25hbWVzMi5kZWZhdWx0KShjbGFzc2VzLmljb24sIGNoaWxkLnByb3BzLmNsYXNzTmFtZSlcbiAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHJldHVybiBjaGlsZDtcbiAgICAgICAgICB9KVxuICAgICAgICApXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gSWNvbkJ1dHRvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cbkljb25CdXR0b24uZGVmYXVsdFByb3BzID0ge1xuICBjb2xvcjogJ2RlZmF1bHQnLFxuICBkaXNhYmxlZDogZmFsc2UsXG4gIGRpc2FibGVSaXBwbGU6IGZhbHNlXG59O1xuZXhwb3J0cy5kZWZhdWx0ID0gKDAsIF93aXRoU3R5bGVzMi5kZWZhdWx0KShzdHlsZXMsIHsgbmFtZTogJ011aUljb25CdXR0b24nIH0pKEljb25CdXR0b24pO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL0ljb25CdXR0b24vSWNvbkJ1dHRvbi5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvbWF0ZXJpYWwtdWkvSWNvbkJ1dHRvbi9JY29uQnV0dG9uLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAzIDYgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzcgMzggMzkgNDAgNDQgNDUgNDYgNDkgNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfSWNvbkJ1dHRvbiA9IHJlcXVpcmUoJy4vSWNvbkJ1dHRvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9JY29uQnV0dG9uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9JY29uQnV0dG9uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMSAzIDYgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzcgMzggMzkgNDAgNDQgNDUgNDYgNDkgNTUiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5leHBvcnRzLnN0eWxlcyA9IHVuZGVmaW5lZDtcblxudmFyIF9leHRlbmRzMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9leHRlbmRzJyk7XG5cbnZhciBfZXh0ZW5kczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9leHRlbmRzMik7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5Jyk7XG5cbnZhciBfZGVmaW5lUHJvcGVydHkzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZGVmaW5lUHJvcGVydHkyKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczIgPSByZXF1aXJlKCdiYWJlbC1ydW50aW1lL2hlbHBlcnMvb2JqZWN0V2l0aG91dFByb3BlcnRpZXMnKTtcblxudmFyIF9vYmplY3RXaXRob3V0UHJvcGVydGllczMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9vYmplY3RXaXRob3V0UHJvcGVydGllczIpO1xuXG52YXIgX2dldFByb3RvdHlwZU9mID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9jb3JlLWpzL29iamVjdC9nZXQtcHJvdG90eXBlLW9mJyk7XG5cbnZhciBfZ2V0UHJvdG90eXBlT2YyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfZ2V0UHJvdG90eXBlT2YpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9jbGFzc0NhbGxDaGVjaycpO1xuXG52YXIgX2NsYXNzQ2FsbENoZWNrMyA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX2NsYXNzQ2FsbENoZWNrMik7XG5cbnZhciBfY3JlYXRlQ2xhc3MyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzJyk7XG5cbnZhciBfY3JlYXRlQ2xhc3MzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlQ2xhc3MyKTtcblxudmFyIF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMiA9IHJlcXVpcmUoJ2JhYmVsLXJ1bnRpbWUvaGVscGVycy9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuJyk7XG5cbnZhciBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuMik7XG5cbnZhciBfaW5oZXJpdHMyID0gcmVxdWlyZSgnYmFiZWwtcnVudGltZS9oZWxwZXJzL2luaGVyaXRzJyk7XG5cbnZhciBfaW5oZXJpdHMzID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfaW5oZXJpdHMyKTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfcmVhY3QyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfcmVhY3QpO1xuXG52YXIgX2NsYXNzbmFtZXMgPSByZXF1aXJlKCdjbGFzc25hbWVzJyk7XG5cbnZhciBfY2xhc3NuYW1lczIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jbGFzc25hbWVzKTtcblxudmFyIF93aXRoU3R5bGVzID0gcmVxdWlyZSgnLi4vc3R5bGVzL3dpdGhTdHlsZXMnKTtcblxudmFyIF93aXRoU3R5bGVzMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dpdGhTdHlsZXMpO1xuXG52YXIgX2hlbHBlcnMgPSByZXF1aXJlKCcuLi91dGlscy9oZWxwZXJzJyk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlID0gcmVxdWlyZSgncmVhY3QnKS5iYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Ob2RlIHx8IHJlcXVpcmUoJ3Byb3AtdHlwZXMnKS5hbnk7XG5cbnZhciBzdHlsZXMgPSBleHBvcnRzLnN0eWxlcyA9IGZ1bmN0aW9uIHN0eWxlcyh0aGVtZSkge1xuICByZXR1cm4ge1xuICAgIHJvb3Q6IHtcbiAgICAgIGRpc3BsYXk6ICdpbmxpbmUtYmxvY2snLFxuICAgICAgZmlsbDogJ2N1cnJlbnRDb2xvcicsXG4gICAgICBoZWlnaHQ6IDI0LFxuICAgICAgd2lkdGg6IDI0LFxuICAgICAgdXNlclNlbGVjdDogJ25vbmUnLFxuICAgICAgZmxleFNocmluazogMCxcbiAgICAgIHRyYW5zaXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmNyZWF0ZSgnZmlsbCcsIHtcbiAgICAgICAgZHVyYXRpb246IHRoZW1lLnRyYW5zaXRpb25zLmR1cmF0aW9uLnNob3J0ZXJcbiAgICAgIH0pXG4gICAgfSxcbiAgICBjb2xvckFjY2VudDoge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUuc2Vjb25kYXJ5LkEyMDBcbiAgICB9LFxuICAgIGNvbG9yQWN0aW9uOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uYWN0aXZlXG4gICAgfSxcbiAgICBjb2xvckNvbnRyYXN0OiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5nZXRDb250cmFzdFRleHQodGhlbWUucGFsZXR0ZS5wcmltYXJ5WzUwMF0pXG4gICAgfSxcbiAgICBjb2xvckRpc2FibGVkOiB7XG4gICAgICBjb2xvcjogdGhlbWUucGFsZXR0ZS5hY3Rpb24uZGlzYWJsZWRcbiAgICB9LFxuICAgIGNvbG9yRXJyb3I6IHtcbiAgICAgIGNvbG9yOiB0aGVtZS5wYWxldHRlLmVycm9yWzUwMF1cbiAgICB9LFxuICAgIGNvbG9yUHJpbWFyeToge1xuICAgICAgY29sb3I6IHRoZW1lLnBhbGV0dGUucHJpbWFyeVs1MDBdXG4gICAgfVxuICB9O1xufTtcblxudmFyIGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX0NvbG9yID0gcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSk7XG5cbnZhciBiYWJlbFBsdWdpbkZsb3dSZWFjdFByb3BUeXBlc19wcm9wdHlwZV9Qcm9wcyA9IHtcbiAgLyoqXG4gICAqIEVsZW1lbnRzIHBhc3NlZCBpbnRvIHRoZSBTVkcgSWNvbi5cbiAgICovXG4gIGNoaWxkcmVuOiB0eXBlb2YgYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSA9PT0gJ2Z1bmN0aW9uJyA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA/IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUuaXNSZXF1aXJlZCA6IGJhYmVsUGx1Z2luRmxvd1JlYWN0UHJvcFR5cGVzX3Byb3B0eXBlX05vZGUgOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc2hhcGUoYmFiZWxQbHVnaW5GbG93UmVhY3RQcm9wVHlwZXNfcHJvcHR5cGVfTm9kZSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogVXNlZnVsIHRvIGV4dGVuZCB0aGUgc3R5bGUgYXBwbGllZCB0byBjb21wb25lbnRzLlxuICAgKi9cbiAgY2xhc3NlczogcmVxdWlyZSgncHJvcC10eXBlcycpLm9iamVjdCxcblxuICAvKipcbiAgICogQGlnbm9yZVxuICAgKi9cbiAgY2xhc3NOYW1lOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBUaGUgY29sb3Igb2YgdGhlIGNvbXBvbmVudC4gSXQncyB1c2luZyB0aGUgdGhlbWUgcGFsZXR0ZSB3aGVuIHRoYXQgbWFrZXMgc2Vuc2UuXG4gICAqL1xuICBjb2xvcjogcmVxdWlyZSgncHJvcC10eXBlcycpLm9uZU9mKFsnaW5oZXJpdCcsICdhY2NlbnQnLCAnYWN0aW9uJywgJ2NvbnRyYXN0JywgJ2Rpc2FibGVkJywgJ2Vycm9yJywgJ3ByaW1hcnknXSkuaXNSZXF1aXJlZCxcblxuICAvKipcbiAgICogUHJvdmlkZXMgYSBodW1hbi1yZWFkYWJsZSB0aXRsZSBmb3IgdGhlIGVsZW1lbnQgdGhhdCBjb250YWlucyBpdC5cbiAgICogaHR0cHM6Ly93d3cudzMub3JnL1RSL1NWRy1hY2Nlc3MvI0VxdWl2YWxlbnRcbiAgICovXG4gIHRpdGxlQWNjZXNzOiByZXF1aXJlKCdwcm9wLXR5cGVzJykuc3RyaW5nLFxuXG4gIC8qKlxuICAgKiBBbGxvd3MgeW91IHRvIHJlZGVmaW5lIHdoYXQgdGhlIGNvb3JkaW5hdGVzIHdpdGhvdXQgdW5pdHMgbWVhbiBpbnNpZGUgYW4gc3ZnIGVsZW1lbnQuXG4gICAqIEZvciBleGFtcGxlLCBpZiB0aGUgU1ZHIGVsZW1lbnQgaXMgNTAwICh3aWR0aCkgYnkgMjAwIChoZWlnaHQpLFxuICAgKiBhbmQgeW91IHBhc3Mgdmlld0JveD1cIjAgMCA1MCAyMFwiLFxuICAgKiB0aGlzIG1lYW5zIHRoYXQgdGhlIGNvb3JkaW5hdGVzIGluc2lkZSB0aGUgc3ZnIHdpbGwgZ28gZnJvbSB0aGUgdG9wIGxlZnQgY29ybmVyICgwLDApXG4gICAqIHRvIGJvdHRvbSByaWdodCAoNTAsMjApIGFuZCBlYWNoIHVuaXQgd2lsbCBiZSB3b3J0aCAxMHB4LlxuICAgKi9cbiAgdmlld0JveDogcmVxdWlyZSgncHJvcC10eXBlcycpLnN0cmluZy5pc1JlcXVpcmVkXG59O1xuXG52YXIgU3ZnSWNvbiA9IGZ1bmN0aW9uIChfUmVhY3QkQ29tcG9uZW50KSB7XG4gICgwLCBfaW5oZXJpdHMzLmRlZmF1bHQpKFN2Z0ljb24sIF9SZWFjdCRDb21wb25lbnQpO1xuXG4gIGZ1bmN0aW9uIFN2Z0ljb24oKSB7XG4gICAgKDAsIF9jbGFzc0NhbGxDaGVjazMuZGVmYXVsdCkodGhpcywgU3ZnSWNvbik7XG4gICAgcmV0dXJuICgwLCBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybjMuZGVmYXVsdCkodGhpcywgKFN2Z0ljb24uX19wcm90b19fIHx8ICgwLCBfZ2V0UHJvdG90eXBlT2YyLmRlZmF1bHQpKFN2Z0ljb24pKS5hcHBseSh0aGlzLCBhcmd1bWVudHMpKTtcbiAgfVxuXG4gICgwLCBfY3JlYXRlQ2xhc3MzLmRlZmF1bHQpKFN2Z0ljb24sIFt7XG4gICAga2V5OiAncmVuZGVyJyxcbiAgICB2YWx1ZTogZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgdmFyIF9wcm9wcyA9IHRoaXMucHJvcHMsXG4gICAgICAgICAgY2hpbGRyZW4gPSBfcHJvcHMuY2hpbGRyZW4sXG4gICAgICAgICAgY2xhc3NlcyA9IF9wcm9wcy5jbGFzc2VzLFxuICAgICAgICAgIGNsYXNzTmFtZVByb3AgPSBfcHJvcHMuY2xhc3NOYW1lLFxuICAgICAgICAgIGNvbG9yID0gX3Byb3BzLmNvbG9yLFxuICAgICAgICAgIHRpdGxlQWNjZXNzID0gX3Byb3BzLnRpdGxlQWNjZXNzLFxuICAgICAgICAgIHZpZXdCb3ggPSBfcHJvcHMudmlld0JveCxcbiAgICAgICAgICBvdGhlciA9ICgwLCBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXMzLmRlZmF1bHQpKF9wcm9wcywgWydjaGlsZHJlbicsICdjbGFzc2VzJywgJ2NsYXNzTmFtZScsICdjb2xvcicsICd0aXRsZUFjY2VzcycsICd2aWV3Qm94J10pO1xuXG5cbiAgICAgIHZhciBjbGFzc05hbWUgPSAoMCwgX2NsYXNzbmFtZXMyLmRlZmF1bHQpKGNsYXNzZXMucm9vdCwgKDAsIF9kZWZpbmVQcm9wZXJ0eTMuZGVmYXVsdCkoe30sIGNsYXNzZXNbJ2NvbG9yJyArICgwLCBfaGVscGVycy5jYXBpdGFsaXplRmlyc3RMZXR0ZXIpKGNvbG9yKV0sIGNvbG9yICE9PSAnaW5oZXJpdCcpLCBjbGFzc05hbWVQcm9wKTtcblxuICAgICAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KFxuICAgICAgICAnc3ZnJyxcbiAgICAgICAgKDAsIF9leHRlbmRzMy5kZWZhdWx0KSh7XG4gICAgICAgICAgY2xhc3NOYW1lOiBjbGFzc05hbWUsXG4gICAgICAgICAgZm9jdXNhYmxlOiAnZmFsc2UnLFxuICAgICAgICAgIHZpZXdCb3g6IHZpZXdCb3gsXG4gICAgICAgICAgJ2FyaWEtaGlkZGVuJzogdGl0bGVBY2Nlc3MgPyAnZmFsc2UnIDogJ3RydWUnXG4gICAgICAgIH0sIG90aGVyKSxcbiAgICAgICAgdGl0bGVBY2Nlc3MgPyBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgICAgICAndGl0bGUnLFxuICAgICAgICAgIG51bGwsXG4gICAgICAgICAgdGl0bGVBY2Nlc3NcbiAgICAgICAgKSA6IG51bGwsXG4gICAgICAgIGNoaWxkcmVuXG4gICAgICApO1xuICAgIH1cbiAgfV0pO1xuICByZXR1cm4gU3ZnSWNvbjtcbn0oX3JlYWN0Mi5kZWZhdWx0LkNvbXBvbmVudCk7XG5cblN2Z0ljb24uZGVmYXVsdFByb3BzID0ge1xuICB2aWV3Qm94OiAnMCAwIDI0IDI0JyxcbiAgY29sb3I6ICdpbmhlcml0J1xufTtcblN2Z0ljb24ubXVpTmFtZSA9ICdTdmdJY29uJztcbmV4cG9ydHMuZGVmYXVsdCA9ICgwLCBfd2l0aFN0eWxlczIuZGVmYXVsdCkoc3R5bGVzLCB7IG5hbWU6ICdNdWlTdmdJY29uJyB9KShTdmdJY29uKTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL1N2Z0ljb24uanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL21hdGVyaWFsLXVpL1N2Z0ljb24vU3ZnSWNvbi5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAyIDMgNCA1IDYgNyA4IDkgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMzQgMzYgMzkgNDMgNDggNTAgNTEgNTIgNTMiLCIndXNlIHN0cmljdCc7XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBcIl9fZXNNb2R1bGVcIiwge1xuICB2YWx1ZTogdHJ1ZVxufSk7XG5cbnZhciBfU3ZnSWNvbiA9IHJlcXVpcmUoJy4vU3ZnSWNvbicpO1xuXG5PYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ2RlZmF1bHQnLCB7XG4gIGVudW1lcmFibGU6IHRydWUsXG4gIGdldDogZnVuY3Rpb24gZ2V0KCkge1xuICAgIHJldHVybiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9TdmdJY29uKS5kZWZhdWx0O1xuICB9XG59KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9tYXRlcmlhbC11aS9TdmdJY29uL2luZGV4LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDIgMyA0IDUgNiA3IDggOSAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAzNCAzNiAzOSA0MyA0OCA1MCA1MSA1MiA1MyIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsID0gcmVxdWlyZSgnLi91dGlscy9jcmVhdGVFYWdlckVsZW1lbnRVdGlsJyk7XG5cbnZhciBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9jcmVhdGVFYWdlckVsZW1lbnRVdGlsKTtcblxudmFyIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnRGdW5jdGlvbkNvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUZhY3RvcnkgPSBmdW5jdGlvbiBjcmVhdGVGYWN0b3J5KHR5cGUpIHtcbiAgdmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50ID0gKDAsIF9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50Mi5kZWZhdWx0KSh0eXBlKTtcbiAgcmV0dXJuIGZ1bmN0aW9uIChwLCBjKSB7XG4gICAgcmV0dXJuICgwLCBfY3JlYXRlRWFnZXJFbGVtZW50VXRpbDIuZGVmYXVsdCkoZmFsc2UsIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50LCB0eXBlLCBwLCBjKTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGNyZWF0ZUZhY3Rvcnk7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2NyZWF0ZUVhZ2VyRmFjdG9yeS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGdldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gZ2V0RGlzcGxheU5hbWUoQ29tcG9uZW50KSB7XG4gIGlmICh0eXBlb2YgQ29tcG9uZW50ID09PSAnc3RyaW5nJykge1xuICAgIHJldHVybiBDb21wb25lbnQ7XG4gIH1cblxuICBpZiAoIUNvbXBvbmVudCkge1xuICAgIHJldHVybiB1bmRlZmluZWQ7XG4gIH1cblxuICByZXR1cm4gQ29tcG9uZW50LmRpc3BsYXlOYW1lIHx8IENvbXBvbmVudC5uYW1lIHx8ICdDb21wb25lbnQnO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gZ2V0RGlzcGxheU5hbWU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2dldERpc3BsYXlOYW1lLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvZ2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF90eXBlb2YgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgdHlwZW9mIFN5bWJvbC5pdGVyYXRvciA9PT0gXCJzeW1ib2xcIiA/IGZ1bmN0aW9uIChvYmopIHsgcmV0dXJuIHR5cGVvZiBvYmo7IH0gOiBmdW5jdGlvbiAob2JqKSB7IHJldHVybiBvYmogJiYgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIG9iai5jb25zdHJ1Y3RvciA9PT0gU3ltYm9sICYmIG9iaiAhPT0gU3ltYm9sLnByb3RvdHlwZSA/IFwic3ltYm9sXCIgOiB0eXBlb2Ygb2JqOyB9O1xuXG52YXIgaXNDbGFzc0NvbXBvbmVudCA9IGZ1bmN0aW9uIGlzQ2xhc3NDb21wb25lbnQoQ29tcG9uZW50KSB7XG4gIHJldHVybiBCb29sZWFuKENvbXBvbmVudCAmJiBDb21wb25lbnQucHJvdG90eXBlICYmIF90eXBlb2YoQ29tcG9uZW50LnByb3RvdHlwZS5pc1JlYWN0Q29tcG9uZW50KSA9PT0gJ29iamVjdCcpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gaXNDbGFzc0NvbXBvbmVudDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvaXNDbGFzc0NvbXBvbmVudC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzQ2xhc3NDb21wb25lbnQuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9pc0NsYXNzQ29tcG9uZW50ID0gcmVxdWlyZSgnLi9pc0NsYXNzQ29tcG9uZW50Jyk7XG5cbnZhciBfaXNDbGFzc0NvbXBvbmVudDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9pc0NsYXNzQ29tcG9uZW50KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQgPSBmdW5jdGlvbiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50KENvbXBvbmVudCkge1xuICByZXR1cm4gQm9vbGVhbih0eXBlb2YgQ29tcG9uZW50ID09PSAnZnVuY3Rpb24nICYmICEoMCwgX2lzQ2xhc3NDb21wb25lbnQyLmRlZmF1bHQpKENvbXBvbmVudCkgJiYgIUNvbXBvbmVudC5kZWZhdWx0UHJvcHMgJiYgIUNvbXBvbmVudC5jb250ZXh0VHlwZXMgJiYgKHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbicgfHwgIUNvbXBvbmVudC5wcm9wVHlwZXMpKTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IGlzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQ7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL2lzUmVmZXJlbnRpYWxseVRyYW5zcGFyZW50RnVuY3Rpb25Db21wb25lbnQuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9pc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudEZ1bmN0aW9uQ29tcG9uZW50LmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2hvdWxkVXBkYXRlID0gcmVxdWlyZSgnLi9zaG91bGRVcGRhdGUnKTtcblxudmFyIF9zaG91bGRVcGRhdGUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2hvdWxkVXBkYXRlKTtcblxudmFyIF9zaGFsbG93RXF1YWwgPSByZXF1aXJlKCcuL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lID0gcmVxdWlyZSgnLi9zZXREaXNwbGF5TmFtZScpO1xuXG52YXIgX3NldERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3NldERpc3BsYXlOYW1lKTtcblxudmFyIF93cmFwRGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3dyYXBEaXNwbGF5TmFtZScpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF93cmFwRGlzcGxheU5hbWUpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG52YXIgcHVyZSA9IGZ1bmN0aW9uIHB1cmUoQmFzZUNvbXBvbmVudCkge1xuICB2YXIgaG9jID0gKDAsIF9zaG91bGRVcGRhdGUyLmRlZmF1bHQpKGZ1bmN0aW9uIChwcm9wcywgbmV4dFByb3BzKSB7XG4gICAgcmV0dXJuICEoMCwgX3NoYWxsb3dFcXVhbDIuZGVmYXVsdCkocHJvcHMsIG5leHRQcm9wcyk7XG4gIH0pO1xuXG4gIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgcmV0dXJuICgwLCBfc2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKCgwLCBfd3JhcERpc3BsYXlOYW1lMi5kZWZhdWx0KShCYXNlQ29tcG9uZW50LCAncHVyZScpKShob2MoQmFzZUNvbXBvbmVudCkpO1xuICB9XG5cbiAgcmV0dXJuIGhvYyhCYXNlQ29tcG9uZW50KTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHB1cmU7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3B1cmUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9wdXJlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfc2V0U3RhdGljID0gcmVxdWlyZSgnLi9zZXRTdGF0aWMnKTtcblxudmFyIF9zZXRTdGF0aWMyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0U3RhdGljKTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIHNldERpc3BsYXlOYW1lID0gZnVuY3Rpb24gc2V0RGlzcGxheU5hbWUoZGlzcGxheU5hbWUpIHtcbiAgcmV0dXJuICgwLCBfc2V0U3RhdGljMi5kZWZhdWx0KSgnZGlzcGxheU5hbWUnLCBkaXNwbGF5TmFtZSk7XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXREaXNwbGF5TmFtZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2V0RGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zZXREaXNwbGF5TmFtZS5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiXCJ1c2Ugc3RyaWN0XCI7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIgc2V0U3RhdGljID0gZnVuY3Rpb24gc2V0U3RhdGljKGtleSwgdmFsdWUpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgLyogZXNsaW50LWRpc2FibGUgbm8tcGFyYW0tcmVhc3NpZ24gKi9cbiAgICBCYXNlQ29tcG9uZW50W2tleV0gPSB2YWx1ZTtcbiAgICAvKiBlc2xpbnQtZW5hYmxlIG5vLXBhcmFtLXJlYXNzaWduICovXG4gICAgcmV0dXJuIEJhc2VDb21wb25lbnQ7XG4gIH07XG59O1xuXG5leHBvcnRzLmRlZmF1bHQgPSBzZXRTdGF0aWM7XG5cblxuLy8vLy8vLy8vLy8vLy8vLy8vXG4vLyBXRUJQQUNLIEZPT1RFUlxuLy8gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3NldFN0YXRpYy5qc1xuLy8gbW9kdWxlIGNodW5rcyA9IDAgMSAzIDQgNSA2IDEwIDExIDEyIDEzIDE0IDE1IDE2IDE3IDI0IDI2IDI3IDI4IDMxIDM0IDM1IDM2IDM3IDM4IDM5IDQwIDQzIDQ0IDQ1IDQ2IDQ5IiwiJ3VzZSBzdHJpY3QnO1xuXG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuXG52YXIgX3NoYWxsb3dFcXVhbCA9IHJlcXVpcmUoJ2ZianMvbGliL3NoYWxsb3dFcXVhbCcpO1xuXG52YXIgX3NoYWxsb3dFcXVhbDIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9zaGFsbG93RXF1YWwpO1xuXG5mdW5jdGlvbiBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KG9iaikgeyByZXR1cm4gb2JqICYmIG9iai5fX2VzTW9kdWxlID8gb2JqIDogeyBkZWZhdWx0OiBvYmogfTsgfVxuXG5leHBvcnRzLmRlZmF1bHQgPSBfc2hhbGxvd0VxdWFsMi5kZWZhdWx0O1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS9zaGFsbG93RXF1YWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9yZWFjdCA9IHJlcXVpcmUoJ3JlYWN0Jyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUgPSByZXF1aXJlKCcuL3NldERpc3BsYXlOYW1lJyk7XG5cbnZhciBfc2V0RGlzcGxheU5hbWUyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfc2V0RGlzcGxheU5hbWUpO1xuXG52YXIgX3dyYXBEaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vd3JhcERpc3BsYXlOYW1lJyk7XG5cbnZhciBfd3JhcERpc3BsYXlOYW1lMiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3dyYXBEaXNwbGF5TmFtZSk7XG5cbnZhciBfY3JlYXRlRWFnZXJGYWN0b3J5ID0gcmVxdWlyZSgnLi9jcmVhdGVFYWdlckZhY3RvcnknKTtcblxudmFyIF9jcmVhdGVFYWdlckZhY3RvcnkyID0gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChfY3JlYXRlRWFnZXJGYWN0b3J5KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxuZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3RvcikgeyBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkgeyB0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpOyB9IH1cblxuZnVuY3Rpb24gX3Bvc3NpYmxlQ29uc3RydWN0b3JSZXR1cm4oc2VsZiwgY2FsbCkgeyBpZiAoIXNlbGYpIHsgdGhyb3cgbmV3IFJlZmVyZW5jZUVycm9yKFwidGhpcyBoYXNuJ3QgYmVlbiBpbml0aWFsaXNlZCAtIHN1cGVyKCkgaGFzbid0IGJlZW4gY2FsbGVkXCIpOyB9IHJldHVybiBjYWxsICYmICh0eXBlb2YgY2FsbCA9PT0gXCJvYmplY3RcIiB8fCB0eXBlb2YgY2FsbCA9PT0gXCJmdW5jdGlvblwiKSA/IGNhbGwgOiBzZWxmOyB9XG5cbmZ1bmN0aW9uIF9pbmhlcml0cyhzdWJDbGFzcywgc3VwZXJDbGFzcykgeyBpZiAodHlwZW9mIHN1cGVyQ2xhc3MgIT09IFwiZnVuY3Rpb25cIiAmJiBzdXBlckNsYXNzICE9PSBudWxsKSB7IHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvbiwgbm90IFwiICsgdHlwZW9mIHN1cGVyQ2xhc3MpOyB9IHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwgeyBjb25zdHJ1Y3RvcjogeyB2YWx1ZTogc3ViQ2xhc3MsIGVudW1lcmFibGU6IGZhbHNlLCB3cml0YWJsZTogdHJ1ZSwgY29uZmlndXJhYmxlOiB0cnVlIH0gfSk7IGlmIChzdXBlckNsYXNzKSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3Quc2V0UHJvdG90eXBlT2Yoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIDogc3ViQ2xhc3MuX19wcm90b19fID0gc3VwZXJDbGFzczsgfVxuXG52YXIgc2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gc2hvdWxkVXBkYXRlKHRlc3QpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChCYXNlQ29tcG9uZW50KSB7XG4gICAgdmFyIGZhY3RvcnkgPSAoMCwgX2NyZWF0ZUVhZ2VyRmFjdG9yeTIuZGVmYXVsdCkoQmFzZUNvbXBvbmVudCk7XG5cbiAgICB2YXIgU2hvdWxkVXBkYXRlID0gZnVuY3Rpb24gKF9Db21wb25lbnQpIHtcbiAgICAgIF9pbmhlcml0cyhTaG91bGRVcGRhdGUsIF9Db21wb25lbnQpO1xuXG4gICAgICBmdW5jdGlvbiBTaG91bGRVcGRhdGUoKSB7XG4gICAgICAgIF9jbGFzc0NhbGxDaGVjayh0aGlzLCBTaG91bGRVcGRhdGUpO1xuXG4gICAgICAgIHJldHVybiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybih0aGlzLCBfQ29tcG9uZW50LmFwcGx5KHRoaXMsIGFyZ3VtZW50cykpO1xuICAgICAgfVxuXG4gICAgICBTaG91bGRVcGRhdGUucHJvdG90eXBlLnNob3VsZENvbXBvbmVudFVwZGF0ZSA9IGZ1bmN0aW9uIHNob3VsZENvbXBvbmVudFVwZGF0ZShuZXh0UHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRlc3QodGhpcy5wcm9wcywgbmV4dFByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIFNob3VsZFVwZGF0ZS5wcm90b3R5cGUucmVuZGVyID0gZnVuY3Rpb24gcmVuZGVyKCkge1xuICAgICAgICByZXR1cm4gZmFjdG9yeSh0aGlzLnByb3BzKTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBTaG91bGRVcGRhdGU7XG4gICAgfShfcmVhY3QuQ29tcG9uZW50KTtcblxuICAgIGlmIChwcm9jZXNzLmVudi5OT0RFX0VOViAhPT0gJ3Byb2R1Y3Rpb24nKSB7XG4gICAgICByZXR1cm4gKDAsIF9zZXREaXNwbGF5TmFtZTIuZGVmYXVsdCkoKDAsIF93cmFwRGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQsICdzaG91bGRVcGRhdGUnKSkoU2hvdWxkVXBkYXRlKTtcbiAgICB9XG4gICAgcmV0dXJuIFNob3VsZFVwZGF0ZTtcbiAgfTtcbn07XG5cbmV4cG9ydHMuZGVmYXVsdCA9IHNob3VsZFVwZGF0ZTtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgaWQgPSAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2Uvc2hvdWxkVXBkYXRlLmpzXG4vLyBtb2R1bGUgY2h1bmtzID0gMCAxIDMgNCA1IDYgMTAgMTEgMTIgMTMgMTQgMTUgMTYgMTcgMjQgMjYgMjcgMjggMzEgMzQgMzUgMzYgMzcgMzggMzkgNDAgNDMgNDQgNDUgNDYgNDkiLCIndXNlIHN0cmljdCc7XG5cbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5cbnZhciBfZXh0ZW5kcyA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gKHRhcmdldCkgeyBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykgeyB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldOyBmb3IgKHZhciBrZXkgaW4gc291cmNlKSB7IGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7IHRhcmdldFtrZXldID0gc291cmNlW2tleV07IH0gfSB9IHJldHVybiB0YXJnZXQ7IH07XG5cbnZhciBfcmVhY3QgPSByZXF1aXJlKCdyZWFjdCcpO1xuXG52YXIgX3JlYWN0MiA9IF9pbnRlcm9wUmVxdWlyZURlZmF1bHQoX3JlYWN0KTtcblxuZnVuY3Rpb24gX2ludGVyb3BSZXF1aXJlRGVmYXVsdChvYmopIHsgcmV0dXJuIG9iaiAmJiBvYmouX19lc01vZHVsZSA/IG9iaiA6IHsgZGVmYXVsdDogb2JqIH07IH1cblxudmFyIGNyZWF0ZUVhZ2VyRWxlbWVudFV0aWwgPSBmdW5jdGlvbiBjcmVhdGVFYWdlckVsZW1lbnRVdGlsKGhhc0tleSwgaXNSZWZlcmVudGlhbGx5VHJhbnNwYXJlbnQsIHR5cGUsIHByb3BzLCBjaGlsZHJlbikge1xuICBpZiAoIWhhc0tleSAmJiBpc1JlZmVyZW50aWFsbHlUcmFuc3BhcmVudCkge1xuICAgIGlmIChjaGlsZHJlbikge1xuICAgICAgcmV0dXJuIHR5cGUoX2V4dGVuZHMoe30sIHByb3BzLCB7IGNoaWxkcmVuOiBjaGlsZHJlbiB9KSk7XG4gICAgfVxuICAgIHJldHVybiB0eXBlKHByb3BzKTtcbiAgfVxuXG4gIHZhciBDb21wb25lbnQgPSB0eXBlO1xuXG4gIGlmIChjaGlsZHJlbikge1xuICAgIHJldHVybiBfcmVhY3QyLmRlZmF1bHQuY3JlYXRlRWxlbWVudChcbiAgICAgIENvbXBvbmVudCxcbiAgICAgIHByb3BzLFxuICAgICAgY2hpbGRyZW5cbiAgICApO1xuICB9XG5cbiAgcmV0dXJuIF9yZWFjdDIuZGVmYXVsdC5jcmVhdGVFbGVtZW50KENvbXBvbmVudCwgcHJvcHMpO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gY3JlYXRlRWFnZXJFbGVtZW50VXRpbDtcblxuXG4vLy8vLy8vLy8vLy8vLy8vLy9cbi8vIFdFQlBBQ0sgRk9PVEVSXG4vLyAuL25vZGVfbW9kdWxlcy9yZWNvbXBvc2UvdXRpbHMvY3JlYXRlRWFnZXJFbGVtZW50VXRpbC5qc1xuLy8gbW9kdWxlIGlkID0gLi9ub2RlX21vZHVsZXMvcmVjb21wb3NlL3V0aWxzL2NyZWF0ZUVhZ2VyRWxlbWVudFV0aWwuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIid1c2Ugc3RyaWN0JztcblxuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcblxudmFyIF9nZXREaXNwbGF5TmFtZSA9IHJlcXVpcmUoJy4vZ2V0RGlzcGxheU5hbWUnKTtcblxudmFyIF9nZXREaXNwbGF5TmFtZTIgPSBfaW50ZXJvcFJlcXVpcmVEZWZhdWx0KF9nZXREaXNwbGF5TmFtZSk7XG5cbmZ1bmN0aW9uIF9pbnRlcm9wUmVxdWlyZURlZmF1bHQob2JqKSB7IHJldHVybiBvYmogJiYgb2JqLl9fZXNNb2R1bGUgPyBvYmogOiB7IGRlZmF1bHQ6IG9iaiB9OyB9XG5cbnZhciB3cmFwRGlzcGxheU5hbWUgPSBmdW5jdGlvbiB3cmFwRGlzcGxheU5hbWUoQmFzZUNvbXBvbmVudCwgaG9jTmFtZSkge1xuICByZXR1cm4gaG9jTmFtZSArICcoJyArICgwLCBfZ2V0RGlzcGxheU5hbWUyLmRlZmF1bHQpKEJhc2VDb21wb25lbnQpICsgJyknO1xufTtcblxuZXhwb3J0cy5kZWZhdWx0ID0gd3JhcERpc3BsYXlOYW1lO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBpZCA9IC4vbm9kZV9tb2R1bGVzL3JlY29tcG9zZS93cmFwRGlzcGxheU5hbWUuanNcbi8vIG1vZHVsZSBjaHVua3MgPSAwIDEgMyA0IDUgNiAxMCAxMSAxMiAxMyAxNCAxNSAxNiAxNyAyNCAyNiAyNyAyOCAzMSAzNCAzNSAzNiAzNyAzOCAzOSA0MCA0MyA0NCA0NSA0NiA0OSIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuaW1wb3J0IHsgY29ubmVjdCB9IGZyb20gJ3JlYWN0LXJlZHV4JztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuaW1wb3J0IEdyaWQgZnJvbSAnbWF0ZXJpYWwtdWkvR3JpZCc7XG5pbXBvcnQgQ2FyZCwgeyBDYXJkSGVhZGVyLCBDYXJkQ29udGVudCB9IGZyb20gJ21hdGVyaWFsLXVpL0NhcmQnO1xuXG5pbXBvcnQgTWF5YXNoRWRpdG9yLCB7IGNyZWF0ZUVkaXRvclN0YXRlIH0gZnJvbSAnLi4vLi4vLi4vbGliL21heWFzaC1lZGl0b3InO1xuXG5pbXBvcnQgRXJyb3JQYWdlIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvRXJyb3JQYWdlJztcblxuY29uc3Qgc3R5bGVzID0ge1xuICByb290OiB7XG4gICAgZmxleEdyb3c6IDEsXG4gICAgcGFkZGluZ1RvcDogJzVweCcsXG4gIH0sXG4gIHRpdGxlOiB7XG4gICAgdGV4dEFsaWduOiAnY2VudGVyJyxcbiAgfSxcbiAgZmxleEdyb3c6IHtcbiAgICBmbGV4OiAnMSAxIGF1dG8nLFxuICB9LFxuICBjYXJkOiB7XG4gICAgYm9yZGVyUmFkaXVzOiAnOHB4JyxcbiAgfSxcbn07XG5cbmNsYXNzIFJlc3VtZVBhZ2UgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgaG92ZXI6IGZhbHNlLFxuICAgIH07XG4gIH1cblxuICAvLyBzaG91bGRDb21wb25lbnRVcGRhdGUobmV4dFN0YXRlLCBuZXh0UHJvcHMpIHtcbiAgLy8gICByZXR1cm4gdGhpcy5zdGF0ZSAhPT0gbmV4dFN0YXRlIHx8IHRoaXMucHJvcHMgIT09IG5leHRQcm9wcztcbiAgLy8gfVxuXG4gIG9uTW91c2VFbnRlciA9ICgpID0+IHRoaXMuc2V0U3RhdGUoeyBob3ZlcjogdHJ1ZSB9KTtcbiAgb25Nb3VzZUxlYXZlID0gKCkgPT4gdGhpcy5zZXRTdGF0ZSh7IGhvdmVyOiBmYWxzZSB9KTtcbiAgb25DaGFuZ2UgPSAoKSA9PiB7fTtcblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjbGFzc2VzLCBlbGVtZW50cywgbWF0Y2ggfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyB1c2VybmFtZSB9ID0gbWF0Y2gucGFyYW1zO1xuICAgIGNvbnN0IHsgaG92ZXIgfSA9IHRoaXMuc3RhdGU7XG5cbiAgICBjb25zdCB1ID0gT2JqZWN0LmtleXMoZWxlbWVudHMpLmZpbmQoXG4gICAgICAoaWQpID0+IGVsZW1lbnRzW2lkXS51c2VybmFtZSA9PT0gdXNlcm5hbWUsXG4gICAgKTtcblxuICAgIGNvbnN0IGVsZW1lbnQgPSBlbGVtZW50c1t1XTtcblxuICAgIGlmICghZWxlbWVudCB8fCBlbGVtZW50LnN0YXR1c0NvZGUgPj0gMzAwKSB7XG4gICAgICByZXR1cm4gPEVycm9yUGFnZSB7Li4uZWxlbWVudH0gLz47XG4gICAgfVxuXG4gICAgaWYgKGVsZW1lbnQuZWxlbWVudFR5cGUgIT09ICd1c2VyJykge1xuICAgICAgcmV0dXJuIDxFcnJvclBhZ2UgLz47XG4gICAgfVxuXG4gICAgaWYgKCFlbGVtZW50LnJlc3VtZSkge1xuICAgICAgcmV0dXJuIDxFcnJvclBhZ2UgLz47XG4gICAgfVxuXG4gICAgcmV0dXJuIChcbiAgICAgIDxHcmlkXG4gICAgICAgIGNvbnRhaW5lclxuICAgICAgICBqdXN0aWZ5PVwiY2VudGVyXCJcbiAgICAgICAgc3BhY2luZz17MH1cbiAgICAgICAgY2xhc3NOYW1lPXtjbGFzc2VzLnJvb3R9XG4gICAgICAgIG9uTW91c2VFbnRlcj17dGhpcy5vbk1vdXNlRW50ZXJ9XG4gICAgICAgIG9uTW91c2VMZWF2ZT17dGhpcy5vbk1vdXNlTGVhdmV9XG4gICAgICA+XG4gICAgICAgIDxHcmlkIGl0ZW0geHM9ezExfSBzbT17MTF9IG1kPXsxMX0gbGc9ezExfSB4bD17MTF9PlxuICAgICAgICAgIDxDYXJkIHJhaXNlZD17aG92ZXJ9IGNsYXNzTmFtZT17Y2xhc3Nlcy5jYXJkfT5cbiAgICAgICAgICAgIDxDYXJkSGVhZGVyIHRpdGxlPXsnUmVzdW1lJ30gY2xhc3NOYW1lPXtjbGFzc2VzLnRpdGxlfSAvPlxuICAgICAgICAgICAgPENhcmRDb250ZW50PlxuICAgICAgICAgICAgICA8TWF5YXNoRWRpdG9yXG4gICAgICAgICAgICAgICAgZWRpdG9yU3RhdGU9e2NyZWF0ZUVkaXRvclN0YXRlKGVsZW1lbnQucmVzdW1lKX1cbiAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZX1cbiAgICAgICAgICAgICAgICByZWFkT25seVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9DYXJkQ29udGVudD5cbiAgICAgICAgICA8L0NhcmQ+XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cblJlc3VtZVBhZ2UucHJvcFR5cGVzID0ge1xuICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIG1hdGNoOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gIGVsZW1lbnRzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cyB9KSA9PiAoeyBlbGVtZW50cyB9KTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMpKHdpdGhTdHlsZXMoc3R5bGVzKShSZXN1bWVQYWdlKSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2NvbnRhaW5lcnMvUmVzdW1lUGFnZS9pbmRleC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3JlYWN0L2xpYi9SZWFjdFByb3BUeXBlcyc7XG5pbXBvcnQgY2xhc3NOYW1lcyBmcm9tICdjbGFzc25hbWVzJztcblxuaW1wb3J0IHdpdGhTdHlsZXMgZnJvbSAnbWF0ZXJpYWwtdWkvc3R5bGVzL3dpdGhTdHlsZXMnO1xuXG4vLyBpbXBvcnQgSWNvbiBmcm9tICdtYXRlcmlhbC11aS9JY29uJztcbmltcG9ydCBJY29uQnV0dG9uIGZyb20gJ21hdGVyaWFsLXVpL0ljb25CdXR0b24nO1xuaW1wb3J0IFRvb2x0aXAgZnJvbSAnbWF0ZXJpYWwtdWkvVG9vbHRpcCc7XG5cbmltcG9ydCBUaXRsZUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvVGl0bGUnO1xuaW1wb3J0IEZvcm1hdEJvbGRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEJvbGQnO1xuaW1wb3J0IEZvcm1hdEl0YWxpYyBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJdGFsaWMnO1xuaW1wb3J0IEZvcm1hdFVuZGVybGluZWRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdFVuZGVybGluZWQnO1xuaW1wb3J0IEZvcm1hdFF1b3RlSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRRdW90ZSc7XG4vLyBpbXBvcnQgRm9ybWF0Q2xlYXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENsZWFyJztcbi8vIGltcG9ydCBGb3JtYXRDb2xvckZpbGxJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdENvbG9yRmlsbCc7XG4vLyBpbXBvcnQgRm9ybWF0Q29sb3JSZXNldEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0Q29sb3JSZXNldCc7XG4vLyBpbXBvcnQgRm9ybWF0Q29sb3JUZXh0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRDb2xvclRleHQnO1xuLy8gaW1wb3J0IEZvcm1hdEluZGVudERlY3JlYXNlSWNvblxuLy8gICBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRJbmRlbnREZWNyZWFzZSc7XG4vLyBpbXBvcnQgRm9ybWF0SW5kZW50SW5jcmVhc2VJY29uXG4vLyAgIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEluZGVudEluY3JlYXNlJztcblxuaW1wb3J0IENvZGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0NvZGUnO1xuXG5pbXBvcnQgRm9ybWF0TGlzdE51bWJlcmVkSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRMaXN0TnVtYmVyZWQnO1xuaW1wb3J0IEZvcm1hdExpc3RCdWxsZXRlZEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0TGlzdEJ1bGxldGVkJztcblxuaW1wb3J0IEZvcm1hdEFsaWduQ2VudGVySWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkNlbnRlcic7XG5pbXBvcnQgRm9ybWF0QWxpZ25MZWZ0SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9Gb3JtYXRBbGlnbkxlZnQnO1xuaW1wb3J0IEZvcm1hdEFsaWduUmlnaHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Zvcm1hdEFsaWduUmlnaHQnO1xuaW1wb3J0IEZvcm1hdEFsaWduSnVzdGlmeUljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvRm9ybWF0QWxpZ25KdXN0aWZ5JztcblxuaW1wb3J0IEF0dGFjaEZpbGVJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0F0dGFjaEZpbGUnO1xuaW1wb3J0IEluc2VydExpbmtJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydExpbmsnO1xuaW1wb3J0IEluc2VydFBob3RvSWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRQaG90byc7XG5pbXBvcnQgSW5zZXJ0RW1vdGljb25JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0luc2VydEVtb3RpY29uJztcbmltcG9ydCBJbnNlcnRDb21tZW50SWNvbiBmcm9tICdtYXRlcmlhbC11aS1pY29ucy9JbnNlcnRDb21tZW50JztcblxuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25Ub3BJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25Ub3AnO1xuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25Cb3R0b21JY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25Cb3R0b20nO1xuLy8gaW1wb3J0IFZlcnRpY2FsQWxpZ25DZW50ZXJJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1ZlcnRpY2FsQWxpZ25DZW50ZXInO1xuXG4vLyBpbXBvcnQgV3JhcFRleHRJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL1dyYXBUZXh0JztcblxuaW1wb3J0IEhpZ2hsaWdodEljb24gZnJvbSAnbWF0ZXJpYWwtdWktaWNvbnMvSGlnaGxpZ2h0JztcbmltcG9ydCBGdW5jdGlvbnNJY29uIGZyb20gJ21hdGVyaWFsLXVpLWljb25zL0Z1bmN0aW9ucyc7XG5cbmltcG9ydCB7XG4gIEVkaXRvcixcbiAgUmljaFV0aWxzLFxuICAvLyBFbnRpdHksXG4gIC8vIENvbXBvc2l0ZURlY29yYXRvcixcbiAgQXRvbWljQmxvY2tVdGlscyxcbiAgRWRpdG9yU3RhdGUsXG4gIC8vICAgY29udmVydFRvUmF3LFxufSBmcm9tICdkcmFmdC1qcyc7XG5cbi8vIGltcG9ydCB7XG4vLyAgIGhhc2h0YWdTdHJhdGVneSxcbi8vICAgSGFzaHRhZ1NwYW4sXG5cbi8vICAgaGFuZGxlU3RyYXRlZ3ksXG4vLyAgIEhhbmRsZVNwYW4sXG4vLyB9IGZyb20gJy4vY29tcG9uZW50cy9EZWNvcmF0b3JzJztcblxuaW1wb3J0IEF0b21pYyBmcm9tICcuL2NvbXBvbmVudHMvQXRvbWljJztcbmltcG9ydCBzdHlsZXMgZnJvbSAnLi9FZGl0b3JTdHlsZXMnO1xuXG5pbXBvcnQgeyBCbG9ja3MsIEhBTkRMRUQsIE5PVF9IQU5ETEVEIH0gZnJvbSAnLi9jb25zdGFudHMnO1xuXG4vKipcbiAqIE1heWFzaEVkaXRvclxuICovXG5jbGFzcyBNYXlhc2hFZGl0b3IgZXh0ZW5kcyBDb21wb25lbnQge1xuICAvKipcbiAgICogTWF5YXNoIEVkaXRvcidzIHByb3B0eXBlcyBhcmUgZGVmaW5lZCBoZXJlLlxuICAgKi9cbiAgc3RhdGljIHByb3BUeXBlcyA9IHtcbiAgICBjbGFzc2VzOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG5cbiAgICBlZGl0b3JTdGF0ZTogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYy5pc1JlcXVpcmVkLFxuXG4gICAgcGxhY2Vob2xkZXI6IFByb3BUeXBlcy5zdHJpbmcsXG5cbiAgICByZWFkT25seTogUHJvcFR5cGVzLmJvb2wuaXNSZXF1aXJlZCxcblxuICAgIC8qKlxuICAgICAqIFRoaXMgYXBpIGZ1bmN0aW9uIHNob3VsZCBiZSBhcHBsaWVkIGZvciBlbmFibGluZyBwaG90byBhZGRpbmdcbiAgICAgKiBmZWF0dXJlIGluIE1heWFzaCBFZGl0b3IuXG4gICAgICovXG4gICAgYXBpUGhvdG9VcGxvYWQ6IFByb3BUeXBlcy5mdW5jLFxuICB9O1xuXG4gIHN0YXRpYyBkZWZhdWx0UHJvcHMgPSB7XG4gICAgcmVhZE9ubHk6IHRydWUsXG4gICAgcGxhY2Vob2xkZXI6ICdXcml0ZSBIZXJlLi4uJyxcbiAgfTtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIC8vIHRoaXMuc3RhdGUgPSB7fTtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBiZSBzdXBwbGllZCB0byBEcmFmdC5qcyBFZGl0b3IgdG8gaGFuZGxlXG4gICAgICogb25DaGFuZ2UgZXZlbnQuXG4gICAgICogQGZ1bmN0aW9uIG9uQ2hhbmdlXG4gICAgICogQHBhcmFtIHtPYmplY3R9IGVkaXRvclN0YXRlXG4gICAgICovXG4gICAgdGhpcy5vbkNoYW5nZSA9IChlZGl0b3JTdGF0ZSkgPT4ge1xuICAgICAgdGhpcy5wcm9wcy5vbkNoYW5nZShlZGl0b3JTdGF0ZSk7XG4gICAgfTtcblxuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZm9jdXMgZXZlbnQgaW4gRHJhZnQuanMgRWRpdG9yLlxuICAgICAqIEBmdW5jdGlvbiBmb2N1c1xuICAgICAqL1xuICAgIHRoaXMuZm9jdXMgPSAoKSA9PiB0aGlzLmVkaXRvck5vZGUuZm9jdXMoKTtcblxuICAgIHRoaXMub25UYWIgPSB0aGlzLm9uVGFiLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblRpdGxlQ2xpY2sgPSB0aGlzLm9uVGl0bGVDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25Db2RlQ2xpY2sgPSB0aGlzLm9uQ29kZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblF1b3RlQ2xpY2sgPSB0aGlzLm9uUXVvdGVDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25MaXN0QnVsbGV0ZWRDbGljayA9IHRoaXMub25MaXN0QnVsbGV0ZWRDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25MaXN0TnVtYmVyZWRDbGljayA9IHRoaXMub25MaXN0TnVtYmVyZWRDbGljay5iaW5kKHRoaXMpO1xuICAgIHRoaXMub25Cb2xkQ2xpY2sgPSB0aGlzLm9uQm9sZENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkl0YWxpY0NsaWNrID0gdGhpcy5vbkl0YWxpY0NsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vblVuZGVyTGluZUNsaWNrID0gdGhpcy5vblVuZGVyTGluZUNsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkhpZ2hsaWdodENsaWNrID0gdGhpcy5vbkhpZ2hsaWdodENsaWNrLmJpbmQodGhpcyk7XG4gICAgdGhpcy5oYW5kbGVLZXlDb21tYW5kID0gdGhpcy5oYW5kbGVLZXlDb21tYW5kLmJpbmQodGhpcyk7XG4gICAgdGhpcy5vbkNsaWNrSW5zZXJ0UGhvdG8gPSB0aGlzLm9uQ2xpY2tJbnNlcnRQaG90by5iaW5kKHRoaXMpO1xuXG4gICAgLy8gdGhpcy5ibG9ja1JlbmRlcmVyRm4gPSB0aGlzLmJsb2NrUmVuZGVyZXJGbi5iaW5kKHRoaXMpO1xuXG4gICAgLy8gY29uc3QgY29tcG9zaXRlRGVjb3JhdG9yID0gbmV3IENvbXBvc2l0ZURlY29yYXRvcihbXG4gICAgLy8gICB7XG4gICAgLy8gICAgIHN0cmF0ZWd5OiBoYW5kbGVTdHJhdGVneSxcbiAgICAvLyAgICAgY29tcG9uZW50OiBIYW5kbGVTcGFuLFxuICAgIC8vICAgfSxcbiAgICAvLyAgIHtcbiAgICAvLyAgICAgc3RyYXRlZ3k6IGhhc2h0YWdTdHJhdGVneSxcbiAgICAvLyAgICAgY29tcG9uZW50OiBIYXNodGFnU3BhbixcbiAgICAvLyAgIH0sXG4gICAgLy8gXSk7XG4gIH1cblxuICAvKipcbiAgICogb25UYWIoKSB3aWxsIGhhbmRsZSBUYWIgYnV0dG9uIHByZXNzIGV2ZW50IGluIEVkaXRvci5cbiAgICogQGZ1bmN0aW9uIG9uVGFiXG4gICAqIEBwYXJhbSB7RXZlbnR9IGVcbiAgICovXG4gIG9uVGFiKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLm9uVGFiKGUsIGVkaXRvclN0YXRlLCA0KTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gVGl0bGUgQnV0dG9uLlxuICAgKiBAZnVuY3Rpb24gb25UaXRsZUNsaWNrXG4gICAqL1xuICBvblRpdGxlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2hlYWRlci1vbmUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gQ29kZSBCdXR0b24uXG4gICAqIEBmdW5jdGlvbiBvbkNvZGVDbGlja1xuICAgKi9cbiAgb25Db2RlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2NvZGUtYmxvY2snKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCBoYW5kbGUgZXZlbnQgb24gUXVvdGUgQnV0dG9uLlxuICAgKiBAZnVuY3Rpb24gb25RdW90ZUNsaWNrXG4gICAqL1xuICBvblF1b3RlQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShlZGl0b3JTdGF0ZSwgJ2Jsb2NrcXVvdGUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCB1cGRhdGUgYmxvY2sgd2l0aCB1bm9yZGVyZWQgbGlzdCBpdGVtc1xuICAgKiBAZnVuY3Rpb24gb25MaXN0QnVsbGV0ZWRDbGlja1xuICAgKi9cbiAgb25MaXN0QnVsbGV0ZWRDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlQmxvY2tUeXBlKFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICAndW5vcmRlcmVkLWxpc3QtaXRlbScsXG4gICAgKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqIFRoaXMgZnVuY3Rpb24gd2lsbCB1cGRhdGUgYmxvY2sgd2l0aCBvcmRlcmVkIGxpc3QgaXRlbS5cbiAgICogQGZ1bmN0aW9uIG9uTGlzdE51bWJlcmVkQ2xpY2tcbiAgICovXG4gIG9uTGlzdE51bWJlcmVkQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUJsb2NrVHlwZShcbiAgICAgIGVkaXRvclN0YXRlLFxuICAgICAgJ29yZGVyZWQtbGlzdC1pdGVtJyxcbiAgICApO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGdpdmUgc2VsZWN0ZWQgY29udGVudCBCb2xkIHN0eWxpbmcuXG4gICAqIEBmdW5jdGlvbiBvbkJvbGRDbGlja1xuICAgKi9cbiAgb25Cb2xkQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnQk9MRCcpO1xuXG4gICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gIH1cblxuICAvKipcbiAgICogVGhpcyBmdW5jdGlvbiB3aWxsIGdpdmUgaXRhbGljIHN0eWxpbmcgdG8gc2VsZWN0ZWQgY29udGVudC5cbiAgICogQGZ1bmN0aW9uIG9uSXRhbGljQ2xpY2tcbiAgICovXG4gIG9uSXRhbGljQ2xpY2soKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLnRvZ2dsZUlubGluZVN0eWxlKGVkaXRvclN0YXRlLCAnSVRBTElDJyk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSB1bmRlcmxpbmUgc3R5bGluZyB0byBzZWxlY3RlZCBjb250ZW50LlxuICAgKiBAZnVuY3Rpb24gb25VbmRlckxpbmVDbGlja1xuICAgKi9cbiAgb25VbmRlckxpbmVDbGljaygpIHtcbiAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgbmV3RWRpdG9yU3RhdGUgPSBSaWNoVXRpbHMudG9nZ2xlSW5saW5lU3R5bGUoXG4gICAgICBlZGl0b3JTdGF0ZSxcbiAgICAgICdVTkRFUkxJTkUnLFxuICAgICk7XG5cbiAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgaGlnaGxpZ2h0IHNlbGVjdGVkIGNvbnRlbnQuXG4gICAqIEBmdW5jdGlvbiBvbkhpZ2hsaWdodENsaWNrXG4gICAqL1xuICBvbkhpZ2hsaWdodENsaWNrKCkge1xuICAgIGNvbnN0IHsgZWRpdG9yU3RhdGUgfSA9IHRoaXMucHJvcHM7XG5cbiAgICBjb25zdCBuZXdFZGl0b3JTdGF0ZSA9IFJpY2hVdGlscy50b2dnbGVJbmxpbmVTdHlsZShlZGl0b3JTdGF0ZSwgJ0NPREUnKTtcblxuICAgIHRoaXMub25DaGFuZ2UobmV3RWRpdG9yU3RhdGUpO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqL1xuICBvbkNsaWNrSW5zZXJ0UGhvdG8oKSB7XG4gICAgdGhpcy5waG90by52YWx1ZSA9IG51bGw7XG4gICAgdGhpcy5waG90by5jbGljaygpO1xuICB9XG5cbiAgLyoqXG4gICAqXG4gICAqL1xuICBvbkNoYW5nZUluc2VydFBob3RvID0gYXN5bmMgKGUpID0+IHtcbiAgICAvLyBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgY29uc3QgZmlsZSA9IGUudGFyZ2V0LmZpbGVzWzBdO1xuXG4gICAgaWYgKGZpbGUudHlwZS5pbmRleE9mKCdpbWFnZS8nKSA9PT0gMCkge1xuICAgICAgY29uc3QgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcbiAgICAgIGZvcm1EYXRhLmFwcGVuZCgncGhvdG8nLCBmaWxlKTtcblxuICAgICAgY29uc3QgeyBzdGF0dXNDb2RlLCBlcnJvciwgcGF5bG9hZCB9ID0gYXdhaXQgdGhpcy5wcm9wcy5hcGlQaG90b1VwbG9hZCh7XG4gICAgICAgIGZvcm1EYXRhLFxuICAgICAgfSk7XG5cbiAgICAgIGlmIChzdGF0dXNDb2RlID49IDMwMCkge1xuICAgICAgICAvLyBoYW5kbGUgRXJyb3JcbiAgICAgICAgY29uc29sZS5lcnJvcihzdGF0dXNDb2RlLCBlcnJvcik7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgY29uc3QgeyBwaG90b1VybDogc3JjIH0gPSBwYXlsb2FkO1xuXG4gICAgICBjb25zdCB7IGVkaXRvclN0YXRlIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgICBjb25zdCBjb250ZW50U3RhdGUgPSBlZGl0b3JTdGF0ZS5nZXRDdXJyZW50Q29udGVudCgpO1xuICAgICAgY29uc3QgY29udGVudFN0YXRlV2l0aEVudGl0eSA9IGNvbnRlbnRTdGF0ZS5jcmVhdGVFbnRpdHkoXG4gICAgICAgIEJsb2Nrcy5QSE9UTyxcbiAgICAgICAgJ0lNTVVUQUJMRScsXG4gICAgICAgIHsgc3JjIH0sXG4gICAgICApO1xuXG4gICAgICBjb25zdCBlbnRpdHlLZXkgPSBjb250ZW50U3RhdGVXaXRoRW50aXR5LmdldExhc3RDcmVhdGVkRW50aXR5S2V5KCk7XG5cbiAgICAgIGNvbnN0IG1pZGRsZUVkaXRvclN0YXRlID0gRWRpdG9yU3RhdGUuc2V0KGVkaXRvclN0YXRlLCB7XG4gICAgICAgIGN1cnJlbnRDb250ZW50OiBjb250ZW50U3RhdGVXaXRoRW50aXR5LFxuICAgICAgfSk7XG5cbiAgICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gQXRvbWljQmxvY2tVdGlscy5pbnNlcnRBdG9taWNCbG9jayhcbiAgICAgICAgbWlkZGxlRWRpdG9yU3RhdGUsXG4gICAgICAgIGVudGl0eUtleSxcbiAgICAgICAgJyAnLFxuICAgICAgKTtcblxuICAgICAgdGhpcy5vbkNoYW5nZShuZXdFZGl0b3JTdGF0ZSk7XG4gICAgfVxuICB9O1xuXG4gIC8qKlxuICAgKiBUaGlzIGZ1bmN0aW9uIHdpbGwgZ2l2ZSBjdXN0b20gaGFuZGxlIGNvbW1hbmRzIHRvIEVkaXRvci5cbiAgICogQGZ1bmN0aW9uIGhhbmRsZUtleUNvbW1hbmRcbiAgICogQHBhcmFtIHtzdHJpbmd9IGNvbW1hbmRcbiAgICogQHJldHVybiB7c3RyaW5nfVxuICAgKi9cbiAgaGFuZGxlS2V5Q29tbWFuZChjb21tYW5kKSB7XG4gICAgY29uc3QgeyBlZGl0b3JTdGF0ZSB9ID0gdGhpcy5wcm9wcztcblxuICAgIGNvbnN0IG5ld0VkaXRvclN0YXRlID0gUmljaFV0aWxzLmhhbmRsZUtleUNvbW1hbmQoZWRpdG9yU3RhdGUsIGNvbW1hbmQpO1xuXG4gICAgaWYgKG5ld0VkaXRvclN0YXRlKSB7XG4gICAgICB0aGlzLm9uQ2hhbmdlKG5ld0VkaXRvclN0YXRlKTtcbiAgICAgIHJldHVybiBIQU5ETEVEO1xuICAgIH1cblxuICAgIHJldHVybiBOT1RfSEFORExFRDtcbiAgfVxuXG4gIC8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbiAgYmxvY2tSZW5kZXJlckZuKGJsb2NrKSB7XG4gICAgc3dpdGNoIChibG9jay5nZXRUeXBlKCkpIHtcbiAgICAgIGNhc2UgQmxvY2tzLkFUT01JQzpcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBjb21wb25lbnQ6IEF0b21pYyxcbiAgICAgICAgICBlZGl0YWJsZTogZmFsc2UsXG4gICAgICAgIH07XG5cbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuICAvKiBlc2xpbnQtZW5hYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cblxuICAvKiBlc2xpbnQtZGlzYWJsZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzICovXG4gIGJsb2NrU3R5bGVGbihibG9jaykge1xuICAgIHN3aXRjaCAoYmxvY2suZ2V0VHlwZSgpKSB7XG4gICAgICBjYXNlICdibG9ja3F1b3RlJzpcbiAgICAgICAgcmV0dXJuICdSaWNoRWRpdG9yLWJsb2NrcXVvdGUnO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gIH1cbiAgLyogZXNsaW50LWVuYWJsZSBjbGFzcy1tZXRob2RzLXVzZS10aGlzICovXG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHtcbiAgICAgIGNsYXNzZXMsXG4gICAgICByZWFkT25seSxcbiAgICAgIG9uQ2hhbmdlLFxuICAgICAgZWRpdG9yU3RhdGUsXG4gICAgICBwbGFjZWhvbGRlcixcbiAgICB9ID0gdGhpcy5wcm9wcztcblxuICAgIC8vIEN1c3RvbSBvdmVycmlkZXMgZm9yIFwiY29kZVwiIHN0eWxlLlxuICAgIGNvbnN0IHN0eWxlTWFwID0ge1xuICAgICAgQ09ERToge1xuICAgICAgICBiYWNrZ3JvdW5kQ29sb3I6ICdyZ2JhKDAsIDAsIDAsIDAuMDUpJyxcbiAgICAgICAgZm9udEZhbWlseTogJ1wiSW5jb25zb2xhdGFcIiwgXCJNZW5sb1wiLCBcIkNvbnNvbGFzXCIsIG1vbm9zcGFjZScsXG4gICAgICAgIGZvbnRTaXplOiAxNixcbiAgICAgICAgcGFkZGluZzogMixcbiAgICAgIH0sXG4gICAgfTtcblxuICAgIHJldHVybiAoXG4gICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3Nlcy5yb290fT5cbiAgICAgICAgeyFyZWFkT25seSA/IChcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyh7ICdlZGl0b3ItY29udHJvbHMnOiAnJyB9KX0+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIlRpdGxlXCIgaWQ9XCJ0aXRsZVwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiVGl0bGVcIiBvbkNsaWNrPXt0aGlzLm9uVGl0bGVDbGlja30+XG4gICAgICAgICAgICAgICAgPFRpdGxlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkJvbGRcIiBpZD1cImJvbGRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkJvbGRcIiBvbkNsaWNrPXt0aGlzLm9uQm9sZENsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0Qm9sZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJJdGFsaWNcIiBpZD1cIml0YWxpY1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSXRhbGljXCIgb25DbGljaz17dGhpcy5vbkl0YWxpY0NsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0SXRhbGljIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiVW5kZXJsaW5lXCIgaWQ9XCJ1bmRlcmxpbmVcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b25cbiAgICAgICAgICAgICAgICBhcmlhLWxhYmVsPVwiVW5kZXJsaW5lXCJcbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uVW5kZXJMaW5lQ2xpY2t9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0VW5kZXJsaW5lZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJDb2RlXCIgaWQ9XCJjb2RlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJDb2RlXCIgb25DbGljaz17dGhpcy5vbkNvZGVDbGlja30+XG4gICAgICAgICAgICAgICAgPENvZGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiUXVvdGVcIiBpZD1cInF1b3RlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJRdW90ZVwiIG9uQ2xpY2s9e3RoaXMub25RdW90ZUNsaWNrfT5cbiAgICAgICAgICAgICAgICA8Rm9ybWF0UXVvdGVJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgIHRpdGxlPVwiVW5vcmRlcmVkIExpc3RcIlxuICAgICAgICAgICAgICBpZD1cInVub3JkZXJkLWxpc3RcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVbm9yZGVyZWQgTGlzdFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkxpc3RCdWxsZXRlZENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEZvcm1hdExpc3RCdWxsZXRlZEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJPcmRlcmVkIExpc3RcIiBpZD1cIm9yZGVyZWQtbGlzdFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJPcmRlcmVkIExpc3RcIlxuICAgICAgICAgICAgICAgIG9uQ2xpY2s9e3RoaXMub25MaXN0TnVtYmVyZWRDbGlja31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRMaXN0TnVtYmVyZWRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gTGVmdFwiIGlkPVwiYWxpZ24tbGVmdFwiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiQWxpZ24gTGVmdFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkxlZnRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gQ2VudGVyXCIgaWQ9XCJhbGlnbi1jZW50ZXJcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIENlbnRlclwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkNlbnRlckljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJBbGlnbiBSaWdodFwiIGlkPVwiYWxpZ24tcmlnaHRcIiBwbGFjZW1lbnQ9XCJib3R0b21cIj5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFsaWduIFJpZ2h0XCIgZGlzYWJsZWQ+XG4gICAgICAgICAgICAgICAgPEZvcm1hdEFsaWduUmlnaHRJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQWxpZ24gUmlnaHRcIiBpZD1cImFsaWduLXJpZ2h0XCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBbGlnbiBSaWdodFwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxGb3JtYXRBbGlnbkp1c3RpZnlJY29uIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwIHRpdGxlPVwiQXR0YWNoIEZpbGVcIiBpZD1cImF0dGFjaC1maWxlXCIgcGxhY2VtZW50PVwiYm90dG9tXCI+XG4gICAgICAgICAgICAgIDxJY29uQnV0dG9uIGFyaWEtbGFiZWw9XCJBdHRhY2ggRmlsZVwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxBdHRhY2hGaWxlSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgICA8VG9vbHRpcCB0aXRsZT1cIkluc2VydCBMaW5rXCIgaWQ9XCJpbnNlcnQtbGlua1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IExpbmtcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8SW5zZXJ0TGlua0ljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXAgdGl0bGU9XCJJbnNlcnQgUGhvdG9cIiBpZD1cImluc2VydC1waG90b1wiIHBsYWNlbWVudD1cImJvdHRvbVwiPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJJbnNlcnQgUGhvdG9cIlxuICAgICAgICAgICAgICAgIGRpc2FibGVkPXt0eXBlb2YgdGhpcy5wcm9wcy5hcGlQaG90b1VwbG9hZCAhPT0gJ2Z1bmN0aW9uJ31cbiAgICAgICAgICAgICAgICBvbkNsaWNrPXt0aGlzLm9uQ2xpY2tJbnNlcnRQaG90b31cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRQaG90b0ljb24gLz5cbiAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJmaWxlXCJcbiAgICAgICAgICAgICAgICAgIGFjY2VwdD1cImltYWdlL2pwZWd8cG5nfGdpZlwiXG4gICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5vbkNoYW5nZUluc2VydFBob3RvfVxuICAgICAgICAgICAgICAgICAgcmVmPXsocGhvdG8pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5waG90byA9IHBob3RvO1xuICAgICAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgICAgIHN0eWxlPXt7IGRpc3BsYXk6ICdub25lJyB9fVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvSWNvbkJ1dHRvbj5cbiAgICAgICAgICAgIDwvVG9vbHRpcD5cbiAgICAgICAgICAgIDxUb29sdGlwXG4gICAgICAgICAgICAgIHRpdGxlPVwiSW5zZXJ0IEVtb3RpY29uXCJcbiAgICAgICAgICAgICAgaWQ9XCJpbnNlcnRFLWVtb3RpY29uXCJcbiAgICAgICAgICAgICAgcGxhY2VtZW50PVwiYm90dG9tXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkluc2VydCBFbW90aWNvblwiIGRpc2FibGVkPlxuICAgICAgICAgICAgICAgIDxJbnNlcnRFbW90aWNvbkljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJJbnNlcnQgQ29tbWVudFwiXG4gICAgICAgICAgICAgIGlkPVwiaW5zZXJ0LWNvbW1lbnRcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvbiBhcmlhLWxhYmVsPVwiSW5zZXJ0IENvbW1lbnRcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8SW5zZXJ0Q29tbWVudEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJIaWdobGlnaHQgVGV4dFwiXG4gICAgICAgICAgICAgIGlkPVwiaGlnaGxpZ2h0LXRleHRcIlxuICAgICAgICAgICAgICBwbGFjZW1lbnQ9XCJib3R0b21cIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICA8SWNvbkJ1dHRvblxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJIaWdobGlnaHQgVGV4dFwiXG4gICAgICAgICAgICAgICAgb25DbGljaz17dGhpcy5vbkhpZ2hsaWdodENsaWNrfVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEhpZ2hsaWdodEljb24gLz5cbiAgICAgICAgICAgICAgPC9JY29uQnV0dG9uPlxuICAgICAgICAgICAgPC9Ub29sdGlwPlxuICAgICAgICAgICAgPFRvb2x0aXBcbiAgICAgICAgICAgICAgdGl0bGU9XCJBZGQgRnVuY3Rpb25zXCJcbiAgICAgICAgICAgICAgaWQ9XCJhZGQtZnVuY3Rpb25zXCJcbiAgICAgICAgICAgICAgcGxhY2VtZW50PVwiYm90dG9tXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgPEljb25CdXR0b24gYXJpYS1sYWJlbD1cIkFkZCBGdW5jdGlvbnNcIiBkaXNhYmxlZD5cbiAgICAgICAgICAgICAgICA8RnVuY3Rpb25zSWNvbiAvPlxuICAgICAgICAgICAgICA8L0ljb25CdXR0b24+XG4gICAgICAgICAgICA8L1Rvb2x0aXA+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICkgOiBudWxsfVxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT17Y2xhc3NOYW1lcyh7ICdlZGl0b3ItYXJlYSc6ICcnIH0pfT5cbiAgICAgICAgICA8RWRpdG9yXG4gICAgICAgICAgICAvKiBCYXNpY3MgKi9cblxuICAgICAgICAgICAgZWRpdG9yU3RhdGU9e2VkaXRvclN0YXRlfVxuICAgICAgICAgICAgb25DaGFuZ2U9e29uQ2hhbmdlfVxuICAgICAgICAgICAgLyogUHJlc2VudGF0aW9uICovXG5cbiAgICAgICAgICAgIHBsYWNlaG9sZGVyPXtwbGFjZWhvbGRlcn1cbiAgICAgICAgICAgIC8vIHRleHRBbGlnbm1lbnQ9XCJjZW50ZXJcIlxuXG4gICAgICAgICAgICAvLyB0ZXh0RGlyZWN0aW9uYWxpdHk9XCJMVFJcIlxuXG4gICAgICAgICAgICBibG9ja1JlbmRlcmVyRm49e3RoaXMuYmxvY2tSZW5kZXJlckZufVxuICAgICAgICAgICAgYmxvY2tTdHlsZUZuPXt0aGlzLmJsb2NrU3R5bGVGbn1cbiAgICAgICAgICAgIGN1c3RvbVN0eWxlTWFwPXtzdHlsZU1hcH1cbiAgICAgICAgICAgIC8vIGN1c3RvbVN0eWxlRm49eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvKiBCZWhhdmlvciAqL1xuXG4gICAgICAgICAgICAvLyBhdXRvQ2FwaXRhbGl6ZT1cInNlbnRlbmNlc1wiXG5cbiAgICAgICAgICAgIC8vIGF1dG9Db21wbGV0ZT1cIm9mZlwiXG5cbiAgICAgICAgICAgIC8vIGF1dG9Db3JyZWN0PVwib2ZmXCJcblxuICAgICAgICAgICAgcmVhZE9ubHk9e3JlYWRPbmx5fVxuICAgICAgICAgICAgc3BlbGxDaGVja1xuICAgICAgICAgICAgLy8gc3RyaXBQYXN0ZWRTdHlsZXM9e2ZhbHNlfVxuXG4gICAgICAgICAgICAvKiBET00gYW5kIEFjY2Vzc2liaWxpdHkgKi9cblxuICAgICAgICAgICAgLy8gZWRpdG9yS2V5XG5cbiAgICAgICAgICAgIC8qIENhbmNlbGFibGUgSGFuZGxlcnMgKi9cblxuICAgICAgICAgICAgLy8gaGFuZGxlUmV0dXJuPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgaGFuZGxlS2V5Q29tbWFuZD17dGhpcy5oYW5kbGVLZXlDb21tYW5kfVxuICAgICAgICAgICAgLy8gaGFuZGxlQmVmb3JlSW5wdXQ9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVQYXN0ZWRUZXh0PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8gaGFuZGxlUGFzdGVkRmlsZXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVEcm9wcGVkRmlsZXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBoYW5kbGVEcm9wPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogS2V5IEhhbmRsZXJzICovXG5cbiAgICAgICAgICAgIC8vIG9uRXNjYXBlPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgb25UYWI9e3RoaXMub25UYWJ9XG4gICAgICAgICAgICAvLyBvblVwQXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBvblJpZ2h0QXJyb3c9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBvbkRvd25BcnJvdz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uTGVmdEFycm93PXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLy8ga2V5QmluZGluZ0ZuPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogTW91c2UgRXZlbnQgKi9cblxuICAgICAgICAgICAgLy8gb25Gb2N1cz17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8vIG9uQmx1cj17KCkgPT4ge319XG5cbiAgICAgICAgICAgIC8qIE1ldGhvZHMgKi9cblxuICAgICAgICAgICAgLy8gZm9jdXM9eygpID0+IHt9fVxuXG4gICAgICAgICAgICAvLyBibHVyPXsoKSA9PiB7fX1cblxuICAgICAgICAgICAgLyogRm9yIFJlZmVyZW5jZSAqL1xuXG4gICAgICAgICAgICByZWY9eyhub2RlKSA9PiB7XG4gICAgICAgICAgICAgIHRoaXMuZWRpdG9yTm9kZSA9IG5vZGU7XG4gICAgICAgICAgICB9fVxuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgKTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCB3aXRoU3R5bGVzKHN0eWxlcykoTWF5YXNoRWRpdG9yKTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3IuanMiLCIvKiogQGZvcm1hdCAqL1xuXG5pbXBvcnQgeyBFZGl0b3JTdGF0ZSwgY29udmVydEZyb21SYXcsIENvbXBvc2l0ZURlY29yYXRvciB9IGZyb20gJ2RyYWZ0LWpzJztcblxuaW1wb3J0IHtcbiAgaGFzaHRhZ1N0cmF0ZWd5LFxuICBIYXNodGFnU3BhbixcbiAgaGFuZGxlU3RyYXRlZ3ksXG4gIEhhbmRsZVNwYW4sXG59IGZyb20gJy4vY29tcG9uZW50cy9EZWNvcmF0b3JzJztcblxuY29uc3QgZGVmYXVsdERlY29yYXRvcnMgPSBuZXcgQ29tcG9zaXRlRGVjb3JhdG9yKFtcbiAge1xuICAgIHN0cmF0ZWd5OiBoYW5kbGVTdHJhdGVneSxcbiAgICBjb21wb25lbnQ6IEhhbmRsZVNwYW4sXG4gIH0sXG4gIHtcbiAgICBzdHJhdGVneTogaGFzaHRhZ1N0cmF0ZWd5LFxuICAgIGNvbXBvbmVudDogSGFzaHRhZ1NwYW4sXG4gIH0sXG5dKTtcblxuZXhwb3J0IGNvbnN0IGNyZWF0ZUVkaXRvclN0YXRlID0gKFxuICBjb250ZW50ID0gbnVsbCxcbiAgZGVjb3JhdG9ycyA9IGRlZmF1bHREZWNvcmF0b3JzLFxuKSA9PiB7XG4gIGlmIChjb250ZW50ID09PSBudWxsKSB7XG4gICAgcmV0dXJuIEVkaXRvclN0YXRlLmNyZWF0ZUVtcHR5KGRlY29yYXRvcnMpO1xuICB9XG4gIHJldHVybiBFZGl0b3JTdGF0ZS5jcmVhdGVXaXRoQ29udGVudChjb252ZXJ0RnJvbVJhdyhjb250ZW50KSwgZGVjb3JhdG9ycyk7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBjcmVhdGVFZGl0b3JTdGF0ZTtcblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3NyYy9saWIvbWF5YXNoLWVkaXRvci9FZGl0b3JTdGF0ZS5qcyIsIi8qKlxuICogVGhpcyBmaWxlIGNvbnRhaW5zIGFsbCB0aGUgQ1NTLWluLUpTIHN0eWxlcyBvZiBFZGl0b3IgY29tcG9uZW50LlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gICdAZ2xvYmFsJzoge1xuICAgICcuUmljaEVkaXRvci1yb290Jzoge1xuICAgICAgYmFja2dyb3VuZDogJyNmZmYnLFxuICAgICAgYm9yZGVyOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgICAgZm9udEZhbWlseTogXCInR2VvcmdpYScsIHNlcmlmXCIsXG4gICAgICBmb250U2l6ZTogJzE0cHgnLFxuICAgICAgcGFkZGluZzogJzE1cHgnLFxuICAgIH0sXG4gICAgJy5SaWNoRWRpdG9yLWVkaXRvcic6IHtcbiAgICAgIGJvcmRlclRvcDogJzFweCBzb2xpZCAjZGRkJyxcbiAgICAgIGN1cnNvcjogJ3RleHQnLFxuICAgICAgZm9udFNpemU6ICcxNnB4JyxcbiAgICAgIG1hcmdpblRvcDogJzEwcHgnLFxuICAgIH0sXG4gICAgJy5wdWJsaWMtRHJhZnRFZGl0b3JQbGFjZWhvbGRlci1yb290Jzoge1xuICAgICAgbWFyZ2luOiAnMCAtMTVweCAtMTVweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgfSxcbiAgICAnLnB1YmxpYy1EcmFmdEVkaXRvci1jb250ZW50Jzoge1xuICAgICAgbWFyZ2luOiAnMCAtMTVweCAtMTVweCcsXG4gICAgICBwYWRkaW5nOiAnMTVweCcsXG4gICAgICAvLyBtaW5IZWlnaHQ6ICcxMDBweCcsXG4gICAgfSxcbiAgICAnLlJpY2hFZGl0b3ItYmxvY2txdW90ZSc6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJzVweCBzb2xpZCAjZWVlJyxcbiAgICAgIGJvcmRlckxlZnQ6ICc1cHggc29saWQgI2VlZScsXG4gICAgICBjb2xvcjogJyM2NjYnLFxuICAgICAgZm9udEZhbWlseTogXCInSG9lZmxlciBUZXh0JywgJ0dlb3JnaWEnLCBzZXJpZlwiLFxuICAgICAgZm9udFN0eWxlOiAnaXRhbGljJyxcbiAgICAgIG1hcmdpbjogJzE2cHggMCcsXG4gICAgICBwYWRkaW5nOiAnMTBweCAyMHB4JyxcbiAgICB9LFxuICAgICcucHVibGljLURyYWZ0U3R5bGVEZWZhdWx0LXByZSc6IHtcbiAgICAgIGJhY2tncm91bmRDb2xvcjogJ3JnYmEoMCwgMCwgMCwgMC4wNSknLFxuICAgICAgZm9udEZhbWlseTogXCInSW5jb25zb2xhdGEnLCAnTWVubG8nLCAnQ29uc29sYXMnLCBtb25vc3BhY2VcIixcbiAgICAgIGZvbnRTaXplOiAnMTZweCcsXG4gICAgICBwYWRkaW5nOiAnMjBweCcsXG4gICAgfSxcbiAgfSxcbiAgcm9vdDoge1xuICAgIC8vIHBhZGRpbmc6ICcxJScsXG4gIH0sXG4gIGZsZXhHcm93OiB7XG4gICAgZmxleDogJzEgMSBhdXRvJyxcbiAgfSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvRWRpdG9yU3R5bGVzLmpzIiwiLyoqXG4gKlxuICogQGZvcm1hdFxuICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBCbG9ja3MgfSBmcm9tICcuLi9jb25zdGFudHMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHBob3RvOiB7XG4gICAgd2lkdGg6ICcxMDAlJyxcbiAgICAvLyBGaXggYW4gaXNzdWUgd2l0aCBGaXJlZm94IHJlbmRlcmluZyB2aWRlbyBjb250cm9sc1xuICAgIC8vIHdpdGggJ3ByZS13cmFwJyB3aGl0ZS1zcGFjZVxuICAgIHdoaXRlU3BhY2U6ICdpbml0aWFsJyxcbiAgfSxcbn07XG5cbi8qKlxuICpcbiAqIEBjbGFzcyBBdG9taWMgLSB0aGlzIFJlYWN0IGNvbXBvbmVudCB3aWxsIGJlIHVzZWQgdG8gcmVuZGVyIEF0b21pY1xuICogY29tcG9uZW50cyBvZiBEcmFmdC5qc1xuICpcbiAqIEB0b2RvIC0gY29uZmlndXJlIHRoaXMgZm9yIGF1ZGlvLlxuICogQHRvZG8gLSBjb25maWd1cmUgdGhpcyBmb3IgdmlkZW8uXG4gKi9cbmNsYXNzIEF0b21pYyBleHRlbmRzIENvbXBvbmVudCB7XG4gIHN0YXRpYyBwcm9wVHlwZXMgPSB7XG4gICAgY29udGVudFN0YXRlOiBQcm9wVHlwZXMub2JqZWN0LmlzUmVxdWlyZWQsXG4gICAgYmxvY2s6IFByb3BUeXBlcy5hbnkuaXNSZXF1aXJlZCxcbiAgfTtcblxuICBjb25zdHJ1Y3Rvcihwcm9wcykge1xuICAgIHN1cGVyKHByb3BzKTtcbiAgICB0aGlzLnN0YXRlID0ge307XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc3QgeyBjb250ZW50U3RhdGUsIGJsb2NrIH0gPSB0aGlzLnByb3BzO1xuXG4gICAgY29uc3QgZW50aXR5ID0gY29udGVudFN0YXRlLmdldEVudGl0eShibG9jay5nZXRFbnRpdHlBdCgwKSk7XG4gICAgY29uc3QgeyBzcmMgfSA9IGVudGl0eS5nZXREYXRhKCk7XG4gICAgY29uc3QgdHlwZSA9IGVudGl0eS5nZXRUeXBlKCk7XG5cbiAgICBpZiAodHlwZSA9PT0gQmxvY2tzLlBIT1RPKSB7XG4gICAgICByZXR1cm4gKFxuICAgICAgICA8ZGl2PlxuICAgICAgICAgIDxpbWcgYWx0PXsnYWx0J30gc3JjPXtzcmN9IHN0eWxlPXtzdHlsZXMucGhvdG99IC8+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKTtcbiAgICB9XG5cbiAgICByZXR1cm4gPGRpdiAvPjtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBBdG9taWM7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9BdG9taWMuanMiLCIvKiBlc2xpbnQtZGlzYWJsZSAqL1xuLy8gZXNsaW50IGlzIGRpc2FibGVkIGhlcmUgZm9yIG5vdy5cbmltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XG5pbXBvcnQgc3R5bGUgZnJvbSAnLi4vc3R5bGUnO1xuXG5jb25zdCBIQU5ETEVfUkVHRVggPSAvXFxAW1xcd10rL2c7XG5jb25zdCBIQVNIVEFHX1JFR0VYID0gL1xcI1tcXHdcXHUwNTkwLVxcdTA1ZmZdKy9nO1xuXG5mdW5jdGlvbiBmaW5kV2l0aFJlZ2V4KHJlZ2V4LCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKSB7XG4gIGNvbnN0IHRleHQgPSBjb250ZW50QmxvY2suZ2V0VGV4dCgpO1xuICBsZXQgbWF0Y2hBcnIsXG4gICAgc3RhcnQ7XG4gIHdoaWxlICgobWF0Y2hBcnIgPSByZWdleC5leGVjKHRleHQpKSAhPT0gbnVsbCkge1xuICAgIHN0YXJ0ID0gbWF0Y2hBcnIuaW5kZXg7XG4gICAgY2FsbGJhY2soc3RhcnQsIHN0YXJ0ICsgbWF0Y2hBcnJbMF0ubGVuZ3RoKTtcbiAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gaGFuZGxlU3RyYXRlZ3koY29udGVudEJsb2NrLCBjYWxsYmFjaywgY29udGVudFN0YXRlKSB7XG4gIGZpbmRXaXRoUmVnZXgoSEFORExFX1JFR0VYLCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGhhc2h0YWdTdHJhdGVneShjb250ZW50QmxvY2ssIGNhbGxiYWNrLCBjb250ZW50U3RhdGUpIHtcbiAgZmluZFdpdGhSZWdleChIQVNIVEFHX1JFR0VYLCBjb250ZW50QmxvY2ssIGNhbGxiYWNrKTtcbn1cblxuZXhwb3J0IGNvbnN0IEhhbmRsZVNwYW4gPSBwcm9wcyA9PiA8c3BhbiBzdHlsZT17c3R5bGUuaGFuZGxlfT57cHJvcHMuY2hpbGRyZW59PC9zcGFuPjtcblxuZXhwb3J0IGNvbnN0IEhhc2h0YWdTcGFuID0gcHJvcHMgPT4gPHNwYW4gc3R5bGU9e3N0eWxlLmhhc2h0YWd9Pntwcm9wcy5jaGlsZHJlbn08L3NwYW4+O1xuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaGFuZGxlU3RyYXRlZ3ksXG4gIEhhbmRsZVNwYW4sXG5cbiAgaGFzaHRhZ1N0cmF0ZWd5LFxuICBIYXNodGFnU3Bhbixcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29tcG9uZW50cy9EZWNvcmF0b3JzLmpzIiwiLyoqXG4gKiBTb21lIG9mIHRoZSBjb25zdGFudHMgd2hpY2ggYXJlIHVzZWQgdGhyb3VnaG91dCB0aGlzIHByb2plY3QgaW5zdGVhZCBvZlxuICogZGlyZWN0bHkgdXNpbmcgc3RyaW5nLlxuICpcbiAqIEBmb3JtYXRcbiAqL1xuXG4vKipcbiAqIEBjb25zdGFudCBCbG9ja3NcbiAqL1xuZXhwb3J0IGNvbnN0IEJsb2NrcyA9IHtcbiAgVU5TVFlMRUQ6ICd1bnN0eWxlZCcsXG4gIFBBUkFHUkFQSDogJ3Vuc3R5bGVkJyxcblxuICBIMTogJ2hlYWRlci1vbmUnLFxuICBIMjogJ2hlYWRlci10d28nLFxuICBIMzogJ2hlYWRlci10aHJlZScsXG4gIEg0OiAnaGVhZGVyLWZvdXInLFxuICBINTogJ2hlYWRlci1maXZlJyxcbiAgSDY6ICdoZWFkZXItc2l4JyxcblxuICBPTDogJ29yZGVyZWQtbGlzdC1pdGVtJyxcbiAgVUw6ICd1bm9yZGVyZWQtbGlzdC1pdGVtJyxcblxuICBDT0RFOiAnY29kZS1ibG9jaycsXG5cbiAgQkxPQ0tRVU9URTogJ2Jsb2NrcXVvdGUnLFxuXG4gIEFUT01JQzogJ2F0b21pYycsXG4gIFBIT1RPOiAnYXRvbWljOnBob3RvJyxcbiAgVklERU86ICdhdG9taWM6dmlkZW8nLFxufTtcblxuLyoqXG4gKiBAY29uc3RhbnQgSW5saW5lXG4gKi9cbmV4cG9ydCBjb25zdCBJbmxpbmUgPSB7XG4gIEJPTEQ6ICdCT0xEJyxcbiAgQ09ERTogJ0NPREUnLFxuICBJVEFMSUM6ICdJVEFMSUMnLFxuICBTVFJJS0VUSFJPVUdIOiAnU1RSSUtFVEhST1VHSCcsXG4gIFVOREVSTElORTogJ1VOREVSTElORScsXG4gIEhJR0hMSUdIVDogJ0hJR0hMSUdIVCcsXG59O1xuXG4vKipcbiAqIEBjb25zdGFudCBFbnRpdHlcbiAqL1xuZXhwb3J0IGNvbnN0IEVudGl0eSA9IHtcbiAgTElOSzogJ0xJTksnLFxufTtcblxuLyoqXG4gKiBAY29uc3RhbnQgSFlQRVJMSU5LXG4gKi9cbmV4cG9ydCBjb25zdCBIWVBFUkxJTksgPSAnaHlwZXJsaW5rJztcblxuLyoqXG4gKiBDb25zdGFudHMgdG8gaGFuZGxlIGtleSBjb21tYW5kc1xuICovXG5leHBvcnQgY29uc3QgSEFORExFRCA9ICdoYW5kbGVkJztcbmV4cG9ydCBjb25zdCBOT1RfSEFORExFRCA9ICdub3RfaGFuZGxlZCc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgQmxvY2tzLFxuICBJbmxpbmUsXG4gIEVudGl0eSxcbn07XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3IvY29uc3RhbnRzLmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuaW1wb3J0IEVkaXRvciBmcm9tICcuL0VkaXRvcic7XG5cbmV4cG9ydCB7IE1heWFzaEVkaXRvciB9IGZyb20gJy4vRWRpdG9yJztcblxuZXhwb3J0IHsgY3JlYXRlRWRpdG9yU3RhdGUgfSBmcm9tICcuL0VkaXRvclN0YXRlJztcblxuZXhwb3J0IGRlZmF1bHQgRWRpdG9yO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2xpYi9tYXlhc2gtZWRpdG9yL2luZGV4LmpzIiwiLyoqIEBmb3JtYXQgKi9cblxuZXhwb3J0IGNvbnN0IHN0eWxlID0ge1xuICByb290OiB7XG4gICAgcGFkZGluZzogMjAsXG4gICAgd2lkdGg6IDYwMCxcbiAgfSxcbiAgZWRpdG9yOiB7XG4gICAgYm9yZGVyOiAnMXB4IHNvbGlkICNkZGQnLFxuICAgIGN1cnNvcjogJ3RleHQnLFxuICAgIGZvbnRTaXplOiAxNixcbiAgICBtaW5IZWlnaHQ6IDQwLFxuICAgIHBhZGRpbmc6IDEwLFxuICB9LFxuICBidXR0b246IHtcbiAgICBtYXJnaW5Ub3A6IDEwLFxuICAgIHRleHRBbGlnbjogJ2NlbnRlcicsXG4gIH0sXG4gIGhhbmRsZToge1xuICAgIGNvbG9yOiAncmdiYSg5OCwgMTc3LCAyNTQsIDEuMCknLFxuICAgIGRpcmVjdGlvbjogJ2x0cicsXG4gICAgdW5pY29kZUJpZGk6ICdiaWRpLW92ZXJyaWRlJyxcbiAgfSxcbiAgaGFzaHRhZzoge1xuICAgIGNvbG9yOiAncmdiYSg5NSwgMTg0LCAxMzgsIDEuMCknLFxuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGU7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvbGliL21heWFzaC1lZGl0b3Ivc3R5bGUvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9