webpackJsonp([73],{

/***/ "./src/client/actions/courses/getAll.js":
/*!**********************************************!*\
  !*** ./src/client/actions/courses/getAll.js ***!
  \**********************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAll = undefined;

var _courses = __webpack_require__(/*! ../../constants/courses */ "./src/client/constants/courses.js");

/**
 * This action will update courses reducer
 * @function getAll
 * @param {Object} payload
 * @param {Object} payload.statusCode -
 * @param {Object[]} payload.courses
 * @param {Object} payload.courses[]
 * @param {number} payload.courses[].courseId
 * @param {number} payload.courses[].authorId
 * @param {string} payload.courses[].title
 * @param {string} payload.courses[].description
 * @param {Object} payload.courses[].syllabus
 * @param {Object[]} payload.courses[].courseModules
 * @param {Object} payload.courses[].courseModules[]
 * @param {number} payload.courses[].courseModules[].moduleId
 * @param {string} payload.courses[].timestamp
 *
 * @returns {Object}
 */
var getAll = exports.getAll = function getAll(payload) {
  return {
    type: _courses.COURSES_GET,
    payload: payload
  };
}; /**
    * @format
    * 
    */

exports.default = getAll;

/***/ }),

/***/ "./src/client/api/courses/users/getAll.js":
/*!************************************************!*\
  !*** ./src/client/api/courses/users/getAll.js ***!
  \************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = __webpack_require__(/*! babel-runtime/helpers/extends */ "./node_modules/babel-runtime/helpers/extends.js");

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

/**
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
/**
 * @format
 * 
 */

var getAll = function () {
  var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(_ref2) {
    var token = _ref2.token,
        userId = _ref2.userId;
    var url, res, status, statusText, json;
    return _regenerator2.default.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            url = _config.HOST + '/api/users/' + userId + '/courses';
            _context.next = 4;
            return (0, _isomorphicFetch2.default)(url, {
              method: 'GET',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: token
              }
            });

          case 4:
            res = _context.sent;
            status = res.status, statusText = res.statusText;

            if (!(status >= 300)) {
              _context.next = 8;
              break;
            }

            return _context.abrupt('return', {
              statusCode: status,
              error: statusText
            });

          case 8:
            _context.next = 10;
            return res.json();

          case 10:
            json = _context.sent;
            return _context.abrupt('return', (0, _extends3.default)({}, json));

          case 14:
            _context.prev = 14;
            _context.t0 = _context['catch'](0);

            console.error(_context.t0);

            return _context.abrupt('return', {
              statusCode: 400,
              error: 'Something went wrong, please try again...'
            });

          case 18:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, this, [[0, 14]]);
  }));

  return function getAll(_x) {
    return _ref.apply(this, arguments);
  };
}();

var _isomorphicFetch = __webpack_require__(/*! isomorphic-fetch */ "./node_modules/isomorphic-fetch/fetch-npm-browserify.js");

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _config = __webpack_require__(/*! ../../config */ "./src/client/api/config.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = getAll;

/***/ }),

/***/ "./src/client/containers/CourseTimeline/index.js":
/*!*******************************************************!*\
  !*** ./src/client/containers/CourseTimeline/index.js ***!
  \*******************************************************/
/*! dynamic exports provided */
/*! all exports used */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _regenerator = __webpack_require__(/*! babel-runtime/regenerator */ "./node_modules/babel-runtime/regenerator/index.js");

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = __webpack_require__(/*! babel-runtime/helpers/asyncToGenerator */ "./node_modules/babel-runtime/helpers/asyncToGenerator.js");

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _getPrototypeOf = __webpack_require__(/*! babel-runtime/core-js/object/get-prototype-of */ "./node_modules/babel-runtime/core-js/object/get-prototype-of.js");

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = __webpack_require__(/*! babel-runtime/helpers/classCallCheck */ "./node_modules/babel-runtime/helpers/classCallCheck.js");

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = __webpack_require__(/*! babel-runtime/helpers/createClass */ "./node_modules/babel-runtime/helpers/createClass.js");

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = __webpack_require__(/*! babel-runtime/helpers/possibleConstructorReturn */ "./node_modules/babel-runtime/helpers/possibleConstructorReturn.js");

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = __webpack_require__(/*! babel-runtime/helpers/inherits */ "./node_modules/babel-runtime/helpers/inherits.js");

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = __webpack_require__(/*! react */ "./node_modules/react/react.js");

var _react2 = _interopRequireDefault(_react);

var _propTypes = __webpack_require__(/*! prop-types */ "./node_modules/prop-types/index.js");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _redux = __webpack_require__(/*! redux */ "./node_modules/redux/es/index.js");

var _reactRedux = __webpack_require__(/*! react-redux */ "./node_modules/react-redux/es/index.js");

var _withStyles = __webpack_require__(/*! material-ui/styles/withStyles */ "./node_modules/material-ui/styles/withStyles.js");

var _withStyles2 = _interopRequireDefault(_withStyles);

var _Grid = __webpack_require__(/*! material-ui/Grid */ "./node_modules/material-ui/Grid/index.js");

var _Grid2 = _interopRequireDefault(_Grid);

var _reactLoadable = __webpack_require__(/*! react-loadable */ "./node_modules/react-loadable/lib/index.js");

var _reactLoadable2 = _interopRequireDefault(_reactLoadable);

var _Loading = __webpack_require__(/*! ../../components/Loading */ "./src/client/components/Loading.js");

var _Loading2 = _interopRequireDefault(_Loading);

var _getAll = __webpack_require__(/*! ../../api/courses/users/getAll */ "./src/client/api/courses/users/getAll.js");

var _getAll2 = _interopRequireDefault(_getAll);

var _getAll3 = __webpack_require__(/*! ../../actions/courses/getAll */ "./src/client/actions/courses/getAll.js");

var _getAll4 = _interopRequireDefault(_getAll3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import actions from '../../actions';

/** @format */

var styles = {
  root: {}
};

var AsyncCourseCreate = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(61).then(__webpack_require__.bind(null, /*! ../CourseCreate */ "./src/client/containers/CourseCreate/index.js"));
  },
  modules: ['../CourseCreate'],
  loading: _Loading2.default
});

var AsyncCourse = (0, _reactLoadable2.default)({
  loader: function loader() {
    return __webpack_require__.e/* import() */(40).then(__webpack_require__.bind(null, /*! ../Course */ "./src/client/containers/Course/index.js"));
  },
  modules: ['../Course'],
  loading: _Loading2.default
});

var CourseTimeline = function (_Component) {
  (0, _inherits3.default)(CourseTimeline, _Component);

  function CourseTimeline(props) {
    (0, _classCallCheck3.default)(this, CourseTimeline);

    var _this = (0, _possibleConstructorReturn3.default)(this, (CourseTimeline.__proto__ || (0, _getPrototypeOf2.default)(CourseTimeline)).call(this, props));

    _this.state = {};
    return _this;
  }

  (0, _createClass3.default)(CourseTimeline, [{
    key: 'componentDidMount',
    value: function () {
      var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
        var _props, elements, element, _elements$user, userId, token, id, elementType, _ref2, statusCode, error, payload, next;

        return _regenerator2.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _props = this.props, elements = _props.elements, element = _props.element;
                _elements$user = elements.user, userId = _elements$user.id, token = _elements$user.token;
                id = element.id, elementType = element.elementType;
                _context.next = 6;
                return (0, _getAll2.default)({
                  token: token,
                  userId: userId
                });

              case 6:
                _ref2 = _context.sent;
                statusCode = _ref2.statusCode;
                error = _ref2.error;
                payload = _ref2.payload;
                next = _ref2.next;

                if (!(statusCode >= 300)) {
                  _context.next = 14;
                  break;
                }

                // handle error
                console.error(error);
                return _context.abrupt('return');

              case 14:

                this.props.actionCoursesGet({
                  id: userId,
                  statusCode: statusCode,
                  courses: payload
                });
                _context.next = 20;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context['catch'](0);

                console.error(_context.t0);

              case 20:
              case 'end':
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 17]]);
      }));

      function componentDidMount() {
        return _ref.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: 'render',
    value: function render() {
      var _props2 = this.props,
          classes = _props2.classes,
          elements = _props2.elements,
          element = _props2.element;
      var _elements$user2 = elements.user,
          isSignedIn = _elements$user2.isSignedIn,
          userId = _elements$user2.id;
      var id = element.id,
          courses = element.courses;


      return _react2.default.createElement(
        _Grid2.default,
        { container: true, justify: 'center', spacing: 0, className: classes.root },
        _react2.default.createElement(
          _Grid2.default,
          { item: true, xs: 12, sm: 9, md: 7, lg: 6, xl: 6 },
          isSignedIn && userId === id ? _react2.default.createElement(AsyncCourseCreate, null) : null,
          typeof courses !== 'undefined' && courses.map(function (_ref3) {
            var courseId = _ref3.courseId;
            return _react2.default.createElement(AsyncCourse, { key: courseId, courseId: courseId });
          })
        )
      );
    }
  }]);
  return CourseTimeline;
}(_react.Component);

CourseTimeline.propTypes = {
  classes: _propTypes2.default.object.isRequired,
  elements: _propTypes2.default.object.isRequired,
  element: _propTypes2.default.shape({
    id: _propTypes2.default.number,
    username: _propTypes2.default.string,
    courses: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      postId: _propTypes2.default.number
    }))
  }),
  actionCoursesGet: _propTypes2.default.func.isRequired
};

var mapStateToProps = function mapStateToProps(_ref4) {
  var elements = _ref4.elements,
      courses = _ref4.courses;
  return { elements: elements, courses: courses };
};
var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({
    actionCoursesGet: _getAll4.default
  }, dispatch);
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _withStyles2.default)(styles)(CourseTimeline));

/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY2xpZW50L2FjdGlvbnMvY291cnNlcy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9hcGkvY291cnNlcy91c2Vycy9nZXRBbGwuanMiLCJ3ZWJwYWNrOi8vLy4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVRpbWVsaW5lL2luZGV4LmpzIl0sIm5hbWVzIjpbImdldEFsbCIsInBheWxvYWQiLCJ0eXBlIiwidG9rZW4iLCJ1c2VySWQiLCJ1cmwiLCJtZXRob2QiLCJoZWFkZXJzIiwiQWNjZXB0IiwiQXV0aG9yaXphdGlvbiIsInJlcyIsInN0YXR1cyIsInN0YXR1c1RleHQiLCJzdGF0dXNDb2RlIiwiZXJyb3IiLCJqc29uIiwiY29uc29sZSIsInN0eWxlcyIsInJvb3QiLCJBc3luY0NvdXJzZUNyZWF0ZSIsImxvYWRlciIsIm1vZHVsZXMiLCJsb2FkaW5nIiwiQXN5bmNDb3Vyc2UiLCJDb3Vyc2VUaW1lbGluZSIsInByb3BzIiwic3RhdGUiLCJlbGVtZW50cyIsImVsZW1lbnQiLCJ1c2VyIiwiaWQiLCJlbGVtZW50VHlwZSIsIm5leHQiLCJhY3Rpb25Db3Vyc2VzR2V0IiwiY291cnNlcyIsImNsYXNzZXMiLCJpc1NpZ25lZEluIiwibWFwIiwiY291cnNlSWQiLCJwcm9wVHlwZXMiLCJvYmplY3QiLCJpc1JlcXVpcmVkIiwic2hhcGUiLCJudW1iZXIiLCJ1c2VybmFtZSIsInN0cmluZyIsImFycmF5T2YiLCJwb3N0SWQiLCJmdW5jIiwibWFwU3RhdGVUb1Byb3BzIiwibWFwRGlzcGF0Y2hUb1Byb3BzIiwiZGlzcGF0Y2giXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUtBOztBQXdCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CTyxJQUFNQSwwQkFBUyxTQUFUQSxNQUFTLENBQUNDLE9BQUQ7QUFBQSxTQUErQjtBQUNuREMsOEJBRG1EO0FBRW5ERDtBQUZtRCxHQUEvQjtBQUFBLENBQWYsQyxDQWhEUDs7Ozs7a0JBcURlRCxNOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeENmOzs7Ozs7Ozs7O0FBYkE7Ozs7OztzRkF1QkE7QUFBQSxRQUF3QkcsS0FBeEIsU0FBd0JBLEtBQXhCO0FBQUEsUUFBK0JDLE1BQS9CLFNBQStCQSxNQUEvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUVVQyxlQUZWLGtDQUVxQ0QsTUFGckM7QUFBQTtBQUFBLG1CQUlzQiwrQkFBTUMsR0FBTixFQUFXO0FBQzNCQyxzQkFBUSxLQURtQjtBQUUzQkMsdUJBQVM7QUFDUEMsd0JBQVEsa0JBREQ7QUFFUCxnQ0FBZ0Isa0JBRlQ7QUFHUEMsK0JBQWVOO0FBSFI7QUFGa0IsYUFBWCxDQUp0Qjs7QUFBQTtBQUlVTyxlQUpWO0FBYVlDLGtCQWJaLEdBYW1DRCxHQWJuQyxDQWFZQyxNQWJaLEVBYW9CQyxVQWJwQixHQWFtQ0YsR0FibkMsQ0Fhb0JFLFVBYnBCOztBQUFBLGtCQWVRRCxVQUFVLEdBZmxCO0FBQUE7QUFBQTtBQUFBOztBQUFBLDZDQWdCYTtBQUNMRSwwQkFBWUYsTUFEUDtBQUVMRyxxQkFBT0Y7QUFGRixhQWhCYjs7QUFBQTtBQUFBO0FBQUEsbUJBc0J1QkYsSUFBSUssSUFBSixFQXRCdkI7O0FBQUE7QUFzQlVBLGdCQXRCVjtBQUFBLHdFQXdCZ0JBLElBeEJoQjs7QUFBQTtBQUFBO0FBQUE7O0FBMEJJQyxvQkFBUUYsS0FBUjs7QUExQkosNkNBNEJXO0FBQ0xELDBCQUFZLEdBRFA7QUFFTEMscUJBQU87QUFGRixhQTVCWDs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHOztrQkFBZWQsTTs7Ozs7QUFsQmY7Ozs7QUFDQTs7OztrQkFvRGVBLE07Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDeERmOzs7O0FBQ0E7Ozs7QUFFQTs7QUFDQTs7QUFFQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFFQTs7OztBQUVBOzs7O0FBQ0E7Ozs7OztBQUVBOztBQWpCQTs7QUFtQkEsSUFBTWlCLFNBQVM7QUFDYkMsUUFBTTtBQURPLENBQWY7O0FBSUEsSUFBTUMsb0JBQW9CLDZCQUFTO0FBQ2pDQyxVQUFRO0FBQUEsV0FBTSxvSkFBTjtBQUFBLEdBRHlCO0FBRWpDQyxXQUFTLENBQUMsaUJBQUQsQ0FGd0I7QUFHakNDO0FBSGlDLENBQVQsQ0FBMUI7O0FBTUEsSUFBTUMsY0FBYyw2QkFBUztBQUMzQkgsVUFBUTtBQUFBLFdBQU0sd0lBQU47QUFBQSxHQURtQjtBQUUzQkMsV0FBUyxDQUFDLFdBQUQsQ0FGa0I7QUFHM0JDO0FBSDJCLENBQVQsQ0FBcEI7O0lBTU1FLGM7OztBQUNKLDBCQUFZQyxLQUFaLEVBQW1CO0FBQUE7O0FBQUEsc0pBQ1hBLEtBRFc7O0FBRWpCLFVBQUtDLEtBQUwsR0FBYSxFQUFiO0FBRmlCO0FBR2xCOzs7Ozs7Ozs7Ozs7O3lCQUlpQyxLQUFLRCxLLEVBQTNCRSxRLFVBQUFBLFEsRUFBVUMsTyxVQUFBQSxPO2lDQUNZRCxTQUFTRSxJLEVBQTNCekIsTSxrQkFBSjBCLEUsRUFBWTNCLEssa0JBQUFBLEs7QUFDWjJCLGtCLEdBQW9CRixPLENBQXBCRSxFLEVBQUlDLFcsR0FBZ0JILE8sQ0FBaEJHLFc7O3VCQUV1QyxzQkFBa0I7QUFDbkU1Qiw4QkFEbUU7QUFFbkVDO0FBRm1FLGlCQUFsQixDOzs7O0FBQTNDUywwQixTQUFBQSxVO0FBQVlDLHFCLFNBQUFBLEs7QUFBT2IsdUIsU0FBQUEsTztBQUFTK0Isb0IsU0FBQUEsSTs7c0JBS2hDbkIsY0FBYyxHOzs7OztBQUNoQjtBQUNBRyx3QkFBUUYsS0FBUixDQUFjQSxLQUFkOzs7OztBQUlGLHFCQUFLVyxLQUFMLENBQVdRLGdCQUFYLENBQTRCO0FBQzFCSCxzQkFBSTFCLE1BRHNCO0FBRTFCUyx3Q0FGMEI7QUFHMUJxQiwyQkFBU2pDO0FBSGlCLGlCQUE1Qjs7Ozs7Ozs7QUFNQWUsd0JBQVFGLEtBQVI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs2QkFJSztBQUFBLG9CQUNnQyxLQUFLVyxLQURyQztBQUFBLFVBQ0NVLE9BREQsV0FDQ0EsT0FERDtBQUFBLFVBQ1VSLFFBRFYsV0FDVUEsUUFEVjtBQUFBLFVBQ29CQyxPQURwQixXQUNvQkEsT0FEcEI7QUFBQSw0QkFFNEJELFNBQVNFLElBRnJDO0FBQUEsVUFFQ08sVUFGRCxtQkFFQ0EsVUFGRDtBQUFBLFVBRWlCaEMsTUFGakIsbUJBRWEwQixFQUZiO0FBQUEsVUFHQ0EsRUFIRCxHQUdpQkYsT0FIakIsQ0FHQ0UsRUFIRDtBQUFBLFVBR0tJLE9BSEwsR0FHaUJOLE9BSGpCLENBR0tNLE9BSEw7OztBQUtQLGFBQ0U7QUFBQTtBQUFBLFVBQU0sZUFBTixFQUFnQixTQUFRLFFBQXhCLEVBQWlDLFNBQVMsQ0FBMUMsRUFBNkMsV0FBV0MsUUFBUWpCLElBQWhFO0FBQ0U7QUFBQTtBQUFBLFlBQU0sVUFBTixFQUFXLElBQUksRUFBZixFQUFtQixJQUFJLENBQXZCLEVBQTBCLElBQUksQ0FBOUIsRUFBaUMsSUFBSSxDQUFyQyxFQUF3QyxJQUFJLENBQTVDO0FBQ0drQix3QkFBY2hDLFdBQVcwQixFQUF6QixHQUE4Qiw4QkFBQyxpQkFBRCxPQUE5QixHQUFzRCxJQUR6RDtBQUVHLGlCQUFPSSxPQUFQLEtBQW1CLFdBQW5CLElBQ0NBLFFBQVFHLEdBQVIsQ0FBWTtBQUFBLGdCQUFHQyxRQUFILFNBQUdBLFFBQUg7QUFBQSxtQkFDViw4QkFBQyxXQUFELElBQWEsS0FBS0EsUUFBbEIsRUFBNEIsVUFBVUEsUUFBdEMsR0FEVTtBQUFBLFdBQVo7QUFISjtBQURGLE9BREY7QUFXRDs7Ozs7QUFHSGQsZUFBZWUsU0FBZixHQUEyQjtBQUN6QkosV0FBUyxvQkFBVUssTUFBVixDQUFpQkMsVUFERDtBQUV6QmQsWUFBVSxvQkFBVWEsTUFBVixDQUFpQkMsVUFGRjtBQUd6QmIsV0FBUyxvQkFBVWMsS0FBVixDQUFnQjtBQUN2QlosUUFBSSxvQkFBVWEsTUFEUztBQUV2QkMsY0FBVSxvQkFBVUMsTUFGRztBQUd2QlgsYUFBUyxvQkFBVVksT0FBVixDQUNQLG9CQUFVSixLQUFWLENBQWdCO0FBQ2RLLGNBQVEsb0JBQVVKO0FBREosS0FBaEIsQ0FETztBQUhjLEdBQWhCLENBSGdCO0FBWXpCVixvQkFBa0Isb0JBQVVlLElBQVYsQ0FBZVA7QUFaUixDQUEzQjs7QUFlQSxJQUFNUSxrQkFBa0IsU0FBbEJBLGVBQWtCO0FBQUEsTUFBR3RCLFFBQUgsU0FBR0EsUUFBSDtBQUFBLE1BQWFPLE9BQWIsU0FBYUEsT0FBYjtBQUFBLFNBQTRCLEVBQUVQLGtCQUFGLEVBQVlPLGdCQUFaLEVBQTVCO0FBQUEsQ0FBeEI7QUFDQSxJQUFNZ0IscUJBQXFCLFNBQXJCQSxrQkFBcUIsQ0FBQ0MsUUFBRDtBQUFBLFNBQ3pCLCtCQUNFO0FBQ0VsQjtBQURGLEdBREYsRUFJRWtCLFFBSkYsQ0FEeUI7QUFBQSxDQUEzQjs7a0JBUWUseUJBQVFGLGVBQVIsRUFBeUJDLGtCQUF6QixFQUNiLDBCQUFXakMsTUFBWCxFQUFtQk8sY0FBbkIsQ0FEYSxDIiwiZmlsZSI6IjczLmJ1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGZvcm1hdFxuICogQGZsb3dcbiAqL1xuXG5pbXBvcnQgeyBDT1VSU0VTX0dFVCB9IGZyb20gJy4uLy4uL2NvbnN0YW50cy9jb3Vyc2VzJztcblxudHlwZSBDb3Vyc2UgPSB7XG4gIGNvdXJzZUlkOiBudW1iZXIsXG4gIGF1dGhvcklkOiBudW1iZXIsXG4gIHRpdGxlOiBzdHJpbmcsXG4gIGRlc2NyaXB0aW9uPzogc3RyaW5nLFxuICBzeWxsYWJ1cz86IG1peGVkLFxuICBjb3Vyc2VNb2R1bGVzPzogQXJyYXk8e1xuICAgIG1vZHVsZUlkOiBudW1iZXIsXG4gIH0+LFxuICB0aW1lc3RhbXA6IHN0cmluZyxcbn07XG5cbnR5cGUgUGF5bG9hZCA9IHtcbiAgc3RhdHVzQ29kZTogbnVtYmVyLFxuICBjb3Vyc2VzOiBBcnJheTxDb3Vyc2U+LFxufTtcblxudHlwZSBBY3Rpb24gPSB7XG4gIHR5cGU6IHN0cmluZyxcbiAgcGF5bG9hZDogUGF5bG9hZCxcbn07XG5cbi8qKlxuICogVGhpcyBhY3Rpb24gd2lsbCB1cGRhdGUgY291cnNlcyByZWR1Y2VyXG4gKiBAZnVuY3Rpb24gZ2V0QWxsXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZFxuICogQHBhcmFtIHtPYmplY3R9IHBheWxvYWQuc3RhdHVzQ29kZSAtXG4gKiBAcGFyYW0ge09iamVjdFtdfSBwYXlsb2FkLmNvdXJzZXNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmNvdXJzZXNbXVxuICogQHBhcmFtIHtudW1iZXJ9IHBheWxvYWQuY291cnNlc1tdLmNvdXJzZUlkXG4gKiBAcGFyYW0ge251bWJlcn0gcGF5bG9hZC5jb3Vyc2VzW10uYXV0aG9ySWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNvdXJzZXNbXS50aXRsZVxuICogQHBhcmFtIHtzdHJpbmd9IHBheWxvYWQuY291cnNlc1tdLmRlc2NyaXB0aW9uXG4gKiBAcGFyYW0ge09iamVjdH0gcGF5bG9hZC5jb3Vyc2VzW10uc3lsbGFidXNcbiAqIEBwYXJhbSB7T2JqZWN0W119IHBheWxvYWQuY291cnNlc1tdLmNvdXJzZU1vZHVsZXNcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkLmNvdXJzZXNbXS5jb3Vyc2VNb2R1bGVzW11cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLmNvdXJzZXNbXS5jb3Vyc2VNb2R1bGVzW10ubW9kdWxlSWRcbiAqIEBwYXJhbSB7c3RyaW5nfSBwYXlsb2FkLmNvdXJzZXNbXS50aW1lc3RhbXBcbiAqXG4gKiBAcmV0dXJucyB7T2JqZWN0fVxuICovXG5leHBvcnQgY29uc3QgZ2V0QWxsID0gKHBheWxvYWQ6IFBheWxvYWQpOiBBY3Rpb24gPT4gKHtcbiAgdHlwZTogQ09VUlNFU19HRVQsXG4gIHBheWxvYWQsXG59KTtcblxuZXhwb3J0IGRlZmF1bHQgZ2V0QWxsO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9hY3Rpb25zL2NvdXJzZXMvZ2V0QWxsLmpzIiwiLyoqXG4gKiBAZm9ybWF0XG4gKiBAZmxvd1xuICovXG5cbmltcG9ydCBmZXRjaCBmcm9tICdpc29tb3JwaGljLWZldGNoJztcbmltcG9ydCB7IEhPU1QgfSBmcm9tICcuLi8uLi9jb25maWcnO1xuXG50eXBlIFBheWxvYWQgPSB7fFxuICB0b2tlbj86IHN0cmluZyxcbiAgdXNlcklkOiBudW1iZXIsXG58fTtcblxuLyoqXG4gKiBAYXN5bmNcbiAqIEBmdW5jdGlvbiBnZXRBbGxcbiAqIEBwYXJhbSB7T2JqZWN0fSBwYXlsb2FkIC1cbiAqIEBwYXJhbSB7bnVtYmVyfSBwYXlsb2FkLnVzZXJJZCAtXG4gKiBAcGFyYW0ge3N0cmluZ30gcGF5bG9hZC50b2tlbiAtXG4gKiBAcmV0dXJuIHtQcm9taXNlfSAtXG4gKlxuICogQGV4YW1wbGVcbiAqL1xuYXN5bmMgZnVuY3Rpb24gZ2V0QWxsKHsgdG9rZW4sIHVzZXJJZCB9OiBQYXlsb2FkKSB7XG4gIHRyeSB7XG4gICAgY29uc3QgdXJsID0gYCR7SE9TVH0vYXBpL3VzZXJzLyR7dXNlcklkfS9jb3Vyc2VzYDtcblxuICAgIGNvbnN0IHJlcyA9IGF3YWl0IGZldGNoKHVybCwge1xuICAgICAgbWV0aG9kOiAnR0VUJyxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgQWNjZXB0OiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgICdDb250ZW50LVR5cGUnOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICAgIEF1dGhvcml6YXRpb246IHRva2VuLFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIGNvbnN0IHsgc3RhdHVzLCBzdGF0dXNUZXh0IH0gPSByZXM7XG5cbiAgICBpZiAoc3RhdHVzID49IDMwMCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3RhdHVzQ29kZTogc3RhdHVzLFxuICAgICAgICBlcnJvcjogc3RhdHVzVGV4dCxcbiAgICAgIH07XG4gICAgfVxuXG4gICAgY29uc3QganNvbiA9IGF3YWl0IHJlcy5qc29uKCk7XG5cbiAgICByZXR1cm4geyAuLi5qc29uIH07XG4gIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICByZXR1cm4ge1xuICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgZXJyb3I6ICdTb21ldGhpbmcgd2VudCB3cm9uZywgcGxlYXNlIHRyeSBhZ2Fpbi4uLicsXG4gICAgfTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBnZXRBbGw7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvY2xpZW50L2FwaS9jb3Vyc2VzL3VzZXJzL2dldEFsbC5qcyIsIi8qKiBAZm9ybWF0ICovXG5cbmltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gJ3Byb3AtdHlwZXMnO1xuXG5pbXBvcnQgeyBiaW5kQWN0aW9uQ3JlYXRvcnMgfSBmcm9tICdyZWR1eCc7XG5pbXBvcnQgeyBjb25uZWN0IH0gZnJvbSAncmVhY3QtcmVkdXgnO1xuXG5pbXBvcnQgd2l0aFN0eWxlcyBmcm9tICdtYXRlcmlhbC11aS9zdHlsZXMvd2l0aFN0eWxlcyc7XG5pbXBvcnQgR3JpZCBmcm9tICdtYXRlcmlhbC11aS9HcmlkJztcbmltcG9ydCBMb2FkYWJsZSBmcm9tICdyZWFjdC1sb2FkYWJsZSc7XG5cbmltcG9ydCBMb2FkaW5nIGZyb20gJy4uLy4uL2NvbXBvbmVudHMvTG9hZGluZyc7XG5cbmltcG9ydCBhcGlVc2VyQ291cnNlc0dldCBmcm9tICcuLi8uLi9hcGkvY291cnNlcy91c2Vycy9nZXRBbGwnO1xuaW1wb3J0IGFjdGlvbkNvdXJzZXNHZXQgZnJvbSAnLi4vLi4vYWN0aW9ucy9jb3Vyc2VzL2dldEFsbCc7XG5cbi8vIGltcG9ydCBhY3Rpb25zIGZyb20gJy4uLy4uL2FjdGlvbnMnO1xuXG5jb25zdCBzdHlsZXMgPSB7XG4gIHJvb3Q6IHt9LFxufTtcblxuY29uc3QgQXN5bmNDb3Vyc2VDcmVhdGUgPSBMb2FkYWJsZSh7XG4gIGxvYWRlcjogKCkgPT4gaW1wb3J0KCcuLi9Db3Vyc2VDcmVhdGUnKSxcbiAgbW9kdWxlczogWycuLi9Db3Vyc2VDcmVhdGUnXSxcbiAgbG9hZGluZzogTG9hZGluZyxcbn0pO1xuXG5jb25zdCBBc3luY0NvdXJzZSA9IExvYWRhYmxlKHtcbiAgbG9hZGVyOiAoKSA9PiBpbXBvcnQoJy4uL0NvdXJzZScpLFxuICBtb2R1bGVzOiBbJy4uL0NvdXJzZSddLFxuICBsb2FkaW5nOiBMb2FkaW5nLFxufSk7XG5cbmNsYXNzIENvdXJzZVRpbWVsaW5lIGV4dGVuZHMgQ29tcG9uZW50IHtcbiAgY29uc3RydWN0b3IocHJvcHMpIHtcbiAgICBzdXBlcihwcm9wcyk7XG4gICAgdGhpcy5zdGF0ZSA9IHt9O1xuICB9XG5cbiAgYXN5bmMgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgZWxlbWVudHMsIGVsZW1lbnQgfSA9IHRoaXMucHJvcHM7XG4gICAgICBjb25zdCB7IGlkOiB1c2VySWQsIHRva2VuIH0gPSBlbGVtZW50cy51c2VyO1xuICAgICAgY29uc3QgeyBpZCwgZWxlbWVudFR5cGUgfSA9IGVsZW1lbnQ7XG5cbiAgICAgIGNvbnN0IHsgc3RhdHVzQ29kZSwgZXJyb3IsIHBheWxvYWQsIG5leHQgfSA9IGF3YWl0IGFwaVVzZXJDb3Vyc2VzR2V0KHtcbiAgICAgICAgdG9rZW4sXG4gICAgICAgIHVzZXJJZCxcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc3RhdHVzQ29kZSA+PSAzMDApIHtcbiAgICAgICAgLy8gaGFuZGxlIGVycm9yXG4gICAgICAgIGNvbnNvbGUuZXJyb3IoZXJyb3IpO1xuICAgICAgICByZXR1cm47XG4gICAgICB9XG5cbiAgICAgIHRoaXMucHJvcHMuYWN0aW9uQ291cnNlc0dldCh7XG4gICAgICAgIGlkOiB1c2VySWQsXG4gICAgICAgIHN0YXR1c0NvZGUsXG4gICAgICAgIGNvdXJzZXM6IHBheWxvYWQsXG4gICAgICB9KTtcbiAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG4gICAgfVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnN0IHsgY2xhc3NlcywgZWxlbWVudHMsIGVsZW1lbnQgfSA9IHRoaXMucHJvcHM7XG4gICAgY29uc3QgeyBpc1NpZ25lZEluLCBpZDogdXNlcklkIH0gPSBlbGVtZW50cy51c2VyO1xuICAgIGNvbnN0IHsgaWQsIGNvdXJzZXMgfSA9IGVsZW1lbnQ7XG5cbiAgICByZXR1cm4gKFxuICAgICAgPEdyaWQgY29udGFpbmVyIGp1c3RpZnk9XCJjZW50ZXJcIiBzcGFjaW5nPXswfSBjbGFzc05hbWU9e2NsYXNzZXMucm9vdH0+XG4gICAgICAgIDxHcmlkIGl0ZW0geHM9ezEyfSBzbT17OX0gbWQ9ezd9IGxnPXs2fSB4bD17Nn0+XG4gICAgICAgICAge2lzU2lnbmVkSW4gJiYgdXNlcklkID09PSBpZCA/IDxBc3luY0NvdXJzZUNyZWF0ZSAvPiA6IG51bGx9XG4gICAgICAgICAge3R5cGVvZiBjb3Vyc2VzICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAgICAgICAgY291cnNlcy5tYXAoKHsgY291cnNlSWQgfSkgPT4gKFxuICAgICAgICAgICAgICA8QXN5bmNDb3Vyc2Uga2V5PXtjb3Vyc2VJZH0gY291cnNlSWQ9e2NvdXJzZUlkfSAvPlxuICAgICAgICAgICAgKSl9XG4gICAgICAgIDwvR3JpZD5cbiAgICAgIDwvR3JpZD5cbiAgICApO1xuICB9XG59XG5cbkNvdXJzZVRpbWVsaW5lLnByb3BUeXBlcyA9IHtcbiAgY2xhc3NlczogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50czogUHJvcFR5cGVzLm9iamVjdC5pc1JlcXVpcmVkLFxuICBlbGVtZW50OiBQcm9wVHlwZXMuc2hhcGUoe1xuICAgIGlkOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgIHVzZXJuYW1lOiBQcm9wVHlwZXMuc3RyaW5nLFxuICAgIGNvdXJzZXM6IFByb3BUeXBlcy5hcnJheU9mKFxuICAgICAgUHJvcFR5cGVzLnNoYXBlKHtcbiAgICAgICAgcG9zdElkOiBQcm9wVHlwZXMubnVtYmVyLFxuICAgICAgfSksXG4gICAgKSxcbiAgfSksXG4gIGFjdGlvbkNvdXJzZXNHZXQ6IFByb3BUeXBlcy5mdW5jLmlzUmVxdWlyZWQsXG59O1xuXG5jb25zdCBtYXBTdGF0ZVRvUHJvcHMgPSAoeyBlbGVtZW50cywgY291cnNlcyB9KSA9PiAoeyBlbGVtZW50cywgY291cnNlcyB9KTtcbmNvbnN0IG1hcERpc3BhdGNoVG9Qcm9wcyA9IChkaXNwYXRjaCkgPT5cbiAgYmluZEFjdGlvbkNyZWF0b3JzKFxuICAgIHtcbiAgICAgIGFjdGlvbkNvdXJzZXNHZXQsXG4gICAgfSxcbiAgICBkaXNwYXRjaCxcbiAgKTtcblxuZXhwb3J0IGRlZmF1bHQgY29ubmVjdChtYXBTdGF0ZVRvUHJvcHMsIG1hcERpc3BhdGNoVG9Qcm9wcykoXG4gIHdpdGhTdHlsZXMoc3R5bGVzKShDb3Vyc2VUaW1lbGluZSksXG4pO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vc3JjL2NsaWVudC9jb250YWluZXJzL0NvdXJzZVRpbWVsaW5lL2luZGV4LmpzIl0sInNvdXJjZVJvb3QiOiIifQ==