# Mayash Database

This library is a collection of all the functions to interact with database.

## Configurations

  config.js file contains all the configurations required for datastore.

## Kinds / Tables

  This file contians all the kinds/tables name and their basic schema as example.

## Elements

  This folder contains all the functions related to Elements.

## Circles
  
  This file containes all the functions related to circles only, it is a sub-part of elements
  and only focuses on circles.

## Posts

  This folder contains all the functions related to Posts.

## Courses

  This folder contains all the functions related to Courses.

## Classrooms

  This folder contains all the functions related to Classrooms.

## Search

  This folder contains all the functions related to Search queries.

## Folder Structure

    .
    ├── README.md
    ├─┬ datastore
    │ ├── auth.js
    │ ├── bookmarks.js
    │ ├─┬ circles
    │ │ ├── index.js
    │ │ ├── members.js
    │ │ ├── edu.js
    │ │ ├── field.js
    │ │ └── org.js
    │ ├── classrooms.js
    │ ├── config.js
    │ ├── courseModules.js
    │ ├─┬ courses
    │ │ ├─┬ circles
    │ │ │ ├── edu.js
    │ │ │ ├── field.js
    │ │ │ └── org.js
    │ │ ├─┬ discussion
    │ │ │ ├── answer.js
    │ │ │ └── question.js
    │ │ └── users.js
    │ ├── courses.js
    │ ├── elements.js
    │ ├── faker.js
    │ ├── follow.js
    │ ├── index.js
    │ ├── index.yaml
    │ ├── kinds.js
    │ ├── notes.js
    │ ├── photos.js
    │ ├─┬ posts
    │ │ ├── comments.js
    │ │ ├── index.js
    │ │ └── reactions.js
    │ ├── resume.js
    │ └── users.js
    ├─┬ elasticSearch
    │ ├── config.js
    │ ├── create.js
    │ ├── delete.js
    │ ├── elasticsearch.yml
    │ ├── index.js
    │ ├── search.js
    │ └── update.js
    └─┬ redis
      └── index.js