import { getAsync } from './config';

type Params = {
  key: string,
};

type Success = {
  statusCode: number,
  message: string,
};

type ErrorResponse = {
  statusCode: number,
  error: string,
};

/**
 * This function will get the value stored at the key
 * @async
 * @function
 * @param {Object} payload
 * @param {String} payload.key
 * @returns {Promise}
 *
 * @example
 *  get({ key: 'one' })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export async function get({ key }: Params): Promise<Success | ErrorResponse> {
  try {
    const res = await getAsync(key);

    if (!res) {
      return {
        statusCode: 404,
        error: 'Key not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Redis Database Server Error',
    };
  }
}

// get({ key: 'one' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

export default get;
