/**
 *
 * @format
 * @flow
 */
import { setAsync } from './config';

type Payload = {
  key: string,
  value: string,
};

type Success = {
  statusCode: number,
  message: string,
};

type ErrorResponse = {
  statusCode: number,
  error: string,
};

/**
 * This function will create a key value pair for caching
 * @async
 * @function
 * @param {Object} payload
 * @param {String} payload.key
 * @param {String} payload.value
 * @returns {Promise}
 *
 * @example
 * create({ key: 'one', value: 'two' })
 *  .then((e) => console.log(e))
 *  .catch((e) => console.log(e));
 */
async function create({
  key,
  value,
}: Payload): Promise<Success | ErrorResponse> {
  try {
    /* eslint-disable no-unused-vars */
    const res = await setAsync(key, value);
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Redis Database Server Error',
    };
  }
}

// create({ key: 'one', value: 'two' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

export default create;
