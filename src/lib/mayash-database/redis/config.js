/**
 * @format
 * @flow
 */

import redis from 'redis';
import { promisify } from 'util';

const { NODE_ENV } = process.env;

const isDev = NODE_ENV !== 'production';

const client = redis.createClient({
  host: isDev ? 'redis' : '35.200.174.162',
  port: 6379,
});

export const getAsync = promisify(client.get).bind(client);
export const setAsync = promisify(client.set).bind(client);

export default {
  getAsync,
  setAsync,
};
