/** @format */

import create from './create';
import update from './update';
import Delete from './delete';
import search from './search';

export default {
  create,
  update,
  delete: Delete,
  search,
};
