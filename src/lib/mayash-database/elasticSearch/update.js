/** @format */
import client from './config';

/**
 * This function will add the document to the index, making it searchable
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.index - Index of the document
 * (Posts/Courses/Elements)
 * @param {string} payload.type - Type(post/course/element)
 * @param {number} payload.id -
 * ID of specified type (postId/courseId/userId/circleId)
 * @param {Object} payload.body - Other information of the entity
 * @returns {Promise}
 *
 * @example
 *
 * update({
 * index: 'five',
 * type: 'posts',
 * id: '4512586541782511',
 * body: {
 *  authorId: 4512586141782562,
 *  },
 * })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
async function update({ index, type, id, body }) {
  try {
    /* eslint-disable no-unused-vars */
    const res = await client.update({ index, type, id, body });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Document updated',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// update({
//   index: 'one',
//   type: 'post',
//   id: '8512586541781266',
//   body: {
//     title: 'Second Post',
//   },
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

export default update;
