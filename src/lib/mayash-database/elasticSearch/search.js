/** @format */

import client from './config';

/**
 * This function will search through the indexes
 * @async
 * @function search
 * @param {Object} payload
 * @param {string} payload.index - Index used in E-Search
 * @param {Object} payload.q - Query Object
 * @returns {Promise}
 *
 * @example
 * search({
 *  index: 'one',
 *  q: 'title: First post',
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
async function search({ index = 'mayash', q }) {
  try {
    const res = await client.search({ index, q });

    if (res.timed_out) {
      return {
        statusCode: 408,
        error: 'Request timed out',
      };
    }

    const result = res.hits.hits;

    return {
      statusCode: 200,
      message: 'Success',
      payload: result.map(({ _type, _source }) => ({
        type: _type,
        ..._source,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'ES Database Server Error',
    };
  }
}

// search({
//   q: 'First',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

export default search;
