/** @format */

import client from './config';

/**
 *  This function will delete index from the elastic search
 * @async
 * @function deleteIndex
 * @param {object} params
 * @param {object} params.index - index to be added in E-Search
 * @returns {Promise}
 *
 * @example
 *
 * deleteIndex({ index: 'third' })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e)); */
async function deleteIndex({ index }) {
  try {
    const res = await client.indices.delete({ index });

    if (!res) {
      return {
        statusCode: 404,
        error: 'Index not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Index deleted',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'ES Database Server Error',
    };
  }
}
// deleteIndex({ index: '_all' })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

export default deleteIndex;
