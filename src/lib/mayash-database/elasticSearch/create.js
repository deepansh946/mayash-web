/**
 * @format
 * @flow
 */

import client from './config';

/**
 * This function will add the document to the index, making it searchable
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.index - Index of the document
 * (Posts/Courses/Elements)
 * @param {string} payload.type - Type(post/course/element)
 * @param {number} payload.id -
 * ID of specified type (postId/courseId/userId/circleId)
 * @param {Object} payload.body - Other information of the entity
 * @returns {Promise}
 *
 * @example
 *
 * create({
 *  index: 'third',
 *  type: 'posts',
 *  id: '4512586541782563',
 *  body: {
 *  postId: id,
 *  authorId: 4512586541782561,
 *  },
 * })
 *  .then(e => console.log(e)
 *  .catch(e => console.log(e));
 */
async function create({
  index = 'mayash',
  type,
  id,
  body,
}: {|
  index?: string,
  type: string,
  id: number,
  body: Object,
|}): Promise<
  | {
      statusCode: number,
      message: string,
    }
  | {
      statusCode: number,
      error: string,
    },
> {
  try {
    /* eslint-disable no-unused-vars */
    const res = await client.create({ index, type, id, body });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'ES Database Server Error',
    };
  }
}

// create({
//   type: 'course',
//   id: 8512586541781255,
//   body: {
//     title: 'First Post',
//     id: 8512586541781253,
//   },
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

export default create;
