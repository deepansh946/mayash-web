/** @format */

const elasticsearch = require('elasticsearch');

const { NODE_ENV } = process.env;

const isDev = NODE_ENV !== 'production';

const client = new elasticsearch.Client({
  host: isDev ? 'elasticsearch:9200' : '35.200.241.173:9200',
});

export default client;
