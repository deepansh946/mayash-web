/**
 * This contain all the function realted to the auth
 *
 * @format
 */

import db from './config';
import { AUTH, ELEMENTS } from './kinds';

/**
 * @async
 * This function will create a admin
 * @param {object} payload
 * @param {number} payload.userId -Id of the User
 * @returns {Promise}
 *
 * @example
 * createAdmin({ userId: 1234567890123456 })
 *    .then(e => console.log(e))
 *    .catch(e=>console.log(e));
 */
export async function createAdmin({ userId }) {
  try {
    const key = db.key([AUTH, userId]);

    const data = {
      admin: true,
      userId: db.key([ELEMENTS, userId]),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createAdmin({ userId: 1134567891223446 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @async
 * This function will get all admin
 * @param {Object} payload
 * @returns {Promise}

 * @example
 * 
 * getAllAdmin()
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAllAdmin() {
  try {
    const query = db.createQuery(AUTH);

    const [admins] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: admins.map(({ userId, admin }) => ({
        admin,
        userId: parseInt(userId.id, 10),
      })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Error',
    };
  }
}

// getAllAdmin()
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a admin
 * @async
 * @function getAdmin
 * @param {object} payload
 * @param {number} payload.userId -Id of the User
 * @returns {Promise}
 *
 * @example
 * getAdmin({ userId: 1134567891223446 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAdmin({ userId }) {
  try {
    const key = db.key([AUTH, userId]);

    const [admin] = await db.get(key);

    if (!admin) {
      return {
        statusCode: 404,
        error: 'Admin Not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Admin Found',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAdmin({ userId: 1134567891223446 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @async
 * This function will delete a admin
 * @param {Object} payload
 * @param {number} payload.userId -Id of the User
 * @returns {Promise}
 *
 * @example
 * deleteAdmin({ userId: 1134567891223446 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function deleteAdmin({ userId }) {
  try {
    const key = db.key([AUTH, userId]);

    const [admin] = await db.delete(key);

    if (admin.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Admin not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Successfully Deleted',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteAdmin({ userId: 1134567891223446 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
