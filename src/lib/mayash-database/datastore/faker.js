/* eslint-disable */

import faker from 'faker';

import db from './config';

import * as usersdb from './users';
import * as circlesdb from './circles';
import * as postsdb from './posts';
import * as coursesdb from './courses';

import {
  ELEMENTS, POSTS,
  COURSES,
} from './kinds';


/**
 * 
 */
function createFakeUsers() {
  for (let i = 0; i < 100; i += 1) {
    const user = {
      name: faker.name.findName(),
      username: faker.internet.userName().toLowerCase(),
      avatar: '/public/photos/mayash-logo-transparent.png',
      email: faker.internet.email(),
      password: 'aA1@@',
    };

    setTimeout(() => {
      usersdb
        .createUser(user)
        .then(e => console.log(e))
        .catch(e => console.log(e));
    }, 100 * i);
  }
}

function createFakeGurus() {
  const query = db.createQuery(ELEMENTS)
    .filter('elementType', '=', 'user')
    .limit(1000);
  let cnt = 0;
    
  query.runStream()
    .on('error', console.error)
    .on('data', function (e) {
      const n = Math.floor(Math.random() * 10);
      
      const id = parseInt(e.id.id, 10);
      if(n % 2 === 0) {
        usersdb
        .enableGuru({ id })
          .then(e => console.log(e))
          .catch(e => console.error(e));
      }
    })
    .on('end', function () {
      console.log('end');
    });  
}
// createFakeGurus();


/**
 * This function will create fake data of Educational institutes
 * @param {number} creatorId - user id as a creatorId.
 */
function createFakeCircleEdus(creatorId) {
  for (let i = 0; i < 100; i += 1) {
    const edu = {
      name: `${faker.company.companyName()} College`,
      username: `${faker.internet.userName().toLowerCase()}.edu`,
      avatar: '/public/photos/mayash-logo-transparent.png',
      creatorId,
    };

    setTimeout(() => {
      circlesdb
        .createEdu(edu)
        .then(e => console.log(e))
        .catch(e => console.log(e));
    }, 100 * i);
  }
}

// createFakeCircleEdus(5963751069057024);


/**
 * 
 * @param {number} creatorId - admin user id.
 */
function createFakeCircleOrgs(creatorId) {
  for (let i = 0; i < 100; i += 1) {
    const org = {
      name: `${faker.company.companyName()} ${faker.company.companySuffix()}`,
      username: faker.internet.userName().toLowerCase(),
      avatar: '/public/photos/mayash-logo-transparent.png',
      creatorId,
    };

    setTimeout(() => {
      circlesdb
        .createOrg(org)
        .then(e => console.log(e))
        .catch(e => console.log(e));
    }, 100 * i);
  }
}

// createFakeCircleOrgs(5963751069057024);


/**
 * 
 */
function createFakeCircleFields() {
  for (let i = 0; i < 100; i += 1) {
    const name = faker.commerce.department();
    const field = {
      name,
      username: name.toLowerCase(),
      avatar: '/public/photos/mayash-logo-transparent.png',
    };

    setTimeout(() => {
      circlesdb
        .createField(field)
        .then(e => console.log(e))
        .catch(e => console.log(e));
    }, 100 * i);
  }
}

// createFakeCircleFields();


/**
 * 
 */
function createFakeCircleLocations() {

}


/**
 * 
 */
function addFakeMembersToCircles() {
  const query = db.createQuery(ELEMENTS)
    .filter('elementType', '=', 'circle')
    .filter('circleType', '=', 'edu')
  // .filter('circleType', '=', 'org')
  // .limit(10);

  let count = 0;

  query.runStream()
    .on('err', console.error)
    .on('data', ({ id }) => {
      const circleId = parseInt(id.id, 10);
      const limit = Math.floor(Math.random() * 15);
      const offset = Math.floor(Math.random() * 80);

      const query2 = db.createQuery(ELEMENTS)
        .filter('elementType', '=', 'user')
        .offset(offset)
        .limit(limit);

      setTimeout(async () => {

        const [users] = await db.runQuery(query2);

        users.map(({ id }, i) => {

          const memberId = parseInt(id.id, 10);

          const n = Math.floor(Math.random() * 3);

          setTimeout(() => {

            circlesdb.addMemberByCircleIdAndMemberId({
              circleId,
              memberId,
              role: n % 2 ? 'admin' : 'member',
            }).then(e => console.log(e))
              .catch(e => console.log(e));

          }, 500 * i);
        });

      }, 500 * (++count));
    })
    .on('end', () => console.log('end'));
}

// addFakeMembersToCircles();


/**
 * 
 * @param {number} authorId - author of post is a user or admin of circle.
 */
function createFakePost(authorId) {
  const post = {
    id: authorId,
    postType: 'article',
    title: faker.lorem.sentence(),
  };

  postsdb.createPostById(post, res => console.log(res));
}

// createFakePost(5066549580791808);

/**
 * 
 */
function createFakePosts() {
  const query = db.createQuery(ELEMENTS);
  // .filter('elementType', 'user')
  // .limit(2);

  query.runStream()
    .on('error', console.error)
    .on('data', function (e) {
      const n = Math.floor(Math.random() * 25);

      for (let i = 0; i < n; i += 1) {
        setTimeout(() => {

          createFakePost(parseInt(e.id.id, 10));

        }, 500 * i);
      }
    })
    .on('end', function () {
      console.log('end');
    });
}

// createFakePosts();


function createFakeCourses() {
  const query = db.createQuery(ELEMENTS)
    .filter('guru', '=', true)
    .limit(5);

  query.runStream()
    .on('error', console.error)
    .on('data', function(e) {
      const n = Math.floor(Math.random() * 25);

      for(let i = 0; i < n; i += 1) {
        const title = faker.lorem.sentence();
        const id = parseInt(e.id.id, 10);
        coursesdb
          .createCourseById({ id, title})
          .then(e => console.log(e))
          .catch(e => console.error(e));
      }
    })
    .on('end', function() {
      console.log('end');
    })
}

// createFakeCourses();

function createFakeCourseModules() {
  
}
