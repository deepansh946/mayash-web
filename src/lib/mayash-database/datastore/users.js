/**
 * This file is an extention of elements.js file, this file
 * contains all the specific functions to users only.
 *
 * @format
 */

import crypto from 'crypto';

import db from './config';
import { ELEMENTS, GURU_REQUESTS, CIRCLE_MEMBERS } from './kinds';
import { checkUsername } from './elements';

/**
 * This function will create a new user and this function will not ensures
 * whether the username or email exists in the database or not, so before
 * sending data to this function, make sure you check it.
 * @param {object} payload -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.email -
 * @param {string} payload.avatar -
 * @param {string} payload.password -
 * @param {function} callback -
 */
export async function createUser({
  username,
  name,
  email,
  avatar = '/public/photos/mayash-logo-transparent.png',
  password,
}) {
  try {
    const { statusCode } = await checkUsername({ username });

    if (statusCode >= 500) {
      return {
        statusCode,
        error: 'Database Server Error.',
      };
    }

    if (statusCode === 200) {
      return {
        statusCode: 400,
        error: 'Username already taken.',
      };
    }

    const [keys] = await db.allocateIds(db.key(ELEMENTS), 1);

    const key = keys[0];
    const data = {
      id: key,
      username,
      name,
      email,
      avatar,
      elementType: 'user',
      password: crypto
        .createHash('sha256')
        .update(password)
        .digest('hex'),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'User Created',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}

// createUser({
//   username: 'hbarve1',
//   name: 'Himank Barve',
//   email: 'hbarve1@gmail.com',
//   avatar: '/public/photos/mayash-logo-transparent.png',
//   password: '12345',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 *
 */
export async function getUsers() {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('elementType', '=', 'user')
      .limit(50);

    const [users] = await db.runQuery(query);

    return {
      statusCode: 200,
      payload: users.map((u) => ({
        ...u,
        id: parseInt(u.id.id, 10),
      })),
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getUsers()
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @param {object} payload -
 * @param {number} payload.userId -
 */
export async function getUserCirclesByUserId({ id }) {
  try {
    const query = db
      .createQuery(CIRCLE_MEMBERS)
      .filter('memberId', '=', db.key([ELEMENTS, id]));

    const [userCircles] = await db.runQuery(query);

    const keys = userCircles.map(({ circleId }) => circleId);

    const [circles] = await db.get(keys);

    return {
      statusCode: 200,
      message: 'Success',
      payload: circles.map(
        ({
          id: circleId,
          username,
          name,
          avatar,
          elementType,
          circleType,
        }) => ({
          id: parseInt(circleId.id, 10),
          username,
          name,
          avatar,
          elementType,
          circleType,
        }),
      ),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getUserCirclesByUserId({ userId: 1011214555 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * @param {object} payload -
 * @param {number} payload.id -
 */
export async function getUserById({ id }) {
  try {
    // create a datastore key.
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    const {
      name,
      username,
      email,
      elementType,
      guru,
      avatar,
      cover,
      resume,
    } = user;

    const payload = {
      elementType,
      id,
      username,
      name,
      avatar,
      email,
      guru,
      cover,
      resume,
    };

    return {
      statusCode: 200,
      message: 'Successful.',
      payload,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 *
 * @param {object} payload -
 * @param {string} payload.username -
 */
export async function getUserByUsername({ username }) {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('username', '=', username)
      .filter('elementType', '=', 'user');

    const [users] = await db.runQuery(query);

    if (users.length === 0) {
      return {
        statusCode: 404,
        error: 'User Not Found.',
      };
    }

    const {
      id,
      name,
      email,
      elementType,
      guru,
      avatar,
      cover,
      resume,
    } = users[0];

    const payload = {
      elementType,
      id: parseInt(id.id, 10),
      username,
      name,
      avatar,
      email,
      guru,
      cover,
      resume,
    };

    return {
      statusCode: 200,
      message: 'Success.',
      payload,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 *
 * @param {object} payload -
 * @param {number} payload.id -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @param {object} payload.resume -
 * @return {object} -
 */
export async function updateUserById({ id, name, avatar, cover, resume }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    let data = { ...user };

    if (typeof name === 'string') {
      data = { ...data, name };
    }

    if (typeof cover === 'string') {
      data = { ...data, cover };
    }

    if (typeof avatar === 'string') {
      data = { ...data, avatar };
    }

    if (typeof resume === 'object') {
      data = { ...data, resume };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will update the username of a user by its id
 * @param {number} id -
 * @param {string} username -
 */
export async function updateUsernameById({ id, username }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    const { statusCode } = await checkUsername({ username });

    if (statusCode >= 500) {
      return {
        statusCode,
        error: 'Database Server Error',
      };
    }

    if (statusCode === 200) {
      return {
        statusCode: 400,
        message: 'Username already taken',
      };
    }

    const data = { ...user, username };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Username successfully updated',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// updateUsernameById({ id: 5629499534213120, username: 'deepansh946asd' })
//  .then(e => console.log(e))
//  .catch(e => console.log(e));

/**
 * This function will retrive user's resume by it's id.
 * @param {object} payload -
 * @param {number} payload.id -
 * @return {object}
 */
export async function getUserResumeById({ id }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    const { resume } = user;
    const payload = { id, resume };

    return {
      statusCode: 200,
      message: 'Success',
      payload,
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getUserResumeById({ id: 6086896371367936 })
//   .then(e => console.log(e))
//   .catch(e => console.error(e));

/**
 * This function will update user's resume by it's id.
 * @param {object} payload -
 * @param {number} payload.id -
 * @param {object} payload.resume -
 * @return {object}
 */
export async function updateUserResumeById({ id, resume }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    const data = { ...user, resume };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Update successfully.',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// updateUserResumeById({ id: 6086896371367936, resume: {} })
//   .then(e => console.log(e))
//   .catch(e => console.error(e));

export async function createPassword({ userId, password }) {
  try {
    const key = db.key([ELEMENTS, userId]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User not found',
      };
    }

    const hashedPassword = crypto
      .createHash('sha256')
      .update(password)
      .digest('hex');

    const data = { ...user, password: hashedPassword };

    /* eslint-disable no-unused-vars  */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars  */

    return {
      statusCode: 201,
      message: 'Password created',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// createPassword({
//   userId: 5715999101812736,
//   password: 'Aa@1234',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will give the password of the user
 * @param {object} payload
 * @param {number} payload.id
 */
export async function getPassword({ id }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User not found',
      };
    }

    const { password } = user;

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        password,
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getPassword({
//   id: 5585519069102080,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

export async function updatePasswordById({ id, password }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User not found',
      };
    }

    const hashedPassword = crypto
      .createHash('sha256')
      .update(password)
      .digest('hex');

    const data = { ...user, password: hashedPassword };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Password updated succssfully',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// updatePasswordById({
//   id: 5585519069102080,
//   oldPassword: 'qweqwe1',
//   newPassword: 'qweqwe23',
//   confirmPassword: 'qweqwe23',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

export async function checkPasswordExists({ id }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    if (!user.password) {
      return {
        statusCode: 401,
        error: 'User password does not exist',
      };
    }

    return {
      statusCode: 200,
      message: 'User password exist',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// checkPasswordExists({ id: 5585519069102080 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 *
 * @param {object} payload -
 * @param {object} payload.id -
 * @returns {object} -
 */
export async function applyForGuru({ id }) {
  try {
    // use user's element id to make new key in GURU_REQUEST kind/table.
    const key = db.key([GURU_REQUESTS, id]);

    const data = {
      id: db.key([ELEMENTS, id]),
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// applyForGuru({ id: 6086896371367936 })
//   .then(e => console.log(e))
//   .catch(e => console.error(e));

/**
 *
 * @returns {object} -
 */
export async function getAllGuruRequest() {
  try {
    const query = db.createQuery(GURU_REQUESTS);

    const [requests] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: requests.map(({ id, timestamp }) => ({
        id: parseInt(id.id, 10),
        timestamp,
      })),
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllGuruRequest()
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will enable 'guru' feature for user.
 * @param {object} payload -
 * @param {number} payload.id -
 * @return {object} -
 */
export async function enableGuru({ id }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User Not Found',
      };
    }

    const data = { ...user, guru: true };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// enableGuru({ id: 5066549580791808 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function is for admin. Admin will decide
 * whether the guru must be enabled or not.
 * If enable is false, then it will be saved and
 * if it is true, then we will enable guru for that user
 * @param {object} payload -
 * @param {number} payload.id -
 * @param {boolean} payload.enable -
 * @param {string} payload.reason -
 * @returns {object} -
 */
export async function handleGuruRequest({ id, enable, reason }) {
  try {
    const key = db.key([GURU_REQUESTS, id]);
    if (!enable) {
      const [user] = await db.get(key);

      if (!user) {
        return {
          statusCode: 404,
          error: 'User Not Found',
        };
      }

      const data = { ...user, enable, reason };

      /* eslint-disable no-unused-vars */
      const [res] = await db.save({ key, data });
      /* eslint-disable no-unused-vars */

      return {
        statusCode: 200,
        message: 'Success',
      };
    }
    // delete entry from GURU-REQUEST table.
    db.delete(key);

    // enable guru for user and return it's response to user.
    return await enableGuru({ id });
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function will delete guru feature
 * @param {Object} payload
 * @param {number} payload.id
 * @returns {Promise}
 *
 */
export async function deleteGuru({ id }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: '404',
        error: 'User not found',
      };
    }

    const data = { ...user, guru: false };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: '200',
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @returns {Promise}
 */
export async function getGuru({ id }) {
  try {
    const key = db.key([ELEMENTS, id]);

    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User not found',
      };
    }

    const { guru } = user;

    return {
      statusCode: 200,
      message: 'Success',
      payload: guru,
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: '500',
      error: 'Database Server Error',
    };
  }
}

export async function getAllGurus() {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('guru', '=', true)
      .limit(50);

    const [gurus] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: gurus.map((g) => ({
        ...g,
        id: parseInt(g.id.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
