/**
 * This will contain all the database function of the resume
 *
 * @format
 */

import db from './config';
import { RESUME } from './kinds';

/**
 * this function will create the resume template
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.resumeId - resume id in string
 * @param {string} payload.name - resume name
 * @param {Object} payload.data - draft.js raw content state
 * @returns {Promise}
 *
 * @example
 * create({
 *  resumeId: 'resume-feature',
 *  name: 'Resume feature',
 *  data: { age: 12 },
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function create({ resumeId, name, data }) {
  try {
    const key = db.key([RESUME, resumeId]);

    const payload = { resumeId, name, data };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Successfully Created',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// create({
//   resumeId: 'resume-feature',
//   name: 'Resume feature',
//   data: { age: 12 },
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * this function will update the resume template
 * @async
 * @function update
 * @param {Object} payload
 * @param {string} payload.resumeId
 * @param {Object} payload.data
 * @returns {Promise}
 *
 * @example
 * update({ resumeId: '12312', data: { hello: 'hello1' } })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function update({ resumeId, name, data }) {
  try {
    const key = db.key([RESUME, resumeId]);

    const payload = { resumeId, name, data };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Resume Updated',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// update({
//   resumeId: '12312',
//   name: 'hello',
//   data: { hello2: 'hello1' },
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * this function will display all the resume templates
 * @async
 * @function getAll
 * @return {Promise}
 *
 * @example
 * getAll()
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll() {
  try {
    const query = db.createQuery(RESUME);

    const [resumes] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Sucess',
      payload: resumes,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAll()
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * this function will display a resume template
 * @async
 * @function get
 * @param {Object} payload
 * @param {string} payload.resumeId
 * @returns {Promise}
 *
 * @example
 * get({ resumeId: 'resume-feature' })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function get({ resumeId }) {
  try {
    const key = db.key([RESUME, resumeId]);

    const [resume] = await db.get(key);

    if (!resume) {
      return {
        statusCode: 404,
        message: 'Resume Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: resume,
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// get({ resumeId: 'resume-feature' })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
