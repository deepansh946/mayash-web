/**
 * /* eslint consistent-return: "off"
 *
 * @format
 */

/**
 * This file contains all the functions related to classroom feature
 */
import db from './config';
import {
  ELEMENTS,
  CIRCLE_COURSES,
  DEPARTMENTS,
  DEGREES,
  SEMESTERS,
  CLASSROOM,
} from './kinds';

/**
 * This function will enable classroom for a circle
 * @async
 * @function enableClassroom
 * @param {object} payload
 * @param {number} payload.circleId
 * @returns {Promise}
 *
 * @example
 *
 * enableClassroom ({circleId: 7654563218976234})
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function enableClassroom({ circleId }) {
  try {
    const key = db.key([ELEMENTS, circleId]);

    const [circle] = await db.get(key);

    if (!circle) {
      return {
        statusCode: 404,
        error: 'Circle Not Found',
      };
    }

    const data = { ...circle, classroom: true };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Classroom feature enabled for circle',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// enableClassroom({ circleId: 7654563218976234 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will disable classroom feature for a circle
 * @async
 * @function disableClassroom
 * @param {object} payload
 * @param {number} payload.circleId
 * @returns {Promise}
 *
 * @example
 *
 * disableClassroom ({circleId: 7654563218976234})
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function disableClassroom({ circleId }) {
  try {
    const key = db.key([ELEMENTS, circleId]);

    const [circle] = await db.get(key);

    if (!circle) {
      return {
        statusCode: 404,
        error: 'Circle Not found',
      };
    }

    const { classroom, ...restCircle } = circle;

    const data = { ...restCircle };

    /* eslint-disable no-unused-vars */
    const [res] = db.save({ key, data });
    /* eslint-disable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Classroom feature disabled for circle',
    };
  } catch (err) {
    console.log(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// disableClassroom({ circleId: 7654563218976234 })
// .then(e => console.log(e))
// .catch(e => console.log(e));

/**
 * This function will create a classroom in a semester
 * @async
 * @function createClassroom
 * @param {object} payload
 * @param {number} payload.profId
 * @param {number} payload.courseId
 * @returns {Promise}
 *
 * @example
 * createClassroom({
 * profId: 123,
 * courseId: 444,
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function createClassroom({ profId, courseId }) {
  try {
    const [keys] = await db.allocateIds(db.key(CLASSROOM), 1);
    const key = keys[0];

    const data = {
      classId: key,
      profId,
      courseId,
    };

    const [res] = await db.save({ key, data });

    return {
      statusCode: 200,
      message: 'Classroom created',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createClassroom({
//   profId: 123,
//   courseId: 444,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 *  Getting all the courses in a classroom
 * @async
 * @function getClassroomCourses
 * @param {object} payload
 * @param {number} payload.id
 * @param {string} payload.degree
 * @param {number} payload.semester
 * @param {string} payload.next
 *
 *
 */
export async function getClassroomCourses({ id, degree, semester, next }) {
  try {
    const query = db
      .createQuery(CIRCLE_COURSES)
      .filter('circleId', '=', db.key([ELEMENTS, id]))
      .limit(10);

    if (semester) {
      query.filter('semester', '=', semester.toString());
    }
    if (degree) {
      query.filter('degree', '=', degree);
    }
    if (next) {
      query.start(next);
    }

    const [classroomCourses] = await db.runQuery(query);

    const keys = classroomCourses.map((course) => course.courseId);

    const [course, info] = await db.get(keys);

    // Using all the course ids to display them
    const course2 = course.map((course1) => {
      const temp = classroomCourses.find((c) => (c) =>
        c.courseId.id === course.courseId.id,
      );
      return temp;
    });

    const payload = {
      ...course,
      ...course2,
      circleId: course2.circleId.id,
      courseId: course.courseId.id,
      authorId: course.authorId.id,
    };

    return {
      statusCode: 200,
      message: 'Success',
      payload,
      next:
        info.moreResults !== db.NO_MORE_RESULTS ? info.endCursor : undefined,
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function will create a new department in a classroom.
 * NOTE: Before calling this function make sure you check whether the
 * circle exists or not and classroom feature is enabled or not.
 * @async
 * @function createDepartmentInCircle
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.title
 * @param {number} payload.hodId
 * @returns {Promise}
 *
 * @example
 * createDepartmentInCircle({
 * circleId: 41112,
 * deptId: 'ece',
 * title: 'Computer Science',
 * hodId: 123,
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function createDepartmentInCircle({
  circleId,
  deptId,
  title,
  hodId,
}) {
  try {
    const key = db.key([ELEMENTS, circleId, DEPARTMENTS, deptId]);

    const data = {
      deptId: key,
      title,
      hodId,
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Department created in circle',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createDepartmentInCircle({
//   circleId: 41112,
//   deptId: 'ece',
//   title: 'Computer Science',
//   hodId: 123,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will update the department inside a circle.
 * NOTE: Before running that function make sure the circle exists
 * and the department also exists.
 * @async
 * @function updateDepartmentInCircle
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.title
 * @param {number} payload.hodId
 * @returns {Promise}
 *
 * @example
 * updateDepartmentInCircle({
 * circleId: 5493160092368896,
 * deptId: 'ece',
 * title: 'Computer Science',
 * hodId: 465,
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function updateDepartmentInCircle({
  circleId,
  deptId,
  title,
  hodId,
}) {
  try {
    const key = db.key([ELEMENTS, circleId, DEPARTMENTS, deptId]);

    const [dept] = await db.get(key);

    if (!dept) {
      return {
        statusCode: 404,
        error: 'Department not found',
      };
    }

    const data = { deptId, title, hodId };

    /* eslint-disable no-unused-vars */
    const [result] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Department updated',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// updateDepartmentInCircle({
//   circleId: 5493160092368896,
//   deptId: 'ece',
//   title: 'Computer Science',
//   hodId: 465,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will delete a department from a circle
 * @async
 * @function deleteDepartmentInCircle
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {number} payload.deptId
 * @returns {Promise}
 *
 * @example
 * deleteDepartmentInCircle({
 * circleId: 5493160092368896,
 * deptId: 'ece',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function deleteDepartmentInCircle({ circleId, deptId }) {
  try {
    const key = db.key([ELEMENTS, circleId, DEPARTMENTS, deptId]);
    const [dept] = await db.get(key);

    if (!dept) {
      return {
        statusCode: 404,
        error: 'Department Not Found',
      };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Department deleted successfully',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteDepartmentInCircle({
//   circleId: 5493160092368896,
//   deptId: 'ece',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will display a single department inside a circle
 * @async
 * @function getDepartmentInCircle
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {number} payload.deptId
 * @returns {Promise}
 *
 * @example
 * getDepartmentInCircle({
 * circleId: 41112,
 *  deptId: 'cse'
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function getDepartmentInCircle({ circleId, deptId }) {
  try {
    const key = db.key([ELEMENTS, circleId, DEPARTMENTS, deptId]);

    const [res] = await db.get(key);

    if (!res) {
      return {
        statusCode: 404,
        error: 'Department not found',
      };
    }

    const { title, hodId } = res;

    const payload = { title, hodId, deptId };

    return {
      statusCode: 200,
      message: 'Department found',
      payload,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getDepartmentInCircle({ circleId: 41112, deptId: 'cse' })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will display all the departments in a circle
 * @async
 * @function getAllDepartmentInCircle
 * @param {object} payload
 * @param {number} payload.circleId
 * @returns {Promise}
 *
 * @example
 * getAllDepartmentsInCircle({ circleId: 41112 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function getAllDepartmentsInCircle({ circleId }) {
  try {
    const key = db.key([ELEMENTS, circleId]);
    const query = db.createQuery(DEPARTMENTS).hasAncestor(key);

    const [res] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: res.map(({ deptId, title, hodId }) => ({
        deptId: deptId.name,
        title,
        hodId,
      })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllDepartmentsInCircle({ circleId: 41112 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will display all the degrees in a department
 * @async
 * @function getAllDegrees
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @returns {Promise}
 *
 * @example
 * getAllDegrees({
 * circleId: 123,
 * deptId: 'cse',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAllDegrees({ circleId, deptId }) {
  try {
    const key = db.key([ELEMENTS, circleId, DEPARTMENTS, deptId]);
    const query = db.createQuery(DEGREES).hasAncestor(key);

    const [res] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Degrees found',
      payload: res.map(({ degreeId, title, duration, description }) => ({
        degreeId,
        title,
        duration,
        description,
      })),
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllDegrees({
//   circleId: 123,
//   deptId: 'cse',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will create a degree in department
 * @async
 * @function createDegreeInDepartment
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @param {string} payload.title
 * @returns {Promise}
 *
 * @example
 * createDegreeInDepartment({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * title: 'Bachelor Of Technology',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function createDegreeInDepartment({
  circleId,
  deptId,
  degreeId,
  title,
}) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
    ]);

    const data = {
      degreeId: key,
      title,
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Degree created in Department',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createDegreeInDepartment({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
//   title: 'Bachelor Of Technology',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will update degree in Department
 * @async
 * @function updateDegreeInDepartment
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @param {string} payload.title
 * @param {number} payload.duration
 * @param {string} payload.description
 * @returns {Promise}
 *
 * @example
 * updateDegreeInDepartment({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * title: 'Bachelor Of Technology asd',
 * duration: 48
 * description: 'lorem ipsum',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function updateDegreeInDepartment({
  circleId,
  deptId,
  degreeId,
  title,
  duration,
  description,
}) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
    ]);

    const [degree] = await db.get(key);

    if (!degree) {
      return {
        statusCode: 404,
        error: 'Degree Not Found',
      };
    }

    const data = { title, duration, description };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Degree updated',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// updateDegreeInDepartment({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
//   title: 'Bachelor Of Technology asd',
//   duration: 48
//   description: 'lorem ipsum',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will delete degree in a department
 * @async
 * @function deleteDegreeInDepartment
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @returns {Promise}
 *
 */

export async function deleteDegreeInDepartment({ circleId, deptId, degreeId }) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
    ]);
    const [degree] = await db.get(key);

    if (!degree) {
      return {
        statusCode: 404,
        error: 'Degree Not Found',
      };
    }

    /* eslint-disable no-unused-vars */
    const [result] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Degree deleted successfully',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function will display a degree in a Department
 * @async
 * @function getDegreeInDepartment
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @returns {Promise}
 *
 * @example
 * getDegreeInDepartment({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function getDegreeInDepartment({ circleId, deptId, degreeId }) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
    ]);

    const [res] = await db.get(key);

    if (!res) {
      return {
        statusCode: 404,
        error: 'No Degree found in Department',
      };
    }

    const { title, duration, description } = res;

    const payload = { title, duration, description, degreeId };

    return {
      statusCode: 200,
      message: 'Degree found in Department',
      payload,
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// getDegreeInDepartment({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will display all the semesters in a degree
 * @async
 * @function getAllSemesterInDegree
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @returns {Promise}
 * @example
 *
 * getAllSemesterInDegree({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function getAllSemesterInDegree({ circleId, deptId, degreeId }) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
    ]);

    const query = db.createQuery(SEMESTERS).hasAncestor(key);

    const [res] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Degrees found',
      payload: res.map(({ semId }) => ({ semId })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllSemesterInDegree({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will create a semester inside a degree.
 * NOTE: Before using this function make sure that the circle exists,
 * the department exists and the degree exists.
 * @async
 * @function createSemesterInDegree
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @param {number} payload.semId
 * @returns {Promise}
 *
 * @example
 * createSemesterInDegree({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * semId: 1,
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function createSemesterInDegree({
  circleId,
  deptId,
  degreeId,
  semId,
}) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
      SEMESTERS,
      semId,
    ]);

    const data = {
      semId,
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Semester added in Degree',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createSemesterInDegree({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
//   semId: 1,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will update semester in degree
 * @async
 * @function updateSemesterInDegree
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @param {number} payload.semId
 * @returns {Promise}
 *
 * @example
 * updateSemesterInDegree({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * semId: 1,
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function updateSemesterInDegree({
  circleId,
  deptId,
  degreeId,
  semId,
}) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
      SEMESTERS,
      semId,
    ]);

    const [semester] = await db.get(key);

    if (!semester) {
      return {
        statusCode: 404,
        error: 'Semester Not Found',
      };
    }

    const data = { semId };

    /* eslint-disable no-unused-vars */
    const [result] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Semester updated',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// updateSemesterInDegree({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
//   semId: 1,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will delete semester in degree
 * @async
 * @function deleteSemesterInDegree
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @param {number} payload.semId
 * @returns {Promise}
 *
 * @example
 * deleteSemesterInDegree({
 * circleId: 123,
 * deptId: 'cse',
 * degreeId: 'btech',
 * semId: 1,
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */

export async function deleteSemesterInDegree({
  circleId,
  deptId,
  degreeId,
  semId,
}) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
      SEMESTERS,
      semId,
    ]);

    const [semester] = await db.get(key);

    if (!semester) {
      return {
        statusCode: 404,
        error: 'Semester not found',
      };
    }

    /* eslint-disable no-unused-vars */
    const [result] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Semester deleted successfully',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteSemesterInDegree({
//   circleId: 123,
//   deptId: 'cse',
//   degreeId: 'btech',
//   semId: 1,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will display a single semester in degree
 * @async
 * @function getSemesterInDegree
 * @param {object} payload
 * @param {number} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.degreeId
 * @param {number} payload.semId
 * @returns {promise}
 */

export async function getSemesterInDegree({
  circleId,
  deptId,
  degreeId,
  semId,
}) {
  try {
    const key = db.key([
      ELEMENTS,
      circleId,
      DEPARTMENTS,
      deptId,
      DEGREES,
      degreeId,
      SEMESTERS,
      semId,
    ]);

    const [semester] = await db.get(key);

    if (!semester) {
      return {
        statusCode: 404,
        error: 'Semester Not found',
      };
    }

    const payload = { circleId, deptId, degreeId, semId };

    return {
      statusCode: 200,
      message: 'Semester found in Degree',
      payload,
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
