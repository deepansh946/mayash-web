/** @format */

// import path from 'path';
import datastore from '@google-cloud/datastore';

/**
 * importing environment variables.
 */
const { NODE_ENV, PROJECT_ID } = process.env;

/**
 * 'options' object contains options to initialize datastore.
 * @type {Object}
 */
const options = {
  projectId: PROJECT_ID || 'mayash-web',
  // keyFilename: path.join(
  //   __dirname,
  //   '..',
  //   '..',
  //   '..',
  //   '..',
  //   'secrets/mayash-web-secrets.json',
  // ),
};

/**
 * [if description]: this 'if' statement will add development
 * apiEndpoint of local datastore
 * emulator.
 * @param  {[type]} [NODE_ENV=== 'development'] [description]
 * @return {[type]}              [description]
 */
if (NODE_ENV !== 'production') {
  /* Here we have to specify service name of google's datastore in 
   * docker-compose
   * file with port number.
   */
  options.apiEndpoint = 'http://datastore:8081';
}

/**
 * db: this will initialize datastore database with 'options'.
 * @type {Object}
 */
const db = datastore(options);

export default db;
