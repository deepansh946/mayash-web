/**
 * This contain all the function realted to the auth for admin only.
 *
 * @format
 * @flow
 */

import db from '../config';
import { AUTH, ELEMENTS } from '../kinds';

// Common Types are defined here

type ErrorNotFound = {
  statusCode: 404,
  error: string,
};

type ErrorDatabase = {
  statusCode: 500,
  error: string,
};

/** ************************************************************************ */

type CreatePayload = {|
  userId: number,
|};

type CreateSuccess = {
  statusCode: 201,
  message: string,
};

/**
 * This function will create a admin
 *
 * @async
 * @function create
 * @param {object} payload
 * @param {number} payload.userId -Id of the User
 * @returns {Promise}
 *
 * @example
 * createAdmin({ userId: 1234567890123456 })
 *    .then(res => console.log(res))
 *    .catch(error => console.error(error));
 */
export async function create({
  userId,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const key = db.key([AUTH, userId]);

    const data = {
      admin: true,
      userId: db.key([ELEMENTS, userId]),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createAdmin({ userId: 1134567891223446 })
//   .then(res => console.log(res))
//   .catch(error => console.error(error));

/** ************************************************************************ */

type GetAllSuccess = {
  statusCode: 200,
  message: 'Success',
  payload: Array<{ admin: Boolean, userId: number }>,
};

/**
 * This function will get all admin
 * 
 * @async
 * @function getAll
 * @returns {Promise}

 * @example
 * 
 * getAllAdmin()
 *    .then(res => console.log(res))
 *    .catch(error => console.error(error));
 */
export async function getAll(): Promise<GetAllSuccess | ErrorDatabase> {
  try {
    const query = db.createQuery(AUTH).filter('admin', '=', true);

    const [admins] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: admins.map(({ userId, admin }) => ({
        admin,
        userId: parseInt(userId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAll()
//   .then(res => console.log(res))
//   .catch(error => console.error(error));

/** ************************************************************************ */

type GetSucess = { statusCode: 200, message: string };

/**
 * This function will get a admin
 * @async
 * @function get
 * @param {Object} payload
 * @param {number} payload.userId -Id of the User
 * @returns {Promise}
 *
 * @example
 * get({ userId: 1134567891223446 })
 *    .then(res => console.log(res))
 *    .catch(error => console.error(error));
 */
export async function get({
  userId,
}: CreatePayload): Promise<GetSucess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([AUTH, userId]);

    const [admin] = await db.get(key);

    if (!admin || admin.admin !== true) {
      return {
        statusCode: 404,
        error: 'Admin Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Admin Found',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// get({ userId: 1134567891223446 })
//   .then(res => console.log(res))
//   .catch(error => console.error(error));

/** ************************************************************************ */

type DeleteNoteSuccess = { statusCode: 200, message: string };

/**
 * This function will delete a admin
 *
 * @async
 * @function deleteAdmin
 * @param {Object} payload
 * @param {number} payload.userId -Id of the User
 * @returns {Promise}
 *
 * @example
 * deleteAdmin({ userId: 1134567891223446 })
 *    .then(res => console.log(res))
 *    .catch(error => console.error(error));
 */
export async function deleteAdmin({
  userId,
}: CreatePayload): Promise<DeleteNoteSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([AUTH, userId]);

    const [admin] = await db.delete(key);

    if (admin.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Admin not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Successfully Deleted',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteAdmin({ userId: 1134567891223446 })
//   .then(res => console.log(res))
//   .catch(error => console.error(error));
