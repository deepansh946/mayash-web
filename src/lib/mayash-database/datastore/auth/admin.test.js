/**
 * This file contain all the test functions of admin.js
 */
/* eslint-disable no-undef */

import { create, get, getAll, deleteAdmin } from './admin';

// Test User Id
const userId = 1234567890123456;

describe('Database Datastore Auth for Admins', () => {
  // Admin Create Tests are Defined here.

  test('Create Admin Success', async () => {
    expect.assertions(1);

    const res = await create({ userId });

    expect(res).toMatchObject({
      statusCode: 201,
      message: 'Success',
    });
  });

  // // if the promise will resolve, the test will fail.
  // test('create admin error', async () => {
  //   expect.assertions(1);

  //   await expect(create({ userId })).rejects.toHaveProperty(
  //     'statusCode',
  //     500,
  //   );
  // });

  test('Get Admin Found', async () => {
    expect.assertions(1);

    const res = await get({ userId });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Admin Found',
    });
  });

  test('Get Admin Not Found', async () => {
    expect.assertions(1);

    const res = await get({ userId: userId + 1 });

    expect(res).toMatchObject({ statusCode: 404, error: 'Admin Not Found' });
  });

  test('Get All Admin', async () => {
    expect.assertions(1);

    const res = await getAll();

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
      // payload: expect.arrayContaining({ userId: expect.toBe(number) }),
    });
  });

  test('Delete a Admin', async () => {
    expect.assertions(1);

    const res = await deleteAdmin({ userId });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Successfully Deleted',
    });
  });
});
