/**
 * This file contains all the variables and functions related to courses
 *
 * @format
 */

import db from './config';
import { ELEMENTS, COURSES } from './kinds';

/**
 * getCourse will give course by it's courseId.
 * @async
 * @function getCourse
 * @param {Object} payload - courseId
 * @param {number} payload.courseId - courseId
 * @return {Promise}
 *
 * @example
 * getCourse({ courseId: 4661929301770240 })
 *    .then(res => console.log(res))
 *    .catch(err => console.log(err));
 */
export async function getCourse({ courseId }) {
  try {
    const key = db.key([COURSES, courseId]);

    const [course] = await db.get(key);

    if (!course) {
      return {
        statusCode: 404,
        error: 'Course Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Course Found',
      payload: {
        ...course,
        authorId: parseInt(course.authorId.id, 10),
        courseId,
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// getCourse({ courseId: 4661929301770240 })
//   .then(res => console.log(res))
//   .catch(err => console.log(err));

/**
 * getCourses will display a course by it's Id.
 * @async
 * @function getCourses
 * @param {Object} payload - id
 * @param {number} payload.id
 * @return {Promise}
 *
 * @example
 *
 * getCourses({
 *  id: 6086896371367936,
 *  next: 'ClEKJwoJdGltZXN0YW1wEhoaGDIwMTctMTItMzBUMDc6NDU6MzkuNj'
 *  + 'U4WhIiagptYXlhc2gtd2VichQLEgdjb3Vyc2VzGICAgICAxIYLDBgAIAE=',
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.error(e));
 */
export async function getCourses({ id, next }) {
  try {
    const query = db
      .createQuery(COURSES)
      .filter('authorId', '=', db.key([ELEMENTS, id]))
      .limit(10)
      .order('timestamp', {
        descending: true,
      });

    if (next) {
      query.start(next);
    }

    const [courses, info] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Courses found',
      payload: courses.map((c) => ({
        ...c,
        courseId: parseInt(c.courseId.id, 10),
        authorId: id,
      })),
      next:
        info.moreResults !== db.NO_MORE_RESULTS ? info.endCursor : undefined,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// getCourses({
//   id: 6086896371367936,
//   next: 'ClEKJwoJdGltZXN0YW1wEhoaGDIwMTctMTItMzBUMDc6NDU6MzkuNjU4Wh'
//    + 'IiagptYXlhc2gtd2VichQLEgdjb3Vyc2VzGICAgICAxIYLDBgAIAE=',
// })
//   .then(e => console.log(e))
//   .catch(e => console.error(e));

/**
 * createCourse will create a course by Id.
 * @async
 * @function createCourse
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.title -
 * @param {boolean} payload.privacy - True(Private) | False(Public)
 * @param {number} payload.price - price of course in INR
 * @return {Promise}
 *
 * @example
 * createCourse({
 *   id: 6086896371367936,
 *   title: 'Introduction to JavaScript'),
 *   privacy: false,
 *   price: 0,
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function createCourse({ id, title, privacy = false, price = 0 }) {
  try {
    const [keys] = await db.allocateIds(db.key(COURSES), 1);

    const key = keys[0];
    const authorId = id;
    const courseId = parseInt(key.id, 10);

    const data = {
      authorId: db.key([ELEMENTS, id]),
      courseId: key,
      title,
      privacy,
      price,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Course created',
      payload: { authorId, courseId, privacy, price },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// createCourse({
//   id: 6086896371367936,
//   title: 'Introduction to JavaScript Part 2',
//   privacy: false,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * updateCourse will update a course by it's Id.
 * @async
 * @function updateCourse
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus - draft.js raw data object
 * @param {Object[]} payload.courseModules -
 * @param {number} payload.courseModules[].moduleId -
 * @param {boolean} payload.privacy - True(Private) | False(Public)
 * @param {number} payload.price - Price of course in INR
 * @return {Promise}
 *
 * @example
 *
 * updateCourse({
 *  courseId: 4661929301770240,
 *  title: 'Introduction to JavaScript',
 *  description: 'Introduction to JavaScript',
 *  syllabus: {}, // draft.js rawState data.
 *  courseModules: [
 *    { moduleId: 12345 },
 *    { moduleId: 12346 },
 *    { moduleId: 12347 },
 *  ],
 * privacy: true,
 * price: 200,
 * rating: 4
 * })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 *
 * @todo add course level
 */
export async function updateCourse({
  courseId,
  title,
  description,
  syllabus,
  courseModules,
  privacy,
  price,
  rating,
}) {
  try {
    const key = db.key([COURSES, courseId]);

    const [course] = await db.get(key);

    if (!course) {
      return {
        statusCode: 404,
        error: 'Course Not Found.',
      };
    }

    let data = { ...course };

    if (title) {
      data = { ...data, title };
    }

    if (description) {
      data = { ...data, description };
    }

    if (syllabus) {
      data = { ...data, syllabus };
    }

    if (courseModules) {
      data = { ...data, courseModules };
    }

    if (typeof privacy !== 'undefined') {
      data = { ...data, privacy };
    }

    if (price) {
      data = { ...data, price };
    }

    if (rating) {
      data = { ...data, rating };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Course updated.',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * Deleting a particular course with the help of course id
 * @async
 * @function deleteCourse
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @return {Promise}
 *
 * @example
 * deleteCourse({ courseId: 6298002603900928 })
 *   .then(res => console.log(res))
 *   .catch(e => console.error(e));
 *
 * @todo delete all modules also
 * @todo delete all discussions data
 * @todo delete all test yourself data
 * @todo delete all feedback
 */
export async function deleteCourse({ courseId }) {
  try {
    const key = db.key([COURSES, courseId]);

    const [res] = await db.delete(key);

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Course Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Course Deleted successfully.',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteCourse({ courseId: 6298002603900928 })
//   .then(res => console.log(res))
//   .catch(e => console.error(e));
