/**
 * @format
 * @flow
 */

import db from '../config';
import { ELEMENTS, POSTS } from '../kinds';

// Common Types are defined here

type ErrorNotFound = {
  statusCode: 404,
  error: string,
};

type ErrorDatabase = {
  statusCode: 500,
  error: string,
};

type Success = {
  statusCode: 200,
  message: string,
};

/** ************************************************************************ */

type GetPayload = {|
  postId: number,
|};

type GetSuccess = {|
  statusCode: 200,
  message: string,
  payload: {
    postId: number,
    postType: 'article',
    authorId: number,
    title: string,
    timestamp: string,
  },
|};

/**
 * get() will give a post by it's postId.
 * @todo: every time this function is called, increase the counter
 * of post view in database.
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @returns {Promise}
 *
 * @example
 *
 * get({ postId: 5910974510923776 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function get({
  postId,
}: GetPayload): Promise<GetSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([POSTS, postId]);

    const [post] = await db.get(key);

    if (!post) {
      return {
        statusCode: 404,
        error: 'Post Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        ...post,
        postId,
        authorId: parseInt(post.authorId.id, 10),
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// get({ postId: 5066549580791808 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type GetAllPayload = {|
  id: number,
  next: string,
|};

type GetAllSuccess = {|
  statusCode: 200,
  message: string,
  payload: Array<{
    postId: number,
    authorId: number,
    postType: 'article',
    title: string,
    timestamp: string,
  }>,
  next?: string,
|};

/**
 * getAll() will give a post by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.id
 * @param {string} payload.next
 *
 * @example
 *
 * getAll({
 *    id: 5963751069057024,
 *    next: 'CksKIwoJdGltZXN0YW1wEhYaFDIwMTctMTItMjJUMTE6Mjc6MTJaEiBqCm1heWFzaC13ZWJyEgsSBXBvc3RzGICAgICA4KkIDBgAIAE==',
 *  })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({
  id,
  next,
}: GetAllPayload): Promise<GetAllSuccess | ErrorDatabase> {
  try {
    const query = db
      .createQuery(POSTS)
      .filter('authorId', '=', db.key([ELEMENTS, id]))
      // limiting the result to 10 responses
      .limit(10)
      .order('timestamp', {
        descending: true,
      });

    if (next) {
      query.start(next);
    }

    const [posts, info] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: posts.map((p) => ({
        ...p,
        postId: parseInt(p.postId.id, 10),
        authorId: id,
      })),
      next:
        info.moreResults !== db.NO_MORE_RESULTS ? info.endCursor : undefined,
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({
//   id: 123,
//   next:
//     'CksKIwoJdGltZXN0YW1wEhYaFDIwMTctMTItMjJUMTE6Mjc6MTJaE' +
//     'iBqCm1heWFzaC13ZWJyEgsSBXBvc3RzGICAgICA4KkIDBgAIAE==',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type CreatePayload = {|
  id: number,
  postType: string,
  title: string,
  privacy: boolean,
|};

type CreateSuccess = {|
  statusCode: 201,
  message: string,
  payload: {
    postId: number,
    authorId: number,
    postType: string,
    title: string,
    timestamp: string,
  },
|};

/**
 * This function will create the the post by id
 * @async
 * @function create
 * @param {Object} payload
 * @param {number} payload.id - id
 * @param {string} payload.postType - 'article' only
 * @param {string} payload.title - title
 * @param {boolean} payload.privacy - True: Private, False: Public
 * @returns {Promise}
 *
 * @example
 *
 * create({ id: 123, postType: 'article', title: 'any', privacy: false })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function create({
  id,
  postType,
  title,
  privacy,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const [keys] = await db.allocateIds(db.key(POSTS), 1);

    const key = keys[0];

    const data = {
      postId: key,
      authorId: db.key([ELEMENTS, id]),
      postType,
      title,
      privacy: false,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const res = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...data,
        authorId: id,
        postId: parseInt(key.id, 10),
      },
    };
  } catch (error) {
    console.log(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// create({
//   id: 123,
//   postType: 'article',
//   title: 'this is a post title',
//    privacy: false,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type UpdatePayload = {|
  postId: number,
  title: string,
  description: string,
  cover: string,
  privacy: boolean,
  keywords: Array<string>,
  data: mixed,
|};

/**
 * This function will update the post
 * @async
 * @function update
 * @param {Object} payload - postId , payload
 * @param {number} payload.postId
 * @param {string} payload.title
 * @param {string} payload.description
 * @param {string} payload.data
 * @param {cover} payload.cover
 * @param {boolean} payload.privacy - Either false or true
 * @param {Object[]} payload.keywords - Post keywords
 * @returns {Promise}
 *
 * @example
 *
 * update({
 *  postId: 5224879255191552,
 *  title: 'hello',
 *  description: 'this is the description of the post',
 * })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function update({
  postId,
  title,
  description,
  privacy,
  data,
  cover,
  keywords,
}: UpdatePayload): Promise<Success | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([POSTS, postId]);

    const [post] = await db.get(key);

    if (!post) {
      return { statusCode: 404, error: 'Post Not Found' };
    }

    let postData = { ...post };

    if (title) {
      postData = { ...postData, title };
    }

    if (description) {
      postData = { ...postData, description };
    }

    if (data) {
      postData = { ...postData, data };
    }

    if (cover) {
      postData = { ...postData, cover };
    }

    if (typeof privacy !== 'undefined') {
      postData = { ...postData, privacy };
    }

    if (keywords) {
      postData = { ...postData, keywords };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: postData });
    /* eslint-enable no-unused-vars */

    return { statusCode: 200, message: 'Update successfully' };
  } catch (error) {
    console.error(error);

    return { statusCode: 500, error: 'Database Server Error' };
  }
}

// update({
//   postId: 4936875928190976,
//   title: 'hello',
//   description: 'this is the description of the post',
//   privacy: true,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type DeletePayload = {|
  postId: number,
|};

/**
 * This function will delete the post by postId
 * @async
 * @function deletePost
 * @param {Object} payload
 * @param {number} payload.postId
 * @returns {Promise}
 *
 * @example
 * deletePost({ postId: 5910974510923776 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function deletePost({
  postId,
}: DeletePayload): Promise<Success | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([POSTS, postId]);

    const [res] = await db.delete(key);

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Post Not found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Successfully deleted',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deletePost({ postId: 5910974510923776 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
