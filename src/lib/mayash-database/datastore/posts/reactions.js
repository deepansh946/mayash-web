/**
 * @format
 * @flow
 */

import db from '../config';
import { ELEMENTS, POSTS, POST_REACTIONS } from '../kinds';

// Comment Type are defined here

type ErrorNotFound = {
  statusCode: 404,
  error: string,
};

type ErrorDatabase = {
  statusCode: 500,
  error: string,
};

/** ************************************************************************ */

type GetPayload = {
  postId: number,
  userId: number,
};

type GetSuccess = {|
  statusCode: 200,
  message: 'Success',
  payload: {
    key: number,
    reaction: string,
    userId: number,
    postId: number,
  },
|};
/**
 * This function will get the reaction of a post
 * @async
 * @function getPostReaction
 * @param {object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.userId -
 * @return {Promise} -
 *
 * @example
 *
 * getPostReaction({
 * userId: 5629499534213120,
 * postId: 4705085133160448,
 * })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getPostReaction({
  postId,
  userId,
}: GetPayload): Promise<GetSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const query = db
      .createQuery(POST_REACTIONS)
      .hasAncestor(db.key([POSTS, postId, ELEMENTS, userId]))
      .limit(1);

    const [reactions] = await db.runQuery(query);

    if (reactions.length === 0) {
      return {
        statusCode: 404,
        error: 'Reaction Not Found.',
      };
    }

    const { key, reaction } = reactions[0];

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        key: parseInt(key.id, 10),
        reaction,
        userId,
        postId,
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getPostReaction({
//   userId: 5629499534213120,
//   postId: 4705085133160448,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type GetAllPayload = {|
  postId: number,
|};

type GetAllSuccess = {
  statusCode: 200,
  message: 'Success',
  payload: {
    key: number,
    reaction: string,
    userId: number,
    postId: number,
  },
};

/**
 * This function will get all the post reaction of a post
 * @async
 * @function getAllPostReaction
 * @param {object} payload -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 *
 * getAllPostReaction({
 * postId: 4705085133160448,
 * })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getAllPostReaction({
  postId,
}: GetAllPayload): Promise<GetAllSuccess | ErrorDatabase> {
  try {
    const query = db
      .createQuery(POST_REACTIONS)
      .hasAncestor(db.key([POSTS, postId]))
      .limit(10);

    const [reactions] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: reactions.map(({ key, userId, reaction }) => ({
        key: parseInt(key.id, 10),
        reaction,
        userId: parseInt(userId.id, 10),
        postId,
      })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllPostReaction({
//   postId: 4705085133160448,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type CreatePayload = {|
  postId: number,
  userId: number,
  reaction: string,
|};

type CreateSuccess = {|
  statusCode: 201,
  message: 'Success',
  payload: {
    key: number,
    postId: number,
    reaction: string,
    userId: number,
  },
|};

/**
 * This function will add a like to a post with postId by user with userId.
 * @async
 * @function createPostReaction
 * @param {object} payload -
 * @param {number} payload.postId - postId
 * @param {number} payload.userId - id in elements kind with
 * elementType='user' only.
 * @return {Promise} -
 *
 * @example
 *
 * createPostReaction({
 *  userId: 5629499534213120,
 *  postId: 4705085133160448,
 *  reaction: 'like',
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.error(e));
 */
export async function createPostReaction({
  postId,
  userId,
  reaction,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const { statusCode, error, payload } = await getPostReaction({
      postId,
      userId,
    });

    if (statusCode >= 500) {
      return { statusCode, error };
    }

    if (statusCode !== 200) {
      let key = db.key([POSTS, postId, ELEMENTS, userId, POST_REACTIONS]);
      const [keys] = await db.allocateIds(key, 1);

      key = keys[0];

      const data = {
        key,
        userId: db.key([ELEMENTS, userId]),
        reaction,
      };

      /* eslint-disable no-unused-vars */
      const [res] = await db.save({ key, data });
      /* eslint-disable no-unused-vars */

      return {
        statusCode: 201,
        message: 'Success',
        payload: {
          key: parseInt(key.id, 10),
          postId,
          userId,
          reaction,
        },
      };
    }

    const { key: keyId } = payload;
    const key = db.key([
      POSTS,
      postId,
      ELEMENTS,
      userId,
      POST_REACTIONS,
      keyId,
    ]);

    const data = {
      key,
      userId: db.key([ELEMENTS, userId]),
      reaction,
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-disable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        key: parseInt(key.id, 10),
        postId,
        userId,
        reaction,
      },
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createPostReaction({
//   userId: 5629499534213120,
//   postId: 4705085133160448,
//   reaction: 'like',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.error(e));

/** ************************************************************************ */

type DeletePayload = {|
  postId: number,
  userId: number,
  keyId: number,
|};

type DeleteSuccess = {|
  statusCode: 200,
  message: 'Success',
|};

/**
 * this function will delete a post reaction
 * @async
 * @function deletePostReaction
 * @param {object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.userId -
 * @param {number} payload.keyId -
 * @return {Promise} -
 *
 * @example
 * deletePostReaction({
 *  userId: 5629499534213120,
 *  postId: 4705085133160448,
 *  keyId: 6491516650389504,
 * })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function deletePostReaction({
  postId,
  userId,
  keyId,
}: DeletePayload): Promise<DeleteSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([
      POSTS,
      postId,
      ELEMENTS,
      userId,
      POST_REACTIONS,
      keyId,
    ]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-disable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deletePostReaction({
//   userId: 5629499534213120,
//   postId: 4705085133160448,
//   keyId: 6491516650389504,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
