/**
 * @format
 * @flow
 */

import db from '../config';
import { ELEMENTS, POSTS, POST_COMMENTS } from '../kinds';

// Comment Type are defined here

type ErrorNotFound = {
  statusCode: 404,
  error: 'Not Found.',
};

type ErrorDatabase = {
  statusCode: 500,
  error: 'Database Server Error.',
};

/** ************************************************************************ */

type CreatePayload = {|
  postId: number,
  userId: number,
  data: any,
|};

type CreateSuccess = {|
  statusCode: 201,
  message: 'Success',
  payload: {
    postId: number,
    commentId: number,
    authorId: number,
    data: any,
    timestamp: string,
  },
|};

/**
 * This function will add a like to a post with postId by user with userId.
 * @async
 * @function create
 * @param {object} payload -
 * @param {number} payload.postId - postId
 * @param {number} payload.userId - id in elements kind with
 * elementType='user' only.
 * @param {Object} payload.data - comment title
 * @return {Promise}
 *
 * @example
 *
 * create({ postId: 123, userId: 123, data: 'asd' })
 *  .then((e) => console.log(e))
 *  .catch((e) => console.log(e));
 */
export async function create({
  postId,
  userId,
  data,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const [keys] = await db.allocateIds(
      db.key([POSTS, postId, POST_COMMENTS]),
      1,
    );

    const key = keys[0];

    const postData = {
      commentId: key,
      postId: db.key([POSTS, postId]),
      authorId: db.key([ELEMENTS, userId]),
      data,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: postData });
    /* eslint-disable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...postData,
        authorId: userId,
        postId,
        commentId: parseInt(key.id, 10),
        data,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({
//   postId: 1234567890,
//   userId: 1234567890,
//   data: 'Hello',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type GetPayload = {|
  postId: number,
  commentId: number,
|};

type GetSuccess = {|
  statusCode: 200,
  message: 'Success',
  payload: {
    postId: number,
    commentId: number,
    authorId: number,
    title: string,
    timestamp: string,
  },
|};

/**
 * This function will get post's comment of a user
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.commentId -
 * @return {Promise} -
 *
 * @example
 *
 * get({
 *  postId: 123456789012,
 *  commentId: 4766520345362432,
 * })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function get({
  postId,
  commentId,
}: GetPayload): Promise<GetSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([POSTS, postId, POST_COMMENTS, commentId]);

    const [comment] = await db.get(key);

    if (!comment) {
      return {
        statusCode: 404,
        error: 'Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        ...comment,
        postId,
        authorId: parseInt(comment.authorId.id, 10),
        commentId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({
//   postId: 123456789012,
//   commentId: 4766520345362432,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type GetAllPayload = {|
  postId: number,
|};

type GetAllSuccess = {|
  statusCode: 200,
  message: 'Success',
  payload: {
    postId: number,
    commentId: number,
    authorId: number,
    title: string,
    timestamp: string,
  },
|};

/**
 * This function will get all post comment
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 *
 * getAll({
 *  postId: 4705085133160448,
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 *
 */
export async function getAll({
  postId,
}: GetAllPayload): Promise<GetAllSuccess | ErrorDatabase> {
  try {
    const query = db
      .createQuery(POST_COMMENTS)
      .hasAncestor(db.key([POSTS, postId]))
      .limit(10);

    const [comments] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: comments.map(({ commentId, authorId, ...rest }) => ({
        ...rest,
        postId,
        commentId: parseInt(commentId.id, 10),
        authorId: parseInt(authorId.id, 10),
      })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// getAll({ postId: 1234567890 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type UpdatePayload = {|
  postId: number,
  userId: number,
  commentId: number,
  title: string,
|};

type UpdateSuccess = {|
  statusCode: 200,
  message: 'Success',
  payload: {
    postId: number,
    commentId: number,
    authorId: number,
    title: string,
    timestamp: string,
  },
|};

/**
 * This function will update the post comment
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.postId - postId
 * @param {number} payload.userId - author id
 * @param {number} payload.commentId -
 *
 * @return {Promise} -
 *
 * @example
 *
 * update({
 *  postId: 123456789012,
 *  userId: 123456789012,
 *  commentId: 5928566696968192,
 *  title: 'hello1',
 * })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function update({
  postId,
  commentId,
  title,
}: UpdatePayload): Promise<UpdateSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([POSTS, postId, POST_COMMENTS, commentId]);

    const [comment] = await db.get(key);

    if (!comment) {
      return {
        statusCode: 404,
        error: 'Not Found.',
      };
    }

    const data = {
      ...comment,
      title,
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-disable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        ...comment,
        postId,
        commentId,
        authorId: parseInt(comment.authorId.id, 10),
        title,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// commentId: 6210041673678848,

// update({
//   postId: 123456789012,
//   commentId: 5928566696968192,
//   title: 'hello1',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type DeletePayload = {|
  postId: number,
  commentId: number,
|};

type DeleteSuccess = {|
  statusCode: 200,
  message: 'Success',
|};

/**
 * this function will delete Post's comment
 * @async
 * @function deletePostComment
 * @param {object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.commentId -
 * @return {Promise} -
 *
 * @example
 *
 * deletePostComment({
 *  postId: 123456789012,
 *  commentId: 5928566696968192,
 * })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function deletePostComment({
  postId,
  commentId,
}: DeletePayload): Promise<DeleteSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([POSTS, postId, POST_COMMENTS, commentId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-disable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deletePostComment({
//   postId: 123456789012,
//   commentId: 5928566696968192,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
