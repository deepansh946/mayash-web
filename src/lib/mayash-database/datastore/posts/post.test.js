/**
 * This file contain all the test functions of index.js
 *
 * @format
 */
/* eslint-disable no-undef */

// All the function are imported
import { create, get, getAll, deletePost, update } from './index';

// Sample Data

const id = 123456789012;
const postType = 'article';
const title = 'title of the post';
const description = 'desription of the post';

describe('Database Datastore POSTS for post', () => {
  // All the test are defined here

  // test for creating a post
  test('Create Post Success', async () => {
    expect.assertions(1);

    const res = await create({ id, postType, title });

    expect(res).toMatchObject({
      statusCode: 201,
      message: 'Success',
    });
  });

  // test for getting a post as first we have to create a post to get that post

  test('Get post', async () => {
    expect.assertions(1);

    const res = await create({ id, postType, title });

    const res1 = await get({ postId: res.payload.postId });

    expect(res1).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });

  // test for getting all post

  test('Get All the post', async () => {
    expect.assertions(1);

    const res = await getAll({ id });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });

  // test for update a post

  test('Update a post', async () => {
    expect.assertions(1);

    const res = await create({ id, postType, title });

    const res1 = await update({
      postId: res.payload.postId,
      title,
      description,
      data: {},
    });

    expect(res1).toMatchObject({
      statusCode: 200,
      message: 'Update successfully',
    });
  });

  // test for delete a post

  test('Delete a post', async () => {
    expect.assertions(1);

    const res = await create({ id, postType, title });

    const res1 = await deletePost({ postId: res.payload.postId });

    expect(res1).toMatchObject({
      statusCode: 200,
      message: 'Successfully deleted',
    });
  });
});
