/**
 * This file contain all the test function of the reaction.js
 *
 * @format
 */
/* eslint-disable no-undef */

// All the function are imported
import {
  createPostReaction,
  getPostReaction,
  getAllPostReaction,
  deletePostReaction,
} from './reactions';

// Sample data
const postId = 123456789012;
const reaction = 'like';
const userId = 123456789012;

describe('Database Datastore POSTS for reaction', () => {
  // All the test are described here

  // test for creating a reaction
  test('Creating a Post Reaction', async () => {
    expect.assertions(5);

    const res = await createPostReaction({ postId, userId, reaction });

    expect(res).toMatchObject({
      statusCode: 201,
      message: 'Success',
    });

    expect(res).toHaveProperty('payload.key', expect.any(Number));
    expect(res).toHaveProperty('payload.postId', expect.any(Number));
    expect(res).toHaveProperty('payload.reaction', expect.any(String));
    expect(res).toHaveProperty('payload.userId', expect.any(Number));
  });

  // test for getting a post reaction

  test('Getting a Post Reaction', async () => {
    expect.assertions(1);

    const res = await getPostReaction({ postId, userId });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });

  // test for getting all the post
  test('Getting all the Post Reaction', async () => {
    // expect.assertions(5);

    const res = await getAllPostReaction({ postId });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });

    // matching payload
    expect(res.payload).toMatchObject([
      {
        key: expect.any(Number),
        reaction: expect.stringContaining('like'), // expect.any(String),
        userId: expect.any(Number),
        postId: expect.any(Number),
      },
    ]);
  });

  // test for Deleting a post reaction
  test('Deleting a Post Reaction', async () => {
    expect.assertions(1);

    const res = await createPostReaction({ postId, userId, reaction });

    const res1 = await deletePostReaction({
      postId,
      userId,
      keyId: res.payload.key,
    });

    expect(res1).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });
});
