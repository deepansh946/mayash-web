/**
 * This file conatin all the test function for the comments.js
 *
 * @format
 */
/* eslint-disable no-undef */

// All the database function are imported here
import {
  createPostComment,
  getPostComment,
  getAllPostComment,
  updatePostComment,
  deletePostComment,
} from './comments';

// Sample data
const postId = 123456789012;
const id = 123456789012;
const title = 'title of the post';

describe('Database Datastore POSTS for comments', () => {
  // All the test are defined here

  // test for creating the post comment
  test('create a post comment', async () => {
    expect.assertions(1);

    const res = await createPostComment({ postId, id, title });

    expect(res).toMatchObject({
      statusCode: 201,
      message: 'Success',
    });
  });

  // // test for getting a post
  // test('Get a post', async () => {
  //   expect.assertions(1);

  //   // const res = await createPostComment({ postId, id, title });

  //   const res1 = await getPostComment({
  //     userId: 123456789012,
  //     postId: 123456789012,
  //     commentId: 6693826789900288, // res.payload.commentId,
  //   });

  //   expect(res1).toMatchObject({
  //     statusCode: 200,
  //     message: 'Success',
  //   });
  // });

  // test for getting all the post
  test('Get All the post', async () => {
    expect.assertions(1);

    const res = await getAllPostComment({ postId });

    console.log(1);

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });

  // // test for deleting a post
  // test('Deleting a post', async () => {
  //   expect.assertions(1);

  //   const res = await createPostComment({ id, postId, title });

  //   const res1 = await deletePostComment({
  //     postId,
  //     userId: id,
  //     commentId: res.payload.commentId,
  //   });

  //   expect(res1).toMatchObject({
  //     statusCode: 200,
  //     message: 'Success',
  //   });
  // });

  // // test for updating the post
  // test('Updating the post', async () => {
  //   expect.assertions(1);

  //   const res = await createPostComment({ postId, id, title });

  //   const res1 = await updatePostComment({
  //     postId,
  //     id,
  //     commentId: res.payload.commentId,
  //     title: 'hello',
  //   });

  //   expect(res1).toMatchObject({
  //     statusCode: 200,
  //     message: 'Success',
  //   });
  // });
});
