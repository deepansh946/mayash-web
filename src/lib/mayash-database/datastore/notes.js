/**
 * This file contain all the function related to notes of a user
 *
 * @format
 * @flow
 */

import db from './config';
import { NOTES, ELEMENTS } from './kinds';

type ErrorNotFound = {
  statusCode: 404,
  error: string,
};

type ErrorDatabase = {
  statusCode: 500,
  error: string,
};

type CreatePayload = {
  id: number,
  noteType: 'note',
  title: string,
};

type CreateSuccess = {
  statusCode: 201,
  message: string,
  payload: {
    id: number,
    noteId: number,
  },
};

/**
 * This function will create a note to the user
 * @async
 * @function create
 * @param {Object} payload
 * @param {number} payload.id - id of the user
 * @param {string} payload.noteType - todo/note
 * @param {string} payload.title - title of Note
 * @returns {Promise}
 *
 * @example
 *
 * createNote({
 *  id: 5483539365625856,
 *  noteType: 'note',
 *  title: 'Third Note',
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function createNote({
  id,
  noteType,
  title,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const [keys] = await db.allocateIds(db.key([ELEMENTS, id, NOTES]), 1);

    const key = keys[0];

    const data = {
      noteId: key,
      noteType,
      title,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Note created',
      payload: {
        id,
        noteId: parseInt(key.id, 10),
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createNote({
//   id: 5483539365625856,
//   noteType: 'note',
//   title: 'Third Note',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

type updatePayload = {
  id: number,
  noteId: number,
  noteType: 'note',
  title: string,
};

/**
 * This will update the note of the user
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.id-id of the user
 * @param {number} payload.noteId-id of the note of a user
 * @param {string} payload.noteType - type of the note
 * @param {string} payload.title-title of the note
 * @returns {Promise}
 *
 * @example
 *
 * updateNote({
 *  id: 5483539365625856,
 *  noteId: 6192449487634432,
 *  noteType: 'note',
 *  title: 'hello',
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function updateNote({
  id,
  noteId,
  noteType,
  title,
}: updatePayload): Promise<
  | {
      statusCode: 200,
      message: string,
    }
  | ErrorNotFound
  | ErrorDatabase,
> {
  try {
    const key = db.key([ELEMENTS, id, NOTES, noteId]);

    const [note] = await db.get(key);

    if (!note) {
      return {
        statusCode: 404,
        error: 'Note not found',
      };
    }

    const data = { ...note, title, noteType };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Note updated',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// updateNote({
//   id: 5483539365625856,
//   noteId: 6192449487634432,
//   noteType: 'note',
//   title: 'hello',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

type getPayload = { id: number, noteId: number };

/**
 * This will display a single note of the user
 * using its noteId and userId
 * @async
 * @function get
 * @param {Object} payload
 * @param {number} payload.id - id of the user
 * @param {number} payload.noteId - id of the note of user
 * @returns {Promise}
 *
 * @example
 * getNote({ id: 5483539365625856, noteId: 6192449487634432 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getNote({
  id,
  noteId,
}: getPayload): Promise<
  | {
      statusCode: 200,
      message: string,
      payload: {
        id: number,
        noteId: number,
        title: string,
        timestamp: string,
      },
    }
  | ErrorNotFound
  | ErrorDatabase,
> {
  try {
    const key = db.key([ELEMENTS, id, NOTES, noteId]);

    const [note] = await db.get(key);

    if (!note) {
      return {
        statusCode: 404,
        error: 'Note not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Note found',
      payload: {
        ...note,
        id,
        noteId,
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// getNote({ id: 5483539365625856, noteId: 6192449487634432 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

type deleteNotePayload = { id: number, noteId: number };

/**
 * This function will delete a note of the user
 * @async
 * @function deleteNote
 * @param {Object} payload
 * @param {number} payload.id - id of the user
 * @param {number} payload.noteId - id of the note of user
 * @returns {Promise}
 *
 * @example
 * deleteNote({ id: 5483539365625856, noteId: 6192449487634432 })
 *      .then(e => console.log(e))
 *      .catch(e => console.log(e));
 */
export async function deleteNote({
  id,
  noteId,
}: deleteNotePayload): Promise<
  | {
      statusCode: 200,
      message: string,
    }
  | ErrorNotFound
  | ErrorDatabase,
> {
  try {
    const key = db.key([ELEMENTS, id, NOTES, noteId]);

    const [note] = await db.delete(key);

    if (note.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Note not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Note deleted successfully',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
// deleteNote({ id: 5483539365625856, noteId: 6192449487634432 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

type getAllPayload = { id: number, next?: string };

/**
 * This function will display all the notes of the user
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.id - id of the user
 * @returns {Promise}
 *
 * @example
 *
 * getAllNotes({ id: 5483539365625856 })
 *     .then(e => console.log(e))
 *     .catch(e => console.log(e));
 */
export async function getAllNotes({
  id,
  next,
}: getAllPayload): Promise<
  | {
      statusCode: 200,
      message: string,
      payload: Array<{
        id: number,
        noteId: number,
        noteType: 'note',
        title: string,
      }>,
    }
  | ErrorDatabase,
> {
  try {
    const key = db.key([ELEMENTS, id]);

    const query = db
      .createQuery(NOTES)
      .hasAncestor(key)
      .limit(15)
      .order('timestamp', {
        descending: true,
      });

    if (next) {
      query.start(next);
    }

    const [notes, info] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: notes.map(({ noteId, noteType, title }) => ({
        id,
        noteId: parseInt(noteId.id, 10),
        noteType,
        title,
      })),
      next:
        info.moreResults !== db.NO_MORE_RESULTS ? info.endCursor : undefined,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllNotes({ id: 5483539365625856 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
