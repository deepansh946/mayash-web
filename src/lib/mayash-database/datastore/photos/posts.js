/**
 * This file contains all the functions of photos for posts.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, POSTS, PHOTOS } from '../kinds';

/**
 * getAll will give a post by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.postId
 * @returns {Promise}
 *
 * @example
 * getAll({ postId: 123456789012 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getAll({ postId }) {
  try {
    const query = db
      .createQuery(PHOTOS)
      .hasAncestor(db.key([POSTS, postId]))
      // limiting the result to 5 responses
      .limit(5);

    const [photos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: photos.map((p) => ({
        ...p,
        postId,
        photoId: parseInt(p.photoId.id, 10),
        authorId: parseInt(p.authorId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ postId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This Function will a get a photo by id's of photo and post
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * get({ postId: 123456789012, photoId: 5647091720257536 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ postId, photoId }) {
  try {
    const key = db.key([POSTS, postId, PHOTOS, photoId]);

    const [photo] = await db.get(key);

    if (!photo) {
      return {
        statusCode: 404,
        error: 'Photo Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...photo,
        authorId: parseInt(photo.authorId.id, 10),
        postId,
        photoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ postId: 123456789012, photoId: 5647091720257536 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create a photo
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.id - author id
 * @param {number} payload.postId -
 * @param {string} payload.type - photo extension i.e. (jpg|jpeg|png|gif)
 * @returns {Promise}
 *
 * @example
 * create({ id: 123456789012, postId: 123456789012, type: 'jpg' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ id, postId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([POSTS, postId, PHOTOS]), 1);

    const key = keys[0];
    const authorId = id;
    const photoId = parseInt(key.id, 10);

    const data = {
      photoId: key,
      authorId: db.key([ELEMENTS, id]),
      postId: db.key([POSTS, postId]),
      name: `${photoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, postId, photoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ id: 123456789012, postId: 123456789012, type: 'jpg' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This Function will delete a photo by it id of photo and post
 * @async
 * @function deletePhoto
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * deletePhoto({ postId: 123456789012, photoId: 5647091720257536 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deletePhoto({ postId, photoId }) {
  try {
    const key = db.key([POSTS, postId, PHOTOS, photoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deletePhoto({ postId: 123456789012, photoId: 5647091720257536 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
