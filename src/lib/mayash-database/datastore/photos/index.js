/** @format */

import * as users from './users';
import * as circles from './circles';
import * as posts from './posts';
import * as courses from './courses';

export default {
  users,
  circles,
  posts,
  courses,
};
