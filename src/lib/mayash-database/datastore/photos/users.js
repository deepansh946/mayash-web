/**
 * This file contains all the functions for photos of users.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, PHOTOS } from '../kinds';

/**
 * This function will get all the photo's of a user
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.userId
 * @returns {Promise}
 *
 * @example
 * getAll({ userId: 123456789012 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getAll({ userId }) {
  try {
    const query = db
      .createQuery(PHOTOS)
      .hasAncestor(db.key([ELEMENTS, userId]))
      // limiting the result to 5 responses
      .limit(10);

    const [photos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: photos.map((p) => ({
        ...p,
        photoId: parseInt(p.photoId.id, 10),
        authorId: userId,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ userId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will get a photo of a user
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * get({ userId: 123456789012, photoId: 5084141766836224 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ userId, photoId }) {
  try {
    const key = db.key([ELEMENTS, userId, PHOTOS, photoId]);

    const [photo] = await db.get(key);

    if (!photo) {
      return {
        statusCode: 404,
        error: 'Photo Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...photo,
        authorId: userId,
        photoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// get({ userId: 123456789012, photoId: 5084141766836224 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
/**
 * This function will create the the photo
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.type - photo extension i.e. (jpg|jpeg|png|gif)
 * @returns {Promise}
 *
 * @example
 * create({ userId: 123456789012, type: 'jpg' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ userId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([ELEMENTS, userId, PHOTOS]), 1);

    const key = keys[0];
    const authorId = userId;
    const photoId = parseInt(key.id, 10);

    const data = {
      photoId: key,
      authorId: db.key([ELEMENTS, userId]),
      name: `${photoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, photoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ userId: 123456789012, type: 'jpg' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete a photo of a user
 * @async
 * @function deletePhoto
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * deletePhoto({ userId: 123456789012, photoId: 5084141766836224 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deletePhoto({ userId, photoId }) {
  try {
    const key = db.key([ELEMENTS, userId, PHOTOS, photoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// deletePhoto({ userId: 123456789012, photoId: 5084141766836224 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
