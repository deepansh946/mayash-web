/**
 * This file contains all the functions of photos for courses.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, COURSES, PHOTOS } from '../kinds';

/**
 * This Function will get all the photo's of the course
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @returns {Promise}
 *
 * @example
 * getAll({ courseId: 123456789012 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getAll({ courseId }) {
  try {
    const query = db
      .createQuery(PHOTOS)
      .hasAncestor(db.key([COURSES, courseId]))
      // limiting the result to 5 responses
      .limit(5);

    const [photos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: photos.map((p) => ({
        ...p,
        courseId,
        photoId: parseInt(p.photoId.id, 10),
        authorId: parseInt(p.authorId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ courseId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will get a photo by id's of course and photo
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * get({ courseId: 123456789012, photoId: 4521191813414912 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ courseId, photoId }) {
  try {
    const key = db.key([COURSES, courseId, PHOTOS, photoId]);

    const [photo] = await db.get(key);

    if (!photo) {
      return {
        statusCode: 404,
        error: 'Photo Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...photo,
        authorId: parseInt(photo.authorId.id, 10),
        courseId,
        photoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ courseId: 123456789012, photoId: 4521191813414912 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This Function will create the photo by user under course section
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.id - author Id
 * @param {number} payload.courseId -
 * @param {string} payload.type - photo extention i.e. (jpg|jpeg|png|gif)
 * @returns {Promise}
 *
 * @example
 * create({ id: 123456789012, courseId: 123456789012, type: 'jpg' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ id, courseId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([COURSES, courseId, PHOTOS]), 1);

    const key = keys[0];
    const authorId = id;
    const photoId = parseInt(key.id, 10);

    const data = {
      photoId: key,
      authorId: db.key([ELEMENTS, id]),
      courseId: db.key([COURSES, courseId]),
      name: `${photoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, courseId, photoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// create({ id: 123456789012, courseId: 123456789012, type: 'jpg' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete the photo by courseId,photoId
 * @async
 * @function deletePhoto
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 */
export async function deletePhoto({ courseId, photoId }) {
  try {
    const key = db.key([COURSES, courseId, PHOTOS, photoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }
    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// deletePhoto({ courseId: 123456789012, photoId: 4521191813414912 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
