/**
 * This file contains all the functions of photos for circles
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, PHOTOS } from '../kinds';

/**
 * This function getAll will give all photos by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.circleId
 * @returns {Promise}
 *
 * @example
 * getAll({ circleId: 123456789012 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({ circleId }) {
  try {
    const query = db
      .createQuery(PHOTOS)
      .hasAncestor(db.key([ELEMENTS, circleId]))
      // limiting the result to 5 responses
      .limit(5);

    const [photos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: photos.map((p) => ({
        ...p,
        photoId: parseInt(p.photoId.id, 10),
        authorId: circleId,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ circleId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function get will get a photo by its id
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * get({ circleId: 123456789012, photoId: 6720215068966912 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ circleId, photoId }) {
  try {
    const key = db.key([ELEMENTS, circleId, PHOTOS, photoId]);

    const [photo] = await db.get(key);

    if (!photo) {
      return {
        statusCode: 404,
        error: 'Photo Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...photo,
        authorId: circleId,
        photoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ circleId: 123456789012, photoId: 6720215068966912 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create a photo by circleId
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.type - photo extension i.e. (jpg|jpeg|png|gif)
 * @returns {Promise}
 *
 * @example
 * create({ circleId: 123456789012, type: 'jpg' })
 *    .then((a) => console.log(a))
 *    .catch((a) => console.log(a));
 */
export async function create({ circleId, type }) {
  try {
    const [keys] = await db.allocateIds(
      db.key([ELEMENTS, circleId, PHOTOS]),
      1,
    );

    const key = keys[0];
    const authorId = circleId;
    const photoId = parseInt(key.id, 10);

    const data = {
      authorId: db.key([ELEMENTS, circleId]),
      photoId: key,
      name: `${photoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, photoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// create({ circleId: 123456789012, type: 'jpg' })
//   .then((a) => console.log(a))
//   .catch((a) => console.log(a));

/**
 * This function will delete a photo by its id
 * @async
 * @function deletePhoto
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.photoId -
 * @returns {Promise}
 *
 * @example
 * deletePhoto({ circleId: 123456789012, photoId: 5594315162124288 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deletePhoto({ circleId, photoId }) {
  try {
    const key = db.key([ELEMENTS, circleId, PHOTOS, photoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deletePhoto({ circleId: 123456789012, photoId: 5594315162124288 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
