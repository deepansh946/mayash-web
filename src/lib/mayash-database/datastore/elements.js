/**
 * @format
 * @file This file contains all the functions related to elements
 * @author Himank Barve
 */

import db from './config';
import { ELEMENTS } from './kinds';

// There are two types of elements : elementType = ['user', 'circle']
// Define each of them circle = ['field', 'location', 'org', 'edu']

/**
 * This function will be used to check if Element with 'username'
 * exists or not.
 * @async
 * @function checkUsername
 * @param {string} username -
 * @return {object} -
 *
 * @example
 * checkUsername({ username: 'hbarve' })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function checkUsername({ username }) {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('username', '=', username)
      .limit(1);

    const [elements] = await db.runQuery(query);

    if (elements.length !== 0) {
      return {
        statusCode: 200,
        message: 'Username Taken',
      };
    }

    return {
      statusCode: 404,
      message: 'Not found',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      message: 'Database Server Error.',
    };
  }
}

// checkUsername({ username: 'hbarve' })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * checkEmail function will check is email exists
 * in our database or not.
 * @async
 * @function checkEmail
 * @param {object} payload -
 * @param {string} payload.email -
 * @return {Promise} -
 *
 * @example
 *
 * checkEmail({ email: 'hbarve1@gmail.com' })
 *    .then(e => console.log(e))
 *    .catch(e => console.error(e));
 */
export async function checkEmail({ email }) {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('email', '=', email)
      .limit(1);

    const [elements] = await db.runQuery(query);

    // User field is empty
    if (elements.length === 0) {
      return {
        statusCode: 404,
        error: 'Email Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Email already registered.',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// checkEmail({ email: 'hbarve1@gmail.com' })
//   .then(e => console.log(e))
//   .catch(e => console.error(e));

/**
 * Here we are signing on the website with the help of username
 * @async
 * @function signInByUsername
 * @param {object} payload -
 * @param {string} payload.username -
 * @return {object} -
 */
export async function signInByUsername({ username }) {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('elementType', '=', 'user')
      .filter('username', '=', username)
      .limit(1);

    const [elements] = await db.runQuery(query);

    // User field is empty
    if (elements.length === 0) {
      return {
        statusCode: 404,
        error: 'User Not Found.',
      };
    }

    const {
      id,
      elementType,
      name,
      avatar,
      cover,
      guru,
      email,
      password,
    } = elements[0];

    return {
      statusCode: 200,
      message: 'Sign in using username successfull.',
      payload: {
        id: parseInt(id.id, 10),
        username,
        name,
        elementType,
        avatar,
        cover,
        guru,
        email,
        password,
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will take auth credentials from google auth and check
 * if user already exists or not. if user already exists: it will create
 * an auth token. if user does not exists, it will create one with the
 * passed credentials and issue an new token.
 * @async
 * @function signInByGoogleAuth
 * @param {Object} payload
 * @param {string} payload.googleId - this is a number out of limit from
 * JavaScript, so represented in String.
 * @param {string} payload.email
 * @param {string} payload.name
 * @param {Object} payload.raw - this is an object contains info about user's
 *  data which came from google.
 * @return {Promise}
 */
export async function signInByGoogleAuth({ googleId, email, name, raw }) {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('googleId', '=', googleId)
      .limit(1);

    const [elements] = await db.runQuery(query);

    if (elements.length !== 0) {
      const { id, username, name, email, elementType } = elements[0];

      return {
        statusCode: 200,
        message: 'Successful.',
        payload: {
          id: parseInt(id.id, 10),
          username,
          name,
          email,
          elementType,
        },
      };
    }

    const [keys] = await db.allocateIds(db.key(ELEMENTS), 1);

    const key = keys[0];
    const data = {
      id: key,
      username: `user${key.id}`,
      name,
      email,
      googleId,
      // avatar,
      elementType: 'user',
      googleRaw: raw,
    };

    const [res] = await db.save({ key, data });

    return {
      statusCode: 200,
      message: 'Success',
      payload: { ...data, id: parseInt(key.id, 10) },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 *
 * @since 1.0.0
 * @version 1.0.0
 * @async
 * @function getElements
 * @param {Object} payload -
 * @param {Object[]} payload.ids[] -
 * @param {Object[]} payload.ids[].id -
 * @returns {Promise}
 *
 * @example
 *
 * getElements({
 *  ids: [
 *    4521191813414912,
 *    4538783999459328,
 *    4591560557592576,
 *  ],
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function getElements({ ids }) {
  try {
    const keys = ids.map((id) => db.key([ELEMENTS, id]));

    const [users] = await db.get(keys);

    return {
      statusCode: 200,
      message: 'Success',
      payload: users.map(
        ({
          id,
          username,
          elementType,
          name,
          avatar,
          cover,
          email,
          guru,
          resume,
        }) => ({
          id: parseInt(id.id, 10),
          username,
          elementType,
          name,
          avatar,
          cover,
          email,
          guru,
          resume,
        }),
      ),
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function give an element(user or circle) if exist by it's id.
 * @async
 * @function getElementById
 * @param {Object} payload - This object will contain all the
 * necessary key to find an element.
 * @param {number} payload.id -
 * @returns {Promise}
 *
 * @example
 */
export async function getElementById({ id }) {
  try {
    // create a datastore key.
    const key = db.key([ELEMENTS, id]);

    const [element] = await db.get(key);

    if (!element) {
      return {
        statusCode: 404,
        error: 'Not Found',
      };
    }

    const {
      name,
      username,
      email,
      elementType,
      circleType,
      guru,
      classroom,
      avatar,
      cover,
      resume,
    } = element;

    const payload = {
      elementType,
      circleType,
      id,
      username,
      name,
      avatar,
      cover,
      email,
      guru,
      classroom,
      resume,
    };

    return {
      statusCode: 200,
      message: 'Element with given id found',
      payload,
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * Get Element by username
 * @async
 * @function getElementByUsername
 * @param {Object} payload -
 * @param {string} payload.username -
 * @param {function} callback -
 * @returns {promise}
 *
 */
export async function getElementByUsername({ username }) {
  try {
    const query = db.createQuery(ELEMENTS).filter('username', '=', username);

    const [elements] = await db.runQuery(query);

    if (elements.length === 0) {
      return {
        statusCode: 404,
        error: 'Not Found.',
      };
    }

    const {
      id,
      name,
      email,
      elementType,
      circleType,
      guru,
      classroom,
      avatar,
      cover,
      resume,
    } = elements[0];

    const payload = {
      elementType,
      circleType,
      id: parseInt(id.id, 10),
      username,
      name,
      avatar,
      cover,
      email,
      resume,
      guru,
      classroom,
    };

    return {
      statusCode: 200,
      message: 'Element with given username found',
      payload,
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function will delete on element with it's 'id'.
 * @async
 * @function deleteElementById
 * @param {object} payload
 * @param {string} id
 * @returns {promise}
 *
 * @example
 * deleteElementById({ id: 5629499534213120 })
 *    .then(e =>console.log(e))
 *    .catch(e =>console.log(e));
 */
export async function deleteElementById({ id }) {
  try {
    const [keys] = db.key(['elements', id]);

    const key = keys[0];

    const [res] = await db.delete(key);

    return {
      statusCode: 200,
      message: 'Element successfully deleted',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteElementById({ id: 5629499534213120 })
//   .then(e =>console.log(e))
//   .catch(e =>console.log(e));

/**
 * This function will give all the elements in the database.
 * @param {function} callback
 */
const getAllElements = (callback) => {
  const query = db.createQuery(ELEMENTS);

  db.runQuery(query, (err, elements) => {
    if (err) {
      console.error(err);

      return callback({
        statusCode: 500,
        error: 'Server Error',
      });
    }

    return callback({
      statusCode: 200,
      payload: elements.map((e) => ({ ...e, id: parseInt(e.id.id, 10) })),
    });
  });
};

// getAllElements(e => console.log(e));

/* eslint-disable */
/**
 * This function will delete all the elements in the 'elements' database table.
 * Note: be very carefull while using this function.
 * @param {function} callback
 */
const deleteAllElements = (callback) => {
  getAllElements((response) => {
    if (response.statusCode >= 400) {
      return callback(response);
    }

    response.payload.map((e) => {
      console.log(e);
      // deleteElementById(e.id.id, (result) => {
      //   console.log(result);
      // });
    });

    return callback({
      statusCode: 200,
      message: 'All elements deleted',
    });
  });
};
/* eslint-enable */

/**
 * This function will update Element(user/circle) by it's id.
 * @param {object} payload -
 * @param {number} payload.id -
 * @param {string} payload.name -
 * @param {string} payload.username -
 * @param {string} payload.avatar -
 * @param {function} callback -
 */
export function updateElementById({ id, name, username, avatar }, callback) {
  const key = db.key([ELEMENTS, id]);

  db
    .get(key)
    .then(([element]) => {
      if (!element) {
        callback({
          statusCode: 404,
          error: 'Not Found',
        });
        return;
      }

      let data = { ...JSON.parse(JSON.stringify(element)) };

      if (typeof name === 'string') {
        data = { ...data, name };
      }
      if (typeof username === 'string') {
        data = { ...data, username };
      }
      if (typeof avatar === 'string') {
        data = { ...data, avatar };
      }

      db
        .save({ key, data })
        .then(() => {
          callback({
            statusCode: 200,
            message: 'Element updated by id',
          });
        })
        .catch((err) => {
          console.error(err);
          callback({
            statusCode: 500,
            error: 'Database Server Error.',
          });
        });
    })
    .catch((err) => {
      console.error(err);
      callback({
        statusCode: 500,
        error: 'Database Server Error.',
      });
    });
}

/**
 * This function will update username of elements by its id.
 * This function will also check if the username already exists or not and
 * will update only if it is available.
 * @param {object} payload
 * @param {number} payload.id
 * @param {string} payload.username
 */
export async function updateUsernameById({ id, username }) {
  try {
    const { statusCode } = await checkUsername({ username });

    if (statusCode >= 500) {
      return {
        statusCode,
        error: 'Database Server Error',
      };
    }

    if (statusCode === 200) {
      return {
        statusCode: 400,
        error: 'Username already taken',
      };
    }

    const key = db.key([ELEMENTS, id]);
    const [user] = await db.get(key);

    if (!user) {
      return {
        statusCode: 404,
        error: 'User not found',
      };
    }

    const data = { ...user, username };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-disable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Username updated using id',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// updateUsernameById({id: 6192449487634432, username: 'qwsde'})
//   .then(res => console.log(res))
//   .catch(err => console.error(err));

/**
 * This function will enable classroom feature for element.
 * NOTE: classroom feature is only available for user, edu, org,
 * field. make sure to check it.
 * @param {any} payload -
 * @param {any} callback
 */
export function enableClassroomForElement({ id }, callback) {
  db
    .get([ELEMENTS, id])
    .then((element) => {
      if (!element[0]) {
        callback({
          statusCode: 404,
          message: 'Not Found',
        });
      } else {
        const key = element[0].id;
        const data = {
          ...JSON.parse(JSON.stringify(element[0])),
          classroom: true,
        };

        db
          .save({ key, data })
          .then(() => {
            callback({
              statusCode: 200,
              message: 'Classroom enabled for element',
            });
          })
          .catch((err) => {
            console.error(err);
            callback({
              statusCode: 500,
              message: 'Server Error',
            });
          });
      }
    })
    .catch((error) => {
      console.error(error);
      callback({
        statusCode: 500,
        message: 'Server Error',
      });
    });
}
