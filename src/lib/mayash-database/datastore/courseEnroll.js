/**
 * This file contains all the variables and functions related to courses enrollment
 *
 * @format
 */

import db from './config';
import { COURSES, ELEMENTS, COURSE_ENROLLMENTS } from './kinds';

/**
 * This function is used to enroll the user in a course
 * @async
 * @function
 * @param {number} courseId - Course id
 * @param {number} userId - user id
 * @returns {Promise}
 *
 * @example
 * create({ courseId: 4588741125663256, userId: 4588741125663256 })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function create({ courseId, userId }) {
  try {
    const key = db.key([
      COURSES,
      courseId,
      ELEMENTS,
      userId,
      COURSE_ENROLLMENTS,
      1,
    ]);

    const data = {
      key,
      courseId,
      userId,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function is used to disenroll the user from the course
 * @async
 * @function
 * @param {number} courseId - Course id
 * @param {number} userId - user id
 * @returns {Promise}
 *
 * @example
 * remove({ courseId: 4588741125663256, userId: 4588741125663256 })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function remove({ courseId, userId }) {
  try {
    const key = db.key([
      COURSES,
      courseId,
      ELEMENTS,
      userId,
      COURSE_ENROLLMENTS,
      1,
    ]);

    const [res] = await db.delete(key);

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Enrollment not found',
      };
    }

    return {
      statusCode: 200,
      message: 'User disenrolled successfully',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function is used to check whether the user is
 * enrolled in a particular course or not.
 * @async
 * @function
 * @param {number} courseId - Course id
 * @param {number} userId - user id
 * @returns {Promise}
 *
 * @example
 * get({ courseId: 4588741125663256, userId: 4588741125663256 })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function get({ courseId, userId }) {
  try {
    const key = db.key([
      COURSES,
      courseId,
      ELEMENTS,
      userId,
      COURSE_ENROLLMENTS,
      1,
    ]);

    const [enroll] = await db.get(key);

    if (!enroll) {
      return {
        statusCode: 404,
        error: 'Enrollment not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function is used to get list of all users enrolled in a course
 * @async
 * @function
 * @param {number} courseId - Course id
 * @param {number} userId - user id
 * @returns {Promise}
 *
 * @example
 * getAllByCourseId({ courseId: 4588741125663256 })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function getAllByCourseId({ courseId }) {
  try {
    const key = db.key([COURSES, courseId]);

    const query = db.createQuery(COURSE_ENROLLMENTS).hasAncestor(key);

    const [users] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Users found',
      payload: users.map(({ userId, timestamp }) => ({
        userId,
        timestamp,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * This function is used to get the list of all course in which
 * user is enrolled
 * @async
 * @function
 * @param {number} courseId - Course id
 * @param {number} userId - user id
 * @returns {Promise}
 *
 * @example
 * getAllByUserId({ userId: 4588741125663256 })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function getAllByUserId({ userId }) {
  try {
    const query = db
      .createQuery(COURSE_ENROLLMENTS)
      .filter('userId', '=', userId);

    const [courses] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: courses.map(({ courseId, timestamp }) => ({
        courseId,
        timestamp,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}
