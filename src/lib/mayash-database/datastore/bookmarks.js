/**
 * This file contain all the function related to
 * bookmark of a user
 *
 * @format
 * @flow
 */

import db from './config';
import { ELEMENTS, BOOKMARKS } from './kinds';

// Common Types are defined here

type ErrorNotFound = {
  statusCode: 404,
  error: string,
};

type ErrorDatabase = {
  statusCode: 500,
  error: string,
};

/** ************************************************************************ */

type CreatePayload = {|
  userId: number,
  type: string,
  courseId: number,
  postId: number,
|};

type CreateSuccess = {|
  statusCode: 201,
  message: 'Success',
  payload: {
    userId: number,
    bookmarkId: number,
  },
|};
/**
 * @async
 * This function will create a bookmark
 * @param {object} payload
 * @param {number} payload.userId - id of user
 * @param {string} payload.type - type of bookmark: post/course
 * @param {number} payload.courseId - id of course
 * @param {number} payload.postId - id of post
 * @returns {Promise}
 *
 * @example
 * create({
 *    userId: 102,
 *    type: 'post',
 *    postId: 1022,
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function create({
  userId,
  type,
  courseId,
  postId,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const [keys] = await db.allocateIds(
      db.key([ELEMENTS, userId, BOOKMARKS]),
      1,
    );

    const key = keys[0];

    let data = {
      bookmarkId: key,
      type,
      timestamp: new Date().toISOString(),
    };

    if (postId) {
      data = { ...data, postId };
    }

    if (courseId) {
      data = { ...data, courseId };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        userId,
        bookmarkId: parseInt(key.id, 10),
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// create({
//   userId: 102,
//   type: 'post',
//   postId: 1022,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/** ************************************************************************ */

type GetAllPayload = {|
  userId: number,
|};

type GetAllSuccess = {|
  statusCode: 200,
  message: 'Success',
  payload: Array<{
    type: string,
    postId: number,
    courseId: number,
  }>,
|};
/** @async
 * This will return all the notes of a User
 * @param {object} payload
 * @param {number} payload.id
 * @returns {Promise}
 *
 * @example
 * getAll({
 *  userId: 102
 *  })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function getAll({
  userId,
}: GetAllPayload): Promise<GetAllSuccess | ErrorDatabase> {
  try {
    const key = db.key([ELEMENTS, userId]);

    const query = db
      .createQuery(BOOKMARKS)
      .hasAncestor(key)
      .order('timestamp', {
        descending: true,
      });

    const [bookmark] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: bookmark.map(({ type, postId, courseId }) => ({
        type,
        postId,
        courseId,
      })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllBookmarks({ userId: 5699868278390785 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/** ************************************************************************ */

type DeletePayload = {|
  id: number,
  bookmarkId: number,
|};

type DeleteSuccess = {|
  statusCode: 200,
  message: 'Bookmark Deleted Succesfully',
|};
/**
 * @async
 * This Function will delete a bookmark for user
 * @param {object} payload
 * @param {number} payload.id
 * @param {number} payload.bookmarkId
 * @returns {Promise}
 *
 * @example
 * deleteBookmark({ id: 6614661952700416,
 * bookmarkId: 6614661952700416
 * })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function deleteBookmark({
  id,
  bookmarkId,
}: DeletePayload): Promise<DeleteSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([ELEMENTS, id, BOOKMARKS, bookmarkId]);

    const [bookmark] = await db.delete(key);

    if (bookmark.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Bookmark not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Bookmark Deleted Succesfully',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteBookmark({
//   id: 1661466195270041,
//   bookmarkId: 6614661952700416,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/** ************************************************************************ */

type GetPayload = {|
  id: number,
  bookmarkId: number,
|};

type GetSuccess = {|
  statusCode: 200,
  message: 'Success',
  payload: {
    id: number,
    bookmarkId: number,
  },
|};
/**
 * @async
 * This function will display a single bookmark of a user
 * @param {object} payload
 * @param {number} payload.id
 * @param {number} payload.bookmarkId
 * @returns {Promise}
 *
 * @example
 * get({ id: 102, bookmarkId: 4573968371548160 })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */

export async function get({
  id,
  bookmarkId,
}: GetPayload): Promise<GetSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([ELEMENTS, id, BOOKMARKS, bookmarkId]);

    const [bookmark] = await db.get(key);

    if (!bookmark) {
      return {
        statusCode: 404,
        error: 'Bookmark not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        ...bookmark,
        id,
        bookmarkId: parseInt(bookmarkId, 10),
      },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Datbase Server Error',
    };
  }
}

// get({ id: 102, bookmarkId: 4573968371548160 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/** ************************************************************************ */

type UpdatePayload = {|
  userId: number,
  bookmarkId: number,
  type: number,
|};

type UpdateSuccess = {|
  statusCode: 200,
  message: 'Bookmark updated',
|};
/**
 * @async
 * This will update the Bookmark of the user
 * @param {object} payload
 * @param {number} payload.id
 * @param {number} payload.bookmarId
 * @param {string} payload.type
 * @example
 *
 * update({
 * id: 102,
 * bookmarkId: 5699868278390784,
 * type: 'post',
 * })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */

export async function update({
  userId,
  bookmarkId,
  type,
}: UpdatePayload): Promise<UpdateSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([ELEMENTS, userId, BOOKMARKS, bookmarkId]);

    const [bookmark] = await db.get(key);

    if (!bookmark) {
      return {
        statusCode: 404,
        error: 'Bookmark not found',
      };
    }

    const data = { ...bookmark, type };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Bookmark updated',
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// update({
//   id: 5699868278390788,
//   bookmarkId: 5699868278390784,
//   type: 'post',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
