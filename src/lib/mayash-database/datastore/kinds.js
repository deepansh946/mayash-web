/**
 * @format
 */

/**
 * This file contains the Google's Datastore database table name i.e.
 * kind and it's basic schema. This file is only used for development
 * phase, it has no use at production phase.
 */
import db from './config';

/* =================================================================
                               ELEMENTS
   ================================================================= */

/**
 * ELEMENTS: this kind/table will contain all the information of 'users'
 *  and 'circles'
 * @constant {string}
 *
 *
 * @example
 *
 * // Terms are defined here.
 * // id - element id
 * // username - username of the user
 * // email - default email address for element.
 * // elementType - Type of the element (user/circle)
 * // circleType - Type of the circle (edu/field/org), NOTE: only if elementType is 'circle'
 * // name - name of the element
 * // avatar - avatar photo for element
 * // cover - cover photo for element
 * // googleId - id of the user from google account, (Only for user signed in By Google)
 * // googleRaw - this object contains all the data came from signin in by Google, (only for user)
 * // guru - this will enable guru feature for user, (only for user)
 * // classroom - classroom feature for the circle, (only for circle(edu/org))
 *
 *
 * // Schema for Storing the information of user
 *
 * const elements = {
 *  id: db.key([ELEMENTS, id]),
 *  username: 'hbarve1',
 *  elementType: 'user',
 *  name: 'Himank Honey Barve',
 *  email: 'hbarve1592-at-the-rate-gmail.com',
 *  password: 'long-hashed-password-string', // if user is signed in from google, this will not be present but user can create it later,
 *  avatar: '/avatar-photo-url.jpeg',
 *  cover: '/cover-photo-url.jpeg',
 *  guru: true, // only if guru feature is enabled, otherwise this key does not exists
 *  googleId: '123123123123132131', only if signed in by Google
 *  googleRaw: {}, // this object contains all the data recieved from google
 * };
 *
 *
 * // Schema for Storing the information of the circle(edu)
 *
 * const elements = {
 *  id: db.key([ELEMENTS, id]),
 *  username: 'iitdhn',
 *  elementType: 'circle',
 *  circleType: 'edu',
 *  name: 'IIT Dhanbad',
 *  email: 'contact-us-at-the-rate-iitdhn.ac.in',
 *  avatar: '/avatar-photo-url.jpeg',
 *  cover: '/cover-photo-url.jpeg',
 *  classroom: true, // this will be present for all educational institutes, subscribed to our classroom feature.
 * };
 *
 * // Schema for storing the information of the circle(field)
 *
 * const elements = {
 *  id: db.key([ELEMENTS,id]),
 *  username: 'javascript',
 *  elementType: 'circle',
 *  circleType: 'edu',
 *  name: 'Javascript',
 *  avatar: /avatar-photo-url.jpeg,
 *  cover: '/cover-photo-url.jpeg',
 * };
 *
 * // Schema for storing the information of the circle(org)
 *
 * const elements = {
 *  id: db.key([ELEMENTS,id]),
 *  username: 'google',
 *  elementType: 'circle',
 *  circleType: 'org',
 *  name: 'Google',
 *  email: 'support-in-at-the-rate-google.com'
 *  avatar: /avatar-photo-url.jpeg,
 *  cover: '/cover-photo-url.jpeg',
 *  classroom: true, //this will enable for those organization which has classroom feature enable
 * };
 */
export const ELEMENTS = 'elements';

/* =================================================================
                              AUTH
   ================================================================= */

/**
 * AUTH: This kind/table will store the information of those user who are admin
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // key - id of the user who is admin
 * // userId - id of the user who is admin
 * // admin - Boolean (whether user is admin or not)
 *
 * // Schema to store the information on AUTH
 *
 * const auth = {
 *  key: db.key([AUTH, userId]),
 *  userId: db.key([ELEMENTS, userId]),
 *  admin: true,
 * };
 */
export const AUTH = 'auth';

/* =================================================================
                              FOLLOW
   ================================================================= */

/**
 * FOLLOW: This kind/table will contain data of followings and followers.
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // key - unique id of the follower
 * // followerId - id of the user who is followed
 * // followingId - id of the user who is following
 * // timestamp - time at user follows
 *
 * // Schema to store the information
 *
 * const follow = {
 *  key: db.key([ELEMENTS, followerId, FOLLOW, followingId]),
 *  followerId: db.key([ELEMENTS, followerId]),
 *  followingId: db.key([ELEMENTS, followingId]),
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const FOLLOW = 'follow';

/* =================================================================
                              CIRCLE_MEMBERS
   ================================================================= */

/**
 * circleMembers: This kind/table contain the information of the member
 * of the circle
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // key - id of the member in a circle
 * // circleId - id of the circle (edu/org/field)
 * // memberId - id of the user
 * // doj - date of joining
 * // role - member role's in a circle (admin/member/request)
 * // note: 'request' is for those member those who want to join the circle
 *
 * // Schema for storing the information about the admin of the circle
 *
 * const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]),
 * const data = {
 *  key: db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]),
 *  circleId: db.key([ELEMENTS, circleId]),
 *  memberId: db.key([ELEMENTS, memberId]),
 *  role: 'admin',
 *  doj: (new Date()).toISOString(),
 * };
 *
 * // Schema for storing the information about the member of the circle
 *
 * const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]),
 * const data = {
 *  key: db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]),
 *  circleId: db.key([ELEMENTS,circleId]),
 *  memberId: db.key([ELEMENTS,memberId]),
 *  role: 'member',
 *  doj: (new Date()).toISOString(),
 * };
 *
 * // Schema for storing the information about the member whose who want to join the circle
 *
 * const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]),
 * const data = {
 *  key: db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]),
 *  circleId: db.key([ELEMENTS,circleId]),
 *  memberId: db.key([ELEMENTS,memberId]),
 *  role: 'request',
 *  doj: (new Date()).toISOString(),
 * };
 */
export const CIRCLE_MEMBERS = 'circleMembers';

/**
 * CIRCLE_REQUESTS: This kind/table contains data related to requests made by the user
 * for creating new circle.
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are definded here
 * // userId - the id of the user wants to make the circle
 * // type - Type of the circle (edu/org)
 * // name - name of the educational/organization institution
 * // description - about the institution
 * // email - official email address
 * // website - website URL of the institution
 * // founder - founder of the institution
 *
 * // Schema for storing the information about user for making the edu circle request
 *
 * const newCircleEdu = {
 *  userId: db.key([ELEMENTS, userId]),
 *  type: 'edu',
 *  name: 'Javascript edu',
 *  address: 'Indore,India',
 *  description: 'Programming Language',
 *  email: 'javascript_edu-at-the-rate-gmail.com',
 *  wesbite: 'www.javascript.edu',
 *  founder: 'Brendan Eich',
 * };
 *
 * // Schema for storing the information about user for making the org circle request
 *
 * const newCircleOrg = {
 *  userId: db.key([ELEMENTS, userId]),
 *  type: 'org',
 *  name: 'React School',
 *  address: 'Indore,India',
 *  description: 'React is a Front-end Library',
 *  email: 'react@a.com',
 *  wesbite: 'www.reactjs.com',
 *  founder: 'Jordan Walke',
 * };
 */
export const CIRCLE_REQUESTS = 'circleRequests';

/* =================================================================
                              GURU_REQUESTS
   ================================================================= */

/**
 * This kind/table will store all the request of user to become a 'guru'.
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here.
 * // userId: id of the user who is requesting to become a guru.
 * // timestamp: date of request.
 *
 * const guru = {
 *  userId: db.key([ELEMENTS, userId]),
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const GURU_REQUESTS = 'guruRequests';

/* =================================================================
                            POSTS
   ================================================================= */

/**
 * POSTS: this kind/table will contain all the data related to 'posts'.
 *
 * @type {String}
 *
 * @example
 *
 * // Terms are defined here
 * // postId - id of the post
 * // authorId - id of the user who created the post
 * // postType - 'article', 'status'. default is 'article'
 * // title - title of the post
 * // description - description of the post
 * // data - this object contains all the raw content of draft.js editor
 * // timestamp - time at which post is created by the user
 *
 * // Schema to store the information in POSTS
 *
 * const posts = {
 *  postId: db.key([POSTS,postId]),
 *  authorId: db.key([ELEMENTS,authorId]),
 *  postType: 'article',
 *  title: 'Title',
 *  description: 'Description of the post',
 *  data: {},
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const POSTS = 'posts';

/**
 * POST_REACTIONS - table/kind contains all the reactions made by user to the post.
 * Note: only user can give reaction to the post, not circle.
 *
 * @example
 *
 * const postReaction = {
 *  key: db.key([POSTS, postId, ELEMENTS, userId, POST_REACTIONS, keyId]),
 *  userId: db.key([ELEMENTS, userId]),
 *  reaction: 'like',
 * };
 */
export const POST_REACTIONS = 'postReactions';

/**
 * POSTS_COMMENTS: This kind/table will contain comments on post.
 *
 * @type {String}
 *
 * @example
 *
 * // Terms are defined here
 * // postId - id of the post
 * // authorId - id of the user who created the comment
 * // title - comment message(must be a String)
 * // timestamp - time at which the user make the comment on the post
 *
 * // Schema to store the information on the POST_COMMENTS
 *
 * const postComment = {
 *  commentId: db.key([POSTS, postId, POST_COMMENTS, commentId]),
 *  postId: db.key([POSTS,postId]),
 *  authorId: db.key([ELEMENTS,authorId]),
 *  title: 'this will be a comment text.',
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const POST_COMMENTS = 'postComments';

/* =================================================================
                            COURSES
   ================================================================= */

/**
 * COURSES: this kind/table will contain all the data related to 'courses'.
 *
 * @type {String}
 *
 * @example
 *
 * // Terms are defined here
 * // authorId - user who created the course
 * // courseId - id of the course
 * // title - title of the course
 * // description - description about the course
 * // rating - rating of the course //Note: must be number and in between 0 to 5
 * // level - difficulty level of the course //Note: must be a number in between 1 to 10
 * // syllabus - it is the raw draft.js //Note: this object come from front-end
 * // courseModules - Array of moduleIds
 * // price - Price of the course in INR
 *
 * // Schema to store the information on the course
 *
 * const course = {
 *  courseId: db.key([COURSES, courseId]),
 *  authorId: db.key([ELEMENTS, authorId]),
 *  title: 'Chemical Bonding',
 *  description: 'Different Types of Chemical Bond has been illustrated in this course',
 *  rating: 5,
 *  syllabus: {}, // draft.js raw content
 *  courseModules: [
 *    { moduleId: moduleId1 },
 *    { moduleId: moduleId2 },
 *    { moduleId: moduleId3 },
 *    { moduleId: moduleId4 },
 *    { moduleId: moduleId5 },
 *  ],
 * };
 */
export const COURSES = 'courses';

/**
 * COURSE_MODULES: This kind/table stores the information the course module of a course
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // authorId - the user who created the module
 * // courseId - the id of the course
 * // moduleId - the id of the module
 * // title - title of the module of a course
 * // data - draft.js raw data //Note: this will come from front-end
 * // timestamp - time at which module was created for the course
 * // price - price of the course in INR
 * // rating - rating of the course
 *
 * // Schema for storing the information on courseModule
 *
 * const courseModule = {
 *  moduleId: db.key([COURSES, courseId, COURSES_MODULES, moduleId]),
 *  courseId: db.key([COURSES, courseId]),
 *  authorId: db.key([ELEMENTS, authorId]),
 *  title: 'Chapter 1',
 *  data: {},
 *  timestamp: (new Date()).toISOString(),
 *  price: 20
 *  rating: 5
 * };
 */
export const COURSE_MODULES = 'courseModules';

/**
 * COURSE_DISCUSSION_QUESTIONS: This kind/table is for the discussion questions
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // questionId - id of the question created by the user
 * // userId - user who create the question
 * // title - this will be question (String)
 *
 * // Schema for storing the information about the COURSE_DISCUSSION_QUESTIONS
 *
 * const key = db.key([COURSES, courseId, COURSE_DISCUSSION_QUESTIONS, questionId]);
 * const payload = {
 *  questionId: db.key([COURSES, courseId, COURSE_DISCUSSION_QUESTIONS, questionId]),
 *  userId: db.key([ELEMENTS, userId]),
 *  title: 'What is a Question?',
 *  data: {}, // this is draft.js raw content state.
 * };
 */

export const COURSE_ENROLLMENTS = 'courseEnrollments';

/**
 * COURSE_ENROLLMENT: This kind is for the enrollment in a course
 *
 * @example
 *
 * // Terms are defined here
 * // userId - user id who wants to enroll in the course
 * // courseId - course id in which the user will be enrolled
 * // enrollmentId -  Enrollment id
 *
 * // Schema to store the information
 * const key = db.key([COURSES, courseId, ELEMENTS, userId])
 *
 */
export const COURSE_DISCUSSION_QUESTIONS = 'courseDiscussionQuestions';

/**
 * COURSE_DISCUSSION_ANSWERS: this kind/table is for the discussion answer for question
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // userId - user who answer the question
 * // courseId -
 * // questionId - id of the question
 * // answerId - id of the answer
 * // title - answer of the question (must be a String)
 *
 * // Schema to store the information in COURSE_DISCUSSION_ANSWERS
 * const key = db.key([COURSES, courseId, COURSE_DISCUSSION_QUESTIONS, questionId, COURSE_DISCUSSION_ANSWERS, answerId]);
 * const payload = {
 *  answerId: db.key([COURSES, courseId, COURSE_DISCUSSION_QUESTIONS, questionId, COURSE_DISCUSSION_ANSWERS, answerId]),
 *  userId: db.key([ELEMENTS, userId]),
 *  title: 'Answer of the Question is bla bla bla',
 * };
 */
export const COURSE_DISCUSSION_ANSWERS = 'courseDiscussionAnswers';

/**
 * This kind/table is for test yourself feature.
 * NOTE: question in this table for course are MCQ types.
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * //
 *
 * const courseQuestion = {
 *  questionId: db.key([COURSES, courseId, COURSE_QUESTIONS, questionId]),
 *  title: 'this is a question for testYourSelf feature',
 *  options: [
 *    {
 *      title: 'this is first option',
 *      answer: true,
 *      description: 'this description will explain the answer',
 *    },
 *    {
 *      title: 'this is second option',
 *    },
 *    {
 *      title: 'this is third option',
 *    },
 *    {
 *      title: 'this is fourth option',
 *    },
 *  ],
 * };
 */
export const COURSE_QUESTIONS = 'courseQuestions';

/* =================================================================
                            PHOTOS
   ================================================================= */

/**
 * This table/kind will be used to store photos information.
 * @constant {string}
 *
 * @example
 *
 * // id - element id
 * // photoId - photo id
 * // postId - post id
 * // courseId - course id
 * // fileType - this defines the extention of photo file (mp4)
 * // timestamp - date should be in ISO format
 *
 *
 * // Schema for storing photos uploaded for element(user/circle)
 *
 * const photo = {
 *  photoId: db.key([ELEMENTS, id, PHOTOS, photoId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  name: `${photoId}.${fileType}`, //
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 *
 * // Schema for storing photos uploaded for posts
 *
 * const photo = {
 *  photoId: db.key([POSTS, postId, PHOTOS, photoId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  postId: db.key([POSTS, postId]),
 *  name: `${photoId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 *
 * // Schema for storing photos uploaded for courses
 *
 * const photo = {
 *  photoId: db.key([COURSES, courseId, PHOTOS, photoId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  courseId: db.key([COURSES, courseId]),
 *  name: `${photoId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const PHOTOS = 'photos';

/* =================================================================
                          VIDEOS
   ================================================================= */

/**
 * This table/kind will be used to store videos information.
 * @constant {string}
 *
 * @example
 *
 * // id - element id
 * // videoId - video id
 * // postId - post id
 * // courseId - course id
 * // fileType - this defines the extention of video file (mp4)
 * // timestamp - date should be in ISO format
 *
 *
 * // Schema for storing videos uploaded for element(user/circle)
 *
 * const video = {
 *  videoId: db.key([ELEMENTS, id, VIDEOS, videoId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  name: `${videoId}.${fileType}`, //
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 *
 * // Schema for storing videos uploaded for posts
 *
 * const video = {
 *  videoId: db.key([POSTS, postId, VIDEOS, videoId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  postId: db.key([POSTS, postId]),
 *  name: `${videoId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 *
 * // Schema for storing videos uploaded for courses
 *
 * const video = {
 *  videoId: db.key([COURSES, courseId, VIDEOS, videoId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  courseId: db.key([COURSES, courseId]),
 *  name: `${videoId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const VIDEOS = 'videos';

/* =================================================================
                          FILES
   ================================================================= */

/**
 * This table/kind will be used to store file's information.
 * @constant {string}
 *
 * @example
 *
 * // id - element id
 * // fileId - file id
 * // postId - post id
 * // courseId - course id
 * // fileType - this defines the extention of file (.pdf)
 * // timestamp - date should be in ISO format
 *
 *
 * // Schema for storing files uploaded for element(user/circle)
 *
 * const file = {
 *  fileId: db.key([ELEMENTS, id, FILES, fileId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  name: `${fileId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 *
 * // Schema for storing files uploaded for posts
 *
 * const file = {
 *  fileId: db.key([POSTS, postId, FILES, fileId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  postId: db.key([POSTS, postId]),
 *  name: `${fileId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 *
 * // Schema for storing files uploaded for courses
 *
 * const file = {
 *  fileId: db.key([COURSES, courseId, FILES, fileId]),
 *  authorId: db.key([ELEMENTS, id]),
 *  courseId: db.key([COURSES, courseId]),
 *  name: `${fileId}.${fileType}`,
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const FILES = 'files';

/* =================================================================
                            NOTES
   ================================================================= */

/**
 * NOTES: This kind/table will store the information about the notes/todo of a user
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // noteId - id of the notes
 * // noteType - note/todo
 * // text - This will contain a Array of todo or title of the note
 * // title - title of the note
 * // checkbox - status of todo //Note: This will be absent if noteType is note
 *
 * // Schema for storing the information of todo of a user
 *
 * const note = {
 *  noteId: db.key([ELEMENTS, userId, NOTES, noteId]),
 *  noteType: 'todo',
 *  title: 'title of the todo',
 *  data: [
 *   {
 *     title: 'Note 1',
 *     checkbox: true,
 *   },
 *   {
 *     title: 'Note 2',
 *     checkbox: false,
 *   },
 *  ]
 * };
 *
 * // Schema for Storing the information of a notes of a user
 *
 * const note = {
 *  noteId: db.key([ELEMENTS, userId, NOTES, noteId]),
 *  noteType: 'note',
 *  title: 'This is the first Title of the note',
 * };
 */
export const NOTES = 'notes';

/* =================================================================
                          BOOKMARKS
   ================================================================= */

/**
 * BOOKMARKS: This kind/table will store the information about the bookmarks of a user
 *
 * @constant {string}
 *
 * @example
 *
 * // Terms are defined here
 * // bookmarkId - id of the bookmark created by the user
 * // type - bookmark is for course or for post
 * // postId - id of the course or id of the post //Note: it will depend upon the type
 * // timestamp - time at which the bookmark are created
 *
 * // Schema to store the information about the bookmarks of a post of a user
 *
 * const bookmarks = {
 *  bookmarkId: db.key([ELEMENTS, userId, BOOKMARKS, bookmarkId]),
 *  type: 'post',
 *  postId: db.key([POSTS, postId]),
 *  timestamp: (new Date()).toISOString(),
 * };
 *
 * // Schema to store the information in bookmarks of a course
 *
 * const bookmarks = {
 *  bookmarkId: db.key([ELEMENTS, userId, BOOKMARKS, bookmarkId]),
 *  type: 'course',
 *  courseId: db.key([COURSES, courseId]),
 *  timestamp: (new Date()).toISOString(),
 * };
 */
export const BOOKMARKS = 'bookmarks';

/* =================================================================
                            RESUME
   ================================================================= */

/**
 * This table/kind will store resume templates, not user's resume.
 *
 * @constant {string}
 *
 * @example
 *
 * const resume = {
 *  resumeId: db.key([RESUME, resumeId]),
 *  title: 'Resume Title',
 *  data: {}, // draft.js row content
 * };
 */
export const RESUME = 'resume';

/* =================================================================
                              CLASSROOMS
   ================================================================= */
/**
 * DEPARTMENTS: This kind/table stores the information about the department in circle
 *
 * @constant {string}
 *
 * @example
 *
 * //Terms defined here
 * key - id of the department of a circle
 * deptId - short name of department (Eg:cse)
 * title - title of the department(Eg:Computer Science and Engineering)
 * hodId - head of department(must be user)
 * professors - Array of professors in a department
 *
 * //Schema to store the information in the DEPARTMENT
 *
 * const DEPARTMENT = {
 *  key: db.get([ELEMENTS,circleId,DEPARTMENTS,departmentId]),
 *  deptId: 'cse',
 *  title: 'Computer Science And Engineering',
 *  hodId: db.key([ELEMENTS,hodId]),
 *  professors: [
 *   {profId:db.key([ELEMENTS,profId])},
 *   {profId:db.key([ELEMENTS,profId])},
 *  ],
 * };
 */
export const DEPARTMENTS = 'departments';

const departments = [
  {
    key: db.key([ELEMENTS, 123, DEPARTMENTS, 74]),
    deptId: 'cse',
    title: 'Computer Science and Engineering',
    // hodId should be taken from 'elements' kind/table.
    hodId: db.key([ELEMENTS, 123]),
    professors: [
      {
        // profId should be taken from 'elements' kind/table.
        profId: db.key([ELEMENTS, 153]),
      },
      {
        profId: db.key([ELEMENTS, 526]),
      },
      {
        profId: db.key([ELEMENTS, 741]),
      },
    ],
  },
];

/**
 * DEGREES : This is a kind/table for storing the information about the no of degress in a semester
 *
 * @constant {string}
 *
 * @example
 *
 * //Terms are defined here
 * key - Id of the Degree
 * degreeId - short term for the degree (eg:Btech)
 * title - title of the degree (eg:Bachelor of Engineering)
 * duration - no of months to complete the degree
 * description - to become engineer pursue this
 *
 * //Schema for storing the information about the B.E. degree
 *
 * const DEGREE = {
 *  key: db.key([ELEMENTS,circleId,DEPARTMENTS,departmentId,DEGREES,degreeId]),
 *  degreeId: 'B.E.',
 *  title: 'Bachelor Of Engineering,
 *  duration: 48,
 *  description:'Engineering',
 * };
 *
 * //Schema for storing the information about the B.Tech. degree
 *
 * const DEGREE = {
 *   key:db.key([ELEMENTS,circleId,DEPARTMENTS,departmentId,DEGREES,degreeId]),
 *  degreeId: 'B.Tech',
 *  title: 'Bachelor of Technology',
 *  duration: 48,
 *  description: 'Engineering',
 * };
 */
export const DEGREES = 'degrees';

const degrees = [
  {
    key: db.key([ELEMENTS, 45, DEPARTMENTS, 4, DEGREES, 1]),
    degreeId: 'be',
    title: 'Bachelor Of Engineering',
    duration: 48, // duration is in months
    description: 'Ye toh galti se bhi Matt karna',
  },
  {
    key: db.key([ELEMENTS, 45, DEPARTMENTS, 4, DEGREES, 1]),
    degreeId: 'btech',
    title: 'Bachelor Of Technology',
    duration: 48, // duration is in months
    description: 'Matt karna',
  },
];

/**
 * SEMESTERS: This kind/table Stores the information about semester
 *
 * @constant {string}
 *
 * @example
 *
 * //Terms are defined here
 * semId - id of the semester
 *
 * //Schema to store the information in semesters
 *
 * const semesters = {
 *  semId = db.key([ELEMENTS,circleId,DEPARTMENTS,departmentId,DEGREES,degreeId,SEMESTERS,semesterId]),
 * };
 */
export const SEMESTERS = 'semesters';

const semesters = [
  {
    semId: 1,
  },
  {
    semId: 2,
  },
  {
    semId: 3,
  },
];

/**
 * CLASSROOM: This kind/table stores the information about the Classroon feature
 * @todo documentation will be updated later
 * @constant {string}
 *
 * @example
 *
 * //Terms defined here
 * key - id of the classroom
 * profId - id the professor of the classroom
 * courseId - id of the course
 * classId -
 * strength -
 * assigments -
 */
export const CLASSROOM = 'classroom';

const classroom = [
  {
    key: db.key([ELEMENTS, 12, DEPARTMENTS, 456, DEGREES, 12, CLASSROOM, 47]),
    profId: 465,
    courseId: 123,
    classId: 455,
    strength: [123, 11, 12],
    assignments: [],
  },
];

export const CLASSROOM_REQUEST = 'classroomRequest';

/**
 * TEST: This kind/table stores the information about the Test yourself feature
 * @constant {string}
 *
 * @example
 *
 * // Terms defined here
 * // courseId(number) - id of the course
 * // authorId(number) - id of the author
 * // questionId(number) - id of the question
 * // title(string) -Title of the question
 * // description(string) - Description of the question
 * // options(Array of objects) - Options of the question
 *
 * const courseTest = [
 * {
 *  courseId: 1234567891252631,
 *  authorId: 1234567891252635,
 *  questionId: db.key([
 *    ELEMENTS,
 *    1234567891252635,
 *    COURSES,
 *    1234567891252631,
 *    TEST,
 *  ]),
 *  title: 'Question Title',
 *  description: 'Question Description',
 *  options: [
 *    { title: 'Option A', answer: true, description: 'Answer description' },
 *    { title: 'Option B', answer: false },
 *    { title: 'Option C', answer: true, description: 'Answer description' },
 *    { title: 'Option D', answer: false },
 *  ],
 * },
 *];
 */
export const COURSE_TEST = 'courseTest';
