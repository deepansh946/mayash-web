/**
 * This file contains all the function related to only 'circle' of 'elements'.
 * This file is the extention of 'elements.js'
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, CIRCLE_MEMBERS } from '../kinds';

/**
 * This function will add member's request to join circle.
 * later admin of circle will update it's request to 'member' or 'admin' role.
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.memberId -
 * @returns {Promise} -
 *
 * @example
 * create({
 *   circleId: 1234,
 *   memberId: 6534,
 * })
 *   .then(error => console.log(error))
 *   .catch(error => console.error(error));
 */

export async function create({ circleId, memberId }) {
  try {
    const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]);

    const data = {
      key,
      circleId: db.key([ELEMENTS, circleId]),
      memberId: db.key([ELEMENTS, memberId]),
      role: 'request',
      doj: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message:
        'Request for joining the circle has been taken.' +
        'We will revet to you back shortly.',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({
//   circleId: 1234,
//   memberId: 6534,
// })
//   .then(error => console.log(error))
//   .catch(error => console.error(error));

/**
 * This function will give members list of circle element.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @return {Promise} -
 *
 * @example
 * getAll({ circleId: 202 })
 *    .then(error => console.log(error))
 *    .catch(error => console.error(error));
 */
export async function getAll({ circleId }) {
  try {
    const key = db.key([ELEMENTS, circleId]);

    const query = db
      .createQuery(CIRCLE_MEMBERS)
      .hasAncestor(key)
      .limit(10);

    const [members] = await db.runQuery(query);

    if (members.length === 0) {
      return {
        statusCode: 404,
        error: 'Member not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Members found',
      payload: members.map((m) => ({
        ...m,
        key: parseInt(m.key.id, 10),
        circleId,
        memberId: parseInt(m.memberId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// getAll({ circleId: 202 })
//   .then(error => console.log(error))
//   .catch(error => console.error(error));

/**
 * get member in circle
 * @async
 * @function get
 * @param {Object} payload
 * @param {number} payload.circleId
 * @param {number} payload.memberId
 * @return {Promise} -
 *
 * @example
 * get({
 * circleId: 201,
 * memberId: 101,
 * })
 *    .then(error => console.log(error))
 *    .catch(error => console.log(error));
 */
export async function get({ circleId, memberId }) {
  try {
    const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]);

    const [members] = await db.get(key);

    if (members.length === 0) {
      return {
        statusCode: 404,
        error: 'Circle or Member does not exist',
      };
    }

    return {
      statusCode: 200,
      message: 'User is a member of this circle',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// get({
//   circleId: 201,
//   memberId: 101,
// }).then(error => console.log(error))
//   .catch(error => console.log(error));

/**
 * Update role of member in circle
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.circleId
 * @param {number} payload.memberId
 * @param {string} payload.role - 'member' or 'admin'
 * @return {Promise} -
 *
 * @example
 * update({
 * circleId: 201,
 * memberId: 101,
 * role: 'member',
 * })
 *    .then(error => console.log(error))
 *    .catch(error => console.log(error));
 */
export async function update({ circleId, memberId, role }) {
  try {
    const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]);

    const [members] = await db.get(key);

    if (members.length === 0) {
      return {
        statusCode: 404,
        error: 'Circle or Member does not exist',
      };
    }

    const data = { ...members[0], role };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Member role updated',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// update({
//   circleId: 201,
//   memberId: 101,
//   role: 'member',
// })
//   .then((error) => console.log(error))
//   .catch((error) => console.log(error));

/**
 * remove member from circle.
 * @async
 * @function deleteMemeber
 * @param {Object} payload
 * @param {number} payload.circleId
 * @param {number} payload.memberId
 * @return {Promise}
 *
 * @example
 * deleteMember({
 *   circleId: 202,
 *   memberId: 101,
 * })
 *   .then(error => console.log(error))
 *   .catch(error => console.log(error));
 */
export async function deleteMember({ circleId, memberId }) {
  try {
    const key = db.key([ELEMENTS, circleId, CIRCLE_MEMBERS, memberId]);

    const [res] = await db.delete(key);

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Member deleted from circle',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteMember({
//   circleId: 202,
//   memberId: 101,
// })
//   .then(error => console.log(error))
//   .catch(error => console.log(error));
