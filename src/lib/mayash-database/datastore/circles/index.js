/**
 * This file contains all the function related to only 'circle' of 'elements'.
 * This file is the extention of 'elements.js'
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, CIRCLE_REQUEST, CIRCLE_MEMBERS } from '../kinds';
import { checkUsername } from '../elements';

/**
 *
 * @async
 * @function getAll
 * @param {Object} params
 * @param {string} params.circleType - 'edu', 'org', 'location' ...
 * @returns {Promise}
 *
 *
 * @example
 */
export async function getAll({ circleType }) {
  try {
    const query = db
      .createQuery(ELEMENTS)
      .filter('elementType', 'circle')
      .limit(25);

    if (typeof circleType !== 'undefined') {
      query.filter('circleType', circleType);
    }

    const [circles] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: circles.map((c) => {
        const temp = {
          ...c,
          id: parseInt(c.id.id, 10),
        };

        if (c.creatorId) {
          temp.creatorId = parseInt(c.creatorId.id, 10);
        }

        return temp;
      }),
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will register a new educational institute as circle.
 * This function does not ensure whether the entered username exists
 * in the database or not, so before calling this function call
 * checkUsername function.
 * @param {Object} payload -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {number} payload.creatorId -
 * @param {function} callback
 */
export async function createEdu({ username, name, avatar, creatorId }) {
  try {
    const { statusCode } = await checkUsername({ username });

    if (statusCode >= 500) {
      return {
        statusCode,
        error: 'Database Server Error.',
      };
    }

    if (statusCode === 200) {
      return {
        statusCode: 400,
        error: 'Username already taken.',
      };
    }

    const [keys] = await db.allocateIds(db.key(ELEMENTS), 1);
    const key = keys[0];

    const data = {
      id: key,
      username,
      name,
      avatar,
      creatorId: db.key([ELEMENTS, creatorId]),
      elementType: 'circle',
      circleType: 'edu',
    };

    const [res] = await db.save({ key, data });

    return {
      statusCode: 200,
      message: 'Edu Circle Created',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}

// createEdu({
//   username: 'iitd',
//   name: 'Indian Institute of Technology, Delhi',
//   avatar: '/public/photos/mayash-logo-transparent.png',
//   creatorId: 5066549580791808,
// })
//   .then(error => console.log(error))
//   .catch(error => console.log(error));

/**
 * This function will create a new field but this function does not
 * check the username of the field so you should check whether the
 * username exists in the database or not then call this function
 * @param {Object} payload -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @return {return} -
 */
export async function createField({ username, name, avatar }) {
  try {
    const { statusCode } = await checkUsername({ username });

    if (statusCode >= 500) {
      return {
        statusCode,
        error: 'Database Server Error.',
      };
    }

    if (statusCode === 200) {
      return {
        statusCode: 400,
        error: 'Username already taken.',
      };
    }

    const [keys] = await db.allocateIds(db.key(ELEMENTS), 1);

    const key = keys[0];
    const data = {
      id: key,
      username,
      name,
      avatar,
      elementType: 'circle',
      circleType: 'field',
    };

    const [res] = await db.save({ key, data });

    return {
      statusCode: 200,
      message: 'Field Circle created',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createField({
//   username: 'javascript',
//   name: 'JavaScript',
//   avatar: '/public/photos/mayash-logo-transparent.png'
// })
//   .then(error => console.log(error))
//   .catch(error => console.log(error));

/**
 * This function will create a organisation(company/industry) but this
 * function does not check whether the username exists in the database
 * or not so you must check it before calling this function.
 * @async
 * @function createOrg
 * @param {Object} payload -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {number} payload.creatorId -
 * @return {Promise} -
 */
export async function createOrg({ username, name, avatar, creatorId }) {
  try {
    const { statusCode } = await checkUsername({ username });

    if (statusCode >= 500) {
      return {
        statusCode,
        error: 'Database Server Error.',
      };
    }

    if (statusCode === 200) {
      return {
        statusCode: 400,
        error: 'Username already taken.',
      };
    }

    const [keys] = await db.allocateIds(db.key(ELEMENTS), 1);

    const key = keys[0];
    const data = {
      id: key,
      username,
      name,
      avatar,
      elementType: 'circle',
      circleType: 'org',
      creatorId: db.key([ELEMENTS, creatorId]),
    };

    const [res] = await db.save({ key, data });

    return {
      statusCode: 200,
      message: 'Organisation Circle created',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createOrg({
//   username: 'google',
//   name: 'Google',
//   avatar: '/public/photos/mayash-logo-transparent.png',
//   creatorId: 5066549580791808
// })
//   .then(error => console.log(error))
//   .catch(error => console.log(error));

/**
 * this function will register a request to add on circle.
 * this will not be stored in directly in 'elements' table but in separate
 * table. we will review it and if found genuine, we will add in elements table
 * @async
 * @function submitCircle
 * @param {Object} payload -
 * @param {number} payload.userId - this is the id of a user who is
 * submitting the request.
 *
 *
 * @example
 *
 * submitCircle({
 *   userId: 100,
 *   name: 'Deepan',
 *   type: 'edu',
 *   address: 'Gwalior',
 *   description: 'New Circle',
 *   website: 'www.com',
 *   founder: 'Hello World',
 * }).then(error => console.log(error))
 *   .catch(error => console.error(error));
 */
export async function submitCircle({
  userId,
  name,
  type,
  address,
  description,
  website,
  founder,
}) {
  try {
    const [keys] = await db.allocateIds(db.key(CIRCLE_REQUEST), 1);

    const key = keys[0];

    const data = {
      // userId of the user who made the request
      circleId: key,
      userId: db.key([ELEMENTS, userId]),
      name,
      circleType: type,
      address,
      description,
      website,
      founder,
    };

    const [res] = await db.save({ key, data });

    return {
      statusCode: 200,
      message:
        'Request for becoming a circle accepted.' +
        ' We will get back to you soon.',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// submitCircle({
//   userId: 100,
//   name: 'Deepan',
//   type: 'edu',
//   address: 'Gwalior',
//   description: 'New Circle',
//   website: 'www.com',
//   founder: 'Hello World',
// }).then(error => console.log(error))
//   .catch(error => console.error(error));

/**
 *
 * @async
 * @function getCircle
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @returns {Promise}
 *
 * @example
 */
export async function getCircle({ circleId }) {
  try {
    // create a datastore key.
    const key = db.key([ELEMENTS, circleId]);

    const [circle] = await db.get(key);

    if (!circle) {
      return {
        statusCode: 404,
        error: 'Circle Not Found',
      };
    }

    const { name, username, email, elementType, classroom, avatar } = circle;

    const payload = {
      elementType,
      id: circleId,
      username,
      name,
      avatar,
      email,
      classroom,
    };

    return {
      statusCode: 200,
      message: 'Circle found',
      payload,
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 *
 * @async
 * @function deleteCircle
 * @param {Object} payload
 * @param {number} payload.circleId
 * @return {Promise}
 *
 * @example
 * deleteCircle({
 *   circleId: 202,
 * })
 *   .then(error => console.log(error))
 *   .catch(error => console.log(error));
 */
export async function deleteCircle({ circleId }) {
  try {
    const key = db.key([ELEMENTS, circleId]);

    const [res] = await db.delete(key);

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Circle Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Circle Deleted successfully.',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteCircle({
//   circleId: 202,
//   memberId: 101,
// })
//   .then(error => console.log(error))
//   .catch(error => console.log(error));

/**
 * Update circle
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.name -
 * @return {Promise} -
 *
 * @example
 */
export async function update({ circleId, name }) {
  try {
    const key = db.key([ELEMENTS, circleId]);

    const [circle] = await db.get(key);

    if (!circle) {
      return {
        statusCode: 404,
        error: 'Circle Not Found',
      };
    }

    let data = { ...circle };

    if (typeof name === 'string') {
      data = { ...data, name };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will check is a member is admin of the circle or not.
 * @async
 * @function isCircleAdmin
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.memberId -
 * @return {Promise}
 *
 * @example
 */
export async function isCircleAdmin({ circleId, memberId }) {
  try {
    const query = db
      .createQuery(CIRCLE_MEMBERS)
      .filter('circleId', db.key([ELEMENTS, circleId]))
      .filter('memberId', db.key([ELEMENTS, memberId]))
      .limit(1);

    const [members] = await db.runQuery(query);

    if (members.length === 0) {
      return {
        statusCode: 404,
        message: 'Member Not Found.',
      };
    }

    if (members[0].role !== 'admin') {
      return {
        statusCode: 400,
        message: 'Member is not Admin.',
      };
    }

    return {
      statusCode: 200,
      message: 'Member is admin.',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will check if a user is member of the circle or not.
 * @async
 * @function isCircleMember
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.memberId -
 * @returns {Promise}
 *
 * @example
 */
export async function isCircleMember({ circleId, memberId }) {
  try {
    const query = db
      .createQuery(CIRCLE_MEMBERS)
      .filter('circleId', db.key([ELEMENTS, circleId]))
      .filter('memberId', db.key([ELEMENTS, memberId]))
      .limit(1);

    const [members] = await db.runQuery(query);

    if (members.length === 0) {
      return {
        statusCode: 404,
        message: 'Member Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'The given element is a member of circle.',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
