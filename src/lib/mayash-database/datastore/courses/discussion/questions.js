/**
 * This function will contain all the function related to questions
 *
 * @format
 * @flow
 */

import db from '../../config';
import { COURSE_DISCUSSION_QUESTIONS, COURSES, ELEMENTS } from '../../kinds';

type ErrorDatabase = {
  statusCode: 500,
  error: string,
};

type ErrorNotFound = {
  statusCode: 404,
  error: string,
};

type CreatePayload = {
  courseId: number,
  userId: number,
  title: string,
  data: any,
};

type Success = {
  statusCode: number,
  message: string,
  payload?: any,
};

type createSuccess = {
  statusCode: number,
  message: string,
  payload: {
    courseId: number,
    authorId: number,
    questionId: number,
    title: string,
  },
};

/**
 * this function will create the question
 * @async
 * @function create
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.userId
 * @param {string} payload.title
 * @returns {Promise}
 *
 * @example
 *
 * create({ courseId: 102, userId: 111, title: 'what is JavaScript' })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function create({
  courseId,
  userId,
  title,
  data,
}: CreatePayload): Promise<createSuccess | ErrorDatabase> {
  try {
    const [keys] = await db.allocateIds(
      db.key([COURSES, courseId, COURSE_DISCUSSION_QUESTIONS]),
      1,
    );

    const key = keys[0];

    let payload = {
      questionId: key,
      authorId: db.key([ELEMENTS, userId]),
      title,
      timestamp: new Date().toISOString(),
    };

    if (data) {
      payload = {
        ...payload,
        data,
      };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Successfully Created',
      payload: {
        courseId,
        authorId: userId,
        questionId: parseInt(key.id, 10),
        title,
        timestamp: data.timestamp,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// create({ courseId: 111, userId: 111, title: 'what is a question' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type getAllPayload = {
  courseId: number,
};

type getAllSuccess = {
  statusCode: number,
  message: string,
  payload: Array<{
    courseId: number,
    questionId: number,
    authorId: number,
    title: string,
    data?: any,
    timestamp: string,
  }>,
};

/**
 * This function will get all the question
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.courseId
 * @returns {Promise}
 *
 * @example
 *
 * getAll({ courseId: 102 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({
  courseId,
}: getAllPayload): Promise<getAllSuccess | ErrorDatabase> {
  try {
    const key = db.key([COURSES, courseId]);

    const query = db
      .createQuery(COURSE_DISCUSSION_QUESTIONS)
      .hasAncestor(key)
      .order('timestamp', { descending: true });

    const [question] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: question.map((q) => ({
        ...q,
        authorId: parseInt(q.authorId.id, 10),
        courseId,
        questionId: parseInt(q.questionId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAll({ courseId: 111 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type getPayload = {
  courseId: number,
  questionId: number,
};

type getSuccess = {
  statusCode: number,
  message: string,
  payload: {
    courseId: number,
    questionId: number,
    authorId: number,
    title: string,
    data?: any,
    timestamp: string,
  },
};

/**
 * @async
 * @function get
 * @param {Object} payload
 * @param {Number} payload.courseId
 * @param {Number} payload.questionId
 * @returns {Promise}
 *
 * @example
 *
 * get({ courseId: 111, questionId: 6227633859723264 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function get({
  courseId,
  questionId,
}: getPayload): Promise<getSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([
      COURSES,
      courseId,
      COURSE_DISCUSSION_QUESTIONS,
      questionId,
    ]);

    const [question] = await db.get(key);

    if (!question) {
      return {
        statusCode: 404,
        error: 'Question not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        ...question,
        courseId,
        questionId,
        authorId: parseInt(question.authorId.id, 10),
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// get({ courseId: 111, questionId: 5646129647583232 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type updatePayload = {
  courseId: number,
  questionId: number,
  title?: string,
  data?: any,
};

/**
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.userId
 * @param {number} payload.questionId
 * @param {string} payload.title
 * @param {Object} payload.data
 * @returns {Promise}
 *
 * @example
 *
 * update({
 *  courseId: 111,
 *  questionId: 6227633859723264,
 *  title: 'hello'
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function update({
  courseId,
  questionId,
  title,
  data,
}: updatePayload): Promise<Success | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([
      COURSES,
      courseId,
      COURSE_DISCUSSION_QUESTIONS,
      questionId,
    ]);

    const [question] = await db.get(key);

    if (!question) {
      return {
        statusCode: 404,
        error: 'Question Not Found',
      };
    }

    let payload = { ...question };

    if (title) {
      payload = { ...payload, title };
    }

    if (data) {
      payload = { ...payload, data };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Successfully Updated',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// update({
//   courseId: 111,
//   questionId: 5646129647583232,
//   title: 'hello',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type deleteQuestionPayload = {
  courseId: number,
  questionId: number,
};

type deleteSuccess = {
  statusCode: 200,
  message: string,
};

/**
 * This function will delete the question
 * @async
 * @function deleteQuestion
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @returns {Promise}
 *
 * @example
 *
 * deleteQuestion({ courseId: 111, questionId: 4538783999459328 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function deleteQuestion({
  courseId,
  questionId,
}: deleteQuestionPayload): Promise<
  deleteSuccess | ErrorNotFound | ErrorDatabase,
> {
  try {
    const key = db.key([
      COURSES,
      courseId,
      COURSE_DISCUSSION_QUESTIONS,
      questionId,
    ]);

    const [question] = await db.delete(key);

    if (question.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Question Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteQuestion({ courseId: 111, questionId: 4538783999459328 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
