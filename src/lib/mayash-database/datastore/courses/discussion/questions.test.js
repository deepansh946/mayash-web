/**
 * This function will contain all the function related to questions
 *
 * @format
 * @flow
 */
/* eslint-disable no-undef */

import { create, update, get, getAll, deleteQuestion } from './questions';

describe("Course Discussion's Questions Tests", () => {
  test('Create Question', async () => {
    expect.assertions(1);

    const payload = {
      courseId: 123456789,
      userId: 123456789,
      title: 'What is JavaScript',
    };

    const res = await create(payload);

    expect(res).toMatchObject({
      statusCode: 201,
      message: 'Successfully Created',
      payload,
    });
  });

  test('Update Question', async () => {
    expect.assertions(1);

    const payload = {
      courseId: 123456789,
      userId: 123456789,
      title: 'What is JavaScript',
    };

    const res1 = await create(payload);
    const { questionId } = res1.payload || {};

    const updatePayload = {
      ...payload,
      questionId,
      title: 'What is this new JavaScript',
    };

    const res = await update(updatePayload);

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Successfully Updated',
    });
  });

  test('Get Course Question', async () => {
    expect.assertions(1);

    const payload = {
      courseId: 123456789,
      userId: 123456789,
      title: 'What is JavaScript',
    };

    const res1 = await create(payload);
    const { questionId } = res1.payload || {};

    const res = await get({ courseId: payload.courseId, questionId });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
      payload: {
        ...payload,
        questionId,
      },
    });
  });

  test('Get All Course Question', async () => {
    expect.assertions(1);

    const payload = { courseId: 123456789 };

    const res = await getAll(payload);

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });

  test('Delete Course Question', async () => {
    expect.assertions(1);

    const payload = {
      courseId: 123456789,
      userId: 123456789,
      title: 'What is JavaScript',
    };

    const res1 = await create(payload);
    const { questionId } = res1.payload || {};

    const res = await deleteQuestion({
      courseId: 123456789,
      questionId,
    });

    expect(res).toMatchObject({
      statusCode: 200,
      message: 'Success',
    });
  });
});
