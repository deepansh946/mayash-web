/**
 * This contain all the database function for the answer
 *
 * @format
 * @flow
 */

import db from '../../config';
import {
  ELEMENTS,
  COURSES,
  COURSE_DISCUSSION_QUESTIONS,
  COURSE_DISCUSSION_ANSWERS,
} from '../../kinds';

type ErrorDatabase = {
  statusCode: number,
  error: string,
};

type ErrorNotFound = {
  statusCode: number,
  error: string,
};

type CreatePayload = {
  courseId: number,
  questionId: number,
  userId: number,
  data: any,
};

type CreateSuccess = {
  statusCode: number,
  message: string,
  payload: {
    courseId: number,
    questionId: number,
    answerId: number,
    authorId: number,
    data: any,
    timestamp: string,
  },
};

/**
 * This function will create the answer for the question
 * @async
 * @function create
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.userId
 * @param {string} payload.title
 * @returns {Promise}
 *
 * @example
 *
 * create({
 *  courseId: 111,
 *  questionId: 4820258976169984,
 *  userId: 111,
 *  title: 'answer1',
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function create({
  courseId,
  questionId,
  userId,
  data,
}: CreatePayload): Promise<CreateSuccess | ErrorDatabase> {
  try {
    const [keys] = await db.allocateIds(
      db.key([
        COURSES,
        courseId,
        COURSE_DISCUSSION_QUESTIONS,
        questionId,
        COURSE_DISCUSSION_ANSWERS,
      ]),
      1,
    );

    const key = keys[0];

    const payload = {
      answerId: key,
      authorId: db.key([ELEMENTS, userId]),
      data,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const res = db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...payload,
        courseId,
        questionId,
        answerId: parseInt(key.id, 10),
        authorId: userId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// create({
//   userId: 111,
//   courseId: 111,
//   questionId: 4749890231992320,
//   title: 'answer1',
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type GetAllPayload = {
  courseId: number,
  questionId: number,
};

type GetAllSuccess = {
  statusCode: number,
  message: string,
  payload: Array<{
    courseId: number,
    questionId: number,
    answerId: number,
    authorId: number,
    title: string,
    data?: any,
    timestamp: string,
  }>,
};

/**
 * This function will getAll Answer
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @returns {Promise}
 *
 * @example
 *
 * getAll({ courseId: 111, questionId: 4820258976169984 })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function getAll({
  courseId,
  questionId,
}: GetAllPayload): Promise<GetAllSuccess | ErrorDatabase> {
  try {
    const key = db.key([
      COURSES,
      courseId,
      COURSE_DISCUSSION_QUESTIONS,
      questionId,
    ]);

    const query = db
      .createQuery(COURSE_DISCUSSION_ANSWERS)
      .hasAncestor(key)
      .order('timestamp', { descending: true });

    const [answers] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: answers.map(({ authorId, answerId, ...rest }) => ({
        ...rest,
        courseId,
        questionId,
        answerId: parseInt(answerId.id, 10),
        authorId: parseInt(authorId.id, 10),
      })),
    };
  } catch (error) {
    console.log(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAll({ courseId: 111, questionId: 4820258976169984 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/** ************************************************************************ */

type GetPayload = {
  courseId: number,
  questionId: number,
  answerId: number,
};

type GetSuccess = {
  statusCode: number,
  message: string,
  payload: {
    courseId: number,
    questionId: number,
    answerId: number,
    authorId: number,
    title: string,
    data?: any,
    timestamp: string,
  },
};

/**
 * This function get a answer
 * @async
 * @function get
 * @param {Object} payload
 * @param {Number} payload.courseId
 * @param {Number} payload.questionId
 * @param {Number} payload.answerId
 * @returns {Promise}
 *
 * @example
 *
 * get({
 *  courseId: 111,
 *  questionId: 4749890231992320,
 *  answerId: 5312840185413632,
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function get({
  courseId,
  questionId,
  answerId,
}: GetPayload): Promise<GetSuccess | ErrorDatabase> {
  try {
    const key = db.key(
      [
        COURSES,
        courseId,
        COURSE_DISCUSSION_QUESTIONS,
        questionId,
        COURSE_DISCUSSION_ANSWERS,
        answerId,
      ],
      1,
    );

    const [question] = await db.get(key);

    if (!question) {
      return {
        statusCode: 404,
        error: 'Question Not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: {
        ...question,
        courseId,
        questionId,
        answerId,
        authorId: parseInt(question.authorId.id, 10),
      },
    };
  } catch (error) {
    console.log(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// get({
//   courseId: 111,
//   questionId: 4749890231992320,
//   answerId: 5083179694161920,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/** ************************************************************************ */

type UpdatePayload = {
  courseId: number,
  questionId: number,
  answerId: number,
  title?: string,
  data?: any,
};

type UpdateSuccess = {
  statusCode: number,
  message: string,
};

/**
 * This function will update the answer
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.answerId
 * @returns {Promise}
 *
 * @example
 *
 * update({
 *  courseId: 111,
 *  questionId: 4749890231992320,
 *  answerId: 5312840185413632,
 *  title: 'YO',
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function update({
  courseId,
  questionId,
  answerId,
  data,
}: UpdatePayload): Promise<UpdateSuccess | ErrorDatabase> {
  try {
    const key = db.key([
      COURSES,
      courseId,
      COURSE_DISCUSSION_QUESTIONS,
      questionId,
      COURSE_DISCUSSION_ANSWERS,
      answerId,
    ]);

    const [answer] = await db.get(key);

    if (!answer) {
      return {
        statusCode: 404,
        error: 'Not Found',
      };
    }

    let payload = { ...answer };

    if (data) {
      payload = { ...answer, data };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.log(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// update({
//   courseId: 111,
//   questionId: 4749890231992320,
//   answerId: 5312840185413632,
//   answer: 'YO',
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/** ************************************************************************ */

type DeletePayload = {
  courseId: number,
  questionId: number,
  answerId: number,
};

type DeleteSuccess = {
  statusCode: number,
  message: string,
};

/**
 * This function will delete the answer of a question
 * @async
 * @function deleteAnswer
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.answerId
 * @returns {Promise}
 *
 * @example
 *
 * deleteAnswer({
 *  courseId: 111,
 *  questionId: 4820258976169984,
 *  answerId: 5453577673768960,
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function deleteAnswer({
  courseId,
  questionId,
  answerId,
}: DeletePayload): Promise<DeleteSuccess | ErrorNotFound | ErrorDatabase> {
  try {
    const key = db.key([
      COURSES,
      courseId,
      COURSE_DISCUSSION_QUESTIONS,
      questionId,
      COURSE_DISCUSSION_ANSWERS,
      answerId,
    ]);

    const [answer] = await db.get(key);

    if (answer.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteAnswer({
//   courseId: 111,
//   questionId: 4820258976169984,
//   answerId: 5453577673768960,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
