/**
 * This file contains all the variables and functions related to courses
 *
 * @format
 */

import db from './config';
import { COURSES, COURSE_MODULES } from './kinds';

/**
 * Getting all modules of a course.
 * @async
 * @function getModules
 * @param {object} payload -
 * @param {number} payload.courseId -
 * @return {Promise} -
 *
 * @example
 * getModules({ courseId: 6491516650389504 })
 *   .then(res => console.log(res))
 *   .catch(err => console.log(err));
 */
export async function getModules({ courseId }) {
  try {
    const query = db
      .createQuery(COURSE_MODULES)
      .hasAncestor(db.key([COURSES, courseId]))
      .order('timestamp', {
        descending: true,
      });

    const [modules] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: modules.map((m) => ({
        ...m,
        courseId,
        moduleId: parseInt(m.moduleId.id, 10),
      })),
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// getModules({ courseId: 6491516650389504 })
//   .then(res => console.log(res))
//   .catch(err => console.log(err));

/**
 * getModule will give a module by courseId and moduleId.
 * @async
 * @function getModule
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @return {Promise}
 *
 * @example
 * getModule({
 *   courseId: 4943404278480896,
 *   moduleId: 4943404278480896,
 * }).then(e => console.log(e))
 *   .catch(e => console.error(e));
 */
export async function getModule({ courseId, moduleId }) {
  try {
    const key = db.key([COURSES, courseId, COURSE_MODULES, moduleId]);

    const [courseModule] = await db.get(key);

    if (!courseModule) {
      return {
        statusCode: 404,
        message: 'Module Not Found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Module Found',
      payload: { ...courseModule, courseId, moduleId },
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// getModule({
//   courseId: 4943404278481236,
//   moduleId: 4943404278480896,
// }).then(e => console.log(e))
//   .catch(e => console.error(e));

/**
 * createModule will create modules by it's courseId
 * @async
 * @function createModule
 * @param {object} payload -
 * @param {number} payload.courseId -
 * @param {string} payload.title -
 * @return {Promise}
 *
 * @example
 * createModule({
 *   courseId: 6491516650389504,
 *   title: 'Introduction',
 * })
 *   .then(res => console.log(res))
 *   .catch(err => console.error(err));
 */
export async function createModule({ courseId, title }) {
  try {
    const [keys] = await db.allocateIds(
      db.key([COURSES, courseId, COURSE_MODULES]),
      1,
    );

    const key = keys[0];
    const data = {
      courseId: db.key([COURSES, courseId]),
      moduleId: key,
      title,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Course module created',
      payload: {
        ...data,
        courseId,
        moduleId: parseInt(key.id, 10),
      },
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * updateModule will update module by moduleId.
 * @async
 * @function updateModule
 * @param {object} payload -
 * @param {number} payload.courseId - ModuleId
 * @param {number} payload.moduleId - ModuleId
 * @param {string} payload.title - Module Title
 * @param {object} payload.data - Module content in draft.js raw content.
 * @return {Promise}
 *
 * @example
 */
export async function updateModule({ courseId, moduleId, title, data }) {
  try {
    const key = db.key([COURSES, courseId, COURSE_MODULES, moduleId]);

    const [module] = await db.get(key);

    if (!module) {
      return {
        statusCode: 404,
        error: 'Module Not Found.',
      };
    }

    let payload = { ...module };

    if (title) {
      payload = { ...payload, title };
    }

    if (data) {
      payload = { ...payload, data };
    }

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data: payload });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 200,
      message: 'Course Module updated successfully.',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

/**
 * deleteModule will delete a module by it's Id.
 * @async
 * @function deleteModule
 * @param {object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @return {Promise}
 *
 * @example
 * deleteModule({
 *    courseId: 5506354231902208,
 *    moduleId: 5506354231902208,
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function deleteModule({ courseId, moduleId }) {
  try {
    const key = db.key([COURSES, courseId, COURSE_MODULES, moduleId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Module Not found.',
      };
    }

    return {
      statusCode: 200,
      message: 'Course Module Successfully Deleted.',
    };
  } catch (e) {
    console.error(e);
    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteModule({
//   courseId: 5506354231902208,
//   moduleId: 5506354231902208,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
