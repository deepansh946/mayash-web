/**
 * This file contains all the function related to follow feature
 *
 * @format
 * @description - followingId - user/circle who will get followed
 * NOTE: only user will be a follower, circle will not follow any user or other circle.
 */

import db from './config';
import { ELEMENTS, FOLLOW } from './kinds';

/**
 * This function will create a follower to a user
 * @async
 * @function createFollower
 * @param {Object} payload
 * @param {number} payload.followerId - user's id who will follow
 * @param {number} payload.followingId - user/circle to be followed
 * @returns {Promise}
 *
 * @example
 * createFollower({
 *  followerId: 5585519019102080,
 *  followingId: 5585519069102080,
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function createFollower({ followerId, followingId }) {
  try {
    const key = db.key([ELEMENTS, followerId, FOLLOW, followingId]);

    const data = {
      followerId: db.key([ELEMENTS, followerId]),
      followingId: db.key([ELEMENTS, followingId]),
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { followerId, followingId },
    };
  } catch (e) {
    console.error(e);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// createFollower({
//   followerId: 5585519019102080,
//   followingId: 5585519019102081,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will check if the follower exists or not
 * @async
 * @function getFollower
 * @param {Object} payload
 * @param {number} payload.followerId
 * @param {number} payload.followingId
 * @returns {Promise}
 *
 * @example
 * getFollower({
 *  followerId: 5585519019102080,
 *  followingId: 5585519019102081,
 * })
 *   .then(e => console.log(e))
 *   .catch(e => console.log(e));
 */
export async function getFollower({ followerId, followingId }) {
  try {
    const key = db.key([ELEMENTS, followerId, FOLLOW, followingId]);

    const [follower] = await db.get(key);

    if (!follower) {
      return {
        statusCode: 404,
        error: 'Follower not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Follower found',
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getFollower({
//   followerId: 5585519019102080,
//   followingId: 5585519019102081,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will give the list of all followers
 * @async
 * @function getAllFollowers
 * @param {Object} payload
 * @param {number} payload.followingId
 * @returns {Promise}
 *
 * @example
 * getAllFollowers({ followingId: 5585519019102081 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e))
 */
export async function getAllFollowers({ followingId }) {
  try {
    const key = db.key([ELEMENTS, followingId]);

    const query = db
      .createQuery(FOLLOW)
      .filter('followingId', '=', key)
      .order('timestamp', { descending: true });

    const [followers] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: followers.map(({ followerId }) => parseInt(followerId.id, 10)),
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllFollowers({ followingId: 5585519019102081 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will display all the followings of a follower
 * @async
 * @function getAllFollowings
 * @param {Object} payload
 * @param {number} payload.followerId
 * @returns {Promise}
 *
 * @example
 * getAllFollowings({ followerId: 5585519019102080 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAllFollowings({ followerId }) {
  try {
    const key = db.key([ELEMENTS, followerId]);

    const query = db
      .createQuery(FOLLOW)
      .hasAncestor(key)
      .order('timestamp', { descending: true });

    const [followings] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success',
      payload: followings.map(({ followingId }) => ({
        followingId: parseInt(followingId.id, 10),
      })),
    };
  } catch (err) {
    console.error(err);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// getAllFollowings({ followerId: 5585519019102080 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will delete the follower
 * @async
 * @function deleteFollower
 * @param {Object} payload
 * @param {number} payload.followerId - user's id who will follow
 * @param {number} payload.followingId - user/circle to be followed
 * @returns {Promise}
 *
 * @example
 * deleteFollower({
 *  followerId: 5585519019102080,
 *  followingId: 5585519069102080,
 * })
 *  .then(e => console.log(e))
 *  .catch(e => console.log(e));
 */
export async function deleteFollower({ followerId, followingId }) {
  try {
    const key = db.key([ELEMENTS, followerId, FOLLOW, followingId]);

    const [result] = await db.delete(key);

    if (result.indexUpdates === 0) {
      return {
        statusCode: 404,
        error: 'Follower not found',
      };
    }

    return {
      statusCode: 200,
      message: 'Follower deleted',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error',
    };
  }
}

// deleteFollower({
//   followerId: 5585519019102080,
//   followingId: 5585519019102081,
// })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));
