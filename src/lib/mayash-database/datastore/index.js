/**
 * This is the main file for mayash-database repository
 * This file contains all the sub-files and will export it in a
 * single object
 *
 * @format
 */

// import all the functions and variables from elements.js file to
// elements variable
import * as elements from './elements';

// import all the functions of users from users.js file to
// users variable
import * as users from './users';

// import all the functions and variables from circles.js file to
// circles variable
import * as circles from './circles';

// import all the functions and variables from posts.js file to
// posts variable
import * as posts from './posts';

// import all the functions and variables from courses.js file to
// courses variable
import * as courses from './courses';

// import all the functions and variables from classrooms.js file
// to classrooms variable
import * as classrooms from './classrooms';

// to photos variable
import * as photos from './photos';

// import the files to a file variable

import * as files from './files';

// import all the functions and variables from notes.js file
// to notes variable
import * as notes from './notes';

// import all the functions and variables from bookmarks.js file
// to bookmarks variable
import * as bookmarks from './bookmarks';

// import all the functions and variables from follow.js file
// to follow variable
import * as follow from './follow';

// import all function and variable form auth.js file
// to auth variable
import * as auth from './auth';
/*
* This is done because we need all the objects as a single one.
* Elements , Posts , courses and Classrooms in a single object.
*/
// import all function and variable form resume.js file
// to resume variable
import * as resume from './resume';

// import all function and variable from
// question.js file from /courses/discussion/question
// to  courseDiscussionQuestions variable

import * as courseDiscussionQuestions from './courses/discussion/questions';
// import all function and variable from
// answer.js file from /courses/discussion/answer
// to  courseDiscussionAnswer variable

import * as courseDiscussionAnswers from './courses/discussion/answers';

// export all variables and functions of all files in a single object by default
export default {
  elements,
  users,
  circles,
  posts,
  courses,
  classrooms,
  photos,
  files,
  notes,
  bookmarks,
  auth,
  resume,
  follow,
  courseDiscussionQuestions,
  courseDiscussionAnswers,
};
