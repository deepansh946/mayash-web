/**
 * This file contains all the functions of files for courses.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, COURSES, FILES } from '../kinds';

/**
 * This function will get all the files of the courses
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @returns {Promise}
 *
 * @example
 * getAll({ courseId: 5963751069057024 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({ courseId }) {
  try {
    const query = db
      .createQuery(FILES)
      .hasAncestor(db.key([COURSES, courseId]))
      // limiting the result to 5 responses
      .limit(5);

    const [files] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: files.map((p) => ({
        ...p,
        courseId,
        fileId: parseInt(p.fileId.id, 10),
        authorId: parseInt(p.authorId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ courseId: 5963751069057024 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a file of a course
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 * get({ courseId: 123456789012, fileId: 5154510511013888 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ courseId, fileId }) {
  try {
    const key = db.key([COURSES, courseId, FILES, fileId]);

    const [file] = await db.get(key);

    if (!file) {
      return {
        statusCode: 404,
        error: 'File Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...file,
        authorId: parseInt(file.authorId.id, 10),
        courseId,
        fileId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ courseId: 123456789012, fileId: 5154510511013888 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create a file of course
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {number} payload.courseId -
 * @param {string} payload.type - file extention i.e. (pdf)
 * @returns {Promise}
 *
 * @example
 * create({ id: 123456789012, courseId: 123456789012, type: 'pdf' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ id, courseId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([COURSES, courseId, FILES]), 1);

    const key = keys[0];
    const authorId = id;
    const fileId = parseInt(key.id, 10);

    const data = {
      fileId: key,
      authorId: db.key([ELEMENTS, id]),
      courseId: db.key([COURSES, courseId]),
      name: `${fileId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, courseId, fileId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ id: 123456789012, courseId: 123456789012, type: 'pdf' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

// 5717460464435200;
/**
 *
 * @async
 * @function deleteFile
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 */
export async function deleteFile({ courseId, fileId }) {
  try {
    const key = db.key([COURSES, courseId, FILES, fileId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteFile({ courseId: 123456789012, fileId: 5154510511013888 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
