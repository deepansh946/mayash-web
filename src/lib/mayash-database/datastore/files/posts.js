/**
 * This file contains all the functions of files for posts.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, POSTS, FILES } from '../kinds';

/**
 * getAll will give a post by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.postId
 * @returns {Promise}
 *
 * @example
 * getAll({ postId: 5963751069057024 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({ postId }) {
  try {
    const query = db
      .createQuery(FILES)
      .hasAncestor(db.key([POSTS, postId]))
      // limiting the result to 5 responses
      .limit(5);

    const [files] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: files.map((p) => ({
        ...p,
        postId,
        fileId: parseInt(p.fileId.id, 10),
        authorId: parseInt(p.authorId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ postId: 5963751069057024 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a file of a post
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 * get({ postId: 123456789012, fileId: 6280410417856512 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ postId, fileId }) {
  try {
    const key = db.key([POSTS, postId, FILES, fileId]);

    const [file] = await db.get(key);

    if (!file) {
      return {
        statusCode: 404,
        error: 'File Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...file,
        authorId: parseInt(file.authorId.id, 10),
        postId,
        fileId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ postId: 123456789012, fileId: 6280410417856512 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create the files under the post
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {number} payload.postId -
 * @param {string} payload.type - file extention i.e. (pdf)
 * @returns {Promise}
 *
 * @example
 * create({ id: 123456789012, postId: 123456789012, type: 'pdf' })\
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ id, postId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([POSTS, postId, FILES]), 1);

    const key = keys[0];
    const authorId = id;
    const fileId = parseInt(key.id, 10);

    const data = {
      fileId: key,
      authorId: db.key([ELEMENTS, id]),
      postId: db.key([POSTS, postId]),
      name: `${fileId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, postId, fileId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ id: 123456789012, postId: 123456789012, type: 'pdf' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * this function will delete the file of a post
 * @async
 * @function deleteFile
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 * deleteFile({ postId: 123456789012, fileId: 6280410417856512 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deleteFile({ postId, fileId }) {
  try {
    const key = db.key([POSTS, postId, FILES, fileId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// deleteFile({ postId: 123456789012, fileId: 6280410417856512 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
