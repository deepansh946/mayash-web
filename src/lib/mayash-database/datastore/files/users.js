/**
 * This file contains all the functions for files of users.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, FILES } from '../kinds';

/**
 * This function will get the files of a user's
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload
 * @returns {Promise}
 *
 * @example
 * getAll({ userId: 5963751069057024 })
 * .then(e => console.log(e))
 * .catch(e => console.log(e));
 */
export async function getAll({ userId }) {
  try {
    const query = db
      .createQuery(FILES)
      .hasAncestor(db.key([ELEMENTS, userId]))
      // limiting the result to 5 responses
      .limit(10);

    const [files] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: files.map((p) => ({
        ...p,
        fileId: parseInt(p.fileId.id, 10),
        authorId: userId,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ userId: 5963751069057024 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a file of a user
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 */
export async function get({ userId, fileId }) {
  try {
    const key = db.key([ELEMENTS, userId, FILES, fileId]);

    const [file] = await db.get({ key });

    if (!file) {
      return {
        statusCode: 404,
        error: 'File Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...file,
        authorId: userId,
        fileId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will create the file of a user
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.type - file extention i.e. (pdf)
 * @returns {Promise}
 *
 * @example
 * create({ userId: 123456789012, type: 'pdf' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ userId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([ELEMENTS, userId, FILES]), 1);

    const key = keys[0];
    const authorId = userId;
    const fileId = parseInt(key.id, 10);

    const data = {
      fileId: key,
      authorId: db.key([ELEMENTS, userId]),
      name: `${fileId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, fileId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// 4873035534303232;
// create({ userId: 123456789012, type: 'pdf' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
/**
 * this function will delete the file of user
 * @async
 * @function deleteFile
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 * deleteFile({ userId: 123456789012, fileId: 4873035534303232 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deleteFile({ userId, fileId }) {
  try {
    const key = db.key([ELEMENTS, userId, FILES, fileId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// deleteFile({ userId: 123456789012, fileId: 4873035534303232 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
