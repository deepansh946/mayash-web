/**
 * This file contains all the functions of files for circles
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, FILES } from '../kinds';

/**
 * getAll will give a post by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.circleId
 * @returns {Promise}
 *
 * @example
 * getAll({ circleId: 123456789012 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getAll({ circleId }) {
  try {
    const query = db
      .createQuery(FILES)
      .hasAncestor(db.key([ELEMENTS, circleId]))
      // limiting the result to 5 responses
      .limit(5);

    const [files] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: files.map((p) => ({
        ...p,
        fileId: parseInt(p.fileId.id, 10),
        authorId: circleId,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ circleId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will get a file of a circle
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 * get({ circleId: 123456789012, fileId: 4591560557592576 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ circleId, fileId }) {
  try {
    const key = db.key([ELEMENTS, circleId, FILES, fileId]);

    const [file] = await db.get(key);

    if (!file) {
      return {
        statusCode: 404,
        error: 'File Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...file,
        authorId: circleId,
        fileId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ circleId: 123456789012, fileId: 4591560557592576 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create the files for a circle
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.type - file extension (mp4)
 * @returns {Promise}
 *
 * @example
 * create({ circleId: 123456789012, type: 'pdf' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ circleId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([ELEMENTS, circleId, FILES]), 1);

    const key = keys[0];
    const authorId = circleId;
    const fileId = parseInt(key.id, 10);

    const data = {
      authorId: db.key([ELEMENTS, circleId]),
      fileId: key,
      name: `${fileId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, fileId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ circleId: 123456789012, type: 'pdf' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 *
 * @async
 * @function deleteFile
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.fileId -
 * @returns {Promise}
 *
 * @example
 * deleteFile({ circleId: 123456789012, fileId: 5506354231902208 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deleteFile({ circleId, fileId }) {
  try {
    const key = db.key([ELEMENTS, circleId, FILES, fileId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteFile({ circleId: 123456789012, fileId: 5506354231902208 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
