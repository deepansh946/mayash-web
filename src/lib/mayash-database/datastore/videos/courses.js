/**
 * This file contains all the functions of videos for courses.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, COURSES, VIDEOS } from '../kinds';

/**
 * This function will get the videos of the course
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @returns {Promise}
 *
 * @example
 * getAll({ courseId: 123456789012 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function getAll({ courseId }) {
  try {
    const query = db
      .createQuery(VIDEOS)
      .hasAncestor(db.key([COURSES, courseId]))
      // limiting the result to 5 responses
      .limit(5);

    const [videos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: videos.map((p) => ({
        ...p,
        courseId,
        videoId: parseInt(p.videoId.id, 10),
        authorId: parseInt(p.authorId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ courseId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will get a video of a post
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 * get({ courseId: 123456789012, videoId: 6350779162034176 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ courseId, videoId }) {
  try {
    const key = db.key([COURSES, courseId, VIDEOS, videoId]);

    const [video] = await db.get(key);

    if (!video) {
      return {
        statusCode: 404,
        error: 'Video Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...video,
        authorId: parseInt(video.authorId.id, 10),
        courseId,
        videoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ courseId: 123456789012, videoId: 6350779162034176 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create the video under the course
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {number} payload.courseId -
 * @param {string} payload.type - video extension i.e. (mp4)
 * @returns {Promise}
 *
 * @example
 * create({ id: 123456789012, courseId: 123456789012, type: 'mp4' })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export async function create({ id, courseId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([COURSES, courseId, VIDEOS]), 1);

    const key = keys[0];
    const authorId = id;
    const videoId = parseInt(key.id, 10);

    const data = {
      videoId: key,
      authorId: db.key([ELEMENTS, id]),
      courseId: db.key([COURSES, courseId]),
      name: `${videoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, courseId, videoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ id: 123456789012, courseId: 123456789012, type: 'mp4' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete the video of the course
 * @async
 * @function deleteVideo
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 * deleteVideo({ courseId: 123456789012, videoId: 6350779162034176 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deleteVideo({ courseId, videoId }) {
  try {
    const key = db.key([COURSES, courseId, VIDEOS, videoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}
// deleteVideo({ courseId: 123456789012, videoId: 6350779162034176 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
