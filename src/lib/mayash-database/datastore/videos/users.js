/**
 * This file contains all the functions for videos of users.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, VIDEOS } from '../kinds';

/**
 * this function will get all the videos of a user
 * @async
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.userId
 * @returns {Promise}
 *
 * @example
 * getAll({ userId: 5963751069057024 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({ userId }) {
  try {
    const query = db
      .createQuery(VIDEOS)
      .hasAncestor(db.key([ELEMENTS, userId]))
      // limiting the result to 5 responses
      .limit(10);

    const [videos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: videos.map((p) => ({
        ...p,
        videoId: parseInt(p.videoId.id, 10),
        authorId: userId,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ userId: 5963751069057024 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a video of a user
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 */
export async function get({ userId, videoId }) {
  try {
    const key = db.key([ELEMENTS, userId, VIDEOS, videoId]);

    const [video] = await db.get(key);

    if (!video) {
      return {
        statusCode: 404,
        error: 'Video Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...video,
        authorId: userId,
        videoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

/**
 * This function will create a video for a user
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.type - video extension i.e. (mp4)
 * @returns {Promise}
 *
 * @example
 * create({ userId: 123456789012, type: 'mp4' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ userId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([ELEMENTS, userId, VIDEOS]), 1);

    const key = keys[0];
    const authorId = userId;
    const videoId = parseInt(key.id, 10);

    const data = {
      videoId: key,
      authorId: db.key([ELEMENTS, userId]),
      name: `${videoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, videoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ userId: 123456789012, type: 'mp4' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete the video of a user
 * @async
 * @function deleteVideo
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 */
export async function deleteVideo({ userId, videoId }) {
  try {
    const key = db.key([ELEMENTS, userId, VIDEOS, videoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteVideo({ userId: 12356789012, videoId: 6069304185323520 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
