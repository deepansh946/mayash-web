/**
 * This file contains all the functions of videos for circles
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, VIDEOS } from '../kinds';

/**
 * getAll will give a post by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @returns {Promise}
 *
 * @example
 * getAll({ circleId: 123456789012 })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export async function getAll({ circleId }) {
  try {
    const query = db
      .createQuery(VIDEOS)
      .hasAncestor(db.key([ELEMENTS, circleId]))
      // limiting the result to 5 responses
      .limit(5);

    const [videos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: videos.map((p) => ({
        ...p,
        videoId: parseInt(p.videoId.id, 10),
        authorId: circleId,
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ circleId: 123456789012 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will get a video of a circle
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 * get({ circleId: 123456789012, videoId: 5787829208612864 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ circleId, videoId }) {
  try {
    const key = db.key([ELEMENTS, circleId, VIDEOS, videoId]);

    const [video] = await db.get(key);

    if (!video) {
      return {
        statusCode: 404,
        error: 'Video Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...video,
        authorId: circleId,
        videoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ circleId: 123456789012, videoId: 5787829208612864 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create a video for a circle
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.type - video extension i.e. (mp4)
 * @returns {Promise}
 *
 * @example
 * create({ circleId: 123456789012, type: 'mp4' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ circleId, type }) {
  try {
    const [keys] = await db.allocateIds(
      db.key([ELEMENTS, circleId, VIDEOS]),
      1,
    );

    const key = keys[0];
    const authorId = circleId;
    const videoId = parseInt(key.id, 10);

    const data = {
      authorId: db.key([ELEMENTS, circleId]),
      videoId: key,
      name: `${videoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, videoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ circleId: 123456789012, type: 'mp4' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete the video of the circle
 * @async
 * @function deleteVideo
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 */
export async function deleteVideo({ circleId, videoId }) {
  try {
    const key = db.key([ELEMENTS, circleId, VIDEOS, videoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteVideo({ circleId: 123456789012, videoId: 5787829208612864 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
