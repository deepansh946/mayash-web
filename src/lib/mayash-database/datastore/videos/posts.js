/**
 * This file contains all the functions of videos for posts.
 *
 * @format
 */

import db from '../config';
import { ELEMENTS, POSTS, VIDEOS } from '../kinds';

/**
 * getAll will give a post by it's Id.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.postId
 * @returns {Promise}
 *
 * @example
 * getAll({ postId: 5963751069057024 })
 *    .then(e => console.log(e))
 *    .catch(e => console.log(e));
 */
export async function getAll({ postId }) {
  try {
    const query = db
      .createQuery(VIDEOS)
      .hasAncestor(db.key([POSTS, postId]))
      // limiting the result to 5 responses
      .limit(5);

    const [videos] = await db.runQuery(query);

    return {
      statusCode: 200,
      message: 'Success.',
      payload: videos.map((p) => ({
        ...p,
        postId,
        videoId: parseInt(p.videoId.id, 10),
        authorId: parseInt(p.authorId.id, 10),
      })),
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database server error.',
    };
  }
}

// getAll({ postId: 5963751069057024 })
//   .then(e => console.log(e))
//   .catch(e => console.log(e));

/**
 * This function will get a video under the post
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 * get({ postId: 123456789012, videoId: 4943404278480896 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function get({ postId, videoId }) {
  try {
    const key = db.key([POSTS, postId, VIDEOS, videoId]);

    const [video] = await db.get(key);

    if (!video) {
      return {
        statusCode: 404,
        error: 'Video Not Found',
      };
    }

    return {
      statusCode: 201,
      message: 'Success',
      payload: {
        ...video,
        authorId: parseInt(video.authorId.id, 10),
        postId,
        videoId,
      },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// get({ postId: 123456789012, videoId: 4943404278480896 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will create the video under the post
 * @async
 * @function create
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {number} payload.postId -
 * @param {string} payload.type - video extension i.e. (mp4)
 * @returns {Promise}
 *
 * @example
 * create({ id: 123456789012, postId: 123456789012, type: 'mp4' })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function create({ id, postId, type }) {
  try {
    const [keys] = await db.allocateIds(db.key([POSTS, postId, VIDEOS]), 1);

    const key = keys[0];
    const authorId = id;
    const videoId = parseInt(key.id, 10);

    const data = {
      videoId: key,
      authorId: db.key([ELEMENTS, id]),
      postId: db.key([POSTS, postId]),
      name: `${videoId}.${type}`,
      timestamp: new Date().toISOString(),
    };

    /* eslint-disable no-unused-vars */
    const [res] = await db.save({ key, data });
    /* eslint-enable no-unused-vars */

    return {
      statusCode: 201,
      message: 'Success',
      payload: { authorId, postId, videoId },
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// create({ id: 123456789012, postId: 123456789012, type: 'mp4' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete the video of the post
 * @async
 * @function deleteVideo
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {number} payload.videoId -
 * @returns {Promise}
 *
 * @example
 * deleteVideo({ postId: 123456789012, videoId: 4943404278480896 })
 *    .then((e) => console.log(e))
 *    .catch((e) => console.log(e));
 */
export async function deleteVideo({ postId, videoId }) {
  try {
    const key = db.key([POSTS, postId, VIDEOS, videoId]);

    /* eslint-disable no-unused-vars */
    const [res] = await db.delete(key);
    /* eslint-enable no-unused-vars */

    if (res.indexUpdates === 0) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 500,
      error: 'Database Server Error.',
    };
  }
}

// deleteVideo({ postId: 123456789012, videoId: 4943404278480896 })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
