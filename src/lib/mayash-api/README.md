## Mayash API

This is an Hapi.js plugins. It is a combination of all the route APIs.

This library is divided into sub parts depending on their features.

**Mayash API Elements**: this library contains all the API routes related to elements.

**Mayash API Posts**: this library contains all the API routes related to posts.

**Mayash API Courses**: this library contains all the API routes related to courses.

**Mayash API Classrooms**: this library contains all the API routes related to classrooms.

**Mayash API Photos**: this library contains all the API routes related to photos.

**Mayash API Videos**: this library contains all the API routes related to videos.

**Mayash API Files**: this library contains all the API routes related to files.
