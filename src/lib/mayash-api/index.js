/**
 * This Hapi Plugin collects all the sub API routes and export it as
 * on Hapi plugin for APIs.
 *
 * @note - don't change the routes sequence.
 * @format
 */

import apiAuth from '../mayash-api-auth';
import apiElements from '../mayash-api-elements';
import apiPosts from '../mayash-api-posts';
import apiCourses from '../mayash-api-courses';
import apiNotes from '../mayash-api-notes';
import apiBookmarks from '../mayash-api-bookmarks';
import apiFollow from '../mayash-api-follow';
import apiResumes from '../mayash-api-resume';

import apiPhotos from '../mayash-api-photos';
import apiVideos from '../mayash-api-videos';
import apiFiles from '../mayash-api-files';
// import apiSearch from '../mayash-api-search';

const { NODE_ENV } = process.env;

let routes = [
  ...apiAuth,
  ...apiElements,
  ...apiPosts,
  ...apiCourses,
  ...apiNotes,
  ...apiBookmarks,
  ...apiFollow,
  ...apiResumes,
];

if (NODE_ENV === 'development') {
  // routes = [...routes, ...apiSearch];
}

routes = [...routes, ...apiPhotos, ...apiVideos, ...apiFiles];

const register = (server, options, next) => {
  server.route([
    ...routes,

    /**
     * This API will handle all Not defined APIs
     * and will return 'API Not Found' error message.
     */
    {
      method: 'GET',
      path: '/{url*}',
      config: {
        handler: (request, reply) => {
          reply({
            statusCode: 404,
            error: 'API Not Found.',
          });
        },
      },
    },
  ]);

  next();
};

register.attributes = {
  pkg: {
    name: 'mayash-api',
    version: '0.0.0',
    description: '',
  },
};

export default register;
