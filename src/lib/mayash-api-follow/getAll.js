/** @format */

import Joi from 'joi';

import { Id, Headers /* , Next */ } from '../mayash-common/schema';
import {
  getAllFollowers,
  getAllFollowings,
} from '../mayash-database/datastore/follow';

export default {
  tags: ['api', 'follow'],
  description: 'This will display all followers',
  notes: 'If sucessfull it return a success message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      id: Id.required(),
    }),
    query: Joi.object({
      type: Joi.string()
        .valid('followers', 'followings')
        .required(),
      // next: Next,
    })
      .min(1)
      .max(2),
  },
  async handler(request, reply) {
    try {
      const { id } = request.params;
      const { type /* , next */ } = request.query;

      if (type === 'followers') {
        const res = await getAllFollowers({ followingId: id });

        reply(res);
        return;
      }

      const res = await getAllFollowings({ followerId: id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
