/** @format */

import followGetAll from './getAll';
import followGet from './get';
import followCreate from './create';
import followDelete from './delete';

const routes = [];

/* ------------------------------------------------------------------
                        Routes for Follow
   ------------------------------------------------------------------ */
routes.push({
  method: 'GET',
  path: '/follow/{id}',
  config: followGetAll,
});

routes.push({
  method: 'GET',
  path: '/follow/{followerId}/{followingId}',
  config: followGet,
});

routes.push({
  method: 'POST',
  path: '/follow/{followerId}/{followingId}',
  config: followCreate,
});

routes.push({
  method: 'DELETE',
  path: '/follow/{followerId}/{followingId}',
  config: followDelete,
});

export default routes;
