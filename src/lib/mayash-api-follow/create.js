/** @format */

import Joi from 'joi';

import { Id, Headers } from '../mayash-common/schema';
import { createFollower } from '../mayash-database/datastore/follow';

export default {
  tags: ['api', 'follow'],
  description: 'This will create a new follower',
  notes: 'If sucessfull it return a success message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      followerId: Id.required(),
      followingId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { followerId, followingId } = request.params;

      const res = await createFollower({ followerId, followingId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
