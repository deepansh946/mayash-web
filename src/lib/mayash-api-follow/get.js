/** @format */

import Joi from 'joi';

import { Id, Headers } from '../mayash-common/schema';
import { getFollower } from '../mayash-database/datastore/follow';

export default {
  tags: ['api', 'follow'],
  description: 'This will display a follower',
  notes: 'If sucessfull it return a success message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      followerId: Id.required(),
      followingId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { followerId, followingId } = request.params;
      const res = await getFollower({ followerId, followingId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
