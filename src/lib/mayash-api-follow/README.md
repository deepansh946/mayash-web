## Mayash API Follow

This library is sub part of **Mayash API** plugin.

It contains all the API routes for follow.

## Folder Structure

    .
    ├── README.md
    ├── create.js
    ├── delete.js
    ├── get.js
    ├── getAll.js
    └── index.js