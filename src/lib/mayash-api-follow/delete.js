/** @format */

import Joi from 'joi';

import { Id, Headers } from '../mayash-common/schema';
import { deleteFollower } from '../mayash-database/datastore/follow';

export default {
  tags: ['api', 'follow'],
  description: 'This will delete follower',
  notes: 'If sucessfull it return a success message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      followerId: Id.required(),
      followingId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { followerId, followingId } = request.params;
      const res = await deleteFollower({ followerId, followingId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
