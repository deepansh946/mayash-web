## Mayash Common

This library contains all the common functions required in development of server. for example Joi schemas etc.

## Folder Structure

    .
    ├── README.md
    ├─┬ RegExp
    │ ├── ClassId.js
    │ ├── CourseId.js
    │ ├── DegreeId.js
    │ ├── DeptId.js
    │ ├── Description.js
    │ ├── ModuleId.js
    │ ├── Name.js
    │ ├── Password.js
    │ ├── PostId.js
    │ ├── SemesterId.js
    │ ├── Title.js
    │ ├── Username.js
    │ └── index.js
    ├── index.js
    └── schema.js
