/**
 * /*
 * Here we are creating validation schemes for our data
 *
 * @format
 */

import Joi from 'joi';
import RegExp from './RegExp';

/* ==================================================================
                            Headers
   ================================================================== */
// This is for auth token generated by jwt.
// TODO: improve it.
export const Authorization = Joi.string();

// This schema is for header validation.
export const Headers = Joi.object({
  authorization: Authorization.required(),
}).unknown();

/* ==================================================================
                            Elements
   ================================================================== */
// Schema for id
export const Id = Joi.number().integer();

// Schema for Username
export const Username = Joi.string()
  .trim()
  .regex(RegExp.Username);

// console.log(Joi.validate( 'nS99', Username));

// Schema for Password
export const Password = Joi.string().regex(RegExp.Password);

// console.log(Joi.validate('Ab12', Password));

// Schema for Mobile
export const Mobile = Joi.number()
  .min(7000000000)
  .max(9999999999)
  .integer();

// Schema for email
export const Email = Joi.string().email();

// Schema for Name
export const Name = Joi.string().regex(RegExp.Name);

// console.log(Joi.validate('Asg', Name));

// Schema for Date Of Birth
export const Dob = Joi.date().min('1-1-1974');

// Schema for type of Elements
export const ElementType = Joi.string().valid('user', 'circle');

// Schema for types of circles
export const CircleType = Joi.string().valid('edu', 'org', 'location', 'field');

export const CircleMemberRole = Joi.string().valid(['admin', 'member']);

// Schema for Guru
export const Guru = Joi.boolean();

// Schema for Classroom
export const Classroom = Joi.boolean();

// Schema for Semester
export const Semester = Joi.number().integer();
// Schema for Degree
export const Degree = Joi.string();
// Schema for Next

/* ==================================================================
                            Posts
   ================================================================== */

// Schema for postId
export const PostId = Joi.number().integer();

// Schema for postType
export const PostType = Joi.string().valid('status', 'article', 'report');

// Schema for Title
export const Title = Joi.string()
  .min(3)
  .max(148);

// Schema for Description
export const Description = Joi.string()
  .min(1)
  .max(300);

// Schema for Data, this schema will validate draft.js rowState.
export const Data = Joi.object();

export const Reaction = Joi.any().valid('like');
export const ReactionCounts = Joi.array()
  .items(
    Joi.number()
      .integer()
      .positive(),
  )
  .length(1);

/* ==================================================================
                            Courses
   ================================================================== */

// Schema for CourseId
export const CourseId = Joi.number().integer();

// Schema for ModuleId
export const ModuleId = Joi.number().integer();

// Schema for CourseModules
export const CourseModules = Joi.array().items(
  Joi.object({
    moduleId: ModuleId,
  }).length(1),
);

// console.log(Joi.validate(
//   [
//     {
//       moduleId: 456,
//     },
//     {
//       moduleId: 456,
//     },
//     {
//       moduleId: 456,
//     },
//     {
//       moduleId: 456,
//     },
//   ],
//   CourseModules,
// ));

// Schema for CourseUrl
export const CourseUrl = Joi.string().regex(/[a-zA-Z0-9-]+/);

// Schema for Standard
export const Standard = Joi.string().valid(
  'primary',
  'secondary',
  'higher-secondary',
  'graduation',
  'post-graduation',
  'phd',
);

// Schema for Difficulty Level
export const Level = Joi.number()
  .integer()
  .min(1)
  .max(5);

/* ==================================================================
                            Photos
   ================================================================== */

// Schema for ImageId
export const PhotoId = Joi.number().integer();

export const Photo = Joi.object({
  hapi: Joi.object({
    headers: Joi.object({
      'content-type': Joi.any()
        .valid('image/jpeg', 'image/png', 'image/gif')
        .required(),
    })
      .unknown()
      .required(),
  })
    .unknown()
    .required(),
})
  .unknown()
  .required();

// Schema for ImageName
export const PhotoName = Joi.string().regex(
  /^([0-9]{16})+(.jpg|.jpeg|.png|.gif)$/,
);

/* ==================================================================
                          Videos
 ================================================================== */

// Schema for VideoId
export const VideoId = Joi.number().integer();

export const Video = Joi.object({
  hapi: Joi.object({
    headers: Joi.object({
      'content-type': Joi.any()
        .valid('video/mp4', 'video/webm', 'video/ogg')
        .required(),
    })
      .unknown()
      .required(),
  })
    .unknown()
    .required(),
})
  .unknown()
  .required();

// Schema for VideoName
export const VideoName = Joi.string().regex(/^([0-9]{16})+(.mp4|.webm|.ogg)$/);

/* ==================================================================
                          Files
 ================================================================== */

// Schema for FileId
export const FileId = Joi.number().integer();

export const File = Joi.object({
  hapi: Joi.object({
    headers: Joi.object({
      'content-type': Joi.any()
        .valid('application/pdf')
        .required(),
    })
      .unknown()
      .required(),
  })
    .unknown()
    .required(),
})
  .unknown()
  .required();

// Schema for FileName
export const FileName = Joi.string().regex(/^([0-9]{16})+(.pdf)$/);

export const Next = Joi.string();

/* ==================================================================
                            Notes
   ================================================================== */

export const NoteType = Joi.valid('note', 'todo');

// Cover rest all the things here.
