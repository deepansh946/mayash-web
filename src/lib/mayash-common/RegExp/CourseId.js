/**
 * This file contains the RegExp of Course Id.
 *
 * @format
 */

export default '(^[1-9][0-9]{15}$)';
