/**
 * This file contains the RegExp of Degree Id.
 *
 * @format
 */

export default '([a-z]{2,8}$)';
