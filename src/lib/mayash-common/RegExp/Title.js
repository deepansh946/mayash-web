/**
 * This file contains RegExp for title
 *
 * @format
 */

export default '((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{5,20})';
