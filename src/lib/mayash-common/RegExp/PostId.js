/**
 * This file contains the RegExp of Post Id.
 *
 * @format
 */

export default new RegExp('^[1-9][0-9]{15}$');
