/**
 * This file contains the RegExp of password. The password should
 * follow these rules.
 * 1. It should contain atleast one lowercase letter
 * 2. It should contain atleast one uppercase letter
 * 3. It should contain atleast one digit
 * 4. It should contain one special character
 * 5. It's minimum length should be 5.
 * 6. It's maximum length should be 20.
 *
 * @format
 */

export default new RegExp(
  '((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{5,20})',
);
