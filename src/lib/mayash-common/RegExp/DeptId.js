/**
 * This file contains the RegExp of Department Id.
 * 1. Length of DeptId should be 3.
 * 2. It should contain only lowercase letters
 *
 * @format
 */

export default '([a-z]{3}$)';
