/**
 * This file contains the RegExp of username. The username should follow
 * these rules.
 * 1. The username should not start with a number.
 * 2. It should only contain lowercase letters and numbers.
 * 3. It should not end with a dot or an underscore.
 * 4. The underscore or dot should be in between the numbers and alphabets
 * (if entered).
 * 5. It's minimum length should be 3.
 * 6. It's maximum length should be 20.
 *
 * @format
 */

export default new RegExp('(?=^.{3,20}$)^[a-z]+[._]?[a-z0-9]+$');
