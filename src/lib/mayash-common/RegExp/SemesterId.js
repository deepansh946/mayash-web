/**
 * This file contains the RegExp of Semester Id.
 *
 * @format
 */

export default '([0-9]{2}$)';
