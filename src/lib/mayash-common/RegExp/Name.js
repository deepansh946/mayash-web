/**
 * This file contains RegExp for Name. The name should follow all
 * these rules
 * 1. The name should start with an uppercase letter.
 * 2. It's minimum length should be 2.
 * 3. It's maximum length should be 30.
 *
 * @format
 */

export default new RegExp('((?=.*[a-zA-Z]).{2,30})');
