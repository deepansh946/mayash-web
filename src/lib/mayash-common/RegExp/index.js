/** @format */

import Name from './Name';
import Username from './Username';
import Password from './Password';

import Title from './Title';
import Description from './Description';

import PostId from './PostId';

import CourseId from './CourseId';
import ModuleId from './ModuleId';

import DeptId from './DeptId';
import DegreeId from './DegreeId';
import SemesterId from './SemesterId';
import ClassId from './ClassId';

export default {
  Name,
  Username,
  Password,
  Title,
  Description,
  PostId,
  CourseId,
  ModuleId,
  DeptId,
  DegreeId,
  SemesterId,
  ClassId,
};
