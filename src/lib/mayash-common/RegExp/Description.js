/**
 * This file contains RegExp for Description
 *
 * @format
 */

export default '((?=.*[a-zA-Z0-9]).{6,200})';
