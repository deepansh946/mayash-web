/**
 * This file contains the RegExp of ModuleId.
 *
 * @format
 */

export default '(^[1-9][0-9]{15}$)';
