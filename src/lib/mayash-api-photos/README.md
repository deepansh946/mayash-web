## Mayash API Photos

This library is sub part of **Mayash API** plugin.

It contains all the API routes for photos.

## Folder Structure

    .
    ├── README.md
    ├── index.js
    ├── users
    │   ├── create.js
    │   └── get.js
    ├── circles
    │   ├── create.js
    │   └── get.js
    ├── posts
    │   ├── circles
    │   │   └── create.js
    │   ├── get.js
    │   └── users
    │       └── create.js
    └── courses
        ├── create.js
        └── get.js