/** @format */

// TODO: use any of these libraries to manipulate photos
// https://github.com/oliver-moran/jimp
// https://github.com/EyalAr/lwip
// http://camanjs.com/

import userGet from './users/get';
import userCreate from './users/create';

import circleGet from './circles/get';
import circleCreate from './circles/create';

import postGet from './posts/get';
import postUserCreate from './posts/users/create';
import postCircleCreate from './posts/circles/create';

import courseGet from './courses/get';
import courseCreate from './courses/create';

const routes = [
  /* -------------------------------------------------------------
                              For Users
     ------------------------------------------------------------- */

  {
    method: 'POST',
    path: '/users/{userId}/photos',
    config: userCreate,
  },

  {
    method: 'GET',
    path: '/users/{userId}/photos/{photoName}',
    config: userGet,
  },

  /* -------------------------------------------------------------
                             For Circles
     ------------------------------------------------------------- */

  {
    method: 'POST',
    path: '/circles/{circleId}/photos',
    config: circleCreate,
  },

  {
    method: 'GET',
    path: '/circles/{circleId}/photos/{photoName}',
    config: circleGet,
  },

  /* --------------------------------------------------------------
                             For Posts
     -------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/posts/{postId}/photos/{photoName}',
    config: postGet,
  },

  {
    method: 'POST',
    path: '/users/{userId}/posts/{postId}/photos',
    config: postUserCreate,
  },

  {
    method: 'POST',
    path: '/circles/{circleId}/posts/{postId}/photos',
    config: postCircleCreate,
  },

  /* -----------------------------------------------------------------
                             For Courses
     ------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/courses/{courseId}/photos/{photoName}',
    config: courseGet,
  },

  {
    method: 'POST',
    path: '/users/{userId}/courses/{courseId}/photos',
    config: courseCreate,
  },
];

if (process.env.NODE_ENV === 'development') {
  /* ---------------------------------------------------------------
                             For Users
  ------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/photos/{photoName}',
    config: {
      tags: ['api', 'users', 'photos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  /* --------------------------------------------------------------
                             For Circles
     ------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/circles/{circleId}/photos/{photoName}',
    config: {
      tags: ['api', 'circles', 'photos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  /* --------------------------------------------------------------
                             For Posts
     ------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/posts/{postId}/photos/{photoName}',
    config: {
      tags: ['api', 'users', 'posts', 'photos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'DELETE',
    path: '/circles/{circleId}/posts/{postId}/photos/{photoName}',
    config: {
      tags: ['api', 'circles', 'posts', 'photos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  /* -----------------------------------------------------------------
                             For Courses
     ------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/courses/{courseId}/photos/{photoName}',
    config: {
      tags: ['api', 'users', 'courses', 'photos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });
}

export default routes;
