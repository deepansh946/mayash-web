/** @format */

import Joi from 'joi';

import { create } from '../../mayash-database/datastore/photos/users';

import { Headers, Id, Photo } from '../../mayash-common/schema';

import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

const validate = {
  headers: Headers,
  params: Joi.object({
    userId: Id.required(),
  }),
  payload: Joi.object({
    photo: Photo,
  }),
};

async function handler(request, reply) {
  try {
    const { userId } = request.params;
    const { photo } = request.payload;

    const { filename, headers } = photo.hapi;
    const filenameArray = filename.split('.');

    const fileType = filenameArray[filenameArray.length - 1];
    const contentType = headers['content-type'];

    const { statusCode, error, payload } = await create({
      userId,
      type: fileType,
    });

    if (statusCode >= 300) {
      reply({ statusCode, error });
      return;
    }

    const { photoId } = payload;

    const newPhoto = storage.file(
      `${ROOT}photos/elements/${userId}/${photoId}.${fileType}`,
    );

    photo
      .pipe(
        newPhoto.createWriteStream({
          // @todo: work on privacy of these files.
          // private: true,
          public: true,
          metadata: {
            contentType,
            // max-age is in seconds.
            cacheControl: 'public, max-age=2592000',
          },
        }),
      )
      .on('error', (err) => {
        console.error(err);

        reply({
          statusCode: 500,
          error: 'Storage Server Error',
        });
      })
      .on('finish', () =>
        reply({
          statusCode: 201,
          message: 'Photo upload success.',
          payload: {
            userId,
            photoId,
            photoName: `${photoId}.${fileType}`,
            photoUrl: `/api/users/${userId}/photos/${photoId}.${fileType}`,
          },
        }),
      );
  } catch (error) {
    console.error(error);

    reply({
      statusCode: 500,
      error: 'Server Error.',
    });
  }
}

export default {
  tags: ['api', 'users', 'photos'],
  description: 'Upload an image to the database',
  notes: 'If successful, it returns a success code with message and',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  payload: {
    maxBytes: 307200, // 300 Kb is the size limit.
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  validate,
  handler,
};
