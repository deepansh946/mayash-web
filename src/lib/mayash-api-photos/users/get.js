/** @format */

import Joi from 'joi';

import { /* Headers, */ Id, PhotoName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;
/** @format */

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'photos', 'users'],
  description: 'Retrieve an image from the database',
  notes:
    'If successful, it returns a success code with message and' + ' the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    // Header not Required here.
    // headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      photoName: PhotoName.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, photoName } = request.params;

      const file = storage.file(
        `${ROOT}photos/elements/${userId}/${photoName}`,
      );

      file
        .createReadStream()
        .on('data', () => {})
        .on('response', (response) => reply(response));
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
