/** @format */

import Joi from 'joi';

import { Id, PhotoName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;
/** @format */

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'courses', 'photos'],
  description: 'Retrieve an image from the database',
  notes:
    'If successful, it returns a success code with message and' + ' the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    params: Joi.object({
      courseId: Id.required(),
      photoName: PhotoName.required(),
    }),
  },
  handler(request, reply) {
    const { courseId, photoName } = request.params;

    const file = storage.file(`${ROOT}photos/courses/${courseId}/${photoName}`);

    file
      .createReadStream()
      .on('data', () => {})
      .on('response', (response) => reply(response));
  },
};
