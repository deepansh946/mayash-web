/** @format */

import Joi from 'joi';

import { create } from '../../mayash-database/datastore/photos/circles';
import { Id, Photo, Headers } from '../../mayash-common/schema';

import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

/** @format */

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

async function handler(request, reply) {
  try {
    const { circleId } = request.params;
    const { photo } = request.payload;

    const { filename, headers } = photo.hapi;
    const filenameArray = filename.split('.');

    const fileType = filenameArray[filenameArray.length - 1];
    const contentType = headers['content-type'];

    const { statusCode, error, payload } = await create({
      circleId,
      type: fileType,
    });

    if (statusCode >= 300) {
      reply({ statusCode, error });
      return;
    }

    const { photoId } = payload;

    const newPhoto = storage.file(
      `${ROOT}photos/elements/${circleId}/${photoId}.${fileType}`,
    );

    photo
      .pipe(
        newPhoto.createWriteStream({
          public: true,
          metadata: {
            contentType,
            // max-age is in seconds.
            cacheControl: 'public, max-age=2592000',
          },
        }),
      )
      .on('error', (err) => {
        console.error(err);

        return reply({
          statusCode: 500,
          error: 'Storage Server Error',
        });
      })
      .on('finish', () =>
        reply({
          statusCode: 201,
          message: 'Photo upload success.',
          payload: {
            circleId,
            photoId,
            photoName: `${photoId}.${fileType}`,
            photoUrl: `/api/circles/${circleId}/photos/${photoId}.${fileType}`,
          },
        }),
      );
  } catch (error) {
    console.error(error);

    reply({
      statusCode: 500,
      error: 'Server Error',
    });
  }
}

export default {
  tags: ['api', 'circles', 'photos'],
  description: 'Upload an image to the database',
  notes: 'If successful, it returns a success code with message and',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'circleAdmin'],
  },
  payload: {
    maxBytes: 307200, // 300 Kb is the size limit.
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
    }),
    payload: Joi.object({
      photo: Photo,
    }),
  },
  handler,
};
