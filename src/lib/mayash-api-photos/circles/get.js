/** @format */

import Joi from 'joi';

import { Id, PhotoName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;
/** @format */

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'photos', 'circles'],
  description: 'Retrieve an image from the database',
  notes:
    'If successful, it returns a success code with message and' + ' the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    params: Joi.object({
      circleId: Id.required(),
      photoName: PhotoName.required(),
    }),
  },
  handler: (request, reply) => {
    const { circleId, photoName } = request.params;

    const file = storage.file(
      `${ROOT}photos/elements/${circleId}/${photoName}`,
    );

    file
      .createReadStream()
      .on('data', () => {})
      .on('response', (response) => reply(response));
  },
};
