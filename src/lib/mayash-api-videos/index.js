/** @format */

import userGet from './users/get';
import userCreate from './users/create';
// import userDelete from './users/delete';

import circleGet from './circles/get';
import circleCreate from './circles/create';

import postGet from './posts/get';
import postUserCreate from './posts/users/create';
import postCircleCreate from './posts/circles/create';

import courseGet from './courses/get';
import courseCreate from './courses/create';

const routes = [
  /* -----------------------------------------------------------------
                            For Users
     ------------------------------------------------------------------ */

  {
    method: 'GET',
    path: '/users/{userId}/videos/{videoName}',
    config: userGet,
  },

  {
    method: 'POST',
    path: '/users/{userId}/videos',
    config: userCreate,
  },

  /* ----------------------------------------------------------------------
                            For Circles
  ---------------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/circles/{circleId}/videos/{videoName}',
    config: circleGet,
  },

  {
    method: 'POST',
    path: '/circles/{circleId}/videos',
    config: circleCreate,
  },

  /* ----------------------------------------------------------------------
                           For Posts
     ---------------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/posts/{postId}/videos/{videoName}',
    config: postGet,
  },

  // if user is creating and uploading videos for post.
  {
    method: 'POST',
    path: '/users/{userId}/posts/{postId}/videos',
    config: postUserCreate,
  },

  // if video is being uploaded by circle's member admin to create post.
  {
    method: 'POST',
    path: '/circles/{circleId}/posts/{postId}/videos',
    config: postCircleCreate,
  },

  /* ----------------------------------------------------------------------
                               For Courses
     ---------------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/courses/{courseId}/videos/{videoName}',
    config: courseGet,
  },

  {
    method: 'POST',
    path: '/users/{userId}/courses/{courseId}/videos',
    config: courseCreate,
  },
];

if (process.env.NODE_ENV === 'development') {
  /* -----------------------------------------------------------------
                            For Users
  ------------------------------------------------------------------ */

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/videos/{videoName}',
    config: {
      tags: ['api', 'users', 'courses', 'videos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  /* ----------------------------------------------------------------------
                              For Circles
    ---------------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/circles/{circleId}/videos/{videoName}',
    config: {
      tags: ['api', 'circles', 'videos'],
      description: 'Delete video by circleId and video file name.',
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  /* ----------------------------------------------------------------------
                             For Posts
     ---------------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/posts/{postId}/videos/{videoName}',
    config: {
      tags: ['api', 'users', 'posts', 'videos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'DELETE',
    path: '/circles/{circleId}/posts/{postId}/videos/{videoName}',
    config: {
      tags: ['api', 'circles', 'posts', 'videos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  /* ----------------------------------------------------------------------
                               For Courses
     ---------------------------------------------------------------------- */

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/courses/{courseId}/videos/{videoName}',
    config: {
      tags: ['api', 'users', 'courses', 'videos'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });
}

export default routes;
