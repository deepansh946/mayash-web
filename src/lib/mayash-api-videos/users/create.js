/** @format */

import Joi from 'joi';

import { create } from '../../mayash-database/datastore/videos/users';

import { Headers, Id, Video } from '../../mayash-common/schema';

import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

const validate = {
  headers: Headers,
  params: Joi.object({
    userId: Id.required(),
  }),
  payload: Joi.object({
    video: Video,
  }),
};

async function handler(request, reply) {
  try {
    const { userId } = request.params;
    const { video } = request.payload;

    const { filename, headers } = video.hapi;
    const filenameArray = filename.split('.');

    const fileType = filenameArray[filenameArray.length - 1];
    const contentType = headers['content-type'];

    const { statusCode, error, payload } = await create({
      userId,
      type: fileType,
    });

    if (statusCode >= 300) {
      reply({ statusCode, error });
      return;
    }

    const { videoId } = payload;

    const newvideo = storage.file(
      `${ROOT}videos/elements/${userId}/${videoId}.${fileType}`,
    );

    video
      .pipe(
        newvideo.createWriteStream({
          metadata: { contentType },
        }),
      )
      .on('error', (err) => {
        console.error(err);

        reply({
          statusCode: 500,
          error: 'Storage Server Error',
        });
      })
      .on('finish', () =>
        reply({
          statusCode: 201,
          message: 'video upload success.',
          payload: {
            userId,
            videoId,
            videoName: `${videoId}.${fileType}`,
            videoUrl: `/api/users/${userId}/videos/${videoId}.${fileType}`,
          },
        }),
      );
  } catch (error) {
    console.error(error);

    reply({
      statusCode: 500,
      error: 'Server Error.',
    });
  }
}

export default {
  tags: ['api', 'users', 'videos'],
  description: 'Upload an image to the database',
  notes: 'If successful, it returns a success code with message and',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  payload: {
    maxBytes: 307200, // 300 Kb is the size limit.
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  validate,
  handler,
};
