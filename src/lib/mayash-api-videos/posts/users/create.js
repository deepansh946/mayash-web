/** @format */

import Joi from 'joi';

import { Headers, Id, PostId, Video } from '../../../mayash-common/schema';
import { create } from '../../../mayash-database/datastore/videos/posts';
import storage from '../../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

const validate = {
  headers: Headers,
  params: Joi.object({
    userId: Id.required(),
    postId: PostId.required(),
  }),
  payload: Joi.object({
    video: Video,
  }),
};

async function handler(request, reply) {
  try {
    const { userId: id, postId } = request.params;
    const { video } = request.payload;

    const { filename, headers } = video.hapi;
    const filenameArray = filename.split('.');

    const fileType = filenameArray[filenameArray.length - 1];
    const contentType = headers['content-type'];

    const { statusCode, error, payload } = await create({
      id,
      postId,
      type: fileType,
    });

    if (statusCode >= 300) {
      reply({ statusCode, error });
      return;
    }

    const { videoId } = payload;

    const newVideo = storage.file(
      `${ROOT}videos/posts/${postId}/${videoId}.${fileType}`,
    );

    video
      .pipe(
        newVideo.createWriteStream({
          metadata: { contentType },
        }),
      )
      .on('error', (err) => {
        console.error(err);

        reply({
          statusCode: 500,
          error: 'Storage Server Error',
        });
      })
      .on('finish', () =>
        reply({
          statusCode: 201,
          message: 'Video upload success.',
          payload: {
            id,
            videoId,
            videoName: `${videoId}.${fileType}`,
            videoUrl: `/api/posts/${postId}/videos/${videoId}.${fileType}`,
          },
        }),
      );
  } catch (error) {
    console.error(error);

    reply({
      statusCode: 500,
      error: 'Server Error.',
    });
  }
}

export default {
  tags: ['api', 'videos', 'posts'],
  description: 'Upload an image to the database',
  notes: 'If successful, it returns a success code with message and',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  payload: {
    maxBytes: 307200, // 300 Kb is the size limit.
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  validate,
  handler,
};
