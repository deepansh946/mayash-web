/** @format */

import Joi from 'joi';

import { Id, VideoName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'courses', 'videos'],
  description: 'Retrieve an image from the database',
  notes:
    'If successful, it returns a success code with message and' + ' the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    params: Joi.object({
      courseId: Id.required(),
      videoName: VideoName.required(),
    }),
  },
  handler(request, reply) {
    const { courseId, videoName } = request.params;

    const file = storage.file(`${ROOT}videos/courses/${courseId}/${videoName}`);

    file
      .createReadStream()
      .on('data', () => {})
      .on('response', (response) => reply(response));
  },
};
