/**
 * This API route will display list of all courses in which
 * user is enrolled
 *
 * @format
 */

import Joi from 'joi';

import { getAllByUserId } from '../../mayash-database/datastore/courseEnroll';
import { Headers, Id } from '../../mayash-common/schema';

export default {
  tags: ['api', 'courses', 'courseEnroll'],
  description: 'Display all the courses in which a user is enrolled',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId } = request.params;

      const res = await getAllByUserId({ userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
