/**
 * This API route will display list of all users enrolled in a course
 *
 * @format
 */

import Joi from 'joi';

import { getAllByCourseId } from '../../mayash-database/datastore/courseEnroll';
import { Headers, Id } from '../../mayash-common/schema';

export default {
  tags: ['api', 'courses', 'courseEnroll'],
  description: 'Display all the users in a course',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      courseId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;

      const res = await getAllByCourseId({ courseId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
