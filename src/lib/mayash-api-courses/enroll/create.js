/**
 * This API route will enroll a user to a course,
 *
 * @format
 */

import Joi from 'joi';

import { create } from '../../mayash-database/datastore/courseEnroll';
import { Headers, Id } from '../../mayash-common/schema';

export default {
  tags: ['api', 'courses', 'courseEnroll'],
  description: 'Enroll a user to course',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      courseId: Id.required(),
      userId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, userId } = request.params;

      const res = await create({ courseId, userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
