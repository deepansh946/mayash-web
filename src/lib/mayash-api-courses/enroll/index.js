import create from './create';
import remove from './remove';
import getAllByCourseId from './getAllByCourseId';
import getAllByUserId from './getAllByUserId';

const routes = [];

routes.push({
  method: 'POST',
  path: '/users/{userId}/courses/{courseId}/enroll',
  config: create,
});

routes.push({
  method: 'DELETE',
  path: '/users/{userId}/courses/{courseId}/enroll',
  config: remove,
});

routes.push({
  method: 'GET',
  path: '/courses/{courseId}/enroll',
  config: getAllByCourseId,
});

routes.push({
  method: 'GET',
  path: '/users/{userId}/courses/enroll',
  config: getAllByUserId,
});

export default routes;
