/** @format */

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { getCourses } from '../../mayash-database/datastore/courses';

export default {
  tags: ['api', 'circles', 'courses'],
  description: 'Display all the courses of a circle',
  notes:
    'If successful, it returns a success code with message' +
    ' and display all the courses.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'circleAdmin'],
  },
  validate: {
    params: Joi.object({
      userId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;

      const res = await getCourses({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
