/** @format */

// This API will create course of a circle

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { createCourse } from '../../mayash-database/datastore/courses';

export default {
  tags: ['api', 'circles', 'courses'],
  description: 'Create a courses of a circle',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'circleAdmin'],
  },
  validate: {
    payload: Joi.object({
      title: Joi.string().required(),
      courseId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId: id, title } = request.payload;

      const res = await createCourse({ id, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
