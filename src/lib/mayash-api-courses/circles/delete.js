/** @format */

// This API will delete course of a circle

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { deleteCourse } from '../../mayash-database/datastore/courses';

export default {
  tags: ['api', 'circles', 'courses'],
  description: 'Delete a courses of a circle',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'circleAdmin'],
  },
  validate: {
    params: Joi.object({
      courseId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;

      const res = await deleteCourse({ courseId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
