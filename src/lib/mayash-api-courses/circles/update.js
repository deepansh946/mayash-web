/** @format */

// This API will update the course of a circle

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { updateCourse } from '../../mayash-database/datastore/courses';

export default {
  tags: ['api', 'circles', 'courses'],
  description: 'Update the course of a circle',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'circleAdmin'],
  },
  validate: {
    params: Joi.object({
      courseId: Id.required(),
    }),
    payload: Joi.object({
      title: Joi.string().required(),
      description: Joi.string().required(),
      syllabus: Joi.object().required(),
      courseModules: [
        Joi.object({
          moduleId: Id.required(),
        }),
      ],
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;

      const { title, description, syllabus, courseModules } = request.payload;

      const res = await updateCourse({
        courseId,
        title,
        description,
        syllabus,
        courseModules,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
