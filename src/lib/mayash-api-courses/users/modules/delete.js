/** @format */

import Joi from 'joi';

import { Headers, Id, CourseId, ModuleId } from '../../../mayash-common/schema';
import { deleteModule } from '../../../mayash-database/datastore/courseModules';

export default {
  tags: ['api', 'users', 'courses'],
  description: "Delete course's module of a user",
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
      moduleId: ModuleId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, moduleId } = request.params;

      const res = await deleteModule({ courseId, moduleId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
