/** @format */

import Joi from 'joi';

import {
  Headers,
  Id,
  CourseId,
  ModuleId,
  Title,
  Data,
} from '../../../mayash-common/schema';
import { updateModule } from '../../../mayash-database/datastore/courseModules';

const update = {
  tags: ['api', 'users', 'courses'],
  description: "Update a course's module internal info of a user",
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
      moduleId: ModuleId.required(),
    }),
    payload: Joi.object({
      title: Title,
      data: Data,
    })
      .min(1)
      .max(2),
  },
  async handler(request, reply) {
    try {
      const { courseId, moduleId } = request.params;
      const { title, data } = request.payload;

      const res = await updateModule({ courseId, moduleId, title, data });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default update;
