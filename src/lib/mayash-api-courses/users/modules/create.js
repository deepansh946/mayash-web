/** @format */

import Joi from 'joi';

import { Headers, Id, CourseId, Title } from '../../../mayash-common/schema';
import { createModule } from '../../../mayash-database/datastore/courseModules';

const create = {
  tags: ['api', 'users', 'courses'],
  description: "Create a course's module by a user",
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
    }),
    payload: Joi.object({
      title: Title.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;
      const { title } = request.payload;

      const res = await createModule({ courseId, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default create;
