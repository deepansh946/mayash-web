/** @format */

import Joi from 'joi';

import { Headers, Id, CourseId } from '../../mayash-common/schema';
import { deleteCourse } from '../../mayash-database/datastore/courses';

const courseDelete = {
  tags: ['api', 'users', 'courses'],
  description: 'Delete a course of a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id, courseId } = request.params;

      const res = await deleteCourse({ id, courseId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default courseDelete;
