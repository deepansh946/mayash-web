/** @format */

import Joi from 'joi';

import {
  Headers,
  Id,
  CourseId,
  Title, // Level, Standard,
  Description,
  Data,
  CourseModules,
} from '../../mayash-common/schema';
import { updateCourse } from '../../mayash-database/datastore/courses';

const update = {
  tags: ['api', 'users', 'courses'],
  description: 'Update the course of a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
    }),
    payload: Joi.object({
      title: Title,
      // level: Level,
      // standard: Standard,
      description: Description,
      syllabus: Data,
      courseModules: CourseModules,
      // this should be cover photo for course.
      cover: Joi.string(),
    })
      .min(1)
      .max(4),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;
      const {
        title,
        description,
        syllabus,
        courseModules,
        cover,
      } = request.payload;

      const res = await updateCourse({
        courseId,
        title,
        description,
        syllabus,
        courseModules,
        cover,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default update;
