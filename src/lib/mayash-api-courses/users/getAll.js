/** @format */

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { getCourses } from '../../mayash-database/datastore/courses';

const getAll = {
  tags: ['api', 'users', 'courses'],
  description: 'Display all the courses of a user',
  notes:
    'If successful, it returns a success code with message' +
    ' and display all the courses.',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    params: Joi.object({
      userId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;

      const res = await getCourses({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default getAll;
