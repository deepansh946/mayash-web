/**
 * This API will delete the question of a user
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../../../mayash-common/schema';
import { deleteAnswer } from '../../../../mayash-database/datastore/courses/discussion/answers';

export default {
  tags: ['api', 'courses'],
  description: 'Delete a new answer for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: Id.required(),
      questionId: Id.required(),
      answerId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, questionId, answerId } = request.params;
      const res = await deleteAnswer({ courseId, questionId, answerId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
