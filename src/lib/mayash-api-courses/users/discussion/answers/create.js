/**
 * This API will create the answer
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../../../mayash-common/schema';
import { create } from '../../../../mayash-database/datastore/courses/discussion/answers';

export default {
  tags: ['api', 'courses'],
  description: 'Create a new answer for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: Id.required(),
      questionId: Id.required(),
    }),
    payload: Joi.object({
      data: Joi.object().required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, courseId, questionId } = request.params;
      const { data } = request.payload;

      const res = await create({ courseId, questionId, userId, data });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
