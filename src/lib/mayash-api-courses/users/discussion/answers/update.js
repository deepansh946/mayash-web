/**
 * This API will update the answer of a question of user
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../../../mayash-common/schema';
import { update } from '../../../../mayash-database/datastore/courses/discussion/answers';

export default {
  tags: ['api', 'courses'],
  description: 'update a answer for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: Id.required(),
      questionId: Id.required(),
      answerId: Id.required(),
    }),
    payload: Joi.object({
      title: Joi.string().required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, questionId, answerId } = request.params;
      const { title } = request.payload;

      const res = await update({ courseId, questionId, answerId, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
