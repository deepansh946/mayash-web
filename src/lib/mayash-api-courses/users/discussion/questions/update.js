/**
 * This API will update the question of a user
 *
 * @format
 */

import Joi from 'joi';

import {
  Headers,
  Id,
  CourseId,
  Title,
  Data,
} from '../../../../mayash-common/schema';
import { update } from '../../../../mayash-database/datastore/courses/discussion/questions';

export default {
  tags: ['api', 'courses', 'discussion'],
  description: 'update a question for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
      questionId: Id.required(),
    }),
    payload: Joi.object({
      title: Title,
      data: Data,
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, courseId, questionId } = request.params;
      const { title, data } = request.payload;

      const res = await update({ userId, courseId, questionId, title, data });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
