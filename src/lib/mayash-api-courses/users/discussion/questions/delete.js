/**
 * This API will delete the question of a user
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id, CourseId } from '../../../../mayash-common/schema';
import { deleteQuestion } from '../../../../mayash-database/datastore/courses/discussion/questions';

export default {
  tags: ['api', 'courses', 'discussion'],
  description: 'Delete a new question for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
      questionId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, courseId, questionId } = request.params;

      const res = await deleteQuestion({ userId, courseId, questionId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
