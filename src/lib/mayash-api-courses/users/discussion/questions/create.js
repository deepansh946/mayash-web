/**
 * This API will create the question
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id, CourseId, Title } from '../../../../mayash-common/schema';
import { create } from '../../../../mayash-database/datastore/courses/discussion/questions';

export default {
  tags: ['api', 'courses'],
  description: 'Create a new question for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      courseId: CourseId.required(),
    }),
    payload: Joi.object({
      title: Title.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, courseId } = request.params;
      const { title } = request.payload;

      const res = await create({ courseId, userId, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
