/** @format */

import Joi from 'joi';

import { Headers, Id, Title } from '../../mayash-common/schema';
import { createCourse } from '../../mayash-database/datastore/courses';

export const create = {
  tags: ['api', 'users', 'courses'],
  description: 'Create a course of a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }),
    payload: Joi.object({
      title: Title.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;
      const { title } = request.payload;

      const res = await createCourse({ id, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default create;
