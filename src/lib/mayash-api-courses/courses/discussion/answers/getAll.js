/**
 * This API will getAll the answer of a user
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../../../mayash-common/schema';
import { getAll } from '../../../../mayash-database/datastore/courses/discussion/answers';

export default {
  tags: ['api', 'courses'],
  description: 'Get All answer of a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      courseId: Id.required(),
      questionId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, questionId } = request.params;
      const res = await getAll({ courseId, questionId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
