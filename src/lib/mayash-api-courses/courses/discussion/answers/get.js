/**
 * This will display the answer of the question
 *
 * @format
 */

import Joi from 'joi';

import { get } from '../../../../mayash-database/datastore/courses/discussion/answers';

import { Id } from '../../../../mayash-common/schema';

export default {
  tags: ['api', 'courses', 'course-discussion'],
  description: "Display a course's answer of a question to a visitor",
  notes:
    'If successful, it returns a success code with message' +
    ' and the course question.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string().required(),
    }).unknown(),
    params: Joi.object({
      courseId: Id.required(),
      questionId: Id.required(),
      answerId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, questionId, answerId } = request.params;

      const res = await get({ courseId, questionId, answerId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
