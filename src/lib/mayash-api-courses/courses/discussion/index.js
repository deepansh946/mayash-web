/** @format */

// Routes of Questions And Answers

import getQuestion from '../discussion/questions/get';
import getAllQuestion from '../discussion/questions/getAll';

import getAnswer from '../discussion/answers/get';
import getAllAnswer from '../discussion/answers/getAll';

import createQuestion from '../../users/discussion/questions/create';
import deleteQuestion from '../../users/discussion/questions/delete';
import updateQuestion from '../../users/discussion/questions/update';

import createAnswer from '../../users/discussion/answers/create';
import deleteAnswer from '../../users/discussion/answers/delete';
import updateAnswer from '../../users/discussion/answers/update';

const routes = [];

routes.push({
  method: 'GET',
  path: '/courses/{courseId}/discussion',
  config: getAllQuestion,
});

routes.push({
  method: 'GET',
  path: '/courses/{courseId}/discussion/{questionId}',
  config: getQuestion,
});

routes.push({
  method: 'POST',
  path: '/users/{userId}/courses/{courseId}/discussion',
  config: createQuestion,
});

routes.push({
  method: 'PUT',
  path: '/users/{userId}/courses/{courseId}/discussion/{questionId}',
  config: updateQuestion,
});

routes.push({
  method: 'DELETE',
  path: '/users/{userId}/courses/{courseId}/discussion/{questionId}',
  config: deleteQuestion,
});

/** ************************************************************************
                              Course Discussion Answers
    ************************************************************************ */

routes.push({
  method: 'GET',
  path: '/courses/{courseId}/discussion/{questionId}/answers',
  config: getAllAnswer,
});

routes.push({
  method: 'GET',
  path: '/courses/{courseId}/discussion/{questionId}/answers/{answerId}',
  config: getAnswer,
});

routes.push({
  method: 'POST',
  path: '/users/{userId}/courses/{courseId}/discussion/{questionId}/answers',
  config: createAnswer,
});

routes.push({
  method: 'PUT',
  path:
    '/users/{userId}/courses/{courseId}/discussion/{questionId}/answers/{answerId}',
  config: updateAnswer,
});

routes.push({
  method: 'DELETE',
  path:
    '/users/{userId}/courses/{courseId}/discussion/{questionId}/answers/{answerId}',
  config: deleteAnswer,
});

export default routes;
