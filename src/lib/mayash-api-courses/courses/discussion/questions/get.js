/**
 *
 * This will display the questions from a particular question
 * of a course with the help of course id and question id
 *
 * @format
 */

import Joi from 'joi';

import { Headers, CourseId, Id } from '../../../../mayash-common/schema';

import { get } from '../../../../mayash-database/datastore/courses/discussion/questions';

export default {
  tags: ['api', 'courses'],
  description: "Display a course's question of a user to a visitor",
  notes: `If successful, it returns a success code with 
    message and the course question.`,
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      courseId: CourseId.required(),
      questionId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, questionId } = request.params;

      const res = await get({ courseId, questionId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
