/**
 * This API will getAll the question of a user
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../../../mayash-common/schema';
import { getAll } from '../../../../mayash-database/datastore/courses/discussion/questions';

export default {
  tags: ['api', 'courses'],
  description: 'Get All question of a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      courseId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;
      const res = await getAll({ courseId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
