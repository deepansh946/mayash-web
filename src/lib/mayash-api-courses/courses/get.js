/** @format */

import Joi from 'joi';

import { CourseId } from '../../mayash-common/schema';
import { getCourse } from '../../mayash-database/datastore/courses';

/*
* This will display a single course
* with the help of course id
*/
const get = {
  tags: ['api', 'courses'],
  description: 'Display a course of a user to a visitor',
  notes:
    'If successful, it returns a success code with message' +
    ' and the course.',
  auth: {
    mode: 'required',
    strategies: ['visitor', 'course'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    params: Joi.object({
      courseId: CourseId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;

      const res = await getCourse({ courseId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default get;
