/** @format */

import Joi from 'joi';

import { Headers, CourseId } from '../../../mayash-common/schema';
import { getModules } from '../../../mayash-database/datastore/courseModules';

const getAll = {
  tags: ['api', 'courses'],
  description: 'Display all the course modules of a user',
  notes:
    'If successful, it returns a success code with message' +
    ' and the list of all course modules.',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      courseId: CourseId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId } = request.params;

      const res = await getModules({ courseId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default getAll;
