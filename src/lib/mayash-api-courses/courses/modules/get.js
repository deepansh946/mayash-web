/** @format */

import Joi from 'joi';

import { CourseId, ModuleId } from '../../../mayash-common/schema';
import { getModule } from '../../../mayash-database/datastore/courseModules';

/*
* This will display the module of a particular course
* with the help of module id and course id
*/
const get = {
  tags: ['api', 'courses'],
  description: 'Display course module of a user to a visitor',
  notes:
    'If successful, it returns a success code with message' +
    ' and the course module.',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    params: Joi.object({
      courseId: CourseId.required(),
      moduleId: ModuleId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { courseId, moduleId } = request.params;

      const res = getModule({ courseId, moduleId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default get;
