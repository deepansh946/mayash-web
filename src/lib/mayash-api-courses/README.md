# Mayash API Courses

This library is sub part of **Mayash API** plugin.

It contains all the API routes for courses.

## Folder Structure

    .
    ├── README.md
    ├─┬ circles
    │ ├── create.js
    │ ├── delete.js
    │ ├── getAll.js
    │ └── update.js
    ├─┬ courses
    │ ├─┬ discussion
    │ │ ├─┬ answers
    │ │ │ ├── get.js
    │ │ │ ├── create.js
    │ │ │ ├── delete.js
    │ │ │ ├── getAll.js
    │ │ │ └── update.js
    │ │ ├── index.js
    │ │ ├─┬ questions
    │ │ │ ├── get.js
    │ │ │ ├── create.js
    │ │ │ ├── delete.js
    │ │ │ ├── getAll.js
    │ │ │ └── update.js
    │ │ ├─┬ answers
    │ │ │ ├── get.js
    │ │ │ ├── create.js
    │ │ │ ├── delete.js
    │ │ │ ├── getAll.js
    │ │ │ └── update.js
    │ │ └─┬ questions
    │ │   ├── get.js
    │ │   ├── create.js
    │ │   ├── delete.js
    │ │   ├── getAll.js
    │ │   └── update.js
    │ ├── get.js
    │ └─┬ modules
    │   ├── get.js
    │   ├── getAll.js
    │   ├── create.js
    │   ├── delete.js
    │   └── update.js
    ├── index.js
    └─┬ users
      ├── create.js
      ├── delete.js
      ├─┬ discussion
      │ ├─┬ answers
      │ │ ├── get.js
      │ │ ├── create.js
      │ │ ├── delete.js
      │ │ ├── getAll.js
      │ │ └── update.js
      │ ├── index.js
      │ ├─┬ questions
      │ │ ├── get.js
      │ │ ├── create.js
      │ │ ├── delete.js
      │ │ ├── getAll.js
      │ │ └── update.js
      │ ├─┬ answers
      │ │ ├── get.js
      │ │ ├── create.js
      │ │ ├── delete.js
      │ │ ├── getAll.js
      │ │ └── update.js
      │ └─┬ questions
      │   ├── get.js
      │   ├── create.js
      │   ├── delete.js
      │   ├── getAll.js
      │   └── update.js
      ├── getAll.js
      ├─┬ modules
      │ ├── get.js
      │ ├── getAll.js
      │ ├── create.js
      │ ├── delete.js
      │ └── update.js
      └── update.js
