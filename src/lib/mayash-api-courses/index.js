/** @format */

import courseGet from './courses/get';
import courseModulesGetAll from './courses/modules/getAll';
import courseModuleGet from './courses/modules/get';

import userCoursesGetAll from './users/getAll';
import userCourseCreate from './users/create';
import userCourseUpdate from './users/update';
import userCourseDelete from './users/delete';

import userCourseModuleCreate from './users/modules/create';
import userCourseModuleUpdate from './users/modules/update';
import userCourseModuleDelete from './users/modules/delete';

import circleGetAll from './circles/getAll';
import circleCreate from './circles/create';
import circleUpdate from './circles/update';
import circleDelete from './circles/delete';

import discussionRoutes from './courses/discussion';
import enroll from './enroll';

const { NODE_ENV } = process.env;

const routes = [
  /* --------------------------------------------------
                    COURSES
     -------------------------------------------------- */
  {
    method: 'GET',
    path: '/courses/{courseId}',
    config: courseGet,
  },
  {
    method: 'GET',
    path: '/courses/{courseId}/modules',
    config: courseModulesGetAll,
  },
  {
    method: 'GET',
    path: '/courses/{courseId}/modules/{moduleId}',
    config: courseModuleGet,
  },

  /* --------------------------------------------------
                       USERS
    -------------------------------------------------- */

  {
    method: 'GET',
    path: '/users/{userId}/courses',
    config: userCoursesGetAll,
  },
  {
    method: 'POST',
    path: '/users/{userId}/courses',
    config: userCourseCreate,
  },
  {
    method: 'PUT',
    path: '/users/{userId}/courses/{courseId}',
    config: userCourseUpdate,
  },
  {
    method: 'DELETE',
    path: '/users/{userId}/courses/{courseId}',
    config: userCourseDelete,
  },

  // Course Modules
  {
    method: 'POST',
    path: '/users/{userId}/courses/{courseId}/modules',
    config: userCourseModuleCreate,
  },
  {
    method: 'PUT',
    path: '/users/{userId}/courses/{courseId}/modules/{moduleId}',
    config: userCourseModuleUpdate,
  },
  {
    method: 'DELETE',
    path: '/users/{userId}/courses/{courseId}/modules/{moduleId}',
    config: userCourseModuleDelete,
  },

  /* --------------------------------------------------
                       CIRCLES
    -------------------------------------------------- */

  {
    method: 'GET',
    path: '/circles/{circleId}/courses',
    config: circleGetAll,
  },
  {
    method: 'POST',
    path: '/circles/{circleId}/courses',
    config: circleCreate,
  },
  {
    method: 'PUT',
    path: '/circles/{circleId}/courses/{courseId}',
    config: circleUpdate,
  },
  {
    method: 'DELETE',
    path: '/circles/{circleId}/courses/{courseId}',
    config: circleDelete,
  },
];

// if (NODE_ENV === 'development') {
routes.push(...discussionRoutes);
// }

routes.push(...enroll);

export default routes;
