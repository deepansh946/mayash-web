/**
 * This API route will create a new bookmark,
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../mayash-common/schema';
import { create } from '../mayash-database/datastore/bookmarks';

export default {
  tags: ['api', 'bookmark'],
  description: 'Create a new bookmark for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }),
    payload: Joi.object({
      type: Joi.string().required(),
      courseId: Id,
      postId: Id,
    }),
  },
  async handler(request, reply) {
    try {
      const { userId } = request.params;
      const { type, courseId, postId } = request.payload;

      const res = await create({
        userId,
        type,
        courseId,
        postId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
