/** @format */

import create from './create';
import getAll from './getAll';
import get from './get';
import update from './update';
import Delete from './delete';

const routes = [];

routes.push({
  method: 'POST',
  path: '/users/{userId}/bookmarks',
  config: create,
});

routes.push({
  method: 'GET',
  path: '/users/{userId}/bookmarks',
  config: getAll,
});

routes.push({
  method: 'GET',
  path: '/users/{userId}/bookmarks/{bookmarkId}',
  config: get,
});

routes.push({
  method: 'PUT',
  path: '/users/{userId}/bookmarks/{bookmarkId}',
  config: update,
});

routes.push({
  method: 'DELETE',
  path: '/users/{userId}/bookmarks/{bookmarkId}',
  config: Delete,
});

export default routes;
