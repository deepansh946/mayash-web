/**
 * This API route will get a bookmark,
 *
 * @format
 */

import Joi from 'joi';

import { getBookmark } from '../mayash-database/datastore/bookmarks';
import { Headers, Id } from '../mayash-common/schema';

export default {
  tags: ['api', 'bookmark'],
  description: 'Display a bookmark for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      bookmarkId: Id.required(),
    }).length(2),
  },
  async handler(request, reply) {
    try {
      const { userId: id, bookmarkId } = request.params;

      const res = await getBookmark({ id, bookmarkId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
