/**
 * This API route will delete a bookmark,
 *
 * @format
 */

import Joi from 'joi';

import { deleteBookmark } from '../mayash-database/datastore/bookmarks';
import { Headers, Id } from '../mayash-common/schema';

export default {
  tags: ['api', 'bookmark'],
  description: 'Delete a bookmark for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      bookmarkId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id, bookmarkId } = request.params;

      const res = await deleteBookmark({ id, bookmarkId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
