/**
 * This API route will get the list of all Bookmark
 *
 * @format
 */

import Joi from 'joi';

import { getAllBookmarks } from '../mayash-database/datastore/bookmarks';
import { Headers, Id } from '../mayash-common/schema';

export default {
  tags: ['api', 'bookmark'],
  description: 'Display the list of all bookmark for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { userId } = request.params;
      const res = await getAllBookmarks({ userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
