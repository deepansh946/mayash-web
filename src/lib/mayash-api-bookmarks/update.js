/**
 * This API route will create a update a bookmark,
 */

import Joi from 'joi';

import { updateBookmark } from '../mayash-database/datastore/bookmarks';
import { Headers, Id } from '../mayash-common/schema';

export default {
  tags: ['api', 'bookmark'],
  description: 'Update a bookmark for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      bookmarkId: Id.required(),
    }).length(2),
    payload: Joi.object({
      type: Joi.string().required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { userId, bookmarkId } = request.params;
      const { type } = request.payload;

      const res = updateBookmark({ userId, bookmarkId, type });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
