/**
 *
 * @format
 */

import { get } from '../mayash-database/datastore/courses';

const { TOKEN_KEY } = process.env;

export default {
  key: TOKEN_KEY,
  async validateFunc(decoded, request, callback) {
    const courseId = parseInt(request.params.courseId, 10);

    const { statusCode, payload } = await get({ courseId });

    if (statusCode >= 300) {
      return callback(null, true);
    }

    const { privacy, authorId } = payload;

    // This is for all the courses which were created before privacy feature
    if (typeof privacy === 'undefined') {
      return callback(null, true);
    }

    // If course is private and then only the owner of the course can view it
    // So we check the author id with the id present in token
    if (privacy === true && decoded.id === authorId) {
      return callback(null, true);
    }

    // If privacy is false then the course is public, anyone can view it
    if (privacy === false) {
      return callback(null, true);
    }

    return callback(null, false);
  },
};
