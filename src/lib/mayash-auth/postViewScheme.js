import Boom from 'boom';
import jwt from 'jsonwebtoken';

import { get } from '../mayash-database/datastore/posts';

const { TOKEN_KEY } = process.env;

/**
 * Steps which are done
 * - Get the post id from the request object
 * - Fetch the post using that id
 * - Check if the statusCode is above 300 then
 * the post doesnt exists so all are authorized
 * - Check if the privacy of the post is public then all are authorized
 * - Check if the privacy is private
 *   - Get the token from request object
 *   - Check if the token does not exist then the user is unauthorized
 *   - If the token exists
 *     - Verify the token
 *     - Get the user id from the token
 *     - Check whether the user id is matched with author id
 *     - If true then the user is authorized else not
 * - If all above cases fails reply unauthorized
 *
 */
const postViewScheme = (server, options) => ({
  async authenticate(request, reply) {
    const postId = parseInt(request.params.postId, 10);

    const { statusCode, payload } = await get({ postId });
    const { privacy } = payload;

    if (statusCode >= 300) {
      return reply.continue({ credentials: {} });
    }

    if (typeof privacy === 'undefined' || privacy === false) {
      return reply.continue({ credentials: {} });
    }

    if (privacy === true) {
      const token = request.state.token;

      if (!token) {
        return reply(
          Boom.unauthorized('You are not authorized to view this post'),
        );
      }

      const res = await jwt.verify(token, TOKEN_KEY);

      if (res.id === payload.authorId) {
        return reply.continue({ credentials: {} });
      }
    }

    return reply(Boom.unauthorized('You are not authorized to view this post'));
  },
});

export default postViewScheme;
