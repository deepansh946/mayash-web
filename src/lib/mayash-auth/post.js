/**
 *
 * @format
 */

import { get } from '../mayash-database/datastore/posts';

const { TOKEN_KEY } = process.env;

export default {
  key: TOKEN_KEY,
  async validateFunc(decoded, request, callback) {
    const postId = parseInt(request.params.postId, 10);

    const { statusCode, payload } = await get({ postId });

    if (statusCode >= 300) {
      return callback(null, true);
    }

    const { privacy, authorId } = payload;

    // This is for all the posts which were created before privacy feature
    if (typeof privacy === 'undefined') {
      return callback(null, true);
    }

    // If post is private and then only the owner of the post can view it
    // So we check the author id with the id present in token
    if (privacy === true && decoded.id === authorId) {
      return callback(null, true);
    }

    // If privacy is false then the post is public, anyone can view it
    if (privacy === false) {
      return callback(null, true);
    }

    return callback(null, false);
  },
};
