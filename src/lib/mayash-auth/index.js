/**
 * This file contains all the authorisation strategies
 *
 * @format
 */

import admin from './admin';
import user from './user';
import owner from './owner';
import guru from './guru';

import circleAdmin from './circleAdmin';
import circleMember from './circleMember';

import post from './post';
import postViewScheme from './postViewScheme';

import course from './course';

const { NODE_ENV } = process.env;

const register = (server, options, next) => {
  // All the auth strategies are registered here.

  // 'admin' strategy will allow only those who has admin rights.
  server.auth.strategy('admin', 'jwt', admin);

  // 'user' strategy will allow only those who has got our authorised TOKEN_KEY.
  server.auth.strategy('user', 'jwt', user);

  // 'owner' strategy will allow only those who has got our authorised
  // TOKEN_KEY and he/she should be making request.
  server.auth.strategy('owner', 'jwt', owner);

  // 'guru' auth is allow user to create courses.
  server.auth.strategy('guru', 'jwt', guru);

  // circleAdmin will check is a user is circle's admin or not.
  server.auth.strategy('circleAdmin', 'jwt', circleAdmin);

  server.auth.strategy('circleMember', 'jwt', circleMember);

  server.auth.strategy('post', 'jwt', post);

  server.auth.scheme('postViewScheme', postViewScheme);
  server.auth.strategy('postView', 'postViewScheme');

  server.auth.strategy('course', 'jwt', course);

  // Auth attributes from google
  server.auth.strategy('google', 'bell', {
    provider: 'google',
    password: 'cookie_encryption_password_should_be-secure',
    clientId:
      '484568395161-42jj62d1d3u07n3oanlvvrekm2auqn36.apps.googleusercontent.com',
    clientSecret: '_meRsMHtf3a9hT4VPKMs8PX7',
    // turn true for production as we are using HTTPS but for
    // development it should be false.
    isSecure: NODE_ENV === 'production',
    // this will force to use https in production.
    forceHttps: NODE_ENV === 'production',
  });

  next();
};

register.attributes = {
  pkg: {
    name: 'mayash-auth',
    version: '0.0.0',
    description:
      'This plugins create all the TOKEN_KEY auth ' +
      'strategy for this project.',
  },
};

export default register;
