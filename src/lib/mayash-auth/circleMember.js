/**
 * This is a strategies where circleMember is validated under validFunc
 * When ever this strategies is accessed by user , its credentials (id)
 * are accessed by decoded and circleId by params
 *
 * then id,circleId is passed to function isCircleMember
 * if statusCode is 200 then user is authenticated
 * else user will not to alllowed to access that route
 *
 * @format
 */

import { isCircleMember } from '../mayash-database/datastore/circles';

const { TOKEN_KEY } = process.env;

export default {
  key: TOKEN_KEY,
  validateFunc(decoded, request, callback) {
    const { id: memberId } = decoded;
    const { circleId } = request.params;

    isCircleMember({ circleId, memberId })
      .then(({ statusCode }) => {
        if (statusCode === 200) {
          return callback(null, true);
        }
        return callback(null, false);
      })
      .catch((error) => {
        console.error(error);

        return callback(null, false);
      });
  },
};
