## Mayash Auth

This is Hapi.js plugins. This plugins contains all the __Authentication__ parts.

**visitor**

  This auth type is for visitors, who have not signed in. So for any route which does not require signin in. this auth will be used.

**owner**

  This auth type is for Users, who have signed in.

**guru**

  This auth type is for all users who have active Classrooms feature.

**admin**

  This auth type is for user , who are admin

**circleAdmin**

  This auth is for circle , who are admin of the circle

**circleMember**

  This auth is for circle , who are member of the circle 

__Rest all will be added here.__


## Folder Structure

    .
    ├── README.md
    ├── admin.js
    ├── circleAdmin.js
    ├── circleMember.js
    ├── guru.js
    ├── index.js
    ├── owner.js
    └── user.js
