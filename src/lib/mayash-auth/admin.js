/**
 * This is a strategies where Admin is validated under validFunc
 * When ever this strategies is accessed by user , its credentials (id)
 * are accessed by decoded
 *
 * then id is passed to function getAdmin
 * if statusCode is 200 then user is authenticated
 * else user will not to alllowed to access that route
 *
 * @format
 */

import { getAdmin } from '../mayash-database/datastore/auth';

const { TOKEN_KEY } = process.env;

export default {
  key: TOKEN_KEY,
  validateFunc(decoded, request, callback) {
    const { id: userId } = decoded;

    getAdmin({ userId })
      .then(({ statusCode }) => {
        if (statusCode >= 300) {
          callback(null, false);
          return;
        }

        callback(null, true);
      })
      .catch((error) => {
        console.error(error);

        callback(null, false);
      });
  },
};
