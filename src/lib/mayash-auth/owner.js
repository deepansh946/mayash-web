/**
 * This is a strategies where user is checked whether user is signed in
 * or not
 * If the URL userId/id matched with the user's Id
 * then the user is authenticated otherwise not
 *
 * @format
 */

const { TOKEN_KEY } = process.env;

export default {
  key: TOKEN_KEY,
  validateFunc(decoded, request, callback) {
    const { id, userId } = request.params;

    if (decoded.id === parseInt(id, 10)) {
      return callback(null, true);
    }

    if (decoded.id === parseInt(userId, 10)) {
      return callback(null, true);
    }

    return callback(null, false);
  },
};
