/**
 * This is a strategies where user is validated under validFunc
 * userId is checked whether user has classroom feature enable or not
 *
 * @format
 */

const { TOKEN_KEY } = process.env;

export default {
  key: TOKEN_KEY,
  validateFunc(decoded, request, callback) {
    return callback(null, false);
  },
};
