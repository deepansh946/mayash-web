/**
 *
 * @format
 */

import Joi from 'joi';
import crypto from 'crypto';
import jwt from 'jsonwebtoken';

import { signInByUsername } from '../mayash-database/datastore/elements';

import { Username, Password } from '../mayash-common/schema';

const { TOKEN_KEY } = process.env;

/**
 *
 * This function is used to sign in using username and password
 * Also traffic check strategy is also imposed in this function
 * @param {string} username: Username of the user
 * @param {*} password: Password of the user
 *
 * username and password of the user is passed to the function signInByUserName
 * if the status code is 200 then the password of the user is hashed
 * using sha256 algorithm if the credentials match then the user is authenticated
 * and user is signed in
 */
export default {
  tags: ['api', 'auth'],
  description: 'SignIn Using Username and Password',
  notes: "If successful, it returns user's info with authentication token.",
  auth: {
    mode: 'required',
    strategies: ['trafficCheck', 'visitor'],
  },
  validate: {
    payload: Joi.object({
      username: Username.required(),
      password: Password.required(),
    }).length(2),
  },
  async handler(request, reply) {
    try {
      const { username, password } = request.payload;

      const { statusCode, error, payload } = await signInByUsername({
        username,
      });

      if (statusCode !== 200) {
        reply({ statusCode, error });
        return;
      }

      const hashedPassword = crypto
        .createHash('sha256')
        .update(password)
        .digest('hex');

      if (payload.password !== hashedPassword) {
        reply({
          statusCode: 400,
          error: 'Username password does not match.',
        });
        return;
      }

      const options = {
        expiresIn: '10 days',
      };

      jwt.sign(payload, TOKEN_KEY, options, (err, token) => {
        if (err) {
          console.error(err);

          reply({
            statusCode: 500,
            error: 'Server Error in Sign In',
          });
          return;
        }

        const { id, name, avatar, guru } = payload;

        reply({
          statusCode: 200,
          message: 'Sign In Successful.',
          payload: { id, username, name, avatar, guru, token },
        });
      });
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
