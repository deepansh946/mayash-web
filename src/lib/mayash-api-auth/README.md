## Mayash API Auth

This library is sub part of **Mayash API** plugin.

It contains all the API routes for authentications.

## Folder Structure

    .
    ├── AuthGoogle.js
    ├── README.md
    ├── SignIn.js
    ├─┬ admin
    │ ├── create.js
    │ ├── delete.js
    │ ├── get.js
    │ └── getAll.js
    └── index.js
