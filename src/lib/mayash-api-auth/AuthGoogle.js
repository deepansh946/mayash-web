/** @format */

import jwt from 'jsonwebtoken';

import { signInByGoogleAuth } from '../mayash-database/datastore/elements';

const { TOKEN_KEY } = process.env;

/**
 * This function will generate an html page with following parameters.
 * @param {string} script:
 * Script in the form of string literal should be passed here.
 * @param {string} message: Message to be displayed in page should be here.
 */
const html = (script, message) => `<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  ${script}
</head>
<body>
  ${message}
</body>
</html>
`;

/**
 * Sign In / Sign Up using Google.
 *
 * Description: we are using google to take user's credentials
 *  and add them to our database.
 * When user will enter this route, it will redirect them to google's auth
 * page,
 * if User apporves, it will return with users data.
 * if Not, it will show false.
 */
export default {
  tags: ['api', 'auth'],
  description: 'SignIn Using Google OAuth',
  notes:
    "If successful, it returns user's info with google's authentication token.",
  auth: {
    strategy: 'google',
    mode: 'try',
  },
  async handler(request, reply) {
    try {
      if (!request.auth.isAuthenticated) {
        reply.redirect('/sign-in');
        return;
      }

      const { profile } = request.auth.credentials;

      const { id: googleId, displayName, email, raw } = profile;

      const { statusCode, error, payload } = await signInByGoogleAuth({
        googleId,
        email,
        name: displayName,
        raw,
      });

      if (statusCode !== 200) {
        const script = `<script>
          setTimeout(function(){ location.href = "/sign-in"; }, 2000);
        </script>`;
        reply(html(script, error));
        return;
      }

      const options = {
        expiresIn: '10 days',
      };

      jwt.sign(payload, TOKEN_KEY, options, (err, token) => {
        if (err) {
          console.log(err);

          reply({
            statusCode: 500,
            error: 'Server Error.',
          });
        }

        const { id, username } = payload;

        const script = `<script>
  document.cookie = 'id=${id}; path=/';
  document.cookie = 'username="${username}"; path=/';
  document.cookie = 'token=${token}; path=/';
  document.cookie = 'isSignedIn=true; path=/';
  setTimeout(function() { location.href = "/"; }, 1500);
</script>`;
        const message = 'Sign In using Google is Successful.';
        reply(html(script, message));
      });
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
