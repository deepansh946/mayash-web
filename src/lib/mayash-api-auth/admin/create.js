/**
 * This API route will create a new Admin
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../mayash-common/schema';
import { createAdmin } from '../../mayash-database/datastore/auth';

export default {
  tags: ['api', 'admin'],
  description: 'Create a new admin for a user',
  notes: 'If successful, it will return a successs code with message',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    payload: Joi.object({
      userId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId } = request.payload;

      const res = await createAdmin({ userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
