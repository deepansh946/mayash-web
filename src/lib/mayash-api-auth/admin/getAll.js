/**
 * This API route will get the list of all Admin
 *
 * @format
 */

import { Headers } from '../../mayash-common/schema';
import { getAllAdmin } from '../../mayash-database/datastore/auth';

export default {
  tags: ['api', 'admin'],
  description: 'Display the list of all admin',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
  },
  async handler(request, reply) {
    try {
      const res = await getAllAdmin();

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
