/**
 * This API route will get a particular admin
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../mayash-common/schema';
import { getAdmin } from '../../mayash-database/datastore/auth';

export default {
  tags: ['api', 'admin'],
  description: 'Display a particular admin',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { userId } = request.params;

      const res = await getAdmin({ userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
