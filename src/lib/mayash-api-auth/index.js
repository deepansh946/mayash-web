/**
 * this file contain all the routes of the admin and sign-in
 *
 * @format
 */

import SignIn from './SignIn';
import AuthGoogle from './AuthGoogle';

import adminCreate from './admin/create';
import adminGetAll from './admin/getAll';
import adminGet from './admin/get';
import adminDelete from './admin/delete';

const routes = [];

routes.push({
  method: 'POST',
  path: '/signin',
  config: SignIn,
});

routes.push({
  method: ['GET', 'POST'],
  path: '/auth/google',
  config: AuthGoogle,
});

routes.push({
  method: 'POST',
  path: '/auth/admins',
  config: adminCreate,
});

routes.push({
  method: 'GET',
  path: '/auth/admins',
  config: adminGetAll,
});

routes.push({
  method: 'GET',
  path: '/auth/admins/{userId}',
  config: adminGet,
});

routes.push({
  method: 'DELETE',
  path: '/auth/admins/{userId}',
  config: adminDelete,
});

export default routes;
