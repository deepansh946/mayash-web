/**
 * This file contains all the variables for elements
 * All the authentication strategies and validation are defined here
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../mayash-common/schema';
import { getElements } from '../../mayash-database/datastore/elements';

export default {
  tags: ['api', 'elements'],
  description: 'Display an element by its ids',
  notes: [
    'If successful, it returns a success code with message',
    ' and the element details',
  ].join(' '),
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    query: Joi.object({
      ids: Joi.array()
        .items(Id)
        .required(),
    }).length(1),
  },
  handler: async (request, reply) => {
    try {
      const { ids } = request.query;

      const res = await getElements({ ids });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
