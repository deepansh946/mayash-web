/** @format */

import Joi from 'joi';

import { getElementByUsername } from '../../mayash-database/datastore/elements';
import { Headers, Username } from '../../mayash-common/schema';

export default {
  tags: ['api', 'elements'],
  description: 'Display an element by its Username',
  notes:
    'If successful, it returns a success code with message' +
    ' and the element details',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    params: Joi.object({
      username: Username,
    }).length(1),
  },
  handler: async (request, reply) => {
    try {
      const { username } = request.params;

      const res = await getElementByUsername({ username });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
