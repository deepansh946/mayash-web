/** @format */

import Joi from 'joi';

import { Headers, Username, Email } from '../../mayash-common/schema';
import {
  checkEmail,
  checkUsername,
} from '../../mayash-database/datastore/elements';

export default {
  tags: ['api', 'elements'],
  description: 'This is used to validate the username or email address',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    query: Joi.object({
      username: Username,
      email: Email,
    })
      .length(1)
      .required(), // Using only one : id or username
  },
  async handler(request, reply) {
    try {
      const { username, email } = request.query;

      // Using only the defined one i.e username or the email
      if (typeof username !== 'undefined') {
        const res = await checkUsername({ username });

        reply(res);
        return;
      }

      const res = await checkEmail({ email });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
