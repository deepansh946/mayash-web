/** @format */

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { getElementById } from '../../mayash-database/datastore/elements';

export default {
  tags: ['api', 'elements'],
  description: 'Display an element by its id',
  notes: 'If successful, it returns a arrays of object of elements details',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    params: Joi.object({
      id: Id,
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { id } = request.params;

      const res = await getElementById({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
