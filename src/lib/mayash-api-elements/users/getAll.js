/** @format */

// import Joi from 'joi';

import { Headers } from '../../mayash-common/schema';
import { getUsers } from '../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Get All User.',
  notes: 'Only for admin.',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
  },
  async handler(request, reply) {
    try {
      const res = await getUsers();

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
