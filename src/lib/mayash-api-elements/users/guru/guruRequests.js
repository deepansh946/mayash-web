/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { handleGuruRequest } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'This will handle guru requests',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
    payload: Joi.object({
      enable: Joi.boolean().required(),
      reason: Joi.string().required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;
      const { enable, reason } = request.payload;

      const res = await handleGuruRequest({ id, enable, reason });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
