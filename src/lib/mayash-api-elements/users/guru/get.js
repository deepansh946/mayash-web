/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { getGuru } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users', 'courses'],
  description: 'Check whether the user is guru or not',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'admin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      id: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { id } = request.params;

      const res = await getGuru({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
