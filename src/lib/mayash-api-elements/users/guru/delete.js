/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { deleteGuru } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users', 'courses'],
  description: 'Delete guru',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      id: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { id } = request.params;

      const res = await deleteGuru({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
