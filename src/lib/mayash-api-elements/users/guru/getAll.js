/** @format */

import Joi from 'joi';

import { Headers } from '../../../mayash-common/schema';
import {
  getAllGuruRequest,
  getAllGurus,
} from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users', 'courses'],
  description: 'Display all guru requests',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'admin'],
  },
  validate: {
    headers: Headers,
    query: Joi.object({
      type: Joi.string()
        .valid('guru', 'request')
        .required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { type } = request.query;

      if (type === 'request') {
        const res = await getAllGuruRequest();
        reply(res);
      }

      if (type === 'guru') {
        const res = await getAllGurus();
        reply(res);
      }
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
