/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../mayash-common/schema';
// import dbUser from '../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'delete the user by its id',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      reply({
        statusCode: 503,
        error: 'Service Unavailable',
      });
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
