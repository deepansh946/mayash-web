/** @format */

import Joi from 'joi';

import { Authorization, Id } from '../../mayash-common/schema';
import { getUserById } from '../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Display the user by its id',
  notes:
    'If successful, it returns a success code with message' +
    ' and the user details',
  auth: {
    mode: 'required',
    strategies: ['visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Authorization,
    }).unknown(),
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;

      const res = await getUserById({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
