/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { getUserCirclesByUserId } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Display the list of circles of which user is a member',
  notes:
    'If successful, it returns a success code with message' +
    ' and the list of circles',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id,
    }).length(1),
  },
  handler: async (request, reply) => {
    try {
      const { userId: id } = request.params;

      const res = await getUserCirclesByUserId({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
