/** @format */

import Joi from 'joi';

import {
  Headers,
  Username,
  Name,
  Email,
  Password,
} from '../../mayash-common/schema';
import { createUser } from '../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Create New User.',
  notes: 'Only for admin.',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    payload: Joi.object({
      username: Username.required(),
      name: Name.required(),
      email: Email.required(),
      password: Password.required(),
    }).length(4),
  },
  async handler(request, reply) {
    try {
      const { username, name, email, password } = request.payload;

      const res = await createUser({ username, name, email, password });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
