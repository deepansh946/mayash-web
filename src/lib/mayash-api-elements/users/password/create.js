/** @format */

import Joi from 'joi';

import { Headers, Id, Password } from '../../../mayash-common/schema';
import { createPassword } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Create a password for a user',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
    payload: Joi.object({
      password: Password.required(),
    }).length(1),
  },
  handler: async (request, reply) => {
    try {
      const { userId } = request.params;
      const { password } = request.payload;

      const res = await createPassword({ userId, password });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
