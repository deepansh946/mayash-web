/** @format */

import Joi from 'joi';
import crypto from 'crypto';

import { Headers, Id, Password } from '../../../mayash-common/schema';
import {
  updatePasswordById,
  getPassword,
} from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: "Update the user's password by its id",
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
    payload: Joi.object({
      oldPassword: Password,
      newPassword: Password.required(),
      confirmPassword: Password.required(),
    })
      .min(2)
      .max(3),
  },
  handler: async (request, reply) => {
    try {
      const { userId: id } = request.params;
      const { oldPassword, newPassword, confirmPassword } = request.payload;

      if (newPassword !== confirmPassword) {
        reply({
          statusCode: 406,
          error: 'New passwords do not match',
        });
        return;
      }

      if (oldPassword && oldPassword === newPassword) {
        reply({
          statusCode: 406,
          error: 'Old and New password should not match',
        });
        return;
      }

      const { statusCode, error, payload } = await getPassword({ id });

      if (statusCode !== 200) {
        reply({ statusCode, error });
        return;
      }

      const { password: oldPasswordDb } = payload;

      if (oldPasswordDb) {
        const oldHashedPassword = crypto
          .createHash('sha256')
          .update(oldPassword)
          .digest('hex');

        if (oldPasswordDb && oldPasswordDb !== oldHashedPassword) {
          reply({
            statusCode: 406,
            error: 'Your entered old password is incorrect',
          });
          return;
        }
      }

      const res = await updatePasswordById({
        id,
        password: newPassword,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
