/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { checkPasswordExists } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Check whether the password exist for a user',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
  },
  handler: async (request, reply) => {
    try {
      const { userId: id } = request.params;

      const res = await checkPasswordExists({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
