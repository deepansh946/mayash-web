/** @format */

import Joi from 'joi';

import { Headers, Id, Username } from '../../../mayash-common/schema';
import { updateUsernameById } from '../../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: "Update the user's username by its id",
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
    payload: Joi.object({
      username: Username.required(),
    }).length(1),
  },
  handler: async (request, reply) => {
    try {
      const { userId: id } = request.params;
      const { username } = request.payload;

      const res = await updateUsernameById({ id, username });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
