/** @format */

import Joi from 'joi';

import { Headers, Id, Name, Data } from '../../mayash-common/schema';
import { updateUserById } from '../../mayash-database/datastore/users';

export default {
  tags: ['api', 'users'],
  description: 'Update the user by its id',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }).length(1),
    payload: Joi.object({
      name: Name,
      avatar: Joi.string(),
      cover: Joi.string(),
      resume: Data,
    })
      .min(1)
      .max(3),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;
      const { name, avatar, cover, resume } = request.payload;

      const res = await updateUserById({ id, name, avatar, cover, resume });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
