/** @format */

import Joi from 'joi';

import { Id, Authorization } from '../../mayash-common/schema';
import { getCircle } from '../../mayash-database/datastore/circles';

export default {
  tags: ['api', 'circles'],
  description: 'Display the circle by a particular id',
  notes:
    'If successful, it returns a success code with message' +
    ' and display the circle.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Authorization,
    }).unknown(),
    params: Joi.object({
      circleId: Id.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;

      const res = await getCircle({ circleId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
