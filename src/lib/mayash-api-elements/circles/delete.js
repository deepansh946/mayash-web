/**
 * This API route will create a new circle,
 * this route is only for admin authorisation.
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id } from '../../mayash-common/schema';
import { deleteCircle } from '../../mayash-database/datastore/circles';

export default {
  tags: ['api', 'circles'],
  description: 'Create a circle of type edu, org, field and location.',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'admin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;

      const res = await deleteCircle({ circleId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
