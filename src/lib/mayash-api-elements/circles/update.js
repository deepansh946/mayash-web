/**
 * update circle by it's id,
 * auth should be for only admin of circle can update.
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id, Name } from '../../mayash-common/schema';
import { update } from '../../mayash-database/datastore/circles';

export default {
  tags: ['api', 'circles'],
  description: 'Update the circle by its id',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
    }).length(1),
    payload: Joi.object({
      name: Name,
    })
      .min(1)
      .max(3),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;
      const { name } = request.payload;

      const res = await update({ circleId, name });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
