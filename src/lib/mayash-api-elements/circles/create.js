/**
 * This API route will create a new circle,
 * this route is only for admin authorisation.
 *
 * @format
 */

import Joi from 'joi';

import {
  Headers,
  Id,
  Username,
  Name,
  CircleType,
} from '../../mayash-common/schema';
import {
  createEdu,
  createOrg,
  createField,
} from '../../mayash-database/datastore/circles';

export default {
  tags: ['api', 'circles'],
  description: 'Create a circle of type edu, org, field and location.',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'admin'],
  },
  validate: {
    headers: Headers,
    payload: Joi.object({
      userId: Id.required(),
      username: Username.required(),
      name: Name.required(),
      circleType: CircleType.required(),
      avatar: Joi.string().default(
        '/public/photos/mayash-logo-transparent.png',
      ),
    }),
  },
  async handler(request, reply) {
    try {
      const {
        userId: id,
        username,
        name,
        circleType,
        avatar,
      } = request.payload;

      if (circleType === 'edu') {
        const res = await createEdu({ username, name, avatar, creatorId: id });

        reply(res);
        return;
      }

      if (circleType === 'org') {
        const res = await createOrg({ username, name, avatar, creatorId: id });

        reply(res);
        return;
      }

      if (circleType === 'field') {
        const res = await createField({ username, name, avatar });

        reply(res);
        return;
      }

      if (circleType === 'location') {
        reply({
          statusCode: 503,
          error: 'Service Unavailable',
        });
      }
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
