/**
 * This API route will create a new circle,
 * this route is only for admin authorisation.
 *
 * @format
 */

import Joi from 'joi';

import { Headers, CircleType } from '../../mayash-common/schema';
import { getAll } from '../../mayash-database/datastore/circles';

export default {
  tags: ['api', 'circles'],
  description: 'get all Circles.',
  notes: 'get all circles.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'admin'],
  },
  validate: {
    headers: Headers,
    query: Joi.object({
      circleType: CircleType,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleType } = request.query;

      const res = await getAll({ circleType });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
