/** @format */

import Joi from 'joi';

import { Headers, Id, CircleMemberRole } from '../../../mayash-common/schema';
import { update } from '../../../mayash-database/datastore/circles/members';

export default {
  tags: ['api', 'circles'],
  description: 'Update role of a user with its circle id and member id',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
      memberId: Id.required(),
    }).length(2),
    payload: Joi.object({
      role: CircleMemberRole.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, memberId } = request.params;
      const { role } = request.payload;

      const res = await update({ circleId, memberId, role });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
