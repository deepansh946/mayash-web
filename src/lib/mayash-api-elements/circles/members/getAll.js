/** @format */

import Joi from 'joi';

import { Id, Next } from '../../../mayash-common/schema';
import { getAll } from '../../../mayash-database/datastore/circles/members';

/**
 *
 * @todo - complete next part of this API
 */
export default {
  tags: ['api', 'circles'],
  description: 'Display members of a particular circle.',
  notes:
    'If successful, it returns a success code with message' +
    ' and the list of all the members.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    params: Joi.object({
      circleId: Id.required(),
    }).length(1),
    query: Joi.object({
      next: Next,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;
      const { next } = request.query;

      const res = await getAll({ circleId, next });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
