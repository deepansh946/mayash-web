/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { create } from '../../../mayash-database/datastore/circles/members';

export default {
  tags: ['api', 'circles'],
  description: 'User wants to join a circle',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
      memberId: Id.required(),
    }).length(2),
  },
  async handler(request, reply) {
    try {
      const { circleId, memberId } = request.params;

      const res = await create({ circleId, memberId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
