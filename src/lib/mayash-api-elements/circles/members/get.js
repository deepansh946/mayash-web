/** @format */

import Joi from 'joi';

import { Id } from '../../../mayash-common/schema';
import { get } from '../../../mayash-database/datastore/circles/members';

export default {
  tags: ['api', 'circles'],
  description: 'Display members of a particular circle.',
  notes:
    'If successful, it returns a success code with message' +
    ' and the list of all the members.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    headers: Joi.object({
      authorization: Joi.string(),
    }).unknown(),
    params: Joi.object({
      circleId: Id.required(),
      memberId: Id.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { circleId, memberId } = request.params;

      const res = await get({ circleId, memberId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
