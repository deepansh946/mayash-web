/** @format */

import Joi from 'joi';

import { Headers, Id } from '../../../mayash-common/schema';
import { deleteMember } from '../../../mayash-database/datastore/circles/members';

export default {
  tags: ['api', 'circles'],
  description: 'Delete a member by its circle id and member id.',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
      memberId: Id.required(),
    }).length(2),
  },
  async handler(request, reply) {
    try {
      const { circleId, memberId } = request.params;

      const res = await deleteMember({ circleId, memberId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
