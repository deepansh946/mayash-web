/**
 * This file contains all the routes related to elements
 *
 * @format
 */

import validate from './elements/validate';

import elementsGetAll from './elements/getElements';
import elementGetById from './elements/getElementById';
import elementGetByUsername from './elements/getByUsername';

import userGet from './users/get';
import usersGet from './users/getAll';
import userCreate from './users/create';
import userUpdate from './users/update';
import userDelete from './users/delete';

import userUsernameUpdate from './users/username/update';

import userGetCircle from './users/circles/userGetCircle';

import guruRequests from './users/guru/guruRequests';
import createGuru from './users/guru/create';
import getAllGuru from './users/guru/getAll';
import getGuru from './users/guru/get';
import deleteGuru from './users/guru/delete';

import userPasswordGet from './users/password/get';
import userPasswordCreate from './users/password/create';
import userPasswordUpdate from './users/password/update';

import circleGet from './circles/get';
import circlesGetAll from './circles/getAll';
import circleCreate from './circles/create';
import circleUpdate from './circles/update';

import circleMembersGetAll from './circles/members/getAll';
import circleMemberCreate from './circles/members/create';
import circleMemberUpdate from './circles/members/update';
import circleMemberDelete from './circles/members/delete';

// this function will be used for only routes which are defined but
// not implemented.
const handler = (request, reply) =>
  reply({
    statusCode: 503,
    error: 'Service Unavailable.',
  });

const routes = [
  /* ------------------------------------------------------------------
                            Routes for Elements
  -------------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/elements/validate',
    config: validate,
  },

  {
    method: 'GET',
    path: '/elements',
    config: elementsGetAll,
  },

  {
    method: 'GET',
    path: '/elements/{id}',
    config: elementGetById,
  },

  {
    method: 'GET',
    path: '/elements/@{username}',
    config: elementGetByUsername,
  },

  /* ------------------------------------------------------------------
                            Routes for Users
  -------------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/users',
    config: usersGet,
  },
  {
    method: 'POST',
    path: '/users',
    config: userCreate,
  },
  {
    method: 'GET',
    path: '/users/{userId}',
    config: userGet,
  },
  {
    method: 'PUT',
    path: '/users/{userId}',
    config: userUpdate,
  },
  {
    method: 'PUT',
    path: '/users/{userId}/username',
    config: userUsernameUpdate,
  },

  /** Routes for User's password */
  {
    method: 'GET',
    path: '/users/{userId}/password',
    config: userPasswordGet,
  },
  {
    method: 'POST',
    path: '/users/{userId}/password',
    config: userPasswordCreate,
  },
  {
    method: 'PUT',
    path: '/users/{userId}/password',
    config: userPasswordUpdate,
  },

  /** Routes for User's circles */
  {
    method: 'GET',
    path: '/users/{userId}/circles',
    config: userGetCircle,
  },

  {
    method: 'PUT',
    path: '/users/{userId}/gururequest',
    config: guruRequests,
  },

  {
    method: 'POST',
    path: '/users/{userId}/guru',
    config: createGuru,
  },

  {
    method: 'GET',
    path: '/users/{userId}/gurus/all',
    config: getAllGuru,
  },

  {
    method: 'GET',
    path: '/users/{userId}/guru',
    config: getGuru,
  },

  {
    method: 'DELETE',
    path: '/users/{userId}/guru',
    config: deleteGuru,
  },

  /* ------------------------------------------------------------------
                      Routes for Circles
     ------------------------------------------------------------------ */
  // Get all circles,
  // Note: Only for Admins
  {
    method: 'GET',
    path: '/circles',
    config: circlesGetAll,
  },

  // This route will reply circle's info with it's id.
  {
    method: 'GET',
    path: '/circles/{circleId}',
    config: circleGet,
  },

  // Create New Circle, this is for admin only
  {
    method: 'POST',
    path: '/circles',
    config: circleCreate,
  },

  // Update circle by it's Id, only circle's admin member can change
  // its info.
  {
    method: 'PUT',
    path: '/circles/{circleId}',
    config: circleUpdate,
  },

  /* ------------------------------------------------------------------
                      Routes for Circles Members
     ------------------------------------------------------------------ */
  // Get members of circle.
  {
    method: 'GET',
    path: '/circles/{circleId}/members',
    config: circleMembersGetAll,
  },

  // Submit to join the circle.

  {
    method: 'POST',
    path: '/circles/{circleId}/members/{memberId}',
    config: circleMemberCreate,
  },

  /**
   * Accept member's request to join circle or
   * Update role of member in circle.
   * this API is only accessible to circle's admin.
   */
  {
    method: 'PUT',
    path: '/circles/{circleId}/members/{memberId}',
    config: circleMemberUpdate,
  },

  // Delete circle member or leave circle by circle id and member id.
  {
    method: 'DELETE',
    path: '/circles/{circleId}/members/{memberId}',
    config: circleMemberDelete,
  },
];

if (process.env.NODE_ENV === 'development') {
  routes.push({
    method: 'DELETE',
    path: '/users/{userId}',
    config: userDelete,
  });

  /* ------------------------------------------------------------------
                          Routes for User's Timeline
     ------------------------------------------------------------------ */

  routes.push({
    method: 'GET',
    path: '/users/{userId}/timeline',
    config: {
      tags: ['api', 'users'],
      description: 'This is for timeline feature for user.',
      handler,
    },
  });

  /* ------------------------------------------------------------------
                          Routes for Circles
     ------------------------------------------------------------------ */

  // Get all the request of users to join circles.
  routes.push({
    method: 'GET',
    path: '/circles/{circleId}/member_requests',
    config: { handler },
  });
}

export default routes;
