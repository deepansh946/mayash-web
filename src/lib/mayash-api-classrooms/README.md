## Mayash API Classrooms

This library is sub part of **Mayash API** plugin.

It contains all the API routes for Classrooms.

## Folder Structure

    .
    ├─┬ Classrooms
    │ ├── createClassroom.js
    │ ├── disableClassroom.js
    │ ├── enableClassroom.js
    │ ├── getAllClassroomCourses.js
    │ ├── getClassroomCourses.js
    │ └── getCourses.js
    ├─┬ Degrees
    │ ├── createDegreeInDepartment.js
    │ ├── deleteDegreeInDepartment.js
    │ ├── getAllDegrees.js
    │ ├── getDegreeInDepartment.js
    │ └── updateDegreeInDepartment.js
    ├─┬ Departments
    │ ├── createDepartmentInCircle.js
    │ ├── deleteDepartmentInCircle.js
    │ ├── getAllDepartmentInCircle.js
    │ ├── getDepartmentInCircle.js
    │ └── updateDepartmentInCircle.js
    ├── README.md
    ├─┬ Semester
    │ ├── createSemesterInDegree.js
    │ ├── deleteSemesterInDegree.js
    │ ├── getAllSemesterInDegree.js
    │ └── updateSemesterInDegree.js
    └── index.js
