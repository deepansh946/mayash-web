/** @format */

import Joi from 'joi';

import { getAll } from '../../mayash-database/datastore/classrooms';
import { Id, DeptId, DegreeId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'semester'],
  description: 'Display all semesters in a degree',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'user'],
  },
  validate: {
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
      degreeId: DegreeId,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId, degreeId } = request.params;

      const res = await getAll({
        circleId,
        deptId,
        degreeId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
