/** @format */

import Joi from 'joi';

import { updateSemesterInDegree } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, DeptId, DegreeId, SemesterId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'semester'],
  description: 'Update a semester in a degree',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
      degreeId: DegreeId,
      semId: SemesterId,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId, degreeId, semId } = request.params;

      const res = await db.classrooms.updateSemesterInDegree({
        circleId,
        deptId,
        degreeId,
        semId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
