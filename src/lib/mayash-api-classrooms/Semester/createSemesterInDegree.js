/** @format */

import Joi from 'joi';

import { createSemesterInDegree } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId, DegreeId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'semester'],
  description: 'Create a semester in a degree',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
    }),
    payload: Joi.object({
      deptId: DeptId,
      degreeId: DegreeId,
      semId: Joi.integer(),
      title: Title,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;
      const { deptId, degreeId, semId, title } = request.payload;

      const res = await createSemesterInDegree({
        circleId,
        deptId,
        degreeId,
        semId,
        title,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
