/** @format */

import Joi from 'joi';

import { deleteDepartmentInCircle } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, DeptId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'departments'],
  description: 'Delete a department in a circle',
  notes: 'If successful, it returns statusCode with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId } = request.params;

      const res = await deleteDepartmentInCircle({
        circleId,
        deptId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
