/** @format */

import Joi from 'joi';

import { getAllDepartmentInCircle } from '../../mayash-database/datastore/classrooms';
import { Id, Headers } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'departments'],
  description: 'Display all departments in a circle',
  notes:
    'If successful, it returns statusCode with a message' +
    'and all department details',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;

      const res = await getAllDepartmentInCircle({ circleId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
