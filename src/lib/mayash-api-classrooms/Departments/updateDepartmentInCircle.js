/** @format */

import Joi from 'joi';

import { updateDepartmentInCircle } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'departments'],
  description: 'Update a department in a circle',
  notes: 'If successful, it returns statusCode with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
    }),
    payload: Joi.object({
      deptId: DeptId,
      title: Title,
      hodId: Id,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;
      const { deptId, title, hodId } = request.paylaod;

      const res = await updateDepartmentInCircle({
        circleId,
        deptId,
        title,
        hodId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
