/** @format */

import Joi from 'joi';

import { getDepartmentInCircle } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'departments'],
  description: 'Display a department in a circle',
  notes:
    'If successful, it returns statusCode with a message' +
    'and department details',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
    }),
    payload: Joi.object({
      title: Title,
      hodId: Id,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId } = request.params;

      const res = await getDepartmentInCircle({
        circleId,
        deptId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
