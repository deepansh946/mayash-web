/** @format */

import Joi from 'joi';

import { createDepartmentInCircle } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'departments'],
  description: 'Create a department in a circle',
  notes: 'If successful, it returns statusCode with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
    }),
    payload: Joi.object({
      deptId: DeptId,
      title: Title,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;
      const { deptId, title } = request.payload;

      const res = await createDepartmentInCircle({
        circleId,
        deptId,
        title,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
