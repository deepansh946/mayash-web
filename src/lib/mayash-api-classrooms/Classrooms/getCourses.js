/** @format */

import Joi from 'joi';

import { getClassroomCourses } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Degree, Semester, Next } from '../../mayash-common';

export default {
  tags: ['api', 'classrooms'],
  description: 'Display all the courses',
  notes: 'If successful, it returns list of all the courses.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'owner', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      id: Id.required(),
    }),
    query: Joi.object({
      semester: Semester,
      degree: Degree,
      next: Next,
    }).max(3),
  },
  async handler(request, reply) {
    try {
      const params = {
        id: request.params.id,
        ...request.query,
      };

      const res = await getClassroomCourses(params);

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
