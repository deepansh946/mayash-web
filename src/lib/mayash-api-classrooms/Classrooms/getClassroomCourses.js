/** @format */

import Joi from 'joi';

import { getClassroomCourses } from '../../mayash-database/datastore/classrooms';

import { Id, Headers, Degree, Semester, Next } from '../../mayash-common';

export default {
  tags: ['api', 'classrooms'],
  description: 'Display a classroom course in a circle',
  notes: 'If successful, it will return a success message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      id: Id.required(),
    }),
    query: Joi.object({
      degree: Degree.required(),
      semester: Semester.required(),
      next: Next,
    }).max(3),
  },
  async handler(request, reply) {
    try {
      const params = {
        id: request.params.id,
        ...request.query,
      };

      const res = await getClassroomCourses(params, (result) => reply(result));

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
