/** @format */

import Joi from 'joi';

import { disableClassroom } from '../../mayash-database/datastore/classrooms';

import { Id, Headers } from '../../mayash-common';

export default {
  tags: ['api', 'classrooms'],
  description: 'Disable classroom for a circle',
  notes: 'If successful, it will return a success message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'admin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      id: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const params = {
        id: request.params.id,
      };

      const res = await disableClassroom(params);

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
