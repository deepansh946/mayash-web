/** @format */

import Joi from 'joi';

import { createClassroom } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, CourseId } from '../../mayash-common';

export default {
  tags: ['api', 'classrooms'],
  description: 'Create a classroom in a semester',
  notes: 'If successful, it will create a classroom.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      profId: Id.required(),
      courseId: CourseId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const params = {
        profId: request.params.profId,
        courseId: request.params.courseId,
      };

      const res = await createClassroom(params);

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
