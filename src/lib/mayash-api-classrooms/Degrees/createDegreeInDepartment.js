/** @format */

import Joi from 'joi';

import { createDegreeInDepartment } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId, DegreeId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'degrees'],
  description: 'Create a degree in a department',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
    }),
    payload: Joi.object({
      deptId: DeptId,
      degreeId: DegreeId,
      title: Title,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId } = request.params;
      const { deptId, degreeId, title } = request.paylaod;

      const res = await createDegreeInDepartment({
        circleId,
        deptId,
        degreeId,
        title,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
