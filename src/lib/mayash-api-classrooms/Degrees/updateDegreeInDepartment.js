/** @format */

import Joi from 'joi';

import { updateDegreeInDepartment } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId, DegreeId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'degrees'],
  description: 'Update a degree in a department',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
      degreeId: DegreeId,
    }),
    payload: Joi.object({
      title: Title,
      duration: Joi.number(),
      description: Joi.string(),
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId, degreeId } = request.params;
      const { title, duration, description } = request.paylaod;

      const res = await updateDegreeInDepartment({
        circleId,
        deptId,
        degreeId,
        title,
        duration,
        description,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
