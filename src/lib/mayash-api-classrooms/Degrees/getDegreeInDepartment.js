/** @format */

import Joi from 'joi';

import { getDegreeInDepartment } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, Title, DeptId, DegreeId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'degrees'],
  description: 'Get a degree in a department',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
      degreeId: DegreeId,
    }),
    payload: Joi.object({
      title: Title,
      duration: Joi.number(),
      description: Joi.string(),
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId, degreeId } = request.params;

      const res = await getDegreeInDepartment({
        circleId,
        deptId,
        degreeId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
