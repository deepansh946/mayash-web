/** @format */

import Joi from 'joi';

import { getAllDegrees } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, DeptId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'degrees'],
  description: 'Display all degrees in a department',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId } = request.params;
      const res = getAllDegrees({ circleId, deptId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
