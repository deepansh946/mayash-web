/** @format */

import Joi from 'joi';

import { deleteDegreeInDepartment } from '../../mayash-database/datastore/classrooms';
import { Id, Headers, DeptId, DegreeId } from '../../mayash-common';

export default {
  tags: ['api', 'circles', 'degrees'],
  description: 'Delete a degree in a department',
  notes: 'If successful, it returns code with a message.',
  auth: {
    mode: 'required',
    strategies: ['trafficCheckScheme', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id,
      deptId: DeptId,
      degreeId: DegreeId,
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId, deptId, degreeId } = request.params;

      const res = await deleteDegreeInDepartment({
        circleId,
        deptId,
        degreeId,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
