/** @format */

import createDepartmentInCircle from './Departments/createDepartmentInCircle';
import updateDepartmentInCircle from './Departments/updateDepartmentInCircle';
import deleteDepartmentInCircle from './Departments/deleteDepartmentInCircle';
import getDepartmentInCircle from './Departments/getDepartmentInCircle';
import getAllDepartmentInCircle from './Departments/getAllDepartmentInCircle';

import createDegreeInDepartment from './Degrees/createDegreeInDepartment';
import updateDegreeInDepartment from './Degrees/updateDegreeInDepartment';
import deleteDegreeInDepartment from './Degrees/deleteDegreeInDepartment';
import getAllDegrees from './Degrees/getAllDegrees';
import getDegreeInDepartment from './Degrees/getDegreeInDepartment';

import createSemesterInDegree from './Semester/createSemesterInDegree';
import getAllSemesterInDegree from './Semester/getAllSemesterInDegree';
import deleteSemesterInDegree from './Semester/deleteSemesterInDegree';

import createClassroom from './Classrooms/createClassroom';
import getCourses from './Classrooms/getCourses';

const routes = [];

/* -----------------------------------------------------------------
                          For Departments
  ------------------------------------------------------------------ */

routes.push({
  method: 'GET',
  path: '/circles/{circleId}/departments/{deptId}',
  config: getDepartmentInCircle,
});

routes.push({
  method: 'GET',
  path: '/circles/{circleId}/departments/',
  config: getAllDepartmentInCircle,
});

routes.push({
  method: 'PUT',
  path: '/circles/{circleId}/departments/{deptId}',
  config: updateDepartmentInCircle,
});

routes.push({
  method: 'POST',
  path: '/circles/{circleId}/departments/',
  config: createDepartmentInCircle,
});

routes.push({
  method: 'DELETE',
  path: '/circles/{circleId}/departments/{deptId}',
  config: deleteDepartmentInCircle,
});

/* -----------------------------------------------------------------
                          For Degrees
  ------------------------------------------------------------------ */

routes.push({
  method: 'POST',
  path: '/circles/{circleId}/departments/{deptId}/degrees/{degreeId}',
  config: createDegreeInDepartment,
});

routes.push({
  method: 'PUT',
  path: '/circles/{circleId}/departments/{deptId}/degrees/{degreeId}',
  config: updateDegreeInDepartment,
});

routes.push({
  method: 'PUT',
  path: '/circles/{circleId}/departments/{deptId}/degrees/{degreeId}',
  config: getDegreeInDepartment,
});

routes.push({
  method: 'GET',
  path: '/circles/{circleId}/departments/{deptId}/degrees',
  config: getAllDegrees,
});

routes.push({
  method: 'DELETE',
  path: '/circles/{circleId}/departments/{deptId}/degrees/{degreeId}',
  config: deleteDegreeInDepartment,
});

/* -----------------------------------------------------------------
                          For Semesters
  ------------------------------------------------------------------ */

routes.push({
  method: 'POST',
  path: [
    '/circles/{circleId}/departments/{deptId}/',
    'degrees/{degreeId}/semesters/',
  ].join(),
  config: createSemesterInDegree,
});

routes.push({
  method: 'GET',
  path: [
    '/circles/{circleId}/departments/{deptId}/',
    'degrees/{degreeId}/semesters',
  ].join(),
  config: getAllSemesterInDegree,
});

routes.push({
  method: 'DELETE',
  path: [
    '/circles/{circleId}/departments/{deptId}/',
    'degrees/{degreeId}/semesters/{semId}',
  ].join(),
  config: deleteSemesterInDegree,
});

if (process.env.NODE_ENV === 'development') {
  routes.push({
    method: 'PUT',
    path: [
      '/circles/{circleId}/departments/{deptId}/',
      'degrees/{degreeId}/semesters/{semId}',
    ].join(),
    config: {
      tags: ['api', 'circles', 'semesters'],
      description: 'Update a semester in a degree',
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });
}

/* -----------------------------------------------------------------
                          For Semester Classes
  ------------------------------------------------------------------ */

routes.push({
  method: 'GET',
  path: [
    '/circles/{circleId}/departments/{deptId}/',
    'degrees/{degreeId}/semesters/{semId}/classes',
  ].join(),
  config: getCourses,
});

routes.push({
  method: 'POST',
  path: [
    '/circles/{circleId}/departments/{deptId}/',
    'degrees/{degreeId}/semesters/{semId}/classes/{classId}',
  ].join(),
  config: createClassroom,
});

if (process.env.NODE_ENV === 'development') {
  routes.push({
    method: 'PUT',
    path: [
      '/circles/{circleId}/departments/{deptId}/',
      'degrees/{degreeId}/semesters/{semId}/classes/{classId}',
    ].join(),
    config: {
      tags: ['api', 'circles', 'classroom'],
      description: 'Update a class in a semester',
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'DELETE',
    path: [
      '/circles/{circleId}/departments/{deptId}/',
      'degrees/{degreeId}/semesters/{semId}/classes/{classId}',
    ].join(),
    config: {
      tags: ['api', 'circles', 'classroom'],
      description: 'Delete a class in a semester',
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });
}

export default routes;
