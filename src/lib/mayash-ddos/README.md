# Mayash DOS

## Mayash Denial of Service

This is a Hapi.js Plugin.

This plugin will detect and keep track of all the Traffic that comes to Mayash server. If anyone tries to hack server with DDOS attack, this plugin will stop them from doing it.

## Folder Structure

    .
    ├── README.md
    ├── index.js
    └── trafficCheckScheme.js