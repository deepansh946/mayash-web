/**
 *
 *
 * @format
 */
/* eslint no-unused-vars: 1 */

import Boom from 'boom';

const { TOKEN_KEY } = process.env;

const IP = {};

/*
* This function will check the traffic on
* the website by limiting the number of requests
*/
const trafficCheckScheme = (server, options) => ({
  authenticate(request, reply) {
    const minute = Math.floor(Date.now() / 120000);
    const { remoteAddress } = request.info;

    if (!IP[minute]) {
      IP[minute] = {};
      IP[minute][remoteAddress] = 1;
    } else if (!IP[minute][remoteAddress]) {
      IP[minute][remoteAddress] = 1;
    } else {
      IP[minute][remoteAddress] += 1;
    }

    if (IP[minute] && IP[minute][remoteAddress] > 20) {
      return reply(
        Boom.tooManyRequests(
          'you have exceeded your request limit, try after 2 minutes.',
        ),
      );
    }

    return reply.continue({ credentials: {} });
  },
});

/**
 * This function will delete 5 minute old value from 'userRead', 'userWrite' and 'IP' variable
 */
setInterval(() => {
  const minute = Math.floor(Date.now() / 120000);

  if (IP[minute - 2]) {
    delete IP[minute - 2];
  }
}, 3000);
