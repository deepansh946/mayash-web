/**
 *
 * @format
 */
/* eslint no-unused-vars: "off" */

// off for now, will change it later.

import Boom from 'boom';

const { NODE_ENV, TOKEN_KEY } = process.env;

/**
 * Interval contant will set limit to handle request in (ms).
 *
 * @example: in production, interval will be of 250ms to check
 *  if user is sending limited request to server and in development,
 * the interval will be of 100ms to check because in development
 * we need to send more requests as compared to production
 *
 */
const TIME_LIMIT = NODE_ENV === 'production' ? 250 : 100;

const IP = {};
const visitor = {};
const userRead = {};
const userWrite = {};

/**
 * This function will check the traffic on
 * the website by limiting the number of requests
 * Here interval is 20 seconds
 */
const trafficCheckScheme = (server) => ({
  authenticate(request, reply) {
    const interval = Math.floor(Date.now() / TIME_LIMIT);

    if (!IP[interval]) {
      IP[interval] = {};
      IP[interval][request.info.remoteAddress] = 1;
    } else if (!IP[interval][request.info.remoteAddress]) {
      IP[interval][request.info.remoteAddress] = 1;
    } else {
      IP[interval][request.info.remoteAddress] += 1;
    }

    if (IP[interval] && IP[interval][request.info.remoteAddress] > 20) {
      return reply(
        Boom.tooManyRequests(
          'you have exceeded your request limit, try after 2 minutes.',
        ),
      );
    }

    return reply.continue({ credentials: {} });
  },
});

/*
* This function will limit the number of users on
* our website in a single session
* Visitor auth is applied to only GET methods and 
* sign in/auth token is not required
*/
const visitorScheme = (server, options) => ({
  authenticate(request, reply) {
    const interval = Math.floor(Date.now() / TIME_LIMIT);

    if (!visitor[interval]) {
      visitor[interval] = {};
      visitor[interval][request.info.remoteAddress] = 1;
    } else if (!visitor[interval][request.info.remoteAddress]) {
      visitor[interval][request.info.remoteAddress] = 1;
    } else {
      visitor[interval][request.info.remoteAddress] += 1;
    }

    if (
      visitor[interval] &&
      visitor[interval][request.info.remoteAddress] > 20
    ) {
      return reply(
        Boom.tooManyRequests(
          'you have exceeded your request limit, try after 2 minutes.',
        ),
      );
    }

    return reply.continue({ credentials: {} });
  },
});

const register = (server, options, next) => {
  // This strategy will check the limit of certain IP for 'visitors',
  // who are not logged in.
  server.auth.scheme('trafficCheckScheme', trafficCheckScheme);
  server.auth.strategy('trafficCheck', 'trafficCheckScheme');

  server.auth.scheme('visitorScheme', visitorScheme);
  server.auth.strategy('visitor', 'visitorScheme');

  // This auth strategy will check the traffic limit for data read for users
  // ReadTrafficCheck is applied to the users of the website
  // which are signed in and is used for reading purposes.
  server.auth.strategy('ReadTrafficCheck', 'jwt', {
    key: TOKEN_KEY,
    validateFunc(decoded, request, callback) {
      const interval = Math.floor(Date.now() / TIME_LIMIT);

      if (!userRead[interval]) {
        userRead[interval] = {};
        userRead[interval][decoded.id] = 1;
      } else if (!userRead[interval][decoded.id]) {
        userRead[interval][decoded.id] = 1;
      } else {
        userRead[interval][decoded.id] += 1;
      }

      if (userRead[interval] && userRead[interval][decoded.id] > 20) {
        return callback(
          Boom.tooManyRequests(
            'you have exceeded your request limit, try after 2 minutes.',
          ),
          false,
        );
      }

      return callback(null, true);
    },
  });

  // This auth strategy will check the traffic limit for data write for users
  server.auth.strategy('WriteTrafficCheck', 'jwt', {
    key: TOKEN_KEY,
    validateFunc(decoded, request, callback) {
      const interval = Math.floor(Date.now() / TIME_LIMIT);

      if (!userWrite[interval]) {
        userWrite[interval] = {};
        userWrite[interval][decoded.id] = 1;
      } else if (!userWrite[interval][decoded.id]) {
        userWrite[interval][decoded.id] = 1;
      } else {
        userWrite[interval][decoded.id] += 1;
      }

      if (userWrite[interval] && userWrite[interval][decoded.id] > 2) {
        return callback(
          Boom.tooManyRequests(
            'you have exceeded your request limit, try after 2 minutes.',
          ),
          false,
        );
      }

      return callback(null, true);
    },
  });

  next();
};

register.attributes = {
  pkg: {
    name: 'mayash-traffic',
    version: '0.0.0',
    description: 'This plugin will add function for creating traffic check.',
  },
};

/*
 * This function will set minute value to 'userRead', 
 * 'userWrite' and 'IP' variable.
 */
setInterval(() => {
  const interval = Math.floor(Date.now() / TIME_LIMIT);

  if (!IP[interval]) {
    IP[interval] = {};
  }

  if (!userRead[interval]) {
    userRead[interval] = {};
  }

  if (!userWrite[interval]) {
    userWrite[interval] = {};
  }
}, 1000);

/*
 * This function will delete 5 minute old value from 'userRead', 
 * 'userWrite' and 'IP' variable
 */
setInterval(() => {
  const interval = Math.floor(Date.now() / TIME_LIMIT);

  if (IP[interval - 2]) {
    delete IP[interval - 2];
  }

  if (userRead[interval - 2]) {
    delete userRead[interval - 2];
  }

  if (userWrite[interval - 2]) {
    delete userWrite[interval - 2];
  }
}, 3000);

export default register;
