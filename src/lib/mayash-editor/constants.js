/**
 * Some of the constants which are used throughout this project instead of
 * directly using string.
 *
 * @format
 */

/**
 * @constant Blocks
 */
export const Blocks = {
  UNSTYLED: 'unstyled',
  PARAGRAPH: 'unstyled',

  H1: 'header-one',
  H2: 'header-two',
  H3: 'header-three',
  H4: 'header-four',
  H5: 'header-five',
  H6: 'header-six',

  OL: 'ordered-list-item',
  UL: 'unordered-list-item',

  CODE: 'code-block',

  BLOCKQUOTE: 'blockquote',

  ATOMIC: 'atomic',
  PHOTO: 'atomic:photo',
  VIDEO: 'atomic:video',
};

/**
 * @constant Inline
 */
export const Inline = {
  BOLD: 'BOLD',
  CODE: 'CODE',
  ITALIC: 'ITALIC',
  STRIKETHROUGH: 'STRIKETHROUGH',
  UNDERLINE: 'UNDERLINE',
  HIGHLIGHT: 'HIGHLIGHT',
};

/**
 * @constant Entity
 */
export const Entity = {
  LINK: 'LINK',
};

/**
 * @constant HYPERLINK
 */
export const HYPERLINK = 'hyperlink';

/**
 * Constants to handle key commands
 */
export const HANDLED = 'handled';
export const NOT_HANDLED = 'not_handled';

export default {
  Blocks,
  Inline,
  Entity,
};
