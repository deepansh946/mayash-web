# Mayash Editor

This is an text editor build on top of draft-js and is customised for Mayash Webapp requirements.

## Reference draft.js editor

### Library
* https://github.com/nikgraf/awesome-draft-js
* https://reactrocket.com/post/getting-started-with-draft-js/
* https://github.com/bkniffler/draft-wysiwyg/
* https://github.com/jpuri/react-draft-wysiwyg
* https://github.com/nikgraf/awesome-draft-js
* http://howtox.com/medium-editor-clones-in-js/
* http://slatejs.org

### Videos

* https://www.youtube.com/watch?v=rHat0n1xBVc
* https://www.youtube.com/watch?v=w-PqnpMizcQ


Note:
* http://www.freeformatter.com/html-escape.html, use this website to study escaping characters.

## Folder Structure 

    .
    ├── Editor.js
    ├── EditorState.js
    ├── README.md
    ├─┬ components
    │ └── Decorators.js
    ├── convertToString.js
    ├── index.js
    └─┬ style
      └── index.js