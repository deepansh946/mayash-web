/**
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Blocks } from '../constants';

const styles = {
  photo: {
    width: '100%',
    // Fix an issue with Firefox rendering video controls
    // with 'pre-wrap' white-space
    whiteSpace: 'initial',
  },
};

/**
 *
 * @class Atomic - this React component will be used to render Atomic
 * components of Draft.js
 *
 * @todo - configure this for audio.
 * @todo - configure this for video.
 */
class Atomic extends Component {
  static propTypes = {
    contentState: PropTypes.object.isRequired,
    block: PropTypes.any.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { contentState, block } = this.props;

    const entity = contentState.getEntity(block.getEntityAt(0));
    const { src } = entity.getData();
    const type = entity.getType();

    if (type === Blocks.PHOTO) {
      return (
        <div>
          <img alt={'alt'} src={src} style={styles.photo} />
        </div>
      );
    }

    return <div />;
  }
}

export default Atomic;
