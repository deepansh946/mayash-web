/** @format */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import classNames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';

// import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';

import TitleIcon from 'material-ui-icons/Title';
import FormatBoldIcon from 'material-ui-icons/FormatBold';
import FormatItalic from 'material-ui-icons/FormatItalic';
import FormatUnderlinedIcon from 'material-ui-icons/FormatUnderlined';
import FormatQuoteIcon from 'material-ui-icons/FormatQuote';
// import FormatClearIcon from 'material-ui-icons/FormatClear';
// import FormatColorFillIcon from 'material-ui-icons/FormatColorFill';
// import FormatColorResetIcon from 'material-ui-icons/FormatColorReset';
// import FormatColorTextIcon from 'material-ui-icons/FormatColorText';
// import FormatIndentDecreaseIcon
//   from 'material-ui-icons/FormatIndentDecrease';
// import FormatIndentIncreaseIcon
//   from 'material-ui-icons/FormatIndentIncrease';

import CodeIcon from 'material-ui-icons/Code';

import FormatListNumberedIcon from 'material-ui-icons/FormatListNumbered';
import FormatListBulletedIcon from 'material-ui-icons/FormatListBulleted';

import FormatAlignCenterIcon from 'material-ui-icons/FormatAlignCenter';
import FormatAlignLeftIcon from 'material-ui-icons/FormatAlignLeft';
import FormatAlignRightIcon from 'material-ui-icons/FormatAlignRight';
import FormatAlignJustifyIcon from 'material-ui-icons/FormatAlignJustify';

import AttachFileIcon from 'material-ui-icons/AttachFile';
import InsertLinkIcon from 'material-ui-icons/InsertLink';
import InsertPhotoIcon from 'material-ui-icons/InsertPhoto';
import InsertEmoticonIcon from 'material-ui-icons/InsertEmoticon';
import InsertCommentIcon from 'material-ui-icons/InsertComment';

// import VerticalAlignTopIcon from 'material-ui-icons/VerticalAlignTop';
// import VerticalAlignBottomIcon from 'material-ui-icons/VerticalAlignBottom';
// import VerticalAlignCenterIcon from 'material-ui-icons/VerticalAlignCenter';

// import WrapTextIcon from 'material-ui-icons/WrapText';

import HighlightIcon from 'material-ui-icons/Highlight';
import FunctionsIcon from 'material-ui-icons/Functions';

import {
  Editor,
  RichUtils,
  // Entity,
  // CompositeDecorator,
  AtomicBlockUtils,
  EditorState,
  //   convertToRaw,
} from 'draft-js';

// import {
//   hashtagStrategy,
//   HashtagSpan,

//   handleStrategy,
//   HandleSpan,
// } from './components/Decorators';

import Atomic from './components/Atomic';
import styles from './EditorStyles';

import { Blocks, HANDLED, NOT_HANDLED } from './constants';

/**
 * MayashEditor
 */
class MayashEditor extends Component {
  /**
   * Mayash Editor's proptypes are defined here.
   */
  static propTypes = {
    classes: PropTypes.object.isRequired,

    editorState: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,

    placeholder: PropTypes.string,

    readOnly: PropTypes.bool.isRequired,

    /**
     * This api function should be applied for enabling photo adding
     * feature in Mayash Editor.
     */
    apiPhotoUpload: PropTypes.func,
  };

  static defaultProps = {
    readOnly: true,
    placeholder: 'Write Here...',
  };

  constructor() {
    super();
    // this.state = {};

    /**
     * This function will be supplied to Draft.js Editor to handle
     * onChange event.
     * @function onChange
     * @param {Object} editorState
     */
    this.onChange = (editorState) => {
      this.props.onChange(editorState);
    };

    /**
     * This function will handle focus event in Draft.js Editor.
     * @function focus
     */
    this.focus = () => this.editorNode.focus();

    this.onTab = this.onTab.bind(this);
    this.onTitleClick = this.onTitleClick.bind(this);
    this.onCodeClick = this.onCodeClick.bind(this);
    this.onQuoteClick = this.onQuoteClick.bind(this);
    this.onListBulletedClick = this.onListBulletedClick.bind(this);
    this.onListNumberedClick = this.onListNumberedClick.bind(this);
    this.onBoldClick = this.onBoldClick.bind(this);
    this.onItalicClick = this.onItalicClick.bind(this);
    this.onUnderLineClick = this.onUnderLineClick.bind(this);
    this.onHighlightClick = this.onHighlightClick.bind(this);
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
    this.onClickInsertPhoto = this.onClickInsertPhoto.bind(this);

    // this.blockRendererFn = this.blockRendererFn.bind(this);

    // const compositeDecorator = new CompositeDecorator([
    //   {
    //     strategy: handleStrategy,
    //     component: HandleSpan,
    //   },
    //   {
    //     strategy: hashtagStrategy,
    //     component: HashtagSpan,
    //   },
    // ]);
  }

  /**
   * onTab() will handle Tab button press event in Editor.
   * @function onTab
   * @param {Event} e
   */
  onTab(e) {
    e.preventDefault();
    const { editorState } = this.props;

    const newEditorState = RichUtils.onTab(e, editorState, 4);

    this.onChange(newEditorState);
  }

  /**
   * This function will handle event on Title Button.
   * @function onTitleClick
   */
  onTitleClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleBlockType(editorState, 'header-one');

    this.onChange(newEditorState);
  }

  /**
   * This function will handle event on Code Button.
   * @function onCodeClick
   */
  onCodeClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleBlockType(editorState, 'code-block');

    this.onChange(newEditorState);
  }

  /**
   * This function will handle event on Quote Button.
   * @function onQuoteClick
   */
  onQuoteClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleBlockType(editorState, 'blockquote');

    this.onChange(newEditorState);
  }

  /**
   * This function will update block with unordered list items
   * @function onListBulletedClick
   */
  onListBulletedClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleBlockType(
      editorState,
      'unordered-list-item',
    );

    this.onChange(newEditorState);
  }

  /**
   * This function will update block with ordered list item.
   * @function onListNumberedClick
   */
  onListNumberedClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleBlockType(
      editorState,
      'ordered-list-item',
    );

    this.onChange(newEditorState);
  }

  /**
   * This function will give selected content Bold styling.
   * @function onBoldClick
   */
  onBoldClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleInlineStyle(editorState, 'BOLD');

    this.onChange(newEditorState);
  }

  /**
   * This function will give italic styling to selected content.
   * @function onItalicClick
   */
  onItalicClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleInlineStyle(editorState, 'ITALIC');

    this.onChange(newEditorState);
  }

  /**
   * This function will give underline styling to selected content.
   * @function onUnderLineClick
   */
  onUnderLineClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleInlineStyle(
      editorState,
      'UNDERLINE',
    );

    this.onChange(newEditorState);
  }

  /**
   * This function will highlight selected content.
   * @function onHighlightClick
   */
  onHighlightClick() {
    const { editorState } = this.props;

    const newEditorState = RichUtils.toggleInlineStyle(editorState, 'CODE');

    this.onChange(newEditorState);
  }

  /**
   *
   */
  onClickInsertPhoto() {
    this.photo.value = null;
    this.photo.click();
  }

  /**
   *
   */
  onChangeInsertPhoto = async (e) => {
    // e.preventDefault();
    const file = e.target.files[0];

    if (file.type.indexOf('image/') === 0) {
      const formData = new FormData();
      formData.append('photo', file);

      const { statusCode, error, payload } = await this.props.apiPhotoUpload({
        formData,
      });

      if (statusCode >= 300) {
        // handle Error
        console.error(statusCode, error);
        return;
      }

      const { photoUrl: src } = payload;

      const { editorState } = this.props;

      const contentState = editorState.getCurrentContent();
      const contentStateWithEntity = contentState.createEntity(
        Blocks.PHOTO,
        'IMMUTABLE',
        { src },
      );

      const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

      const middleEditorState = EditorState.set(editorState, {
        currentContent: contentStateWithEntity,
      });

      const newEditorState = AtomicBlockUtils.insertAtomicBlock(
        middleEditorState,
        entityKey,
        ' ',
      );

      this.onChange(newEditorState);
    }
  };

  /**
   * This function will give custom handle commands to Editor.
   * @function handleKeyCommand
   * @param {string} command
   * @return {string}
   */
  handleKeyCommand(command) {
    const { editorState } = this.props;

    const newEditorState = RichUtils.handleKeyCommand(editorState, command);

    if (newEditorState) {
      this.onChange(newEditorState);
      return HANDLED;
    }

    return NOT_HANDLED;
  }

  /* eslint-disable class-methods-use-this */
  blockRendererFn(block) {
    switch (block.getType()) {
      case Blocks.ATOMIC:
        return {
          component: Atomic,
          editable: false,
        };

      default:
        return null;
    }
  }
  /* eslint-enable class-methods-use-this */

  /* eslint-disable class-methods-use-this */
  blockStyleFn(block) {
    switch (block.getType()) {
      case 'blockquote':
        return 'RichEditor-blockquote';

      default:
        return null;
    }
  }
  /* eslint-enable class-methods-use-this */

  render() {
    const {
      classes,
      readOnly,
      onChange,
      editorState,
      placeholder,
    } = this.props;

    // Custom overrides for "code" style.
    const styleMap = {
      CODE: {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
        fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
        fontSize: 16,
        padding: 2,
      },
    };

    return (
      <div className={classes.root}>
        {!readOnly ? (
          <div className={classNames({ 'editor-controls': '' })}>
            <Tooltip title="Title" id="title" placement="bottom">
              <IconButton aria-label="Title" onClick={this.onTitleClick}>
                <TitleIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Bold" id="bold" placement="bottom">
              <IconButton aria-label="Bold" onClick={this.onBoldClick}>
                <FormatBoldIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Italic" id="italic" placement="bottom">
              <IconButton aria-label="Italic" onClick={this.onItalicClick}>
                <FormatItalic />
              </IconButton>
            </Tooltip>
            <Tooltip title="Underline" id="underline" placement="bottom">
              <IconButton
                aria-label="Underline"
                onClick={this.onUnderLineClick}
              >
                <FormatUnderlinedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Code" id="code" placement="bottom">
              <IconButton aria-label="Code" onClick={this.onCodeClick}>
                <CodeIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Quote" id="quote" placement="bottom">
              <IconButton aria-label="Quote" onClick={this.onQuoteClick}>
                <FormatQuoteIcon />
              </IconButton>
            </Tooltip>
            <Tooltip
              title="Unordered List"
              id="unorderd-list"
              placement="bottom"
            >
              <IconButton
                aria-label="Unordered List"
                onClick={this.onListBulletedClick}
              >
                <FormatListBulletedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Ordered List" id="ordered-list" placement="bottom">
              <IconButton
                aria-label="Ordered List"
                onClick={this.onListNumberedClick}
              >
                <FormatListNumberedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Align Left" id="align-left" placement="bottom">
              <IconButton aria-label="Align Left" disabled>
                <FormatAlignLeftIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Align Center" id="align-center" placement="bottom">
              <IconButton aria-label="Align Center" disabled>
                <FormatAlignCenterIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Align Right" id="align-right" placement="bottom">
              <IconButton aria-label="Align Right" disabled>
                <FormatAlignRightIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Align Right" id="align-right" placement="bottom">
              <IconButton aria-label="Align Right" disabled>
                <FormatAlignJustifyIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Attach File" id="attach-file" placement="bottom">
              <IconButton aria-label="Attach File" disabled>
                <AttachFileIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Insert Link" id="insert-link" placement="bottom">
              <IconButton aria-label="Insert Link" disabled>
                <InsertLinkIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Insert Photo" id="insert-photo" placement="bottom">
              <IconButton
                aria-label="Insert Photo"
                disabled={typeof this.props.apiPhotoUpload !== 'function'}
                onClick={this.onClickInsertPhoto}
              >
                <InsertPhotoIcon />
                <input
                  type="file"
                  accept="image/jpeg|png|gif"
                  onChange={this.onChangeInsertPhoto}
                  ref={(photo) => {
                    this.photo = photo;
                  }}
                  style={{ display: 'none' }}
                />
              </IconButton>
            </Tooltip>
            <Tooltip
              title="Insert Emoticon"
              id="insertE-emoticon"
              placement="bottom"
            >
              <IconButton aria-label="Insert Emoticon" disabled>
                <InsertEmoticonIcon />
              </IconButton>
            </Tooltip>
            <Tooltip
              title="Insert Comment"
              id="insert-comment"
              placement="bottom"
            >
              <IconButton aria-label="Insert Comment" disabled>
                <InsertCommentIcon />
              </IconButton>
            </Tooltip>
            <Tooltip
              title="Highlight Text"
              id="highlight-text"
              placement="bottom"
            >
              <IconButton
                aria-label="Highlight Text"
                onClick={this.onHighlightClick}
              >
                <HighlightIcon />
              </IconButton>
            </Tooltip>
            <Tooltip
              title="Add Functions"
              id="add-functions"
              placement="bottom"
            >
              <IconButton aria-label="Add Functions" disabled>
                <FunctionsIcon />
              </IconButton>
            </Tooltip>
          </div>
        ) : null}
        <div className={classNames({ 'editor-area': '' })}>
          <Editor
            /* Basics */

            editorState={editorState}
            onChange={onChange}
            /* Presentation */

            placeholder={placeholder}
            // textAlignment="center"

            // textDirectionality="LTR"

            blockRendererFn={this.blockRendererFn}
            blockStyleFn={this.blockStyleFn}
            customStyleMap={styleMap}
            // customStyleFn={() => {}}

            /* Behavior */

            // autoCapitalize="sentences"

            // autoComplete="off"

            // autoCorrect="off"

            readOnly={readOnly}
            spellCheck
            // stripPastedStyles={false}

            /* DOM and Accessibility */

            // editorKey

            /* Cancelable Handlers */

            // handleReturn={() => {}}

            handleKeyCommand={this.handleKeyCommand}
            // handleBeforeInput={() => {}}

            // handlePastedText={() => {}}

            // handlePastedFiles={() => {}}

            // handleDroppedFiles={() => {}}

            // handleDrop={() => {}}

            /* Key Handlers */

            // onEscape={() => {}}

            onTab={this.onTab}
            // onUpArrow={() => {}}

            // onRightArrow={() => {}}

            // onDownArrow={() => {}}

            // onLeftArrow={() => {}}

            // keyBindingFn={() => {}}

            /* Mouse Event */

            // onFocus={() => {}}

            // onBlur={() => {}}

            /* Methods */

            // focus={() => {}}

            // blur={() => {}}

            /* For Reference */

            ref={(node) => {
              this.editorNode = node;
            }}
          />
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(MayashEditor);
