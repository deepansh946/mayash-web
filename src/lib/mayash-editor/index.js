/** @format */

import Editor from './Editor';

export { MayashEditor } from './Editor';

export { createEditorState } from './EditorState';

export default Editor;
