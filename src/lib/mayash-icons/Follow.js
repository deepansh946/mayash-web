/** @format */

import React from 'react';
import PropTypes from 'prop-types';

const Follow = ({ style, width, height }) => (
  <svg
    version="1.1"
    id="mayash_follow"
    x="0px"
    y="0px"
    width={width || '32px'}
    height={height || '32px'}
    viewBox="0 0 455.731 455.731"
    style={{ enableBackground: 'new 0 0 455.731 455.731' }}
  >
    <g>
      <rect
        x="0"
        y="0"
        fill="rgb(245, 245, 245)"
        width="455.731"
        height="455.731"
      />
      <g>
        <path
          fill={style.follow ? 'rgba(3,169,144,1)' : 'rgba(177,177,177,1)'}
          d="M296.208,159.16C234.445,97.397,152.266,63.382,64.81,63.382v64.348c70.268,0,136.288,27.321,185.898,76.931c49.609,49.61,76.931,115.63,76.931,185.898h64.348C391.986,303.103,357.971,220.923,296.208,159.16z"
        />
        <path
          fill={style.follow ? 'rgba(3,169,144,1)' : 'rgba(177,177,177,1)'}
          d="M64.143,172.273v64.348c84.881,0,153.938,69.056,153.938,153.939h64.348C282.429,270.196,184.507,172.273,64.143,172.273z"
        />
        <circle
          fill={style.follow ? 'rgba(3,169,144,1)' : 'rgba(177,177,177,1)'}
          cx="109.833"
          cy="346.26"
          r="46.088"
        />
      </g>
    </g>
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
    <g />
  </svg>
);

Follow.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  style: PropTypes.object,
};

export default Follow;
