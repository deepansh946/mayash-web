# Mayash Icons

This library contains all the icons designed and build by **Mayash** Team.

## Folder Structure

    .
    ├── Css3.js
    ├── Facebook.js
    ├── Gmail.js
    ├── Google.js
    ├── GooglePlus.js
    ├── Html5.js
    ├── Instagram.js
    ├── LinkedIn.js
    ├── README.md
    ├── Skype.js
    ├── Twitter.js
    └── index.js