/** @format */

import Joi from 'joi';

import { create } from '../../../mayash-database/datastore/posts/comments';

import { Headers, Id, Reaction } from '../../../mayash-common/schema';

export default {
  tags: ['api', 'posts'],
  description: 'Create Comment',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: Id.required(),
    }),
    payload: Joi.object({
      data: Joi.object().required(),
    }),
  },
  handler: async (request, reply) => {
    try {
      const { userId, postId } = request.params;
      const { data } = request.payload;

      const res = await create({ userId, postId, data });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error.',
      });
    }
  },
};
