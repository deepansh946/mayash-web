/** @format */

import Joi from 'joi';

import { getAll } from '../../../mayash-database/datastore/posts/comments';
import { Headers, Id } from '../../../mayash-common/schema';

export default {
  tags: ['api', 'posts'],
  description: 'get all post reactions',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      postId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { postId } = request.params;

      const res = await getAll({ postId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
