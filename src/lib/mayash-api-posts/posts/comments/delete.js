/** @format */

import Joi from 'joi';

import { deletePostComment } from '../../../mayash-database/datastore/posts/comments';
import { Headers, Id } from '../../../mayash-common/schema';

export default {
  tags: ['api', 'posts'],
  description: 'delete Post Comment.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: Id.required(),
      commentId: Id.required(),
    }),
  },
  handler: async (request, reply) => {
    try {
      const { postId, commentId } = request.params;

      const res = await deletePostComment({ postId, commentId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error.',
      });
    }
  },
};
