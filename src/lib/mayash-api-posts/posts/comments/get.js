/** @format */

import Joi from 'joi';

import { get } from '../../../mayash-database/datastore/posts/comments';
import { Headers, Id, PostId } from '../../../mayash-common/schema';

export default {
  tags: ['api', 'posts'],
  description: 'get one post comment.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      postId: PostId.required(),
      commentId: Id.required(),
    }),
  },
  handler: async (request, reply) => {
    try {
      const { postId, commentId } = request.params;

      const res = await get({ postId, commentId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
