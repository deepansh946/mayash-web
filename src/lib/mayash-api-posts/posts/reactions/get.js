/** @format */

import Joi from 'joi';

import { getPostReaction } from '../../../mayash-database/datastore/posts/reactions';
import { Headers, Id } from '../../../mayash-common/schema';

const get = {
  tags: ['api', 'posts'],
  description:
    'This file will handle all "like", "unlike", "love" ' +
    'etc. responses on post.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: Id.required(),
    }),
  },
  handler: async (request, reply) => {
    try {
      const { userId, postId } = request.params;

      const res = await getPostReaction({ postId, userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default get;
