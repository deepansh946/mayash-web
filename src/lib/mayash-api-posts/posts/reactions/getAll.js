/** @format */

import Joi from 'joi';

import { getAllPostReaction } from '../../../mayash-database/datastore/posts/reactions';
import { Headers, Id } from '../../../mayash-common/schema';

const getAll = {
  tags: ['api', 'posts'],
  description: 'get all post reactions',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: Id.required(),
    }),
  },
  handler: async (request, reply) => {
    try {
      const { userId, postId } = request.params;

      const res = await getAllPostReaction({ postId, userId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};

export default getAll;
