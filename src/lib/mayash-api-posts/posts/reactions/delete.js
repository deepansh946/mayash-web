/** @format */

import Joi from 'joi';

import { deletePostReaction } from '../../../mayash-database/datastore/posts/reactions';
import { Headers, Id } from '../../../mayash-common/schema';

const deleteReaction = {
  tags: ['api', 'posts'],
  description:
    'This file will handle all "like", "love" ' + 'etc. responses on post.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: Id.required(),
      keyId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, postId, keyId } = request.params;

      const res = await deletePostReaction({ userId, postId, keyId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error.',
      });
    }
  },
};

export default deleteReaction;
