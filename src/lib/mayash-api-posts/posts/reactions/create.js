/** @format */

import Joi from 'joi';

import { createPostReaction } from '../../../mayash-database/datastore/posts/reactions';
import { Headers, Id, Reaction } from '../../../mayash-common/schema';

const create = {
  tags: ['api', 'posts'],
  description:
    'This file will handle all "like", "love" ' + 'etc. responses on post.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'user'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: Id.required(),
    }),
    payload: Joi.object({
      reaction: Reaction.required(),
    }).length(1),
  },
  async handler(request, reply) {
    try {
      const { userId, postId } = request.params;
      const { reaction } = request.payload;

      const res = await createPostReaction({ userId, postId, reaction });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error.',
      });
    }
  },
};

export default create;
