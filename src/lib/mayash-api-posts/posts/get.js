/** @format */

import Joi from 'joi';

import { PostId } from '../../mayash-common/schema';
import { get } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'posts'],
  description: 'Display a post to a visitor with the help of post id',
  notes: 'If successful, it returns a success code with message and the post',
  auth: {
    mode: 'required',
    strategies: ['visitor', 'postView'],
  },
  validate: {
    params: Joi.object({
      postId: PostId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { postId } = request.params;

      const res = await get({ postId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
