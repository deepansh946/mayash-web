/** @format */

import Joi from 'joi';

import { Headers, Id, PostId } from '../../mayash-common/schema';
import { deletePost } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'circles', 'posts'],
  description:
    'delete a post for circle by circleId. Here auth' +
    ' will be check for the admin role of the circle.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
      postId: PostId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { postId } = request.params;

      const res = await deletePost({ postId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
