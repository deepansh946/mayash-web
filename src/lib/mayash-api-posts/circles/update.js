/** @format */

import Joi from 'joi';

import {
  Headers,
  Id,
  PostId,
  // PostType,
  Title,
  Description,
  Data,
} from '../../mayash-common/schema';
import { update } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'circles', 'posts'],
  description:
    'update post for circle by circleId. Here auth' +
    ' will be check for the admin role of the circle.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
      postId: PostId.required(),
    }),
    payload: Joi.object({
      title: Title,
      description: Description,
      data: Data,
    })
      .min(1)
      .max(3),
  },
  async handler(request, reply) {
    try {
      const { postId } = request.params;
      const { title, description, data } = request.payload;

      const res = await update({ postId, title, description, data });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
