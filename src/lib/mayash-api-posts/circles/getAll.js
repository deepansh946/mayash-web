/** @format */

import Joi from 'joi';

import { Id } from '../../mayash-common/schema';
import { getAll } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'circles', 'posts'],
  description: 'Display all posts to a visitor',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    params: Joi.object({
      circleId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { circleId: id } = request.params;

      const res = await getAll({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
