/**
 * This function is used to create the post of a circle by a user
 * Also a auth strategies is applied so that user can't create
 * more than 3 posts at a single time
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id, PostType, Title } from '../../mayash-common/schema';
import { create } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'circles', 'posts'],
  description:
    'create a post for circle by circleId. Here auth' +
    ' will be check for the admin role of the circle.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'circleAdmin'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
    }),
    payload: Joi.object({
      postType: PostType.required(),
      title: Title.required(),
    }).length(2),
  },
  async handler(request, reply) {
    try {
      const { circleId: id } = request.params;
      const { title, postType } = request.payload;

      const res = await create({ id, postType, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
