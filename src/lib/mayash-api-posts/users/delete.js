/** @format */

import Joi from 'joi';

import { Headers, Id, PostId } from '../../mayash-common/schema';
import { deletePost } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'users', 'posts'],
  description: 'delete a post of user by userId and postId.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: PostId.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { postId } = request.params;

      const res = await deletePost({ postId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
