/**
 * This function is used to create the posts by a user
 * Also a auth strategies is applied so that user can't create
 * more than 3 posts at a single time
 *
 * @format
 */

import Joi from 'joi';

import { Headers, Id, PostType, Title } from '../../mayash-common/schema';
import { create } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'users', 'posts'],
  description: 'create a new post for user by userId.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }),
    payload: Joi.object({
      postType: PostType.required(),
      title: Title.required(),
    }).length(2),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;
      const { title, postType } = request.payload;

      const res = await create({ id, title, postType });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
