/** @format */

import Joi from 'joi';

import {
  Headers,
  Id,
  PostId,
  // PostType,
  Title,
  Description,
  Data,
} from '../../mayash-common/schema';
import { update } from '../../mayash-database/datastore/posts';

export default {
  tags: ['api', 'users', 'posts'],
  description: 'update a post by userId and postId.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      postId: PostId.required(),
    }),
    payload: Joi.object({
      title: Title,
      description: Description,
      data: Data,
      cover: Joi.string(),
      privacy: Joi.boolean(),
      keywords: Joi.array().items([Joi.string()]),
    })
      .min(1)
      .max(6),
  },
  async handler(request, reply) {
    try {
      const { postId } = request.params;
      const {
        title,
        description,
        data,
        cover,
        privacy,
        keywords,
      } = request.payload;

      const res = await update({
        postId,
        title,
        description,
        data,
        cover,
        privacy,
        keywords,
      });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
