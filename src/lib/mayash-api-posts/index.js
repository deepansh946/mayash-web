/** @format */

import getPost from './posts/get';
import reactionGet from './posts/reactions/get';
import commentGet from './posts/comments/get';

import userPostsGet from './users/getAll';
import userPostCreate from './users/create';
import userPostUpdate from './users/update';
import userPostDelete from './users/delete';

import circlePostsGet from './circles/getAll';
import circlePostCreate from './circles/create';
import circlePostUpdate from './circles/update';
import circlePostDelete from './circles/delete';

import reactionGetAll from './posts/reactions/getAll';
import reactionCreate from './posts/reactions/create';
import reactionDelete from './posts/reactions/delete';

import commentGetAll from './posts/comments/getAll';
import commentCreate from './posts/comments/create';
import commentDelete from './posts/comments/delete';

const { NODE_ENV } = process.env;

const routes = [
  // as get Routes are independent of user/circle, so keep
  // them separately.
  {
    method: 'GET',
    path: '/posts/{postId}',
    config: getPost,
  },
  {
    method: 'GET',
    path: '/posts/{postId}/reactions',
    config: reactionGetAll,
  },
  {
    method: 'GET',
    path: '/posts/{postId}/comments',
    config: commentGetAll,
  },
  {
    method: 'GET',
    path: '/posts/{postId}/comments/{commentId}',
    config: commentGet,
  },

  /* --------------------------------------------------------------
                      Routes for users
     -------------------------------------------------------------- */
  {
    method: 'GET',
    path: '/users/{userId}/posts',
    config: userPostsGet,
  },

  {
    method: 'POST',
    path: '/users/{userId}/posts',
    config: userPostCreate,
  },

  {
    method: 'PUT',
    path: '/users/{userId}/posts/{postId}',
    config: userPostUpdate,
  },

  {
    method: 'DELETE',
    path: '/users/{userId}/posts/{postId}',
    config: userPostDelete,
  },

  /* -------------------------------------------------------------
                Routes for circles
                Only admin of circle can create post for circle.
   ------------------------------------------------------------- */
  {
    method: 'GET',
    path: '/circles/{circleId}/posts',
    config: circlePostsGet,
  },

  {
    method: 'POST',
    path: '/circles/{circleId}/posts',
    config: circlePostCreate,
  },

  {
    method: 'PUT',
    path: '/circles/{circleId}/posts/{postId}',
    config: circlePostUpdate,
  },

  {
    method: 'DELETE',
    path: '/circles/{circleId}/posts/{postId}',
    config: circlePostDelete,
  },

  /* --------------------------------------------------------------
                          POST COMMENT
   -------------------------------------------------------------- */

  {
    method: 'POST',
    path: '/users/{userId}/posts/{postId}/comments',
    config: commentCreate,
  },

  {
    method: 'DELETE',
    path: '/users/{userId}/posts/{postId}/comments/{commentId}',
    config: commentDelete,
  },

  /* --------------------------------------------------------------
                          POST REACTIONS
     ------------------------------------------------------------- */

  {
    method: 'GET',
    path: '/users/{userId}/posts/{postId}/reactions',
    config: reactionGet,
  },

  {
    method: 'POST',
    path: '/users/{userId}/posts/{postId}/reactions',
    config: reactionCreate,
  },

  {
    method: 'DELETE',
    path: '/users/{userId}/posts/{postId}/reactions/{keyId}',
    config: reactionDelete,
  },
];

// if any route is in development stage, keep them here
if (NODE_ENV === 'development') {
  // for example
  // routes.push({
  //   method: 'GET',
  //   path: '/posts/{postId}',
  //   config: {
  //     handler(request, reply) {
  //       reply('this route is in development');
  //     },
  //   },
  // });
}

export default routes;
