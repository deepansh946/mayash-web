# Mayash API Posts

This library is sub part of **Mayash API** plugin.

It contains all the API routes for posts.

## Folder Structure

    .
    ├── README.md
    ├── index.js
    ├── posts
    │   ├── get.js
    │   ├── comments
    │   │   ├── create.js
    │   │   ├── delete.js
    │   │   ├── getAll.js
    │   │   └── get.js
    │   └── reactions
    │       ├── create.js
    │       ├── delete.js
    │       ├── getAll.js
    │       └── get.js
    ├── users
    │   ├── create.js
    │   ├── delete.js
    │   ├── getAll.js
    │   └── update.js
    └── circles
        ├── create.js
        ├── delete.js
        ├── getAll.js
        └── update.js
