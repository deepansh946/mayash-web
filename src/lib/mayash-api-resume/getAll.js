/**
 * This API will getall the resume
 *
 * @format
 */

import { Headers } from '../mayash-common/schema';
import { getAll } from '../mayash-database/datastore/resume';

export default {
  tags: ['api', 'resume'],
  description: 'get all resume for a user',
  notes: 'If successfull it will return a success message',
  auth: {
    mode: 'required',
    strategies: ['user'],
  },
  validate: {
    headers: Headers,
  },
  async handler(request, reply) {
    try {
      const res = await getAll();

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
