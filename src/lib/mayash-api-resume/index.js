/** @format */

import createNote from './create';
import getNote from './get';
import updateNote from './update';
import getAllNote from './getAll';

const routes = [];

routes.push({
  method: 'POST',
  path: '/resumes',
  config: createNote,
});

routes.push({
  method: 'GET',
  path: '/resumes/{resumeId}',
  config: getNote,
});

routes.push({
  method: 'PUT',
  path: '/resumes/{resumeId}',
  config: updateNote,
});

routes.push({
  method: 'GET',
  path: '/resumes',
  config: getAllNote,
});

export default routes;
