/**
 * This API will get a resume template
 *
 * @format
 */

import Joi from 'joi';

import { Headers } from '../mayash-common/schema';
import { get } from '../mayash-database/datastore/resume';

export default {
  tags: ['api', 'resume'],
  description: 'This will get a resume template',
  notes: 'If successfull it will return a success message',
  auth: {
    mode: 'required',
    strategies: ['user'],
  },
  validate: {
    headers: Headers,
    params: {
      resumeId: Joi.string().required(),
    },
  },
  async handler(request, reply) {
    try {
      const { resumeId } = request.params;

      const res = await get({ resumeId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
