/**
 * This API will create the resume template
 *
 * @format
 */

import Joi from 'joi';

import { Headers } from '../mayash-common/schema';
import { create } from '../mayash-database/datastore/resume';

export default {
  tags: ['api', 'resume'],
  description: 'Create a new resume template',
  notes: 'If successfull it will return a success message',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    payload: {
      resumeId: Joi.string().required(),
      name: Joi.string().required(),
      data: Joi.object().required(),
    },
  },
  async handler(request, reply) {
    try {
      const { resumeId, name, data } = request.payload;

      const res = await create({ resumeId, name, data });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
