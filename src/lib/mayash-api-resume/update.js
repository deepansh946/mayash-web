/**
 * This API will update the resume template
 *
 * @format
 */

import Joi from 'joi';

import { Headers } from '../mayash-common/schema';

import { update } from '../mayash-database/datastore/resume';

export default {
  tags: ['api', 'resume'],
  description: 'This will update a resume template',
  notes: 'If successfull it will return a success message',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    params: {
      resumeId: Joi.string().required(),
    },
    payload: {
      name: Joi.string().required(),
      data: Joi.object().required(),
    },
  },
  async handler(request, reply) {
    try {
      const { resumeId, name, data: data1 } = request.payload;

      const res = await update({ resumeId, name, data1 });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
