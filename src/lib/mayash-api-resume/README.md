## Mayash API Resume

This library is sub part of **Mayash API** plugin.

It contains all the API routes for Resume.

## Folder Structure

    .
    ├── README.md
    ├── create.js
    ├── get.js
    ├── getAll.js
    ├── index.js
    └── update.js