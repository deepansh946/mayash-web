/** @format */

import getNote from './getNote';
import getAllNotes from './getAllNotes';
import createNote from './createNote';
import updateNote from './updateNote';
import deleteNote from './deleteNote';

const routes = [];

routes.push({
  method: 'GET',
  path: '/users/{userId}/notes/{noteId}',
  config: getNote,
});

routes.push({
  method: 'GET',
  path: '/users/{userId}/notes',
  config: getAllNotes,
});

routes.push({
  method: 'POST',
  path: '/users/{userId}/notes',
  config: createNote,
});

routes.push({
  method: 'PUT',
  path: '/users/{userId}/notes/{noteId}',
  config: updateNote,
});

routes.push({
  method: 'DELETE',
  path: '/users/{userId}/notes/{noteId}',
  config: deleteNote,
});

export default routes;
