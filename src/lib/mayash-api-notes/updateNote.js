/**
 * This API route will create a update a note,
 *
 * @format
 */

import Joi from 'joi';

import db from '../mayash-database/datastore';
import { Headers, Id, Title, NoteType } from '../mayash-common/schema';

export default {
  tags: ['api', 'notes'],
  description: 'Update a note for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      noteId: Id.required(),
    }),
    payload: Joi.object({
      noteType: NoteType.required(),
      title: Title.required(),
    }),
  },
  handler: (request, reply) => {
    const { userId: id, noteId } = request.params;
    const { noteType, title } = request.payload;

    db.notes
      .updateNote({ id, noteId, noteType, title })
      .then((res) => reply(res))
      .catch((err) => reply(err));
  },
};
