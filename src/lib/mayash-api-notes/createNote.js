/**
 * This API route will create a new note,
 *
 * @format
 */

import Joi from 'joi';

import { createNote } from '../mayash-database/datastore/notes';
import { Headers, Id, Title, NoteType } from '../mayash-common/schema';

export default {
  tags: ['api', 'notes'],
  description: 'Create a note for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }),
    payload: Joi.object({
      noteType: NoteType.required(),
      title: Title.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;
      const { noteType, title } = request.payload;

      const res = await createNote({ id, noteType, title });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
