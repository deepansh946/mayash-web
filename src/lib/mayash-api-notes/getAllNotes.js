/**
 * This API route will get the list of all notes
 *
 * @format
 */

import Joi from 'joi';

import { getAllNotes } from '../mayash-database/datastore/notes';
import { Headers, Id } from '../mayash-common/schema';

export default {
  tags: ['api', 'notes'],
  description: 'Display the list of all notes for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id } = request.params;

      const res = await db.notes.getAllNotes({ id });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
