## Mayash API Notes

This library is sub part of **Mayash API** plugin.

It contains all the API routes for notes.


## Folder Structure

    .
    ├── README.md
    ├── createNote.js
    ├── deleteNote.js
    ├── getAllNotes.js
    ├── getNote.js
    ├── index.js
    └── updateNote.js