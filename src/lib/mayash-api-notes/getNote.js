/**
 * This API route will get a note,
 *
 * @format
 */

import Joi from 'joi';

import { getNote } from '../mayash-database/datastore/notes';
import { Headers, Id } from '../mayash-common/schema';

export default {
  tags: ['api', 'notes'],
  description: 'Display a note for a user',
  notes: 'If successful, it returns a success code with message.',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'owner'],
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      noteId: Id.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId: id, noteId } = request.params;

      const res = await getNote({ id, noteId });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
