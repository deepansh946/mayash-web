/** @format */

/* eslint consistent-return: "off" */

import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';
import Joi from 'joi';

import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from 'jss';
import preset from 'jss-preset-default';
// import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import createGenerateClassName from 'material-ui/styles/createGenerateClassName';

import { PostId } from '../../mayash-common/schema';

import AppRoutes from '../../../client/AppRoutes';

import actionPostGet from '../../../client/actions/posts/get';

import configureStore from '../../../client/store/configureStore';

const { NODE_ENV } = process.env;

export default {
  auth: {
    mode: 'required',
    strategies: ['postView'],
  },
  validate: {
    params: Joi.object({
      postId: PostId.required(),
    }),
  },
  handler: (request, reply) => {
    const { postId } = request.params;

    // This state is coming from browser cookie. All are in string
    const { isSignedIn, id, username, token } = request.state;

    // This context is for react router
    const context = {};

    // View Context is for Handlebar template
    const viewContext = {
      title: 'Welcome to Mayash',
      description: [
        'Mayash is a learning and sharing platform, where ',
        'all the educational institute, teachers, professional, students ',
        'can learn and share their knowledge. Here we are covering all ',
        'the fields of education with freedom of language.',
      ].join(),
      keywords: [
        'mayash',
        'mayash.com',
        'mayash.in',
        'mayash.io',
        'mayash.xyz',
        'mayash.edu',
        'mayash education',
        'online classroom',
        'online courses',
      ].join(),
      imageUrl:
        'https://storage.googleapis.com/mayash/website/mayash-title.png',
      type: 'website',
      url: `https://mayash.io${request.path}`,
      site_name: 'Transforming Education',
      app: '',
      css: '',
      initialState: '',
      PRODUCTION: NODE_ENV === 'production',
    };

    // Configuring redux store
    let store = configureStore();
    const initialState = store.getState();

    // Create a sheetsRegistry instance.
    const sheetsRegistry = new SheetsRegistry();

    // Configure JSS
    const jss = create(preset());
    jss.options.createGenerateClassName = createGenerateClassName;

    if (isSignedIn === 'true') {
      store = configureStore({
        ...initialState,
        elements: {
          ...initialState.elements,
          user: {
            ...initialState.elements.user,
            isSignedIn: true,
            id: parseInt(id, 10),
            username,
            token,
          },
        },
      });
    }

    store.dispatch(actionPostGet({ postId, token }));

    const unsubscribe = store.subscribe(() => {
      unsubscribe();

      viewContext.app = renderToString(
        <JssProvider registry={sheetsRegistry} jss={jss}>
          <Provider store={store}>
            <StaticRouter context={context} location={request.url.path}>
              <AppRoutes />
            </StaticRouter>
          </Provider>
        </JssProvider>,
      );

      viewContext.initialState = JSON.stringify(
        encodeURIComponent(JSON.stringify(store.getState())),
      );

      // Grab the CSS from our sheetsRegistry.
      viewContext.css = sheetsRegistry
        .toString()
        // this will remove '\n', '\r'.
        .replace(/[\n\r]+/g, '')
        // this will get rid of more than 1 space
        .replace(/\s{2,10}/g, ' ');

      if (context.url) {
        return reply.redirect(context.url);
      }

      const tempState = store.getState();

      const post = tempState.posts[postId];

      if (post.isError) {
        viewContext.title = `${post.statusCode}: ${post.error}`;
      }

      if (post.isFetched) {
        viewContext.title = post.title;
        viewContext.description = post.description;
        viewContext.imageUrl =
          typeof post.cover === 'undefined'
            ? viewContext.imageUrl
            : `https://mayash.io${post.cover}`;

        viewContext.keywords =
          typeof post.keywords === 'undefined'
            ? viewContext.keywords
            : post.keywords.join(', ');
      }

      return reply.view('index', viewContext);
    });
  },
};
