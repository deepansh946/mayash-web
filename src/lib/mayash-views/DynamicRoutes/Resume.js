/**
 * @format
 */

/* eslint consistent-return: "off" */

import Joi from 'joi';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';

import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from 'jss';
import preset from 'jss-preset-default';
// import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
import createGenerateClassName from 'material-ui/styles/createGenerateClassName';

import { Username } from '../../mayash-common/schema';

import AppRoutes from '../../../client/AppRoutes';

import actionGetElementByUsername from '../../../client/actions/elements/getByUsername';

import configureStore from '../../../client/store/configureStore';

const { NODE_ENV } = process.env;

export default {
  validate: {
    params: Joi.object({
      username: Username.required(),
    }),
  },
  handler(request, reply) {
    // This context is for React Router
    const context = {};

    // Handlebars Context
    const viewContext = {
      title: 'Welcome to Mayash',
      description: [
        'Mayash is a learning and sharing platform, where ',
        'all the educational institute, teachers, professional, students ',
        'can learn and share their knowledge. Here we are covering all ',
        'the fields of education with freedom of language.',
      ].join(),
      keywords: [
        'mayash',
        'mayash.com',
        'mayash.in',
        'mayash.io',
        'mayash.xyz',
        'mayash.edu',
        'mayash education',
        'online classroom',
        'online courses',
      ].join(),
      imageUrl:
        'https://storage.googleapis.com/' + 'mayash/website/mayash-title.png',
      type: 'website',
      url: request.path,
      site_name: 'Transforming Education',
      app: '',
      css: '',
      initialState: '',
      PRODUCTION: NODE_ENV === 'production',
    };

    let store = configureStore();
    const initialState = store.getState();

    const { isSignedIn, id, username, token } = request.state;

    // Create a sheetsRegistry instance.
    const sheetsRegistry = new SheetsRegistry();

    // Configure JSS
    const jss = create(preset());
    jss.options.createGenerateClassName = createGenerateClassName;

    if (isSignedIn === 'true') {
      store = configureStore({
        ...initialState,
        elements: {
          user: {
            ...initialState.elements.user,
            isSignedIn: true,
            id: parseInt(id, 10),
            username,
            token,
          },
        },
      });
    }

    store.dispatch(
      actionGetElementByUsername({
        username: request.params.username,
        token,
      }),
    );

    const unsubscribe = store.subscribe(() => {
      unsubscribe();

      viewContext.app = renderToString(
        <JssProvider registry={sheetsRegistry} jss={jss}>
          <Provider store={store}>
            <StaticRouter context={context} location={request.url.path}>
              <AppRoutes />
            </StaticRouter>
          </Provider>
        </JssProvider>,
      );

      viewContext.initialState = JSON.stringify(
        encodeURIComponent(JSON.stringify(store.getState())),
      );

      // Grab the CSS from our sheetsRegistry.
      viewContext.css = sheetsRegistry
        .toString()
        // this will remove '\n', '\r'.
        .replace(/[\n\r]+/g, '')
        // this will get rid of more than 1 space
        .replace(/\s{2,10}/g, ' ');

      if (context.url) {
        return reply.redirect(context.url);
      }
      return reply.view('index', viewContext);
    });
  },
};
