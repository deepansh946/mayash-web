## Mayash Views

This Library is a Hapi.js plugin which contains all the Routes for server.

**Static Routes**

  This folder contains all the routes which are static in nature, means they are just to display informations.


**Dynamic Routes**

  These routes are dynamic in nature, means their contents will changes over parameter and users.

## Folder Structure

    .
    ├─┬ DynamicRoutes
    │ ├── Classroom.js
    │ ├── Course.js
    │ ├── Dashboard.js
    │ ├── Element.js
    │ ├── Home.js
    │ ├── Others.js
    │ ├── Post.js
    │ ├── Profile.js
    │ ├── PublicFolder.js
    │ └── Resume.js
    ├── README.md
    ├─┬ StaticRoutes
    │ ├── AboutUs.js
    │ ├── ContactUs.js
    │ └── Team.js
    └── index.js