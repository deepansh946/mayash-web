/** @format */

import Home from './DynamicRoutes/Home';
import Profile from './DynamicRoutes/Profile';
import Dashboard from './DynamicRoutes/Dashboard';
import Element from './DynamicRoutes/Element';
import Resume from './DynamicRoutes/Resume';
import Post from './DynamicRoutes/Post';
import Course from './DynamicRoutes/Course';
import Classroom from './DynamicRoutes/Classroom';
import PublicFolder from './DynamicRoutes/PublicFolder';
import Others from './DynamicRoutes/Others';

import Introduction from './StaticRoutes/Introduction';
import Services from './StaticRoutes/Services';
import Pricing from './StaticRoutes/Pricing';
import Team from './StaticRoutes/Team';
import JoinUs from './StaticRoutes/JoinUs';
import DevelopersTraining from './StaticRoutes/DevelopersTraining';
import ContactUs from './StaticRoutes/ContactUs';

const { NODE_ENV } = process.env;

const routes = [
  { method: 'GET', path: '/', config: Home },

  // all static routes will be defined here.
  { method: 'GET', path: '/introduction', config: Introduction },
  { method: 'GET', path: '/services', config: Services },
  { method: 'GET', path: '/pricing', config: Pricing },
  { method: 'GET', path: '/team', config: Team },
  { method: 'GET', path: '/join-us', config: JoinUs },
  { method: 'GET', path: '/developers-training', config: DevelopersTraining },
  { method: 'GET', path: '/contact-us', config: ContactUs },

  // all dynamic routes should be defined here

  { method: 'GET', path: '/profile', config: Profile },
  { method: 'GET', path: '/dashboard', config: Dashboard },

  { method: 'GET', path: '/@{username}/resume', config: Resume },

  { method: 'GET', path: '/@{username}/{url*}', config: Element },

  { method: 'GET', path: '/posts/{postId}', config: Post },

  { method: 'GET', path: '/courses/{courseId}/{url*}', config: Course },

  { method: 'GET', path: '/public/{url*}', config: PublicFolder },

  {
    method: 'GET',
    path: '/favicon.ico',
    handler: (request, reply) => reply.file('./public/photos/favicon.ico'),
  },
];

if (NODE_ENV === 'development') {
  routes.push({
    method: 'GET',
    path: '/classrooms/@{username}/{url*}',
    config: Classroom,
  });
}

// Always add this route in the last to handle rest of the undefined routes.
routes.push({
  method: 'GET',
  path: '/{url*}',
  config: Others,
});

const register = (server, options, next) => {
  server.route(routes);

  next();
};

register.attributes = {
  pkg: {
    name: 'mayash-views',
    version: '0.0.0',
    description:
      'This plugin contains all the features related to React server ' +
      'side rendering.',
  },
};

export default register;
