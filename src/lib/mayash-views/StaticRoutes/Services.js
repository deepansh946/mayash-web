/**
 *
 *
 * @format
 */

import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { Provider } from 'react-redux';

import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import { create } from 'jss';
import preset from 'jss-preset-default';
import createGenerateClassName from 'material-ui/styles/createGenerateClassName';

import AppRoutes from '../../../client/AppRoutes';

import configureStore from '../../../client/store/configureStore';

const { NODE_ENV } = process.env;

export default {
  handler(request, reply) {
    const context = {};
    const viewContext = {
      title: 'Services',
      description:
        'Our products and services will help students as well as teachers to ' +
        'improve their knowledge and skills.',
      keywords: [
        'mayash',
        'mayash.com',
        'mayash.in',
        'mayash.io',
        'mayash.xyz',
        'mayash.edu',
        'mayash education',
        'online classroom',
        'online courses',
        'mayash services',
        'services',
        'learning and sharing',
      ].join(),
      imageUrl:
        'https://storage.googleapis.com/mayash/website/mayash-title.png',
      type: 'website',
      url: request.path,
      site_name: 'Mayash',
      app: '',
      initialState: '',
      PRODUCTION: NODE_ENV === 'production',
    };

    let store = configureStore();
    const initialState = store.getState();

    const { isSignedIn, id, username, token } = request.state;

    if (isSignedIn === 'true') {
      store = configureStore({
        ...initialState,
        elements: {
          user: {
            ...initialState.elements.user,
            isSignedIn: true,
            id: parseInt(id, 10),
            username,
            token,
          },
        },
      });
    }

    // Create a sheetsRegistry instance.
    const sheetsRegistry = new SheetsRegistry();

    // Configure JSS
    const jss = create(preset());
    jss.options.createGenerateClassName = createGenerateClassName;

    viewContext.app = renderToString(
      <JssProvider registry={sheetsRegistry} jss={jss}>
        <Provider store={store}>
          <StaticRouter context={context} location={request.url.path}>
            <AppRoutes />
          </StaticRouter>
        </Provider>
      </JssProvider>,
    );

    viewContext.initialState = JSON.stringify(
      encodeURIComponent(JSON.stringify(store.getState())),
    );

    // Grab the CSS from our sheetsRegistry.
    viewContext.css = sheetsRegistry
      .toString()
      // this will remove '\n', '\r'.
      .replace(/[\n\r]+/g, '')
      // this will get rid of more than 1 space
      .replace(/\s{2,10}/g, ' ');

    if (context.url) {
      reply.redirect(context.url);
      return;
    }

    reply.view('index', viewContext);
  },
};
