/** @format */

import Joi from 'joi';

import { PostId, FileName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'files', 'posts'],
  description: 'Retrieve an image from the database',
  notes:
    'If successful, it returns a success code with message and' + ' the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    params: Joi.object({
      postId: PostId.required(),
      fileName: FileName.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { postId, fileName } = request.params;

      const file = storage.file(`${ROOT}files/posts/${postId}/${fileName}`);

      file
        .createReadStream()
        .on('data', () => {})
        .on('response', (response) => reply(response));
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
