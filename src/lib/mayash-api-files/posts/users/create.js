/** @format */

import Joi from 'joi';

import { Headers, Id, PostId, File } from '../../../mayash-common/schema';
import { create } from '../../../mayash-database/datastore/files/posts';
import storage from '../../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

const validate = {
  headers: Headers,
  params: Joi.object({
    userId: Id.required(),
    postId: PostId.required(),
  }),
  payload: Joi.object({
    file: File,
  }),
};

async function handler(request, reply) {
  try {
    const { userId: id, postId } = request.params;
    const { file } = request.payload;

    const { filename, headers } = file.hapi;
    const filenameArray = filename.split('.');

    const fileType = filenameArray[filenameArray.length - 1];
    const contentType = headers['content-type'];

    const { statusCode, error, payload } = await create({
      id,
      postId,
      type: fileType,
    });

    if (statusCode >= 300) {
      reply({ statusCode, error });
      return;
    }

    const { fileId } = payload;

    const newFile = storage.file(
      `${ROOT}files/posts/${postId}/${fileId}.${fileType}`,
    );

    file
      .pipe(
        newFile.createWriteStream({
          metadata: { contentType },
        }),
      )
      .on('error', (err) => {
        console.error(err);

        reply({
          statusCode: 500,
          error: 'Storage Server Error',
        });
      })
      .on('finish', () =>
        reply({
          statusCode: 201,
          message: 'File upload success.',
          payload: {
            id,
            fileId,
            fileName: `${fileId}.${fileType}`,
            fileUrl: `/api/posts/${postId}/files/${fileId}.${fileType}`,
          },
        }),
      );
  } catch (error) {
    console.error(error);

    reply({
      statusCode: 500,
      error: 'Server Error.',
    });
  }
}

export default {
  tags: ['api', 'files', 'posts'],
  description: 'Upload an image to the database',
  notes: 'If successful, it returns a success code with message and',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'owner'],
  },
  payload: {
    maxBytes: 307200, // 300 Kb is the size limit.
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  validate,
  handler,
};
