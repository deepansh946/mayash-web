/** @format */

const routes = [];

if (process.env.NODE_ENV === 'development') {
  routes.push({
    method: 'GET',
    path: '/users/{userId}/files/{fileName}',
    config: {
      tags: ['api', 'users', 'files'],
      description: 'Display the files by its id',
      handler(request, reply) {
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        });
      },
    },
  });

  routes.push({
    method: 'POST',
    path: '/users/{userId}/files',
    config: {
      tags: ['api', 'users', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/files/{fileName}',
    config: {
      tags: ['api', 'users', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'GET',
    path: '/posts/{postId}/files/{fileName}',
    config: {
      tags: ['api', 'posts', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'POST',
    path: '/users/{userId}/posts/{postId}/files',
    config: {
      tags: ['api', 'users', 'posts', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/posts/{postId}/files/{fileName}',
    config: {
      tags: ['api', 'users', 'posts', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'GET',
    path: '/courses/{courseId}/files/{fileName}',
    config: {
      tags: ['api', 'courses', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'POST',
    path: '/users/{userId}/courses/{courseId}/files',
    config: {
      tags: ['api', 'users', 'courses', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });

  routes.push({
    method: 'DELETE',
    path: '/users/{userId}/courses/{courseId}/files/{fileName}',
    config: {
      tags: ['api', 'users', 'courses', 'files'],
      handler: (request, reply) =>
        reply({
          statusCode: 503,
          error: 'Service Unavailable.',
        }),
    },
  });
}

export default routes;
