/** @format */

import Joi from 'joi';

import { /* Headers, */ Id, FileName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'files', 'users'],
  description: 'Retrieve an image from the database',
  notes:
    'If successful, it returns a success code with message and' + ' the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    // Header not Required here.
    // headers: Headers,
    params: Joi.object({
      userId: Id.required(),
      fileName: FileName.required(),
    }),
  },
  async handler(request, reply) {
    try {
      const { userId, fileName } = request.params;

      const file = storage.file(`${ROOT}files/elements/${userId}/${fileName}`);

      file
        .createReadStream()
        .on('data', () => {})
        .on('response', (response) => reply(response));
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
