/** @format */

import Joi from 'joi';

import { create } from '../../mayash-database/datastore/files/circles';
import { Id, File, Headers } from '../../mayash-common/schema';

import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

async function handler(request, reply) {
  try {
    const { circleId } = request.params;
    const { file } = request.payload;

    const { filename, headers } = file.hapi;
    const filenameArray = filename.split('.');

    const fileType = filenameArray[filenameArray.length - 1];
    const contentType = headers['content-type'];

    const { statusCode, error, payload } = await create({
      circleId,
      type: fileType,
    });

    if (statusCode >= 300) {
      reply({ statusCode, error });
      return;
    }

    const { fileId } = payload;

    const newFile = storage.file(
      `${ROOT}files/elements/${circleId}/${fileId}.${fileType}`,
    );

    file
      .pipe(
        newFile.createWriteStream({
          metadata: { contentType },
        }),
      )
      .on('error', (err) => {
        console.error(err);

        return reply({
          statusCode: 500,
          error: 'Storage Server Error',
        });
      })
      .on('finish', () =>
        reply({
          statusCode: 201,
          message: 'File upload success.',
          payload: {
            circleId,
            fileId,
            fileName: `${fileId}.${fileType}`,
            fileUrl: `/api/circles/${circleId}/files/${fileId}.${fileType}`,
          },
        }),
      );
  } catch (error) {
    console.error(error);

    reply({
      statusCode: 500,
      error: 'Server Error',
    });
  }
}

export default {
  tags: ['api', 'circles', 'files'],
  description: 'Upload an image to the database',
  notes: 'If successful, it returns a success code with message and',
  auth: {
    mode: 'required',
    strategies: ['WriteTrafficCheck', 'circleAdmin'],
  },
  payload: {
    maxBytes: 307200, // 300 Kb is the size limit.
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data',
  },
  validate: {
    headers: Headers,
    params: Joi.object({
      circleId: Id.required(),
    }),
    payload: Joi.object({
      file: File,
    }),
  },
  handler,
};
