/** @format */

import Joi from 'joi';

import { Id, FileName } from '../../mayash-common/schema';
import storage from '../../mayash-storage';

const { NODE_ENV } = process.env;

const ROOT = NODE_ENV === 'development' ? 'dev/' : '';

export default {
  tags: ['api', 'files', 'circles'],
  description: 'Retrieve an image from the database',
  notes: 'If successful, it returns a success code with message and the image',
  auth: {
    mode: 'required',
    strategies: ['ReadTrafficCheck', 'visitor'],
  },
  validate: {
    params: Joi.object({
      circleId: Id.required(),
      fileName: FileName.required(),
    }),
  },
  handler: (request, reply) => {
    const { circleId, fileName } = request.params;

    const file = storage.file(`${ROOT}files/elements/${circleId}/${fileName}`);

    file
      .createReadStream()
      .on('data', () => {})
      .on('response', (response) => reply(response));
  },
};
