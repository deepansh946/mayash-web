/**
 *   This file should build and give a devServer to build front-end things.
 *
 *   Done:
 *     1. webpack is building files on server start.
 *     2. webpack dev server is running perfectly.
 *
 *   TODO:
 *     1. configure is to build only once on server start.
 *     2. pass webpack configuration as an 'options' on this hapi plugin.
 *
 * @format
 */
/* eslint-disable import/no-extraneous-dependencies  */

import path from 'path';
import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import DuplicatePackage from 'duplicate-package-checker-webpack-plugin';

const HtmlWebpackPlugin = require('html-webpack-plugin');

const { IP, PORT, NODE_ENV } = process.env;

const webpackConfig = {
  entry: {
    app: [
      'babel-polyfill',
      'react-hot-loader/patch',
      `webpack-dev-server/client?http://localhost:${parseInt(PORT)}/`,
      'webpack/hot/only-dev-server',
      path.resolve(__dirname, '..', '..', 'client/index.js'),
    ],
    vendor: [
      'react',
      'react-dom',
      'react-router',
      'react-router-dom',
      'prop-types',
      'react-redux',
      'redux',
      'redux-thunk',
      'classnames',
      'draft-js',
      'isomorphic-fetch',
      'jss',
      'jss-preset-default',
      'jss-theme-reactor',
      'react-loadable',
    ],
  },
  output: {
    path: path.resolve(__dirname, '..', '..', '..', 'public', 'build'),
    publicPath: '/public/build/',
    pathinfo: true,
    // filename: 'index.js',
    filename: '[name].js',
    chunkFilename: '[name].bundle.js',
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /(\.js)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: ['env', 'stage-0', 'react'],
            plugins: ['transform-runtime'],
            cacheDirectory: true,
          },
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),

    // prints more readable module names in the browser console on HMR updates
    new webpack.NamedModulesPlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      minChunks: Infinity,
      name: 'vendor',
      // filename: 'vendor.[chunkhash].js',
      filename: 'vendor.js',
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'meta',
      chunks: ['vendor'],
      filename: 'meta.js',
      // filename: 'meta.[hash].js',
    }),

    // enable HMR globally
    new webpack.HotModuleReplacementPlugin(),

    new HtmlWebpackPlugin({
      template: path.resolve(
        __dirname,
        '..',
        '..',
        '..',
        'templates/index.hbs',
      ),
      inject: 'body',
    }),

    new BundleAnalyzerPlugin({
      analyzerHost: '0.0.0.0',
      analyzerPort: 5051,
    }),

    new DuplicatePackage({
      // Also show module that is requiring each duplicate package (default: false)
      verbose: true,
      // Emit errors instead of warnings (default: false)
      emitError: false,
      // Show help message if duplicate packages are found (default: true)
      showHelp: false,
      // Warn also if major versions differ (default: true)
      strict: true,
      /**
       * Exclude instances of packages from the results.
       * If all instances of a package are excluded, or all instances except one,
       * then the package is no longer considered duplicated and won't be emitted as a warning/error.
       * @param {Object} instance
       * @param {string} instance.name The name of the package
       * @param {string} instance.version The version of the package
       * @param {string} instance.path Absolute path to the package
       * @param {?string} instance.issuer Absolute path to the module that requested the package
       * @returns {boolean} true to exclude the instance, false otherwise
       */
      // exclude(instance) {
      //   return instance.name === 'fbjs';
      // },
    }),
  ],
};

const register = (server, options, next) => {
  // This will compile and build all the files from webpackConfig and put it in
  // public/build folder.
  webpack(webpackConfig, (err, stats) => {
    if (err) {
      console.error(err);
    } else {
      console.log('webpack bundle build complete.');
    }
  });

  // Webpack return a  compiler when we pass webpack configuration.
  const compiler = webpack(webpackConfig);

  // Webpack dev server return a server which is used for
  // front-end development.
  const devServer = new WebpackDevServer(compiler, {
    hot: true,

    // if this option is 'true', it will not let any updates fron webpack to
    // print on console.
    // quiet: true,
    stats: { colors: true },

    // This option will proxy all other requests to hapi server.
    proxy: {
      '*': `http://localhost:${parseInt(PORT, 10) - 1}`,
    },
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, '..', '..', '..', 'public', 'build'),
    publicPath: '/public/build/',
  });

  devServer.listen(parseInt(PORT, 10), IP, () => {
    console.log(
      `Webpack dev server starting on http://${IP}:${parseInt(PORT, 10)}`,
    );
  });

  next();
};

register.attributes = {
  pkg: {
    name: 'hapi-webpack-dev-server',
    version: '0.0.1',
    description: '',
  },
};

export default register;
