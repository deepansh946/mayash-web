/**
 *
 * This file configures the google cloud storage APIs.
 * For DEVELOPMENT ENV: for dev env, as we are running server on docker
 * container, we have to pass auth credentials to connect with google cloud.
 *
 * For PRODUCTION ENV: we don't require auth credentials in app engine.
 *
 * @format
 */

import path from 'path';
import gcs from '@google-cloud/storage';

const { NODE_ENV, PROJECT_ID } = process.env;

const isPro = NODE_ENV === 'production';

const keyFilename = path.join(
  __dirname,
  '..',
  '..',
  '..',
  'secrets/mayash-web-secrets.json',
);

const option = {
  projectId: PROJECT_ID,
  keyFilename,
};

const gcsConfig = isPro ? gcs() : gcs(option);

export const BUCKET_NAME = 'mayash-web';

export const storage = gcsConfig.bucket(BUCKET_NAME);

export default storage;
