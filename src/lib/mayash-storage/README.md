# Mayash Storage

This library contains all the functions to interact with our cloud storage.

## Folder Structure

    .
    ├── README.md
    ├── config.js
    └── index.js