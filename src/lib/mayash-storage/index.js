/** @format */

import storage from './config';

export { BUCKET_NAME, storage } from './config';

export default storage;
