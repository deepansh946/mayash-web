/** @format */

import create from './create';
import Delete from './delete';
import get from './get';

const routes = [];

routes.push({
  method: 'POST',
  path: '/search/create',
  config: create,
});

routes.push({
  method: 'DELETE',
  path: '/search/delete',
  config: Delete,
});

routes.push({
  method: 'GET',
  path: '/search',
  config: get,
});

export default routes;
