/** @format */

import Joi from 'joi';

import { Headers } from '../mayash-common/schema';
import search from '../mayash-database/elasticSearch/search';

export default {
  tags: ['api', 'search'],
  description: 'Search for the specified query',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['user'],
  },
  validate: {
    headers: Headers,
    query: {
      q: Joi.string().required(),
      index: Joi.string(),
      filter: Joi.string(),
    },
  },
  async handler(request, reply) {
    try {
      const { index, q } = request.query;

      const res = await search({ index, q });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
