/** @format */

import Joi from 'joi';

import { Headers } from '../mayash-common';
import deleteIndex from '../mayash-database/elasticSearch/delete';

export default {
  tags: ['api', 'search'],
  description: 'Delete the index for search',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    payload: {
      index: Joi.string().required(),
    },
  },
  async handler(request, reply) {
    try {
      const index = request.payload;

      const res = await deleteIndex({ index });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
