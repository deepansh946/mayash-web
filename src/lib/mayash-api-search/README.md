## Mayash API Search

This library is sub part of **Mayash API** plugin.

It contains all the API routes for Search.

## Folder Structure

    .
    ├── README.md
    ├── create.js
    ├── createIndex.js
    ├── delete.js
    ├── index.js
    └── search.js