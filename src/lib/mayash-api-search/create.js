/** @format */

import Joi from 'joi';

import { Headers, Id } from '../mayash-common/schema';
import create from '../mayash-database/elasticSearch/create';

export default {
  tags: ['api', 'search'],
  description: 'Create a document for searching',
  notes: 'If successful, it returns a success code with message',
  auth: {
    mode: 'required',
    strategies: ['admin'],
  },
  validate: {
    headers: Headers,
    payload: {
      index: Joi.string().required(),
      type: Joi.string().required(),
      id: Id.required(),
      body: Joi.object().required(),
    },
  },
  async handler(request, reply) {
    try {
      const { index, type, id, body } = request.payload;

      const res = await create({ index, type, id, body });

      reply(res);
    } catch (error) {
      console.error(error);

      reply({
        statusCode: 500,
        error: 'Server Error',
      });
    }
  },
};
