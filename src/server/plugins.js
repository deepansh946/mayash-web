/**
 * This file exports an array of plugins which have to be registered for server.
 * Plugins may be build-in or made by our developers.
 *
 * @format
 */

import Inert from 'inert';
import Vision from 'vision';
import Good from 'good';
import Bell from 'bell';
import HapiAuthJwt2 from 'hapi-auth-jwt2';
import HapiSwagger from 'hapi-swagger';

import MayashDdos from '../lib/mayash-ddos';
import MayashAuth from '../lib/mayash-auth';
import MayashAPI from '../lib/mayash-api';
import MayashView from '../lib/mayash-views';

const { NODE_ENV, REACT } = process.env;

const plugins = [];

// For serving Static file/folder to client.
plugins.push({
  register: Inert,
  options: {},
});

plugins.push({
  register: Vision,
  options: {},
});

/*
 * Here 'good' plugin is added to log all the requests to console.
 * 
 * TODO: try to filter few requests, as in google app engine it constantly 
 * checks server's health, but we don't need that info too much.
 */
plugins.push({
  register: Good,
  options: {
    ops: {
      interval: 1000,
    },
    reporters: {
      console: [
        {
          module: 'good-squeeze',
          name: 'Squeeze',
          args: [{ log: '*', response: '*' }],
        },
        {
          module: 'good-console',
        },
        'stdout',
      ],
    },
  },
});

plugins.push({
  register: Bell,
  options: {},
});

plugins.push({
  register: HapiAuthJwt2,
  options: {},
});

plugins.push({
  register: HapiSwagger,
  options: {
    info: {
      title: 'Mayash Web API Documentation',
      version: '0.0.0-alpha.0',
      contact: {
        name: 'Mayash Developers',
        email: 'developers@mayash.io',
      },
    },
    grouping: 'tags',
    tags: [
      {
        name: 'api',
        description: 'APIs',
      },
      {
        name: 'auth',
        description: 'API of all auth',
      },
      {
        name: 'elements',
        description: 'API of all elements',
      },
      {
        name: 'users',
        description: 'API of all users',
      },
      {
        name: 'circles',
        description: 'API of all circles',
      },
      {
        name: 'posts',
        description: 'API of all posts',
      },
      {
        name: 'courses',
        description: 'API of all courses',
      },
      {
        name: 'course-discussion',
        description: 'Course Discussion APIs',
      },
      {
        name: 'classrooms',
        description: 'API of all classrooms',
      },
      {
        name: 'notes',
        description: 'API of all notes',
      },
      {
        name: 'bookmarks',
        description: 'API of all bookmarks',
      },
      {
        name: 'follow',
        description: 'API of follow feature',
      },
      {
        name: 'resume',
        description: 'API of the resume feature',
      },
      {
        name: 'search',
        description: 'API of search feature',
      },
      {
        name: 'courseEnroll',
        description: 'API of course enrollment',
      },
    ],
  },
});

if (
  NODE_ENV === 'development' &&
  typeof REACT !== 'undefined' &&
  REACT === '1'
) {
  /* eslint-disable */
  plugins.push({
    register: require('../lib/hapi-webpack-dev-server').default,
    options: {},
  });
  /* eslint-enable */
}

plugins.push({
  register: MayashDdos,
  options: {},
});

plugins.push({
  register: MayashAuth,
  options: {},
});

plugins.push({
  register: MayashAPI,
  routes: {
    prefix: '/api',
  },
  options: {},
});

plugins.push({
  register: MayashView,
  options: {},
});

export default plugins;
