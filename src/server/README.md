## Mayash Server

This folder contains all the code for server.

We are using Hapi.js for our server development.

As Hapi.js supports plugin features, we are developing all plugins in seperate folder "__lib__";
