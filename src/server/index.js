/**
 * This is main starting point for server. This file will start server
 * depending upon the NODE_ENV value as for dev. and pro. few
 * configurations are different.
 *
 * @format
 */

// babel-polyfill is added to support advance JS
// feature like async/await etc. in server side code.
import 'babel-polyfill';
// import Loadable from 'react-loadable';

const { NODE_ENV /* , REACT */ } = process.env;

/* eslint-disable global-require */
if (NODE_ENV === 'production') {
  module.exports = require('./server.pro');
} else {
  module.exports = require('./server.dev');
}
/* eslint-enable global-require */

// if (REACT === '1' || NODE_ENV === 'production') {
//   Loadable
//     .preloadAll()
//     .then(() => console.log('All React Async Components are loaded ' +
//      'in server.'));
// }
