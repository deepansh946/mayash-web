/**
 * This is the main server file to run 'mayash-web' server.
 * Node.js cluster feature is used here to extent it's performance.
 *
 * @format
 */

import { Server } from 'hapi';
import Hoek from 'hoek';
import Handlebars from 'handlebars';
import cluster from 'cluster';
import { cpus } from 'os';

import { serverConfig } from './config';
import plugins from './plugins';

// if cluster is 'master', it will create workers which depends on the number of
// cpu core in host machine.
// if 'worker' is online, it will display message with worker's process id.
// if 'worker' stops or exit, it will log error message with that worker
// and will start a new worker.
if (cluster.isMaster) {
  const cores = cpus().length;
  const numWorkers = cores < 2 ? 2 : cores;

  console.log(`Master cluster setting up ${numWorkers} workers...`);

  for (let i = 0; i < numWorkers; i += 1) {
    cluster.fork();
  }

  cluster.on('online', (worker) => {
    console.log(`Worker ${worker.process.pid} is online`);
  });

  cluster.on('exit', (worker, code, signal) => {
    console.error(
      `Worker ${
        worker.process.pid
      } died with code: ${code}, and signal: ${signal}`,
    );
    console.log('Starting a new worker');
    cluster.fork();
  });
} else {
  // For every master or worker, start new Hapi server.
  const server = new Server();
  server.connection(serverConfig);

  server.register(plugins, (error1) => {
    if (error1) {
      throw error1;
    }
    console.log('All plugins are added.');

    server.views({
      engines: {
        html: Handlebars,
      },
      relativeTo: `${__dirname}/../../`,
      path: 'public/build/',
    });

    server.start((error2) => {
      Hoek.assert(!error2, error2);

      console.log(`Hapi server started @ ${server.info.uri}`);
    });
  });
}
