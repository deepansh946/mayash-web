/**
 * Destructure all environment variables.
 *
 * @format
 */

const { IP, PORT, NODE_ENV, REACT } = process.env;

/**
 * Server variable contains two keys
 * 'host' for host IP name of server.
 * 'port' to listion request from client.
 * @type {Object}
 */
export const serverConfig = {
  host: IP || '0.0.0.0',

  /* eslint-disable */
  // If 'IP' env variable is not given, 5000 is default value of port.
  // if 'IP' is given,
  //      if REACT env is present means you are working on front-end.
  //      if REACT env is not preset means you are working on only back-end.
  port: NODE_ENV === 'production' 
          ? PORT 
          : ( PORT !== 'undefined' 
                ? (REACT === '1' ? PORT - 1 : PORT) 
                : 5000 
            ),
  /* eslint-enable */
};

export default {
  serverConfig,
};
