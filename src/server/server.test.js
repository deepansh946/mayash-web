/**
 * This file will test server is running successfully.
 *
 * @format
 */
/* eslint-disable no-undef */

import server from './server.dev';

describe('Server', () => {
  beforeAll((done) => {
    server.on('start', () => {
      done();
    });
    server.start();
  });

  afterAll((done) => {
    server.on('stop', () => {
      done();
    });
    server.stop();
  });

  test('test server', (done) => {
    expect.assertions(1);

    server.inject('/test', (res) => {
      expect(res.statusCode).toBe(200);

      done();
    });
  });
});
