/**
 * This is the main server file to run 'mayash-web' server.
 * Node.js cluster feature is used here to extent it's performance.
 *
 * @format
 */

import { Server } from 'hapi';
import Hoek from 'hoek';
import Handlebars from 'handlebars';

import { serverConfig } from './config';
import plugins from './plugins';

const { NODE_ENV } = process.env;

const server = new Server();
server.connection(serverConfig);

server.register(plugins, (error1) => {
  if (error1) {
    throw error1;
  }
  console.log('All plugins are added.');

  server.views({
    engines: {
      html: Handlebars,
    },
    relativeTo: `${__dirname}/../../`,
    path: 'public/build/',
  });

  if (NODE_ENV !== 'test') {
    server.start((error2) => {
      Hoek.assert(!error2, error2);

      console.log(`Hapi server started @ ${server.info.uri}`);
    });
  }
});

export default server;
