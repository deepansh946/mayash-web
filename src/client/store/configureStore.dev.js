/**
 * Store configuration for development environement.
 *
 * @format
 */
/* eslint import/no-extraneous-dependencies: "off" */

import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const middleware = [thunk];

const composeEnhancers = composeWithDevTools({
  // Specify name here, actionsBlacklist, actionsCreators and
  // other options if needed
});

const enhancer = composeEnhancers(
  applyMiddleware(...middleware),
  // other store enhancers if any
);

export default function (initialState) {
  const store = createStore(rootReducer, initialState, enhancer);

  if (module.hot) {
    /* eslint-disable */
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers').default),
    );
    /* eslint-enable */
  }

  return store;
}
