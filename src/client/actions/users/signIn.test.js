/**
 *
 * @format
 */
/* eslint-disable no-undef */

import { SIGN_IN } from '../../constants/users';

import signIn from './signIn';

describe('Sign In action', () => {
  test('Sign In Action Test', () => {
    const payload = {
      id: 12345,
      username: 'hbarve1',
      token: 'head.body.tail',
    };

    expect(signIn(payload)).toMatchObject({
      type: SIGN_IN,
      payload,
    });
  });
});
