/**
 * @format
 * @flow
 */

import { SIGN_OUT } from '../../constants/users';

type Action = {
  type: string,
};

export default (): Action => ({ type: SIGN_OUT });
