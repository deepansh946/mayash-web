/**
 * @format
 */
/* eslint-disable no-undef */

import { SIGN_OUT } from '../../constants/users';

import signOut from './signOut';

describe('Sign In action', () => {
  test('Sign In Action Test', () => {
    expect(signOut()).toMatchObject({ type: SIGN_OUT });
  });
});
