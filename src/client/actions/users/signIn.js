/**
 * @format
 * @flow
 */

import { SIGN_IN } from '../../constants/users';

type Payload = {
  id: number,
  username: string,
  token: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * Sign In action
 * @function signIn
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.username -
 * @param {string} payload.token -
 *
 * @return {Object}
 */
export default (payload: Payload): Action => ({ type: SIGN_IN, payload });
