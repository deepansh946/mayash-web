/**
 * @format
 * @flow
 */

import {
  USER_GET_START,
  USER_GET_ERROR,
  USER_GET_SUCCESS,
} from '../../constants/users';
import apiUserGet from '../../api/users/get';

/** ************************************************************************ */

type getStartPayload = {
  userId: number,
};

type getStartAction = {
  type: string,
  payload: getStartPayload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.userId
 * @return {Object}
 */
const getUserStart = (payload: getStartPayload): getStartAction => ({
  type: USER_GET_START,
  payload,
});

/** ************************************************************************ */

type getSuccessPayload = {
  statusCode: number,
  userId: number,
  username: string,
  name: string,
  email: string,
};

type getSuccessAction = {
  type: string,
  payload: getSuccessPayload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.userId
 * @param {string} payload.username
 * @param {string} payload.name
 * @param {string} payload.email
 *
 * @return {Object}
 */
const getUserSuccess = (payload: getSuccessPayload): getSuccessAction => ({
  type: USER_GET_SUCCESS,
  payload,
});

/** ************************************************************************ */

type getErrorPayload = {
  userId: number,
  statusCode: number,
  error: string,
};

type getErrorAction = {
  type: string,
  payload: getErrorPayload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.userId
 * @param {number} payload.statusCode
 * @param {string} payload.error
 *
 * @returns {Object}
 */
const getUserError = (payload: getErrorPayload): getErrorAction => ({
  type: USER_GET_ERROR,
  payload,
});

/** ************************************************************************ */

type getPayload = {
  token: string,
  userId: number,
};

// @todo: update action type here.
type Dispatch = (action: {
  type: string,
  payload: any,
}) => void;

/**
 * This action will get User by userId
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 */
const get = ({ token, userId }: getPayload) => (dispatch: Dispatch) => {
  dispatch(getUserStart({ userId }));

  apiUserGet({ token, userId })
    .then(({ statusCode, error, payload }) => {
      if (statusCode === 200) {
        dispatch(getUserSuccess({ userId, statusCode, ...payload }));
        return;
      }

      dispatch(getUserError({ userId, statusCode, error }));
    })
    .catch(({ statusCode, error }) => {
      dispatch(getUserError({ userId, statusCode, error }));
    });
};

export default get;
