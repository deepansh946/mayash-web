/**
 * @format
 * @flow
 */

import USER_UPDATE from '../../constants/users/update';

type Payload = {
  id: number,
  username?: string,
  name?: string,
  avatar?: string,
  cover?: string,
  resume?: any,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * User update action.
 * User can update their info in redux store by it's 'id' only.
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.username -
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @param {Object} payload.resume -
 *
 * @returns {Object}
 */
const update = (payload: Payload): Action => ({ type: USER_UPDATE, payload });

export default update;
