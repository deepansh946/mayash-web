/**
 * @format
 * @flow
 */

import { USER_CIRCLES_GET } from '../../../constants/users/circles';

type Payload = {
  id: number,
  circles: Array<{
    id: number,
  }>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * get all user's circles action
 * @function getAll
 * @param {Object} payload -
 * @param {Object} payload.id -
 * @param {Object[]} payload.circles -
 * @param {Object} payload[].circles -
 * @param {number} payload[].circles.id -
 * @returns {Object} -
 */
const getAll = (payload: Payload): Action => ({
  type: USER_CIRCLES_GET,
  payload,
});

export default getAll;
