/**
 *
 */
/* eslint-disable no-undef */

import USER_UPDATE from '../../constants/users/update';

import update from './update';

describe('User Update Action', () => {
  test('User update action', () => {
    const payload = { id: 12345, username: 'hbarve1', name: 'Himank' };
    const action = update(payload);

    expect(action).toMatchObject({
      type: USER_UPDATE,
      payload,
    });
  });
});
