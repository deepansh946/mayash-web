/** @format */

import apiGetByUsername from '../../api/elements/getByUsername';
import {
  ELEMENT_GET_START,
  ELEMENT_GET_ERROR,
  ELEMENT_GET_SUCCESS,
} from '../../constants/elements';

/**
 *
 * @param {Object} payload
 * @param {string} payload.username
 * @return {Object}
 */
const getElementStart = (payload) => ({ type: ELEMENT_GET_START, payload });

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType - user | circle
 * @param {string} payload.circleType - only if elementType is 'circle
 * @param {string} payload.name
 * @return {Object}
 */
const getElementSuccess = (payload) => ({ type: ELEMENT_GET_SUCCESS, payload });

/**
 *
 * @param {Object} payload
 * @param {string} payload.username
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
const getElementError = (payload) => ({ type: ELEMENT_GET_ERROR, payload });

/**
 *
 * @param {Object} payload
 * @param {string} payload.token
 * @param {string} payload.username
 * @return {Object}
 */
const getElement = ({ username, token }) => (dispatch) => {
  dispatch(getElementStart({ username }));

  apiGetByUsername({ username, token })
    .then(({ statusCode, error, payload }) => {
      if (statusCode >= 300) {
        dispatch(getElementError({ username, statusCode, error }));
        return;
      }

      dispatch(getElementSuccess({ statusCode, ...payload }));
    })
    .catch(({ statusCode, error }) => {
      dispatch(getElementError({ username, statusCode, error }));
    });
};

export default getElement;
