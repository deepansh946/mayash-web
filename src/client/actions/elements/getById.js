/** @format */

import {
  ELEMENT_GET_START,
  ELEMENT_GET_ERROR,
  ELEMENT_GET_SUCCESS,
} from '../../constants/elements';

import apiGetById from '../../api/elements/getById';

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @return {Object}
 */
const getElementStart = (payload) => ({ type: ELEMENT_GET_START, payload });

/**
 *
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id
 * @param {string} payload.username
 * @param {string} payload.elementType
 * @param {string} payload.circleType - only if elementType is 'circle'
 * @param {string} payload.name -
 * @param {string} payload.avatar -
 * @param {string} payload.cover -
 * @return {Object}
 */
const getElementSuccess = (payload) => ({ type: ELEMENT_GET_SUCCESS, payload });

/**
 *
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @return {Object}
 */
const getElementError = (payload) => ({ type: ELEMENT_GET_ERROR, payload });

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.id
 */
const get = ({ token, id }) => (dispatch) => {
  dispatch(getElementStart({ id }));

  apiGetById({ token, id })
    .then(({ statusCode, error, payload }) => {
      if (statusCode >= 300) {
        dispatch(getElementError({ id, statusCode, error }));

        return;
      }

      dispatch(getElementSuccess({ id, statusCode, ...payload }));
    })
    .catch(({ statusCode, error }) => {
      dispatch(getElementError({ id, statusCode, error }));
    });
};

export default get;
