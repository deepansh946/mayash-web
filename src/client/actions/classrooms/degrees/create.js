/**
 * @format
 * @flow
 */
import { DEGREE_CREATE } from '../../../constants/classroom';

/**
 *
 * @param {Object} payload
 * @param {string} payload.title
 */
const create = (payload) => ({
  type: DEGREE_CREATE,
  payload,
});

export default create;
