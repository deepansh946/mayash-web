/**
 * @format
 * @flow
 */

import { DEPARTMENT_CREATE } from '../../../constants/classroom';

/**
 *
 * @param {Object} payload
 * @param {string} payload.deptId
 * @param {string} payload.title
 * @returns {Object}
 */
const create = (payload) => ({
  type: DEPARTMENT_CREATE,
  payload,
});

export default create;
