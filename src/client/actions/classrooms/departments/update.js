import { DEPARTMENT_UPDATE } from '../../../constants/classroom';

/**
 *
 * @param {Object} payload
 * @param {string} payload.title
 * @param {string} payload.description
 * @returns {Object}
 */
const update = (payload) => ({
  type: DEPARTMENT_UPDATE,
  payload,
});

export default update;
