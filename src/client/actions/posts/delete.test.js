/**
 * @format
 */
/* eslint-disable no-undef */

import { POST_DELETE } from '../../constants/posts';

import _delete from './delete';

describe('Post Delete Action', () => {
  test('Post Delete Action', () => {
    const payload = {
      postId: 12345,
      authorId: 12345,
    };

    expect(_delete(payload)).toMatchObject({
      type: POST_DELETE,
      payload,
    });
  });
});
