/**
 * @format
 * @flow
 */

import { POST_UPDATE } from '../../constants/posts';

type Payload = {
  postId: number,
  title?: string,
  description?: string,
  data?: any,
  cover?: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function update
 * @param {Object} payload -
 * @param {number} payload.postId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.data -
 * @param {string} payload.cover -
 *
 * @returns {Object}
 */
const update = (payload: Payload): Action => ({ type: POST_UPDATE, payload });

export default update;
