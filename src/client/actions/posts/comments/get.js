/** @format */

import { POST_COMMENT_GET } from '../../../constants/posts/comments';

/**
 *
 * @function get
 * @param {object} payload
 */
const get = (payload) => ({ type: POST_COMMENT_GET, payload });

export default get;
