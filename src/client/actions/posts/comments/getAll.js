/** @format */

import { POST_COMMENTS_GET } from '../../../constants/posts/comments';

/**
 *
 * @function getAll
 * @param {object} payload
 */
const getAll = (payload) => ({ type: POST_COMMENTS_GET, payload });

export default getAll;
