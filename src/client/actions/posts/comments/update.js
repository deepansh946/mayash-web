/** @format */

import { POST_COMMENT_UPDATE } from '../../../constants/posts/comments';

/**
 *
 * @function update
 * @param {Object} payload
 */
const update = (payload) => ({ type: POST_COMMENT_UPDATE, payload });

export default update;
