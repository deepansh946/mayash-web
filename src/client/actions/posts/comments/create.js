/** @format */

import { POST_COMMENT_CREATE } from '../../../constants/posts/comments';

/**
 * Create Action
 * @function create
 * @param {object} payload
 */
const create = (payload) => ({ type: POST_COMMENT_CREATE, payload });

export default create;
