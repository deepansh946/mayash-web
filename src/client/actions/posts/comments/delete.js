/** @format */

import { POST_COMMENT_DELETE } from '../../../constants/posts/comments';

/**
 *
 * @function deleteNote
 * @param {object} payload
 */
const deleteComment = (payload) => ({ type: POST_COMMENT_DELETE, payload });

export default deleteComment;
