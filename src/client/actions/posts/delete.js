/**
 * @format
 * @flow
 */

import { POST_DELETE } from '../../constants/posts';

type Payload = {
  postId: number,
  authorId: number,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function deletePost
 * @param {Object} payload
 * @param {number} payload.postId
 * @returns {Object}
 */
const deletePost = (payload: Payload): Action => ({
  type: POST_DELETE,
  payload,
});

export default deletePost;
