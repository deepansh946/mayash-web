/**
 * @format
 * @flow
 */

import { POSTS_GET } from '../../constants/posts';

type Post = {
  postId: number,
  authorId: number,
  title: string,
  description?: string,
  data?: any,
  timestamp: string,
};

type Payload = {
  statusCode: number,
  id: number,
  posts: Array<Post>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will update 'posts' store
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id - element id of which all these posts belong
 * @param {Object[]} payload.posts
 * @param {Object} payload.posts[]
 * @param {number} payload.posts[].postId
 * @param {number} payload.posts[].authorId
 * @param {string} payload.posts[].title
 * @param {string} payload.posts[].description
 * @param {Object} payload.posts[].data
 * @param {string} payload.posts[].timestamp
 *
 * @returns {Object}
 */
const getAll = (payload: Payload): Action => ({ type: POSTS_GET, payload });

export default getAll;
