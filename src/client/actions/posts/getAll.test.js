/**
 * @format
 */
/* eslint-disable no-undef */

import { POSTS_GET } from '../../constants/posts';

import getAll from './getAll';

describe('Get All Posts by id', () => {
  test('Post Get All', () => {
    const payload = {
      id: 12345,
    };

    expect(getAll(payload)).toMatchObject({
      type: POSTS_GET,
      payload,
    });
  });
});
