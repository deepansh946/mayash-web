/** @format */

import { POST_REACTION_CREATE } from '../../../constants/posts/reactions';

/**
 * Create Action
 * @function create
 * @param {object} payload
 */
const create = (payload) => ({ type: POST_REACTION_CREATE, payload });

export default create;
