/** @format */

import { POST_REACTION_GET } from '../../../constants/posts/reactions';

/**
 *
 * @function get
 * @param {object} payload
 */
const get = (payload) => ({ type: POST_REACTION_GET, payload });

export default get;
