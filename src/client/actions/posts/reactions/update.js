/** @format */

import { POST_REACTION_UPDATE } from '../../../constants/posts/reactions';

/**
 *
 * @function update
 * @param {Object} payload
 */
const update = (payload) => ({ type: POST_REACTION_UPDATE, payload });

export default update;
