/** @format */

import { POST_REACTIONS_GET } from '../../../constants/posts/reactions';

/**
 *
 * @function getAll
 * @param {object} payload
 */
const getAll = (payload) => ({ type: POST_REACTIONS_GET, payload });

export default getAll;
