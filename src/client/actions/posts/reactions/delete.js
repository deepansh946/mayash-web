/** @format */

import { POST_REACTION_DELETE } from '../../../constants/posts/reactions';

/**
 *
 * @function deleteNote
 * @param {object} payload
 */
const deleteNote = (payload) => ({ type: POST_REACTION_DELETE, payload });

export default deleteNote;
