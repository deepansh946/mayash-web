/**
 * @format
 * @flow
 */

import { POST_CREATE } from '../../constants/posts';

type Payload = {
  id: number,
  postId: number,
  title: string,
  timestamp: string, // date and time in ISO format
  statusCode: 201,
  message: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will create post in store.
 * @function create
 * @param {Object} payload
 * @param {number} payload.id
 * @param {number} payload.postId
 * @param {string} payload.title
 * @param {string} payload.timestamp
 *
 * @param {number} payload.statusCode
 * @param {string} payload.message
 *
 * @returns {Object}
 */
const create = (payload: Payload): Action => ({ type: POST_CREATE, payload });

export default create;
