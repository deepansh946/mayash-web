/**
 * @format
 * @flow
 */

import apiPostGet from '../../api/posts/get';

import {
  POST_GET_START,
  POST_GET_SUCCESS,
  POST_GET_ERROR,
} from '../../constants/posts';

type getStartPayload = {
  postId: number,
};

type getStartAction = {
  type: string,
  payload: getStartPayload,
};

/**
 *
 * @function getPostStart
 * @param {Object} payload
 * @param {number} payload.postId
 * @returns {Object}
 */
const getPostStart = (payload: getStartPayload): getStartAction => ({
  type: POST_GET_START,
  payload,
});

/** ************************************************************************ */

type getErrorPayload = {
  postId: number,
  statusCode: number,
  error: string,
};

type getErrorAction = {
  type: string,
  payload: getErrorPayload,
};

/**
 *
 * @function
 * @param {Object} payload
 * @param {number} payload.postId
 * @param {number} payload.statusCode
 * @param {string} payload.error
 * @returns {Object}
 */
const getPostError = (payload: getErrorPayload): getErrorAction => ({
  type: POST_GET_ERROR,
  payload,
});

/** ************************************************************************ */

type getSuccessPayload = {
  postId: number,
  authorId: number,
  title: string,
  description?: string,
  data?: any, // draft.js raw content state
  timestamp: string,
};

type getSuccessAction = {
  type: string,
  payload: getSuccessPayload,
};

/**
 *
 * @function getPostSuccess
 * @param {Object} payload
 * @param {number} payload.postId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {string} payload.description
 * @param {Object} payload.data
 * @param {number} payload.statusCode
 * @param {string} payload.timestamp
 *
 * @returns {Object}
 */
const getPostSuccess = (payload: getSuccessPayload): getSuccessAction => ({
  type: POST_GET_SUCCESS,
  payload,
});

/** ************************************************************************ */

type getPayload = {
  token: string,
  postId: number,
};

type Dispatch = (action: {
  type: string,
  payload: any,
}) => void;

/**
 *
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.postId
 */
const get = ({ token, postId }: getPayload) => (dispatch: Dispatch) => {
  dispatch(getPostStart({ postId }));

  apiPostGet({ token, postId })
    .then(({ statusCode, error, payload }) => {
      if (statusCode >= 400) {
        dispatch(getPostError({ postId, statusCode, error }));
        return;
      }

      dispatch(getPostSuccess({ postId, statusCode, ...payload }));
    })
    .catch(({ statusCode, error }) => {
      dispatch(getPostError({ postId, statusCode, error }));
    });
};

export default get;
