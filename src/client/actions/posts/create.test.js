/**
 * @format
 */
/* eslint-disable no-undef */

import { POST_CREATE } from '../../constants/posts';

import create from './create';

describe('Post Create Action', () => {
  test('Post Create Action', () => {
    const payload = {
      id: 12345,
      postId: 23456,
      title: 'This is Post Test Title',
      timestamp: new Date().toISOString(),
    };

    expect(create(payload)).toMatchObject({
      type: POST_CREATE,
      payload,
    });
  });
});
