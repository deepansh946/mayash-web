/**
 * @format
 */
/* eslint-disable no-undef */

import { POST_UPDATE } from '../../constants/posts';

import update from './update';

describe('Post Update Action', () => {
  test('Post Update Action', () => {
    const payload = {
      postId: 123,
      title: 'This is updated string',
    };

    expect(update(payload)).toMatchObject({
      type: POST_UPDATE,
      payload,
    });
  });
});
