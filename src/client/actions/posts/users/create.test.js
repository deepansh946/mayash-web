/**
 * @format
 * @flow
 */
/* eslint-disable no-undef */

import { POST_CREATE } from '../../../constants/posts';

import create from './create';

describe('Post Create By User', () => {
  test('Post Create By User', () => {
    const payload = {
      postId: 1234,
      authorId: 2345,
      title: 'Post Created By User.',
      timestamp: new Date().toISOString(),

      statusCode: 201,
      message: 'Post Created Succeess',
    };

    expect(create(payload)).toMatchObject({
      type: POST_CREATE,
      payload,
    });
  });
});
