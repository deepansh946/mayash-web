/**
 * @format
 * @flow
 */

import {
  POSTS_GET_START,
  POSTS_GET_ERROR,
  POSTS_GET_SUCCESS /* POSTS_GET, */,
} from '../../../constants/posts';

import apiUserPostsGet from '../../../api/posts/users/getAll';

type StartPayload = {
  userId: number,
};

type StartAction = {
  type: string,
  payload: StartPayload,
};

/**
 *
 * @function getPostsStart
 * @param {Object} payload
 * @param {number} payload.userId
 * @returns {Object}
 */
const getPostsStart = (payload: StartPayload): StartAction => ({
  type: POSTS_GET_START,
  payload,
});

/** ************************************************************************ */

type Post = {
  postId: number,
  authorId: number,
  title: string,
  description?: string,
  data?: any,
  timestamp: string,
};

type SuccessPayload = {
  userId: number,
  statusCode: number,
  posts: Array<Post>,
  next: string,
};

type SuccessAction = {
  type: string,
  payload: SuccessPayload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.userId
 * @param {number} payload.statusCode
 * @param {Object[]} payload.posts
 * @param {Object} payload.posts[]
 * @param {number} payload.posts[].postId
 * @param {number} payload.posts[].authorId
 * @param {string} payload.posts[].title
 * @param {string} payload.posts[].description
 * @param {Object} payload.posts[].data
 * @param {string} payload.posts[].timestamp
 * @param {number} payload.next
 *
 * @returns {Object}
 */
const getPostsSuccess = (payload: SuccessPayload): SuccessAction => ({
  type: POSTS_GET_SUCCESS,
  payload,
});

/** ************************************************************************ */

type ErrorPayload = {
  userId: number,
  statusCode: number,
  error: string,
};

type ErrorAction = {
  type: string,
  payload: ErrorPayload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.userId
 * @param {number} payload.statusCode
 * @param {string} payload.error
 */
const getPostsError = (payload: ErrorPayload): ErrorAction => ({
  type: POSTS_GET_ERROR,
  payload,
});

/** ************************************************************************ */

type getPayload = {
  token: string,
  userId: number,
  next: string,
};

type Dispatch = (action: { type: string, payload: any }) => void;

/**
 * Get all posts of users.
 * @function getAll
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 */
const getAll = ({ token, userId, next }: getPayload) => (
  dispatch: Dispatch,
) => {
  dispatch(getPostsStart({ userId }));

  apiUserPostsGet({ token, userId, next })
    .then(({ statusCode, error, payload, next: resNext }) => {
      if (statusCode >= 300) {
        dispatch(getPostsError({ userId, statusCode, error }));
        return;
      }

      dispatch(
        getPostsSuccess({
          userId,
          statusCode,
          posts: payload,
          next: resNext,
        }),
      );
    })
    .catch(({ statusCode, error }) => {
      dispatch(getPostsError({ userId, statusCode, error }));
    });
};

export default getAll;
