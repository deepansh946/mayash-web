/**
 * @format
 * @flow
 */

import { POST_CREATE } from '../../../constants/posts';

type Payload = {
  postId: number,
  authorId: number,
  title: string,
  timestamp: string,

  statusCode: number,
  message: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will create post for user in store.
 * @function create
 * @param {Object} payload
 * @param {number} payload.postId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {string} payload.timestamp
 *
 * @param {number} payload.statusCode
 * @param {string} payload.message
 *
 * @returns {Object}
 */
const create = (payload: Payload): Action => ({ type: POST_CREATE, payload });

export default create;
