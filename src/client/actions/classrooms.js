/**
 * This is the action for all classroom
 *
 * @format
 */

import api from '../api/classrooms';
import { getCoursesStart, getCoursesError, getCoursesSuccess } from './courses';

import {
  CLASSROOM_COURSES_GET_START,
  CLASSROOM_COURSES_GET_ERROR,
  CLASSROOM_COURSES_GET_SUCCESS,
  DEPARTMENT_ADD,
  DEGREE_ADD,
  SEMESTER_ADD,
  CLASS_ADD,
} from '../constants/classroom';

/**
 *
 * @param {object} payload : information about action (eg. course, courseId ..)
 * It return an action(object) : action contain two thing
 *                            1. Type of action
 *                            2. Payload of information
 */
const getClassroomCoursesStart = (payload) => ({
  type: CLASSROOM_COURSES_GET_START,
  payload,
});
const getClassroomCoursesSuccess = (payload) => ({
  type: CLASSROOM_COURSES_GET_SUCCESS,
  payload,
});
const getClassroomCoursesError = (payload) => ({
  type: CLASSROOM_COURSES_GET_ERROR,
  payload,
});
export const getClassroomCourses = ({ id, token }) => (dispatch) => {
  dispatch(getClassroomCoursesStart({ id }));
  dispatch(getCoursesStart({ id }));

  api.getClassroomCourses({ id, token }, (json) => {
    if (json.statusCode === 200) {
      dispatch(getClassroomCoursesSuccess(json.payload));
      dispatch(getCoursesSuccess(json.payload));
    } else if (json.statusCode >= 400) {
      dispatch(getClassroomCoursesError(json));
      dispatch(getCoursesError(json));
    }
  });
};

/**
 * This is an action to add deptId and title in Classroom.
 * @param {object} payload
 * @param {string} payload.circleId
 * @param {string} payload.deptId
 * @param {string} payload.title
 * @return {object}
 */
const departmentAdd = (payload) => ({
  type: DEPARTMENT_ADD,
  payload,
});

/**
 * This is an action to add degreeId and title in Department.
 * @param {object} payload
 * @param {string} payload.degreeId
 * @param {string} payload.title
 * @return {object}
 */
const degreeAdd = (payload) => ({
  type: DEGREE_ADD,
  payload,
});

/**
 * This is an action adds semester in degree
 * @param {object} payload
 * @return {object}
 */
const semesterAdd = (payload) => ({
  type: SEMESTER_ADD,
  payload,
});

/**
 * This is an action to add courseId and profId in class.
 * @param {object} payload
 * @param {object} courseId
 * @param {object} profId
 * @return {object}
 */
const classAdd = (payload) => ({
  type: CLASS_ADD,
  payload,
});

export default {
  departmentAdd,
  degreeAdd,
  semesterAdd,
  classAdd,
};
