/** @format */

import { BOOKMARK_UPDATE } from '../../constants/bookmarks';

/**
 *
 * @function update
 * @param {Object} payload
 */
const update = (payload) => ({ type: BOOKMARK_UPDATE, payload });

export default update;
