/** @format */

import { BOOKMARKS_GET } from '../../constants/bookmarks';

/**
 *
 * @function getAll
 * @param {object} payload
 */
const getAll = (payload) => ({ type: BOOKMARKS_GET, payload });

export default getAll;
