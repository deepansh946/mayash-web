/** @format */

import { BOOKMARK_CREATE } from '../../constants/bookmarks';

/**
 * Create Action
 * @function create
 * @param {object} payload
 */
const create = (payload) => ({ type: BOOKMARK_CREATE, payload });

export default create;
