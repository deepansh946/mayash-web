/** @format */

import { BOOKMARK_GET } from '../../constants/bookmarks';

/**
 *
 * @function get
 * @param {object} payload
 */
const get = (payload) => ({ type: BOOKMARK_GET, payload });

export default get;
