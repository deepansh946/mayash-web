/** @format */

import { BOOKMARK_DELETE } from '../../constants/bookmarks';

/**
 *
 * @function deleteNote
 * @param {object} payload
 */
const deleteNote = (payload) => ({ type: BOOKMARK_DELETE, payload });

export default deleteNote;
