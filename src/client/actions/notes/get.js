/**
 * @format
 * @flow
 */

import { GET_NOTE } from '../../constants/users';

type Payload = {
  noteId: number,
};

type Return = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function get
 * @param {object} payload
 */
const get = (payload: Payload): Return => ({ type: GET_NOTE, payload });

export default get;
