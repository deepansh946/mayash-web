/**
 * @format
 * @flow
 */

import { NOTE_CREATE } from '../../constants/notes';

type Payload = {
  noteId: number,
  title: string,
};

type Return = {
  type: string,
  payload: Payload,
};

/**
 * Create Action
 * @function create
 * @param {Object} payload
 * @param {number} payload.noteId
 * @param {string} payload.title
 *
 * @returns {Object}
 */
const create = (payload: Payload): Return => ({ type: NOTE_CREATE, payload });

export default create;
