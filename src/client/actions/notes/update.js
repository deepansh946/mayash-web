/**
 * @format
 * @flow
 */

import { NOTE_UPDATE } from '../../constants/notes';

type Payload = {
  noteId: number,
  authorId: number,
  title: string,
};

type Return = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function update
 * @param {Object} payload
 */
const update = (payload: Payload): Return => ({ type: NOTE_UPDATE, payload });

export default update;
