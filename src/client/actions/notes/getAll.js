/** @format */

import { NOTES_GET } from '../../constants/notes';

type Note = {
  noteId: number,
  authorId: number,
  title: string,
  timestamp: string,
};

type Payload = {
  statusCode: number,
  notes: Array<Note>,
};

type Return = {
  type: string,
  payload: Payload,
};

/**
 * This action will update 'notes' store
 * @function getAll
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {number} payload.id - element id of which all these notes belong
 * @param {Object[]} payload.notes
 * @param {Object} payload.notes[]
 * @param {number} payload.notes[].noteId
 * @param {number} payload.notes[].authorId
 * @param {string} payload.notes[].title
 * @param {string} payload.notes[].timestamp
 *
 * @returns {Object}
 */
const getAll = (payload: Payload): Return => ({ type: NOTES_GET, payload });

export default getAll;
