/**
 * @format
 * @flow
 */

import { NOTE_DELETE } from '../../constants/notes';

type Payload = {
  noteId: number,
};

type Return = {
  type: string,
  payload: Payload,
};

/**
 * Delete Note Action.
 * @function deleteNote
 * @param {Object} payload
 * @param {number} payload.noteId
 *
 * @returns {Object}
 */
const deleteNote = (payload: Payload): Return => ({
  type: NOTE_DELETE,
  payload,
});

export default deleteNote;
