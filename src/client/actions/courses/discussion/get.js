/**
 * Get a Question of a Course Discussion.
 * @format
 * @flow
 */

import { QUESTION_GET } from '../../../constants/courses';

type Payload = {
  courseId: number,
  questionId: number,
  authorId: number,
  title: string,
  data?: any,
  timestamp: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {Object} payload.data
 * @param {string} payload.timestamp
 *
 * @returns {Object}
 */
const get = (payload: Payload): Action => ({ type: QUESTION_GET, payload });

export default get;
