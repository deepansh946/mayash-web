/**
 * Get all course discussion's questions.
 * @format
 * @flow
 */

import { QUESTIONS_GET } from '../../../constants/courses';

type Question = {
  courseId: number,
  questionId: number,
  authorId: number,
  title: string,
  data?: any,
  timestamp: string,
};

type Payload = {
  courseId: number,
  statusCode: number,
  discussion: Array<Question>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.statusCode
 * @param {Object[]} payload.discussion
 * @param {Object} payload.discussion[]
 * @param {number} payload.discussion[].courseId
 * @param {number} payload.discussion[].authorId - author of a question.
 * @param {number} payload.discussion[].questionId
 * @param {string} payload.discussion[].title
 * @param {Object} payload.discussion[].data - draft.js raw content state.
 * @param {string} payload.discussion[].timestamp
 *
 * @returns {Object}
 */
const getAll = (payload: Payload): Action => ({ type: QUESTIONS_GET, payload });

export default getAll;
