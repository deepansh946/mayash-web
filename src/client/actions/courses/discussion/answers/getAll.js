/**
 * Get all course discussion's questions.
 * @format
 * @flow
 */

import { ANSWERS_GET } from '../../../../constants/courses';

type Answer = {
  courseId: number,
  questionId: number,
  answerId: number,
  authorId: number,
  title: string,
  data?: any,
  timestamp: string,
};

type Payload = {
  courseId: number,
  questionId: number,
  statusCode: number,
  discussion: Array<Answer>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.statusCode
 * @param {Object[]} payload.answers
 * @param {Object} payload.answers[]
 * @param {number} payload.answers[].courseId
 * @param {number} payload.answers[].authorId - author of a question.
 * @param {number} payload.answers[].questionId
 * @param {number} payload.answers[].answerId
 * @param {string} payload.answers[].title
 * @param {Object} payload.answers[].data - draft.js raw content state.
 * @param {string} payload.answers[].timestamp
 *
 * @returns {Object}
 */
const getAll = (payload: Payload): Action => ({ type: ANSWERS_GET, payload });

export default getAll;
