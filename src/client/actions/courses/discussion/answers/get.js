/**
 * Get a Question of a Course Discussion.
 * @format
 * @flow
 */

import { ANSWER_GET } from '../../../../constants/courses';

type Payload = {
  courseId: number,
  questionId: number,
  answerId: number,
  authorId: number,
  title: string,
  data?: any,
  timestamp: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.questionId
 * @param {number} payload.answerId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {Object} payload.data
 * @param {string} payload.timestamp
 *
 * @returns {Object}
 */
const get = (payload: Payload): Action => ({ type: ANSWER_GET, payload });

export default get;
