/**
 * update a question in course discussion.
 * @format
 * @flow
 */

import { QUESTION_UPDATE } from '../../../../constants/courses';

type Payload = {
  authorId: number,
  courseId: number,
  questionId: number,
  title?: string,
  data?: mixed,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will update course module in store
 * @function update
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @param {string} payload.title -
 * @param {Object} payload.data -
 * @return {Object}
 */
const update = (payload: Payload): Action => ({
  type: QUESTION_UPDATE,
  payload,
});

export default update;
