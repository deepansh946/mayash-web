/**
 * Delete a question in course discussion.
 * @format
 * @flow
 */

import { QUESTION_DELETE } from '../../../../constants/courses';

type Payload = {
  authorId: number,
  courseId: number,
  questionId: number,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @return {Object}
 */
const deleteModule = (payload: Payload): Action => ({
  type: QUESTION_DELETE,
  payload,
});

export default deleteModule;
