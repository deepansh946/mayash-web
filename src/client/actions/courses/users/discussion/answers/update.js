/**
 * update a question in course discussion.
 * @format
 * @flow
 */

import { ANSWER_UPDATE } from '../../../../../constants/courses';

type Payload = {
  courseId: number,
  questionId: number,
  answerId: number,
  title?: string,
  data?: any,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will update course module in store
 * @function update
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @param {number} payload.answerId -
 * @param {string} payload.title -
 * @param {Object} payload.data -
 * @return {Object}
 */
const update = (payload: Payload): Action => ({
  type: ANSWER_UPDATE,
  payload,
});

export default update;
