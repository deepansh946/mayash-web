/**
 * Create a question in Course Discussion.
 * @format
 * @flow
 */

import { ANSWER_CREATE } from '../../../../../constants/courses';

type Payload = {
  authorId: number,
  courseId: number,
  questionId: number,
  title: string,
  timestamp: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will create an answer in discussion of course's question.
 * @function create
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId - author of a answer.
 * @param {number} payload.questionId -
 * @param {string} payload.title -
 * @param {string} payload.timestamp -
 * @returns {Object}
 */
const create = (payload: Payload): Action => ({
  type: ANSWER_CREATE,
  payload,
});

export default create;
