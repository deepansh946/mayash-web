/**
 * Delete a question in course discussion.
 * @format
 * @flow
 */

import { ANSWER_DELETE } from '../../../../../constants/courses';

type Payload = {
  courseId: number,
  questionId: number,
  answerId: number,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @param {number} payload.answerId -
 * @return {Object}
 */
const deleteAnswer = (payload: Payload): Action => ({
  type: ANSWER_DELETE,
  payload,
});

export default deleteAnswer;
