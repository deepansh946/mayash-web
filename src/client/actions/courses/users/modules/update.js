/**
 * @format
 * @flow
 */

import { MODULE_UPDATE } from '../../../../constants/courses';

type Payload = {
  authorId: number,
  courseId: number,
  moduleId: number,
  title?: string,
  data?: mixed,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will update course module in store
 * @function update
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.title -
 * @param {Object} payload.data -
 * @return {Object}
 */
const update = (payload: Payload): Action => ({ type: MODULE_UPDATE, payload });

export default update;
