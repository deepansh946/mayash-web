/**
 * @format
 * @flow
 */

import { MODULE_DELETE } from '../../../../constants/courses';

type Payload = {
  authorId: number,
  courseId: number,
  moduleId: number,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @return {Object}
 */
const deleteModule = (payload: Payload): Action => ({
  type: MODULE_DELETE,
  payload,
});

export default deleteModule;
