/**
 * @format
 * @flow
 */

import { MODULE_CREATE } from '../../../../constants/courses';

type Payload = {
  authorId: number,
  courseId: number,
  moduleId: number,
  title: string,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will create a module is store
 * @function create
 * @param {Object} payload -
 * @param {number} payload.authorId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.title -
 * @returns {Object}
 */
const create = (payload: Payload): Action => ({ type: MODULE_CREATE, payload });

export default create;
