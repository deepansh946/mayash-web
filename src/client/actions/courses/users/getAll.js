/** @format */

import apiCoursesGet from '../../../api/courses/users/getAll';

import {
  COURSES_GET_START,
  COURSES_GET_ERROR,
  COURSES_GET_SUCCESS,
} from '../../../constants/courses';

/**
 *
 * @function
 * @param {Object} payload
 */
export const getCoursesStart = (payload) => ({
  type: COURSES_GET_START,
  payload,
});

/**
 *
 * @param {Object[]} payload
 * @param {Object[]} payload
 * @param {Object} payload[]
 * @param {number} payload[].courseId
 * @param {number} payload[].authorId
 * @param {string} payload[].title
 * @param {string} payload[].description
 * @returns {Object}
 */
export const getCoursesSuccess = (payload) => ({
  type: COURSES_GET_SUCCESS,
  payload,
});

/**
 *
 * @function
 * @param {Object} payload
 * @param {number} payload.userId
 * @returns {Object}
 */
export const getCoursesError = (payload) => ({
  type: COURSES_GET_ERROR,
  payload,
});

/**
 *
 * @function
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 */
const getAll = ({ token, userId }) => (dispatch) => {
  dispatch(getCoursesStart({ userId }));

  apiCoursesGet({ userId, token })
    .then(({ statusCode, error, payload }) => {
      if (statusCode >= 300) {
        dispatch(getCoursesError({ userId, statusCode, error }));
        return;
      }

      dispatch(getCoursesSuccess(payload));
    })
    .catch(({ statusCode, error }) => {
      dispatch(getCoursesError({ userId, statusCode, error }));
    });
};

export default getAll;
