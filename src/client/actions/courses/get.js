/**
 * @format
 * @flow
 */

import apiCourseGet from '../../api/courses/get';

import {
  COURSE_GET_START,
  COURSE_GET_ERROR,
  COURSE_GET_SUCCESS,
} from '../../constants/courses';

type getStartPayload = {
  courseId: number,
};

type getStartAction = {
  type: string,
  payload: getStartPayload,
};

/**
 *
 * @function
 * @param {Object} payload
 * @param {number} payload.courseId
 * @returns {Object}
 */
const getCourseStart = (payload: getStartPayload): getStartAction => ({
  type: COURSE_GET_START,
  payload,
});

/** ************************************************************************ */

type getSuccessPayload = {
  statusCode: number,
  message: string,
  courseId: number,
  authorId: number,
  title: string,
  description?: string,
  syllabus?: mixed,
  courseModules?: Array<{
    moduleId: number,
  }>,
};

type getSuccessAction = {
  type: string,
  payload: getSuccessPayload,
};

/**
 *
 * @function
 * @param {Object} payload
 * @param {number} payload.statusCode
 * @param {string} payload.message
 * @param {number} payload.courseId
 * @param {number} payload.authorId
 * @param {string} payload.title
 * @param {string} payload.description
 * @param {Object} payload.syllabus
 * @param {Object[]} payload.courseModules
 * @param {Object} payload.courseModules[]
 * @param {number} payload.courseModules[].moduleId
 * @returns {Object}
 */
const getCourseSuccess = (payload: getSuccessPayload): getSuccessAction => ({
  type: COURSE_GET_SUCCESS,
  payload,
});

/** ************************************************************************ */

type getErrorPayload = {
  courseId: number,
  statusCode: number,
  error: string,
};

type getErrorAction = {
  type: string,
  payload: getErrorPayload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.statusCode
 * @param {string} payload.error
 */
const getCourseError = (payload: getErrorPayload): getErrorAction => ({
  type: COURSE_GET_ERROR,
  payload,
});

/** ************************************************************************ */

type getPayload = {
  courseId: number,
  token?: string,
};

type Dispatch = (action: mixed) => mixed;

/**
 * @function get
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {string} payload.token
 * @returns {Object}
 */
const get = ({ courseId, token }: getPayload) => (dispatch: Dispatch) => {
  dispatch(getCourseStart({ courseId: parseInt(courseId, 10) }));

  apiCourseGet({ courseId, token })
    .then(({ statusCode, error, message, payload }) => {
      if (statusCode >= 300) {
        dispatch(getCourseError({ courseId, statusCode, error }));
        return;
      }

      dispatch(
        getCourseSuccess({
          courseId,
          statusCode,
          message,
          ...payload,
        }),
      );
    })
    .catch(({ statusCode, error }) => {
      dispatch(getCourseError({ courseId, statusCode, error }));
    });
};

export default get;
