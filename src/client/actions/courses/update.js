/**
 * @format
 * @flow
 */

import { COURSE_UPDATE } from '../../constants/courses';

type Payload = {
  courseId: number,
  authorId: number,
  cover?: string,
  title?: string,
  description?: string,
  syllabus?: mixed,
  courseModules?: Array<{
    moduleId: number,
  }>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus -
 * @param {Object[]} payload.courseModules -
 * @param {Object} payload.courseModules[] -
 * @param {number} payload.courseModules[].moduleId -
 *
 * @return {Object} -
 */
const update = (payload: Payload): Action => ({ type: COURSE_UPDATE, payload });

export default update;
