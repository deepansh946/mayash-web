/**
 * @format
 * @flow
 */

import { COURSE_CREATE } from '../../constants/courses';

type Payload = {|
  authorId: number,
  courseId: number,
  title: string,
  statusCode: number,
  timestamp: string,
|};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * create action will add newly created course to store.
 * @param {Object} payload -
 * @param {number} payload.statusCode -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @param {string} payload.title -
 * @param {string} payload.timestamp -
 *
 * @return {Object} -
 */
const create = (payload: Payload): Action => ({ type: COURSE_CREATE, payload });

export default create;
