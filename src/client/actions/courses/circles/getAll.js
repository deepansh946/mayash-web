/** @format */

import apiCircleCourses from '../../../api/courses/circles/getAll';

import {
  COURSES_GET_START,
  COURSES_GET_ERROR,
  COURSES_GET_SUCCESS,
} from '../../../constants/courses';

/**
 *
 * @function
 * @param {Object} payload
 */
export const getCoursesStart = (payload) => ({
  type: COURSES_GET_START,
  payload,
});

/**
 *
 * @param {Object[]} payload
 * @param {Object[]} payload
 * @param {Object} payload[]
 * @param {number} payload[].courseId
 * @param {number} payload[].authorId
 * @param {string} payload[].title
 * @param {string} payload[].description
 * @returns {Object}
 */
export const getCoursesSuccess = (payload) => ({
  type: COURSES_GET_SUCCESS,
  payload,
});

/**
 *
 * @function
 * @param {Object} payload
 * @param {number} payload.id
 * @returns {Object}
 */
export const getCoursesError = (payload) => ({
  type: COURSES_GET_ERROR,
  payload,
});

/**
 *
 * @function
 * @param {Object} payload
 * @param {number} payload.id
 * @param {string} payload.token
 */
const getCourses = ({ id, token }) => (dispatch) => {
  dispatch(getCoursesStart({ id }));

  apiCircleCourses({ id, token })
    .then(({ statusCode, error, payload }) => {
      if (statusCode >= 300) {
        dispatch(getCoursesError({ id, statusCode, error }));
        return;
      }
      dispatch(getCoursesSuccess(payload));
    })
    .catch(({ statusCode, error }) => {
      dispatch(getCoursesError({ id, statusCode, error }));
    });
};

export default getCourses;
