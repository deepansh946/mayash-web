/**
 * @format
 * @flow
 */

import { MODULE_GET } from '../../../constants/courses';

type Payload = {
  courseId: number,
  moduleId: number,
  title: string,
  data?: mixed,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function get
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.moduleId
 * @param {string} payload.title
 * @param {Object} payload.data
 *
 * @returns {Object}
 */
const get = (payload: Payload): Action => ({ type: MODULE_GET, payload });

export default get;
