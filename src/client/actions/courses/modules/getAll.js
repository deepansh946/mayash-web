/**
 * @format
 * @flow
 */

import { MODULES_GET } from '../../../constants/courses';

type Module = {
  courseId: number,
  moduleId: number,
  title: string,
  data?: mixed,
};

type Payload = {
  courseId: number,
  statusCode: number,
  courseModules: Array<Module>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.statusCode
 * @param {Object[]} payload.courseModules
 * @param {Object} payload.courseModules[]
 * @param {number} payload.courseModules[].moduleId
 * @param {number} payload.courseModules[].courseId
 * @param {string} payload.courseModules[].title
 * @param {Object} payload.courseModules[].data - draft.js raw content state.
 *
 * @returns {Object}
 */
const getAll = (payload: Payload): Action => ({ type: MODULES_GET, payload });

export default getAll;
