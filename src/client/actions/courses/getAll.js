/**
 * @format
 * @flow
 */

import { COURSES_GET } from '../../constants/courses';

type Course = {
  courseId: number,
  authorId: number,
  title: string,
  description?: string,
  syllabus?: mixed,
  courseModules?: Array<{
    moduleId: number,
  }>,
  timestamp: string,
};

type Payload = {
  statusCode: number,
  courses: Array<Course>,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 * This action will update courses reducer
 * @function getAll
 * @param {Object} payload
 * @param {Object} payload.statusCode -
 * @param {Object[]} payload.courses
 * @param {Object} payload.courses[]
 * @param {number} payload.courses[].courseId
 * @param {number} payload.courses[].authorId
 * @param {string} payload.courses[].title
 * @param {string} payload.courses[].description
 * @param {Object} payload.courses[].syllabus
 * @param {Object[]} payload.courses[].courseModules
 * @param {Object} payload.courses[].courseModules[]
 * @param {number} payload.courses[].courseModules[].moduleId
 * @param {string} payload.courses[].timestamp
 *
 * @returns {Object}
 */
export const getAll = (payload: Payload): Action => ({
  type: COURSES_GET,
  payload,
});

export default getAll;
