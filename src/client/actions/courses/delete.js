/**
 * @format
 * @flow
 */

import { COURSE_DELETE } from '../../constants/courses';

type Payload = {
  courseId: number,
  authorId: number,
};

type Action = {
  type: string,
  payload: Payload,
};

/**
 *
 * @function deleteCourse
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.authorId -
 * @return {Object} -
 */
export default (payload: Payload): Action => ({ type: COURSE_DELETE, payload });
