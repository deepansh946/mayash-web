/** @format */

import { CIRCLE_MEMBERS_GET } from '../../../constants/circle/member';

/**
 *
 * @function
 * @param {Object[]} payload -
 * @param {Object} payload[] -
 * @param {number} payload[].id -
 * @param {string} payload[].username -
 * @param {string} payload[].elementType - 'user' or 'circle'
 * @param {string} payload[].circleType - if elementType is 'circle' then
 * 'edu', 'org', 'field', 'location', 'social' else this key not exists.
 * @param {string} payload[].name -
 * @param {string} payload[].avatar -
 * @returns {Object} - redux action type.
 */
const getAll = (payload) => ({ type: CIRCLE_MEMBERS_GET, payload });

export default getAll;
