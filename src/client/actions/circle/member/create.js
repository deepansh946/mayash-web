/** @format */

import { CIRCLE_JOIN } from '../../../constants/circle/member';

/**
 *
 * @function
 * @param {Object[]} payload -
 * @param {Object} payload[] -
 * @param {number} payload[].memberId -
 * @param {string} payload[].circleId -
 * @returns {Object} - redux action type.
 */
const create = (payload) => ({ type: CIRCLE_JOIN, payload });

export default create;
