/**
 *
 * @format
 */

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';

import configureStore from './store/configureStore';
import App from './App';

/* eslint-disable */
// initial state is comming in string with encodedURIComponent form so 
// first we have to decodeURIComponent and then parse string to convert to JavaScript object.
const initialState = JSON.parse(decodeURIComponent(window.__INITIAL_STATE__));
// delete window.__INITIAL_STATE__;
/* eslint-disable */

const store = configureStore(initialState);


const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <BrowserRouter>
          <Component />
        </BrowserRouter>
      </Provider>
    </AppContainer>,
    document.getElementById('app'),
  );
};

render(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    /* eslint-disable */
    const App = require('./App').default;
    render(App);
    /* eslint-enable */
  });
}
