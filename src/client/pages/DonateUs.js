/**
 * This component represents Donation page for mayash
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';

import Card, { CardHeader, CardMedia, CardContent } from 'material-ui/Card';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  cardHeader: {
    textAlign: 'center',
    padding: '3em',
  },
  media: {
    height: 400,
    width: 400,
    [theme.breakpoints.down('md')]: {
      height: 300,
      width: 300,
    },
  },
  cardContent: {
    paddingLeft: '8em',
    textAlign: 'justify',
    paddingRight: '8em',
    [theme.breakpoints.down('md')]: {
      paddingLeft: '2em',
      paddingRight: '2em',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '10px',
    },
  },
});

class DonateUs extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card>
          <CardHeader title="Support us" className={classes.cardHeader} />
          <CardContent className={classes.cardContent}>
            <Typography variant="headline" type="headline">
              To support the Hosting, SSL and Domain costs we are not inclined
              towards charging the users of the website for any of the service
              right now or putting Ads. As we scale with tremendous feature we
              cannot rely on free services and we have already migrated to
              standard services. Any amount would be highly appreciated :)
            </Typography>
          </CardContent>
          <Grid
            container
            spacing={4}
            justify="center"
            alignItems="center"
            alignContent="center"
          >
            <Grid item xs={12} sm={12} md={5} lg={4} xl={4}>
              <CardHeader title="Direct Transfer TO :" />
              <CardContent>
                <Typography type="headline">
                  Account No. : 3748 5885 985
                </Typography>
                <Typography type="headline">
                  IFSC Code : SBIN0000421{' '}
                </Typography>
                <Typography type="headline">
                  Contact No.: 8770693644{' '}
                </Typography>
                <Typography type="headline">UPI : 87706933644@upi </Typography>
              </CardContent>
            </Grid>
            <Grid item xs={12} sm={12} md={5} lg={4} xl={4}>
              <CardHeader title="Donate Through UPI :" />
              <CardMedia
                className={classes.media}
                image="https://storage.googleapis.com/mayash-web/drive/donate-upi.jpg"
                title="UPI : 87706933644@upi "
              />
            </Grid>
          </Grid>
        </Card>
        <NavigationButtonNext to={'/developers-training'} />
      </div>
    );
  }
}

DonateUs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(DonateUs);
