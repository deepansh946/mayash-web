/**
 * This component contains pricing feature of mayash
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styleSheet = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexGrow: 1,
    alignItems: 'center',
  },
  flexChild: {
    flex: 'flex-grow',
    margin: 'auto',
  },
  cardMediaImage: {
    width: '100%',
    height: 'auto',
  },
  title: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 30,
    },
  },
  subheader: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 16,
    },
  },
  card: {
    height: '490px',
    width: '350px',
    margin: theme.spacing.unit,
  },
  cardHeaderTop: {
    textAlign: 'center',
    backgroundColor: theme.palette.primary[400],
    height: '67px',
  },
  cardHeader: {
    textAlign: 'right',
    backgroundColor: theme.palette.primary[500],
    height: '67px',
    '&:hover': {
      textShadow: '1px 0 0 currentColor',
    },
  },
  pricingTitle: {
    height: '20vh',
    backgroundImage:
      'url("http://ginva.com/wp-content/uploads/2016/08/img_57a000bf9846c.png")',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
    borderRightWidth: 100,
    borderTopWidth: 100,
    borderRightColor: 'transparent',
    borderTopColor: '#ff0066',
    marginTop: '-100px',
    '&:hover': {
      textShadow: '1px 0 0 currentColor',
    },
  },
  header: {
    transform: 'rotate(-45deg)',
    marginTop: '-60px',
    marginLeft: '23px',
    color: theme.palette.getContrastText(theme.palette.primary[700]),
  },
  ul: {
    width: '320px',
    marginTop: '8px',
    marginLeft: '-40px',
  },

  li: {
    position: 'relative',
    display: 'block',
    padding: '10px',
    marginBottom: '5px',
    textAlign: 'center',
    transition: '0.2s',
    '&:hover': {
      transform: 'scale(1.03)',
      boxShadow: '3px 3px 5px 6px #ccc',
    },
  },
  paragraph: {
    textAlign: 'center',
  },
});

const Pricing = ({ classes }) => (
  <Grid container spacing={0} justify="center" className={classes.root}>
    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
      <Card raised>
        <CardHeader
          title={
            <Typography className={classes.title} type="display3">
              PRICING
            </Typography>
          }
          subheader={
            <Typography className={classes.subheader} type="headline">
              Our plans are affordable to everyone
            </Typography>
          }
          className={classnames(classes.cardHeaderTop, classes.pricingTitle)}
        />
      </Card>
    </Grid>
    <div className={classes.flexChild}>
      <Card raised className={classes.card}>
        <CardHeader
          title={'Basic Plan'}
          className={classes.cardHeader}
          classes={{
            title: classes.title,
          }}
        />
        <div className={classes.triangle}>
          <div className={classes.header}>FREE</div>
        </div>
        <CardContent>
          <Typography type="title" component="div" align="center">
            Features
          </Typography>
          <Typography type="body1" component="div">
            <ul className={classes.ul}>
              <li className={classes.li}>Create posts</li>
              <li className={classes.li}>
                Can View all free courses available.
              </li>
              <li className={classes.li}>Can view others profile.</li>
              <li className={classes.li}>
                Basic Details of Educational Institutes and Organisations.
              </li>
              <li className={classes.li}>Can create and share your CV.</li>
            </ul>
          </Typography>
        </CardContent>
      </Card>
    </div>
    <div className={classes.flexChild}>
      <Card raised className={classes.card}>
        <CardHeader
          title={'Guru Plan'}
          className={classes.cardHeader}
          classes={{
            title: classes.title,
          }}
        />
        <div className={classes.triangle}>
          <div className={classes.header}>FREE</div>
        </div>
        <CardContent>
          <Typography type="title" component="div" align="center">
            Features
          </Typography>
          <Typography type="body1" component="div">
            <ul className={classes.ul}>
              <li className={classes.li}>Create and Share your CV.</li>
              <li className={classes.li}>Create Posts & Courses.</li>
              <li className={classes.li}>
                Share your knowledge Publically or Privatly or keep it Secret.
              </li>
              <li className={classes.li}>Classroom Feature.</li>
            </ul>
          </Typography>
        </CardContent>
        {/* <CardContent>
          <Typography type="title" component="div">
            T&C:
          </Typography>
          <Typography type="body1" component="div">
            <ul>
              <li>If you are a recognized personality or a teacher
              in a recognized institute, you are eligible to this plan.</li>
              <li>
                If you are not a recognized personality, you have to prove
                that you are expert in that field.
                <ul>
                  <li>Write at least 25 articles on your field of
                  expertise.</li>
                  <li>You should get X number of average views.</li>
                </ul>
              </li>
              <li>To ensure quality of our community standard, we are taking
              25k as security deposit which is refundable.</li>
            </ul>
          </Typography>
        </CardContent> */}
      </Card>
    </div>
    <div className={classes.flexChild}>
      <Card raised className={classes.card}>
        <CardHeader
          title={'Educational Institute Plan'}
          className={classes.cardHeader}
          classes={{
            title: classes.title,
          }}
        />
        <div className={classes.triangle}>
          <div className={classes.header}>FREE</div>
        </div>
        <CardContent>
          <Typography type="title" component="div" align="center">
            Features
          </Typography>
          <Typography type="body1" component="div">
            <ul className={classes.ul}>
              <li className={classes.li}>
                Students, Teachers and Alumni on a single network.
              </li>
              <li className={classes.li}>
                All your teachers can create courses.
              </li>
              <li className={classes.li}>
                You will get a personalized classroom feature for your
                institute.
              </li>
              <li className={classes.li}>
                You can maintain your public profile, placement reports, reviews
                etc.
              </li>
              <li className={classes.li}>
                If they want to share their internal courses outside the
                institute, they can share.
              </li>
            </ul>
          </Typography>
        </CardContent>
        {/* <CardContent>
          <Typography type="title" component="div">
            T&C:
          </Typography>
          <Typography type="body1" component="div">
            <ul>
              <li>Whole education institute should be registered.</li>
              <li>Your public profile should be shared with us.</li>
              <li>You should prove your authenticity that you are
              recognized by government.</li>
            </ul>
          </Typography>
        </CardContent> */}
      </Card>
    </div>
    <div className={classes.flexChild}>
      <Card raised className={classes.card}>
        <CardHeader
          title={'Corporate Plan'}
          className={classes.cardHeader}
          classes={{
            title: classes.title,
          }}
        />
        <div className={classes.triangle}>
          <div className={classes.header}>FREE</div>
        </div>
        <CardContent>
          <Typography type="title" component="div" align="center">
            Features
          </Typography>
          <Typography type="body1" component="div">
            <ul className={classes.ul}>
              <li className={classes.li}>
                Can give description of jobs in your company.
              </li>
              <li className={classes.li}>
                Can create courses for the technology on you are working.
              </li>
              <li className={classes.li}>Your whole company profile.</li>
              <li className={classes.li}>
                Can create posts on any new technology.
              </li>
              <li className={classes.li}>
                If they want to share their internal courses outside the
                institute, they can share.
              </li>
            </ul>
          </Typography>
        </CardContent>
        {/* <CardContent>
          <Typography type="title" component="div">
            T&C:
          </Typography>
          <Typography type="body1" component="div">
            <ul>
              <li>If you are a recognized organization, you are eligible
              for this plan.</li>
              <li>If you are not then you must be registered.</li>
              <li>Basic Details of the Organization.</li>
              <li>Your expectation from freshers.</li>
            </ul>
          </Typography>
        </CardContent> */}
      </Card>
    </div>
    <NavigationButtonNext to={'/team'} />
  </Grid>
);

Pricing.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(Pricing);
