/**
 * This component contains the info to join mayash
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import Button from 'material-ui/Button';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  gridItem: {
    padding: '1%',
  },
  cardHeader: {
    textAlign: 'center',
  },
  joinUsTitle: {
    height: '20vh',
    backgroundImage:
      'url("https://storage.googleapis.com/mayash-web/drive/' + '11.jpg")',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  dialogStyle: {},
  [theme.breakpoints.up('xl')]: {},
  [theme.breakpoints.between('md', 'lg')]: {},
  [theme.breakpoints.between('sm', 'md')]: {},
  [theme.breakpoints.between('xs', 'sm')]: {},
  [theme.breakpoints.down('xs')]: {},
});

class JoinUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      open1: false,
      open2: false,
      open3: false,
      open4: false,
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleOpen1 = this.handleOpen1.bind(this);
    this.handleOpen2 = this.handleOpen2.bind(this);
    this.handleOpen3 = this.handleOpen3.bind(this);
    this.handleOpen4 = this.handleOpen4.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleClose1 = this.handleClose1.bind(this);
    this.handleClose2 = this.handleClose2.bind(this);
    this.handleClose3 = this.handleClose3.bind(this);
    this.handleClose4 = this.handleClose4.bind(this);
  }

  handleOpen() {
    this.setState({ open: true });
  }
  handleOpen1() {
    this.setState({ open1: true });
  }
  handleOpen2() {
    this.setState({ open2: true });
  }
  handleOpen3() {
    this.setState({ open3: true });
  }
  handleOpen4() {
    this.setState({ open4: true });
  }
  handleClose() {
    this.setState({ open: false });
  }
  handleClose1() {
    this.setState({ open1: false });
  }
  handleClose2() {
    this.setState({ open2: false });
  }
  handleClose3() {
    this.setState({ open3: false });
  }
  handleClose4() {
    this.setState({ open4: false });
  }

  render() {
    const { classes } = this.props;
    const { open, open1, open2, open3, open4 } = this.state;
    return (
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <Card raised>
            <CardHeader
              title={'Join Us'}
              subheader={
                'Connect with us to make world a better place to live.'
              }
              className={classnames(classes.cardHeader, classes.joinUsTitle)}
            />
          </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'Developers Team'}
              subheader={
                'Join our developer team to learn and build with ' +
                'latest technologies.'
              }
              className={classes.cardHeader}
              onClick={this.handleOpen}
            />
          </Card>
        </Grid>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          className={classes.dialogStyle}
        >
          <DialogContent>
            <Typography type="body1" component="div" className={''}>
              <strong>Note</strong>: We are using advanced JavaScript(ES6, ES7,
              ES8 & JSX).
              <ul>
                <li>
                  Front End Team:
                  <ul>
                    <li>
                      Web Designer(UI/UX):
                      <ul>
                        <li>
                          <strong>Skills Required: </strong> Material Design,
                          UI/UX Experience, Graphic Designing, Animation.
                        </li>
                      </ul>
                    </li>
                    <li>
                      Web Developer:
                      <ul>
                        <li>
                          <strong>Skills Required: </strong> React.js, Redux.js,
                          Webpack, React Router, Material UI, Flux Architecture.
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  Backend Team:
                  <ul>
                    <li>
                      Server:
                      <ul>
                        <li>
                          <strong>Skills Required: </strong> Node.js, Hapi.js,
                          JWT, Templating Engine(Handlebars.js), Server Side
                          Rendering, Authentication, Authorization, Security.
                        </li>
                      </ul>
                    </li>
                    <li>
                      Database:
                      <ul>
                        <li>
                          <strong>Skills Required: </strong> NoSQL Database
                          preferably MongoDB or Google’s DataStore.
                        </li>
                      </ul>
                    </li>
                    <li>
                      DevOps:
                      <ul>
                        <li>
                          <strong>Skills Required: </strong> Docker, CI/CD,
                          Kubernetes.
                        </li>
                      </ul>
                    </li>
                    <li>
                      Testing:
                      <ul>
                        <li>
                          <strong>Skills Required: </strong> Jest.js.
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
            </Typography>
            <DialogActions>
              <Button onClick={this.handleClose}>Close</Button>
            </DialogActions>
          </DialogContent>
        </Dialog>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'Marketing & Promotion Team'}
              subheader={
                'Join our marking and promotion team to make people ' +
                'aware about Mayash.'
              }
              className={classes.cardHeader}
              onClick={this.handleOpen1}
            />
          </Card>
        </Grid>
        <Dialog open={this.state.open1} onClose={this.handleClose1}>
          <DialogContent>
            <Typography type="body1">
              As almost everyone wants to change our education system, few are
              making efforts to bring the change. Join this team to make people
              aware about Mayash to truly improve education system.
            </Typography>
            <Typography type="body1" component="div" className={''}>
              <ul>
                <li>
                  <strong>Skills Required:</strong> Good communication skills,
                  writing skills, presentation skills, Social Media Expert.
                </li>
              </ul>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose1}>Close</Button>
          </DialogActions>
        </Dialog>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'Content Writing Team'}
              subheader={
                'Ideas are not of any worth, if not presented in good format.'
              }
              className={classes.cardHeader}
              onClick={this.handleOpen2}
            />
          </Card>
        </Grid>
        <Dialog open={this.state.open2} onClose={this.handleClose2}>
          <DialogContent>
            <Typography type="body1" component="div" className={''}>
              <ul>
                <li>
                  <strong>Skills Required:</strong> Understanding of SEO,
                  Writing skills, Content Marketing.
                </li>
              </ul>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose2}>Close</Button>
          </DialogActions>
        </Dialog>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'Graphic Design & Video Editing Team'}
              subheader={'Design good poster and videos.'}
              className={classes.cardHeader}
              onClick={this.handleOpen3}
            />
          </Card>
        </Grid>
        <Dialog open={this.state.open3} onClose={this.handleClose3}>
          <DialogContent>
            <Typography type="body1" component="div" className={''}>
              <ul>
                <li>
                  <strong>Skills Required:</strong> Adobe Photoshop, Video
                  Editing, Adobe Illustrator or any other photo editing
                  software.
                </li>
              </ul>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose3}>Close</Button>
          </DialogActions>
        </Dialog>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'Chartered Accountant (CA)'}
              className={classes.cardHeader}
            />
          </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'Legal Officer'}
              className={classes.cardHeader}
            />
          </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={8}
          md={7}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={'College Ambassador'}
              subheader={'Representative of Mayash in their own college.'}
              className={classes.cardHeader}
              onClick={this.handleOpen4}
            />
          </Card>
        </Grid>
        <Dialog open={this.state.open4} onClose={this.handleClose4}>
          <DialogContent>
            <Typography type={'body1'}>
              The college ambassador program provides college students
              interested in developing leadership and communication skills with
              the opportunity to serve as role models, advocates and peer
              advisors to fellow students. Ambassadors have to educate students
              about availability of mayash on campus, encourage involvement and
              engagement in campus life. Student interested in serving as
              college ambassador are urged to apply asap.
            </Typography>
            <Typography type="body1" component="div" className={''}>
              <ul>
                <li>
                  <strong>Responsibilities:</strong>
                  <ul>
                    <li>Survey Filling</li>
                    <li>Organize talks in the college</li>
                    <li>Register people in the website</li>
                    <li>Feedback of the website</li>
                    <li>Data of Alumnis</li>
                    <li>
                      Data of Students, Departments, Subjects and Teachers
                    </li>
                  </ul>
                </li>
                <li>
                  <strong>Benefits:</strong>
                  <ul>
                    <li>Free Goodies from Mayash (T-shirts, Mugs)</li>
                    <li>Certificate and Internship Offer</li>
                    <li>50% off in Full Stack Developers Training Program</li>
                  </ul>
                </li>
              </ul>
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose4}>Close</Button>
          </DialogActions>
        </Dialog>
        <NavigationButtonNext to={'/developers-training'} />
      </Grid>
    );
  }
}

JoinUs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(JoinUs);
