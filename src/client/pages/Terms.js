/**
 * This component represents Donation page for mayash
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';

import Card, { CardHeader, CardMedia, CardContent } from 'material-ui/Card';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  cardHeader: {
    textAlign: 'center',
    padding: '3em',
  },
  media: {
    height: 300,
    width: 300,
  },
  medium: {
    padding: '40px',
  },
  cardContent: {
    paddingLeft: '8em',
    textAlign: 'justify',
    paddingRight: '8em',
    [theme.breakpoints.down('md')]: {
      paddingLeft: '2em',
      paddingRight: '2em',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '10px',
    },
  },
});

class Temrs extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card>
          <CardHeader
            title="Terms and conditions of use"
            subheader={'Hbarve technologies (OPC) private limited Terms of Service'} 
            className={classes.cardHeader} 
          />
          <CardContent className={classes.cardContent}>
            <Typography variant="headline" type="headline">
              1. Introduction
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  These terms and conditions shall govern your use of our
                  website.
                </li>
                <li>
                  By using our website, you accept these terms and
                  conditions in full; accordingly, if you disagree with
                  these terms and conditions or any part of these terms and
                  conditions, you must not use our website.
                </li>
                <li>
                  If you register with our Hbarve Information Technology
                  (OPC) Private Limited, submit any material to our website
                  or use any of our website services, we will ask you to
                  expressly agree to the Privacy Policy.
                </li>
                <li>
                  Our website uses cookies; by using our website or agreeing
                  to these terms and conditions, you consent to our use of
                  cookies in accordance with the terms of our privacy
                  policy.
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              2. Credit
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  This document is an SEQ Legal document from Website
                  Contracts https://mayash.io. You must retain the above
                  credit. Use of this document without the credit is an
                  infringement of copyright. However, you can purchase from
                  us an equivalent document that does not include the
                  credit.
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              3. Copyright notice
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  Copyright (c) [2017(s) of first publication] [Mayash
                  codes].
                </li>
                <li>
                  Subject to the express provisions of these terms and conditions:
                  <ul>
                    <li>
                      we, together with our licensors, own and control all
                      the copyright and other intellectual property rights
                      in our website and the material on our website; and
                    </li>
                    <li>
                      all the copyright and other intellectual property
                      rights in our website and the material on our website
                      are reserved.
                    </li>
                  </ul>
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              4. Licence to use website
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  You may:
                  <ul>
                    <li>view pages from our website in a web browser;</li>
                    <li>
                      download pages from our website for caching in a web
                      browser;
                    </li>
                    <li>print pages from our website;</li>
                    <li>
                      [stream audio and video files from our website]; and
                    </li>
                    <li>
                      [use [our website services] by means of a web
                      browser], subject to the other provisions of these
                      terms and conditions.
                    </li>
                  </ul>
                </li>
                <li>
                  Except as expressly permitted by Section 4.1 or the other
                  provisions of these terms and conditions, you must not
                  download any material from our website or save any such
                  material to your computer.
                </li>
                <li>
                  You may only use our website for [your own personal and
                  business purposes], and you must not use our website for
                  any other purposes.
                </li>
                <li>
                  Except as expressly permitted by these terms and
                  conditions, you must not edit or otherwise modify any
                  material on our website.
                </li>
                <li>
                  Unless you own or control the relevant rights in the material, you must not:
                  <ul>
                    <li>
                      sell, rent or sub-license material from our website;
                    </li>
                    <li>
                      exploit material from our website for a commercial
                      purpose; or
                    </li>
                  </ul>
                </li>
                <li>
                  Notwithstanding Section 4.5, you may redistribute [our
                  newsletter] in [print and electronic form] to [any
                  person].
                </li>
                <li>
                  We reserve the right to restrict access to areas of our
                  website, or indeed our whole website, at our discretion;
                  you must not circumvent or bypass, or attempt to
                  circumvent or bypass, any access restriction measures on
                  our website.
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              5. Acceptable use
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  You must not:
                  <ul>
                    <li>
                      use our website in any way or take any action that
                      causes, or may cause, damage to the website or
                      impairment of the performance, availability or
                      accessibility of the website;
                    </li>
                    <li>
                      use our website in any way that is unlawful, illegal,
                      fraudulent or harmful, or in connection with any
                      unlawful, illegal, fraudulent or harmful purpose or
                      activity;
                    </li>
                    <li>
                      use our website to copy, store, host, transmit, send,
                      use, publish or distribute any material which consists
                      of (or is linked to) any spyware, computer virus,
                      Trojan horse, worm, keystroke logger, rootkit or other
                      malicious computer software;
                    </li>
                    <li>
                      [conduct any systematic or automated data collection
                      activities (including without limitation scraping,
                      data mining, data extraction and data harvesting) on
                      or in relation to our website without our express
                      written consent]
                    </li>
                    <li>
                      [access or otherwise interact with our website using
                      any robot, spider or other automated means[, except
                      for the purpose of [search engine indexing]]];
                    </li>
                    <li>
                      [violate the directives set out in the robots.txt file
                      for our website]; or
                    </li>
                    <li>
                      [use data collected from our website for any direct
                      marketing activity (including without limitation email
                      marketing, SMS marketing, telemarketing and direct
                      mailing)]. [additional list items]
                    </li>
                  </ul>
                </li>
                <li>
                  You must not use data collected from our website to
                  contact individuals, companies or other persons or
                  entities.
                </li>
                <li>
                  You must ensure that all the information you supply to us
                  through our website, or in relation to our website, is
                  [true, accurate, current, complete and non-misleading].
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              6. Registration and accounts
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  You may register for an account with our website by
                  [completing and submitting the account registration form
                  on our website
                </li>
                <li>
                  You must not allow any other person to use your account to
                  access the website.
                </li>
                <li>
                  You must notify us in writing immediately if you become
                  aware of any unauthorised use of your account
                </li>
                <li>
                  You must not use any other person's account to access the
                  website[, unless you have that person's express permission
                  to do so].
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              7. User login details
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  If you register for an account with our website, [we will
                  provide you with] OR [you will be asked to choose] [a user
                  ID and password].
                </li>
                <li>
                  Your user ID must not be liable to mislead and must comply
                  with the content rules set out in Section 10; you must not
                  use your account or user ID for or in connection with the
                  impersonation of any person.
                </li>
                <li>You must keep your password confidential.</li>
                <li>
                  You must notify us in writing immediately if you become
                  aware of any disclosure of your password.
                </li>
                <li>
                  You are responsible for any activity on our website
                  arising out of any failure to keep your password
                  confidential, and may be held liable for any losses
                  arising out of such a failure.
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              8. Cancellation and suspension of account
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  We may:
                  <ul>
                    <li>[suspend your account];</li>
                    <li>[cancel your account]; and/or</li>
                    <li>
                      [edit your account details],at any time in our sole
                      discretion without notice or explanation.
                    </li>
                  </ul>
                </li>
                <li>
                  You may cancel your account on our website [using your
                  account control panel on the website].
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              9. Your content: licence
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  In these terms and conditions, "your content" means [all
                  works and materials (including without limitation text,
                  graphics, images, audio material, video material,
                  audio-visual material, scripts, software and files) that
                  you submit to us or our website for storage or publication
                  on, processing by, or transmission via, our website].
                </li>
                <li>
                  You grant to us a [worldwide, irrevocable, non-exclusive,
                  royalty-free licence] to [use, reproduce, store, adapt,
                  publish, translate and distribute your content in any
                  existing or future media] OR [reproduce, store and publish
                  your content on and in relation to this website and any
                  successor website] OR [reproduce, store and, with your
                  specific consent, publish your content on and in relation
                  to this website]. We can also use it to make big data.
                </li>
                <li>
                  You grant to us the right to sub-license the rights
                  licensed under Section 9.2.
                </li>
                <li>
                  You grant to us the right to bring an action for
                  infringement of the rights licensed under Section 9.2.
                </li>
                <li>
                  You hereby waive all your moral rights in your content to
                  the maximum extent permitted by applicable law; and you
                  warrant and represent that all other moral rights in your
                  content have been waived to the maximum extent permitted
                  by applicable law.
                </li>
                <li>
                  You may edit your content to the extent permitted using
                  the editing functionality made available on our website.
                </li>
                <li>
                  Without prejudice to our other rights under these terms
                  and conditions, if you breach any provision of these terms
                  and conditions in any way, or if we reasonably suspect
                  that you have breached these terms and conditions in any
                  way, we may delete, unpublish or edit any or all of your
                  content.
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              10. Limited warranties
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  We do not warrant or represent:
                  <ul>
                    <li>
                      the completeness or accuracy of the information
                      published on our website;
                    </li>
                    <li>
                      that the material on the website is up to date; or
                    </li>
                    <li>
                      that the website or any service on the website will
                      remain available.
                    </li>
                  </ul>
                </li>
                <li>
                  We reserve the right to discontinue or alter any or all of
                  our website services, and to stop publishing our website,
                  at any time in our sole discretion without notice or
                  explanation; and save to the extent expressly provided
                  otherwise in these terms and conditions, you will not be
                  entitled to any compensation or other payment upon the
                  discontinuance or alteration of any website services, or
                  if we stop publishing the website.these terms and
                  conditions, you must not use our website.
                </li>
                <li>
                  To the maximum extent permitted by applicable law and
                  subject to Section 12.1, we exclude all representations
                  and warranties relating to the subject matter of these
                  terms and conditions, our website and the use of our
                  website.
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              11. Limitations and exclusions of liability
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  Nothing in these terms and conditions will:
                  <ul>
                    <li>
                      limit or exclude any liability for death or personal
                      injury resulting from negligence;
                    </li>
                    <li>
                      limit or exclude any liability for fraud or fraudulent
                      misrepresentation;
                    </li>
                    <li>
                      limit any liabilities in any way that is not permitted
                      under applicable law; or
                    </li>
                    <li>
                      exclude any liabilities that may not be excluded under
                      applicable law.
                    </li>
                  </ul>
                </li>
                <li>
                  The limitations and exclusions of liability set out in this Section 12 and elsewhere in these terms and conditions:
                  <ul>
                    <li>are subject to Section 12.1; and</li>
                    <li>
                      govern all liabilities arising under these terms and
                      conditions or relating to the subject matter of these
                      terms and conditions, including liabilities arising in
                      contract, in tort (including negligence) and for
                      breach of statutory duty, except to the extent
                      expressly provided otherwise in these terms and
                      conditions.
                    </li>
                  </ul>
                </li>
                <li>
                  To the extent that our website and the information and
                  services on our website are provided free of charge, we
                  will not be liable for any loss or damage of any nature.
                </li>
                <li>
                  We will not be liable to you in respect of any losses
                  arising out of any event or events beyond our reasonable
                  control.
                </li>
                <li>
                  We will not be liable to you in respect of any business
                  losses, including (without limitation) loss of or damage
                  to profits, income, revenue, use, production, anticipated
                  savings, business, contracts, commercial opportunities or
                  goodwill.
                </li>
                <li>
                  We will not be liable to you in respect of any loss or
                  corruption of any data, database or software.
                </li>
                <li>
                  We will not be liable to you in respect of any special,
                  indirect or consequential loss or damage.
                </li>
                <li>
                  You accept that we have an interest in limiting the
                  personal liability of our officers and employees and,
                  having regard to that interest, you acknowledge that we
                  are a limited liability entity; you agree that you will
                  not bring any claim personally against our officers or
                  employees in respect of any losses you suffer in
                  connection with the website or these terms and conditions
                  (this will not, of course, limit or exclude the liability
                  of the limited liability entity itself for the acts and
                  omissions of our officers and employees).
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              12. Breaches of these terms and conditions
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  Without prejudice to our other rights under these terms and conditions, if you breach these terms and conditions in any way, or if we reasonably suspect that you have breached these terms and conditions in any way, we may:
                  <ul>
                    <li>send you one or more formal warnings;</li>
                    <li>temporarily suspend your access to our website;</li>
                    <li>
                      permanently prohibit you from accessing our website;
                    </li>
                    <li>
                      [block computers using your IP address from accessing
                      our website];
                    </li>
                    <li>
                      [contact any or all of your internet service providers
                      and request that they block your access to our
                      website];
                    </li>
                    <li>
                      commence legal action against you, whether for breach
                      of contract or otherwise; and/or
                    </li>
                    <li>
                      [suspend or delete your account on our website].
                      <br />
                      [additional list items]
                    </li>
                  </ul>
                </li>
                <li>
                  Where we suspend or prohibit or block your access to our
                  website or a part of our website, you must not take any
                  action to circumvent such suspension or prohibition or
                  blocking[ (including without limitation [creating and/or
                  using a different account])].
                </li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              13. Variation
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  We may revise these terms and conditions from time to
                  time.
                </li>
                <li>
                  [Hbarve technologies (OPC) private limited may revise
                  these terms of service for its website at any time without
                  notice. By using this website you are agreeing to be bound
                  by the then current version of these terms of service.]
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              14. Assignment
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  You hereby agree that we may assign, transfer,
                  sub-contract or otherwise deal with our rights and/or
                  obligations under these terms and conditions.
                </li>
                <li>
                  You may not without our prior written consent assign,
                  transfer, sub-contract or otherwise deal with any of your
                  rights and/or obligations under these terms and
                  conditions.
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              15. Severability
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  If a provision of these terms and conditions is determined
                  by any court or other competent authority to be unlawful
                  and/or unenforceable, the other provisions will continue
                  in effect.
                </li>
                <li>
                  If any unlawful and/or unenforceable provision of these
                  terms and conditions would be lawful or enforceable if
                  part of it were deleted, that part will be deemed to be
                  deleted, and the rest of the provision will continue in
                  effect.
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              16. Third party rights
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  A contract under these terms and conditions is for our
                  benefit and your benefit, and is not intended to benefit
                  or be enforceable by any third party.
                </li>
                <li>
                  The exercise of the parties' rights under a contract under
                  these terms and conditions is not subject to the consent
                  of any third party.
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              17. Entire agreement
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  Subject to Section 12.1, these terms and conditions[,
                  together with [our privacy and cookies policy],] shall
                  constitute the entire agreement between you and us in
                  relation to your use of our website and shall supersede
                  all previous agreements between you and us in relation to
                  your use of our website.
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              18. Law and jurisdiction
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>
                  These terms and conditions shall be governed by and
                  construed in accordance with Indian law.
                </li>
                <li>
                  Any disputes relating to these terms and conditions shall
                  be subject to the jurisdiction of the courts of India.
                </li>
              </ol>
            </Typography>

            <Typography variant="headline" type="headline">
              19. Our details
            </Typography>
            <Typography variant="headline" type="display">
              <ol>
                <li>This website is owned and operated by Honey Barve.</li>
                <li>
                  We are registered in [ India ] under registration number
                  [number], and our registered office is at [Ranchi].
                </li>
                <li>Our principal place of business is at [Gwalior].</li>
                <li>
                  You can contact us:
                  <ul>
                    <li>
                      https://docs.google.com/forms/d/e/1FAIpQLScRqqZ_nRF9AhCBM73qFYvVx0L2SwFTtv6t9RxqKJ4zxr-l0w/viewform
                    </li>
                    <li>Contact Number:- 8770693644</li>
                    <li>E mail:- contact-us@mayash.io</li>
                    <li>Website:- https://Mayash.io.</li>
                  </ul>
                </li>
              </ol>
            </Typography>
          </CardContent>
         {/*
          <CardHeader title="TERMS OF SERVICE" subheader={'Hbarve technologies (OPC) private limited Terms of Service'} className={classes.cardHeader} />
          <CardContent className={classes.cardContent}>
            <Typography variant="headline" type="headline">
              1. Terms
            </Typography>
            <Typography variant="headline" type="display">
              By accessing the website at https://mayash.io, you are agreeing to be bound by these terms of service, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this website are protected by applicable trademark law.
            </Typography>

            <Typography variant="headline" type="headline">
              2. Use License
            </Typography>
            <Typography variant="headline" type="display">
              <ul>
                <li>
                  <ol>
                    Permission is granted to temporarily download one copy of the materials (information or software) on Hbarve technologies (OPC) private limited's website for personal, non-commercial transitory viewing only. This is the grant of a license, not a transfer of title, and under this license you may not:
                    <li>modify or copy the materials;</li>
                    <li>
                      use the materials for any commercial purpose, or for
                      any public display (commercial or non-commercial);
                    </li>
                    <li>
                      attempt to decompile or reverse engineer any software
                      contained on Hbarve technologies (OPC) private
                      limited's website;
                    </li>
                    <li>
                      remove any copyright or other proprietary notations
                      from the materials; or
                    </li>
                    <li>
                      transfer the materials to another person or "mirror"
                      the materials on any other server.
                    </li>
                  </ol>
                </li>
                <li>
                  This license shall automatically terminate if you violate
                  any of these restrictions and may be terminated by Hbarve
                  technologies (OPC) private limited at any time. Upon
                  terminating your viewing of these materials or upon the
                  termination of this license, you must destroy any
                  downloaded materials in your possession whether in
                  electronic or printed format.
                </li>
              </ul>
            </Typography>

            <Typography variant="headline" type="headline">
              3. Disclaimer
            </Typography>
            <Typography variant="headline" type="display">
              <ul>
                <li>
                  The materials on Hbarve technologies (OPC) private
                  limited's website are provided on an 'as is' basis. Hbarve
                  technologies (OPC) private limited makes no warranties,
                  expressed or implied, and hereby disclaims and negates all
                  other warranties including, without limitation, implied
                  warranties or conditions of merchantability, fitness for a
                  particular purpose, or non-infringement of intellectual
                  property or other violation of rights.
                </li>
                <li>
                  Further, Hbarve technologies (OPC) private limited does
                  not warrant or make any representations concerning the
                  accuracy, likely results, or reliability of the use of the
                  materials on its website or otherwise relating to such
                  materials or on any sites linked to this site.
                </li>
              </ul>
            </Typography>

            <Typography variant="headline" type="headline">
              4. Limitations
            </Typography>
            <Typography variant="headline" type="display">
              In no event shall Hbarve technologies (OPC) private limited or its suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials on Hbarve technologies (OPC) private limited's website, even if Hbarve technologies (OPC) private limited or a Hbarve technologies (OPC) private limited authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
            </Typography>

            <Typography variant="headline" type="headline">
              5. Accuracy of materials
            </Typography>
            <Typography variant="headline" type="display">
              The materials appearing on Hbarve technologies (OPC) private limited website could include technical, typographical, or photographic errors. Hbarve technologies (OPC) private limited does not warrant that any of the materials on its website are accurate, complete or current. Hbarve technologies (OPC) private limited may make changes to the materials contained on its website at any time without notice. However Hbarve technologies (OPC) private limited does not make any commitment to update the materials.
            </Typography>

            <Typography variant="headline" type="headline">
              6. Links
            </Typography>
            <Typography variant="headline" type="display">
              Hbarve technologies (OPC) private limited has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by Hbarve technologies (OPC) private limited of the site. Use of any such linked website is at the user's own risk.
            </Typography>

            <Typography variant="headline" type="headline">
              7. Modifications
            </Typography>
            <Typography variant="headline" type="display">
              Hbarve technologies (OPC) private limited may revise these terms of service for its website at any time without notice. By using this website you are agreeing to be bound by the then current version of these terms of service.
            </Typography>
            <Typography variant="headline" type="headline">
              8. Governing Law
            </Typography>
            <Typography variant="headline" type="display">
              These terms and conditions are governed by and construed in accordance with the laws of India and you irrevocably submit to the exclusive jurisdiction of the courts in that State or location.
            </Typography>
          </CardContent>
          */}
        </Card>
        <NavigationButtonNext to={'/developers-training'} />
      </div>
    );
  }
}

Temrs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Temrs);
