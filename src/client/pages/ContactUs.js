/**
 * This component contain all the info to contact us
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardContent } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import PhoneIcon from 'material-ui-icons/Phone';
import EmailIcon from 'material-ui-icons/Email';

import NavigationButtonNext from '../components/NavigationButtonNext';
import Input from '.././components/Input';

import Facebook from '../../lib/mayash-icons/Facebook';
import Twitter from '../../lib/mayash-icons/Twitter';
import LinkedIn from '../../lib/mayash-icons/LinkedIn';
import Instagram from '../../lib/mayash-icons/Instagram';
import Gmail from '../../lib/mayash-icons/Gmail';

const styles = (theme) => ({
  root: {},
  gridItem: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  content: {
    marginLeft: '150px',
    marginRight: '100px',
    [theme.breakpoints.up('xl')]: {
      marginLeft: '150px',
      marginRight: '100px',
    },
    [theme.breakpoints.down('xl')]: {
      marginLeft: '150px',
      marginRight: '100px',
    },
    [theme.breakpoints.down('lg')]: {
      marginLeft: '150px',
      marginRight: '100px',
    },
    [theme.breakpoints.down('md')]: {
      marginLeft: '0',
      marginRight: '0',
    },
    [theme.breakpoints.down('sm')]: {
      marginLeft: '0',
      marginRight: '0',
    },
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  textArea: {
    width: '100%',
    height: '100px',
    border: '2px solid #dadada',
    borderRadius: '7px',
    overflow: 'hidden',
  },
  input: {
    margin: '6px',
    padding: '15px',
  },
  child: {
    alignSelf: 'flex-start',
  },
  iconCard: {
    width: '100px',
    marginTop: '-25rem',
    marginLeft: '-30px',
    marginBottom: '20px',
    boxShadow:
      '11px 8px 11px 8px rgba(0, 0, 0, 0.2),' +
      ' 0 6px 20px 0 rgba(0, 0, 0, 0.19)',

    [theme.breakpoints.up('xl')]: {
      marginTop: '-25rem',
      marginLeft: '-30px',
    },
    [theme.breakpoints.down('lg')]: {
      marginTop: '-25rem',
      marginLeft: '-30px',
    },
    [theme.breakpoints.down('md')]: {
      marginTop: '0',
      marginLeft: '0',
      marginRight: '0',
      boxShadow: 'none',
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: '0',
      marginLeft: '0',
      marginRight: '0',
      boxShadow: 'none',
    },
  },
  paragraph: {
    padding: '1%',
    textAlign: 'center',
  },
  contactsDetalis: {
    display: 'flex',
    marginTop: '-50%',
    justifyContent: '',
    position: 'relative',
    flexDirection: 'column',
    flexWrap: 'wrap',
    marginRight: '300px',
    width: '200px',
    [theme.breakpoints.up('xl')]: {
      marginTop: '-45%',
      position: 'relative',
      flexDirection: 'column',
      marginRight: '300px',
      width: '200px',
    },
    [theme.breakpoints.down('xl')]: {
      marginTop: '-45%',
      position: 'relative',
      marginRight: '300px',
      flexDirection: 'column',
      width: '200px',
    },
    [theme.breakpoints.down('lg')]: {
      marginTop: '-45%',
      position: 'relative',
      marginRight: '300px',
      width: '200px',
      flexDirection: 'column',
    },
    [theme.breakpoints.down('md')]: {
      marginTop: '0',
      position: 'absolute',
      marginRight: '0',
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor: '#fff',
      width: '87%',
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: '0',
      position: 'absolute',
      flexDirection: 'row',
      justifyContent: 'center',
      backgroundColor: '#fff',
      marginRight: '0',
      width: '87%',
    },
  },
});

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
    };
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeEmail = this.onChangeEmail.bind(this);
  }

  onChangeName(e) {
    this.setState({ name: e.target.value });
  }

  onChangeEmail(e) {
    this.setState({ email: e.target.value });
  }

  render() {
    const { classes } = this.props;
    const { name, email } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={7}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised className={classes.card}>
            <CardContent className={classes.content}>
              <Typography type="display2" align="center">
                Get IN Touch
              </Typography>
              <Typography type="headline" color="secondary" align="center">
                Feel free to drop us a line below
              </Typography>
              <Typography component="div" className={classes.input}>
                <Input
                  value={name}
                  placeholder={'Your name'}
                  onChange={this.onChangeName}
                />
              </Typography>
              <Typography component="div" className={classes.input}>
                <Input
                  value={email}
                  placeholder={'Your email'}
                  onChange={this.onChangeEmail}
                />
              </Typography>
              <Typography component="div" className={classes.input}>
                Your mesege
                <textarea className={classes.textArea} />
              </Typography>
            </CardContent>
            <CardContent className={classes.content}>
              <Typography type="title">You can also contact us :</Typography>
              <Typography>
                <PhoneIcon />
                <a href="tel:8770693644" className={classes.link}>
                  {'  '}
                  +91-8770693644
                </a>
              </Typography>
              <Typography type="headline">
                <EmailIcon />
                <a href="mailto:hbarve1@mayash.io" className={classes.link}>
                  {'  '}
                  hbarve1@mayash.io
                </a>
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={7}
          xl={6}
          className={classes.gridItem}
        >
          <Card raised className={classes.iconCard}>
            <CardContent className={classes.contactsDetalis}>
              <Button
                onClick={() => {
                  const href = 'https://www.facebook.com/1mayash';
                  window.open(href);
                }}
                className={classes.child}
              >
                <Facebook />
              </Button>
              <Button
                onClick={() => {
                  const href = 'https://www.instagram.com/mayash.io';
                  window.open(href);
                }}
                className={classes.child}
              >
                <Instagram />
              </Button>
              <Button
                onClick={() => {
                  const href = 'https://twitter.com/mayash_io';
                  window.open(href);
                }}
                className={classes.child}
              >
                <Twitter />
              </Button>
              <Button
                onClick={() => {
                  const href = 'https://www.linkedin.com/company/13274247/';
                  window.open(href);
                }}
                className={classes.child}
              >
                <LinkedIn />
              </Button>
              <Button
                onClick={() => {
                  const href = 'emailto:contact-us@mayash.io';
                  window.open(href);
                }}
                className={classes.child}
              >
                <Gmail />
              </Button>
            </CardContent>
          </Card>
        </Grid>
        <NavigationButtonNext to={'/sign-in'} />
      </Grid>
    );
  }
}

ContactUs.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContactUs);
