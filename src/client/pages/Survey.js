/**
 * Survey page component
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {
    // paddingTop: '3%',
  },
  cardHeader: {
    textAlign: 'center',
    height: '20vh',
    backgroundImage:
      'url("https://storage.googleapis.com/mayash-web/drive/37.jpg")',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  paragraph: {
    padding: '2%',
    textAlign: 'center',
  },
  [theme.breakpoints.up('xl')]: {},
  [theme.breakpoints.between('md', 'lg')]: {},
  [theme.breakpoints.between('sm', 'md')]: {},
  [theme.breakpoints.between('xs', 'sm')]: {},
  [theme.breakpoints.down('xs')]: {},
});

const Survey = ({ classes }) => (
  <Grid container spacing={0} justify="center" className={classes.root}>
    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
      <Card raised>
        <CardHeader
          title={'Survey'}
          subheader={
            "Want to know people's point of view on present" +
            ' education system.'
          }
          className={classes.cardHeader}
        />
        <CardContent>
          <Typography className={classes.paragraph}>
            Mayash is conducting a survey on persisting education system and
            welcoming views on how to make it better. With the help of survey it
            will be easy to recognize the field or section of present education
            system in which we have to work first.
          </Typography>
          <Typography className={classes.paragraph}>
            <a href="https://goo.gl/forms/0AHNRbPNcspJsoNC2" target="blank">
              Google Survey Form
            </a>
          </Typography>
        </CardContent>
      </Card>
    </Grid>
    <NavigationButtonNext to={'/contact-us'} />
  </Grid>
);

Survey.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Survey);
