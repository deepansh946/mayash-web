/**
 * This component contains discription of services of mayash
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';

import Typography from 'material-ui/Typography';
import Collapse from 'material-ui/transitions/Collapse';

import Button from 'material-ui/Button';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  cardHeader: {
    textAlign: 'center',
  },
  card: {
    marginTop: '2%',
    backgroundColor: theme.palette.background.default,
  },
  title: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 30,
    },
  },
  subheader: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 16,
    },
  },

  servicesTitle: {
    textAlign: 'center',
    height: '20vh',
    backgroundImage:
      'url("https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg")',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },

  paragraph: {
    padding: '2%',
  },
  gridItem: {
    padding: '2%',
  },

  media: {
    width: '100%',
    position: 'relative',
    top: '-28px',
    opacity: 1,
    cursor: 'crosshair',
    transitionDuration: '0.3s',
    borderRadius: '8px 8px 8px 8px',
    boxShadow:
      '0 8px 11px 8px rgba(0, 0, 0, 0.2),' +
      ' 0 6px 20px 0 rgba(0, 0, 0, 0.19)',
    '&:hover': {
      transform: 'scale(1.02)',
      opacity: 0.9,
    },
    [theme.breakpoints.up('xl')]: {
      marginBottom: '-85px',
      height: '170px',
    },
    [theme.breakpoints.down('xl')]: {
      marginBottom: '-85px',
      height: '170px',
    },
    [theme.breakpoints.down('lg')]: {
      marginBottom: '-85px',
      height: '170px',
    },
    [theme.breakpoints.down('md')]: {
      marginBottom: '-85px',
      height: '170px',
    },
    [theme.breakpoints.down('sm')]: {
      marginBottom: '-85px',
      height: '150px',
    },
    [theme.breakpoints.down('xs')]: {
      marginBottom: '-85px',
      height: '150px',
    },
  },

  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  transform: 'rotate(180deg)',
  expandOpen: {
    // height: '2px',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  back: {
    backgroundColor: theme.palette.primary[400],
    borderRadius: '50%',
    position: 'relative',
    [theme.breakpoints.up('xl')]: {
      marginTop: '-10%',
    },
    [theme.breakpoints.down('xl')]: {
      marginTop: '-5%',
    },
    [theme.breakpoints.down('lg')]: {
      marginTop: '-3%',
    },
    [theme.breakpoints.down('md')]: {
      marginTop: '-5%',
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: '1%',
    },
    [theme.breakpoints.down('xs')]: {
      marginTop: '-10%',
    },
  },
});

class Services extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false,
      expand: false,
      onexpand: false,
      onexpanded: false,
    };
  }

  handleExpandClick = () => this.setState({ expanded: !this.state.expanded });

  secondHandleExpandClick = () => this.setState({ expand: !this.state.expand });

  thirdHandleExpandClick = () =>
    this.setState({ onexpand: !this.state.onexpand });

  fourthHandleExpandClick = () =>
    this.setState({ onexpanded: !this.state.onexpanded });

  render() {
    const { classes } = this.props;

    return (
      <Grid
        container
        spacing={0}
        justify="space-around"
        className={classes.root}
      >
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Card raised>
            <CardHeader
              title={
                <Typography className={classes.title} type="display3">
                  SERVICES
                </Typography>
              }
              subheader={
                <Typography className={classes.subheader} type="headline">
                  Our products and services will help students as well as
                  teachers to improve their knowledge and skills.
                </Typography>
              }
              className={classnames(classes.cardHeader, classes.servicesTitle)}
            />
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={3} className={classes.gridItem}>
          <Card raised className={classes.card}>
            <CardContent>
              <img
                className={classes.media}
                src={
                  'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg'
                }
                alt={'hia'}
              />
            </CardContent>
            <CardActions disableActionSpacing>
              <div className={classes.flexGrow} />
              <div className={classes.back}>
                <Button
                  className={classnames(classes.expand, {
                    [classes.expandOpen]: this.state.expanded,
                  })}
                  onClick={this.handleExpandClick}
                  aria-expanded={this.state.expanded}
                  aria-label="Show more"
                  fab
                  color="accent"
                  raised
                >
                  <ExpandMoreIcon />
                </Button>
              </div>
              <div className={classes.flexGrow} />
            </CardActions>
            <Collapse in={this.state.expanded} timeout="auto" unmountOnExit>
              <CardContent className={classes.content}>
                <Typography type="body2">
                  <ol>
                    <li>Get organised Notes.</li>
                    <li>Doubts discussion Channel</li>
                    <li>Connect with your alumni</li>
                    <li>Get latest updates in your respective field</li>
                    <li>Test yourself.</li>
                  </ol>
                </Typography>
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={3} className={classes.gridItem}>
          <Card raised className={classes.card}>
            <CardContent>
              <img
                className={classes.media}
                src={
                  'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg'
                }
                alt={'hia'}
              />
            </CardContent>
            <CardActions disableActionSpacing>
              <div className={classes.flexGrow} />
              <div className={classes.back}>
                <Button
                  className={classnames(classes.expand, {
                    [classes.expandOpen]: this.state.expand,
                  })}
                  onClick={this.secondHandleExpandClick}
                  aria-expanded={this.state.expand}
                  aria-label="Show more"
                  fab
                  color="accent"
                  raised
                >
                  <ExpandMoreIcon />
                </Button>
              </div>
              <div className={classes.flexGrow} />
            </CardActions>
            <Collapse in={this.state.expand} timeout="auto" unmountOnExit>
              <CardContent>
                <Typography type="body2">
                  <ol>
                    <li>Connect with your students.</li>
                    <li>
                      Doubts discussion Channel to clear Doubts of your
                      students.
                    </li>
                    <li>
                      Share your knowledge with not only your students but with
                      whole world.
                    </li>
                    <li>Get latest updates in your respective field.</li>
                  </ol>
                </Typography>
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={3} className={classes.gridItem}>
          <Card raised className={classes.card}>
            <CardContent>
              <img
                className={classes.media}
                src={
                  'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg'
                }
                alt={'hia'}
              />
            </CardContent>
            <CardActions disableActionSpacing>
              <div className={classes.flexGrow} />
              <div className={classes.back}>
                <Button
                  className={classnames(classes.expand, {
                    [classes.expandOpen]: this.state.onexpand,
                  })}
                  onClick={this.thirdHandleExpandClick}
                  aria-expanded={this.state.onexpand}
                  aria-label="Show more"
                  fab
                  color="accent"
                  raised
                >
                  <ExpandMoreIcon />
                </Button>
              </div>
              <div className={classes.flexGrow} />
            </CardActions>
            <Collapse in={this.state.onexpand} timeout="auto" unmountOnExit>
              <CardContent>
                <Typography type="body2">
                  <ol>
                    <li>Connect with your students.</li>
                    <li>
                      Doubts discussion Channel to clear Doubts of your
                      students.
                    </li>
                    <li>
                      Share your knowledge with not only your students but with
                      whole world.
                    </li>
                    <li>Get latest updates in your respective field.</li>
                  </ol>
                </Typography>
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6} md={6} lg={3} className={classes.gridItem}>
          <Card raised className={classes.card}>
            <CardContent>
              <img
                className={classes.media}
                src={
                  'https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg'
                }
                alt={'hia'}
              />
            </CardContent>
            <CardActions disableActionSpacing>
              <div className={classes.flexGrow} />
              <div className={classes.back}>
                <Button
                  className={classnames(classes.expand, {
                    [classes.expandOpen]: this.state.onexpanded,
                  })}
                  onClick={this.fourthHandleExpandClick}
                  aria-expanded={this.state.onexpanded}
                  aria-label="Show more"
                  fab
                  color="accent"
                  raised
                >
                  <ExpandMoreIcon />
                </Button>
              </div>
              <div className={classes.flexGrow} />
            </CardActions>
            <Collapse in={this.state.onexpanded} timeout="auto" unmountOnExit>
              <CardContent>
                <Typography type="body2">
                  <ol>
                    <li>Get updated knowledge on latest technologies.</li>
                    <li>Stay updated with industries trends.</li>
                    <li>See growth of your employes.</li>
                  </ol>
                </Typography>
              </CardContent>
            </Collapse>
          </Card>
        </Grid>
        <NavigationButtonNext to={'/pricing'} />
      </Grid>
    );
  }
}

Services.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Services);
