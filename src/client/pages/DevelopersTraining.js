/**
 * This component contains about developers training provided by mayash
 */

import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
import Chip from 'material-ui/Chip';
import Avatar from 'material-ui/Avatar';
import Button from 'material-ui/Button';
import DoneIcon from 'material-ui-icons/Done';
import SendIcon from 'material-ui-icons/Send';

import styleSheet from './style/DevelopersTraining';

import Html5 from '../../lib/mayash-icons/Html5';
import Css3 from '../../lib/mayash-icons/Css3';
import NodeJs from '../../lib/mayash-icons/NodeJs';
import Redux from '../../lib/mayash-icons/Redux';
import ReactJs from '../../lib/mayash-icons/React';
import Docker from '../../lib/mayash-icons/Docker';
import Hapi from '../../lib/mayash-icons/Hapi';
import Git from '../../lib/mayash-icons/Git';
import Webpack from '../../lib/mayash-icons/Webpack';
import ReactRouter from '../../lib/mayash-icons/ReactRouter';
import MaterialDesign from '../../lib/mayash-icons/MaterialDesign';
import Kubernates from '../../lib/mayash-icons/Kubernates';
import Database from '../../lib/mayash-icons/Database';

const DevelopersTraining = ({ classes }) => (
  <Grid container spacing={0} justify="center" className={classes.root}>
    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
      <Card raised>
        <CardHeader
          title={
            <Typography className={classes.mediaTitle} type="display3">
              Developers Training
            </Typography>
          }
          subheader={
            <Typography className={classes.subheader} type="headline">
              The new technologies are better to use, easy to handle and are
              very efficient.
            </Typography>
          }
          className={classnames(classes.cardHeaderTop, classes.developersTitle)}
        />
      </Card>
    </Grid>
    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
      <div>
        <Card raised>
          <CardContent>
            <Typography
              type="body1"
              align="justify"
              gutterBottom
              className={classnames(classes.header)}
            >
              Technology is changing rapidly, demand of the industryis ever
              increasing, on the other hand the youth is not provided with
              sufficient guidance to pick up new technologies. The new
              technologies are better to use, easy to handle and are very
              efficient. Learning these technologies is becoming increasingly
              difficult due to lack of proper guidance available at low cost,
              since they are not able to get their hands on these technologies,
              so, they are becoming incompetent.
              <ul>
                <li>
                  We are providing a full stack developer training programto
                  make the youngsters stay in-sync with the new trends and stay
                  ahead of the competition, always.
                </li>
                <li>
                  By joining this program, the students will get a chance to
                  work on live projects and be able to make real world apps.
                </li>
              </ul>
            </Typography>
            <Typography
              color="primary"
              align="center"
              className={classes.headline}
            >
              Technologies, you will learn
            </Typography>
            <div className={classes.flex}>
              <Typography
                className={classnames(
                  classes.headline,
                  classes.flexChild,
                  classes.headingColor,
                )}
              >
                HTML
                <Html5 height={'40px'} width={'40px'} />
              </Typography>
              <Typography
                className={classnames(classes.content, classes.flexChild)}
              >
                HTML is the standard markup language for creating Web pages.
                HTML stands for Hyper Text Markup Language HTML describes the
                structure of Web pages using markup HTML elements are the
                building blocks of HTML pages HTML elements are represented by
                tags HTML tags label pieces of content such as {'"heading" '},
                {'"paragraph" , "table" '}, and so on Browsers do not display
                the HTML HTML tags, but use them to render the content of the
                page
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                CSS
                <Css3 height={'40px'} width={'40px'} />
              </Typography>
              <Typography className={classes.content}>
                The language which is used to change the presentation of the
                content in a web page. By it we can add many things in our web
                page from text formatting to animations
                <ul>
                  <li>Inline CSS</li>
                  Writing css inside the html code
                  <li>Flex Box</li>
                  The new layout of CSS3 which is widely used at industrial
                  level due to its flexibility across pages.
                  <li>CSS in JS</li>
                  In this styling we divide our web page in components which
                  makes it very fast, flexible and easy to design.
                </ul>
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography className={classes.headline}>
                <h2 className={classes.javaScript}>JavaScript</h2>
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                JavaScript is one of the three basic languages of WorldWide Web
                along with HTML and CSS
                <br />
                <ul>
                  <li>ES5 - Basic JavaScript Concepts </li>
                  <li>
                    ES6 -Released in 2015 , it is well organized as compared to
                    previous versions of JS.
                  </li>
                  <li> ES7 - ECMAScript 2016</li>
                  <li> ES8 - ECMAScript 2017</li>
                  <li> Extended JavaScript (JSX) </li>
                  This is designed by facebook and is used in React.js. It’s
                  main feature is to write HTML and CSS inside JavaScript.
                  <br />
                  <li> Functional Programming </li>
                  It is a new way of programming which makes the code easier to
                  read, reuse, test and debug.
                  <br />
                  <li> JSON Web Token (JWT) </li>
                  It is used for authorization in the form of access tokens.
                  <br />
                </ul>
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <NodeJs width="235px" height="55px" />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                An open source JavaScript library used for running scripts at
                server side. It is 90 times faster than PHP.Node.js is a
                JavaScript runtime built on Chromes V8 JavaScript engine.
                Node.js uses an event-driven, non-blocking I/O model that makes
                it lightweight and efficient. Node.js package ecosystem, npm, is
                the largest ecosystem of open source libraries in the world.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Hapi width="120px" height="120px" />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                A rich framework for building applications and services hapi
                enables developers to focus on writing reusable application
                logic instead of spending time building infrastructure. hapi's
                stability and reliability is empowering many companies today.
                There are dozens of plugins for hapi, ranging from documentation
                to authentication, and much more.
              </Typography>
            </div>

            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Database width={'70px'} height={'80px'} /> Database
              </Typography>
              <Typography
                type={'headline'}
                className={classnames(classes.content, classes.flex)}
              >
                <div>
                  <strong>Relational Databases</strong>
                  <br />
                  MySql: MySQL is an open-source relational database management
                  system.
                </div>
                <div>
                  <strong>{'Non-Relational Databases '}</strong>
                  <br />
                  {'MongoDB: MongoDB is a free and open-source cross-platform' +
                    'document-oriented database program.'}
                  <br />
                  {'Google’s Datastore: Google Cloud Datastore is a highly' +
                    'scalable, fully managed NoSQL database service offered by' +
                    'Google on the Google Cloud Platform.'}
                  <br />
                </div>
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <ReactJs width="200px" height="80px" />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                React is one of Facebook’s first open source projects that is
                both under very active development and is also being used to
                ship code to everybody on facebook.com. React is worked on
                full-time by Facebook’s product infrastructure and Instagram’s
                user interface engineering teams. They’re often around and
                available for questions.Now React is used most of the big
                company for frontend development.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <ReactRouter />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                Components are the heart of React's powerful, declarative
                programming model. React Router is a collection of navigational
                components that compose declaratively with your application.
                Whether you want to have bookmarkable URLs for your web app or a
                composable way to navigate in React Native, React Router works
                wherever React is rendering--so take your pick!
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Redux width="210px" height="80px" />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                It is also a open source state management library of JS. As the
                requirements for JavaScript single-page applications have become
                increasingly complicated, our code must manage more state than
                ever before. This state can include server responses and cached
                data, as well as locally created data that has not yet been
                persisted to the server. UI state is also increasing in
                complexity, as we need to manage active routes, selected tabs,
                spinners, pagination controls, and so on. Following in the steps
                of Flux, CQRS, and Event Sourcing, Redux attempts to make state
                mutations predictable by imposing certain restrictions on how
                and when updates can happen.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Webpack />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                At its core, webpack is a static module bundler for modern
                JavaScript applications. When webpack processes your
                application, it recursively builds a dependency graph that
                includes every module your application needs, then packages all
                of those modules into one or more bundles.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                Material Design
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                Material Design makes more liberal use of grid-based layouts,
                responsive animations and transitions, padding, and depth
                effects such as lighting and shadows.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Docker width="120px" height="120px" />
              </Typography>
              <Typography
                type={'headline'}
                className={classnames(classes.content, classes.flex)}
              >
                Docker is a platform for developers and sysadmins to develop,
                deploy, and run applications with containers. The use of Linux
                containers to deploy applications is called containerization.
                Containers are not new, but their use for easily deploying
                applications is. Docker is a container management service. The
                keywords of Docker are develop, ship and run anywhere. The whole
                idea of Docker is for developers to easily develop applications,
                ship them into containers which can then be deployed anywhere.
                The initial release of Docker was in March 2013 and since then,
                it has become the buzzword for modern world development,
                especially in the face of Agile-based projects.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Kubernates width="100px" height="100px" />
              </Typography>
              <Typography
                type={'headline'}
                className={classnames(classes.content)}
              >
                Kubernetes is an open-source system for automating deployment,
                scaling, and management of containerized applications. It groups
                containers that make up an application into logical units for
                easy management and discovery. Kubernetes builds upon 15 years
                of experience of running production workloads at Google,
                combined with best-of-breed ideas and practices from the
                community.
              </Typography>
            </div>
            <div className={classes.flex}>
              <Typography
                className={classnames(classes.headline, classes.headingColor)}
              >
                <Git width="160px" height="60px" />
              </Typography>
              <Typography type={'headline'} className={classes.content}>
                <ul>
                  <li>Git</li>
                  <li>
                    Git Flow : It is a extended version of Git which helps in
                    increasing the functionality of git and to work in a better
                    way.
                  </li>
                </ul>
              </Typography>
            </div>
          </CardContent>
        </Card>
      </div>
    </Grid>

    {/* Pricing code starts here */}
    <Grid>
      <Card>
        <CardContent className={classnames(classes.content, classes.apply)}>
          We have taken out all the confusion out of the equation, we will be
          providing you with What to learn and How to learn, in a systematic
          manner, moving from the Beginner Level, all the way to the Expert
          Level.<br />
          We are going to provide you with a program which will include all the
          technologies you require, together in a three-level system to become a
          professional full stack developer at a minimal price which otherwise,
          in the market, will cost you much more.<br />
          If you desire, you can also pick one level at a time, the details are
          provided below :
        </CardContent>
      </Card>
    </Grid>
    <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
      <CardContent className={classes.level}>
        <Chip
          avatar={<Avatar>DTP</Avatar>}
          label="Level 1"
          className={classes.chip}
        />
        <ol>
          <li>HTML5</li>
          <li>
            CSS3
            <ul>
              <li>Flexbox</li>
            </ul>
          </li>
          <li>
            JavaScript
            <ul>
              <li> ES5, ES6, ES7, ES8</li>
              <li> ES-Next</li>
              <li> Extended JavaScript (JSX)</li>
            </ul>
          </li>
          <li>React.js</li>
          <li>Redux,js</li>
          <li>React Router v3</li>
          <li>Node.js</li>
          <li>Hapi.js</li>
          <li>MongoDB</li>
          <li>Basic Git</li>
        </ol>
        <Chip
          label="Price : 5000 INR."
          className={classes.chip}
          deleteIcon={<DoneIcon />}
        />
      </CardContent>
    </Grid>
    <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
      <CardContent className={classes.level}>
        <Chip
          avatar={<Avatar>DTP</Avatar>}
          label="Level 2"
          className={classes.chip}
        />
        <ol>
          <li>Inline CSS</li>
          <li>CSS in JS</li>
          <li>React Router v4</li>
          <li>Material UI Basics</li>
          <li>Babel</li>
          <li>APIs</li>
          <li>JWT/Token Authentication</li>
          <li>
            Advanced Git
            <ul>
              <li>Git Flow</li>
            </ul>
          </li>
          <li>MongoDB</li>
          <li>Markdown</li>
        </ol>
        <Chip
          label="Price : 6000 INR."
          className={classes.chip}
          deleteIcon={<DoneIcon />}
        />
      </CardContent>
    </Grid>
    <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
      <CardContent className={classes.level}>
        <Chip
          avatar={<Avatar>DTP</Avatar>}
          label="Level 3"
          className={classes.chip}
        />
        <ol>
          <li>Advanced React.js, Redux.js, React Router</li>
          <li>Advance Material Design concepts</li>
          <li>Webpack</li>
          <li>Server-Side Rendering</li>
          <li>Templating Engine</li>
          <li>
            DevOps (deploying application on multiple machines in cloud for
            production)
            <ul>
              <li>
                Docker(Container technology for developing on different machines
                in the same environment)
              </li>
            </ul>
          </li>
        </ol>
        <Chip
          label="Price : 7000 INR."
          className={classes.chip}
          deleteIcon={<DoneIcon />}
        />
      </CardContent>
    </Grid>
    <CardContent>
      Offer : If you choose to take up all the three levels at once, you will
      get a discount of Rs.3000/-
    </CardContent>
    <Button
      raised
      className={classes.button}
      color="accent"
      onClick={() => {
        const href = ' https://goo.gl/crvLxC';
        window.open(href);
      }}
    >
      {'Apply Now  '}
      <SendIcon />
    </Button>
  </Grid>
);

DevelopersTraining.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styleSheet)(DevelopersTraining);
