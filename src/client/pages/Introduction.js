/**
 * This is the introduction page of mayash.io
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';
// import Collapse from 'material-ui/transitions/Collapse';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  gridItem: {
    paddingBottom: '5px',
  },
  cardHeader: {
    textAlign: 'center',
    height: '20vh',
    backgroundImage:
      'url("https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg")',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  paragraph: {
    textAlign: 'center',
  },
  // expand: {
  //   transform: 'rotate(0deg)',
  //   transition: theme.transitions.create('transform', {
  //     duration: theme.transitions.duration.shortest,
  //   }),
  // },
  content: {
    lineHeight: '25px',
    fontSize: '1rem',
    paddingBottom: '1rem',
    [theme.breakpoints.up('xl')]: {
      paddingLeft: '12em',
      paddingRight: '12em',
    },
    [theme.breakpoints.down('xl')]: {
      paddingLeft: '12em',
      paddingRight: '12em',
    },
    [theme.breakpoints.down('lg')]: {
      paddingLeft: '8em',
      paddingRight: '8em',
    },
    [theme.breakpoints.down('md')]: {
      paddingLeft: '3em',
      paddingRight: '3em',
    },
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '4px',
      paddingRight: '4px',
    },
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '4px',
      paddingRight: '4px',
    },
  },
  about: {
    [theme.breakpoints.up('xl')]: {
      paddingLeft: '12rem',
      paddingRight: '12rem',
    },
    [theme.breakpoints.down('xl')]: {
      paddingLeft: '12rem',
      paddingRight: '12rem',
    },
    [theme.breakpoints.down('lg')]: {
      paddingLeft: '8rem',
      paddingRight: '8rem',
    },
    [theme.breakpoints.down('md')]: {
      paddingLeft: '3rem',
      paddingRight: '3rem',
    },
    [theme.breakpoints.down('sm')]: {
      paddingLeft: '4px',
      paddingRight: '4px',
    },
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '4px',
      paddingRight: '4px',
    },
  },
  title: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 30,
    },
  },
  subheader: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 16,
    },
  },
  // introductionHeader: {
  //   backgroundImage:
  //     'url("https://storage.googleapis.com/mayash-web/drive/26.jpg")',
  //   backgroundPosition: 'center',
  //   backgroundRepeat: 'no-repeat',
  //   [theme.breakpoints.up('xl')]: {},
  //   [theme.breakpoints.down('xl')]: {},
  //   [theme.breakpoints.down('lg')]: {},
  //   [theme.breakpoints.down('md')]: {},
  //   [theme.breakpoints.down('sm')]: {},
  //   [theme.breakpoints.down('xs')]: {},
  // },
  // aboutUsHeader: {
  //   backgroundImage:
  //     'url("https://storage.googleapis.com/mayash-web/drive/27.jpg")',
  //   backgroundAttachment: 'fixed',
  //   backgroundPosition: 'center',
  //   backgroundRepeat: 'no-repeat',
  //   backgroundSize: 'cover',
  //   [theme.breakpoints.up('xl')]: {},
  //   [theme.breakpoints.down('xl')]: {},
  //   [theme.breakpoints.down('lg')]: {},
  //   [theme.breakpoints.down('md')]: {},
  //   [theme.breakpoints.down('sm')]: {},
  //   [theme.breakpoints.down('xs')]: {},
  // },
  // motivationHeader: {
  //   backgroundImage:
  //     'url("https://storage.googleapis.com/mayash-web/drive/28.jpg")',
  //   backgroundAttachment: 'fixed',
  //   backgroundPosition: 'center',
  //   backgroundRepeat: 'no-repeat',
  //   backgroundSize: 'cover',
  //   [theme.breakpoints.up('xl')]: {},
  //   [theme.breakpoints.down('xl')]: {},
  //   [theme.breakpoints.down('lg')]: {},
  //   [theme.breakpoints.down('md')]: {},
  //   [theme.breakpoints.down('sm')]: {},
  //   [theme.breakpoints.down('xs')]: {},
  // },
  // visionHeader: {
  //   backgroundImage:
  //     'url("https://storage.googleapis.com/mayash-web/drive/29.jpg")',
  //   backgroundAttachment: 'fixed',
  //   backgroundPosition: 'center',
  //   backgroundRepeat: 'no-repeat',
  //   backgroundSize: 'cover',
  //   [theme.breakpoints.up('xl')]: {},
  //   [theme.breakpoints.down('xl')]: {},
  //   [theme.breakpoints.down('lg')]: {},
  //   [theme.breakpoints.down('md')]: {},
  //   [theme.breakpoints.down('sm')]: {},
  //   [theme.breakpoints.down('xs')]: {},
  // },
  // missionHeader: {
  //   backgroundImage:
  //     'url("https://storage.googleapis.com/mayash-web/drive/30.jpg")',
  //   backgroundAttachment: 'fixed',
  //   backgroundPosition: 'center',
  //   backgroundRepeat: 'no-repeat',
  //   backgroundSize: 'cover',
  //   [theme.breakpoints.up('xl')]: {},
  //   [theme.breakpoints.down('xl')]: {},
  //   [theme.breakpoints.down('lg')]: {},
  //   [theme.breakpoints.down('md')]: {},
  //   [theme.breakpoints.down('sm')]: {},
  //   [theme.breakpoints.down('xs')]: {},
  // },
});

class Introduction extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    const Title = ({ children }) => (
      <Typography className={classes.title} type="display3">
        {children}
      </Typography>
    );
    const SubHeader = ({ children }) => (
      <Typography className={classes.subheader} type="headline">
        {children}
      </Typography>
    );

    return (
      <div>
        <Card raised>
          <CardHeader
            title={<Title>MAYASH</Title>}
            subheader={
              <SubHeader>
                Our mission is to provide world class education to everyone.
              </SubHeader>
            }
            className={classes.cardHeader}
            // aria-expanded={intro}
            aria-label="Introduction"
          />
          <CardContent>
            {/* <Typography
                type="title"
                align="center"
                className={classes.paragraph}
                gutterBottom
              >
                  Are you satisfied with the current education system?
              </Typography>
              <Typography
                type="title"
                align="center"
                className={classes.paragraph}
                gutterBottom
              >
                  Are you fed-up of pressurized only for marks and grades?
              </Typography> */}
            <hr />
            <Typography
              type="body1"
              align="justify"
              className={classes.content}
              gutterBottom
            >
              Mayash is an educational platform based on the concept of learning
              and sharing. Mayash strives to provide quality content in every
              field of education to the students,incorporating the teaching
              material from the various professors of this country and the
              latest stuff from the organizations. Mayash is a one stop solution
              for all your requirements, doubts and queries. We have the top
              notch people of the country who are ready to help you in the best
              possible way.
            </Typography>
          </CardContent>
          {/* </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
          xl={12}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={<Title>About Us</Title>}
              subheader={
                <SubHeader>
                  Know our ambition and passion for this country.
                </SubHeader>
              }
              className={classnames(classes.cardHeader,
                classes.aboutUsHeader, classes.expand,
              )}
              aria-expanded={aboutUs}
              aria-label="About Us"
            /> */}
          <CardContent>
            <Typography
              type="display1"
              className={classes.about}
              // align="left"
            >
              About Us
              <hr />
            </Typography>
            <Typography
              type="body1"
              className={classes.content}
              align="justify"
            >
              Mayash is a team of dedicated people who have made it their
              mission to solve some of the major problems which our society is
              facing right now. We are making a simple but effective platform
              for learning and sharing knowledge consisting of two schemas :<br />
            </Typography>
            <Typography type={'display3'} className={classes.content}>
              1. Learn what you need.<br />
              2. share what you can.<br />
            </Typography>
            <Typography className={classes.content}>
              We are working towards the betterment of our educational system,
              where we will provide updated knowledge and the latest
              technologies for users to help them upgrade their skills and be
              ahead in this competitive ecosystem.
            </Typography>
            <Typography
              type="body1"
              className={classes.content}
              align="justify"
            >
              Founded by an IIT Dhanbad dropout and supported by a team of
              IITians, Mayash is focussed on the improvement of education system
              of our country by making the users technically competent and
              hence, making them better than the competition.
            </Typography>
          </CardContent>
          <CardContent>
            <Typography
              type="body1"
              className={classes.content}
              component="div"
              align="justify"
            >
              <Typography type="title">
                Persisting drawbacks of current Education system :
                <hr />
              </Typography>
              <ul>
                <li>
                  Quantitative orientation of Education system where Quality is
                  compromised.
                </li>
                <li>
                  Decade old syllabus whereas the technology gets manifold
                  changes every year
                </li>
                <li>
                  Education is focused more on theoretical aspect rather than on
                  practical understanding.
                </li>
                <li>
                  Students prefer to take tuitions, though there are schools
                  present, just to stay ahead in the rat race of getting good
                  marks.
                </li>
              </ul>
            </Typography>
            <Typography
              type="body1"
              className={classes.content}
              component="div"
            >
              <Typography type="title">
                Solution - Mayash:
                <hr />
              </Typography>
              <ul>
                <li>
                  Mayash is a platform which will cover all the fields of
                  education, for all age groups.
                </li>
                <li>
                  We are connecting students/teachers/professionals from
                  multiple colleges/ universities/ corporates on a single
                  platform where they can easily learn and share their
                  knowledge.
                </li>
                <li>
                  We will create a discussion channel where students can get
                  their doubts and queries cleared.
                </li>
              </ul>
            </Typography>
          </CardContent>
          {/* </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
          xl={12}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={<Title>Motivation</Title>}
              subheader={
                <SubHeader>
                  To start something good motivation is necessary.
                </SubHeader>
              }
              className={classnames(classes.cardHeader,
                classes.motivationHeader, classes.expand,
              )}
            /> */}
          <CardContent>
            <Typography type="display1" className={classes.about}>
              Why Mayash?
              <hr />
            </Typography>
            <Typography
              type="body1"
              className={classes.content}
              component="div"
            >
              Today the We are facing the following problems :
              <ul>
                Students:-
                <li>Costly Education</li>
                <li>Lack of practical understanding.</li>
                <li>High Student-Teacher ratio.</li>
                <li>Outdated curriculum.</li>
                <li>Rapid change in technology</li>
                <li>Lack of competent teachers.</li>
                <li>Lack of field specialist mentors.</li>
                <li>Lack of Industrial exposure.</li>
                <li>Language barrier..</li>
              </ul>
              <br />
              <ul>
                professors:-
                <li>Staying updated with latest technologies is difficult.</li>
                <li>High academic load.</li>
                <li>Difficult to address to a large no. of students</li>
              </ul>
              <br />
              <ul>
                Educational Institutes:-
                <li>Difficult to ensure skilled faculties.</li>
                <li>It is costly to hire Industrial experts.</li>
                <li>It is costly to create skill development programs.</li>
              </ul>
              <br />
              <ul>
                Organisations:-
                <li> It is costly to train freshers.</li>
                <li>Hiring procedure is long.</li>
              </ul>
            </Typography>
          </CardContent>
          {/* </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
          xl={12}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={<Title>Vision</Title>}
              subheader={
                <SubHeader>
                  Our vision is to make world a better place to live.
                </SubHeader>
              }
              className={classnames(classes.cardHeader,
                classes.visionHeader, classes.expand,
              )}
            /> */}
          {/* <CardContent>
            <Typography type="body1" className={classes.content}>
                  Providing World class quality education to everyone in
                affordable price. We are making a simple but effective
                platform for learning and sharing knowledge where people
                can learn, share and grow.
            </Typography>
          </CardContent> */}
          {/* </Card>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={12}
          lg={12}
          xl={12}
          className={classes.gridItem}
        >
          <Card raised>
            <CardHeader
              title={<Title>Mission</Title>}
              subheader={
                <SubHeader>
                  Our mission is to provide world class education to eveyone.
                </SubHeader>
              }
              className={classnames(classes.cardHeader,
                classes.missionHeader, classes.expand,
              )}
              onClick={() => this.setState({ mission: !mission })}
            /> */}
          <CardContent>
            <Typography type="body1" className={classes.content}>
              Aim of mayash is to solve all the above problems. Our Vision is to
              improve the education system of our country. Our mission is to
              bring out best skills of students as well as teachers so that they
              all can grow collaboratively and bring out the best from every
              individual connected with Mayash.
            </Typography>
            <hr />
            <hr />
          </CardContent>
        </Card>
        <NavigationButtonNext to={'/services'} />
      </div>
    );
  }
}

Introduction.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Introduction);
