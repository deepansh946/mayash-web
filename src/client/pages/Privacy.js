/**
 * This component represents Donation page for mayash
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';

import Card, { CardHeader, CardMedia, CardContent } from 'material-ui/Card';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  cardHeader: {
    textAlign: 'center',
    padding: '3em',
  },
  media: {
    height: 300,
    width: 300,
  },
  medium: {
    padding: '40px',
  },
  cardContent: {
    paddingLeft: '8em',
    textAlign: 'justify',
    paddingRight: '8em',
    [theme.breakpoints.down('md')]: {
      paddingLeft: '2em',
      paddingRight: '2em',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '10px',
    },
  },
});

class Privacy extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card>
          <CardHeader
            title="PRIVACY STATEMENT"
            className={classes.cardHeader}
          />
          <CardContent className={classes.cardContent}>
            <Typography variant="headline" type="display">
              <ol>
                <li>WHAT DO WE DO WITH YOUR INFORMATION?</li>
                <li>CONSENT</li>
                <li> DISCLOSURE </li>
                <li>PAYMENT</li>
                <li>THIRD-PARTY SERVICES</li>
                <li>SECURITY</li>
                <li>COOKIES</li>
                <li>AGE OF CONSENT</li>
                <li>CHANGES TO THIS PRIVACY POLICY</li>
              </ol>
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 1 - WHAT DO WE DO WITH YOUR INFORMATION?
            </Typography>
            <Typography variant="headline" type="display">
              Any personal information provided to or gathered by Hbarve
              Information technologies (OPC) Private Limited under this Privacy
              Notice will be stored and controlled by Mayash i.e Hbarve
              Information technologies (OPC) Private Limited
              <ul>
                <li>
                  Information provided in the profile of your account wich
                  includes User ID, Name, Bio, Linkedin, Facebook, Google plus,
                  Instagram, Twitter, Skype and your Resume will be shared
                  publicly, it can be accessed by any one using this website
                </li>
                <li>
                  Our Privacy Policy explains how we collect, use, share and
                  protect information in relation to our mobile services, web
                  site, and any software provided on or in connection with
                  Mayash services (collectively, the "Service"), and your
                  choices about the collection and use of your information.
                </li>
                <li>
                  By using our Service you understand and agree that we are
                  providing a platform for you to post content, courses,
                  including photos, comments and other materials ("User
                  Content"), to the Service and to share User Content publicly.
                  This means that other Users may search for, see, use, or share
                  any of your User Content that you make publicly available
                  through the Service, consistent with the terms and conditions
                  of this Privacy Policy and our Terms of Use (which can be
                  found at
                </li>
                <li>
                  We give you the power to share as part of our mission to make
                  the world more open and connected..Sharing of the courses will
                  be done only by your consent.
                </li>
              </ul>
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 2 - CONSENT
            </Typography>
            <Typography variant="headline" type="display">
              How do you get my consent? When you provide us with personal
              information to complete a transaction, verify your credit card,,
              we imply that you consent to our collecting it and using it for
              that specific reason only. If we ask for your personal information
              for a secondary reason, like marketing, we will either ask you
              directly for your expressed consent, or provide you with an
              opportunity to say no. How do I withdraw my consent? If after you
              opt-in, you change your mind, you may withdraw your consent for us
              to contact you, for the continued collection, use or disclosure of
              your information, at anytime, by contacting us at Mayash i.e
              Hbarve Information Technology (OPC) Private Limited, Contact
              Email- contact-us@mayash.io or website https://mayash.io
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 3 - DISCLOSURE
            </Typography>
            <Typography variant="headline" type="display">
              We may disclose your personal information if we are required by
              law to do so or if you violate our Terms of Service.
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 4 - PAYMENT
            </Typography>
            <Typography variant="headline" type="display">
              We use Razorpay for processing payments. We/Razorpay do not store
              your card data on their servers. The data is encrypted through the
              Payment Card Industry Data Security Standard (PCI-DSS) when
              processing payment. Your purchase transaction data is only used as
              long as is necessary to complete your purchase transaction. After
              that is complete, your purchase transaction information is not
              saved. Our payment gateway adheres to the standards set by PCI-DSS
              as managed by the PCI Security Standards Council, which is a joint
              effort of brands like Visa, MasterCard, American Express and
              Discover. PCI-DSS requirements help ensure the secure handling of
              credit card information by our store and its service providers.
              For more insight, you may also want to read terms and conditions
              of razorpay on https://razorpay.com
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 5 - THIRD-PARTY SERVICES
            </Typography>
            <Typography variant="headline" type="display">
              <ul>
                <li>
                  In general, the third-party providers used by us will only
                  collect, use and disclose your information to the extent
                  necessary to allow them to perform the services they provide
                  to us.
                </li>
                <li>
                  However, certain third-party service providers, such as
                  payment gateways and other payment transaction processors,
                  have their own privacy policies in respect to the information
                  we are required to provide to them for your payment-related
                  transactions.
                </li>
                <li>
                  For these providers, we recommend that you read their privacy
                  policies so you can understand the manner in which your
                  personal information will be handled by these providers.
                </li>
                <li>
                  When you click on links to advertisement, they may direct you
                  away from our site. We are not responsible for the privacy
                  practices of other sites and encourage you to read their
                  privacy statements.
                </li>
              </ul>
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 6 - SECURITY
            </Typography>
            <Typography variant="headline" type="display">
              To protect your personal information, we take reasonable
              precautions and follow industry best practices to make sure it is
              not inappropriately lost, misused, accessed, disclosed, altered or
              destroyed.
            </Typography>
            <Typography variant="headline" type="headline">
              SECTION 7 - COOKIES
            </Typography>
            <Typography variant="headline" type="display">
              We use cookies to maintain session of your user. It is not used to
              personally identify you on other websites.
            </Typography>

            <Typography variant="headline" type="headline">
              SECTION 8 - AGE OF CONSENT
            </Typography>
            <Typography variant="headline" type="display">
              By using this site, you represent that you are at least the age of
              majority in your state or province of residence, or that you are
              the age of majority in your state or province of residence and you
              have given us your consent to allow any of your minor dependents
              to use this site.
            </Typography>

            <Typography variant="headline" type="headline">
              SECTION 9 - CHANGES TO THIS PRIVACY POLICY
            </Typography>
            <Typography variant="headline" type="display">
              We reserve the right to modify this privacy policy at any time, so
              please review it frequently. Changes and clarifications will take
              effect immediately upon their posting on the website. If we make
              material changes to this policy, we will notify you here that it
              has been updated, so that you are aware of what information we
              collect, how we use it, and under what circumstances, if any, we
              use and/or disclose it. If our store is acquired or merged with
              another company, your information may be transferred to the new
              owners so that we may continue to sell products to you.
            </Typography>

            <Typography variant="headline" type="headline">
              QUESTIONS AND CONTACT INFORMATION
            </Typography>
            <Typography variant="headline" type="display">
              If you would like to: access, correct, amend or delete any
              personal information we have about you, register a complaint, or
              simply want more information contact our Privacy Compliance
              Officer at hbarve1@mayash.io or by mail at H-no 273 NEW AG
              co-operative, colony, Kadru, Ranch, 834002 [Re: Privacy Compliance
              Officer]
            </Typography>
          </CardContent>
        </Card>
        <NavigationButtonNext to={'/developers-training'} />
      </div>
    );
  }
}

Privacy.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Privacy);
