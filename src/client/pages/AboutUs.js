/**
 * This Component contains interroduction about MAYASH
 */
import React, { Component } from 'react';
import Link from 'react-router-dom/Link';
import PropTypes from 'prop-types';
// import * as colors from 'material-ui/styles/colors';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Card, { CardHeader, CardMedia, CardContent, CardActions } from 'material-ui/Card';

import AppLayout from '../containers/AppLayout';
import NavigationButtonNext from '../components/NavigationButtonNext';

/**
 * material ui have inline styling  property.
 * this newly created styleshhet can be override on our component
 * [styleSheet description]
 * @type {[type]}
 */
const styleSheet = createStyleSheet('AboutUs', theme => {
  return {
    root: {
      flexGrow: 1,
      padding: '5%',
    },
    gridContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    cardMediaImage: {
      width: '100%',
      height: 'auto',
    },
    cardHeader: {
      textAlign:'center',
    },
    paragraph: {
      padding: '2%',
      textAlign: 'center',
    },

    /*Breakpoints are the point a which your sites content will respond to provide the user with the best possible layout on any device.*/
    [theme.breakpoints.up('xl')]: {

    },
    //control between medium and large screen
    [theme.breakpoints.between('md', 'lg')]: {

    },
    //control between small and medium
    [theme.breakpoints.between('sm', 'md')]: {

    },
    //control between extra small and small

    [theme.breakpoints.between('xs', 'sm')]: {

    },
    [theme.breakpoints.down('xs')]: {

    },
  };
});

class AboutUs extends Component {
  render () {
    const classes = this.props.classes;

    return (
      <div className={classes.root}>
        <Grid container className={classes.gridContainer}>
          /* Only 12 col can be rest in  a single row
             xs={12}=> on extra small screen this will be fill in a row(12)
             xl={4} => on extra large screen this will be fit in a row with three column of 4 */
          <Grid item xs={12} sm={8} md={8} lg={6} xl={4}>
            <Card raised>
              {/*
                <CardMedia>
                  <img
                    src={'/public/photos/sample-linkedin-banner.png'}
                    alt="About Us"
                    className={classes.cardMediaImage}
                  />
                </CardMedia>
              */}
              <CardHeader
                title="About Us"
                subheader="Our mission is to provide world class education to eveyone."
                className={classes.cardHeader}
              />
              <CardContent>
                <Typography className={classes.paragraph}>
                  Mayash is a Team of IITians, committed to solve one of the major
                  problem in our society.
                </Typography>
                <Typography className={classes.paragraph}>
                  asdfadf ads f ads f a dsf a sdf  ad sfa d f adsfadfad sf a df a df
                  adf a sdfasdf  adf  ad f ad f ad f a df ad fa df adf ad f asdfadf
                </Typography>
                <Typography className={classes.paragraph}>
                  sdf asd f ad f ad f a dsfa dsf adf asdfafadf a f a df da sfadf
                  adsf ad f ad f ads fa df a sdf sd
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        </Grid>
        <NavigationButtonNext to={'/our-vision'} />
      </div>
    );
  }
}

/**
 * This will validate all the incoming props from parent component
 * to AboutUS component.
 * @type {Object}
 */
AboutUs.propTypes = {
  classes: PropTypes.object.isRequired,
};

/**
 * Error Page component is exported with inline style
 * API  = withStyles(styles, [options]) => Higher-order Component
 */
export default withStyles(styleSheet)(AboutUs);
