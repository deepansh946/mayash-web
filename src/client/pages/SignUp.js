/**
 * Signup page component
 * This page will handle signup functinality
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardActions } from 'material-ui/Card';

const styles = (theme) => ({
  root: {
    flex: '1 0 100%',
    padding: '1%',
  },
  cardHeader: {
    textAlign: 'center',
  },
  cardContent: {
    textAlign: 'center',
  },
  textField: {
    width: '100%',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
});

const SignUp = ({ classes }) => (
  <Grid container className={classes.root}>
    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
      <Grid container gutter={0}>
        <Grid item xs={12} sm={6} md={6} lg={4} xl={4}>
          <Card raised>
            <CardHeader title={'Sign Up'} className={classes.cardHeader} />
            <CardActions>
              <Button>Sign Up Using Facebook</Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);

SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignUp);
