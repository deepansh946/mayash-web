/**
 * This component represents Donation page for mayash
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import PhoneIcon from 'material-ui-icons/Phone';
import EmailIcon from 'material-ui-icons/Email';

import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';

import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  cardHeader: {
    textAlign: 'center',
    padding: '1em',
  },
  media: {
    height: 300,
    width: 300,
  },
  medium: {
    padding: '40px',
  },
  cardContent: {
    paddingLeft: '8em',
    textAlign: 'justify',
    paddingRight: '8em',
    [theme.breakpoints.down('md')]: {
      paddingLeft: '2em',
      paddingRight: '2em',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '10px',
    },
  },
});

class Refund extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Card>
          <CardHeader title="Refund Policy" className={classes.cardHeader} />
          <CardContent className={classes.cardContent}>
            <Typography variant="headline" type="headline">
              Thank you for using Mayash. Please read this policy carefully.
              This is the Return and Refund Policy of Mayash. Return and Refund
              Policy for Mayash based on the
              <a href="https://termsfeed.com/blog/sample-return-policy-ecommerce-stores/">
                {' '}
                Return Policy example from TermsFeed.
              </a>
            </Typography>
            <CardHeader
              title="Digital products"
              className={classes.cardHeader}
            />
            <Typography variant="headline" type="headline">
              We issue refunds for digital services within 7 days of the
              original purchase of the Services applicable in certain cases.
              <ul>
                <li>When Subscription for service is made by mistake, </li>
              </ul>
              We recommend contacting us for assistance if you experience any
              issues receiving or downloading our products.
            </Typography>
            <CardHeader
              title="Digital Services"
              className={classes.cardHeader}
            />
            <Typography variant="headline" type="headline">
              We do not issue refunds for digital Services once the order is
              confirmed and the Service is sent. We recommend contacting us for
              assistance if you experience any issues receiving or Using our
              services.
            </Typography>
            <CardHeader title="Contact us" className={classes.cardHeader} />
            <Typography variant="headline" type="headline">
              If you have any questions about our Returns and Refunds Policy,
              please contact us:
              <Typography>
                <PhoneIcon />
                <a href="tel:8770693644" className={classes.link}>
                  {'  '}
                  +91-8770693644
                </a>
              </Typography>
              <Typography type="headline">
                <EmailIcon />
                <a href="mailto:hbarve1@mayash.io" className={classes.link}>
                  {'  '}
                  hbarve1@mayash.io
                </a>
              </Typography>
              {/* By visiting this page on */}
              {/* our website: https://goo.gl/forms/lBO2OIDFryXUVjMw1 */}
            </Typography>
          </CardContent>
        </Card>
        <NavigationButtonNext to={'/contact-us'} />
      </div>
    );
  }
}

Refund.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Refund);
