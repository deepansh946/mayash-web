/**
 * Sign In Page Component.
 * This page will hadle Sign In functinality.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import withStyles from 'material-ui/styles/withStyles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';

import FlexGrow from '../components/FlexGrow';
import GoogleIcon from '../../lib/mayash-icons/Google';

import apiSignIn from '.././api/users/signIn';
import actionSignIn from '../actions/users/signIn';

const styles = (theme) => ({
  root: {
    flexGrow: '1',
  },
  gridContainer: {
    padding: '2px',
    height: 'calc(100vh - 90px)',
  },
  card: {
    padding: '2px',
  },
  cardHeader: {
    textAlign: 'center',
  },
  cardContent: {
    textAlign: 'center',
  },
  cardActions: {},
  textField: {
    width: '95%',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  message: {
    color: 'green',
    textAlign: 'center',
  },
  buttonGoogle: {
    backgroundColor: theme.palette.primary[500],
    borderRadius: '50px',
    marginTop: '40px',
    width: '230px',
  },
});

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      message: '',
    };
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChangeUsername = (e) => this.setState({ username: e.target.value });
  onChangePassword = (e) => this.setState({ password: e.target.value });

  async onSubmit() {
    try {
      const { username, password } = this.state;

      if (username.length === 0 || password.length === 0) {
        this.setState({ message: 'Username or Password must not be empty' });
        setTimeout(() => this.setState({ message: '' }), 1500);
        return;
      }

      const { statusCode, error, message, payload } = await apiSignIn({
        username,
        password,
      });

      if (statusCode >= 400) {
        this.setState({ message: error });
        setTimeout(() => this.setState({ message: '' }), 1500);
        return;
      }

      this.setState({ message });
      this.props.actionSignIn({ ...payload });
      this.props.history.push('/');
    } catch (e) {
      console.error(e);
    }
  }

  onClickGoogleAuth = () => {
    location.href = '/api/auth/google';
  };

  render() {
    const { classes } = this.props;
    const { message } = this.state;

    return (
      <Grid container spacing={0} className={classes.root}>
        <Grid item xs={12}>
          <Grid
            container
            spacing={0}
            direction="row"
            justify="center"
            alignItems="center"
            className={classes.gridContainer}
          >
            <Grid item xs={11} sm={6} md={6} lg={4} xl={4}>
              <Card raised className={classes.card}>
                <div className={classes.back}>
                  <CardHeader
                    title={'Sign In'}
                    className={classes.cardHeader}
                  />
                </div>
                <CardContent className={classes.cardContent}>
                  <TextField
                    id="username"
                    label="username"
                    value={this.state.name}
                    onChange={this.onChangeUsername}
                    className={classes.textField}
                    margin="normal"
                    autoFocus
                  />
                  <TextField
                    id="password"
                    label="password"
                    type="password"
                    className={classes.textField}
                    value={this.state.name}
                    onChange={this.onChangePassword}
                    margin="normal"
                  />
                </CardContent>
                <CardActions>
                  <div className={classes.flexGrow} />
                  <Button raised onClick={this.onSubmit} color="accent">
                    Sign in
                  </Button>
                </CardActions>
                {message ? (
                  <CardContent className={classes.message}>
                    {message}
                  </CardContent>
                ) : null}
                <hr />
                <CardActions className={classes.cardActions}>
                  <FlexGrow />
                  <Button
                    raised
                    onClick={this.onClickGoogleAuth}
                    className={classes.buttonGoogle}
                  >
                    Sign In with <GoogleIcon style={{ paddingLeft: '20px' }} />
                  </Button>
                  <FlexGrow />
                </CardActions>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

SignIn.propTypes = {
  // match: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  actionSignIn: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionSignIn,
    },
    dispatch,
  );

export default withRouter(
  connect(null, mapDispatchToProps)(withStyles(styles)(SignIn)),
);
