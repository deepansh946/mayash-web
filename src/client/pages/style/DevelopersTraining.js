/**
 * /*
 *   This Style file is for Developers Training
 */

const styles = (theme) => ({
  root: {
    justifyContent: 'center',
    textAlign: 'justify',
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    padding: '5px',
    paddingBottom: '3em',
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      paddingBottom: '2em',
    },
  },
  flexChild: {
    margin: 'auto',
    color: '#6d6d6d',
  },
  cardHeaderTop: {
    textAlign: 'center',
  },
  mediaTitle: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
  },
  subheader: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },
  },
  header: {
    paddingLeft: '9em',
    paddingRight: '9em',
    lineHeight: '35px',
    fontSize: '1.2em',
    [theme.breakpoints.down('lg')]: {
      paddingLeft: '1em',
      paddingRight: '1em',
      lineHeight: '25px',
      fontSize: '1em',
    },
    [theme.breakpoints.down('sm')]: {
      padding: 0,
      lineHeight: '25px',
      fontSize: '1em',
    },
  },
  developersTitle: {
    height: '20vh',
    backgroundSize: 'cover',
    backgroundImage:
      'url("https://storage.googleapis.com/mayash-web/drive/DTP.png")',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
  },
  headline: {
    flex: 1,
    fontSize: '2em',
    margin: 'auto',
    paddingLeft: '9rem',
    [theme.breakpoints.down('lg')]: {
      fontSize: '1.5em',
      paddingLeft: '2rem',
    },
    [theme.breakpoints.down('md')]: {
      fontSize: '1.5em',
      padding: 0,
    },
  },
  content: {
    flex: 3,
    lineHeight: '35px',
    fontSize: '1.2em',
    padding: '5px',
    paddingRight: '9em',
    [theme.breakpoints.down('lg')]: {
      padding: '5px',
      paddingLeft: '2rem',
      lineHeight: '25px',
      fontSize: '1em',
    },
    [theme.breakpoints.down('md')]: {
      padding: 0,
      lineHeight: '25px',
      fontSize: '1em',
    },
  },
  headingColor: {
    color: '#6d6d6d',
  },
  javaScript: {
    background:
      'linear-gradient(to right, rgba(255,190,0,1), rgba(255,255,0,1))',
    display: 'inline',
    paddingTop: '40px',
    paddingLeft: '5px',
    color: 'black',
    transition: 'all 2s',
    [theme.breakpoints.down('md')]: {
      paddingTop: '20px',
    },
    '&:hover': {
      background:
        'linear-gradient(to right, rgba(255,255,0,1), rgba(255,190,0,1))',
    },
  },
  level: {
    lineHeight: '35px',
    fontSize: '1.2em',
    // padding: '5px',
    // paddingRight: '9em',
    // paddingLeft: '9em',
    [theme.breakpoints.down('lg')]: {
      padding: '5px',
      paddingLeft: '2rem',
      lineHeight: '25px',
      fontSize: '1em',
    },
    [theme.breakpoints.down('md')]: {
      paddingLeft: '6px',
      paddingRight: '6px',
      lineHeight: '25px',
      fontSize: '1em',
    },
  },
  chip: {
    margin: theme.spacing.unit,
  },
  apply: {
    margin: 'auto',
    paddingLeft: '9rem',
    [theme.breakpoints.down('lg')]: {
      paddingLeft: '2rem',
    },
    [theme.breakpoints.down('md')]: {
      padding: '15px',
    },
    [theme.breakpoints.down('md')]: {
      padding: '5px',
    },
  },
  button: {
    position: 'fixed',
    right: '2%',
    bottom: '4%',
    [theme.breakpoints.down('md')]: {
      right: '2%',
      bottom: '2%',
    },
    [theme.breakpoints.down('sm')]: {
      right: '2%',
      bottom: '1%',
    },
  },
});

export default styles;
