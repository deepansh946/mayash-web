/**
 * Team page component
 * This component contains info about mayash team
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';

import TeamMember2 from '../components/TeamMember2';
import NavigationButtonNext from '../components/NavigationButtonNext';

const styles = (theme) => ({
  root: {},
  gridItem: {
    padding: '1%',
  },
  cardHeader: {
    textAlign: 'center',
  },
  teamTitle: {
    height: '20vh',
    backgroundImage:
      'url("https://storage.googleapis.com/mayash-web/drive/12.jpg")',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  title: {
    fontWeight: theme.typography.fontWeightMedium,
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('sm')]: {
      fontSize: 30,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 30,
    },
  },
  subheader: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: 16,
    },
  },
});

class Team extends Component {
  constructor(props) {
    super(props);
    this.state = {
      members: [
        {
          name: 'Himank Barve',
          avatar:
            'https://storage.googleapis.com/mayash-web/drive/' +
            'team/Himank-Barve.jpg',
          role: 'Founder & CEO',
          description:
            'Science & Technolgy Lover, Spiritual, Computer' +
            ' Geek. Dropout from IIT Dhanbad.',
          facebook: 'hbarve1',
          instagram: 'hbarve1',
          linkedin: 'hbarve1',
          twitter: 'hbarve1',
          email: 'hbarve1@mayash.io',
        },
        {
          name: 'Gaurav Goyal',
          avatar:
            'https://storage.googleapis.com/mayash-web/drive/' +
            'team/Gaurav-Goyal.jpg',
          role: 'Mentor',
          description:
            'B.Tech. from IIT Dhanbad, Software engineering' +
            ' @Ericssion. Faculty @The Art of Living',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: 'goyal.gaurav27@gmail.com',
        },
        {
          name: 'Avinash Yadav',
          avatar:
            'https://storage.googleapis.com/mayash-web/drive/' +
            'team/Avinash-Yadav.jpg',
          role: 'Mentor',
          description:
            'M.Tech. from IIT Dhanbad, MBA from IIT Bombay.' +
            ' Customer Relationship Manager at ICICI Bank',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: 'avinash.yadav@sjmsom.in',
        },
        {
          name: 'Ankita Varma',
          avatar: '',
          role: 'Mentor',
          description: '',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: 'ankitaverma2203@gmail.com',
        },
        {
          name: 'Vaibhav Khaitan',
          avatar: '',
          role: 'Mentor',
          description:
            'A techsmith currently building a real estate' +
            ' startup and running a digital marketing company. Also teaches' +
            ' The Art of Living - Happiness Program.',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: '',
        },
        {
          name: 'Shreya Sinha',
          avatar:
            'https://storage.googleapis.com/mayash-web/drive/' +
            'team/Shreya-Sinha.jpg',
          role: 'Management',
          description:
            'Passionate about exploring new opportunities and' +
            ' ideas. Completed MBA in HR from pune university, with 1 year' +
            ' of corporate experience.',
          facebook: 'shreya.sinha.12576',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: 'shreyashe283@gmail.com',
        },
        {
          name: 'Shubham Maurya',
          avatar: '',
          role: 'CTO',
          description: 'Student @IIT Dhanbad',
          facebook: 'code.shaurya',
          instagram: '',
          linkedin: 'codeshaurya',
          twitter: 'code_shaurya',
          email: 'code.shaurya@gmail.com',
        },

        {
          name: 'Deepansh Bhargava',
          avatar: '',
          role: 'Developer',
          description:
            'Student at MITS Gwalior with specialization in' +
            ' Computer Science, a dedicated coder who will learn every skill' +
            ' which comes in his way towards excellence.',
          facebook: 'deepansh.alien',
          instagram: 'd33p4nsh/',
          linkedin: 'deepansh-bhargava-22463411b/',
          twitter: '',
          email: '',
        },
        {
          name: 'Anshita Vishwakarma',
          avatar: '',
          role: 'Developer',
          description: 'Student @MITS Gwalior',
          facebook: 'anshita946',
          instagram: 'ansh_098',
          linkedin: 'anshita-vishwakarma-48060211b',
          twitter: '',
          email: '',
        },
        {
          name: 'Palash Gupta',
          avatar: '',
          role: 'Developer',
          description: 'Student @MITS Gwalior',
          facebook: '',
          instagram: 'palashgupta7563',
          linkedin: 'palash-gupta-608136142',
          twitter: 'palashg7563',
          email: 'palashg7563@gmail.com',
        },
        {
          name: 'Aman Singh',
          avatar: '',
          role: 'Promotion',
          description: 'Student @IIT Dhanbad',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: '',
        },
        {
          name: 'Trisha Das',
          avatar: '',
          role: 'Promotion',
          description: 'Student @IIT Dhanbad',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: '',
        },
        {
          name: 'Sneha Vishwa',
          avatar: '',
          role: 'Content Writer',
          description:
            "Pursuing Master's in chemistry and an aspirant" +
            ' trying to be a part of pool of highly experienced and talented' +
            ' team of content writers, editors and copywriters.',
          facebook: 'sneha.vishwa.9',
          instagram: 'sneha_vishwa',
          linkedin: '',
          twitter: 'sneha2vishwa',
          email: 'sneha2vishwa@gmail.com',
        },
        {
          name: 'Mansi Saini',
          avatar: '',
          role: 'Content Writer',
          description: '',
          facebook: '',
          instagram: '',
          linkedin: '',
          twitter: '',
          email: '',
        },
        // {
        //   name: 'Ankita Bhattacharya',
        //   avatar: '',
        //   role: 'Promotion',
        //   description: 'Student @IIT Dhanbad',
        //   facebook: '',
        //   instagram: '',
        //   linkedin: '',
        //   twitter: '',
        //   email: '',
        // },
        // {
        //   name: 'Aditya Warade',
        //   avatar: '',
        //   role: 'Promotion',
        //   description: 'Marketing enthusiast and entrepreneur. Mostly' +
        //     ' interested in Technology, stocks, security.',
        //   facebook: '',
        //   instagram: '',
        //   linkedin: '',
        //   twitter: '',
        //   email: '',
        // },
      ],
    };
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid container spacing={0} className={classes.root}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <Card raised>
            <CardHeader
              title={
                <Typography className={classes.title} type="display3">
                  {'Our Team'}
                </Typography>
              }
              subheader={
                <Typography className={classes.subheader} type="headline">
                  {'Success of any initiative, good team is required.'}
                </Typography>
              }
              className={classnames(classes.cardHeader, classes.teamTitle)}
            />
          </Card>
        </Grid>
        {this.state.members.map((m, i) => (
          <Grid
            item
            xs={12}
            sm={6}
            md={4}
            lg={3}
            xl={3}
            className={classes.gridItem}
            key={i + 1}
          >
            <TeamMember2 {...m} />
          </Grid>
        ))}
        <NavigationButtonNext to={'/join-us'} />
      </Grid>
    );
  }
}

Team.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Team);
