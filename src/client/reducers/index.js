/**
 * This component combine all reducers
 *
 * @format
 */

import { combineReducers } from 'redux';

import elements from './elements/index';
import posts from './posts/index';
import courses from './courses/index';
import notes from './notes/index';

export default combineReducers({
  elements,
  posts,
  courses,
  notes,
});
