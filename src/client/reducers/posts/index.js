/**
 * posts reducer component
 * This component will handle all the dispatch actions on posts
 *
 * @format
 */

import {
  POSTS_GET,
  POST_CREATE,
  POST_GET_START,
  POST_GET_ERROR,
  POST_GET_SUCCESS,
  POST_DELETE,
  POST_UPDATE,
} from '../../constants/posts';

import {
  POST_COMMENT_CREATE,
  POST_COMMENTS_GET,
  POST_COMMENT_DELETE,
} from '../../constants/posts/comments';

export default function reducer(state = {}, action) {
  switch (action.type) {
    case POST_CREATE: {
      const { postId, statusCode } = action.payload;

      return {
        ...state,
        [`${postId}`]: {
          ...action.payload,

          statusCode,
          isFetched: true,
          isFetching: false,
          isError: false,

          lastModified: Date.now(),
        },
      };
    }

    /**
     *
     * @todo: optimize this action.
     */
    case POSTS_GET: {
      const { posts } = action.payload;

      let newState = { ...state };

      posts.map((p) => {
        newState = {
          ...newState,
          [`${p.postId}`]: {
            ...p,

            isFetched: true,
            isFetching: false,
            isError: false,
            statusCode: 200,

            lastModified: Date.now(),
          },
        };

        return p;
      });

      return newState;
    }

    case POST_GET_START: {
      const { postId } = action.payload;

      return {
        ...state,
        [`${postId}`]: {
          postId,

          isFetched: false,
          isFetching: true,
          isError: false,

          lastModified: Date.now(),
        },
      };
    }

    case POST_GET_ERROR: {
      const { postId, statusCode, error } = action.payload;

      return {
        ...state,
        [`${postId}`]: {
          ...state[`${postId}`],

          isFetching: false,
          isFetched: false,
          isError: true,

          statusCode,
          error,

          lastModified: Date.now(),
        },
      };
    }

    case POST_GET_SUCCESS: {
      const { postId, statusCode } = action.payload;

      return {
        ...state,
        [`${postId}`]: {
          ...state[`${postId}`],
          ...action.payload,

          isFetching: false,
          isFetched: true,
          isError: false,

          statusCode,

          lastModified: Date.now(),
        },
      };
    }

    case POST_UPDATE: {
      const {
        postId,
        title,
        description,
        data,
        cover,
        privacy,
      } = action.payload;

      let newPostState = { ...state[`${postId}`] };

      if (title) {
        newPostState = { ...newPostState, title };
      }

      if (description) {
        newPostState = { ...newPostState, description };
      }

      if (data) {
        newPostState = { ...newPostState, data };
      }

      if (privacy !== undefined) {
        newPostState = { ...newPostState, privacy };
      }
      if (cover) {
        newPostState = { ...newPostState, cover };
      }

      return { ...state, [`${postId}`]: newPostState };
    }

    case POST_DELETE: {
      const { postId } = action.payload;

      return {
        ...state,
        [`${postId}`]: undefined,
      };
    }

    case POST_COMMENT_CREATE: {
      const { postId, payload } = action.payload;
      const { commentId } = payload;

      const newState = {
        ...state,
        [`${postId}`]: {
          ...state[postId],
          comments: {
            [`${commentId}`]: {
              ...payload,
            },
            ...state[postId].comments,
          },
        },
      };
      return newState;
    }

    case POST_COMMENTS_GET: {
      const { postId, payload } = action.payload;
      let newState = { ...state };

      let comments;

      payload.map((c) => {
        comments = {
          ...comments,
          [`${c.commentId}`]: {
            ...c,
          },
        };

        return c;
      });

      newState = {
        ...newState,
        [`${postId}`]: {
          ...newState[postId],
          comments,
        },
      };

      return newState;
    }

    case POST_COMMENT_DELETE: {
      const { postId, commentId } = action.payload;
      const newState = {
        ...state,
        [`${postId}`]: {
          ...state[postId],
          comments: {
            ...state[postId].comments,
            [`${commentId}`]: null,
          },
        },
      };

      return newState;
    }

    default:
      return state;
  }
}
