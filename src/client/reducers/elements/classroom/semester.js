const initialState = {
  semClasses: [],
};

function semesterReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default semesterReducer;
