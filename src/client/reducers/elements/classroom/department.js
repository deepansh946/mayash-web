
import { DEPARTMENT_ADD } from '../../../constants/classroom';

const initialState = {
  deptId: '',
  title: '',
  hodId: 0,
  degrees: [],
  professors: [],
};

function departmentReducer(state = initialState, action) {
  switch (action.type) {
    case DEPARTMENT_ADD: {
      const { deptId, title } = action.payload;

      return {
        ...state,
        deptId,
        title,
      };
    }

    default:
      return state;
  }
}

export default departmentReducer;
