const initialState = {
  degreeId: '',
  title: '',
  duration: 0,
  description: '',
  semesters: [],
};

function degreeReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default degreeReducer;
