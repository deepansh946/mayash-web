
import { DEPARTMENT_ADD } from '../../../constants/classroom';

const initialState = {
  departments: [],
};

function classroomReducer(state = initialState, action) {
  switch (action.type) {
    case DEPARTMENT_ADD: {
      const { deptId, title } = action.payload;

      return [
        ...state,
        { deptId, title },
      ];
    }

    default:
      return state;
  }
}

export default classroomReducer;
