const initialState = {
  profId: 0,
  courseId: 0,
  classId: 0,
  assignments: [],
  strength: [],
};

function classreducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default classreducer;
