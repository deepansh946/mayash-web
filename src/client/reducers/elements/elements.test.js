/**
 * @format
 * @flow
 */
/* eslint-disable no-undef */

import * as ELEMENTS from '../../constants/elements';
import * as USERS from '../../constants/users';
import * as POSTS from '../../constants/posts';
import * as COURSES from '../../constants/courses';

import reducer, { initialElementsState } from './index';

describe('Elements Reducer', () => {
  describe('Initial State Test', () => {
    test('Initial State of Elements', () => {
      const result = { ...initialElementsState };

      expect(
        reducer(undefined, {
          type: 'INITIALIZE',
        }),
      ).toMatchObject(result);
    });
  });

  describe('User Actions', () => {
    test('SIGN_IN', () => {
      const action = {
        type: USERS.SIGN_IN,
        payload: {
          id: 12345,
          username: 'hbarve1',
          token: 'head.body.tail',
        },
      };

      const finalState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          ...action.payload,
          isSignedIn: true,
        },
      };

      expect(reducer(initialElementsState, action)).toMatchObject(finalState);
    });

    test('SIGN_OUT', () => {
      const action = { type: USERS.SIGN_OUT };

      const state = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          isSignedIn: true,
          id: 12345,
          username: 'hbarve1',
          token: 'head.body.token',
        },
      };
      const finalState = { ...initialElementsState };

      expect(reducer(state, action)).toMatchObject(finalState);
    });

    test('USER_GET_START', () => {
      const action = { type: USERS.USER_GET_START };

      const state = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          isSignedIn: true,
          id: 12345,
          username: 'hbarve1',
          token: 'head.body.token',
        },
      };
      const finalState = {
        ...state,
        user: {
          ...state.user,
          isFetching: true,
        },
      };

      expect(reducer(state, action)).toMatchObject(finalState);
    });

    test('USER_GET_ERROR', () => {
      const action = {
        type: USERS.USER_GET_ERROR,
        payload: {
          userId: 12345,
          statusCode: 500,
          error: 'Error Message',
        },
      };

      const state = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          isSignedIn: true,
          id: 12345,
          username: 'hbarve1',
          token: 'head.body.token',
        },
      };
      const finalState = {
        ...state,
        user: {
          ...state.user,
          isError: true,
          statusCode: action.payload.statusCode,
          error: action.payload.error,
        },
      };

      expect(reducer(state, action)).toMatchObject(finalState);
    });

    test('USER_GET_SUCCESS', () => {
      const action = { type: USERS.SIGN_OUT };

      const state = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          isSignedIn: true,
          id: 12345,
          username: 'hbarve1',
          token: 'head.body.token',
        },
      };
      const finalState = { ...initialElementsState };

      expect(reducer(state, action)).toMatchObject(finalState);
    });
  });

  describe('Element Actions', () => {
    /* ************************************************************************
                              ELEMENT GET START
     *********************************************************************** */

    test('ELEMENT_GET_START by "username" when user is not signed in.', () => {
      const username = 'testUsername';
      const payload = { username };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { ...payload },
      };

      const finalState = {
        ...initialElementsState,
        [username]: {
          username,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(initialElementsState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_START by "username" when user is signed in.', () => {
      const username = 'testUsername';

      const ThisInitialState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          username,
          id: 1234567890,
          isSignedIn: true,
          token: 'head.body.tail',
        },
      };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { username },
      };

      const finalState = {
        ...ThisInitialState,
        user: {
          ...ThisInitialState.user,
          username,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(ThisInitialState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_START by "id" when user is not signed in.', () => {
      const id = 1234567890;
      const payload = { id };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { ...payload },
      };

      const finalState = {
        ...initialElementsState,
        [id]: {
          id,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(initialElementsState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_START by "id" when user is signed in.', () => {
      const id = 1234567890;

      const ThisInitialState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          id,
          isSignedIn: true,
          token: 'head.body.tail',
        },
      };

      const action = { type: ELEMENTS.ELEMENT_GET_START, payload: { id } };

      const finalState = {
        ...ThisInitialState,
        user: {
          ...ThisInitialState.user,
          id,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(ThisInitialState, action)).toMatchObject(finalState);
    });

    /* ************************************************************************
                              ELEMENT GET ERROR
     *********************************************************************** */
    test('ELEMENT_GET_ERROR by "username" when user is signed in.', () => {
      const username = 'testUsername';

      const ThisInitialState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          username,
          id: 1234567890,
          isSignedIn: true,
          token: 'head.body.tail',
        },
      };

      const action = {
        type: ELEMENTS.ELEMENT_GET_ERROR,
        payload: {
          username,
          statusCode: 404, // any error statusCode
          error: 'Not Found',
        },
      };

      const finalState = {
        ...ThisInitialState,
        user: {
          ...ThisInitialState.user,
          username,
          isFetched: false,
          isFetching: false,
          isError: true,
          ...action.payload,
        },
      };

      expect(reducer(ThisInitialState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_ERROR by "id" when user is not signed in.', () => {
      const id = 1234567890;
      const payload = { id };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { ...payload },
      };

      const finalState = {
        ...initialElementsState,
        [id]: { id, isFetched: false, isFetching: true, isError: false },
      };

      expect(reducer(initialElementsState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_ERROR by "id" when user is signed in.', () => {
      const id = 1234567890;

      const ThisInitialState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          id,
          isSignedIn: true,
          token: 'head.body.tail',
        },
      };

      const action = { type: ELEMENTS.ELEMENT_GET_START, payload: { id } };

      const finalState = {
        ...ThisInitialState,
        user: {
          ...ThisInitialState.user,
          id,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(ThisInitialState, action)).toMatchObject(finalState);
    });

    /* ************************************************************************
                              ELEMENT GET SUCCESS
     *********************************************************************** */

    test('ELEMENT_GET_SUCCESS by "username" when user is not signed in.', () => {
      const username = 'testUsername';
      const payload = { username };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { ...payload },
      };

      const finalState = {
        ...initialElementsState,
        [username]: {
          username,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(initialElementsState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_SUCCESS by "username" when user is signed in.', () => {
      const username = 'testUsername';

      const ThisInitialState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          username,
          id: 1234567890,
          isSignedIn: true,
          token: 'head.body.tail',
        },
      };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { username },
      };

      const finalState = {
        ...ThisInitialState,
        user: {
          ...ThisInitialState.user,
          username,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(ThisInitialState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_SUCCESS by "id" when user is not signed in.', () => {
      const id = 1234567890;
      const payload = { id };

      const action = {
        type: ELEMENTS.ELEMENT_GET_START,
        payload: { ...payload },
      };

      const finalState = {
        ...initialElementsState,
        [id]: { id, isFetched: false, isFetching: true, isError: false },
      };

      expect(reducer(initialElementsState, action)).toMatchObject(finalState);
    });

    test('ELEMENT_GET_SUCCESS by "id" when user is signed in.', () => {
      const id = 1234567890;

      const ThisInitialState = {
        ...initialElementsState,
        user: {
          ...initialElementsState.user,
          id,
          isSignedIn: true,
          token: 'head.body.tail',
        },
      };

      const action = { type: ELEMENTS.ELEMENT_GET_START, payload: { id } };

      const finalState = {
        ...ThisInitialState,
        user: {
          ...ThisInitialState.user,
          id,
          isFetched: false,
          isFetching: true,
          isError: false,
        },
      };

      expect(reducer(ThisInitialState, action)).toMatchObject(finalState);
    });
  });

  // describe("Element's Post", () => {
  //   test('User Get', () => {
  //     const payload = {};
  //   });
  // });

  // describe("Element's Course", () => {
  //   test('User Get', () => {
  //     const payload = {};
  //   });
  // });
});
