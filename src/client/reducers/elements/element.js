
const initialState = {
  isFetching: false,
  isFetched: false,

  statusCode: 0,
  isError: false,
  error: '',

  message: '',
  lastUpdated: Date.now(),
};

function elementReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default elementReducer;
