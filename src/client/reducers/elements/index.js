/**
 * @format
 * @flow
 */

import {
  ELEMENTS_GET,
  ELEMENT_GET_START,
  ELEMENT_GET_SUCCESS,
  ELEMENT_GET_ERROR,
} from '../../constants/elements';
import {
  USER_GET_START,
  USER_GET_ERROR,
  USER_GET_SUCCESS,
  USER_UPDATE,
  SIGN_IN,
  SIGN_OUT,
} from '../../constants/users';

import { CIRCLE_MEMBERS_GET, CIRCLE_JOIN } from '../../constants/circle/member';

import { POSTS_GET, POST_CREATE } from '../../constants/posts';

import { COURSES_GET, COURSE_CREATE } from '../../constants/courses';

import { NOTES_GET, NOTE_CREATE } from '../../constants/notes';

import {
  DEPARTMENT_CREATE,
  DEPARTMENT_UPDATE,
  DEGREE_CREATE,
} from '../../constants/classroom';

import localStoreWrite from '../../api/browser/localStoreWrite';
import localStoreRemove from '../../api/browser/localStoreRemove';
import cookieWrite from '../../api/browser/cookieWrite';
import cookieRemoveAll from '../../api/browser/cookieRemoveAll';

// Initial state
const initialElementState = {
  posts: [],

  isFetching: false,
  isFetched: false,

  statusCode: 404,
  isError: false,
  error: '',

  message: '',
  // lastUpdated: Date.now(),
};

export const initialElementsState = {
  user: {
    ...initialElementState,
    isSignedIn: false,
  },
};

type State = {
  user: any,
  [x: number]: any,
};

type Action = {
  type: string,
  payload?: any,
};

/**
 *
 * @param {Object} state
 * @param {Object} action
 * @param {string} action.type
 * @param {Object} action.payload
 *
 * @returns {Object}
 */
export default function reducer(
  state: State = initialElementsState,
  action: Action,
): State {
  switch (action.type) {
    case SIGN_IN: {
      const { payload } = action;
      const { id, username, token } = payload;

      localStoreWrite('user', payload);
      cookieWrite('isSignedIn', true);
      cookieWrite('id', id);
      cookieWrite('username', username);
      cookieWrite('token', token);

      // window.location.href = '/';

      return {
        ...state,
        user: { ...state.user, id, username, token, isSignedIn: true },
      };
    }

    case SIGN_OUT: {
      cookieRemoveAll();
      localStoreRemove('user');

      // window.location.href = '/';
      return initialElementsState;
    }

    case USER_GET_START: {
      return {
        ...state,
        user: { ...state.user, isFetching: true, lastUpdated: Date.now() },
      };
    }

    case USER_GET_ERROR: {
      const { statusCode, error } = action.payload;

      return {
        ...state,
        user: {
          ...state.user,
          isFetched: false,
          isFetching: false,
          isError: true,
          lastUpdated: Date.now(),
          statusCode,
          error,
        },
      };
    }

    case USER_GET_SUCCESS: {
      return {
        ...state,
        user: {
          ...state.user,
          isFetched: true,
          isFetching: false,
          isError: false,
          lastUpdated: Date.now(),
          ...action.payload,
        },
      };
    }

    case USER_UPDATE: {
      return {
        ...state,
        user: {
          ...state.user,
          isFetched: true,
          isFetching: false,
          isError: false,
          ...action.payload,
        },
      };
    }

    /**
     * Element Get Start will have four CASES
     *
     * CASE 1: GET BY USERNAME
     *   CASE 1.1: USER IS SIGNED IN
     *     CASE 1.1.1: user.username === username
     *     CASE 1.1.2: user.username !== username
     *   CASE 1.2: USER IS NOT SIGNED IN
     *
     * CASE 2: GET BY ID
     *   CASE 2.1: USER IS SIGNED IN
     *     CASE 2.1.1: user.id === id
     *     CASE 2.1.2: user.id !== id
     *   CASE 2.2: if user is not signed in.
     */
    case ELEMENT_GET_START: {
      const { user } = state;
      const { username, id } = action.payload;

      // CASE 1
      if (typeof username !== 'undefined') {
        // CASE 1.1

        // CASE 1.1.1
        if (user.isSignedIn === true && user.username === username) {
          return { ...state, user: { ...state.user, isFetching: true } };
        }

        // CASE 1.1.2
        return {
          ...state,
          [username]: {
            username,
            isFetched: false,
            isFetching: true,
            isError: false,
          },
        };
      }

      // CASE 2

      // CASE 2.1 & CASE 2.1.1
      if (user.isSignedIn === true && user.id === id) {
        return { ...state, user: { ...state.user, isFetching: true } };
      }

      // CASE 2.1.2 & CASE 2.2
      return {
        ...state,
        [id]: { id, isFetched: false, isFetching: true, isError: false },
      };
    }

    /* 
     *
     *
     */
    case ELEMENT_GET_ERROR: {
      const { user } = state;
      const { username, id, statusCode, error } = action.payload;

      /* 
       * if we are fetching user then it will be signed in for sure, so
       * there will be no case for fetching user when user is not signed in.
       */
      if (typeof username !== 'undefined') {
        if (user.username === username) {
          return {
            ...state,
            user: {
              ...state.user,
              username,
              statusCode,
              error,
              isError: true,
              isFetched: false,
              isFetching: false,
            },
          };
        }

        return {
          ...state,
          [username]: {
            ...state[username],
            username,
            statusCode,
            error,
            isError: true,
            isFetched: false,
            isFetching: false,
          },
        };
      }

      if (user.isSignedIn === true && user.id === id) {
        return {
          ...state,
          user: {
            ...state.user,
            id,
            statusCode,
            error,
            isError: true,
            isFetched: false,
            isFetching: false,
          },
        };
      }

      return {
        ...state,
        [id]: {
          ...state[id],
          id,
          isFetched: false,
          isFetching: false,
          isError: true,
          statusCode,
          error,
        },
      };
    }

    case ELEMENT_GET_SUCCESS: {
      const { statusCode, username, id, ...rest } = action.payload;
      const { user } = state;

      if (
        user.isSignedIn === true &&
        (user.id === id || user.username === username)
      ) {
        return {
          ...state,
          user: {
            ...user,
            ...rest,
            statusCode,
            isFetched: true,
            isFetching: false,
            isError: false,
          },
        };
      }

      const newState = {
        ...state,
        [id]: {
          id,
          username,
          ...rest,
          isFetched: true,
          isFetching: false,
          isError: false,
          statusCode,
        },
        [username]: undefined,
      };

      return { ...newState };
    }

    case ELEMENTS_GET: {
      const { elements } = action.payload;

      let newState = { ...state };

      elements.map((e) => {
        newState = {
          ...newState,
          [`${e.id}`]: {
            ...e,
            isFetched: true,
            isFetching: false,
            isError: false,
            lastUpdated: Date.now(),
          },
        };
        return e.id;
      });

      return newState;
    }

    case CIRCLE_MEMBERS_GET: {
      const { circleId, membersInfo: members } = action.payload;

      let newState = { ...state };

      newState = {
        ...newState,
        [`${circleId}`]: { ...newState[circleId], members },
      };

      return newState;
    }

    case CIRCLE_JOIN: {
      const { circleId, memberId } = action.payload;

      const newMember = {
        memberId,
        doj: Date.now(),
        role: 'request',
      };

      let newState = { ...state };
      newState = {
        ...newState,
        [`${circleId}`]: {
          ...newState[circleId],
          members: [newMember, ...newState[circleId].members],
        },
      };

      return newState;
    }

    /**
     * This action will update post for element
     */
    case POST_CREATE: {
      const { authorId, postId } = action.payload;

      let newState = { ...state };

      if (newState.user.id === authorId) {
        newState = {
          ...newState,
          user: {
            ...newState.user,
            posts: [{ postId }, ...newState.user.posts],
          },
        };
      } else {
        newState = {
          ...newState,
          [`${authorId}`]: {
            ...newState[`${authorId}`],
            posts: [{ postId }, ...newState.user.posts],
          },
        };
      }

      return newState;
    }

    case POSTS_GET: {
      const { id } = action.payload;

      const posts = action.payload.posts.map(({ postId }) => ({ postId }));

      let newState = { ...state };

      if (newState.user.id === id) {
        newState = { ...newState, user: { ...newState.user, posts } };
      } else {
        newState = { ...newState, [`${id}`]: { ...newState[`${id}`], posts } };
      }

      return newState;
    }

    /**
     * This action will update courses for element
     */
    case COURSE_CREATE: {
      const { authorId, courseId } = action.payload;

      let newState = { ...state };

      if (newState.user.id === authorId) {
        newState = {
          ...newState,
          user: {
            ...newState.user,
            courses: [{ courseId }, ...newState.user.courses],
          },
        };
      } else {
        newState = {
          ...newState,
          [`${authorId}`]: {
            ...newState[`${authorId}`],
            courses: [{ courseId }, ...newState.user.courses],
          },
        };
      }

      return newState;
    }

    case COURSES_GET: {
      const { id } = action.payload;

      const courses = action.payload.courses.map(({ courseId }) => ({
        courseId,
      }));

      let newState = { ...state };

      if (newState.user.id === id) {
        newState = { ...newState, user: { ...newState.user, courses } };
      } else {
        newState = {
          ...newState,
          [`${id}`]: { ...newState[`${id}`], courses },
        };
      }

      return newState;
    }

    /**
     * This action will update notes array for element
     */
    case NOTE_CREATE: {
      const { id, noteId } = action.payload;

      let newState = { ...state };

      if (newState.user.id === id) {
        newState = {
          ...newState,
          user: {
            ...newState.user,
            notes: [{ noteId }, ...newState.user.notes],
          },
        };
      } else {
        newState = {
          ...newState,
          [`${id}`]: {
            ...newState[`${id}`],
            notes: [{ noteId }, ...newState.user.notes],
          },
        };
      }

      return newState;
    }

    case NOTES_GET: {
      const { id } = action.payload;

      const notes = action.payload.notes.map(({ noteId }) => ({ noteId }));

      let newState = { ...state };
      newState = { ...newState, user: { ...newState.user, notes } };

      return newState;
    }

    case DEPARTMENT_CREATE: {
      const { circleId, deptId, title } = action.payload;

      const departments = state[circleId].departments || [];

      return {
        ...state,
        [circleId]: {
          ...state[circleId],
          departments: [
            ...departments,
            {
              deptId,
              title,
            },
          ],
        },
      };
    }

    case DEPARTMENT_UPDATE: {
      const { circleId, deptId, title, description } = action.payload;

      const { departments } = state[circleId] || [];

      const index = departments.findIndex((d) => d.deptId === deptId);

      if (index === -1) {
        return { ...state };
      }

      let department = departments[index];

      if (title) {
        department = { ...department, title };
      }

      if (description) {
        department = { ...department, description };
      }

      return {
        ...state,
        [circleId]: {
          ...state[circleId],
          departments: [
            ...departments.slice(0, index),
            department,
            ...departments.slice(index + 1, departments.length),
          ],
        },
      };
    }

    case DEGREE_CREATE: {
      const { circleId, deptId, degreeId, title } = action.payload;

      const { departments = [] } = state[circleId];

      const index = departments.findIndex((d) => d.deptId === deptId);

      if (index === -1) {
        return {
          ...state,
        };
      }

      const { degrees = [] } = departments[index] || {};

      const degree = { degreeId, title };

      return {
        ...state,
        [circleId]: {
          ...state[circleId],
          departments: [
            ...departments.slice(0, index),
            {
              ...departments[index],
              degrees: [...degrees, degree],
            },
            ...departments.slice(index + 1, departments.length),
          ],
        },
      };
    }

    default:
      return state;
  }
}
