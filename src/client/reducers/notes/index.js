/**
 * notes reducer
 * This component will handle all the dispatch actions on notes
 *
 * @format
 */

// import uniqBy from 'lodash/uniqBy';

import {
  NOTES_GET,
  NOTE_CREATE,
  NOTE_GET_START,
  NOTE_GET_SUCCESS,
  NOTE_GET_ERROR,
  NOTE_UPDATE,
  NOTE_DELETE,
} from '../../constants/notes';

const initialNotesState = {
  notes: 500,
  fetchedNotes: 20,
};

export default function reducer(state = {}, action) {
  switch (action.type) {
    case NOTE_CREATE: {
      const { noteId, statusCode } = action.payload;

      return {
        ...state,
        [`${noteId}`]: {
          ...action.payload,

          statusCode,
          isFetched: true,
          isFetching: false,
          isError: false,

          lastModified: Date.now(),
        },
      };
    }

    /**
     *
     * @todo: optimize this action.
     */
    case NOTES_GET: {
      const { notes } = action.payload;

      let newState = { ...state };
      notes.map((n) => {
        newState = {
          ...newState,
          [`${n.noteId}`]: {
            ...n,

            isFetched: true,
            isFetching: false,
            isError: false,
            statusCode: 200,

            lastModified: Date.now(),
          },
        };

        return n;
      });

      return newState;
    }

    case NOTE_UPDATE: {
      const { noteId, title } = action.payload;

      return {
        ...state,
        [`${noteId}`]: {
          ...state[noteId],
          title,
        },
      };
    }

    case NOTE_DELETE: {
      const { noteId } = action.payload;

      return {
        ...state,
        [`${noteId}`]: undefined,
      };
    }

    default:
      return state;
  }
}

// // reducer to handle each note
// const noteReducer = (state = {}, action) => {
//   switch (action.type) {
//     case NOTE_CREATE: {
//       return {
//         isFetching: false,
//         isFetched: true,

//         statusCode: 201,
//         isError: false,
//         error: '',
//         message: '',
//         lastUpdated: Date.now(),

//         ...state,
//         ...action.payload,
//       };
//     }
//     case NOTE_GET_SUCCESS: {
//       return {
//         ...state,
//         ...action.payload,

//         isFetching: false,
//         isFetched: true,

//         statusCode: 200,
//         isError: false,
//         error: '',
//         message: '',
//         lastUpdated: Date.now(),
//       };
//     }

//     default:
//       return state;
//   }
// };

// // reducer to handle all note
// export const notesReducer = (state = initialNotesState, action) => {
//   switch (action.type) {
//     case NOTE_CREATE: {
//       const { noteId } = action.payload;

//       return {
//         ...state,
//         [`${noteId}`]: {

//           isFetching: false,
//           isFetched: true,

//           statusCode: 201,
//           isError: false,
//           error: '',
//           message: '',
//           lastUpdated: Date.now(),

//           ...action.payload,
//         },
//       };
//     }
//     case NOTE_DELETE: {
//       const { noteId } = action.payload;

//       return {
//         ...state,
//         [`${noteId}`]: undefined,
//       };
//     }
