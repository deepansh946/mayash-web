/**
 * courses reducer component
 * This component will handle all the dispatch actions on courses
 *
 * @format
 * @flow
 */

import {
  COURSES_GET,
  COURSE_GET_START,
  COURSE_GET_ERROR,
  COURSE_GET_SUCCESS,

  // COURSE_GET,
  COURSE_CREATE,
  COURSE_UPDATE,
  COURSE_DELETE,
  MODULES_GET,
  // MODULE_GET,
  MODULE_CREATE,
  MODULE_UPDATE,
  MODULE_DELETE,

  // discussion question contants
  QUESTIONS_GET,
  QUESTION_GET,
  QUESTION_CREATE,
  QUESTION_UPDATE,
  QUESTION_DELETE,

  // answers contants
  ANSWERS_GET,
  ANSWER_GET,
  ANSWER_CREATE,
  ANSWER_UPDATE,
  ANSWER_DELETE,
} from '../../constants/courses';

type CourseModule = {
  courseId: number,
  moduleId: number,
  authorId: number,
  title: string,
  data?: mixed,
};

type Answer = {
  courseId: number,
  // here author is of Answer, not course neither question.
  authorId: number,
  questionId: number,
  answerId: number,
  title: string,
  data?: any,
  timestamp: string,
};

type Question = {
  courseId: number,
  // here author is for question and not course's author.
  authorId: number,
  questionId: number,
  title: string,
  data?: any,
  timestamp: string,
  answers: {
    [answerId: number]: Answer,
  },
};

type Course = {
  courseId: number,
  authorId: number,
  title: string,
  description?: string,
  courseModules?: Array<CourseModule>,
  discussion: {
    [questionId: number]: Question,
  },
  timestamp?: string,

  statusCode: number,
  error?: string,
  message?: string,

  // lastModified: number,
};

type State = {
  [courseId: number]: Course,
};

type Action = {
  type: string,
  payload: any,
};

export default function reducer(state: State = {}, action: Action): State {
  switch (action.type) {
    case COURSE_CREATE: {
      const {
        authorId,
        courseId,
        title,
        timestamp,
        statusCode,
      } = action.payload;

      return {
        ...state,
        [courseId]: {
          authorId,
          courseId,
          title,

          timestamp,
          isFetching: false,
          isFetched: true,
          isError: false,
          statusCode,
          lastModified: Date.now(),
        },
      };
    }

    case COURSES_GET: {
      let newState = { ...state };

      action.payload.courses.map((c) => {
        newState = {
          ...newState,
          [c.courseId]: {
            ...c,
            isFetched: true,
            isFetching: false,
            isError: false,
            statusCode: 200,
            lastModified: Date.now(),
          },
        };

        return c;
      });

      return newState;
    }

    case COURSE_GET_START: {
      const { courseId } = action.payload;

      return {
        ...state,
        [courseId]: {
          courseId,
          isFetching: true,
          isFetched: false,
          isError: false,
          lastModified: Date.now(),
        },
      };
    }

    case COURSE_GET_ERROR: {
      const { courseId, statusCode, error } = action.payload;

      return {
        ...state,
        [courseId]: {
          courseId,
          isFetching: false,
          isFetched: false,
          isError: true,
          statusCode,

          error,
          lastModified: Date.now(),
        },
      };
    }

    case COURSE_GET_SUCCESS: {
      const { courseId } = action.payload;

      return {
        ...state,
        [courseId]: {
          courseId,
          isFetching: false,
          isFetched: true,
          isError: false,
          ...action.payload,
          lastModified: Date.now(),
        },
      };
    }

    case COURSE_DELETE: {
      const { courseId } = action.payload;

      return { ...state, [courseId]: undefined };
    }

    case COURSE_UPDATE: {
      const {
        courseId,
        cover,
        title,
        description,
        syllabus,
        courseModules,
      } = action.payload;

      let newCourseState = { ...state[courseId] };

      if (cover) {
        newCourseState = { ...newCourseState, cover };
      }

      if (title) {
        newCourseState = { ...newCourseState, title };
      }

      if (description) {
        newCourseState = { ...newCourseState, description };
      }

      if (syllabus) {
        newCourseState = { ...newCourseState, syllabus };
      }

      // this is a bug, fix it later.
      if (courseModules) {
        newCourseState = { ...newCourseState, courseModules };
      }

      return { ...state, [courseId]: newCourseState };
    }

    case MODULES_GET: {
      const { courseId, courseModules } = action.payload;

      return { ...state, [courseId]: { ...state[courseId], courseModules } };
    }

    case MODULE_CREATE: {
      const { courseId, authorId, moduleId, title } = action.payload;

      let newState;

      if (state[courseId].courseModules === undefined) {
        newState = {
          ...state,
          [`${courseId}`]: {
            courseModules: [{ courseId, authorId, moduleId, title }],
            ...state[courseId],
          },
        };
      } else {
        newState = {
          ...state,
          [`${courseId}`]: {
            ...state[courseId],
            courseModules: [
              ...state[courseId].courseModules,
              { courseId, authorId, moduleId, title },
            ],
          },
        };
      }

      return newState;
    }

    case MODULE_UPDATE: {
      const { courseId, moduleId, title, data } = action.payload;

      const { courseModules = [] } = state[courseId];

      const index = courseModules.findIndex((m) => m.moduleId === moduleId);
      let newModule = courseModules[index];

      if (title) {
        newModule = { ...newModule, title };
      }

      if (data) {
        newModule = { ...newModule, data };
      }

      return {
        ...state,
        [`${courseId}`]: {
          ...state[courseId],
          courseModules: [
            ...courseModules.slice(0, index),
            newModule,
            ...courseModules.slice(index + 1, courseModules.length),
          ],
        },
      };
    }

    case MODULE_DELETE: {
      const { courseId, moduleId } = action.payload;

      const { courseModules = [] } = state[courseId];

      const index = courseModules.findIndex((m) => m.moduleId === moduleId);

      return {
        ...state,
        [courseId]: {
          ...state[courseId],
          courseModules: [
            ...courseModules.slice(0, index),
            ...courseModules.slice(index + 1, courseModules.length),
          ],
        },
      };
    }

    case QUESTION_CREATE: {
      const {
        courseId,
        authorId,
        questionId,
        title,
        timestamp,
      } = action.payload;

      const course = state[courseId];
      const { discussion = {} } = course;

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: {
            [questionId]: { courseId, authorId, questionId, title, timestamp },
            ...discussion,
          },
        },
      };
    }

    case QUESTION_UPDATE: {
      const { courseId, questionId, ...rest } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: { ...discussion, [questionId]: { ...question, ...rest } },
        },
      };
    }

    case QUESTION_GET: {
      const { courseId, questionId, ...rest } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: { ...discussion, [questionId]: { ...question, ...rest } },
        },
      };
    }

    case QUESTIONS_GET: {
      const { courseId, discussion } = action.payload;

      const course = state[courseId];
      let newDiscussion = { ...course.discussion };

      discussion.map((q) => {
        newDiscussion = { ...newDiscussion, [q.questionId]: { ...q } };

        return q;
      });

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: { ...course.discussion, ...newDiscussion },
        },
      };
    }

    case QUESTION_DELETE: {
      const { courseId, questionId } = action.payload;

      const course = state[courseId];
      const { discussion } = course;

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: { ...discussion, [questionId]: undefined },
        },
      };
    }

    case ANSWER_CREATE: {
      const {
        courseId,
        questionId,
        answerId,
        authorId,
        title,
        timestamp,
      } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];
      const { answers } = question;

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: {
            ...discussion,
            [questionId]: {
              ...question,
              answers: {
                ...answers,
                [answerId]: {
                  courseId,
                  questionId,
                  answerId,
                  authorId,
                  title,
                  timestamp,
                },
              },
            },
          },
        },
      };
    }

    case ANSWERS_GET: {
      const { courseId, questionId, answers } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];
      const { answers: oldAnswers = {} } = question;
      let newAnswers = { ...oldAnswers };

      answers.map(({ answerId, ...rest }) => {
        newAnswers = { ...newAnswers, [answerId]: { answerId, ...rest } };

        return answerId;
      });

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: {
            ...discussion,
            [questionId]: { ...question, answers: newAnswers },
          },
        },
      };
    }

    case ANSWER_GET: {
      const { courseId, questionId, answerId, ...restAnswer } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: {
            ...discussion,
            [questionId]: {
              ...question,
              answers: {
                [answerId]: { courseId, questionId, answerId, ...restAnswer },
              },
            },
          },
        },
      };
    }

    case ANSWER_UPDATE: {
      const {
        courseId,
        questionId,
        answerId,
        authorId,
        ...rest
      } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: {
            ...discussion,
            [questionId]: {
              ...question,
              answers: {
                [answerId]: {
                  courseId,
                  questionId,
                  answerId,
                  authorId,
                  ...rest,
                },
              },
            },
          },
        },
      };
    }

    case ANSWER_DELETE: {
      const { courseId, questionId, answerId } = action.payload;

      const course = state[courseId];
      const { discussion } = course;
      const question = discussion[questionId];

      return {
        ...state,
        [courseId]: {
          ...course,
          discussion: {
            ...discussion,
            [questionId]: { ...question, answers: { [answerId]: undefined } },
          },
        },
      };
    }

    default:
      return state;
  }
}
