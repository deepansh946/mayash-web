/**
 * @format
 * @file
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * This function update a post
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.postId -
 * @param {number} payload.commentId -
 * @param {string} payload.title -
 * @return {Promise} -
 *
 * @example
 */
async function update({ token, userId, postId, commentId, title }) {
  try {
    const url = `${HOST}/api/posts/${postId}/users/${userId}/comments/${commentId}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ title }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
