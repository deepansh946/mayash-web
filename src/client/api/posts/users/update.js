/**
 * @format
 * @file
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * This function update a post
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.data -
 * @param {string} payload.cover -
 * @param {Boolean} payload.privacy -
 * @return {Promise} -
 *
 * @example
 */
async function update({
  userId,
  token,
  postId,
  title,
  description,
  data,
  cover,
  privacy,
}) {
  try {
    const url = `${HOST}/api/users/${userId}/posts/${postId}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      // here if any key (title, description or data) is undefined
      // JSON.stringify() will not convert it to string and will removed it.
      body: JSON.stringify({ title, description, data, cover, privacy }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
