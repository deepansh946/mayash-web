/**
 * @format
 * @file delete post of user.
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * delete post of user
 * @async
 * @function deletePost
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 */
async function deletePost({ userId, token, postId }) {
  try {
    const url = `${HOST}/api/users/${userId}/posts/${postId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default deletePost;
