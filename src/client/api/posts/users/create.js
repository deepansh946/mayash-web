/**
 * @format
 * @file create post for user
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * This function will create post by user.
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {string} payload.postType
 * @param {string} payload.title
 * @return {Promise} -
 *
 * @example
 * create({
 *   userId: 12314123134,
 *   token: 'head.body.tail',
 *   postType: 'article',
 *   title: 'This is awesome article',
 * })
 *   .then(res => console.log(res))
 *   .catch(err => console.error(err));
 */
async function create({ token, userId, postType, title }) {
  try {
    const url = `${HOST}/api/users/${userId}/posts`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ postType, title }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
