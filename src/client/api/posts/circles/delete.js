/**
 * @format
 * @file delete post of circle.
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * delete post of circle
 * @async
 * @function deletePost
 * @param {Object} payload -
 * @param {number} payload.circleId -
 * @param {string} payload.token -
 * @param {number} payload.postId -
 * @return {Promise} -
 *
 * @example
 */
async function deletePost({ circleId, token, postId }) {
  try {
    const url = `${HOST}/api/circles/${circleId}/posts/${postId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default deletePost;
