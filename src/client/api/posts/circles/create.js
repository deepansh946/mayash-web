/**
 * @format
 * @file create post for circle
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * This function will create post by circle.
 * @async
 * @function create
 * @param {Object} payload
 * @param {number} payload.circleId
 * @param {string} payload.token
 * @param {string} payload.postType
 * @param {string} payload.title
 * @return {Promise} -
 *
 * @example
 * create({
 *   circleId: 12314123134,
 *   token: 'head.body.tail',
 *   postType: 'article',
 *   title: 'This is awesome article',
 * })
 *   .then(res => console.log(res))
 *   .catch(err => console.error(err));
 */
async function create({ circleId, token, postType, title }) {
  try {
    const url = `${HOST}/api/circles/${circleId}/posts`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ postType, title }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
