/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.postId
 * @param {string} payload.title
 * @return {Promise} -
 *
 * @example
 */
async function create({ token, userId, postId, title }) {
  try {
    const url = `${HOST}/api/users/${userId}/posts/${postId}/comments`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ title }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
