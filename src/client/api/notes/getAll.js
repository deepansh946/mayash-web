/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @return {Promise} -
 */
async function getAllNote({ token, userId }) {
  try {
    const url = `${HOST}/api/users/${userId}/notes`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getAllNote;
