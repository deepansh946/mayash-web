/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.noteId
 * @return {Promise} -
 */
async function getNote({ token, userId, noteId }) {
  try {
    const url = `${HOST}/api/users/${userId}/notes/${noteId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getNote;
