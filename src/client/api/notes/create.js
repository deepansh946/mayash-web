/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {string} payload.noteType
 * @param {string} payload.title
 * @return {Promise} -
 */
async function createNote({ token, userId, noteType, title }) {
  try {
    const url = `${HOST}/api/users/${userId}/notes`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ noteType, title }),
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json, noteType };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default createNote;
