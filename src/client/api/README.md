# API for client

## This folder contains all the APIs for client side.

These files contains all the get functions to fetch data from server, but create, update and delete calls will be made directly to components.

## Folder Structure

    .
    ├── README.md
    ├── config.js
    ├── elements
    │   ├── getAll.js
    │   ├── getById.js
    │   └── getByUsername.js
    ├── users
    │   ├── signIn.js
    │   ├── get.js
    │   ├── getAll.js
    │   ├── create.js
    │   ├── update.js
    │   ├── delete.js
    │   ├── password
    │   │   ├── get.js
    │   │   ├── create.js
    │   │   └── update.js
    │   └── circles
    │       └── getAll.js
    ├── circles
    │   ├── get.js
    │   └── members
    │       ├── create.js
    │       ├── delete.js
    │       ├── getAll.js
    │       ├── get.js
    │       └── update.js
    ├── posts
    │   ├── circles
    │   │   ├── create.js
    │   │   ├── delete.js
    │   │   ├── getAll.js
    │   │   └── update.js
    │   ├── comments
    │   │   ├── create.js
    │   │   ├── delete.js
    │   │   ├── getAll.js
    │   │   ├── get.js
    │   │   └── update.js
    │   ├── get.js
    │   ├── reactions
    │   │   ├── create.js
    │   │   ├── delete.js
    │   │   ├── getAll.js
    │   │   ├── get.js
    │   │   └── update.js
    │   └── users
    │       ├── create.js
    │       ├── delete.js
    │       ├── getAll.js
    │       └── update.js
    ├── courses
    │   ├── circles
    │   │   └── getAll.js
    │   ├── discussions
    │   ├── get.js
    │   ├── modules
    │   │   ├── getAll.js
    │   │   └── get.js
    │   └── users
    │       ├── create.js
    │       ├── delete.js
    │       ├── discussions
    │       ├── getAll.js
    │       ├── modules
    │       │   ├── create.js
    │       │   ├── delete.js
    │       │   └── update.js
    │       └── update.js
    ├── classrooms
    ├── notes
    │   ├── create.js
    │   ├── delete.js
    │   ├── getAll.js
    │   ├── get.js
    │   └── update.js
    ├── bookmarks
    │   ├── create.js
    │   ├── delete.js
    │   ├── getAll.js
    │   ├── get.js
    │   └── update.js
    ├── photos
    |   └── posts
    |       └── users
    |           └── create.js
    └── browser
        ├── cookieRead.js
        ├── cookieRemoveAll.js
        ├── cookieRemove.js
        ├── cookieWrite.js
        ├── localStoreRead.js
        ├── localStoreRemove.js
        └── localStoreWrite.js
