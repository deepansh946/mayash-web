/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * Get circle by circleId
 * @async
 * @function get
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.circleId
 * @return {Promise}
 */
async function get({ token, circleId }) {
  try {
    const url = `${HOST}/api/circles/${circleId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default get;
