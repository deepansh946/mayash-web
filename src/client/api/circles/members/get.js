/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function get
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.circleId -
 * @param {number} payload.memberId -
 * @return {Promise} -
 *
 * @example
 */
async function get({ token, circleId, memberId }) {
  try {
    const url = `${HOST}/api/circles/${circleId}/members/${memberId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default get;
