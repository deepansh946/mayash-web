/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.circleId -
 * @param {string} payload.memberId -
 * @return {Promise} -
 *
 * @example
 */
async function create({ token, circleId, memberId }) {
  try {
    const url = `${HOST}/api/circles/${circleId}/members/${memberId}`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
