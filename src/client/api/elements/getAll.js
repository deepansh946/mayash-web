/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function getElements
 * @param {Object} payload
 * @param {number} payload.token
 * @param {Object[]} payload.ids
 * @param {number} payload.ids[].id -
 * @returns {Promise}
 *
 * @example
 */
async function getAll({ token, ids }) {
  try {
    const url = `${HOST}/api/elements?ids=${JSON.stringify(ids)}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getAll;
