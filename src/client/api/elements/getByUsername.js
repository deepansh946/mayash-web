/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * get element(user/circle) by it's username
 * @async
 * @function getByUsername
 * @param {Object} payload
 * @param {string} payload.token
 * @param {string} payload.username
 * @return {Promise}
 *
 * @example
 */
async function getByUsername({ token, username }) {
  try {
    const url = `${HOST}/api/elements/@${username}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getByUsername;
