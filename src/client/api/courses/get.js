/**
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

type Payload = {|
  token?: string,
  courseId: number,
|};

type Success = {
  statusCode: number,
  message: string,
  payload: {
    courseId: number,
    authorId: number,
    title: string,
    description?: string,
    syllabus?: any,
    courseModules?: Array<{
      moduleId: number,
    }>,
  },
};

type ERROR = {
  statusCode: number,
  error: string,
};

/**
 * This function will get course with courseId.
 * @async
 * @function get
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.courseId -
 * @return {Promise} -
 *
 * @example
 */
async function get({ token, courseId }: Payload): Promise<Success | ERROR> {
  try {
    const url = `${HOST}/api/courses/${courseId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default get;
