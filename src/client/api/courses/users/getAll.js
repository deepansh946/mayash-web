/**
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

type Payload = {|
  token?: string,
  userId: number,
|};

/**
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
async function getAll({ token, userId }: Payload) {
  try {
    const url = `${HOST}/api/users/${userId}/courses`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getAll;
