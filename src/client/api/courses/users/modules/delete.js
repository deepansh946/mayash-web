/**
 * @file
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../../config';

type Payload = {
  token: string,
  userId: number,
  courseId: number,
  moduleId: number,
};

/**
 * For deleting course module
 * @async
 * @function deleteModule
 * @param {Object} payload
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @return {Promise} -
 *
 * @example
 */
async function deleteModule({ token, userId, courseId, moduleId }: Payload) {
  try {
    const url = `${HOST}/api/users/${userId}/courses/${courseId}/modules/${moduleId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default deleteModule;
