/**
 * @file
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../../config';

type Payload = {
  token: string,
  userId: number,
  courseId: number,
  title: string,
};

/**
 * Create Modules
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {string} payload.title -
 * @return {Promise}
 *
 * @example
 */
async function create({ token, userId, courseId, title }: Payload) {
  try {
    const url = `${HOST}/api/users/${userId}/courses/${courseId}/modules`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ title }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
