/**
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

type Payload = {
  token: string,
  userId: number,
  courseId: number,
  cover?: string,
  title?: string,
  description?: string,
  syllabus?: any,
  courseModules?: Array<{
    moduleId: number,
  }>,
};

/**
 * Update Course
 * @async
 * @function update
 * @param {Object} payload
 * @param {number} payload.courseId
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @param {string} payload.cover -
 * @param {string} payload.title -
 * @param {string} payload.description -
 * @param {Object} payload.syllabus -
 * @param {Object[]} payload.courseModules -
 * @param {number} payload.courseModules[].moduleId -
 * @return {Promise} -
 *
 * @example
 */
async function updateCourse({
  userId,
  courseId,
  token,
  cover,
  title,
  description,
  syllabus,
  courseModules,
}: Payload) {
  try {
    const url = `${HOST}/api/users/${userId}/courses/${courseId}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({
        cover,
        title,
        description,
        syllabus,
        courseModules,
      }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default updateCourse;
