/**
 * @fileOverview
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../../../config';

type Payload = {
  token: string,
  userId: number,
  courseId: number,
  questionId: number,
  answerId: number,
  title?: string,
  data?: any,
};

/**
 * Update Module
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @param {number} payload.answerId -
 * @param {string} payload.title -
 * @param {Object} payload.data -
 * @return {Promise} -
 *
 * @example
 */
async function update({
  token,
  userId,
  courseId,
  questionId,
  answerId,
  title,
  data,
}: Payload) {
  try {
    const url = `${HOST}/api/users/${userId}/courses/${courseId}/discussion/${questionId}/answers/${answerId}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ title, data }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
