/**
 * @file
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../../config';

type Payload = {
  token: string,
  userId: number,
  courseId: number,
  questionId: number,
};

/**
 *
 * @async
 * @function deleteQuestion
 * @param {Object} payload
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.courseId -
 * @param {number} payload.questionId -
 * @return {Promise} -
 *
 * @example
 */
async function deleteQuestion({
  token,
  userId,
  courseId,
  questionId,
}: Payload) {
  try {
    const url = `${HOST}/api/users/${userId}/courses/${courseId}/discussion/${questionId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default deleteQuestion;
