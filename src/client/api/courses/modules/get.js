/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 * get module api will fetch perticular module of course.
 * @param {object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.token -
 * @return {Promise} -
 */
async function getModule({ courseId, moduleId, token }) {
  try {
    const url = `${HOST}/api/courses/${courseId}/modules/${moduleId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getModule;
