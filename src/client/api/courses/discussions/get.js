/**
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

type Payload = {
  token: string,
  courseId: number,
  questionId: number,
};

/**
 *
 * @async
 * @function get
 * @param {Object} payload -
 * @param {number} payload.courseId -
 * @param {number} payload.moduleId -
 * @param {string} payload.token -
 * @return {Promise} -
 */
async function get({ token, courseId, questionId }: Payload) {
  try {
    const url = `${HOST}/api/courses/${courseId}/discussion/${questionId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default get;
