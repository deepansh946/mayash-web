/**
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

type Payload = {
  token: string,
  courseId: number,
};

/**
 * get all modules of a course with courseId.
 * @async
 * @function getAll
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.courseId -
 *
 * @return {Promise}
 */
async function getAll({ token, courseId }: Payload) {
  try {
    const url = `${HOST}/api/courses/${courseId}/discussion`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getAll;
