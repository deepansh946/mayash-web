/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.userId -
 * @param {string} payload.password -
 * @return {Promise} -
 *
 * @example
 */
async function create({ token, userId, password }) {
  try {
    const url = `${HOST}/api/users/${userId}/password`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ password }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
