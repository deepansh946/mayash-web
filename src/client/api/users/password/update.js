/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {string} payload.oldPassword -
 * @param {string} payload.newPassword -
 * @param {string} payload.confirmNewPassword -
 * @return {Promise} -
 *
 * @example
 */
async function update({
  token,
  userId,
  oldPassword,
  newPassword,
  confirmPassword,
}) {
  try {
    const url = `${HOST}/api/users/${userId}/password`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ oldPassword, newPassword, confirmPassword }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();
    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
