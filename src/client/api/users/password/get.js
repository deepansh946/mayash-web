/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function get
 * @param {object} payload -
 * @param {number} payload.userId -
 * @param {string} payload.token -
 * @return {Promise} -
 *
 * @example
 */
async function get({ userId, token }) {
  try {
    const url = `${HOST}/api/users/${userId}/password`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default get;
