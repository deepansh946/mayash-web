/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * This function will take username, password
 * to get auth token for user.
 * @async
 * @function signIn
 * @param {Object} payload -
 * @param {string} payload.username -
 * @param {string} payload.password -
 * @return {Promise} -
 */
export async function signIn({ username, password }) {
  try {
    const url = `${HOST}/api/signin`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default signIn;
