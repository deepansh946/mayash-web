/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.name -
 * @param {string} payload.username -
 * @param {string} payload.avatar -
 * @param {string} payload.email -
 * @return {Promise} -
 */
async function create({ token, name, username, avatar, email }) {
  try {
    const url = `${HOST}/api/users`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ name, username, email, avatar }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
