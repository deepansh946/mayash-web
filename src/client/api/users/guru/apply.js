/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function apply
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.userId -
 * @param {string} payload.reason -
 * @return {Promise} -
 */
async function apply({ token, userId, reason }) {
  try {
    const url = `${HOST}/api/users/${userId}/gururequest`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ userId, reason }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default apply;
