/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../../config';

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @param {number} payload.username -
 * @return {Promise} -
 *
 * @example
 */
async function update({ token, userId, username }) {
  try {
    const url = `${HOST}/api/users/${userId}/username`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ username }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
