/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {number} payload.id -
 * @param {string} payload.token -
 * @param {string} payload.name -
 * @param {Object} payload.avatar -
 * @param {Object} payload.cover -
 * @param {Object} payload.resume -
 * @return {Promise} -
 *
 * @example
 */
async function update({ id, token, name, avatar, cover, resume }) {
  try {
    const url = `${HOST}/api/users/${id}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ name, avatar, cover, resume }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
