/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * Delete User
 * NOTE: only admin can delete an user.
 * @async
 * @function deleteUser
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {number} payload.userId -
 * @return {Promise} -
 */
export default async function deleteUser({ token, userId }) {
  try {
    const url = `${HOST}/api/users/${userId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}
