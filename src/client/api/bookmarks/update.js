/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function update
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.bookmarkId
 * @param {string} payload.bookmarkType
 * @param {string} payload.title
 * @return {Promise} -
 */
async function update({ token, userId, bookmarkId, type, postId, courseId }) {
  try {
    const url = `${HOST}/api/users/${userId}/bookmarks/${bookmarkId}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ type, postId, courseId }),
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default update;
