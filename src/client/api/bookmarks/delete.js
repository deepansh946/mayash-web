/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function deleteBookmark
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.bookmarkId
 * @return {Promise} -
 */
async function deleteNote({ token, userId, bookmarkId }) {
  try {
    const url = `${HOST}/api/users/${userId}/bookmarks/${bookmarkId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default deleteNote;
