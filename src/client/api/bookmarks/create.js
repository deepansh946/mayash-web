/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function create
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {string} payload.type - 'post', 'course'
 * @param {number} payload.postId
 * @param {number} payload.courseId
 * @return {Promise} -
 */
async function create({ token, userId, type, postId, courseId }) {
  try {
    const url = `${HOST}/api/users/${userId}/bookmarks`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ type, postId, courseId }),
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { userId, type, postId, courseId, ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
