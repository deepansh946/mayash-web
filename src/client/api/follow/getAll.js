/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * @param {object} payload
 * @param {string} payload.token
 * @param {number} payload.followerId
 * @param {number} payload.followingId
 * @return {Promise} -
 */
async function getAll({ token, followerId, followingId }) {
  try {
    const url = `${HOST}/api/follow/${followerId}/${followingId}`;

    const res = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;
    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default getAll;
