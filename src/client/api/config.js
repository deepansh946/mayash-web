/**
 * config.js file contains all the configurations needed
 * for client APIs.
 *
 * @format
 */

const { NODE_ENV } = process.env;

// isServer variable will be false for client side and true for server side.
const isServer = typeof window === 'undefined';

// isPro variable will be true when website is running in production.
const isPro = NODE_ENV === 'production';

const proURL = 'https://mayash-web.appspot.com';

const devURL = 'http://localhost:5050';

/* eslint-disable no-nested-ternary */
/**
 * https://mayash-web.appspot.com is added as a default HOST for APIs request
 * for production.
 */
export const HOST = isServer ? (isPro ? proURL : devURL) : '';

export default {
  HOST,
};
