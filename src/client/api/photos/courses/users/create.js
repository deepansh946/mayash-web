/**
 * @format
 * @flow
 */

import fetch from 'isomorphic-fetch';

import { HOST } from '../../../config';

type Payload = {
  token: string,
  userId: number,
  courseId: number,
  formData: any,
};

type Success = {
  statusCode: number,
  message: string,
  payload?: any,
};

type ErrorResponse = {
  statusCode: number,
  error: string,
};

/**
 *
 * @async
 * @function
 * @param {Object} payload
 * @param {string} payload.token
 * @param {number} payload.userId
 * @param {number} payload.postId
 * @param {Object} payload.formData
 * @returns {Promise}
 *
 * @example
 */
export default async function create({
  token,
  userId,
  courseId,
  formData,
}: Payload): Promise<Success | ErrorResponse> {
  try {
    const url = `${HOST}/api/users/${userId}/courses/${courseId}/photos`;

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        // 'Content-Type': 'application/json',
        Authorization: token,
      },
      body: formData,
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}
