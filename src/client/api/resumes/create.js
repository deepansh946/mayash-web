/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function create
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.resumeId -
 * @param {string} payload.name -
 * @param {Object} payload.data -
 * @return {Promise} -
 *
 * @example
 */
async function create({ token, resumeId, name, data }) {
  try {
    const url = `${HOST}/api/resumes`;
    // const url = 'https://mayash.io/api/resumes';

    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ resumeId, name, data }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}

export default create;
