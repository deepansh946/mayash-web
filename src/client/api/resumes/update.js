/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 *
 * @async
 * @function update
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.resumeId -
 * @param {string} payload.name -
 * @param {Object} payload.data -
 * @return {Promise} -
 *
 * @example
 */
export default async function update({ token, resumeId, name, data }) {
  try {
    const url = `${HOST}/api/resumes/${resumeId}`;
    // const url = `https://mayash.io/api/resumes/${resumeId}`;

    const res = await fetch(url, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
      body: JSON.stringify({ name, data }),
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}
