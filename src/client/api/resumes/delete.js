/** @format */

import fetch from 'isomorphic-fetch';
import { HOST } from '../config';

/**
 * Delete User
 * NOTE: only admin can delete an resume.
 * @async
 * @function deleteResume
 * @param {Object} payload -
 * @param {string} payload.token -
 * @param {string} payload.resumeId -
 * @return {Promise} -
 *
 * @example
 */
export default async function deleteResume({ token, resumeId }) {
  try {
    const url = `${HOST}/api/resumes/${resumeId}`;

    const res = await fetch(url, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: token,
      },
    });

    const { status, statusText } = res;

    if (status >= 300) {
      return {
        statusCode: status,
        error: statusText,
      };
    }

    const json = await res.json();

    return { ...json };
  } catch (error) {
    console.error(error);

    return {
      statusCode: 400,
      error: 'Something went wrong, please try again...',
    };
  }
}
