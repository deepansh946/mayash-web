/**
 * AppRoute component
 * This component contain all routes .
 *
 * @format
 */

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import { createMuiTheme } from 'material-ui/styles';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import primary from 'material-ui/colors/lightBlue';
import secondary from 'material-ui/colors/red';

import Loadable from 'react-loadable';

import Loading from './components/Loading';
import AppLayout from './containers/AppLayout';

const asyncHome = Loadable({
  loader: () => import('./containers/Home'),
  modules: ['./containers/Home'],
  loading: Loading,
});

const asyncIntroduction = Loadable({
  loader: () => import('./pages/Introduction'),
  modules: ['./pages/Introduction'],
  loading: Loading,
});

const asyncServices = Loadable({
  loader: () => import('./pages/Services'),
  modules: ['./pages/Services'],
  loading: Loading,
});

const asyncPricing = Loadable({
  loader: () => import('./pages/Pricing'),
  modules: ['./pages/Pricing'],
  loading: Loading,
});

const asyncTeam = Loadable({
  loader: () => import('./pages/Team'),
  modules: ['./pages/Team'],
  loading: Loading,
});

const asyncJoinUs = Loadable({
  loader: () => import('./pages/JoinUs'),
  modules: ['./pages/JoinUs'],
  loading: Loading,
});

const asyncDevelopersTraining = Loadable({
  loader: () => import('./pages/DevelopersTraining'),
  modules: ['./pages/DevelopersTraining'],
  loading: Loading,
});

// const asyncSurvey = Loadable({
//   loader: () => import('./pages/Survey'),
//   modules: ['./pages/Survey'],
//   loading: Loading,
// });

const asyncContactUs = Loadable({
  loader: () => import('./pages/ContactUs'),
  modules: ['./pages/ContactUs'],
  loading: Loading,
});

const asyncDonateUS = Loadable({
  loader: () => import('./pages/DonateUs'),
  modules: ['./pages/DonateUs'],
  loading: Loading,
});

const asyncSignIn = Loadable({
  loader: () => import('./pages/SignIn'),
  modules: ['./pages/SignIn'],
  loading: Loading,
});

const asyncTerms = Loadable({
  loader: () => import('./pages/Terms'),
  modules: ['./pages/Terms'],
  loading: Loading,
});

const asyncPrivacy = Loadable({
  loader: () => import('./pages/Privacy'),
  modules: ['./pages/Privacy'],
  loading: Loading,
});

const asyncRefund = Loadable({
  loader: () => import('./pages/Refund'),
  modules: ['./pages/Refund'],
  loading: Loading,
});

const asyncProfilePage = Loadable({
  loader: () => import('./containers/ProfilePage'),
  modules: ['./containers/ProfilePage'],
  loading: Loading,
});

// const asyncDashboardPage = Loadable({
//   loader: () => import('./containers/Dashboard'),
//   modules: ['./containers/Dashboard'],
//   loading: Loading,
// });

const asyncResumePage = Loadable({
  loader: () => import('./containers/ResumePage'),
  modules: ['./containers/ResumePage'],
  loading: Loading,
});

const asyncElementPage = Loadable({
  loader: () => import('./containers/ElementPage'),
  modules: ['./containers/ElementPage'],
  loading: Loading,
});

const asyncClassroomPage = Loadable({
  loader: () => import('./containers/ClassroomPage'),
  modules: ['./containers/ClassroomPage'],
  loading: Loading,
});

const asyncPostPage = Loadable({
  loader: () => import('./containers/PostPage'),
  modules: ['./containers/PostPage'],
  loading: Loading,
});

const asyncCoursePage = Loadable({
  loader: () => import('./containers/CoursePage'),
  modules: ['./containers/CoursePage'],
  loading: Loading,
});

const asyncSearch = Loadable({
  loader: () => import('./containers/Search'),
  modules: ['./containers/Search'],
  loading: Loading,
});

const asyncErrorPage = Loadable({
  loader: () => import('./components/ErrorPage'),
  modules: ['./components/ErrorPage'],
  loading: Loading,
});

const { NODE_ENV } = process.env;

/**
 * AppRoutes Component contains all the routes defined in our website.
 * As react router v4 has more focus on dynamic routing rather then
 * static routing, so there may be possibility that some component
 * may contain further routing.
 * @returns {XML}
 * @constructor
 */
export default class AppRoutes extends Component {
  componentDidMount() {
    const jssStyles = document.getElementById('jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const theme = createMuiTheme({
      palette: {
        primary,
        secondary,
      },
    });

    return (
      <MuiThemeProvider theme={theme}>
        <AppLayout>
          {/* Only one component can rest on screen which is controlled
           by Switch */}
          <Switch>
            <Route exact path={'/'} component={asyncHome} />

            <Route exact path={'/introduction'} component={asyncIntroduction} />
            <Route exact path={'/services'} component={asyncServices} />
            <Route exact path={'/pricing'} component={asyncPricing} />
            <Route exact path={'/team'} component={asyncTeam} />
            <Route exact path={'/join-us'} component={asyncJoinUs} />
            <Route
              exact
              path={'/developers-training'}
              component={asyncDevelopersTraining}
            />
            {/* <Route exact path={'/survey'} component={asyncSurvey} /> */}
            <Route exact path={'/contact-us'} component={asyncContactUs} />

            <Route exact path={'/donate'} component={asyncDonateUS} />
            <Route exact path={'/privacy'} component={asyncPrivacy} />
            <Route exact path={'/terms'} component={asyncTerms} />
            <Route exact path={'/refund'} component={asyncRefund} />

            <Route exact path={'/sign-in'} component={asyncSignIn} />

            <Route path={'/profile'} component={asyncProfilePage} />
            {/* <Route path={'/dashboard'} component={asyncDashboardPage} /> */}
            <Route
              exact
              path={'/@:username/resume'}
              component={asyncResumePage}
            />

            <Route exact path={'/@:username'} component={asyncElementPage} />
            <Route exact path={'/posts/:postId'} component={asyncPostPage} />
            <Route path={'/courses/:courseId'} component={asyncCoursePage} />

            {NODE_ENV === 'development' ? (
              <Route
                path={'/classrooms/@:username'}
                component={asyncClassroomPage}
              />
            ) : null}

            {NODE_ENV === 'development' ? (
              <Route exact path={'/search'} component={asyncSearch} />
            ) : null}

            {/* This will render Not Found page as non of the above routes
              were matched */}
            <Route component={asyncErrorPage} />
          </Switch>
        </AppLayout>
      </MuiThemeProvider>
    );
  }
}
