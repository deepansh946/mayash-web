/**
 * This component is create to follow or follower
 * @format
 */

const styles = (theme) => ({
  root: {
    margin: 'auto',
  },
  button: {
    borderRadius: '15px',
    fontWeight: 'bold',
  },
});

export default styles;
