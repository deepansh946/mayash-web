/**
 * This component is create to follow or follower
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import { connect } from 'react-redux';

import apiGetFollower from '../../api/follow/get';
import apiCreateFollower from '../../api/follow/create';
import apiDeleteFollower from '../../api/follow/delete';

import styles from './styles';

/**
 * Follow component is designed for displaying a user
 * is following any element or not.
 *
 */
class Follow extends Component {
  static propTypes = {
    /**
     * classes object contains the styles for Follow component.
     */
    classes: PropTypes.object.isRequired,

    /**
     * user object contains user's info. this comes from redux store.
     */
    user: PropTypes.object.isRequired,

    /**
     * element object contains all the info of element of which user is
     * going to follow or unfollow.
     */
    element: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      follow: false,
    };

    this.onClickFollow = this.onClickFollow.bind(this);
    this.onClickUnfollow = this.onClickUnfollow.bind(this);
  }

  async componentWillMount() {
    try {
      const { user, element } = this.props;
      const { id: followerId, token } = user;
      const { id: followingId } = element;

      const res = await apiGetFollower({
        token,
        followerId,
        followingId,
      });

      if (res.statusCode < 300) {
        this.setState({ follow: true });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async onClickFollow() {
    try {
      const { user, element } = this.props;
      const { id: followerId, token } = user;
      const { id: followingId } = element;

      const { statusCode, error } = await apiCreateFollower({
        token,
        followerId,
        followingId,
      });

      if (statusCode >= 300) {
        console.log(error);
        return;
      }

      this.setState({ follow: true });
    } catch (error) {
      console.error(error);
    }
  }

  async onClickUnfollow() {
    try {
      const { user, element } = this.props;
      const { id: followerId, token } = user;
      const { id: followingId } = element;

      const res = await apiDeleteFollower({
        token,
        followerId,
        followingId,
      });

      if (res.statusCode >= 300) {
        console.log(res.error);
        return;
      }

      this.setState({ follow: false });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes } = this.props;
    const { follow } = this.state;

    return (
      <div className={classes.root}>
        <Button
          raised
          color={follow ? 'accent' : null}
          onClick={!follow ? this.onClickFollow : this.onClickUnfollow}
          className={classes.button}
        >
          {!follow ? 'Follow' : 'Following'}
        </Button>
      </div>
    );
  }
}
const mapStateToProps = ({ elements }) => ({ user: elements.user });

export default connect(mapStateToProps)(withStyles(styles)(Follow));
