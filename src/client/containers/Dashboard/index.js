/**
 * Dashboard component of a user
 * It will dispaly user's all post, courses, circle etc.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import Loading from '../../components/Loading';

// import actions from '../../actions';

const AsyncTabSummary = Loadable({
  loader: () => import('./TabSummary'),
  modules: ['./TabSummary'],
  loading: Loading,
});

const AsyncTabMyPosts = Loadable({
  loader: () => import('./TabMyPosts'),
  modules: ['./TabMyPosts'],
  loading: Loading,
});

const AsyncTabMyCourses = Loadable({
  loader: () => import('./TabMyCourses'),
  modules: ['./TabMyCourses'],
  loading: Loading,
});

const AsyncTabMyCircles = Loadable({
  loader: () => import('./TabMyCircles'),
  modules: ['./TabMyCircles'],
  loading: Loading,
});

const AsyncTabTodo = Loadable({
  loader: () => import('./TabTodo'),
  modules: ['./TabTodo'],
  loading: Loading,
});

const AsyncErrorPage = Loadable({
  loader: () => import('../../components/ErrorPage'),
  modules: ['../../components/ErrorPage'],
  loading: Loading,
});

const styles = {
  root: {},
};

// this function will return index of route url in Dashboard component page.
const routeStates = (route = 'default') => {
  const routes = {
    '/dashboard': 0,
    '/dashboard/posts': 1,
    '/dashboard/courses': 2,
    '/dashboard/circles': 3,
    '/dashboard/todos': 4,
    default: 0,
  };

  // if routes[index] value is undefined, it will return 0;
  return routes[route] || 0;
};

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: routeStates(props.location.pathname),
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(event, value) {
    this.setState({ value });
    const { history } = this.props;
    switch (value) {
      case 0:
        history.push('/dashboard');
        break;
      case 1:
        history.push('/dashboard/posts');
        break;
      case 2:
        history.push('/dashboard/courses');
        break;
      case 3:
        history.push('/dashboard/circles');
        break;
      case 4:
        history.push('/dashboard/todos');
        break;
      default:
        history.push('/dashboard');
        break;
    }
  }

  render() {
    const { classes } = this.props;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.onChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Summary" disabled />
            <Tab label="Posts" disabled />
            <Tab label="My Courses" disabled />
            <Tab label="My Circles" />
            <Tab label="TODO" disabled />
          </Tabs>
        </AppBar>
        <Switch>
          <Route exact path="/dashboard" component={AsyncTabSummary} />
          <Route exact path="/dashboard/posts" component={AsyncTabMyPosts} />
          <Route
            exact
            path="/dashboard/courses"
            component={AsyncTabMyCourses}
          />
          <Route
            exact
            path="/dashboard/circles"
            component={AsyncTabMyCircles}
          />
          <Route exact path="/dashboard/todos" component={AsyncTabTodo} />
          <Route component={AsyncErrorPage} />
        </Switch>
      </div>
    );
  }
}

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  // elements: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });
// const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

/**
 * 'connect' method connect state to action creators
 * When we call connect, immediately we call Home component
 * and all states and action creators are passed to Home component
 */
export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(withStyles(styles)(Dashboard));
