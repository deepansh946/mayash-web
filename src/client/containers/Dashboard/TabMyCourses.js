/** @format */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

const styles = {
  root: {},
};

const TabMyCourses = ({ classes }) => (
  <div className={classes.root}>My Courses</div>
);

TabMyCourses.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabMyCourses);
