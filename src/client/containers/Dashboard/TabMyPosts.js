/** @format */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

const styles = {
  root: {},
};

const TabMyPosts = ({ classes }) => (
  <div className={classes.root}>My Posts</div>
);

TabMyPosts.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabMyPosts);
