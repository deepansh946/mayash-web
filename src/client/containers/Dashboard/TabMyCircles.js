/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import MenuItem from 'material-ui/Menu/MenuItem';
import TextField from 'material-ui/TextField';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

const styles = {
  root: {
    padding: '1%',
  },
  menu: {},
};

class TabMyCircles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      circleTypes: [
        {
          value: 'edu',
          label: 'Educational Institute',
        },
        {
          value: 'org',
          label: 'Company or Organisation',
        },
        {
          value: 'location',
          label: 'Location',
          disabled: true,
        },
        {
          value: 'social',
          label: 'Social Circle',
          disabled: true,
        },
      ],

      name: '',
      circleType: 'edu',
      address: '',
      description: '',
      website: '',
      founder: '',
    };

    this.onClickOpen = this.onClickOpen.bind(this);
    this.onRequestClose = this.onRequestClose.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onClickOpen() {
    this.setState((preState) => ({ ...preState, open: true }));
  }

  onRequestClose() {
    this.setState((preState) => ({ ...preState, open: false }));
  }

  onChange(name) {
    return (e) => {
      this.setState({ [name]: e.target.value });
    };
  }

  render() {
    const { classes } = this.props;
    const {
      circleTypes,
      name,
      circleType,
      address,
      description,
      website,
      founder,
    } = this.state;

    return (
      <Grid container spacing={0} className={classes.root}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <Button onClick={this.onClickOpen} disabled>
            Submit Circle
          </Button>
          <Dialog open={this.state.open} onRequestClose={this.onRequestClose}>
            <DialogTitle>Add New Circle</DialogTitle>
            <DialogContent>
              <DialogContentText>
                Submit educational institute or company/organisation you know.
              </DialogContentText>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Name"
                type="text"
                value={name}
                onChange={this.onChange('name')}
                fullWidth
                required
              />
              <TextField
                id="select-currency"
                select
                label="Select"
                className={classes.textField}
                value={circleType}
                onChange={this.onChange('circleType')}
                SelectProps={{
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
                helperText="Please select your currency"
                margin="normal"
              >
                {circleTypes.map(({ value, label, disabled }) => (
                  <MenuItem key={value} value={value} disabled={disabled}>
                    {label}
                  </MenuItem>
                ))}
              </TextField>
              <TextField
                margin="dense"
                id="name"
                label="Address"
                type="text"
                value={address}
                fullWidth
                required
              />
              <TextField
                margin="dense"
                id="name"
                label="Description"
                type="text"
                value={description}
                fullWidth
                required
              />
              <TextField
                margin="dense"
                id="name"
                label="Website"
                type="text"
                value={website}
                fullWidth
                required
              />
              <TextField
                margin="dense"
                id="email"
                label="Founder"
                type="email"
                value={founder}
                fullWidth
                required
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.onRequestClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.onRequestClose} color="primary">
                Subscribe
              </Button>
            </DialogActions>
          </Dialog>
        </Grid>
      </Grid>
    );
  }
}

TabMyCircles.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabMyCircles);
