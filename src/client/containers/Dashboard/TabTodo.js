/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import { findDOMNode } from 'react-dom';

import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui-icons/MoreVert';
import Card, { CardHeader, CardActions, CardContent } from 'material-ui/Card';

import withStyles from 'material-ui/styles/withStyles';

import Input from '../../components/Input';

const styles = {
  root: {},
  gridItem: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
};

class TabTodo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, user } = this.props;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Paper className={classes.avatar} elevation={1}>
                  <Link to={`/@${user.username}`}>
                    <Avatar
                      alt={user.name}
                      src={'/public/photos/mayash-logo-transparent.png'}
                    />
                  </Link>
                </Paper>
              }
              title={
                <Link to={`/@${user.username}`} className={classes.title}>
                  {user.name}
                </Link>
              }
              subheader={
                <Link to={`/@${user.username}`} className={classes.subheader}>
                  @{user.username}
                </Link>
              }
            />
            <CardContent>
              <Input
                // onChange={'d.s,}
                value={'string'}
                placeholder={'string'}
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

TabTodo.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

export default withStyles(styles)(TabTodo);
