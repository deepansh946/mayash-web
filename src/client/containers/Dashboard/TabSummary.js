/** @format */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

const styles = {
  root: {},
};

const TabSummary = ({ classes }) => <div className={classes.root}>Summary</div>;

TabSummary.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabSummary);
