/**
 * Post container component
 * It will display one post with some of actions (eg. Bookmarks, Share, Delete)
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import withStyles from 'material-ui/styles/withStyles';

import Card, { CardMedia, CardContent, CardActions } from 'material-ui/Card';

import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Chip from 'material-ui/Chip';

const styles = (theme) => ({
  root: {},
  card: {
    borderRadius: '8px',
    display: 'flex',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  cover: {
    width: theme.spacing.unit * 10,
    height: theme.spacing.unit * 10,
    margin: theme.spacing.unit * 2,
    borderRadius: '50%',
    flexShrink: 0,
    backgroundColor: theme.palette.background.default,
  },
  chip: {
    // margin: theme.spacing.unit,
  },
  link: {
    textDecoration: 'none',
    '&:link': {
      textDecoration: 'none',
      color: 'accent',
    },
    '&:visited': {
      textDecoration: 'none',
      color: 'black',
    },
    '&:hover': {
      color: 'none',
    },
    '&:active': {
      color: 'none',
    },
  },
  flexGrow: {
    flex: '1 1 auto',
  },
});

class Circle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
    };
  }

  shouldComponentUpdate(nextState, nextProp) {
    return this.state !== nextState || this.props !== nextProp;
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const {
      classes,
      id,
      username,
      name,
      avatar,
      cover,
      circleType,
    } = this.props;

    let circle;

    if (circleType === 'field') {
      circle = 'FIELD';
    }

    if (circleType === 'edu') {
      circle = 'EDUCATIONAL INSTITUTE';
    }

    if (circleType === 'org') {
      circle = 'ORGNIZATION';
    }

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card className={classes.card}>
          <CardMedia className={classes.cover} image={avatar} title="Field" />
          <div className={classes.details}>
            <CardContent>
              <Link to={`@${username}`} className={classes.link}>
                <Typography type="headline">{name}</Typography>
              </Link>
              <Link to={`@${username}`} className={classes.link}>
                <Typography type="subheading">{username}</Typography>
              </Link>
            </CardContent>
            <CardActions>
              <Chip label={circle} className={classes.chip} />
            </CardActions>
          </div>
        </Card>
      </div>
    );
  }
}

Circle.propTypes = {
  classes: PropTypes.object.isRequired,
  id: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  circleType: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  cover: PropTypes.string,
  avatar: PropTypes.string,
};

export default withStyles(styles)(Circle);
