/**
 * For creating course
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';

import { withStyles } from 'material-ui/styles';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';

import Input from '../../components/Input';

import actionCourseCreate from '../../actions/courses/create';

import apiCourseCreate from '../../api/courses/users/create';

const styles = {
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  cardHeader: {},
  flexGrow: {
    flex: '1 1 auto',
  },
};

class CourseCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Course Title...',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ title: e.target.value });
  }

  async onSubmit(e) {
    e.preventDefault();
    const { id, token } = this.props.elements.user;
    const { title } = this.state;
    const body = { title };

    this.setState({ message: 'Creating course...' });

    const { statusCode, error, payload } = await apiCourseCreate({
      token,
      userId: id,
      title,
    });

    if (statusCode >= 300) {
      // handle error
      this.setState({ statusCode, error });
      return;
    }

    this.setState({ message: 'Successfully Created.' });

    setTimeout(() => {
      this.setState({
        valid: false,
        statusCode: 0,
        error: '',
        message: '',

        title: '',
        titleLength: 0,
      });
    }, 1500);

    this.props.actionCourseCreate({
      statusCode,
      ...payload,
      ...body,
    });
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });
  onFocus = () => this.setState({ active: true });

  render() {
    const { active, placeholder, title, message } = this.state;
    const { classes, elements } = this.props;
    const { name, avatar, username } = elements.user;

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={this.state.hover} className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${username}`}>
                  <Avatar
                    alt={name}
                    src={avatar || '/public/photos/mayash-logo-transparent.png'}
                    className={classes.avatar}
                  />
                </Link>
              </Paper>
            }
            title={
              <Input
                value={title}
                onChange={this.onChange}
                placeholder={placeholder}
              />
            }
            className={classes.cardHeader}
            onFocus={this.onFocus}
          />
          {active && message.length ? (
            <CardContent>
              <Typography>{message}</Typography>
            </CardContent>
          ) : null}
          {active ? (
            <CardActions disableActionSpacing>
              <div className={classes.flexGrow} />
              <Button raised color="accent" onClick={this.onSubmit}>
                Create
              </Button>
            </CardActions>
          ) : null}
        </Card>
      </div>
    );
  }
}

CourseCreate.propTypes = {
  classes: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  actionCourseCreate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCourseCreate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CourseCreate),
);
