/** @format */

const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  title: {
    textAlign: 'center',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

export default styles;
