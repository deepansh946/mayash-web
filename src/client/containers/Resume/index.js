/**
 * Resume container
 * This component will display user's resume
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';

import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';

import { convertToRaw } from 'draft-js';

import MayashEditor, { createEditorState } from '../../../lib/mayash-editor';

import apiUserUpdate from '../../api/users/update';

import actionUserUpdate from '../../actions/users/update';

import styles from './styles';

/**
 * Resume component for user's resume
 */
class Resume extends Component {
  constructor(props) {
    super(props);

    const { resume } = props.element;

    this.state = {
      edit: false,
      resume: createEditorState(resume),
    };
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { classes } = this.props;
    const { hover, resume } = this.state;

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={hover} className={classes.card}>
          <CardContent>
            <MayashEditor editorState={resume} onChange={() => {}} readOnly />
          </CardContent>
        </Card>
      </div>
    );
  }
}

Resume.propTypes = {
  classes: PropTypes.object.isRequired,
  // user: PropTypes.object.isRequired,
  element: PropTypes.object.isRequired,
};

export default withStyles(styles)(Resume);
