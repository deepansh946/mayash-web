import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

import Card, { CardHeader } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';

import CoverImage from './CoverImage';
import AvatarComponent from './AvatarComponent';

const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  media: {
    height: 250,
  },
  username: {},
  card: {},
  cardHeaderRoot: {
    minHeight: '100px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  cardHeaderAvatar: {},
  cardHeaderContent: {},
  avatar: {
    width: '8rem',
    position: 'relative',
    height: '8rem',
    border: '1px solid grey',
  },
  avatarIcon: {
    position: 'relative',
    top: '-40px',
    marginBottom: '-48px',
  },
});

class ElementInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, user, element } = this.props;
    const { id: userId, token } = user;
    const { id, username, name, avatar, cover } = element;
    const urlAvatar =
      'https://storage.googleapis.com/mayash-web/drive/avatar/user_m.svg';
    const urlCover =
      'http://ginva.com/wp-content/uploads/2016/08/img_57a000bf9846c.png';

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <CardHeader
            avatar={
              userId === id ? (
                <Avatar
                  alt={name}
                  src={avatar || urlAvatar}
                  image={avatar || urlAvatar}
                  component={(thisProps) => (
                    <AvatarComponent
                      {...thisProps}
                      token={token}
                      userId={userId}
                      actionUserUpdate={this.props.actionUserUpdate}
                    />
                  )}
                  className={classes.avatar}
                />
              ) : (
                <Avatar
                  alt={name}
                  src={avatar || urlAvatar}
                  className={classes.avatar}
                />
              )
            }
            title={name}
            subheader={`@${username}`}
            action={
              userId === id ? (
                <CoverImage
                  token={token}
                  userId={userId}
                  actionUserUpdate={this.props.actionUserUpdate}
                />
              ) : null
            }
            classes={{
              root: classes.cardHeaderRoot,
              avatar: classes.cardHeaderAvatar,
              content: classes.cardHeaderContent,
            }}
            style={{ backgroundImage: `url(${cover || urlCover})` }}
          />
        </Card>
      </div>
    );
  }
}

ElementInfo.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }),

  element: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    avatar: PropTypes.string,
    cover: PropTypes.string,
  }),
  actionUserUpdate: PropTypes.func.isRequired,
};

export default withStyles(styles)(ElementInfo);
