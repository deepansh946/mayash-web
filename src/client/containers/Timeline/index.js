/**
 * Timeline Display component
 * This component will display either course or Post timeline
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

import Post from '../Post';
import Course from '../Course';

const styles = {
  root: {
    flexGrow: '1',
  },
};

class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes, type, posts, courses, element } = this.props;

    return (
      <div className={classes.root}>
        {type === 'posts'
          ? element.posts.map((p) => <Post key={p.postId} postId={p.postId} />)
          : element.courses.map((c) => (
            <Course key={c.courseId} courseId={c.courseId} />
          ))}
      </div>
    );
  }
}

Timeline.propTypes = {
  classes: PropTypes.object.isRequired,
  type: PropTypes.string.isRequired, // 'posts' or 'courses'
  element: PropTypes.object.isRequired,
  posts: PropTypes.object,
  courses: PropTypes.object,
};

export default withStyles(styles)(Timeline);
