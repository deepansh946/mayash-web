/**
 * This is a Layout Component
 * It combine AppDrawer and AppHeader component and displays as a single page
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import classnames from 'classnames';
import Loadable from 'react-loadable';

import withStyles from 'material-ui/styles/withStyles';
// these imports are to optimize webpack bundle, do not remove or change this.
import 'material-ui/Card';
import 'material-ui/Typography';
import 'material-ui/Button';
import 'material-ui/Tooltip';

import Loading from '../../components/Loading';

import actionUserGet from '../../actions/users/get';

const AsyncAppHeader = Loadable({
  loader: () => import('../AppHeader'),
  modules: ['../AppHeader'],
  loading: Loading,
});

const AsyncAppDrawer = Loadable({
  loader: () => import('../AppDrawer'),
  modules: ['../AppDrawer'],
  loading: Loading,
});

const styles = (theme) => ({
  root: {
    display: 'flex',
    alignItems: 'stretch',
    minHeight: '100vh',
    width: '100%',
    backgroundAttachment: 'fixed',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  landingBackground: {
    background:
      'linear-gradient(45deg, rgba(208,133,255,1) 0%,' +
      'rgba(0,128,128,1) 100%)',
  },
  // rootBackground: {
  //   animation: 'fadein 1s linear',
  //   backgroundImage: 'url("https://static.pexels.com/photos/248797/' +
  //   'pexels-photo-248797.jpeg")',
  // },
  // '@keyframes fadein': {
  //   from: {
  //     opacity: '0',
  //   },
  //   to: {
  //     opacity: '1',
  //   },
  // },

  content: {
    width: '100%',
  },
  // extra styling on each breakpoints
  contentMargin: {
    [theme.breakpoints.up('xl')]: {
      marginTop: 66,
    },
    [theme.breakpoints.down('xl')]: {
      marginTop: 65,
    },
    [theme.breakpoints.down('lg')]: {
      marginTop: 64,
    },
    [theme.breakpoints.down('sm')]: {
      marginTop: 56,
    },
  },
});

class AppLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      drawer: false,
      backgroundImage: false,
    };

    this.openDrawer = this.openDrawer.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
  }

  componentDidMount() {
    const { elements } = this.props;
    const { isSignedIn, isFetched, id: userId, token } = elements.user;

    if (isSignedIn && !isFetched) {
      this.props.actionUserGet({ userId, token });
    }

    // fetch('https://static.pexels.com/photos/248797/pexels-photo-248797.jpeg')
    //   .then(() => {
    //     this.setState({ backgroundImage: true });
    //   });
  }

  // This function will set 'drawer' value to 'false'
  // which will close the drawer.
  closeDrawer() {
    this.setState((prevState) => ({ ...prevState, drawer: false }));
  }

  // This function will set 'drawer' value to 'true'
  // which will open the drawer.
  openDrawer() {
    this.setState((prevState) => ({ ...prevState, drawer: true }));
  }

  render() {
    const { drawer } = this.state;
    const { children, classes, elements, location } = this.props;
    const { isSignedIn } = elements.user;
    const { pathname } = location;

    return (
      <div
        className={classnames(classes.root, {
          [`${classes.landingBackground}`]: pathname === '/' && !isSignedIn,
        })}
      >
        {/* openDrawer is passed to the AppHeader component as props */}
        <AsyncAppHeader openDrawer={this.openDrawer} />

        {/* closeDrawer is passed to the AppHeader component as props */}
        <AsyncAppDrawer open={drawer} closeDrawer={this.closeDrawer} />

        <div
          className={classnames(classes.content, {
            [`${classes.contentMargin}`]: !(!isSignedIn && pathname === '/'),
          })}
        >
          {children}
        </div>
      </div>
    );
  }
}

AppLayout.propTypes = {
  // match: PropTypes.object.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  // history: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  children: PropTypes.element.isRequired,

  elements: PropTypes.object.isRequired,

  actionUserGet: PropTypes.func.isRequired,
};

// Map only those state from store that are required by this component.
const mapStateToProps = ({ elements }) => ({ elements });

// Map only those action from actions that are required by this component.
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionUserGet,
    },
    dispatch,
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AppLayout)),
);
