/**
 * This is a Header Component
 * It will be displayed on Head
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Link from 'react-router-dom/Link';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';

import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Menu, { MenuItem } from 'material-ui/Menu';

import MoreVertIcon from 'material-ui-icons/MoreVert';
import MenuIcon from 'material-ui-icons/Menu';

import Search from './Search';

import actionSignOut from '../../actions/users/signOut';

const styles = (theme) => ({
  root: {
    width: '100%',
    // backgroundColor: theme.palette.primary[700],
    color: theme.palette.getContrastText(theme.palette.primary[700]),
  },
  rootHome: {
    boxShadow: 'none',
    background: 'transparent',
  },
  menuButton: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
  },
  title: {
    flex: '0 1 auto',
  },
  titleLink: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    textDecoration: 'none',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  moreVertButton: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
  },
});

class AppHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
      open: false,
    };

    this.onClick = this.onClick.bind(this);
    this.onRequestClose = this.onRequestClose.bind(this);
  }

  onClick(e) {
    this.setState({
      open: true,
      anchorEl: e.currentTarget,
    });
  }

  onRequestClose() {
    this.setState({ open: false });
  }

  onSignOut = () => {
    this.props.actionSignOut();
    this.props.history.push('/');
  };

  render() {
    const { classes, elements, openDrawer } = this.props;
    const { isSignedIn } = elements.user;
    const { open, anchorEl } = this.state;
    const { pathname } = this.props.location;

    return (
      <AppBar
        className={classnames(classes.root, {
          [`${classes.rootHome}`]: !isSignedIn && pathname === '/',
        })}
      >
        <Toolbar>
          <IconButton
            aria-label="Menu"
            onClick={openDrawer}
            className={classnames(classes.menuButton)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            className={classes.title}
            type="title"
            color="inherit"
            noWrap
          >
            {!(!isSignedIn && pathname === '/') ? (
              <Link to={'/'} className={classes.titleLink}>
                Mayash
              </Link>
            ) : null}
          </Typography>
          <div className={classes.flexGrow} />
          {/* {isSignedIn && <Search />} */}
          <IconButton
            aria-label="More"
            aria-owns={open ? 'long-menu' : null}
            aria-haspopup="true"
            onClick={this.onClick}
            className={classnames(classes.moreVertButton)}
          >
            <MoreVertIcon />
          </IconButton>
          <Menu
            id="long-menu"
            anchorEl={anchorEl}
            open={open}
            onRequestClose={this.onRequestClose}
            PaperProps={{
              style: {
                maxHeight: 250,
                width: 200,
              },
            }}
          >
            {isSignedIn ? (
              <MenuItem onClick={this.onSignOut}>Sign Out</MenuItem>
            ) : (
              <MenuItem
                button
                component={Link}
                to={'/sign-in'}
                onClick={this.onRequestClose}
              >
                Sign In
              </MenuItem>
            )}
          </Menu>
        </Toolbar>
      </AppBar>
    );
  }
}

AppHeader.propTypes = {
  // match: PropTypes.object.isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  openDrawer: PropTypes.func.isRequired,
  actionSignOut: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionSignOut,
    },
    dispatch,
  );

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(AppHeader)),
);
