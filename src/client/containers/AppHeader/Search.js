/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyle from 'material-ui/styles/withStyles';
import { fade } from 'material-ui/styles/colorManipulator';

import SearchIcon from 'material-ui-icons/Search';

const styles = (theme) => ({
  root: {
    fontFamily: theme.typography.fontFamily,
    position: 'relative',
    borderRadius: '6px',
    background: fade(theme.palette.common.white, 0.15),
    [theme.breakpoints.up('lg')]: {
      background: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        background: fade(theme.palette.common.white, 0.25),
      },
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 190,
        '&:focus': {
          width: 290,
        },
      },
    },
    [theme.breakpoints.down('lg')]: {
      background: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        background: fade(theme.palette.common.white, 0.25),
      },
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 190,
        '&:focus': {
          width: 290,
        },
      },
    },
    [theme.breakpoints.down('md')]: {
      background: 'none',
      borderRadius: '6px',
      '& $input': {
        transition: theme.transitions.create('width'),
        width: 0,
        borderRadius: '6px',
        '&:focus': {
          width: 130,
          borderRadius: '6px',
          background: fade(theme.palette.common.white, 0.25),
        },
      },
    },
  },
  search: {
    width: theme.spacing.unit * 10,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    font: 'inherit',
    padding: `${theme.spacing.unit}px ${theme.spacing.unit}px ${
      theme.spacing.unit
    }px ${theme.spacing.unit * 9}px`,
    border: 0,
    display: 'block',
    verticalAlign: 'middle',
    whiteSpace: 'normal',
    background: 'none',
    margin: 0, // Reset for Safari
    color: 'inherit',
    width: '100%',
    '&:focus': {
      outline: 0,
    },
  },
});

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    };
  }

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <div className={classes.root}>
        <div className={classes.search}>
          <SearchIcon />
        </div>
        <input
          value={value}
          onChange={(e) => this.setState({ value: e.target.value })}
          className={classes.input}
        />
      </div>
    );
  }
}

SearchBox.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyle(styles)(SearchBox);
