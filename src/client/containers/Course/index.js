/**
 * course page component
 * This component will display onlu one course information
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withRouter from 'react-router/withRouter';
import Link from 'react-router-dom/Link';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Card, {
  CardMedia,
  CardHeader,
  CardContent,
  CardActions,
} from 'material-ui/Card';
import Dialog, { DialogActions, DialogTitle } from 'material-ui/Dialog';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import Paper from 'material-ui/Paper';

import MoreVertIcon from 'material-ui-icons/MoreVert';
import FavoriteIcon from 'material-ui-icons/Favorite';
import TurnedInIcon from 'material-ui-icons/TurnedIn';
import DeleteIcon from 'material-ui-icons/Delete';
import ShareIcon from 'material-ui-icons/Share';

import apiCourseDelete from '../../api/courses/users/delete';
import actionGetElementById from '../../actions/elements/getById';
import actionCourseDelete from '../../actions/courses/delete';

const styles = {
  root: {
    padding: '1%',
  },
  gridContainer: {},
  name: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  username: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  post: {
    padding: '1%',
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  cardMedia: {},
  media: {
    height: 194,
  },
  cardMediaImage: {
    width: '100%',
  },
  margin: {
    border: '0.5px solid rgba(209,209,209,1)',
    marginBottom: '-10px',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

class Course extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      deleteDialog: false,
    };
  }

  componentWillMount() {
    const { elements, courses, courseId } = this.props;
    const { token, id: userId } = elements.user;

    const { authorId } = courses[courseId];
    const author = elements[authorId];

    // if author is not found
    if (userId !== authorId && !author) {
      this.props.actionGetElementById({ id: authorId, token });
    }
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  onClickContent = () => {
    const { courseId } = this.props;

    this.props.history.push(`/courses/${courseId}`);
  };

  deleteDialogOpen = () => {
    this.setState({ deleteDialog: true });
  };

  deleteDialogClose = () => {
    this.setState({ deleteDialog: false });
  };

  deleteCourseClick = async () => {
    const { token, id } = this.props.elements.user;
    const { courseId } = this.props;

    const { statusCode, error } = await apiCourseDelete({
      token,
      userId: id,
      courseId,
    });
    if (statusCode !== 200) {
      // show some error message.
      console.log(error);
      return;
    }

    this.props.actionCourseDelete({ courseId });
    this.setState({ deleteDialog: false });
  };

  render() {
    const { classes, courses, elements, courseId } = this.props;

    const { user } = elements;
    const { isSignedIn, id: userId } = user;

    const course = courses[courseId];

    if (typeof course === 'undefined') {
      return <div />;
    }

    const { title, description, authorId, cover } = course;
    const author = authorId === userId ? user : elements[authorId];

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={this.state.hover} className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Avatar
                  alt={author.name}
                  src={
                    author.avatar ||
                    '/public/photos/mayash-logo-transparent.png'
                  }
                />
              </Paper>
            }
            action={
              <div>
                <Tooltip title="Coming soon" placement="bottom">
                  <IconButton disabled>
                    <MoreVertIcon />
                  </IconButton>
                </Tooltip>
              </div>
            }
            title={
              <Link to={`/@${author.username}`} className={classes.name}>
                {author.name}
              </Link>
            }
            subheader={
              <Link to={`/@${author.username}`} className={classes.username}>
                {`@${author.username}`}
              </Link>
            }
          />
          <CardMedia
            className={classes.media}
            image={
              cover ||
              'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
            }
          />
          <CardContent onClick={this.onClickContent}>
            <Typography type="title" gutterBottom>
              {title}
            </Typography>
            {typeof description === 'string' ? (
              <Typography type="body1" gutterBottom>
                {description}
              </Typography>
            ) : null}
          </CardContent>
          <CardActions>
            <Tooltip title="Add to favorites" placement="bottom">
              <IconButton aria-label="Add to favorites" disabled>
                <FavoriteIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Bookmark" placement="bottom">
              <IconButton aria-label="Book Marks" disabled>
                <TurnedInIcon />
              </IconButton>
            </Tooltip>
            {isSignedIn && userId === author.id ? (
              <div>
                <IconButton aria-label="Delete" onClick={this.deleteDialogOpen}>
                  <DeleteIcon />
                </IconButton>
                <Dialog
                  open={this.state.deleteDialog}
                  onClose={this.deleteDialogClose}
                  aria-labelledby={`course-${courseId}-delete-dialog`}
                >
                  <DialogTitle id={`course-${courseId}-delete-dialog`}>
                    Are you sure?
                  </DialogTitle>
                  <DialogActions>
                    <Button onClick={this.deleteDialogClose} color="primary">
                      Cancel
                    </Button>
                    <Button
                      onClick={this.deleteCourseClick}
                      color="primary"
                      autoFocus
                    >
                      Delete
                    </Button>
                  </DialogActions>
                </Dialog>
              </div>
            ) : null}
            <div className={classes.flexGrow} />
            <IconButton aria-label="Share" disabled>
              <ShareIcon />
            </IconButton>
          </CardActions>
        </Card>
      </div>
    );
  }
}

Course.propTypes = {
  history: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,
  courses: PropTypes.object.isRequired,
  courseId: PropTypes.number.isRequired,

  actionGetElementById: PropTypes.func.isRequired,
  actionCourseDelete: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements, courses }) => ({ elements, courses });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionGetElementById,
      actionCourseDelete,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(withRouter(Course)),
);
