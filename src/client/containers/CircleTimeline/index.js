/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import intersectionBy from 'lodash/intersectionBy';

import Circle from '../Circle';

import apiGetUserCircles from '../../api/users/circles/getAll';

import actionGetAllElements from '../../actions/elements/getAll';
import actionGetAllUserCircles from '../../actions/users/circles/getAll';

const styles = (theme) => ({
  root: {
    display: 'flex-start',
    paddingLeft: '2px',
    paddingRight: '2px',
    // [theme.breakpoints.down('md')]: {
    //   paddingLeft: '2%',
    //   paddingRight: '2%',
    // },
    // [theme.breakpoints.down('sm')]: {
    //   paddingLeft: '2%',
    //   paddingRight: '2%',
    // },
    // [theme.breakpoints.down('xs')]: {
    //   paddingLeft: '2%',
    //   paddingRight: '2%',
    // },
  },
  gridItem: {
    padding: '4px 2px 2px 2px',
  },
});

class CircleTimeline extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentWillMount() {
    const { id, token } = this.props.elements[0];

    const { statusCode, error, payload } = await apiGetUserCircles({
      token,
      userId: id,
    });

    if (statusCode !== 200) {
      // handle error
      console.log(statusCode, error);
      return;
    }

    this.props.actionGetAllElements(payload);
    this.props.actionGetAllUserCircles({
      id,
      circles: payload.map((c) => ({ id: c.id })),
    });
  }

  render() {
    const { classes, elements } = this.props;
    const user = elements[0];

    const circles = intersectionBy(elements, user.circles, 'id');

    return (
      <Grid
        container
        // justify="center"
        // alignContent="center"
        spacing={0}
        // className={classes.root}
        // alignItems="center"
      >
        {typeof circles !== 'undefined' &&
          circles.map((c) => (
            <Grid
              key={c.id}
              item
              xs={6}
              sm={6}
              md={4}
              lg={4}
              xl={4}
              className={classes.gridItem}
            >
              <Circle {...c} />
            </Grid>
          ))}
      </Grid>
    );
  }
}

CircleTimeline.propTypes = {
  classes: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  actionGetAllElements: PropTypes.func.isRequired,
  actionGetAllUserCircles: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionGetAllElements,
      actionGetAllUserCircles,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CircleTimeline),
);
