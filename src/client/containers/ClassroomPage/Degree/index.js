/** @format */

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Degree from './Degree';
import SemesterRoute from '../SemesterRoute';

import NotFound from '../../../components/ErrorPage';

const DegreeRoute = ({ circle, department, match }) => {
  const degreeId = match.params.degreeId;
  const degree = (department.degrees || []).find(
    (d) => d.degreeId === degreeId,
  );

  if (!degree) {
    return <NotFound />;
  }

  return (
    <div>
      <Switch>
        <Route
          exact
          path="/classrooms/@:username/:deptId/:degreeId"
          component={(thisProps) => (
            <Degree {...thisProps} circle={circle} degree={degree} />
          )}
        />
        <Route
          path="/classrooms/@:username/:deptId/:degreeId/:semId"
          component={(thisProps) => (
            <SemesterRoute {...thisProps} circle={circle} degree={degree} />
          )}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default DegreeRoute;
