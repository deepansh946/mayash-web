/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

// This page will be use for all non-existing routes.
import NotFound from '../../../components/ErrorPage';

const styles = {
  root: {},
  cardHeaderRoot: {
    minHeight: '100px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage:
      "url('http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg')",
  },
};

class Degree extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      error: false,
      title: '',
    };
    this.onRequestClose = this.onRequestClose.bind(this);
    this.onClickOpen = this.onClickOpen.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  onRequestClose() {
    this.setState({ open: false });
  }

  onClickOpen() {
    this.setState({ open: true });
  }

  onCancel() {
    this.setState({
      open: false,
      value: '',
      error: false,
    });
  }

  onSubmit() {
    const { open } = this.state;
    const department = this.props.departments.find((d) => (d) =>
      d.deptId === this.props.match.params.deptId,
    );
    this.setState({
      open: false,
      value: 0,
    });
  }

  onChange(e) {
    const { value } = e.target;
    this.setState({ title: value });
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { open, error, title, value } = this.state;
    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <CardHeader className={classes.cardHeaderRoot} title={'Degree'} />
        </Card>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          scrollable
          scrollButtons="auto"
        >
          <Tab label="About" />
          <Tab label="Activity" />
          <Tab label="Semesters" />
          <Tab label="Calendar" />
          <Tab label="Members" />
        </Tabs>

        {value === 0 && <div> About</div>}
        {value === 1 && <div> Activity</div>}
        {value === 2 && <div> Semesters</div>}
        {value === 3 && <div> Calendar</div>}
        {value === 4 && <div> Members</div>}
      </div>
    );
    // <div className={classes.root}>
    //   <Button
    //     raised
    //     color="primary"
    //     className={classes.button}
    //     onClick={this.onClickOpen}
    //   >
    //     Add Semester
    //   </Button>

    //   <Dialog open={open} onRequestClose={this.onRequestClose}>
    //     <DialogTitle>Add Semester</DialogTitle>
    //     <DialogContent>
    //       <TextField
    //         margin="dense"
    //         type="text"
    //         onChange={this.onChange}
    //         label="Semester"
    //         fullWidth
    //       />
    //     </DialogContent>
    //     <DialogActions>
    //       <Button
    //         raised
    //         onClick={this.onSubmit}
    //         color="primary"
    //         // disabled={!!error}
    //       >
    //         Add
    //       </Button>
    //       <Button raised onClick={this.onCancel} color="primary">
    //         Cancel
    //       </Button>
    //     </DialogActions>
    //   </Dialog>
    //   {this.props.degree.semesters.map((sem, i) => (
    //     <div key={i + 1} style={{ paddingTop: '1%' }}>
    //       <Card raised>
    //         <CardHeader
    //           title={`Semester ${i + 1}`}
    //           onClick={() => {
    //             this.props.history.push(
    //               `${this.props.location.pathname}/${i + 1}`,
    //             );
    //           }}
    //         />
    //       </Card>
    //     </div>
    //   ))}
    // </div>
  }
}

Degree.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      username: PropTypes.string.isRequired,
      deptId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  // elements: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }),
  location: PropTypes.object.isRequired,
  degree: PropTypes.object,
  departments: PropTypes.object.isRequired,
};

export default withStyles(styles)(Degree);
