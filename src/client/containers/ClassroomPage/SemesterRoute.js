/** @format */

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Semester from './Semester';
import ClassRoute from './ClassRoute';

import NotFound from '../../components/ErrorPage';

const SemesterRoute = ({ degree, match }) => {
  const { semId } = match.params;
  const semester = degree.semesters[semId];

  if (!semester) {
    return <NotFound />;
  }

  return (
    <div>
      <Switch>
        <Route
          exact
          path="/classrooms/@:username/:deptId/:degreeId/:semId"
          component={(props) => <Semester {...props} semester={semester} />}
        />
        <Route
          path="/classrooms/@:username/:deptId/:degreeId/:semId/:classId"
          component={(props) => <ClassRoute {...props} semester={semester} />}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default SemesterRoute;
