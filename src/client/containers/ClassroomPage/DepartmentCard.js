import React, { Component } from 'react';
import withStyles from 'material-ui/styles/withStyles';
import PropTypes from 'prop-types';

import Card, {
  CardActions,
  CardHeader,
  CardMedia,
  CardContent,
} from 'material-ui/Card';

const styles = (theme) => ({
  root: {
    paddingTop: '1%',
  },
});

class DepartmentCard extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }),
    location: PropTypes.object,
    deptId: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      classes,
      history,
      location,
      title,
      description,
      deptId,
    } = this.props;

    return (
      <div className={classes.root}>
        <Card raised>
          <CardHeader
            title={title}
            subheader={description}
            onClick={() => {
              history.push(`${location.pathname}/${deptId}`);
            }}
          />
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(DepartmentCard);
