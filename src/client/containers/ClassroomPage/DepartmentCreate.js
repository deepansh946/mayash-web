/**
 * classroom page component
 * It will display only one classroom info.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

import AddIcon from 'material-ui-icons/Add';

import actionCreateDepartment from '../../actions/classrooms/departments/create';

const styles = (theme) => ({
  root: {},
  button: {
    position: 'fixed',
    right: '10px',
    bottom: '10px',
    margin: theme.spacing.unit,
  },
});

class DepartmentCreate extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    circleId: PropTypes.number,
    add: PropTypes.func,
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      error: false,
      deptId: '',
      title: '',
    };
  }

  onRequestClose = () => {
    this.setState({ open: false });
  };

  onClickOpen = () => {
    this.setState({ open: true });
  };

  onCancel = () => {
    this.setState({ open: false, value: '', error: false });
  };

  onChangeId = (e) => {
    const { value } = e.target;
    // const department = this.props.departments.find(d =>
    //   d.deptId === this.props.match.params.deptId,
    // );
    this.setState({
      deptId: value,
      // error: department.degrees.find(({ degreeId }) => value === degreeId),
    });
  };

  onSubmit = () => {
    const { deptId, title } = this.state;
    const { user, circleId } = this.props;
    // const { id: userId, token } = user;

    this.props.actionCreateDepartment({ circleId, deptId, title });
  };

  onChangeTitle = (e) => {
    const { value } = e.target;
    this.setState({ title: value });
  };

  onChange = (key) => (e) => {
    this.setState({
      [key]: e.target.value,
    });
  };

  render() {
    const { classes } = this.props;
    const { error, open } = this.state;

    return (
      <div className={classes.root}>
        <Button
          fab
          color="accent"
          aria-label="edit"
          className={classes.button}
          onClick={this.onClickOpen}
        >
          <AddIcon />
        </Button>

        <Dialog open={open} onRequestClose={this.onRequestClose}>
          <DialogTitle>Add Class</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin={'dense'}
              label={'Department Name'}
              onChange={this.onChange('title')}
              fullWidth
              error={!!error}
            />
            <TextField
              margin="dense"
              type="text"
              onChange={this.onChange('degreeId')}
              label="Degree Name"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button
              raised
              onClick={this.onSubmit}
              color="primary"
              disabled={!!error}
            >
              Create
            </Button>
            <Button raised onClick={this.onCancel} color="primary">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = ({ elements }) => ({ user: elements.user });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCreateDepartment,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(DepartmentCreate),
);
