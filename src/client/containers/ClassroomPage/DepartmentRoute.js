/** @format */

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Department from './Department';
import DegreeRoute from './Degree/index';

import NotFound from '../../components/ErrorPage';

const DeparmentRoute = ({ circle, match }) => {
  const { deptId } = match.params;
  const department = (circle.departments || []).find(
    (d) => d.deptId === deptId,
  );

  if (!department) {
    return <NotFound />;
  }

  return (
    <div>
      <Switch>
        <Route
          exact
          path="/classrooms/@:username/:deptId"
          component={(thisProps) => (
            <Department
              {...thisProps}
              circle={circle}
              department={department}
            />
          )}
        />
        <Route
          path="/classrooms/@:username/:deptId/:degreeId"
          component={(thisProps) => (
            <DegreeRoute
              {...thisProps}
              circle={circle}
              department={department}
            />
          )}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default DeparmentRoute;
