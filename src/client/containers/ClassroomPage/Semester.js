/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

// This page will be use for all non-existing routes.
import NotFound from '../../components/ErrorPage';

const styles = {
  root: {},
};

class Semester extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        {this.props.semester.classes.map(({ classId, title }) => (
          <div key={classId} style={{ paddingTop: '1%' }}>
            <Card raised>
              <CardHeader
                title={title}
                onClick={() => {
                  this.props.history.push(
                    `${this.props.location.pathname}/${classId}`,
                  );
                }}
              />
            </Card>
          </div>
        ))}
      </div>
    );
  }
}

Semester.propTypes = {
  // elements: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }),
  location: PropTypes.object.isRequired,
  semester: PropTypes.object.isRequired,
};

export default withStyles(styles)(Semester);
