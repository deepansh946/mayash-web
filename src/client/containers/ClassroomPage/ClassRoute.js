/** @format */

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Class from './Class';

// This page will be use for all non-existing routes.
import NotFound from '../../components/ErrorPage';

const ClassRoute = ({ semester, match }) => {
  const { classId } = match.params;
  const semClass = semester.classes.find((d) => d.classId == classId);

  if (!semClass) {
    return <NotFound />;
  }

  return (
    <div>
      <Switch>
        <Route
          exact
          path="/classrooms/@:username/:deptId/:degreeId/:semId/:classId"
          component={(props) => <Class {...props} semClass={semClass} />}
        />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default ClassRoute;
