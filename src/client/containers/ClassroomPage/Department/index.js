/**
 * classroom page component
 * It will display only one classroom info.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardMedia } from 'material-ui/Card';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

import AddIcon from 'material-ui-icons/Add';
import Avatar from 'material-ui/Avatar';

import TabDegrees from './TabDegrees';
// This page will be use for all non-existing routes.
import NotFound from '../../../components/ErrorPage';

const styles = {
  root: {},
  button: {
    position: 'fixed',
    right: '10px',
    bottom: '10px',
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 100,
    height: 100,
  },
  cardHeaderRoot: {
    minHeight: '100px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage:
      "url('http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg')",
  },
};

class Department extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      error: false,
      value: 1,
      title: '',
      degreeId: '',
    };

    this.onRequestClose = this.onRequestClose.bind(this);
    this.onClickOpen = this.onClickOpen.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onChangeDegId = this.onChangeDegId.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onRequestClose() {
    this.setState({ open: false });
  }

  onClickOpen() {
    this.setState({ open: true });
  }

  onCancel() {
    this.setState({
      open: false,
      title: '',
      error: false,
    });
  }

  onChangeDegId(e) {
    const { title } = e.target;
    const department = this.props.departments.find(
      (d) => d.deptId === this.props.match.params.deptId,
    );
    this.setState({
      degreeId: title,
      error: department.degrees.find(({ degreeId }) => title === degreeId),
    });
  }

  onSubmit() {
    const { open } = this.state;
    const department = this.props.departments.find(
      (d) => d.deptId === this.props.match.params.deptId,
    );
    this.setState({
      open: false,
    });
  }

  onChange(e) {
    const { title } = e.target;
    this.setState({ title });
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, circle, department } = this.props;
    const { value, error, open } = this.state;

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <CardHeader
            className={classes.cardHeaderRoot}
            title={'Computer Science Department'}
          />
        </Card>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          scrollable
          scrollButtons="auto"
        >
          <Tab label="About" />
          <Tab label="Activity" />
          <Tab label="Degrees" />
          <Tab label="Calendar" />
          <Tab label="Members" />
        </Tabs>

        {value === 0 && <div> About</div>}
        {value === 1 && <div> Activity</div>}
        {value === 2 && <TabDegrees circle={circle} {...this.props} />}
        {value === 3 && <div> Calendar</div>}
        {value === 4 && <div> Members</div>}
      </div>
    );
  }
}

Department.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      username: PropTypes.string.isRequired,
      deptId: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  // elements: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }),
  location: PropTypes.object.isRequired,
  departments: PropTypes.object,
  department: PropTypes.object.isRequired,
};

export default withStyles(styles)(Department);
