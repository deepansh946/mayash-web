/**
 * classroom page component
 * It will display only the degrees of a classroom
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardMedia } from 'material-ui/Card';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

import AddIcon from 'material-ui-icons/Add';
import Avatar from 'material-ui/Avatar';

// This page will be use for all non-existing routes.
import NotFound from '../../../components/ErrorPage';

import actionCreateDegree from '../../../actions/classrooms/degrees/create';

const styles = {
  root: {},
  button: {
    position: 'fixed',
    right: '10px',
    bottom: '10px',
  },
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    width: 100,
    height: 100,
  },
  cardHeaderRoot: {
    minHeight: '100px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage:
      "url('http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg')",
  },
};

class TabDegrees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      error: false,
      value: 0,
      title: '',
      degreeId: '',
    };

    this.onRequestClose = this.onRequestClose.bind(this);
    this.onClickOpen = this.onClickOpen.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onRequestClose() {
    this.setState({ open: false });
  }

  onClickOpen() {
    this.setState({ open: true });
  }

  onCancel() {
    this.setState({
      open: false,
      title: '',
      error: false,
    });
  }

  onSubmit() {
    const { degreeId, title, open } = this.state;

    const { id: circleId } = this.props.circle;
    const { deptId } = this.props.match.params;

    this.props.actionCreateDegree({
      circleId,
      deptId,
      degreeId,
      title,
    });
  }

  onChange = (value) => (e) => {
    this.setState({ [value]: e.target.value });
  };

  render() {
    const { classes, department, history, location } = this.props;
    const { degrees = [] } = department;

    const { error, open } = this.state;

    return (
      <div className={classes.root}>
        <Button
          fab
          raised
          color="primary"
          aria-label="edit"
          className={classes.button}
          onClick={this.onClickOpen}
        >
          <AddIcon />
        </Button>
        <Dialog open={open} onRequestClose={this.onRequestClose}>
          <DialogTitle>Add degree</DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              margin={'dense'}
              label={'Degree Id'}
              onChange={this.onChange('degreeId')}
              fullWidth
              error={!!error}
            />
            <TextField
              margin="dense"
              type="text"
              onChange={this.onChange('title')}
              label="Degree Name"
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button
              raised
              onClick={this.onSubmit}
              color="primary"
              disabled={!!error}
            >
              Create
            </Button>
            <Button raised onClick={this.onCancel} color="primary">
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
        {degrees.map(({ degreeId, title }) => (
          <div key={degreeId} style={{ paddingTop: '1%' }}>
            <Card raised>
              <CardHeader
                title={title}
                onClick={() => {
                  history.push(`${location.pathname}/${degreeId}`);
                }}
              />
            </Card>
          </div>
        ))}
      </div>
    );
  }
}

TabDegrees.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      username: PropTypes.string.isRequired,
      deptId: PropTypes.string.isRequired,
    }).isRequired,
  }),
  // elements: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }),
  location: PropTypes.object,
  departments: PropTypes.object,
  department: PropTypes.object,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCreateDegree,
    },
    dispatch,
  );

export default connect(null, mapDispatchToProps)(
  withStyles(styles)(TabDegrees),
);
