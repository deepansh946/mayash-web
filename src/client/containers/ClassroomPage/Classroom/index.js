/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Tabs, { Tab } from 'material-ui/Tabs';

import DepartmentCreate from '../DepartmentCreate';
import DepartmentCard from '../DepartmentCard';

import TabAbout from './TabAbout';

const styles = {
  root: {},
  cardHeaderRoot: {
    minHeight: '100px',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage:
      "url('http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg')",
  },
};

class Classroom extends Component {
  constructor(props) {
    super(props);
    this.state = { value: 0 };
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes, circle } = this.props;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <Card className={classes.card}>
          <CardHeader
            className={classes.cardHeaderRoot}
            title={'IIT Dhanbad Class'}
          />
        </Card>
        <Tabs
          value={value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          scrollable
          scrollButtons="auto"
        >
          <Tab label="About" />
          <Tab label="Assignments" />
          <Tab label="Attendance" />
          <Tab label="Exam" />
        </Tabs>

        {value === 0 && <TabAbout />}
        {value === 1 && <div>Assignments</div>}
        {value === 2 && <div>Attendance</div>}
        {value === 3 && <div>Exam</div>}
      </div>
    );
  }
}

Classroom.propTypes = {
  classes: PropTypes.object.isRequired,

  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  location: PropTypes.object.isRequired,

  circle: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    departments: PropTypes.array,
  }).isRequired,
};

export default withStyles(styles)(Classroom);
