import React, { Component } from 'react';

import Input, { InputLabel } from 'material-ui/Input';
import { MenuItem } from 'material-ui/Menu';
import { FormControl } from 'material-ui/Form';
import { ListItemText } from 'material-ui/List';
import Select from 'material-ui/Select';
import withStyles from 'material-ui/styles/withStyles';

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
    maxWidth: 300,
  },
});
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const departments = ['CS', 'Mech', 'Civil'];

const degrees = ['BTech', 'Mtech', 'Ph.D', 'B.E.'];

const semesters = ['1', '2', '3', '4'];

class TabAbout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departments: [],
      degrees: [],
      semesters: [],
    };
  }

  handleChange = (key) => (event) => {
    this.setState({ [key]: event.target.value });
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <div className={classes.root}>
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="select-multiple">Departments</InputLabel>
          <Select
            multiple
            value={this.state.departments}
            onChange={this.handleChange('departments')}
            input={<Input id="select-multiple" />}
            MenuProps={MenuProps}
          >
            {departments.map((name) => (
              <MenuItem
                key={name}
                value={name}
                style={{
                  fontWeight: 5,
                }}
              >
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <FormControl className={classes.formControl}>
          <InputLabel htmlFor="select-multiple">Degrees</InputLabel>
          <Select
            multiple
            value={this.state.departments}
            onChange={this.handleChange('degrees')}
            input={<Input id="select-multiple" />}
            MenuProps={MenuProps}
          >
            {degrees.map((name) => (
              <MenuItem
                key={name}
                value={name}
                style={{
                  fontWeight: 5,
                }}
              >
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        {/* <FormControl className={classes.formControl}>
          <InputLabel htmlFor="select-multiple">Semesters</InputLabel>
          <Select
            multiple
            value={this.state.departments}
            onChange={this.handleChange('semesters')}
            input={<Input id="select-multiple" />}
            MenuProps={MenuProps}
          >
            {semesters.map((name) => (
              <MenuItem
                key={name}
                value={name}
                style={{
                  fontWeight: 5,
                }}
              >
                {name}
              </MenuItem>
            ))}
          </Select>
        </FormControl> */}
      </div>
    );
  }
}

export default withStyles(styles)(TabAbout);
