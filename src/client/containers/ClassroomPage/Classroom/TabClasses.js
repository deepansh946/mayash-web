import React, { Component } from 'react';

import withStyles from 'material-ui/styles/withStyles';

import DepartmentCard from '../DepartmentCard';
import DepartmentCreate from '../DepartmentCreate';

const styles = (theme) => ({
  root: {},
});

class TabClasses extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, circle } = this.props;

    const { departments = [] } = circle;
    return (
      <div className={classes.root}>
        <DepartmentCreate circleId={circle.id} />
        {departments.map((d) => (
          <DepartmentCard key={d.deptId} {...d} {...this.props} />
        ))}
      </div>
    );
  }
}

export default withStyles(styles)(TabClasses);
