/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';
import Button from 'material-ui/Button';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

// This page will be use for all non-existing routes.
import NotFound from '../../components/ErrorPage';

const styles = {
  root: {},
};

class Class extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, semClass } = this.props;
    const { title } = semClass;

    return (
      <div className={classes.root}>
        <Card raised>
          <CardHeader title={title} />
        </Card>
      </div>
    );
  }
}

Class.propTypes = {
  // elements: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  semClass: PropTypes.object.isRequired,
};

export default withStyles(styles)(Class);
