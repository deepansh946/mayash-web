/**
 * Post container component
 * It will display one post with some of actions (eg. Bookmarks, Share, Delete)
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withRouter from 'react-router/withRouter';
import Link from 'react-router-dom/Link';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';

import Card, {
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
} from 'material-ui/Card';
import Dialog, { DialogActions, DialogTitle } from 'material-ui/Dialog';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Badge from 'material-ui/Badge';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import Tooltip from 'material-ui/Tooltip';

import MoreVertIcon from 'material-ui-icons/MoreVert';
import FavoriteIcon from 'material-ui-icons/Favorite';
import TurnedInIcon from 'material-ui-icons/TurnedIn';
import CommentIcon from 'material-ui-icons/Comment';
import DeleteIcon from 'material-ui-icons/Delete';
import RemoveRedEyeIcon from 'material-ui-icons/RemoveRedEye';
import ShareIcon from 'material-ui-icons/Share';

// All actions are defined here.
import getElementById from '../../actions/elements/getById';

import apiPostDelete from '../../api/posts/users/delete';
import actionPostDelete from '../../actions/posts/delete';

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  title: {
    // color: '#365899',
    cursor: 'pointer',
    textDecoration: 'none',
  },
  subheader: {
    // color: '#365899',
    cursor: 'pointer',
    textDecoration: 'none',
  },
  media: {
    height: 194,
  },
  favoriteIconActive: {
    color: '#F50057',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  badge: {
    margin: `0 ${theme.spacing.unit * 2}px`,
    color: 'blue',
  },
  margin: {
    border: '0.5px solid rgba(209,209,209,1)',
    marginBottom: '-10px',
  },
  content: {
    textAlign: 'justify',
  },
});

class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      favorite: false,
      deleteDialog: false,
    };
  }

  // componentWillMount is executed before rendering
  componentWillMount() {
    const { elements, posts, postId } = this.props;
    const { token, id: userId } = elements.user;
    const { authorId } = posts[postId];

    const author = elements[authorId];

    // if author is not found
    if (userId !== authorId && !author) {
      this.props.getElementById({ id: authorId, token });
    }
  }

  onClickFavorite = async () => {
    const { favorite } = this.state;

    this.setState({ favorite: !favorite });
  };

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  onClickPost = () => this.props.history.push(`/posts/${this.props.postId}`);

  deleteDialogOpen = () => {
    this.setState({ deleteDialog: true });
  };

  deleteDialogClose = () => {
    this.setState({ deleteDialog: false });
  };

  deletePostClick = async () => {
    const { token, id } = this.props.elements.user;
    const { postId } = this.props;

    const { statusCode, error } = await apiPostDelete({
      token,
      userId: id,
      postId,
    });

    if (statusCode !== 200) {
      // show some error message.
      console.log(error);
      return;
    }

    this.props.actionPostDelete({ postId });
    this.setState({ deleteDialog: false });
  };

  render() {
    const { classes, posts, elements, postId } = this.props;
    const { user } = elements;
    const { isSignedIn, id: userId } = user;

    const post = posts[postId];

    const { favorite } = this.state;

    if (typeof post === 'undefined') {
      return <div />;
    }

    const { cover, title, description, authorId } = post;
    const author = authorId === userId ? user : elements[authorId];

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={this.state.hover} className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${author.username}`}>
                  <Avatar
                    alt={author.name}
                    src={'/public/photos/mayash-logo-transparent.png'}
                  />
                </Link>
              </Paper>
            }
            action={
              <Tooltip title="Coming soon" placement="bottom">
                <IconButton disabled>
                  <MoreVertIcon />
                </IconButton>
              </Tooltip>
            }
            title={
              <Link to={`/@${author.username}`} className={classes.title}>
                {author.name}
              </Link>
            }
            subheader={
              <Link to={`/@${author.username}`} className={classes.subheader}>
                @{author.username}
              </Link>
            }
          />
          <CardMedia
            className={classes.media}
            image={
              cover ||
              'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
            }
            title={title}
          />
          <CardContent onClick={this.onClickPost}>
            <Typography type="title">{title}</Typography>
            {typeof description === 'string' ? (
              <Typography type="body1" gutterBottom className={classes.content}>
                {description}
              </Typography>
            ) : null}
          </CardContent>
          {/* <div className={classes.margin} /> */}
          <CardActions>
            <IconButton onClick={this.onClickFavorite}>
              <Badge badgeContent={0}>
                <FavoriteIcon
                  className={classnames({
                    [`${classes.favoriteIconActive}`]: favorite,
                  })}
                />
              </Badge>
            </IconButton>
            <IconButton aria-label="Comment">
              <Badge badgeContent={0}>
                <CommentIcon />
              </Badge>
            </IconButton>
            <IconButton aria-label="Book Marks">
              <TurnedInIcon />
            </IconButton>
            {isSignedIn && userId === authorId ? (
              <div>
                <IconButton aria-label="Delete" onClick={this.deleteDialogOpen}>
                  <DeleteIcon />
                </IconButton>
                <Dialog
                  open={this.state.deleteDialog}
                  onClose={this.deleteDialogClose}
                  aria-labelledby={`post-${postId}-delete-dialog`}
                >
                  <DialogTitle id={`post-${postId}-delete-dialog`}>
                    Are you sure?
                  </DialogTitle>
                  <DialogActions>
                    <Button onClick={this.deleteDialogClose} color="primary">
                      Cancel
                    </Button>
                    <Button
                      onClick={this.deletePostClick}
                      color="primary"
                      autoFocus
                    >
                      Delete
                    </Button>
                  </DialogActions>
                </Dialog>
              </div>
            ) : null}
            <div className={classes.flexGrow} />
            <IconButton aria-label="Share">
              <Badge badgeContent={0}>
                <RemoveRedEyeIcon />
              </Badge>
            </IconButton>
            <IconButton aria-label="Share" disabled>
              <ShareIcon />
            </IconButton>
          </CardActions>
        </Card>
      </div>
    );
  }
}

Post.propTypes = {
  history: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,
  posts: PropTypes.object.isRequired,

  // post: PropTypes.shape({
  //   authorId: PropTypes.number.isRequired,
  //   postId: PropTypes.number.isRequired,
  //   title: PropTypes.string.isRequired,
  //   description: PropTypes.string,
  // }).isRequired,

  postId: PropTypes.number.isRequired,

  getElementById: PropTypes.func.isRequired,
  actionPostDelete: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements, posts }) => ({ elements, posts });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getElementById,
      actionPostDelete,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(withRouter(Post)),
);
