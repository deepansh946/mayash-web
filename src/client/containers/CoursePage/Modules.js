/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import Loadable from 'react-loadable';

import Loading from '../../components/Loading';

const styles = {
  root: {
    // padding: '1%',
  },
  gridItem: {
    paddingTop: '1%',
    paddingLeft: '1%',
    paddingRight: '1%',
  },
};

const AsyncModuleCreate = Loadable({
  loader: () => import('./ModuleCreate.js'),
  modules: ['./ModuleCreate'],
  loading: Loading,
});

const AsyncModule = Loadable({
  loader: () => import('./Module.js'),
  modules: ['./Module'],
  loading: Loading,
});

class Modules extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, user, course } = this.props;
    const { isSignedIn, id: userId } = user;
    const { authorId, courseModules } = course;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        {isSignedIn &&
          userId === authorId && (
            <Grid
              item
              xs={12}
              sm={10}
              md={8}
              lg={8}
              xl={8}
              className={classes.gridItem}
            >
              <AsyncModuleCreate course={course} />
            </Grid>
          )}
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={8}
          xl={8}
          className={classes.gridItem}
        >
          {typeof courseModules !== 'undefined' &&
            courseModules.map((m, i) => (
              <AsyncModule
                key={m.moduleId}
                authorId={authorId}
                index={i + 1}
                {...m}
              />
            ))}
        </Grid>
      </Grid>
    );
  }
}

Modules.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,
};

export default withStyles(styles)(Modules);
