/**
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import IconButton from 'material-ui/IconButton';
import InsertPhotoIcon from 'material-ui-icons/InsertPhoto';

import apiCourseUpdate from '../../../api/courses/users/update';
import apiPhotoUpload from '../../../api/photos/courses/users/create';

class CoverImage extends Component {
  static propTypes = {
    style: PropTypes.object.isRequired,
    className: PropTypes.string.isRequired,
    actionCourseUpdate: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};

    this.onClick = this.onClick.bind(this);
    // this.onChange = this.onChange.bind(this);
  }

  onClick() {
    this.photo.value = null;
    this.photo.click();
  }

  onChange = async (e) => {
    if (e) {
      e.preventDefault();
    }

    const file = e.target.files[0];

    if (file.type.indexOf('image/') === 0) {
      const formData = new FormData();
      formData.append('photo', file);

      const { authorId: userId, courseId, token } = this.props;

      const { statusCode, error, payload } = await apiPhotoUpload({
        token,
        userId,
        courseId,
        formData,
      });

      if (statusCode >= 300) {
        // handle Error
        console.error(statusCode, error);
      }

      const { photoUrl: cover } = payload;

      apiCourseUpdate({
        token,
        userId,
        courseId,
        cover,
      });

      this.props.actionCourseUpdate({
        courseId,
        cover,
      });
    }
  };

  render() {
    return (
      <div
        style={{
          position: 'relative',
          ...this.props.style,
        }}
        className={this.props.className}
      >
        <IconButton aria-label="Insert Photo" onClick={this.onClick}>
          <InsertPhotoIcon />
          <input
            type="file"
            accept="image/jpeg|png|gif"
            onChange={this.onChange}
            ref={(photo) => {
              this.photo = photo;
            }}
            style={{ display: 'none' }}
          />
        </IconButton>
      </div>
    );
  }
}

export default CoverImage;
