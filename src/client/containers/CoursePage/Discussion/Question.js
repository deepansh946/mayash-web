/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { convertToRaw } from 'draft-js';

import withStyles from 'material-ui/styles/withStyles';

import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import Card from 'material-ui/Card';
import CardHeader from 'material-ui/Card/CardHeader';
import CardContent from 'material-ui/Card/CardContent';
import IconButton from 'material-ui/IconButton';

import MoreVertIcon from 'material-ui-icons/MoreVert';
import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';

import Loadable from 'react-loadable';

import Loading from '../../../components/Loading';

import Title from '../../../components/Title';
import MayashEditor, { createEditorState } from '../../../../lib/mayash-editor';

import apiUpdate from '../../../api/courses/users/discussion/update';
import actionUpdate from '../../../actions/courses/users/discussion/update';

const AsyncAnswerTimeline = Loadable({
  loader: () => import('./AnswerTimeline'),
  modules: ['./AnswerTimeline'],
  loading: Loading,
});

const styles = {
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
};

class Question extends Component {
  constructor(props) {
    super(props);
    const { course, match } = props;
    const { questionId } = match.params;
    const { title, data } = course.discussion[questionId];

    this.state = {
      hover: false,
      edit: false,

      title,
      data: createEditorState(data),
    };
  }

  onEdit = () => {
    const { edit } = this.state;
    this.setState({ edit: !edit });
  };

  onSave = async () => {
    const { edit, title, data } = this.state;
    const { user, course, match } = this.props;
    const { id: userId, token } = user;
    const { courseId, questionId } = match.params;

    const { title: titleProp } = course.discussion[questionId];

    let body = {};

    if (title !== titleProp) {
      body = { ...body, title };
    }

    body.data = convertToRaw(data.getCurrentContent());

    const { statusCode, error } = await apiUpdate({
      token,
      userId,
      courseId,
      questionId,
      ...body,
    });

    if (statusCode >= 300) {
      // handle error;
      console.error(error);
      return;
    }

    this.props.actionUpdate({ courseId, questionId, ...body });

    // when editing is done set 'edit' to false
    this.setState({ edit: !edit });
  };

  onChangeTitle = (e) => this.setState({ title: e.target.value });
  onChange = (data) => this.setState({ data });

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { classes, user, course, match } = this.props;
    const questionId = parseInt(match.params.questionId, 10);
    const { authorId, timestamp } = course.discussion[questionId];

    const { hover, edit, title, data } = this.state;

    const author = { authorId, username: 'hbarve1', name: 'Himank' };

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={hover} className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${author.username}`}>
                  <Avatar
                    alt={author.name}
                    src={
                      author.avatar ||
                      '/public/photos/mayash-logo-transparent.png'
                    }
                  />
                </Link>
              </Paper>
            }
            title={author.name}
            subheader={moment(timestamp).fromNow()}
            action={
              <div>
                <IconButton onClick={edit ? this.onSave : this.onEdit}>
                  {edit ? <SaveIcon /> : <EditIcon />}
                </IconButton>
                <IconButton>
                  <MoreVertIcon />
                </IconButton>
              </div>
            }
          />
          <CardContent>
            <Title
              readOnly={!edit}
              value={title}
              onChange={this.onChangeTitle}
            />
            <MayashEditor
              editorState={data}
              onChange={this.onChange}
              readOnly={!edit}
              placeholder={'Explain your question here...'}
            />
          </CardContent>
          <AsyncAnswerTimeline
            user={user}
            course={course}
            questionId={questionId}
          />
        </Card>
      </div>
    );
  }
}

Question.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      courseId: PropTypes.string, // this should be number.
      questionId: PropTypes.string, // this should be number.
    }).isRequired,
  }).isRequired,
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,

  actionUpdate: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionUpdate,
    },
    dispatch,
  );

export default connect(null, mapDispatchToProps)(withStyles(styles)(Question));
