/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Loadable from 'react-loadable';

import Loading from '../../../components/Loading';

import apiGetAllQuestions from '../../../api/courses/discussions/getAll';
import actionGetAllQuestions from '../../../actions/courses/discussion/getAll';

const AsyncErrorPage = Loadable({
  loader: () => import('../../../components/ErrorPage'),
  modules: ['../../../components/ErrorPage'],
  loading: Loading,
});

const styles = {
  root: {},
};

const AsyncQuestionTimeline = Loadable({
  loader: () => import('./QuestionTimeline.js'),
  modules: ['./QuestionTimeline'],
  loading: Loading,
});

const AsyncQuestion = Loadable({
  loader: () => import('./Question.js'),
  modules: ['./Question'],
  loading: Loading,
});

class DiscussionRoute extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const { token } = this.props.user;
    const { courseId, discussion } = this.props.course;

    if (discussion) {
      return;
    }

    const { statusCode, payload } = await apiGetAllQuestions({
      token,
      courseId,
    });

    if (statusCode >= 300) {
      return;
    }

    this.props.actionGetAllQuestions({
      courseId,
      statusCode,
      discussion: payload,
    });
  }

  render() {
    const { classes, user, course } = this.props;

    return (
      <div className={classes.root}>
        <Switch>
          <Route
            exact
            path="/courses/:courseId/discussion"
            component={(restProps) => (
              <AsyncQuestionTimeline
                user={user}
                course={course}
                {...restProps}
              />
            )}
          />
          <Route
            exact
            path="/courses/:courseId/discussion/:questionId"
            component={(restProps) => (
              <AsyncQuestion user={user} course={course} {...restProps} />
            )}
          />
          <Route component={AsyncErrorPage} />
        </Switch>
      </div>
    );
  }
}

DiscussionRoute.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,

  actionGetAllQuestions: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionGetAllQuestions,
    },
    dispatch,
  );

export default connect(null, mapDispatchToProps)(
  withStyles(styles)(DiscussionRoute),
);
