/**
 * PostCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create post for
 * their profile only.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import CardHeader from 'material-ui/Card/CardHeader';
import CardContent from 'material-ui/Card/CardContent';
import CardActions from 'material-ui/Card/CardActions';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import Input from '../../../components/Input';

import apiCreate from '../../../api/courses/users/discussion/answers/create';
import actionCreate from '../../../actions/courses/users/discussion/answers/create';

/**
 * Define all styles here for PostCreate Component.
 */
const styles = {
  root: {
    padding: '1%',
  },
  gridContainer: {},
  post: {
    padding: '1%',
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

class AnswerCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      title: '',
      titleLength: 0,

      placeholder: 'Answer Title...',
      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const title = e.target.value;
    const titleLength = title.length;
    const valid = titleLength > 0 && titleLength < 148;

    this.setState({
      title,
      titleLength,
      valid,
      error: !valid ? 'Title length should be in between 1 to 148.' : '',
    });
  }

  async onSubmit(e) {
    try {
      const { user, course, questionId } = this.props;
      const { id: userId, token } = user;
      const { courseId } = course;

      const { title } = this.state;

      this.setState({ message: 'Creating Answer...' });

      const { statusCode, error, payload } = await apiCreate({
        token,
        userId,
        courseId,
        questionId,
        title,
      });

      if (statusCode >= 300) {
        // handle error
        this.setState({ statusCode, error });
        return;
      }

      this.setState({ message: 'Successfully Created.' });

      setTimeout(() => {
        this.setState({
          valid: false,
          statusCode: 0,
          error: '',
          message: '',

          title: '',
          titleLength: 0,
        });
      }, 1500);

      this.props.actionCreate({
        statusCode,
        courseId,
        questionId,
        authorId: userId,
        title,
        ...payload,
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { active, placeholder, title, message } = this.state;
    const { classes, user } = this.props;
    const { name, avatar } = user;

    return (
      <div className={classes.root}>
        <CardHeader
          avatar={
            <Avatar
              alt={name}
              src={avatar || '/favicon.ico'}
              className={classes.avatar}
            />
          }
          title={
            <Input
              value={title}
              onChange={this.onChange}
              placeholder={placeholder}
            />
          }
          className={classes.cardHeader}
          onFocus={() => this.setState({ active: true })}
        />
        {active && message.length ? (
          <CardContent>
            <Typography>{message}</Typography>
          </CardContent>
        ) : null}
        {active ? (
          <CardActions disableActionSpacing>
            <div className={classes.flexGrow} />
            <Button raised color="accent" onClick={this.onSubmit}>
              Create
            </Button>
          </CardActions>
        ) : null}
      </div>
    );
  }
}

AnswerCreate.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,
  questionId: PropTypes.number.isRequired,

  actionCreate: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCreate,
    },
    dispatch,
  );

export default connect(null, mapDispatchToProps)(
  withStyles(styles)(AnswerCreate),
);
