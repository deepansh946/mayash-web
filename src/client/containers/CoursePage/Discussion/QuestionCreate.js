/**
 * QuestionCreate component will be used to create new question.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create question.
 * i.e. person should be logged in and user can create question for any course.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

import Input from '../../../components/Input';

import actionQuestionCreate from '../../../actions/courses/users/discussion/create';
import apiQuestionCreate from '../../../api/courses/users/discussion/create';

/**
 * Define all styles here for PostCreate Component.
 */
const styles = {
  root: {
    padding: '1%',
  },
  gridContainer: {},
  post: {
    padding: '1%',
  },
  cardHeader: {
    // textAlign: 'center'
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

class QuestionCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      title: '',
      titleLength: 0,

      placeholder: 'Question Title...',
      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const title = e.target.value;
    const titleLength = title.length;
    const valid = titleLength > 0 && titleLength < 148;

    this.setState({
      title,
      titleLength,
      valid,
      error: !valid ? 'Title length should be in between 1 to 148.' : '',
    });
  }
  async onSubmit(e) {
    try {
      const { elements, course } = this.props;
      const { id: userId, token } = elements.user;
      const { courseId } = course;

      const { title } = this.state;

      this.setState({ message: 'Creating course module...' });

      const { statusCode, error, payload } = await apiQuestionCreate({
        token,
        userId,
        courseId,
        title,
      });

      if (statusCode >= 300) {
        // handle error
        this.setState({ statusCode, error });
        return;
      }

      this.setState({ message: 'Successfully Created.' });

      setTimeout(() => {
        this.setState({
          valid: false,
          statusCode: 0,
          error: '',
          message: '',

          title: '',
          titleLength: 0,
        });
      }, 1500);

      this.props.actionQuestionCreate({
        statusCode,
        authorId: userId,
        ...payload,
        title,
      });
    } catch (error) {
      console.error(error);
    }
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { active, placeholder, title, message } = this.state;
    const { classes, elements } = this.props;
    const { name, avatar } = elements.user;

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card className={classes.card} raised={this.state.hover}>
          <CardHeader
            avatar={
              <Avatar
                alt={name}
                src={avatar || '/favicon.ico'}
                className={classes.avatar}
              />
            }
            title={
              <Input
                value={title}
                onChange={this.onChange}
                placeholder={placeholder}
              />
            }
            className={classes.cardHeader}
            onFocus={() => this.setState({ active: true })}
          />
          {active && message.length ? (
            <CardContent>
              <Typography>{message}</Typography>
            </CardContent>
          ) : null}
          {active ? (
            <CardActions disableActionSpacing>
              <div className={classes.flexGrow} />
              <Button onClick={this.onSubmit}>Create</Button>
            </CardActions>
          ) : null}
        </Card>
      </div>
    );
  }
}

QuestionCreate.propTypes = {
  classes: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,

  actionQuestionCreate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionQuestionCreate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(QuestionCreate),
);
