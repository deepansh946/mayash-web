/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import Loadable from 'react-loadable';

import Loading from '../../../components/Loading';

const styles = {
  root: {
    // padding: '1%',
  },
  gridItem: {
    paddingTop: '1%',
    paddingLeft: '1%',
    paddingRight: '1%',
  },
};

const AsyncQuestionCreate = Loadable({
  loader: () => import('./QuestionCreate.js'),
  modules: ['./QuestionCreate'],
  loading: Loading,
});

const AsyncQuestionCard = Loadable({
  loader: () => import('./QuestionCard.js'),
  modules: ['./QuestionCard'],
  loading: Loading,
});

class QuestionTimeline extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, user, course } = this.props;
    const { isSignedIn } = user;
    const { discussion } = course;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        {isSignedIn && (
          <Grid
            item
            xs={12}
            sm={10}
            md={8}
            lg={8}
            xl={8}
            className={classes.gridItem}
          >
            <AsyncQuestionCreate course={course} />
          </Grid>
        )}
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={8}
          xl={8}
          className={classes.gridItem}
        >
          {typeof discussion !== 'undefined' &&
            Object.values(discussion).map((q) => (
              <AsyncQuestionCard key={q.questionId} {...q} />
            ))}
        </Grid>
      </Grid>
    );
  }
}

QuestionTimeline.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,
};

export default withStyles(styles)(QuestionTimeline);
