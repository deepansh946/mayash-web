/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';

import moment from 'moment';

import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';

import withStyles from 'material-ui/styles/withStyles';
import Card from 'material-ui/Card';
import CardHeader from 'material-ui/Card/CardHeader';

const styles = {
  root: {
    paddingBottom: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
};

class AnswerCard extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });
  onClick = () => {
    const { courseId, questionId } = this.props;
    this.props.history.push(`/courses/${courseId}/discussion/${questionId}`);
  };

  render() {
    const {
      classes,
      courseId,
      authorId,
      questionId,
      title,
      data,
      timestamp,
    } = this.props;
    const { hover } = this.state;

    const author = { authorId, username: 'hbarve1', name: 'Himank' };

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={hover} className={classes.card} onClick={this.onClick}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${author.username}`}>
                  <Avatar
                    alt={author.name}
                    src={
                      author.avatar ||
                      '/public/photos/mayash-logo-transparent.png'
                    }
                  />
                </Link>
              </Paper>
            }
            title={title}
            subheader={moment(timestamp).fromNow()}
          />
        </Card>
      </div>
    );
  }
}

AnswerCard.propTypes = {
  // match: PropTypes.shape({
  //   params: PropTypes.shape({
  //     courseId: PropTypes.string, // this should be number.
  //     questionId: PropTypes.string, // this should be number.
  //   }).isRequired,
  // }).isRequired,
  history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,

  classes: PropTypes.object.isRequired,

  courseId: PropTypes.number.isRequired,
  authorId: PropTypes.number.isRequired,
  questionId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  data: PropTypes.object,
  timestamp: PropTypes.string,
};

export default withRouter(withStyles(styles)(AnswerCard));
