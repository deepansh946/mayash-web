/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import Loadable from 'react-loadable';

import Loading from '../../../components/Loading';

import apiGetAll from '../../../api/courses/discussions/answers/getAll';
import actionGetAll from '../../../actions/courses/discussion/answers/getAll';

const styles = {
  root: {
    // padding: '1%',
  },
  gridItem: {
    paddingTop: '1%',
    paddingLeft: '1%',
    paddingRight: '1%',
  },
};

const AsyncAnswerCreate = Loadable({
  loader: () => import('./AnswerCreate.js'),
  modules: ['./AnswerCreate'],
  loading: Loading,
});

const AsyncAnswerCard = Loadable({
  loader: () => import('./AnswerCard.js'),
  modules: ['./AnswerCard'],
  loading: Loading,
});

class AnswerTimeline extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const { user, course, questionId } = this.props;
    const { token } = user;
    const { courseId } = course;

    if (course.discussion[questionId].answers) {
      return;
    }

    const { statusCode, payload } = await apiGetAll({
      token,
      courseId,
      questionId,
    });

    this.props.actionGetAll({
      statusCode,
      courseId,
      questionId,
      answers: payload,
    });
  }

  render() {
    const { classes, user, course, questionId } = this.props;
    const { isSignedIn } = user;
    const { answers } = course.discussion[questionId];

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        {isSignedIn && (
          <Grid item xs={12} className={classes.gridItem}>
            <AsyncAnswerCreate {...this.props} />
          </Grid>
        )}
        <Grid item xs={12} className={classes.gridItem}>
          {typeof answers !== 'undefined' &&
            Object.values(answers).map((answer) => (
              <AsyncAnswerCard key={answer.answerId} {...answer} />
            ))}
        </Grid>
      </Grid>
    );
  }
}

AnswerTimeline.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,
  questionId: PropTypes.number.isRequired,

  actionGetAll: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionGetAll,
    },
    dispatch,
  );

export default connect(null, mapDispatchToProps)(
  withStyles(styles)(AnswerTimeline),
);
