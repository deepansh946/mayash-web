/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader } from 'material-ui/Card';

const styles = {
  root: {
    padding: '1%',
  },
};

class Answer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, title, description } = this.props;

    return (
      <div className={classes.root}>
        <Card raised>
          <CardHeader title={title} subheader={description} />
        </Card>
      </div>
    );
  }
}

Answer.propTypes = {
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default withStyles(styles)(Answer);
