/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import Tooltip from 'material-ui/Tooltip';
import Collapse from 'material-ui/transitions/Collapse';

import { convertToRaw } from 'draft-js';

import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';
import DeleteIcon from 'material-ui-icons/Delete';
import ExpandMoreIcon from 'material-ui-icons/ExpandMore';

import Title from '../../components/Title';
import MayashEditor, { createEditorState } from '../../../lib/mayash-editor';

import apiModuleUpdate from '../../api/courses/users/modules/update';
import apiModuleDelete from '../../api/courses/users/modules/delete';

import actionUpdate from '../../actions/courses/users/modules/update';
import actionDelete from '../../actions/courses/users/modules/delete';

const styles = (theme) => ({
  root: {
    paddingBottom: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
});

class Module extends Component {
  constructor(props) {
    super(props);
    const { title, data } = props;
    this.state = {
      hover: false,
      expanded: false,
      edit: false,

      title,
      data: createEditorState(data),

      message: '',
    };

    this.handleExpandClick = this.handleExpandClick.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onClickDelete = this.onClickDelete.bind(this);
  }

  onChange(data) {
    this.setState({ data });
  }

  onChangeTitle = (e) => this.setState({ title: e.target.value });

  onEdit() {
    this.setState({
      edit: !this.state.edit,
      expanded: true,
    });
  }

  async onSave() {
    try {
      const { courseId, moduleId, title: titleProp, elements } = this.props;
      const { userId, token } = elements.user;

      const { edit, title, data } = this.state;

      let body = {};

      if (titleProp !== title) {
        body = { ...body, title };
      }

      body = {
        ...body,
        data: convertToRaw(data.getCurrentContent()),
      };

      const { statusCode, error, message } = await apiModuleUpdate({
        token,
        userId,
        courseId,
        moduleId,
        ...body,
      });

      if (statusCode >= 300) {
        this.setState({ message: error });
        return;
      }

      this.setState({ message });

      this.props.actionUpdate({
        authorId: userId,
        courseId,
        moduleId,
        ...body,
        statusCode,
      });

      this.setState({ edit: !edit });
    } catch (e) {
      console.error(e);
    }
  }

  async onClickDelete() {
    try {
      const { moduleId, user, course } = this.props;
      const { authorId, courseId } = course;
      const { token, id: userId } = user;

      const { statusCode, message, error } = await apiModuleDelete({
        token,
        userId,
        courseId,
        moduleId,
      });

      if (statusCode >= 300) {
        this.setState({ message: error });
        return;
      }

      this.setState({ message });

      this.props.actionDelete({
        statusCode,
        moduleId,
        courseId,
        authorId,
      });
    } catch (error) {
      console.error(error);
    }
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  handleExpandClick() {
    this.setState({ expanded: !this.state.expanded });
  }

  render() {
    const { classes, elements, authorId, index, data: dataProp } = this.props;
    const { id: userId, isSignedIn } = elements.user;

    const { edit, expanded, title, data } = this.state;

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={this.state.hover} className={classes.card}>
          <CardHeader
            title={
              <Title
                readOnly={!edit}
                value={edit ? title : `${index}. ${title}`}
                onChange={this.onChangeTitle}
              />
            }
          />
          {edit || typeof dataProp !== 'undefined' ? (
            <Collapse in={expanded} timeout={'auto'} unmountOnExit>
              <CardContent>
                <MayashEditor
                  editorState={data}
                  onChange={this.onChange}
                  readOnly={!edit}
                  placeholder={'Module Content'}
                />
              </CardContent>
            </Collapse>
          ) : null}
          <CardActions disableActionSpacing>
            <div className={classes.flexGrow} />
            {isSignedIn &&
              authorId === userId && (
                <IconButton
                  aria-label="Edit"
                  onClick={edit ? this.onSave : this.onEdit}
                >
                  {edit ? <SaveIcon /> : <EditIcon />}
                </IconButton>
              )}
            {isSignedIn &&
              authorId === userId && (
                <IconButton onClick={this.onClickDelete} aria-label="Delete">
                  <DeleteIcon />
                </IconButton>
              )}
            <Tooltip title="Expand" placement="bottom">
              <IconButton
                className={classnames(classes.expand, {
                  [classes.expandOpen]: expanded,
                })}
                onClick={this.handleExpandClick}
                aria-expanded={expanded}
                aria-label="Show more"
              >
                <ExpandMoreIcon />
              </IconButton>
            </Tooltip>
          </CardActions>
        </Card>
      </div>
    );
  }
}

Module.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  authorId: PropTypes.number.isRequired,
  courseId: PropTypes.number.isRequired,
  moduleId: PropTypes.number.isRequired,

  // index is module number in course.
  index: PropTypes.number.isRequired,

  title: PropTypes.string.isRequired,
  // description: PropTypes.string, // no using it for now.
  data: PropTypes.object,

  actionUpdate: PropTypes.func.isRequired,
  actionDelete: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionUpdate,
      actionDelete,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Module),
);
