/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';

import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';

import { convertToRaw } from 'draft-js';

import MayashEditor, { createEditorState } from '../../../lib/mayash-editor';

import apiCourseUpdate from '../../api/courses/users/update';
import actionCourseUpdate from '../../actions/courses/update';

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  title: {
    textAlign: 'center',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  editButton: {
    position: 'fixed',
    [theme.breakpoints.up('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('md')]: {
      bottom: '30px',
      right: '30px',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
      bottom: '25px',
      right: '25px',
    },
  },
});

class Syllabus extends Component {
  constructor(props) {
    super(props);
    const { syllabus } = props.course;

    this.state = {
      edit: false,

      syllabus: createEditorState(syllabus),
    };

    this.onChange = this.onChange.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  onChange(syllabus) {
    this.setState({ syllabus });
  }
  onEdit() {
    const { edit } = this.state;
    this.setState({ edit: !edit });
  }
  async onSave() {
    try {
      const { elements, course } = this.props;
      const { id: userId, token } = elements.user;
      const { courseId } = course;

      const { edit } = this.state;

      const syllabus = convertToRaw(this.state.syllabus.getCurrentContent());

      const { statusCode, error } = await apiCourseUpdate({
        userId,
        courseId,
        token,
        syllabus,
      });

      if (statusCode !== 200) {
        // handle error;
        console.error(error);
        return;
      }

      this.props.actionCourseUpdate({ courseId, syllabus });
      this.setState({ edit: !edit });
    } catch (e) {
      console.error(e);
    }
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { classes, elements, course } = this.props;
    const { isSignedIn, id: userId } = elements.user;
    const { authorId } = course;

    const { edit, syllabus } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={12} md={10} lg={8} xl={8}>
          <div
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
          >
            <Card raised={this.state.hover} className={classes.card}>
              <CardHeader title={'Syllabus'} className={classes.title} />
              <CardContent>
                <MayashEditor
                  editorState={syllabus}
                  onChange={this.onChange}
                  readOnly={!edit}
                  placeholder={'Syllabus Content'}
                />
              </CardContent>
            </Card>
          </div>
          {isSignedIn &&
            authorId === userId && (
              <Button
                fab
                color="accent"
                aria-label="Edit"
                className={classes.editButton}
                onClick={edit ? this.onSave : this.onEdit}
              >
                {edit ? <SaveIcon /> : <EditIcon />}
              </Button>
            )}
        </Grid>
      </Grid>
    );
  }
}

Syllabus.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,
  course: PropTypes.object.isRequired,

  actionCourseUpdate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCourseUpdate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Syllabus),
);
