/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, {
  CardHeader,
  CardMedia,
  CardActions,
  CardContent,
} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';

import Typography from 'material-ui/Typography';
import Avatar from 'material-ui/Avatar';
import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';
import FavoriteIcon from 'material-ui-icons/Favorite';
import MoreVertIcon from 'material-ui-icons/MoreVert';
import ShareIcon from 'material-ui-icons/Share';
import Tooltip from 'material-ui/Tooltip';

import Title from '../../components/Title';
import Description from '../../components/Description';
// import RatingStar from '../../components/RatingStar';
import CoverImage from './CoverImage';

import apiCourseUpdate from '../../api/courses/users/update';

import actionElementGetById from '../../actions/elements/getById';
import actionCourseUpdate from '../../actions/courses/update';

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  title: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  subheader: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  avatar: {
    borderRadius: '50%',
  },
  media: {
    height: 194,
    borderRadius: '8px 8px 0px 0px',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  margin: {
    border: '0.5px solid rgba(209,209,209,1)',
    marginBottom: '-10px',
  },
  editButton: {
    position: 'fixed',
    [theme.breakpoints.up('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('md')]: {
      bottom: '30px',
      right: '30px',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
      bottom: '25px',
      right: '25px',
    },
  },
});

class Introduction extends Component {
  constructor(props) {
    super(props);
    const { id: userId } = this.props.elements.user;
    const { authorId, title, description } = this.props.course;

    this.state = {
      edit: false,
      // this value will be true only if user is the author of course.
      isAuthor: userId === authorId,
      title,
      description,
    };

    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentDidMount() {
    const { elements, course } = this.props;
    const { token, id: userId } = elements.user;
    const { authorId } = course;

    let author;
    if (authorId === userId) {
      author = elements.user;
    } else {
      author = elements[authorId];
    }

    if (!author || (!author.isFetching && !author.isFetched)) {
      this.props.actionElementGetById({ token, id: authorId });
    }
  }

  onEdit() {
    const { edit } = this.state;
    this.setState({ edit: !edit });
  }

  async onSave() {
    const { user } = this.props.elements;
    const { id: userId, token } = user;

    const { courseId, authorId } = this.props.course;
    const { edit, title, description } = this.state;

    const { statusCode, error } = await apiCourseUpdate({
      token,
      userId,
      courseId,
      title,
      description,
    });

    if (statusCode !== 200) {
      // handle error
      console.error(error);
      return;
    }

    this.props.actionCourseUpdate({
      courseId,
      authorId,
      title,
      description,
    });

    this.setState({ edit: !edit });
  }

  onChangeTitle = (e) => this.setState({ title: e.target.value });
  onChangeDescription = (e) => this.setState({ description: e.target.value });

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { classes, course, elements } = this.props;
    const { user } = elements;
    const { isSignedIn, id: userId, token } = user;
    const { authorId, cover } = course;
    let author = { authorId };

    if (userId === authorId) {
      author = { ...author, ...user };
    } else {
      author = {
        ...author,
        ...elements[authorId],
      };
    }

    const { hover, edit, title, description } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={12} md={10} lg={8} xl={8}>
          <Card
            raised={hover}
            className={classes.card}
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
          >
            {/* don't change this component for now */}
            {userId === authorId ? (
              <CardMedia
                className={classes.media}
                component={(thisProp) => (
                  <CoverImage
                    {...thisProp}
                    {...course}
                    token={token}
                    actionCourseUpdate={this.props.actionCourseUpdate}
                  />
                )}
                image={
                  cover ||
                  'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
                }
              />
            ) : (
              <CardMedia
                className={classes.media}
                image={
                  cover ||
                  'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
                }
              />
            )}
            <CardHeader
              avatar={
                <Paper className={classes.avatar} elevation={1}>
                  <Link to={`/@${author.username}`}>
                    <Avatar
                      alt={author.name}
                      src={
                        author.avatar ||
                        '/public/photos/mayash-logo-transparent.png'
                      }
                    />
                  </Link>
                </Paper>
              }
              action={
                <div>
                  <Tooltip title="Coming soon" placement="bottom">
                    <IconButton disabled>
                      <MoreVertIcon />
                    </IconButton>
                  </Tooltip>
                </div>
              }
              title={
                <Link to={`/@${author.username}`} className={classes.title}>
                  {author.name}
                </Link>
              }
              subheader={
                <Link to={`/@${author.username}`} className={classes.subheader}>
                  {`@${author.username}`}
                </Link>
              }
            />
            <CardContent>
              <Typography type="title" component="div">
                <Title
                  readOnly={!edit}
                  value={title}
                  onChange={this.onChangeTitle}
                />
              </Typography>
              <Typography type="body1" component="div" gutterBottom>
                {typeof description === 'string' || edit ? (
                  <Description
                    readOnly={!edit}
                    value={description}
                    onChange={this.onChangeDescription}
                  />
                ) : null}
              </Typography>
            </CardContent>
            <div className={classes.margin} />
            <CardActions disableActionSpacing>
              <div />
              <Tooltip title="Add to favorites" placement="bottom">
                <IconButton aria-label="Add to favorites" disabled>
                  <FavoriteIcon />
                </IconButton>
              </Tooltip>
              <Tooltip title="Share" placement="bottom">
                <IconButton aria-label="Share" disabled>
                  <ShareIcon />
                </IconButton>
              </Tooltip>
            </CardActions>
          </Card>
        </Grid>
        {isSignedIn &&
          authorId === userId && (
            <Button
              fab
              color="accent"
              aria-label="Edit"
              className={classes.editButton}
              onClick={edit ? this.onSave : this.onEdit}
            >
              {edit ? <SaveIcon /> : <EditIcon />}
            </Button>
          )}
      </Grid>
    );
  }
}

Introduction.propTypes = {
  /** classes object comes from withStyle component wrapper. */
  classes: PropTypes.object.isRequired,

  /** elements comes from Redux store */
  elements: PropTypes.object.isRequired,

  /** course object comes from parent component. */
  course: PropTypes.object.isRequired,

  /** these actions comes from mapDispatchToProps()  */
  actionElementGetById: PropTypes.func.isRequired,
  actionCourseUpdate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionElementGetById,
      actionCourseUpdate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Introduction),
);
