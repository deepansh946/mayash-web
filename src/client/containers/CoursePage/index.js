/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import withStyles from 'material-ui/styles/withStyles';
import Tabs, { Tab } from 'material-ui/Tabs';
import AppBar from 'material-ui/AppBar';

import Loading from '../../components/Loading';

import apiCourseModulesGet from '../../api/courses/modules/getAll';

import actionElementGet from '../../actions/elements/getById';
import actionCourseModulesGet from '../../actions/courses/modules/getAll';

const { NODE_ENV } = process.env;
const isDev = NODE_ENV === 'development';

const CommingSoon = () => <div>Comming Soon.</div>;

const AsyncErrorPage = Loadable({
  loader: () => import('../../components/ErrorPage'),
  modules: ['../../components/ErrorPage'],
  loading: Loading,
});

const AsyncIntroduction = Loadable({
  loader: () => import('./Introduction'),
  modules: ['./Introduction'],
  loading: Loading,
});

const AsyncSyllabus = Loadable({
  loader: () => import('./Syllabus'),
  modules: ['./Syllabus'],
  loading: Loading,
});

const AsyncModules = Loadable({
  loader: () => import('./Modules'),
  modules: ['./Modules'],
  loading: Loading,
});

const AsyncDiscussion = Loadable({
  loader: () => import('./Discussion'),
  modules: ['./Discussion'],
  loading: Loading,
});

const AsyncTestYourself = Loadable({
  loader: () => import('./TestYourself'),
  modules: ['./TestYourself'],
  loading: Loading,
});

const styles = {
  root: {
    flexGrow: 1,
    width: '100%',
  },
  textField: {
    width: '100%',
  },
};

const routeStates = (route, courseId) => {
  const routes = {
    [`/courses/${courseId}`]: 0,
    [`/courses/${courseId}/syllabus`]: 1,
    [`/courses/${courseId}/modules`]: 2,
    [`/courses/${courseId}/discussion`]: 3,
    [`/courses/${courseId}/test-yourself`]: 4,
    [`/courses/${courseId}/feedback`]: 5,
  };

  if (route.includes(`/courses/${courseId}/discussion`)) {
    return 3;
  }

  return typeof routes[route] !== 'undefined' ? routes[route] : 6;
};

class CoursePage extends Component {
  constructor(props) {
    super(props);
    const { courseId } = props.match.params;

    this.state = {
      value: routeStates(props.location.pathname, courseId),
    };

    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    try {
      const { elements, courses, match } = this.props;
      const { token } = elements.user;
      const { courseId } = match.params;
      const course = courses[courseId];

      // if course is not found, don't fetch it's modules
      if (!course || course.statusCode >= 300) {
        return;
      }

      // if course is already fetched then don't fetch it again.
      if (course.courseModules) {
        return;
      }

      const { statusCode, message, payload, error } = await apiCourseModulesGet(
        {
          token,
          courseId,
        },
      );

      if (statusCode >= 300) {
        console.error(error);
        return;
      }

      this.props.actionCourseModulesGet({
        courseId,
        courseModules: payload,
      });
    } catch (error) {
      console.error(error);
    }
  }

  handleChange(event, value) {
    const { match, history } = this.props;
    const { courseId } = match.params;

    this.setState({ value });

    switch (value) {
      case 0:
        history.push(`/courses/${courseId}`);
        break;
      case 1:
        history.push(`/courses/${courseId}/syllabus`);
        break;
      case 2:
        history.push(`/courses/${courseId}/modules`);
        break;
      case 3:
        history.push(`/courses/${courseId}/discussion`);
        break;
      case 4:
        history.push(`/courses/${courseId}/test-yourself`);
        break;
      case 5:
        history.push(`/courses/${courseId}/feedback`);
        break;
      default:
        history.push(`/courses/${courseId}`);
        break;
    }
  }

  render() {
    const { classes, courses, match } = this.props;
    const { courseId } = match.params;

    const course = courses[courseId];

    if (!course || course.statusCode >= 300) {
      return <AsyncErrorPage {...course} />;
    }

    const { user } = this.props.elements;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Introduction" />
            <Tab label="Syllabus" />
            <Tab label="Modules" />
            <Tab label="Discussion" />
            <Tab label="Test Yourself" disabled={!isDev} />
            <Tab label="Feedback" disabled />
            {/* Not Found tab is hidden and it will be usefull to handle 
                unknown routes. */}
            <Tab label="Not Found" style={{ display: 'none' }} />
          </Tabs>
        </AppBar>
        <Switch>
          <Route
            exact
            path="/courses/:courseId"
            component={(restProps) => (
              <AsyncIntroduction course={course} {...restProps} />
            )}
          />
          <Route
            exact
            path="/courses/:courseId/syllabus"
            component={(restProps) => (
              <AsyncSyllabus course={course} {...restProps} />
            )}
          />
          <Route
            exact
            path="/courses/:courseId/modules"
            component={(restProps) => (
              <AsyncModules user={user} course={course} {...restProps} />
            )}
          />
          <Route
            // exact
            path="/courses/:courseId/discussion"
            component={(restProps) => (
              <AsyncDiscussion user={user} course={course} {...restProps} />
            )}
          />
          <Route
            exact
            path="/courses/:courseId/test-yourself"
            component={(restProps) => (
              <AsyncTestYourself user={user} course={course} {...restProps} />
            )}
          />
          <Route
            exact
            path="/courses/:courseId/feedback"
            component={() => <CommingSoon />}
          />
          <Route component={AsyncErrorPage} />
        </Switch>
      </div>
    );
  }
}

CoursePage.propTypes = {
  classes: PropTypes.object.isRequired,

  match: PropTypes.shape({
    params: PropTypes.shape({
      courseId: PropTypes.string.isRequired, // This should be number.
    }).isRequired,
  }).isRequired,
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,
  courses: PropTypes.object.isRequired,

  actionElementGet: PropTypes.func.isRequired,
  actionCourseModulesGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements, courses }) => ({ elements, courses });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionElementGet,
      actionCourseModulesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CoursePage),
);
