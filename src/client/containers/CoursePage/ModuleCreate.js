/**
 * ModuleCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create
 * post for their profile only.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';

import Input from '../../components/Input';

import actionModuleCreate from '../../actions/courses/users/modules/create';
import apiModuleCreate from '../../api/courses/users/modules/create';

// Define all styles here for ModuleCreate Component.
const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

class ModuleCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Module Title...',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const title = e.target.value;
    const titleLength = title.length;
    const valid = titleLength > 0 && titleLength < 148;

    this.setState({
      title,
      titleLength,
      valid,
      error: !valid ? 'Title length should be in between 1 to 148.' : '',
    });
  }

  async onSubmit(e) {
    try {
      const { elements, course } = this.props;
      const { id: userId, token } = elements.user;
      const { courseId } = course;

      const { title } = this.state;

      this.setState({ message: 'Creating course module...' });

      const { statusCode, error, payload } = await apiModuleCreate({
        token,
        userId,
        courseId,
        title,
      });

      if (statusCode >= 300) {
        // handle error
        this.setState({ statusCode, error });
        return;
      }

      this.setState({ message: 'Successfully Created.' });

      setTimeout(() => {
        this.setState({
          valid: false,
          statusCode: 0,
          error: '',
          message: '',

          title: '',
          titleLength: 0,
        });
      }, 1500);

      this.props.actionModuleCreate({
        statusCode,
        authorId: userId,
        ...payload,
        title,
      });
    } catch (error) {
      console.error(error);
    }
  }

  onFocus = () => this.setState({ active: true });

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { active, placeholder, title, message } = this.state;
    const { classes, elements } = this.props;
    const { name, avatar, username } = elements.user;

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card className={classes.card} raised={this.state.hover}>
          <CardHeader
            avatar={
              <Paper elevation={1} className={classes.avatar}>
                <Link to={`/@${username}`}>
                  <Avatar
                    alt={name}
                    src={avatar || '/public/photos/mayash-logo-transparent.png'}
                    className={classes.avatar}
                  />
                </Link>
              </Paper>
            }
            title={
              <Input
                value={title}
                onChange={this.onChange}
                placeholder={placeholder}
              />
            }
            className={classes.cardHeader}
            onFocus={this.onFocus}
          />
          {active && message.length ? (
            <CardContent>
              <Typography>{message}</Typography>
            </CardContent>
          ) : null}
          {active ? (
            <CardActions>
              <div className={classes.flexGrow} />
              <Button raised color="accent" onClick={this.onSubmit}>
                Create
              </Button>
            </CardActions>
          ) : null}
        </Card>
      </div>
    );
  }
}

ModuleCreate.propTypes = {
  /**
   * Classes object is comming from withStyles.
   */
  classes: PropTypes.object.isRequired,

  /**
   * elements object is comming from Redux store.
   */
  elements: PropTypes.object.isRequired,

  /**
   * Course Object contains course info and is coming from parent component.
   */
  course: PropTypes.object.isRequired,

  /**
   * Action to Create New Course Module.
   */
  actionModuleCreate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionModuleCreate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(ModuleCreate),
);
