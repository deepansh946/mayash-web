import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';

import withStyles from 'material-ui/styles/withStyles';

import Loading from '../../../components/Loading';
import QuestionCreate from './QuestionCreate';

const AsyncQuestion = Loadable({
  loader: () => import('./Question'),
  modules: ['./Question'],
  loading: Loading,
});

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
});

class TestYourself extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    course: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      questions: [
        {
          qId: 1,
          title: 'Question 1',
          description: 'Test Description',
          options: [
            {
              title: 'Option A',
              answer: true,
              explaination: 'Test Explaination',
            },
            {
              title: 'Option B',
              answer: true,
              explaination: 'Test Explaination',
            },
            { title: 'Option C', explaination: 'Test Explaination' },
            { title: 'Option D', explaination: 'Test Explaination' },
          ],
        },
        {
          qId: 2,
          title: 'Question 2',
          description: '2 Test Description',
          options: [
            {
              title: 'Option A',
              answer: true,
              explaination: 'Test Explaination',
            },
            {
              title: 'Option B',
              answer: true,
              explaination: 'Test Explaination',
            },
            { title: 'Option C', explaination: 'Test Explaination' },
            { title: 'Option D', explaination: 'Test Explaination' },
          ],
        },
      ],
    };
  }

  createQuestion = ({ title, description, options }) => {
    const { questions } = this.state;

    this.setState({
      questions: [
        ...questions,
        {
          qId: 6, // questions.length,
          title,
          description,
          options,
        },
      ],
    });
  };

  render() {
    const { questions } = this.state;
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <QuestionCreate
          questions={questions}
          createQuestion={this.createQuestion}
        />

        {questions.map((q) => <AsyncQuestion key={q.qId} {...q} />)}
      </div>
    );
  }
}

export default withStyles(styles)(TestYourself);
