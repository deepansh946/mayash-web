/**
 * This is the form Component
 * Generaly it will be used creating posts and modules
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Card, { CardContent, CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';

import Input from '../../components/Input';

const styles = {
  root: {
    flex: '1 0 100%',
  },
  card: {
    borderRadius: '8px',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
      active: false,
      focus: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onChange(e) {
    this.setState({ value: e.target.value });
  }

  onClick() {
    this.props.onSubmit(this.props.keyName, this.state.value);
  }

  render() {
    const { value, focus } = this.state;
    const { classes, title, placeholder, disabled } = this.props;

    return (
      <div
        className={classes.root}
        onMouseEnter={() => this.setState({ hover: true })}
        onMouseLeave={() => this.setState({ hover: false })}
      >
        <Card raised={this.state.hover} className={classes.card}>
          {/* onBlur is opposit to onFocus, Generaly it is used when the 
          user leaves a form field */}
          <CardContent
            onFocus={() => this.setState({ focus: true })}
            onBlur={() =>
              this.setState({
                focus: !(this.props.value === value),
              })
            }
          >
            {`${title}: `}
            <Input
              value={value}
              onChange={this.onChange}
              placeholder={placeholder}
              disabled={disabled}
            />
          </CardContent>
          {focus && !(disabled === true) ? (
            <CardActions>
              <div className={classes.flexGrow} />
              <Button raised color="accent" onClick={this.onClick}>
                Submit
              </Button>
            </CardActions>
          ) : null}
        </Card>
      </div>
    );
  }
}

/**
 * This will validate all the props comming to the component
 */
Form.propTypes = {
  classes: PropTypes.object.isRequired,
  keyName: PropTypes.string,
  title: PropTypes.string.isRequired,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onSubmit: PropTypes.func,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

// Default props for the component
Form.defaultProp = {
  disabled: false,
  placeholder: 'Input',
};

export default withStyles(styles)(Form);
