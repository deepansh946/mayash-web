/**
 * This is Home page component
 * Home page will be different for SignedIn and unSignedIn user
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';
import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import Loading from '../../components/Loading';

import actionUserUpdate from '../../actions/users/update';
import actionUserPostsGet from '../../actions/posts/users/getAll';

const AsyncLandingPage = Loadable({
  loader: () => import('./LandingPage'),
  modules: ['./LandingPage'],
  loading: Loading,
});

const AsyncTimeline = Loadable({
  loader: () => import('../../containers/Timeline'),
  modules: ['../../containers/Timeline'],
  loading: Loading,
});

const AsyncTabMyResume = Loadable({
  loader: () => import('./TabMyResume'),
  modules: ['./TabMyResume'],
  loading: Loading,
});

const AsyncCourseTimeline = Loadable({
  loader: () => import('../../containers/CourseTimeline'),
  modules: ['../../containers/CourseTimeline'],
  loading: Loading,
});

const AsyncPostTimeline = Loadable({
  loader: () => import('../../containers/PostTimeline'),
  modules: ['../../containers/PostTimeline'],
  loading: Loading,
});

const AsyncMyCircles = Loadable({
  loader: () => import('../../containers/CircleTimeline'),
  modules: ['../../containers/CircleTimeline'],
  loading: Loading,
});

const AsyncMyNotes = Loadable({
  loader: () => import('../../containers/NoteTimeline'),
  modules: ['../../containers/NoteTimeline'],
  loading: Loading,
});

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
  },
});

// const ComingSoon = () => (<div>Coming Soon.</div>);

/**
 *
 */
class Home extends Component {
  static propTypes = {
    /**
     * classes object contains all the styles for Home component.
     */
    classes: PropTypes.object.isRequired,

    /**
     * elements object contains all the data from redux store of elements.
     */
    elements: PropTypes.object.isRequired,

    /**
     * all actions are here.
     */
    actionUserUpdate: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: 2,
    };

    this.onChange = this.onChange.bind(this);
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.state !== nextState && this.props !== nextProps;
  // }

  onChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes, elements } = this.props;
    const { user } = elements;
    const { guru = false } = user;

    const { value } = this.state;

    // when user is signed in then render it
    return (
      <div>
        <AppBar position="static" color="default">
          {guru ? (
            <Tabs
              value={value}
              onChange={this.onChange}
              indicatorColor="primary"
              textColor="primary"
              scrollable
              scrollButtons="auto"
            >
              <Tab label="Timeline" disabled />
              <Tab label="My Resume" />
              <Tab label="My Posts" />
              <Tab label="My Courses" />
              <Tab label="My Circles" disabled />
              <Tab label="My Notes" disabled />
            </Tabs>
          ) : (
            <Tabs
              value={value}
              onChange={this.onChange}
              indicatorColor="primary"
              textColor="primary"
              scrollable
              scrollButtons="auto"
            >
              <Tab label="Timeline" disabled />
              <Tab label="My Resume" />
              <Tab label="My Posts" />
              <Tab label="My Circles" disabled />
              <Tab label="My Notes" disabled />
            </Tabs>
          )}
        </AppBar>
        {/* {value === 0 && <AsyncTimeline />} */}

        {value === 1 && (
          <AsyncTabMyResume
            user={user}
            actionUserUpdate={this.props.actionUserUpdate}
          />
        )}

        {value === 2 && <AsyncPostTimeline element={user} />}

        {guru && value === 3 && <AsyncCourseTimeline element={user} />}

        {/* {((guru && value === 4) || (!guru && value === 3)) &&
          <AsyncMyCircles
            user={user}
          />} */}

        {/* {((guru && value === 5) || (!guru && value === 4)) && (
          <AsyncMyNotes element={user} />
        )} */}
      </div>
    );
  }
}

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionUserUpdate,
      actionUserPostsGet,
      // actionUserCoursesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Home),
);
