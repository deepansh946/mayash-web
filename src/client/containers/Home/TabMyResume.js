/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardContent } from 'material-ui/Card';

import Button from 'material-ui/Button';
import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';

import { convertToRaw } from 'draft-js';

import MayashEditor, { createEditorState } from '../../../lib/mayash-editor';

import apiUserUpdate from '../../api/users/update';

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  card: {
    borderRadius: '8px',
  },
  editButton: {
    position: 'fixed',
    [theme.breakpoints.up('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('md')]: {
      bottom: '30px',
      right: '30px',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
      bottom: '25px',
      right: '25px',
    },
  },
});

class TabMyResume extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resume: createEditorState(props.user.resume),
    };

    this.onChange = this.onChange.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  // This on change function is for MayashEditor
  onChange(resume) {
    this.setState({ resume });
  }

  onEdit() {
    this.setState({ edit: !this.state.edit });
  }

  async onSave() {
    const { resume } = this.state;
    const { id, token } = this.props.user;

    const { statusCode, error } = await apiUserUpdate({
      id,
      token,
      resume: convertToRaw(resume.getCurrentContent()),
    });

    if (statusCode !== 200) {
      // error handle
      console.error(error);
      return;
    }

    this.props.actionUserUpdate({
      id,
      resume: convertToRaw(resume.getCurrentContent()),
    });

    this.setState({ edit: false });
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { classes } = this.props;
    const { hover, edit, resume } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={10} md={8} lg={6} xl={6}>
          <div
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
          >
            <Card raised={hover} className={classes.card}>
              <CardContent>
                <MayashEditor
                  editorState={resume}
                  onChange={this.onChange}
                  readOnly={!edit}
                />
              </CardContent>
            </Card>
            <Button
              fab
              color="accent"
              aria-label="Edit"
              className={classes.editButton}
              onClick={edit ? this.onSave : this.onEdit}
            >
              {edit ? <SaveIcon /> : <EditIcon />}
            </Button>
          </div>
        </Grid>
      </Grid>
    );
  }
}

TabMyResume.propTypes = {
  /**
   * TabMyResume style object
   */
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,

  actionUserUpdate: PropTypes.func.isRequired,
};

export default withStyles(styles)(TabMyResume);
