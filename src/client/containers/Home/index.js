/**
 * This is Home page component
 * Home page will be different for SignedIn and unSignedIn user
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import Loadable from 'react-loadable';

import withStyles from 'material-ui/styles/withStyles';

import Loading from '../../components/Loading';

const AsyncLandingPage = Loadable({
  loader: () => import('./LandingPage'),
  modules: ['./LandingPage'],
  loading: Loading,
});

const AsyncHome = Loadable({
  loader: () => import('./Home'),
  modules: ['./Home'],
  loading: Loading,
});

const styles = {
  root: {
    flexGrow: 1,
    width: '100%',
  },
};

/**
 *
 */
class Index extends Component {
  static propTypes = {
    /**
     * Classes object contains the styling.
     */
    classes: PropTypes.object.isRequired,

    /**
     * This user object will come from redux store's elements part.
     */
    user: PropTypes.shape({
      isSignedIn: PropTypes.bool.isRequired,
    }),
  };

  shouldComponentUpdate(nextProps, nextState) {
    return this.state !== nextState && this.props !== nextProps;
  }

  render() {
    const { classes, user } = this.props;
    const { isSignedIn } = user;

    return (
      <div className={classes.root}>
        {isSignedIn ? <AsyncHome /> : <AsyncLandingPage />}
      </div>
    );
  }
}

const mapStateToProps = ({ elements }) => ({ user: elements.user });

export default connect(mapStateToProps)(withStyles(styles)(Index));
