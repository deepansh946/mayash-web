/** @format */

import React from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';

const styles = (theme) => ({
  hero: {
    // Makes the hero full height until we get some more content.
    minHeight: '100vh',
    flex: '0 0 auto',
    display: 'flex',
    backgroundColor: 'rgba(0,0,0,.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    padding: '10% 10px',
    textAlign: 'center',
    [theme.breakpoints.up('sm')]: {
      padding: '10px 10px',
    },
  },
  title: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
  },
  subheading: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
  },
  button: {
    marginTop: 20,
    backgroundColor: theme.palette.secondary[300],
  },
  logo: {
    marginBottom: '10px',
    width: '100%',
    height: '100%',
    animation: 'spin infinite 20s linear',
  },
  '@keyframes spin': {
    from: {
      transform: 'rotate(0deg)',
    },
    to: {
      transform: 'rotate(360deg)',
    },
  },
});

const LandingPage = ({ classes }) => (
  <div className={classes.hero}>
    <div className={classes.content}>
      <img
        src={'/public/photos/mayash-logo-transparent.png'}
        alt={'Mayash Logo'}
        className={classes.logo}
      />
      <Typography type="display2" component="h1" className={classes.title}>
        Mayash
      </Typography>
      <Typography
        type="subheading"
        component="h2"
        className={classes.subheading}
      >
        Transforming Education...
      </Typography>
      <Button
        component={Link}
        className={classes.button}
        raised
        to="/introduction"
      >
        Introduction
      </Button>
    </div>
  </div>
);

LandingPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LandingPage);
