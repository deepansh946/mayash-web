/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router-dom';
import Loadable from 'react-loadable';
import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Loading from '../../components/Loading';

// import actionCirclePostsGet from '../../actions/posts/circles/getAll';
// import actionCircleCoursesGet from '../../actions/courses/circles/getAll';

const AsyncElementInfo = Loadable({
  loader: () => import('../ElementInfo'),
  modules: ['../ElementInfo'],
  loading: Loading,
});

const AsyncFollow = Loadable({
  loader: () => import('../Follow'),
  modules: ['../Follow'],
  loading: Loading,
});

const AsyncPostTimeline = Loadable({
  loader: () => import('../PostTimeline'),
  modules: ['../PostTimeline'],
  loading: Loading,
});

// const AsyncTabCourses = Loadable({
//   loader: () => import('./Tabs/TabCourses'),
//   modules: ['./Tabs/TabCourses'],
//   loading: Loading,
// });

const AsyncTabMembers = Loadable({
  loader: () => import('./Tabs/TabMembers'),
  modules: ['./Tabs/TabMembers'],
  loading: Loading,
});

const AsyncErrorPage = Loadable({
  loader: () => import('../../components/ErrorPage'),
  modules: ['../../components/ErrorPage'],
  loading: Loading,
});

const styles = {
  root: {},
};

// this function will return index of route url in Dashboard component page.
const routeStates = (route, username) => {
  const routes = {
    [`/@${username}`]: 0,
    [`/@${username}/posts`]: 1,
    [`/@${username}/courses`]: 2,
    [`/@${username}/members`]: 3,
  };

  // if routes[index] value is undefined, it will return 0;
  return routes[route] || 0;
};

class CircleEdu extends Component {
  constructor(props) {
    super(props);
    const { pathname } = props.location;
    const { username } = props.match.params;

    this.state = {
      value: routeStates(pathname, username),
    };

    this.onChange = this.onChange.bind(this);
  }

  componentDidMount() {
    const { elements, element } = this.props;
    const { token } = elements.user;
    const { id } = element;

    // this.props.actionCirclePostsGet({ id, token });
    // this.props.actionCircleCoursesGet({ id, token });
  }

  onChange(event, value) {
    this.setState({ value });
    const { history, match } = this.props;
    const { username } = match.params;
    switch (value) {
      case 0:
        history.push(`/@${username}`);
        break;
      case 1:
        history.push(`/@${username}/posts`);
        break;
      case 2:
        history.push(`/@${username}/courses`);
        break;
      case 3:
        history.push(`/@${username}/members`);
        break;
      default:
        history.push(`/@${username}`);
        break;
    }
  }

  render() {
    const { classes, elements, element, posts /* , courses */ } = this.props;
    const { user } = elements;
    const { id: userId, isSignedIn } = user;
    const { id } = element;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AsyncElementInfo user={user} element={element} />

        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.onChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Posts" />
            <Tab label="Courses" disabled />
            <Tab label="Members" disabled />

            {isSignedIn && userId !== id ? (
              <AsyncFollow element={element} />
            ) : null}
          </Tabs>
        </AppBar>
        <Switch>
          <Route
            exact
            path="/@:username"
            component={(props) => (
              <AsyncPostTimeline
                {...props}
                user={user}
                element={element}
                posts={posts}
              />
            )}
          />
          <Route
            exact
            path="/@:username/posts"
            component={(props) => (
              <AsyncPostTimeline
                {...props}
                user={user}
                element={element}
                posts={posts}
              />
            )}
          />
          <Route
            exact
            path="/@:username/courses"
            component={() => <div>Comming Soon</div>}
          />
          <Route
            exact
            path="/@:username/members"
            component={(props) => (
              <AsyncTabMembers {...props} user={user} circle={element} />
            )}
          />
          <Route component={AsyncErrorPage} />
        </Switch>
      </div>
    );
  }
}

CircleEdu.propTypes = {
  classes: PropTypes.object.isRequired,

  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      username: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,

  element: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  posts: PropTypes.object.isRequired,
  // courses: PropTypes.object.isRequired,

  // actionCirclePostsGet: PropTypes.func.isRequired,
  // actionCircleCoursesGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ posts, courses }) => ({ posts, courses });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // actionCirclePostsGet,
      // actionCircleCoursesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CircleEdu),
);
