/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';
import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Loading from '../../components/Loading';

import actionCirclePostsGet from '../../actions/posts/circles/getAll';

const AsyncElementInfo = Loadable({
  loader: () => import('../ElementInfo'),
  modules: ['../ElementInfo'],
  loading: Loading,
});

const AsyncTabPosts = Loadable({
  loader: () => import('./Tabs/TabPosts'),
  modules: ['./Tabs/TabPosts'],
  loading: Loading,
});

// const AsyncErrorPage = Loadable({
//   loader: () => import('../../components/ErrorPage'),
//   modules: ['../../components/ErrorPage'],
//   loading: Loading,
// });

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

class CircleLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const { elements, element } = this.props;
    const { token } = elements[0];
    const { id } = element;

    this.props.actionCirclePostsGet({ id, token });
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes, elements, element } = this.props;

    const { user } = elements;
    // const posts = this.props.posts.filter(a => a.authorId === element.id);

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AsyncElementInfo user={user} element={element} />

        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Posts" />
          </Tabs>
        </AppBar>
        {value === 0 && <AsyncTabPosts />}
      </div>
    );
  }
}

CircleLocation.propTypes = {
  classes: PropTypes.object.isRequired,

  element: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  posts: PropTypes.object.isRequired,

  actionCirclePostsGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ posts }) => ({ posts });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCirclePostsGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CircleLocation),
);
