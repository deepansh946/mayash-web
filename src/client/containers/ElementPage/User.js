/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Loadable from 'react-loadable';
import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import Loading from '../../components/Loading';

import apiUserCoursesGet from '../../api/courses/users/getAll';
import apiUserPostsGet from '../../api/posts/users/getAll';

import actionUserUpdate from '../../actions/users/update';
import actionUserPostsGet from '../../actions/posts/getAll';
import actionUserCoursesGet from '../../actions/courses/getAll';

import ElementInfo from '../ElementInfo/index';

const AsyncFollow = Loadable({
  loader: () => import('../Follow'),
  modules: ['../Follow'],
  loading: Loading,
});

const AsyncTabResume = Loadable({
  loader: () => import('./Tabs/TabResume'),
  modules: ['./Tabs/TabResume'],
  loading: Loading,
});

const AsyncTabPosts = Loadable({
  loader: () => import('./Tabs/TabPosts'),
  modules: ['./Tabs/TabPosts'],
  loading: Loading,
});

const AsyncTabCourses = Loadable({
  loader: () => import('./Tabs/TabCourses'),
  modules: ['./Tabs/TabCourses'],
  loading: Loading,
});

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
  },
  media: {
    height: 250,
  },
  follow: {
    margin: 'auto',
  },
  button: {
    borderRadius: '15px',
    fontWeight: 'bold',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
});

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      follow: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    try {
      const { element } = this.props;
      const { id, token, courses, posts } = element;

      if (typeof posts === 'undefined' || Object.keys(posts).length === 0) {
        const { statusCode, error, payload, next } = await apiUserPostsGet({
          token,
          userId: id,
        });

        if (statusCode >= 300) {
          // handle error
          console.error(error);
          return;
        }

        this.props.actionUserPostsGet({
          id,
          statusCode,
          posts: payload,
        });
      }

      if (typeof courses === 'undefined') {
        const { statusCode, error, payload, next } = await apiUserCoursesGet({
          token,
          userId: id,
        });

        if (statusCode >= 300) {
          // handle error
          console.error(error);
          return;
        }

        this.props.actionUserCoursesGet({
          id,
          statusCode,
          courses: payload,
        });
      }
    } catch (error) {
      console.error(error);
    }
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes, elements, element, posts, courses } = this.props;
    const { user } = elements;
    const { isSignedIn, id: userId } = user;

    const { guru = false, id } = element;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <ElementInfo
          user={user}
          element={element}
          actionUserUpdate={this.props.actionUserUpdate}
        />

        <AppBar position="static" color="default">
          {guru ? (
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              scrollable
              scrollButtons="auto"
            >
              <Tab label="Resume" />
              <Tab label="Posts" />
              <Tab label="Courses" />

              {isSignedIn && userId !== id ? (
                <AsyncFollow element={element} />
              ) : null}
            </Tabs>
          ) : (
            <Tabs
              value={value}
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              scrollable
              scrollButtons="auto"
            >
              <Tab label="Resume" />
              <Tab label="Posts" />

              {isSignedIn && userId !== id ? (
                <AsyncFollow element={element} />
              ) : null}
            </Tabs>
          )}
        </AppBar>
        {value === 0 && (
          <AsyncTabResume
            user={user}
            element={element}
            actionUserUpdate={this.props.actionUserUpdate}
          />
        )}
        {value === 1 && (
          <AsyncTabPosts user={user} element={element} posts={posts} />
        )}
        {guru &&
          value === 2 && (
            <AsyncTabCourses user={user} element={element} courses={courses} />
          )}
      </div>
    );
  }
}

User.propTypes = {
  classes: PropTypes.object.isRequired,

  element: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  posts: PropTypes.object.isRequired,
  courses: PropTypes.object.isRequired,
  actionUserUpdate: PropTypes.func,
  actionUserPostsGet: PropTypes.func.isRequired,
  actionUserCoursesGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ posts, courses }) => ({ posts, courses });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionUserUpdate,
      actionUserPostsGet,
      actionUserCoursesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(User),
);
