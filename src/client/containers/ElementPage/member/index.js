/**
 * This component is created to edit content of the article
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Link from 'react-router-dom/Link';

import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';
import DeleteIcon from 'material-ui-icons/Delete';

import styles from './styles';

class Member extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    elements: PropTypes.object.isRequired,
    member: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, elements } = this.props;

    let { member } = this.props;
    member = { ...member, ...elements[member.memberId] };

    const { name, username, avatar, doj, role } = member;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={11} md={12} lg={12} xl={12}>
          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Paper className={classes.avatar} elevation={1}>
                  <Link to={`/@${username}`}>
                    <Avatar
                      alt={name}
                      src={
                        avatar || '/public/photos/mayash-logo-transparent.png'
                      }
                    />
                  </Link>
                </Paper>
              }
              title={
                <Link to={`/@${username}`} className={classes.name}>
                  {name}
                </Link>
              }
              subheader={<div>{username}</div>}
              action={
                <div>
                  <Typography>{moment(doj).fromNow()}</Typography>
                  <Typography>{role}</Typography>
                </div>
              }
            />
          </Card>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = ({ elements }) => ({
  elements,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Member),
);
