/**
 * course page component
 * This component will display onlu one course information
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import Link from 'react-router-dom/Link';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

import Button from 'material-ui/Button';

import Input from '../../../components/Input';
import styles from './styles';

import apiCircleJoin from '../../../api/circles/members/join';

import actionCircleJoin from '../../../actions/circle/member/create';

class MemberCreate extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    circleAdminId: PropTypes.number.isRequired,
    elements: PropTypes.object.isRequired,
    actionCircleJoin: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      hover: false,
      message: '',
      memberUsername: '',
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmitAdd = this.onSubmitAdd.bind(this);
    this.onSubmitJoin = this.onSubmitJoin.bind(this);
  }

  onChange(e) {
    this.setState({ memberUsername: e.target.value });
  }

  onSubmitAdd() {
    this.setState({ memberUsername: '' });
  }

  async onSubmitJoin() {
    try {
      const { circleId, elements } = this.props;
      const { id: userId, token } = elements.user;

      const { statusCode, message } = await apiCircleJoin({
        token,
        circleId,
        memberId: userId,
      });

      if (statusCode >= 300) {
        this.setState({ message: 'There is some problem' });
        return;
      }

      this.setState({ message });

      setTimeout(() => {
        this.setState({ message: '' });
      }, 1500);

      this.props.actionCircleJoin({ memberId: userId, circleId });
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    const { classes, elements, circleAdminId } = this.props;
    const { memberUsername, hover, message } = this.state;

    const { name, username, avatar, id: userId } = elements.user;

    const isAdmin = userId === circleAdminId;

    return (
      <div className={classes.root}>
        <Card raised={hover} className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${username}`}>
                  <Avatar
                    alt={name}
                    src={avatar || '/public/photos/mayash-logo-transparent.png'}
                    className={classes.avatar}
                  />
                </Link>
              </Paper>
            }
            title={
              isAdmin ? (
                <Input
                  value={memberUsername}
                  onChange={this.onChange}
                  placeholder={'Enter member ID'}
                />
              ) : (
                <div>
                  <Link to={`/@${username}`} className={classes.name}>
                    {name}
                  </Link>
                  <br />
                  <Link to={`/@${username}`} className={classes.name}>
                    {username}
                  </Link>
                </div>
              )
            }
            action={
              <Button
                raised
                color="accent"
                className={classes.button}
                onClick={isAdmin ? this.onSubmitAdd : this.onSubmitJoin}
              >
                {isAdmin ? 'Add' : 'Join Group'}
              </Button>
            }
            className={classes.cardHeader}
            onFocus={this.onFocus}
          />
          {message.length ? (
            <CardContent>
              <Typography>{message}</Typography>
            </CardContent>
          ) : null}
        </Card>
      </div>
    );
  }
}

const mapStateToProps = ({ elements }) => ({
  elements,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ actionCircleJoin }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(MemberCreate),
);
