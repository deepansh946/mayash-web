const styles = {
  root: {
    padding: '1%',
  },
  name: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  username: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  button: {
    marginTop: '10px',
  },
};

export default styles;
