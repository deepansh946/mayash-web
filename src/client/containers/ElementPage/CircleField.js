/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';
import { withStyles } from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Loading from '../../components/Loading';

// import actionCirclePostsGet from '../../actions/posts/circles/getAll';
// import actionCircleCoursesGet from '../../actions/courses/circles/getAll';

const AsyncElementInfo = Loadable({
  loader: () => import('../ElementInfo'),
  modules: ['../ElementInfo'],
  loading: Loading,
});

const AsyncFollow = Loadable({
  loader: () => import('../Follow'),
  modules: ['../Follow'],
  loading: Loading,
});

const AsyncTabPosts = Loadable({
  loader: () => import('./Tabs/TabPosts'),
  modules: ['./Tabs/TabPosts'],
  loading: Loading,
});

const AsyncTabCourses = Loadable({
  loader: () => import('./Tabs/TabCourses'),
  modules: ['./Tabs/TabCourses'],
  loading: Loading,
});

// const AsyncErrorPage = Loadable({
//   loader: () => import('../../components/ErrorPage'),
//   modules: ['../../components/ErrorPage'],
//   loading: Loading,
// });

const styles = (theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
});

class CircleField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const { elements, element } = this.props;
    const { token } = elements.user;
    const { id } = element;

    // this.props.actionCirclePostsGet({ id, token });
    // this.props.actionCircleCoursesGet({ id, token });
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes, elements, element, posts /* , courses */ } = this.props;
    const { user } = elements;
    const { id: userId, isSignedIn } = user;
    const { id } = element;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AsyncElementInfo user={user} element={element} />

        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Posts" disabled />
            <Tab label="Courses" disabled />
            <Tab label="Children" disabled />

            {isSignedIn && userId !== id ? (
              <AsyncFollow element={element} />
            ) : null}
          </Tabs>
        </AppBar>
        {value === 0 && (
          <AsyncTabPosts user={user} element={element} posts={posts} />
        )}
        {value === 1 && <AsyncTabCourses />}
        {value === 2 && <div>Sub Categories</div>}
      </div>
    );
  }
}

CircleField.propTypes = {
  classes: PropTypes.object.isRequired,

  element: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  posts: PropTypes.object.isRequired,
  // courses: PropTypes.object.isRequired,

  // actionCirclePostsGet: PropTypes.func.isRequired,
  // actionCircleCoursesGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ posts, courses }) => ({ posts, courses });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      // actionCirclePostsGet,
      // actionCircleCoursesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CircleField),
);
