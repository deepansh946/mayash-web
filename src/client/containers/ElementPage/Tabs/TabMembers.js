/** @format */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import apiGetAllMember from '../../../api/circles/members/getAll';
import apiGetAllElements from '../../../api/elements/getAll';

import actionGetAllElements from '../../../actions/elements/getAll';
import actionGetAllMembers from '../../../actions/circle/member/getAll';

// import MemberCreate from '../member/Create';
import Member from '../member/index';

const styles = {
  root: {},
};

class TabMembers extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    elements: PropTypes.object.isRequired,
    circleId: PropTypes.number.isRequired,
    actionGetAllElements: PropTypes.func.isRequired,
    actionGetAllMembers: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    try {
      const { elements, circleId } = this.props;
      const { token, id: userId } = elements.user;

      const { statusCode, payload } = await apiGetAllMember({
        token,
        circleId,
      });

      if (statusCode >= 300) {
        return;
      }

      const membersInfo = payload.map((p) => ({
        role: p.role,
        memberId: p.memberId,
        doj: p.doj,
      }));

      this.props.actionGetAllMembers({ circleId, membersInfo });

      // Only fetch those member those are not already fetched
      let memberIds = payload.map((p) => {
        if (elements[p.memberId] === undefined) {
          return p.memberId;
        }
        return 0;
      });

      memberIds = memberIds.filter((m) => m !== 0);

      if (memberIds.length === 0) {
        return;
      }

      const res = await apiGetAllElements({ token, ids: memberIds });

      if (res.statusCode >= 300) {
        return;
      }

      this.props.actionGetAllElements({ elements: res.payload });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes, elements, circleId } = this.props;

    const { members } = elements[circleId];

    // Futher it will interchange with circle admin Id
    const circleAdminId = 'userId';

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={9} md={7} lg={6} xl={6}>
          <MemberCreate circleId={circleId} circleAdminId={circleAdminId} />
        </Grid>
        {members !== undefined ? (
          <Grid item xs={12} sm={9} md={7} lg={6} xl={6}>
            {members.map((m) => <Member member={m} />)}
          </Grid>
        ) : null}
      </Grid>
    );
  }
}

const mapStateToProps = ({ elements }) => ({ elements });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ actionGetAllElements, actionGetAllMembers }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(TabMembers),
);
