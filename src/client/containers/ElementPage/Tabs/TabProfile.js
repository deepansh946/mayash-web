/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';

import ProfileWidget from '../../../components/ProfileWidget';
import Resume from '../../Resume';

const styles = {
  root: {},
  gridItem: {
    padding: '1%',
  },
};

class TabProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, user, element } = this.props;
    const { elementType } = element;

    return (
      <Grid container spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={4}
          md={4}
          lg={3}
          xl={3}
          className={classes.gridItem}
        >
          <ProfileWidget {...element} />
        </Grid>
        {elementType === 'user' ? (
          <Grid
            item
            xs={12}
            sm={8}
            md={8}
            lg={9}
            xl={9}
            className={classes.gridItem}
          >
            <Resume user={user} element={element} />
          </Grid>
        ) : null}
      </Grid>
    );
  }
}

TabProfile.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  element: PropTypes.object.isRequired,
};

export default withStyles(styles)(TabProfile);
