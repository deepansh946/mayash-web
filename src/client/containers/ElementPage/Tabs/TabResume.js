/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardContent } from 'material-ui/Card';

import Button from 'material-ui/Button';
import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';

import { convertToRaw } from 'draft-js';

import MayashEditor, { createEditorState } from '../../../../lib/mayash-editor';

import apiUserUpdate from '../../../api/users/update';

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  card: {
    borderRadius: '8px',
  },
  editButton: {
    position: 'fixed',
    zIndex: 1,
    [theme.breakpoints.up('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('md')]: {
      bottom: '20px',
      right: '20px',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
      bottom: '10px',
      right: '10px',
    },
  },
});

class TabResume extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resume: createEditorState(props.element.resume),
    };

    this.onChange = this.onChange.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  // This on change function is for MayashEditor
  onChange(resume) {
    this.setState({ resume });
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  onEdit() {
    this.setState({ edit: !this.state.edit });
  }
  async onSave() {
    const { resume } = this.state;
    const { id: userId, token } = this.props.user;

    const { statusCode, error } = await apiUserUpdate({
      id: userId,
      token,
      resume: convertToRaw(resume.getCurrentContent()),
    });

    if (statusCode !== 200) {
      // error handle
      console.error(error);
      return;
    }

    this.props.actionUserUpdate({
      id: userId,
      resume: convertToRaw(resume.getCurrentContent()),
    });

    this.setState({ edit: false });
  }

  render() {
    const { classes, element, user } = this.props;

    const { id: elementId, isSignedIn } = element;
    const { id: userId } = user;

    const { hover, edit, resume } = this.state;

    return (
      <Grid container spacing={0} justify="center" className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={10}
          lg={9}
          xl={9}
          className={classes.gridItem}
        >
          <div
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
          >
            <Card raised={hover} className={classes.card}>
              <CardContent>
                <MayashEditor
                  editorState={resume}
                  onChange={this.onChange}
                  readOnly={!edit}
                />
              </CardContent>
            </Card>
            {isSignedIn && userId === elementId ? (
              <Button
                fab
                color="accent"
                aria-label="Edit"
                className={classes.editButton}
                onClick={edit ? this.onSave : this.onEdit}
              >
                {edit ? <SaveIcon /> : <EditIcon />}
              </Button>
            ) : null}
          </div>
        </Grid>
      </Grid>
    );
  }
}

TabResume.propTypes = {
  classes: PropTypes.object.isRequired,

  user: PropTypes.object.isRequired,
  element: PropTypes.object.isRequired,

  actionUserUpdate: PropTypes.func,
};

export default withStyles(styles)(TabResume);
