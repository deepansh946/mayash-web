/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { withStyles } from 'material-ui/styles';
import Grid from 'material-ui/Grid';

import CourseCreate from '../../CourseCreate';
import Timeline from '../../Timeline';

const styles = {
  root: {},
};

class TabCourses extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, user, element, courses } = this.props;
    const { isSignedIn, id } = user;

    return (
      <div className={classes.root}>
        <Grid container justify="center" spacing={0}>
          <Grid item xs={12} sm={9} md={7} lg={6} xl={6}>
            {isSignedIn && id === element.id ? <CourseCreate /> : null}
            <Timeline type={'courses'} courses={courses} element={element} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

TabCourses.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  element: PropTypes.object.isRequired,
  courses: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => state;
const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(TabCourses),
);
