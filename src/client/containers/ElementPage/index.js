/** @format */

import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import Loading from '../../components/Loading';

const AsyncUser = Loadable({
  loader: () => import('./User'),
  modules: ['./User'],
  loading: Loading,
});

const AsyncCircleEdu = Loadable({
  loader: () => import('./CircleEdu'),
  modules: ['./CircleEdu'],
  loading: Loading,
});

const AsyncCircleOrg = Loadable({
  loader: () => import('./CircleOrg'),
  modules: ['./CircleOrg'],
  loading: Loading,
});

const AsyncCircleField = Loadable({
  loader: () => import('./CircleField'),
  modules: ['./CircleField'],
  loading: Loading,
});

const AsyncCircleLocation = Loadable({
  loader: () => import('./CircleLocation'),
  modules: ['./CircleLocation'],
  loading: Loading,
});

const AsyncErrorPage = Loadable({
  loader: () => import('../../components/ErrorPage'),
  modules: ['../../components/ErrorPage'],
  loading: Loading,
});

const ElementPage = (props) => {
  const { match, elements } = props;
  const { username } = match.params;

  const element = Object.values(elements).find(
    (e) => typeof e.username !== 'undefined' && e.username === username,
  );

  if (typeof element === 'undefined') {
    return <AsyncErrorPage {...element} />;
  }

  const { statusCode, elementType, circleType } = element;

  if (typeof statusCode !== 'number' || statusCode >= 300) {
    return <AsyncErrorPage {...element} />;
  }

  if (elementType === 'user') {
    return <AsyncUser element={element} {...props} />;
  }

  if (elementType === 'circle' && circleType === 'edu') {
    return <AsyncCircleEdu element={element} {...props} />;
  }

  if (elementType === 'circle' && circleType === 'org') {
    return <AsyncCircleOrg element={element} {...props} />;
  }

  if (elementType === 'circle' && circleType === 'field') {
    return <AsyncCircleField element={element} {...props} />;
  }

  if (elementType === 'circle' && circleType === 'location') {
    return <AsyncCircleLocation element={element} {...props} />;
  }

  // all posible errors are handled above, still something goes odd, this error
  // page will be rendered.
  return <AsyncErrorPage {...element} />;
};

ElementPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      username: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,

  elements: PropTypes.object,
};

const mapStateToProps = ({ elements }) => ({ elements });

export default connect(mapStateToProps)(ElementPage);
