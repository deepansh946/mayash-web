/**
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import List, { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

import HomeIcon from 'material-ui-icons/Home';
import PhoneIcon from 'material-ui-icons/Phone';
import LocalLibraryIcon from 'material-ui-icons/LocalLibrary';
import ShoppingCartIcon from 'material-ui-icons/ShoppingCart';
import PeopleIcon from 'material-ui-icons/People';
import WbSunnyIcon from 'material-ui-icons/WbSunny';
import GroupAddIcon from 'material-ui-icons/GroupAdd';
// import NoteIcon from 'material-ui-icons/Note';
import WebIcon from 'material-ui-icons/Web';
import VpnKeyIcon from 'material-ui-icons/VpnKey';
import ExitToAppIcon from 'material-ui-icons/ExitToApp';

import TitleButton from './TitleButton';
import ListItemButton from './ListItemButton';

class DrawerNotSignedIn extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    // This array contains list of all the routes if user is not signed in
    const routeList = [
      {
        title: 'Home',
        to: '/',
        icon: HomeIcon,
      },
      {
        title: 'Introduction',
        to: '/introduction',
        icon: WbSunnyIcon,
      },
      // {
      //   title: 'About Us',
      //   to: '/about-us',
      //   icon: AccountBalanceIcon,
      // },
      // {
      //   title: 'Our Vision',
      //   to: '/our-vision',
      //   icon: RemoveRedEyeIcon,
      // },
      {
        title: 'Services',
        to: '/services',
        icon: LocalLibraryIcon,
      },
      {
        title: 'Pricing',
        to: '/pricing',
        icon: ShoppingCartIcon,
      },
      {
        title: 'Team',
        to: '/team',
        icon: PeopleIcon,
      },
      {
        title: 'Join Us',
        to: '/join-us',
        icon: GroupAddIcon,
      },
      {
        title: 'Developers Training',
        to: '/developers-training',
        icon: WebIcon,
      },
      {
        title: 'Privacy Statement',
        to: '/privacy',
        icon: VpnKeyIcon,
      },
      {
        title: 'Terms of Services',
        to: '/terms',
        icon: VpnKeyIcon,
      },
      {
        title: 'Refund Policy',
        to: '/refund',
        icon: VpnKeyIcon,
      },
      {
        title: 'Contact Us',
        to: '/contact-us',
        icon: PhoneIcon,
      },
      {
        title: 'Donate Us',
        to: '/donate',
        icon: VpnKeyIcon,
      },
      {
        title: 'Sign In',
        to: '/sign-in',
        icon: VpnKeyIcon,
      },
    ];

    return (
      <Drawer
        open={this.props.open}
        onRequestClose={this.props.closeDrawer}
        onClick={this.props.closeDrawer}
      >
        <List className={classes.list} disablePadding>
          {/* Title or Heading of Drawer. */}

          <Avatar
            alt={'jsks'}
            src={'/public/photos/mayash-logo-transparent.png'}
            className={classes.avatarUnsignedPage}
          />

          <TitleButton
            title={'Mayash'}
            description={'Transforming Education...'}
            to={'/'}
          />

          {/* This Divider will seperate the Title from 
              rest of the routes. */}
          <Divider />
          {/* Here all the routs are listed. */}
          {routeList.map((route, i) => (
            <div key={i + 1} className={classes.button}>
              <ListItemButton
                title={route.title}
                to={route.to}
                Icon={route.icon}
              />
            </div>
          ))}
          <div className={classes.button}>
            <ListItem
              button
              onClick={() => {
                const href = 'https://goo.gl/forms/EZnM9keVfb1DZzLy2';
                window.open(href);
              }}
            >
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary={'Survey'} />
            </ListItem>
          </div>
        </List>
      </Drawer>
    );
  }
}

DrawerNotSignedIn.propTypes = {
  classes: PropTypes.object.isRequired,

  open: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
};

export default DrawerNotSignedIn;
