/**
 * This is a Drawer Container Component.
 * This will include all the routes when users are not signed in.
 * When users are signed in, it will display their personal informations.
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';
// import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import withStyles from 'material-ui/styles/withStyles';

import Loading from '../../components/Loading';

import styles from './styles';

const AsyncDrawerNotSignedIn = Loadable({
  loader: () => import('./DrawerNotSignedIn'),
  modules: ['./DrawerNotSignedIn'],
  loading: Loading,
});

const AsyncDrawerSignedIn = Loadable({
  loader: () => import('./DrawerSignedIn'),
  modules: ['./DrawerSignedIn'],
  loading: Loading,
});

function AppDrawer(props) {
  const { isSignedIn } = props.elements.user;

  return isSignedIn ? (
    <AsyncDrawerSignedIn {...props} />
  ) : (
    <AsyncDrawerNotSignedIn {...props} />
  );
}

/**
 * This will validate all the incoming props from parent component
 * to AppDrawer component.
 * @type {Object}
 */
AppDrawer.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  open: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

/**
 * @param {object} dispatch : dipatch meesege (action type)
 * @returns {object} : action creators
 */
/*
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
*/

/**
 * 'connect' method connect state to action creators
 * When we call connect, immediately we call AppDrawer component
 * and all states and action creators are passed to AppDrawer component
 */
export default connect(
  mapStateToProps,
  // mapDispatchToProps,
)(withStyles(styles)(AppDrawer));
