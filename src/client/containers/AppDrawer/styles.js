/**
 * /*
 *   This Style file is for index.js file.
 *
 * @format
 */

const styles = (theme) => ({
  list: {
    width: 250,
    flex: 'initial',
  },
  listFull: {
    width: 'auto',
    flex: 'initial',
  },
  user: {
    minHeight: '100px',
    paddingTop: '12px',
    paddingRight: '16px',
    paddingBottom: '12px',
    paddingLeft: '16px',
    backgroundColor: theme.palette.primary[700],
    backgroundImage:
      'url("https://drivenlocal.com/wp-content/uploads/2015/10/Material-design.jpg")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
  },
  avatar: {
    width: 60,
    height: 60,
    paddingTop: 10,
    paddingBottom: 10,
  },

  avatarUnsignedPage: {
    float: 'left',
    paddingTop: 10,
  },
  button: {
    transitionDuration: '0.4s',
    '&:hover': {
      background: theme.palette.primary[100],
      transform: 'scale(1.09)',
    },
  },
  info: {
    color: theme.palette.getContrastText(theme.palette.primary[700]),
    paddingTop: 2,
    paddingBottom: 2,
  },
});

export default styles;
