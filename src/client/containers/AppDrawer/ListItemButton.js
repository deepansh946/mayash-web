/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import { ListItem, ListItemIcon, ListItemText } from 'material-ui/List';

class ListItemButton extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props !== nextProps;
  }

  render() {
    const { to, title, Icon } = this.props;

    return (
      <ListItem button component={Link} to={to}>
        <ListItemIcon>
          <Icon />
        </ListItemIcon>
        <ListItemText primary={title} />
      </ListItem>
    );
  }
}

ListItemButton.propTypes = {
  to: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  // Icon should be taken from material-ui-icons library,
  // It's type is function but it is a react component rendering an icon.
  Icon: PropTypes.func.isRequired,
};

export default ListItemButton;
