/**
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import List from 'material-ui/List';

import HomeIcon from 'material-ui-icons/Home';
import PersonIcon from 'material-ui-icons/Person';

import ListItemButton from './ListItemButton';

class DrawerSignedIn extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, elements } = this.props;
    const { user } = elements;

    // This array contains list of all the routes if user is signed in
    const SignedInRouteList = [
      {
        title: 'Home',
        to: '/',
        icon: HomeIcon,
      },
      {
        title: 'Profile',
        to: '/profile',
        icon: PersonIcon,
      },
      // {
      //   title: 'Dashboard',
      //   to: '/dashboard',
      //   icon: DashboardIcon,
      // },
    ];

    return (
      <Drawer
        open={this.props.open}
        onRequestClose={this.props.closeDrawer}
        onClick={this.props.closeDrawer}
      >
        <List className={classes.list} disablePadding>
          <div className={classes.user}>
            <Avatar
              alt={user.name}
              className={classes.avatar}
              src={user.avatar || '/public/photos/mayash-logo-transparent.png'}
            />
            <div className={classes.info}>
              <div>{user.name}</div>
              <div>{`@${user.username}`}</div>
            </div>
          </div>

          {/* This Divider will seperate the Title from rest of the routes. */}
          <Divider />

          {/* List all the things which is required for user 
            when he/she signed in. */}
          {SignedInRouteList.map((route, i) => (
            <div key={i + 1} className={classes.button}>
              <ListItemButton
                title={route.title}
                to={route.to}
                Icon={route.icon}
              />
            </div>
          ))}
        </List>
      </Drawer>
    );
  }
}

DrawerSignedIn.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  open: PropTypes.bool.isRequired,
  closeDrawer: PropTypes.func.isRequired,
};

export default DrawerSignedIn;
