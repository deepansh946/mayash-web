/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import { ListItem, ListItemText } from 'material-ui/List';

class TitleButton extends Component {
  shouldComponentUpdate(nextProps) {
    return this.props !== nextProps;
  }

  render() {
    const { title, description, to } = this.props;

    return (
      <ListItem button component={Link} to={to}>
        <ListItemText primary={title} secondary={description} />
      </ListItem>
    );
  }
}

TitleButton.propTypes = {
  to: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default TitleButton;
