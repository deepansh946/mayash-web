/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import Loadable from 'react-loadable';
import Loading from '../../components/Loading';

import apiNotesGet from '../../api/notes/getAll';
import actionNotesGet from '../../actions/notes/getAll';

const styles = (theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    align: 'center',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
});

const AsyncNoteCreate = Loadable({
  loader: () => import('../NoteCreate'),
  modules: ['../NoteCreate'],
  loading: Loading,
});

const AsyncNote = Loadable({
  loader: () => import('../Note'),
  modules: ['../Note'],
  loading: Loading,
});

class NoteTimeline extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    try {
      const { elements, element } = this.props;
      const { id: userId, token } = elements.user;
      const { id, elementType } = element;

      const { statusCode, error, payload, next } = await apiNotesGet({
        token,
        userId,
      });

      if (statusCode >= 300) {
        // handle error
        console.error(error);
        return;
      }

      this.props.actionNotesGet({
        id: userId,
        statusCode,
        notes: payload,
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes, elements, element } = this.props;

    const { isSignedIn, id: userId } = elements.user;
    const { notes, id } = element;

    return (
      <div className={classes.root}>
        <Grid container justify="center" spacing={0}>
          <Grid item xs={12} sm={9} md={7} lg={6} xl={6}>
            <AsyncNoteCreate />
          </Grid>
        </Grid>
        <Grid container justify="center" spacing={8}>
          {typeof notes !== 'undefined'
            ? notes.map(({ noteId }) => (
              <Grid item key={noteId} xs={6} sm={6} md={4} lg={3} xl={3}>
                <AsyncNote key={noteId} noteId={noteId} />
              </Grid>
            ))
            : null}
        </Grid>
      </div>
    );
  }
}

NoteTimeline.propTypes = {
  classes: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  element: PropTypes.shape({
    id: PropTypes.number,
    username: PropTypes.string,
    notes: PropTypes.arrayOf(
      PropTypes.shape({
        noteId: PropTypes.number,
      }),
    ),
  }),
  actionNotesGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements, notes }) => ({ elements, notes });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionNotesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(NoteTimeline),
);
