/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Loadable from 'react-loadable';

import Loading from '../../components/Loading';

import apiUserCoursesGet from '../../api/courses/users/getAll';
import actionCoursesGet from '../../actions/courses/getAll';

// import actions from '../../actions';

const styles = {
  root: {},
};

const AsyncCourseCreate = Loadable({
  loader: () => import('../CourseCreate'),
  modules: ['../CourseCreate'],
  loading: Loading,
});

const AsyncCourse = Loadable({
  loader: () => import('../Course'),
  modules: ['../Course'],
  loading: Loading,
});

class CourseTimeline extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    try {
      const { elements, element } = this.props;
      const { id: userId, token } = elements.user;
      const { id, elementType } = element;

      const { statusCode, error, payload, next } = await apiUserCoursesGet({
        token,
        userId,
      });

      if (statusCode >= 300) {
        // handle error
        console.error(error);
        return;
      }

      this.props.actionCoursesGet({
        id: userId,
        statusCode,
        courses: payload,
      });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes, elements, element } = this.props;
    const { isSignedIn, id: userId } = elements.user;
    const { id, courses } = element;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={9} md={7} lg={6} xl={6}>
          {isSignedIn && userId === id ? <AsyncCourseCreate /> : null}
          {typeof courses !== 'undefined' &&
            courses.map(({ courseId }) => (
              <AsyncCourse key={courseId} courseId={courseId} />
            ))}
        </Grid>
      </Grid>
    );
  }
}

CourseTimeline.propTypes = {
  classes: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
  element: PropTypes.shape({
    id: PropTypes.number,
    username: PropTypes.string,
    courses: PropTypes.arrayOf(
      PropTypes.shape({
        postId: PropTypes.number,
      }),
    ),
  }),
  actionCoursesGet: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements, courses }) => ({ elements, courses });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCoursesGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CourseTimeline),
);
