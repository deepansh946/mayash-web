/**
 * /* eslint no-unused-vars: "off"
 *
 * @format
 */

// change this setting later.

/**
 * ClassroomCard component
 * This component is used to display only one classroom
 */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardMedia, CardActions } from 'material-ui/Card';

const styles = {
  root: {
    flex: '1 0 100%',
    padding: '1%',
  },
  grid: {
    padding: '2%',
  },
  media: {
    height: 250,
  },
};

/**
 * This is the another view to create a react component
 * @param {*} param0
 */
const ClassroomCard = ({
  classes,
  id,
  username,
  name,
  description,
  avatar,
  elementType,
  circleType,
}) => (
  <Grid item xs={12} sm={4} md={3} lg={3} xl={3} className={classes.grid}>
    <Card raised>
      <CardMedia
        className={classes.media}
        image="/public/photos/mayash-logo-transparent.png"
        title="Contemplative Reptile"
      />
      <CardHeader title={name} subheader={`@${username}`} />
      <CardActions>
        <Button dense color="primary">
          Share
        </Button>
        <Button dense color="primary">
          Learn More
        </Button>
      </CardActions>
    </Card>
  </Grid>
);

/**
 * This will validate all the incoming props from parent component
 * to ClassroomCard component.
 * @type {Object}
 */
ClassroomCard.propTypes = {
  classes: PropTypes.object.isRequired,
  id: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  elementType: PropTypes.string.isRequired,
  circleType: PropTypes.string.isRequired,
};

export default withStyles(styles)(ClassroomCard);
