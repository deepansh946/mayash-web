/**
 * This is the Classrooms page Component that is goint to collect
 * all the classes in a single page
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import ClassroomCard from './ClassroomCard';

/**
 * material ui have inline styling  property.
 * this newly created styleshhet can be override on our component
 * [styleSheet description]
 * @type {[type]}
 */
const styles = {
  root: {
    flex: '1 0 100%',
    padding: '1%',
  },
  grid: {
    padding: '2%',
  },
  textField: {
    width: '100%',
  },
};

class ClassroomsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // initial state for all classroom
      classrooms: [
        {
          id: 1,
          username: 'google',
          elementType: 'circle',
          circleType: 'org',
          name: 'Google',
          avatar: '/public/photos/mayash-logo-transparent.png',
          description: 'Google is a Tech. giant',
        },
        {
          id: 2,
          username: 'iitdhn',
          elementType: 'circle',
          circleType: 'edu',
          name: 'IIT Dhanbad',
          avatar: '/public/photos/mayash-logo-transparent.png',
          description: 'Indian Institute of Technology (ISM), Dhanbad',
        },
        {
          id: 3,
          username: 'javascript',
          elementType: 'circle',
          circleType: 'field',
          name: 'JavaScript',
          avatar: '/public/photos/mayash-logo-transparent.png',
          description: 'Worlds most popular language.',
        },
        {
          id: 4,
          username: 'apple',
          elementType: 'circle',
          circleType: 'org',
          name: 'Apple Inc.',
          avatar: '/public/photos/mayash-logo-transparent.png',
          description:
            'Apple Inc. is most popular company in technology' + ' field.',
        },
        {
          id: 5,
          username: 'unacademy',
          elementType: 'circle',
          circleType: 'org',
          name: 'Unacademy',
          avatar: '/public/photos/mayash-logo-transparent.png',
          description:
            'Prepare for examinations and take any number of' +
            ' courses from various topics on Unacademy - an education' +
            ' revolution.',
        },
      ],
    };
  }

  render() {
    const { classes } = this.props;

    // here classrooms is the state define in this component.
    const { classrooms } = this.state;

    return (
      <div className={classes.root}>
        <Grid container>
          {classrooms.map((c) => <ClassroomCard key={c.id} {...c} />)}
        </Grid>
      </div>
    );
  }
}

ClassroomsPage.propTypes = {
  classes: PropTypes.object.isRequired,
  // match: PropTypes.object.isRequired,
};

export default withStyles(styles)(ClassroomsPage);
