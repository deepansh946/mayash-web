/**
 * PostCreate component will be used to create new post.
 *
 * Note: before adding this component in any place, make sure
 * the person who is accessing this component should have right to create post.
 * i.e. person should be logged in and user can only create post for their
 * profile only.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Link from 'react-router-dom/Link';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Paper from 'material-ui/Paper';

import Input from '../../components/Input';

import apiPostCreate from '../../api/posts/users/create';
import actionPostCreate from '../../actions/posts/users/create';

const styles = {
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  gridContainer: {},
  post: {
    padding: '1%',
  },
  cardHeader: {
    // textAlign: 'center'
  },
  cardMedia: {},
  cardMediaImage: {
    width: '100%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

/**
 * PostCreate component will create a new post for signed in user.
 * @class PostCreate
 * @version 1.0.0
 */
class PostCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      title: '',
      titleLength: 0,
      placeholder: 'Title...',
      postType: 'article',

      valid: false,
      statusCode: 0,
      error: '',
      message: '',
      hover: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const title = e.target.value;
    const titleLength = title.length;
    const valid = titleLength > 0 && titleLength < 148;

    this.setState({
      title,
      titleLength,
      valid,
      error: !valid ? 'Title length should be in between 1 to 148.' : '',
    });
  }

  async onSubmit(e) {
    e.preventDefault();
    const { token, id: userId } = this.props.elements.user;
    const { postType, title } = this.state;

    this.setState({ message: 'Creating post...' });

    const { statusCode, error, payload } = await apiPostCreate({
      token,
      userId,
      postType,
      title,
    });

    if (statusCode >= 300) {
      // handle error
      this.setState({ statusCode, error });
      return;
    }

    this.setState({ message: 'Successfully Created.' });

    setTimeout(() => {
      this.setState({
        valid: false,
        statusCode: 0,
        error: '',
        message: '',

        postType: 'article',

        title: '',
        titleLength: 0,
      });
    }, 1500);

    this.props.actionPostCreate({
      statusCode,
      ...payload,
      title,
      postType,
    });
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });
  onFocus = () => this.setState({ active: true });

  render() {
    const { hover, active, placeholder, title, message } = this.state;
    const { classes, elements } = this.props;
    const { name, avatar, username } = elements.user;

    return (
      <div
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Card raised={hover} className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${username}`}>
                  <Avatar
                    alt={name}
                    src={avatar || '/public/photos/mayash-logo-transparent.png'}
                    className={classes.avatar}
                  />
                </Link>
              </Paper>
            }
            title={
              <Input
                value={title}
                onChange={this.onChange}
                placeholder={placeholder}
              />
            }
            className={classes.cardHeader}
            onFocus={this.onFocus}
          />
          {active && message.length ? (
            <CardContent>
              <Typography>{message}</Typography>
            </CardContent>
          ) : null}
          {active ? (
            <CardActions>
              <div className={classes.flexGrow} />
              <Button raised color="accent" onClick={this.onSubmit}>
                Create
              </Button>
            </CardActions>
          ) : null}
        </Card>
      </div>
    );
  }
}

PostCreate.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  actionPostCreate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });
const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionPostCreate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(PostCreate),
);
