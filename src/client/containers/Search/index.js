/** @format */ /**
 * search component
 * This component filters all the posts and courses and element
 * and display in different tabs
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';
import Card, { CardHeader } from 'material-ui/Card';

// import actions from '../../actions';

const styles = {
  root: {
    flex: '1 0 100%',
  },
};

// for displaying Element
const ElementCard = ({
  type,
  elementType,
  id,
  username,
  name,
  guru,
  classroom,
}) => (
  <div style={{ padding: '1%' }}>
    <Card raised>
      <CardHeader title={name} subheader={`@${username}`} />
    </Card>
  </div>
);

ElementCard.propTypes = {
  type: PropTypes.string.isRequired,
  elementType: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  guru: PropTypes.bool.isRequired,
  classroom: PropTypes.bool.isRequired,
};

// for displaying posts
const PostCard = ({ type, postType, postId, title, description }) => (
  <div style={{ padding: '1%' }}>
    <Card raised>
      <CardHeader title={title} subheader={description} />
    </Card>
  </div>
);

PostCard.propTypes = {
  type: PropTypes.string.isRequired,
  postType: PropTypes.string.isRequired,
  postId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

// for displaying course
const CourseCard = ({ type, postType, courseId, title, description }) => (
  <div style={{ padding: '1%' }}>
    <Card raised>
      <CardHeader title={title} subheader={description} />
    </Card>
  </div>
);

CourseCard.propTypes = {
  type: PropTypes.string.isRequired,
  postType: PropTypes.string.isRequired,
  courseId: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

// for displaying Timeline (Timeline contains post, course, element)
const Timeline = ({ data }) => (
  <div>
    {data.map((e, i) => {
      if (e.type === 'element') {
        return <ElementCard key={e.id} {...e} />;
      }
      if (e.type === 'post') {
        return <PostCard key={e.postId} {...e} />;
      }
      if (e.type === 'course') {
        return <PostCard key={e.courseId} {...e} />;
      }
      return <div key={i + 1}>{e.type}</div>;
    })}
  </div>
);

Timeline.propTypes = {
  data: PropTypes.array.isRequired,
};

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
      data: [
        {
          type: 'element',
          elementType: 'user',
          id: 1,
          username: 'hbarve1',
          name: 'Himank Barve',
          guru: true,
          classroom: true,
        },
        {
          type: 'element',
          elementType: 'user',
          id: 2,
          username: 'shubham',
          name: 'Shubham Maurya',
          guru: true,
        },
        {
          type: 'element',
          elementType: 'user',
          id: 3,
          username: 'deepansh',
          name: 'Deepansh Bhargawa',
        },
        {
          type: 'element',
          elementType: 'circle',
          circleType: 'edu',
          id: 4,
          username: 'iitdhn',
          name: 'Indian Institute of Technology(ISM), Dhanbad',
          classroom: true,
        },
        {
          type: 'element',
          elementType: 'circle',
          circleType: 'field',
          id: 5,
          username: 'javascript',
          name: 'JavaScript',
          classroom: true,
        },
        {
          type: 'element',
          elementType: 'circle',
          circleType: 'org',
          id: 6,
          username: 'google',
          name: 'Google Inc.',
          classroom: true,
        },
        {
          type: 'post',
          postType: 'article',
          postId: 1,
          authorId: 1,
          title: 'This is First Post',
          description: 'This is First Post description.',
        },
        {
          type: 'post',
          postType: 'article',
          postId: 2,
          authorId: 1,
          title: 'This is Second Post',
          description: 'This is Second Post description.',
        },
        {
          type: 'course',
          courseId: 1,
          authorId: 1,
          title: 'This is First Course',
          description: 'This is First Course description.',
        },
        {
          type: 'course',
          courseId: 2,
          authorId: 1,
          title: 'This is Second Course',
          description: 'This is First Course description.',
        },
      ],
    };
  }

  handleChange(event, value) {
    this.setState({ value });
  }

  render() {
    const { classes } = this.props;

    const { value, data } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="All" />
            <Tab label="Users" />
            <Tab label="Circles" />
            <Tab label="Posts" />
            <Tab label="Courses" />
            <Tab label="Classrooms" />
          </Tabs>
        </AppBar>
        {/* This will show all searched element */}
        {value === 0 && <Timeline data={data} />}
        {/* This will show filtered searched element of elementType */}
        {value === 1 && (
          <Timeline
            data={data.filter(
              (d) => d.type === 'element' && d.elementType === 'user',
            )}
          />
        )}
        {value === 2 && (
          <Timeline
            data={data.filter(
              (d) => d.type === 'element' && d.elementType === 'circle',
            )}
          />
        )}
        {value === 3 && (
          <Timeline data={data.filter((d) => d.type === 'post')} />
        )}
        {value === 4 && (
          <Timeline data={data.filter((d) => d.type === 'course')} />
        )}
        {value === 5 && (
          <Timeline
            data={data.filter(
              (d) => d.type === 'element' && d.classroom === true,
            )}
          />
        )}
      </div>
    );
  }
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Search),
);
