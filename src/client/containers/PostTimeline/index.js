/**
 * PostTimeline component will display only posts
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Loadable from 'react-loadable';

import Loading from '../../components/Loading';

import apiUserPostsGet from '../../api/posts/users/getAll';

import actionPostsGet from '../../actions/posts/getAll';

// import actions from '../../actions';

import styles from './styles';

const AsyncPostCreate = Loadable({
  loader: () => import('../PostCreate'),
  modules: ['../PostCreate'],
  loading: Loading,
});

const AsyncPost = Loadable({
  loader: () => import('../Post'),
  modules: ['../Post'],
  loading: Loading,
});

class PostTimeline extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,

    elements: PropTypes.object.isRequired,

    element: PropTypes.shape({
      id: PropTypes.number,
      username: PropTypes.string,
      posts: PropTypes.arrayOf(
        PropTypes.shape({
          postId: PropTypes.number,
        }),
      ),
    }),

    actionPostsGet: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    try {
      const { elements, element } = this.props;
      const { id: userId, token } = elements.user;
      const { id, elementType } = element;

      if (elementType !== 'user' || element.posts.length !== 0) {
        // update This timeline component for circles also.
        return;
      }

      const { statusCode, error, payload, next } = await apiUserPostsGet({
        token,
        userId,
      });

      if (statusCode >= 300) {
        // handle error
        console.error(error);
        return;
      }

      this.props.actionPostsGet({
        id: userId,
        statusCode,
        posts: payload,
      });
    } catch (error) {
      console.error(error);
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.state !== nextState && this.props !== nextProps;
  // }

  render() {
    const { classes, elements, element } = this.props;
    const { isSignedIn, id: userId } = elements.user;
    const { id, posts } = element;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={9} md={7} lg={6} xl={6}>
          {/* if user has signed in then also show create post component */}
          {isSignedIn && userId === id ? <AsyncPostCreate /> : null}

          {typeof posts !== 'undefined' &&
            posts.map(({ postId }) => (
              <AsyncPost key={postId} postId={postId} />
            ))}
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionPostsGet,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(PostTimeline),
);
