/**
 * This file contains all the styles for Article Component.
 *
 * @format
 */

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  name: {
    // color: '#365899',
    cursor: 'pointer',
    textDecoration: 'none',
  },
  avatar: {
    borderRadius: '50%',
  },
  card: {
    borderRadius: '8px',
  },
  username: {
    // color: '#365899',
    cursor: 'pointer',
    textDecoration: 'none',
  },
  media: {
    height: 194,
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  editButton: {
    position: 'fixed',
    zIndex: 1,
    [theme.breakpoints.up('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('xl')]: {
      bottom: '40px',
      right: '40px',
    },
    [theme.breakpoints.down('md')]: {
      bottom: '30px',
      right: '30px',
    },
    [theme.breakpoints.down('sm')]: {
      display: 'none',
      bottom: '10px',
      right: '10px',
    },
  },
  margin: {
    border: '0.5px solid rgba(209,209,209,1)',
    marginBottom: '-10px',
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
  },
});

export default styles;
