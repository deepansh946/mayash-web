/**
 * For displaying Article with Title, Description & content
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import Loadable from 'react-loadable';

import Loading from '../../components/Loading';

const AsyncArticle = Loadable({
  loader: () => import('./Article'),
  modules: ['./Article'],
  loading: Loading,
});

const AsyncErrorPage = Loadable({
  loader: () => import('../../components/ErrorPage'),
  modules: ['../../components/ErrorPage'],
  loading: Loading,
});

class PostPage extends Component {
  state = {};

  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.state !== nextState && this.props !== nextProps;
  // }

  render() {
    const { match, posts } = this.props;
    const { postId } = match.params;
    const post = posts[postId];
    const { statusCode } = post;

    // if post is not found or any other error occurs.
    if (!post || typeof statusCode !== 'number' || statusCode >= 300) {
      return <AsyncErrorPage {...post} />;
    }

    return <AsyncArticle post={post} {...this.props} />;
  }
}

PostPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      postId: PropTypes.string.isRequired, // This should be number.
    }).isRequired,
  }).isRequired,

  posts: PropTypes.object.isRequired,
};

const mapStateToProps = ({ posts }) => ({ posts });

export default connect(mapStateToProps)(PostPage);
