/**
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import IconButton from 'material-ui/IconButton';
import InsertPhotoIcon from 'material-ui-icons/InsertPhoto';

import apiPostUpdate from '../../../api/posts/users/update';
import apiPhotoUpload from '../../../api/photos/posts/users/create';

class CoverImage extends Component {
  static propTypes = {
    style: PropTypes.object.isRequired,
    className: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {};

    this.onClick = this.onClick.bind(this);
    // this.onChange = this.onChange.bind(this);
  }

  onClick() {
    this.photo.value = null;
    this.photo.click();
  }

  onChange = async (e) => {
    if (e) {
      e.preventDefault();
    }

    const file = e.target.files[0];

    if (file.type.indexOf('image/') === 0) {
      const formData = new FormData();
      formData.append('photo', file);

      const { authorId: userId, postId, token } = this.props;

      const { statusCode, error, payload } = await apiPhotoUpload({
        token,
        userId,
        postId,
        formData,
      });

      if (statusCode >= 300) {
        // handle Error
        console.error(statusCode, error);
      }

      const { photoUrl: cover } = payload;

      apiPostUpdate({
        token,
        userId,
        postId,
        cover,
      });

      this.props.actionPostUpdate({
        postId,
        cover,
      });
    }
  };

  render() {
    return (
      <div
        style={{
          position: 'relative',
          ...this.props.style,
        }}
        className={this.props.className}
      >
        <IconButton aria-label="Insert Photo" onClick={this.onClick}>
          <InsertPhotoIcon />
          <input
            type="file"
            accept="image/jpeg|png|gif"
            onChange={this.onChange}
            ref={(photo) => {
              this.photo = photo;
            }}
            style={{ display: 'none' }}
          />
        </IconButton>
      </div>
    );
  }
}

export default CoverImage;
