/**
 * This component is created to edit content of the article
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Link from 'react-router-dom/Link';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Loadable from 'react-loadable';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, {
  CardHeader,
  CardMedia,
  CardContent,
  CardActions,
} from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';

import FavoriteIcon from 'material-ui-icons/Favorite';
import ShareIcon from 'material-ui-icons/Share';
import TurnedInIcon from 'material-ui-icons/TurnedIn';
import MoreVertIcon from 'material-ui-icons/MoreVert';

import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';
import CommentIcon from 'material-ui-icons/Comment';
import Badge from 'material-ui/Badge';
import Button from 'material-ui/Button';
import Tooltip from 'material-ui/Tooltip';
import { convertToRaw } from 'draft-js';

import Loading from '../../components/Loading';
import MayashEditor from '../../../lib/mayash-editor/Editor';
import { createEditorState } from '../../../lib/mayash-editor';

import Title from '../../components/Title';
import Description from '../../components/Description';
import CardMediaComponent from './CardMediaComponent';
import Privacy from '../Privacy/index';
import ErrorPage from '../../components/ErrorPage';

import apiPostUpdate from '../../api/posts/users/update';
import apiPhotoUserCreate from '../../api/photos/posts/users/create';

import actionElementGetById from '../../actions/elements/getById';
import actionPostUpdate from '../../actions/posts/update';

import styles from './ArticleStyles';

const AsyncComment = Loadable({
  loader: () => import('../Comment/index'),
  modules: ['../Comment/index'],
  loading: Loading,
});

class Article extends Component {
  constructor(props) {
    super(props);
    const { title, description, data } = props.post;

    this.state = {
      edit: false,
      title,
      description,
      data: createEditorState(data),
      commentBox: false,
    };

    this.onChange = this.onChange.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onClickCommentIcon = this.onClickCommentIcon.bind(this);
  }

  componentDidMount() {
    const { elements, post } = this.props;
    const { token, id: userId } = elements.user;
    const { authorId } = post;

    let author;
    if (authorId === userId) {
      author = elements.user;
    } else {
      author = elements[authorId];
    }

    if (!author || (!author.isFetching && !author.isFetched)) {
      this.props.actionElementGetById({ token, id: authorId });
    }
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   return this.state !== nextState || this.props !== nextProps;
  // }

  // This on change function is for MayashEditor
  onChange(data) {
    this.setState({ data });
  }

  // it changes the current editing state
  onEdit() {
    const { edit } = this.state;
    this.setState({ edit: !edit });
  }
  async onSave() {
    const { edit, title, description, data } = this.state;

    const { elements, post } = this.props;
    const { id: userId, token } = elements.user;
    const { postId, title: titleProp, description: descriptionProp } = post;

    let body = {};

    if (title !== titleProp) {
      body = { ...body, title };
    }

    if (
      typeof description !== 'undefined' &&
      description.length &&
      description !== descriptionProp
    ) {
      body = { ...body, description };
    }

    body.data = convertToRaw(data.getCurrentContent());

    const { statusCode, error } = await apiPostUpdate({
      userId,
      postId,
      token,
      ...body,
    });

    if (statusCode >= 300) {
      // handle error;
      console.error(error);
      return;
    }

    this.props.actionPostUpdate({ postId, ...body });

    // when editing is done set 'edit' to false
    this.setState({ edit: !edit });
  }

  onClickCommentIcon() {
    this.setState({ commentBox: true });
  }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });

  render() {
    const { classes, post, elements } = this.props;
    const { user } = elements;
    const { hover, edit, title, description, data, commentBox } = this.state;

    const { isSignedIn, token, id: userId } = user;
    const { authorId, postId, cover, comments, privacy } = post;

    let commentsSize;

    if (comments !== undefined) {
      commentsSize = Object.keys(comments).length;
    }

    let author = { authorId };

    if (userId === authorId) {
      author = { ...author, ...user };
    } else {
      author = { ...author, ...elements[authorId] };
    }

    if (authorId !== userId && privacy) return <ErrorPage statusCode={401} />;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={11} md={8} lg={8} xl={8}>
          <Card
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
            raised={hover}
            className={classes.card}
          >
            <CardHeader
              avatar={
                <Paper className={classes.avatar} elevation={1}>
                  <Link to={`/@${author.username}`}>
                    <Avatar
                      alt={author.name}
                      src={
                        author.avatar ||
                        '/public/photos/mayash-logo-transparent.png'
                      }
                    />
                  </Link>
                </Paper>
              }
              title={
                <Link to={`/@${author.username}`} className={classes.name}>
                  {author.name}
                </Link>
              }
              subheader={
                <Link to={`/@${author.username}`} className={classes.username}>
                  @{author.username}
                </Link>
              }
              action={
                <div className={classes.flex}>
                  <Privacy
                    privacy={privacy}
                    postId={postId}
                    authorId = {authorId}
                    token={token}
                    userId={userId}
                    actionPostUpdate={this.props.actionPostUpdate}
                  />
                  <Tooltip title="Coming soon" placement="bottom">
                    <IconButton>
                      <MoreVertIcon />
                    </IconButton>
                  </Tooltip>
                </div>
              }
            />
            {/* don't change this component for now */}
            {userId === authorId ? (
              <CardMedia
                className={classes.media}
                component={(thisProp) => (
                  <CardMediaComponent
                    {...thisProp}
                    {...post}
                    token={token}
                    actionPostUpdate={this.props.actionPostUpdate}
                  />
                )}
                image={
                  cover ||
                  'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
                }
              />
            ) : (
              <CardMedia
                className={classes.media}
                image={
                  cover ||
                  'http://ginva.com/wp-content/uploads/2016/08/ginva_2016-08-02_02-30-54.jpg'
                }
              />
            )}
            <CardHeader
              title={
                <Title
                  readOnly={!edit}
                  value={title}
                  onChange={(e) => this.setState({ title: e.target.value })}
                />
              }
              subheader={
                edit || typeof description !== 'undefined' ? (
                  <Description
                    readOnly={!edit}
                    value={description}
                    onChange={(e) =>
                      this.setState({
                        description: e.target.value,
                      })
                    }
                  />
                ) : null
              }
            />
            {edit || typeof post.data !== 'undefined' ? (
              <CardContent>
                <MayashEditor
                  editorState={data}
                  onChange={this.onChange}
                  readOnly={!edit}
                  placeholder={'Content goes here...'}
                  apiPhotoUpload={({ formData }) =>
                    apiPhotoUserCreate({
                      token,
                      userId,
                      postId,
                      formData,
                    })
                  }
                />
              </CardContent>
            ) : null}
            <div className={classes.margin} />
            <CardActions>
              <IconButton aria-label="Add to favorites" disabled>
                <FavoriteIcon />
              </IconButton>
              <IconButton aria-label="Book Marks" disabled>
                <TurnedInIcon />
              </IconButton>
              <IconButton
                aria-label="Comment"
                onClick={this.onClickCommentIcon}
              >
                <Badge badgeContent={commentsSize}>
                  <CommentIcon />
                </Badge>
              </IconButton>
              <div className={classes.flexGrow} />
              <Tooltip title="Share" placement="bottom">
                <IconButton aria-label="Share" disabled>
                  <ShareIcon />
                </IconButton>
              </Tooltip>
            </CardActions>
          </Card>
          {commentBox ? <AsyncComment postId={postId} /> : null}
        </Grid>
        {isSignedIn &&
          authorId === userId && (
            <Button
              fab
              color="accent"
              aria-label="edit"
              className={classes.editButton}
              onClick={edit ? this.onSave : this.onEdit}
            >
              {edit ? <SaveIcon /> : <EditIcon />}
            </Button>
          )}
      </Grid>
    );
  }
}

Article.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,
  post: PropTypes.shape({
    postId: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
    data: PropTypes.object,
  }),

  actionElementGetById: PropTypes.func.isRequired,
  actionPostUpdate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionElementGetById,
      actionPostUpdate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Article),
);
