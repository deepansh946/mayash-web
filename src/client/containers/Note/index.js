/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';

import Card, { CardContent, CardActions, CardHeader } from 'material-ui/Card';

import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

import IconButton from 'material-ui/IconButton';
import DeleteIcon from 'material-ui-icons/Delete';
import CreateIcon from 'material-ui-icons/Create';
import SaveIcon from 'material-ui-icons/Save';
import Dialog, {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog';

import Slide from 'material-ui/transitions/Slide';

import Input from '../../components/Input';

import apiNoteDelete from '../../api/notes/delete';
import apiNoteUpdate from '../../api/notes/update';

import actionNoteDelete from '../../actions/notes/delete';
import actionNoteUpdate from '../../actions/notes/update';

const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  title: {
    textDecoration: 'none',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class Note extends Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      open: false,
      message: '',
      value: '',
    };

    this.onClickDelete = this.onClickDelete.bind(this);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  async onClickDelete() {
    try {
      const { elements, noteId } = this.props;
      const { token, id: userId } = elements.user;

      const res = await apiNoteDelete({ token, userId, noteId });

      const { statusCode, message } = res;

      if (statusCode >= 400) {
        this.setState({
          message: 'Note deletion Failed due to some technical problem',
        });

        setTimeout(() => {
          this.setState({ open: false });
        }, 1500);

        return;
      }

      this.props.actionNoteDelete({ token, userId, noteId });
    } catch (e) {
      console.log(e);
    }
  }

  onEdit() {
    const { edit, value } = this.state;
    const { noteId, notes } = this.props;
    const note = notes[noteId];

    this.setState({
      edit: !edit,
      value: note.title,
    });
  }

  onChange(e) {
    const { value } = e.target;
    this.setState({ value });
  }

  async onSave() {
    try {
      const { value } = this.state;
      const { noteId, elements, notes } = this.props;
      const note = notes[noteId];
      const { noteType } = note;
      const { token, id } = elements.user;
      const res = await apiNoteUpdate({
        token,
        userId: id,
        noteId,
        noteType: 'note',
        title: value,
      });

      const { statusCode, message, error } = res;

      if (statusCode >= 400) {
        this.setState({
          message: 'Note can not be updated',
        });

        setTimeout(() => {
          this.setState({ edit: false });
        }, 1000);

        return;
      }
      this.setState({ message, edit: false });

      this.props.actionNoteUpdate({ noteId, noteType, title: value });
    } catch (e) {
      console.log(e);
    }
  }

  handleClickOpen = () => {
    this.setState({ open: true, message: '' });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, noteId, notes } = this.props;
    const { open, message, edit, value } = this.state;
    const note = notes[noteId];

    if (typeof note === 'undefined') {
      return <div />;
    }
    const { title } = note;

    return (
      <Card className={classes.card}>
        <CardContent>
          <Typography type="title">
            {edit ? (
              <CardHeader
                title={
                  <Input
                    value={value}
                    onChange={this.onChange}
                    placeholder={'Note ...'}
                  />
                }
              />
            ) : (
              title
            )}
          </Typography>
        </CardContent>
        <CardActions>
          <div className={classes.flexGrow} />
          {!edit ? (
            <IconButton aria-label="Update">
              <CreateIcon onClick={this.onEdit} />
            </IconButton>
          ) : (
            <IconButton aria-label="Save">
              <SaveIcon onClick={this.onSave} />
            </IconButton>
          )}
          <IconButton aria-label="Delete">
            <DeleteIcon onClick={this.handleClickOpen} />
          </IconButton>
          <Dialog
            open={open}
            transition={Transition}
            keepMounted
            onClose={this.handleClose}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
          >
            <DialogTitle id="title">{'Are You sure?'}</DialogTitle>
            {message ? (
              <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                  {message}
                </DialogContentText>
              </DialogContent>
            ) : null}
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancel
              </Button>
              <Button onClick={this.onClickDelete} color="primary">
                Delete
              </Button>
            </DialogActions>
          </Dialog>
        </CardActions>
      </Card>
    );
  }
}

Note.propTypes = {
  classes: PropTypes.object.isRequired,

  notes: PropTypes.object.isRequired,
  noteId: PropTypes.number.isRequired,

  actionNoteDelete: PropTypes.func.isRequired,
  actionNoteUpdate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements, notes }) => ({ elements, notes });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionNoteDelete,
      actionNoteUpdate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Note),
);
