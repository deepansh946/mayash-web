/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

import MayashEditor, { createEditorState } from '../../../lib/mayash-editor';

import ErrorPage from '../../components/ErrorPage';

const styles = {
  root: {
    flexGrow: 1,
    paddingTop: '5px',
  },
  title: {
    textAlign: 'center',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  card: {
    borderRadius: '8px',
  },
};

class ResumePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hover: false,
    };
  }

  // shouldComponentUpdate(nextState, nextProps) {
  //   return this.state !== nextState || this.props !== nextProps;
  // }

  onMouseEnter = () => this.setState({ hover: true });
  onMouseLeave = () => this.setState({ hover: false });
  onChange = () => {};

  render() {
    const { classes, elements, match } = this.props;
    const { username } = match.params;
    const { hover } = this.state;

    const u = Object.keys(elements).find(
      (id) => elements[id].username === username,
    );

    const element = elements[u];

    if (!element || element.statusCode >= 300) {
      return <ErrorPage {...element} />;
    }

    if (element.elementType !== 'user') {
      return <ErrorPage />;
    }

    if (!element.resume) {
      return <ErrorPage />;
    }

    return (
      <Grid
        container
        justify="center"
        spacing={0}
        className={classes.root}
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <Grid item xs={11} sm={11} md={11} lg={11} xl={11}>
          <Card raised={hover} className={classes.card}>
            <CardHeader title={'Resume'} className={classes.title} />
            <CardContent>
              <MayashEditor
                editorState={createEditorState(element.resume)}
                onChange={this.onChange}
                readOnly
              />
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

ResumePage.propTypes = {
  classes: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  elements: PropTypes.object.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

export default connect(mapStateToProps)(withStyles(styles)(ResumePage));
