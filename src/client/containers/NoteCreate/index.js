/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Card, { CardHeader, CardActions, CardContent } from 'material-ui/Card';

import withStyles from 'material-ui/styles/withStyles';

import Input from '../../components/Input';

import apiNoteCreate from '../../api/notes/create';
import actionNoteCreate from '../../actions/notes/create';

const styles = (theme) => ({
  root: {
    padding: '1%',
  },
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  gridContainer: {},
  message: {
    color: '#4CAF50',
    textAlign: 'center',
  },
  cardMediaImage: {
    width: '100%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
});

class NoteCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      message: '',
      active: false,
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const { value: note } = e.target;
    this.setState({
      title: note,
    });
  }

  onFocus = () => this.setState({ active: true });

  async onSubmit(e) {
    try {
      const { id, token } = this.props.elements.user;
      const { title } = this.state;

      this.setState({ message: 'Creating note...' });

      const res = await apiNoteCreate({
        token,
        userId: id,
        noteType: 'note',
        title,
      });

      const { statusCode, message, payload } = res;

      if (statusCode >= 400) {
        this.setState({ message });
        return;
      }

      this.props.actionNoteCreate({
        statusCode,
        ...payload,
        title,
        noteType: 'note',
      });

      this.setState({ message, title: '' });

      setTimeout(() => this.setState({ message: '' }), 1000);
    } catch (error) {
      this.setState({
        message:
          'Note creation failed due to some technical problem. Please try ' +
          'agian',
      });
    }
  }

  render() {
    const { classes, elements } = this.props;
    const { title, message, active } = this.state;
    const { username, name } = elements.user;

    return (
      <div className={classes.root}>
        <Card raised className={classes.card}>
          <CardHeader
            avatar={
              <Paper className={classes.avatar} elevation={1}>
                <Link to={`/@${username}`}>
                  <Avatar
                    alt={name}
                    src={'/public/photos/mayash-logo-transparent.png'}
                    className={classes.avatar}
                  />
                </Link>
              </Paper>
            }
            title={
              <Input
                value={title}
                onChange={this.onChange}
                placeholder={'Note ...'}
              />
            }
            className={classes.cardHeader}
            onFocus={this.onFocus}
          />
          {message.length ? (
            <CardContent className={classes.messege}>{message}</CardContent>
          ) : null}
          {active ? (
            <CardActions>
              <div className={classes.flexGrow} />
              <Button raised color="accent" onClick={this.onSubmit}>
                ADD
              </Button>
            </CardActions>
          ) : null}
        </Card>
      </div>
    );
  }
}

NoteCreate.propTypes = {
  classes: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  actionNoteCreate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionNoteCreate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(NoteCreate),
);
