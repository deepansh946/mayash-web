import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import withStyles from 'material-ui/styles/withStyles';

import Menu, { MenuItem } from 'material-ui/Menu';

import IconButton from 'material-ui/IconButton';
import PublicIcon from '../../../lib/mayash-icons/Public';
import PrivateIcon from '../../../lib/mayash-icons/Private';

import apiPostUpdate from '../../api/posts/users/update';

const styles = {
  root: {},
};

class Privacy extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    privacy: PropTypes.bool.isRequired,
    userId: PropTypes.number.isRequired,
    postId: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    actionPostUpdate: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = { anchorEl: null, privacy: props.privacy };
    this.onChangePrivacy = this.onChangePrivacy.bind(this);
  }

  onClickPrivacy = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  async onChangePrivacy() {
    try {
      const { postId, userId, token } = this.props;

      const privacy = !this.state.privacy;

      const { statusCode, message } = await apiPostUpdate({
        userId,
        token,
        postId,
        privacy,
      });

      if (statusCode >= 300) {
        return;
      }

      this.setState({ privacy });
      this.props.actionPostUpdate({ postId, privacy });
    } catch (e) {
      console.log(e);
    }
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl, privacy } = this.state;
    const { classes,authorId,userId } = this.props;

    return (
      <div>
        <IconButton
          aria-owns={anchorEl ? 'change privacy' : null}
          aria-haspopup="false"
          onClick={this.onClickPrivacy}
          disabled={userId !== authorId}
        >
          {privacy ? <PrivateIcon /> : <PublicIcon />}
        </IconButton>
        <Menu
          id="change privacy"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          PaperProps={{
            style: {
              height: 65,
              width: 65,
            },
          }}
        >
          <MenuItem onClick={this.handleClose}>
            <IconButton
              aria-owns={anchorEl ? 'change privacy' : null}
              aria-haspopup="false"
              onClick={this.onChangePrivacy}
            >
              {!privacy ? <PrivateIcon /> : <PublicIcon />}
            </IconButton>
          </MenuItem>
        </Menu>
      </div>
    );
  }
}

export default withStyles(styles)(Privacy);
