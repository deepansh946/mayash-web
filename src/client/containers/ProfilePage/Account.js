/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-fetch';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';

import apiUpdateUsername from '../../api/users/username/update';
import apiValidateUsername from '../../api/users/username/validate';

const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  gridItem: {
    padding: '1%',
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed',
    },
    '&:valid': {
      outline: 'none',
      borderColor: 'green',
      boxShadow: '0 0 10px #9ecaed',
    },
    '&:invalid': {
      outline: 'none',
      borderColor: 'red',
      boxShadow: '0 0 10px #9ecaed',
    },
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

// This regular expression is for Username.
const PATTERN = '(?=^.{3,20}$)^[a-zA-Z][a-zA-Z0-9]*[._-]?[a-zA-Z0-9]+$';

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: props.user.username,
      focus: false,
      isValid: false,
      isAvailable: false,
      error: '',
      message: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onClickApply = this.onClickApply.bind(this);
  }

  async onChange(e) {
    try {
      const username = e.target.value;
      const regex = new RegExp(PATTERN);

      if (!regex.test(username)) {
        this.setState({ username, isValid: false });
        return;
      }
      this.setState({ username, isValid: true });

      if (this.props.user.username === username) {
        return;
      }

      const { statusCode } = await apiValidateUsername({ username });

      this.setState({ isAvailable: statusCode === 200 });
    } catch (error) {
      console.error(error);
    }
  }

  // before submiting make sure to check everything.
  async onSubmit() {
    try {
      const { id: userId, token } = this.props.user;
      const { username } = this.state;

      const { statusCode, message, error } = await apiUpdateUsername({
        token,
        userId,
        username,
      });

      if (statusCode >= 300) {
        this.setState({ message });
        return;
      }

      this.setState({ message });

      setTimeout(() => {
        this.setState({ message: '' });
      }, 1000);

      this.props.userUpdate({ userId, username });
    } catch (error) {
      console.log(error);
    }
  }

  onClickApply() {
    console.log(this.props);
  }

  render() {
    const { classes, disabled } = this.props;
    const { focus, isValid, isAvailable, message, username } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <div
            onMouseEnter={() => this.setState({ hover: true })}
            onMouseLeave={() => this.setState({ hover: false })}
          >
            <Card raised={this.state.hover} className={classes.card}>
              <CardHeader title={'Username'} />
              <CardContent
                onFocus={() => this.setState({ focus: true })}
                onBlur={() =>
                  this.setState({
                    focus: !(this.props.user.username === username),
                  })
                }
                style={{ display: 'flex' }}
              >
                <input
                  placeholder={'Username'}
                  value={username}
                  onChange={this.onChange}
                  minLength={3}
                  maxLength={20}
                  pattern={PATTERN}
                  required
                  disabled={disabled}
                  className={classes.input}
                />
              </CardContent>
              {isAvailable ? (
                <CardContent>Username is already taken.</CardContent>
              ) : null}
              {message.length ? <CardContent>{message}</CardContent> : null}
              {focus && !(disabled === true) ? (
                <CardActions>
                  <div className={classes.flexGrow} />
                  <Button
                    raised
                    color="accent"
                    onClick={this.onSubmit}
                    disabled={!isValid || isAvailable}
                  >
                    Submit
                  </Button>
                </CardActions>
              ) : null}
            </Card>
          </div>
          <CardActions>
            <div className={classes.flexGrow} />
            <Button raised color="accent" onClick={this.onClickApply}>
              Apply for Guru
            </Button>
          </CardActions>
        </Grid>
      </Grid>
    );
  }
}

Account.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    username: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  userUpdate: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

Account.defaultProps = {
  disabled: false,
};

export default withStyles(styles)(Account);
