/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-fetch';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import Form from '../Form';

import apiUserUpdate from '../../api/users/update';

const styles = {
  root: {},
  gridItem: {
    paddingBottom: '1%',
  },
  form: {
    paddingTop: '1%',
  },
};

class Profile extends Component {
  constructor(props) {
    super(props);
    const { id, username, name, description } = props.user;

    this.state = { id, username, name, description, message: '' };

    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { name, username, description } = nextProps.user;
    this.setState({ name, username, description });
  }

  async onSubmit(key, value) {
    try {
      const { id, token } = this.props.user;
      const { statusCode, message, error } = await apiUserUpdate({
        id,
        token,
        name: value,
      });

      if (statusCode >= 300) {
        this.setState({
          message: 'Your request failed due to some technical problem',
        });
        return;
      }

      this.setState({ message });

      this.props.userUpdate({ id, name: value });
    } catch (e) {
      console.log(e);
    }
  }

  render() {
    const { classes } = this.props;
    const { id, name, message } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <div className={classes.form}>
            <Form title={'UserId'} value={id} disabled />
          </div>
          <div className={classes.form}>
            <Form
              title={'Name'}
              keyName={Object.keys({ name })[0]}
              value={name}
              placeholder={'Name'}
              onSubmit={this.onSubmit}
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Bio'}
              placeholder={'Bio Description'}
              value={''}
              disabled
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Linkedin'}
              placeholder={'Linkedin URL'}
              value={''}
              disabled
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Facebook'}
              placeholder={'Facebook URL'}
              value={''}
              disabled
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Google Plus'}
              placeholder={'Google Plus URL'}
              value={''}
              disabled
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Instagram'}
              placeholder={'Instagram URL'}
              value={''}
              disabled
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Twitter'}
              placeholder={'Twitter URL'}
              value={''}
              disabled
            />
          </div>
          <div className={classes.form}>
            <Form
              title={'Skype'}
              placeholder={'Skype Username'}
              value={''}
              disabled
            />
          </div>
        </Grid>
      </Grid>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    username: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  userUpdate: PropTypes.func.isRequired,
};

export default withStyles(styles)(Profile);
