/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetch from 'isomorphic-fetch';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';

const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  gridItem: {
    padding: '1%',
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed',
    },
    '&:valid': {
      outline: 'none',
      borderColor: 'green',
      boxShadow: '0 0 10px #9ecaed',
    },
    '&:invalid': {
      outline: 'none',
      borderColor: 'red',
      boxShadow: '0 0 10px #9ecaed',
    },
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

const PATTERN = '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$';

class Email extends Component {
  constructor(props) {
    super(props);
    const { email } = props.user;

    this.state = {
      email,
      focus: false,
      isValid: false,
      error: '',
      message: '',
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    const { value: email } = e.target;
    const { user } = this.props;

    const regex = new RegExp(PATTERN);

    if (!regex.test(email)) {
      this.setState({ email, isValid: false });
      return;
    }
    this.setState({ email, isValid: true });

    if (user.email === email) {
      return;
    }
    console.log(email);
  }

  onSubmit() {
    const { email } = this.state;
    const { id, token } = this.props.user;
  }

  render() {
    const { classes, disabled, user } = this.props;
    const { focus, isValid, message } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <div
            onMouseEnter={() => this.setState({ hover: true })}
            onMouseLeave={() => this.setState({ hover: false })}
          >
            <Card raised={this.state.hover} className={classes.card}>
              <CardHeader title={'Email'} />
              <CardContent
                onFocus={() => this.setState({ focus: true })}
                onBlur={() =>
                  this.setState({
                    focus: !(user.email === this.state.email),
                  })
                }
                style={{ display: 'flex' }}
              >
                <input
                  placeholder={'Username'}
                  value={this.state.email}
                  onChange={this.onChange}
                  pattern={PATTERN}
                  required
                  disabled
                  className={classes.input}
                />
              </CardContent>
              {focus && !(disabled === true) ? (
                <CardActions>
                  <div className={classes.flexGrow} />
                  <Button
                    raised
                    color="primary"
                    onClick={this.onSubmit}
                    disabled={!isValid}
                  >
                    Submit
                  </Button>
                </CardActions>
              ) : null}
            </Card>
          </div>
        </Grid>
      </Grid>
    );
  }
}

Email.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    name: PropTypes.string,
    email: PropTypes.string,
  }).isRequired,
  disabled: PropTypes.bool.isRequired,
};

Email.defaultProps = {
  disabled: false,
};

export default withStyles(styles)(Email);
