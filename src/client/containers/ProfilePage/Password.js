/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';

import Input from '../../components/Input';

import REGEX_PASSWORD from '../../../lib/mayash-common/RegExp/Password';

import apiPasswordGet from '../../api/users/password/get';
import apiPasswordCreate from '../../api/users/password/create';
import apiPasswordUpdate from '../../api/users/password/update';

const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  gridItem: {
    padding: '1%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  errorMessege: {
    color: 'red',
  },
  cardHeader: {
    textAlign: 'center',
  },
};

class Password extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPasswordExit: false,
      isValid: false,
      error: '',
      message: '',
      oldPassword: '',
      newPassword: '',
      confirmPassword: '',
    };

    this.onChangeOld = this.onChangeOld.bind(this);
    this.onChangeNew = this.onChangeNew.bind(this);
    this.onChangeConfirm = this.onChangeConfirm.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  async componentWillMount() {
    try {
      const { id: userId, token } = this.props.user;
      const { statusCode } = await apiPasswordGet({ userId, token });
      this.setState({ isPasswordExit: statusCode === 200 });
    } catch (error) {
      console.error(error);
    }
  }

  onChangeOld(e) {
    const { value: password } = e.target;
    this.setState({
      oldPassword: password,
    });
  }

  onChangeNew(e) {
    const { value: password } = e.target;
    this.setState({
      newPassword: password,
    });
    if (!REGEX_PASSWORD.test(password)) {
      this.setState({
        message:
          'New Password Should contain atleast one lowercase letter ,' +
          'one upper case letter ,one digit and only one special character. ' +
          'Minimun and maximum length of password is 5 & 20  ',
        isValid: false,
      });
      return;
    }
    this.setState({
      isValid: true,
      message: '',
    });
  }

  onChangeConfirm(e) {
    const { value: password } = e.target;
    this.setState({
      confirmPassword: password,
    });
  }

  async onSubmit() {
    try {
      const {
        oldPassword,
        newPassword,
        confirmPassword,
        isPasswordExit,
      } = this.state;
      const { id: userId, token } = this.props.user;

      if (oldPassword === newPassword) {
        this.setState({ message: 'Your new password is same as old' });
        return;
      }

      if (newPassword !== confirmPassword) {
        this.setState({ message: 'Your Confirmed password is not correct' });
        return;
      }

      this.setState({ message: '' });
      let statusCode;

      if (isPasswordExit) {
        const res = await apiPasswordUpdate({
          token,
          userId,
          oldPassword,
          newPassword,
          confirmPassword,
        });

        statusCode = res.statusCode;
      } else {
        const res = await apiPasswordCreate({
          token,
          userId,
          password: newPassword,
        });

        statusCode = res.statusCode;
      }

      if (statusCode >= 300) {
        this.setState({ message: 'Sorry Unable to process' });
        return;
      }

      this.setState({ message: 'Your password updated successfully' });

      setTimeout(() => {
        this.setState({
          message: '',
          oldPassword: '',
          newPassword: '',
          confirmPassword: '',
          isValid: false,
        });
      }, 1500);
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    const { classes } = this.props;
    const {
      oldPassword,
      isValid,
      confirmPassword,
      newPassword,
      isPasswordExit,
      message,
    } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <Card className={classes.card}>
            <CardHeader
              title={'Change your password'}
              className={classes.cardHeader}
            />
            {isPasswordExit ? (
              <CardContent>
                <Input
                  type={'password'}
                  value={oldPassword}
                  onChange={this.onChangeOld}
                  placeholder={'Enter Old Password'}
                />
              </CardContent>
            ) : null}
            <CardContent>
              <Input
                type={'password'}
                value={newPassword}
                onChange={this.onChangeNew}
                placeholder={'Enter New Password'}
              />
            </CardContent>
            <CardContent>
              <Input
                type={'password'}
                value={confirmPassword}
                onChange={this.onChangeConfirm}
                placeholder={'Confirm New Password'}
              />
            </CardContent>
            {message ? (
              <CardContent className={classes.errorMessege}>
                {message}
              </CardContent>
            ) : null}
            <CardActions>
              <div className={classes.flexGrow} />
              <Button
                raised
                color="accent"
                onClick={this.onSubmit}
                disabled={!(newPassword && confirmPassword && isValid)}
              >
                Submit
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    );
  }
}

Password.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
    username: PropTypes.string,
  }).isRequired,
};

export default withStyles(styles)(Password);
