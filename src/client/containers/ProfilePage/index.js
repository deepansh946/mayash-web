/**
 * profile page component
 * It will render  Account, Billing, Account, Password etc.
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';
import { Route, Switch } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import AppBar from 'material-ui/AppBar';
import Tabs, { Tab } from 'material-ui/Tabs';

import Loading from '../../components/Loading';

import actionElementGet from '../../actions/elements/getById';
import actionUserUpdate from '../../actions/users/update';

const AsyncProfile = Loadable({
  loader: () => import('./Profile'),
  modules: ['./Profile'],
  loading: Loading,
});

const AsyncAccount = Loadable({
  loader: () => import('./Account'),
  modules: ['./Account'],
  loading: Loading,
});

const AsyncBilling = Loadable({
  loader: () => import('./Billing'),
  modules: ['./Billing'],
  loading: Loading,
});

const AsyncEmail = Loadable({
  loader: () => import('./Email'),
  modules: ['./Email'],
  loading: Loading,
});

const AsyncPassword = Loadable({
  loader: () => import('./Password'),
  modules: ['./Password'],
  loading: Loading,
});

const AsyncApplyForGuru = Loadable({
  loader: () => import('./ApplyForGuru'),
  modules: ['./ApplyForGuru'],
  loading: Loading,
});

const AsyncErrorPage = Loadable({
  loader: () => import('../../components/ErrorPage'),
  modules: ['../../components/ErrorPage'],
  loading: Loading,
});

const styles = {
  root: {
    flex: '1 0 100%',
  },
};

// this function will return index of route url in Dashboard component page.
const routeStates = (route = '/profile') => {
  const routes = {
    '/profile': 0,
    '/profile/account': 1,
    '/profile/billing': 2,
    '/profile/email': 3,
    '/profile/password': 4,
  };

  // if routes[index] value is undefined, it will return routes.length;
  return typeof routes[route] !== 'undefined'
    ? routes[route]
    : Object.keys(routes).length;
};

class ProfilePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: routeStates(props.location.pathname),
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(event, value) {
    this.setState({ value });

    const { history } = this.props;

    switch (value) {
      case 0:
        history.push('/profile');
        break;
      case 1:
        history.push('/profile/account');
        break;
      case 2:
        history.push('/profile/billing');
        break;
      case 3:
        history.push('/profile/email');
        break;
      case 4:
        history.push('/profile/password');
        break;
      case 5:
        history.push('/profile/guru');
        break;
      default:
        history.push('/profile');
    }
  }

  render() {
    const { classes, elements } = this.props;
    const user = elements.user;

    const { value } = this.state;

    return (
      <div className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.onChange}
            indicatorColor="primary"
            textColor="primary"
            scrollable
            scrollButtons="auto"
          >
            <Tab label="Profile" />
            <Tab label="Account" />
            <Tab label="Billing" disabled />
            <Tab label="Email" />
            <Tab label="Password" />
            <Tab label="Guru" />
            {/* Not Found tab is hidden and it will be usefull to handle 
                unknown routes. */}
            <Tab label="Not Found" style={{ display: 'none' }} />
          </Tabs>
        </AppBar>
        <Switch>
          <Route
            exact
            path="/profile"
            component={(nextProps) => (
              <AsyncProfile
                {...nextProps}
                user={user}
                userUpdate={this.props.actionUserUpdate}
              />
            )}
          />
          <Route
            exact
            path="/profile/account"
            component={(nextProps) => (
              <AsyncAccount
                {...nextProps}
                user={user}
                userUpdate={this.props.actionUserUpdate}
              />
            )}
          />
          <Route exact path="/profile/billing" component={AsyncBilling} />
          <Route
            exact
            path="/profile/email"
            component={(nextProps) => (
              <AsyncEmail
                {...nextProps}
                user={user}
                userUpdate={this.props.actionUserUpdate}
              />
            )}
          />
          <Route
            exact
            path="/profile/password"
            component={(nextProps) => (
              <AsyncPassword {...nextProps} user={user} />
            )}
          />
          <Route
            exact
            path="/profile/guru"
            component={(nextProps) => (
              <AsyncApplyForGuru {...nextProps} user={user} />
            )}
          />
          <Route component={AsyncErrorPage} />
        </Switch>
      </div>
    );
  }
}

ProfilePage.propTypes = {
  classes: PropTypes.object.isRequired,

  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
  history: PropTypes.object.isRequired,

  elements: PropTypes.object.isRequired,

  actionElementGet: PropTypes.func.isRequired,
  actionUserUpdate: PropTypes.func.isRequired,
};

const mapStateToProps = ({ elements }) => ({ elements });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionElementGet,
      actionUserUpdate,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(ProfilePage),
);
