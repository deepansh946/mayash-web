/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

import TextArea from '../../components/TextArea';
import apiApplyForGuru from '../../api/users/guru/apply';

const styles = {
  root: {},
  card: {
    borderRadius: '8px',
  },
  avatar: {
    borderRadius: '50%',
  },
  gridItem: {
    padding: '1%',
  },
  input: {
    flexGrow: '1',
    border: '2px solid #dadada',
    borderRadius: '7px',
    padding: '8px',
    '&:focus': {
      outline: 'none',
      borderColor: '#9ecaed',
      boxShadow: '0 0 10px #9ecaed',
    },
    '&:valid': {
      outline: 'none',
      borderColor: 'green',
      boxShadow: '0 0 10px #9ecaed',
    },
    '&:invalid': {
      outline: 'none',
      borderColor: 'red',
      boxShadow: '0 0 10px #9ecaed',
    },
  },
  errorMessage: {
    color: 'red',
    padding: '5px',
  },
  successMessage: {
    color: 'green',
    padding: '5px',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
};

class ApplyForGuru extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: '',
      reason: '',
      success: false,
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ reason: e.target.value });
  }
  async onSubmit() {
    try {
      const { reason } = this.state;
      const { userId, token } = this.props.user;

      const { statusCode } = await apiApplyForGuru({ token, userId, reason });
      if (statusCode >= 400) {
        this.setState({
          message:
            'There is any server error while you are applying for guru.' +
            ' Please try again',
        });
        console.log(this.state);
        return;
      }
      this.setState({
        message:
          'You have successfully applied for guru.We willl review you request',
        success: true,
      });
      console.log(this.state);
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes, user } = this.props;
    const { message, success, reason } = this.state;

    const { guru } = user;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid
          item
          xs={12}
          sm={10}
          md={8}
          lg={6}
          xl={6}
          className={classes.gridItem}
        >
          <div
            onMouseEnter={() =>
              this.setState({
                hover: true,
              })
            }
            onMouseLeave={() => this.setState({ hover: false })}
          >
            {!guru ? (
              <Card raised={this.state.hover} className={classes.card}>
                <CardContent>
                  {'Guru feature is already enable to your account'}
                </CardContent>
                <CardContent>{'there is no need to apply again'}</CardContent>
              </Card>
            ) : (
              <Card raised={this.state.hover} className={classes.card}>
                <CardHeader title={'What is guru feature ??'} />
                <CardContent>
                  To become guru you should have full-fill these requirement:
                  <ul>
                    <li>you should have complete your resume</li>
                    <li>Atleast 10 posts were created by you</li>
                    <li>You should have experties in any field</li>
                  </ul>
                  If you are a guru, you can create your own courses
                  <TextArea
                    onChange={this.onChange}
                    disable={false}
                    placeholder="Why do you want to become guru..."
                    value={reason}
                  />
                </CardContent>
                <Typography
                  className={
                    success ? classes.successMessage : classes.errorMessage
                  }
                >
                  {message || null}
                </Typography>
                <CardActions>
                  <div className={classes.flexGrow} />
                  <Button raised color="accent" onClick={this.onSubmit}>
                    Apply For Guru
                  </Button>
                </CardActions>
              </Card>
            )}
          </div>
        </Grid>
      </Grid>
    );
  }
}

ApplyForGuru.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    token: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(ApplyForGuru);
