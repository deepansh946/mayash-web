/**
 * This component is created to edit content of the article
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Link from 'react-router-dom/Link';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';

import IconButton from 'material-ui/IconButton';
import SendIcon from 'material-ui-icons/Send';

import styles from './styles';
import Input from '../../components/Input';

import apiCreateComment from '../../api/posts/comments/create';

import actionCreateComment from '../../actions/posts/comments/create';

class CommentCreate extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    postId: PropTypes.number.isRequired,
    user: PropTypes.object.isRequired,
    actionCreateComment: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      comment: '',
    };
    this.onChange = this.onChange.bind(this);
    this.onClickPost = this.onClickPost.bind(this);
  }

  onChange(e) {
    this.setState({ comment: e.target.value });
  }

  async onClickPost() {
    try {
      const { user, postId } = this.props;
      const { userId, token } = user;
      const { comment } = this.state;

      const { statusCode, payload } = await apiCreateComment({
        token,
        userId,
        postId,
        title: comment,
      });

      if (statusCode >= 300) {
        return;
      }

      this.props.actionCreateComment({ payload, postId });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes, user } = this.props;
    const { comment } = this.state;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Paper className={classes.avatar} elevation={1}>
                  <Link to={`/@${user.username}`}>
                    <Avatar
                      alt={user.name}
                      src={
                        user.avatar ||
                        '/public/photos/mayash-logo-transparent.png'
                      }
                    />
                  </Link>
                </Paper>
              }
              title={
                <Link to={`/@${user.username}`} className={classes.name}>
                  {user.name}
                </Link>
              }
              subheader={
                <Input
                  onChange={this.onChange}
                  placeholder="Write your comment.."
                  value={comment}
                />
              }
              action={
                <IconButton
                  color="accent"
                  className={classes.button}
                  onClick={this.onClickPost}
                >
                  <SendIcon />
                </IconButton>
              }
            />
          </Card>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = ({ elements }) => ({ user: elements.user });

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCreateComment,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CommentCreate),
);
