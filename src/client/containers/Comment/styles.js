const styles = {
  root: {},
  name: {
    cursor: 'pointer',
    textDecoration: 'none',
  },
  avatar: {
    borderRadius: '50%',
  },
  flexGrow: {
    flex: '1 1 auto',
  },
  button: {
    marginTop: '30px',
  },
};

export default styles;
