/**
 * This component will display for creating comment and all the comments
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import uniqBy from 'lodash/uniqBy';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Loadable from 'react-loadable';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';

import styles from './styles';
import Loading from '../../components/Loading';

import apiGetAllComment from '../../api/posts/comments/getAll';
import apiGetAllElement from '../../api/elements/getAll';

import actionGetAllComment from '../../actions/posts/comments/getAll';
import actionGetAllElements from '../../actions/elements/getAll';

const AsyncCommentCreate = Loadable({
  loader: () => import('./Create'),
  modules: ['./Create'],
  loading: Loading,
});

const AsyncCommentTimeline = Loadable({
  loader: () => import('./Timeline'),
  modules: ['./Timeline'],
  loading: Loading,
});

class Comment extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    postId: PropTypes.number.isRequired,
    user: PropTypes.object.isRequired,
    posts: PropTypes.object.isRequired,
    actionGetAllComment: PropTypes.func.isRequired,
    actionGetAllElements: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
  }

  async componentDidMount() {
    try {
      const { user, postId, posts } = this.props;
      const { token } = user;

      if (posts[postId].comments !== undefined) {
        return;
      }

      const { statusCode, payload } = await apiGetAllComment({
        token,
        postId,
      });

      if (statusCode >= 300) {
        return;
      }

      const elements = uniqBy(payload.map((p) => p.authorId));

      const res = await apiGetAllElement({ token, ids: elements });

      /*eslint-disable*/
      if (res.statusCode >= 300) {
        this.setState({
          message:
            'There is any internal problem while loading comments. Please refresh the page',
        });
      }
      /* eslint-enable */

      this.props.actionGetAllComment({ postId, payload });

      this.props.actionGetAllElements({ elements: res.payload });
    } catch (error) {
      console.error(error);
    }
  }

  render() {
    const { classes, postId, posts } = this.props;

    const { comments } = posts[postId];

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          <AsyncCommentCreate postId={postId} />

          {comments !== undefined
            ? Object.keys(comments).map((commentId) => (
              <AsyncCommentTimeline
                key={commentId}
                postId={postId}
                comment={comments[commentId]}
              />
            ))
            : null}
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = ({ posts, elements }) => ({
  posts,
  user: elements.user,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionGetAllComment,
      actionGetAllElements,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(Comment),
);
