/**
 * This component is created to edit content of the article
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Link from 'react-router-dom/Link';

import moment from 'moment';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardHeader } from 'material-ui/Card';
import IconButton from 'material-ui/IconButton';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import Typography from 'material-ui/Typography';

import EditIcon from 'material-ui-icons/Edit';
import SaveIcon from 'material-ui-icons/Save';
import DeleteIcon from 'material-ui-icons/Delete';

import styles from './styles';

import apiCommentDelete from '../../api/posts/comments/delete';

import actionCommentDelete from '../../actions/posts/comments/delete';

class CommentTimeline extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    postId: PropTypes.number.isRequired,
    elements: PropTypes.object.isRequired,
    comment: PropTypes.object,
    actionCommentDelete: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      edit: false,
    };
    this.onChange = this.onChange.bind(this);
    this.onClickEdit = this.onClickEdit.bind(this);
    this.onClickSave = this.onClickSave.bind(this);
    this.onClickDelete = this.onClickDelete.bind(this);
  }

  onChange(e) {
    this.setState({ comment: e.target.value });
  }

  onClickEdit() {
    this.setState({ edit: true });
    console.log('Editing');
  }

  onClickSave() {
    this.setState({ edit: false });
    console.log('Saving');
  }

  async onClickDelete() {
    try {
      const { comment, elements, postId } = this.props;
      const { commentId } = comment;
      const { token, id } = elements.user;

      const { statusCode } = await apiCommentDelete({
        token,
        userId: id,
        postId,
        commentId,
      });

      if (statusCode >= 300) {
        return;
      }

      this.props.actionCommentDelete({ commentId, postId });
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    const { classes, comment, elements } = this.props;
    const { user } = elements;
    const { edit } = this.state;

    if (comment === null) return <div />;

    const { timestamp, title, authorId: commentAuthorId } = comment;

    let commentAuthor;

    if (commentAuthorId === user.id) commentAuthor = user;
    else commentAuthor = elements[commentAuthorId];

    const { avatar, username, name } = commentAuthor;

    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <Grid item xs={12} sm={11} md={12} lg={12} xl={12}>
          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Paper className={classes.avatar} elevation={1}>
                  <Link to={`/@${username}`}>
                    <Avatar
                      alt={name}
                      src={
                        avatar || '/public/photos/mayash-logo-transparent.png'
                      }
                    />
                  </Link>
                </Paper>
              }
              title={
                <Link to={`/@${username}`} className={classes.name}>
                  {name}
                </Link>
              }
              subheader={
                <div>
                  {moment(timestamp).fromNow()}
                  <Typography component="p">{title}</Typography>
                </div>
              }
              action={
                user.id === commentAuthorId ? (
                  <div>
                    <IconButton
                      onClick={!edit ? this.onClickEdit : this.onClickSave}
                    >
                      {!edit ? <EditIcon /> : <SaveIcon />}
                    </IconButton>
                    <IconButton>
                      <DeleteIcon onClick={this.onClickDelete} />
                    </IconButton>
                  </div>
                ) : null
              }
            />
          </Card>
        </Grid>
      </Grid>
    );
  }
}

const mapStateToProps = ({ posts, elements }) => ({
  posts,
  elements,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      actionCommentDelete,
    },
    dispatch,
  );

export default connect(mapStateToProps, mapDispatchToProps)(
  withStyles(styles)(CommentTimeline),
);
