/**
 * Title component is for editable component to edit title with textarea.
 * This component should be used with CardHeader title place.
 * TODO: make textarea autoresizable.
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

const styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit',
  },
};

const Title = ({
  classes,
  readOnly,
  value,
  placeholder,
  onChange,
  minLength,
  maxLength,
}) => {
  if (readOnly) {
    return <div className={classes.value}>{value}</div>;
  }

  return (
    <textarea
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      className={classes.textarea}
      minLength={minLength || 1}
      maxLength={maxLength || 148}
      rows={1}
      readOnly={readOnly}
    />
  );
};

Title.propTypes = {
  classes: PropTypes.object.isRequired,
  readOnly: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  minLength: PropTypes.number,
  maxLength: PropTypes.number,
};

Title.defaultProps = {
  value: 'Untitled',
  placeholder: 'Title goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 148,
};

export default withStyles(styles)(Title);
