/**
 * This is the Error page
 * If  any Error occur while excuting the website then it will be seen
 *
 * @format
 */

import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import CircularProgress from 'material-ui/Progress/CircularProgress';

import ErrorPage from './ErrorPage';

const styles = {
  root: {
    flex: '1 0 100%',
    padding: '1%',
  },
  progress: {},
};

const Error = { error: 'Something went wrong!! please try again..' };

const TimedOut = { message: 'Please try again..' };

const Loading = ({ classes, error, timedOut, pastDelay }) => {
  if (error) {
    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <ErrorPage {...Error} />
      </Grid>
    );
  }

  if (timedOut) {
    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <ErrorPage {...TimedOut} />
      </Grid>
    );
  }

  if (pastDelay) {
    return (
      <Grid container justify="center" spacing={0} className={classes.root}>
        <CircularProgress className={classes.progress} />
      </Grid>
    );
  }

  return (
    <Grid container justify="center" spacing={0} className={classes.root}>
      <CircularProgress className={classes.progress} />
    </Grid>
  );
};

Loading.propTypes = {
  classes: PropTypes.object.isRequired,
  error: PropTypes.any,
  timedOut: PropTypes.any,
  pastDelay: PropTypes.any,
};

export default withStyles(styles)(Loading);
