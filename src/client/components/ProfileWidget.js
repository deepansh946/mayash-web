/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';

import withStyles from 'material-ui/styles/withStyles';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

const styles = {
  root: {
    // padding: '1%',
  },
  cardHeader: {
    textAlign: 'center',
  },
  image: {
    width: '100%',
    height: 'auto',
    borderRadius: '50%',
  },
  link: {
    '&:link': {
      textDecoration: 'none',
      color: 'black',
    },
    '&:visited': {
      textDecoration: 'none',
      color: 'red',
    },
    '&:hover': {
      textDecoration: 'underline',
      color: 'none',
    },
    '&:active': {
      textDecoration: 'underline',
      color: 'none',
    },
  },
};

class ProfileWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes, name, username, avatar } = this.props;

    return (
      <div
        className={classes.root}
        onMouseEnter={() => this.setState({ hover: true })}
        onMouseLeave={() => this.setState({ hover: false })}
      >
        <Card raised={this.state.hover} className={classes.card}>
          <CardContent>
            <img
              alt={name}
              src={avatar || '/public/photos/mayash-logo-transparent.png'}
              className={classes.image}
            />
          </CardContent>
          <CardHeader
            title={
              <Link to={`/@${username}`} className={classes.link}>
                {name}
              </Link>
            }
            subheader={
              <Link to={`/@${username}`} className={classes.link}>
                {`@${username}`}
              </Link>
            }
            className={classes.cardHeader}
          />
        </Card>
      </div>
    );
  }
}

ProfileWidget.defaultProps = {
  name: 'Name',
  username: 'Username',
  avatar: '/public/photos/mayash-logo-transparent.png',
};

ProfileWidget.propTypes = {
  classes: PropTypes.object.isRequired,
  // id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  avatar: PropTypes.string,
};

export default withStyles(styles)(ProfileWidget);
