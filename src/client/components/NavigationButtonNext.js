/**
 * This component is create to show naviagtion button to move next page
 *
 * @format
 */

import React from 'react';
import Link from 'react-router-dom/Link';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';

import NavigateNextIcon from 'material-ui-icons/NavigateNext';

const styles = (theme) => ({
  root: {
    position: 'fixed',
    right: '2%',
    bottom: '4%',
    [theme.breakpoints.down('md')]: {
      right: '2%',
      bottom: '2%',
    },
    [theme.breakpoints.down('sm')]: {
      right: '2%',
      bottom: '1%',
    },
  },
});

/**
 *
 * @param {object} classes
 * @param {path} to
 */
const NavigateButtonNext = ({ classes, to = '/' }) => (
  <div className={classes.root}>
    <Button
      fab
      color="accent"
      component={Link}
      className={classes.button}
      raised
      to={to}
    >
      <NavigateNextIcon />
    </Button>
  </div>
);

NavigateButtonNext.propTypes = {
  classes: PropTypes.object.isRequired,
  to: PropTypes.string.isRequired,
};

NavigateButtonNext.defaultProps = {
  to: '/',
};

export default withStyles(styles)(NavigateButtonNext);
