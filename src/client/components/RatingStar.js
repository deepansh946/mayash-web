/**
 * RatingStar component.
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import IconButton from 'material-ui/IconButton';
import colors from 'material-ui/colors';

import StarIcon from 'material-ui-icons/Star';
import StarBorderIcon from 'material-ui-icons/StarBorder';

/**
 * Star component.
 * @param {Object} param
 * @param {bool} bool - 'value' must be boolean.
 */
const Star = ({ bool }) => (
  <IconButton>
    {bool ? <StarIcon style={{ color: 'lightgreen' }} /> : <StarBorderIcon />}
  </IconButton>
);

Star.propTypes = {
  bool: PropTypes.bool.isRequired,
};

const RatingStar = ({ className, style, disabled, value }) => (
  <div>
    {[0, 0, 0, 0, 0].map((v, i) => <Star key={i + 1} bool={i - value < 0} />)}
  </div>
);

RatingStar.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  max: PropTypes.number,
  value: PropTypes.number,
};

RatingStar.defaultProps = {
  disabled: false,
  max: 5,
  value: 0,
};

export default RatingStar;
