/**
 * This component is only for occupying spaces.
 * It will grow to cover all the spaces it can get.
 *
 * @format
 */

import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import withStyles from 'material-ui/styles/withStyles';

const styles = {
  root: {
    flex: '1 1 auto',
  },
};

const FlexGrow = ({ classes }) => <div className={classes.root} />;

FlexGrow.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FlexGrow);
