/**
 *
 * TODO: use this link to improve this component further.
 * https://codepen.io/hbarve1/pen/VrxZBb?editors=1100
 *
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import withStyles from 'material-ui/styles/withStyles';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

import MenuIcon from 'material-ui-icons/Menu';
import ArrowBackIcon from 'material-ui-icons/ArrowBack';

const styles = {
  root: {
    padding: '1%',
  },
  card: {
    position: 'relative',
    height: '0',
    paddingBottom: 'calc(100% - 16px)',
    marginBottom: '6.6em',
    display: 'block',
    boxSizing: 'border-box',
    // overflow: 'hidden',
  },
  '@keyframes spin': {
    '0%': {
      transform: 'rotate(0deg)',
    },
    '100%': {
      transform: 'rotate(359deg)',
    },
  },
  spin: {
    animation: 'spin 2s infinite linear',
  },
  h2: {
    position: 'absolute',
    top: 'calc(100% - 16px)',
    left: '0',
    width: '100%',
    padding: '10px 0px 10px 16px',
    color: '#fff',
    fontSize: '1.4em',
    lineHeight: '1.6em',
    margin: '0',
    zIndex: '10',
    transition: 'all 0.3s',

    backgroundColor: '#F44336',

    '&:before': {
      content: '""',
      position: 'absolute',
      left: '0',
      top: '-16px',
      width: '0',
      border: '8px solid',
      transition: 'all 0.3s',

      borderTopColor: 'transparent',
      borderRightColor: '#B71C1C',
      borderBottomColor: '#B71C1C',
      borderLeftColor: 'transparent',
    },
    '&:after': {
      content: '""',
      position: 'absolute',
      left: '0',
      // top: '-16px',
      width: '0',
      border: '8px solid',
      transition: 'all 0.3s',

      top: 'auto',
      bottom: '0',

      borderTopColor: '#F44336',
      borderRightColor: '#F44336',
      borderBottomColor: 'transparent',
      borderLeftColor: 'transparent',
    },
  },
  h2Active: {
    top: '0',
    width: 'calc(100% - 80px)',
    paddingTop: '10px',
    paddingRight: '0px',
    paddingBottom: '10px',
    paddingLeft: '90px',

    '&:before': {
      top: '0',

      borderTopColor: 'transparent',
      borderRightColor: '#F44336',
      borderBottomColor: '#F44336',
      borderLeftColor: 'transparent',
    },

    '&:after': {
      bottom: '-16px',

      borderTopColor: '#B71C1C',
      borderRightColor: '#B71C1C',
      borderBottomColor: 'transparent',
      borderLeftColor: 'transparent',
    },
  },
  span: {
    display: 'block',
  },
  strong: {
    fontWeight: '400',
    display: 'block',
    fontSize: '.8em',
  },
  content: {
    position: 'absolute',
    right: '0',
    top: '0',
    bottom: '16px',
    left: '16px',
    // border: '8px solid',
    transition: 'all 0.3s',
  },
  contentActive: {
    paddingTop: '5.6em',

    backgroundColor: '#FFEBEE',
  },
  imageContainer: {
    overflow: 'hidden',
    position: 'absolute',
    left: '0',
    top: '0',
    width: '100%',
    height: '100%',
    zIndex: '3',
    transition: 'all 0.3s',
  },
  imageContainerActive: {
    borderRadius: '50%',
    left: '0',
    top: '12px',
    width: '60px',
    height: '60px',
    zIndex: '20',
  },
  image: {
    display: 'block',
    width: '100%',
    maxWidth: '100%',
    height: 'auto',
  },
  actionButton: {
    position: 'absolute',
    right: '16px',
    top: '15px',
    borderRadius: '50%',
    border: '5px solid',
    width: '54px',
    height: '54px',
    lineHeight: '44px',
    textAlign: 'center',
    color: '#fff',
    cursor: 'pointer',
    zIndex: '20',
    transition: 'all 0.3s',

    backgroundColor: '#F44336',

    '&:hover': {
      backgroundColor: '#B71C1C',
    },
  },
  activeActionButton: {
    top: '62px',

    borderColor: '#FFEBEE',
  },
  description: {
    position: 'absolute',
    top: '100%',
    right: '30px',
    left: '30px',
    bottom: '54px',
    // overflow: 'hidden',
    opacity: '0',
    filter: 'alpha(opacity = 0)',
    transition: 'all 1.2s',
  },
  descriptionActive: {
    top: '50px',
    paddingTop: '5.6em',
    opacity: '1',
    filter: 'alpha(opacity = 100)',
  },
  footer: {
    height: '0',
    overflow: 'hidden',
    transition: 'all 0.3s',
  },
  activeFooter: {
    overflow: 'visible',
    position: 'absolute',
    top: 'calc(100% - 16px)',
    left: '16px',
    right: '0',
    height: '82px',
    paddingTop: '15px',
    paddingLeft: '25px',

    backgroundColor: '#FFCDD2',
  },
  footerH4: {
    position: 'absolute',
    top: '0px',
    left: '30px',
    padding: '0',
    margin: '0',
    fontSize: '16px',
    fontWeight: '700',
    transition: 'all 1.4s',

    color: '#B71C1C',
  },
  activeFooterH4: {
    top: '-32px',
  },
  footerA: {
    display: 'block',
    float: 'left',
    position: 'relative',
    width: '52px',
    height: '52px',
    marginLeft: '5px',
    marginBottom: '15px',
    fontSize: '28px',
    color: '#fff',
    lineHeight: '52px',
    textDecoration: 'none',
    top: '200px',

    backgroundColor: '#B71C1C',
  },
  activeFooterA: {
    top: '0',
  },
};

class TeamMember extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
    };

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.preventDefault();
    const { active } = this.state;
    this.setState({ active: !active });
  }

  render() {
    const {
      classes,
      name,
      avatar,
      role,
      description,
      facebook,
      instagram,
      linkedin,
      twitter,
      email,
    } = this.props;

    const { active } = this.state;

    return (
      <div className={classes.root}>
        <article className={classes.card}>
          <h2
            className={classnames(classes.h2, {
              [`${classes.h2Active}`]: active,
            })}
          >
            <span className={classes.span}>{name}</span>
            <strong className={classes.strong}>{role}</strong>
          </h2>
          <div
            className={classnames(classes.content, {
              [`${classes.contentActive}`]: active,
            })}
          >
            <div
              className={classnames(classes.imageContainer, {
                [`${classes.imageContainerActive}`]: active,
              })}
            >
              <img
                className={classes.image}
                alt={name}
                src={avatar || '/public/photos/mayash-logo-transparent.png'}
              />
            </div>
            <div
              className={classnames(classes.description, {
                [`${classes.descriptionActive}`]: active,
              })}
            >
              {description}
            </div>
          </div>
          <button
            className={classnames(classes.actionButton, {
              [`${classes.activeActionButton}`]: active,
            })}
            onClick={this.onClick}
          >
            <MenuIcon />
          </button>
          <div
            className={classnames(classes.footer, {
              [`${classes.activeFooter}`]: active,
            })}
          >
            <h4
              className={classnames(classes.footerH4, {
                [`${classes.activeFooterH4}`]: active,
              })}
            >
              Social
            </h4>
            <a
              className={classnames(classes.footerA, {
                [`${classes.activeFooterA}`]: active,
              })}
            >
              FB
            </a>
          </div>
        </article>
      </div>
    );
  }
}

TeamMember.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  email: PropTypes.string,
  facebook: PropTypes.string,
  instagram: PropTypes.string,
  twitter: PropTypes.string,
  linkedin: PropTypes.string,
};

TeamMember.defaultProps = {
  avatar: '/public/photos/mayash-logo-transparent.png',
};

export default withStyles(styles)(TeamMember);
