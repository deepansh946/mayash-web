/**
 * This component is create to follow or follower
 * @format
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';

import apiGetFollower from '../api/follow/get';
import apiCreateFollower from '../api/follow/create';
import apiDeleteFollower from '../api/follow/delete';

const styles = (theme) => ({
  root: {
    margin: 'auto',
  },
  button: {
    borderRadius: '15px',
    fontWeight: 'bold',
  },
});

class FollowButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      follow: false,
    };
    this.onClickFollow = this.onClickFollow.bind(this);
    this.onClickUnfollow = this.onClickUnfollow.bind(this);
  }

  async componentWillMount() {
    try {
      const { token, followerId, followingId } = this.props;

      const res = await apiGetFollower({
        token,
        followerId,
        followingId,
      });

      if (res.statusCode < 300) {
        this.setState({ follow: true });
      }
    } catch (error) {
      console.error(error);
    }
  }

  async onClickFollow() {
    try {
      const { token, followerId, followingId } = this.props;

      const { statusCode, error } = await apiCreateFollower({
        token,
        followerId,
        followingId,
      });

      if (statusCode >= 300) {
        return;
      }

      this.setState({ follow: true });
    } catch (error) {
      console.error(error);
    }
  }

  async onClickUnfollow() {
    try {
      const { token, followerId, followingId } = this.props;

      const res = await apiDeleteFollower({
        token,
        followerId,
        followingId,
      });

      if (res.statusCode >= 300) {
        return;
      }

      this.setState({ follow: false });
    } catch (error) {
      console.error(error);
    }
  }
  render() {
    const { classes } = this.props;
    const { follow } = this.state;

    return (
      <div className={classes.root}>
        <Button
          raised
          color={follow ? 'accent' : null}
          onClick={!follow ? this.onClickFollow : this.onClickUnfollow}
          className={classes.button}
        >
          {!follow ? 'Follow' : 'Following'}
        </Button>
      </div>
    );
  }
}

FollowButton.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FollowButton);
