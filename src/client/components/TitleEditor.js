/**
 * For edititng the title
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import { Editor } from 'draft-js';

import withStyles from 'material-ui/styles/withStyles';

/**
 * Define all styles here for PostCreate Component.
 */
const styles = {
  root: {},
};

/**
 * TitleEditor
 * @param {bool} editorState : value must be boolean
 */
const TitleEditor = ({ classes, editorState, onChange, placeholder }) => (
  <div className={classes.root}>
    <Editor
      editorState={editorState}
      onChange={onChange}
      placeholder={placeholder}
    />
  </div>
);

TitleEditor.propTypes = {
  classes: PropTypes.object.isRequired,
  editorState: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
};

export default withStyles(styles)(TitleEditor);
