/**
 * This is the Error page
 * If  any Error occur while excuting the website then it will be seen
 *
 */
import React, { Component } from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card from 'material-ui/Card';

const styles = {
  root: {
    flexGrow: 1,
    padding: '4%',
  },
  cardHeader: {
    textAlign: 'center',
    fontSize: '90px',
  },
  cardHeader1: {
    textAlign: 'center',
  },
};

class ErrorPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { classes, statusCode, error } = this.props;

    if (statusCode === 401) {
      return (
        <div>
          You can not view this post because author have kept this post private
        </div>
      );
    }

    return (
      <Grid container className={classes.root} justify="center">
        <Grid item xs={3} className={classes.cardHeader}>
          <Card>4</Card>
        </Grid>
        <Grid item xs={3} className={classes.cardHeader}>
          <Card>0</Card>
        </Grid>
        <Grid item xs={3} className={classes.cardHeader}>
          <Card>4</Card>
        </Grid>
        <Grid item xs={12} sm={8} className={classes.cardHeader1}>
          <Card>OOPS! SORRY WE CAN'T FIND THAT PAGE!</Card>
        </Grid>
      </Grid>
    );
  }
}

/**
 * This will validate all the incoming props from parent
 * component to AppDrawer component.
 * If there is any invalid prop it will show an error in console.
 * @type {Object}
 */
ErrorPage.propTypes = {
  classes: PropTypes.object.isRequired,
  statusCode: PropTypes.number,
  error: PropTypes.string,
};

/**
 * Error Page component is exported with inline style
 * but in real this will combine both and export a new component
 * without disturbing the previous one (ErrorPage) component
 * API  = withStyles(styles, [options]) => Higher-order Component
 */
export default withStyles(styles)(ErrorPage);
