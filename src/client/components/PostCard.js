/**
 * This component will show either course or post
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';

import Avatar from 'material-ui/Avatar';
import Card, { CardHeader, CardContent, CardActions } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import Button from 'material-ui/Button';
import IconButton from 'material-ui/IconButton';

import FavoriteIcon from 'material-ui-icons/Favorite';
import ShareIcon from 'material-ui-icons/Share';
import TurnedInIcon from 'material-ui-icons/TurnedIn';

/**
 * Define all styles here for PostCard Component.
 */
const styles = {
  root: {},
  flexGrow: {
    flex: '1 1 auto',
  },
};

const PostCard = ({
  classes,
  // id,
  name,
  username,
  avatar,
  // type,
  // postId,
  // courseId,
  title,
  description,
  url,
}) => (
  <div className={classes.root}>
    <Card raised>
      <CardHeader
        avatar={<Avatar alt={name} src={avatar} />}
        title={name}
        subheader={username}
      />
      <CardContent>
        <Typography type="title">{title}</Typography>
        {typeof description === 'string' ? (
          <Typography type="body1" gutterBottom>
            {description}
          </Typography>
        ) : null}
      </CardContent>
      <CardActions>
        <IconButton aria-label="Add to favorites">
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="Book Marks">
          <TurnedInIcon />
        </IconButton>
        <IconButton aria-label="Share">
          <ShareIcon />
        </IconButton>
        <div className={classes.flexGrow} />
        <Button onClick={() => window.open(url, '_blank')}>Learn More</Button>
      </CardActions>
    </Card>
  </div>
);

/**
 * You can define default values for your props by assigning
 * to the special defaultProps property: this will provide
 * a default value if there not passed any props
 */
PostCard.defaultProps = {
  avatar: '/public/photos/mayash-logo-transparent.png',
};

PostCard.propTypes = {
  classes: PropTypes.object.isRequired,
  // id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,

  // 'post', 'course'
  // type: PropTypes.string.isRequired,
  // if type='post' then postId will be there
  // else courseId for type='course'
  // postId: PropTypes.number,
  // courseId: PropTypes.number,

  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  url: PropTypes.string.isRequired,
};

export default withStyles(styles)(PostCard);
