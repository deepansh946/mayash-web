/** @format */

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import withStyles from 'material-ui/styles/withStyles';
import Grid from 'material-ui/Grid';
import Card, { CardContent, CardMedia } from 'material-ui/Card';

import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import Chip from 'material-ui/Chip';

import Facebook from '../../lib/mayash-icons/Facebook';
import Twitter from '../../lib/mayash-icons/Twitter';
import Instagram from '../../lib/mayash-icons/Instagram';
import LinkedIn from '../../lib/mayash-icons/LinkedIn';

const styles = (theme) => ({
  root: {},
  card: {
    display: 'flex',
    borderRadius: '8px',
    '&:hover': {
      transform: 'scale(1.03)',
    },
  },
  chip: {
    width: '8em',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
  },
  cover: {
    width: theme.spacing.unit * 10,
    height: theme.spacing.unit * 10,
    margin: theme.spacing.unit * 2,
    borderRadius: '50%',
    flexShrink: 0,
    transition: '0.2s',
    backgroundColor: theme.palette.background.default,
    '&:hover': {
      transform: 'scale(1.06)',
    },
  },
  controls: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: 10,
  },
  icon: {
    margin: theme.spacing.unit,
    fontSize: 18,
    width: 22,
    height: 22,
    '&:hover': {
      transform: 'scale(1.02)',
    },
  },
});

class TeamMember2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      expanded: false,
    };

    this.handleExpandClick = this.handleExpandClick.bind(this);
  }

  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };

  render() {
    const {
      classes,
      name,
      avatar,
      role,
      description,
      email,
      facebook,
      instagram,
      linkedin,
      twitter,
    } = this.props;

    return (
      <div>
        <Card
          className={classes.card}
          onClick={this.handleExpandClick}
          aria-expanded={this.state.expanded}
        >
          <CardMedia
            className={classes.cover}
            image={avatar || '/public/photos/mayash-logo-transparent.png'}
          />
          <div className={classes.details}>
            <CardContent>
              <Typography type="subheading">{name}</Typography>
              <Typography type="subheading" color="secondary">
                <Chip label={role} className={classes.chip} />
              </Typography>
            </CardContent>
            <div className={classes.controls}>
              <IconButton
                aria-label="facebook"
                component="button"
                className={classes.icon}
                disabled={!facebook}
                onClick={() => {
                  const href = `https://facebook.com/${facebook}`;
                  window.open(href);
                }}
              >
                <Facebook width={'16px'} />
              </IconButton>
              <IconButton
                aria-label="twitter"
                component="button"
                className={classes.icon}
                disabled={!twitter}
                onClick={() => {
                  const href = `https://twitter.com/${twitter}`;
                  window.open(href);
                }}
              >
                <Twitter />
              </IconButton>
              <IconButton
                aria-label="linkedIn"
                component="button"
                className={classes.icon}
                disabled={!linkedin}
                onClick={() => {
                  const href = `https://www.linkedin.com/in/${linkedin}`;
                  window.open(href);
                }}
              >
                <LinkedIn />
              </IconButton>
              <IconButton
                aria-label="instagram"
                component="button"
                className={classes.icon}
                disabled={!instagram}
                onClick={() => {
                  const href = `https://instagram.com/${instagram}`;
                  window.open(href);
                }}
              >
                <Instagram width={'18px'} />
              </IconButton>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

TeamMember2.propTypes = {
  classes: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  avatar: PropTypes.string,
  role: PropTypes.string.isRequired,
  description: PropTypes.string,
  email: PropTypes.string,
  facebook: PropTypes.string,
  twitter: PropTypes.string,
  instagram: PropTypes.string,
  linkedin: PropTypes.string,
};

export default withStyles(styles)(TeamMember2);
