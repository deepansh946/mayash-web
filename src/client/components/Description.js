/**
 * Description component is for editable component to edit description
 * with textarea.
 * This component should be used with CardHeader subheader place.
 * TODO: make textarea autoresizable.
 *
 * @format
 */

import React from 'react';
import PropTypes from 'prop-types';

import { withStyles } from 'material-ui/styles';

const styles = {
  value: {},
  textarea: {
    width: '100%',
    height: 'auto',
    border: 'none',
    // outline: 'none',
    resize: 'none',
    font: 'inherit',
  },
};

const Description = ({
  classes,
  readOnly,
  placeholder,
  value,
  onChange,
  minLength,
  maxLength,
}) => {
  if (readOnly) {
    return <div className={classes.value}>{value}</div>;
  }

  return (
    <textarea
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      className={classes.textarea}
      minLength={minLength || 1}
      maxLength={maxLength || 300}
      rows={1}
      readOnly={readOnly}
    />
  );
};

Description.propTypes = {
  classes: PropTypes.object.isRequired,
  readOnly: PropTypes.bool.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  minLength: PropTypes.number,
  maxLength: PropTypes.number,
};

Description.defaultProps = {
  placeholder: 'Description goes here...',
  readOnly: true,
  minLength: 1,
  maxLength: 300,
};

export default withStyles(styles)(Description);
