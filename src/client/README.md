## Mayash Client

This folder contains all the code related to client side.
Codes are organised in following manner.

**index.js**

  This file is the entry point of React.js app.

**App.js**

  This is just a Head of App.

**AppRoutes.js**

  This Component contains all the routes of Mayash webapp.

**pages**

  This folder contains all the route Components.

**containers**

  This folder contains all the container Components.

**Pure Components**

  These components are kept seperate in __lib__ folder under __mayash-component__ library.

**actions**

  This folder contains all the actions for redux.

**api**

  This contains APIs.

**constants**

  This contains contants for Redux store and operations.

**reducers**

  This contains reducers for Redux store.

**store**

  This folder contains configuration for Redux store.
