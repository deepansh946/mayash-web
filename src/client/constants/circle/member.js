/** @format */

export const CIRCLE_MEMBERS_GET = 'CIRCLE_MEMBERS_GET';
export const CIRCLE_MEMBER_GET = 'CIRCLE_MEMBER_GET';
export const CIRCLE_MEMBER_UPDATE = 'CIRCLE_MEMBER_UPDATE';
export const CIRCLE_MEMBER_DELETE = 'CIRCLE_MEMBER_DELETE';

export const CIRCLE_JOIN = 'CIRCLE_JOIN';
export const CIRCLE_LEAVE = 'CIRCLE_LEAVE';
