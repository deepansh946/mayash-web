/**
 * This file contains action constants for User's update
 *
 * @format
 */

const USER_UPDATE = 'USER_UPDATE';

export default USER_UPDATE;
