/**
 * This file contains action constants for User's circles
 *
 * @format
 */

export const USER_CIRCLES_GET = 'USER_CIRCLES_GET';
