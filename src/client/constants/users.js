/** @format */

export const SIGN_IN = 'SIGN_IN';
export const SIGN_OUT = 'SIGN_OUT';

export const USER_GET_START = 'USER_GET_START';
export const USER_GET_ERROR = 'USER_GET_ERROR';
export const USER_GET_SUCCESS = 'USER_GET_SUCCESS';
export const USER_GET = 'USER_GET';

export const USER_UPDATE = 'USER_UPDATE';
