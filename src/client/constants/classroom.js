/**
 * These are the action that an classroom user can perform
 * It is a good practice to define all tha action as a constant
 *
 * @format
 */

export const CLASSROOM_COURSES_GET_START = 'CLASSROOM_COURSES_GET_START';
export const CLASSROOM_COURSES_GET_SUCCESS = 'CLASSROOM_COURSES_GET_SUCCESS';
export const CLASSROOM_COURSES_GET_ERROR = 'CLASSROOM_COURSES_GET_ERROR';

// All the actions of Classroom's departments are described here.
export const DEPARTMENT_CREATE = 'DEPARTMENT_CREATE';
export const DEPARTMENT_UPDATE = 'DEPARTMENT_UPDATE';
export const DEPARTMENT_DELETE = 'DEPARTMENT_DELETE';
export const DEPARTMENT_GET = 'DEPARTMENT_GET';
export const DEPARTMENT_GET_ALL = 'DEPARTMENT_GET_ALL';

// All the actions of Department's degrees are described here.
export const DEGREE_CREATE = 'DEGREE_CREATE';

// All the actions of Degree's semesters are described here.
export const SEMESTER_ADD = 'SEMESTER_ADD';

// All the actions of Semester's classes are described here.
export const CLASS_ADD = 'CLASS_ADD';
