/**
 * These are the action that an user can perform on their courses
 * It is a good practice to define all tha action as a constant
 *
 * @format
 */

export const COURSE_GET_START = 'COURSE_GET_START';
export const COURSE_GET_SUCCESS = 'COURSE_GET_SUCCESS';
export const COURSE_GET_ERROR = 'COURSE_GET_ERROR';

export const COURSES_GET = 'COURSE_GET';
export const COURSE_GET = 'COURSE_GET';
export const COURSE_CREATE = 'COURSE_CREATE';
export const COURSE_UPDATE = 'COURSE_UPDATE';
export const COURSE_DELETE = 'COURSE_DELETE';

/** Course Modules Contansts */

export const MODULES_GET = 'MODULES_GET';
export const MODULE_GET = 'MODULE_GET';
export const MODULE_CREATE = 'MODULE_CREATE';
export const MODULE_UPDATE = 'MODULE_UPDATE';
export const MODULE_DELETE = 'MODULE_DELETE';

/** Course Discussion Contansts */

export const QUESTIONS_GET = 'QUESTIONS_GET';
export const QUESTION_GET = 'QUESTION_GET';
export const QUESTION_CREATE = 'QUESTION_CREATE';
export const QUESTION_UPDATE = 'QUESTION_UPDATE';
export const QUESTION_DELETE = 'QUESTION_DELETE';

export const ANSWERS_GET = 'ANSWERS_GET';
export const ANSWER_GET = 'ANSWER_GET';
export const ANSWER_CREATE = 'ANSWER_CREATE';
export const ANSWER_UPDATE = 'ANSWER_UPDATE';
export const ANSWER_DELETE = 'ANSWER_DELETE';
