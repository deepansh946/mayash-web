/**
 * These are the action that an user can perform on their notes
 * It is a good practice to define all tha action as a constant
 *
 * @format
 * @flow
 */

export const NOTES_GET: string = 'NOTES_GET';

export const NOTE_CREATE: string = 'NOTE_CREATE';

export const NOTE_GET_START: string = 'NOTE_GET_START';
export const NOTE_GET_SUCCESS: string = 'NOTE_GET_SUCCESS';
export const NOTE_GET_ERROR: string = 'NOTE_GET_ERROR';

export const NOTE_UPDATE: string = 'NOTE_UPDATE';

export const NOTE_DELETE: string = 'NOTE_DELETE';
