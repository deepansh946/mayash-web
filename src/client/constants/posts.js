/**
 * These are the action that an user can perform on their post
 * It is a good practice to define all tha action as a constant
 *
 * @format
 */

export const POST_CREATE = 'POST_CREATE';

export const POSTS_GET = 'POSTS_GET';
export const POSTS_GET_START = 'POSTS_GET_START';
export const POSTS_GET_SUCCESS = 'POSTS_GET_SUCCESS';
export const POSTS_GET_ERROR = 'POSTS_GET_ERROR';

export const POST_GET = 'POST_GET';
export const POST_GET_START = 'POST_GET_START';
export const POST_GET_SUCCESS = 'POST_GET_SUCCESS';
export const POST_GET_ERROR = 'POST_GET_ERROR';

export const POST_UPDATE = 'POST_UPDATE';

export const POST_DELETE = 'POST_DELETE';

export const ADD_TO_FAVORITES = 'ADD_TO_FAVORITES';
