/**
 * These are the action that an element user can perform
 * It is a good practice to define all tha action as a constant
 *
 * @format
 */

export const ELEMENTS_GET = 'ELEMENTS_GET';

export const ELEMENT_GET_START = 'ELEMENT_GET_START';
export const ELEMENT_GET_SUCCESS = 'ELEMENT_GET_SUCCESS';
export const ELEMENT_GET_ERROR = 'ELEMENT_GET_ERROR';
