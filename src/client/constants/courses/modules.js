/** @format */

export const MODULE_CREATE = 'MODULE_CREATE';

export const MODULE_GET = 'MODULE_GET';

export const MODULES_GET = 'MODULES_GET';
