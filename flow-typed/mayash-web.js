/**
 *
 */

export type Id = number;

export type PostId = number;

export type CourseId = number;

export type ModuleId = number;

export type AuthorId = Id;

export type Name = string;

export type Username = string;

export type Email = string;

export type ElementType = 'user' | 'circle';

export type CircleType = 'edu' | 'org' | 'field' | 'location' | 'social';

export type Password = string;

export type Title = string;

export type Description = string;

// This is actually and object type of draft.js raw content.
export type Data = any;

export type Timestamp = string;

export type statusCode = number;

export type error = string;

export type message = string;

export type User = {
  id: Id,
  username: Username,
  email: Email,
  password?: Password,
  name: Name,
  elementType: 'user',
};

export type Circle = {
  id: Id,
  username: Username,
  email: Email,
  name: Name,
  elementType: 'circle',
  circleType: CircleType,
};

export type Element = User | Circle;

export type Post = {
  postId: PostId,
  authorId: AuthorId,
  title: Title,
  description?: Description,
  data?: Data,
  timestamp: Timestamp,
};

export type Course = {
  coursId: CourseId,
  authorId: AuthorId,
  title: Title,
  description?: Description,
  timestamp: Timestamp,
  // syllabus is also a draft.js raw content.
  syllabus?: Data,
  courseModules?: Array<{
    moduleId: ModuleId,
  }>,
};

export type CourseModule = {
  moduleId: ModuleId,
  courseId: CourseId,
  authorId: AuthorId,
  title: Title,
  data: Date,
  timestamp: Timestamp,
};
