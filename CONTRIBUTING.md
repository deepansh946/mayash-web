# Note to Contributors

when you are contributing to development of this project, keep the following things in mind. We are going to follow few rules and guidelines to ensure the ease of development accross the team.

## Prerequisites

* Node.js v8.4.0
* Git
* Docker
* Docker Compose

Note: 

1. To install Node.js use

    https://www.digitalocean.com/community/tutorials/how-to-install-node-js-with-nvm-node-version-manager-on-a-vps

## Folder Structure

    |- README.md
    |- package.json
    |- yarn.lock
    |- app.yaml
    |- .gitignore
    |- Dockerfile.dev
    |- docker-compose.yml
    |- .dockerignore
    |- .babelrc
    |- CONTRIBUTING.md
    |- CHANGELOG
    |- templates
    |   |- index.hbs
    |- public
    |   |- photos
    |   |- build
    |- src
    |   |- server
    |   |- client
    |   |   |- pages
    |   |   |- components
    |   |   |- containers
    |   |   |- constants
    |   |   |- actions
    |   |   |- reducers
    |   |   |- store
    |   |   |- api
    |   |- lib
    |   |   |- mayash-common
    |   |   |- mayash-database
    |   |   |- mayash-auth
    |   |   |- mayash-traffic
    |   |   |- mayash-api
    |   |   |- mayash-api-auth
    |   |   |- mayash-api-elements
    |   |   |- mayash-api-posts
    |   |   |- mayash-api-courses
    |   |   |- mayash-api-classrooms
    |   |   |- mayash-api-photos
    |   |   |- mayash-api-videos
    |   |   |- mayash-api-files
    |   |   |- mayash-api-elements
    |   |   |- mayash-views
    |   |   |- hapi-webpack-dev-server
    |- dist
