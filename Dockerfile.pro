# Base image
FROM node:8.9.0

WORKDIR /app

EXPOSE 9090

ENV IP '0.0.0.0'

ADD package.json /app
ADD yarn.lock /app

RUN yarn install

ADD . /app
