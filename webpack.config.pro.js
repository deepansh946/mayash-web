/** @format */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const DuplicatePackage = require('duplicate-package-checker-webpack-plugin');

module.exports = {
  entry: {
    app: ['./src/client/index.js'],
    vendor: [
      'react',
      'react-dom',
      'react-router',
      'react-router-dom',
      'prop-types',
      'react-redux',
      'redux',
      'redux-thunk',
      'classnames',
      'draft-js',
      'isomorphic-fetch',
      'jss',
      'jss-preset-default',
      // 'jss-theme-reactor',
      'react-loadable',
    ],
  },
  output: {
    path: path.resolve(__dirname, 'public', 'build'),
    publicPath: '/public/build/',
    pathinfo: true,
    filename: '[name].[hash].js',
    chunkFilename: '[name].bundle.[hash].js',
  },
  // devtool: 'source-map',
  module: {
    rules: [
      {
        test: /(\.js)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            babelrc: false,
            presets: ['env', 'stage-0', 'react'],
            plugins: ['transform-runtime'],
            cacheDirectory: true,
          },
        },
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
    }),

    new webpack.optimize.CommonsChunkPlugin({
      minChunks: Infinity,
      name: 'vendor',
      // filename: 'vendor.js',
      filename: 'vendor.[hash].js',
    }),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'meta',
      chunks: ['vendor'],
      // filename: 'meta.js',
      filename: 'meta.[hash].js',
    }),

    new webpack.optimize.UglifyJsPlugin({
      ecma: 5,
      warnings: false,
      parse: {},
      compress: {
        sequences: true,
        dead_code: true,
        drop_debugger: true,
        unsafe: false,
        booleans: true,
        unused: true,
        join_vars: true,
        warnings: false,
      },
      output: { comments: false },
      sourceMap: true,
      beautify: false,
      dead_code: true,
    }),

    new HtmlWebpackPlugin({
      template: './templates/index.hbs',
      inject: 'body',
    }),

    new DuplicatePackage({
      // Also show module that is requiring each duplicate package (default: false)
      verbose: true,
      // Emit errors instead of warnings (default: false)
      emitError: false,
      // Show help message if duplicate packages are found (default: true)
      showHelp: false,
      // Warn also if major versions differ (default: true)
      strict: true,
      /**
       * Exclude instances of packages from the results.
       * If all instances of a package are excluded, or all instances except one,
       * then the package is no longer considered duplicated and won't be emitted as a warning/error.
       * @param {Object} instance
       * @param {string} instance.name The name of the package
       * @param {string} instance.version The version of the package
       * @param {string} instance.path Absolute path to the package
       * @param {?string} instance.issuer Absolute path to the module that requested the package
       * @returns {boolean} true to exclude the instance, false otherwise
       */
      // exclude(instance) {
      //   return instance.name === 'fbjs';
      // },
    }),
  ],
};
