
// .prettierrc.js
/** @format */

module.exports = {
  printWidth: 80,
  parser: 'flow',
  tabWidth: 2,
  useTabs: false,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  bracketSpacing: true,
  jsxBracketSameLine: false,

  arrowParens: 'always',

  // range
  rangeStart: 0,
  rangeEnd: Infinity,

  parser: 'babylon',

  // filepath: './src',

  requirePragma: false,

  insertPragma: false,

  // update this later
  // proseWrap: 'always',
};
